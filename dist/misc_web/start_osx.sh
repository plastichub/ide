#!/usr/bin/env bash
cd -- "$(dirname "$0")"

cd server/nodejs

node start.js --export=false --web=true "$@"

