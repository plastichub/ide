#!/usr/bin/env bash
set -x
DATA_ROOT=data
MONGO_PREFIX=_MONGO
MONGO_DATA=./$DATA_ROOT/$MONGO_PREFIX

MONGO_BIN="./mongod"
#rm "%MONGO_DATA%/mongod.lock"

$MONGO_BIN --quiet --dbpath $MONGO_DATA


