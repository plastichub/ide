#!/usr/bin/env bash

BASEDIR=$(cd $(dirname $0) && pwd)
cd $BASEDIR

cd server/nodejs

node file=export.js --system="../../data" --user="$1" --target="../../exported"

echo "Application exported to ../../exported"