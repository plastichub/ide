#!/usr/bin/env bash

mkdir control-freak;
cd control-freak;
#rm -rf *
wget http://pearls-media.com/control-freak/?ddownload=3807 -O dist.zip
unzip -o dist.zip

sudo apt-get install build-essential
sudo apt-get install gawk
sudo apt-get install libzmq3-dev
sudo apt-get install libVLC
sudo apt-get install mongodb-server
sudo apt-get install libasound2-dev
sudo apt-get install libapache2-mod-php
sudo apt-get install php5-mcrypt
sudo apt-get install php5-curl


cd server/nodejs
npm  install --production --unsafe-perm
