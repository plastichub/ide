setlocal
cd /d %~dp0


SET DATA_ROOT=data
SET MONGO_PREFIX=_MONGO
SET "MONGO_DATA=%DATA_ROOT%\%MONGO_PREFIX%"

SET RUN_HIDDEN="TRUE"

REM taskkill /f /im mongod.exe

set MONGO_BIN="C:\Program Files\MongoDB\Server\3.2\bin\mongod.exe"
ECHO %MONGO_BIN%


REM Delete lock file
del "%MONGO_DATA%\mongod.lock"

REM :: Start Mongo-DB for device server
%MONGO_BIN% --quiet --dbpath %MONGO_DATA% --storageEngine=mmapv1


