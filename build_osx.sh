#!/usr/bin/env bash
cd /Volumes/data/PMaster/projects/x4mm
gitus
cd Code/client/
gitus
cd ../..
cd server-template
gitus
cd ..
cd server/nodejs
git submodule foreach "git reset --hard origin"
gitus
grunt platform
cd dist/osx
gitc
cd ../../
cd ../../
grunt dist-prepare
cd build/electron-template
grunt platform
grunt platform --package=true --dist=true