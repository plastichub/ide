#!/usr/bin/env bash

#grunt build-platform-server --stop=false --platform=osx

##just to get server/nodejs/osx_64
#grunt dist-prepare
if [ ! -d server/nodejs/dist/osx_64/node_modules ]; then
    echo "Cant find node modules in osx ----- incomplete build !!!!"
    #exit;
fi

#grunt build-platform-electron --platform=osx --stop=false
#calls after: grunt update-dist --buildClient=false --platforms=osx --php=false --commit=false
grunt build-platform-installer --platform=osx --stop=false
grunt deploy-installer --platform=osx --stop=false
grunt deploy-zip --stop=false --platform=osx