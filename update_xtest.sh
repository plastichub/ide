#!/usr/bin/env bash

cd Code/client/src
sh buildxtest.sh

cd ../
grunt cssmin:xtest

cd ../../

rm -rf dist/windows/Code/client/src/lib/xtest

cp -rf Code/client/src/lib/xtest dist/windows/Code/client/src/lib/xtest

cd dist/windows
git add -A
gitc "update jsfiddle assets"

