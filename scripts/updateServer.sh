#!/bin/sh


#####################################################################################
#
#   Script for package maintainer only !
#
#   Requirements
#       - Script assumes, that user account has a ssh key on the destination server (http://www.linuxproblem.org/art_9.html).
#       - $SSH_ADMIN_USER is the svn user who has write permissions in all directories
#
#   Remarks :
#       - In case this executed the first time from PHPStorm, its possible, that the ssh certificated isn't yet added
#         to the key ring of the calling user : Run from a terminal once
#       - Its possible the server asks explicitly for the svn user's password.
#

set -x
# set variables
. ./setEnv.sh


echo "Sync Core to branch 1.0"
#sync Core/* to branch
ssh $SSH_ADMIN_USER@$TARGET_HOST "cd httpdocs/x4mm/scripts;sh update.sh"

