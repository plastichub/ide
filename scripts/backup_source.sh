#!/usr/bin/env bash

BACKUP_TARGET=/mnt/anne/backups/x4mm
BACKUP_PREFIX=x4mm
BACKUP_SUFFIX=-$(date +%Y-%m-%d).tar
BACKUP_EXCLUDES=scripts/backup_excludes
BACKUP_LABEL=$1

mkdir -p $BACKUP_TARGET

set -x
tar cvf $BACKUP_TARGET/$BACKUP_PREFIX-$BACKUP_LABEL-$BACKUP_SUFFIX \
--exclude=**node_modules \
--exclude=*.map \
--exclude=*.idea \
--exclude=*.vscode \
--exclude=user/claycenter \
--exclude=user/claycenter2 \
--exclude=*.uncompressed.js \
--exclude=server/nodejs/vscode \
--exclude=server/nodejs/git-module \
--exclude=server/nodejs/vlc-ffi \
--exclude=server/nodejs/tests \
--exclude=server/nodejs/testPackage \
--exclude=server/nodejs/tasks \
--exclude=*.mp4 \
--exclude=*.mp3 \
--exclude=Gruntfile.js \
--exclude-vcs \
export \
data \
documentation \
server-template \
scripts \
server/nodejs \
*.sh \
*.md \
user \
Code/client \
cmd \
dist/all \
dist/conf \
dist/misc 
