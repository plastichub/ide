#!/bin/sh
# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

cd $BASEDIR/..

ROOT_DIR="$(pwd)"


##
#   /lib/xcf
##

if [ ! -d $ROOT_DIR/Code/client/src/lib/xcf ]; then
    echo "missing"
    cd $ROOT_DIR/Code/client/src/lib/
    ln -s ../../../../private/client/src/lib/xcf ./xcf
else
    echo "lib/xcf exists"
fi

if [ ! -d $ROOT_DIR/Code/xapp/xcf ]; then
    cd $ROOT_DIR/Code/xapp
    ln -s ../../private/xapp/xcf/xcf ./xcf
else
    echo "xapp/xcf exists"
fi

if [ ! -d $ROOT_DIR/Code/utils/node_modules/xcfnode ]; then

    cd $ROOT_DIR/Code/utils/node_modules/
    ln -s ../../../private/utils/node_modules/xcfnode ./xcfnode
else
    echo "node_modules/xcfnode exists"
fi

