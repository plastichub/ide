#!/usr/bin/env bash

BACKUP_TARGET=/mnt/anne/backups/x4mm
BACKUP_PREFIX=x4mm
BACKUP_SUFFIX=-$(date +%Y-%m-%d).tar
BACKUP_EXCLUDES=scripts/backup_excludes
BACKUP_LABEL=$1

mkdir -p $BACKUP_TARGET

set -x
tar cvf $BACKUP_TARGET/$BACKUP_PREFIX-$BACKUP_LABEL-$BACKUP_SUFFIX . --exclude-from=$BACKUP_EXCLUDES

