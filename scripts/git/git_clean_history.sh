git checkout --orphan latest_branch
git add -A
git commit -am "reset history"
git branch -D master
git branch -m master
git push -f origin master
git push --set-upstream origin master