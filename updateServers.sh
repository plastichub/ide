#!/usr/bin/env bash
grunt build-platform-server --stop=false --vm=windows
grunt build-platform-server --stop=false --vm=linux_64
grunt build-platform-server --stop=false --vm=linux_32
grunt build-platform-server --stop=false --vm=osx
