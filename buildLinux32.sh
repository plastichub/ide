#!/usr/bin/env bash

#grunt build-platform-server --stop=false --platform=linux_32

if [ ! -d server/nodejs/dist/linux_32/node_modules ]; then
    echo "Cant find node modules in linux_32 ----- incomplete build !!!!"
    exit;
fi
##just to get server/nodejs/osx_64
#grunt dist-prepare

grunt update-dist --platforms=linux_32 --commit=true

grunt build-platform-electron  --stop=false --platform=linux_32
    #calls after: grunt update-dist --buildClient=false --platforms=linux_32

grunt build-platform-installer --stop=false --platform=linux_32

grunt deploy-installer --stop=false --platform=linux_32

cd dist/linux_32
git lfs track ./Control-Freak
git add -A .
gitc "update"
cd ../..
grunt deploy-zip --stop=false --platform=linux_32
