#!/usr/bin/env bash
gitc "$1"
git-module commit-modules --message=$1
git-module commit-modules --profile="control-freak" --message=$1
cd Code/client
sh commit.sh "$1"
