#!/usr/bin/env bash
killall nginx-linux
killall server
killall mongod-linux

node nxappmain/dojoApp.js --user="/PMaster/projects/x4mm/user/"

cp build/server/nxappmain/serverbuild.js.uncompressed.js ../../myapp/server/nodejs_linux/nxappmain/serverbuild.js

cd ../../myapp
mkdir www/Code -p
ln -s ../../../Code/client/ www/Code/client
rm -rf data/_MONGO/*
sh start_linux.sh
