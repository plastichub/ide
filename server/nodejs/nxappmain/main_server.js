require([
	"dojo/node!readline",
	"nxapp/Commons",
	"xide/types",
	"nxapp/utils/TCPUtils",
	"nxappmain/nxAppBase",
	'nxapp/manager/DeviceServerContext',
	'nxapp/client/WebSocket',
	'xlog/Server',
	"dojo/node!winston",
	"dojo/node!path",
	"dojo/node!fs",
	"dojo/node!util",
	"dojo/node!vm",
	'nxapp/manager/MQTTManager',
	'nxapp/Commons',
	'nxapp/utils/FileUtils',
	'nxapp/Logger',
	'nxapp/utils/_console',
	'require',
	"dojo/node!tcp-port-used",
	"dojo/Deferred"
], function (readline, Commons, types, TCPUtils, nxAppBase,
	DeviceServerContext,
	WebSocket, LogServer, winston, path, fs, util, vm, MQTTManager, _Commons, FileUtils, Logger, _console, require, tcpPortUsed, Deferred) {

	global['dojoRequire'] = require;
	global['_'] = _;
	var fileLogOptions = {
		filename: 'all.log',
		dirname: dojoConfig.logRoot,
		json: true
	};

	var logglyOptions = {
		"subdomain": "your-subdomain",
		"inputToken": "13f608cc-4cb7-4734-8e74-a6201211373a",
		"auth": {
			"username": "mc007",
			"password": "213,,asd"
		}
	};

	var logger = new LogServer({});
	logger.start({
		fileLogger: fileLogOptions,
		console: null
	});

	var app = new nxAppBase({
		commander: null
	});

	var bootDfd = new Deferred();
	var HOST, PORT, PROTOCOL;
	app.init();
	var profile = app.profile;

	var commander = app.commander;
	if (commander.diagnose) {
		console.error('run diagnose');
		console.log('loaded modules: ', global.nRequire.cache !== null);
		for (var p in global.nRequire.cache) {
			console.log(p);
		}
		return;
	}
	if (commander.modules) {
		var blacklist = [
			'/nxappmain/server',
			'/nxappmain/dojoConfig',
			'/dojo/dojo',
			'/dojo/_base/configNode'
		];
		var out = "";
		var i = 0;
		for (var p in global.nRequire.cache) {
			var module = p.replace(process.cwd(), '').replace(path.sep + 'node_modules' + path.sep, "").replace('.js', "");
			if (blacklist.indexOf(module) === -1) {
				out += "var a_" + i + " = require(\"" + module + "\");\n";
				i++;
			}

		}
		console.log(out);
		return out;
	}
	if (commander.file) {
		var file = path.resolve(commander.file);
		if (fs.existsSync(file)) {
			var context = new vm.createContext({
				require: require.nodeRequire,
				commander: commander,
				console: console,
				process: process,
				setTimeout: setTimeout,
				global: global
			});
			var content = FileUtils.readFile(file);
			var script = new vm.Script(content);
			script.runInContext(context);
			return;
		} else {
			console.error('file specified but doesnt exists ' + commander.file);
		}
	}


	//replay command line options over profile!
	if (commander.port) {
		profile.socket_server.port = commander.port;
	}
	if (commander.host) {
		profile.socket_server.host = commander.host;
	}

	if (app.commander.info) {
		var options = {
			host: 'http://' + app.profile.socket_server.host,
			port: app.profile.socket_server.port
		};
		console.log(JSON.stringify(options));
		process.exit();
		return;
	}

	var ctx = null;
	try {
		ctx = new DeviceServerContext();
		ctx.profile = profile;
	} catch (e) {
		console.error('error launching ', e);
	}
	try {
		ctx.constructManagers();
		ctx.initManagers(profile);
		ctx.logManager = logger;

		var _MQTTManager = new MQTTManager({
			ctx: ctx,
			profile: profile
		});

		_MQTTManager.init(profile);
		ctx.connectionManager.mqttManager = _MQTTManager;
		ctx.mqttManager = _MQTTManager;
	} catch (e) {
		console.error('launching', e);
	}

	var createSocketServer = profile.common.createSocketServer;
	var hasSocketServer = false;
	var socketServer = null;

	/**
	 * Factories
	 * @returns {nxapp.client.WebSocket}
	 */
	var createClient = function () {

		var socket = new WebSocket();
		socket.init({
			options: {
				host: 'http://' + app.profile.socket_server.host,
				port: app.profile.socket_server.port,
				debug: app.profile.debug

			}
		});
		socket.connect();

		socket.onSignal(types.SIGNAL_RESPONSE, function (data) {

			if (profile.common.stdout) {
				process.stdout.write(data + '\n');
			}

			if (app.commander.oneshot && app.commander.command) {
				process.exit();
			}
		});
		return socket;
	};
	/***
	 * Main
	 */
	var main = function () {
		// Getting host, port and protocol from command line
		if (app.commander.host) {
			HOST = app.commander.host;
		}
		if (app.commander.port) {
			PORT = parseInt(app.commander.port);
		}
		if (app.commander.protocol) {
			PROTOCOL = app.commander.protocol;
		}

		/**
		 * Command line switches
		 */
		//run a driver
		if (app.commander.driver) {

			console.error('run in driver mode');
			var socket = createClient();

			var data = {
				driver: 'xcf/driver/SubTest',
				host: '192.168.1.20',
				port: 23,
				protocol: 'tcp'
			};
			var signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;
			data.manager_command = types.SOCKET_SERVER_COMMANDS.START_DRIVER;
			signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;
			socket.emit(signal, data);
			return;
		}

		/***
		 * Send command to device
		 */
		if (app.commander.send) {

			console.error('sending command');
			console.error('  create web socket client to control server : ' + 'http://' + app.profile.socket_server.host + ':' + app.profile.socket_server.port);

			var socket = createClient();
			var sendSingle = function (cmd) {


				if (socket !== null && cmd != null) {
					var data = {
						command: cmd,
						host: HOST,
						port: PORT,
						protocol: PROTOCOL
					};
					var signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_DEVICE;

					if (cmd == "startDriver") {
						data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_START_DRIVER;
						signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

					} else if (cmd == "status") {
						data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_STATUS;
						signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

					} else if (cmd == "closeall") {
						data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_CLOSE_ALL;
						signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

					} else {
						data.device_command = types.SOCKET_SERVER_COMMANDS.DEVICE_SEND;

					}
					socket.emit(signal, data);
				}
			};

			/***
			 * Device send function
			 * @param cmd
			 */
			var send = function (cmd) {

				if (socket !== null && cmd != null) {
					/***
					 * Support a row of commands
					 */
					if (cmd.indexOf && cmd.indexOf(';') != -1) {

						var commands = cmd.split(';');
						for (var i = 0; i < commands.length; i++) {
							sendSingle(commands[i]);
						}
						return;
					} else {
						sendSingle(cmd);
					}
				}
			};


			var rl = readline.createInterface({
				input: process.stdin,
				output: process.stdout
			});


			rl.on('line', function (cmd) {

				if (cmd != null && cmd.length > 0) {
					console.log("sending stdin : " + cmd);
					sendSingle(cmd);
				}
			});
			if (typeof app.commander.command == 'string') {
				send(app.commander.command);
			}

			console.error('Send command. ^C to close connection and quit');
		}
	};
	var client = null;
	/***
	 * Fulfil dependencies, check we have a socket server
	 */
	if (createSocketServer && !hasSocketServer) {
		function boot() {
			socketServer = TCPUtils.createDeviceServer(profile, ctx);
			ctx.deviceServer = socketServer;
			ctx.logger = winston;
			ctx.deviceServer.logger = winston;
			ctx.logManager = logger;
		}
		tcpPortUsed.check(profile.socket_server.port, profile.socket_server.host).then(function (inUse) {
			if (!inUse) {
				boot();
				try {
					main();
				} catch (e) {
					console.error('error main', e);
				}
			} else {
				console.log('device server already started');
				client = createClient();
			}
		}, function (err) {
			boot();
			try {
				main();
			} catch (e) {
				console.error('error main', e);
			}
		});

	} else {
		try {
			main();
		} catch (e) {
			console.error('app crash!');
		}
	}
	return bootDfd;
});
