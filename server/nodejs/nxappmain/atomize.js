var initModule = "nxappmain/main_atomize";

var libRoot = '../../Code/client/src/lib/';

dojoConfig = {
	hasCache: {
		"host-node": 1,
        "host-browser":0,
        "dojo-sync-loader":1
	},
	trace: 1,
	async: 0,
	baseUrl: ".",
    packages: [
        {
            name: "dojo",
            location: "dojo"
        },
        {
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "nxapp",
            location: "nxapp"
        },
        {
            name: "xapp",
            location: "xapp"
        },
        /*{
            name: "xide",
            location: "xide"
        },*/
        {
            name: "xcf",
            location: libRoot +'xcf'
        },
        {
            name: "xide",
            location: libRoot + 'xide'
        },
        {
            name: "dcl",
            location: libRoot +'dcl'
        },
        {
            name: "xblox",
            location: libRoot + 'xblox'
        },
        {
            name: "xdojo",
            location: libRoot + 'xdojo'
        }
    ],
	deps: [initModule]
	
};

// Load dojo/dojo
require("../dojo/dojo.js");
