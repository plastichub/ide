define([
    "dojo/_base/lang",
    "dojo/_base/declare",
    "nxapp/utils/FileUtils",
    'dojo/Stateful',
    "dojo/node!commander",
    "xide/factory/Events",
    "nxappmain/nxAppBase"
], function (lang, declare, xutils, Stateful, commander, Events, nxAppBase) {

    return declare("nxappmain.nxAppBlox", [nxAppBase],

        {
            profilePath: '/nxappmain/profile_xblox.json',

            init: function () {
                commander
                    .version('0.0.1')
                    .option('-r, --run <string>', 'run blox file')
                    .option('-i, --info', 'return service profile')
                    .option('-P, --port <number>', 'set the device connection port')
                    .option('-H, --host <address>', 'set the device host')
                    .option('-T, --protocol <name>', 'set the device connection protocol')
                    .option('-D, --driver <name>', 'runs a driver')
                    .option('-j, --jhelp', 'output options as json');

                this.commander = commander;

                this.commander.parse(process.argv);

                if (commander.jhelp) {
                    var options = [];
                    commander.options.map(function (option) {
                        options.push(option);
                    });
                    console.log(JSON.stringify(options));
                    process.exit();
                }

                this.profile = this.getProfile(null);

                //now mixin command line options into profile
                if (this.profile != null) {
                    lang.mixin(this.profile, this.commander);
                }
            }
        });
});