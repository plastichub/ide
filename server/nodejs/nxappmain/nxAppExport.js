define([
    "dcl/dcl",
    "nxapp/utils/FileUtils",
    "dojo/node!commander",
    "dojo/node!fs",
    "dojo/node!os",
    "dojo/node!path",
    "xide/factory/Events",
    "dojo/has",
    "xide/model/Base",
    "xide/utils",
    'nxappmain/nxAppBase'
], function (dcl, FileUtils, commander, fs, os,path, Events, has, Base,utils,nxAppBase) {

    has.add('windows', function () {
        return os.platform() === 'win32';
    });

    has.add('osx', function () {
        return os.platform() === 'darwin';
    });

    has.add('linux', function () {
        return os.platform() === 'linux';
    });

    has.add('arm', function () {
        return os.arch() === 'arm';
    });

    return dcl(nxAppBase, {
        declaredClass: "nxappmain.nxAppExport",
        profilePath: '/nxappmain/profile_device_server.json',
        init: function () {

            if (!this.commander) {
                //console.error('have no commander!');
                commander
                    .version('0.0.1')
                    .option('-f, --file <path>', 'run a file')
                    .option('-r, --root <path>', 'root path to Control-Freak')
                    .option('-u, --user <path>', 'user directory')
                    .option('-t, --target <path>', 'target directory')
                    .option('-s, --system <name>', 'path to system scope location')
                    .option('-d, --dist <path>', 'path to the pre-compiled NodeJS servers')
                    .option('--windows <boolean>', 'true/false to export windows NodeJS server')
                    .option('--osx <boolean>', 'true/false to export OSX NodeJS server')
                    .option('--linux32 <boolean>', 'true/false to export Linux-32 Bit NodeJS server')
                    .option('--linux64 <boolean>', 'true/false to export Linux-64 NodeJS server')
                    .option('--arm <boolean>', 'true/false to export ARM-v7 NodeJS server')
                    .option('--client <string>', 'path to client root');
                
                this.commander = commander;
                this.commander.allowUnknownOption(true);
                this.commander.parse(process.argv);
            }

            if (commander.jhelp) {
                var options = [];
                commander.options.map(function (option) {
                    options.push(option);
                });
                console.log(JSON.stringify(options));
                process.exit();
            }

            this.profile = this.getProfile(this.profilePath);


            if(!commander.root){
                this.profile.root = FileUtils.resolve('../..');
            }
            
            if(!commander.target){
                this.profile.target = FileUtils.resolve('../../myapp');
            }


            //now mixin command line options into profile
            utils.mixin(this.profile, this.commander);

            var dataRoot = path.resolve(this.profile.system);

            if (fs.existsSync(dataRoot)) {
                this.profile.system = dataRoot;
            } else {
                dataRoot = process.cwd() + '/data/system';
                this.profile.system = dataRoot;
                commander.system = dataRoot;
            }
        }
    });
});