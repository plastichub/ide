define([
	"dcl/dcl",
	"nxapp/utils/FileUtils",
	"dojo/node!commander",
	"dojo/node!fs",
	"dojo/node!os",
	"dojo/node!path",
	"dojo/node!util",
	"xide/factory/Events",
	"dojo/has",
	"xide/model/Base",
	"xide/utils"
], function (dcl, xutils, commander, fs, os, path, util, Events, has, Base, utils) {

	has.add('windows', function () {
		return os.platform() === 'win32';
	});

	has.add('osx', function () {
		return os.platform() === 'darwin';
	});

	has.add('linux', function () {
		return os.platform() === 'linux';
	});

	has.add('arm', function () {
		return os.arch() === 'arm';
	});

	return dcl(Base.dcl, {
		declaredClass: "nxappmain.nxAppBase",
		profile: null,
		commander: null,
		profilePath: '/nxappmain/profile_device_server.json',
		getDefaultProfilePath: function () {
			var cwd = dojoConfig.cwd;
			return cwd + this.profilePath;
		},
        /**
         *
         * @param path
         * @returns {*}
         */
		getProfile: function (path) {

			var result = {};
			if (!path) {
				//try config from command line
				if (this.commander.profile && this.commander.profile.length > 0) {
					path = this.commander.profile;
				} else {
					path = this.getDefaultProfilePath();
				}
			}

			var cwd = dojoConfig.cwd;
			try {
				if (fs.existsSync(path)) {
				} else {
					path = cwd + '/' + this.profilePath;
				}
			} catch (e) {
				console.error('error reading profile path ');
			}
			var profileContent = xutils.readFile(path);
			if (profileContent) {
				result = dojo.fromJson(profileContent);
			} else {
				console.error('have no profile at : ' + path);
			}


			var data = this.commander.system;
			var dataResolved = data ? xutils.resolve(data) : null;
			if (dataResolved) {
				result.system = dataResolved;
				result.data = dataResolved;
				result.watchr.paths = [
					dataResolved + path.sep + 'system',
					dataResolved + path.sep + 'workspace'
				];
			}

			return result;
		},
		init: function () {
			try {
				if (!this.commander) {
					commander
						.version('0.0.1')
						.option('-i, --info', 'return service profile')
						.option('-f, --file <path>', 'run a file')
						.option('-u, --user <path>', 'user directory')
						.option('--diagnose <boolean>', 'do some diagnose')
						.option('--modules', 'list all modules')
						.option('--start <boolean>', 'start devices')
						.option('--serverSide <boolean>', 'make devices server side')
						.option('-s, --system <name>', 'path to system scope')
						.option('-j, --jhelp', 'output options as json')
						.option('-t, --test', 'enable test mode')
						.option('--mqtt <boolean>', 'enable or disable MQTT functionality');

					this.commander = commander;
					this.commander.allowUnknownOption(true);
					this.commander.parse(process.argv);
				}

				if (commander.jhelp) {
					var options = [];
					commander.options.map(function (option) {
						options.push(option);
					});
					console.log(JSON.stringify(options));
					process.exit();
				}

				this.profile = this.getProfile(this.profilePath);

				//now mixin command line options into profile
				utils.mixin(this.profile, this.commander);

				var cwd = dojoConfig.cwd;
				//console.log('cwd:',cwd);
				//console.log('this:',this.profile);
				if (!this.profile.system) {

				}
				this.profile.system = path.join(cwd, this.profile.system);

				var dataRoot = path.resolve(this.profile.system);
				if (fs.existsSync(dataRoot)) {

				} else {
					dataRoot = process.cwd() + '/data/system';
					this.profile.system = dataRoot;
					commander.system = dataRoot;
				}

				if (this.profile.user) {
					if (fs.existsSync(this.profile.user)) {
						this.profile.user = this.profile.user;
					} else {
						this.profile.user = path.join(cwd, this.profile.user);
					}
					var userRoot = path.resolve(this.profile.user);
					if (fs.existsSync(userRoot)) {
						this.profile.user = userRoot;
					}
				}
				if (this.commander.start) {
					this.profile.start = this.commander.start === 'true';
				}
				if (this.commander.serverSide) {
					this.profile.serverSide = this.commander.serverSide === 'true';
				}
			} catch (e) {
				console.error('error init ', e);
			}
		}
	});
});
