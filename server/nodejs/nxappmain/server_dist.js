

var commander = require('commander'),
    path = require('path'),
    lodash= require('lodash'),
    libRoot = path.resolve('./')+"/",
    clientRoot =path.resolve('../../Code/client/src/'),
    dojoRunner = require('./dojoRunner'),
    defaultProfile = 'nxappmain/profile_xcom.json',
    initModule = "nxappmain/main";


global['_'] = lodash;


dojoConfig = dojoRunner.createDojoConfig(libRoot,path.resolve('./logs'),initModule,null,clientRoot);
dojoConfig.profilePath = defaultProfile;
require("../dojo/dojo.js");
/*
global['dirname']=__dirname+'/';
if(process.platform=='cygwin'){
    module.paths[0]=__dirname +'/../node_modules/win32/';
}

var path = require('path');
var initModule = "nxappmain/main";

var libRoot = path.resolve('../client/src/lib/')+"/";

var dojoRunner = require('./dojoRunner');

dojoConfig = dojoRunner.createDojoConfig(libRoot,path.resolve('./logs'),initModule);


try{

    require("../dojo/dojo.js");

}catch(e){
    console.error('crash in main : ' + e);
	debugger;
}
*/