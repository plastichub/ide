require([
    "dojo/_base/lang",
    "dojo/node!util",
    "dojo/node!readline",
    "dojo/node!commander",
    "xide/debugger/DebuggerServer"

], function(lang,util,readline,commander,DServer)
{

    commander
        .version('0.0.1')
        .option('-d, --debug <app>','Debug an App')
        .option('-s, --start', 'Starts the debugger instance')
        .option('-t, --stop <pid>', 'Stops the debugger instance')
        .option('--web_host <host>','Web host for the debugger webapp')
        .option('--web_port <port>','Web port for the debugger webapp')
        .option('--debug_port <port>','Port for debugging messages')
        .option('--no_live_edit','Disable live edit save')
        .option('--auto_start_server','Starts the debugger instance if not available')
    ;

    commander.parse(process.argv);

    var DS = new DServer;

    DS.init({
        web_port:parseInt(commander.web_port),
        web_host:commander.web_host,
        debug_port:parseInt(commander.debug_port),
        no_live_edit:(commander.no_live_edit!=null),
        auto_start_debug_server:commander.auto_start_server
    });


    if (commander.start) {

        DS.start(function(url,pid) {
            console.log("Server ready in "+url);
            console.log("Process PID: "+pid);
        },
        function(msg) {
            console.error("Error starting server: "+msg);
        });
    }

    if (commander.debug) {

        DS.run(commander.debug,
            function(url,pid,spid) {
                if (spid) {
                    console.log("Server started in "+url);
                    console.log("Debugger PID: "+spid);
                } else {
                    console.log("Server already present in "+url);
                }
                console.log("App process PID: "+pid);
            },
            function(msg) {
                console.error("Error starting module "+commander.debug+": "+msg);
            });
    }

    if (commander.stop) {
        var pid = commander.stop;
        DS.stop(pid);
        console.log("Stop signal sent to "+pid);
    }




});