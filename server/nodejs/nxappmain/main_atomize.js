require([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "nxapp/utils/FileUtils",
    "nxapp/Commons",
    "nxappmain/nxAppBase",
    "nxapp/server/AtomizeServer",
    "dojo/node!commander"
], function(declare,lang,FileUtils,Commons,nxAppBase,AtomizeServer,commander)
{

    // Command line options
    commander
        .version('0.0.1')
        .option('-h, --host <host>','DO server host')
        .option('-p, --port <port>','DO server port')
        .option('-c, --channel <channel>','DO server channel (default:atomize)')
        .option('-i, --info','Returns DO server configuration information')
        .parse(process.argv);

    // Read the profile file
    var app = new nxappmain.nxAppBase({
        profilePath:'/nxappmain/profile_atomize_server.json'
    });
    app.commander = commander;
    var profile =  app.getProfile();

    // Pass the profile options to the Atomize Server
    var options = {
        host:profile.socket_server.host,
        port:profile.socket_server.port,
        channel:profile.socket_server.channel
    };

    var doServer = new AtomizeServer;

    // Override options with the command line sets
    if (commander.host) {
        options.host = commander.host;
    }
    if (commander.port) {
        options.port = commander.port;
    }
    if (commander.channel) {
        options.channel = commander.channel;
    }

    // Atomize server init
    doServer.init(options);

    if (commander.info)
    {
        //return our own run options
        var _options={
            host: 'http://'+options.host,
            port: options.port
        };
        console.log(JSON.stringify(_options));
        process.exit();

    } else {

        // Starts the Atomize server if it's not already running
        doServer.check(function(isON) {
            if (isON) {
                console.log("The DO server is already running in "+doServer.url());
            } else {
                doServer.start();
                console.log("DO server started in "+doServer.url());
            }
        });

    }

});