require([
    "dojo/_base/lang",
    "dojo/node!util",
    "dojo/node!readline",
    "nxapp/Commons",
    "nxapp/types",
    "nxapp/utils/FileUtils",
    "nxapp/utils/TCPUtils",
    "nxappmain/nxAppBase",
    'nxapp/manager/Context',
    'nxapp/manager/FileManager',
    'nxapp/client/WebSocket',
    "dojo/node!dcl",
    "dojo/node!winston",
    "dojo/node!path"
], function(lang,util,readline,Commons,types,fileUtils,TCPUtils,nxAppBase,Context,FileManager,WebSocket,dcl,winston,path)
{






    //console.dir('asdf');
    var app = new nxappmain.nxAppBase({
        profilePath:'/nxappmain/profile_xide_serverm.json'
    });

    app.init();
    var profile = app.profile;

    var ctx = new Context();
    ctx.constructManagers();
    ctx.initManagers(profile);

    var createSocketServer=profile.common.createSocketServer;
    var hasSocketServer=false;
    var socketServer = null;
    /**
     * Factories
     * @returns {nxapp.client.WebSocket}
     */
    var createClient = function(){

        var socket = new WebSocket();
        socket.init({
            options: {
                host: 'http://'+app.profile.socket_server.host,
                port: app.profile.socket_server.port,
                debug: app.profile.debug

            }
        });
        socket.connect();

        socket.onSignal(types.SOCKET_SERVER_COMMANDS.SIGNAL_RESPONSE,function(data) {

            if(profile.common.stdout){

                process.stdout.write(data + '\n');
            }

            if (app.commander.oneshot  && app.commander.command){
                process.exit();
            }
        });
        return socket;
    };

    //return our own run options
    if (app.commander.info) {
        var options={
            host: 'http://'+app.profile.socket_server.host,
            port: app.profile.socket_server.port
        };
        console.log(JSON.stringify(options));
        process.exit();
    }

    /***
     * Main
     */
    var main = function(){


    };
    /***
     * Fulfil dependencies
     */
    //check we have a socket server
    if(createSocketServer && !hasSocketServer){

        var fileManager = new FileManager({ctx:ctx});
        ctx.fileManager=fileManager;

        fileManager.init(app.profile);
        var _checkServerCB = function(error,status,host,port){

            var ok=false;

            //we dont' have a server yet
            if(status=='closed'){
                console.error('have no socket server yet, creating a new one  : ');
                socketServer=TCPUtils.createFileServer(profile,ctx);
                ok=true;
            }else if(status=='open'){
                ok=true;
            }
            if(ok){

                main();
            }
        };
        TCPUtils.checkPort(profile.socket_server.port,profile.socket_server.host,_checkServerCB);
    }else{
        try{
            main();
        }catch(e){
            console.error('app crash!');
            console.dir(e);
        }
    }
});