var commander = require('commander'),
    path = require('path'),
    libRoot = path.resolve('../client/src/lib/')+"/",
    clientRoot =path.resolve('../../Code/client/src/'),
    dojoRunner = require('./dojoRunner'),
    defaultProfile = 'nxappmain/profile_xcom.json',
    initModule = "nxappmain/main_xcom";


commander
    .version('0.1.1')
    .option('-p, --profilePath <name>', 'path to profile',path.resolve(defaultProfile))
    .option('-s, --sourceRoot <name>', 'path to dojo source root',path.resolve(libRoot))
    .option('-c, --clientRoot <name>', 'path to client root',path.resolve(clientRoot))
    .option('-i, --info', 'return service profile')
    .option('-j, --jhelp', 'output options as json');

commander.parse(process.argv);
dojoConfig = dojoRunner.createDojoConfig(libRoot,path.resolve('./logs'),initModule,commander,clientRoot);

/*
/**
 * Create a Dojo config which works with Node.JS
 * Notice that we specify package locations explicitly and relative to 'libRoot'
 */
/*
dojoConfig = {
	cwd:__dirname,
    root:libRoot,
    hasCache: {
		"host-node": 1,
        "host-browser":0,
        "dom":0,
        "dojo-amd-factory-scan":1,
        "dojo-has-api":0,
        "dojo-inject-api":0,
        "dojo-timeout-api":0,
        "dojo-trace-api":0,
        "dojo-log-api":0,
        "dojo-dom-ready-api":0,
        "dojo-publish-privates":0,
        "dojo-config-api":0,
        "dojo-sniff":1,
        "dojo-sync-loader":0,
        "dojo-test-sniff":0,
        "config-deferredInstrumentation":0,
        "config-useDeferredInstrumentation":"report-unhandled-rejections",
        "config-tlmSiblingOfDojo":1

	},
	trace: 1,
	async: 0,
	baseUrl: ".",
    packages: [
        {
            name: "dojo",
            location: "dojo"
        },{
            name: "dijit",
            location: "dijit"
        },{
            name: "dojox",
            location: "dojox"
        },
        {
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "nxapp",
            location: "nxapp"
        },
        {
            name: "xapp",
            location: "xapp"
        },
        {
            name: "xcf",
            location: libRoot +'xcf'
        },
        {
            name: "xide",
            location: libRoot + 'xide'
        },
        {
            name: "xfile",
            location: libRoot + 'xfile'
        },
        {
            name: "dcl",
            location: libRoot +'dcl'
        },
        {
            name: "xblox",
            location: libRoot + 'xblox'
        },
        {
            name: "dijit",
            location: libRoot + 'dijit'
        }

    ],
	deps: [initModule]
};
*/
// Load dojo/dojo
require("../dojo/dojo.js");
