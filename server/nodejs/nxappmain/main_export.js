require([
    "dojo/node!path",
    "dojo/node!fs",
    "dojo/node!util",
    'nxapp/Commons',
    'nxapp/Logger',
    'nxapp/utils/_console',
    'xide/utils',
    "nxappmain/nxAppExport",
    "nxapp/manager/ExportManager",
    "nxapp/model/ExportOptions",
    "xide/types",
    'require'
], function(path,fs,util,_Commons,Logger,_console,utils,nxAppExport,ExportManager,ExportOptions,types,require){

    var debugPaths = true;
    var app = new nxAppExport({
        commander:null
    });
    app.init();
    var profile = app.profile;
    var commander = app.commander;

    if(debugPaths){
        _console.log('export ' + profile.user + ' and system data:  ' +profile.system);
    }
    var exportOptions = new ExportOptions(profile);
    var eManager = new ExportManager();
    eManager.initWithOptions(profile);
});