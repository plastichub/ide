/** @module xaction/DefaultActions **/
define("xaction/DefaultActions", [
    "dcl/dcl",
    'dcl/inherited',
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xlang/i18'
], function(dcl, inherited, declare, types, utils, i18) {
    /**
     * @mixin module:xide/action/DefaultActions
     */
    const Module = declare("xaction/DefaultActions", null, {});
    /**
     *
     * @param title
     * @param command
     * @param group
     * @param icon
     * @param handler
     * @param accelKey
     * @param keyCombo
     * @param keyProfile
     * @param keyTarget
     * @param keyScope
     * @param mixin
     * @returns {{title: *, command: *, group: *, icon: *, handler: *, accelKey: *, keyCombo: *, keyProfile: *, keyTarget: *, keyScope: *}}
     */
    Module.createActionParameters = (
        title,
        command,
        group,
        icon,
        handler,
        accelKey,
        keyCombo,
        keyProfile,
        keyTarget,
        keyScope,
        mixin
    ) => ({
        title: title,
        command: command,
        group: group,
        icon: icon,
        handler: handler,
        accelKey: accelKey,
        keyCombo: keyCombo,
        keyProfile: keyProfile,
        keyTarget: keyTarget,
        keyScope: keyScope,
        mixin: mixin
    });
    /**
     *
     * @param label
     * @param command
     * @param icon
     * @param keycombo
     * @param tab
     * @param group
     * @param filterGroup
     * @param onCreate
     * @param handler
     * @param mixin
     * @param shouldShow
     * @param shouldDisable
     * @param container
     * @returns {*}
     */
    const createAction = (
        label,
        command,
        icon,
        keycombo,
        tab,
        group,
        filterGroup,
        onCreate,
        handler,
        mixin,
        shouldShow,
        shouldDisable,
        container
    ) => {
        if (keycombo) {
            if (_.isString(keycombo)) {
                keycombo = [keycombo];
            }
        }

        mixin = utils.mixin({
            filterGroup: filterGroup || "item|view",
            tab: tab || 'File',
            onCreate: onCreate || (action => {}),
            shouldShow: shouldShow || (() => true),
            shouldDisable: shouldDisable || (() => false)
        }, mixin);

        const _action = Module.createActionParameters(
            label,
            command,
            group || 'File', //Group
            icon, handler || null, "", keycombo, null, container, null, mixin);

        utils.mixin(_action, mixin);

        return _action;
    };

    /**
     * Find action in permission
     * @param what
     * @returns {boolean}
     */
    function hasAction(permissions, what) {
        return _.includes(permissions, what);
    }

    /**
     * After action default handler, trys:
     * - this::onAfterAction
     * - emit onAfterAction
     *
     * @param dfdResult
     * @param event
     * @param action
     * @private
     */
    function _afterAction(dfdResult, event, action) {
        const who = this;
        // call onAfterAction with this results
        let onAfterActionDfd = null;
        who.onAfterAction && (onAfterActionDfd = who.onAfterAction(action, dfdResult, event));

        who._emit && who._emit('onAfterAction', {
            action: action,
            result: dfdResult,
            source: who,
            afterAction: onAfterActionDfd
        });
    }
    /**
     * Default handler, does
     * - try this::runAction || action#handler
     * - call afterAction
     *
     * As last cal
     * @param action {module:xaction/ActionModel}
     * @param event
     */
    function defaultHandler(action, event) {
        let actionDfd;
        const who = this;

        who && who.onBeforeAction && who.onBeforeAction(action);
        if (who.runAction) {
            actionDfd = who.runAction.apply(who, [action, null, event]);
        } else if (action.handler) {
            actionDfd = action.handler.apply(who, [action, null, event]);
        }
        if (actionDfd && actionDfd.then) {
            actionDfd.then(actionResult => {
                _afterAction.apply(who, [actionResult, event, action]);
            });

        } else {
            _afterAction.apply(who, [actionDfd, event, action]);
        }
        return actionDfd;
    }

    /**
     *
     * @param permissions
     * @param grid
     * @param owner
     * @returns {Array}
     */
    function getDefaultActions(permissions, grid, owner) {
        /**
         *
         * @param selection
         * @param reference
         * @param visibility
         * @returns {boolean}
         */
        function shouldDisableDefaultEmptySelection(selection, reference, visibility) {
            selection = selection || grid ? grid.getSelection() : [];

            if (!selection || !selection.length) {
                return true;
            }
            return false;
        }
        /**
         *
         * @param selection
         * @param reference
         * @param visibility
         * @returns {boolean}
         */
        function shouldDisableDefaultFileOnly(selection, reference, visibility) {

            if (shouldDisableDefaultEmptySelection.apply(this, arguments)) {
                return true;
            }
            selection = selection || grid ? grid.getSelection() : [];

            if (selection && selection[0].isDir === true) {
                return true;
            }
            return false;
        }

        const root = 'File/';
        const thiz = this;
        const renderActions = [];
        const VISIBILITY = types.ACTION_VISIBILITY;
        let result = [];
        const ACTION = types.ACTION;
        const ACTION_ICON = types.ACTION_ICON;
        const creator = owner || grid;

        /**
         *
         * @param label
         * @param command
         * @param icon
         * @param keycombo
         * @param tab
         * @param group
         * @param filterGroup
         * @param onCreate
         * @param handler
         * @param mixin
         * @param shouldShow
         * @param shouldDisable
         */
        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {
            let action = null;
            mixin = mixin || {};
            utils.mixin(mixin, {
                owner: owner || grid
            });

            if (mixin.addPermission || hasAction(permissions, command)) {

                handler = handler || defaultHandler;

                action = createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, grid.domNode);

                if (action) {
                    if (owner && owner.addAction) {
                        owner.addAction(null, action);
                    }
                    result.push(action);
                }
            }
        }
        if (hasAction(permissions, ACTION.CLIPBOARD) && grid.getClipboardActions) {
            result.push(creator.createAction({
                label: 'Clipboard',
                command: 'Edit/Clipboard',
                icon: 'fa-clipboard',
                tab: 'Edit',
                group: 'Clipboard',
                mixin: {
                    addPermission: true,
                    dynamic: true,
                    quick: true
                },
                onCreate: function (action) {
                    action.setVisibility(VISIBILITY.RIBBON, {
                        expand: true,
                        tab: "File"
                    });
                }
            }));

            result = result.concat(grid.getClipboardActions(addAction));
        }
        if (hasAction(permissions, ACTION.TOOLBAR) || hasAction(permissions, ACTION.HEADER)) {
            result.push(creator.createAction({
                label: 'Show',
                command: 'View/Show',
                icon: 'fa-eye',
                tab: 'View',
                group: 'Show',
                mixin: {
                    addPermission: true,
                    dynamic: true
                },
                onCreate: function (action) {
                    action.setVisibility(VISIBILITY.RIBBON, {
                        expand: true
                    });
                }
            }));
        }


        if (hasAction(permissions, ACTION.LAYOUT) && grid.getRendererActions) {
            result = result.concat(grid.getRendererActions());
        }

        if (hasAction(permissions, ACTION.COLUMNS) && grid.getColumnHiderActions) {
            result = result.concat(grid.getColumnHiderActions(permissions));
        }
        ///////////////////////////////////////
        //
        //  Open/Edit
        //
        //
        result.push(creator.createAction({
            label: 'Edit',
            command: 'File/Edit',
            icon: ACTION_ICON.EDIT,
            tab: 'Home',
            group: 'Open',
            keycombo: ['f4', 'enter', 'dblclick'],
            mixin: {
                quick: true
            },
            shouldDisable: shouldDisableDefaultFileOnly
        }));


        ///////////////////////////////////////
        //
        //  Organize
        //
        result.push(creator.createAction({
            label: 'Delete',
            command: 'File/Delete',
            icon: ACTION_ICON.DELETE,
            tab: 'Home',
            group: 'Organize',
            keycombo: ['f8', 'delete'],
            mixin: {
                quick: true
            },
            shouldDisable: shouldDisableDefaultEmptySelection
        }));

        addAction('Rename', 'File/Rename', 'fa-edit', ['f2'], 'Home', 'Organize', 'item', null, null, null, null, shouldDisableDefaultEmptySelection);

        result.push(creator.createAction({
            label: 'Reload',
            command: 'File/Reload',
            icon: ACTION_ICON.RELOAD,
            tab: 'Home',
            group: 'File',
            keycombo: ['ctrl l'],
            mixin: {
                quick: true
            }
        }));
        addAction('Create archive', 'File/Compress', ACTION_ICON.COMPRESS, ['ctrl z'], 'Home', 'Organize', 'item|view', null, null, null, null, shouldDisableDefaultEmptySelection);

        ///////////////////////////////////////
        //
        //  File
        //
        addAction('Extract', 'File/Extract', ACTION_ICON.EXTRACT, ['ctrl e'], 'Home', 'File', 'item|view', null, null, null, null, () => //return shouldDisableDefaultFileOnly.apply(this,arguments);
            true);

        result.push(creator.createAction({
            label: 'Download',
            command: 'File/Download',
            icon: ACTION_ICON.DOWNLOAD,
            tab: 'Home',
            group: 'File',
            keycombo: ['ctrl down'],
            mixin: {
                quick: true
            }
        }));

        //////////////////////////////////////////
        //
        //  New
        //
        if (hasAction(permissions, ACTION.NEW_DIRECTORY) || hasAction(permissions, ACTION.NEW_FILE)) {
            addAction('New', 'File/New', 'fa-magic', null, 'Home', 'New', 'item|view', null, null, {}, null, null);
        }
        addAction('New Folder', ACTION.NEW_DIRECTORY, 'fa-folder', ['f7'], 'Home', 'New', 'item|view', null, null, {
            quick: true
        }, null, null);
        addAction('New File', ACTION.NEW_FILE, 'el-icon-file', ['ctrl f4'], 'Home', 'New', 'item|view', null, null, {
            quick: true
        }, null, null);


        //////////////////////////////////////////
        //
        //  Preview
        //
        if (hasAction(permissions, ACTION.PREVIEW)) {
            result.push(creator.createAction({
                label: 'Preview',
                command: 'File/Preview',
                icon: 'fa-eye',
                tab: 'Home',
                group: 'Open',
                keycombo: ['f3'],
                mixin: {
                    quick: true
                },
                shouldDisable: shouldDisableDefaultFileOnly
            }));
        }

        ///////////////////////////////////////
        //
        //  Selection
        //
        if (hasAction(permissions, ACTION.SELECTION)) {
            result.push(createAction('Select', 'File/Select', 'fa-hand-o-up', null, 'Home', 'Select', 'item|view', action => {
                action.setVisibility(VISIBILITY.RIBBON, {
                    expand: true
                });
            }, null, null, null, null, grid.domNode));

            const _mixin = {
                owner: owner || grid
            };

            const container = grid.domNode;

            result.push(createAction('Select all', 'File/Select/All', 'fa-th', ['ctrl a'], 'Home', 'Select', 'item|view', null, () => {
                grid.selectAll();
            }, _mixin, null, null, container));

            result.push(createAction('Select none', 'File/Select/None', 'fa-square-o', 'ctrl d', 'Home', 'Select', 'item|view', null, () => {
                grid.deselectAll();
            }, _mixin, null, null, container));

            result.push(createAction('Invert selection', 'File/Select/Invert', 'fa-square', ['ctrl i'], 'Home', 'Select', 'item|view', null, () => {
                grid.invertSelection();
            }, _mixin, null, null, container));
        }
        return result;
    }

    Module.createAction = createAction;
    Module.hasAction = hasAction;
    Module.getDefaultActions = getDefaultActions;
    Module.defaultHandler = defaultHandler;

    return Module;
});