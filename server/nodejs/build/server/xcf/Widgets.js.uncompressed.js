define("xcf/Widgets", [
    'dojo/_base/declare',
    'xide/widgets/TemplatedWidgetBase',
    'xide/widgets/SelectWidget',
    'xide/widgets/ScriptWidget',
    'xide/widgets/Expression',
    'xide/widgets/ExpressionEditor',
    'xide/widgets/WidgetReference',
    'xide/form/Select',
    'xide/widgets/JSONEditorWidget'
], function (declare){
    return declare("xcf.Widgets", null,{});
});


