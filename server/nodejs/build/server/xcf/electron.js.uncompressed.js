define("xcf/electron", [
    'dcl/dcl',
    'xdojo/has',
    'xide/utils'
],function(dcl,has,utils){

    const electron = has('electron');
    const Module =  dcl(null,{
        declaredClass:"xcf.electron",
        env:function(){
            return utils.fromJson(this.getBrowserWindow().env);
        },
        isDebug:function(){
            return this.env().name === 'development';            
        },
        require:function(){
            return window['eRequire'].apply(null,arguments);
        },
        srequire:function(){
            return this.require('electron').remote.require.apply(null,arguments);
        },
        remote:function(){
            const _require = window['eRequire'];
            const remote = _require('electron').remote;
            return remote;
        },
        getBrowserWindow:function(){
            return this.remote().BrowserWindow;
        },
        getContent:function(item){
            const thiz = this;
            const ctx = thiz.ctx || sctx;
            const resourceManager = ctx.getResourceManager();
            const vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {};

            const os = this.require('os');
            const shell = this.require('electron').shell;
            const path = this.require("path");
            const fs = this.require("fs");

            let mount = item.mount.replace('/','');
            if(!vfsConfig[mount]){
                console.error('open in os failed: have no VFS config for ' + mount);
                return;
            }

            mount = vfsConfig[mount];
            mount = utils.replaceAll(' ','',mount);

            let itemPath = item.path.replace('./','/');
            itemPath = utils.replaceAll('/',path.sep,itemPath);

            let realPath = path.resolve(mount + path.sep + itemPath);//"/PMaster/projects/x4mm/build/electron-template" in debug version;

            realPath = utils.replaceAll(' ','',realPath);

            if(fs.existsSync(realPath)){
                const size = fs.statSync(realPath).size;
                const buf = new Buffer(size);
                const fd = fs.openSync(realPath, 'r');
                if (!size)
                    return "";

                fs.readSync(fd, buf, 0, size, 0);
                fs.closeSync(fd);
                return buf.toString();
            }else{

                console.error('path ' + realPath + ' doesnt exists');
                return null;
            }
        },
        getApp:function () {
            return this.remote().app;
        },
        getAppDir:function(){
            const jet = this.srequire('fs-jetpack');
            const path = this.srequire('path');
            const app = this.getApp();

            if(this.isDebug()) {
                return jet.cwd(path.resolve(app.getAppPath().toString() +'/../../..')).path() + path.sep;
            }else{
                const appDir = jet.cwd(path.resolve(app.getAppPath().toString()));
                return path.resolve(appDir.path() + '/../../') + path.sep;
            }
        }
    });

    /*
    if(electron) {
        var ctx = sctx,
            resourceManager = ctx.getResourceManager(),
            vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {};

        var instance = new Module();
        //console.log(instance.srequire('fs-jetpack'));
        return;
        console.log(instance.getContent({
            mount:"root",
            path:'index.css'
        }))


        //var _require = window['eRequire'];
        //var fs = _require('fs');
        //console.dir(fs);
    }
    */

    return Module;



});

