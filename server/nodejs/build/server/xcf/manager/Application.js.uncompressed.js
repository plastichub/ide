define("xcf/manager/Application", [
    'dcl/dcl',
    'require',
    'xide/manager/Application',
    'xide/utils',
    'xide/types',
    'xdojo/declare',
    'xdojo/has',
    'xdojo/has!xcf-ui?xcf/manager/Application_UI',
    'xdojo/has!xexpression?xexpression/Expression'
], function (dcl, require, Application, utils, types, dDeclare, has, Application_UI, Expression/*,Embedded,ExpressionJavaScript*/) {
    const bases =  0  ? [Application, Application_UI] : Application;
    
    
    /*
    var b = new Promise((resolve, reject) => {
        resolve();
    });
    b.then(()=>{
        console.log('resolve');
    })

    setTimeout(()=>{

    },1000);
    */

    return dcl(bases, {
        declaredClass: "xcf.manager.Application",
        vfsItems: null,
        onReloaded: function () {
        },
        onMountDataReady: function (evt) {
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  @TODO: remove back compat xbox
        //  XFile specifics (FileManager)
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onPluginLoaded: function (data) {
            const name = data.name;
            this.publish(types.EVENTS.ON_PLUGIN_READY, {
                name: name,
                mainView: this.rootView,
                config: this.config,
                ctx: this.ctx,
                panelManager: this,
                fileManager: this.ctx.getFileManager(),
                store: this.store,
                serviceObject: this.ctx.getService()
            }, this);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Main - View related
        //
        /////////////////////////////////////////////////////////////////////////////////////
        _onXBlocksReady: function () {
            const blockManagerClass = 'xfile.manager.BlockManager';
            const blockManagerProto = require('xfile/manager/BlockManager');
            const ctx = this.ctx;
            const self = this;

            if (ctx.blockManager) {
                ctx.blockManager.onReady();
            }
            {
                const _re = require;
                _re(['xcf/manager/BlockManager'], function (bm) {
                    ctx.blockManager = new bm();
                    ctx.blockManager.ctx = ctx;
                    ctx.blockManager.updatePalette = false;
                    ctx.blockManager.init();
                    ctx.blockManager.onReady();
                    ctx.blockManager.serviceObject = ctx.fileManager.serviceObject;
                });
            }
            const _editor = require('xblox/views/BlocksFileEditor');
            ctx.registerEditorExtension('xBlocks', 'xblox', 'fa-play-circle-o', self, true, null, _editor, {
                ctx: self.ctx,
                mainView: self.mainView,
                blockManagerClass: blockManagerClass,
                registerView: true
            });
        }
    });
});
