define("xcf/manager/BlockManager", [
    'dcl/dcl',
    'xide/types',
    'xide/manager/ManagerBase',
    'xblox/manager/BlockManager',
    "xide/lodash"
], function (dcl, types, ManagerBase, BlockManager,_) {
    return dcl([ManagerBase, BlockManager], {
        declaredClass: "xcf.manager.BlockManager",
        //nulled for server mode
        onReady: function () {},
        addDriverFunctions: function (target, source) {
            for (const i in source) {
                if (i === 'constructor' || i === 'inherited') {
                    continue;
                }
                if (_.isFunction(source[i])) {
                    target[i] = source[i];
                }
            }
        },
        /**
         * One time call per blox scope creation. This adds various functions
         * to the blox's owner object. This enables expressions to access the
         * object but also block specific functions like getVariable
         * @param obj
         * @param scope
         * @param owner
         */
        setScriptFunctions: function (obj, scope, owner) {
            const thiz = owner;
            if (!scope.context) {
                scope.context = obj;//set the context of the blox scope
            }
            if (!obj.blockScope) {
                obj.blockScope = scope;
            }
            //add shortcuts to the device
            if (scope.context) {

                if (scope.context.instance) {//no real driver instance!
                    scope.device = scope.context.instance;//but we have a device less instance
                    /*scope.context = scope.context.instance;*/
                    /*this.addDriverFunctions(obj,scope.context.instance);*/

                } else {
                    /*this.addDriverFunctions(obj,scope.context.instance);*/

                    /*
                     scope.device = {//we have a real device : add 'sendMessage'
                     _object: obj,
                     _scope: scope,
                     sendMessage: function (message) {

                     //case when we've been constructed with no real device
                     if (this._scope && this._scope.context && this._scope.context.instance) {
                     this._scope.context.instance.sendMessage(message);
                     return;
                     }
                     //console.log('sending device message : ' + message);
                     //xlog('sending message : ' + message);
                     //xtrace('test');
                     //xtrace('test');
                     //console.trace("Device.Manager.Send.Message : " + message, this);//sending device message
                     }
                     }
                     */
                }
            }
            ///////////////////////////////////////////////////////////////////////////////
            //
            //  Commands
            //
            ///////////////////////////////////////////////////////////////////////////////
            /**
             * Add 'setVariable'
             * @param title
             * @param value
             */
            if (!obj.callCommand) {
                obj.callCommand = function (title) {
                    const _block = this.blockScope.getBlockByName(title);
                    if (_block) {
                        _block.solve(this.blockScope);
                    }
                };
            }
            ///////////////////////////////////////////////////////////////////////////////
            //
            //  Variables
            //
            ///////////////////////////////////////////////////////////////////////////////
            /**
             * Add 'setVariable'
             * @param title
             * @param value
             */
            if (!obj.setVariable) {
                obj.setVariable = function (title, value, save, publish, source) {
                    const _variable = this.blockScope.getVariable(title);
                    if (_variable) {
                        _variable.value = value;
                        _variable.set('value', value);
                    } else {
                        return;
                    }
                    if (publish !== false) {
                        thiz.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, {
                            item: _variable,
                            scope: scope,
                            driver: obj,
                            owner: thiz,
                            save: save === true,
                            source: source || types.MESSAGE_SOURCE.BLOX  //for prioritizing
                        });
                    }
                };
            }
            /**
             * Add getVariable
             * @param title
             */
            if (!obj.getVariable) {
                obj.getVariable = function (title) {
                    const _variable = this.blockScope.getVariable(title);
                    if (_variable) {
                        return _variable.value;
                    }
                    return '';
                };
            }
            this.inherited(arguments);
        },
        onReloaded: function () {
            this.init();
        },
        getDeviceVariablesAsEventOptions: function () {},
        /**
         * Callback for VariableSwitch::getFields('variable').
         *
         * @param evt {object}
         * @param evt.CI {xide/types/ConfigurableInformation}
         * @param evt.owner {xblox/model/variables/Variable} The variable
         */
        onCreateVariableCI: function (evt) {}
    });
});
