define("xcf/manager/DriverManager_Server", [
    'dcl/dcl',
    'dojo/_base/declare',
    'xide/types',
    'xide/utils'
], function(dcl,declare,types,utils){

    function isItemPath(startNeedle,path){
        const _start = startNeedle;
        if (path.indexOf(_start) != -1) {
            const libPath = path.substr(path.indexOf(_start) + (_start.length + 1 ), path.length);
            return libPath;
        }
        return null;
    }

    return dcl(null,{
        declaredClass:'xcf.manager.DriverManager_Server',
        onDriverBlocksChanged:function(dataPath,shortPath){

        },
        onFileChanged:function(evt){
            if(evt.type!=='changed'){
                return;
            }
            if(evt._didb){
                return;
            }
            evt._didb=true;
            let path = utils.replaceAll('\\', '/', evt.path);
            path = utils.replaceAll('//', '/', path);
            path = path.replace(/\\/g,"/");
            const isDriver = isItemPath('system/driver',path);
            if(isDriver && isDriver.indexOf('.xblox')!==-1){
                console.log('driver blocks changed ' + isDriver + ' @ '+ path, evt);
                this.onDriverBlocksChanged(path,isDriver);
            }
        },
        /**
         *
         * @param storeItem
         * @param readyCB
         */
        createDriverInstance:function(storeItem,readyCB){

            const thiz=this;
            const baseDriverPrefix = this.driverScopes['system_drivers'];
            const baseDriverRequire = baseDriverPrefix + 'DriverBase';
            require([baseDriverRequire],function(baseDriver){

                baseDriver.prototype.declaredClass=baseDriverRequire;
                const meta = storeItem['user'];
                const driverPrefix = thiz.driverScopes[storeItem['scope']];
                const driver  = utils.getCIInputValueByName(meta,types.DRIVER_PROPERTY.CF_DRIVER_CLASS);
                if(!driver){
                    console.error('cant find driver class in meta');
                    return;
                }
                let requirePath  = driverPrefix + driver;
                requirePath=requirePath.replace('.js','');
                requirePath=requirePath.replace('./','');
                require([requirePath],function(driverProtoInstance){
                    const baseClass = baseDriver;
                    const driverProto = declare([baseClass],driverProtoInstance.prototype);
                    const driverInstance = new driverProto();
                    driverInstance.baseClass = baseClass.prototype.declaredClass;
                    driverInstance.modulePath = utils.replaceAll('//','/',requirePath);
                    driverInstance.delegate=thiz;
                    storeItem.instance = driverInstance;
                    if(readyCB){
                        readyCB(driverInstance);
                    }
                    try{
                        driverInstance.start();
                    }catch(e){

                    }
                    return driverInstance;
                });
            });
        }
    });
});
