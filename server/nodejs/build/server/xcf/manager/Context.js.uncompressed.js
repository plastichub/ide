/** module:xcf/manager/Context **/
define("xcf/manager/Context", [
    'dcl/dcl',
    'xide/manager/Context',
    'xcf/manager/DriverManager',
    'xcf/manager/DeviceManager',
    'xcf/manager/ProtocolManager',
    'xcf/manager/Application',
    'xcf/manager/LogManager',
    'xide/manager/ResourceManager',
    'xide/manager/PluginManager',
    'xdojo/has'
], function (dcl, Context, DriverManager, DeviceManager, ProtocolManager, Application, LogManager, ResourceManager, PluginManager, has) {
    /**
     * @class module:xcf/manager/Context
     * @extends module:xide/manager/Context
     */
    return dcl([Context], {
        declaredClass: "xcf.manager.Context",
        /***
         * Standard application context, creates & holds all manager instances
         */
        /////////////////////////////////////////////////////////////
        //
        //  Variables
        //
        /////////////////////////////////////////////////////////////
        namespace: 'xcf.manager.',
        driverManager: null,
        deviceManager: null,
        protocolManager: null,
        debugManager: null,
        nodeServiceManager: null,
        blockManager: null,
        /////////////////////////////////////////////////////////////
        //
        //  Getters for managers
        //
        /////////////////////////////////////////////////////////////
        getBlockManager: function () {
            return this.blockManager;
        },
        getProtocolManager: function () {
            return this.protocolManager;
        },
        getDriverManager: function () {
            return this.driverManager;
        },
        getDeviceManager: function () {
            return this.deviceManager;
        },
        getNodeServiceManager: function () {
            return this.nodeServiceManager;
        },
        getDebugManager: function () {
            return this.debugManager;
        },
        getLibraryManager:function(){
            return this.libraryManager;
        },
        constructManagers: function (appClass) {
            const isEditor = this.isEditor();
            const isBrowser =  0 ;
            if (isBrowser) {
                if (!isEditor) {
                    this.driverManager = this.createManager(DriverManager, null);
                    this.deviceManager = this.createManager(DeviceManager, null);
                    this.protocolManager = this.createManager(ProtocolManager, null);
                    this.logManager = this.createManager(LogManager);
                }
                this.pluginManager = this.createManager(PluginManager);
                this.resourceManager = this.createManager(ResourceManager);
                this.application = this.createManager(appClass || Application);
            }
        },
        initManagers: function () {
            const isEditor = this.isEditor();
            if (!isEditor) {
                if ( true ) {
                    this.driverManager.init();
                }
                if ( true ) {
                    this.deviceManager.init();
                }
                if (has('protocols')) {
                    this.protocolManager.init();
                }

                if (has('log')) {
                    this.logManager.init();
                }
            }
            this.resourceManager && this.resourceManager.init();
        },
        getUserDirectory: function () {
            if(this.args && this.args.userDirectory){
                return this.args.userDirectory;
            }
            const resourceManager = this.getResourceManager();
            if (resourceManager) {
                return resourceManager.getVariable("USER_DIRECTORY");
            }
        }
    });
});
