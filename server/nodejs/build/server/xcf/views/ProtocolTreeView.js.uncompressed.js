define("xcf/views/ProtocolTreeView", [
    'xdojo/declare',
    'xide/utils',
    'xide/types',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xaction/DefaultActions',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'xgrid/Search',
    'xaction/types'
], function (declare, utils, types, Grid, TreeRenderer, DefaultActions, KeyboardNavigation, _Widget, Search,ATypes) {
    const ACTION = ATypes.ACTION;
    const gridClass = Grid.createGridClass('ProtocolTreeView', {
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress: false,
            isGroup: function (item) {

                if (item) {
                    return item.isDir === true;
                } else {
                    return false;

                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                };
            },
            postMixInProperties: function () {
                this.collection = this.collection.filter(this.getRootFilter());
                this.columns = this.getColumns();
                this.inherited(arguments);
            },
            runAction: function (action, item) {
                this.inherited(arguments);
            }

        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            WIDGET: {
                CLASS: _Widget
            },
            SEARCH: {
                CLASS: Search
            }
        },
        {
            RENDERER: TreeRenderer
        },
        null,
        null);

    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.HEADER,
        ACTION.SEARCH,
        'File/New Group'
    ];

    return declare('xcf.views.ProtocolTreeView', gridClass, {
        icon: 'fa-file-code-o',
        beanType: types.ITEM_TYPE.PROTOCOL,
        permissions: DefaultPermissions,
        deselectOnRefresh: false,
        showHeader: false,
        toolbarInitiallyHidden: true,
        //attachDirect:true,
        groupOrder: {
            'Clipboard': 110,
            'Device': 105,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'New': 5,
            'Navigation': 3,
            'Select': 0
        },
        startup: function () {
            const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
            this.inherited(arguments);
            this.addActions(_defaultActions);
            const thiz = this;
            this.refresh().then(function () {
                thiz.select([0], null, true, {
                    focus: true
                });
            });
            this.subscribe(types.EVENTS.ON_STORE_CHANGED, function (evt) {
                if (evt.store == thiz.collection) {
                    thiz.refresh();
                }
            });
            thiz.collection.on('update', function () {
                thiz.refresh();
            });
            thiz.collection.on('delete', function () {
                thiz.refresh();
            });

        },
        //////////////////////////////////////////////////////////////////////
        //
        //  Actions - Impl.
        //
        ///////////////////////////////////////////////////////////////
        openItem: function (item, device) {
            if (item && !item.isDir && !item.virtual) {
                this.delegate.openItemSettings(item, device);
            }
        },
        getColumns: function () {
            const thiz = this;
            return [
                {
                    renderExpando: true,
                    label: "Name",
                    field: "name",
                    sortable: false,
                    formatter: function (name, item) {


                        if (!thiz.isGroup(item) && item['user']) {
                            const meta = item['user'].meta;
                            const _in = meta ? utils.getCIInputValueByName(meta, types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE) : null;
                            if (meta) {
                                return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                            } else {
                                return item.name;
                            }
                            return _in;
                        } else {
                            return item.name;
                        }
                    }
                }
            ];
        }
    });
});
