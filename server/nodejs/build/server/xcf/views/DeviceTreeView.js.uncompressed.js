/** @module xcf/views/DeviceTreeView */
define("xcf/views/DeviceTreeView", [
    'xdojo/declare',
    'dojo/has',
    'xide/utils',
    'xide/types',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xaction/DefaultActions',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'xgrid/Search',
    "xgrid/Noob",
    "dojo/Deferred",
    'dojo/promise/all',
    'xdojo/has!xideve?xcf/views/DeviceTreeView_VE'
], function (declare, has, utils, types,
             Grid, TreeRenderer,
             DefaultActions,KeyboardNavigation,_Widget,Search,Noob,
             Deferred,all,DeviceTreeView_VE) {

    const ACTION = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const isVE = has('xideve');
    const veExtras = isVE ? DeviceTreeView_VE : Noob;

    const gridClass = Grid.createGridClass('deviceTreeView',{
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Instance': 3,
                'Window': 2

            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress:false,
            isGroup:function(item){
                if(item){
                    return item.isDir===true;
                }else{
                    return false;
                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                };
            },
            __set:function(key,value){
                if(key==='title'){
                    console.log('set title');
                    return;
                }
                return this.inherited(arguments);
            },
            postMixInProperties: function() {
                const thiz = this;
                this.collection = this.getDefaultCollection();
                function getIconClass(item) {
                    if(item.iconClass){
                        return item.iconClass;
                    }
                    const opened = true;
                    const iclass = (!item || item.isDir) ? (opened ? "dijitFolderOpened" : "dijitFolderClosed") : "dijitLeaf";

                    //commands or variables
                    if (item.ref && item.ref.item && item.ref.item.getIconClass) {
                        const _clz = item.ref.item.getIconClass();
                        if (_clz) {
                            return _clz;
                        }
                    } else if (!thiz.isGroup(item)) {
                        return thiz.delegate.getDeviceStatusIcon(item);
                    }

                    if (utils.toString(item.name) === 'Commands') {
                        return 'el-icon-video';
                    }
                    return iclass;
                }

                this.collection = this.collection.filter(this.getRootFilter());
                this.columns=[
                    {
                        renderExpando: true,
                        label: "Name",
                        field: "name",
                        sortable: true,
                        formatter: function (name, item) {

                            const meta = item['user'];


                            if(item.isDevice){
                                return '<span class="grid-icon ' + getIconClass(item) + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            }

                            if(item.iconClass){
                                return '<span class="grid-icon ' + item.iconClass + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            }

                            if (thiz.isGroup(item)) {
                                return '<span class="grid-icon ' + 'fa-folder' + '"></span>' + item.name;
                            }

                            if (item.icon) {
                                return '<span class="grid-icon ' + item.icon + '"></span>' + item.name;
                            }

                            //commands or variables
                            if (item.ref && item.ref.item) {
                                //@TODO why is here a difference ?
                                if (item.ref.item.name) {
                                    return item.ref.item.name;
                                }
                                if (item.ref.item.title) {
                                    return item.ref.item.title;
                                }
                            }

                            //true device
                            if (!thiz.isGroup(item)) {
                                if (meta) {
                                    return '<span class="grid-icon ' + getIconClass(item) + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                                } else {
                                    return item.name;
                                }
                            } else {
                                return item.name;
                            }
                        }
                    }
                ];

                this.inherited(arguments);
                this._on('onAddAction',function(action){
                    if(action.command === types.ACTION.EDIT){
                        action.label = 'Settings';
                    }
                });
            },
            /**
             * Override render row to add additional CSS class
             * for indicating a block's enabled state
             * @param object
             * @returns {*}
             */
            renderRow:function(object){
                const res = this.inherited(arguments);
                const ref = object.ref;
                const item = ref ? ref.item  : null;


                if(item!=null && item.scope && item.enabled===false){
                    $(res).addClass('disabledBlock');
                }
                return res;
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR:types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            WIDGET:{
                CLASS:_Widget
            },
            SEARCH:{
                CLASS:Search
            },
            EXTRAS:{
                CLASS:veExtras
            }

        },
        {
            RENDERER: TreeRenderer
        },
        null,
        null);

    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.HEADER,
        ACTION.SEARCH,

        'File/ConnectDevice',
        'File/DisconnectDevice',
        'File/New Group',
        'File/New Device',
        'Instance/EditInstance',
        'File/Edit Source',
        'File/Edit Driver',
        'File/Edit Driver Code',
        'Instance/Edit Variables',
        'Instance/Edit Commands',
        'Instance/Show Log',
        'Instance/Console',
        'File/Enable',
        'Instance/ConsoleWizard',
        'View/Source',
        ACTION.CONTEXT_MENU
    ];

    /**
     * Device tree view
     * @class module:xcf/views/DeviceTreeView
     * @extends module:xide/widgets/_Widget
     * @extends module:xgrid/Base
     */
    const Module = declare('xcf.views.DeviceTreeView',gridClass,{
        iconClass: 'fa-sliders',
        beanType: 'device',
        permissions : DefaultPermissions,
        deselectOnRefresh: false,
        showHeader: false,
        toolbarInitiallyHidden:true,
        //attachDirect:true,
        contextMenuArgs:{
            actionFilter:{
                quick:true
            }
        },
        groupOrder: {
            'Clipboard': 110,
            'Device':105,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'New': 5,
            'Navigation': 3,
            'Select': 0
        },
        /**
         *
         * @param container
         * @param permissions
         * @param grid
         * @returns {Array}
         */
        getDeviceActions: function (container,permissions,grid) {
            const actions = [];
            const thiz = this;
            const delegate = thiz.delegate;
            const ACTION = types.ACTION;

            function _selection(){
                const selection = grid.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                if(!item){
                    console.error('have no item');
                    return null;
                }
                return selection;
            }
            function _device(){
                const selection = _selection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
               return item.device || item.ref && item.ref.device ? item.ref.device : item.isDevice ? item  : null;

            }
            ///////////////////////////////////////////////////////////////
            //
            //      Disable Functions
            //
            ///////////////////////////////////////////////////////////////
            function shouldDisableEdit(){

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                return false;
            }
            function shouldDisableEditInstance(){

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                const device = _device();
                if(!device){
                    return true;
                }
                const instance = delegate.getInstance(device);
                if(instance){
                    return false;
                }else{
                    return true;
                }

                return false;
            }
            function shouldDisableRun(){

                const selection = _selection();
                if (!selection || !selection[0].ref || !selection[0].ref.item) {
                    return true;
                }
                return false;
            }
            function shouldDisableConnect(){
                const selection = _selection();
                if (!selection || !selection[0] || !selection[0].path || selection[0].path.indexOf('.json')===-1) {
                    return true;
                }
                return false;
            }

            ///////////////////////////////////////////////////////////////
            //
            //      Actions
            //
            ///////////////////////////////////////////////////////////////

            const defaultMixin = {
                          addPermission:true
                      };

            const DEVICE_COMMAND_GROUP = 'Device/Command';


            this.hasPermission(ACTION.EDIT) && actions.push(this.createAction('Edit',ACTION.EDIT,types.ACTION_ICON.EDIT,['f4', 'enter','dblclick'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEdit,permissions,container,grid
            ));

            this.hasPermission('File/Edit Driver') && actions.push(this.createAction('Edit Driver','File/Edit Driver','fa-exchange',['f3', 'ctrl enter'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEdit,permissions,container,grid
            ));


            this.hasPermission('Instance/EditInstance') && actions.push(this.createAction('Edit Instance','Instance/EditInstance','fa-exchange' + ' text-warning',['f6'],'Home','Instance','item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            this.hasPermission('File/Edit Source') && actions.push(this.createAction('Edit %%Source','File/Edit Source',types.ACTION_ICON.EDIT,null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({},defaultMixin),null,shouldDisableEdit,permissions,container,grid
            ));

            this.hasPermission('File/Edit Driver Code') && actions.push(this.createAction('Edit Driver Code','File/Edit Driver Code','fa-code',null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            this.hasPermission('Instance/Console') && actions.push(this.createAction('Open Console','Instance/Console','fa-terminal',['f5'],'Home','Instance','item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            this.hasPermission('Instance/ConsoleWizard') && actions.push(this.createAction('Open Expression Wizard','Instance/ConsoleWizard','fa-info',['f7'],'Home','Instance','item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            actions.push(thiz.createAction({
                label: 'Run',
                command:ACTION.RUN ,
                icon: types.ACTION_ICON.RUN,
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: shouldDisableRun,
                mixin: utils.mixin({quick:true},defaultMixin)
            }));

            this.hasPermission('Instance/Edit Variables') && actions.push(thiz.createAction({
                label: 'Edit Variables',
                command:'Instance/Edit Variables',
                icon: 'fa-list-alt',
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEditInstance,
                mixin: defaultMixin
            }));

            this.hasPermission('Instance/Edit Commands') && actions.push(thiz.createAction({
                label: 'Edit Commands',
                command:'Instance/Edit Commands',
                icon: types.ACTION_ICON.EDIT,
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEditInstance,
                mixin: defaultMixin
            }));

            this.hasPermission('Instance/Show Log') && actions.push(thiz.createAction({
                label: 'Show Log',
                command:'Instance/Show Log',
                icon: 'fa-calendar',
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEdit,
                mixin: utils.mixin({quick:true},defaultMixin)
            }));

            this.hasPermission('File/Enable') && actions.push(thiz.createAction({
                label: 'Enable',
                command: 'File/Enable',
                icon: 'fa-on',
                tab: 'View',
                group: 'Device/Command',
                shouldDisable: shouldDisableEdit,
                mixin:{
                    addPermission: true,
                    closeOnClick:false,
                    actionType:'multiToggle',
                    quick:true
                },
                onCreate:function(action){

                    const device = _device();
                    if(device){
                        const enabled = thiz.delegate.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_ENABLED);
                        action.set('value',enabled);
                    }
                    //action.actionType = types.ACTION_TYPE.SINGLE_TOGGLE;
                    //action.set('value',_grid._highlight===true);
                    //action.setVisibility(VISIBILITY.ACTION_TOOLBAR, false);
                },
                onChange:function(property,value){
                    //_grid._highlight=value;
                    const device = _device();
                    if(device){
                        device.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED,value);
                    }
                }
            }));

            /*
             actions.push(this.createAction('Run', ACTION.RUN,types.ACTION_ICON.RUN, ['r'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
             defaultMixin,null,shouldDisableRun,permissions,container,grid
             ));
             */

            this.hasPermission('File/New Group') && actions.push(this.createAction('New Group', 'File/New Group' ,'fa-file-o',
                ['f7'],'Home','New','item|view',null,null,
                utils.mixin({quick:true},defaultMixin),null,null,permissions,container,grid
            ));

            actions.push(this.createAction('New Device', 'File/New Device' ,types.ACTION_ICON.NEW_FILE, ['ctrl n'],'Home','New','item',null,null,
                utils.mixin({quick:true},defaultMixin),null,null,permissions,container,grid
            ));



            ///////////////////////////////////////////////////////////////
            //  Device Control
            this.hasPermission('File/New Device') && actions.push(this.createAction('Disconnect', 'File/DisconnectDevice','fa-unlink text-danger', ['s'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableConnect,permissions,container,grid
            ));

            this.hasPermission('File/ConnectDevice') && actions.push(this.createAction('%%Connect', 'File/ConnectDevice','fa-link text-success', ['c'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableConnect,permissions,container,grid
            ));

            //add source actions
            actions.push(this.createAction('Source', 'View/Source', 'fa-hdd-o', ['f4'], 'Home', 'Navigation', 'item', null,
                function () {
                },
                {
                    addPermission: true,
                    tab: 'Home',
                    dummy: true
                }, null, null, permissions, container, thiz
            ));
            let i=0;
            _.each([
                {
                    label:'System',
                    scope:'system_devices'
                },
                {
                    label:'User',
                    scope:'user_devices'
                }
            ], function(item) {
                const label = item.label;
                actions.push(thiz.createAction(label, 'View/Source/' + label, 'fa-hdd-o', ['alt f' + i], 'Home', 'Navigation', 'item', null,
                    function () {
                    },
                    {
                        addPermission: true,
                        tab: 'Home',
                        item: item,
                        quick:true
                    }, null, null, permissions, container, thiz
                ));
                i++;
            });
            return actions;
        },
        editItem: function (item) {
            const thiz = this;
            if (item.type === types.ITEM_TYPE.BLOCK) {
                return thiz.blockManager.editBlock(item.ref.item, null, false);
            }
            if(item.driver){
                item = item.driver;
            }

            thiz.openItem(item);
        },
        onSelectionChanged:function(evt){
            const selection = evt.selection;
            const item = selection[0];
            const thiz = this;
            const ctx = this.ctx;
            const actionStore = thiz.getActionStore();
            const contextMenu = this.getContextMenu();
            const toolbar = this.getToolbar();
            const mainToolbar = ctx.getWindowManager().getToolbar();
            const itemActions = item && item.getActions ? item.getActions() : null;
            const _debugTree =false;
            const enableAction = this.getAction('File/Enable');

            if(item && item.isDevice && enableAction){
                const enabled = thiz.delegate.getMetaValue(item, types.DEVICE_PROPERTY.CF_DEVICE_ENABLED);
                enableAction.set('value',enabled);

            }
            //console.log('item',item);
        },
        startup:function(){
            const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
            this.inherited(arguments);
            this.addActions(_defaultActions);
            this.addActions(this.getDeviceActions(this.domNode,this.permissions,this));
            const thiz = this;
            this.refresh().then(function(){
                thiz.select([0], null, true, {
                    focus: true
                });
            });

            this.subscribe(types.EVENTS.ON_STORE_CHANGED,function(evt){
                if(evt.store.id == thiz.collection.id){
                    thiz.refresh();
                }
                if(evt.store == thiz.collection){
                    thiz.refresh();
                }
            });

            thiz.collection.on('update',function(evt){
                if(evt && evt.target){
                    try{
                        const cell = thiz.cell(evt.target.path, '0');
                        if (cell) {
                            thiz.refreshCell(cell);
                        }
                    }catch(e){
                        logError(e);
                    }
                }else {
                    thiz.refresh();
                }
                thiz.refreshActions();
            });

            this.wireStore(this.collection);

            this._on('selectionChanged',this.onSelectionChanged,this);
            /*
            $(this.domNode).on('focusout',function(){
               console.error('focus out');
            });
            $(this.domNode).on('blur',function(){
                console.error('blur');
            });
            */
        },
        getDefaultSort: function () {
            return [{property: 'name', descending: false, ignoreCase: true}];
        },
        getDefaultCollection:function(){
            const _sort = this.getDefaultSort();
            return this.collection.sort(_sort);
        },
        //////////////////////////////////////////////////////////////////////
        //
        //  Actions - Impl.
        //
        clipboardPaste: function (items,target) {
            const dfd = new Deferred();
            const isCut = this.currentCutSelection;
            const self = this;

            const defaultDfdArgs = {
                focus: true,
                append: false
            };

            items = items || isCut ? this.currentCutSelection : this.currentCopySelection;
            const allDfds = [];
            const newItems = [];
            const head = new Deferred();
            target = target || this.getSelectedItem() || {};
            if(target.isDevice){
                target = target.getParent();
            }
            if(!target || !target.isDir){
                head.resolve();
                return head;
            }
            items = items.filter(function(item){
                return item.isDevice;
            });

            if(!items || !items.length){
                head.resolve();
                return head;
            }
            for (let i = 0; i < items.length; i++) {
                const sub = self.delegate.cloneDevice(items[i],target);
                //collect new items
                sub.then(function(newItem){
                    newItems.push(newItem);
                });
                allDfds.push(sub);
            }
            all(allDfds).then(function(sub){
                defaultDfdArgs.select = newItems;
                head.resolve(defaultDfdArgs);
            });
            return head;
        },
        wireStore:function(store){
            const scope = store.scope;
            if(this['_storeConnected'+scope]){
                return;
            }

            this['_storeConnected'+scope]=true;

            const thiz = this;
            /**
             * @TODO: kill the cat, add some mixin to have better timers
             */
            function updateGrid(what) {
                if(what && what.type==='update' && what.target){
                    const cell = thiz.cell(what.target.path, '0');
                    if (cell) {
                        thiz.refreshCell(cell);
                    }
                    return;
                }
                thiz._refreshTimer && clearTimer(thiz._refreshTimer) && delete thiz._refreshTimer;
                thiz._refreshTimer == setTimeout(function () {
                }, 100);
                thiz.refresh();
            }
            this.addHandle('update',store.on('update', updateGrid));
            this.addHandle('added',store.on('added', updateGrid));
            this.addHandle('remove',store.on('remove', updateGrid));
            this.addHandle('delete',store.on('delete', updateGrid));
        },
        changeSource:function(source){
            const self = this;
            const delegate = self.delegate;
            const scope = source.scope;
            let store = delegate.getStore(scope);

            function ready(store){
                self.collection = store;
                const collection = store.filter(self.getRootFilter());
                self.set('collection',collection);
                self.set('title','Devices ('+source.label+')');
                self.scope = scope;
                self.wireStore(store);
            }

            if(store.then){
                store.then(function(){
                    store = delegate.getStore(scope);
                    ready(store);
                });
            }else{
                //is store
                ready(store);
            }
        },
        ///////////////////////////////////////////////////////////////
        runAction: function (action,_item,event,from) {
            action = this.getAction(action);

            if (!action || !action.command) {
                return;
            }
            const ACTION = types.ACTION;
            const thiz = from || this;
            const delegate = thiz.delegate || this.delegate;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            const driverManager = ctx.getDriverManager();
            const windowManager = ctx.getWindowManager();
            let item = thiz.getSelectedItem() || _item;
            let device;

            if(item) {
                device = item && item.device ? item.device : item.ref && item.ref.device ? item.ref.device : item.isDevice ? item : null;
            }



            if(action.command.indexOf('View/Source')!=-1){
                return this.changeSource(action.item);
            }


            const driverInstance  =  device ? deviceManager.getInstance(device) : null;
            const driverId = device ? device.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER) : null;
            const driver = driverInstance ? driverInstance.driver : driverManager.getItemById(driverId);
            const defaultSelect = {
                focus:true,
                append:false,
                delay:1,
                expand:true
            };

            switch (action.command) {

                case 'Instance/EditInstance':{

                    if(driverInstance){
                        return driverManager.openItemSettings(driver,item);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'File/Edit Source':{
                    deviceManager.getFile(item).then(function(file){
                        const docker = ctx.mainView.getDocker();
                        windowManager.openItem(file,windowManager.createDefaultTab());
                    });
                    break;
                }
                case 'Instance/Edit Variables':{

                    if(driverInstance){
                        return driverManager.openItemSettings(driver,item,{
                            showBasicCommands:false,
                            showConditionalCommands:false,
                            showResponseBlocks:false,
                            showLog:false,
                            showConsole:false,
                            showVariables:true,
                            showSettings:false,
                            showInitTab:false
                        },true);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'Instance/Show Log':{
                    return driverManager.openItemSettings(driver,item,{
                        showBasicCommands:false,
                        showConditionalCommands:false,
                        showResponseBlocks:false,
                        showLog:true,
                        showConsole:false,
                        showVariables:false,
                        showSettings:false,
                        showInitTab:false,
                        isSingle:true
                    },true);
                }
                case 'Instance/Edit Commands':{
                    if(driverInstance){
                        return driverManager.openItemSettings(driver,item,{
                            showBasicCommands:true,
                            showConditionalCommands:true,
                            showResponseBlocks:false,
                            showLog:false,
                            showConsole:false,
                            showVariables:false,
                            showSettings:false,
                            showInitTab:false
                        },true);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'File/Edit Driver':{
                    if(driver){
                        return driverManager.openItemSettings(driver,null);
                    }
                    break;
                }

                case ACTION.RUN:{
                    if (device && driverInstance) {
                        const scope = driverInstance.blockScope;
                        const block = scope.getBlockById(item.id);
                        if(block){
                            block.solve(block.scope);
                        }
                    }
                    break;
                }
                case 'File/Edit Driver Code':
                {
                    if(device && driverInstance){
                        item = driverInstance.driver;
                    }
                    return driverManager.editDriver(null,item);
                }
                case 'Instance/Console':
                {
                    if(device && driverInstance) {
                        deviceManager.startDevice(device,true).then(function(){
                            deviceManager.openConsole(device);
                        });
                        return;
                    }
                    return deviceManager.openConsole(device);
                }
                case 'Instance/ConsoleWizard':
                {
                    if(device && driverInstance) {
                        deviceManager.startDevice(device,true).then(function(){
                            deviceManager.openExpressionConsole(device);
                        });
                        return;
                    }
                    return deviceManager.openExpressionConsole(device);
                }
                case 'File/Log':
                {
                    return deviceManager.openLog(device);
                }

                case ACTION.EDIT:
                {
                    return thiz.editItem(item);
                }
                case ACTION.RELOAD:
                {
                    return thiz.refresh();
                }
                case ACTION.DELETE:
                {
                    const _res = delegate.onDeleteItem(item);

                    return _res;
                }
                case 'File/DisconnectDevice':{
                    const dfd = delegate.stopDevice(item);
                    item._userStopped=true;
                    return dfd;
                }
                case 'File/ConnectDevice':{
                    delegate.startDevice(item,true);
                    thiz.refresh();
                    return false;
                }
                case 'File/New Device':{
                    var res = delegate.newItem(item,thiz.scope);
                    res.then(function(item){
                        thiz.refresh();
                        thiz.select(item,null,true,defaultSelect);
                    });
                    return res;
                }
                case 'File/New Group':{
                    var res = delegate.newGroup(item,thiz.scope);
                    res.then(function(item){
                        thiz.refresh();
                        thiz.select(item,null,true,defaultSelect);
                    });
                    return res;
                }
            }

            return this.inherited(arguments);
        },
        changeScope: function (name) {
            this.delegate.ls(name + '_devices');
        },
        openItem:function(item,device){
            if(item && !item.isDir && !item.virtual){
                this.delegate.openItemSettings(item,device);
            }
        },
        refresh:function(){
            return this.inherited(arguments);
        }
    });

    Module.DefaultPermissions = DefaultPermissions;
    Module.getActions = Module.prototype.getDeviceActions;

    return Module;
});
