/**
 * This file is used to reconfigure parts of the loader at runtime for this application. We’ve put this extra
 * configuration in a separate file, instead of adding it directly to index.html, because it contains options that
 * can be shared if the application is run on both the client and the server.
 *
 * If you aren’t planning on running your app on both the client and the server, you could easily move this
 * configuration into index.html (as a dojoConfig object) if it makes your life easier.
 */
require({
    // The base path for all packages and modules. If you don't provide this, baseUrl defaults to the directory
    // that contains dojo.js. Since all packages are in the root, we just leave it blank. (If you change this, you
    // will also need to update app.profile.js).

    // A list of packages to register. Strictly speaking, you do not need to register any packages,
    // but you can't require "xbox" and get xbox/main.js if you do not register the "xbox" package (the loader will look
    // for a module at <baseUrl>/xbox.js instead). Unregistered packages also cannot use the packageMap feature, which
    // might be important to you if you need to relocate dependencies. TL;DR, register all your packages all the time:
    // it will make your life easier.
    packages: [
        // If you are registering a package that has an identical name and location, you can just pass a string
        // instead, and it will configure it using that string for both the "name" and "location" properties. Handy!
        {
            name: 'dojo',
            location: 'dojo',
            packageMap: {}
        },
        {
            name: 'xgrid',
            _location: 'xgrid',
            location: 'build/xgrid-release/xgrid',
            packageMap: {}
        },
        {
            name: 'xlog',
            _location: 'xgrid',
            location: 'build/xlog-release/xlog',
            packageMap: {}
        },
        {
            name: 'xnode',
            _location: 'xnode',
            location: 'build/xnode-release/xnode',
            packageMap: {}
        },
        {
            name: 'dijit',
            location: 'build/dijit-release/dijit',
            //location: 'dijit',
            packageMap: {}
        },
        {
            name: 'orion',
            _location: 'build/orion-release/orion',
            location: 'orion',
            packageMap: {}
        },
        {
            name: 'dojox',
            location: 'build/dojox-release/dojox',
            //location: 'dojox',
            packageMap: {}
        },
        {
            name: 'xapp',
            location: 'xapp',
            packageMap: {}
        },
        {

            name: 'xas',
            location: 'xas',
            packageMap: {}
        },
        {
            name: 'xbox',
            location: 'xbox',
            packageMap: {}
        },

        {
            name: 'xappconnect',
            location: 'xappconnect',
            packageMap: {}
        },
        {
            name: 'xide',
            location: 'xide',
            packageMap: {}
        },
        {
            name: 'dgrid',
            //location: 'dgrid',
            packageMap: {},
            location: 'build/dgrid-release/dgrid'
        },
        {
            name: 'put-selector',
            location: 'put-selector',
            packageMap: {}
        },
        {
            name: 'xstyle',
            location: 'xstyle',
            packageMap: {}
        },
        {
            name: 'xfile',
            location: 'build/xfile-release/xfile',
            _location: 'xfile',
            packageMap: {}
        },
        {
            name: 'xblox',
            _location: 'xblox',
            location: 'build/xblox-release/xblox',
            packageMap: {}
        },
        {
            name: 'xcf',
            location: 'xcf',
            packageMap: {}
        },
        {
            name: 'dbootstrap',
            location: 'dbootstrap',
            packageMap: {}
        },
        {
            name: 'davinci',
            _location: 'davinci',
            location: 'build/davinci-release/davinci',
            packageMap: {}
        },
        {
            name: 'xideve',
            //location: 'xideve',
            location: 'build/xideve-release/xideve',
            packageMap: {}
        },
        {
            name: 'system',
            location: 'system',
            packageMap: {}
        },
        {
            name: 'preview',
            location: 'preview',
            packageMap: {}
        },
        {
            name: 'xwire',
            location: 'xwire',
            packageMap: {}
        },
        {
            name: 'xexpression',
            location: 'xexpression',
            packageMap: {}
        },
        {
            name: 'ace',
            location: '../xfile/ext/ace',
            packageMap: {}
        },
        {
            name: 'liaison',
            location: 'ibm-js/liaison',
            packageMap: {}
        },
        {
            name: 'decor',
            location: 'ibm-js/decor',
            packageMap: {}
        },
        {
            name: 'dmodel',
            location: 'dmodel',
            packageMap: {}
        },
        {
            name: 'requirejs-dplugins',
            location: 'requirejs-dplugins'
        },
        {
            name: 'xdocker',
            location: 'xdocker',
            //location: 'build/xdocker-release/xdocker',
            packageMap: {}
        },
        {
            name: 'wcDocker',
            location: 'xdocker/wcDockerFork/Code',
            packageMap: {}
        },

        'xcf'
    ],
    cache: {}
// Require 'xbox'. This loads the main application file, xbox/main.js.
}, ['xcf']);


