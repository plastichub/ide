//>>built
define("xcf/model/SwitchCommand",["dcl/dcl","xcf/model/Command","xblox/model/logic/CaseBlock"],function(g,h,k){return g(h,{declaredClass:"xcf.model.SwitchCommand",variable:null,items:null,solve:function(e){for(var f=!1,c=[],a=0;a<this.items.length;a++){var b=this.items[a];if("xblox.model.logic.CaseBlock"===b.declaredClass){var d=void 0,d=b.solve(e,this);0!=d&&(f=!0,this.addToEnd(c,d))}}if(!f)for(a=0;a<this.items.length;a++)b=this.items[a],"xblox.model.logic.CaseBlock"!==b.declaredClass&&this.addToEnd(c,
b.solve(e));return c}})});
//# sourceMappingURL=SwitchCommand.js.map