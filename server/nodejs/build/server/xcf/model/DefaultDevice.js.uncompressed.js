/** @module xcf/model/DefaultDevice */
define("xcf/model/DefaultDevice", ['xide/utils', 'xide/types', 'xcf/types/Types'], function (utils, types, cfTypes) {
    /**
     * Default device CIS
     * @param context {module:xcf/manager/Context}
     * @param offerSystemDrivers {boolean}
     * @returns {module:xide/types/ConfigurableInformation[]}
     */
    function create(context, offerSystemDrivers, device) {
        const DEVICE_PROPERTY = cfTypes.DEVICE_PROPERTY;
        const ECIType = types.ECIType;
        const PROTOCOL = types.PROTOCOL;
        const NetworkGroup = 'Network';
        const CommonGroup = 'Common';
        return [
            utils.createCI('Title', ECIType.STRING, 'My New Device', {
                group: CommonGroup
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_ENABLED, ECIType.BOOL, false, {
                group: CommonGroup
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_HOST, ECIType.STRING, 'Search', {
                group: NetworkGroup
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_ID, ECIType.STRING, utils.createUUID(), {
                group: NetworkGroup,
                visible: false
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_PROTOCOL, ECIType.ENUMERATION, 'tcp', {
                group: NetworkGroup,
                options: [{
                        label: "TCP",
                        value: PROTOCOL.TCP
                    },
                    {
                        label: "UDP",
                        value: PROTOCOL.UDP
                    },
                    {
                        label: "Driver",
                        value: PROTOCOL.DRIVER
                    },
                    {
                        label: "SSH",
                        value: PROTOCOL.SSH
                    },
                    {
                        label: "Serial",
                        value: PROTOCOL.SERIAL
                    },
                    {
                        label: "MQTT",
                        value: PROTOCOL.MQTT
                    }
                ]
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_PORT, ECIType.STRING, '23', {
                group: NetworkGroup
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS, ECIType.FLAGS, 0, {
                group: 'Driver',
                "data": [{
                        value: 2,
                        label: 'Runs Server Side'
                    },
                    {
                        value: 4,
                        label: 'Show Debug Messages'
                    },
                    /*,
                                        {
                                            value: 8,
                                            label: 'Allow Multiple Device Connections'
                                        },
                                        */
                    {
                        value: 16,
                        label: 'Server'
                    }
                ]
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_OPTIONS, ECIType.STRUCTURE, "{}", {
                group: NetworkGroup
            }),
            utils.createCI(DEVICE_PROPERTY.CF_DEVICE_DRIVER, ECIType.ENUMERATION, 'auto-create', {
                group: 'Common',
                enumType: 'Driver',
                options: [].concat(context.getDriverManager().getDriversAsEnumeration(null, offerSystemDrivers))
            })
        ];
    }
    return create;
});