define("xcf/model/SwitchCommand", [
    'dcl/dcl',
    "xcf/model/Command",
    "xblox/model/logic/CaseBlock"
], function (dcl, Command, CaseBlock) {
    // summary:
    //		The switch command model. These kind of commands takes a existing variable and applies some comparations.
    //      Depending on the comparation results, the code into each case block is executed or not.

    // module:
    //		xcf.model.Command
    return dcl(Command, {

        //declaredClass: String (dcl internals, private!)
        declaredClass: "xcf.model.SwitchCommand",

        // switchVariable: xcf.model.Variable
        //  3.12.10.3. The “variable” field indicates the variable name to be evaluated
        //  into the switch block
        variable: null,
        //items: Array (xblox.model.CaseBlock / xblox.model.Block)
        //  Each case to be evaluated / Block to be executed if none of the cases occurs.
        //
        //
        items: null,
        /***
         * Solve the switchblock
         *
         * @param scope
         * @returns {string} execution result
         */
        solve: function (scope) {
            let anyCase = false;    // check if any case is reached
            const ret = [];
            // iterate all case blocks
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];

                if (block.declaredClass === 'xblox.model.logic.CaseBlock'/* instanceof CaseBlock*/) {
                    let caseret;
                    // solve each case block. If the comparison result is false, the block returns "false"
                    caseret = block.solve(scope, this);
                    if (caseret != false) {
                        // If the case block return is not false, don't run "else" block
                        anyCase = true;
                        this.addToEnd(ret, caseret);
                    }
                }
            }
            // iterate all "else" blocks if none of the cases occurs
            if (!anyCase) {
                for (var n = 0; n < this.items.length; n++) {
                    var block = this.items[n];
                    if (!(block.declaredClass === 'xblox.model.logic.CaseBlock')) {
                        this.addToEnd(ret, block.solve(scope));
                    }
                }
            }
            return ret;
        }
    });
});
