/** @module xcf/model/Variable */
define("xcf/model/Variable", [
    'dcl/dcl',
    'xide/types',
    "xblox/model/variables/Variable"
], function(dcl,types,Variable){
    /**
     *
     * Model for a variable. It extends the base model class
     * and acts a source.
     *
     * @module xcf/model/Variable
     * @extends module:xblox/model/Variables/Variable
     * @augments module:xide/mixins/EventedMixin
     * @augments module:xide/data/Model
     * @augments module:xide/data/Source
     */
    return dcl(Variable,{
        declaredClass: "xcf.model.Variable",
        /**
         * @private
         */
        hasInlineEdits:true,
        /////////////////////////////////////////////////
        //
        //  On-Change related members
        //
        // 3.12.9.4. a group of options that define a task to be performed when the variable value is changed.
        //
        /////////////////////////////////////////////////
        /**
         * 3.12.9.4.1. on a change to the variable, any object (button, text field, etc.) which references
         * the variable should be evaluated and updated accordingly. If Cmd is selected this should deselect.
         * @private
         */
        gui:"off",
        /**
         * 3.12.9.4.2. on a change to the variable, any command which references the variable
         * should be executed. If GUI is selected this should deselect.
         * @private
         */
        cmd:"off",
        /**
         * 3.12.9.4.3. selecting this checkbox signifies that the variable value automatically gets saved
         * to an xml file whenever changed for recallability.  When active,
         * the Initialize field should show “saved value” and at startup the saved
         * value should be assigned to the variable from the Saved Values File.
         * @private
         */
        save:false,
        /**
         * @private
         */
        target:'None',
        /**
         * The name of the variable
         */
        name:'No Title',
        /**
         * The value of the variable
         */
        value:-1,
        /**
         * @private
         */
        observed:[
            'value',
            'initial',
            'name'
        ],

        solve:function(){
            let extra = "";
            if(this.group==='processVariables'){
                let _val = this.scope.getVariable("value");
                if(_val) {
                    _val = _val.value;
                    if(!this.isNumber(_val)){
                        _val = ''+_val;
                        _val = "'" + _val + "'";
                    }
                    extra = "var value = " + _val +";\n";
                }
            }
            const _result = this.scope.parseExpression(extra + this.getValue(),true);
            return _result;
        },
        /**
         * @private
         * @returns {boolean}
         */
        canEdit:function(){
            return true;
        },
        /**
         * @private
         * @returns {boolean}
         */
        canDisable:function(){
            return false;
        },
        /**
         * @private
         * @returns {*}
         */
        getFields:function(){
            const fields = this.getDefaultFields();
            fields.push(this.utils.createCI('title',13,this.name,{
                group:'General',
                title:'Name',
                dst:'name'
            }));


            const thiz=this;

            const defaultArgs = {
                allowACECache:true,
                showBrowser:false,
                showSaveButton:true,
                editorOptions:{
                    showGutter:false,
                    autoFocus:false,
                    hasConsole:false
                },
                aceOptions:{
                    hasEmmet:false,
                    hasLinking:false,
                    hasMultiDocs:false
                },
                item:this
            };


            //this.types.ECIType.EXPRESSION_EDITOR
            fields.push(this.utils.createCI('value',this.types.ECIType.EXPRESSION,this.value,{
                group:'General',
                title:'Value',
                dst:'value',
                widget:defaultArgs,
                delegate:{
                    runExpression:function(val,run,error){
                        if(thiz.group=='processVariables'){
                            let _val = thiz.scope.getVariable("value");
                            var extra = "";
                            if(_val) {
                                _val = _val.value;
                                if(!thiz.isNumber(_val)){
                                    _val = ''+_val;
                                    _val = "'" + _val + "'";
                                }
                                extra = "var value = " + _val +";\n";
                            }
                        }
                        return thiz.scope.expressionModel.parse(thiz.scope,extra + val,false,run,error);
                    }
                }
            }));

            fields.push(this.utils.createCI('flags', 5, this.flags, {
                group: 'General',
                title: 'Flags',
                dst: 'flags',
                data: [
                    {
                        value: 0x000001000,
                        label: 'Dont parse',
                        title: "Do not parse the string and use it as is"
                    },
                    {
                        value: 0x00000800,//2048
                        label: 'Expression',
                        title: 'Parse it as Javascript'
                    }
                ],
                widget: {
                    hex: true
                }
            }));

            /*
                        //this.types.ECIType.EXPRESSION_EDITOR
                        fields.push(this.utils.createCI('initial',this.types.ECIType.EXPRESSION,this.initial,{
                            group:'General',
                            title:'Initial',
                            dst:'initial',
                            widget:defaultArgs,
                            delegate:{
                                runExpression:function(val,run,error){
                                    if(thiz.group=='processVariables'){
                                        var _val = thiz.scope.getVariable("value");
                                        var extra = "";
                                        if(_val) {
                                            _val = _val.value;
                                            if(!thiz.isNumber(_val)){
                                                _val = ''+_val;
                                                _val = "'" + _val + "'";
                                            }
                                            extra = "var value = " + _val +";\n";
                                        }
                                    }
                                    return thiz.scope.expressionModel.parse(thiz.scope,extra + val,false,run,error);
                                }
                            }
                        }));
                        */

            return fields;
        },
        onChangeField:function(field,newValue,cis) {
            if(field==='value'){
                this.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, {
                    item: this,
                    scope: this.scope,
                    save: false,
                    source: types.MESSAGE_SOURCE.GUI  //for prioritizing
                });
            }
        }
    });
});
