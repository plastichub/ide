/** @module xwire/_Base */
define("xwire/_Base", [
    'dcl/dcl',
    'xide/mixins/EventedMixin'
],function(dcl,EventedMixin){
    /**
     * @param {object} o An object.
     * @param {string} path The path from object, either dot-concatenated string or an array.
     * @returns The value of the object path.
     */
    function getObjectPath(o, path) {
        for (var comps = getPathComps(path), i = 0, l = comps.length; i < l; ++i) {
            var comp = comps[i];
            o = o == null ? o : o[comp];
        }
        return o;
    }

    /**
     * Utility to split an object path from a dot separated string into an array
     * @param path
     * @param create
     * @returns {Array}
     */
    function getPathComps(path, create) {
        return path === "" ? [] : typeof path.splice !== "function" ? path.split(".") : create ? path.slice() : path;
    }
    /**
     * Sets a value to an object path.
     * @param {object} o An object.
     * @param {string} path The path from object, either dot-concatenated string or an array.
     * @returns The value set. Undefined if value cannot be set.
     */
    function setObjectPath(o, path, value) {
        var comps = getPathComps(path, true),
            prop = comps.pop();
        o = comps.length > 0 ? getObjectPath(o, comps) : o;
        return Object(o) !== o || !path ? undefined : // Bail if the target is not an object
            typeof o.set === "function" ? o.set(prop, value) :
                (o[prop] = value);
    }

    /**
     * Abstract binding source
     * @class xwire/Source
     * @extends {module:xwire/_Base}
     * @extends {module:xide/mixins/EventedMixin}
     */
    var _base  = dcl([EventedMixin.dcl],{
        declaredClass:'xwire._Base',
        /***
         * Standard constructor for all subclassing bindings
         * @param {array} arguments
         */
        constructor: function(args){
            //simple mixin of constructor arguments
            for (var prop in args) {
                if (args.hasOwnProperty(prop)) {

                    this[prop] = args[prop];
                }
            }
        },
        getValue:function(object,path){
            return getObjectPath(object,path);
        },
        setValue:function(object,path,value){
            return setObjectPath(object,path,value);
        }

    });
    return _base;
});