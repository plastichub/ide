define("xwire/types/Types", [
    'xide/types'
], function (types) {
    types.TRIGGER_TYPE = {
        EVENT: 'triggerTypeEvent'
    };
    return types;
});