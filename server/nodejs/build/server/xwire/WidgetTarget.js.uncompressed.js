/** @module xwire/WidgetTarget */
define("xwire/WidgetTarget", [
    'dcl/dcl',
    'xide/types',
    'xwire/Target',
    'xide/utils'
],function(dcl,types,Target,utils){
    /**
     * Widget based binding target
     * @class xwire/WidgetTarget
     */
    return dcl(Target,{
        declaredClass:"xwire.WidgetTarget",
        targetFilter:null,
        /**
         * type {module:xwire/Binding}
         */
        binding:null,
        /**
         * type {module:xblox/RunScript}
         */
        delegate:null,
        /**
         * Run the action
         */
        run:function(data){
            var thiz = this;
            var isDelite = thiz.object !==null && thiz.object.render!=null;
            var acceptFn = this.binding.accept ? this.binding.accept.getFunction() : null;
            var transformFn = this.binding.transform ? this.binding.transform.getFunction() : null;
            if(thiz.object){
                var value = data.value;
                if(this.targetFilter!=null && this.targetFilter.length && this.delegate && this.delegate.resolveFilter){
                    value = this.delegate.resolveFilter(this.targetFilter,value,this.object);
                }
                thiz.object.ignore=true;
                if(acceptFn){
                    var accept = acceptFn.apply(this.delegate,[value,this.binding.accept,this.delegate,,this.object]);
                    if(!accept){
                        return;
                    }
                }
                if(transformFn){
                    var _value = transformFn.apply(this.delegate,[value,this.binding.transform,this.delegate,this.object]);
                    if(_value!==null && _value!==undefined){
                        value = _value;
                    }
                }
                if(!isDelite && thiz.object.set) {
                    thiz.object.set(thiz.path, value);
                }else if(thiz.object._set){
                    thiz.object._set(thiz.path, value);
                    var _funcPath = "set" + utils.capitalize(thiz.path);
                    if(thiz.object[_funcPath]) {
                        thiz.object[_funcPath](value,data);
                    }
                }else{
                    thiz.object[thiz.path] = value;
                }
                thiz.object.ignore=false;
            }
        }
    });
});