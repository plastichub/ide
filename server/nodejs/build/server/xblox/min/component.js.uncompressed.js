define("xblox/min/component", [
    "dojo/_base/declare",
    "dojo/has",
    "xide/model/Component"

], function (declare,has,Component) {
    /**
     * @class xblox.component
     * @inheritDoc
     */
    return declare([Component], {
        /**
         * @inheritDoc
         */
        beanType:'BLOCK',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        _load:function(){
        },
        hasEditors:function(){
            return ['xblox'];
        },
        getDependencies:function(){
            if(has('xblox-ui')) {
                return [
                    'xide/xide',
                    'xblox/types/Types',
                    'xblox/manager/BlockManager',
                    'xblox/manager/BlockManagerUI',
                    'xblox/embedded_ui',
                    'xide/widgets/ExpressionJavaScript',
                    'xide/widgets/ImageWidget',
                    'xide/widgets/Expression',
                    'xide/widgets/ArgumentsWidget',
                    'xide/widgets/RichTextWidget',
                    'xide/widgets/JSONEditorWidget',
                    'xide/widgets/ExpressionEditor',
                    'xide/widgets/WidgetReference',
                    'xide/widgets/DomStyleProperties',
                    'xblox/views/BlocksFileEditor'
                    //'xide/widgets/BlockPickerWidget',
                    //'xide/widgets/BlockSettingsWidget'
                ];
            }else{
                return [
                    'xide/xide',
                    'xblox/types/Types',
                    'xblox/manager/BlockManager',
                    'xblox/embedded'
                ];
            }
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return 'xblox';
        },
        /**
         * @inheritDoc
         */
        getBeanType:function(){
            return this.beanType;
        }
    });
});

