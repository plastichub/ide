/** @module xblox/model/variables/VariableSwitch **/
define("xblox/model/variables/VariableSwitch", [
    'dcl/dcl',
    "xblox/model/logic/SwitchBlock",
    'xide/types',
    "xblox/model/logic/CaseBlock",
    "xblox/model/logic/DefaultBlock",
    "dojo/Deferred"
], function(dcl,SwitchBlock,types,CaseBlock,DefaultBlock,Deferred){
    /**
     *
     * The switch command model. These kind of commands takes a existing variable and applies some comparison.
     * Depending on the comparison results, the code into each case block is executed or not.
     * @class module:xblox/model/variables/VariableSwitch
     * @extends module:xblox/model/Block
     */
    return dcl(SwitchBlock,{
        declaredClass:"xblox.model.variables.VariableSwitch",
        name:'Switch on Variable',
        icon:'',
        variable:"PowerState",
        toText:function(){
            var _variable = this.scope.getVariableById(this.variable);
            var _text = _variable ? _variable.name : '';
            return this.getBlockIcon('H')  + this.name + ' ' + _text;
        },
        //  standard call for editing
        getFields:function(){
            //options:this.scope.getVariablesAsOptions(),
            var fields = this.getDefaultFields(false,false);
            fields = fields.concat([
                this.utils.createCI('Variable',3,this.variable,
                    {
                        group:'General',
                        widget:{
                            store:this.scope.blockStore,
                            labelField:'name',
                            valueField:'id',
                            query:[
                                {
                                    group:'basicVariables'
                                },
                                {
                                    group:'processVariables'
                                }
                            ]

                        },
                        dst:'variable'
                    }
                )
            ]);
            return fields;
        }
    });
});