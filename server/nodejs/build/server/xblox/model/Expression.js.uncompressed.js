/** @module xblox/model/Expression */
define("xblox/model/Expression", [
    "xdojo/declare",
    "xdojo/has",
    "xide/utils",
    "xide/types",
    "xblox/model/ModelBase"
], function (declare, has, utils, types, ModelBase, tracer, _console) {
    'use strict';
    var isServer =  true ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if (isServer && tracer && console && console.error) {
        console = _console;
    }
    var _debug = false;
    /**
     * The expression
     * @class module:xblox.model.Expression
     * @extends module:xblox/model/ModelBase
     */
    return declare("xblox.model.Expression", [ModelBase], {
        id: null,
        context: null,
        // Constants
        variableDelimiters: {
            begin: "[",
            end: "]"
        },
        blockCallDelimiters: {
            begin: "{",
            end: "}"
        },
        expressionCache: null,
        variableFuncCache: null,
        constructor: function () {
            this.reset();
        },
        reset: function () {
            this.expressionCache = {};
            this.variableFuncCache = {};
        },
        /**
         * Replace variable calls width variable values
         * @param scope
         * @param expression
         * @param _evaluate
         * @param _escape
         * @param variableOverrides
         * @returns {*}
         */
        replaceVariables: function (scope, expression, _evaluate, _escape, variableOverrides, useVariableGetter, variableDelimiters, flags) {
            var FLAG = types.CIFLAG;
            variableDelimiters = variableDelimiters || this.variableDelimiters;
            flags = flags || FLAG.NONE;
            if (flags & FLAG.DONT_ESCAPE) {
                _escape = false;
            }
            if (flags & FLAG.DONT_PARSE) {
                _evaluate = false;
            }
            var occurrence = this.findOccurrences(expression, variableDelimiters);
            if (occurrence) {
                for (var n = 0; n < occurrence.length; n++) {
                    // Replace each variable call width the variable value
                    var oc = occurrence[n];
                    oc = oc.replace(variableDelimiters.begin, '');
                    oc = oc.replace(variableDelimiters.end, '');
                    var _var = this._getVar(scope, oc);
                    if (_var && _var.flags & FLAG.DONT_PARSE) {
                        _evaluate = false;
                    }
                    var value = null;
                    if (_var) {
                        if (useVariableGetter) {
                            expression = expression.replace(occurrence[n], 'this.getVariable(\'' + _var.name + '\')');
                            continue;
                        }
                        value = this.getValue(_var.value);
                        if (variableOverrides && _var.name in variableOverrides) {
                            value = variableOverrides[_var.name];
                        }
                        if (this.isScript(value) && _evaluate !== false) {
                            try {
                                //put other variables on the stack: should be avoided
                                var _otherVariables = scope.variablesToJavascript(_var, true);
                                if (_otherVariables) {
                                    value = _otherVariables + value;
                                }
                                var _parsed = (new Function("{\n" + value + "\n}")).call(scope.context || {});
                                //wasnt a script
                                if (_parsed === 'undefined' || typeof _parsed === 'undefined') {
                                    value = '' + _var.value;
                                } else {
                                    value = _parsed;
                                    !(flags & FLAG.DONT_ESCAPE) && (value = "'" + value + "'");
                                }
                            } catch (e) {
                                console.log(' parsed variable expression failed \n' + value, e);
                            }
                        } else {
                            if (!this.isNumber(value)) {
                                if (_escape !== false) {
                                    value = "'" + value + "'";
                                }
                            }
                        }
                    } else {
                        _debug && console.log('   expression failed, no such variable :' + occurrence[n] + ' ! setting to default ' + '');
                        value = occurrence[n];
                    }
                    expression = expression.replace(occurrence[n], value);
                }
            }
            return expression;
        },
        /**
         *
         * @param scope
         * @param expression
         * @param addVariables
         * @param runCallback
         * @param errorCallback
         * @param context
         * @param variableOverrides
         * @param args {[*]}
         * @param flags {CIFLAGS}
         * @returns {*}
         */
        parse: function (scope, expression, addVariables, runCallback, errorCallback, context, variableOverrides, args, flags) {
            expression = this.replaceAll("''", "'", expression);
            var expressionContext = context || scope.context || scope.getContext() || {};
            var useVariableGetter = expressionContext['getVariable'] != null;
            expression = this.replaceVariables(scope, expression, null, null, variableOverrides, useVariableGetter, null, flags);
            var isExpression = this.isScript(expression);
            if (!isExpression && (this.isString(expression) || this.isNumber(expression))) {
                if (runCallback) {
                    runCallback('Expression ' + expression + ' evaluates to ' + expression);
                }
                return expression;
            }
            if (expression.indexOf('return') == -1 && isExpression) {
                expression = 'return ' + expression;
            }
            addVariables = false;
            if (addVariables === true) {
                var _otherVariables = scope.variablesToJavascript(null, expression);
                if (_otherVariables) {
                    expression = _otherVariables + expression;
                    expression = this.replaceAll("''", "'", expression);//weird!
                }
            }
            var parsed = this;
            try {
                expression = this.replaceAll("''", "'", expression);
                var _function = this.expressionCache[expression];
                if (!_function) {
                    _debug && console.log('create function ' + expression);
                    _function = new Function("{" + expression + "; }");
                    this.expressionCache[expression] = _function;
                } else {

                }
                parsed = _function.apply(expressionContext, args);
            } catch (e) {
                console.error('invalid expression : \n' + expression, e);
                if (errorCallback) {
                    errorCallback('invalid expression : \n' + expression + ': ' + e, e);
                }
                parsed = '' + expression;
                return parsed;
            }
            if (parsed === true) {
                _debug && console.log('expression return true! : ' + expression);
            }

            if (runCallback) {
                runCallback('Expression ' + expression + ' evaluates to ' + parsed);
            }
            return parsed;
        },
        parseVariable: function (scope, _var, _prefix, escape, allowCache, context, args) {
            var value = '' + _var.value;
            _prefix = _prefix || '';
            if (allowCache !== false) {
                var _function = this.variableFuncCache[scope.id + '|' + _var.title];
                if (!_function) {
                    _function = new Function("{" + _prefix + value + "}");
                    this.variableFuncCache[scope.id + '|' + _var.title] = _function;
                }
            } else {
                _function = new Function("{" + _prefix + value + "}");
            }
            var _parsed = _function.apply(context || scope.context || {}, args || []);
            if (_parsed === 'undefined' || typeof _parsed === 'undefined') {
                value = '' + _var.value;
            } else {
                if (!this.isNumber(_parsed) && escape !== false) {
                    value = '' + _parsed;
                    value = "'" + value + "'";
                } else {
                    value = _parsed;
                }
            }
            return value;
        },
        // Replace block call with block result
        replaceBlockCalls: function (scope, expression) {
            var occurrences = this.findOccurrences(expression, this.blockCallDelimiters);
            if (occurrences) {
                for (var n = 0; n < occurrences.length; n++) {
                    // Replace each block call with block result
                    var blockName = this._removeDelimiters(occurrences[n], this.blockCallDelimiters);
                    var blockResult = scope.solveBlock(blockName).join("\n");
                    expression = expression.replace(occurrences[n], blockResult);
                }
            }
            return expression;
        },
        // gets a variable from the scope using text [variableName]
        _getVar: function (scope, string) {
            return scope.getVariable(this._getVarName(string));
        },
        _getVarName: function (string) {
            return this._removeDelimiters(string, this.variableDelimiters);
        },
        _removeDelimiters: function (text, delimiters) {
            return text.replace(delimiters.begin, '').replace(delimiters.end, '');
        },
        // escape regular expressions special chars
        _escapeRegExp: function (string) {
            var special = ["[", "]", "(", ")", "{", "}", "*", "+", "."];
            for (var n = 0; n < special.length; n++) {
                string = string.replace(special[n], "\\" + special[n]);
            }
            return string;
        },
        /**
         * Finds a term in an expression by start and end delimiters
         * @param expression
         * @param delimiters
         * @private
         */
        findOccurrences: function (expression, delimiters) {
            var d = {
                begin: this._escapeRegExp(delimiters.begin),
                end: this._escapeRegExp(delimiters.end)
            };
            return expression.match(new RegExp(d.begin + "(" + "[^" + d.end + "]*" + ")" + d.end, 'g'));
        }
    });
});