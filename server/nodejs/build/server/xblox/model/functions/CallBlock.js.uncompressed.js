define("xblox/model/functions/CallBlock", [
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'dojo/Deferred',
    "xblox/model/Block",
    "xcf/model/Command"
], function(dcl,utils,types,Deferred,Block,Command){

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.functions.CallBlock
    /**
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl(Command,{
        declaredClass:"xblox.model.functions.CallBlock",
        //command: (String)
        //  block action name
        command:'Select command please',
        icon:'',
        args:null,
        _timeout:100,

        isCommand:true,


        _commandHandles:null,
        /**
         * onCommandFinish will be excecuted which a driver did run a command
         * @param msg {object}
         * @param msg.id {string} the command job id
         * @param msg.src {string} the source id, which is this block id
         * @param msg.cmd {string} the command string being sent
         */
        onCommandProgress:function(msg){

            var scope = this.getScope();
            var context = scope.getContext();//driver instance
            var result = {};
            var params = msg.params;

            if(params && params.id){
                this._emit('cmd:'+msg.cmd + '_' + params.id,{
                    msg:msg
                });
                msg.lastResponse && this.storeResult(msg.lastResponse);
                this._emit('progress',{
                    msg:msg,
                    id:params.id
                });
            }

            var command = this._lastCommand;

            this._lastResult = null;


            this._lastResult = msg ? msg.result : null;

            var items = this.getItems(types.BLOCK_OUTLET.PROGRESS);
            if(!this._lastSettings){
                this._lastSettings = {}
            }
            this._lastSettings.override = {};
            if(items.length) {
                this.runFrom(items,0,this._lastSettings);
            }
        },
        stop:function(){
            this._lastCommand && this._lastCommand.stop();
        },
        pause:function(){
            this._lastCommand && this._lastCommand.pause();
        },
        destroy:function(){
            _.invoke(this._commandHandles,'remove');
            delete this._commandHandles;
            delete this._lastCommand;
        },
        /***
         * Returns the block run result
         * @param scope
         */
        solve:function(scope,settings) {
            if(!this._commandHandles){
                this._commandHandles=[];
            }else{
                //_.invoke(this._commandHandles,'remove');
                this._commandHandles = [];
            }

            var timeout = this._timeout || 50;
            if(_.isString(timeout)){
                timeout = parseInt(timeout);
            }

            var dfd = new Deferred();

            var handles = this._commandHandles;

            settings = settings || {}

            setTimeout(function(){
                if (this.command){

                    var _args = null;
                    if(this.args){

                        settings.override = settings.override || {};
                        var args = scope.expressionModel.replaceVariables(scope,this.args,false,false,null,null,{
                            begin:"%%",
                            end:"%%"
                        });
                        try {
                            _args = utils.fromJson(args);
                        }catch(e){
                            _args = args;
                        }
                        settings.override['args']= _.isArray(_args) ? _args : [args];
                        settings.override['mixin']=_args;
                    }
                    this._lastCommand = scope.resolveBlock(this.command);
                    
                    if(this._lastCommand && this._lastCommand._on){
                        handles.push(this._lastCommand._on('paused',this.onCommandPaused,this));
                        handles.push(this._lastCommand._on('finished',this.onCommandFinish,this));
                        handles.push(this._lastCommand._on('stopped',this.onCommandStopped,this));
                        handles.push(this._lastCommand._on('error',this.onCommandError,this));
                        handles.push(this._lastCommand._on('progress',this.onCommandProgress,this));                    
                    }
                    
                    var res = scope.solveBlock(this.command,settings);
                    if(res){
                        this.onSuccess(this,settings);
                    }else{
                        this.onFailed(this,settings);
                    }
                    dfd.resolve(res);
                    return res;
                }
            }.bind(this),timeout);
            return dfd;
        },
        hasInlineEdits:true,
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable:function(field,pos,type,title,mode,options,value){
            var optionsString = "";
            return "<a " + optionsString + "  tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable editable-click'  href='#'>" + this[field] +"</a>";
        },
        getFieldOptions:function(field){
            if(field ==="command"){
                return this.scope.getCommandsAsOptions("text");
            }
        },
        toText:function(){
            var text = 'Unknown';
            var block = this.scope.getBlock(this.command);
            if(block){
                text = block.name;
            }
            if(this.command.indexOf('://')!==-1) {
                text = '<span class="text-info">' +this.scope.toFriendlyName(this,this.command) + '</span>';
            }
            var _out = this.getBlockIcon('D') + 'Call Command : ' + text;
            return _out;
        },
        //  standard call for editing
        getFields:function(){

            var fields = this.getDefaultFields();
            var thiz=this;

            var title = 'Command';

            if(this.command.indexOf('://')){
                title = this.scope.toFriendlyName(this,this.command);
            }

            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.command,{
                    group:'General',
                    title:'Command',
                    dst:'command',
                    options:this.scope.getCommandsAsOptions(),
                    block:this,
                    pickerType:'command',
                    value:this.command
            }));

            fields.push(utils.createCI('arguments',27,this.args,{
                group:'Arguments',
                title:'Arguments',
                dst:'args'
            }));

            fields.push(utils.createCI('timeout',13,this._timeout,{
                group:'General',
                title:'Delay',
                dst:'_timeout'
            }));

            return fields;
        }
    });
});