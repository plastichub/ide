define("xblox/model/functions/SetProperties", [
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'dojo/Deferred',
    "xblox/model/Block",
    "xide/lodash"
], function(dcl,utils,types,Deferred,Block,_){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl(Block,{
        declaredClass:"xblox.model.functions.SetProperties",
        command:'Select block',
        icon:'',
        args:null,
        _timeout:100,
        hasInlineEdits:false,
        solve:function(scope,settings) {
            var dfd = new Deferred();
            if (this.command){
                var block = scope.resolveBlock(this.command);
                if(block && this.props){
                    for(var prop in this.props){
                        block.set(prop,this.props[prop]);
                        block[prop] = this.props[prop];
                        block.onChangeField && block.onChangeField(prop,this.props[prop]);
                    }
                    this.onSuccess(this,settings);
                }else{
                    this.onFailed(this,settings);
                }
                dfd.resolve([]);
                return dfd;
            }
            return dfd;
        },
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable:function(field,pos,type,title,mode){
            var optionsString = "";
            return "<a " + optionsString + "  tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable editable-click'  href='#'>" + this[field] +"</a>";
        },
        getFieldOptions:function(field){
            if(field ==="command"){
                return this.scope.getCommandsAsOptions("text");
            }
        },
        toText:function(){
            var text = 'Unknown';
            var block = this.scope.getBlock(this.command);
            if(block){
                text = block.name;
            }
            if(this.command.indexOf('://')!==-1) {
                text = '<span class="text-info">' +this.scope.toFriendlyName(this,this.command) + '</span>';
            }
            return this.getBlockIcon('D') + 'Set Properties : ' + text;
        },
        serializeObject:function(field){
            return field === 'props';
        },
        onChangeField:function(field){
            if(field==='command'){
                delete this.props;
                this.props = {};
            }
        },
        init:function(){
            if(this.props && _.isString(this.props)){
                this.props = utils.fromJson(this.props);
            }

        },
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.command,{
                    group:'General',
                    title:'Command',
                    dst:'command',
                    options:this.scope.getCommandsAsOptions(),
                    block:this,
                    pickerType:'command',
                    value:this.command
            }));
            var block = this.scope.resolveBlock(this.command);
            if(block && block.getFields){
                if(!this.props){
                    this.props = {};
                }
                var _fields = block.getFields();
                var descr = _.find(_fields,{
                    dst:"description"
                });
                _fields.remove(descr);
                _.each(_fields,function(_field){
                    _field.group = "Properties";
                    _field.value = utils.getAt(this.props,_field.dst,_field.value);
                    _field.dst = "props." + _field.dst;

                },this);
                fields = fields.concat(_fields);
            }
            return fields;
        }
    });
});