define("xblox/model/Statement", [
    "dcl/dcl",
    "xblox/model/Block"
], function(dcl,Block){

    // summary:
    //		The statement block is only a wrapper for items like in 'else'

    // module:
    //		xblox.model.Statement
    return dcl(Block,{
        declaredClass:"xblox.model.Statement",
        /**
         * Return block name
         * @returns {name|*}
         */
        toText:function(){
            return this.name;
        },
        /**
         *
         * @returns {items|*}
         */
        getChildren:function(){
            return this.items;
        }
    });
});