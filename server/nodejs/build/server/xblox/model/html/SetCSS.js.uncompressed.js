define("xblox/model/html/SetCSS", [
    "dojo/_base/declare",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xblox/model/Targeted'
], function(declare,Block,utils,types,EventedMixin,Targeted){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return declare("xblox.model.html.SetCSS",[Block,EventedMixin,Targeted],{
        //method: (String)
        //  block name
        name:'Set CSS',
        file:'',
        reference:'',
        references:null,
        description:'Sets HTML Node CSS',
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        solve:function(scope,settings) {
            this.onSuccess(this,settings);
        },
        toText:function(){
            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if(this.event){
                result+= this.event;
            }
            return result;
        },

        //  standard call for editing
        getFields:function(){
            try {
                var fields = this.inherited(arguments) || this.getDefaultFields();
                fields.push(utils.createCI('File', types.ECIType.FILE, this.file, {
                    group: 'General',
                    dst: 'file',
                    value: this.file,
                    intermediateChanges: false,
                    acceptFolders: false,
                    acceptFiles: true,
                    encodeFilePath: false,
                    buildFullPath: true,
                    filePickerOptions: {
                        dialogTitle: 'Select CSS File',
                        filePickerMixin: {
                            beanContextName: 'CSSFilePicker',
                            persistent: false,
                            globalPanelMixin: {
                                allowLayoutCookies: false
                            }
                        },
                        configMixin: {
                            beanContextName: 'CSSFilePicker',
                            LAYOUT_PRESET: types.LAYOUT_PRESET.SINGLE,
                            PANEL_OPTIONS:{
                                ALLOW_MAIN_MENU:false
                            }
                        },
                        defaultStoreOptions: {
                            "fields": 1663,
                            "includeList": "css",
                            "excludeList": "*"
                        },
                        startPath: this.file
                    }
                }));
                fields.push(utils.createCI('Target', types.ECIType.WIDGET_REFERENCE, this.reference, {
                    group: 'General',
                    dst: 'reference',
                    value: this.reference
                }));

            }catch(e){

            }
            return fields;
        },
        getBlockIcon:function(){
            return '<span class="fa-paint-brush"></span>';
        },
        onReferenceChanged:function(newValue){
            this._destroy();//unregister previous event(s)
            this.reference = newValue;
        },
        onChangeField:function(field,newValue,cis){
            if(field=='reference'){
                this.onReferenceChanged(newValue,cis);
            }
            this.inherited(arguments);
        },
        activate:function(){
            this._destroy();
        },
        deactivate:function(){
            this._destroy();
        },
        _destroy:function(){
        }
    });
});