/** @module xblox/model/mqtt/Subscribe **/
define("xblox/model/mqtt/Subscribe", [
    'dcl/dcl',
    'xdojo/has',
    "xblox/model/Block",
    'xide/utils',
    'xblox/model/Contains',
    'xide/types'
    //'dojo/has!host-node?dojo/node!tracer',
    //'dojo/has!host-node?nxapp/utils/_console'
], function(dcl,has,Block,utils,Contains,types,tracer,_console){

    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    var isServer =  true ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if(isServer && tracer && console && console.error){
        console = _console;
    }
    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    /**
     * Base block class.
     *
     * @class module:xblox/model/mqtt/Subscribe
     * @extends module:xblox/model/Block
     */
    return dcl([Block,Contains],{
        declaredClass:"xblox.model.mqtt.Subscribe",
        //method: (String)
        //  block action name
        name:'Subscribe',
        //method: (String)
        //  block action name
        topic:'Topic',
        args:'',
        deferred:false,
        sharable:true,
        context:null,
        icon:'fa-bell',
        /**
         * @type {string|null}
         */
        path:'',
        qos:0,
        stop:function(){
            var instance = this.getInstance();
            if(instance){
                instance.callMethod('unSubscribeTopic',utils.mixin({
                    topic:this.topic
                },utils.getJson(this.args || {})),this.id,this.id);
            }
        },
        onData:function(message){
            if(message && message.topic && message.topic==this.topic){
                delete message['src'];
                delete message['id'];
                var items = this[this._getContainer()];
                var settings = this._lastSettings;
                var ret=[];
                if(items.length>0){
                    var value = message;
                    var path = this.path && this.path.length ? this.path : (message.payload!==null) ? 'payload' : null;
                    if(path && _.isObject(message)){
                        value = utils.getAt(message,path,message);
                    }
                    for(var n = 0; n < this.items.length ; n++)
                    {
                        var block = this.items[n];
                        if(block.enabled) {
                            block.override ={
                                args: [value]
                            };
                            ret.push(block.solve(this.scope,settings));
                        }
                    }
                }
                this.onSuccess(this, this._lastSettings);
                return ret;
            }
        },
        observed:[
            'topic'
        ],
        getContext:function(){
            return this.context || (this.scope.getContext ?  this.scope.getContext() : this);
        },
        solve:function(scope,settings){
            this._currentIndex = 0;
            this._return=[];
            this._lastSettings = settings;
            var instance = this.getInstance();

            if(instance){
                instance.callMethod('subscribeTopic',utils.mixin({
                    topic:this.topic,
                    qos:this.qos
                },utils.getJson(this.args ||"{}")),this.id,this.id);
            }
            settings = settings || {};
            this.onRunThis(settings);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){
            var result = '<span style="">' +
                this.getBlockIcon() + '' + this.makeEditable('topic','bottom','text','Enter a topic','inline') + ' :: '+'</span>';

            if(this.topic){
                result+= this.topic.substr(0,30);
            }
            return result;
        },
        //  standard call from interface
        canAdd:function(){
            return [];
        },
        //  standard call for editing
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(
                utils.createCI('name',13,this.name,{
                    group:'General',
                    title:'Name',
                    dst:'name'
                })
            );

            fields.push(utils.createCI('arguments',27,this.args,{
                    group:'Arguments',
                    title:'Arguments',
                    dst:'args'
                }));

            fields.push(utils.createCI('topic',types.ECIType.STRING,this.topic,{
                    group:'General',
                    title:'Topic',
                    dst:'topic',
                    select:true
                }));

            fields.push(utils.createCI('name',types.ECIType.ENUMERATION,this.qos,{
                    group:'General',
                    title:'QOS',
                    dst:'qos',
                    widget: {
                        options: [
                            {
                                label:"0 - at most once",
                                value:0
                            },
                            {
                                label:"1 - at least once",
                                value:1
                            },
                            {
                                label:"2 - exactly once",
                                value:2
                            }
                        ]
                    }
                })
            );
            fields.push(utils.createCI('path',types.ECIType.STRING,this.path,{
                group:'General',
                title:'Value Path',
                dst:'path'
            }));
            return fields;
        }
    });
});