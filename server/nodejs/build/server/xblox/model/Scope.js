//>>built
define("xblox/model/Scope","dcl/dcl ./ModelBase ./Expression xide/factory xide/utils xide/types xide/mixins/EventedMixin dojo/_base/lang dojo/has xide/encoding/MD5 xcf/model/Variable xdojo/has!host-node?nxapp/utils/_console".split(" "),function(r,t,A,w,k,q,B,x,C,y,D,u){var m="undefined"!==typeof window?window.console:"undefined"!==typeof global?global.console:u;u&&(m=u);var z=C("xcf-ui");t=r([t,B.dcl],{declaredClass:"xblox.model.Scope",variableStore:null,serviceObject:null,context:null,blockStore:null,
expressionModel:null,start:function(){if(!0===this.__didStartBlocks)m.error("already started blocks");else{this.__didStartBlocks=!0;var a=this.getVariable("value");a||(a=new D({id:k.createUUID(),name:"value",value:"",scope:this,type:13,group:"processVariables",gui:!1,cmd:!1}),this.blockStore.putSync(a));var a=[],b=this.getBlocks({group:q.COMMAND_TYPES.INIT_COMMAND}),c=this;try{_.each(b,function(a){!1!==a.enabled&&!0!==a.__started&&(a.solve(c),a.__started=!0)},this)}catch(e){m.error("starting init blocks failed",
e),logError(e,this)}a=a.concat(this.getBlocks({group:q.COMMAND_TYPES.BASIC_COMMAND}));for(b=0;b<a.length;b++){var d=a[b];d.enabled&&d.start&&d.startup&&!0!==d.__started&&(d.start(),d.__started=!0)}}},getExpressionModel:function(){this.expressionModel||(this.expressionModel=new A);return this.expressionModel},toFriendlyName:function(a,b){if(!b||!a)return null;var c=this,d=this.ctx,e=this.driver,e=d.getDeviceManager(),g=d.getDriverManager();if(-1==b.indexOf("://"))return(a=c.getBlockById(b))?a.name:
b;var d=k.parse_url(b),d=k.urlArgs(d.host),f=e.getItemById(d.device.value);if(f){var h=e.toDeviceControlInfo(f),e=g.getDriverById(h.driverId);if((f=f.driverInstance)||e){c=e.blockScope?e.blockScope:f?f.blockScope:c;if(!c)return m.error("failed to generate url "+b,a),"failed";if((a=c.getStore().getSync(d.block.value))||f&&f.blockScope&&(a=f.blockScope.getBlock(d.block.value)))return h.title+"/"+a.name}}return b},getContext:function(){return this.instance},toString:function(){var a={blocks:null,variables:null},
b=this.blocksToJson();try{k.fromJson(JSON.stringify(b))}catch(c){return}a.blocks=b;return JSON.stringify(a,null,2)},initWithData:function(a,b){a&&this.blocksFromJson(a,null,b);this.clearCache()},getService:function(){return this.serviceObject},getStore:function(){return this.blockStore},reset:function(){this.getExpressionModel().reset()},empty:function(){this.clearCache();var a=this.blockStore,b=this.getBlocks();a.silent(!0);_.each(b,function(b){b&&a.removeSync(b.id)});a.setData([]);a.silent(!1)},
fromScope:function(a){var b=this.blockStore;b.silent(!0);this.empty();a=a.blocksToJson();this.blocksFromJson(a);b.silent(!1)},clearCache:function(){this.getExpressionModel().reset()},getVariableStore:function(){return this.blockStore},getBlockStore:function(){return this.blockStore},getVariables:function(a){if(!this.blockStore)return[];var b=this.blockStore.data,c=[];if(a&&"processVariables"===a.group){for(a=0;a<b.length;a++)"processVariables"===b[a].group&&c.push(b[a]);return c}if(!a){for(a=0;a<
b.length;a++){var d=b[a],e=d.declaredClass;"xblox.model.variables.Variable"!=e&&"xcf.model.Variable"!=e||c.push(d)}return c}return this.blockStore.query(a)},loopBlock:function(a,b){1==a._destroyed&&m.error("block destroyed");var c=a.getInterval?a.getInterval():0;if(a&&0<c&&a.enabled&&!0!==a._destroyed){var d=this;a._loop&&clearInterval(a._loop);a._loop=setInterval(function(){!a.enabled||a._destroyed?(clearInterval(a._loop),a._loop=null):a.solve(d,b||a._lastSettings)},c)}},getEventsAsOptions:function(a){var b=
[],c;for(c in q.EVENTS)b.push({label:q.EVENTS[c],value:q.EVENTS[c]});b=b.concat([{label:"onclick",value:"onclick"},{label:"ondblclick",value:"ondblclick"},{label:"onmousedown",value:"onmousedown"},{label:"onmouseup",value:"onmouseup"},{label:"onmouseover",value:"onmouseover"},{label:"onmousemove",value:"onmousemove"},{label:"onmouseout",value:"onmouseout"},{label:"onkeypress",value:"onkeypress"},{label:"onkeydown",value:"onkeydown"},{label:"onkeyup",value:"onkeyup"},{label:"onfocus",value:"onfocus"},
{label:"onblur",value:"onblur"},{label:"onchange",value:"onchange"}]);for(c=0;c<b.length;c++){var d=b[c];if(d.value===a){d.selected=!0;break}}return b},getVariablesAsObject:function(){for(var a=this.getVariables(),b={},c=0;c<a.length;c++)b[a[c].title]=a[c].value;return b},getVariablesAsOptions:function(){var a=this.getVariables(),b=[];if(a)for(var c=0;c<a.length;c++)b.push({label:a[c].label,value:a[c].variable});return b},getCommandsAsOptions:function(a){var b=this.getBlocks({declaredClass:"xcf.model.Command"}),
c=[];if(b)for(var d=0;d<b.length;d++){var e={};e[a||"label"]=b[d].name;e.value=b[d].name;c.push(e)}return c},_cached:null,getBlocks:function(a,b){if(!z&&!1!==b&&(this._cached||(this._cached={}),a)){var c=y(JSON.stringify(a),1);if(c=this._cached[c])return c}if(!this.blockStore)return[];a=a||{id:/\S+/};var d=_.isEmpty(a)?this.blockStore.data:this.blockStore.query(a,null,!0);z||!1===b||(c=y(JSON.stringify(a),1),this._cached[c]=d);return d},registerVariable:function(a){this.variables[a.title]=a;this.blockStore&&
this.blockStore.putSync(a)},getVariable:function(a){for(var b=this.getVariables(),c=0;c<b.length;c++){var d=b[c];if(d.name===a)return d}return null},getVariableById:function(a){if(!a)return null;var b=a.split("/"),c=this;2==b.length&&((a=c.owner)&&a.hasScope&&(a.hasScope(b[0])?c=a.getScope(b[0]):m.error("have scope id but cant resolve it",this)),a=b[1]);return(b=c.blockStore.getSync(a))?b:null},registerBlock:function(a,b){var c=this.blockStore;if(c){var d=c.getSync(a.id);if(d)return d;d=null;return d=
a.addToStore?a.addToStore(c):c.putSync(a,b)}},allBlocks:function(a,b){return this.getBlocks({},b)},hasGroup:function(a){for(var b=this.allGroups({},!1),c=0;c<b.length;c++)if(b[c]===a)return!0;return!1},allGroups:function(){for(var a=[],b=this.allBlocks({},!1),c=function(b){for(var c=0;c<a.length;c++)if(a[c]===b)return!0;return!1},d=0;d<b.length;d++){var e=b[d];e.parentId||(e.group?c(e.group)||a.push(e.group):c("No Group")||a.push("No Group"))}return a},variablesToJson:function(){var a=[],b=this.variableStore?
this.getVariables():this.variables,c;for(c in b){var d=b[c];if(!1!==d.serializeMe&&null!=d.keys){var e={},g;for(g in d)if(this.isString(d[g])||this.isNumber(d[g])||this.isBoolean(d[g]))e[g]=d[g];a.push(e)}}return a},isScript:function(a){return this.isString(a)&&(-1!=a.indexOf("return")||-1!=a.indexOf(";")||-1!=a.indexOf("[")||-1!=a.indexOf("{")||-1!=a.indexOf("}"))},variablesToJavascriptEx:function(a,b){for(var c=[],d=this.variableStore?this.getVariables():this.variables,e=0;e<d.length;e++){var g=
d[e];if(g!=a){var f=""+g.value;a&&a.value&&-1==a.value.indexOf(g.title)||b&&-1==b.indexOf(g.title)||0==f.length||(this.isScript(f)||-1!=f.indexOf("'")?this.isScript(f)&&(f=this.expressionModel.parseVariable(this,g)):f="'"+f+"'","''"===f&&(f="'0'"),c.push(f))}}return c},variablesToJavascript:function(a,b){for(var c="",d=this.variableStore?this.getVariables():this.variables||[],e=0;e<d.length;e++){var g=d[e];if(g!=a){var f=""+g.value;a&&a.value&&-1==a.value.indexOf(g.title)||b&&-1==b.indexOf(g.title)||
0==f.length||(this.isScript(f)||-1!=f.indexOf("'")?this.isScript(f)&&(f=this.expressionModel.parseVariable(this,g)):f="'"+f+"'","''"===f&&(f="'0'"),c+="var "+g.title+" \x3d "+f+";",c+="\n")}}return c},variablesFromJson:function(a){for(var b=[],c=0;c<a.length;c++){var d=a[c];d.scope=this;if(d.declaredClass){var e=k.replaceAll(".","/",d.declaredClass);(e=require(e))&&b.push(new e(d))}else m.log("   variable has no class ")}return b},regenerateIDs:function(a){for(var b=this,c=function(a){var d=k.createUUID(),
e=b.getBlocks({parentId:a.id});if(e&&0<e.length)for(var h=0;h<e.length;h++){var l=e[h];l.parentId=d;c(l)}a.id=d},d=0;d<a.length;d++)c(a[d])},cloneBlocks2:function(a,b){a=this.blocksToJson(a);var c=this.owner.getScope(k.createUUID(),null,!1);a=c.blocksFromJson(a,!1);var d=this.blockStore;a=c.allBlocks();c.regenerateIDs(a);a=c.blocksToJson(a);if(b)for(c=0;c<a.length;c++){var e=a[c];null==e.parentId&&(e.group=b)}var g=[];a=this.blocksFromJson(a);_.each(a,function(a){g.push(d.getSync(a.id))});return g},
cloneBlocks:function(a){a=this.blocksToJson(a);var b=this.owner.getScope(k.createUUID(),null,!1);a=b.blocksFromJson(a,!1);a=b.allBlocks();for(b=0;b<a.length;b++){var c=a[b];c.id=k.createUUID();c.parentId=null}this.blocksToJson(a);this.blocksFromJson(a);return a},blockToJson:function(a){var b={_containsChildrenIds:[]},c;for(c in a)if("ctrArgs"!=c&&("function"===typeof a[c]||a.serializeField(c))){if(this.isString(a[c])||this.isNumber(a[c])||this.isBoolean(a[c]))b[c]=a[c];if("parent"!=c)if(this.isBlock(a[c]))b[c]=
a[c].id,b._containsChildrenIds.push(c);else if(this.areBlocks(a[c])){b[c]=[];for(var d=0;d<a[c].length;d++)b[c].push(a[c][d].id);b._containsChildrenIds.push(c)}}return b},blocksToJson:function(a){try{var b=[];a=a&&a.length?a:this.blockStore?this.blockStore.data:this.blocks;for(var c in a){var d=a[c];if(null!=d.keys&&!1!==d.serializeMe){var e={_containsChildrenIds:[]},g;for(g in d)if("ctrArgs"!=g&&("function"===typeof d[g]||d.serializeField(g))){if(this.isString(d[g])||this.isNumber(d[g])||this.isBoolean(d[g]))e[g]=
d[g];_.isObject(d[g])&&d.serializeObject&&!0===d.serializeObject(g)&&(e[g]=JSON.stringify(d[g],null,2));if("parent"!=g)if(this.isBlock(d[g]))e[g]=d[g].id,e._containsChildrenIds.push(g);else if(this.areBlocks(d[g])){e[g]=[];for(var f=0;f<d[g].length;f++)e[g].push(d[g][f].id);e._containsChildrenIds.push(g)}}b.push(e)}}}catch(h){m.error("from json failed : "+h)}return b},_createBlockStore:function(){},blockFromJson:function(a){a.scope=this;null==a._containsChildrenIds&&(a._containsChildrenIds=[]);for(var b=
{},c=0;c<a._containsChildrenIds.length;c++){var d=a._containsChildrenIds[c];b[d]=a[d];a[d]=null}delete a._containsChildrenIds;if(!a.declaredClass)return m.log("   not a class "),null;d=c=null;try{d=k.replaceAll(".","/",a.declaredClass),c=require(d)}catch(e){try{d=k.replaceAll("/",".",a.declaredClass),c=require(d)}catch(g){}}c||(c=r.getObject(a.declaredClass));if(!c)return null;d=null;try{d=w.createBlock(c,a)}catch(e){return logError(e),null}d._children=b;return d},blocksFromJson:function(a,b,c){for(var d=
[],e={},g=0;g<a.length;g++){var f=a[g];f.scope=this;null==f._containsChildrenIds&&(f._containsChildrenIds=[]);for(var h={},l=0;l<f._containsChildrenIds.length;l++){var n=f._containsChildrenIds[l];h[n]=f[n];f[n]=null}delete f._containsChildrenIds;if(f.declaredClass){var p=l=null;try{p=k.replaceAll(".","/",f.declaredClass),l=require(p)}catch(v){m.error("couldnt resolve class "+p)}l||(l=r.getObject(f.declaredClass));if(l){p=null;try{p=w.createBlock(l,f,null,!1)}catch(v){m.error("error in block creation ",
v+" "+f.declaredClass);logError(v);continue}p._children=h;e[p.id]=h;d.push(p)}else m.log("couldnt resolve "+p)}else m.log("   not a class ")}a=this.allBlocks(null,!1);for(g=0;g<a.length;g++){f=a[g];f._children=e[f.id];if(f._children){for(n in f._children)if("string"==typeof f._children[n])(h=this.getBlockById(f._children[n]))?(f[n]=h,h.parent=f,h.postCreate&&h.postCreate()):(this.blockStore.removeSync(f._children[n]),c&&c("   couldnt resolve child: "+f._children[n]+"@"+f.name+":"+f.declaredClass),
m.log("   couldnt resolve child: "+f._children[n]+"@"+f.name+":"+f.declaredClass));else if("object"==typeof f._children[n])for(f[n]=[],l=0;l<f._children[n].length;l++)if(h=this.getBlockById(f._children[n][l]))if(f[n].push(h),p=this.getBlockById(h.parentId))h.parent=p;else{if(h._failed)return;h._failed=!0;c?(h.group="basic",h.parentId=null,h.parent=null,h.name="Failed - "+h.name,c("child has no parent : child.id \x3d "+h.id+"")):m.error("child has no parent ")}else c&&c("   couldnt resolve child: "+
f._children[n]+"@ "+f.name+" : "+f.declaredClass),m.log("   couldnt resolve child: "+f._children[n][l]+"@"+f.name+":"+f.declaredClass);delete f._children}!1!==b&&null!=f.parentId&&null==this.getBlockById(f.parentId)&&(f.parentId=null);f.postCreate()}this.allBlocks();return d},resolveDevice:function(a){var b=this.ctx,c=this.device,d=b.getDeviceManager();b.getDriverManager();if(-1==a.indexOf("://"))return(c=this.getBlockById(a))?c:a;b=k.parse_url(a);b=k.urlArgs(b.host);a=d.getItemById(b.device.value);
!a&&(d=d.getInstanceByName(b.device.value))&&(a=d.device);return c||a},resolveBlock:function(a){var b=this,c=this.ctx,b=this.driver,b=this.device,d=c.getDeviceManager(),c=c.getDriverManager();if(-1==a.indexOf("://"))return(b=this.getBlockById(a))?b:a;a=k.parse_url(a);a=k.urlArgs(a.host);var e=d.getItemById(a.device.value);if(!e){var g=d.getInstanceByName(a.device.value);g&&(e=g.device)}if(e)if(d=d.toDeviceControlInfo(e)){if(b=c.getDriverById(d.driverId),(d=e.driverInstance)||b)if(a=(b=d?d.blockScope:
b.blockScope)?b.getStore().getSync(a.block.value):null)return a}else m.warn("cant get device info for "+e.title,b)},getBlock:function(a){return this.getBlockById(a)},getBlockByName:function(a){if(-1!==a.indexOf("://")){var b=this.resolveBlock(a);if(b)return b}for(var c=this.getBlocks(),d=0;d<c.length;d++)if(b=c[d],b.name===a)return b;return(a=this.blockStore.query({name:a}))&&0<a.length?a[0]:null},getBlockById:function(a){return this.blockStore.getSync(a)},_flatten:function(a){var b=[],c;for(c in a){var d=
a[c];if(null!=d.keys){b.push(d);for(var e in d)if("ctrArgs"!=e&&"parent"!==e)if(this.isBlock(d[e]))b.push(d[e]);else if(this.areBlocks(d[e]))for(var g=0;g<d[e].length;g++)b.push(d[e][g])}}return b},flatten:function(a){var b=[],c;for(c in a){var d=a[c];if(null!=d.keys){var e=_.find(b,{id:d.id});e||b.push(d);for(var g in d)if("ctrArgs"!=g&&"parent"!==g){var f=d[g];if(this.isBlock(f))(e=_.find(b,{id:f.id}))||b.push(f);else if(this.areBlocks(f))for(var h=0;h<f.length;h++){var l=f[h];(e=_.find(b,{id:l.id}))||
b.push(l);b=b.concat(this.flatten([l]))}}}}return b=_.uniq(b,!1,function(a){return a.id})},_getSolve:function(a){return a.prototype?a.prototype.solve:a.__proto__.solve},solveBlock:function(a,b,c,d){b=b||{highlight:!1};var e=null;this.isString(a)?(e=this.getBlockByName(a))||(e=this.getBlockById(a)):this.isObject(a)&&(e=a);a=null;if(e){if(!0!==b.force&&0==e.enabled)return null;!0===b.force&&(b.force=!1);var g=e.declaredClass;(g=x.getObject(k.replaceAll("/",".",g))||x.getObject(g))?g.prototype&&g.prototype.solve&&
(a=g.prototype.solve.apply(e,[this,b])):(a=e.solve(e.getScope(),b,c,d),delete e.override,e.override={})}return a},solve:function(a,b){for(var c="",d=0;d<this.items.length;d++)c+=this.items[d].solve(a,b);return c},parseExpression:function(a,b,c,d,e,g,f,h){return this.getExpressionModel().parse(this,a,b,d,e,g,c,f,h)},isString:function(a){return"string"==typeof a},isNumber:function(a){return"number"==typeof a},isBoolean:function(a){return"boolean"==typeof a},isObject:function(a){return"object"===typeof a},
isBlock:function(a){var b=!1;"object"==typeof a&&null!=a&&void 0==a.length&&a.serializeMe&&(b=!0);return b},areBlocks:function(a){var b=!1;"object"==typeof a&&null!=a&&0<a.length&&this.isBlock(a[0])&&(b=!0);return b},_onVariableChanged:function(a){a.item&&this.getExpressionModel().variableFuncCache[a.item.title]&&delete this.expressionModel.variableFuncCache[a.item.title]},init:function(){this.getExpressionModel();this.subscribe(q.EVENTS.ON_DRIVER_VARIABLE_CHANGED,this._onVariableChanged);var a=this;
this.subscribe(q.EVENTS.ON_MODULE_RELOADED,function(b){var c=b.module,d=b.newModule;(b=a.getBlocks().filter(function(a){return a.declaredClass==c||a.declaredClass==k.replaceAll("/",".",c)?a:null}))&&_.each(b,function(a){var b=d.prototype,c;for(c in b){var e=b[c];e&&_.isFunction(e)&&(a[c]=e)}})})},_destroy:function(){for(var a=this.allBlocks(),b=0;b<a.length;b++){var c=a[b];if(c)try{c&&c.stop&&c.stop(!0),c&&c.reset&&c.reset(),c&&c._destroy&&c._destroy(),c&&c.destroy&&c.destroy(),c._emit&&c._emit(q.EVENTS.ON_ITEM_REMOVED,
{item:c})}catch(d){}}},destroy:function(){this._destroy();this.reset();this._destroyed=!0;delete this.expressionModel},moveTo:function(a,b,c,d){m.log("move to : ",arguments);if(d){if(b.canAdd&&b.canAdd()){var e=this.getBlockById(a.parentId);e&&e.removeBlock(a,!1);return b.add(a,null,null)}m.error("cant reparent");return!1}if(b.parentId||!1!==d)if(!a.parentId&&b.parentId&&0==d)a.group=b.group;else if(a.parentId||b.parentId||!d){if(a.parentId&&b.parentId&&0==d&&a.parentId===b.parentId){e=this.getBlockById(a.parentId);
if(!e)return!1;for(var g=e[e._getContainer(a)],e=a.indexOf(g,a),f=a.indexOf(g,b),g=e>f?-1:1,f=Math.abs(e-(f+(1==c?-1:1))),e=0;e<f-1;e++)a.move(g);return!0}if(a.parentId&&b.parentId&&0==d&&a.parentId!==b.parentId){e=this.getBlockById(a.parentId);if(!e)return!1;var h=this.getBlockById(b.parentId);if(h&&e&&e.removeBlock&&h.canAdd&&h.canAdd())e.removeBlock(a,!1),h.add(a,null,null);else return!1;g=h[h._getContainer(a)];null==g&&m.error("weird : target parent has no item container");e=h.indexOf(g,a);f=
h.indexOf(g,b);if(!e||!f){m.error(" weird : invalid drop processing state, have no valid item indicies");return}g=e>f?-1:1;f=Math.abs(e-(f+(1==c?-1:1)));for(e=0;e<f-1;e++)h.move(a,g);return!0}}else return b.canAdd&&b.canAdd()&&(a.group=null,b.add(a,null,null)),!0;else{(e=this.getBlockById(a.parentId))&&e.removeBlock&&(e.removeBlock(a,!1),a.parentId=null,a.group=b.group);for(var h=[],f=this.getBlocks({group:b.group}),l=[],g=this.getBlockStore(),n=g.storage.index[b.id],e=0;e<f.length;e++){var k=f[e];
null==f[e].parentId&&f[e]!=a&&(k=g.storage.index[k.id],d=c?k>=n:k<=n)&&(h.push(f[e]),l.push(g.storage.index[f[e].id]))}for(e=0;e<h.length;e++)g.remove(h[e].id);this.getBlockStore().remove(a.id);c&&this.getBlockStore().putSync(a);for(e=0;e<h.length;e++)g.put(h[e]);c||this.getBlockStore().putSync(a);return!0}return!1}});r.chainAfter(t,"destroy");return t});
//# sourceMappingURL=Scope.js.map