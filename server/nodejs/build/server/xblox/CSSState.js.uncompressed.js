define("xblox/CSSState", [
    "dcl/dcl",
    "delite/register",
    "delite/CustomElement",
    'xblox/_State',
    'xide/utils',
    'xdojo/has'
], function (dcl, register, CustomElement, _State, utils, has) {
    var extraRules = [],
        extraSheet,
        removeMethod,
        rulesProperty,
        invalidCssChars = /([^A-Za-z0-9_\u00A0-\uFFFF-])/g;

    has.add('dom-contains', function (global, doc, element) {
        return !!element.contains; // not supported by FF < 9
    });

    function removeRule(index) {
        // Function called by the remove method on objects returned by addCssRule.
        var realIndex = extraRules[index],
            i, l;
        if (realIndex === undefined) {
            return; // already removed
        }

        // remove rule indicated in internal array at index
        extraSheet[removeMethod](realIndex);

        // Clear internal array item representing rule that was just deleted.
        // NOTE: we do NOT splice, since the point of this array is specifically
        // to negotiate the splicing that occurs in the stylesheet itself!
        extraRules[index] = undefined;

        // Then update array items as necessary to downshift remaining rule indices.
        // Can start at index + 1, since array is sparse but strictly increasing.
        for (i = index + 1, l = extraRules.length; i < l; i++) {
            if (extraRules[i] > realIndex) {
                extraRules[i]--;
            }
        }
    }
    var Impl = {
        _lastState: null,
        declaredClass: 'xblox/CSSState',
        cssClass: "",
        addCssRule: function (selector, css) {
            // summary:
            //		Dynamically adds a style rule to the document.  Returns an object
            //		with a remove method which can be called to later remove the rule.

            if (!extraSheet) {
                // First time, create an extra stylesheet for adding rules
                extraSheet = document.createElement('style');
                document.getElementsByTagName('head')[0].appendChild(extraSheet);
                // Keep reference to actual StyleSheet object (`styleSheet` for IE < 9)
                extraSheet = extraSheet.sheet || extraSheet.styleSheet;
                // Store name of method used to remove rules (`removeRule` for IE < 9)
                removeMethod = extraSheet.deleteRule ? 'deleteRule' : 'removeRule';
                // Store name of property used to access rules (`rules` for IE < 9)
                rulesProperty = extraSheet.cssRules ? 'cssRules' : 'rules';
            }

            var index = extraRules.length;
            extraRules[index] = (extraSheet.cssRules || extraSheet.rules).length;
            extraSheet.addRule ?
                extraSheet.addRule(selector, css) :
                extraSheet.insertRule(selector + '{' + css + '}', extraRules[index]);
            return {
                get: function (prop) {
                    return extraSheet[rulesProperty][extraRules[index]].style[prop];
                },
                set: function (prop, value) {
                    if (typeof extraRules[index] !== 'undefined') {
                        extraSheet[rulesProperty][extraRules[index]].style[prop] = value;
                    }
                },
                remove: function () {
                    removeRule(index);
                },
                sheet: extraSheet
            };
        },
        escapeCssIdentifier: function (id, replace) {
            return typeof id === 'string' ? id.replace(invalidCssChars, replace || '\\$1') : id;
        },
        detachedCallback: function () {
            this._styles && _.each(this._styles, function (style) {
                style.remove();
            });
            delete this._styles;
        },
        applyTo: function (widget, name) {
            if (this._lastState) {
                this._lastState.remove();
            }
            delete this._lastStateName;
            this._lastStateName = name;
            if (!this._attached) {
                return;
            }
            var cssClass = this.cssClass;
            var isCSSClass = cssClass.length > 0;
            var id = widget.id || utils.createUUID();
            var _uniqueId = widget.tagName.replace(/\./g, "_") + '_' + id;
            var css = '' + this.innerHTML;
            css = css.replace('.style', '');
            css = css.replace(/<(?:.|\n)*?>/gm, '');
            css = css.replace('{', '');
            css = css.replace('}', '');
            css = css.replace(/(\r\n|\n|\r|\t)/gm, "");

            _uniqueId += '_state_' + name;

            $(widget).removeClass($(widget).data('_lastCSSState'));
            $(widget).removeClass($(widget).data('_lastCSSClass'));
            $(widget).removeClass(cssClass);

            if (!cssClass) {
                $(widget).addClass(_uniqueId);
                $(widget).data('_lastCSSState', _uniqueId);
                var selectorPrefix = '.' + this.escapeCssIdentifier(_uniqueId);
                if (!this._styles) {
                    this._styles = [];
                }
                var style = this.addCssRule(selectorPrefix, css);
                this._styles.push(style);
            } else {
                $(widget).addClass(cssClass);
                $(widget).data('_lastCSSClass', cssClass);
            }
        }

    };

    var _class = dcl([_State], Impl);
    //static access to Impl.
    _class.Impl = Impl;
    return register("d-xstate-css", [HTMLElement, CustomElement, _class]);
});