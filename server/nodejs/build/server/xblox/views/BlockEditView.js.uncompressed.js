define("xblox/views/BlockEditView", [
    'dcl/dcl',
    'xide/views/CIView'
],function (dcl,CIView){
    return dcl([CIView],{
        declaredClass:"xblox.views.BlockEditView",
        onSave:function(ci,value){
            if(this.delegate && this.delegate.onSave){
                this.delegate.onSave(ci,value);
            }
        }
    });
});