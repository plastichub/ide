/** @module xblox/views/BlockGrid **/
define("xblox/views/BlockGrid", [
    'dojo/_base/declare',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xblox/views/Grid',
    'xide/layout/_Accordion',
    'xide/views/CIViewMixin',
    'xaction/DefaultActions',
    'dojo/dom-class',
    'dojo/Deferred',
    'xide/views/History',
    'xide/widgets/IconPicker' // not loaded yet
], function (declare, types, utils, factory, Grid, _Accordion, CIViewMixin, DefaultActions, domClass, Deferred, History, IconPicker) {

    var ACTION = types.ACTION;
    var debugMark = false;

    function clearMarkTimer(element) {
        if (element.hightlightTimer) {
            clearTimeout(element.hightlightTimer);
            element.hightlightTimer = null;
        }
    }

    /**
     * Standard block grid class.
     * @class module:xblox/views/BlockGrid
     * @augments module:xide/mixins/EventedMixin
     */
    var IMPLEMENTATION = {
        highlightDelay: 500,
        currentCIView: null,
        targetTop: null,
        lastSelectedTopTabTitle: 'General',
        showHeader: false,
        toolbarInitiallyHidden: true,
        contextMenuArgs: {
            actionFilter: {
                quick: true
            }
        },
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Step': 115,
            'New': 4,
            'Window': 1
        },
        permissions: [
            //ACTION.TOOLBAR
            //ACTION.EDIT,
            //ACTION.RENAME,
            ACTION.RELOAD,
            ACTION.DELETE,
            ACTION.CLIPBOARD,
            ACTION.LAYOUT,
            ACTION.COLUMNS,
            ACTION.SELECTION,
            ACTION.DOWNLOAD,
            //ACTION.PREVIEW,
            ACTION.SAVE,
            ACTION.SEARCH,
            'Step/Run',
            'Step/Move Up',
            'Step/Move Down',
            ACTION.TOOLBAR,
            'Step/Properties',
            'Step/Edit',
            'Step/Back',
            'Step/Pause',
            ACTION.HEADER,
            ACTION.CONTEXT_MENU
        ],
        tabOrder: {
            'Home': 100,
            'View': 50,
            'Settings': 20
        },
        tabSettings: {
            'Step': {
                width: '190px'
            },
            'Show': {
                width: '130px'
            },
            'Settings': {
                width: '100%'
            }
        },
        groupOrder: {
            'Clipboard': 110,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'Select': 0
        },
        options: utils.clone(types.DEFAULT_GRID_OPTIONS),
        columns: [
            {
                renderExpando: true,
                label: "Name",
                field: "name",
                sortable: false,
                formatter: function (value, block) {
                    var blockText = '';
                    if (block.toText) {

                        blockText = block.toText();

                        return '<span class="" style="width: 95%;">' + blockText + '</span>';
                    }
                    return value;
                }
            }
        ],
        getTypeMap: function () {
            return {};
        },
        getBlock: function (id) {
            return this.blockScope.getBlockById(id);
        },
        formatOrder: function (value, block) {

            if (block) {
                var parent = block.getParent();
                var _index = block.index();
                if (parent) {
                    var parents = block.numberOfParents();
                    var _out = '&nbsp;';
                    for (var i = 0; i < parents; i++) {
                        _out += '&nbsp;'
                    }
                    _out += '';

                    for (var i = 0; i < parents; i++) {
                        _out += '-'
                    }
                    _out += '&nbsp;';
                    _index = _out + _index;
                } else {
                    return '&nbsp;' + _index;
                }
                return _index;
            }

            return '';
        },
        focus: function (element) {

            if (!element) {
                return;
            }
            return this.inherited(arguments);
        },
        postMixInProperties: function () {

            var _has = false;

            var self = this;
            _.each(this.columns, function (col) {
                if (col.field == 'order') {
                    _has = true;
                }
            });

            if (!_has && this.blockGroup !== 'basicVariables') {
                this.columns.unshift({
                    label: "Line",
                    field: "order",
                    sortable: false,
                    hidden: false,
                    formatter: function (val, b) {
                        return self.formatOrder(val, b, self);
                    }
                });
            }
            this.inherited(arguments);

        }, /*
         resize:function(){
         console.log('-r');
         return this.inherited(arguments);
         },
         */
        /**
         * Override render row to add additional CSS class
         * for indicating a block's enabled state
         * @param object
         * @returns {*}
         */
        renderRow: function (object) {

            if (!this.collection) {
                console.warn('BlockGrid::renderRow : have no collection! Abort');
                return;
            }

            var res = this.inherited(arguments);

            if (object.enabled == false) {
                domClass.add(res, 'disabledBlock');
            }
            var _row = $(res),
                scope = object.getScope();
            var scopeId = scope ? scope.id : null;

            //restore status css class
            if (object.getStatusClass && scopeId) {
                var clsInfo = object.getStatusClass(scopeId);
                var element = $(res);
                //the block is marked with a class, restore it
                if (clsInfo && !element[0].hightlightTimer) {
                    this.mark($(res), clsInfo.className, object, clsInfo.delay);
                }
            }

            //add inline editors
            if (object.hasInlineEdits) {
                if (_row.__inline) {
                    return;
                }
                _row.__inline = true;
                var _links = _row.find('.editable');
                _.each(_links, function (link) {
                    link.object = object;

                    var $link = $(link),
                        _field = $link.attr('data-prop'),
                        _type = $link.attr('display-type') || 'text',
                        options = object.getFieldOptions ? object.getFieldOptions(_field) : null,
                        _editOptions = {
                            type: _type,
                            placement: $link.attr('pos') || 'right',
                            mode: $link.attr('display-mode') || 'popup',
                            title: $link.prop('data-title'),
                            inputclass: 'input-transparent',
                            success: function (data, value) {
                                var prop = $(this).attr('data-prop');
                                this.object.set(prop, value);
                            }
                        };
                    if (options) {
                        _editOptions.source = options;
                    }
                    $link.editable(_editOptions);
                    $link.attr("tabIndex", "-1");
                });
            }
            return res;
        },
        getCurrentParent: function () {
            var history = this.getHistory();
            var now = history.getNow();
            if (now) {
                return this.getBlock(now);
            } else {
                return null;
            }
        },
        /**
         *
         * @param item
         * @param action
         */
        addItem: function (item, action) {

            if (!item) {
                return false;
            }

            action = action || {};

            var target = this.getSelection()[0],
                proto = item.proto || action.proto,
                ctorArgs = item.ctrArgs || action.ctrArgs,
                where = null,
                newBlock = null;


            if (!target) {
                target = this.getCurrentParent();
            }

            //cache problem:
            if (!target) {
                var _item = this.getSelection()[0];
                if (_item) {
                    target = _item;
                }
            }


            if (ctorArgs) {
                if (ctorArgs.id) {
                    ctorArgs.id = utils.createUUID();
                }
                if (ctorArgs.parent) {
                    ctorArgs.parent = null;
                }
                if (ctorArgs.parentId) {
                    ctorArgs.parentId = null;
                }
                if (ctorArgs.items) {
                    ctorArgs.items = [];
                }
            }

            if (target && proto && ctorArgs) {

                if (target.owner && target.dstField) {
                    where = target.dstField;
                    target = target.owner;
                }

                ctorArgs['parentId'] = target.id;
                ctorArgs['group'] = null;
                ctorArgs['parent'] = target;//should be obselete

                newBlock = target.add(proto, ctorArgs, where);
                newBlock.parent = target;

            } else if (!target && proto && ctorArgs) {

                if (ctorArgs.group === "No Group" && this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.blockGroup) {
                    ctorArgs.group = this.blockGroup;
                }

                if (ctorArgs.group && this.newRootItemGroup && ctorArgs.group !== this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }

                newBlock = factory.createBlock(proto, ctorArgs);//root block
            }

            if (newBlock && newBlock.postCreate) {
                newBlock.postCreate();
            }

            if (!newBlock) {
                console.error('didnt create block');
                return;
            }


            var store = this.getStore(target) || this.collection,
                storeItem = store.getSync(newBlock[store.idProperty]);

            if (!storeItem) {
                console.error('new block not in store');
                store.putSync(newBlock);
            }

            try {
                newBlock.scope._emit(types.EVENTS.ON_ITEM_ADDED, {
                    item: newBlock,
                    owner: newBlock.scope
                });
            } catch (e) {
                logError(e);
            }
            this._itemChanged('added', newBlock, store);

            return storeItem;

        },
        _itemChanged: function (type, item, store) {

            store = store || this.getStore(item);

            var thiz = this;

            function _refreshParent(item, silent) {

                var parent = item.getParent();
                if (parent) {
                    var args = {
                        target: parent
                    };
                    if (silent) {
                        this._muteSelectionEvents = true;
                    }
                    store.emit('update', args);
                    if (silent) {
                        this._muteSelectionEvents = false;
                    }
                } else {
                    thiz.refresh();
                }
            }

            function refresh() {
                thiz.refreshRoot();
                //thiz.refreshCurrent();
                thiz.refresh();
                thiz.refreshCurrent();
            }

            function select(item, reason) {
                thiz.select(item, null, true, {
                    focus: true,
                    delay: 1,
                    append: false,
                    expand: true
                }, reason);
            }


            switch (type) {

                case 'added': {
                    refresh();
                    select(item);
                    break;
                }
                case 'moved': {
                    break;
                }
                case 'deleted': {
                    var parent = item.getParent();
                    var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                    if (parent) {
                        var _container = parent.getContainer();
                        if (_container) {
                            _.each(_container, function (child) {
                                if (child.id == item.id) {
                                    _container.remove(child);
                                }
                            });
                        }
                    }
                    this.refresh();
                    if (_prev) {
                        select(_prev);
                    }
                    break;
                }
            }
        },
        printRootOrder: function (items) {

            var items = items || this.collection.storage.fullData;
            var str = '';
            _.each(items, function (item, i) {
                str += 'id: ' + item.id + ' | ' + i + ' \n';
            });
            console.log('order: \n' + str, JSON.stringify(this.collection.storage.index));

        },
        _refresh:function(){
            console.log('refresh');
            //var res = this.inherited(arguments);
            this.refreshRoot();
            //eturn res;
        },
        /**
         *
         * @param selection
         */
        refreshAll: function (selection) {

            if (!selection) {
                this._preserveSelection();
            }

            this.refreshRoot();
            this.refresh();

            if (!selection) {
                this._restoreSelection();
            }
            var thiz = this;

            if (selection) {
                setTimeout(function () {
                    thiz.select(selection, null, true, {
                        focus: true
                    });
                }, 30);
            }
        },
        getStore: function (item) {
            if (item) {
                if (_.isArray()) {
                    item = item[0];
                }
                if (item._store) {
                    return item._store;
                }
            }
            return null;
        },
        /**
         * Special refresh for changes at root level
         */
        refreshRoot: function () {
            return this.set('collection', this.collection.filter(this.getRootFilter()));
        },
        refresCurrent: function () {
            return this.set('collection', this.collection.filter(this.getRootFilter()));
        },
        /**
         * Standard filter for root level
         * @returns {{group: (null|string|*|string|className)}}
         */
        getRootFilter: function () {
            return {
                group: this.blockGroup
            };
        },
        _onCIChanged: function (ci, block, oldValue, newValue, field) {
        },
        clearPropertyPanel: function () {

            var props = this.getPropertyStruct();

            if (props.currentCIView) {
                props.currentCIView.empty();
                props.currentCIView = null;
            }

            if (props.targetTop) {
                utils.destroy(props.targetTop, true);
                props.targetTop = null;
            }

            props._lastItem = null;

            this.setPropertyStruct(props);
        },
        getState: function (state) {
            state = this.inherited(arguments) || {};
            var right = this.getRightPanel();
            state.properties = right.isExpanded();
            return state;
        },
        onCIChanged: function (ci, block, oldValue, newValue, field, cis) {
            if (oldValue === newValue) {
                return;
            }
            var _col = this.collection;
            block = this.collection.getSync(block.id);
            block.set(field, newValue);
            this.refreshActions();
            block[field] = newValue;
            _col.emit('update', {
                target: block,
                type: 'update'
            });
            block._store.emit('update', {
                target: block
            });
            this._itemChanged('changed', block, _col);
            block.onChangeField(field, newValue, cis, this);
            var cell = this.cell(block.id, '1');
            if (cell && cell.column) {
                this.refreshCell(cell).then(function (e) {
                });
            }
            if (field === 'enabled') {
                this.refresh();
            }
            this._itemChanged('changed', block, _col);
        },
        onShowProperties: function (item) {
            var _console = this._console;
            if (_console && item) {
                var value = item.send || item.method;
                var editor = _console.getConsoleEditor();
                editor.set('value', value);
                this._lastConsoleItem = item;
            }
        },
        showProperties: function (item, force) {
            var dfd = new Deferred(),
                block = item || this.getSelection()[0],
                thiz = this,
                rightSplitPosition = thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);

            if (!block || rightSplitPosition == 1 || block.canEdit()===false) {
                return;
            }
            var right = this.getRightPanel('Properties', null, 'DefaultTab', {});
            right.closeable(false);
            var props = this.getPropertyStruct();
            if (block == props._lastItem && force !== true) {
                return;
            }

            this.clearPropertyPanel();
            props = this.getPropertyStruct();
            props._lastItem = item;
            var _title = block.name || block.title;
            function propertyTabShown(tab, show) {
                if (!tab) {
                    return;
                }
                var title = tab.title;

                if (show) {
                    props.lastSelectedTopTabTitle = title;

                } else if (!show && props.lastSelectedTopTabTitle === title) {
                    props.lastSelectedTopTabTitle = null;
                }
            }

            var tabContainer = props.targetTop;
            if (!tabContainer) {
                tabContainer = utils.addWidget(_Accordion, {}, null, right.containerNode, true);

                $(tabContainer.domNode).addClass('CIView Accordion');
                props.targetTop = tabContainer;
                thiz.addHandle('show', tabContainer._on('show', function (evt) {
                    propertyTabShown(evt.view, true);
                }));

                thiz.addHandle('hide', tabContainer._on('hide', function (evt) {
                    propertyTabShown(evt.view, false);
                }));
            }

            _.each(tabContainer.getChildren(), function (tab) {
                tabContainer.removeChild(tab);
            });

            if (props.currentCIView) {
                props.currentCIView.empty();
            }

            if (!block.getFields) {
                console.log('have no fields', block);
                return;
            }

            var cis = block.getFields();
            for (var i = 0; i < cis.length; i++) {
                cis[i].vertical = true;
            }

            var ciView = new CIViewMixin({
                typeMap: thiz.getTypeMap(),
                tabContainerClass: _Accordion,
                tabContainer: props.targetTop,
                delegate: this,
                viewStyle: 'padding:0px;',
                autoSelectLast: true,
                item: block,
                source: this.callee,

                options: {
                    groupOrder: {
                        'General': 1,
                        'Advanced': 2,
                        'Script': 3,
                        'Arguments': 4,
                        'Description': 5,
                        'Share': 6

                    }
                },
                cis: cis
            });
            ciView.initWithCIS(cis);
            props.currentCIView = ciView;
            if (block.onFieldsRendered) {
                block.onFieldsRendered(block, cis);
            }
            thiz.addHandle('valueChanged', ciView._on('valueChanged', function (evt) {
                setTimeout(function () {
                    thiz.onCIChanged && thiz.onCIChanged(evt.ci, block, evt.oldValue, evt.newValue, evt.ci.dst, cis);
                }, 10);
            }));
            var tabToOpen = tabContainer.getTab(props.lastSelectedTopTabTitle);
            if (tabToOpen) {
                tabContainer.selectChild(props.lastSelectedTopTabTitle, true);
            } else {
                props.lastSelectedTopTabTitle = null;
            }
            this.setPropertyStruct(props);
            this.onShowProperties(block);
            return dfd;

        },
        onSelectionChanged: function (evt, force) {
            var selection = evt.selection;
            var item = selection[0];
            var thiz = this,
                ctx = this.ctx,
                actionStore = thiz.getActionStore(),
                contextMenu = this.getContextMenu(),
                toolbar = this.getToolbar(),
                mainToolbar = ctx.getWindowManager().getToolbar(),
                itemActions = item && item.getActions ? item.getActions() : null,
                _debugTree = false,
                enableAction = this.getAction('Step/Enable');

            if (item) {
                enableAction.value = item.enabled;
            }
            var globals = actionStore.query({
                global: true
            });

            if (globals && !itemActions) {
                itemActions = [];
            }

            _.each(globals, function (globalAction) {
                var globalItemActions = globalAction.getActions ? globalAction.getActions(thiz.permissions, thiz) : null;
                if (globalItemActions && globalItemActions.length) {
                    itemActions = itemActions.concat(globalItemActions);
                }
            });
            contextMenu && contextMenu.removeCustomActions();
            toolbar && toolbar.removeCustomActions();
            mainToolbar && mainToolbar.removeCustomActions();
            if (!itemActions || _.isEmpty(itemActions)) {
                return;
            }
            var newStoredActions = this.addActions(itemActions);
            actionStore._emit('onActionsAdded', newStoredActions);
        },
        startup: function () {
            if (this._started) {
                return;
            }
            this.inherited(arguments);
            this._history = new History();
            if (!this.propertyStruct) {
                this.propertyStruct = {
                    currentCIView: null,
                    targetTop: null,
                    _lastItem: null
                };
            }
            this._dontSubscribeToHighligt = true;

            $(this.domNode).addClass('blockGrid');
            var clipboardData, pastedData;
            /*
             $(this.domNode).on('paste',function(e){
             e.stopPropagation();
             e.preventDefault();
             console.error('paste',e);
             // Get pasted data via clipboard API
             clipboardData = e.clipboardData || window.clipboardData;
             pastedData = clipboardData.getData('Text');

             // Do whatever with pasteddata
             alert(pastedData);
             });
             */
            var thiz = this,
                permissions = this.permissions || [];

            if (permissions) {
                var _defaultActions = DefaultActions.getDefaultActions(permissions, this);
                _defaultActions = _defaultActions.concat(this.getBlockActions(permissions));
                this.addActions(_defaultActions);
            }

            if (DefaultActions.hasAction(permissions, 'Step/Properties')) {
                this._on('selectionChanged', function (evt) {
                    var selection = evt.selection,
                        item = selection[0];

                    clearTimeout(thiz._propsTimer);
                    thiz._propsTimer = null;
                    if (item) {
                        thiz._propsTimer = setTimeout(function () {
                            var props = thiz.getPropertyStruct();
                            thiz.showProperties(item);
                        }, 500);
                    } else if (!item && evt.why == 'clear') {
                        //clear property panel when empty
                        thiz.clearPropertyPanel();
                    }
                    evt.view = thiz;
                    item && thiz.publish('ON_SELECTED_BLOCK',evt);
                });
            }

            this.addHandle('update', this.collection.on('update', function (evt) {
                var item = evt.target,
                    type = evt.type;

                if (type === 'update' && evt.property === 'value') {
                    var node = thiz.toNode(evt);
                    if (node) {
                        thiz.mark(node, 'successBlock', item);
                    }
                }
            }));

            //console.error('- subscribe ' + this.collection.id + ' group ' + this.blockGroup);
            this.addHandle('added', this.collection._on('added', function (e) {
                if (thiz._refreshTimer) {
                    return;
                }

                thiz._refreshTimer = setTimeout(function () {
                    thiz.refresh();
                    delete thiz._refreshTimer;
                    thiz._refreshTimer = null;
                    e.target && thiz.select(e.target, null, true, {
                        focus: true,
                        append: false,
                        expand: true,
                        delay: 10
                    });
                }, 10);
            }));

            this.addHandle('update', this.collection._on('update', function (evt) {
                var item = evt.target,
                    node = thiz.toNode(evt),
                    type = evt.type;
                if (type === 'update' && evt.property === 'value') {
                    thiz.mark(node, 'successBlock', item);
                }
            }));


            this.subscribe(types.EVENTS.ON_RUN_BLOCK, function (evt) {
                thiz.mark(thiz.toNode(evt), 'activeBlock', evt.target, evt.timeout);
            });
            this.subscribe(types.EVENTS.ON_RUN_BLOCK_FAILED, function (evt) {
                thiz.mark(thiz.toNode(evt), 'failedBlock', evt.target, evt.timeout);
            });
            this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS, function (evt) {
                thiz.mark(thiz.toNode(evt), 'successBlock', evt.target, evt.timeout);
            });

            this._on('selectionChanged', this.onSelectionChanged, this);
            this.publish('ON_BLOCK_GRID',{
                view:this
            });
            
            this.setRenderer(this.selectedRenderer, false);
            
        },
        /**
         * Override removeRow to prevent marking leaks
         * @param rowElement {HTMLElement}
         * @param preserveDom {boolean}
         * @returns {*|{dir, lang}}
         */
        removeRow: function (rowElement, preserveDom) {
            rowElement = rowElement.element || rowElement;
            if (rowElement && rowElement.hightlightTimer && preserveDom === false) {
                clearMarkTimer(rowElement);
            }
            return this.inherited(arguments);
        },
        mark: function (element, cssClass, block, timeout) {
            if (!element) {
                return;
            }
            if (!this.isRendered(block)) {
                return;
            }
            block = block || {};
            if (this._highlight === false) {
                return;
            }
            if (block.__ignoreChangeMark === true) {
                return;
            }
            var thiz = this;
            var debugMark = false;
            timeout = timeout == false ? false : (thiz.highlightDelay || 500);
            //already
            if (element[0]._currentHighlightClass === cssClass) {
                return;
            }

            delete element[0]._currentHighlightClass;
            element[0]._currentHighlightClass = null;
            element[0]._currentHighlightClass = cssClass;
            var scopeId = block.getScope().id;
            //debugMark && console.log('mark ' + cssClass + ' delay ' + timeout + ' scope id');;
            element.removeClass('failedBlock successBlock activeBlock');
            element.addClass(cssClass);
            block.setStatusClass(scopeId, {
                className: cssClass,
                delay: timeout
            });
            clearMarkTimer(element[0]);
            if (timeout !== false && timeout > 0) {
                element[0].hightlightTimer = setTimeout(function () {
                    element.removeClass(cssClass);
                    element.removeClass('failedBlock successBlock activeBlock');
                    block.setStatusClass(scopeId, null);
                    delete element[0]._currentHighlightClass;
                    element[0]._currentHighlightClass = null;
                }, timeout);
            }
        },
        toNode: function (evt) {
            var item = evt.target || evt;
            if (item) {
                var row = this.row(item);
                if (row) {
                    var element = row.element;
                    if (element) {
                        return $(element);
                    }
                }
            }
            return null;
        }
    };

    var Module = declare("xblox.views.BlockGrid", Grid,IMPLEMENTATION);

    Module.IMPLEMENTATION = IMPLEMENTATION;

    return Module;

});
