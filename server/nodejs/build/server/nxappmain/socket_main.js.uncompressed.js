require([
    "dojo/_base/lang",
    "dojo/node!util",
    "dojo/node!readline",
    "nxapp/utils",
    "nxapp/types",
    "nxapp/utils/FileUtils",
    "nxapp/utils/ProvadisConverter",
    "nxappmain/nxAppBase",
    'nxapp/manager/Context',
    'nxapp/server/WebSocket'

], function(lang,util,readline,xmlutils,types,fileUtils,ProvadisConverter,base,Context,WebSocket)
{
    var app = new nxappmain.nxAppBase();

    app.init();
    var profile = app.profile;

    var ctx = new Context();
    ctx.constructManagers();
    ctx.initManagers(profile);

    WSocket = new WebSocket();
    WSocket.init({
        options:{
            port:app.profile.socket_server.port,
            context: ctx
        }
    });
    WSocket.start(null,profile);
    WSocket.emit("Test",{data:'data'});
});