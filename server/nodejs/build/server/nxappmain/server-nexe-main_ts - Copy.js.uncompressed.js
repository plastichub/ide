define("nxappmain/server-nexe-main_ts - Copy", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var commander = require('commander');
var _path = require('path');
var os= require('os');
var sockjs= require('sockjs');
var sockjs_client= require('sockjs-client');
var colors= require('colors');
var ansi_to_html= require('ansi-to-html');
var ansi_up= require('ansi_up');
var progress= require('progress');
var request_progress= require('request-progress');
var walk= require('walk');
var readline= require('readline');
var deferred= require('deferred');
var safe= require('colors/safe');
var unzip= require('unzip');
var lodash= require('lodash');
var strip_ansi= require('strip-ansi');

var cwd = _path.resolve('./');

global.moduleCache={
    "deferred":deferred,
    "strip-ansi":strip_ansi,
    "ansi-to-html":ansi_to_html,
    "ansi_up":ansi_up,
    "unzip":unzip,
    "commander":commander
};
var _re = global.require ? global.require : require;

global['_'] = _re('lodash');

global['oriRequire'] = require;

global['logError']=function(e,msg){
    console.error(msg,e);
    var stack = new Error().stack;
    console.log( stack );
};

global['nRequire'] = global.require ? global.require : require;
global['cwd'] = _path.resolve('./');
commander
	.version('0.0.1')
	.option('-c, --command <name>', 'command to send')
	.option('-i, --info', 'return service profile')
	.option('-f, --file <path>', 'run a file')
	.option('--diagnose <boolean>', 'run a diagnose')
	.option('-s, --send', 'Puts the shell into send mode')
	.option('-d, --data <name>', 'specify data root')
	.option('-j, --jhelp', 'output options as json');

commander.allowUnknownOption(true);
commander.parse(process.argv);
if(global.require) {
	global.require('./_build/index');
}
});
