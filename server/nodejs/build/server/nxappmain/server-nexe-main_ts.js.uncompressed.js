define("nxappmain/server-nexe-main_ts", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var commander = require('commander');
var _path = require('path');
var os= require('os');
var lodash= require('lodash');
var cwd = _path.resolve('./');
global.moduleCache={ };
var _re = global.require ? global.require : require;
global['_'] = _re('lodash');
global['oriRequire'] = require;
global['logError']=function(e,msg){
    console.error(msg,e);
    var stack = new Error().stack;
    console.log( stack );
};
global['nRequire'] = global.require ? global.require : require;
global['cwd'] = _path.resolve('./');
commander
	.version('0.0.1')
	.option('-c, --command <name>', 'command to send')
	.option('-i, --info', 'return service profile')
	.option('-f, --file <path>', 'run a file')
	.option('--diagnose <boolean>', 'run a diagnose')
	.option('-s, --send', 'Puts the shell into send mode')
	.option('-d, --data <name>', 'specify data root')
	.option('-j, --jhelp', 'output options as json');

commander.allowUnknownOption(true);
commander.parse(process.argv);
if(global.require) {
	global.require('./_build/index');
}
});
