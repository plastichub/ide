// wrapped by build app
define("nxappmain/dojoBuildConfig", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var clientRoot = "../../Code/client/src/";
var libRoot = clientRoot + "/lib/";
var dojoConfig = {
    hasCache: {
        "host-node": 1,
        "host-browser":0,
        "dom":0,
        "dojo-amd-factory-scan":0,
        "dojo-has-api":1,
        "dojo-inject-api":0,
        "dojo-timeout-api":0,
        "dojo-trace-api":0,
        "dojo-log-api":0,
        "dojo-dom-ready-api":0,
        "dojo-publish-privates":1,
        "dojo-config-api":1,
        "dojo-sniff":1,
        "dojo-sync-loader":0,
        "dojo-test-sniff":0,
        "config-deferredInstrumentation":0,
        "config-useDeferredInstrumentation":"report-unhandled-rejections",
        "config-tlmSiblingOfDojo":1,
        'xblox':true,
        'xlog':true,
        'drivers':true,
        'xreload':true,
        'dojo-undef-api': true,
        "debug":false

    },
    trace: 0,
    async: 0,
    packages: [
        {
            name: "nxapp",
            location: "nxapp"
        },
		{
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "xide",
            location: libRoot + 'xide'
        },
        {
            name: "xlog",
            location: libRoot + 'xlog'
        },
        {
            name: "xlang",
            location: libRoot + 'xlang'
        },
        {
            name: "xcf",
            location: libRoot + 'xcf'
        },
        {
            name: "xblox",
            location: libRoot + 'xblox'
        },
        {
            name: "xgrid",
            location: libRoot + 'xgrid'
        },
        {
            name: "xwire",
            location: libRoot + 'xwire'
        },
        {
            name: "dcl",
            location: libRoot + 'dcl'
        },
        {
            name: "xaction",
            location: libRoot + 'xaction/src'
        },
        {
            name: "dstore",
            location: libRoot + 'dstore'
        },
        {
            name: "xdojo",
            location: "./compat/xdojo"
        }
    ]
};

});
