define("nxappmain/socket_server", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var initModule = "nxappmain/socket_main";


global['dirname']=__dirname+'/';

if(process.platform=='cygwin'){
    module.paths[0]=__dirname +'/../node_modules/win32/';
}

// Dojo Configuration
dojoConfig = {
	hasCache: {
		"host-node": 1,
		"dom": 0,
		"dojo-built": 1
	},
	trace: 1,
	async: 1,
	baseUrl: ".",
	packages: [
    {
		name: "dojo",
		location: "dojo"
	},{
		name: "dijit",
		location: "dijit"
	},{
		name: "dojox",
		location: "dojox"
	},
    {
		name: "nxappmain",
		location: "nxappmain"
	},
    {
        name: "nxapp",
        location: "nxapp"
    },
    {
        name: "xapp",
        location: "xapp"
    }
    ],
	deps: [initModule]
};

// Load dojo/dojo
require("../dojo/dojo.js");

});
