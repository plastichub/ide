require([
    "dojo/_base/lang",
    "dojo/node!util",
    "dojo/node!net",
    "dojo/node!readline",
    "nxapp/utils",
    "nxapp/utils/FileUtils",
    "nxapp/utils/ProvadisConverter",
    "nxappmain/nxAppBase",
    "nxapp/protocols/Tcp"
], function(lang,util,net,readline,xmlutils,fileUtils,ProvadisConverter,base,Tcp)
{
    var app = new nxappmain.nxAppBase();

    app.init();



    if(app.commander.convert){

        console.error('converting course at ' + app.profile.coursePath);

        return;
    }

    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    var client=null;

    rl.on('line', function (cmd) {

        if(client){
            cmd = "" + cmd+"\r";
            cmd= cmd.toUpperCase();
            console.log('sending: '+cmd);
            client.write(cmd);
        }
    });


    if(app.commander.send) {
/*
        var options = {
            host:'192.168.1.20'
        };

        var myOverride = {

        };

        options = lang.mixin(options,myOverride);

        if(lang.isString(options)){
            console.error('opt obj');
        }

        var tcpProtocol=new Tcp({
            options:options
        });




        //console.dir(tcpProtocol);
        return;
*/

        /*tcpProtocol.connect();*/


        var HOST = '192.168.1.20';
        var PORT = 23;

        client = new net.Socket({
            allowHalfOpen: true
        });

        //client.setTimeout(2500);
        client.setEncoding('utf8');

        client.handle = function (data) {
            console.error('handle! : ' + data);
        };
        client.connect(PORT, HOST, function() {
            console.log('CONNECTED TO: ' + HOST + ':' + PORT);
            // Write a message to the client as soon as the client is connected, the server will receive it as message from the client
            //client.write('PWS?');
            //client.destroy();
        });
        client.on('data', function(data) {
            console.log('DATA: ' + data);
            // Close the client client completely
            //client.destroy();
        });


        client.on('error', function(exception){
            console.log('Exception:');
            console.log(exception);
        });


        client.on('drain', function() {
            console.log("drain!");
        });

        client.on('timeout', function() {
            console.log("timeout!");
        });

// Add a 'close' event handler for the client client
        client.on('close', function() {
            console.log('Connection closed');
        });

        console.error('Send command');
        return;
    }

});