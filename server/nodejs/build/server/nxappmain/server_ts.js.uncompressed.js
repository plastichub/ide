define("nxappmain/server_ts", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
function create(root) {
	var commander = require('commander'),
		path = require('path'),
		lodash = require('lodash');

	var clientRoot = path.join(root,'Code/client/src/'),
		nodeRoot = path.join(root,'server/nodejs'),
		_dojoConfig = require(path.join(nodeRoot,'nxappmain/dojoConfig_ts')),
		defaultProfile = 'nxappmain/profile_xcom.json',
		initModule = path.join(nodeRoot,"nxappmain/main_server");

	global.console = console;
	global['_'] = lodash;
	global['nRequire'] = require;
	dojoConfig = _dojoConfig.createDojoConfig(path.resolve('./logs'), initModule, commander, clientRoot,false,nodeRoot);
	global.dojoConfig = dojoConfig;
	dojoConfig.profilePath = defaultProfile;
	dojoConfig.nRequire = require;
	try {
		require(path.join(nodeRoot,"/dojo/dojo.js"));
		//dojo.require('nxappmain/main_server');


	} catch (e) {
		console.error('error loading server : ' + e.message, e.stack);
	}
}

module.exports = create;
});
