define("nxappmain/server_min", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var initModule = "nxappmain/main_min";

dojoConfig = {
	hasCache: {
		"host-node": 1,
        "host-browser":0,
        "dojo-sync-loader":1
	},
	trace: 1,
	async: 0,
	baseUrl: ".",
    packages: [
        {
            name: "dojo",
            location: "dojo"
        },
        {
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "nxapp",
            location: "nxapp"
        },
        {
            name: "xapp",
            location: "xapp"
        }
    ],
	deps: [initModule]
};

// Load dojo/dojo
require("../dojo/dojo.js");
debugger;
});
