define("nxappmain/xas", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var initModule = "nxappmain/main_xide_xas";
var libRoot = '../../Code/client/src/lib/';
var path = require('path');
dojoConfig = {
	cwd:__dirname,
    root:path.resolve('/PMaster/xasweb/docroot/'),
    hasCache: {
		"host-node": 1,
        "host-browser":0,
        "dom":0,
        "dojo-amd-factory-scan":1,
        "dojo-has-api":0,
        "dojo-inject-api":0,
        "dojo-timeout-api":0,
        "dojo-trace-api":0,
        "dojo-log-api":0,
        "dojo-dom-ready-api":0,
        "dojo-publish-privates":0,
        "dojo-config-api":0,
        "dojo-sniff":1,
        "dojo-sync-loader":0,
        "dojo-test-sniff":0,
        "config-deferredInstrumentation":0,
        "config-useDeferredInstrumentation":"report-unhandled-rejections",
        "config-tlmSiblingOfDojo":1

	},
	trace: 1,
	async: 0,
	baseUrl: ".",
    packages: [
        {
            name: "dojo",
            location: "dojo"
        },{
            name: "dijit",
            location: "dijit"
        },{
            name: "dojox",
            location: "dojox"
        },
        {
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "nxapp",
            location: "nxapp"
        },
        {
            name: "xapp",
            location: "xapp"
        },
        {
            name: "xcf",
            location: libRoot +'xcf'
        },
        {
            name: "xide",
            location: libRoot + 'xide'
        },
        {
            name: "dcl",
            location: libRoot +'dcl'
        },
        {
            name: "xblox",
            location: libRoot + 'xblox'
        },
        {
            name: "dijit",
            location: libRoot + 'dijit'
        }
    ],
	deps: [initModule]
};

// Load dojo/dojo
require("../dojo/dojo.js");
});
