define("nxappmain/server", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var commander = require('commander'),
    path = require('path'),
    lodash= require('lodash'),
    clientRoot =path.resolve('../../Code/client/src/'),
    _dojoConfig = require('./dojoConfig'),
    defaultProfile = 'nxappmain/profile_xcom.json',
    initModule = "nxappmain/main_server";

global.console = console;
global['_'] = lodash;
global['nRequire'] = require;
dojoConfig = _dojoConfig.createDojoConfig(path.resolve('./logs'),initModule,null,clientRoot);
global.dojoConfig = dojoConfig;
dojoConfig.profilePath = defaultProfile;
dojoConfig.nRequire = require;
try {
	//console.log('dojo confifg',dojoConfig);
    require("dojo/dojo");
}catch(e){
    console.error('error loading server : '+e.message,e.stack);
}
});
