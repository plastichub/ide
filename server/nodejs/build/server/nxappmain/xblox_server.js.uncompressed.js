define("nxappmain/xblox_server", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
global['dirname']=__dirname+'/';

if(process.platform=='cygwin'){
    module.paths[0]=__dirname +'/../node_modules/win32/';
}

var path = require('path');
var initModule = "nxappmain/main_xblox";

var libRoot = path.resolve('../client/src/lib/')+"/";
var dojoRunner = require('./dojoRunner');
var lodash = require('lodash');
global['_'] = lodash['_'];

dojoConfig = dojoRunner.createDojoConfig(libRoot,path.resolve('./logs'),initModule);

try{

    require("../dojo/dojo.js");

}catch(e){
    console.error('crash in main : ' + e);
	debugger;
}
});
