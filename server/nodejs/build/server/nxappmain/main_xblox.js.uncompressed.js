define("nxappmain/main_xblox", [
    "dojo/node!readline",
    "nxapp/Commons",
    "xide/types",
    "nxapp/utils/TCPUtils",
    "nxapp/utils/FileUtils",
    "nxappmain/nxAppBlox",
    'nxapp/manager/ContextBlox',
    'nxapp/client/WebSocket',
    'xlog/Server',
    "dojo/node!winston",
    "dojo/node!fs",
    "dojo/node!path"
], function(readline,Commons,types,TCPUtils,FileUtils,nxAppBlox,ContextBlox,WebSocket,LogServer,winston,fs,path)
{


    //process.exit();


    var fileLogOptions = {
        filename: 'all.log',
        dirname: dojoConfig.logRoot,
        json: true
    };

    var logglyOptions =  {
        "subdomain": "your-subdomain",
        "inputToken": "13f608cc-4cb7-4734-8e74-a6201211373a",
        "auth": {
            "username": "mc007",
            "password": ""
        }
    };

    var logger = new LogServer({});
    logger.start({
        fileLogger:fileLogOptions,
        console:null
    });

    var app = new nxAppBlox({
        profilePath:'/nxappmain/profile_xblox.json'
    });

    app.init();

    var profile = app.profile;

    var ctx = new ContextBlox();

    ctx.constructManagers();
    ctx.initManagers(profile);

    var createSocketServer=profile.common.createSocketServer;
    var hasSocketServer=false;
    var socketServer = null;


    //return our own run options
    if (app.commander.info) {
        var options={
            host: 'http://'+app.profile.socket_server.host,
            port: app.profile.socket_server.port
        };
        console.log(JSON.stringify(options));
        process.exit();
        return;
    }

    /***
     * Main
     */
    var main = function(){

        console.log('start main #1');
        // Getting host, port and protocol from command line
        if (app.commander.host) {
            HOST =  app.commander.host;
        }
        if (app.commander.port) {
            PORT =  parseInt(app.commander.port);
        }
        if (app.commander.protocol) {
            PROTOCOL =  app.commander.protocol;
        }
        /**
         * Command line switchs
         */

        //run a driver
        if (app.commander.run) {

            var file = app.commander.run;
            var cwd = dojoConfig.cwd;

            if(!fs.existsSync(file)){
                var _try = path.resolve(cwd+'/' + file);
                if(fs.existsSync(_try)){
                    file = _try;
                }
            }else{
                file = path.resolve(file);
            }

            var found = fs.existsSync(file);
            if(!found){
                console.error('cant find file ' + file);
                return;
            }
            console.time("run xblox");
            var bloxContent = FileUtils.readFile(file);
            var bloxScope = null,
                blockId = app.commander.id || '1b89f7b2-8367-cc0c-8fb6-b37ee5ef5d63',
                block = null;
            try {
                if (bloxContent) {
                    bloxScope = ctx.getBlockManager().toScope(bloxContent);
                } else {
                    console.error('have no content in ' + file);
                }
            }catch(e){
                console.error('crash ' + e);
            }


            if(!bloxScope){
                console.error('got no blox scope for file ' + file);
            }

            if(blockId){
                block = bloxScope.getBlockById(blockId);
            }

            if(block){
                console.log('run block ' + blockId);
                console.time("xblox run-time");
                block.solve(block.scope);
                console.timeEnd("xblox run-time");//1ms for a single !

            }else{
                console.log('run blox file ' + file);
            }
            console.timeEnd("run xblox");//5ms

            console.time("xblox run-time-test");
            var a = 0;
            while(a<3){
                a++;
            }

            console.timeEnd("xblox run-time-test");

            return;
        }

        //run a driver
        if (app.commander.driver) {
            console.error('run in driver mode');
            var socket = createClient();
            var data = {
                driver:'xcf/driver/SubTest',
                host:'192.168.1.20',
                port:23,
                protocol: 'tcp'
            };
            var signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;
            data.manager_command = types.SOCKET_SERVER_COMMANDS.START_DRIVER;
            signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;
            socket.emit(signal,data);
            return;
        }

        /***
         * Send command to device
         */
        if (app.commander.send) {

           console.error('sending command');
           console.error('  create web socket client to control server : ' + 'http://'+app.profile.socket_server.host +':'+app.profile.socket_server.port);

            var socket = createClient();
            var sendSingle = function(cmd){


                if(socket!== null && cmd!=null){
                    var data = {
                        command:cmd,
                        host:HOST,
                        port:PORT,
                        protocol: PROTOCOL
                    };
                    var signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_DEVICE;

                    if (cmd == "startDriver") {
                        data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_START_DRIVER;
                        signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

                    }else if(cmd == "status") {
                        data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_STATUS;
                        signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

                    }else if (cmd == "closeall") {
                        data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_CLOSE_ALL;
                        signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

                    } else {
                        data.device_command = types.SOCKET_SERVER_COMMANDS.DEVICE_SEND;

                    }
                    socket.emit(signal,data);
                }
            };

            /***
             * Device send function
             * @param cmd
             */
            var send = function(cmd){

                if(socket!== null && cmd!=null){
                    /***
                     * Support a row of commands
                     */
                    if( cmd.indexOf && cmd.indexOf(';')!=-1){

                        var commands = cmd.split(';');
                        for(var  i =  0  ; i < commands.length ; i++){
                            sendSingle(commands[i]);
                        }
                        return;
                    }else{
                        sendSingle(cmd);
                    }
                }
            };



            var rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });


            rl.on('line', function (cmd) {

                if(cmd !=null && cmd.length>0){
                    console.log("sending stdin : " +cmd);
                    sendSingle(cmd);
                }
            });
            if (typeof app.commander.command == 'string') {
                send(app.commander.command);
            }

            console.error('Send command. ^C to close connection and quit');
        }

    };


    /***
     * Fulfil dependencies, check we have a socket server
     */
    if(createSocketServer && !hasSocketServer){


        var _checkServerCB = function(error,status,host,port){

            var ok=false;

            //we dont' have a server yet
            if(status=='closed'){
                console.error('have no blox server yet, creating a new one!');
                socketServer=TCPUtils.createDeviceServer(profile,ctx);
                ctx.deviceServer = socketServer;
                ctx.logger=winston;
                ctx.deviceServer.logger=winston;
                console.log('start blox server',socketServer.test);
                ok=true;
            }else if(status=='open'){
                ok=true;
            }

            if(ok){
                main();
            }
        };

        TCPUtils.checkPort(profile.socket_server.port,profile.socket_server.host,_checkServerCB);

    }else{
        try{
            main();
        }catch(e){
            console.error('app crash!');

        }
    }
});