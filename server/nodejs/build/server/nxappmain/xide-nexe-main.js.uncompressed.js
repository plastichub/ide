define("nxappmain/xide-nexe-main", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var commander = require('commander');
var path = require('path');
var lodash= require('lodash');
var walk= require('walk');
var os= require('os');
var sockjs= require('sockjs');
var sockjs_client= require('sockjs-client');
var colors= require('colors');
var watchr= require('watchr');




var libRoot = path.resolve('.') +'/',
    clientRoot =path.resolve('../../Code/client/src/'),
    dojoRunner = require('./dojoRunner'),
    defaultProfile = 'nxappmain/profile_xide_server.json',
    initModule = "nxappmain/xide-nexe-run",
    notif = null;

global['_'] = lodash;
var _re =require;
global['nRequire'] = _re;
global['cwd'] = path.resolve('./');

commander.version('0.1.1')
    .option('-p, --profilePath <name>', 'path to profile',path.resolve(defaultProfile))
    .option('-s, --sourceRoot <name>', 'path to dojo source root',path.resolve(libRoot))
    .option('-c, --clientRoot <name>', 'path to client root',path.resolve(clientRoot))
    .option('-i, --info', 'return service profile')
    .option('-r, --root <name>', 'path to lib root',path.resolve(libRoot))
    .option('-j, --jhelp', 'output options as json');

process.argv = process.argv.slice(1);
commander.parse(process.argv);

libRoot = path.resolve(commander.root) + path.sep;
//console.log('root : ' + libRoot);

dojoConfig = dojoRunner.createDojoConfig(libRoot,path.resolve('./logs'),initModule,commander,clientRoot);
dojoConfig.isBuild = true;
// Load dojo/dojo
var _dUrl = "./dojo/dojo.js";
var _re = require;
_re(_dUrl);});
