require([
    "dojo/node!readline",
    "nxapp/Commons",
    "xide/types",
    "nxapp/utils/TCPUtils",
    "nxappmain/nxAppBase",
    'nxapp/manager/Context',
    'nxapp/client/WebSocket',
    'xlog/Server',
    "dojo/node!winston",
    "dojo/node!tcp-port-used"
], function(readline,Commons,types,TCPUtils,nxAppBase,Context,WebSocket,LogServer,winston,tcpPortUsed){
    var fileLogOptions = {
        filename: 'all.log',
        dirname: dojoConfig.logRoot,
        json: true
    };

    var logglyOptions =  {
        "subdomain": "your-subdomain",
        "inputToken": "13f608cc-4cb7-4734-8e74-a6201211373a",
        "auth": {
            "username": "mc007",
            "password": "213,,asd"
        }
    };

    var logger = new LogServer({});
    logger.start({
        fileLogger:fileLogOptions,
        /*loggly:logglyOptions,*/
        console:null
    });

    var app = new nxAppBase({
        commander:null
    });



    app.init();
    var profile = app.profile;

    var ctx = new Context();
    ctx.constructManagers();
    ctx.initManagers(profile);
    ctx.logManager = logger;


    var createSocketServer=profile.common.createSocketServer;
    var hasSocketServer=false;
    var socketServer = null;

    /**
     * Factories
     * @returns {nxapp.client.WebSocket}
     */
    var createClient = function(){
        var socket = new WebSocket();
        socket.init({
            options: {
                host: 'http://'+app.profile.socket_server.host,
                port: app.profile.socket_server.port,
                debug: app.profile.debug

            }
        });
        socket.connect();

        socket.onSignal(types.SIGNAL_RESPONSE,function(data) {

            if(profile.common.stdout){
                process.stdout.write(data + '\n');
            }

            if (app.commander.oneshot  && app.commander.command){
                process.exit();
            }
        });
        return socket;
    };

    //return our own run options
    if (app.commander.info) {
        var options={
            host: 'http://'+app.profile.socket_server.host,
            port: app.profile.socket_server.port
        };
        console.log(JSON.stringify(options));
        process.exit();
        return;
    }

    /***
     * Main
     */
    var main = function(){

        // Getting host, port and protocol from command line
        if (app.commander.host) {
            //HOST =  app.commander.host;
        }
        if (app.commander.port) {
            //PORT =  parseInt(app.commander.port);
        }
        if (app.commander.protocol) {
            //PROTOCOL =  app.commander.protocol;
        }
        /**
         * Command line switchs
         */
        //run a driver
        if (app.commander.driver) {

            console.error('run in driver mode');
            var socket = createClient();

            var data = {
                driver:'xcf/driver/SubTest',
                host:'192.168.1.20',
                port:23,
                protocol: 'tcp'
            };
            var signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;
            data.manager_command = types.SOCKET_SERVER_COMMANDS.START_DRIVER;
            signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;
            socket.emit(signal,data);
            return;
        }

        /***
         * Send command to device
         */
        if (app.commander.send) {

           console.error('sending command');
           console.error('  create web socket client to control server : ' + 'http://'+app.profile.socket_server.host +':'+app.profile.socket_server.port);

            var socket = createClient();
            var sendSingle = function(cmd){


                if(socket!== null && cmd!=null){
                    var data = {
                        command:cmd,
                        host:HOST,
                        port:PORT,
                        protocol: PROTOCOL
                    };
                    var signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_DEVICE;

                    if (cmd == "startDriver") {
                        data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_START_DRIVER;
                        signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

                    }else if(cmd == "status") {
                        data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_STATUS;
                        signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

                    }else if (cmd == "closeall") {
                        data.manager_command = types.SOCKET_SERVER_COMMANDS.MANAGER_CLOSE_ALL;
                        signal = types.SOCKET_SERVER_COMMANDS.SIGNAL_MANAGER;

                    } else {
                        data.device_command = types.SOCKET_SERVER_COMMANDS.DEVICE_SEND;

                    }
                    socket.emit(signal,data);
                }
            };

            /***
             * Device send function
             * @param cmd
             */
            var send = function(cmd){

                if(socket!== null && cmd!=null){
                    /***
                     * Support a row of commands
                     */
                    if( cmd.indexOf && cmd.indexOf(';')!=-1){

                        var commands = cmd.split(';');
                        for(var  i =  0  ; i < commands.length ; i++){
                            sendSingle(commands[i]);
                        }
                        return;
                    }else{
                        sendSingle(cmd);
                    }
                }
            };



            var rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });


            rl.on('line', function (cmd) {

                if(cmd !=null && cmd.length>0){
                    console.log("sending stdin : " +cmd);
                    sendSingle(cmd);
                }
            });
            if (typeof app.commander.command == 'string') {
                send(app.commander.command);
            }

            console.error('Send command. ^C to close connection and quit');
        }
    };


    /***
     * Fulfil dependencies, check we have a socket server
     */
    if(createSocketServer){

        var _checkServerCB = function(error,status,host,port){

            var ok=false;
            if(status=='closed'){
                console.error('have no device server yet, creating a new one!');

            }else if(status=='open'){
                ok=true;
            }

            if(ok){
                main();
            }
        };



        tcpPortUsed.check(profile.socket_server.port, profile.socket_server.host).then(function(inUse) {
            console.log('Started : '+inUse);
            if(!inUse){
                socketServer=TCPUtils.createDeviceServer(profile,ctx);
                ctx.deviceServer = socketServer;
                ctx.logger=winston;
                ctx.deviceServer.logger=winston;
                ctx.logManager = logger;
                main();
            }else{
                console.log('already in use!');
            }
        }, function(err) {

        });
        //TCPUtils.checkPort(profile.socket_server.port,profile.socket_server.host,_checkServerCB);
    }else{

        try{
            main();
        }catch(e){
            console.error('app crash!');
            /*console.dir(e);*/
        }
    }
});