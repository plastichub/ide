define("nxappmain/xwatch", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
/***
 * This file sets up all paths and the Dojo config.
 */

/**
 * initModule is the relative path to the main - node.js application
 *
 * Remarks
 * - the init module loads a profile '/nxappmain/profile_xcom.json' which lists all directories to be watched but
 *   also specifies the port for the web socket server.
 *
 *
 * @type {string}
 */
var initModule = "nxappmain/main_xcom";

/**
 * libRoot is the absolute path to your main module folder. This should point to a directory containing ./dojo , ./dijit and so forth.
 * @type {string}
 */
var libRoot = '/PMaster/projects/xbox-app/client/src/lib/';

/**
 * Node.JS depenendencies
 * @type {exports}
 */
var path = require('path');
var commander = require('commander');


commander
    .version('0.0.1')
    .option('-p, --profile <name>', 'watch profile').
    option('-r, --root <name>', 'watch profile');

console.error('profile ' + commander.profile);
console.error('root ' + commander.root);

var runInfo = {
    root:commander.root,
    profile:commander.profile
};

console.log('run from ' + __dirname);


return;

/**
 * Create a Dojo config which works with Node.JS
 * Notice that we specify package locations explictly and relative to 'libRoot'
 */
dojoConfig = {
	cwd:__dirname,
    root:runInfo.root,
    hasCache: {
		"host-node": 1,
        "host-browser":0,
        "dom":0,
        "dojo-amd-factory-scan":1,
        "dojo-has-api":0,
        "dojo-inject-api":0,
        "dojo-timeout-api":0,
        "dojo-trace-api":0,
        "dojo-log-api":0,
        "dojo-dom-ready-api":0,
        "dojo-publish-privates":0,
        "dojo-config-api":0,
        "dojo-sniff":1,
        "dojo-sync-loader":0,
        "dojo-test-sniff":0,
        "config-deferredInstrumentation":0,
        "config-useDeferredInstrumentation":"report-unhandled-rejections",
        "config-tlmSiblingOfDojo":1

	},
	trace: 1,
	async: 0,
	baseUrl: ".",
    packages: [
        {
            name: "dojo",
            location: "dojo"
        },{
            name: "dijit",
            location: "dijit"
        },{
            name: "dojox",
            location: "dojox"
        },
        {
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "nxapp",
            location: "nxapp"
        },
        {
            name: "xapp",
            location: "xapp"
        },
        {
            name: "xcf",
            location: libRoot +'xcf'
        },
        {
            name: "xide",
            location: libRoot + 'xide'
        },
        {
            name: "xfile",
            location: libRoot + 'xfile'
        },
        {
            name: "dcl",
            location: libRoot +'dcl'
        },
        {
            name: "xblox",
            location: libRoot + 'xblox'
        },
        {
            name: "dijit",
            location: libRoot + 'dijit'
        }

    ],
	deps: [initModule]
};

// Load dojo/dojo
require("../dojo/dojo.js");
});
