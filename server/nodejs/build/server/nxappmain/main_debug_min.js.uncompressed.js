require([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "xcf/driver/DriverBase",
    "dojo/node!commander"
], function(declare,lang,DriverBase,commander)
{
    commander
        .version('0.0.1')
        .option('-d, --driver <driver>','path to the driver')
        .parse(process.argv);


    if (commander.driver) {

        var driverProtoIn = commander.driver; // 'xcf/data/driver/system/Marantz/MyMarantz';

        require([driverProtoIn],function(driverProtoInstance){

            var baseClass = DriverBase;

            var baseClasses = [baseClass];

            var driverProto = null;

            driverProto = declare([driverProtoInstance , baseClass].concat(baseClasses));

            var driverInstance = new driverProto({});
            driverInstance.start();


        });

    } else {
        console.error("Please provide the driver to debug");
    }

});