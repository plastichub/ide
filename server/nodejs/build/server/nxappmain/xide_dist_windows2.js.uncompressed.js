define("nxappmain/xide_dist_windows2", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var commander = require('commander'),
    path = require('path'),
    lodash= require('lodash'),
    _ = require('lodash'),
    libRoot = path.resolve('.')+"/",
    clientRoot =path.resolve('../../Code/client/src/'),
    dojoRunner = require('./dojoRunner'),
    defaultProfile = 'nxappmain/profile_xide_server.json',
    initModule = "nxappmain/main_xide";


global['_'] = lodash;
global['nRequire'] = require;

global['cwd'] = path.resolve('./');

//console.error('process.env.NODE_PATH',process.env.NODE_PATH);

var o ={

};

lodash.extend(o,{
    a:2
});
var _a = require.cache;

function _requireFromString(src, filename) {
    var Module = module.constructor;
    var m = new Module();
    m._compile(src, filename);
    return m.exports;
}
var Module = module.constructor;

//var requireFromString = require('require-from-string');
function requireFromString(code, filename, opts) {
    if (typeof filename === 'object') {
        opts = filename;
        filename = undefined;
    }

    opts = opts || {};

    opts.appendPaths = opts.appendPaths || [];
    opts.prependPaths = opts.prependPaths || [];

    if (typeof code !== 'string') {
        throw new Error('code must be a string, not ' + typeof code);
    }

    var paths = Module._nodeModulePaths(path.dirname(filename));

    var m = new Module(filename, module.parent);
    m.filename = filename;
    m.paths = [].concat(opts.prependPaths).concat(paths).concat(opts.appendPaths);
    m._compile(code, filename);

    m.loaded = true;
    m.parent = require.cache[path.join(__dirname, '/xide_dist_windows2.js')];
    m._cache = [];
    m._cache[filename]=filename;

    require.cache[filename]=lodash.extend({
        id:filename

    },m);


    return m.exports;
};


//var file = path.join(__dirname, '/test_module.js');
var file = path.join(__dirname, '/node.js.uncompressed.js');
var fs = require('fs');

var code = fs.readFileSync(file, 'utf8');
var result = requireFromString(code, path.join(__dirname, '/bla.js'), {
    appendPaths: [],
    prependPaths: []
});

//var _d = require(result);
//console.dir(result._nodeModulePaths);

//var _m = require('./bla');

//var _m = require(path.join(__dirname, '/bla'));

//var cache = _m.mod;
var mods = [];
/*
_.each(cache,function(fn,name){

    console.log('register module : ' + name);
    var module= requireFromString(code,path.join(__dirname, '/'+name + '.js'), {
        appendPaths: [],
        prependPaths: [name,name + '.js']
    });
    mods.push(module);
});
*/

//var utils = require('xide/utils');



//console.log(requireFromString('module.exports = { test: 1}'),'xop');

//var _p = require('xop');

//console.dir(_m);

//console.dir(require.cache);
var dojoConfig = {
    hasCache: {
        "host-node": 1,
        "host-browser": 0,
        "dom": 0,
        "dojo-amd-factory-scan": 1,
        "dojo-has-api": 0,
        "dojo-inject-api": 0,
        "dojo-timeout-api": 0,
        "dojo-trace-api": 0,
        "dojo-log-api": 0,
        "dojo-dom-ready-api": 0,
        "dojo-publish-privates": 0,
        "dojo-config-api": 0,
        "dojo-sniff": 1,
        "dojo-sync-loader": 0,
        "dojo-test-sniff": 0,
        "config-deferredInstrumentation": 0,
        "config-useDeferredInstrumentation": "report-unhandled-rejections",
        "config-tlmSiblingOfDojo": 1,
        packages: [
            {
                name: "dojo",
                location:"dojo"
            }
        ]
    },
    trace: 1,
    async: 0,
    baseUrl: "."
};

require('../dojo/dojo.js');





});
