define("nxappmain/server_debug_min", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var commander = require('commander');
/*
var dlc = require("dcl");
var DriverBase = require("xcf/driver/DriverBase");
var util = require('util');*/
//debugger;
//var a = new DriverBase();
//console.error(util.inspect(a,true,4));
//a.test();


//var b = new driverBase();


commander
    .version('0.0.1')
    .option('-d, --driver <driver>','path to the driver')
    .parse(process.argv);

/*
var watchr = require('watchr');

// Watch a directory or file
//console.log('Watch our paths');
var exec = require('child_process').exec;
exec('ps -aux | grep node', function(err, stdout, stderr) {
    // stdout is a string containing the output of the command.
    // parse it and look for the apache and mysql processes.
    //console.log(util.inspect(JSON.stringify(stdout)));
});
if (commander.driver) {

    //console.error('debug driver : ' +commander.driver);
    var driver = require(commander.driver);
    var dinstance = new driver;
    console.error(util.inspect(dinstance,true,4));
    console.log(dinstance.myProp);
    dinstance.start();
}
*/

console.error('hello');
var initModule = "nxappmain/main_debug_min";
dojoConfig = {
	hasCache: {
		"host-node": 1,
        "host-browser":0,
        "dojo-sync-loader":1
	},
	trace: 1,
	async: 0,
	baseUrl: ".",
    packages: [
        {
            name: "dojo",
            location: "dojo"
        },
        {
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "nxapp",
            location: "nxapp"
        },
        {
            name: "xapp",
            location: "xapp"
        },
        {
            name: "xcf",
            location: "xcf"
        }
    ],
	deps: [initModule]
	
};
// Load dojo/dojo
require("../dojo/dojo.js");

});
