define("nxappmain/xide", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
var commander = require('commander'),
    path = require('path'),
    lodash= require('lodash'),
    libRoot = path.resolve('../client/src/lib/')+"/",
    clientRoot =path.resolve('../../Code/client/src/'),
    dojoRunner = require('./dojoRunner'),
    defaultProfile = 'nxappmain/profile_xide_server.json',
    initModule = "nxappmain/main_xide";

global['_'] = lodash;
global['nRequire'] = require;
global['cwd'] = path.resolve('./');
process.env.NODE_PATH = __dirname;
commander
    .version('0.1.1')
    .option('-p, --profilePath <name>', 'path to profile',path.resolve(defaultProfile))
    .option('-s, --sourceRoot <name>', 'path to dojo source root',path.resolve(libRoot))
    .option('-c, --clientRoot <name>', 'path to client root',path.resolve(clientRoot))
    .option('-i, --info', 'return service profile')
    .option('-j, --jhelp', 'output options as json');

commander.parse(process.argv);

dojoConfig = dojoRunner.createDojoConfig(path.resolve('./logs'),initModule,commander,clientRoot);

// Load dojo/dojo
require("../dojo/dojo.js");
});
