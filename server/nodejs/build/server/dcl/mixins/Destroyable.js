//>>built
(function(a){"undefined"!=typeof define?define("dcl/mixins/Destroyable",["../dcl"],a):"undefined"!=typeof module?module.exports=a(require("../dcl")):dclMixinsDestroyable=a(dcl)})(function(a){var b=a(null,{declaredClass:"dcl/mixins/Destroyable"});a.chainBefore(b,"destroy");return b});
//# sourceMappingURL=Destroyable.js.map