//>>built
(function(a){"undefined"!=typeof define?define("dcl/advices/time",[],a):"undefined"!=typeof module?module.exports=a():dclAdvicesTime=a()})(function(){var a=0;return function(d){var b=0,c=d||"Timer #"+a++;return{before:function(){b++||console.time(c)},after:function(){--b||console.timeEnd(c)}}}});
//# sourceMappingURL=time.js.map