//>>built
define("dojo/request/default",["exports","require","../has"],function(a,e,c){var d=c("config-requestProvider"),b;b=c("host-webworker")?"./xhr":"./node";d||(d=b);a.getPlatformDefaultId=function(){return b};a.load=function(a,c,f,g){e(["platform"==a?b:d],function(a){f(a)})}});
//# sourceMappingURL=default.js.map