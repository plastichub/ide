///////////////////////////CONTROL MCR RJ45 EQ AUTO////////////////////

#include <Thermistor.h>

int vC = 270;                 //Ajust voltatge Cel.les
int calibrat = (27.7 * 36.5); ///Ajust voltatje bateria, primer factor = Vfloat
long temps = (120 * 60000);   //Ajust del temps bulk i eq en milisegons
int sol = 9;                  // sortida control soldador
int vfd = 5;                  // sortida control VFD
float vCell;                  // voltatge cel.les
float vB;                     // voltatge bateries
float vBA = 50.00;            //// FACTOR DEL DIVISOR valor normal 52  +VALOR = MENYS VOLTATGE
float vBAT = 50.00;           //// FACTOR DEL DIVISOR CORRETGIT PER TEMPERATURA valor normal 52  +VALOR = MENYS VOLTATGE
int tempSensor;
int mes = 0;               // increment PWM Soldador
int mesvfd = 0;            // increment PWM VFD
int pp = 0;                // valor del PWM del soldador
float ppu;                 // valor de potenciometre de potrencia de placa instalada o generador
unsigned long tEq;         // temps equalitzacio
unsigned long tAbs;        // temps absocio
Thermistor temp_Sensor(0); // NTC al A0

void setup()
{

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);

  //Serial.begin(9600);
}

void loop()
{

  senseVoltageB();
  controlVoltageB();
  senseVotajeCell();
  SenseTemp();

  pp = ppu / 4; // limit maxim de potenciometre dividit per 4 arriba al 100% PWM
                // 20 PER A 1,5kw, 10 PER A 3kw, 8 PER A 5KW

  ppu = (analogRead(A7)); //kw de placa instal.lada lectura de potenciometre

  if (ppu < 100)
    ppu = 100; // limit mínim de placa instal.lada mínim del potenciometre

  // control del soldador
  if (vB < 29.0 && vCell > 275)
  {
    analogWrite(sol, mes);
    mes = mes + 1; //increment del valor de sortida
    if (mes > pp)
      mes = pp; // per no passar de voltes la sortida PWM
  }

  if (vB > 29.0 || vCell < 275)
  {
    analogWrite(sol, mes);
    mes = mes - 1; //descens del valor de sortida
    if (mes <= 0)
      mes = 0; // per no passar de voltes la sortida PWM
  }

  // control del VFD
  if (vB > 28.8 && vCell > 275)
  {
    analogWrite(vfd, mesvfd);
    mesvfd = mesvfd + 1; //increment del valor de sortida
    if (mesvfd > 255)
      mesvfd = 255; //per no passar de voltes la sortida PWM
    Serial.println(mesvfd);
  }

  if (vB < 28.8 || vCell < 275)
  {
    analogWrite(vfd, mesvfd);
    mesvfd = mesvfd - 1; //descens del valor de sortida
    if (mesvfd <= 0)
      mesvfd = 0; // per no passar de voltes la sortida PWM
  }
}

void senseVoltageB()
{
  vB = (analogRead(A5) * vBAT / calibrat); //calibrat al divisor 6k8+1k+68k
}
void senseVotajeCell()
{
  vCell = (analogRead(A4) * 275.00 / vC); // calibrat al divisor 220+1k5
}

void controlVoltageB()
{
  unsigned long tFlot;
  tFlot = millis();

  if (vB > 28.8)
  { // flotacio
    if ((tFlot - tAbs) > temps || (tFlot - tEq) > temps)
    {
      vBA = 53.30;           //
      digitalWrite(10, LOW); //LED absorcio
      digitalWrite(11, LOW); //LED equalitzacio
                             //Serial.println (tFlot/100);
    }
  }

  if (vB < 26.5 && vB > 23 && vBA >= 50)
  { // absorcio
    vBA = 50.00;
    tAbs = millis();
    digitalWrite(10, HIGH); //LED absorcio
  }

  if (vB < 22.8)
  { //equalitzacio
    vBA = 47.50;
    tEq = millis();
    digitalWrite(11, HIGH); //LED equalitzacio
  }
}
void SenseTemp()
{
  tempSensor = temp_Sensor.getTemp();
  if (tempSensor >= -10){
    vBAT = vBA * (1 + (tempSensor * 0.002)); // 5mV per ºC, 40ºC -2V.
  }
  if (tempSensor < -10){
    vBAT = vBA * 1.04; // per sino es conecta la NTC, -1V. valor de 20ºC
  }
  //Serial.println (tAbs/100);
  //Serial.println (tempSensor);
  //Serial.println (vBAT);
  //Serial.println (vC);
}
