// wrapped by build app
define("nxapp/xide/do/client/test", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
function start () {


    // Init with options
    AtomizeClient.init({
        host:'0.0.0.0',
        port:9999
    });

    // Register a watcher for the first input: update the field if the value changes
    AtomizeClient.registerWatcher("DO.test.y",function(result) {
        document.getElementById('y').value = result;
        console.log("Changed DO.test.y "+result);
    });

    // Register a watcher for the first input: update the field if the value changes
    AtomizeClient.registerWatcher("DO.test.y",function(result) {
        document.getElementById('y').value = result;
        console.log("Changed DO.test.y "+result);
    });


    // Register a watcher for the first input: update the field if the value changes
    AtomizeClient.registerWatcher("Neq.Volume",function(result) {
        document.getElementById('Neq.Volume').value = result;
        console.log("Changed Neq.Volume "+result);
    });

    // Register a watcher for the first input: update the field if the value changes
    AtomizeClient.registerWatcher("Neq.PowerState",function(result) {
        document.getElementById('Neq.PowerState').value = result;
        console.log("Changed Neq.PowerState "+result);
    });

    // Connect
    AtomizeClient.connect(function () {

        // onConnect callback


        // Loads the value for the first input. If not set, sets all the test data
        AtomizeClient.get("DO.test.y",function(value) {
            if (value != undefined)
            {
                document.getElementById('y').value = value;
            } else {
                AtomizeClient.set("DO.test",{y:0,z:1});
            }

        });

        // Loads the value for the first input. If not set, sets all the test data
        AtomizeClient.get("Neq.Volume",function(value) {
            if (value != undefined)
            {
                document.getElementById('Neq.Volume').value = value;
            } else {
                AtomizeClient.set("Neq",{Volume:0});
            }

        });

        // Loads the value for the first input. If not set, sets all the test data
        AtomizeClient.get("Neq.PowerState",function(value) {
            if (value != undefined)
            {
                document.getElementById('Neq.PowerState').value = value;
            } else {
                AtomizeClient.set("Neq",{PowerState:0});
            }

        });

        // Loads the value for the second input
        AtomizeClient.get("DO.test.z",function(value) {
            document.getElementById('z').value = value;
        });


    });

}

// "Change" button action
function onChange(ns,value) {
    AtomizeClient.set(ns,value);
}




});
