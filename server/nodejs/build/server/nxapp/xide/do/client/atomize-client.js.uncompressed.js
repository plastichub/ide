// wrapped by build app
define("nxapp/xide/do/client/atomize-client", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){

var AtomizeClient = {

    atomize:null,
    DO:null,
    watchers:[],

    /***
     * Default options
     */
    _defaultOptions:function(){
        return {
            host:"0.0.0.0",
            port:9000,
            channel: "atomize"
        };
    },

    ///////////////////////////////////////////////////
    //
    // Atomize data manipulation public methods
    //
    ///////////////////////////////////////////////////

    /***
     * Performs a transaction with a "get" operation
     * @param atomize_ns
     * @param callback
     */
    get:function(atomize_ns,callback) {
        var thiz = this;
        this.makeTransaction(function() {
                return thiz._get("root."+atomize_ns);
            },
            callback);
    },

    /***
     * Performs a transaction with a "set" operation
     *
     * @param atomize_ns
     * @param value
     */
    set:function(atomize_ns,value) {
        var thiz = this;
        this.makeTransaction(function() {
                thiz._set("root." + atomize_ns,value);
            },
            function() {});
    },

    /***
     * Deletes a value from an atomize distributed object
     * @param atomize_ns
     */
    unset:function(atomize_ns) {
        var path = atomize_ns.split(".");
        var obj = this.atomize.access(this.atomize,path[0]);
        var end =  path[path.length-1];
        path = path.slice(1,-1);

        for (var n in path) {
            obj = this.atomize.access(obj,path[n]);
        }

        this.atomize.erase(obj,end);
    },

    /***
     * Performs a "transaction"
     *
     * @param action
     * @param onResult
     */
    makeTransaction:function(action,onResult) {
        var _fn = this._get("atomically");

        if(_fn){
            _fn(action,onResult);
        }


    },

    ///////////////////////////////////////////////////
    //
    // Client Control API
    //
    ///////////////////////////////////////////////////

    /***
     * Initializes the client with options
     *
     * @param options
     */
    init:function(options) {
        this.options = this._defaultOptions();
        var myself = this;

        // Mix the default options and the options provided
        for(var property in this.options) {
            if (options[property]) {
                this.options[property] = options[property];
            }
        }
    },

    /***
     * Registers a watcher
     *
     * @param inspect       =>  value to watch
     * @param onChangeCB    =>  callback function
     */
    registerWatcher:function(inspect,onChangeCB) {
        this.watchers.push({
            inspect: inspect,
            lastValue: null,
            callback: onChangeCB
        });
    },

    /***
     * Connects with the atomize server
     *
     * @param callback
     */
    connect:function(callback) {
        this.atomize = new Atomize("http://"+this.options.host+":"+this.options.port+"/"+this.options.channel);
        var thiz = this;

        // Starts when the connection begins
        this.atomize.assign(this.atomize, "onAuthenticated",
            function() {

                // Calls the callback
                callback();

                // Starts the watchers
                thiz.makeTransaction(
                    function() {
                        // empty "action" for the transaction
                    },
                    function(result) {
                        for(var n in thiz.watchers) {
                            thiz._beginWatch(thiz.watchers[n]);
                        }
                    }
                );
            }
        );

        // Connects to the Atomize Server
        this.atomize.access(this.atomize,"connect")();

    },

    ///////////////////////////////////////////////////
    //
    // Private methods
    //
    ///////////////////////////////////////////////////

    /***
     * Watches a value
     *
     * @param watcher
     * @private
     */
    _beginWatch:function(watcher) {

        var thiz = this;
        this.get(watcher.inspect,function(result) {
            watcher.lastValue = result;
        });

        this.makeTransaction(
            function() {
                var readed = thiz._get("root."+watcher.inspect);
                if (watcher.lastValue != readed) {
                    watcher.callback(readed);
                    watcher.lastValue = readed;
                }
                thiz.atomize.access(thiz.atomize,"retry")();
            },
            function(result) {
            }
        );
    },

    /***
     * Get a value from an atomize distributed object
     * @param atomize_ns
     * @param value
     */
    _get:function(atomize_ns) {

        var path = atomize_ns.split(".");

        var obj = this.atomize.access(this.atomize,path[0]);
        path = path.slice(1);

        for (var n in path) {
            if (obj == undefined) {
                return obj;
            }
            obj = this.atomize.access(obj,path[n]);
        }

        return obj;
    },

    /***
     * Set a value into an atomize distributed object
     * @param atomize_ns
     * @param value
     */
    _set:function(atomize_ns,value) {
        var path = atomize_ns.split(".");

        var obj = this.atomize.access(this.atomize,path[0]);
        var toassign =  path[path.length-1];
        path = path.slice(1,-1);

        for (var n in path) {
            obj = this.atomize.access(obj,path[n]);
        }

        if (typeof value == "object") {
            value = this.atomize.access(this.atomize, "lift")(value);
        }

        this.atomize.assign(obj,toassign,value);
    }

};



});
