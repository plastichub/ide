define("nxapp/manager/DriverManager", [
    "dcl/dcl",
    'nxapp/utils/_LogMixin',
    'xide/types',
    'xcf/manager/DriverManager',
    "nxapp/utils/_console",
    "dojo/node!path",
    "xide/mixins/EventedMixin",
    "xide/mixins/ReloadMixin",
    "dojo/Deferred",
    "xcf/model/Variable",
    "nxapp/utils"
],function(dcl,_LogMixin,types,DriverManager,_console,path,EventedMixin,ReloadMixin,Deferred,Variable,utils){

    var console = _console;
    return dcl([DriverManager,_LogMixin],{
        declaredClass:"nxapp.manager.DriverManager",
        /**
         * Creates a full driver instance from a device info
         * @param options
         * @returns {module:dojo/Deferred}
         */
        createDriverInstance:function(options){
            var context = this.ctx;
            var deviceManager = context.getDeviceManager();
            var driverManager = context.getDriverManager();

            var driverScope = options.driverScope;
            var deviceScope = options.deviceScope;
            var dfd = new Deferred();
            var baseDriverPrefix = options['system_drivers'];
            var baseDriverRequire = baseDriverPrefix + '/DriverBase.js';
            var driverRequirePath = options[driverScope] + '/' + options.driver;
            var driverId = options.driverId;
            var deviceId = options.deviceId||options.id;
            var driversPath = options[driverScope];
            var devicesPath = options[deviceScope];
            var hash = options.hash;
            var driver = driverManager.getDriverById(driverId);
            var device = deviceManager.getDeviceById(deviceId,deviceManager.stores[deviceScope]);

            var ignore = false;
            if(!driver || ignore) {
                if (!driversPath) {
                    console.error('invalid drivers path ' + driversPath, options);
                    dfd.reject('invalid drivers path ' + driversPath);
                    return dfd;
                }

                var drivers = context.getDrivers(path.resolve(driversPath), driverScope);
                if (drivers && drivers.length) {
                } else {
                    dfd.reject('have no devices at ' + driversPath);
                    return dfd;
                }
                var driverStore = this.createStore({
                    items: drivers
                }, driverScope, false);

                driver = driverManager.getDriverById(driverId,driverStore);
                if(!driver){
                    dfd.reject('cant find driver '+driverId);
                    return dfd;
                }

            }

            if(!device|| ignore) {
                var devices = context.getDevices(path.resolve(devicesPath), deviceScope);
                if (devices && devices.length){
                } else {
                    dfd.reject('have no devices at ' + devicesPath);
                    return dfd;
                }


                var deviceStore = deviceManager.initStore({
                    items: devices
                }, deviceScope, false);


                device = deviceStore.getSync(options.devicePath);
                if (!device) {
                    dfd.reject('cant find device ' + options.devicePath);
                    return dfd;
                }

            }

            device.info  = options;
            var thiz = this;
            require(['system_drivers/DriverBase'], function (driverBase) {
                driverBase.prototype.declaredClass = baseDriverRequire;
                require([driverScope+'/'+options.driver.replace('.js','')], function (driverProtoInstance) {

                    var baseClass = driverBase;
                    var driverProto = dcl([baseClass, EventedMixin.dcl, ReloadMixin.dcl,driverProtoInstance],{});
                    var driverInstance = new driverProto();
                    
                    driverInstance.declaredClass = driverRequirePath;
                    driverInstance.options = options;
                    driverInstance.baseClass = baseClass.prototype.declaredClass;
                    driverInstance.modulePath = driverRequirePath;
                    driverInstance.delegate = deviceManager;
                    driverInstance.driver = driver;
                    driverInstance.serverSide = options.serverSide;
                    driverInstance.utils = utils;
                    driverInstance.types = types;
                    driverInstance.device = device;
                    driverInstance.id = utils.createUUID();

                    driverInstance.getDevice = function(){
                        return this.device;
                    };
                    driverInstance.getDeviceInfo = function(){
                        return this.getDevice().info;
                    };


                    var meta = driver['user'];

                    var commandsCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
                    if (commandsCI && commandsCI['params']) {
                        driverInstance.sendSettings = utils.getJson(commandsCI['params']);
                    }

                    var responseCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES);
                    if (responseCI && responseCI['params']) {
                        driverInstance.responseSettings = utils.getJson(responseCI['params']);
                    }
                    try {
                        driverInstance.start();
                        driverInstance.initReload();
                    } catch (e) {
                        console.error('crash in driver instance startup! ' + device.toString());
                    }
                    thiz.driverInstance = driverInstance;
                    // Build an id basing on : driver id + driver path
                    // "235eb680-cb87-11e3-9c1a-....ab5_Marantz/Marantz.20.meta.json"
                    var scopeId = driverId + '_' + hash + '_' + device.path;
                    if (!driver.blox || !driver.blox.blocks) {
                        console.error('Attention : INVALID driver', device.toString());
                        driver.blox = {
                            blocks: []
                        };
                    }
                    if (driver.blockPath) {
                        utils.getJson(utils.readFile(driver.blockPath));
                        driver.blox = utils.getJson(utils.readFile(driver.blockPath));
                    }


                    if(!deviceManager.deviceInstances){
                        deviceManager.deviceInstances = {};
                    }
                    deviceManager.deviceInstances[hash] = driverInstance;
                    var scope = context.getBlockManager().createScope({
                        id: scopeId,
                        device: device,
                        driver: driver,
                        instance: driverInstance,
                        ctx: context,
                        serverSide: options.serverSide,
                        uuid:utils.createUUID(),
                        getContext: function () {
                            return this.instance;
                        }

                    }, dojo.clone(driver.blox.blocks));


                    driverInstance.blockScope = scope;
                    device.blockScope = scope;
                    device.driverInstance = driverInstance;
                    thiz.blockScope = scope;
                    dfd.resolve({
                        driverInstance:driverInstance,
                        blockScope:scope
                    });
                });
            });

            dfd.then(function (data){
                var scope = data.blockScope;
                scope.start();

            });
            return dfd;
        }
    });
});