define("nxapp/protocols/Driver", [
    'dcl/dcl',
    "nxapp/utils",
    'nxapp/protocols/ProtocolBase',
    "dojo/node!path",
    "require",
    "xide/types",
    "nxapp/utils/_console"
],function(dcl,utils,ProtocolBase,path,require,types,_console){
    var console = _console;

    var Module = dcl(ProtocolBase,{
        declaredClass:"nxapp.protocols.Driver",
        _socket:null,
        protocolName:'driver',
        instance:null,
        constructor:function(args){
            utils.mixin(this,args);
        },
        onError:function(cmd,options,buffer){
            this.isDebug() && console.info('Driver->onError ' + cmd + ' id ' + options.params.id + ' src ' + options.params.src);
            try {
                this.delegate.onData(this.connection, utils.mixin({
                    cmd: cmd,
                    event: types.EVENTS.ON_COMMAND_ERROR,
                    result:buffer.toString()
                },options),buffer);

            }catch(e){
                console.error('onFinish-Error:',e);
            }
        },
        onConnected:function(){
            this.isDebug() && console.log('# driver onConnected ' + this.options.host + ' : ' + this.options.port + ' @ ' + this.protocolName);
            this.connection.connected = true;
            this.delegate.onConnect2(this.connection);
        },
        onConnect:function(){
            this._socket.onConnect &&  this._socket.onConnect();
        },
        onFinish:function(cmd,options,buffer){
            this.isDebug() && console.info('Driver->onFinish ' + cmd + ' id ' + options.params.id + ' src ' + options.params.src);
            try {
                this.delegate.onData(this.connection, utils.mixin({
                    cmd: cmd,
                    event: types.EVENTS.ON_COMMAND_FINISH,
                    result:buffer.toString()
                },options),buffer);

            }catch(e){
                console.error('onFinish-Error:',e);
            }
        },
        onProgress:function(cmd,options,buffer){
            this.isDebug() && console.info('Driver->onProgress ' + cmd + ' id ' + options.params.id + ' src ' + options.params.src);
            try {
                this.delegate.onData(this.connection, utils.mixin({
                    cmd: cmd,
                    event: types.EVENTS.ON_COMMAND_PROGRESS,
                    result:buffer.toString()
                },options),buffer);

            }catch(e){
                console.error('onFinish-Progress:',e);
            }
        },
        onPaused:function(cmd,options,buffer){
            this.isDebug() && console.info('Driver->onPaused ' + cmd + ' id ' + options.params.id + ' src ' + options.params.src);
            try {
                this.delegate.onData(this.connection, utils.mixin({
                    cmd: cmd,
                    event: types.EVENTS.ON_COMMAND_PAUSED,
                    result:buffer.toString()
                },options),buffer);

            }catch(e){
                console.error('onFinish-Paused:',e);
            }
        },
        onStopped:function(cmd,options,buffer){
            this.isDebug() && console.info('Driver->onStopped ' + cmd + ' id ' + options.params.id + ' src ' + options.params.src);
            try {
                this.delegate.onData(this.connection, utils.mixin({
                    cmd: cmd,
                    event: types.EVENTS.ON_COMMAND_STOPPED,
                    result:buffer.toString()
                },options),buffer);

            }catch(e){
                console.error('onFinish-Stopped:',e);
            }
        },
        _connect:function(module,options,path){

            var instance = new module(options);

            instance.owner = this;

            instance.options = this.options;

            instance.delegate = this.delegate;

            instance.connection = this.connection;

            instance.modulePath = path;

            this.modulePath = path;

            //console.error('call connect');
            if(instance.connect){
            }else{
                console.error('have no connect');
            }

            this._socket = instance;

            this._socket.writable=true;

            instance.connect();

        },
        /**
         * @param evt {object}
         * @param evt.connection {module:nxapp/model/Connection}
         */
        onDeviceDisconnected:function(evt){
            this.delegate.onClose(this.connection,this.options);
        },
        onData:function(string,buffer){
            this.isDebug() && console.log('Driver on data : ');
            buffer = buffer || new Buffer(string);
            this.delegate.onData(this.connection,string,buffer);
            this._socket && this._socket.onData && this._socket.onData(this.connection,string,buffer);
        },
        connect:function(){
            var options = this.options;
            this.isDebug() && console.log('connect custom driver');
            if(!options || !options.driver){
                console.error('no driver in options',options);
                return this;
            }
            try {
                var driversRoot = options[options.driverScope];
                var modulePath = driversRoot + '/' + options.driver;
                modulePath  = utils.replaceAll('/',path.sep,modulePath);
                this.isDebug() && console.log('load driver : '+modulePath + ' drivers root : '+driversRoot + ' from ' + path.dirname(modulePath));
                var thiz = this;
                try {
                	if(!require.undef){
                		console.error('require has no undef');
					}
					require.undef && require.undef(modulePath);
                    require([modulePath], function (what) {
                        if(typeof what!=='function'){
                            thiz.close();
                            thiz.onDeviceDisconnected();
                            return;
                        }

                        try {
                            if (thiz._connect) {
                                thiz._connect(what, options,modulePath);
                            } else {
                                console.error('have no _connect');
                            }
                        } catch (e) {
                            console.error('error calling connect on ' + modulePath + ' | Error ' + e.message, e);
                        }
                    });

                } catch (e) {
                    console.error('-----error loading driver module at drivers root ' +driversRoot +' and module ' + options.driver + ' :: ' + e.message, e.stack);
                    //utils.stack();
                }

            }catch(e){
                console.error('Error connecting driver '+ e.message);
            }
            return this;
        },

        send:function(cmd,options) {
            if(!cmd){
                console.error('TCP : invalid command');
                return;
            }
            this.isDebug() && console.log('send : ',cmd);
            this._socket.write(cmd,options);
        },
        close:function() {
            if(this._socket &&this._socket.destroy) {
                this._socket.destroy();
            }
        }
    });

    dcl.chainAfter(Module,'close');
    dcl.chainAfter(Module,'destroy');

    return Module;
});
