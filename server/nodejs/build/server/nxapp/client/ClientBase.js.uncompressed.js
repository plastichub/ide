define("nxapp/client/ClientBase", [
    'dojo/_base/declare',
    'dojo/Stateful',
    'dojo/_base/lang'
],function(declare,Stateful,lang){

    return declare("nxapp.server.ClientBase", [Stateful],
        {
            options:null,
            _defaultOptions:function(){
                return {

                };
            },
            init:function(args) {
                this.options = lang.mixin(this._defaultOptions(), args.options);
            }

        });
});