define("xide/MaqettaPatch", [
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/aspect',
    'xide/types',
    'xide/factory',
    'xide/utils',
    'davinci/workbench/WidgetLite',
    'davinci/ve/widgets/WidgetProperties',
    'xide/mixins/EventedMixin'

], function (declare, lang, aspect, types, factory, utils, WidgetLite, WidgetProperties, EventedMixin) {
    /***
     * Patch Maqetta classes
     */
    return declare("xide.MaqettaPatch", null, {
        /**
         * replaces functions into an instance
         * @param target
         * @param source
         */
        mergeFunctions: function (target, source) {
            for (var i in source) {
                var o = source[i];
                if (lang.isFunction(source[i]) /*&& lang.isFunction(target[i])*/) {
                    target[i] = source[i];//swap
                }
            }
        },
        patchObject: function (dst, source) {
            dst.prototype.types = types;
            dst.prototype.utils = utils;
            dst.prototype.factory = factory;
        },
        patch: function () {
            this.patchObject(WidgetLite, EventedMixin);
            aspect.after(WidgetProperties.prototype, "_setValues", function () {
                factory.publish(types.EVENTS.WIDGET_PROPERTIES_RENDERED, {view: this}, this);
            }, true);
        }

    });
});