define("xide/mainr", [
    "xide/types",
    "xide/types/Types",
    'xide/utils/StringUtils',
    'xide/utils/HTMLUtils',
    'xide/utils/StoreUtils',
    'xide/utils/WidgetUtils',
    'xide/utils/CIUtils',
    'xide/utils/ObjectUtils',
    'xide/utils/CSSUtils',

    'xide/factory',
    'xide/factory/Objects',
    'xide/factory/Events',
    'xide/factory/Clients',
    "xide/client/ClientBase",

    "xide/client/WebSocket",
    "xide/data/Memory",
    "xide/data/Model",
    "xide/data/ObservableStore",
    "xide/data/Reference",
    "xide/data/Source",
    "xide/data/TreeMemory",
    "xide/data/_Base",
    "xide/encoding/MD5",
    "xide/encoding/SHA1",
    "xide/encoding/_base",

    "xide/model/Component",
    "xide/debug",

    "xide/mixins/EventedMixin",
    "xide/mixins/ReferenceMixin",
    "xide/mixins/ReloadMixin",
    "xide/min",

    "xide/manager/BeanManager",
    "xide/manager/ContextBase",
    "xide/manager/Context",
    "xide/manager/ManagerBase",
    "xide/manager/PluginManager",
    "xide/manager/RPCService",
    "xide/manager/Reloadable",
    "xide/manager/ResourceManager",
    "xide/manager/ServerActionBase",
    'xide/utils'
], function () {

});