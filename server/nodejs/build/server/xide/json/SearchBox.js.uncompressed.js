define("xide/json/SearchBox", [
    'dojo/_base/declare',
    './util'
], function (declare,util) {
    /**
     * @constructor SearchBox
     * Create a search box in given HTML container
     * @param {JSONEditor} editor    The JSON Editor to attach to
     * @param {Element} container               HTML container element of where to
     *                                          create the search box
     */
    function SearchBox (editor, container) {
        const searchBox = this;
        this.editor = editor;
        this.timeout = undefined;
        this.delay = 200; // ms
        this.lastText = undefined;

        this.dom = {};
        this.dom.container = container;

        const table = document.createElement('table');
        this.dom.table = table;
        table.className = 'search';
        container.appendChild(table);
        const tbody = document.createElement('tbody');
        this.dom.tbody = tbody;
        table.appendChild(tbody);
        let tr = document.createElement('tr');
        tbody.appendChild(tr);

        let td = document.createElement('td');
        tr.appendChild(td);
        const results = document.createElement('div');
        this.dom.results = results;
        results.className = 'results';
        td.appendChild(results);

        td = document.createElement('td');
        tr.appendChild(td);
        const divInput = document.createElement('div');
        this.dom.input = divInput;
        divInput.className = 'frame form-control input-transparent';
        divInput.title = 'Search fields and values';
        td.appendChild(divInput);

        // table to contain the text input and search button
        const tableInput = document.createElement('table');
        divInput.appendChild(tableInput);
        const tbodySearch = document.createElement('tbody');
        tableInput.appendChild(tbodySearch);
        tr = document.createElement('tr');
        tbodySearch.appendChild(tr);

        const refreshSearch = document.createElement('button');
        refreshSearch.className = 'refresh';
        td = document.createElement('td');
        td.appendChild(refreshSearch);
        tr.appendChild(td);

        const search = document.createElement('input');
        this.dom.search = search;
        search.oninput = function (event) {
            searchBox._onDelayedSearch(event);
        };
        search.onchange = function (event) { // For IE 9
            searchBox._onSearch(event);
        };
        search.onkeydown = function (event) {
            searchBox._onKeyDown(event);
        };
        search.onkeyup = function (event) {
            searchBox._onKeyUp(event);
        };
        refreshSearch.onclick = function (event) {
            search.select();
        };

        // TODO: ESC in FF restores the last input, is a FF bug, https://bugzilla.mozilla.org/show_bug.cgi?id=598819
        td = document.createElement('td');
        td.appendChild(search);
        tr.appendChild(td);

        const searchNext = document.createElement('button');
        searchNext.title = 'Next result (Enter)';
        searchNext.className = 'next';
        searchNext.onclick = function () {
            searchBox.next();
        };
        td = document.createElement('td');
        td.appendChild(searchNext);
        tr.appendChild(td);

        const searchPrevious = document.createElement('button');
        searchPrevious.title = 'Previous result (Shift+Enter)';
        searchPrevious.className = 'previous';
        searchPrevious.onclick = function () {
            searchBox.previous();
        };
        td = document.createElement('td');
        td.appendChild(searchPrevious);
        tr.appendChild(td);
    }

    /**
     * Go to the next search result
     * @param {boolean} [focus]   If true, focus will be set to the next result
     *                            focus is false by default.
     */
    SearchBox.prototype.next = function(focus) {
        if (this.results != undefined) {
            let index = (this.resultIndex != undefined) ? this.resultIndex + 1 : 0;
            if (index > this.results.length - 1) {
                index = 0;
            }
            this._setActiveResult(index, focus);
        }
    };

    /**
     * Go to the prevous search result
     * @param {boolean} [focus]   If true, focus will be set to the next result
     *                            focus is false by default.
     */
    SearchBox.prototype.previous = function(focus) {
        if (this.results != undefined) {
            const max = this.results.length - 1;
            let index = (this.resultIndex != undefined) ? this.resultIndex - 1 : max;
            if (index < 0) {
                index = max;
            }
            this._setActiveResult(index, focus);
        }
    };

    /**
     * Set new value for the current active result
     * @param {Number} index
     * @param {boolean} [focus]   If true, focus will be set to the next result.
     *                            focus is false by default.
     * @private
     */
    SearchBox.prototype._setActiveResult = function(index, focus) {
        // de-activate current active result
        if (this.activeResult) {
            const prevNode = this.activeResult.node;
            const prevElem = this.activeResult.elem;
            if (prevElem == 'field') {
                delete prevNode.searchFieldActive;
            }
            else {
                delete prevNode.searchValueActive;
            }
            prevNode.updateDom();
        }

        if (!this.results || !this.results[index]) {
            // out of range, set to undefined
            this.resultIndex = undefined;
            this.activeResult = undefined;
            return;
        }

        this.resultIndex = index;

        // set new node active
        const node = this.results[this.resultIndex].node;
        const elem = this.results[this.resultIndex].elem;
        if (elem == 'field') {
            node.searchFieldActive = true;
        }
        else {
            node.searchValueActive = true;
        }
        this.activeResult = this.results[this.resultIndex];
        node.updateDom();

        // TODO: not so nice that the focus is only set after the animation is finished
        node.scrollTo(function () {
            if (focus) {
                node.focus(elem);
            }
        });
    };

    /**
     * Cancel any running onDelayedSearch.
     * @private
     */
    SearchBox.prototype._clearDelay = function() {
        if (this.timeout != undefined) {
            clearTimeout(this.timeout);
            delete this.timeout;
        }
    };

    /**
     * Start a timer to execute a search after a short delay.
     * Used for reducing the number of searches while typing.
     * @param {Event} event
     * @private
     */
    SearchBox.prototype._onDelayedSearch = function (event) {
        // execute the search after a short delay (reduces the number of
        // search actions while typing in the search text box)
        this._clearDelay();
        const searchBox = this;
        this.timeout = setTimeout(function (event) {
                searchBox._onSearch(event);
            },
            this.delay);
    };

    /**
     * Handle onSearch event
     * @param {Event} event
     * @param {boolean} [forceSearch]  If true, search will be executed again even
     *                                 when the search text is not changed.
     *                                 Default is false.
     * @private
     */
    SearchBox.prototype._onSearch = function (event, forceSearch) {
        this._clearDelay();

        const value = this.dom.search.value;
        const text = (value.length > 0) ? value : undefined;
        if (text != this.lastText || forceSearch) {
            // only search again when changed
            this.lastText = text;
            this.results = this.editor.search(text);
            this._setActiveResult(undefined);

            // display search results
            if (text != undefined) {
                const resultCount = this.results.length;
                switch (resultCount) {
                    case 0: this.dom.results.innerHTML = 'no&nbsp;results'; break;
                    case 1: this.dom.results.innerHTML = '1&nbsp;result'; break;
                    default: this.dom.results.innerHTML = resultCount + '&nbsp;results'; break;
                }
            }
            else {
                this.dom.results.innerHTML = '';
            }
        }
    };

    /**
     * Handle onKeyDown event in the input box
     * @param {Event} event
     * @private
     */
    SearchBox.prototype._onKeyDown = function (event) {
        const keynum = event.which;
        if (keynum == 27) { // ESC
            this.dom.search.value = '';  // clear search
            this._onSearch(event);
            event.preventDefault();
            event.stopPropagation();
        }
        else if (keynum == 13) { // Enter
            if (event.ctrlKey) {
                // force to search again
                this._onSearch(event, true);
            }
            else if (event.shiftKey) {
                // move to the previous search result
                this.previous();
            }
            else {
                // move to the next search result
                this.next();
            }
            event.preventDefault();
            event.stopPropagation();
        }
    };

    /**
     * Handle onKeyUp event in the input box
     * @param {Event} event
     * @private
     */
    SearchBox.prototype._onKeyUp = function (event) {
        const keynum = event.keyCode;
        if (keynum != 27 && keynum != 13) { // !show and !Enter
            this._onDelayedSearch(event);   // For IE 9
        }
    };

    return SearchBox;
});

