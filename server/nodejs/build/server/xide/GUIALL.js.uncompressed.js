define("xide/GUIALL", [
    'dojo/_base/declare',
    'xide/factory/Widgets',
    'xide/factory/Views',
    'xide/factory/Widgets',
    'xide/factory/Views',
    'xide/views/CIView',
    'xide/views/_Dialog',
    'xide/views/_CIDialog',
    'xide/layout/Container',
    'xide/layout/_Accordion',
    'xide/layout/_TabContainer'
],function(declare){
    return declare("xide.GUIALL", null,{});
});