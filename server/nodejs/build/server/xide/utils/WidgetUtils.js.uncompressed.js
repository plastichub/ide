define("xide/utils/WidgetUtils", [
    'xide/utils',
    'xide/types',
    'xide/registry'
], function (utils,types,registry) {
    "use strict";
    utils.getParentWidget=function(start,declaredClass,max){
        //sanitize start
        start = start.containerNode || start.domNode || start;
        let i = 0;
        let element = start;
        let widget = null;
        const _max = max || 10;
        let _lastWidget = null;

        while (i < _max && !widget) {
            if (element) {
                element = element.parentNode;
                const _widgetById = registry.byId(element.id);
                const _widget = _widgetById || registry.getEnclosingWidget(element);
                _widget && (_lastWidget = _widget);
                if(_widget && declaredClass &&  _widget.declaredClass && _widget.declaredClass.indexOf(declaredClass)!=-1){
                    widget = _widget;
                }
            }
            i++;
        }
        return widget;
    };
    /**
     *
     * @param type
     * @returns {string}
     */
    utils.getWidgetType = function (type) {
        let res = "";
        const root = 'xide.widgets.';
        if (type == types.ECIType.ENUMERATION) {
            res = root  + "Select";
        }
        if (type == types.ECIType.STRING) {
            res = root  + "TextBox";
        }

        if (type == types.ECIType.ICON) {
            res = root  + "TextBox";
        }

        if (type == types.ECIType.REFERENCE) {
            res = root  + "Button";
        }

        if (type == types.ECIType.EXPRESSION) {
            res = root  + "Expression";
        }

        if (type == types.ECIType.EXPRESSION_EDITOR) {
            res = root  + "ExpressionEditor";
        }

        if (type == types.ECIType.ARGUMENT) {
            res = root  + "ArgumentsWidget";
        }

        if (type == types.ECIType.WIDGET_REFERENCE) {
            res = root  + "WidgetReference";
        }

        if (type == types.ECIType.BLOCK_REFERENCE) {
            res =root  +  "BlockPickerWidget";
        }

        if (type == types.ECIType.BLOCK_SETTINGS) {
            res = root  + "BlockSettingsWidget";
        }

        if (type == types.ECIType.DOM_PROPERTIES) {
            res = root  + "DomStyleProperties";
        }

        if (type == types.ECIType.FILE_EDITOR) {
            res = root  + "FileEditor";
        }

        return res;
    };
    return utils;
});