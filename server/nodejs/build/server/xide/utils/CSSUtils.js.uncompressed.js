/** @module xide/utils/CSSUtils
 *  @description All string related functions
 */
define("xide/utils/CSSUtils", [
    'xide/utils',
    'xide/types'

], function (utils, types) {


    "use strict";

    /**
     *
     * @param styleString
     * @param property
     * @returns {*}
     */
    utils.findStyle=function(styleString,property){
        const parser = new CSSParser();
        let content = ".foo{";
        content+=styleString;
        content+="}";
        const sheet=parser.parse(content, false, true);
        const declarations = sheet.cssRules[0].declarations;
        const declaration = _.find(declarations,{
            property:property
        });

        if(declaration){
            return declaration.valueText;
        }
        return "";
    }

    /**
     *
     * @param styleString
     * @returns {*}
     */
    utils.getBackgroundUrl=function(styleString){
        const background = utils.findStyle(styleString,"background-image");
        if(background) {
            try {
                return background.match(/\((.*?)\)/)[1].replace(/('|")/g, '');
            }catch(e){}
        }
        return null;
    }

    return utils;
});
