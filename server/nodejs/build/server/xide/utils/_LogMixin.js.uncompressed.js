define("xide/utils/_LogMixin", [
    'dcl/dcl',
    'xide/utils',
    'xide/model/Base'
], function (dcl, utils,Base) {
    return dcl(Base.dcl,{
        declaredClass:"xide.utils._LogMixin",
        debug_conf: null,
        initLogger: function (debug_config) {
            this.debug_conf = debug_config;
        },
        log: function (msg, msg_context) {
            if (!msg_context) msg_context = this._debugContext()["main"];
            if (this.showDebugMsg(msg_context)) {
                console.log(msg);
            }
        },
        showDebugMsg: function (msg_context) {
            if (this.debug_conf != null) {
                if (this.debug_conf["all"]) {
                    return true;
                }
                else {
                    if (this.debug_conf[msg_context]) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            } else {
                console.log("No debug config, showing all.");
                this.debug_conf = {
                    "all": true
                };
                return true;
            }
        }
    });
});