define("xide/action/ActionContext", [
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'dojo/aspect',
    'xide/views/History'
], function (dcl, declare, types, aspect, History) {

    const _debug = false;
    /**
     * Mixin to handle different action contexts.
     *
     * @mixin module:xide/action/ActionContext
     */
    const Implementation = {
        currentActionEmitter: null,
        _history: null,
        isEmpty: function () {
            const _emitter = this.getCurrentEmitter();
            if (_emitter) {
                return _emitter.getActionStore().getAll().length == 0;
            }
            return true;
        },
        getCurrentEmitter: function () {
            return this.currentActionEmitter;
        },
        _onRemoveEmitter: function (emitter) {
            this._history.remove(emitter);
            const _next = this._history.getNow();
            const cEmitter = this.currentActionEmitter;
            if (cEmitter == emitter) {
                this.currentActionEmitter = null;
            }
            const _last = _next;
            if (_last) {
                this.setActionEmitter(_last);
            }
        },
        refreshActions: function (actions) {
            actions.forEach((action)=> {
                if (this.renderAction) {
                    this.renderAction(action, null, null, null, null);
                } else {
                    console.error('renderAction not implemented for refresh actions ' + this.declaredClass);
                }
            });
        },
        setActionEmitter: function (emitter, what, event) {
            if (emitter && emitter.getActionStore && !emitter.getActionStore()) {
                _debug && console.warn('setActionEmitter: emitter returns null action store! abort ' + emitter.declaredClass, emitter);
                return;
            }
            if (this.currentActionEmitter == emitter) {
                if (!emitter) {
                    this.setActionStore(null);
                } else {

                }
                return;
            }
            _debug && console.log('setActionEmitter ' + this.id + ' ' + this.declaredClass + ' for : ' + what + ' emitter : ' + emitter.id);
            try {
                const cEmitter = this.currentActionEmitter;
                if (cEmitter) {
                    if (cEmitter.getActionStore) {
                        const store = cEmitter.getActionStore();
                        if (store) {
                            store._all = null;
                        } else {
                            _debug && console.warn('setActionEmitter no store');
                        }
                        this.clearActions();
                    } else {
                        _debug && console.warn('setActionEmitter current emitter has no getActionStore', cEmitter);
                    }
                    cEmitter && cEmitter.onDeactivateActionContext && cEmitter.onDeactivateActionContext(this, event);
                }
            } catch (e) {
                console.warn('setActionEmitter crash', e);
                logError(e);
            }
            if (emitter && !emitter.getActionStore) {
                _debug && console.error('not an action emitter ' + emitter.declaredClass);
                return;
            }
            this.currentActionEmitter = emitter;
            if (!emitter) {
                this.setActionStore(null);
                return;
            }
            const newEmitterStore = emitter.getActionStore();
            if (!newEmitterStore) {
                _debug && console.error('new emitter has no action store ! ' + emitter.declaredClass);
                return;
            }

            newEmitterStore.__all = null;
            this.setActionStore(newEmitterStore, emitter);
            newEmitterStore.addRenderer(this);
            emitter && emitter.onActivateActionContext && emitter.onActivateActionContext(this, event);
            this._emit('setActionEmitter', {
                emitter: emitter
            });
            !this._history && (this._history = new History());
            this._history.setNow(emitter);
        },
        _registerActionEmitter: function (emitter) {
            if (emitter && !emitter.getActionStore) {
                _debug && console.error('_registerActionEmitter: is not an action provider');
                return;
            }
            if (!emitter || !emitter.on) {
                _debug && console.warn('register action emitter : emitter = null');
                return false;
            }
            const thiz = this;

            const handler = function (what, e) {
                thiz.setActionEmitter(emitter, what, e);
            };

            const _handle = emitter._on('selectionChanged', function (e) {
                e[thiz.id + '_aceDid'] = true;
                const type = e.why == 'clear' ? 'selectionCleared' : 'selectionChanged';
                handler(type, e);
            });


            emitter.on('click', function (e) {
                let doHandler = true;
                if (emitter.handleActionClick) {
                    doHandler = emitter.handleActionClick(e);
                }
                doHandler && handler('click', e);
            });
            !this._history && (this._history = new History());
            emitter._on(types.EVENTS.ON_VIEW_SHOW, function (view) {
                if (thiz._history.indexOf(view)) {
                    view.view && (view = view.view);
                    thiz.setActionEmitter(view, types.EVENTS.ON_VIEW_SHOW, view);
                }
            });
        },
        destroy: function () {
            this.inherited && this.inherited(arguments);
            this._history && this._history.destroy() && delete this._history;
        },
        addActionEmitter: function (emitter) {
            if (!emitter) {
                _debug && console.warn('addActionEmitter::emitter is null');
                return;
            }
            const thiz = this;

            !this._history && (this._history = new History());
            if (!emitter.getActionStore) {
                _debug && console.error('invalid emitter ', emitter);
                return;
            }

            this._history.push(emitter);
            thiz._registerActionEmitter(emitter);
            function remove(emitter) {
                thiz._onRemoveEmitter(emitter);
            }

            aspect.after(emitter, 'destroy', function () {
                remove(emitter);
            }, true);

            emitter._on(emitter, 'destroy', function () {
                try {
                    remove(emitter);
                } catch (e) {
                    logError(e, 'addActionEmitter');
                }
            }, true);
        }
    };
    //package via declare
    const Module = declare('xide/action/ActionContext', null, Implementation);
    //package via dcl
    Module.dcl = dcl(null, Implementation);

    return Module;
});