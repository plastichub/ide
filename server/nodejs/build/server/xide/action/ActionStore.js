//>>built
define("xide/action/ActionStore","xdojo/declare xide/data/TreeMemory xide/data/ObservableStore dstore/Trackable dojo/Deferred xide/action/ActionModel".split(" "),function(c,d,e,f,h,g){var b=c("xide.action.ActionStore",[d,f,e],{idProperty:"command",Model:g,renderers:null,observedProperties:["value","icon","disabled","enabled"],getAll:function(){return this.data},addRenderer:function(a){!this.renderers&&(this.renderers=[]);!_.contains(this.renderers,a)&&this.renderers.push(a)}});b.createDefault=function(a){return new b(a)};
return b});
//# sourceMappingURL=ActionStore.js.map