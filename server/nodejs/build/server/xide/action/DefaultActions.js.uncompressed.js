/** @module xide/action/DefaultActions **/
define("xide/action/DefaultActions", [
    'xaction/DefaultActions'
], function (DefaultActions) {
    return DefaultActions;
});