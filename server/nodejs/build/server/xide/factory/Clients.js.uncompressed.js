define("xide/factory/Clients", [
    'xide/factory',
    'xide/utils',
    'xide/types',
    'xide/client/WebSocket'
], function (factory,utils,types,WebSocket)
{
    const debug = false;
    /**
     * Low Level Web-Socket-Client factory method
     * @param ctrArgs
     * @param host
     * @param port
     * @param delegate
     */
    factory.createClient=function(ctrArgs,host,port,delegate){};
    /***
     * High-Level Web-Socket-Client factory method
     * @param store
     * @param serviceName
     * @param ctrArgs
     * @param clientClass : optional client class prototype, default : WebSocket
     * @returns {xide/client/WebSocket} | null
     */
    factory.createClientWithStore=function(store,serviceName,ctrArgs,clientClass){
        const service = utils.queryStoreEx(store,{
                name:serviceName
            },true,true);

        const _ctorArgs = {};

        utils.mixin(_ctorArgs,ctrArgs);

        if(!service||service.length===0){
            console.error('create client with : failed, no such service :  ' + serviceName );
            return;
        }
        if(service.status!==types.SERVICE_STATUS.ONLINE){
            debug && console.error('create client with store : failed! Service ' +  serviceName + ' is not online ');
        }

        let host = 'http://' + service.host;
        let port = service.port;
        const channel='';
        const _clientProto = clientClass || WebSocket;

        if(service.info){
            host=service.info.host;
            port=service.info.port;
        }

        try{
            const client = new _clientProto(_ctorArgs);
            utils.mixin(client,_ctorArgs);
            client.init({
                options:{
                    host:host,
                    port:port,
                    channel:channel,
                    debug:{
                        "all": false,
                        "protocol_connection": true,
                        "protocol_messages": true,
                        "socket_server":true,
                        "socket_client":true,
                        "socket_messages":true,
                        "main":true
                    }
                }
            });
            client.connect();
            return client;
        }catch(e){
            debug && console.error('create client with store : failed : ' + e) && logError(e);
        }
    };
    return factory;
});