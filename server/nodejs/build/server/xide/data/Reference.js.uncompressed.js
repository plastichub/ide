/** @module xide/data/Reference **/
define("xide/data/Reference", [
    "dcl/dcl",
    "xide/utils",
    "xide/lodash",
    "xide/mixins/EventedMixin"
], function (dcl, utils, lodash, EventedMixin) {
    const debug = false;
    /**
     * @class module:xide/data/Reference
     */
    const Implementation = {
        /**
         * @type {Array<module:xide/data/Source>}
         */
        _sources: [],
        /**
         * Activate destruction by default upon store destroy.
         */
        destroyOnRemove: true,
        /**
         * @type {module:xide/data/_Base|null} The store.
         */
        _store: null,
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  deprecated API
        //
        removeSource: function (source) {
        },
        updateSource: function (sources) {
        },
        onSourceUpdate: function (source) {
        },
        onSourceRemoved: function (source) {
        },
        onSourceDelete: function (source) {
        },
        //  deprecated API - end
        /**
         * Placeholder
         * @param args
         */
        onItemChanged: function (args) {
        },
        destroy: function () {
            if (this.item && !this.item.removeReference) {
                debug && console.error('item has no removeReference');
            } else {
                this.item && this.item.removeReference(this);
            }
            this.inherited && this.inherited(arguments);
            if (this._sources) {
                this._sources.forEach(link => {
                    if (link.item) {
                        link.item.removeReference && link.item.removeReference(this);
                    }
                });

                this._sources = null;
            }
        },
        hasSource: function (source) {
            return lodash.find(this._sources, {item: source});
        },
        addSource: function (source, settings) {
            if (this.hasSource(source)) {
                debug && console.warn('already have source');
                return;
            }
            this._sources.push({
                item: source,
                settings: settings
            });
            if (settings && settings.onDelete) {
                this.addHandle('delete', source._store.on('delete', function (evt) {
                    if (evt.target == source) {
                        this._store.removeSync(this[this._store.idProperty]);
                    }
                }.bind(this)));
            }
        },
        updateSources: function (args) {
            this._sources.forEach(link => {
                const item = link.item;
                const settings = link.settings;
                if (args.property && settings.properties && settings.properties[args.property]) {
                    item._store.silent(true);
                    item.set(args.property, args.value);
                    item._store.silent(false);
                    item._store.emit('update', {target: item});
                }
            });
        },
        constructor: function (properties) {
            this._sources = [];
            utils.mixin(this, properties);
        }
    };
    const Module = dcl([EventedMixin.dcl], Implementation);
    Module.Implementation = Implementation;
    return Module;
});