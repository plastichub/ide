/** @module xide/data/TreeMemory **/
define("xide/data/TreeMemory", [
    "dojo/_base/declare",
    'xide/data/Memory',
    'dstore/Tree',
    'dojo/Deferred',
    'dstore/QueryResults'
], function (declare, Memory, Tree, Deferred, QueryResults) {

    /**
     * @class module:xide/data/TreeMemory
     * @deprecated
     * @extends module:xide/data/_Base
     * @extends module:dstore/Tree
     */
    return declare('xide.data.TreeMemory', [Memory, Tree], {
        _state: {
            filter: null
        },
        parentProperty: 'parentId',
        reset: function () {
            this._state.filter = null;
            this.resetQueryLog();
        },
        resetQueryLog: function () {
            this.queryLog = [];
        },
        fetchRange: function () {
            // dstore/Memory#fetchRange always uses fetchSync, which we aren't extending,
            // so we need to extend this as well.
            const results = this._fetchRange(arguments);
            return new QueryResults(results.then(function (data) {
                return data;
            }), {
                totalLength: results.then(function (data) {
                    return data.length;
                })
            });
        },
        filter: function (data) {
            const _res = this.inherited(arguments);
            this._state.filter = data;
            return _res;
        },
        _fetchRange: function (kwArgs) {
            const deferred = new Deferred();
            let _res = this.fetchRangeSync(kwArgs);
            if (this._state.filter) {
                //the parent query
                if (this._state.filter['parent']) {
                    var _item = this.getSync(this._state.filter[this.parentProperty]);
                    if (_item) {
                        this.reset();
                        const _query = {};
                        if (this.getChildrenSync) {
                            _res = this.getChildrenSync(_item);
                        } else {
                            _query[this.parentProperty] = _item[this.idProperty];
                            _res = this.root.query(_query);
                        }
                    }
                }


                //the group query
                if (this._state && this._state.filter && this._state.filter['group']) {
                    const _items = this.getSync(this._state.filter.parent);
                    if (_item) {
                        this.reset();
                        _res = _item.items;
                    }
                }
            }
            deferred.resolve(_res);
            return deferred;
        },
        getChildren: function (object) {
            const filter = {};
            filter[this.parentProperty] = this.getIdentity(object);
            return this.root.filter(filter);
        },
        children: function (parent) {
            const all = this.root.data;
            const out = [];

            all.forEach(obj => {
                if (obj[this.parentProperty] == parent[this.idProperty]) {
                    out.push(obj);
                }
            });

            return all;
        },
        mayHaveChildren: function (parent) {
            if (parent._mayHaveChildren === false) {
                return false;
            }
            return true;
        }
    });
});
