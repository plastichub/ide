define("xide/backup/widgets/ScriptWidget.bak", [
    'dojo/_base/declare',
    'dojo/on',
    'xide/widgets/WidgetBase',
    'xide/views/TextEditor',
    "dojo/keys",
    "xide/utils"
], function (declare, on, WidgetBase, TextEditor, keys, utils) {
    return declare("xide.widgets.ScriptWidget", [WidgetBase], {
        didLoad: false,
        minHeight: "300px",
        editorHeight: "470px",
        jsonEditorHeight: "270px",
        aceEditorHeight: "200px",
        aceEditorOptions: null,
        aceEditor: null,
        aceNode: null,
        templateString: "<div class='jsonEditorWidgetContainer' style='width: 100%;height:300px'>" +
        "<div valign='middle' class='aceEditorWidget' attachTo='aceNode' style='height:100%;padding: 0px;'></div>" +
        "</div>",
        editor: null,
        getValue: function () {
            if (this.aceEditor) {
                return this.aceEditor.get('value');
            }
            return this.inherited(arguments);
        },
        resize: function () {
            this.inherited(arguments);
            if (this.aceEditor) {
                this.aceEditor.resize();
            }
        },
        ctlrKeyDown: false,
        onKeyUp: function (evt) {

            switch (evt.keyCode) {
                case keys.ALT:
                case keys.CTRL:
                {
                    this.ctlrKeyDown = false;
                    break;
                }
            }
        },
        onKey: function (evt) {
            switch (evt.keyCode) {

                case keys.META:
                case keys.ALT:
                case keys.SHIFT:
                case keys.CTRL:
                {
                    this.ctlrKeyDown = true;
                    setTimeout(function () {
                        thiz.ctlrKeyDown = false
                    }, 2000);
                    break;
                }
            }
            var thiz = this;
            if (evt.type && evt.type == 'keyup') {
                return this.onKeyUp(evt);
            }
            var charOrCode = evt.charCode || evt.keyCode;

            if (this.ctlrKeyDown && charOrCode === 83) {
                evt.preventDefault();

                if (this.delegate && this.delegate.onSave) {
                    this.delegate.onSave(this.userData, this.getValue());
                }
            }
        },
        setupEventHandlers: function () {
            var thiz = this;

            on(this.aceNode, "keydown", function (event) {
                thiz.onKey(event);
            });

        },
        setValue: function (value) {
            this.userData = utils.setCIValueByField(this.userData, "value", value);
        },
        _onACEUpdate: function () {
            var _newValue = this.getValue();
            this.setValue(_newValue);
            this.changed = true;
            if (this.userData) {
                this.userData.changed = true;
            }
        },
        _createACEEditor: function (parentNode) {
            var _options = {
                region: "center",
                value: this.getValue(),
                style: "margin: 0; padding: 0; position:relative;overflow: auto;height:inherit;width:inherit;",
                mode: 'javascript',
                readOnly: false,
                tabSize: 2,
                softTabs: false,
                wordWrap: false,
                showPrintMargin: false,
                highlightActiveLine: false,
                fontSize: '13px',
                showGutter: true,
                className: 'editor-ace ace_editor ace-codebox-dark ace_dark'
            };

            var editorPane = new TextEditor(_options, dojo.doc.createElement('div'));

            this.aceNode.appendChild(editorPane.domNode);
            editorPane.startup();
            this.aceEditor = editorPane;
            this.aceEditor.setOptions();

            var editor = this.aceEditor.getEditor();
            var thiz = this;
            editor.on('change', function () {
                thiz._onACEUpdate();
            });

            editor.on('blur', function () {
                thiz.setActive(false);
            });
            editor.on('focus', function () {
                thiz.setActive(true);
            });

        },
        getAceNode: function () {

        },
        startup: function () {
            this.inherited(arguments);

            try {
                if (!this.aceEditor) {
                    this._createACEEditor(this.aceNode);
                }
            } catch (e) {
                console.error('constructing json editor widget failed : ' + e.message);
            }
            this.setupEventHandlers();

        }
    });
});