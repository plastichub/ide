/** @module xide/widgets/_MenuMixin **/
define("xide/backup/widgets/_MenuMixin.bak", [
    'dojo/_base/connect',
    'dijit/DropDownMenu',
    'dijit/PopupMenuBarItem',
    'dijit/MenuSeparator',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-class',
    'dojo/dom-style',
    'dojo/aspect',
    'xide/types',
    'xide/utils',
    'dijit/Menu',
    'dijit/MenuItem',
    'dijit/PopupMenuItem',
    'xide/data/Reference',
    'xaction/DefaultActions'
], function (connect,DropDownMenu, PopupMenuBarItem,MenuSeparator,declare, lang, domClass,domStyle,aspect,types, utils, Menu, MenuItem, PopupMenuItem,Reference,DefaultActions) {


    var _debug = false;
    /**
     * Mixin which provides utils for menu & action related render tasks.
     * This is heavily utilized by MainMenu,ActionToolbar, FilePropertyView and the ContextMenuHandler.
     * It requires that the 'ActionMixin' is part of the game too!
     *
     * @mixin module:xide/widgets/_MenuMixin
     */
    return declare("xide/widgets/_MenuMixin",null, {

        forceRenderSubActions:false,
        _patchMenu: function (widget) {

            var thiz = this;

            aspect.after(widget, 'onOpen', function () {

                _debug && console.log('onOpen');
                var dst = this._popupWrapper,
                    item = this.item,
                    items = item.getChildren ? item.getChildren() : item.items;

                if (dst) {
                    if(item && item.command) {
                        domClass.add(dst,utils.replaceAll('/','-',item.command));
                    }
                }
                if(!widget._didRender){
                    _debug && console.log('did not render yet: ',item);
                    thiz._renderActions(items,widget,MenuItem,true,MenuSeparator,true);
                    widget._didRender = true;
                }
            });
        },
        empty:function(what,newItem){


            var thiz = this,
                menu = what || thiz.rootMenu,
                widgets = menu.getChildren();

            _.each(widgets,function(widget){

                var action = widget.item,
                    remove = true;

                _debug && console.log('empty: ',action.command);

                if(action && action.shouldDestroyWidget){
                    remove = action.shouldDestroyWidget(thiz.visibility,thiz,newItem);
                }
                _debug && console.log(' empty: remove child',action.command);
                remove && menu.removeChild(widget);
            });

        },
        /**
         * The root menu or widget
         * @type dijit/MenuBar|dijit/Toolbar
         * @default null
         */
        rootMenu:null,
        /**
         * The separator class for separating actions
         * @type dijit/MenuSeparator
         * @default dijit/MenuSeparator
         */
        separatorClass:MenuSeparator,
        /**
         * Creates a menu item
         *
         * @param iconClass {String} An icon class, dijit or icon font as font-awesome
         * @param label {String} A label for the menu item, dijit wants one!
         * @param operation {String|Number} Deprecated and here for back-compat
         * @param clickcb {Function} Callback for click
         * @param command {String} A unique command, example: Views/Log or File/Properties
         * @param where {dijit/_Widget|HTMLElement}
         * @returns {dijit/MenuItem}
         */
        createMenuItem: function (iconClass, label, operation, clickcb, command, where) {

            var item = utils.addWidget(MenuItem,{
                iconClass: iconClass,
                label: label,
                operation: operation,
                item: null,
                command: command
            },this,where);

            item.on('click', function () {
                if (clickcb) {
                    clickcb(item);
                } else {
                    this.delegate.onButtonClicked(item);
                }

            }.bind(this));

            return item;
        },
        /**
         * Renders a set of actions into is appropriate widgets. This utilizes this._renderAction and
         * puts the action's widget into its visibility store for global access.
         *
         * @param actions {xaction/Action[]}
         * @param where {dijit/_Widget}
         * @param widgetClass {Module}
         * @private
         */
        _renderActions:function(actions,where,widgetClass,showLabel,separatorClass,force){


            //track the last action group for adding a separator
            var _lastGroup = null,
                thiz = this,
                widgets = [],
                _lastWidget;

            if(!actions){
                console.error('strange, have no actions!');
                return;
            }

            for (var i = 0; i < actions.length; i++) {

                var action = actions[i],
                    sAction = action._store ? action._store.getSync(action.command) : null;

                if(sAction){
                    action = sAction;
                }

                if(!action){
                    console.error('invalid action!');
                    continue;
                }

                //pick command group
                if(i==0 && !_lastGroup && action.group){
                    _lastGroup=action.group;
                }

                //skip if action[visibility].show is false
                if(thiz.getVisibilityField(action,'show')===false){
                    continue;
                }

                var _items = action.getChildren ? action.getChildren() : action.items,
                    _hasItems = _items && _items.length,
                    _expand = thiz.getVisibilityField(action,'expand'),
                    _forceSub = action.forceSubs;


                //action has its own set of actions
                if ((_hasItems || _forceSub ) && !thiz.getVisibilityField(action,'widget')) {

                    if(_expand==true) {

                    }else{
                        thiz.renderSubActions(action, where);
                    }
                    continue;
                }

                force = force !=null ? force : this.forceRenderSubActions;

                //check action for a parent widget
                var parent = action.getParent ? action.getParent() : null;
                if(parent){

                    var parentWidget= thiz.getVisibilityField(parent,'widget');

                    if(parentWidget && force !==true  && _expand!==true){
                        console.log('skip action, has parent and force not TRUE');
                        continue;
                    }
                }

                _debug && console.log('render action: '+action.command,[actions,action]);

                if (action && action.show !== false) {

                    //  Insert separator as soon there is a 'group' and the group actually changed
                    if (!_.isEmpty(action.group) && action.group !== _lastGroup) {

                        var children = where.getChildren();

                        if(children.length>0 && children[children.length-1].prototype !=thiz.separatorClass.prototype){

                            var separator = utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true),
                            widgetArgs = thiz.getVisibilityField(action,'widgetArgs');
                            
                            if(widgetArgs && widgetArgs.style && widgetArgs.style.indexOf('float:right')!=-1){
                                domStyle.set(separator.domNode,{
                                    'float':'right'
                                });
                            }

                        }
                        _lastGroup = action.group;
                    }

                    _lastWidget = thiz._renderAction(action,where,widgetClass,showLabel);
                    _lastWidget && widgets.push(_lastWidget);
                }
            }

            return widgets;
        },
        renderSubActions:function(item,where){

            var thiz=this;
            var items = item.getChildren ? item.getChildren() : item.items;

            _debug && console.log('render subs: ',item.command);
            if (items && items.length && !thiz.getVisibilityField(item,'widget')) {

                var menu = new Menu({parentMenu: where});
                thiz.setVisibilityField(item,'widget',menu);
                thiz._renderActions(items,menu);
                var widgetArgs = {
                    label:item.label,
                    popup:menu,
                    iconClass:item.icon ||'fa-magic'
                };

                utils.mixin(widgetArgs,thiz.getVisibilityField(item,'widgetArgs'));
                where.addChild(new PopupMenuItem(widgetArgs));
            }
        },
        _onDidCreateWidget:function(action,visibility,widget){


            return;
            /*
            if(action._store){
                widget._store = action._store;
            }

            var tooltip = action.tooltip;
            if(tooltip){

                //just a string
                if(utils.isString(action.tooltip)){
                    tooltip = {
                        content:tooltip
                    }
                }
                if(utils.isString(tooltip.content)){
                    tooltip.contentAsHTML=true;
                }
                tooltip.position = "right";
                tooltip.theme = 'tooltipster-light';

                $(widget.domNode).tooltipster(tooltip);
            }
            */
        },
        /**
         * Renders an action respectively to its settings, assumes that this.widgetClass is set to any Widget based module.
         * The actual created widget is being stored in action._visibility[this.visibilty].widget. This allows you
         * to access the widget from anywhere later. Example, in case the action belongs to a toolbar, the widget reference
         * is stored in action.visibility.ACTION_TOOLBAR_val.widget
         * @param action module:xaction/Action
         * @param where {dijit/_Widget} where to add the widget
         * @member
         * @private
         */
        _renderAction:function(action,where,widgetClass,showLabel){
            var thiz=this;


            if (action && action.show!==false){

                var actionVisibility = action.getVisibility !=null ? action.getVisibility(this.visibility) : {};
                //pre-rendered widget
                if (actionVisibility._widget) {

                    utils.addChild(where,actionVisibility._widget);
                    domClass.add(actionVisibility._widget.domNode,utils.replaceAll('/','-',action.command));
                    actionVisibility.widget=actionVisibility._widget;//for clean up
                    thiz._onDidCreateWidget(action,actionVisibility,actionVisibility._widget);
                    return;
                }

                //render only a widget for this action if not already done and the actions isn't a widget itself=dirty yet
                if (!actionVisibility.widget && !action.domNode) {

                    if (!action.label) {//huh, really? @TODO: get rid of label needs in action:render
                    } else {
                        actionVisibility.widget = thiz._addMenuItem(action,where,actionVisibility.widgetClass || widgetClass || thiz.widgetClass,showLabel);
                        actionVisibility.widget.visibility = thiz.visibility;
                    }
                }else{
                    _debug && console.log('have already rendered action',action);
                }
                return actionVisibility.widget;
            }
            return null;
        },
        /**
         * Add a drop down menu to another menu
         * @param item
         * @param where
         * @returns {*}
         * @private
         */
        _addLevel: function (item, where,patch) {

            item.menu = utils.addWidget(DropDownMenu, {
                cssClass:item.command + '',
                item:item
            }, this, null, true);

            if (item.attach) {
                this[item.attach] = item.menu;
            }

            //var _label = '{' + item.label[0] + '}' + item.label.substring(1);
            var _label = this.localize(item.label);
            item.popup = utils.addWidget(PopupMenuBarItem, {
                label: _label,
                popup: item.menu,
                accessKey: item.accessKey || item.label.toUpperCase()[0]
            }, this, where, true);

            patch!==false && utils.patchMenu(item.menu, types.EVENTS.ON_MAIN_MENU_OPEN, this, this, lang.hitch(this, this._onOpenMenu));
            return item;
        },
        /**
         * JQuery and Claro Theme fixer
         * @param button
         */
        addJQueryMenuClasses: function (button) {

            /*
            if (button && button.iconNode) {
                domClass.remove(button.iconNode, 'dijitReset');
                domClass.add(button.iconNode, 'actionToolbarButtonElusive');
            }
            */
        },
        /**
         * JQuery and Claro Theme fixer
         * @param button
         */
        fixButton: function (button) {


        },
        _destroyTop:function(top){
            _.each(top, function (level) {
                if (level.menu) {
                    utils.destroy([level.popup,level.menu],true,level);
                    this._clearItems(level.items);
                }
            },this);
        },
        /**
         * Util to destroy all widgets in the action model tree, recursively and per visibility only!
         *
         * @param actions {module:xaction/Action[]}
         * @private
         */
        _clearItems: function (actions) {

            var thiz=this;
            _.each(actions, function (action) {
                if(action) {
                    var actionVisibility = action.getVisibility !=null ? action.getVisibility(thiz.visibility) : {};
                    
                    action.destroy && action.destroy();
                    utils.destroy(actionVisibility.widget,true,actionVisibility);
                    
                    var _items = action.getChildren ? action.getChildren() : action.items;
                    action.items && thiz._clearItems(_items);
                }
            });
        },
        /**
         * Creates a menu item at given menu
         * @param item {xaction/Action}
         * @param where {dijit/Menu}
         * @returns {dijit/MenuItem|*}
         * @private
         */
        _addMenuItem : function (action, where,widgetClass,showLabel) {

            var thiz=this,
                visibility= action.getVisibility !=null ? action.getVisibility(this.visibility) : {};

            if(!visibility){
                visibility = {};
            }
            var label = showLabel == true ? action.label : visibility.label !=null ? visibility.label : action.label;
            if(showLabel == false){
                label = '';
            }

            var keyComboString =' \n';

            if(action.keyboardMappings) {

                var mappings = action.keyboardMappings;
                var keyCombos = _.pluck(mappings,['keys']);
                if(keyCombos && keyCombos[0]) {
                    keyCombos = keyCombos[0];

                    if(keyCombos.join) {
                        var keycombo = [];
                        keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                    }
                }

            }
            var widgetArgs = {
                item: action,
                iconClass: action.icon,
                label:this.localize(label),
                accelKey: action.accelKey || "",
                title:( this.localize(label) + keyComboString) || ""
            },
            _disabled = action.get ?  action.get('disabled') : false;

            action.shouldDisable && (_disabled = action.shouldDisable(null,null,thiz.visibility));

            _disabled!==null && (widgetArgs.disabled = _disabled);
            
            action.widgetArgs && utils.mixin(widgetArgs,action.widgetArgs);

            visibility.widgetArgs && utils.mixin(widgetArgs,visibility.widgetArgs);

            var menuItem = utils.addWidget(action.widgetClass || widgetClass || MenuItem,widgetArgs, thiz, where, true,null,[Reference]);
            domClass.add(menuItem.domNode,utils.replaceAll('/','-',action.command));
            thiz._onDidCreateWidget(action,visibility,menuItem);

            menuItem.item = action;

            //if(action.handle!==false) {
                menuItem.on('click', function (e) {

                    var _item = this.item,
                        owner = _item.owner;

                    _debug && console.log('run action ',_item.command);

                    return DefaultActions.defaultHandler.apply(owner,[_item,e]);
/*
                    owner && owner.onBeforeAction && owner.onBeforeAction(_item);

                    console.log('run action ',action.command);

                    //try action interface
                    if (_.isObject(_handler) && _.isFunction(_handler.runAction)) {
                        _handler.runAction(thiz.lastItem, _item, thiz);
                        _didRun = true;
                    }
                    else if (owner && owner.runAction) {
                    	//try owner
                        
                        //_item.owner.runAction.apply(_item.owner, [_item]);
                        
                        actionDfd = _item.owner.runAction(_item);
                        
                        _didRun = actionDfd !=null && actionDfd;
                        
                    }
                    //try callback function
                    else if (_.isFunction(_handler)) {
                        _item.handler.apply(this, [_item.operation || _item.command, _item.item, _item.owner, this, action]);
                        _didRun = true;
                    }

                    function after(dfdResult) {

                        if(owner){
		                        
		                        // call onAfterAction with this results
		                        var onAfterActionDfd = null;
		                        owner.onAfterAction && (onAfterActionDfd = owner.onAfterAction(_item,dfdResult));

                            owner._emit && owner._emit('onAfterAction',{
                                action:_item,
                                result:actionDfd,
                                source:thiz,
                                afterAction:onAfterActionDfd
                            });
                        }

                    }

                    if (!_didRun) {
                        console.warn('couldnt run action')
                        after();
                    }else{
                        if(actionDfd && actionDfd.then){
                            actionDfd.then(function(dfdResult){
                            	after(dfdResult);
                            });
                        }else{
                            after();
                        }
                    }
                    */
                });
            //}

            action._emit(this.visibility + '_WIDGET_CREATED',{
                parent:where,
                widget:menuItem,
                owner:thiz
            });


            action._emit('WIDGET_CREATED',{
                parent:where,
                widget:menuItem,
                visibility:this.visibility,
                owner:thiz
            });

            if(action.addReference)
            {
                action.addReference(menuItem, {
                    properties: {
                        "value": true,
                        "disabled":true
                    }
                }, true);
            }

            return menuItem;
        },
        /**
         * Fix focus bug in Dijit/Menu
         * @param menu
         */
        _focusMenu:function(menu){

            setTimeout(function() {
                menu.domNode.focus();
                var _children = menu.getChildren();
                if (!_.isEmpty(_children)){
                    _children[0].domNode.focus();
                }
            },1);
        },
        /**
         * Create a dijit context menu
         * @param targetWidget
         * @param onOpen
         * @param args
         * @returns {Menu}
         */
        createContextMenu:function(targetWidget,onOpen,args){

            try{
                var _menuArgs = null;

                if(targetWidget.id) {
                    _menuArgs = {
                        targetNodeIds: targetWidget.domNode ? [targetWidget.domNode] : [targetWidget.id]
                    };
                }else{
                    _menuArgs = {
                        targetNodeIds: targetWidget.domNode ? [targetWidget.domNode] : [targetWidget.id]
                    };
                }


                if(args){
                    lang.mixin(_menuArgs,args);
                }

                var menu = new Menu(_menuArgs);

                menu.startup();

                var thiz = this;

                menu.bindDomNode(targetWidget.domNode || targetWidget);

                connect.connect(menu, "_openMyself", this, function (e) {
                    try {
                        onOpen(menu,targetWidget);
                        targetWidget.resize();
                        var pop = dijit.popup;
                        pop._repositionAll();
                        thiz._focusMenu(menu);
                    } catch (e) {
                        logError(e,'crash in open'+e,e);
                    }
                });
                return menu;
            }catch(e){
                logError(e,'error creating context menu');
            }

        }
    });
});


