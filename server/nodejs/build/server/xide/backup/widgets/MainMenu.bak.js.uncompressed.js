/** @module xide/widgets/MainMenu **/
define("xide/backup/widgets/MainMenu.bak", [
    "dojo/_base/declare",
    "dojo/dom-construct",
    'dojo/dom-class',
    'dojo/has',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/ActionMixin",
    "xide/widgets/_MenuMixin",
    'xide/types',
    'xide/utils',
    'dojo/aspect',
    'dijit/Menu',
    'dijit/MenuItem',
    'dijit/MenuSeparator',
    'dijit/PopupMenuItem'
], function (declare, domConstruct, domClass, has, TemplatedWidgetBase, ActionMixin, _MenuMixin, types, utils, aspect, Menu, MenuItem, MenuSeparator, PopupMenuItem) {
    /**
     *
     *
     * @class xide/widgets/MainMenu
     *
     * @augments xide/widgets/_MenuMixin
     * @augments xide/mixins/EventedMixin
     * @augments xide/mixins/ReloadMixin
     * @augments xide/model/Base
     * @augments xide/widgets/TemplatedWidgetBase
     * @augments xide/mixins/ActionMixin
     */
    return declare("xide/widgets/MainMenu", [TemplatedWidgetBase, ActionMixin, _MenuMixin], {
        /**
         * The visibility filter. There an event "REGISTER_ACTION" this class is listening. Actions have a visibility
         * mask and this field will reject those actions which don't have this mask
         * @member visibility {module:xide/types/ACTION_VISIBILITY}
         */
        visibility: types.ACTION_VISIBILITY.MAIN_MENU,
        /**
         *
         */
        templateString: "<div><div attachTo='rootMenu' data-dojo-type='dijit/MenuBar' class='ui-widget ui-widget-content ui-widget-header'></div></div>",

        /**
         * The class being used to render an action.
         *
         * @type {dijit/_Widget}
         * @member
         */
        widgetClass: MenuItem,
        /**
         * Root level action widget, this is the menubar in our case
         */
        rootMenu: null,

        /**
         * Reference to the last incoming & new item actions
         */
        _newActions: null,
        /**
         * Top level layout
         */
        _actions: [
            {
                label: "File"
            },
            {
                label: "Edit"
            },
            {
                label: "View"
            },
            {
                label: "Themes",
                fixed: true,
                attach: 'wThemesMenu'
            },
            {
                label: "Help"
            }
        ],
        /**
         * Fixed menu reference
         */
        wThemesMenu: null,
        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Public interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        /**
         * 'setItemActions' does merges incoming actions in the menu tree. It won't render it. The
         * rendering takes place in a menu open callback ('this::_onOpenMenu')
         * @param item
         * @param actions
         */
        setItemActions: function (item, actions) {
            if (item == this.lastItem) {
                return;
            }
            this.lastItem = item;
            this.rootMenu.dirty = true;

            this._newActions = actions;
            //this._computeList(actions);//mix items into this._actions
            /*console.log('new actions',this._actions);*/
        },
        _fixMenu: function (menu) {
            if (menu._popupWrapper && menu._popupWrapper) {
                var dst = menu._popupWrapper;
                domClass.add(dst, 'ui-menu ui-widget ui-widget-content');
            }
        },
        _patchMenu: function (widget) {

            var thiz = this;
            aspect.after(widget, 'onOpen', function () {
                if (this._popupWrapper && this._popupWrapper) {
                    var dst = this._popupWrapper;
                    domClass.add(dst, 'ui-menu ui-widget ui-widget-content');
                }
            });
        },
        /**
         * after user opens a menu, walk over top level menu items, and
         * create/disable menu items if necessary, using the last selected item
         * @private
         */
        _onOpenMenu: function () {

            var thiz = this;
            /**
             * avoid re-creation of menu items
             */
            if (this.rootMenu.dirty !== true) {
                return;
            }
            this.rootMenu.dirty = false;

            this._fixMenu(this.rootMenu);

            /**
             * clear the top menu items
             * @param items
             * @private
             */
            var _clearTopMenu = function (items) {

                _.each(items, function (level) {

                    if (level.menu && level.fixed !== true) {

                        utils.empty(level.menu);

                        thiz._clearItems(level.items);

                    }
                });
            };

            var _attach = function (_items) {

                var _lastGroup = null;
                _.each(_items, function (level) {
                    var clear = true;

                    if (level.menu) {

                        if (level.items) {

                            for (var prop in level.items) {

                                var item = level.items[prop];
                                if (!item) {
                                    continue;
                                }

                                if (thiz.shouldShowAction(item) == false) {
                                    continue;
                                }

                                if (item.items && !thiz.getVisibilityField(item, 'menu')) {

                                    var menu = new Menu({parentMenu: level.menu});

                                    thiz.setVisibilityField(item, 'menu', menu);

                                    thiz._patchMenu(menu);

                                    thiz._renderActions(item.items, menu);

                                    level.menu.addChild(new PopupMenuItem({
                                        label: item.label,
                                        popup: menu,
                                        iconClass: item.icon || 'fa-magic'
                                    }));

                                    continue;
                                }


                                //the item has no menu yet and its not widget
                                if (item && item.show !== false && !item.items) {
                                    /**
                                     * Insert menu seperator as soon there is a 'group' and the group actually changed
                                     */
                                    if (!_.isEmpty(item.group) && item.group !== _lastGroup) {

                                        var children = level.menu.getChildren();
                                        if (children.length > 0 && children[children.length - 1].prototype != MenuSeparator.prototype) {
                                            utils.addWidget(MenuSeparator, {}, null, level.menu, true);
                                        }

                                        _lastGroup = item.group;
                                    }
                                    //item.menu = null;
                                    thiz._renderAction(item, level.menu);


                                }
                            }
                        }
                    }
                });

            }.bind(this);


            //remove all items
            _clearTopMenu(this._actions);
            //_destroyActions

            this._destroyActions(this._actions);

            this._computeList(this._newActions);//mix items into this._actions

            this._computeList(this._permanentActions, true);

            //create new menu items
            _attach(this._actions);

        },
        /**
         * Prepare the top level menu
         * @param data
         * @private
         */
        _buildTopLevel: function (data) {
            _.each(data || this._actions, function (item) {
                this._addLevel(item, this.rootMenu);
            }, this);
        },
        _createWidgets: function () {
            domConstruct.empty(this.rootMenu.containerNode);
            this._buildTopLevel();
        },
        onReloaded: function () {

            this._createWidgets();
        },
        startup: function () {
            this.inherited(arguments);
            this._createWidgets();
            this.subscribe(types.EVENTS.REGISTER_ACTION, this.onRegisterAction);
            this.rootMenu.dirty = true;
            this.setThemes();
        },
        destroy: function () {

            this._destroyTop(this._actions);
            utils.destroy([this.wThemesMenu, this.rootMenu], true, this);
            delete this['_actions'];
            this._actions = null;
            this._lastItem = null;
            this.inherited(arguments);
        },

        /**
         * old
         */
        setThemes: function () {

            // availableThemes[] is just a list of 'official' jQuery themes, you can use ?theme=String
            // for 'un-supported' themes, too. (eg: yours)
            var availableThemes = [
                {theme: "minimal"},
                {theme: "bootstrap"},
                {theme: "dflat"},
                {theme: "metro"},
                {theme: "dot-luv"},
                {theme: "smoothness"},
                {theme: "blitzer"},
                {theme: "cupertino"},
                {theme: "dark-hive"},
                {theme: "eggplant"},
                {theme: "excite-bike"},
                {theme: "flick"},
                {theme: "hot-sneaks"},
                {theme: "humanity"},
                {theme: "le-frog"},
                {theme: "overcast"},
                {theme: "pepper-grinder"},
                {theme: "redmond"},
                {theme: "south-street"},
                {theme: "start"},
                {theme: "sunny"},
                {theme: "swanky-purse"},
                {theme: "ui-darkness"},
                {theme: "ui-lightness"},
                {theme: "vader"}

            ];

            // Get current theme, a11y, and dir setting for page
            var curTheme = location.search.replace(/.*theme=([a-z...\-_/]+).*/, "$1") || "blitzer";
            var a11y = has("highcontrast") || /a11y=true/.test(location.search);
            var rtl = document.body.parentNode.dir == "rtl";

            function setUrl(theme, rtl, a11y) {
                var aParams = utils.getUrlArgs(location.href);
                // Function to reload page with specified theme, rtl, and a11y settings
                var _newUrl = "?theme=" + theme + (rtl ? "&dir=rtl" : "") + (a11y ? "&a11y=true" : "");
                if (aParams) {
                    _.each(aParams, function (value, key) {
                        if (key !== 'theme') {
                            _newUrl += '&' + key + '=' + value;
                        }
                    });
                }
                location.search = _newUrl

            }

            var thiz = this;

            availableThemes = _.sortBy(availableThemes, function (item) {
                return item.theme;
            });


            // Create menu choices to test other themes
            _.each(availableThemes, function (theme) {
                thiz.wThemesMenu.addChild(new MenuItem({
                    label: theme.theme,
                    group: "theme",
                    onClick: function () {
                        // Change theme, keep current a11y and rtl settings
                        setUrl(theme.theme, false, false);
                    }
                }));
            });

        },
        fixButton: function (button) {

            if (button && button.iconNode) {
                domClass.add(button.domNode, 'ui-menu-item');
                domClass.remove(button.iconNode, 'dijitReset');
                domClass.add(button.iconNode, 'actionToolbarButtonElusive');
            }
        }

    });
});


