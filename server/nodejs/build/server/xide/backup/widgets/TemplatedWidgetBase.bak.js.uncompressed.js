define("xide/backup/widgets/TemplatedWidgetBase.bak", [
    'dojo/_base/declare',

    'dijit/_Widget',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',

    'xide/mixins/EventedMixin',
    'xide/utils',
    'xide/widgets/_Widget',
    'xide/_base/_Widget'

], function (declare, _Widget, _TemplatedMixin, _WidgetsInTemplateMixin, EventedMixin,utils,_XWidget) {

    return declare("xide.widgets.TemplatedWidgetBase", [_Widget, _XWidget,_TemplatedMixin, _WidgetsInTemplateMixin, EventedMixin], {
        data: null,
        delegate: null,
        didLoad: false,
        templateString: null,
        getParent:function(){
            return this._parent;
        },
        debounce:function(methodName,_function,delay,options,now){
          return utils.debounce(this,methodName,_function,delay,options,now);
        },
        translate: function (value) {
            return value;

            /*
            if (this._messages == null || this._messages.length == 0) {
                this._setupTranslations();
            }
            if (value) {
                var _titleKey = value.toLocaleLowerCase();
                _titleKey = _titleKey.replace(/\s+/g, "_");
                if (_titleKey && this._messages != null) {
                    var _titleValue = this._messages[_titleKey];
                    if (_titleValue && _titleValue.length > 0) {
                        return _titleValue;
                    }
                }
            }
            return value || '';*/
        },
        _setupTranslations: function () {
            this._messages = [];
            /*
            var dstCtx = this.ctx;

            if (dstCtx && dstCtx.getLocals) {
                var l = dstCtx.getValidLanguage();
                this._messages = dstCtx.getLocals("xide.widgets", "TemplatedWidgetBase");
            } else {
                this._messages = [];
            }
            */
        },
        updateTitleNode: function (value) {
            if (value && this.titleNode && this._messages) {
                var _titleKey = value.toLocaleLowerCase();
                _titleKey = _titleKey.replace(/\s+/g, "_");
                if (_titleKey) {
                    var _titleValue = this._messages[_titleKey];
                    if (_titleValue) {
                        this.titleNode.innerHTML = _titleValue;
                    }
                }
            }
        }
    });
});