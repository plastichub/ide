define("xide/backup/widgets/ToolTipMixin", [
    "dojo/_base/declare",
    'xide/utils'
], function (declare, utils) {
    return declare("xide.widgets.ToolTipMixin", null, {
        ttMaxCount: 3,
        initToolTip: function (text, maxCount, delay, target) {
            return;
            if (!text) {
                return;
            }
            var str = text.replace(/\n/g, '<br/>');
            if (maxCount != null) {
                this.ttMaxCount = maxCount;
            }
            if (str && utils.isValidString(str)) {
                this['tt'] = new Tooltip({
                    innerHTML: str,
                    showDelay: delay != null ? delay : 500
                });
                this['tt'].startup();
                this['tt'].addTarget(target || this.getTooltipNode ? this.getTooltipNode() : this.domNode);
            }
        }
    });
});
