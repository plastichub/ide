/** @module xgrid/Base **/
define("xide/backup/widgets/ActionToolbar", [
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/registry',
    'xide/_base/_Widget',
    "xide/mixins/ActionMixin",
    'xaction/ActionContext',
    'xaction/DefaultActions',
    "xide/model/Path",
    'xaction/Action',
    'xlang/i18',
    "xide/widgets/_MenuMixin4",
    "xide/_Popup",
    "xide/widgets/ActionToolbar4"
], function (dcl,types,utils,registry,_XWidget,
             ActionMixin,ActionContext,DefaultActions,
             Path,Action,i18,
             MenuMixinClass,_Popup,ActionToolbar4){

    return ActionToolbar4;

    var OPEN_DELAY = 200;

    function correctPosition(_root,$sub){
        var level = 0;
        var _levelAttr = $sub.attr('level');
        if(!_levelAttr){
            _levelAttr = parseInt($sub.parent().attr('level'),10);
        }

        if(_levelAttr===0){
            $sub.css({
                left:0
            });
        }
        var parent = _root.parent();
        var parentTopAbs = parent.offset().top;
        var rootOffset = _root.offset();
        var rootPos = _root.position();
        var rootHeight = _root.height();
        var parentDelta = parentTopAbs - rootOffset.top;
        var newTop = (rootPos.top + rootHeight) + parentDelta;
        var offset = -20;
        if(_levelAttr!==0) {
            newTop += offset;
        }
        $sub.css({
            top:newTop
        });
        var autoH = $sub.height() + 0;
        var totalH = $('html').height();
        var pos = $sub.offset();
        var overlapYDown = totalH - (pos.top + autoH);
        if((pos.top + autoH) > totalH){
            $sub.css({
                top: overlapYDown - 30
            });
        }
        ////////////////////////////////////////////////////////////
        var subWidth = $sub.width(),
            subLeft = $sub.offset().left,
            collision = (subWidth+subLeft) > window.innerWidth;

        if(collision){
            $sub.addClass('drop-left');
        }
    }

    var ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        templateString:'<div attachTo="navigation" class="actionToolbar">'+
        '<nav attachTo="navigationRoot" class="" role="navigation">'+
            '<ul attachTo="navBar" class="nav navbar-nav"/>'+
        '</nav>'+
        '</div>'
    });
    /**
     * @class module:xide/widgets/MainMenuActionRenderer
     */
    var ActionRendererClass = dcl(null,{
        renderTopLevel:function(name,where){
            where = where || $(this.getRootContainer());
            var item =$('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) +'<b class="caret"></b></a>'+
                '</li>');

            where.append(item);
            return item;
        },
        getRootContainer:function(){
            return this.navBar;
        }
    });
    var Module = dcl([ContainerClass,MenuMixinClass,ActionRendererClass,_XWidget.StoreMixin],{
        declaredClass:'xide.widgets.Actiontoolbar',
        target:null,
        attachToGlobal:true,
        _isFollowing:false,
        _followTimer:null,
        _zIndex:1,
        hideSubsFirst:true,
        visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
        init2: function(opts){
            var options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root,'click',null,function(e){
                if(!self.isOpen){
                    return;
                }
                self.isOpen=false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display:''
                }).find('.drop-left').removeClass('drop-left');
            });

            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            var _root2 = this.$navigationRoot;
            this.__on(_root2,'mouseleave', '.dropdown-submenu', function(e){
                var _root = $(e.currentTarget);
                var $sub = _root.find('.dropdown-context-sub:first');
                if (self.menu) {
                    if (!$.contains(self.menu[0], _root[0])) {
                        return;
                    }
                }
                $sub.css('display','none');
                $sub.data('left',true);
                clearTimeout($sub.data('openTimer'));
            });
            function onOpened($sub){
                /*
                 var desc = ' li:not(.divider):visible a'
                 //var $items = $sub.find('[role="menu"]' + desc + ', [role="listbox"]' + desc);
                 var $items = $sub.find(desc);
                 //console.log('items',$items);
                 if($items.length){
                 $($items[0]).focus();
                 }
                 */
            }

            this.__on(_root2,'mouseenter', '.dropdown-submenu', function(e){

                var _root = $(e.currentTarget);
                if (self.menu) {
                    if (!$.contains(self.menu[0], _root[0])) {
                        return;
                    }
                }
                var $sub = _root.find('.dropdown-context-sub:first');

                $sub.css('display', 'none');
                if(self.correctSubMenu!==false){
                    correctPosition(_root,$sub);
                }
                $sub.data('left',false);
                //var data = $sub.data('item');
                $sub.css('z-index',_Popup.nextZ());
                //if(data){
                //    console.log('on open '+data.command,data)
                //}
                /*
                if(data && data.subMenu && data.lazy){
                    data.lazy = false;
                    $sub.data('item',data);
                    var items = data.subMenu;
                    _.each(items,function(item){
                        if(item.lazy){
                            item.lazy=false;
                            item._render();
                        }
                    })
                }
                */

                clearTimeout($sub.data('openTimer'));
                $sub.data('openTimer',setTimeout(function(){
                    if($sub.data('left')!==true) {
                        $sub.css('display', 'block');
                        if(self.correctSubMenu!==false){
                            correctPosition(_root,$sub);
                        }

                    }else{
                        $sub.css('display', 'none');
                    }
                },OPEN_DELAY));

            });
        },
        resize:function(){
            utils.resizeTo(this.navigation,this.navBar,true,false);
        },
        destroy:function(){
            utils.destroy(this.$navBar[0]);
            utils.destroy(this.$navigation[0]);
            clearTimeout(this._followTimer);
        },
        buildMenu:function (data, id, subMenu,update) {
            var subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ',
                menuString = '<ul aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>',
                $menu = update ? (this._rootMenu || this.$navBar || $(menuString)) : $(menuString);

            if(!subMenu){
                this._rootMenu = $menu;
            }
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        setActionStore: function (store, owner,subscribe,update,itemActions) {
            if(!update && store && this.store && store!=this.store){
                this.removeCustomActions();
            }

            if(!update){
                this._clear();
                this.addActionStore(store);
            }

            if(!store){
                return;
            }

            this.store = store;

            var self = this,
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            var tree = update ? self.lastTree : self.buildActionTree(store,owner);

            var allActions = tree.allActions,
                rootActions = tree.rootActions,
                allActionPaths = tree.allActionPaths,
                oldMenuData = self.menuData;

            if(subscribe!==false) {
                if(!this['_handleAdded_' + store.id]) {
                    this.addHandle('added', store._on('onActionsAdded', function (actions) {
                        self.onActionAdded(actions);
                    }));

                    this.addHandle('delete', store.on('delete', function (evt) {
                        self.onActionRemoved(evt);
                    }));
                    this['_handleAdded_' + store.id]=true;
                }
            }

            // final menu data
            var data = [];
            if(!update) {
                _.each(tree.root, function (menuActions, level) {
                    var lastGroup = '';
                    _.each(menuActions, function (command) {
                        var action = self.getAction(command, store);
                        var isDynamicAction = false;
                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }
                        if (action) {

                            var renderData = self.getActionData(action),
                                icon = renderData.icon,
                                label = renderData.label,
                                visibility = renderData.visibility,
                                group = renderData.group,
                                lastHeader = {
                                    header: ''
                                };


                            if (visibility === false) {
                                return;
                            }

/*
                            if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 1) {
                                //if (lastGroup !== group) {
                                    var name = groupedActions[group].length >= 2 ? i18.localize(group) : "";
                                    lastHeader = {divider: name,vertial:true};
                                    data.push(lastHeader);
                                    lastGroup = group;
                                //}
                            }
*/

                            var item = self.toMenuItem(action, owner, '', icon, visibility || {}, false);
                            data.push(item);
                            item.level = 0;
                            visibility.widget = item;

                            self.addReference(action, item);

                            var childPaths = new Path(command).getChildren(allActionPaths, false),
                                isContainer = childPaths.length > 0;

                            function parseChildren(command, parent) {
                                var childPaths = new Path(command).getChildren(allActionPaths, false),
                                    isContainer = childPaths.length > 0,
                                    childActions = isContainer ? self.toActions(childPaths, store) : null;
                                if (childActions) {
                                    var subs = [];
                                    _.each(childActions, function (child) {
                                        var _renderData = self.getActionData(child);
                                        var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility,false);
                                        var parentLevel = parent.level || 0;
                                        _item.level = parentLevel + 1;
                                        self.addReference(child, _item);
                                        subs.push(_item);
                                        var _childPaths = new Path(child.command).getChildren(allActionPaths, false),
                                            _isContainer = _childPaths.length > 0;
                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }

                            parseChildren(command, item);

                            self.buildMenuItems(rootContainer, [item], "-" + new Date().getTime());

                        }
                    });
                });
                self.onDidRenderActions(store, owner);
                this.menuData = data;
            }else{
                if(itemActions || !_.isEmpty(itemActions)) {

                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            var action = self.getAction(newAction.command);
                            if (action){


                                var renderData = self.getActionData(action),
                                    icon = renderData.icon,
                                    label = renderData.label,
                                    aVisibility = renderData.visibility,
                                    group = renderData.group,
                                    item = self.toMenuItem(action, owner, label, icon, aVisibility || {},null,false);

                                if(aVisibility.widget){
                                    return;
                                }

                                aVisibility.widget = item;

                                self.addReference(newAction, item);

                                if(!action.getParentCommand){
                                    return;
                                }
                                var parentCommand = action.getParentCommand();
                                var parent = self._findParentData(oldMenuData,parentCommand);
                                if(parent && parent.subMenu){
                                    parent.lazy = true;
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });
                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }

            var parent = $(this.domNode).parent();
            if(parent[0] && parent[0].id!=='staticTopContainer') {
                this._height = this.$navigation.height();
                if (this._height) {
                    parent.css({
                        height: this._height
                    });
                }
            }
            this.resize();
        },
        _follow:function(){
            clearTimeout(this._followTimer);
            this._isFollowing=true;
            this._zIndex =_Popup.nextZ();
            this._zIndex +=2;
            var parent = $(this.domNode).parent();
            var node = $('#staticTopContainer');
            if (!node[0]) {
                var body = $('body');
                node = $('<div id="staticTopContainer" class=""></div>');
                body.prepend(node);
            }
            var what = this.$navigation,
                heightSource = this.$navBar,
                self = this;

            node.append(what);
            function follow(){
                var offset = parent.offset(),
                    currentHeight = what.height();

                if (parent.offset().top === 0 || parent.offset().left === 0 && self._hide!==false) {
                    what.css({
                        display: 'none'
                    });
                    return;
                }
                what.css({
                    top: offset.top,
                    left: offset.left,
                    position: 'absolute',
                    zIndex: self._zIndex,
                    display: 'inherit'
                });

                if (currentHeight) {
                    parent.css({
                        height: currentHeight
                    });
                    utils.resizeTo(what[0], heightSource[0], true, null);
                    utils.resizeTo(what[0], parent[0], null, true);
                    self._height = currentHeight;
                }
            }

            follow();
            this._followTimer = setInterval(function () {
                follow();
            }, 1000);
        },
        _unfollow:function(newParent){
            clearTimeout(this._followTimer);
            this._isFollowing=false;
            var what = this.$navigation;
            $(newParent || this.domNode).append(what);
            what.css({
                top: 0,
                left: 0,
                position: 'relative'
            });
        },
        startup:function(){
            this.correctSubMenu = true;
            this.init2({
                preventDoubleContext: false
            });
            this.menu = this.$navigation;
            this._height = this.$navigation.height();
            if(this.attachToGlobal) {
                this._follow();
                this.correctSubMenu = true;
            }
        }
    });
    dcl.chainAfter(Module,'destroy');
    return Module;
});

