define("xide/backup/widgets/JSONEditorWidget.js.bak", [
    'dcl/dcl',
    'dojo/_base/declare',
    'dojo/_base/connect',
    'dojo/dom-class',
    'dojo/_base/lang',
    'xide/widgets/WidgetBase',
    //'xide/views/TextEditor',
    'xide/utils'

], function (dcl,declare, connect, domClass, lang, WidgetBase, utils) {
    return dcl(WidgetBase, {
        declaredClass:"xide.widgets.JSONEditorWidget",
        didLoad: false,
        minHeight: "525px",
        editorHeight: "470px",
        jsonEditorHeight: "270px",
        aceEditorHeight: "200px",
        newLabel: "New Entry",
        newEntryTemplate: "",
        wNew: null,
        initialTemplate: [],
        wSaveButton: null,
        showACEEditor: true,
        aceEditorOptions: null,
        aceEditor: null,
        aceNode: null,
        wBorderContainer: null,
        wJSONPane: null,
        wACEPane: null,
        wToAceButton: null,
        wFromAceButton: null,
        hasSave: true,
        hasNew: false,
        templateString: "<div class='jsonEditorWidgetContainer' style='width: 100%;height:${!minHeight};'><div attachTo='wBorderContainer' data-dojo-type='xide.layout.BorderContainer' region='center' data-dojo-props=\"design:'sidebar'\" class='' style='width:100%;height:${!minHeight};'>" +
        "<div style='height:30px;'>" +
        "<div class='jsonEditorToolbar' attachTo='menuBar' data-dojo-type='dijit.MenuBar' data-dojo-props=\"region:'top'\">" +
        "<div data-dojo-type='dijit.PopupMenuBarItem' attachTo='menu' style='float: left;' disabled='false'>" +
        "<span style='color: #000000; !important'>New</span>" +
        "<div data-dojo-type='dijit.Menu' style='display: none;'>" +
        "<div data-dojo-type='dijit.MenuItem' attachTo='wNew'>${!newLabel}</div>" +
        "</div>" +
        "</div>" +
        "<div attachTo='wSaveButton' class='scriptEditorReloadButton' data-dojo-type='dijit.form.ToggleButton' data-dojo-props=\"iconClass:'',showLabel:true\">Save</div>" +
        "<div data-dojo-type='dijit.MenuSeparator' style='float: left;width: 1px;height: 27px;background-color: #969696;margin-right: 15px;margin-left: 15px;'></div>" +
        "<div attachTo='wToAceButton' class='' data-dojo-type='dijit.form.ToggleButton' data-dojo-props=\"iconClass:'el-icon-chevron-down',showLabel:false\"></div>" +
        "<div attachTo='wFromAceButton' class='' data-dojo-type='dijit.form.ToggleButton' data-dojo-props=\"iconClass:'el-icon-chevron-up',showLabel:false\"></div>" +
        "</div>" +
        "</div>" +
        "<div attachTo='wJSONPane' style='height:50%;width: 100%;padding: 0px;overflow: hidden;margin:0px;' toggleSplitterFullSize='100%' toggleSplitterCollapsedSize='100px' toggleSplitterState='open' data-dojo-type='dijit.layout.ContentPane' splitter='true' data-dojo-props=\"region:'center',splitter:true\">" +
        "<div valign='middle' class='jsonEditorWidget' attachTo='valueNode' style='height:inherit;'></div>" +
        "</div>" +
        "<div attachTo='wACEPane' style='height:50%;width: 100%;padding: 0px;overflow: hidden;' data-dojo-type='dijit.layout.ContentPane' splitter='true' data-dojo-props=\"region:'bottom',splitter:true\">" +
        "<div valign='middle' class='jsonAceEditorWidget' attachTo='aceNode' style='height:100%;padding: 0px;'></div>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>",
        editor: null,
        getValue: function () {
            if (this.editor) {
                var json = this.editor.get();
                var val = JSON.stringify(json);
                return val;
            }

            return this.inherited(arguments);
        },
        resize: function () {

            this.inherited(arguments);
            this.wBorderContainer.resize();
            var thiz = this;
            try {
                if (this.showACEEditor && !this.aceEditor) {
                    this._createACEEditor(this.aceNode);
                }
            } catch (e) {
                console.error('constructing ace editor widget failed : ' + e.message);

            }
            thiz.wJSONPane.resize();
            thiz.wACEPane.resize();
            thiz.wBorderContainer.resize();
        },
        setupNativeWidgetHandler: function () {
            if (this.nativeWidget) {
                var thiz = this;
                connect.connect(this.nativeWidget, "onChange", function (item) {
                    if (thiz['isSaving']) {
                        // return;
                    }
                    thiz['isSaving'] = true;
                    thiz.widgetChanged(this);
                });
            }
        },
        _createJSONEditor: function (parentNode,options) {
            this.editor = new JSONEditor(options,parentNode);
            this.editor.startup()
        },
        appendNewTemplatedEntry: function () {
            var json = this.editor.get();

            if (!lang.isArray(json)) {
                lang.mixin(json, {}, this.newEntryTemplate);
            } else {
                json.push(this.newEntryTemplate);
            }
            this.editor.set(json);
        },
        onFromAce: function () {

            if (this.aceEditor) {
                var value = this.aceEditor.get("value");
                this.editor.set(dojo.fromJson(value));
            }
        },
        onToAce: function () {
            var json = this.editor.get();
            var val = JSON.stringify(json, null, 2);
            if (this.aceEditor) {
                this.aceEditor.set("value", val);
            }
        },
        onSave: function (value) {

        },
        setupEventHandlers: function () {
            var thiz = this;

            if (this.wNew) {
                connect.connect(this.wNew, "onClick", function (item) {
                    if (thiz.newEntryTemplate) {
                        thiz.appendNewTemplatedEntry(thiz.newEntryTemplate);
                    }
                });
            }
            if (this.wSaveButton) {
                connect.connect(this.wSaveButton, "onClick", function (item) {
                    var json = thiz.editor.get();
                    thiz.setValue(JSON.stringify(json));
                    thiz.onSave(JSON.stringify(json));
                });
            }


            if (this.wFromAceButton) {
                connect.connect(this.wFromAceButton, "onClick", function (item) {
                    thiz.onFromAce();
                });
            }

            if (this.wToAceButton) {
                connect.connect(this.wToAceButton, "onClick", function (item) {
                    thiz.onToAce();
                });
            }
        },
        _createACEEditor: function (parentNode) {

            var _options = {
                region: "center",
                value: '',//this.getValue(),
                style: "margin: 0; padding: 0; position:relative;overflow: auto;height:inherit;width:inherit;",
                mode: 'javascript',
                readOnly: false,
                tabSize: 2,
                softTabs: false,
                wordWrap: false,
                showPrintMargin: false,
                highlightActiveLine: false,
                fontSize: '13px',
                showGutter: true,
                className: 'editor-ace ace_editor ace-codebox-dark ace_dark'
            };

            var editorPane = new TextEditor(_options, dojo.doc.createElement('div'));

            parentNode.appendChild(editorPane.domNode);
            editorPane.startup();
            this.aceEditor = editorPane;
            this.aceEditor.setOptions();
        },
        startup: function () {



            if (!this.hasNew) {
                this.wNew = null;
                utils.destroy(this.wNew);
            }
            if (!this.hasSave) {
                this.wSaveButton = null;
                utils.destroy(this.wSaveButton);
            }

            domClass.remove(this.wFromAceButton.iconNode, 'dijitReset');
            domClass.add(this.wFromAceButton, 'actionToolbarButtonElusive');

            domClass.remove(this.wToAceButton.iconNode, 'dijitReset');
            domClass.add(this.wToAceButton, 'actionToolbarButtonElusive');



            try {
                if (!this.editor) {
                    var pdiv = dojo.doc.createElement('div');

                    this.valueNode.appendChild(pdiv);
                    this._createJSONEditor(pdiv);
                    this._createACEEditor(this.aceNode);
                }
            } catch (e) {
                console.error('constructing json editor widget failed : ' + e.message);
            }

            var jsonStr = utils.toString(this.valueStr);
            if (!jsonStr || jsonStr.length == 0) {
                jsonStr = '[]';
            }

            this.valueStr = jsonStr;

            if (this.editor && this.valueStr) {

                if (this.valueStr != 'Unset') {

                    try {

                        var _json = dojo.fromJson(jsonStr);
                        this.editor.set(_json);
                    } catch (e) {

                        if (jsonStr.indexOf(',') != -1) {
                            var _splitted = jsonStr.split(',');
                            this.editor.set(_splitted);
                        } else {
                            this.editor.set(dojo.fromJson('[]'));
                        }
                    }

                } else {
                    this.editor.set(this.initialTemplate);
                }

                this.editor.expandAll();
            }
            this.setupEventHandlers();
        }
    });
});