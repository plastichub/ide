/** @module xide/widgets/ActionToolbar **/
define("xide/backup/widgets/ActionToolbar2", [
    "xdojo/declare",
    'dijit/form/DropDownButton',
    'xide/widgets/ActionToolbarButton',
    'dijit/Menu',
    'dijit/MenuItem',
    'dijit/Toolbar',
    'dijit/ToolbarSeparator',
    "xide/mixins/ActionMixin",
    "xide/mixins/EventedMixin",
    "xide/widgets/_MenuMixin",
    "xide/types",
    "xide/utils",
    'xaction/ActionContext',
    'xide/data/Reference',
    'xaction/Action',
    'xide/model/Path'

], function (declare,DropDownButton,ActionToolbarButton,Menu,MenuItem,Toolbar,ToolbarSeparator,ActionMixin,EventedMixin,_MenuMixin,types, utils,ActionContext,Reference,Action,Path){

    var debug = true;

    /**
     * Widget
     * @class module:xide/widgets/ActionToolbar
     */
    return declare("xide/widgets/ActionToolbar", [Toolbar,ActionContext, ActionMixin,EventedMixin,_MenuMixin], {

        /**
         * Set visibility filter
         */
        visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
        /**
         * The separator class for separating actions
         */
        separatorClass:ToolbarSeparator,
        /**
         * The class being used to render an action.
         *
         * @type {dijit/_Widget}
         */
        widgetClass:ActionToolbarButton,
        getItemsAtBranch:function(items, path) {
            return new Path(path).getChildren(_.pluck(items,'command'),false);
        },
        /**
         *
         * @param action
         * @param where
         * @param widgetClass
         * @param showLabel
         * @returns {*}
         */
        renderAction: function (action, where, widgetClass, showLabel,actions) {

            /**
             * Collect variables
             */




            var thiz = this,

                parentAction = action.getParent() /*|| new Action({command:action.getParentCommand()}))*/,
                //parentActionItems = this.getItemsAtBranch(actions,parentAction.command),
                _items = action.getChildren(),
                _hasItems = _items && _items.length,
                //_hasItems = parentActionItems && parentActionItems.length,
                isContainer = _hasItems,
                _expand = thiz.getVisibilityField(action, 'expand') == true,
                _forceSub = action.forceSubs,
                widget = thiz.getVisibilityField(action, 'widget'),

                parentWidget = parentAction ? thiz.getVisibilityField(parentAction, 'widget') : null,
                actionVisibility = action.getVisibility(this.visibility),
                customWidget  = actionVisibility.widgetClass,
                isTop = action.command.split('/').length < 1,
                renderLabel = false;


            if(parentAction || parentWidget){
                //renderLabel = true;
            }

            if(action.command ==='View/Show/Toolbar'){
                //console.log('### toolbar : ' + parentAction.command);
            }



            /**
             * Cleanup variables, sanitize
             */

            //case when visibility is not an object, upgrade visibility to an object
            if (!_.isObject(actionVisibility) && action.setVisibility) {
                actionVisibility = {};
                action.setVisibility(this.visibility, actionVisibility);
            }

            //more variables
            var label = renderLabel ? actionVisibility.label || action.label : '',
                widgetArgs = {
                    iconClass: action.icon,
                    label: label,
                    item: action
                };


            if (actionVisibility.widgetArgs) {
                utils.mixin(widgetArgs, actionVisibility.widgetArgs);
            }

            //console.log('##############33   render ' + action.command + ' label = ' +label);

            /**
             * Determine widget class to use
             */
            widgetClass =

                //override
                actionVisibility.widgetClass ||
                    //argument
                widgetClass ||
                    //visibility's default
                thiz.widgetClass;


            if(action.command ==='File/Open In/Default Editor'){
                //console.log('render def',[action,actionVisibility,label]);
            }

            //debug && console.log('render action '+ action.command + ' with ' + widgetClass.prototype.declaredClass);


            // case one: isContainer = true



            if (!widget) {

                if (isContainer) {

                    if (_expand) {
                        return null;
                    }
                    if (!parentWidget) {


                        var menu = utils.addWidget(DropDownButton, widgetArgs, this, where, true, null, [Reference]);

                        var pSubMenu = utils.addWidget(Menu, {
                            item: action
                        }, this, null, true);

                        menu.dropDown = pSubMenu;
                        actionVisibility.widget = pSubMenu;

                        thiz._publishActionWidget(menu,action,where);

                        return menu;

                    } else {

                        //parent widget there

                        var pSubMenu = new Menu({parentMenu: parentWidget});

                        console.error('render sub!');

                        var popup = new dijit.PopupMenuItem({
                            label: label,
                            popup: pSubMenu,
                            iconClass: action.icon
                        });

                        parentWidget.addChild(popup);

                        actionVisibility.widget = pSubMenu;
                        return pSubMenu;

                    }
                }


                if (parentWidget) {

                    where = parentWidget;
                    widgetClass = customWidget ? widgetClass : MenuItem;
                }

            } else {
                console.log('widget already rendered!');
            }

            if (!actionVisibility.widget && !action.domNode) {

                if (!action.label) {
                    //huh, really? @TODO: get rid of label needs in action:render
                } else {
                    actionVisibility.widget = thiz._addMenuItem(action, where, widgetClass, showLabel);
                }
            } else {
                console.log('have already rendered action', action);
            }

            return actionVisibility.widget;

        },
        /**
         * Renders a set of actions into is appropriate widgets. This utilizes this._renderAction and
         * puts the action's widget into its visibility store for global access.
         *
         * @param actions {xaction/Action[]}
         * @param where {dijit/_Widget}
         * @param widgetClass {Module}
         * @private
         */
        _renderActions: function (actions, where, widgetClass, showLabel, separatorClass, force) {




            //track the last action group for adding a separator
            var _lastGroup = null,
                thiz = this,
                widgets = [],
                _lastWidget;

            if (!actions) {
                console.error('strange, have no actions!');
                return;
            }

            for (var i = 0; i < actions.length; i++) {

                var action = actions[i],
                    sAction = action._store ? action._store.getSync(action.command) : null;

                if (sAction && sAction != action) {
                    console.log('weird!');
                }
                if (sAction) {
                    action = sAction;
                }

                if (!action) {
                    console.error('invalid action!');
                    continue;
                }


                //pick command group
                if (action.group && _lastGroup !== action.group) {

                    if(_lastGroup) {
                        utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true);
                    }
                    _lastGroup = action.group;
                }

                //skip if action[visibility].show is false
                if (thiz.getVisibilityField(action, 'show') === false) {
                    continue;
                }

                var _items = action.getChildren ? action.getChildren() : action.items,
                    _hasItems = _items && _items.length,
                    _expand = thiz.getVisibilityField(action, 'expand'),
                    _forceSub = action.forceSubs;


                force = force != null ? force : this.forceRenderSubActions;


                _lastWidget = thiz.renderAction(action, where, widgetClass, showLabel,actions);

            }

            return widgets;
        },
        /**
         * Set action store
         * @param store {ActionStore}
         */
        setActionStore: function (store) {

            this.store = store;

            var self = this,
                allActions = store.query(),

            //return all actions with non-empty tab field
                tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
                groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),

            //now flatten them
                _actionsFlattened = [];


            _.each(groupedTabs,function(items,name){
                _actionsFlattened = _actionsFlattened.concat(items);
            });


            this._renderActions(_actionsFlattened,this,null,null,null,null);

        },
        /**
         *
         */
        clear:function(){
            //_destroyActions
            this._destroyActions(this.visibility);
        },
        /**
         *
         * @param visibility
         * @private
         */
        _destroyActions: function (visibility) {

            var actions = this.getActionStore().query();

            _.each(actions,function(action){

                var actionVisibility = action.getVisibility!= null ? action.getVisibility(visibility) : null;
                if(actionVisibility){
                    var widget = actionVisibility.widget;
                    if(widget){
                        //remove action reference widget
                        action.removeReference && action.removeReference(widget);
                        widget.destroy();
                        this.setVisibilityField(action, 'widget', null);
                    }
                }
            },this);
        },
        destroy:function(){

            //_destroyActions
            this._destroyActions(this.visibility);

            this.inherited(arguments);
        },
        check:function(){
            if(!this.domNode || this._destroyed){
                console.warn('@todo: ActionToolbar::check::orphan!');
                this.destroy();
                return false;
            }
            return true;
        }
    });
});