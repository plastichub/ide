/** @mixin xide/mixins/FlagValueMixin **/
define("xide/mixins/FlagValueMixin", [
    "dojo/_base/declare",
    "dojo/_base/lang",
    'xide/types',
    'xide/factory'
],function (declare,lang,types,factory) {
    /**
     * Adds functions for setting
     * @mixin module:xide/mixins/FlagValueMixin
     */
    return declare(null, {

            _ENUM_NAME:"_NO_NAME",

            _ENUM_VALUE:null,
            /**
             * Utils to create a visibilty
             * @type {function}
             * @returns {module:xide/types/ACTION_VISIBILITY}
             */
            factory: function () {
                //This clone types.ACTION_VISIBILITY and blends in an integer mask
                const _in = arguments[1] || lang.clone(this._ENUM_NAME);

                const _args = arguments;

                /**
                 * A modus when we have the arguments like (1,1,1,2).

                 */
                if(_args[0].length>0 &&  _.isNumber(_args[0][0])) {
                    const _FlagArgs = _args[0];
                    const _val  = null;
                    let _index=0;

                    _.each(_in, function (index, prop) {
                        if (typeof _in[prop] !== 'function') {
                            if (_index < _FlagArgs.length) {
                                //set the value per key but preserve the actualy key by storing
                                //the value in a new key_val field
                                _in[prop+'_val'] = _FlagArgs[_index];
                            }
                        }
                        _index++;
                    });
                }
                /**
                 * A modus when we have the arguments like (MAIN_MENU,something). This will update
                 * an existing visibilty
                 */
                if(_.isString(_args[0][0])){
                    _in[_args[0][0]+'_val']=_args[0][1];
                    return _args[1];
                }
                return _in;
            }

        });
    });