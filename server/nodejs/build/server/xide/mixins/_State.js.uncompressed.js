/** @module xide/mixins/_State**/
define("xide/mixins/_State", [
    "xdojo/declare"
], function (declare) {
    /**
     * Generic state implementation
     * @class module:xide/mixins/_State
     */
    return declare("xide/mixins/_State",null, {
        getState:function(){
            return this.inherited(arguments) || {};
        },
        setState:function(state){
            return this.inherited(arguments);
        }
    });
});