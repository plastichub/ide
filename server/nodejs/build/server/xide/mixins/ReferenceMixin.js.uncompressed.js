define("xide/mixins/ReferenceMixin", [
    'dcl/dcl',
    "dojo/_base/declare",
    'xide/types',
    'xide/utils',
    'xide/registry'
], function (dcl, declare, types, utils, registry) {
    /**
     *
     * @param queryString
     * @param startNode
     * @param single
     * @returns {*}
     */
    utils.find = function (queryString, startNode, single) {
        const nodes = $(startNode).find(queryString);
        if (nodes && nodes.length > 0) {
            return single === false ? nodes : nodes[0];
        }
        return null;
    };

    /**
     *
     * @param scope
     * @param id
     * @returns {*}
     */
    function getElement(scope, id) {
        const dst = scope.document.getElementById(id);
        if (dst) {
            return dst;
        }
        return null;
    }

    const Implementation = {
        _targetReference: null,
        getTarget: function () {
            return this._targetReference || this.inherited(arguments);
        },
        skipWidgetCSSClasses: [
            'dijitButtonHover',
            'dijitHover',
            'dijit',
            'dijitInline',
            'dijitReset',
            'dijitCheckBoxChecked',
            'dijitChecked',
            'dijitLeft'
        ],
        _cssClassesToQuery: function (string) {
            let result = '';
            if (string) {
                const els = string.split(' ');
                for (let i = 0; i < els.length; i++) {

                    if (utils.contains(this.skipWidgetCSSClasses, els[i]) > -1 ||
                        els[i].toLowerCase().includes('hover')) {
                        continue;
                    }
                    result += '' + els[i];
                }
            }
            return result.trim();
        },
        resolveReference: function (params,settings) {
            let override = null;
            try{
                override = this.getTarget();
            }catch(e){
                logError(e);
                return null;
            }
            const scope = this.scope;

            const query = params.reference;


            if (!params || !query || !query.length) {
                if (override) {
                    return [override];
                }
            }

            switch (params.mode) {
                //By id, try widget instance first, then try regular HTMLElement
                //
                case types.WIDGET_REFERENCE_MODE.BY_ID:
                {
                    if (this.scope && this.scope.document) {
                        const out = [];
                        if (query.includes(' ')) {

                            const ids = query.split(' ');
                            for (let i = 0; i < ids.length; i++) {
                                const el = getElement(scope, ids[i]);
                                if (el) {
                                    out.push(el);
                                }
                            }
                        } else {
                            return [getElement(scope, query)];
                        }
                        return out;
                    }

                    const _byRegistry = registry.byId(query);
                    const _byDoc = typeof document !=='undefined' ? document.getElementById(query) : null;
                    if(_byRegistry || _byDoc){
                        return _byRegistry ? [_byRegistry] : [_byDoc];
                    }
                    break;
                }
                //By declared widget class
                //
                case types.WIDGET_REFERENCE_MODE.BY_CLASS:
                {
                    const obj = dojo.getObject(query) || dcl.getObject(query);
                    if (obj) {
                        return [obj];
                    }
                    break;
                }
                // By css class list
                //
                case types.WIDGET_REFERENCE_MODE.BY_CSS:
                {
                    let _query = this._cssClassesToQuery(query);
                    this._parseString && (_query = this._parseString(_query,settings,null,settings && settings.flags ? settings.flags : types.CIFLAG.EXPRESSION) || _query);
                    let _$ = null;
                    if (this.scope && this.scope.global && this.scope.global['$']) {
                        _$ = this.scope.global['$'];
                    }else if(typeof $!=='undefined'){
                        _$ = $;
                    }
                    if (_$) {
                        const _elements = _$(_query);
                        if (_elements) {
                            return _elements;
                        }
                    }
                    const objects = utils.find(_query, null, false);
                    if (objects) {
                        return objects;
                    }
                    break;
                }
            }
            return null;
        }
    };
    const Module = declare('xblox.model.ReferenceMixin', null, Implementation);
    Module.dcl = dcl(null, Implementation);
    return Module;
});