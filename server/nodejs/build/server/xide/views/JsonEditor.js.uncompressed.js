/** @module xgrid/Base **/
define("xide/views/JsonEditor", [
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'dojo/Deferred',
    'xide/widgets/JSONDualEditorWidget',
    'xide/editor/Base'
], function (dcl,types,utils, Deferred,JSONDualEditorWidget,EditorBase){
    return dcl([EditorBase],{
        widget:null,
        createEditor:function(_options,value){
            const ci = {
                value:value
            };
            this.widget = utils.addWidget(JSONDualEditorWidget,{
                userData:ci,
                dfd:this.dfd,
                item:this.item
            },null,this.domNode,false);

            this.add(this.widget,null,false);
            this.widget.startup();
        },
        getActionStore:function(){
            const editorWidget = this.widget.editorWidget;
            return editorWidget.aceEditor.getActionStore();
        },
        startup:function(){

            this.dfd = new Deferred();
            const self = this;
            this.dfd.then(function(){
                const editorWidget = self.widget.editorWidget;
                const oldAction = editorWidget.aceEditor.runAction;

                editorWidget.aceEditor.runAction=function(action){

                    if(action.command===types.ACTION.SAVE){
                        const value = self.widget.getValue();
                        self.saveContent(value);
                    }
                    return oldAction.apply(editorWidget.aceEditor,[action]);
                };
                if(self.ctx && self.registerView){
                    self.ctx.getWindowManager().registerView(self);
                }
            });
            return this.dfd;
        }
    });
});