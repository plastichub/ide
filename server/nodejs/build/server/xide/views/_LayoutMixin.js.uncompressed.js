/** module:xide/views/_LayoutMixin **/
define("xide/views/_LayoutMixin", [
    "xdojo/declare",
    "dcl/dcl",
    "xide/types",
    'xdocker/Docker2',
    "xide/utils",
    "xide/layout/_TabContainer"
], function (declare,dcl,types,Docker,utils,_TabContainer) {
    /**
     * @class module:xide/views/_LayoutMixin
     */
    const Implementation = {
        _docker:null,
        _parent:null,
        __right:null,
        __bottom:null,
        __masterPanel:null,
        __bottomTabContainer:null,
        defaultPanelOptions:null,
        defaultPanelType:'DefaultFixed',
        reparent:true,
        getTop:function(){
          return this._parent;
        },
        resize:function(){
            if(this._docker){
                this._docker.resize();
            }
            if(this.inherited) {
                return this.inherited(arguments);
            }
        },
        getDockerTargetNode:function(){
            return null;
        },
        /**
         * @param container {HTMLElement|module:xide/widgets/_Widget}
         * @returns {module:xdocker/Docker2}
         */
        getDocker:function(container){
            const thiz = this;
            if(!this._docker){
                const _node = this._domNode || this.domNode;
                const _dst = this.getDockerTargetNode() || container || _node.parentNode;
                thiz._docker = Docker.createDefault(_dst);
                thiz._oldParent = thiz._parent;
                const defaultOptions  = this.defaultPanelOptions || {
                        w: '100%',
                        title:false
                    };

                const parent = thiz._docker.addPanel(this.defaultPanelType, types.DOCKER.TOP, null,defaultOptions);
                this.reparent && dojo.place(_node,parent.containerNode);
                this.reparent && thiz._docker.$container.css('top',0);
                thiz._parent = parent;
                thiz.__masterPanel = parent;
                !defaultOptions.title && parent._parent.showTitlebar(false);
                _node.id = this.id;
                thiz.add(thiz._docker);
            }
            return thiz._docker;
        },
        getPanelSplitPosition:function(type){
            if(type == types.DOCKER.DOCK.RIGHT && this.__right){
                const splitter = this.__right.getSplitter();
                if(splitter){
                    return splitter.pos();
                }
            }
            return false;
        },
        setPanelSplitPosition:function(type,position){
            const right = this.__right;
            if(type == types.DOCKER.DOCK.RIGHT && right){
                const splitter = right.getSplitter();
                if(position==1) {
                    splitter._isToggledMin = true;
                    splitter._isToggledMax = true;
                }else if(position<1 && position >0){
                    splitter._isToggledMin = false;
                    splitter._isToggledMax = false;
                }
                splitter.pos(position);
            }
        },
        openRight:function(open){
            const thiz = this;
            const rightSplitPosition=thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);
            if(!open && rightSplitPosition<1){
                //panel is open: close it
                thiz.setPanelSplitPosition(types.DOCKER.DOCK.RIGHT,1);
            }else if(open && rightSplitPosition==1){
                //closed, open it and show properties
                thiz.setPanelSplitPosition(types.DOCKER.DOCK.RIGHT,0.6);
            }
        },
        _getRight:function(){
            return this.__right;
        },
        _getBottom:function(){
            return this.__bottom;
        },
        getBottomTabContainer:function(create){
            if(this.__bottomTabContainer){
                return this.__bottomTabContainer;
            }else if(create!==false){
                this. __bottomTabContainer = utils.addWidget(_TabContainer, {
                    direction: 'below'
                }, null,this.getBottomPanel(false, 0.2), true);

            }
            return this.__bottomTabContainer;
        },
        _addPanel:function(props,location,title,startPosition,type,target){
            const docker = this.getDocker();
            const panel = docker.addPanel(type || 'DefaultFixed', location , target ===false ? null : (target || this._parent), props || {
                w: '30%',
                h:'30%',
                title:title||false
            });
            if(!title) {
                panel._parent.showTitlebar(false);
            }
            if(startPosition){
                const splitter = panel.getSplitter();
                if(startPosition==1 || startPosition==0) {
                    splitter.pos(startPosition);
                }else {
                    splitter.pos(0.6);
                }
            }
            return panel;
        },
        getBottomPanel:function(title,startPosition,type,mixin,target){
            if(this.__bottom || this._getBottom()){
                return this.__bottom || this._getBottom();
            }
            const create = true;
            if(create!==false) {
                this.__bottom = this._addPanel(utils.mixin({
                    w: '30%',
                    title: title || '  '
                },mixin), types.DOCKER.DOCK.BOTTOM, title,startPosition,type,target);
            }
            return this.__bottom;
        },
        getRightPanel:function(title,startPosition,type,props){
            if(this.__right || this._getRight()){
                return this.__right || this._getRight();
            }
            props = utils.mixin({
                w: '30%',
                title:title || '  '
            },props);
            const panel = this._addPanel(props,types.DOCKER.DOCK.RIGHT,title,null,type,props.target);
            this.__right = panel;
            return panel;
        }
    };
    //package via declare
    const _class = declare("xide/views/_LayoutMixin",null,Implementation);
    _class.Implementation = Implementation;
    _class.dcl = dcl(null,Implementation);
    return _class;
});
