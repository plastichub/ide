define("xide/Features", [
    'xdojo/has',
    'xide/lodash'
], function (has, _) {
    const features = [
        'plugins',
        'log',
        'ribbons',
        'xideve',
        'files',
        'xnode',
        'xfile',
        'xace',
        'drivers',
        'debugWidgets',
        'consoleError',
        'xtrack',
        'nserver'
    ];

    if (typeof window !== 'undefined') {
        window['has'] = has;
    }

    const hiddenFeatures = [
        'admin'
    ];

    if ( 0 ) {
        _.each(features, function (feature) {
            has.add(feature, function (global, document, anElement) {
                if (location.href.includes(feature + '=false')) {
                    return false;
                } else {
                    return true;
                }
            }, true, true);
        });

        _.each(hiddenFeatures, function (feature) {
            has.add(feature, function (global, document, anElement) {
                if (location.href.includes(feature)) {
                    return true;
                } else {
                    return false;
                }
            }, true, true);
        });

        has.add("phone", function (global, document, anElement) {
            return (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase())) || location.href.includes('forcePhone=true');
        });

        has.add("mobile", function (global, document, anElement) {
            return (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase()));
        });
        
         false && has.add('debug', function (global, document, anElement) {
            return location.href.includes('debug=true');
        }, true, true);
    }
});