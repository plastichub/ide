/** @module xide/data/Source **/
define("xide/data_/Source", [
    'dcl/dcl',
    "dojo/_base/declare",
    'xide/utils',
    'xide/lodash'
], (dcl, declare, utils, lodash) => {

    var _debug = true;
    /**
     * @class module:xide/data/Source
     * @augments module:xide/data/Model
     */
    var Implementation = {
        /**
         * @type {Array<module:xide/data/Reference>|null}
         */
        _references: null,
        /**
         * @type {module:xide/data/Reference|null}
         */
        _originReference: null,
        /**
         * @type {module:xide/data/_Base|null} The store.
         */
        _store: null,
        onReferenceUpdate: function () {
        },
        onReferenceRemoved: function () {
        },
        onReferenceDelete: function () {
        },
        updateReference: function () {
        },
        destroy: function () {
            this._references = null;
        },
        getReferences: function () {
            return this._references ? utils.pluck(this._references, 'item') : [];
        },
        hasReference: function (source) {
            return lodash.find(this._references, {item: source});
        },
        addReference: function (reference, settings, addSource) {
            !this._references && (this._references = []);
            if (this.hasReference(reference)) {
                _debug && console.warn('already have reference');
                return;
            }
            this._references.push({
                item: reference,
                settings: settings
            });
            if (settings && settings.onDelete) {
                if (reference._store) {
                    reference._store.on('delete', evt => {
                        if (evt.target == reference) {
                            this._store.removeSync(this[this._store.idProperty]);
                            this._references.remove(evt.target);
                        }
                    });
                }
            }
            if (addSource) {
                if (reference.addSource) {
                    reference.addSource(this, settings);
                }
            }
        },
        removeReference: function (Reference) {
            this._references && lodash.each(this._references, function (ref) {
                if (ref && ref.item == Reference) {
                    this._references && this._references.remove(ref);
                    return true;
                }
            }, this);
        },
        updateReferences: function (args) {
            var property = args.property,
                value = args.value;

            if (!this._references) {
                this._references = [];
            }
            for (var i = 0; i < this._references.length; i++) {
                var link = this._references[i],
                    item = link.item,
                    settings = link.settings,
                    store = item._store;

                if (this._originReference == item) {
                    continue;
                }
                if (args.property && settings.properties && settings.properties[args.property]) {
                    if (store) {
                        store.silent(true);
                    }
                    try {
                        if (item.onSourceChanged) {
                            item.onSourceChanged(property, value, args.type);
                        } else {
                            item.set(property, value);
                        }

                    } catch (e) {
                        _debug && console.error('error updating reference! ' + e, e);
                    }
                    if (store) {
                        store.silent(false);
                        store.emit('update', {target: item});
                    }
                }
            }
        },
        constructor: function (properties) {
            this._references = [];
            utils.mixin(this, properties);
        },
        onItemChanged: function (args) {
            this.updateReferences(args);
        }
    };
    //exports for declare & dcl
    var Module = declare('xgrid.data.Source', null, Implementation);
    Module.dcl = dcl(null, Implementation);
    Module.Implementation = Implementation;
    return Module;
});
