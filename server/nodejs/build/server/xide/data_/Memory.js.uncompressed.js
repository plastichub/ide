/** @module xide/data/Memory **/
define("xide/data_/Memory", [
    "dojo/_base/declare",
	'dstore/Memory',
    'xide/data/_Base'
], (declare, Memory, _Base) => /**
 * Base memory class
 * @class module:xide/data/Memory
 * @extends module:xide/data/_Base
 * @extends module:dstore/Memory
 */
declare('xide.data.Memory', [Memory, _Base], {
    /**
     * XIDE specific override to ensure the _store property. This is because the store may not use dmodel in some
     * cases like running server-side but the _store property is expected to be there.
     * @param item {object}
     * @returns {*}
     */
    putSync:function(item){
        var self = this;
        item = this.inherited(arguments);
        item && !item._store && Object.defineProperty(item, '_store', {
            get: function () {
                return self;
            }
        });
        return item;
    }
}));
