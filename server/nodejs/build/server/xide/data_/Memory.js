//>>built
define("xide/data_/Memory",["dojo/_base/declare","dstore/Memory","xide/data/_Base"],function(b,c,d){return b("xide.data.Memory",[c,d],{putSync:function(a){var b=this;(a=this.inherited(arguments))&&!a._store&&Object.defineProperty(a,"_store",{get:function(){return b}});return a}})});
//# sourceMappingURL=Memory.js.map