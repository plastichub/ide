/** @module xide/data/ReferenceMapping **/
define("xide/data_/ReferenceMapping", [
    'dcl/dcl',
    'xide/lodash'
], (dcl, _) => dcl(null, {
    mapping: null,
    getMapping: function (event) {
        if (!this.mapping) {
            this.mapping = {};
        }
        return this.mapping[event];
    },
    addMapping: function (event, from, to) {
        if (!this.mapping) {
            this.mapping = {};
        }
        !this.mapping[event] && (this.mapping[event] = []);
        this.mapping[event].push({
            from: from,
            to: to
        });
    },
    /**
     * Callback when source has changed. This is normally called
     * in Source::updateReferences due to a change in another
     * referencing widget.
     *
     * @param property {string} The property
     * @param value {object|null}
     * @param event {object|null}
     */
    onSourceChanged: function (property, value, event) {
        var mapping = this.getMapping(event);
        var propertyMap = mapping ? _.find(mapping, {from: property}) : null;
        if (propertyMap) {
            return this.set(propertyMap.to, value);
        }
    }
}));