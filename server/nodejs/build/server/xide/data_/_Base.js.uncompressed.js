/** @module xide/data/_Base  **/
define("xide/data_/_Base", [
    "dojo/_base/declare",
    'dstore/QueryResults',
    'xide/mixins/EventedMixin',
    'xide/encoding/MD5',
    'xdojo/has',
    'xide/lodash',
    'dojo/when',
    'dojo/Deferred'
], (declare, QueryResults, EventedMixin, MD5, has, lodash, when, Deferred) => /**
 * Mixin in XIDE basics to dstore classes.
 * @class module:xide/data/_Base
 * @extends module:xide/mixins/EventedMixin
 * @lends module:dstore/Memory
 */
declare("xide/data/_Base", EventedMixin, {
    __all: null,
    allowCache: true,
    _queryCache: null,
    /**
     * Faster alternative to #query.
     * @param query
     * @returns {*}
     * @private
     */
    _find: function (query) {
        var result = lodash.filter(this.data, query);
        if (lodash.isArray(result)) {
            return result;
        } else if (lodash.isObject(result)) {
            return [result];
        }
        return [];
    },
    _query: function (query) {
        var dfd = new Deferred();
        var collection = this.filter(query);
        when(collection.fetch(), data => {
            dfd.resolve(data);
        });
        return dfd;
    },
    constructor: function () {
        var store = this;
        if (store._getQuerierFactory('filter') || store._getQuerierFactory('sort')) {
            this.queryEngine = (query, options) => {
                options = options || {};
                var filterQuerierFactory = store._getQuerierFactory('filter');
                var filter = filterQuerierFactory ? filterQuerierFactory(query) : passthrough;
                var sortQuerierFactory = store._getQuerierFactory('sort');
                var sort = passthrough;
                if (sortQuerierFactory) {
                    sort = sortQuerierFactory(arrayUtil.map(options.sort, criteria => ({
                        property: criteria.attribute,
                        descending: criteria.descending
                    })));
                }

                var range = passthrough;
                if (!isNaN(options.start) || !isNaN(options.count)) {
                    range = data => {
                        var start = options.start || 0,
                            count = options.count || Infinity;

                        var results = data.slice(start, start + count);
                        results.total = data.length;
                        return results;
                    };
                }

                return data => range(sort(filter(data)));
            };
        }
        var objectStore = this;
        // we call notify on events to mimic the old dojo/store/Trackable
        store.on('add,update,delete', event => {
            var type = event.type;
            var target = event.target;
            objectStore.notify(
                (type === 'add' || type === 'update') ? target : undefined,
                (type === 'delete' || type === 'update') ?
                    ('id' in event ? event.id : store.getIdentity(target)) : undefined);
        });
    },
    /**
     * Override destroy to also call destroy an item's individual destroy function. Needed for temporary stores.
     * Then, delete query cache.
     * @returns {*}
     */
    destroy: function () {
        this._emit('destroy', this);
        _.each(this.query(), item => {
            if (item.destroyOnRemove === true) {
                item.destroy && item.destroy();
            }
        });
        delete this._queryCache;
        this._queryCache = null;
    },
    refreshItem: function (item, property) {
        this.emit('update', {
            target: item,
            property: property
        });
    },
    query: function (query, options, allowCache) {
        //no query, return all
        if (lodash.isEmpty(query)) {
            var self = this;
            return _.map(this.data, item => self.getSync(item[self.idProperty]), this);
        } else if (!_.some(query, value => value == null)) {
            //no empty props in query, return lodash.filter
            //return this._find(query);
        }
        var hash = query ? MD5(JSON.stringify(query), 1) : null;
        if (has('xcf-ui')) {
            if (hash && ! true  && allowCache !== false) {
                !this._queryCache && (this._queryCache = {});
                if (this._queryCache[hash]) {
                    return this._queryCache[hash];
                }
            }
        }
        /*
         if(!query && !options && allowCache!==false && this.allowCache){
         return this.data;
         }*/

        // summary:
        //		Queries the store for objects. This does not alter the store, but returns a
        //		set of data from the store.
        // query: String|Object|Function
        //		The query to use for retrieving objects from the store.
        // options: dstore/api/Store.QueryOptions
        //		The optional arguments to apply to the resultset.
        // returns: dstore/api/Store.QueryResults
        //		The results of the query, extended with iterative methods.
        //
        // example:
        //		Given the following store:
        //
        //	...find all items where "prime" is true:
        //
        //	|	store.query({ prime: true }).forEach(function(object){
        //	|		// handle each object
        //	|	});
        options = options || {};
        query = query || {};

        var results = this.filter(query);
        var queryResults;

        // Apply sorting
        var sort = options.sort;
        if (sort) {
            if (Object.prototype.toString.call(sort) === '[object Array]') {
                var sortOptions;
                while ((sortOptions = sort.pop())) {
                    results = results.sort(sortOptions.attribute, sortOptions.descending);
                }
            } else {
                results = results.sort(sort);
            }
        }

        var tracked;
        var _track = false;
        if (_track && results.track && !results.tracking) {
            // if it is trackable, always track, so that observe can
            // work properly.
            results = results.track();
            tracked = true;
        }
        if ('start' in options) {
            // Apply a range
            var start = options.start || 0;
            // object stores support sync results, so try that if available
            queryResults = results[results.fetchRangeSync ? 'fetchRangeSync' : 'fetchRange']({
                start: start,
                end: options.count ? (start + options.count) : Infinity
            });
            queryResults.total = queryResults.totalLength;
        }
        queryResults = queryResults || new QueryResults(results[results.fetchSync ? 'fetchSync' : 'fetch']());
        queryResults.observe = (callback, includeObjectUpdates) => {
            // translate observe to event listeners
            function convertUndefined(value) {
                if (value === undefined && tracked) {
                    return -1;
                }
                return value;
            }

            var addHandle = results.on('add', event => {
                callback(event.target, -1, convertUndefined(event.index));
            });
            var updateHandle = results.on('update', event => {
                if (includeObjectUpdates || event.previousIndex !== event.index || !isFinite(event.index)) {
                    callback(event.target, convertUndefined(event.previousIndex), convertUndefined(event.index));
                }
            });
            var removeHandle = results.on('delete', event => {
                callback(event.target, convertUndefined(event.previousIndex), -1);
            });
            var handle = {
                remove: function () {
                    addHandle.remove();
                    updateHandle.remove();
                    removeHandle.remove();
                }
            };
            handle.cancel = handle.remove;
            return handle;
        };
        if (!has('xcf-ui') && hash && ! true  && allowCache !== false) {
            !this._queryCache && (this._queryCache = {});
            this._queryCache[hash] = queryResults;
        }
        return queryResults;
    }
}));