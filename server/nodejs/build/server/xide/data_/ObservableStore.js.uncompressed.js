/** @module xide/data/ObservableStore **/
define("xide/data_/ObservableStore", [
    "dojo/_base/declare",
    "xide/mixins/EventedMixin",
    "xide/lodash"
], (declare, EventedMixin, _) => /**
 * Mixin to deal with dmodel
 * @class module:xide/data/ObservableStore
 * @lends module:xide/data/_Base
 * @lends module:dstore/Store
 * @lends module:xide/data/Memory
 */
declare('xide/data/Observable', EventedMixin, {
    /**
     * @type {boolean} Toggle to mute notifications during batch operations.
     */
    _ignoreChangeEvents: true,
    /**
     * @type {Array<String>} List of default properties to be observed by dmodel.property.observe.
     */
    observedProperties: [],
    /**
     * Get/Set toggle to prevent notifications for mass store operations. Without there will be performance drops.
     * @param silent {boolean|null}
     */
    silent: function (silent) {
        if (silent === undefined) {
            return this._ignoreChangeEvents;
        }
        if (silent === true || silent === false && silent !== this._ignoreChangeEvents) {
            this._ignoreChangeEvents = silent;
        }
    },
    /**
     * XIDE Override and extend putSync for adding the _store property and observe a new item's properties.
     * @param item
     * @param publish
     * @returns {*}
     */
    putSync: function (item, publish) {
        this.silent(!publish);
        var res = this.inherited(arguments);
        var self = this;
        publish !== false && this.emit('added', res);
        res && !res._store && Object.defineProperty(res, '_store', {
            get: function () {
                return self;
            }
        });
        this._observe(res);
        this.silent(false);
        return res;
    },
    /**
     * Extend and override removeSync to silence notifications during batch operations.
     * @param id {string}
     * @param silent {boolean|null}
     * @returns {*}
     */
    removeSync: function (id, silent) {
        this.silent(silent);
        var _item = this.getSync(id);
        _item && _item.onRemove && _item.onRemove();
        var res = this.inherited(arguments);
        this.silent(false);
        return res;
    },
    /**
     *
     * @param item
     * @param property
     * @param value
     * @param source
     * @private
     */
    _onItemChanged: function (item, property, value, source) {
        if (this._ignoreChangeEvents) {
            return;
        }
        var args = {
            target: item,
            property: property,
            value: value,
            source: source
        };
        this.emit('update', args);
        item.onItemChanged && item.onItemChanged(args);
    },
    /**
     * Observe an item's properties specified in this.observedProperties and item.observed, called upon putSync.
     * @param item {module:xide/data/Model}
     * @private
     */
    _observe: function (item) {
        var props = this.observedProperties;
        item.observed && (props = props.concat(item.observed));
        _.each(props, property => {
            item.property && item.property(property).observe(value => {
                this._onItemChanged(item, property, value, this);
            });
        });
    },
    /**
     * Override setData to bring in dmodel's observe for new items.
     * @param data {object[]}
     * @returns {*}
     */
    setData: function (data) {
        var res = this.inherited(arguments);
        this.silent(true);
        data && _.each(data, this._observe, this);
        this.silent(false);
        return res;
    }
}));