/** @module xide/data/TreeMemory **/
define("xide/data_/TreeMemory", [
    "dojo/_base/declare",
    'xide/data/Memory',
    'dstore/Tree',
    'dojo/Deferred',
    'dstore/QueryResults'
], (declare, Memory, Tree, Deferred, QueryResults) => /**
 * @class module:xide/data/TreeMemory
 * @deprecated
 * @extends module:xide/data/_Base
 * @extends module:dstore/Tree
 */
declare('xide.data.TreeMemory', [Memory, Tree], {
    _state: {
        filter: null
    },
    parentField: 'parentId',
    parentProperty: 'parentId',
    reset: function () {
        this._state.filter = null;
        this.resetQueryLog();
    },
    resetQueryLog: function () {
        this.queryLog = [];
    },
    fetchRange: function () {
        // dstore/Memory#fetchRange always uses fetchSync, which we aren't extending,
        // so we need to extend this as well.
        var results = this._fetchRange(arguments);
        return new QueryResults(results.then(data => data), {
            totalLength: results.then(data => data.length)
        });
    },
    filter: function (data) {
        var _res = this.inherited(arguments);
        this._state.filter = data;
        return _res;
    },
    _fetchRange: function (kwArgs) {
        var deferred = new Deferred();
        var _res = this.fetchRangeSync(kwArgs);
        if (this._state.filter) {
            //the parent query
            if (this._state.filter['parent']) {
                var _item = this.getSync(this._state.filter.parent);
                if (_item) {
                    this.reset();
                    var _query = {};
                    if (this.getChildrenSync) {
                        _res = this.getChildrenSync(_item);
                    } else {
                        _query[this.parentField] = _item[this.idProperty];
                        _res = this.root.query(_query);
                    }
                }
            }

            //the group query
            if (this._state && this._state.filter && this._state.filter['group']) {
                var _items = this.getSync(this._state.filter.parent);
                if (_item) {
                    this.reset();
                    _res = _item.items;
                }
            }
        }
        deferred.resolve(_res);
        return deferred;
    },
    children: function (parent) {
        var all = this.root.data, out = [];
        for (var i = 0; i < all.length; i++) {
            var obj = all[i];
            if (obj[this.parentField] == parent[this.idProperty]) {
                out.push(obj);
            }
        }
        return all;
    },
    mayHaveChildren: function (parent) {
        if (parent._mayHaveChildren === false) {
            return false;
        }
        return true;
    }
}));
