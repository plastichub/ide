define("xide/widgets/_HTMLTemplateMixin", [
    'dojo/_base/declare',
    "dojox/xml/parser",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "xide/utils"
], function (declare, xmlParser, domAttr, lang, utils) {
    return declare("xide.widgets._HTMLTemplateMixin", null, {
        setMixins: function (htmlTemplate) {
            let isWrapped = false;
            const xml = xmlParser.parse(htmlTemplate || this.templateString);
            if (xml && xml.firstChild && xml.firstChild.attributes && xml.firstChild.attributes.length == 1) {
                isWrapped = true;
                const dataAttriubte = domAttr.get(xml.firstChild, 'data-dojo-props');
                if (dataAttriubte) {

                    try {
                        const jsonData = utils.fromJson('{' + dataAttriubte + '}');
                        if (jsonData) {
                            lang.mixin(this, jsonData);
                        }
                    } catch (e) {
                        console.error('couldnt parse template');
                    }
                }
            }
            if (isWrapped == false) {
                this.templateString = '<div>' + this.templateString + '</div>';
            }
        },
        parseHTMLTemplate: function (data) {
            if (data && data.htmlTemplate) {
                if (data.htmlTemplate.indexOf('htm') == -1) {
                    this.templateString = data.htmlTemplate;
                } else {
                    this.templateString = this._getHTMLTemplate(data.htmlTemplate);
                }

                this.setMixins(this.templateString);
            }
        },
        _getHTMLTemplate: function (path) {
            return this._getText(path);
        },
        _getText: function (url) {
            let result;
            const def = dojo.xhrGet({
                url: url,
                sync: true,
                handleAs: 'text',
                load: function (text) {
                    result = text;
                }
            });
            return '' + result + '';
        }
    });
});