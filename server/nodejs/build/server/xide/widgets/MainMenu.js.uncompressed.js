/** @module xide/widgets/MainMenu **/
define("xide/widgets/MainMenu", [
    'dcl/dcl',
    'xide/types',
    'xide/_base/_Widget',
    'xlang/i18',
    "xide/mixins/ActionMixin",
    'xaction/ActionContext',
    "xide/widgets/_MenuMixin4",
    "xide/model/Path",
    "xide/$",
    "xide/lodash",
    "xide/widgets/_MenuKeyboard"
], function (dcl, types, _XWidget, i18, ActionMixin, ActionContext, MenuMixinClass, Path, $, _, _MenuKeyboard) {
    const _debug = false;
    const ContainerClass = dcl([_XWidget, ActionContext.dcl, ActionMixin.dcl], {
        templateString: '<div class="navbar navbar-default mainMenu bg-opaque">' +
            '<nav attachTo="navigation" class="" role="navigation">' +
            '<ul tabindex="-1" attachTo="navBar" class="nav navbar-nav"/>' +
            '</nav>' +
            '</div>'
    });
    const ActionRendererClass = dcl(null, {
        _lastOpened: null,
        destroy: function () {
            this._lastOpened = null;
        },
        renderTopLevel: function (name, where) {
            let val = i18.localize(name);
            let shortcutKey = "";
            let text;
            const ndx = 0;
            if (ndx >= 0) {
                shortcutKey = val.charAt(ndx);
                const prefix = val.substr(0, ndx);
                const suffix = val.substr(ndx + 1);
                val = prefix + '<span class="shortcutKey">' + shortcutKey + '</span>' + suffix;
                this.shortcuts[shortcutKey] = name;
            }
            where = where || $(this.getRootContainer());
            const item = $('<li tabindex="-1" class="dropdown">' +
                '<a href="#" data-delay="1500" class="dropdown-toggle" data-toggle="dropdown">' + val + '<b class="caret"></b></a>' +
                '</li>');
            where.append(item);
            const trigger = item.find('A');
            trigger.data('owner', this);
            trigger.on('click', () => this._lastOpened = trigger);
            item.on('hide.bs.dropdown', (e) => this._lastOpened = null)
            item.on('show.bs.dropdown', (e) => this._lastOpened = trigger);
            if(trigger.bootstrapDropdownHover){
                trigger.bootstrapDropdownHover();
            }else{
                console.error('bootstrapDropdownHover not loaded!');
            }
            return item;
        },
        getRootContainer: function () {
            return this.navBar;
        }
    });
    const KeyboardControl = _MenuKeyboard;
    const MainMenu = dcl([ContainerClass, ActionRendererClass, MenuMixinClass, _XWidget.StoreMixin], {
        target: null,
        visibility: types.ACTION_VISIBILITY.MAIN_MENU,
        attachToGlobal: true,
        declaredClass: 'xide.widgets.MainMenu',
        shortcuts: null,
        _topLevelMenu: null,
        _altDown: false,
        _shiftDown: false,
        addContext: function (selector, data) {
            let id;
            let $menu;
            const self = this;
            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = self.buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {
                const d = new Date();
                id = d.getTime();
                $menu = self.buildMenu(data, id);
                selector.append($menu);
            }
            return $menu;
        },
        onRootAction: function (level, container) {
            return this.renderTopLevel(level, container);
        },
        setActionStore: function (store, owner) {
            this._clear();
            this._tmpActions = [];
            delete this._topLevelMenu;
            this._topLevelMenu = {};
            this.store = store;
            if (!store) {
                return;
            }
            this.addActionStore(store);
            const self = this;
            const visibility = self.visibility;
            const rootContainer = $(self.getRootContainer());
            const tree = self.buildActionTree(store, owner);
            const allActions = tree.allActions;
            const rootActions = tree.rootActions;
            const allActionPaths = tree.allActionPaths;
            const delegate = this.delegate;

            /*
             var mapping = Keyboard.defaultMapping(keyCombo, handler, keyProfile || types.KEYBOARD_PROFILE.DEFAULT, keyTarget, keyScope, [action]);
             mapping = this.registerKeyboardMapping(mapping);
             keyboardMappings.push(mapping);
             */

            function registerRootKeyboardHandler(name) {
                const keyboardCombo = 'alt ' + name[0];
            }

            _.each(tree.root, function (menuActions, level) {

                const root = self.onRootAction(level, rootContainer);
                self._topLevelMenu[level] = root;
                let lastHeader = {
                    header: ''
                };

                // final menu data
                const data = [];
                const groupedActions = menuActions.grouped;

                //temp group string of the last rendered action's group
                let lastGroup = '';
                _.each(menuActions, function (command) {
                    let action = self.getAction(command, store);
                    let isDynamicAction = false;
                    if (!action) {
                        isDynamicAction = true;
                        action = self.createAction(command);
                    }
                    if (action) {
                        const renderData = self.getActionData(action);
                        const icon = renderData.icon;
                        const label = renderData.label;
                        const visibility = renderData.visibility;
                        const group = renderData.group;

                        if (visibility === false) {
                            return;
                        }
                        if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 1) {
                            if (lastGroup !== group) {
                                const name = groupedActions[group].length >= 2 ? i18.localize(group) : "";
                                lastHeader = {
                                    header: name
                                };
                                data.push(lastHeader);
                                lastGroup = group;
                            }
                        }
                        const item = self.toMenuItem(action, owner, label, icon, visibility || {}, true);
                        data.push(item);
                        visibility.widget = item;
                        self.addReference(action, item);

                        function parseChildren(command, parent) {
                            const childPaths = new Path(command).getChildren(allActionPaths, false);
                            const isContainer = childPaths.length > 0;
                            const childActions = isContainer ? self.toActions(childPaths, store) : null;

                            if (isContainer && childActions) {
                                const subs = [];
                                _.each(childActions, function (child) {
                                    const _renderData = self.getActionData(child);
                                    const _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility, true);
                                    self.addReference(child, _item);
                                    subs.push(_item);
                                    const _childPaths = new Path(child.command).getChildren(allActionPaths, false);
                                    _childPaths.length > 0 && parseChildren(child.command, _item);
                                });
                                parent.subMenu = subs;
                            }
                        }

                        parseChildren(command, item);
                    }
                });
                return self.attach(root, data);
            });

            self.onDidRenderActions(store, owner);
        },
        startup: function () {
            this.init({
                preventDoubleContext: false
            });
            if (this.attachToGlobal) {
                let node = $('#staticTopContainer');
                if (!node[0]) {
                    const body = $('body');
                    node = $('<div id="staticTopContainer" class=""></div>');
                    body.prepend(node);
                }
                node.append($(this.domNode));
            }
        },
        lastFocused: null,
        setupKeyboard: function (node) {

            function keyhandler(e) {
                try {
                    if (e && e.target && e.target.className) {
                        const className = e.target.className.toLowerCase();
                        if (e.target.tagName !== 'BUTTON' && className.indexOf('input') == -1) {
                            e.keyCode === 16 && (this._shiftDown = e.type === 'keydown');
                            if (e.keyCode == 27) {
                                const navData = this.keyboardController.toNavigationData($(e.target), this.getRootContainer());
                                navData && navData.element && this.keyboardController.close(navData.element);
                                $(this.lastFocused).focus();
                            }
                            if (this._shiftDown && e.key in this.shortcuts) {
                                this.lastFocused = document.activeElement;
                                //open root
                                this.keyboardController.openRoot(null, this._topLevelMenu[this.shortcuts[e.key]]);
                            }
                        }
                    }
                } catch (e) {
                    logError(e, 'error in keyboard handler');
                }
            }
            $(node).on('keydown', keyhandler.bind(this));
        },
        init: function (opts) {
            if (this._didInit) {
                return;
            }
            this._didInit = true;
            this.shortcuts = {};

            this.setupKeyboard(this.delegate._domNode || typeof document !== 'undefined' ? document : null);

            let options = this.getDefaultOptions();
            this.keyboardController = new KeyboardControl();
            this.keyboardController.setup(this);
            options = $.extend({}, options, opts);
            const self = this;
            const root = $(document);


            this.__on(root, 'click', null, function (e) {
                if (!self.isOpen) {
                    return;
                }
                self.isOpen = false;
                self.onClose(e);
                $('.dropdown-context').css({
                    display: ''
                }).find('.drop-left').removeClass('drop-left');
            });

            if (options.preventDoubleContext) {
                this.__on(root, 'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }

            function mouseEnterHandlerSubs(e) {
                const navigationData = this.keyboardController.toNavigationData($(e.target), self.getRootContainer());
                if (!navigationData) {
                    return;
                }
                const _parent = navigationData.parent;
                _parent.focus();
                navigationData.element.focus();
                _parent.data('currentTarget', navigationData.element);
                this.keyboardController.initContainer(_parent);
            }
            this.__on(this.getRootContainer(), 'mouseenter', '.dropdown-menu >LI', mouseEnterHandlerSubs.bind(this));
        }
    });
    return MainMenu;
});