define("xide/widgets/EditBox", [
    'dcl/dcl',
    "dojo/dom-construct",
    "xide/widgets/WidgetBase",
    "xide/utils",
    "xide/types",
    "xide/$"
], function (dcl, domConstruct, WidgetBase, utils, types, $) {
    return dcl(WidgetBase, {
        declaredClass: 'xide.widgets.EditBox',
        value: "unset",
        _lastValue: null,
        instant: false,
        widgetChanged: function () {
            this.changed = true;
            if (this.userData) {
                this.userData.changed = true;
            }
            if (this.nativeWidget) {
                let _value = this.nativeWidget.val();
                if (this.userData && this.userData.type === types.ECIType.INTEGER) {
                    _value = parseInt(_value, 10);
                }
                this.setValue(_value);
                this.value = _value;
            }
        },
        __onChanged: function (value) {
            const self = this;
            const widget = this.nativeWidget;
            if (self.validator) {
                const validated = self.validator(value, widget);
                if (validated) {
                    this.userData.invalid = false;
                    widget.removeClass('text-danger');
                    widget.addClass('text-success');
                } else {
                    widget.removeClass('text-success');
                    widget.addClass('text-danger');
                    this.userData.invalid = true;
                }
            }
            if (self._lastValue !== null && self._lastValue === value) {
                return;
            }

            self._lastValue = value;
            self.widgetChanged(widget);
            if (this.userData && this.userData.type === types.ECIType.INTEGER) {
                value = parseInt(value, 10);
            }
            if(this.updateOnly){
                this.updateOnly=false;
                //return;
            }
            self._emit('change', value);
        },
        setupNativeWidgetHandler: function () {
            const nativeWidget = this.nativeWidget;
            if (nativeWidget) {
                const thiz = this;
                this.addHandle("change", this.__on(nativeWidget, 'change', null, function () {
                    thiz.__onChanged(nativeWidget.val());
                }));
                if (this.instant) {
                    nativeWidget.on('keyup change', function () {
                        thiz.__onChanged(nativeWidget.val());
                    });
                }
                this.addHandle("blur", nativeWidget.on("blur", function () {
                    thiz.setActive(false);
                }));
                this.addHandle("focus", nativeWidget.on("focus", function () {
                    thiz.setActive(true);
                }));
                
                $(this.nativeWidget).data('widget', this);
                
            }
        },
        set: function (what, value, updateOnly) {
            if (updateOnly === true) {
                this.updateOnly = true;
            }
            const widget = this.nativeWidget;
            if (widget) {
                if (what === 'value') {
                    widget.val(value);
                    this._lastValue=value;
                    return true;
                }
                if (what === 'disabled') {
                    widget[value === true ? 'attr' : 'removeAttr'](what, value);
                }
            }
            return this.inherited(arguments);
        },
        get: function (what) {
            const widget = this.nativeWidget;
            if (what === 'value' && widget) {
                return widget.val();
            }
        },
        postMixInProperties:function(){
            if(this.inline){
                this.templateString = '<div attachTo="valueNode"></div>';
            }
        },
        startup: function () {
            this.valueNode.innerHTML = "";
            const user = this.userData;
            const value = user ? user.value : this.value || "";
            const input = domConstruct.create('input', {
                "class": "form-control input-transparent",
                type: user && user.password ? "password" : "text",
                value: value
            });
            this.nativeWidget = $(input);
            if (!this.title || this.title === '') {
                utils.destroy(this.titleColumn);
            }
            this.valueNode.appendChild(input);
            this.nativeWidget.trigger('change');
            this.setupNativeWidgetHandler();
            this.onReady();
        }
    });
});