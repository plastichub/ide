/**
 * @module xide/widgets/StructuralWidget
 */
define("xide/widgets/StructuralWidget", [
    'dcl/dcl',
    'xide/widgets/WidgetBase',
    'xide/utils',
    'xide/factory'
], function (dcl, WidgetBase, utils, factory) {
    /**
     * @class module:xide/widgets/StructuralWidget
     */
    return dcl(WidgetBase, {
        title: " ",
        declaredClass: 'xide.widgets.StructuralWidget',
        cssClass: "StructuralWidget",
        templateString: "<div class='' style=''>" +
        "<div attachTo='titleColumn' class=''><span attachTo='titleNode'>${!title}</span></div>" +
        "<div valign='middle' class='' attachTo='valueNode'>${!value}</div>" +
        "</div>",
        /**
         *
         * @param evt {object}
         * @param evt.ci {object}
         * @param evt.newValue {object}
         * @param evt.oldValue {object}
         */
        onValueChanged: function (evt) {
            //at this point we have to convert the CIS back to normal json
            //var value = utils.getJson(this.userData.value);
            const result = {};
            _.each(this.cis, function (ci) {
                result[ci.id] = ci.value
            });
            this.setValue(JSON.stringify(result, null, 2));
        },
        render: function (cis) {
            const value = utils.getJson(this.userData.value);
            _.each(value, function (val, key) {
                const optionsCI = utils.getInputCIByName(cis, key);
                if (optionsCI) {
                    optionsCI.value = val;
                }
            });
            this.cis = cis;
            this.$valueNode.empty();
            return factory.renderCIS(cis, this.valueNode, this);
        },
        _render: function (cis) {
            const dfd = factory.renderCIS(cis, this.valueNode, this.owner);
            dfd.then(function () {

            })
            return dfd;
        },
        startup: function () {
            $(this.domNode).addClass(this.cssClass);
            this.onReady();
        }
    });
});