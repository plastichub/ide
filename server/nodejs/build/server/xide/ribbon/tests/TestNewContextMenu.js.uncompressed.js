/** @module xgrid/Base **/
define("xide/ribbon/tests/TestNewContextMenu", [
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "xide/tests/TestUtils",
    "xide/widgets/_Widget",
    "module",
    
    'xide/registry',
    'xide/_base/_Widget',
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    'xide/action/DefaultActions',
    "xaction/ActionProvider",
    "xgrid/Clipboard",
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    'xide/mixins/EventedMixin',
    "xide/widgets/_MenuMixin4",
    "xblox/tests/TestUtils",
    "xide/_Popup",
    'xcf/model/Variable',
    'xide/factory',
    "xblox/views/BlockGrid",
    'xblox/model/variables/VariableAssignmentBlock'

], function (declare, dcl, types, utils, TestUtils, _Widget, module, registry,
             _XWidget,
             ActionMixin, ActionContext, ActionStore, DefaultActions, ActionProvider,
             Clipboard, Path, Action, FTestUtils, i18,
             EventedMixin,_MenuMixin2,_TestBlockUtils,
             _Popup,Variable,factory,BlockGrid,VariableAssignmentBlock) {
    /**
     *
     * description
     */
    const ACTION = types.ACTION;

    const createCallback = function (func, menu, item) {
        return function (event) {
            func(event, menu, item);
        };
    };
    console.clear();

    const _ctx = {};
    const _debug = false;
    const _debugActionRendering = false;
    let grid = null;


    const MenuMixinClass = dcl(null, {
        actionStores: null,
        correctSubMenu: false,
        _didInit: null,
        actionFilter: null,
        hideSubsFirst: false,
        collapseSmallGroups: 0,
        containerClass: '',
        lastTree:null,
        onActionAdded: function (actions) {
            this.setActionStore(this.getActionStore(), actions.owner || this, false, true, actions);
        },
        onActionRemoved: function (evt) {
            this.clearAction(evt.target);
        },
        clearAction: function (action) {
            const self = this;
            if (action) {
                const actionVisibility = action.getVisibility !== null ? action.getVisibility(self.visibility) : {};
                if (actionVisibility) {
                    const widget = actionVisibility.widget;
                    widget && action.removeReference && action.removeReference(widget);
                    if (widget && widget.destroy) {
                        widget.destroy();
                    }
                    delete actionVisibility.widget;
                    actionVisibility.widget = null;
                }
            }
        },
        removeCustomActions: function () {
            const oldStore = this.store;

            const oldActions = oldStore.query({
                    custom: true
                });

            const menuData = this.menuData;

            _.each(oldActions, function (action) {
                oldStore.removeSync(action.command);
                const oldMenuItem = _.find(menuData, {
                    command: action.command
                });
                oldMenuItem && menuData.remove(oldMenuItem);
            });
        },
        /**
         * Return a field from the object's given visibility store
         * @param action
         * @param field
         * @param _default
         * @returns {*}
         */
        getVisibilityField: function (action, field, _default) {
            const actionVisibility = action.getVisibility !== null ? action.getVisibility(this.visibility) : {};
            return actionVisibility[field] !== null ? actionVisibility[field] : action[field] || _default;
        },
        /**
         * Sets a field in the object's given visibility store
         * @param action
         * @param field
         * @param value
         * @returns {*}
         */
        setVisibilityField: function (action, field, value) {
            const _default = {};
            if (action.getVisibility) {
                var actionVisibility = action.getVisibility(this.visibility) || _default;
                actionVisibility[field] = value;
            }
            return actionVisibility;
        },
        shouldShowAction: function (action) {
            if (this.getVisibilityField(action, 'show') === false) {
                return false;
            } else if (action.getVisibility && action.getVisibility(this.visibility) == null) {
                return false;
            }
            return true;
        },
        addActionStore: function (store) {
            if (!this.actionStores) {
                this.actionStores = [];
            }
            if (!this.actionStores.includes(store)) {
                this.actionStores.push(store);
            }
        },
        /**

         tree structure :

         {
            root: {
                Block:{
                    grouped:{
                        Step:[action,action]
                    }
                }
            },
            rootActions: string['File','Edit',...],

            allActionPaths: string[command],

            allActions:[action]
         }

         * @param store
         * @param owner
         * @returns {{root: {}, rootActions: Array, allActionPaths: *, allActions: *}}
         */
        constructor: function (options, node) {
            this.target = node;
            utils.mixin(this, options);
        },
        init: function (opts) {
            if (this._didInit) {
                return;
            }
            this._didInit = true;
            let options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            const self = this;
            const root = $(document);
            this.__on(root, 'click', null, function (e) {
                if (!self.isOpen) {
                    return;
                }
                self.isOpen = false;
                self.onClose(e);
                $('.dropdown-context').css({
                    display: ''
                }).find('.drop-left').removeClass('drop-left');
            });
            if (options.preventDoubleContext) {
                this.__on(root, 'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            this.__on(root, 'mouseenter', '.dropdown-submenu', function (e) {
                try {
                    const _root = $(e.currentTarget);
                    const $sub = _root.find('.dropdown-context-sub:first');
                    if ($sub.length === 0) {
                        return;
                    }
                    if (self.correctSubMenu === false) {
                        return;
                    }
                    if (self.menu) {
                        if (!$.contains(self.menu[0], _root[0])) {
                            return;
                        }
                    }
                    const _disabled = _root.hasClass('disabled');
                    if (_disabled) {
                        $sub.css('display', 'none');
                        return;
                    } else {
                        $sub.css('display', '');
                    }

                    //reset top
                    $sub.css({
                        top: 0
                    });

                    const autoH = $sub.height() + 0;
                    const totalH = $('html').height();
                    const pos = $sub.offset();
                    const overlapYDown = totalH - (pos.top + autoH);
                    if ((pos.top + autoH) > totalH) {
                        $sub.css({
                            top: overlapYDown - 30
                        }).fadeIn(options.fadeSpeed);
                    }

                    ////////////////////////////////////////////////////////////
                    const subWidth = $sub.width();

                    const subLeft = $sub.offset().left;
                    const collision = (subWidth + subLeft) > window.innerWidth;

                    if (collision) {
                        $sub.addClass('drop-left');
                    }
                } catch (e) {
                    logError(e);
                }
            });
        },
        onClose: function (e) {
            this._rootMenu && this._rootMenu.parent().removeClass('open');
        },
        onOpen: function () {
            this._rootMenu && this._rootMenu.parent().addClass('open');
        },
        isLeftToRight: function () {
            return false;
        },
        getDefaultOptions: function () {
            return {
                fadeSpeed: 0,
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: true
            };
        },
        buildMenuItems: function ($menu, data, id, subMenu, addDynamicTag) {
            //this._debugMenu && console.log('build - menu items ', arguments);
            let linkTarget = '';

            const self = this;
            const visibility = this.visibility;

            data.forEach((item, i) => {
                let $sub;
                const widget = item.widget;

                if (typeof item.divider !== 'undefined' && !item.widget) {
                    let divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    divider += '"></li>';
                    item.widget = divider;
                    $menu.append(divider);
                    divider.data('item',item);

                } else if (typeof item.header !== 'undefined' && !item.widget) {
                    let header = item.vertical ? '<li class="divider-vertical' : '<li class="nav-header testClass';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + item.header + '</li>';
                    header = $(header);
                    item.widget = header;
                    $menu.append(header);
                    header.data('item',item);

                } else if (typeof item.menu_item_src !== 'undefined') {

                } else {

                    if (!widget && typeof item.target !== 'undefined') {
                        linkTarget = ' target="' + item.target + '"';
                    }
                    if (typeof item.subMenu !== 'undefined' && !widget) {
                        let sub_menu = '<li tabindex="-1" class="dropdown-submenu ' + this.containerClass;
                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a>';

                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }
                        sub_menu += item.text + '';
                        sub_menu += '</a></li>';
                        $sub = $(sub_menu);

                    } else {
                        if (!widget) {
                            if (item.render) {
                                $sub = item.render(item, $menu);
                            } else {
                                let element = '<li tabindex="-1" class="" ';
                                element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                                element += '><a >';
                                if (typeof data[i].icon !== 'undefined') {
                                    element += '<span class="' + item.icon + '"></span> ';
                                }
                                element += item.text + '</a></li>';
                                $sub = $(element);
                                if (item.postRender) {
                                    item.postRender($sub);
                                }
                            }
                        }
                    }

                    if (typeof item.action !== 'undefined' && !item.widget) {
                        if (item.addClickHandler && item.addClickHandler() === false) {
                        } else {
                            const $action = item.action;
                            if ($sub && $sub.find) {
                                const trigger = $sub.find('a');
                                trigger.addClass('context-event');
                                const handler = createCallback($action, item, $sub);
                                trigger.data('handler',handler).on('click',handler);
                            }
                        }
                    }

                    if ($sub && !widget) {

                        item.widget = $sub;
                        $sub.menu = $menu;
                        $sub.data('item', item);

                        item.$menu = $menu;
                        item.$sub = $sub;

                        item._render = function () {
                            if (item.index === 0) {
                                this.$menu.prepend(this.$sub);
                            } else {
                                this.$menu.append(this.$sub);
                            }
                        };
                        if (!item.lazy) {
                            item._render();
                        }
                    }

                    if ($sub) {
                        $sub.attr('level', item.level);
                    }

                    if (typeof item.subMenu != 'undefined' && !item.subMenuData) {
                        const subMenuData = self.buildMenu(item.subMenu, id, true);
                        $menu.subMenuData = subMenuData;
                        item.subMenuData = subMenuData;
                        $menu.find('li:last').append(subMenuData);
                        subMenuData.attr('level', item.subMenu.level);
                        if (self.hideSubsFirst) {
                            subMenuData.css('display', 'none');
                        }

                        const labelLocalized = self.localize(item.text);
                        const title = labelLocalized;
                        $menu.data('item', item);
                        //subMenuData.attr("title",labelLocalized);

                    } else {
                        if (item.subMenu && item.subMenuData) {
                            this.buildMenuItems(item.subMenuData, item.subMenu, id, true);
                        }
                    }
                }

                if (!$menu._didOnClick) {
                    $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                        e.stopPropagation();
                    });
                    $menu._didOnClick = true;
                }
            });

            return $menu;
        },
        buildMenu: function (data, id, subMenu) {
            const subClass = (subMenu) ? (' dropdown-context-sub ' + this.containerClass ) : ' scrollable-menu ';
            const $menu = $('<ul tabindex="-1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>');
            if (!subMenu) {
                this._rootMenu = $menu;
            }
            const result = this.buildMenuItems($menu, data, id, subMenu);
            $menu.data('data', data);
            return result;
        },
        createNewAction: function (command) {
            const segments = command.split('/');
            const lastSegment = segments[segments.length - 1];
            const action = new Action({
                command: command,
                label: lastSegment,
                group: lastSegment,
                dynamic: true
            });
            return action;
        },
        findAction: function (command) {
            const stores = this.actionStores;
            let action = null;
            _.each(stores, function (store) {
                const _action = store ? store.getSync(command) : null;
                if (_action) {
                    action = _action;
                }
            });

            return action;
        },
        getAction: function (command, store) {
            store = store || this.store;
            let action = null;
            if (store) {
                action = this.findAction(command);
                if (!action) {
                    action = this.createNewAction(command);
                }
            }
            return action;
        },
        getActions: function (query) {
            let result = [];
            const stores = this.actionStores;
            const visibility = this.visibility;
            query = query || this.actionFilter;

            _.each(stores, function (store) {
                if (store) {//tmpFix
                    result = result.concat(store.query(query));
                }
            });
            result = result.filter(function (action) {
                const actionVisibility = action.getVisibility != null ? action.getVisibility(visibility) : {};
                return !(action.show === false || actionVisibility === false || actionVisibility.show === false);

            });

            return result;
        },
        toActions: function (commands, store) {
            const result = [];
            const self = this;
            _.each(commands, function (path) {
                const _action = self.getAction(path, store);
                _action && result.push(_action);
            });
            return result;
        },
        onRunAction: function (action, owner, e) {
            const command = action.command;
            action = this.findAction(command);
            return DefaultActions.defaultHandler.apply(action.owner || owner, [action, e]);
        },
        getActionProperty: function (action, visibility, prop) {
            let value = prop in action ? action[prop] : null;
            if (visibility && prop in visibility) {
                value = visibility[prop];
            }
            return value;
        },
        toMenuItem: function (action, owner, label, icon, visibility, showKeyCombo, lazy) {
            const self = this;
            const labelLocalized = self.localize(label);
            const actionType = visibility.actionType || action.actionType;

            const item = {
                text: labelLocalized,
                icon: icon,
                data: action,
                owner: owner,
                command: action.command,
                lazy: lazy,
                addClickHandler: function () {
                    return actionType !== types.ACTION_TYPE.MULTI_TOGGLE;

                },
                render: function (data, $menu) {
                    if (self.renderItem) {
                        return self.renderItem(this, data, $menu, this.data, owner, label, icon, visibility, showKeyCombo, lazy);
                    }
                    const action = this.data;
                    const parentAction = action.getParent ? action.getParent() : null;
                    const closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
                    let keyComboString = ' \n';
                    let element = null;
                    if (action.keyboardMappings && showKeyCombo !== false) {
                        const mappings = action.keyboardMappings;
                        const keyCombos = mappings[0].keys;
                        if (keyCombos && keyCombos.length) {
                            keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                        }
                    }

                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        element = '<li tabindex="-1" class="" >';
                        const id = action._store.id + '_' + action.command + '_' + self.id;
                        const checked = action.get('value');
                        //checkbox-circle
                        element += '<div class="action-checkbox checkbox checkbox-success ">';
                        element += '<input id="' + id + '" type="checkbox" ' + (checked === true ? 'checked' : '') + '>';
                        element += '<label for="' + id + '">';
                        element += self.localize(data.text);
                        element += '</label>';
                        element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                        element += '</div>';

                        $menu.addClass('noclose');
                        const result = $(element);
                        const checkBox = result.find('INPUT');
                        checkBox.on('change', function (e) {
                            action._originReference = data;
                            action._originEvent = e;
                            action.set('value', checkBox[0].checked);
                            action._originReference = null;
                        });
                        self.setVisibilityField(action, 'widget', data);
                        return result;
                    }
                    closeOnClick === false && $menu.addClass('noclose');
                    if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                        const value = action.value || action.get('value');
                        const parentValue = parentAction.get('value');
                        if (value == parentValue) {
                            icon = 'fa fa-check';
                        }
                    }

                    const title = data.text || labelLocalized || self.localize(action.title);


                    //default:
                    element = '<li tabindex="-1"><a title="' + title + ' ' + keyComboString + '">';
                    const _icon = data.icon || icon;

                    //icon
                    if (typeof _icon !== 'undefined') {
                        //already html string
                        if (/<[a-z][\s\S]*>/i.test(_icon)) {
                            element += _icon;
                        } else {
                            element += '<span class="icon ' + _icon + '"/> ';
                        }
                    }
                    element += data.text;
                    element += '<span style="max-width:100px" class="text-muted pull-right ellipsis keyboardShortCut">' + (showKeyCombo ? keyComboString : "") + '</span></a></li>';
                    self.setVisibilityField(action, 'widget', data);
                    return $(element);
                },
                get: function (key) {
                },
                set: function (key, value) {
                    //_debugWidgets && _.isString(value) && console.log('set ' + key + ' ' + value);
                    const widget = this.widget;

                    function updateCheckbox(widget, checked) {
                        const what = widget.find("input[type=checkbox]");
                        if (what) {
                            if (checked) {
                                what.prop("checked", true);
                            } else {
                                what.removeAttr('checked');
                            }
                        }
                    }

                    if (widget) {
                        if (key === 'disabled') {
                            if (widget.toggleClass) {
                                widget.toggleClass('disabled', value);
                            }
                        }
                        if (key === 'icon') {
                            const _iconNode = widget.find('.icon');
                            if (_iconNode) {
                                _iconNode.attr('class', 'icon');
                                this._lastIcon = this.icon;
                                this.icon = value;
                                _iconNode.addClass(value);
                            }
                        }
                        if (key === 'value') {
                            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE ||
                                actionType === types.ACTION_TYPE.SINGLE_TOGGLE) {
                                updateCheckbox(widget, value);
                            }
                        }
                    }
                },
                action: function (e, data, menu) {
                    _debug && console.log('menu action', data);
                    return self.onRunAction(data.data, owner, e);
                },
                destroy: function () {
                    if (this.widget) {
                        this.widget.remove();
                    }
                }
            };
            return item;
        },
        attach: function (selector, data) {
            this.target = selector;
            this.menu = this.addContext(selector, data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
            return this.menu;
        },
        addReference: function (action, item) {
            if (action.addReference) {
                action.addReference(item, {
                    properties: {
                        "value": true,
                        "disabled": true,
                        "enabled": true
                    }
                }, true);
            }
        },
        onDidRenderActions: function (store, owner) {
            if (owner && owner.refreshActions) {
                owner.refreshActions();
            }
        },
        getActionData: function (action) {
            const actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label: actionVisibility.label != null ? actionVisibility.label : action.label,
                icon: actionVisibility.icon != null ? actionVisibility.icon : action.icon,
                command: actionVisibility.command != null ? actionVisibility.command : action.command,
                visibility: actionVisibility,
                group: actionVisibility.group != null ? actionVisibility.group : action.group,
                tab: actionVisibility.tab != null ? actionVisibility.tab : action.tab,
                expand: actionVisibility.expand != null ? actionVisibility.expand : false,
                widget: actionVisibility.widget
            };
        },
        _clearAction: function (action) {

        },
        _findParentData: function (oldMenuData, parentCommand) {
            const parent = _.find(oldMenuData, {
                command: parentCommand
            });
            if (parent) {
                return parent;
            }
            for (let i = 0; i < oldMenuData.length; i++) {
                const data = oldMenuData[i];
                if (data.subMenu) {
                    const found = this._findParentData(data.subMenu, parentCommand);
                    if (found) {
                        return found;
                    }
                }
            }
            return null;
        },
        _clear: function () {
            let actions = this.getActions();
            const store = this.store;
            if (store) {
                this.actionStores.remove(store);
            }
            const self = this;
            actions = actions.concat(this._tmpActions);
            _.each(actions, function (action) {
                if (action) {
                    const actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        const widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                        delete actionVisibility.widget;
                        actionVisibility.widget = null;
                    }
                }
            });
            this.$navBar && this.$navBar.empty();
        },
        buildActionTree: function (store, owner) {
            const self = this;
            const allActions = self.getActions();
            const visibility = self.visibility;

            self.wireStore(store, function (evt) {
                if (evt.type === 'update') {
                    const action = evt.target;
                    if (action.refreshReferences) {
                        action.refreshReferences(evt.property, evt.value);
                    }
                }
            });

            //return all actions with non-empty tab field
            const tabbedActions = allActions.filter(function (action) {
                    const _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                    if (action) {
                        return _vis.tab || action.tab;
                    }
                });

            const //group all tabbed actions : { Home[actions], View[actions] }
            groupedTabs = _.groupBy(tabbedActions, function (action) {
                const _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                if (action) {
                    return _vis.tab || action.tab;
                }
            });

            let //now flatten them
            _actionsFlattened = [];


            _.each(groupedTabs, function (items) {
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            let rootActions = [];
            _.each(tabbedActions, function (action) {
                const rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            //owner sort of top level
            store.menuOrder && (rootActions = owner.sortGroups(rootActions, store.menuOrder));

            const tree = {};
            //stats to count groups per tab
            let biggestTab = rootActions[0];
            let nbGroupsBiggest = 0;

            _.each(rootActions, function (level) {
                // collect all actions at level (File/View/...)
                let menuActions = owner.getItemsAtBranch(allActions, level);
                // convert action command strings to Action references
                let grouped = self.toActions(menuActions, store);

                // expand filter -------------------
                let addedExpanded = [];
                const toRemove = [];
                _.each(grouped, function (action) {
                    const actionData = self.getActionData(action);
                    if (actionData.expand) {
                        const children = action.getChildren();
                        children && children.length && (addedExpanded = addedExpanded.concat(children));
                        toRemove.push(action);
                    }
                });
                grouped = grouped.concat(addedExpanded);
                grouped = grouped.filter(function (action) {
                    return !toRemove.includes(action);
                });
                // expand filter ---------------    end

                // group all actions by group
                const groupedActions = _.groupBy(grouped, function (action) {
                    const _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                    if (action) {
                        return _vis.group || action.group;
                    }
                });

                let _actions = [];
                _.each(groupedActions, function (items, level) {
                    if (level !== 'undefined') {
                        _actions = _actions.concat(items);
                    }
                });

                //flatten out again
                menuActions = _.pluck(_actions, 'command');
                menuActions.grouped = groupedActions;
                tree[level] = menuActions;

                //update stats
                if (self.collapseSmallGroups) {
                    const nbGroups = _.keys(menuActions.grouped).length;
                    if (nbGroups > nbGroupsBiggest) {
                        nbGroupsBiggest = nbGroups;
                        biggestTab = level;
                    }
                }
            });

            //now move over any tab with less than 2 groups to the next bigger tab
            this.collapseSmallGroups && _.each(tree, function (actions, level) {
                if (_.keys(actions.grouped).length < self.collapseSmallGroups) {
                    //append tab groups of the biggest tab
                    tree[biggestTab] && _.each(actions.grouped, function (group, name) {
                        tree[biggestTab].grouped[name] = group;
                    });
                    //copy manually commands to that tab
                    tree[biggestTab] && _.each(actions, function (action) {
                        tree[biggestTab].push(action);
                    });
                    tree[biggestTab] && delete tree[level];
                }
            });
            const result = {
                root: tree,
                rootActions: rootActions,
                allActionPaths: _.pluck(allActions, 'command'),
                allActions: allActions
            };

            this.lastTree = result;
            return result;
        }
    });

    dcl.chainAfter(MenuMixinClass,'destroy');

    const _debugTree =false;
    const _debugMenuData = false;
    const _debugOldMenuData = false;
    const _debugWidgets = true;
    const ActionRendererClass =dcl(null, {
        renderTopLevel: function (name, where) {
            where = where || $(this.getRootContainer());
            const item = $('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) + '<b class="caret"></b></a>' +
                '</li>');

            where.append(item);
            return item;

        },
        getRootContainer: function () {
            return this.navBar;
        }

    });


    const KeyboardControl = dcl(null,{
        owner:null,
        setup:function(owner){
            this.owner = owner;
        },
        /**
         * Return parent action container for an element
         * @param $el {jQueryObject}
         * @param $max {jQueryObject}
         * @param dataIdentifier {string} The data identifier to find some object in $el
         * @returns {jQueryObject}
         */
        findNavigationContainerElement:function($el,$max,dataIdentifier){
            if(!$el || !$el[0]){
                return;
            }
            if($el && $max && $el[0]==$max[0]){
                return $el;
            }

            const data = $el.data(dataIdentifier || 'data');
            if(data && data!=null){
                return $el;
            }
            return this.findNavigationContainerElement($el.parent(),$max,dataIdentifier);
        },
        /**
         *
         * @param $el
         * @param $max
         * @param dataIdentifier
         * @returns {object}
         */
        findNavigationElement:function($el,$max,dataIdentifier){
            if(!$el || !$el[0]){
                return;
            }
            if($el && $max && $el[0]==$max[0]){
                return $el;
            }
            const data = $el.data(dataIdentifier || 'item') || $el.data(dataIdentifier || 'data');
            if(data && !_.isEmpty(data)){
                return $el;
            }
            return this.findNavigationElement($el.parent());
        },
        /**
         *
         * @param $el {jQueryObject}
         * @param $max {jQueryObject}
         * @param dataIdentifier {string} The data identifier to find some object in $el
         * @param findParent
         * @returns {{element: Object, data: (*|{}), parent: *}}
         */
        toNavigationData:function($el,$max,dataIdentifier,findParent){
            const element = this.findNavigationElement($el,$max,dataIdentifier);
            if(element) {
                const data = element.data(dataIdentifier||'item')||element.data(dataIdentifier||'data');
                if (data) {
                    return {
                        element: element,
                        data: data,
                        parent: findParent !== false ? this.findNavigationContainerElement($el, $max, dataIdentifier) : null
                    };
                }
            }
        },
        /**
         *
         * @param $parent
         * @param $origin
         * @param direction
         * @returns {*}
         */
        next:function($parent,$origin,direction){
            if($origin) {
                const result = $origin[direction == 1 ? 'next' : 'prev']();
                if (result.hasClass('nav-header')) {
                    return this.next($parent, result, direction);
                }else{
                    return result;
                }
            }
        },
        /**
         * Return valid children
         * @param $parent
         * @returns {*}
         */
        children:function($parent){
            return $parent.children('LI').filter(function(i,child){
                return $(child).hasClass('divider') !==true && $(child).hasClass('nav-header') !==true;
            });
        },
        close:function($el){
            const _parent = $el.parent();
            const _parentParent = $el.parent().parent();
            _parent.css('display','');
            _parent.removeClass('open');
            $el.removeClass('open');
            $el.removeClass('focus');
            _parentParent.removeClass('open');
            _parentParent.focus();
        },
        /**
         * Opens the very root menu by a given origin
         * @param $el
         * @param $next
         * @returns {*}
         */
        openRoot:function($el,$next,direction){
            const _next = $next || this.topMost($el).parent()[direction===-1 ? 'prev' : 'next']();
            const _trigger =$(_next.find('A')[0]);
            _trigger.trigger('click');
            const _navData = this.toNavigationData($(_next.find('UL')[0]),this.owner.getRootContainer());
            if(_navData) {
                return this.activate($(children(_navData.element)[0]), _navData.element, true);
            }
        },
        open:function ($el){
            $el.css('display','block');
            let _navData = this.toNavigationData($el,this.owner.getRootContainer(),'data',null,null);
            const firstInner = this.children(_navData.parent)[0];
            if(firstInner){
                _navData = this.toNavigationData($(firstInner),this.owner.getRootContainer());
                this.activate(_navData.element,_navData.parent,true);
            }
        },
        topMost:function ($el){
            if($el) {
                const data = $el.data();
                if(data.item||data.data){
                    const next = $el.parent();
                    const parentData = next.data();
                    if(next && next[0] && (parentData.item||parentData.data)){
                        return this.topMost(next);
                    }
                    return $el;
                }
            }
        },
        keyboardHandler:function(event,$parent){
            const origin = $parent.data('currentTarget');
            if(event.keyCode===13){
                const trigger = origin.find("A");
                const handler = trigger.data('handler');
                if(handler){
                    const actionResult = handler();
                    if(actionResult && actionResult.then){
                        actionResult.then(function(){
                            origin.focus();
                        });
                    }
                    return;
                }
                //perform action
                origin.find("A").click();
                origin.focus();
                return;
            }

            const vertical = event.keyCode == 38 || event.keyCode == 40 || event.keyCode == 36 || event.keyCode == 35;
            const horizontal = event.keyCode == 37 || event.keyCode == 39;
            const direction = vertical ? (event.keyCode == 38 || event.keyCode == 36) ? -1 : 1 : (event.keyCode == 37 ? -1 :1 );
            const max = !!(event.keyCode == 36 || event.keyCode == 35 );

            let nextElement = null;
            let nextData = {};
            let navData = null;
            if(vertical) {
                nextElement = max ? direction == 1 ? $(_.last(this.children($parent))) : $(_.first(this.children($parent))) : this.next($parent,origin,direction);
                nextElement && (nextData = nextElement.data('item'));
            }
            if(horizontal) {
                const data = origin.data('item');
                if(data){
                    if(direction>0) {
                        //sub - items?, open them
                        if(data.subMenuData) {
                            const isOpen = data.subMenuData.css('display') ==='block';
                            if(!isOpen) {
                                return this.open(data.subMenuData);
                            }
                        }else{
                            //root bounce
                            if(this.openRoot(origin,null,1)){
                                return;
                            }
                        }
                    }else{
                        //left
                        this.close(origin);
                        navData = this.toNavigationData(origin,this.owner.getRootContainer());
                        const parentParentData =navData.parent.parent().data();
                        //root bounce
                        if(_.isEmpty(parentParentData)){
                            return this.openRoot(origin,null,-1);
                        }
                        return;
                    }
                }
            }
            if(nextElement && nextData){
                navData = this.toNavigationData(nextElement,this.owner.getRootContainer());
                this.activate(navData.element,navData.parent,true);
            }
        },
        initContainer:function($container){
            const self = this;
            if(!$container.data('didSetup')) {
                $container.data('didSetup',true);
                $container.on('keydown',function(evt){
                    if(($(evt.target).parent()[0]==$container[0])){
                        self.keyboardHandler(evt, $container);
                    }
                });
            }
        },
        activate:function($next,$parent,clear){
            if($parent){
                this.initContainer($parent);
            }
            if(clear && $parent) {
                this.children($parent).each(function (i,c) {
                    $(c).removeClass('focus');
                    $(c).removeClass('open');
                    const _s = $(c).find('.dropdown-context-sub');
                    if(_s[0] && _s.css('display')==='block') {
                        _s.css('display','');
                        _s.removeClass('open');
                    }
                });
            }
            $next.addClass('focus');
            $next.focus();
            $parent.addClass('open');
            $parent.data('currentTarget',$next);
            return true;
        },
        clear:function($parent){
            this.children($parent).each(function (i,c) {
                $(c).removeClass('focus');
                $(c).removeClass('open');
                const _s = $(c).find('.dropdown-context-sub');
                if(_s[0] && _s.css('display')==='block') {
                    _s.css('display','');
                    _s.removeClass('open');
                }
            });
        }
    });

    /**
     * Return parent action container for an element
     * @param $el {jQueryObject}
     * @param $max {jQueryObject}
     * @param dataIdentifier {string} The data identifier to find some object in $el
     * @returns {jQueryObject}
     */
    function findNavigationContainerElement($el,$max,dataIdentifier){
        if(!$el || !$el[0]){
            return;
        }
        if($el && $max && $el[0]==$max[0]){
            return $el;
        }

        const data = $el.data(dataIdentifier || 'data');
        if(data && data!=null){
            return $el;
        }
        return findNavigationContainerElement($el.parent(),$max,dataIdentifier);
    }
    /**
     *
     * @param $el
     * @param $max
     * @param dataIdentifier
     * @returns {object}
     */
    function findNavigationElement($el,$max,dataIdentifier){
        if(!$el || !$el[0]){
            return;
        }
        if($el && $max && $el[0]==$max[0]){
            return $el;
        }
        const data = $el.data(dataIdentifier || 'item') || $el.data(dataIdentifier || 'data');
        if(data && !_.isEmpty(data)){
            return $el;
        }

        return findNavigationElement($el.parent());
    }
    /**
     *
     * @param $el {jQueryObject}
     * @param $max {jQueryObject}
     * @param dataIdentifier {string} The data identifier to find some object in $el
     * @param findParent
     * @returns {{element: Object, data: (*|{}), parent: *}}
     */
    function toNavigationData($el,$max,dataIdentifier,findParent){
        const element = findNavigationElement($el,$max,dataIdentifier);
        if(element) {
            const data = element.data(dataIdentifier||'item')||element.data(dataIdentifier||'data');
            if (data) {
                return {
                    element: element,
                    data: data,
                    parent: findParent !== false ? findNavigationContainerElement($el, $max, dataIdentifier) : null
                };
            }
        }
    }
    /**
     *
     * @param $parent
     * @param $origin
     * @param direction
     * @returns {*}
     */
    function next($parent,$origin,direction){
        if($origin) {
            const result = $origin[direction == 1 ? 'next' : 'prev']();
            if (result.hasClass('nav-header')) {
                return next($parent, result, direction);
            }else{
                return result;
            }
        }
    }
    /**
     * Return valid children
     * @param $parent
     * @returns {*}
     */
    function children($parent){
        return $parent.children('LI').filter(function(i,child){
            return $(child).hasClass('divider') !==true && $(child).hasClass('nav-header') !==true;
        });
    }

    function close($el){
        const _parent = $el.parent();
        const _parentParent = $el.parent().parent();
        _parent.css('display','');
        _parent.removeClass('open');
        $el.removeClass('open');
        $el.removeClass('focus');
        _parentParent.removeClass('open');
        _parentParent.focus();
    }

    /**
     * Opens the very root menu by a given origin
     * @param $el
     * @param $next
     * @returns {*}
     */
    function openRoot($el,$next,direction){
        const _next = $next || topMost($el).parent()[direction===-1 ? 'prev' : 'next']();
        const _trigger =$(_next.find('A')[0]);
        _trigger.trigger('click');

        const _navData = toNavigationData($(_next.find('UL')[0]),this.getRootContainer());
        if(_navData) {
            return activate.apply(this, [$(children(_navData.element)[0]), _navData.element, true]);
        }

    }

    /**
     * Open sub menu
     * @param $el
     */
    function open($el){
        $el.css('display','block');
        let _navData = toNavigationData($el,this.menu,'data',null,null);
        const firstInner = children(_navData.parent)[0];
        if(firstInner){
            _navData = toNavigationData($(firstInner),this.menu);
            activate.apply(this,[_navData.element,_navData.parent,true]);
        }
    }

    /**
     * Recursive search for the most top navigation object
     * @param $el
     * @returns {*}
     */
    function topMost($el){
        if($el) {
            const data = $el.data();
            if(data.item||data.data){
                const next = $el.parent();
                const parentData = next.data();
                if(next && next[0] && (parentData.item||parentData.data)){
                    return topMost(next);
                }
                return $el;
            }
        }
    }
    /**
     * Keyboard handler
     * @param event
     * @param $parent
     * @returns {*}
     */
    function keyboardHandler(event,$parent){
        const origin = $parent.data('currentTarget');
        if(event.keyCode===13){
            const trigger = origin.find("A");
            const handler = trigger.data('handler');
            if(handler){
                const actionResult = handler();
                if(actionResult && actionResult.then){
                    actionResult.then(function(){
                        origin.focus();
                    });
                }
                return;
            }
            //perform action
            origin.find("A").click();
            origin.focus();
            return;
        }

        const vertical = event.keyCode == 38 || event.keyCode == 40 || event.keyCode == 36 || event.keyCode == 35;
        const horizontal = event.keyCode == 37 || event.keyCode == 39;
        const direction = vertical ? (event.keyCode == 38 || event.keyCode == 36) ? -1 : 1 : (event.keyCode == 37 ? -1 :1 );
        const max = !!(event.keyCode == 36 || event.keyCode == 35 );

        let nextElement = null;
        let nextData = {};
        let navData = null;
        if(vertical) {
            nextElement = max ? direction == 1 ? $(_.last(children($parent))) : $(_.first(children($parent))) : next($parent,origin,direction);
            nextElement && (nextData = nextElement.data('item'));
        }
        if(horizontal) {
            const data = origin.data('item');
            if(data){
                if(direction>0) {
                    //sub - items?, open them
                    if(data.subMenuData) {
                        const isOpen = data.subMenuData.css('display') ==='block';
                        if(!isOpen) {
                            return open.apply(this,[data.subMenuData]);
                        }
                    }else{
                        //root bounce
                        if(openRoot.apply(this,[origin,null,1])){
                            return;
                        }
                    }
                }else{
                    //left
                    close(origin);
                    navData = toNavigationData(origin,this.getRootContainer());
                    const parentParentData =navData.parent.parent().data();
                    //root bounce
                    if(_.isEmpty(parentParentData)){
                        return openRoot.apply(this,[origin,null,-1]);
                    }
                    return;
                }
            }
        }
        if(nextElement && nextData){
            navData = toNavigationData(nextElement,this.menu);
            activate.apply(this,[navData.element,navData.parent,true]);
        }

    }
    function initContainer($container){
        const self = this;
        if(!$container.data('didSetup')) {
            $container.data('didSetup',true);
            $container.on('keydown',function(evt){
                if(($(evt.target).parent()[0]==$container[0])){
                    keyboardHandler.apply(self, [evt, $container]);
                }
            });
        }
    }
    function ensureParent($parent){
        if($parent && $parent[0]) {
            const parent = $parent.parent();
            if(parent && parent[0] && parent!=$parent) {
                const data = toNavigationData(parent, this.menu, 'data');
                if (data && data.element) {
                    data.element.addClass('open');
                    if(data.element!=data.parent) {
                        ensureParent.apply(this,[data.parent]);
                    }
                }
            }
        }
    }
    function activate($next,$parent,clear){
        if($parent){
            initContainer.apply(this,[$parent]);
        }
        if(clear && $parent) {
            children($parent).each(function (i,c) {
                $(c).removeClass('focus');
                $(c).removeClass('open');
                const _s = $(c).find('.dropdown-context-sub');
                if(_s[0] && _s.css('display')==='block') {
                    _s.css('display','');
                    _s.removeClass('open');
                }
            });
        }
        $next.addClass('focus');
        $next.focus();
        $parent.addClass('open');
        $parent.data('currentTarget',$next);
        return true;
    }

    const ContextMenu = dcl([_Widget.dcl, ActionContext.dcl, ActionMixin.dcl, ActionRendererClass,MenuMixinClass,_XWidget.StoreMixin], {
        target: null,
        openTarget:null,
        visibility: types.ACTION_VISIBILITY.CONTEXT_MENU,
        correctSubMenu:true,
        limitTo:null,
        declaredClass:'xide.widgets.ContextMenu',
        menuData:null,
        addContext: function (selector, data) {
            this.menuData = data;
            let id;
            let $menu;
            const self = this;
            const target = this.openTarget ? (this.openTarget) : $(self.target);

            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = self.buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {
                const d = new Date();
                id = d.getTime();
                $menu = self.buildMenu(data, id);
                selector.append($menu);
            }

            const options = this.getDefaultOptions();

            this.keyboardController = new KeyboardControl();
            this.keyboardController.setup(this);

            function mouseEnterHandlerSubs(e){
                const navigationData = this.keyboardController.toNavigationData($(e.target),this.getRootContainer());
                if(!navigationData) {
                    return;
                }
                this.keyboardController.clear(navigationData.parent);
                this.menu.focus();
                navigationData.element.focus();
                this.menu.data('currentTarget',navigationData.element);

            }
            function setupContainer($container){
                self.__on($container, 'mouseenter', 'LI', mouseEnterHandlerSubs.bind(self));
            }

            function constextMenuHandler(e) {
                if(self.limitTo){
                    let $target = $(e.target);
                    $target = $target.parent();
                    if(!$target.hasClass(self.limitTo)){
                        return;
                    }
                }
                self.openEvent = e;
                self.isOpen = true;
                this.lastFocused = document.activeElement;
                self.onOpen(e);
                e.preventDefault();
                e.stopPropagation();
                $('.dropdown-context:not(.dropdown-context-sub)').hide();

                const $dd = $('#dropdown-' + id);
                $dd.css('zIndex',_Popup.nextZ(1));
                if(!$dd.data('init')){
                    $dd.data('init',true);
                    setupContainer($dd);
                    self.keyboardController.initContainer($dd);
                }

                if (typeof options.above == 'boolean' && options.above) {
                    $dd.css({
                        top: e.pageY - 20 - $('#dropdown-' + id).height(),
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);

                } else if (typeof options.above == 'string' && options.above == 'auto') {
                    $dd.removeClass('dropdown-context-up');
                    const autoH = $dd.height() + 0;
                    if ((e.pageY + autoH) > $('html').height()) {
                        let top = e.pageY - 20 - autoH;
                        if(top < 0){
                            top = 20;
                        }
                        $dd.css({
                            top: top,
                            left: e.pageX - 13
                        }).fadeIn(options.fadeSpeed);

                    } else {
                        $dd.css({
                            top: e.pageY + 10,
                            left: e.pageX - 13
                        }).fadeIn(options.fadeSpeed);
                    }
                }

                if (typeof options.left == 'boolean' && options.left) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width()
                    }).fadeIn(options.fadeSpeed);
                } else if (typeof options.left == 'string' && options.left == 'auto') {
                    $dd.removeClass('dropdown-context-left');
                    const autoL = $dd.width() - 12;
                    if ((e.pageX + autoL) > $('html').width()) {
                        $dd.addClass('dropdown-context-left').css({
                            left: e.pageX - $dd.width() + 13
                        });
                    }
                }
                this.keyboardController.activate($(this.keyboardController.children($dd)[0]),$dd);
            }
            this.__on(target, 'contextmenu', null,constextMenuHandler.bind(this));

            this.__on($menu,'keydown',function(e){
                if(e.keyCode==27){
                    const navData = this.keyboardController.toNavigationData($(e.target),this.getRootContainer());
                    navData && navData.element && this.keyboardController.close(navData.element);
                    $(this.lastFocused).focus();
                }
            }.bind(this));

            return $menu;
        },
        onRootAction:function(){
            return null;
        },
        buildMenu:function (data, id, subMenu,update) {
            const subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ';
            const menuString = '<ul tabindex="-1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>';
            const $menu = update ? (this._rootMenu || $(menuString)) : $(menuString);

            if(!subMenu){
                this._rootMenu = $menu;
                this._rootMenu.addClass('contextMenu')
            }
            $menu.data('data', data);
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        onActionAdded:function(actions){
            this.setActionStore(this.getActionStore(), this,false,true,actions);
        },
        clearAction : function(action){
            const self = this;
            if(action) {
                const actionVisibility = action.getVisibility !== null ? action.getVisibility(self.visibility) : {};
                if (actionVisibility) {
                    const widget = actionVisibility.widget;
                    action.removeReference && action.removeReference(widget);
                    if (widget && widget.destroy) {
                        widget.destroy();
                    }
                    delete actionVisibility.widget;
                    actionVisibility.widget = null;
                }
            }
        },
        onActionRemoved:function(evt){
            this.clearAction(evt.target);
        },
        removeCustomActions:function(){
            const oldStore = this.store;
            if(!oldStore){
                console.warn('removeCustomActions : have no store');
                return;
            }

            const oldActions = oldStore.query({
                    custom:true
                });

            const menuData=this.menuData;

            _.each(oldActions,function(action){

                oldStore.removeSync(action.command);

                const oldMenuItem = _.find(menuData,{
                    command:action.command
                });
                oldMenuItem && menuData.remove(oldMenuItem);
            });
        },
        setActionStore: function (store, owner,subscribe,update,itemActions) {
            if(!update){
                this._clear();
                this.addActionStore(store);
            }

            const self = this;
            const visibility = self.visibility;
            const rootContainer = $(self.getRootContainer());
            const tree = update ? self.lastTree : self.buildActionTree(store,owner);

            const allActions = tree.allActions;
            const rootActions = tree.rootActions;
            const allActionPaths = tree.allActionPaths;
            const oldMenuData = self.menuData;

            this.store = store;

            const data = [];


            if(subscribe!==false) {
                if(!this['_handleAdded_' + store.id]) {
                    this.addHandle('added', store._on('onActionsAdded', function (actions) {
                        self.onActionAdded(actions);
                    }));

                    this.addHandle('delete', store.on('delete', function (evt) {
                        self.onActionRemoved(evt);
                    }));
                    this['_handleAdded_' + store.id]=true;
                }
            }

            if(!update) {
                _.each(tree.root, function (menuActions, level) {
                    const root = self.onRootAction(level, rootContainer);
                    let lastGroup = '';

                    let lastHeader = {
                        header: ''
                    };

                    const groupedActions = menuActions.grouped;

                    _.each(menuActions, function (command) {
                        let action = self.getAction(command, store);
                        let isDynamicAction = false;

                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }
                        if (action) {
                            const renderData = self.getActionData(action);
                            const icon = renderData.icon;
                            const label = renderData.label;
                            const visibility = renderData.visibility;
                            const group = renderData.group;

                            if(visibility.widget){
                                return;
                            }
                            if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 1) {
                                if (lastGroup !== group) {
                                    const name = groupedActions[group].length >= 2 ? i18.localize(group) : "";
                                    lastHeader = {header: name};
                                    data.push(lastHeader);
                                    lastGroup = group;
                                }
                            }
                            const item = self.toMenuItem(action, owner, label, icon, visibility || {},true);
                            data.push(item);
                            visibility.widget = item;
                            self.addReference(action, item);
                            function parseChildren(command, parent) {
                                const childPaths = new Path(command).getChildren(allActionPaths, false);
                                const isContainer = childPaths.length > 0;
                                const childActions = isContainer ? self.toActions(childPaths, store) : null;
                                if (childActions) {
                                    const subs = [];
                                    _.each(childActions, function (child) {
                                        const _renderData = self.getActionData(child);
                                        const _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility,true);
                                        self.addReference(child, _item);
                                        subs.push(_item);

                                        const _childPaths = new Path(child.command).getChildren(allActionPaths, false);
                                        const _isContainer = _childPaths.length > 0;
                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }

                            parseChildren(command, item);
                        }
                    });
                });
                self.attach($('body'), data);
                self.onDidRenderActions(store, owner);
            }else{

                if(itemActions || !_.isEmpty(itemActions)) {
                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            const action = self.getAction(newAction.command);
                            if (action) {
                                const renderData = self.getActionData(action);
                                const icon = renderData.icon;
                                const label = renderData.label;
                                const aVisibility = renderData.visibility;
                                const group = renderData.group;
                                const item = self.toMenuItem(action, owner, label, icon, aVisibility || {},null,false);

                                aVisibility.widget = item;

                                self.addReference(newAction, item);

                                const parentCommand = action.getParentCommand();
                                const parent = self._findParentData(oldMenuData,parentCommand);
                                if(parent && parent.subMenu){
                                    parent.lazy = true;
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });

                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }
        }
    });

    console.log('--do-tests');

    const actions = [];
    const thiz = this;
    const ACTION_ICON = types.ACTION_ICON;
    let ribbon;
    let CIS;
    const ctx = window.sctx;
    let root;
    function doTests(tab) {

        grid = FTestUtils.createFileGrid('root', {}, {}, 'TestGrid', module.id, true, tab);

        const menuNode = grid.header;
        const context = new ContextMenu({
            //actionStores:[ctx.getActionStore()]
        }, grid.domNode);

        context.openTarget = grid.domNode;
        context.init({preventDoubleContext: false});
        grid.resize();
        tab.add(context, null, false);


        //////////////////////////////////////////////////
        context._registerActionEmitter(grid);

        return;
    }

    function doTestsBlocks(tab) {
        const mixin = {
            onCIChanged: function (ci, block, oldValue, newValue, field, cis) {

                if (oldValue === newValue) {
                    return;
                }
                console.error('on ci change ' + field + ' ' + newValue);
                const _col = this.collection;

                block = this.collection.getSync(block.id);
                block.set(field, newValue);

                this.refreshActions();

                block[field] = newValue;
                _col.emit('update', {
                    target: block,
                    type: 'update'
                });

                block._store.emit('update', {
                    target: block
                });
                this._itemChanged('changed', block, _col);
                block.onChangeField(field, newValue, cis, this);
            }
        };

        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            permissions: [
                ACTION.CLIPBOARD,
                'Step/Properties',
                ACTION.TOOLBAR,
                ACTION.LAYOUT
            ]
        });

        utils.mixin(grid,mixin);

        const context = new ContextMenu({
        }, grid.domNode);

        context.openTarget = grid.domNode;
        context.init({preventDoubleContext: false});
        grid.resize();

        grid.getContextMenu = function(){
            return context;
        }

        tab.add(context, null, false);

        //////////////////////////////////////////////////
        context._registerActionEmitter(grid);

        context.setActionEmitter(grid, 'click');

        function addItem(scope,name){

            const thiz = this;
            const cmd = factory.createBlock(Variable, {
                    name: name || "%%No Title",
                    scope: scope,
                    group: 'click'
                });

            grid.refresh();
        }

        const scope = grid.blockScope;
        const store = scope.blockStore;

        const root  = 'Block/Insert';

        const thiz = grid;

        store.on("added",function(block){

            if(block.declaredClass.includes('model.Variable')){

                const setVariableRoot= root+'/Set Variable';
                const defaultMixin = { addPermission: true };

                const permissions = thiz.permissions;
                const result = [];

                result.push(thiz.createAction({
                    label: block.name,
                    command: ''  + setVariableRoot+'/'+block.name,
                    icon: 'fa-paper-plane',
                    tab: 'Home',
                    group: 'Step',
                    mixin: utils.mixin({
                        item:block,
                        proto:VariableAssignmentBlock,
                        quick:true,
                        ctrArgs: {
                            variable: block.id,
                            variableId: block.id,
                            scope: thiz.blockScope,
                            value: '0'
                        }
                    },defaultMixin)
                }));

                const newStoredActions = thiz.addActions(result);
                const actionStore = grid.getActionStore();
                context.onActionAdded(newStoredActions);
            }
        });

        const actionStore = grid.getActionStore();

        return;
        const blockInsert = grid.getAction('Block/Insert');
        const disable = function (disable) {

            blockInsert.set('disabled', disable);
            setTimeout(function () {
                blockInsert.getReferences().forEach(function (ref) {
                    ref.set('disabled', disable);
                });
            }, 100);

            blockInsert.refresh();

        };



        disable(true);
        setTimeout(function(){
            disable(true);
            return;
        },3000);
        return;
    }



    if (ctx) {
        const parent = TestUtils.createTab(null, null, module.id);
        doTestsBlocks(parent);
        return declare('a', null, {});

    }
    return _Widget;
});




