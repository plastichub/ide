/** @module xgrid/Base **/
define("xide/ribbon/tests/TestRibbon", [
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "xide/tests/TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/_base/_Widget',
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    "xide/model/Path",
    'xfile/tests/TestUtils',
    'xlang/i18',
    "xide/widgets/_MenuMixin4",
    "xblox/tests/TestUtils",
    "xide/layout/_TabContainer",
    "xide/$",
    'xide/popup'
], function (declare,dcl,types,utils,TestUtils,_Widget,module,_XWidget,ActionMixin,ActionContext,
             Path,
             FTestUtils,i18,_MenuMixin4,_TestBlockUtils,
             _TabContainer,$,popup
){
    //http://localhost/projects/x4mm/Code/xapp/xcf/?debug=true&run=run-release-debug&protocols=false&drivers=false&plugins=false&xblox=debug&files=false&dijit=debug&xdocker=debug&xfile=debug&davinci=debug&dgrid=debug&xgrid=debug&xace=debug&xaction=debug&xfile=debug&xideve=debug&davinci=debug&dijit=debug&app=xide&xideve=false&devices=false
    const ACTION = types.ACTION;

    const ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        templateString:'<div class="mainMenu ribbonToolbar" style="min-height: 160px"></div>',
        /**
         * @type {module:xide/layout/_TabContainer}
         */
        tabContainer:null,
        startup:function(){
            //this.tabContainer = this.add(utils.addWidget(_TabContainer,{},this,this,true));
        },
        /**
         * @returns {module:xide/layout/_TabContainer}
         */
        getRootContainer:function(){
            if(this.createTabs) {
                if (!this.tabContainer) {
                    this.tabContainer = this.add(utils.addWidget(_TabContainer, {}, this, this, true));
                }
                return this.tabContainer;
            }else{
                if(!this.root){
                    this.root = $('<div style="height: auto" class="widget nav navbar-nav ribbon_tabbar"></div>');
                    $(this.domNode).append(this.root);
                }
                return this.root[0];
            }
        }
    });

    const ActionRendererClass = dcl(null,{
        createTabs:false,
        renderTopLevel:function(name,where){
            if(this.createTabs) {
                const root = this.getRootContainer();
                let tab = root.getTab('name');
                let node = null;
                if (!tab) {
                    tab = root.createTab(name);
                    node = $(utils.getNode(tab));
                    node.addClass('nav navbar-nav ribbon_tabbar');
                }
                return node;
            }else{
                if(!this.root){
                    this.root = $('<div style="height: 30px" class="widget nav navbar-nav ribbon_tabbar"></div>');
                    $(this.domNode).append(this.root);
                }
                return this.root;
            }
        },
        /**
         *
         * @param item
         * @param data
         * @param $menu
         * @param action
         * @param owner
         * @param label
         * @param icon
         * @param visibility
         * @param showKeyCombo
         * @param lazy
         */
        renderItem:function(item,data,$menu,action, owner, label, icon, visibility, showKeyCombo, lazy){
            const self = this;
            const labelLocalized = self.localize(label);
            const actionType = visibility.actionType || action.actionType;
            const parentAction = action.getParent ? action.getParent() : null;
            const closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
            let keyComboString = ' \n';
            let element = null;
            if (action.keyboardMappings && showKeyCombo !== false) {
                const mappings = action.keyboardMappings;
                const keyCombos = mappings[0].keys;
                if (keyCombos && keyCombos.length) {
                    keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                }
            }

            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                element = '<li class="" >';
                const id = action._store.id + '_' + action.command + '_' + self.id;
                const checked = action.get('value');
                //checkbox-circle
                element += '<div class="action-checkbox checkbox checkbox-success ">';
                element += '<input id="' + id + '" type="checkbox" ' + (checked === true ? 'checked' : '') + '>';
                element += '<label for="' + id + '">';
                element += self.localize(data.text);
                element += '</label>';
                element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                element += '</div>';

                $menu.addClass('noclose');
                const result = $(element);
                const checkBox = result.find('INPUT');
                checkBox.on('change', function (e) {
                    action._originReference = data;
                    action._originEvent = e;
                    action.set('value', checkBox[0].checked);
                    action._originReference = null;
                });
                self.setVisibilityField(action, 'widget', data);
                return result;
            }
            closeOnClick === false && $menu.addClass('noclose');
            if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                const value = action.value || action.get('value');
                const parentValue = parentAction.get('value');
                if (value == parentValue) {
                    icon = 'fa fa-check';
                }
            }



            const title = data.text || labelLocalized || self.localize(action.title);
            //default:
            //element = '<li><a title="' + title + ' ' + keyComboString + '">';
            element = '<li class="ribbon_3rows_button" title="' + title + ' ' + keyComboString + '">';
            const _icon = data.icon || icon;
            //icon
            if (typeof _icon !== 'undefined') {
                //already html string
                if (/<[a-z][\s\S]*>/i.test(_icon)) {
                    element += _icon;
                } else {
                    element += '<span style="" class="icon ' + _icon + '"/> ';
                }
            }
            element +='<a class="">';
            element += data.text;
            //element += '<span style="max-width:100px" class="text-muted pull-right ellipsis keyboardShortCut">' + (showKeyCombo ? keyComboString : "") + '</span></a></li>';
            element += '</a></li>';
            self.setVisibilityField(action, 'widget', data);
            return $(element);
        },
        onRootAction:function(level,container){
            return this.renderTopLevel(level,container);
        }
    });
    const MainMenu = dcl([ContainerClass,ActionRendererClass,_MenuMixin4,_XWidget.StoreMixin],{
        target:null,
        visibility: types.ACTION_VISIBILITY.RIBBON,
        attachToGlobal:false,
        _tmpActions:null,
        collapseSmallGroups:3,
        containerClass: 'ribbon_dropdown',
        _did:false,
        init: function (opts) {
            if (this._didInit) {
                return;
            }
            this._didInit = true;
            let options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            const self = this;
            const root = $(document);
            this.__on(root, 'click', null, function (e) {
                if (!self.isOpen) {
                    return;
                }
                self.isOpen = false;
                self.onClose(e);
                $('.dropdown-context').css({
                    display: ''
                }).find('.drop-left').removeClass('drop-left');
            });
            if (options.preventDoubleContext) {
                this.__on(root, 'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            this.__on(root, 'mouseenter', '.dropdown-submenu', function (e) {
                try {
                    const _root = $(e.currentTarget);
                    let $sub = _root.find('.dropdown-context-sub:first');
                    let didPopup = false;
                    if ($sub.length === 0) {
                        $sub = _root.data('sub');
                        if($sub){
                            didPopup = true;
                        }else {
                            return;
                        }
                    }
                    const data = $sub.data('data');
                    const level = data ? data[0].level : 0;
                    const isFirst = level ===1;
                    if (self.menu) {
                        if (!$.contains(self.menu[0], _root[0])) {
                            return;
                        }
                    }



                    const _disabled = _root.hasClass('disabled');
                    if (_disabled) {
                        $sub.css('display', 'none');
                        return;
                    } else {
                        $sub.css('display', 'block');
                    }


                    const doClose = true;
                    if(isFirst) {
                        $sub.css('display', 'initial');
                        $sub.css('position', 'initial');
                        function close() {
                            const _wrapper = $sub.data('_popupWrapper');
                            popup.close({
                                domNode: $sub[0],
                                _popupWrapper: _wrapper
                            });
                        }

                        if (!didPopup) {
                            _root.data('sub', $sub);
                            $sub.data('owner', self);
                            $sub.on('mouseleave', function () {
                                doClose && close();
                            });
                            _root.on('mouseleave', function () {
                            });
                        }

                        popup.open({
                            popup: $sub[0],
                            around: _root[0],
                            orient: ['below', 'above'],
                            maxHeight: -1,
                            owner: self,
                            extraClass: 'ActionRibbonToolbar',
                            onExecute: function () {
                                self.closeDropDown(true);
                            },
                            onCancel: function () {
                                doClose && close();
                            },
                            onClose: function () {
                                //console.log('close');
                                //domAttr.set(self._popupStateNode, "popupActive", false);
                                //domClass.remove(self._popupStateNode, "dijitHasDropDownOpen");
                                //self._set("_opened", false);	// use set() because _CssStateMixin is watching
                            }
                        });
                        return;
                    }else{
                        if(!$sub.data('didSetup')){
                            $sub.data('didSetup',true);
                            _root.on('mouseleave',function(){
                                doClose && $sub.css('display','');
                            });
                        }
                    }
                    //reset top
                    $sub.css({
                        top: 0
                    });

                    const autoH = $sub.height() + 0;
                    const totalH = $('html').height();
                    const pos = $sub.offset();
                    const overlapYDown = totalH - (pos.top + autoH);
                    if ((pos.top + autoH) > totalH) {
                        $sub.css({
                            top: overlapYDown - 30
                        }).fadeIn(options.fadeSpeed);
                    }

                    ////////////////////////////////////////////////////////////
                    const subWidth = $sub.width();

                    const subLeft = $sub.offset().left;
                    const collision = (subWidth + subLeft) > window.innerWidth;

                    if (collision) {
                        $sub.addClass('drop-left');
                    }
                } catch (e) {
                    logError(e);
                }
            });
        },
        addContext:function(selector,data){
            let id;
            let $menu;
            const self = this;
            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = self.buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {
                const d = new Date();

                id = d.getTime();
                $menu = self.buildMenu(data, id);
                selector.append($menu);
            }

            return $menu;
        },

        setActionStore:function(store,owner){
            this._clear();
            if(this.tabContainer && this.createTabs){
                utils.destroy(this.tabContainer,true,this);
                this.getRootContainer();
            }
            this._tmpActions = [];
            this.store = store;
            this.addActionStore(store);
            const self = this;
            const visibility = self.visibility;
            const rootContainer = $(self.getRootContainer());
            const tree = self.buildActionTree(store,owner);
            const allActions = tree.allActions;
            const rootActions = tree.rootActions;
            const allActionPaths = tree.allActionPaths;

            const groupBlocks = [];

            function createGroupContainer(where,name){
                const block = $('<div class="ribbon_block_base">');
                where.append(block);
                const blockItems = $('<div class="ribbon_block_items"/>');
                block.append(blockItems);
                const blockLabel = $('<div class="ribbon_block_label_fixed">' + name +  '</div>');
                block.append(blockLabel);
                const result = {
                    root:block,
                    items:blockItems,
                    label:blockLabel,
                    count:0
                };
                groupBlocks.push(result);
                return result;
            }

            let lastGroup = '';
            let lastTarget = root;
            const last3BlockRows = null;
            const blockTarget = null;

            let lastGroupContainer = null;

            //track all actions per level
            const stats = [];
            _.each(tree.root, function (menuActions,level) {
                stats.push({
                    name:level,
                    size:menuActions.length
                });
            });

            _.each(tree.root, function (menuActions,level) {
                //console.log('on root '+level,rootContainer);
                const root = self.onRootAction(level,rootContainer);
                lastGroup = null;
                lastTarget = root;

                // final menu data
                const data = [];
                const groupedActions = menuActions.grouped;

                let isNewBlock = false;
                const blockCounter = 0;

                //temp group string of the last rendered action's group
                menuActions.forEach(command => {
                    let action = self.getAction(command,store);
                    let isDynamicAction = false;
                    let lastHeader = null;
                    isNewBlock = false;
                    if (!action) {
                        isDynamicAction = true;
                        action = self.createAction(command);
                        self._tmpActions.push(action);
                        self.setVisibilityField(action,'widget',{
                            widget:root,
                            destroy:function(){
                                this.widget.remove();
                            }
                        });
                    }
                    if(action){
                        const renderData = self.getActionData(action);
                        const icon = renderData.icon;
                        const label = renderData.label;
                        const visibility = renderData.visibility;
                        const group = renderData.group;

                        if (!isDynamicAction && group && groupedActions[group]) {// && groupedActions[group].length >= 2
                            if(lastGroup!==group){
                                lastHeader = {header: i18.localize(group)};
                                lastGroup = group;
                                lastGroupContainer = createGroupContainer(root,group);
                                lastTarget = lastGroupContainer.items;
                            }
                        }

                        if(lastGroupContainer.count > 4){
                            lastGroupContainer.target = null;
                            lastGroupContainer.count = 0;
                        }

                        console.log('render '+command + ' in ' + group);

                        let target = lastGroupContainer.target;

                        if(!target){
                            target = $('<div class="dhxrb_3rows_block"/>');
                            lastTarget.append(target);
                            lastGroupContainer.target = target;
                        }

                        lastGroupContainer.count++;

                        const item = self.toMenuItem(action, owner, label, icon, visibility || {},false);
                        item.level = 0;
                        data.push(item);
                        visibility.widget = item;
                        self.addReference(action,item);

                        //blockCounter++;


                        const childPaths = new Path(command).getChildren(allActionPaths,false);

                        const isContainer = childPaths.length> 0;

                        function parseChildren(_command, parent) {
                            const childPaths = new Path(_command).getChildren(allActionPaths, false);
                            const isContainer = childPaths.length > 0;
                            const childActions = isContainer ? self.toActions(childPaths, store) : null;
                            if (childActions) {
                                const subs = [];
                                _.each(childActions, function (child) {
                                    const _renderData = self.getActionData(child);
                                    const _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility,false);
                                    const parentLevel = parent.level || 0;
                                    _item.level = parentLevel + 1;
                                    self.addReference(child, _item);
                                    subs.push(_item);
                                    const _childPaths = new Path(child.command).getChildren(allActionPaths, false);
                                    const _isContainer = _childPaths.length > 0;
                                    if (_isContainer) {
                                        parseChildren(child.command, _item);
                                    }
                                });
                                parent.subMenu = subs;
                            }
                        }

                        parseChildren(command, item);
                        self.buildMenuItems(target, [item], "-" + new Date().getTime());
                    }
                });
            });
            self.onDidRenderActions(store,owner);

            //size all tab groups to the last know largest height
            let largest = 0;
            _.each(groupBlocks,function(group){
                const height = group.root.height();
                if(height > largest){
                    largest = height;
                }
            });
            largest = largest + 'px';
            _.each(groupBlocks,function(group){
                group.root.css('height',largest);
            });
        },
        startup:function(){
            this.init({preventDoubleContext: false});
        }
    });
    const ctx = window.sctx;
    var root;
    let grid;




    function doTests(tab){
        const grid = FTestUtils.createFileGrid('root',{},{
            _permissions:[
                ACTION.SOURCE,
                //ACTION.CLIPBOARD,
                ACTION.SELECTION
            ]
        },'TestGrid',module.id,false,tab);
        const menuNode = grid.header;
        grid.showToolbar(true,MainMenu,menuNode,true);
        return;
    }
    function doBlockTests(tab){
        grid = _TestBlockUtils.createBlockGrid(ctx,tab);
        const menuNode = grid.header;

        grid.showToolbar(true,MainMenu,menuNode,true,{
            actionFilter:{
                quick:true
            }
        });
        sctx.getWindowManager().registerView(grid);
    }
    if (ctx) {

        const parent = TestUtils.createTab(null,null,module.id);
        //doTests(parent);
        doBlockTests(parent);
        return declare('a',null,{});

    }
    return _Widget;
});

