//>>built
define("xide/ace/base_handler",function(h,k,g){g.exports={HANDLES_ANY:0,HANDLES_EDITOR:1,HANDLES_IMMEDIATE:2,HANDLES_EDITOR_AND_IMMEDIATE:3,language:null,path:null,workspaceDir:null,doc:null,isFeatureEnabled:function(a){return!disabledFeatures[a]},$getIdentifierRegex:function(){return null},completeUpdate:function(a){throw Error("Use worker_util.completeUpdate instead()");},handlesLanguage:function(a){throw Error("base_handler.handlesLanguage() is not overridden");},handlesEditor:function(){return this.HANDLES_EDITOR},
getMaxFileSizeSupported:function(){return 8E5},isParsingSupported:function(){return!1},getIdentifierRegex:function(){return null},getCompletionRegex:function(){return null},getTooltipRegex:function(){return null},parse:function(a,b){b()},findNode:function(a,b,c){c()},getPos:function(a,b){b()},init:function(a){a()},onUpdate:function(a,b){b()},onDocumentOpen:function(a,b,c,e){e()},onDocumentClose:function(a,b){b()},onCursorMove:function(a,b,c,e,d){d()},tooltip:function(a,b,c,e,d){d()},highlightOccurrences:function(a,
b,c,e,d){d()},getRefactorings:function(a,b,c,e,d){d()},outline:function(a,b,c){c()},hierarchy:function(a,b,c){c()},complete:function(a,b,c,e,d){d()},analyze:function(a,b,c,e){c()},getRenamePositions:function(a,b,c,e,d){d()},onRenameBegin:function(a,b){b()},commitRename:function(a,b,c,e,d){d()},onRenameCancel:function(a){a()},codeFormat:function(a,b){b()},jumpToDefinition:function(a,b,c,e,d){d()},getResolutions:function(a,b,c,e){e()},hasResolution:function(a,b,c,e){e()},getInspectExpression:function(a,
b,c,e,d){d()}};for(f in g.exports)"function"===typeof g.exports[f]&&(g.exports[f].base_handler=!0)});
//# sourceMappingURL=base_handler.js.map