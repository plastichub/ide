define("xide/src/src/subscribeTest", ["require", "exports", "dojo-core/aspect", "dojo-core/on"], function (require, exports, aspect_1, on_1) {
    "use strict";
    class hub {
        //full fill EventEmitter
        on(event, listener) { return this; }
        //full fill EventEmitter
        removeListener(event, listener) { return this; }
        // method for aspect/on
        test() { }
        subscribe(type, method) {
            return aspect_1.on(this, method, function () { });
        }
        publish(type, args) {
            return on_1.emit(this, args);
        }
    }
    class Evented {
        constructor() {
            this.listeners = {};
        }
        on(event, callback) {
            if (this.listeners[event] === undefined)
                this.listeners[event] = new Map();
            const callbacks = this.listeners[event];
            callbacks.set(callback, Infinity);
        }
    }
});
