export module Shapes {
    export class Rectangle {        
        constructor (public height: number, public width: number) {}         
         save(callback: (n: number) => any) : void {
            callback(42);
        }

    }
    // This works!
    var rect1 = new Rectangle(10, 4);
}
/////////////////////////////////////////////
// our TypeScript `Point` type
interface Point {
  x: number;
  y: number;
  toString(): string;
}

let Point: {
  new (x: number, y: number): Point;
  new (x: number): Point;
  prototype: Point;
 
  // static class properties and methods are actually part
  // of the constructor type!
  fromOtherPoint(point: Point): Point;

   
};
const a = new Point(2);

