/** module:xide/manager/Application **/
define("xide/manager/Application", [
    'dcl/dcl',
    'xdojo/has',
    'xide/manager/ManagerBase',
    'xide/types',
    'xide/utils',
    'dojo/Deferred',
    'dojo/promise/all',
    'dojo/has!host-browser?xide/manager/Application_UI'
], function (dcl, has, ManagerBase, types, utils, Deferred, all, Application_UI) {
    const bases = [ManagerBase];
    if ( 0 ) {
        bases.push(Application_UI);
    }
    /**
     * @class module:xide/manager/Application
     * @extends module:xide/manager/ManagerBase
     * @extends module:xide/mixins/EventedMixin
     */
    return dcl(bases, {
        declaredClass: "xide.manager.Application",
        container: null,
        ctx: null,
        _debug: true,
        _loadedComponents: {},
        _loadingComponents: null,
        /***********************************************************************/
        /*
         * Component implementation
         */
        getComponents: function () {
            return this._loadedComponents;
        },
        doComponents: function () {
            const componentsToLoad = this.loadComponents();
            const _re = require;
            const thiz = this;
            const inits = [];

            const head = new Deferred();

            if (!componentsToLoad || !componentsToLoad.dfd) {
                console.error('componentsToLoad is null or has no head dfd');
                return componentsToLoad;
            }
            const COMPONENT_NAMES = types.COMPONENT_NAMES;

            function doComponent(feature, fn) {
                if (has(feature) && componentsToLoad[feature]) {
                    if (componentsToLoad[feature].dfd) {
                        const sub = new Deferred();
                        inits.push(sub);
                        componentsToLoad[feature].dfd.then(function () {
                            fn(sub);
                        });
                    }
                }
            }
            doComponent(COMPONENT_NAMES.XTRACK, function (sub) {
                sub.resolve();
            });
            doComponent(COMPONENT_NAMES.XFILE, function (sub) {
                sub.resolve();
            });

            doComponent(COMPONENT_NAMES.XIDEVE, function (sub) {
                thiz.initXIDEVE().then(function () {
                    sub.resolve();
                });
            });
            doComponent(COMPONENT_NAMES.XNODE, function (sub) {
                thiz.initXNODE().then(function () {
                    sub.resolve();
                });
            });

            doComponent(COMPONENT_NAMES.XBLOX, (sub) => {
                _re([
                    'xblox/embedded_ui',
                    'xfile/manager/BlockManager',
                    'xblox/views/BlocksFileEditor'
                ], () => {
                    sub.resolve();
                });
            });

            componentsToLoad.dfd.then(() => {
                const inner = all(inits);
                inner.then(() => {
                    head.resolve();
                });
            });

            head.then(() => {
                thiz.registerEditorExtensions();
            });


            return head;
        },
        loadComponents: function (components) {
            if (this._loadingComponents) {
                return this._loadingComponents;
            }

            const thiz = this;
            let _loadNext;
            let _loadComponent;

            thiz._loadedComponents = {};
            components = components || utils.getJson(this.ctx.getResourceManager().getVariable('COMPONENTS'));
            _.each(components, function (val, component) {
                if (has(component) === false) {
                    delete components[component];
                }
            });

            if (!components) {
                return null;
            }
            /**
             * Loop function to load remaining components
             */
            _loadNext = function () {

                const next = _.find(components, {
                    status: 0
                });
                if (next) {
                    _loadComponent(next.name, next);
                } else {
                    //all loaded
                    components.dfd.resolve(components);
                    thiz.publish(types.EVENTS.ON_ALL_COMPONENTS_LOADED, components);
                    components.dfd.resolve();
                }
            };

            /**
             * private component loader function
             *
             */
            _loadComponent = function (name, process) {
                const settings = process.settings;
                if (has(name) === false) {
                    console.error('loading component ' + name + 'which is disabled by has!');
                }
                thiz.ctx.getPluginManager().loadComponent(name, null, settings === true ? null : settings).then(function (_component) {
                    thiz._loadedComponents[name] = _component;
                    const evtData = {
                        component: _component,
                        name: name
                    };
                    process.dfd.resolve(evtData);
                    thiz.publish(types.EVENTS.ON_COMPONENT_READY, evtData);
                    process.status = 1;
                    _loadNext();
                });
            };

            /**
             * Iterate over components and prepare them for being loaded serial
             */
            for (const component in components) {
                const name = '' + component;
                const settings = components[name];

                if (settings) {
                    components[name] = {
                        settings: settings,
                        status: 0,
                        name: name,
                        dfd: new Deferred()
                    };

                } else {
                    components[name] = {
                        status: 1
                    };
                }
            }
            components.dfd = new Deferred();
            //now fire loading sequence
            _loadNext();

            this._loadingComponents = components;

            return components;
        },
        hasComponent: function (name) {
            const components = utils.getJson(this.ctx.getResourceManager().getVariable('COMPONENTS'));
            return !!(components && components[name]);
        }
    });
});