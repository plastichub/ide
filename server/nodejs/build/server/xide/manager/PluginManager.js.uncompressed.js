/**
 * @module xide/manager/PluginManager
*/
define("xide/manager/PluginManager", [
    'dcl/dcl',
    'dojo/has',
    'xide/manager/ManagerBase',
    'xide/utils',
    'xide/types',
    'xide/factory',
    "dojo/Deferred",
    "dojo/promise/all"
], function (dcl, has, ManagerBase, utils, types, factory, Deferred, all){
    const _debug = false;

    /**
     * Plugin manager which provides loading of additional modules at any time after the main layer(s)
     * have been loaded.
     * @class module:xide/manager/PluginManager
     * @extends module:xide/mixins/ReloadMixin
     */
    return dcl(ManagerBase,{
        declaredClass:"xide.manager.PluginManager",
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Variables
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * When load() receives a module name without a slash, use this prefix
         *
         * @TODO: this must be changed into a map to configure this per 'run-time configuration'
         * @member string _defaultPackageLocationPrefix
         */
        _defaultPackageLocationPrefix:'../../',
        /**
         * When load() receives a module name without a slash, use this prefix
         * @member string _defaultPackageLocationSuffix
         */
        _defaultPackageLocationSuffix:'/component',
        /**
         * Whe loading a component, use this flags by default
         * @member module:xide.types.COMPONENT_FLAGS
         */
        _defaultComponentFlags:{
            /**
             * call load() when loaded
             * @enum module:xide.types.COMPONENT_FLAGS:LOAD
             */
            LOAD:1,
            /**
             * call run() when loaded
             * @enum module:xide.types.COMPONENT_FLAGS:RUN
             */
            RUN:2  //call run() when loaded
        },
        /**
         * Whe loading a component, mixin these properties/members
         * @member {Object}
         */
        defaultComponentMixin:function(flags){
            return {
                owner:this,
                ctx:this.ctx,
                flags:flags
            };
        },
        /**
         * List of modules to add to a components base classes. That will be used to add logging and others
         * @member {String[]}
         */
        componentBaseClasses:null,
        /**
         * Our context object
         * @member module: xide/manager/Context
         */
        ctx:null,
        /**
         * JSON data of plugin data
         * @member {Array}
         */
        pluginResources:null,
        /**
         * Array of plugin instances
         * @member {object[]}
         */
        pluginInstances:null,
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Components
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
         * Called when component::load() is finish
         * @param component
         * @param flags
         * @param deferred
         * @private
         */
        _componentReady:function(component,flags,deferred){
            if (flags.RUN) {

                try {
                    component.run(this.ctx);//fire only, don't bother when it crashes
                }catch(e){
                    _debug && console.error('component: ' + component.getLabel() + ' crashed in run ' + e);
                }
            }
            return deferred.resolve(component);
        },
        /**
         * Called when a component has been loaded
         * @param component
         * @param flags
         * @param componentArguments
         * @param deferred
         * @private
         */
        _componentLoaded:function(component,flags,componentArguments,deferred){
            try {

                //add constructor arguments
                utils.mixin(componentArguments,this.defaultComponentMixin(flags));
                const componentInstance = factory.createInstance(component, componentArguments, this.componentBaseClasses);

                const _afterLoaded = function(componentInstance,flags,deferred){
                    this._componentReady(componentInstance,flags,deferred);
                }.bind(this);


                if (flags.LOAD) {
                    //call load, its async
                    componentInstance.load().then(function(){
                        _afterLoaded(componentInstance,flags,deferred);
                    });
                }else{
                    _afterLoaded(componentInstance,flags,deferred);
                }
            }catch(e){
                _debug && console.error('error in component creation!' + e);
                deferred.reject(arguments);
                logError(e);
            }

        },
        /**
         * Load a component
         *
         * @memberOf module:xide/manager/PluginManager#
         *
         * @param path {string} A require-js module path
         * @param {int} flags being used whilst loading
         * @param {object} componentArguments
         * @param packageLocation {string=}
         * @param packagePath {string=}
         */
        loadComponent:function(path,flags,componentArguments,packageLocation,packagePath){
            //defaults, sanitizing
            componentArguments = componentArguments ==='true' ? {} : componentArguments;

            path = !path.includes('/') ? ( this._defaultPackageLocationPrefix + path + this._defaultPackageLocationSuffix ) : path;

            flags = flags!=null ? flags : this._defaultComponentFlags;

            const deferred = new Deferred();
            const self = this;

            const _component = utils.getObject(path);
            if(_component){
                //not loaded yet?
                if(_.isFunction(_component.then)){
                    _component.then(function(module){
                        self._componentLoaded(module,flags,componentArguments,deferred);
                    },function(err){
                        //shouldn't happen
                        console.error('error in loading component at path ' + path,err);
                    });
                }else{
                    //already loaded
                    this._componentLoaded(_component,flags,componentArguments,deferred);
                }
            }else{
                console.error('cant get object at ' + path);
            }

            _debug &&  console.log('load component ' + path);

            return deferred;
        },
        /**
         * Each component has a resource file within its directory with this pattern : resources-config.json.
         * When component is being loaded, we do load also client resources for that component (css,js,...)
         * @param component {xide.model.Component}
         * @param path
         */
        loadComponentResources:function(component,path){},
        /**
         * @TODO
         *
         * @param component
         */
        reloadComponent:function(component){},
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Plugins: to be removed soon.
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @deprecated
         * @param pluginResources
         */
        loadPlugins:function(pluginResources){
            if(!has('plugins')) {
                return;
            }
            const dfd = new Deferred();
            const pluginPromises = [];
            const thiz = this;

            this.pluginResources = pluginResources;
            this.pluginInstances = [];
            if(this.pluginResources){
                this.pluginResources.forEach(plug => {
                    if(plug.type && plug.type=="JS-PLUGIN" && plug.path){
                        pluginPromises.push(this.loadComponent(plug.path,null,plug).then(function(component){
                            component.pluginResources = plug;
                            thiz.pluginInstances.push(component);
                        }));
                    }else{
                        _debug &&  console.warn('something wrong with plugin data');
                    }
                });
            }
            all(pluginPromises).then(function(){
                dfd.resolve(pluginPromises);
                _debug && console.log('plugins ready',thiz.pluginInstances);
                thiz.publish(types.EVENTS.ALL_PLUGINS_READY,{
                    instances:thiz.pluginInstances,
                    resources:pluginResources
                });
            });
            return dfd;
        }
    });
});