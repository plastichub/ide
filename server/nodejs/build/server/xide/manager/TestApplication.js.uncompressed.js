define("xide/manager/TestApplication", [
    'dcl/dcl',
    'xide/manager/Application',
    'xdojo/has',
    'xide/manager/TestApplication_UI'
], function (dcl,Application,has,TestApplication_UI) {
    const bases =   0  ? [Application, TestApplication_UI] : Application;
    return dcl(bases, {
        declaredClass:"xide.manager.TestApplication",
        vfsItems: null
    });
});
