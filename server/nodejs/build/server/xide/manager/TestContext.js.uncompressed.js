/** module:xide/manager/TestContext **/
define("xide/manager/TestContext", [
    'dcl/dcl',
    'xide/manager/Context',
    'xide/manager/TestApplication',
    'xcf/manager/LogManager',
    'xide/manager/ResourceManager',
    'xide/manager/PluginManager',
    'xdojo/has',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'dojo/Deferred'
],function(dcl,Context,Application,LogManager,ResourceManager,PluginManager,has,types,utils,factory,Deferred){
    /**
     * @class module:xcf/manager/Context
     * @extends module:xide/manager/Context
     */
    return dcl([Context],{
        declaredClass:"xide.manager.TestContext",
        getUserDirectory:function(){
            const resourceManager = this.getResourceManager();
            if(resourceManager){
                return resourceManager.getVariable("USER_DIRECTORY");
            }
        },
        /***
         * Standard application context, creates & holds all manager instances
         */
        /////////////////////////////////////////////////////////////
        //
        //  Variables
        //
        /////////////////////////////////////////////////////////////
        namespace:'xide.manager.',
        nodeServiceManager:null,
        blockManager:null,
        /////////////////////////////////////////////////////////////
        //
        //  Getters for managers
        //
        /////////////////////////////////////////////////////////////
        getBlockManager:function(){
            return this.blockManager;
        },
        getNodeServiceManager:function(){
            return this.nodeServiceManager;
        },
        constructManagers:function(appClass){
            const isEditor = this.isEditor();
            const isBrowser =  0 ;

            if(isBrowser) {
                if(!isEditor) {
                    this.logManager = this.createManager(LogManager);
                }
                this.pluginManager = this.createManager(PluginManager);
                this.resourceManager = this.createManager(ResourceManager);
                this.application = this.createManager(appClass || Application);
            }
        },
        initManagers:function(){
            const isEditor = this.isEditor();
            if(!isEditor) {
                if (has('log')) {
                    this.logManager.init();
                }
            }
            this.resourceManager && this.resourceManager.init();
            this.subscribe([
                types.EVENTS.ON_NODE_SERVICE_STORE_READY
            ]);
        },
        checkDeviceServerConnection: function () {
            if(!this.ctx.getNodeServiceManager){
                return true;
            }
            if (!this.deviceServerClient && this.ctx.getNodeServiceManager) {
                const store = this.ctx.getNodeServiceManager().getStore();
                if (!store) {
                    console.error('checkDeviceServerConnection : have no service store');
                    return false;
                }
                this.createDeviceServerClient(store);
            }
            return true;
        },
        /***
         * Callback when the NodeJS service manager initialized its service store. That may
         * happen multiple times as user can reload the store.
         *
         * @param evt
         * @private
         */
        onNodeServiceStoreReady: function (evt) {
            if (this.deviceServerClient) {
                return;
            }
            const store = evt.store;
            this.createDeviceServerClient(store);
        },
        createDeviceServerClient:function(store){
            const thiz = this;


            const dfd = new Deferred();
            this.deviceServerClient = null;
            this.deviceServerClient = factory.createClientWithStore(store, 'Device Control Server', {
                delegate: {
                    onConnected:function(){
                        dfd.resolve();
                        thiz.publish(types.EVENTS.ON_DEVICE_SERVER_CONNECTED);
                        thiz.getNotificationManager().postMessage({
                            message: 'Connected to Device Server',
                            type: 'success',
                            duration: 3000
                        });

                    },
                    onLostConnection: function(evt){
                        //thiz.onDeviceServerConnectionLost();
                        console.error('lost connection',evt);
                    },
                    onServerResponse: function (data) {
                        const dataIn = data['data'];
                        let msg = null;
                        if (_.isString(dataIn)) {

                            try {
                                msg = JSON.parse(dataIn);
                            } catch (e) {
                                msg = dataIn;
                            }


                            !msg && console.error('invalid incoming message',data);

                            msg = msg || {};


                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_FINISH) {
                                //thiz.onCommandFinish(msg.data.device,msg.data.deviceMessage);
                                return;
                            }
                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_PROGRESS) {
                                //thiz.onCommandProgress(msg.data.device,msg.data.deviceMessage);
                                return;
                            }
                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_PAUSED) {
                                //thiz.onCommandPaused(msg.data.device,msg.data.deviceMessage);
                                return;
                            }

                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_STOPPED) {
                                //thiz.onCommandStopped(msg.data.device,msg.data.deviceMessage);
                                return;
                            }
                            if (msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_ERROR) {
                                //thiz.onCommandError(msg.data.device,msg.data.deviceMessage);
                                return;
                            }
                            if (msg.event === types.EVENTS.ON_DEVICE_DISCONNECTED) {
                                thiz.publish(types.EVENTS.ON_DEVICE_DISCONNECTED, msg.data);
                                return;
                            }

                            if (msg.event === types.EVENTS.SET_DEVICE_VARIABLES) {
                                //return thiz.onSetDeviceServerVariables(msg.data);
                            }

                            if (msg.event === types.EVENTS.ON_RUN_CLASS_EVENT) {
                                return thiz.onRunClassEvent(msg.data);
                            }

                            if (msg.event === types.EVENTS.ON_DEVICE_CONNECTED) {
                                thiz.publish(types.EVENTS.ON_DEVICE_CONNECTED, msg.data);
                                return;
                            }

                            if (msg.event === types.EVENTS.ON_SERVER_LOG_MESSAGE) {
                                thiz.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, msg.data);
                                return;
                            }

                            if (msg.event === types.EVENTS.ON_MQTT_MESSAGE) {
                                thiz.publish(types.EVENTS.ON_MQTT_MESSAGE, msg.data);
                                //thiz.onMQTTMessage(msg.data);
                                return;
                            }

                            if (msg.event === types.EVENTS.ON_FILE_CHANGED) {
                                return thiz.onXIDEMessage(utils.fromJson(data.data));
                            }
                        }
                    }
                }
            });
            if (!this.deviceServerClient) {
                console.log('couldnt connect to device server');
                return;
            } else {
                console.log('did connect to device server');
            }
            this.deviceServerClient.dfd = dfd;
            return this.deviceServerClient;
        }
    });
});
