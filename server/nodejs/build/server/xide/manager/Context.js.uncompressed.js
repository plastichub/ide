/** module:xide/manager/Context **/
define("xide/manager/Context", [
    'dcl/dcl',
    'dojo/Deferred',
    'dojo/has',
    'xide/manager/ContextBase',
    'xide/types',
    'xide/utils',
    'require',
    "dojo/promise/all",
    'xdojo/has!host-browser?xide/manager/Context_UI'
], function (dcl, Deferred, has, ContextBase, types, utils, _require, all, Context_UI) {
    ! 0  &&  true || has.add('xlog', function () {
        return true;
    }, true);

    const isServer =  true ;
    const isBrowser =  0 ;
    const bases = isBrowser ? [ContextBase, Context_UI] : [ContextBase];
    const debugFileChanges = false;
    const debugModuleReload = false;

    /**
     * @class module:xide/manager/Context
     * @extends module:xide/manager/ContextBase
     */
    const Module = dcl(bases, {
        declaredClass: "xide.manager.Context",
        application: null,
        contextManager: null,
        fileManager: null,
        resourceManager: null,
        notificationManager: null,
        serviceObject: null,
        pluginManager: null,
        windowManager: null,
        logManager: null,
        mountManager: null,
        config: null,
        managers: null,
        namespace: 'xide.manager.',
        defaultNamespace: 'xide.manager.',
        vfsMounts: null,
        xideServiceClient: null,
        fileUpdateTimes: {},
        /***********************************************************************/
        /*
         * Global event handlers
         */
        onXIDEMessage: function (data, publish) {
            if (!data || !data.event) {
                return;
            }
            const thiz = this;
            if (data.event === types.EVENTS.ON_FILE_CHANGED) {
                debugFileChanges && console.log("on file changed ", data);
                //inotify plus
                if (data.data && data.data.mask && data.data.mask.includes('delete')) {
                    thiz.publish(types.EVENTS.ON_FILE_DELETED, data);
                    return;
                }

                if (data.data && data.data.type == 'delete') {
                    thiz.publish(types.EVENTS.ON_FILE_DELETED, data);
                    return;
                }
                const _path = data.data.path;
                const timeNow = new Date().getTime();
                if (thiz.fileUpdateTimes[_path]) {
                    const last = thiz.fileUpdateTimes[_path];
                    const diff = timeNow - last;
                    if (diff < 1000) {
                        thiz.fileUpdateTimes[_path] = timeNow;
                        return;
                    }
                }


                publish !== false && thiz.publish(data.event, data);
                thiz.fileUpdateTimes[_path] = timeNow;
                //path is absolute and might look like: /PMaster/projects/xbox-app/client/src/lib/xfile/Views.js
                //fix windows path
                let path = utils.replaceAll('\\', '/', data.data.path);
                path = utils.replaceAll('//', '/', data.data.path);
                path = path.replace(/\\/g, "/");

                if (path.includes('/build/')) {
                    return;
                }

                if (path == null || path.indexOf == null) {
                    return;
                }
                if (path.match(/\.css$/)) {
                    thiz.onCSSChanged({
                        path: path
                    });
                }
                /**
                 * Try generic
                 */
                if (path.match(/\.js$/)) {
                    let modulePath = data.data.modulePath;
                    if (modulePath) {
                        modulePath = modulePath.replace('.js', '');
                        const _re = _require; //hide from gcc
                        //try pre-amd module
                        let module = null;
                        try {
                            module = _re(modulePath);
                        } catch (e) {}


                        //special: driver
                        var _start = 'data/system/drivers';
                        if (path.includes(_start)) {
                            var libPath = path.substr(path.indexOf(_start) + (_start.length + 1), path.length);
                            libPath = libPath.replace('.js', '');
                            modulePath = 'system_drivers/' + libPath;
                        }

                        _start = 'user/drivers';
                        if (path.includes(_start)) {
                            var libPath = path.substr(path.indexOf(_start) + (_start.length + 1), path.length);
                            libPath = libPath.replace('.js', '');
                            modulePath = 'user_drivers/' + libPath;
                        }


                        const resourceManager = this.getResourceManager();
                        const vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') || {} : null;

                        if (vfsConfig && vfsConfig['user_drivers']) {

                            if (path.includes(vfsConfig['user_drivers'])) {
                                var _start = vfsConfig['user_drivers'];
                                _start = _start.replace(/\/+$/, "");
                                var libPath = path.substr(path.indexOf(_start) + (_start.length + 1), path.length);
                                libPath = libPath.replace('.js', '');
                                modulePath = 'user_drivers/' + libPath;
                            }
                        }
                        modulePath = utils.replaceAll('.', '/', modulePath);
                        if (!modulePath.includes('/build/') && !isServer) {
                            setTimeout(function () {
                                debugModuleReload && console.log('reloading module ' + modulePath);
                                thiz._reloadModule(modulePath, true);
                            }, 400);
                        }
                    }
                }
            }
        },
        onNodeServiceStoreReady: function (evt) {},
        mergeFunctions: function (target, source, oldModule, newModule) {
            for (const i in source) {
                const o = source[i];
                if (_.isFunction(source[i])) {
                    if (source[i] && target) {
                        target[i] = source[i];
                    }
                }
            }
        },
        /***********************************************************************/
        /*
         * File - Change - Handlers
         */
        /**
         * Special case when module has been reloaded : update all functions in our singleton
         * managers!
         * @param evt
         */
        reloadModules: function (modules, patch) {
            const head = new Deferred();
            const pluginPromises = [];
            const newModules = [];
            const thiz = this;

            _require({
                cacheBust: 'time=' + new Date().getTime()
            });
            _.each(modules, function (module) {
                let oldModule = null;
                const dfd = new Deferred();
                if (patch !== false) {
                    oldModule = _require(module);
                }
                _require.undef(module);
                _require([module], function (moduleLoaded) {
                    oldModule && thiz.mergeFunctions(oldModule.prototype, moduleLoaded.prototype);
                    newModules.push(moduleLoaded);
                    dfd.resolve();
                });
                pluginPromises.push(dfd);
            });

            all(pluginPromises).then(function () {
                head.resolve(newModules);
                _require({
                    cacheBust: null
                });
            });
            return head;
        },
        _reloadModule: function (_module, reload) {
            let _errorHandle = null;
            const dfd = new Deferred();
            if (!isServer && _module.includes('nodejs')) {
                return;
            }

            _module = _module.replace('0/8', '0.8');
            _module = _module.replace('/src/', '/');

            function handleError(error) {
                debugModuleReload && console.log(error.src, error.id);
                debugModuleReload && console.error('require error ' + _module, error);
                _errorHandle.remove();
                dfd.reject(error);
            }


            //has its own impl.
            let obj = this.getModule(_module);
            if (obj && obj.prototype && obj.prototype.reloadModule) {
                return obj.prototype.reloadModule();
            }

            _errorHandle = _require.on("error", handleError);

            let oldModule = this.getModule(_module);
            if (!oldModule) {
                oldModule = typeof _module !== 'undefined' ? oldModule : null;
                if (!oldModule && typeof window !== 'undefined') {
                    //try global namespace
                    oldModule = utils.getAt(window, utils.replaceAll('/', '.', _module), null);
                    if (oldModule) {
                        obj = oldModule;
                    } else {
                        try {
                            oldModule = _require(utils.replaceAll('.', '/', _module));
                        } catch (e) {
                            debugModuleReload && console.log('couldnt require old module', _module);
                        }
                    }
                }
            }
            if (oldModule) {
                obj = oldModule;
            }

            //remove from dom
            if (isBrowser) {
                const scripts = document.getElementsByTagName('script');
                _.each(scripts, function (script) {
                    if (script && script.src && script.src.includes(_module)) {
                        script.parentElement.removeChild(script);
                    }
                })
            }
            _require.undef(_module);
            const thiz = this;
            if (reload) {
                setTimeout(function () {
                    _require({
                        cacheBust: 'time=' + new Date().getTime(),
                        waitSeconds: 5
                    });
                    try {
                        _require([_module], function (moduleLoaded) {
                            _require({
                                cacheBust: null
                            });
                            if (_.isString(moduleLoaded)) {
                                console.error('module reloaded failed : ' + moduleLoaded + ' for module : ' + _module);
                                return;
                            }
                            moduleLoaded.modulePath = _module;
                            if (obj) {
                                thiz.mergeFunctions(obj.prototype, moduleLoaded.prototype, obj, moduleLoaded);
                                if (obj.prototype && obj.prototype._onReloaded) {
                                    obj.prototype._onReloaded(moduleLoaded);
                                }
                            }

                            if (oldModule && oldModule.onReloaded) {
                                oldModule.onReloaded(moduleLoaded, oldModule);
                            }

                            thiz.publish(types.EVENTS.ON_MODULE_RELOADED, {
                                module: _module,
                                newModule: moduleLoaded
                            });

                            if (moduleLoaded.prototype && moduleLoaded.prototype.declaredClass) {
                                thiz.publish(types.EVENTS.ON_MODULE_UPDATED, {
                                    moduleClass: moduleLoaded.prototype.declaredClass,
                                    moduleProto: moduleLoaded.prototype
                                });
                            }
                            thiz.setModule(_module, moduleLoaded);
                            dfd.resolve(moduleLoaded);
                        });
                    } catch (e) {
                        console.error('error reloading module', e);
                        logError(e, 'error reloading module');
                        dfd.reject(e);
                    }
                }, 100);
            } else {
                dfd.resolve();
            }
            return dfd;
        },
        onCSSChanged: function (evt) {
            if (isBrowser) {
                let path = evt.path;
                const _p = this.findVFSMountPath(path);
                path = utils.replaceAll(_p, '', path);
                path = utils.replaceAll('//', '/', path);
                path = path.replace('/PMaster/', '');
                const reloadFn = window['xappOnStyleSheetChanged'];
                if (reloadFn) {
                    reloadFn(path);
                }
            }
        },
        onDidChangeFileContent: function (evt) {
            if (evt['didProcess']) {
                return;
            }
            evt['didProcess'] = true;
            if (!this.vfsMounts) {
                return;
            }
            if (!evt.path) {
                return;
            }
            const path = evt.path;
            if (path.includes('.css')) {
                if (isBrowser) {
                    this.onCSSChanged(evt);
                }
                return;
            }

            if (path.includes('resources') ||
                path.includes('meta') ||
                path.includes('lib/custom') ||
                !path.includes('.js')) {
                return;
            }

            const mount = evt.mount.replace('/', '');
            let module = null;

            if (!this.vfsMounts[mount]) {
                return;
            }
            module = '' + evt.path;
            module = module.replace('./', '');
            module = module.replace('/', '.');
            module = module.replace('.js', '');
            module = utils.replaceAll('.', '/', module);
            const thiz = this;
            setTimeout(function () {
                try {
                    thiz._reloadModule(module, true);
                } catch (e) {
                    console.error('error reloading module', e);
                }
            }, 100);
        },
        /***********************************************************************/
        /*
         * get/set
         */
        getMount: function (mount) {
            const resourceManager = this.getResourceManager();
            const vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') || {} : null;
            if (vfsConfig && vfsConfig[mount]) {
                return vfsConfig[mount];
            }
            return null;
        },
        toVFSShort: function (path, mount) {
            const resourceManager = this.getResourceManager();
            const vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') || {} : null;
            if (vfsConfig && vfsConfig[mount]) {
                let mountPath = vfsConfig[mount];
                mountPath = utils.replaceAll('//', '/', mountPath);
                mountPath = mountPath.replace(/\/+$/, "");
                if (path.includes(mountPath)) {
                    let _start = mountPath;
                    _start = _start.replace(/\/+$/, "");
                    const libPath = path.substr(path.indexOf(_start) + (_start.length + 1), path.length);
                    return libPath;
                }
            }
            return null;
        },
        findVFSMount: function (path) {
            const resourceManager = this.getResourceManager();
            const vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') || {} : null;
            if (vfsConfig) {
                for (const mount in vfsConfig) {
                    let mountPath = vfsConfig[mount];
                    mountPath = utils.replaceAll('//', '/', mountPath);
                    mountPath = mountPath.replace(/\/+$/, "");
                    if (path.includes(mountPath)) {
                        return mount;
                    }
                }

            }
            return null;
        },
        findVFSMountPath: function (path) {
            const resourceManager = this.getResourceManager();
            const vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') || {} : null;
            if (vfsConfig) {
                for (const mount in vfsConfig) {
                    let mountPath = vfsConfig[mount];
                    mountPath = utils.replaceAll('//', '/', mountPath);
                    mountPath = mountPath.replace(/\/+$/, "");
                    if (path.includes(mountPath)) {
                        return mountPath;
                    }
                }

            }
            return null;
        },
        getBlockManager: function () {
            return this.blockManager;
        },
        getPluginManager: function () {
            return this.pluginManager;
        },
        getService: function () {
            return this.serviceObject;
        },
        getFileManager: function () {
            return this.fileManager;
        },
        getResourceManager: function () {
            return this.resourceManager;
        },
        getMountManager: function () {
            return this.mountManager;
        },
        getContextManager: function () {
            return this.contextManager;
        },
        getLogManager: function () {
            return this.logManager;
        },
        getApplication: function () {
            return this.application;
        },
        /***********************************************************************/
        /*
         * STD - API
         */
        constructor: function (config, args) {
            this.managers = [];
            this.config = config;
            this.args = args;
            this.language = 'en';
            this.subscribe(types.EVENTS.ON_CHANGED_CONTENT, this.onDidChangeFileContent);
        },
        prepare: function () {
            if (this.config) {
                this.initWithConfig(this.config);
            }
        },
        /**
         * The config is put into the index.php as JSON. The server has also some data
         * which gets mixed into the manager instances.
         * @param config
         */
        initWithConfig: function (config) {
            if (config && config.mixins) {
                this.doMixins(config.mixins);
            }
        },
        isEditor: function () {
            return this.args && this.args.file;
        }
    });
    dcl.chainAfter(Module, 'constructManagers');
    dcl.chainAfter(Module, 'initManagers');
    return Module;
});