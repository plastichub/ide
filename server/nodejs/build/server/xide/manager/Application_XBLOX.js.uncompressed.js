define("xide/manager/Application_XBLOX", [
    'dcl/dcl',
    'require',
    'dojo/Deferred'
], function (dcl, require, Deferred) {
    /**
     * @class xide.manager.Application_XBLOX
     * @extends module:xide/manager/ManagerBase
     * @augments mixin:xide/mixins/EventedMixin
     */
    return dcl(null, {
        declaredClass: "xide.manager.Application_XBLOX",
        onXBlocksReady: function () {
            const blockManagerClass = 'xfile.manager.BlockManager';
            const blockManagerProto = require('xfile/manager/BlockManager');
            const ctx = this.ctx;
            const self = this;
            const dfd = new Deferred();

            if (ctx.blockManager) {
                ctx.blockManager.onReady();
            }
            const _re = require;
            _re(['xcf/manager/BlockManager'], function (bm) {
                ctx.blockManager = new bm();
                ctx.blockManager.ctx = ctx;
                ctx.blockManager.updatePalette = false;
                ctx.blockManager.init();
                ctx.blockManager.onReady();
                ctx.blockManager.serviceObject = ctx.fileManager.serviceObject;
                dfd.resolve();
            });

            const _editor = require('xblox/views/BlocksFileEditor');
            ctx.registerEditorExtension('xBlocks', 'xblox', 'fa-play-circle-o', self, true, null, _editor, {
                ctx: ctx,
                mainView: self.mainView,
                blockManagerClass: blockManagerClass,
                registerView: true
            });

            const t = {
                "themeDefaultSet": null,
                "dojoOptions": null,
                "widgetPalette": null,
                "userInfo": {
                    "email": "noemail@noemail.com",
                    "isLocalInstall": false,
                    "userDisplayName": "A",
                    "userId": "A"
                },
                "workbenchState": {
                    "activeEditor": null,
                    "editors": [],
                    "nhfo": [],
                    "project": null,
                    "id": "",
                    "Fields": [],
                    "admin": {
                        "settings": [{
                            "id": "driverView",
                            "value": {
                                "basic": {
                                    "showHeader": false,
                                    "selection": {
                                        "selected": ["9627cb05-2f23-8846-0721-5abcc7d3afbb"]
                                    },
                                    "toolbar": false,
                                    "properties": true
                                },
                                "conditionalProcess": {
                                    "showHeader": false,
                                    "selection": {
                                        "selected": []
                                    },
                                    "toolbar": true,
                                    "properties": true
                                },
                                "basicVariables": {
                                    "showHeader": false,
                                    "selection": {
                                        "selected": []
                                    },
                                    "toolbar": false,
                                    "properties": true
                                }
                            }
                        }, {
                            "id": "theme",
                            "value": "blue"
                        }]
                    }
                },
                "project": null,
                "id": "",
                "Fields": []
            };


            try {
                var palette = require('xblox/views/BlockGridPalette');
                palette.init(ctx);
            } catch (e) {
                logError(e, 'error loading xblox/views/BlockGridPalette');
            }


            return dfd;
        }
    });
});