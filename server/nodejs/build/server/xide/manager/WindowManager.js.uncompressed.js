/** @module xide/manager/WindowManager **/
define("xide/manager/WindowManager", [
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'xide/manager/ManagerBase',
    'xide/editor/Registry',
    'xide/registry',
    'xdojo/has',
    'xide/lodash',
    'dojo/has!xace?xace/views/Editor'
], function (dcl, utils, types, ManagerBase, Registry, registry, has,_,Editor) {

    function register(what, where, _active) {
        where.addActionEmitter(what);
        if (_active !== false) {
            where.setActionEmitter(what);
        }
    }

    function clear(what, where, _active) {
        where.setActionEmitter(what);
    }

    function findFrame(docker) {
        const panels = docker.getPanels();
        if (docker._lastSelected) {
            for (let i = 0; i < panels.length; i++) {
                const panel = panels[i];
                if (panel == docker._lastSelected) {
                    const frame = panel.getFrame();
                    if (frame) {
                        return frame;
                    }
                }
            }
        }
    }

    /**
     * @class module:xide/manager/WindowManager
     * @extends module:xide/manager/ManagerBase
     */
    const Module = dcl(ManagerBase, {
        declaredClass: 'xide.manager.WindowManager',
        actionReceivers: null,
        /**
         *
         * @returns {Array}
         */
        getActions: function () {
            const result = [];
            const thiz = this;
            result.push(this.ctx.createAction('Next', 'View/Next', 'fa-cube', ['alt num_add'], 'Home', 'View', "global",
                //onCreate
                function (action) {
                },
                //handler
                function () {
                    const docker = thiz.ctx.mainView.getDocker();
                    const frame = findFrame(docker);
                    if (!frame) {
                        return;
                    }
                    const pos = frame._curTab;
                    const panel = frame.panel(pos);
                    const next = frame.panel(pos + 1);
                    if (panel || next) {
                        (next || panel ).select();
                        (next || panel ).resize();
                        docker.resize();
                    }
                },
                {
                    addPermission: true,
                    show: true
                }, null, null,
                //permissions:
                [], window, this
            ));
            result.push(this.ctx.createAction('Prev', 'View/Prev', 'fa-cube', ['alt num_subtract'], 'Home', 'View', "global",
                //onCreate
                function (action) {
                },
                //handler
                function () {
                    const docker = thiz.ctx.mainView.getDocker();
                    const frame = findFrame(docker);
                    if (!frame) {
                        return;
                    }
                    const pos = frame._curTab;

                    const panel = frame.panel(pos);
                    const next = frame.panel(pos - 1);
                    if (panel || next) {

                        (next || panel ).select();
                        (next || panel ).resize();
                        docker.resize();
                    }
                },
                {
                    addPermission: true,
                    show: true
                }, null, null,
                //permissions:
                [], window, this
            ));


            return result;
        },
        init: function () {
            this.ctx.addActions(this.getActions());
            this.actionReceivers = [];
        },
        addActionReceiver: function (receiver) {
            if (!this.actionReceivers.includes(receiver)) {
                this.actionReceivers.push(receiver);
                receiver._on('destroy', function () {
                    this.actionReceivers.remove(receiver);
                }, this);
            }
        },
        registerView: function (view, active) {
            const toolbar = this.getToolbar();
            const menu = this.getMainMenu();

            this.publish(types.EVENTS.ON_OPEN_VIEW, {
                view: view,
                menu: menu,
                toolbar: toolbar,
                src: this
            });
            toolbar && register(view, toolbar, active);
            menu && register(view, menu, active);
            const receivers = this.actionReceivers;
            _.each(receivers, function (widget) {
                widget && register(view, widget, active);
            });
            return true;
        },
        clearView: function (view, active) {
            const toolbar = this.getToolbar();
            const menu = this.getMainMenu();
            const breadcrumb = this.getBreadcrumb();
            toolbar && clear(view, toolbar, active);
            menu && clear(view, menu, active);
            return true;
        },
        getBreadcrumb: function () {
            const ctx = this.ctx;
            const mainView = ctx.mainView;
            return mainView && mainView.getBreadcrumb ? mainView.getBreadcrumb() : null;
        },
        getToolbar: function () {
            const ctx = this.ctx;
            const mainView = ctx.mainView;
            return mainView.getToolbar();
        },
        getMainMenu: function () {
            const ctx = this.ctx;
            const mainView = ctx.mainView;
            return mainView.mainMenu;
        },
        /**
         *
         * @param title
         * @param icon
         * @param where
         * @returns {module:xdocker/Panel2}
         */
        createTab: function (title, icon, where) {
            const docker = this.getContext().getMainView().getDocker();
            return docker.addTab(null, {
                title: title,
                target: where ? where._parent : docker.getDefaultPanel(),
                icon: icon || 'fa-code'
            });
        },
        /**
         *
         * @returns {module:xdocker/Panel2}
         */
        getDefaultPanel: function () {
            return this.getContext().getMainView().getDocker().getDefaultPanel();
        },
        /**
         * @param title
         * @param icon
         * @returns {module:xdocker/Panel2}
         */
        createDefaultTab: function (title, icon) {
            return this.getContext().getMainView().getDocker().addTab(null, {
                title: title,
                icon: icon
            });
        },
        /**
         *
         * @param item {module:xfile/model/File}
         * @param where {module:xide/widgets/_Widget|null}
         * @param mixin {object} constructor argument mixin for the new view created
         * @param select {boolean} select the view (will update actions in global action receivers)
         * @returns {module:xide/editor/Base}
         */
        openItem: function (item, where, mixin, select) {
            if (!item || !item.getPath) {
                console.error('invalid item!');
                return null;
            }

            if (item && !item.getPath().match(/^(.*\.(?!(zip|tar|gz|bzip2)$))?[^.]*$/i)) {
                return;
            }
            const thiz = this;
            const ctx = this.ctx;

            let root = where || registry.byId(this.appendTo);
            const docker = ctx.getMainView().getDocker();
            //create tab
            const title = utils.toString(item.name);
            if (_.isFunction(where)) {
                root = where({
                    title: title,
                    icon: 'fa-code',
                    location: null,
                    tabOrientation: null
                });
            } else {
                root = docker.addTab(null, {
                    title: title,
                    target: where ? where._parent : null,
                    icon: 'fa-code'
                });
            }

            if ( false ) {
                if (!root) {
                    console.error('have no attachment parent!');
                    return;
                }
            }

            const args = {
                dataItem: item,
                item: item,
                filePath: item.path,
                config: this.config,
                delegate: this,
                parentContainer: root,
                store: this.store,
                style: 'padding:0px;height:inherit;height:width;',
                iconClass: 'fa-code',
                autoSelect: true,
                ctx: ctx,
                /***
                 * Provide a text editor store delegate
                 */
                storeDelegate: {
                    getContent: function (onSuccess) {
                        thiz.ctx.getFileManager().getContent(item.mount, item.path, onSuccess);
                    },
                    saveContent: function (value, onSuccess, onError) {
                        thiz.ctx.getFileManager().setContent(item.mount, item.path, value, onSuccess);
                    }

                },
                title: title,
                closable: true,
                fileName: item.path
            };

            utils.mixin(args, mixin);
            const editor = utils.addWidget(Editor, args, this, root, true, null, null, select);
            root.resize();
            editor.resize();

            if (select !== false && root.selectChild) {
                root.selectChild(editor);
            }

            docker.resize();
            if (mixin && mixin.register) {
                this.registerView(editor, select);
            }
            return editor;
        }
    });
    return Module;
});