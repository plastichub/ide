define("xide/form/FilterSelect", [
    'dcl/dcl',
    "xdojo/declare",
    'xide/widgets/WidgetBase',
    'dojo/store/Memory'
], function (dcl,declare, WidgetBase, Memory) {
    /**
     * @TODO: trash
     */
    return dcl(WidgetBase, {
        declaredClass:"xide.form.FilterSelect",
        title: null,
        options: null,
        labelValue: null,
        value: null,
        filteringSelect: null,
        templateString: "<div class='fieldrow ' style='padding: 8px'>" +
        "<span class='field-label'>${!title}</span>" +
        "<div data-dojo-attach-point='selectNode' align='left'></div>" +
        "</div>",
        widgetChanged: function (value) {
            this.changed = true;
            if (this.userData) {
                this.userData.changed = true;
            }
        },
        startup: function () {
            const thiz = this;
            const data = this.userData;
            const options = data.options;

            const stateStore = new Memory({
                data: options,
                idProperty: 'value',
                getLabel: function () {
                    console.log('asdfasdf', arguments);
                }
            });

            const selectedItem = stateStore.get(data.value);

            const select = new FilteringSelect({
            name: "state",
            labelType: 'html',
            store: stateStore,
            searchAttr: "label2",
            labelAttr: 'label'
        }, this.selectNode);

            select.startup();

            if (selectedItem) {
                //select.set('value',selectedItem);
                /*select._setValueAttr(selectedItem.label,true,selectedItem.label,selectedItem);*/
                setTimeout(function () {
                    select.set('item', selectedItem);
                }, 100);
            }
            select.on('change', function (val) {
                thiz.userData['value'] = val;
                thiz.setValue(thiz.userData['value']);
                thiz.widgetChanged(true);
            });
            this.filteringSelect = select;
        }
    });
});
