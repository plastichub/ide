/** @module xgrid/Base **/
define("xide/form/Select", [
    'xdojo/declare',
    "dcl/dcl",
    'xide/_base/_Widget',
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'xide/widgets/WidgetBase',
    'xide/utils',
    'xide/$',
    'xide/lodash',
    'dojo/dom-construct'
], function (declare, dcl, _XWidget, TreeMemory, ObservableStore, Trackable, WidgetBase, utils, $, _, domConstruct) {

    const _SelectImplementation = {
        render: function () {
            this.empty();
            this._render();
            const _picker = this.getPicker();
            _picker.val(this.value);
            _picker.render();
        },
        empty: function () {
            this.$selectNode.empty();
        },
        _destroy: function () {
            if (this._picker) {
                this._picker.destroy();
            }
            if (this.store && this.store.destroy && this.store.ownStore) {
                this.store.destroy();
            }
            delete this._picker;
            delete this.store;
        },
        getPicker: function () {
            if (!this._picker) {
                this._picker = this.$selectNode.selectpicker(this.selectOptions).data().selectpicker;
            }
            return this._picker;
        },
        _createStore: function (options) {
            const storeClass = declare('driverStore', [TreeMemory, Trackable, ObservableStore], {});
            const store = new storeClass({
                idProperty: 'value',
                ownStore: true
            });
            store.setData(options);
            return store;
        },
        set: function (what, value, label) {
            if (this.store && what === 'options') {
                this.store.setData(value);
                this._render();
                this.set('value', this.value);
            }
            if (what === 'value') {
                this.value = value;
                const _picker = this.getPicker();
                value !== null && _picker.refresh();
                _picker.val(value, label);
                _picker.render();
                return null;
            }
            return this.inherited(arguments);
        },
        get: function (what) {
            if (what === 'value') {
                const picker = this.getPicker();
                const val = picker.val();
                return val;
            }
            return this.inherited(arguments);
        },
        _render: function () {
            this._destroyItems();

            if (this.store) {
                let items = [];
                if (_.isArray(this.query)) {
                    const self = this;
                    _.each(this.query, function (query) {
                        items = items.concat(self.store.query(query));
                    });
                } else {
                    items = this.store.query(this.query || {});
                }
                this._renderItems(items);
            } else if (this.options) {
                this._items = [];
                _.each(this.options, function (item) {
                    const renderedItem = this._renderItem(item);
                    // look for `placeAt` method to support items rendered as widgets
                    if ('placeAt' in renderedItem) {
                        renderedItem.placeAt(this);
                    }
                }, this)

            }
        },
        _renderItems: function (queryResults) {
            this._items = [];
            let groups = _.groupBy(queryResults, function (obj) {
                return obj.group;
            });
            groups = utils.toArray(groups);
            if (groups.length == 1 && groups[0].name === 'undefined') {
                groups = [];
            }
            if (groups.length) {
                _.each(groups, function (group) {
                    const groupNode = this.selectNode.appendChild($('<optgroup label="' + group.name + '">')[0]);
                    _.each(group.value, function (item) {
                        this._renderItem(item, groupNode);
                    }, this);
                }, this);
            } else {
                queryResults.forEach(function (item) {
                    const renderedItem = this._renderItem(item);
                    // look for `placeAt` method to support items rendered as widgets
                    if ('placeAt' in renderedItem) {
                        renderedItem.placeAt(this);
                    }
                }, this);
            }
        },
        _renderItem: function (item, parent) {
            if (item.divider) {
                return (parent || this.selectNode).appendChild($('<option data-divider="true"></option>')[0]);
            } else {
                const _label = this.labelField ? item[this.labelField] : item.label;
                const _value = this.valueField ? item[this.valueField] : item.value;
                if (!_.find(this._items, {
                        value: _value
                    })) {
                    this._items.push({
                        label: _label,
                        value: _value
                    });
                }
                const option = $('<option value="' + _value + '" data-icon="' + item.icon + '">' + _label + '</option>');
                (parent || this.selectNode).appendChild(option[0]);
                option.data("item", item);
                return option[0];
            }
        },
        _destroyItems: function () {
            const preserveDom = true;
            this.destroyDescendants(preserveDom);
            this.empty();
            this._items = [];
        },
        startupPost: function () {
            const thiz = this;
            this.$selectNode.on('change', function (args) {
                const _value = thiz.get('value');
                thiz.value = _value;
                thiz._emit('change', _value);
                thiz.setValue(_value);
            });
        }
    };
    const _SelectizeImplementation = {
        render: function () {
            this.empty();
            this._render();
            const _picker = this.getPicker();
            _picker.val(this.value, null, true);
        },
        empty: function () {
            this.$selectNode.empty();
        },
        _destroy: function () {
            if (this._picker) {
                this._picker.destroy();
            }
            if (this.store && this.store.destroy && this.store.ownStore) {
                this.store.destroy();
            }
            delete this._picker;
            delete this.store;
        },
        getDefaultOptions: function () {
            return utils.mixin({
                valueField: this.valueField || 'value',
                labelField: this.labelField || 'label',
                searchField: this.labelField || 'label',
                options: [],
                create: true,
                createOnBlur: true,
                plugins: ['restore_on_backspace'],
                persist: false,
                dropdownParent: 'body'
            }, this.selectOptions);
        },
        getPicker: function () {
            if (!this._picker) {
                const _options = this.getDefaultOptions();
                this._picker = $(this.selectNode).selectize(_options).data().selectize;
                this._picker.val = function (val, label, silent) {
                    if (val) {
                        return this.setValue(val, silent);
                    } else {
                        const res = this.getValue();
                        return res;
                    }
                }
            }
            return this._picker;
        },
        _createStore: function (options) {
            const storeClass = this.storeClass || declare('driverStore', [TreeMemory, Trackable, ObservableStore], {});
            const store = new storeClass({
                idProperty: 'value',
                ownStore: true
            });
            store.setData(options);
            return store;
        },
        set: function (what, value, label, silent) {
            const _picker = this.getPicker();
            if (what === 'disabled') {
                value ? _picker.disable() : _picker.enable();
            }
            if (what === 'value') {
                this.value = value;
                _picker.val(value, label, silent);
                return null;
            }
            return this.inherited(arguments);
        },
        get: function (what) {
            if (what === 'value') {
                return this.getPicker().val();
            }
            return this.inherited(arguments);
        },
        _render: function () {
            this._destroyItems();
            if (this.store) {
                let items = [];
                if (_.isArray(this.query)) {
                    const self = this;
                    _.each(this.query, function (query) {
                        items = items.concat(self.store.query(query));
                    });
                } else {
                    items = this.store.query(this.query || {});
                }
                this._renderItems(items);
            } else if (this.options) {
                this._items = [];
                _.each(this.options, function (item) {
                    const renderedItem = this._renderItem(item);
                    // look for `placeAt` method to support items rendered as widgets
                    if (renderedItem && 'placeAt' in renderedItem) {
                        renderedItem.placeAt(this);
                    }
                }, this)

            }
        },
        _renderItems: function (queryResults) {
            this._items = [];
            queryResults.forEach(function (item) {
                const renderedItem = this._renderItem(item);
            }, this);
        },
        _renderItem: function (item) {
            const picker = this.getPicker();
            if (!item.divider) {
                const _label = this.labelField ? item[this.labelField] : item.label;
                const _value = this.valueField ? item[this.valueField] : item.value;
                if (!_.find(this._items, {
                        value: _value
                    })) {
                    const _item = {
                        label: _label,
                        value: _value
                    };
                    this._items.push(_item);
                    picker.addOption(_item);
                }
            }
        },
        _destroyItems: function () {
            const preserveDom = true;
            this.destroyDescendants(preserveDom);
            this.empty();
        },
        startupPost: function () {
            const thiz = this;
            this.getPicker().on('change', function () {
                thiz._emit('change', thiz.get('value'));
                thiz.setValue(thiz.get('value'));
            });
        }
    };

    const substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            let matches;
            // an array that will be populated with substring matches
            matches = [];
            // regex used to determine if a string contains the substring `q`
            //var substrRegex = new RegExp(q, 'i');
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                matches.push(str);
            });
            cb(matches);
        };
    };
    const _TypeAheadImplementation = {
        _items: function () {
            return _.map(this.store ? this.store.query(this.query) : this.options, function (item) {
                const _label = this.labelField ? item[this.labelField] : item.label;
                const _value = this.valueField ? item[this.valueField] : item.value;
                return {
                    name: _label,
                    value: _value,
                    run: item.run
                }
            }, this);
        },
        init: function (value) {
            const input = domConstruct.create('input', {
                "class": "form-control input-transparent",
                type: "text",
                value: value || "No Value"
            });
            this.$selectNode.parent().append(input);
            this.$selectNode.remove();
            this.$editBox = $(input);
            const options = {};
            const self = this;
            const typeahead = $(input).typeahead(utils.mixin({
                hint: true,
                highlight: true,
                minLength: 0,
                name: 'states',
                source: substringMatcher(self._items()),
                autoSelect: true,
                showHintOnFocus: 'all',
                appendTo: $("body"),
                //showHintOnFocus:true,
                select: function () {
                    const val = this.$menu.find('.active').data('value');
                    this.$element.data('active', val);
                    if (this.autoSelect || val) {
                        let newVal = this.updater(val);
                        // Updater can be set to any random functions via "options" parameter in constructor above.
                        // Add null check for cases when updater returns void or undefined.
                        if (!newVal) {
                            newVal = "";
                        }
                        self.typeahead.value = self.valueField ? newVal[self.valueField] : newVal.value;
                        this.$element.val(this.displayText(newVal, true) || newVal).change();
                        this.afterSelect(newVal);
                    }
                    return this.hide();
                },
                highlighter: function (item) {
                    return item;
                },
                displayText: function (item, isValue) {
                    if (isValue) {
                        return self.labelField ? item[self.labelField] : item.name;
                    }
                    return '<span class="text-info">' + item.value + '</span>';
                }
            }, options));

            this.typeahead = $(typeahead).data('typeahead');
            this.$editBox.on('click', function () {
                self.typeahead.lookup("");
            })

        },
        render: function () {
            this.empty();
            const _picker = this.getPicker();
            _picker.val(this.value, null, true);
        },
        empty: function () {
            //this.$selectNode.empty();
        },
        _destroy: function () {
            if (this._picker) {
                this.typeahead.destroy();
            }
            if (this.store && this.store.destroy && this.store.ownStore) {
                this.store.destroy();
            }
            delete this._picker;
            delete this.store;
        },
        getDefaultOptions: function () {
            return utils.mixin({
                valueField: this.valueField || 'value',
                labelField: this.labelField || 'label',
                searchField: this.labelField || 'label',
                options: [],
                create: true,
                createOnBlur: true,
                plugins: ['restore_on_backspace'],
                persist: false,
                dropdownParent: 'body'
            }, this.selectOptions);
        },
        getPicker: function () {
            if (!this._picker) {
                this._picker = this.$editBox;
            }
            return this._picker;
        },
        _createStore: function (options) {
            const storeClass = this.storeClass || declare('driverStore', [TreeMemory, Trackable, ObservableStore], {});
            const store = new storeClass({
                idProperty: 'value',
                ownStore: true
            });
            store.setData(options);
            return store;
        },
        set: function (what, value, label, silent) {
            const _picker = this.getPicker();
            if (what === 'disabled') {
                this.$editBox[value === true ? 'attr' : 'removeAttr'](what, value);
            }
            if (what === 'value') {
                this.value = value;
                this.typeahead.value = value;
                _picker.val(value, label, silent);
                return null;
            }
            return this.inherited(arguments);
        },
        get: function (what) {
            if (what === 'value') {
                return this.typeahead.value;
            }
            return this.inherited(arguments);
        },
        _renderItems: function (queryResults) {
            queryResults.forEach(this._renderItem, this);
        },
        onStoreChanged: function () {
            this.typeahead.source = substringMatcher(this._items());
        },
        _destroyItems: function () {
            const preserveDom = true;
            this.destroyDescendants(preserveDom);
            this.empty();
        },
        startupPost: function () {
            const thiz = this;
            this.getPicker().on('change', function () {
                thiz._emit('change', thiz.get('value'));
                thiz.setValue(thiz.get('value'));
            });
        }
    };

    const Module = dcl([WidgetBase, _XWidget.StoreMixin], {
        declaredClass: 'xide.form.Select',
        $selectNode: null,
        search: false,
        style: 'btn-info',
        store: null,
        query: null,
        title: 'Choose:',
        disabled: false,
        select: null,
        value: null,
        selectOptions: {
            container: 'body'
        },
        typeahead: false,
        noStore: false,
        inline: false,
        widgetClass: 'widgetContainer widgetBorder widgetTable widget',
        templateString: "<div class='${!widgetClass}' style=''>" +
            "<table border='0' cellpadding='5px' width='100%'>" +
            "<tbody align='left'>" +
            "<tr attachTo='extensionRoot' valign='middle' style='height:90%'>" +
            "<td attachTo='titleColumn' width='15%' class='widgetTitle'><span attachTo='titleNode'>${!title}</span></td>" +
            "<td valign='middle' class='widgetValue' attachTo='valueNode' width='100px'>" +
            '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode">' +
            "</td>" +
            "<td class='extension' attachTo='previewNode'></td>" +
            "<td class='extension' attachTo='button0'></td>" +
            "<td class='extension' attachTo='button1'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
        EDITABLE_CLASS: _SelectizeImplementation,
        init: function (value) {},
        _createStore: function (options) {
            const storeClass = declare('driverStore', [TreeMemory, Trackable, ObservableStore], {});
            const store = new storeClass({
                idProperty: 'value',
                ownStore: true
            });
            store.setData(options);
            return store;
        },
        destroy: function () {
            this._destroy && this._destroy();
        },
        onStoreChanged: function () {},
        postMixInProperties: function () {
            if (this.inline) {
                this.templateString = '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode"></select>';
            }
        },
        startup: function () {
            if (!this.title && this.$titleNode) {
                this.$titleNode.remove();
                this.$titleNode = null;
                this.$titleColumn.remove();
                this.$titleColumn = null;
            }
            if (!this.disabled) {
                this.$selectNode.removeAttr('disabled');
            }
            const userData = this.userData || {};
            const value = userData.value || this.value;
            this.$titleNode && this.$titleNode.html(userData.title);

            utils.mixin(this, this.editable ? _TypeAheadImplementation : _SelectImplementation);

            if (this.noStore !== true && !this.store && this.options) {
                this.store = this._createStore(this.options);
                const _selected = _.find(this.options, {
                    selected: true
                });
                _selected && (this.value = _selected.value);
            }

            this.init(value);
            this.select = this.getPicker();
            const self = this;
            if (this.store) {
                this.render();
                this.wireStore(this.store, function (event) {
                    if (event.type === 'update' && event.value === self._lastUpdateValue) {
                        return;
                    }
                    self._lastUpdateValue = event.value;
                    self.onStoreChanged(event);
                });

                this.store._on('update', function (event) {
                    if (event.type === 'update' && event.value === self._lastUpdateValue) {
                        return;
                    }
                    self._lastUpdateValue = event.value;
                    self.onStoreChanged(event);
                });
            } else {
                this.render();
            }
            if (value != null) {
                this.set('value', value, null, true);
            }
            this.startupPost();
            this.onReady();
        }
    });

    Module.SELECTIZE = _SelectizeImplementation;

    Module.SELECT = _SelectImplementation;

    Module.TYPEAHEAD = _TypeAheadImplementation;

    return Module;
});