define("xide/serverDebug", [
    'xdojo/declare',
    'xdojo/has'
],function(declare,has){

    const Module = declare("xide.serverDebug", null,{

    });

    /**
     * @var object
     */
    const local_storage = null;
    const color1 = '#888';
    const color2 = '#0563ad';

    const ALLOWED_TYPES = {
        'group': 1,
        'groupEnd': 1,
        'groupCollapsed': 1,
        'warn': 1,
        'error': 1,
        'info': 1,
        'table': 1,
        'log': 1
    };

    /**
     * @var array
     */
    const queue = [];

    /**
     * @var bool
     */
    const use_queue = true;

    /**
     * should we show line numbers?
     *
     * @return bool
     */
    function _showLineNumbers()
    {
        return true;
    }

    /**
     * logs nicely formatted data in new format
     *
     * @param Object
     * @return void
     */
    function _logData(data, callback)
    {
        //console.log('server log : ',data);

        const column_map = {};
        let column_name;

        for (const key in data.columns) {
            column_name = data.columns[key];
            column_map[column_name] = key;
        }

        const rows = data.rows;
        let i = 0;
        const length = rows.length;

        for (i = 0; i < length; i++) {
            const row = rows[i];
            const backtrace = row[column_map.backtrace];
            const label = row[column_map.label];
            let log = row[column_map.log];
            let type = row[column_map.type] || 'log';

            if (_showLineNumbers() && backtrace !== null) {
                console.log('%c' + backtrace, 'color: ' + color1 + '; font-weight: bold;');
            }

            // new version without label
            let new_version = false;
            if (!data.columns.includes('label')) {
                new_version = true;
            }

            // if this is the old version do some converting
            if (!new_version) {
                const show_label = label && typeof label === "string";

                log = [log];

                if (show_label) {
                    log.unshift(label);
                }
            }

            const logs = [];
            let current_log;
            let last_log;
            let new_string;

            // loop through logs to add in any class name labels that should be here
            for (let j = 0; j < log.length; j++) {
                current_log = log[j];
                last_log = logs[logs.length - 1];

                if (current_log && typeof current_log === 'object' && current_log['___class_name']) {
                    new_string = '%c' + current_log['___class_name'];

                    if (typeof last_log === 'string') {

                        // if the last log was a string we need to append to it
                        // in order for the coloring to work correctly
                        logs[logs.length - 1] = last_log + ' ' + new_string;
                    }
                    else {

                        // otherwise just push the new string to the end of the list
                        logs.push(new_string);
                    }

                    logs.push('color: ' + color2 + '; font-weight: bold;');
                    delete log[j]['___class_name'];
                }

                logs.push(current_log);
            }

            if (!(type in ALLOWED_TYPES)) {
                type = 'log';
            }

            console[type].apply(console, logs);
        }

        if (typeof callback === 'function') {
            callback();
        }
    }
    function _decode(header) {
        return JSON.parse(atob(header));
    }

    Module.logError = function(data){
        _logData(_decode(data));
    }

    return Module;
});