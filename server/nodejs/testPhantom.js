console.log('Loading a web page');
var phantom = require("phantom");
var _ph, _page, _outObj;
phantom.create().then(ph => {
    _ph = ph;
return _ph.createPage();
}).then(page => {
    _page = page;
return _page.open('https://stackoverflow.com/');
}).then(status => {
    console.log(status);
return _page.property('content')
}).then(content => {
    console.log(content);
_page.close();
_ph.exit();
}).catch(e => console.log(e));
/*
var Page = require('./node_modules/phantom/lib/page');
console.dir(Page);
var page = new Page();
var url = 'http://phantomjs.org/';
page.open(url, function (status) {
    //Page is loaded!
    //phantom.exit();
});
*/