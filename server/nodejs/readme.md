xDojoFileWatch
==============

xDojoFileWatch does 
1. setup a web socket server
2. registers directory watchers 
3. broadcasts a message about file changes 


##Installation

``` bash 
git clone https://github.com/mc007/xDojoWatch.git && cd xDojoWatch && git submodule init && git submodule update && npm install
```

##Attention

