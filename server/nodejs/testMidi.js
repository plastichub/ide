var midi = require('midi');

// Set up a new input.
var input = new midi.input(1);

// Count the available input ports.
console.log(input.getPortCount());

// Get the name of a specified input port.
console.log(input.getPortName(1));

// Configure a callback.
input.on('message', function(deltaTime, message) {
    // The message is an array of numbers corresponding to the MIDI bytes:
    //   [status, data1, data2]
    // https://www.cs.cf.ac.uk/Dave/Multimedia/node158.html has some helpful
    // information interpreting the messages.
    console.log('m:' + message + ' d:' + deltaTime);
});
input.openPort(1);