var five = require("johnny-five"),
    board, potentiometer;

board = new five.Board();

board.on("ready", function() {

        // Assuming a sensor is attached to pin "A1"
    this.pinMode(3, five.Pin.ANALOG);
    this.analogRead(3, function(voltage) {
        console.log(voltage);
    });

});
/*
board.on("ready", function() {

    // Create a new `potentiometer` hardware instance.
    potentiometer = new five.Sensor({
        pin: "A3",
        freq: 250
    });

    // Inject the `sensor` hardware into
    // the Repl instance's context;
    // allows direct command line access

    // "data" get the current reading from the potentiometer
    potentiometer.on("data", function() {
        console.log(this.value, this.raw);
    });
});


*/