/*var analytics = require('universal-ga');
analytics.initialize('UA-75837011-1');
analytics.pageview('/about');
*/
var mpanalytics = require('mpanalytics');

var tracker = mpanalytics.create({
   tid: 'UA-75837011-1',
   cid: 'my anonymous client identifier',
   sampleRate: 100,
});

tracker.pageview({
   hostname: 'pearls-media.com',
   page: '/home',
}, function (err) {
   console.log(err);
});

tracker.event({
   category: 'test',
   action: 'alright',
});
