## Pre-Requisites

### Linux

    sudo apt-get install build-essential
    sudo apt-get install gawk
    sudo apt-get install libzmq3-dev libzmq3
    sudo apt-get install libasound2-dev

### Mac
    
    sudo port install zmq
    ffmpeg -i input_file.mpg -pix_fmt yuv420p -f yuv4mpegpipe - | x265-10bit --profile main10 --preset slower --crf 20 --input - --y4m -o output_file.mpg
    

### 1. Build Server - Dojo 

From : Ubuntu

    grunt dojo:server-build

### 2. Next, copy *serverbuild.js* to dist/all

    grunt copy:server-build

### 3. Copy *dist/all* to each platform 

    grunt copy-server

<hr/>


    