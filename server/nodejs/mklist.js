var fs = require('fs');
var path = require('path');

function readFile(file) {
    //file = file;
    var size = fs.statSync(file).size,
        buf = new Buffer(size),
        fd = fs.openSync(file, 'r');
    if (!size)
        return "";
    fs.readSync(fd, buf, 0, size, 0);
    fs.closeSync(fd);
    return buf.toString();
}

function resolve(_path){
    var here = path.resolve(_path);
    if (fs.statSync(here)) {
        return here;
    }
    if (fs.statSync(_path)) {
        return _path;
    } else {
        var __path = process.cwd() + path.sep + _path;
        if (fs.statSync(__path)) {
            return __path;
        }
    }
    return null;
};

function writeFile (file,content){
    fs.writeFile(file, content, function(err) {
        if(err) {
            console.log(err);
        } else {
            //console.log("The file was saved! : \n");
        }
    });
};

console.log('mk list ' +resolve('./list.txt'));

var content = readFile(resolve('./list.txt')).split('\n');
var out = "";

var blacklist = ['serialport',
    'browser-serialport',
    'firmata',
    'gbaumgart.inotify-plusplus',
    'winston',
    'inotify',
    'help-me',
    'mosca',
    'meow',
    'prebuild',
    'currently-unhandled',
    'loud-rejection',
    'fs-ext',
    'kerberos',
    'ascoltatori',
    'hiredis',
    'qlobber',
    'qlobber-fsq',
    'bson',
    'mongodb',
    'mongodb-core',
    'require_optional',
    'rc',
    'aproba',
    'leveldown'
];

for (var i = 0; i < content.length; i++) {
    var dep = content[i];
    if(dep.length && blacklist.indexOf(dep)==-1) {
        out += "var a_" + i + " = require(\"" + dep.trim() + "\");\n";
    }
}
//console.log(out);
writeFile(resolve("nxappmain/deps.js"),out);

