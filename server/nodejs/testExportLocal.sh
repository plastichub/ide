#!/usr/bin/env bash


#./server noob --file="./export.js" --user="../../../../user/" --system="../../../../data" --root="../../../../" --target="../../../../myapp2"
rm -rf ../../myapp2/
node ./nxappmain/export.js --windows=false --linux32=false --osx=false --arm=false --user="../../user/" --system="../../data" --root="../../" --target="../../myapp2" --dist="./dist/" --client="../../dist/windows/Code/client" "$@"

cp build/server/nxappmain/serverbuild.js.uncompressed.js ../../myapp2/server/linux_64/nxappmain/serverbuild.js

cp ../../Code/client/src/lib/xapp/build/main_build.js ../../myapp2/www/Code/client/src/lib/xapp/build/main_build.js

cd ../../myapp2
sh start_linux.sh
