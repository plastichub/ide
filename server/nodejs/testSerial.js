var com = require("serialport");
var readline= require("readline");


var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


var SerialPort = require("serialport");


SerialPort.list(function (err, ports) {
    ports.forEach(function(port) {
        if(port && (port.comName && port.pnpId)) {
            console.log(port.comName);
            console.log(port.pnpId);
            console.log(port.manufacturer);
        }
    });

});


var options = {
    baudrate: 9600,
    parity: 'none',//'none', 'even', 'mark', 'odd', 'space'
    rtscts: false,
    xon: false,
    xoff: false,
    xany: false,
    hupcl:true,
    rts: true,
    cts: false,
    dtr: true,
    dts: false,
    brk: false,
    databits: 8,
    stopbits: 1,
    buffersize: 256,
    parser: com.parsers.readline('\r')
};
var serialPort = new com.SerialPort("/dev/ttyUSB0", options);
/*
var serialPort = new com.SerialPort("/dev/ttyUSB0", {
    baudrate: 9600,
    parser: com.parsers.readline('\r')
});
*/

function writeAndDrain (data, callback) {
    serialPort.write(data, function () {
        serialPort.drain(callback);
    });
}


rl.on('line', function (cmd) {
    var lineBreak = '\\r';
    var lb = JSON.parse('"' + lineBreak + '"');
    /*
     var n1 = lineBreak.charCodeAt(0);
     var n2 = lineBreak.charCodeAt(1);

     if(!isNaN(n1) && !isNaN(n2)){
     if(n1 == 92){// means '\'
     if(n2 == 114 ){
     lineBreak = '\r';
     }
     if(n2 == 110 ){
     lineBreak = '\n';
     }
     }
     }*/

    writeAndDrain(cmd + (lb || '\r'));
});



function test(){

    console.log('start tests');
    writeAndDrain('pwon\n',function(e){
       console.log('--drain');
    });
    //serialPort.write("@ZMON\r");
    //serialPort.write('@pwon\r');
    //serialPort.write('pwon\r');
    /*
    serialPort.write("PWON<CR>");
    serialPort.write("ZMON");
    serialPort.write("@PWON\r");
    serialPort.write("PWON\r");
    serialPort.write("@pwon\r");
    serialPort.write("@MV?\n");
    serialPort.write("@PW?\r");
    serialPort.write("@MV35");
    */
}

serialPort.on('open',function(err) {
    console.log('Port open',err);
    setTimeout(test,3000);
    serialPort.on('data', function(data) {
        console.log('data',data);
    });

});

serialPort.on('data', function(data) {
    console.log('data',data);
});

serialPort.on('error', function(data) {
    console.log('error',data);
});
serialPort.on('close', function(data) {
    console.log('close',data);
});
