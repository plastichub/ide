define([
    'intern!tdd',
    'intern/chai!assert',
    "nxapp/utils/_console"
], function (test,assert,_console) {
    var sctx = global.sctx;
    try {

        if(sctx) {
            var deviceManager = sctx.getDeviceManager();
            var driverManager = sctx.getDeviceManager();

            var loopBackServer = deviceManager.getDeviceByName('Loopback');
            var loopBackClient = deviceManager.getDeviceByName('Loopback-Client');

            var loopBackClientInfo = deviceManager.toDeviceControlInfo(loopBackClient);

            //console.log('loopBackClientInfo ',loopBackClientInfo);
            var loopBackServerInstance = deviceManager.getInstanceByName('Loopback');



            deviceManager.stopDevice(loopBackServer);
            deviceManager.stopDevice(loopBackClient);
            loopBackServer.setEnabled(false);
            loopBackClient.setEnabled(false);

            loopBackServerInstance = deviceManager.getInstanceByName('Loopback');
            if(loopBackServerInstance){
                console.error('found still instance');
            }

            test.suite('Loopback Basics', function () {

                test.test('Found Loopback Server', function () {
                    assert.notEqual(loopBackServer, null, 'Cant get loopback server');
                });
                test.test('Found Loopback Client', function () {
                    assert.notEqual(loopBackClient, null, 'Cant get loopback client');
                });
                test.test('Loopback Client Enable', function () {
                    assert.equal(loopBackClient.isEnabled(), false, 'Loopback client is not disabled');
                });
                test.test('Loopback Server Enable', function () {
                    assert.equal(loopBackServer.isEnabled(), false, 'Loopback server is not disabled');
                });


                test.test('Start Loopback Server - Testing Abort because device not enabled', function (){
                    var async = this.async(3000);
                    deviceManager.startDevice(loopBackServer).then(function(){
                        async.reject();
                    },function(err){
                        async.resolve();
                    });
                });








                test.test('Stop Loopback Server', function (){
                    var async = this.async(1000);
                    deviceManager.stopDevice(loopBackServer);
                    var loopBackServerInstance = deviceManager.getInstanceByName('Loopback');
                    if(!loopBackServerInstance) {
                        async.resolve();
                    }else{
                        async.reject('Instance still there: ' + (loopBackServerInstance ? 'server is up' : 'server is down'));
                    }
                });
                test.test('Stop Loopback Client', function (){
                    var async = this.async(1000);
                    deviceManager.stopDevice(loopBackClient);
                    var loopBackClientInstance = deviceManager.getInstanceByName('Loopback-Client');
                    if(!loopBackClientInstance) {
                        async.resolve();
                    }else{
                        async.reject('Instance still there: ' + (loopBackClientInstance? 'client is up' : 'client is down'));
                    }
                });

                test.test('Enable Loopback Server', function (){
                    loopBackServer.setEnabled(true);
                    assert.equal(loopBackServer.isEnabled(), true, 'Loopback server should be enabled by now');
                });

                test.test('Enable Loopback Client', function (){
                    loopBackClient.setEnabled(true);
                    assert.equal(loopBackClient.isEnabled(), true, 'Loopback client should be enabled by now');
                });

                test.test('Set Loopback Client running server side', function (){
                    loopBackClient.setServerSide(true);
                    assert.equal(loopBackClient.isServerSide(), true, 'Loopback client should be server side by now');
                });


                test.test('Start Loopback Server - Testing DeviceManager::startDevice', function (){
                    var async = this.async(3000);
                    setTimeout(function() {
                        deviceManager.startDevice(loopBackServer).then(function () {
                            async.resolve();
                        }, function (err) {
                            console.log('--- ok, startDevice rejected ');
                            console.log('Device Started failed');
                            async.rejected();
                        });
                    },1000);
                });





                test.test('Start Loopback Client - Testing DeviceManager::startDevice ', function (){
                    var async = this.async(5000);
                    setTimeout(function(){
                        deviceManager.startDevice(loopBackClient).then(function(){
                            async.resolve();
                        },function(err){
                            async.rejected();
                        });
                    },2000);
                });
















                /*
                test.test('Stop Loopback Client & Server', function (){
                    var async = this.async(10000);
                    deviceManager.stopDevice(loopBackServer);
                    deviceManager.stopDevice(loopBackClient);
                    setTimeout(function(){
                        var loopBackServerInstance = deviceManager.getInstanceByName('Loopback');
                        var loopBackClientInstance = deviceManager.getInstanceByName('Loopback-Client');
                        if(!loopBackClientInstance && !loopBackServerInstance) {
                            async.resolve();
                        }else{
                            async.reject('Instance still there: ' + (loopBackClientInstance ? 'client is up' : 'client is down' )  + ' |  ' + (loopBackServerInstance ? 'server is up' : 'server is down'));
                        }
                    },5000);
                });
                */










                /*
                test.test('Stop Loopback Server', function (){
                    var async = this.async(20000);
                    deviceManager.stopDevice(loopBackServer);
                    setTimeout(function(){
                        var loopBackServerInstance = deviceManager.getInstanceByName('Loopback');
                        if(!loopBackServerInstance) {
                            async.resolve();
                        }else{
                            async.reject('Instance still there: ' + (loopBackServerInstance ? 'server is up' : 'server is down'));
                        }
                    },18000);
                });
                */


            });
        }
    }catch(e){
        console.log('eee',e);
    }
});
