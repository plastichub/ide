define([
    'intern!tdd',
    'intern/chai!assert',
    "dojo/node!path",
    "dojo/node!os",
    "dojo/node!util",
    "dojo/node!extend",
    "dojo/node!fs",
    "dojo/node!tracer",
    "dojo/node!yargs",
    "dojo/node!child_process"
], function (test, assert, path, os, util, extend, fs, tracer, yargs, exec) {

    //https://gist.github.com/jason0x43/3e3c00265c3fc573b48d
    //https://github.com/theintern/intern-tutorial/issues/18

    var console = console;
    if (tracer) {
        console = tracer.colorConsole({
            format: "{{title}}: {{message}}",
            dateformat: "HH:MM:ss.L"
        });
    }


    console.log('yargs ', yargs.argv);
    //console.log(grunt.option('asdf'));


    var eol = os.EOL;
    var OS = "linux";
    if (os.platform() === 'win32') {
        OS = 'windows';
    } else if (os.platform() === 'darwin') {
        OS = 'osx';
    } else if (os.arch() === 'arm') {
        OS = 'arm';
    }

    var os_suffix = 'Linux';
    if (os.platform() === 'win32') {
        os_suffix = 'Windows';
    } else if (os.platform() === 'darwin') {
        os_suffix = 'OSX';
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Paths
    //
    var BOILER_ROOT = path.resolve('.');
    var PROJECT_ROOT = path.resolve('../../');
    var DIST_ALL = path.resolve(PROJECT_ROOT + '/dist/all');
    var DIST_FINAL = path.resolve(PROJECT_ROOT + '/dist/' + OS) + path.sep;//windows default
    var DIST_DIRECTORY = path.resolve('./tmp' + os_suffix + '/Control-Freak') + path.sep;//windows default
    if (OS === 'linux' || OS === 'arm') {
        DIST_DIRECTORY = path.resolve('./tmp' + os_suffix + '/Control-Freak/opt/Control-Freak') + '/';
    } else if (OS === 'osx') {
        DIST_DIRECTORY = path.resolve('./tmp' + os_suffix + '/Control-Freak.app/Resources') + '/';
    }

    var APP_ROOT = DIST_DIRECTORY;//compat name
    var UTILS_ROOT = path.resolve(DIST_DIRECTORY + '/Code/utils/');//Utils


    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Defaults
    //
    var options = {
        stdout: true,
        stderr: true,
        stdin: true,
        failOnError: true,
        stdinRawMode: false
    };

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Helpers
    //
    function start(path, args, options, name, report) {

        var debug = false;

        if (OS !== "windows") {
            try {
                exec.execFile('chmod', ['+x', path]);
            } catch (e) {
            }
        }

        if (OS == 'windows') {
            path += '.exe';
        }

        if (!fs.existsSync(path)) {
            debug && console.error("Sorry, but cant start " + name + '. ' + path + 'doesnt exists!');
            report("Does not exists : " + report);
            return;
        }

        debug && console.info('Start ' + name);

        options.path = path;
        options.name = name;

        var process = exec.spawn(path, args || [], options, function (err, stdout, stderr) {
            if (typeof options.callback === 'function') {
                options.callback.call(this, err, stdout, stderr);
            } else {
                if (err && options.failOnError) {
                    debug && console.error('--err ', err);
                }
                //options.callback();
            }
        }.bind(this));

        process.stdout.on('data', function (data) {

            var str = data.toString();

            if (options.silent !== true) {
                debug && console.debug('stdout data (pid:' + process.pid + ' name:' + name + '):');
                debug && console.log(name + '\n\t' + str);
            }

            if (options.already && str.indexOf(options.already) !== -1) {
                debug && console.warn('Abort ' + options.name + ' , seems already running.');
                //pids.remove(process);
                options.killed = true;
                if (options.alreadyCallback) {
                    options.alreadyCallback();
                }
            }
            if (options.ready && options.readyCB && str.indexOf(options.ready) !== -1) {
                options.readyCB();
            }

            report(str);

        });
        process.stderr.on('data', function (data) {
            debug && console.debug('stderr data (pid:' + process.pid + ' name:' + name + '):');
            var str = data.toString();
            var newStr = String(str).split(eol).join(eol + '\t');
            debug && console.log(name + '\n\t' + newStr);
            if (options.already && str.indexOf(options.already) !== -1) {
                debug && console.warn('Abort ' + options.name + ' , seems already running.');
                options.killed = true;
                if (options.alreadyCallback) {
                    options.alreadyCallback();
                }
            }
            report(str);
        });
        process.on('close', function (code) {
            report("Closing " + code);
        });
        process.options = options;
        return process;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Servers
    //

    var pids = [];

    var DO_PHP = true;
    var DO_MONGO = false;
    var DO_NGINX = true;
    var DO_DEVICE_SERVER = true;

    // PHP
    //
    var PHP_CGI = path.resolve(DIST_DIRECTORY + '/php/php-cgi');
    var PHP_CGI_ARGS = ["-b", "127.0.0.1:9012"];
    var PHP_TIME_OUT = 3000;
    var php = null;

    function PHPReady() {
    }

    function killPHP() {
        php && php.kill('SIGINT');
    }


    //
    // NGINX
    //
    var NGINX_EXE = path.resolve(APP_ROOT + '/nginx');
    var NGINX_ARGS = ["-p", APP_ROOT + path.sep, "-c", path.resolve(APP_ROOT + '/conf/nginx.conf')];
    var NGINX_TIME_OUT = 4000;
    var nginx = null;

    function NGINXReady() {
    }

    function NGINXFailed() {
    }

    function killNGINX(nginx) {
        nginx && exec.spawn(nginx.options.kill, nginx.options.killArgs, extend({
            cwd: nginx.options.killCWD
        }, nginx.options), nginx.options.killArgs);
    }

    //
    // Mongo
    //
    var MONGO_SERVER = path.resolve(APP_ROOT + '/mongo/mongod');
    var MONGO_SERVER_ARGS = ["--smallfiles", "--quiet", "--dbpath", path.resolve(APP_ROOT + '/data/_MONGO'), "--storageEngine=mmapv1"];
    var MONGO_TIME_OUT = 8000;
    var mongo;

    function MongoReady() {
        !DO_DEVICE_SERVER && killAll();
    }

    function MongoFailed() {
        killNGINX(nginx);
        killPHP();
        mongo && mongo.kill('SIGINT');
    }

    //
    // NetCMD
    //
    var DEVICE_SERVER = path.resolve(UTILS_ROOT + '/app/xide/server');
    var DEVICE_SERVER_ARGS = [];
    var deviceServer;
    var DEVICE_SERVER_TIME_OUT = 5000;

    function DeviceServerReady() {
        killAll();
    }

    function DeviceServerFailed() {
        killAll();
    }

    function killAll() {
        killNGINX(nginx);
        killPHP();
        mongo && mongo.kill('SIGINT');
        deviceServer && deviceServer.kill('SIGINT');
    }

    test.suite('Servers', function () {
        DO_PHP && test.test('PHP', function () {
            var dfd = this.async(10000);
            var ERROR = 0;
            var ALREADY = false;
            php = start(PHP_CGI, PHP_CGI_ARGS, extend({
                cwd: path.resolve(APP_ROOT + '/php/'),
                already: "Address already in use"
            }, options), "PHP", function (str) {
                if (str.indexOf('Address already in use') !== -1) {
                    ERROR = 'Address already in use';
                    ALREADY = true;
                }
            });
            setTimeout(function () {
                //php.kill('SIGINT');
                if (ERROR === 0) {
                    dfd.resolve('Works!');
                    PHPReady(php);
                } else {
                    ALREADY && php.kill('SIGINT');
                    dfd.reject('\tError PHP : ' + ERROR);
                }
            }, PHP_TIME_OUT);
        });
        DO_NGINX && test.test('NGINX', function () {

            var dfd = this.async(10000);
            var ERROR = 0;
            var ALREADY = false;

            nginx = start(NGINX_EXE, NGINX_ARGS, extend({
                kill: NGINX_EXE,
                killCWD: APP_ROOT,
                killArgs: ['-s', 'stop']
            }, options), "NGINX", function (str) {
                //console.log('-str ' +str);
                if (str.indexOf('Closing 1') !== -1) {
                    ERROR = 'Address already in use';
                    ALREADY = true;
                }
                if (str.indexOf('Closing 0') !== -1) {
                    NGINXReady();
                }
                if (str.indexOf('Address already in use') !== -1) {
                    ERROR = 'Address already in use';
                    ALREADY = true;
                }
            });
            setTimeout(function () {
                if (ERROR === 0) {
                    dfd.resolve('Works!');
                    php.kill('SIGINT');
                    NGINXReady(nginx);
                    killNGINX(nginx);
                } else {
                    ALREADY && nginx.kill('SIGINT');
                    dfd.reject('\tError NGINX : ' + ERROR);
                    killNGINX(nginx);
                }
            }, NGINX_TIME_OUT);
        });
        DO_MONGO && test.test('MONGO', function () {

            var dfd = this.async(10000);
            var ERROR = 0;
            var ALREADY = false;

            mongo = start(MONGO_SERVER, MONGO_SERVER_ARGS, extend({
                cwd: APP_ROOT + '',
                already: "Address already in use",
                silent: true,
                ready: "waiting for connections on port"
            }, options), "NGINX", function (str) {
                if (str.indexOf('Closing 1') !== -1) {
                    ERROR = 'Address already in use';
                    ALREADY = true;
                }
                if (str.indexOf('Address already in use') !== -1) {
                    ERROR = 'Address already in use';
                    ALREADY = true;
                }
            });

            setTimeout(function () {
                if (ERROR === 0) {
                    dfd.resolve('Mongo Ready!');
                    MongoReady();
                } else {
                    ALREADY && nginx.kill('SIGINT');
                    dfd.reject('\tError MONGO : ' + ERROR);
                    MongoFailed();
                }
            }, MONGO_TIME_OUT);
        });
        DO_DEVICE_SERVER && test.test('DEVICE SERVER', function () {

            var dfd = this.async(10000);
            var ERROR = 0;
            var ALREADY = false;
            var timeout = null;

            deviceServer = start(DEVICE_SERVER, DEVICE_SERVER_ARGS, extend({
                cwd: path.resolve(UTILS_ROOT + '/app/xide')
            }, options), "NDEVICE SERVER", function (str) {

                //console.log('\t'+ str);

                if (str.indexOf('You can start') !== -1) {
                    dfd.resolve('Device Server Ready!');
                    clearTimeout(timeout);
                    DeviceServerReady();
                }
                if (str.indexOf('Closing 1') !== -1) {
                    ERROR = 'Closing with error';
                    ALREADY = false;
                }
                if (str.indexOf('Address already in use') !== -1) {
                    ERROR = 'Address already in use';
                    ALREADY = true;
                }
            });

            timeout = setTimeout(function () {
                if (ERROR === 0) {
                    dfd.resolve('Device Server Ready!');
                    DeviceServerReady();
                } else {
                    dfd.reject('\tError Device Server : ' + ERROR);
                    DeviceServerFailed();
                }
            }, DEVICE_SERVER_TIME_OUT);
        });
    });
});