define([
    'intern/lib/util',
    'dojo/has',
    'dojo/has!host-node?dojo/node!istanbul/lib/collector',
    'dojo/has!host-node?dojo/node!istanbul/lib/report/text',
    'dojo/has!host-node?dojo/node!tracer',
    'dojo/has!host-node?dojo/node!colors'
], function (util, has, Collector, TextReporter,tracer,colors) {

    var console = tracer.colorConsole({
        format : "<{{message}}",
        dateformat : "HH:MM:ss.L",
        filters : {
            trace : colors.magenta,
            debug : colors.blue,
            log : colors.blue,
            info : colors.green,
            warn : colors.yellow,
            error : [ colors.red, colors.bold ]
        }
    });

    /**
     * The console reporter outputs to the current environment's console.
     */
    function Console(config) {
        config = config || {};

        this.console = config.console;
        this.hasGrouping = 'group' in this.console && 'groupEnd' in this.console;
        this.testId = this.hasGrouping ? 'name' : 'id';
        this.delegate = config.delegate;
        if (has('host-node')) {
            this._coverageReporter = new TextReporter({
                watermarks: config.watermarks
            });
        }



    }

    Console.prototype = {
        constructor: Console,
        deprecated: function (name, replacement, extra) {
            this.console.warn(name + ' is deprecated.' +
                (replacement ?
                ' Use ' + replacement + ' instead.' :
                ' Please open a ticket at https://github.com/theintern/intern/issues if you still require access ' +
                'to this feature.') +
                (extra ? ' ' + extra : '')
            );
        },

        fatalError: function (error) {
            this.console.warn('FATAL ERROR');
            this.console.error(util.getErrorMessage(error));
        },

        reporterError: function (reporter, error) {
            this.console.error('REPORTER ERROR');
            this.console.error(util.getErrorMessage(error));
        },

        suiteEnd: function (suite) {
            // IE<10 does not provide a global console object when Developer Tools is turned off
            if (!this.console) {
                return;
            }

            var numTests = suite.numTests;
            var numFailedTests = suite.numFailedTests;
            var numSkippedTests = suite.numSkippedTests;
            var message = numFailedTests + '/' + numTests + ' tests failed';

            if (numSkippedTests > 0) {
                message += ' (' + numSkippedTests + ' skipped)';
            }

            console[numFailedTests ? 'warn' : 'info'](message);
            this.hasGrouping && this.console.groupEnd(suite.name);
        },

        suiteError: function (suite) {
            if (!this.console) {
                return;
            }
            this.console.warn('SUITE ERROR');
            console.error(util.getErrorMessage(suite.error));
        },

        suiteStart: function (suite) {
            // only start group for non-root suite
            this.hasGrouping && suite.hasParent && this.console.group(suite.name);
            this.delegate && this.delegate.onStart && this.delegate.onStart(suite);
        },

        testFail: function (test) {
            console.error('FAIL: ' + test[this.testId] + ' (' + test.timeElapsed + 'ms)');
            if(test && test.error){
                this.console.error(util.getErrorMessage(test.error));
            }
            this.delegate && this.delegate.onFail && this.delegate.onFail(test);
        },

        testPass: function (test) {
            console.info('pass: ' + test[this.testId] + ' (' + test.timeElapsed + 'ms)');
            this.delegate && this.delegate.onPass && this.delegate.onPass(test);
        },

        testSkip: function (test) {
            console.log('SKIP: ' + test[this.testId] + (test.skipped ? ' (' + test.skipped + ')' : ''));
        }
    };

    if (has('host-node')) {
        Console.prototype.coverage = function (sessionId, coverage) {
            var collector = new Collector();
            collector.add(coverage);
            // add a newline between test results and coverage results for prettier output
            console.log('');
            this._coverageReporter.writeReport(collector, true);
        };
    }

    return Console;
});
