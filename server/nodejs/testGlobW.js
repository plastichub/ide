//var glob = require("glob-fs")({});
var glob = require("glob");
var fs = require("fs");
var _path = require("path");
var base = require("glob-base");
var flop = require("flop");
var _ = require("lodash");

function toObject(path){

    try {
        var result = fs.statSync(path, function (error) {

        });

        var isDirectory = result.isDirectory();
        return {
            path: path,
            sizeBytes: result.size,
            size: isDirectory ? 'Folder' : '',
            owner: result.uid,
            group: result.gid,
            mode: result.mode,
            isDir: isDirectory,
            directory: isDirectory,
            name: _path.win32.basename(path),
            fileType: isDirectory ? 'folder' : 'file',
            modified: result.mtime.getTime() / 1000
        };

        return result;
    }catch(e){
        return {
            path: path,
            sizeBytes: 0,
            size: 0 ,
            owner: 0,
            group: 0,
            mode: 0,
            directory: false,
            mime: 'unknown',
            name: _path.win32.basename(path),
            fileType: 'file',
            modified: 0
        };
    }
}

function ls(path,options,cb){

    var self = this;
    var _options = {};
    var isWin       = process.platform === 'win32';

    console.log('ls path : ' + path + ' win= '+isWin);

    if(path ==='/*' && isWin){
        var out = [];
        flop.read('/', function(error, data) {
            _.each(data.files,function(item){
                out.push({
                    path: '/' + item.name + ':',
                    sizeBytes: 0,
                    size: 0 ,
                    owner: 0,
                    group: 0,
                    mode: 0,
                    directory: true,
                    mime: 'unknown',
                    name: item.name,
                    fileType: 'folder',
                    modified: 0,
                    drive:true
                })
            });
            self._send({
                files:out
            },'ls',options);
        });
        return;
    }

    var _root = "";

    if (isWin) {
        if(path.length===5){//   /C:/*
            var _parts = path.split(':/');
            if(_parts.length==2){
                _root = _parts[0][1] + ":/";
                _options = {
                    root:_root
                };
                path = "/*"
            }
        }else{
            var _parts = path.split(':/');
            if(_parts.length==2){
                _root = _parts[0][1] + ":/";
                _options = {
                    root:_root
                };
                path = '' + _parts[1];
            }
        }
    }
    function pathFromWin(current, root) {

        if(!current){
            return "";
        }

        if (isWin && (!root || root === '/')) {

            current = '' + current;
                //.replace(':', ':/')
                    //.replace(/\\/g, '/');
        } else {
            current = _path.join(root, current);
        }

        return current;
    }


    var files = glob.sync(path, _options);

    if(!_.isArray(files)){
        files = [files];
    }
    var out = [];
    _.each(files,function(file){
        var object = toObject(file);
        if(isWin && object && object.path) {
            object.path = _path.resolve(pathFromWin(object.path));
        }
        object && out.push(object);
    });

    return out;
}

function list(_path){
    try {
        var options = {};
        var root = base(_path);

        if (root.base) {
            options.cwd = root.base;
            if (root.isGlob) {
                _path = root.glob
            }
        }
        var _glob = new glob();
        var files = _glob.readdirSync(_path, options);
        console.dir(files);
        var ret = [];
        _.each(files, function (file) {
            ret.push((options.cwd ? options.cwd + path.sep : "" ) + file)
        });
    }catch(e){
        return null;
    }
    return ret;
}

var __path  = "C:\\home\\mc007\\Music\\Sasha\\*.mp3";
var files = ls(__path,{});
console.log('files',files);

/*
//var _glob = new glob();
var _path  = "C:\\home\\mc007\\Music\\Sasha\\*";

glob.readdir("*",{
},function(e){
    console.log(e);
});
*/

//console.log(list("C:\\home\\mc007\\Music\\Sasha\\**"));



