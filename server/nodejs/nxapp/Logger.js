define([
    "dcl/dcl",
    'xide/types',
    'xide/factory',
    "dojo/node!winston",
    "nxapp/utils/_console"
], function (dcl, types, factory, winston,_console) {

    var console = _console;

    return dcl(null, {
        declaredClass:"nxapp.Logger",
        fileLogger: null,
        loggly: null,
        delegate: null,
        publishLog: false,
        loggers:{},
        /***
         * Standard constructor for all subclassing bindings
         * @param {array} arguments
         */
        constructor: function (args) {

            //simple mixin of constructor arguments
            for (var prop in arguments) {
                if (arguments.hasOwnProperty(prop)) {

                    this[prop] = args[prop];
                }
            }
        },
        createLogger:function(options){

            var logger = new (winston.Logger)({
                transports: [
                    new (winston.transports.File)(options)
                ]
            });

            if (this.publishLog) {

                logger.on('logging', function (transport, level, msg, meta) {
                    meta.logId = options.filename;
                    var args = {
                        level: level,
                        message: msg,
                        data: meta,
                        time: new Date().getTime()

                    };
                    factory.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE,args);
                });
            }
            return logger;
        },
        log:function(){
            return this.logger.log.apply(this.logger,arguments);
        },
        verbose:function(){
            return this.logger.verbose.apply(this.logger,arguments);
        },
        info:function(){
            return this.logger.info.apply(this.logger,arguments);
        },
        start: function (options) {
            
            this.loggers = {};
            
            this.options = options;
            this.logger = new (winston.Logger)({
                //level:'verbose',
                transports: [
                    new (winston.transports.Console)({
                        level:"debug"
                    })
                ]
            });

            this.logger.transports.console.level = 'debug';

            this.logger.on('logging', function (transport, level, msg, meta) {
                if(console[level]){
                    //console[level](msg,meta);
                }else{
                    if(console.info){
                        //console.info(msg,meta);
                    }
                }

            });

            /*

            
            if (options.fileLogger) {
                this.fileLogger = winston.add(winston.transports.File, options.fileLogger);
                if (this.publishLog) {
                    this.fileLogger.on('logging', function (transport, level, msg, meta) {
                        var args = {
                            level: level,
                            message: msg,
                            data: meta,
                            time: new Date().getTime()
                        };
                        factory.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE,args);
                    });
                }
            }

            if (options.loggly) {
                this.loggly = winston.add(winston.transports.Loggly, options.loggly);
            }
            if (options.console === null) {
                winston.remove(winston.transports.Console);
            }
            */
        }
    });
});