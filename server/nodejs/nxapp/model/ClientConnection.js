/** @module nxapp/model/ClientConnection */
define([
    'dcl/dcl',
    'xide/model/Base',
    'xide/mixins/EventedMixin',
    'xide/encoding/MD5',
    'xide/utils',
    'xide/types'
], function (dcl,Base,EventedMixin,MD5,utils,types) {

    function generateId(options,connectionManager){

        var devices_hash = options[options.deviceScope];
        var driver_hash = options[options.driverScope];

        //this happens in exported apps where --userDirectory hasn't been set due to its plain HTML with url args
        if(!devices_hash){
            if(connectionManager){
                var ctx = connectionManager.ctx;
                var deviceManager = ctx.getDeviceManager();
                if(deviceManager){
                    var device = deviceManager.getDeviceById(options.id);
                    if(device){
                        var info = deviceManager.toDeviceControlInfo(device);
                        devices_hash = info[info.deviceScope];
                        driver_hash = info[info.driverScope];
                    }
                }
            }
        }

        var hash = utils.replaceAll("/","",(devices_hash + driver_hash));
        hash = utils.replaceAll("\\","",hash);
        var hashData = {
            protocol:options.protocol,
            port:options.port,
            host:options.host,
            hash:hash,
            isServer:(options.driverOptions & 1 << types.DRIVER_FLAGS.SERVER)
        };

        return MD5(JSON.stringify(hashData), 1);
    }
    function generateHash(options){

        var devices_hash = options[options.deviceScope];
        var driver_hash = options[options.driverScope];
        var hash = utils.replaceAll("/","",(devices_hash + driver_hash));
        hash = utils.replaceAll("\\","",hash);
        var hashData = {
            protocol:options.protocol,
            port:options.port,
            host:options.host,
            hash:hash,
            isServer:(options.driverOptions & 1 << types.DRIVER_FLAGS.SERVER)
        };
        return hashData;
    }
    /**
     * Model for a connection to a client (app or IDE).
     * @class module:xcf.model.ClientConnection
     * @extends module:xide/mixins/EventedMixin
     */
    var Module = dcl([EventedMixin.dcl],{
        connected:false,
        __registered:false,
        declaredClass:'nxapp/model/Client',
        /**
         * @member client {module:nxapp/protocols/ProtocolBase}
         */
        client:null,
        /**
         * @member options {module:xide/types~DeviceInfo|null}
         */
        options:null,
        socket:null,
        id:null,
        toString:function(){
            var options = this.options;
            return options.deviceScope +'://' + options.host + ':'+options.port+'@'+options.protocol + '@' + (this.isServer() ? 'server' : '');
        },
        /**
         * 
         * @param client {module:nxapp/protocols/ProtocolBase]
         * @param options {module:xide/types~DeviceInfo}
         */
        constructor:function(client,options){            
            this.client = client;            
            this.options = options;            
            utils.mixin(this,{
                protocol:options.protocol,
                port:options.port,
                host:options.host
            });
            this.id = generateId(options);
            client.connection = this;            
        },
        isServer:function(){
            return (this.options.driverOptions & 1 << types.DRIVER_FLAGS.SERVER);
        }
    });

    Module.generateId  = generateId;
    Module.generateHash  = generateHash;

    return Module;
    
});