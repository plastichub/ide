/** @module nxapp/server/ServerBase */
define([
    'dcl/dcl',
    'xide/model/Base',
    'xide/utils',
    'xide/mixins/EventedMixin'
], function (dcl, Base, utils,EventedMixin) {
    /**
     * @class module:nxapp/server/ServerBase
     * @extends  module:xide/model/Base
     */
    return dcl([Base.dcl,EventedMixin.dcl],{
        options: null,
        delegate: null,
        ctx: null,
        declaredClass: "nxapp.server.ServerBase",
        _defaultOptions: function () {
            return {
                delegate: this
            };
        },
        init: function (args) {
            this.options = utils.mixin(this._defaultOptions(), args.options);
        }
    });
});