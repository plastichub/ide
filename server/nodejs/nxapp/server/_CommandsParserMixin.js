define([
    "dcl/dcl",
    'nxapp/types/Types',
    "nxapp/utils/_console"
], function (dcl,types,_console) {

    //var console = _console;
    var debug = false;    
    return dcl(null, {
        declaredClass:"nxapp.server._CommandsParserMixin",

        handleManagerCommand: function (data, conn) {

            var command = data.manager_command;

            switch (command) {

                case types.SOCKET_SERVER_COMMANDS.MANAGER_TEST:
                {
                    console.dir("Test Manager command");
                    break;
                }
                case types.SOCKET_SERVER_COMMANDS.RUN_SHELL:
                {
                    break;
                }

                case types.SOCKET_SERVER_COMMANDS.WATCH:
                {
                    var fMgr = this.ctx.getFileManager();
                    if(fMgr) {
                        fMgr.watch(data.path,data.watch);
                    }
                    break;
                }

                case types.SOCKET_SERVER_COMMANDS.MQTT_PUBLISH:
                {
                    return this.sendMQTTMessage(data, conn);
                }
                case types.SOCKET_SERVER_COMMANDS.MANAGER_START_DRIVER:
                {
                    return this.startDriver(data, conn);
                }
                case types.SOCKET_SERVER_COMMANDS.GET_DEVICE_VARIABLES:
                {
                    return this.getVariables(data, conn);
                }
                case types.SOCKET_SERVER_COMMANDS.WRITE_LOG_MESSAGE:
                {
                    this.logClientMessage(data);
                    break;
                }
                case types.SOCKET_SERVER_COMMANDS.MANAGER_STOP_DRIVER:
                {
                    this.stopDriver(data);
                    break;
                }
                case types.SOCKET_SERVER_COMMANDS.MANAGER_STATUS:
                {
                    var nbpool = this.options.context.getConnectionManager().getNumberOfConnections();
                    var data = {
                        message: 'returning status',
                        deviceConnections: nbpool
                    };
                    this.emit(types.SOCKET_SERVER_COMMANDS.SIGNAL_RESPONSE, JSON.stringify(data));
                    break;
                }

                case types.SOCKET_SERVER_COMMANDS.MANAGER_CLOSE_ALL:
                {
                    this.options.context.getConnectionManager().closeAll();
                    break;
                }
            }

        },
        handleDeviceCommand: function (data, conn) {

            var command = data.device_command;
            var ctx = this.options.context;
            var mysocket = this;

            var thiz = this;

            debug  = false;

            if (this.showDebugMsg("socket_server")) {
                debug && console.log("Handle device command: " + command + ' | '+types.SOCKET_SERVER_COMMANDS.CALL_METHOD);
            }


            console.error('handle device command',data);
            
            var deviceConnection = ctx.getConnectionManager().connect(data.host, data.protocol, data.port, null, null, conn);
            if (!conn) {
                debug &&  console.error('---have no client connect');
            }

            try {
                ctx.getConnectionManager().registerFeedBack(deviceConnection, function (connection, _data) {
                    if (thiz.profile.debug.debugDevices) {
                        debug && console.log("Data received from " + connection.host);
                    }
                    //mysocket.emit(types.SOCKET_SERVER_COMMANDS.SIGNAL_RESPONSE, _data);


                    if(_.isString(_data)){
                        data.lastResponse = '' + _data;
                    }else if(_.isObject(_data)){
                        try {
                            data.lastResponse = JSON.stringify(_data);
                        }catch(e){
                            debug && console.error('error serializing data');
                        }
                    }
                    thiz.onDeviceMessage(connection, _data);

                });
            }catch(e){
                console.error('-register feed back function failed',e);
                console.trace();
            }

            try {
                switch (command) {

                    case types.SOCKET_SERVER_COMMANDS.CALL_METHOD:
                    {
                        this.options.context.getConnectionManager().callMethod(deviceConnection, data, conn);
                        break;
                    }

                    case types.SOCKET_SERVER_COMMANDS.DEVICE_SEND:
                        this.options.context.getConnectionManager().send(deviceConnection, data.command, data.options);
                        break;
                }
            }catch(e){
                console.error('error command parser ' +command,e);
            }
        }
    });
});