/** @module nxapp/server/DeviceServer */
define([
	"dcl/dcl",
	'dojo/_base/lang',
	'xide/types',
	'xide/factory',
	'nxapp/server/WebSocket',
	"dojo/node!http",
	"dojo/node!sockjs",
	"dojo/node!path",
	"dojo/node!fs-jetpack",
	"dojo/node!server-destroy",
	"nxapp/utils/_console",
	"xide/utils",
	"nxapp/model/Connection"
], function (dcl, lang, types, factory, WebSocket, http, sockjs, path, jet, serverDestroy, _console, utils, Connection) {

	var debug = false;
	var debugMQTT = false;
	var debugServerCommands = false;
	var console = _console;
	var enableServerSide = true;
	var debugLogging = false;
	/**
	 * @class module:nxapp/server/DeviceServer
	 * @extends module:nxapp/server/WebSocket
	 */
	return dcl([WebSocket], {
		destroy: function () {
			try {
				if (this.socketServer) {
					this.socketServer.destroy();
				}
				this.ctx.getConnectionManager().destroy();
			} catch (e) {
				console.error('error destroying Device-Server', e);
			}
		},
		mqttManager: null,
		declaredClass: "nxapp.server.DeviceServer",
		/**
		 * @param data
		 * @param connection {module:nxapp/model/ClientConnection}
		 */
		protocolMethod: function (data, connection) {
			var dfd = this.ctx.getConnectionManager().protocolMethod(data.protocol, data.method, data.options);
			if (dfd && dfd.then) {
				var self = this;
				dfd.then(function (result) {
					self.sendClientMessage(connection, null, types.SOCKET_SERVER_COMMANDS.PROTOCOL_METHOD, {
						options: data.options,
						result: result
					});
				});
			} else {
				console.error('calling protocol method failed! method returned nothing', dfd);
			}
		},
		/**
		 * @param data {object|module:xide/types~DeviceInfo}
		 * @param data.command {SERVER_COMMAND}
		 * @param conn {module:nxapp/model/ClientConnection}
		 * @returns {*}
		 */
		handleManagerCommand: function (data, conn) {
			var command = data.manager_command;
			debugServerCommands && console.log('handle server command ' + command);
			switch (command) {
				case types.SOCKET_SERVER_COMMANDS.MANAGER_TEST:
					{
						console.dir("Test Manager command");
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.PROTOCOL_METHOD:
					{
						return this.protocolMethod(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.RUN_SHELL:
					{
						return this.ctx.getDeviceManager().runShell(data.cmd, data.args, data.options);
					}
				case types.SOCKET_SERVER_COMMANDS.RUN_FILE:
					{
						return this.ctx.runFile(data);
					}

				case types.SOCKET_SERVER_COMMANDS.INIT_DEVICES:
					{
						return this.ctx.loadDevices(data);
					}

				case types.SOCKET_SERVER_COMMANDS.RUN_CLASS:
					{
						return this.ctx.runClass(data);
					}

				case types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_CLASS:
					{
						return this.ctx.runAppServerClass(data);
					}
				case types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_CLASS_METHOD:
					{
						return this.ctx.runAppServerClassMethod(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_COMPONENT_METHOD:
					{
						return this.ctx.runAppServerComponentMethod(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.CANCEL_APP_SERVER_COMPONENT_METHOD:
					{
						return this.ctx.cancelAppServerComponentMethod(data, conn);
					}

				case types.SOCKET_SERVER_COMMANDS.ANSWER_APP_SERVER_COMPONENT_METHOD_INTERRUPT:
					{
						return this.ctx.answerAppServerComponentMethodInterrupt(data, conn);
					}

				case types.SOCKET_SERVER_COMMANDS.WATCH:
					{
						if (this.profile.user && data.path) {
							if (this.profile.user !== data.path) {
								console.warn("Error watching path: this path doesnt belong to this instance! " +
									"\t\n This instance is using " + this.profile.user + " as user directory but your path belongs to " +
									"\t\n " + data.path + " : aborting watch command");
								return;
							}
						}
						var fMgr = this.ctx.getFileManager();
						if (fMgr) {
							fMgr.watch(data.path, data.watch);
						}
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.MQTT_PUBLISH:
					{
						return this.sendMQTTMessage(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.MANAGER_START_DRIVER:
					{
						return this.startDriver(data, conn);
					}

				case types.SOCKET_SERVER_COMMANDS.CREATE_CONNECTION:
					{
						return this.createConnection(data, conn);
					}

				case types.SOCKET_SERVER_COMMANDS.START_DEVICE:
					{
						return this.startDevice(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.GET_DEVICE_VARIABLES:
					{
						return this.getVariables(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.WRITE_LOG_MESSAGE:
					{
						this.logClientMessage(data);
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.MANAGER_STOP_DRIVER:
					{
						this.stopDriver(data);
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.STOP_DEVICE:
					{
						//this.stopDevice(data);
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.MANAGER_STATUS:
					{
						var nbpool = this.options.context.getConnectionManager().getNumberOfConnections();
						var data = {
							message: 'returning status',
							deviceConnections: nbpool
						};
						this.emit(types.SOCKET_SERVER_COMMANDS.SIGNAL_RESPONSE, JSON.stringify(data));
						break;
					}

				case types.SOCKET_SERVER_COMMANDS.MANAGER_CLOSE_ALL:
					{
						this.options.context.getConnectionManager().closeAll();
						break;
					}
				default:
					{
						console.error('unkown command ' + command);
					}
			}

		},
		/**
		 * @param data {object}
		 * @param conn {module:nxapp/model/ClientConnection}
		 */
		handleDeviceCommand: function (data, conn) {
			var command = data.device_command;
			var ctx = this.options.context;
			var thiz = this;
			debug = false;
			var options = data.options;
			var deviceConnection = null;
			if (!conn) {
				debug && console.error('---have no client ');
			}
			if (options.host && options.protocol) {
				deviceConnection = ctx.getConnectionManager().connect(options.host, options.protocol, options.port || '', null, options, conn);
			} else {
				console.error('handle device command: invalid args. data.options is not xide/types/DeviceInfo', utils.inspect(data));
			}
			try {
				if (deviceConnection) {
					deviceConnection.options = options;
					ctx.getConnectionManager().registerFeedBack2(deviceConnection, function (connection, _data) {
						if (_.isString(_data)) {
							data.lastResponse = '' + _data;
						} else if (_.isObject(_data)) {
							try {
								data.lastResponse = JSON.stringify(_data);
							} catch (e) {
								debug && console.error('error serializing data');
							}
						}
						thiz.onDeviceMessage(connection, _data);
					});
				}
			} catch (e) {
				console.error('-register feed back function failed', e);
				console.trace(e);
			}

			try {
				switch (command) {
					case types.SOCKET_SERVER_COMMANDS.CALL_METHOD:
						{
							this.options.context.getConnectionManager().callMethod(deviceConnection, data, conn);
							break;
						}
					case types.SOCKET_SERVER_COMMANDS.DEVICE_SEND:
						{
							this.options.context.getConnectionManager().send(deviceConnection, data.command, data.options);
							break;
						}
				}
			} catch (e) {
				console.error('error command parser ' + command, e.stack);
			}
		},
		/**
		 * @param data {object}
		 * @param conn {module:nxapp/model/ClientConnection}
		 */
		sendMQTTMessage: function (data, conn) {
			//debugMQTT=true;
			var device = data.data.device;
			data.data.sourceHost = conn.remoteAddress;
			data.data.sourcePort = conn.remotePort;
			if (device) {
				var connectionManager = this.ctx.getConnectionManager();
				var deviceConnection = connectionManager.getConnection2(device);
				if (deviceConnection) {

					if (deviceConnection.variables) {
						var parts = data.topic.split('/');
						if (parts.length == 4 && parts[2] == 'Variable') {
							var variableName = parts[3];
							if (!variableName) {
								return;
							}
							debugMQTT && console.info('\tsendMQTTMessage: update local variable: ' + variableName);
							for (var i = 0; i < deviceConnection.variables.length; i++) {
								var variable = deviceConnection.variables[i];
								if (variable.name === variableName) {
									variable.value = data.data.value;
								}
							}
						}
					} else {
						debugMQTT && console.error('device has no variables');
					}
				} else {
					debugMQTT && console.error('send MQTT Message :cant find device ' + device.host);
				}

			} else {
				console.error('cant find device');
			}

			if (this.mqttManager) {
				//console.info('sendMQTTMessage/publishTopic: '+data.topic);
				this.mqttManager.publishTopic(data.topic, data.data);
			} else {
				//console.log('have no mqttManager');
			}
		},
		/**
		 * @param state
		 * @param info
		 */
		updateDeviceState: function (state, info) {
			if (this.mqttManager) {
				this.mqttManager.publishTopic(state, info);
			}
		},
		/**
		 *
		 * @param connection {module:nxapp/model/ClientConnection}
		 * @param options {module:xide/types~DeviceInfo|null}
		 * @param event {string}
		 * @param data {object}
		 */
		sendClientMessage: function (connection, options, event, data) {
			//console.log('DeviceServer::sendClientMessage '+event + ' ' +connection.remoteAddress + ' : ' +connection.remotePort + ' extern : ' + connection.isExternal());
			var message = {
				data: utils.mixin({
					device: options
				}, data),
				event: event
			};
			connection.write(JSON.stringify(message));
		},
		/**
		 * @param data {object}
		 * @param conn {module:nxapp/model/ClientConnection}
		 */
		getVariables: function (data, conn) {
			var options = data.device;
			var cm = this.ctx.getConnectionManager();
			var deviceConnection = cm.getConnection2(options);
			var vars = [];

			if (this.profile.user && options.userDirectory) {
				if (this.profile.user !== decodeURIComponent(options.userDirectory)) {
					console.warn("Error in Device#getVariables: the device doesnt belong to this instance:" +
						"\t\n This instance is using " + this.profile.user + " as user directory but your device belongs to " +
						"\t\n " + decodeURIComponent(options.userDirectory) + " : aborting start device command");
					return;
				}
			}

			if (deviceConnection) {

				deviceConnection.options = options;
				var variables = deviceConnection.variables;
				if (variables) {
					vars = variables;
					this.sendClientMessage(conn, options, types.EVENTS.SET_DEVICE_VARIABLES, {
						variables: variables
					});
				} else {
					deviceConnection.variables = data.variables;
					vars = data.variables;
					_.each(vars, function (variable) {
						variable.value = variable.initial;
					});
					this.sendClientMessage(conn, options, types.EVENTS.SET_DEVICE_VARIABLES, {
						variables: data.variables
					});
				}

			} else {
				console.error('cant find device connection');
			}
			var client = conn.mqtt;
			if (client) {
				if (!client.didSubscribe) {
					for (var i = 0; i < vars.length; i++) {
						var variable = vars[i];
						var topic = options.host + '/' + options.port + '/Variable/' + variable.name;
						client.subscribe(topic);
					}
				}
			}
		},
		/**
		 *
		 * indirect callback for types.SOCKET_SERVER_COMMANDS.WRITE_LOG_MESSAGE log something for the client or server.
		 * This might be triggered by xblox/model/logging/Log
		 *
		 * @param evt {object}
		 * @param evt.data {object}
		 * @param evt.data.device {module:xide/types~DeviceInfo|null}
		 * @param evt.data.details {array}
		 * @param evt.data.time {integer}
		 * @param evt.data.type {string}
		 * @param evt.data.level {string}
		 */
		logClientMessage: function (data) {

			/*console.log('#####');
			 console.dir(data);*/
			/*
			 var a = {
			 "device":{
			 "host":"192.168.1.99",
			 "port":"55",
			 "protocol":"tcp"
			 },
			 "deviceMessage":"helo2",
			 "type":"Device",
			 "time":1441905735825,
			 "level":"info",
			 "message":"Device Message",
			 "timestamp":"2015-09-10T17:22:15.826Z"
			 }
			 var x = {
			 "manager_command":"Write_Log_Message",
			 "message":"testResponse",
			 "data":
			 {
			 "device":
			 {
			 "host":"192.168.1.99",
			 "port":"55",
			 "protocol":"tcp",
			 "driver":"Marantz/MyMarantz.js",
			 "scope":"system_drivers",
			 "id":"235eb680-cb87-11e3-9c1a-0800200c9a66",
			 "clientSide":true
			 }
			 },
			 "details":["helo2"],
			 "level":"info",
			 "type":"error",
			 "tag":"Write_Log_Message",
			 "time":1441905735847,
			 "timestamp":"2015-09-10T17:22:15.847Z"
			 }

			 */
			var host = data.data.device;
			var device = data.data.device;
			if (host) {
				this.logEx(data.message, data.level, data.type, {
					device: device,
					deviceMessage: '',
					time: data.time
				});
			} else {
				//happens for Log-Block with no device instance
				console.error('wrong log parameters, host missing', data);
			}

		},
		/**
		 *
		 * @param device * @param evt.data.device {module:xide/types~DeviceInfo}
		 * @returns {*}
		 */
		getLogger: function (device) {
			var logManager = this.ctx.getLogManager();
			var options = logManager.options;
			var fileName = utils.replaceAll('/', '_', device.host) + '_' + device.port + '_' + device.protocol + '.json';
			var logger = logManager.loggers[fileName];
			var scopeDir = device[device.deviceScope];
			var logDirectory = scopeDir ? jet.dir(scopeDir + path.sep + 'logs').path() : options.fileLogger.dirname;
			if (!logger) {
				var logOptions = {
					filename: fileName,
					dirname: logDirectory,
					json: true,
					console: null
				};

				logger = logManager.createLogger(logOptions);
				debugLogging && console.log('created logger : ' + fileName.green + ' in ' + logDirectory, logOptions);
				logManager.loggers[fileName] = logger;
			}
			return logger;
		},
		logEx: function (msg, level, type, data) {
			//debugLogging && console.log('log message', utils.inspect(arguments));
			var logger = this.getLogger(data.device);
			level = level || 'info';
			data = data || {};
			data.type = type || 'Unknown';
			data.time = data.time || new Date().getTime();
			logger.log(level, msg, data);
		},
		/**
		 *
		 * @param options {module:xide/types~DeviceInfo}
		 * @param conn
		 * @returns {*}
		 */
		stopDriver: function (options) {
			var thiz = this,
				connectionManager = this.ctx.getConnectionManager(),
				deviceManager = this.ctx.getDeviceManager(),
				con = connectionManager.getConnection2(options);
			if (con) {
				con.options = options;
				con.client.close();
			}
			connectionManager.removeConnection(con, null, true);

			!con && debug && console.log('have no such connection ' + Connection.generateId(options));
			deviceManager.stopDevice(options.id);
		},
		sendClientEvent: function (connection) {},
		/**
		 *
		 * @param options {module:xide/types~DeviceInfo}
		 * @param conn {module:nxapp/model/ClientConnection}
		 * @returns {*}
		 */
		startDriver: function (options, conn) {
			//console.log('start driver',conn.declaredClass);
			var cm = this.ctx.getConnectionManager();
			var deviceManager = this.ctx.getDeviceManager();
			var _con = cm.getConnection2(options);
			var serverSide = (options.driverOptions & 1 << types.DRIVER_FLAGS.RUNS_ON_SERVER);
			var isServer = (options.driverOptions & 1 << types.DRIVER_FLAGS.SERVER);

			debug && console.log('start driver ' + options.host + ' : ' + options.port + '@' + options.protocol + ' server side = ' + serverSide + ' device Scope ' + options.deviceScope + ' is server = ' + isServer + ' found connection:' + (_con ? 'true' : 'false') + ' external client ' + conn.isExternal());

			console.error('con : ', options.userDirectory);
			console.error('con : ', this.profile.user);
			/**
			 * Start Driver can be called from :
			 * - IDE or client side
			 * - DeviceServer_Context::initDevices
			 *      In this case
			 */
			//if its from out IDE or app, we start the device instead
			if (serverSide /*&& conn.isExternal()*/ ) {

				var _do = true;
				if (enableServerSide && serverSide && _do) {
					if (!_con) {
						if (deviceManager && deviceManager.startDevice) {

						} else {
							console.error('!deviceManager || !startDevice');
						}
						var device = deviceManager.getItemById(options.id);
						if (device) {
							var info = deviceManager.toDeviceControlInfo(device);
							if (info) {
								deviceManager.startDevice(device, true);
								return true;
							}
							info && debug && console.log('driver runs server side, start device! ' + info.toString());
						} else {
							console.warn('DeviceServer::startDriver : cant find device');
							return;
						}
					}
				}
			}

			if (!_con) {
				debug && console.log('start driver: connection doesnt exists ' + options.host + ' : ' + options.port + '@' + options.protocol + ' : ' + options.deviceScope + ' id : ' + Connection.generateId(options));
				if (!options[options.driverScope]) {
					console.error('have no driver scope');
					return null;
				}
				//console.log('start driver: connection doesnt exists ' + options.host + ' : ' + options.port + '@' + options.protocol + ' : ' + options.deviceScope + ' id : ' + Connection.generateId(options));
				return cm.connect(options.host, options.protocol, options.port, options.mqtt, options, conn);
			}
			var deviceConnection = _con;
			deviceConnection.options = options;
			cm.onConnect2(deviceConnection, true, true);
			if (!options.deviceScope) {
				console.error('has no device scope', options);
				throw new Error('has no device scope', options);
			}
			return "xxxx";
		},
		/**
		 *
		 * @param options {module:xide/types~DeviceInfo}
		 * @param conn {module:nxapp/model/ClientConnection}
		 * @returns {*}
		 */
		startDevice: function (options, conn) {
			var deviceManager = this.ctx.getDeviceManager();
			var connectionManager = this.ctx.getConnectionManager();
			var serverSide = (options.driverOptions & 1 << types.DRIVER_FLAGS.RUNS_ON_SERVER);
			var isServer = (options.driverOptions & 1 << types.DRIVER_FLAGS.SERVER);
			var deviceUser = decodeURIComponent(options.userDirectory);
			debug && console.log('start device ' + options.host + ' : ' + options.port + '@' + options.protocol + ' server side = ' + serverSide + ' device Scope ' + options.deviceScope + ' is server = ' + isServer + ' external client ' + conn.isExternal());

			if (this.profile.user && deviceUser) {
				if (this.profile.user !== deviceUser) {
					console.error("Error: the device doesnt belong to this instance:" +
						"\t\n This instance is using " + this.profile.user + " as user directory but your device belongs to " +
						"\t\n " + deviceUser + " : aborting start device command");
					return;
				}
			}
			//if its from out IDE or app, we start the device instead
			var device = deviceManager.getItemById(options.id);
			if (device) {
				deviceManager.updateDevice(options, device);
				var info = deviceManager.toDeviceControlInfo(device);
				if (info) {
					deviceManager.startDevice(device, null, conn).then(function () {
						var _con = connectionManager.getConnection2(options);
						if (_con) {
							connectionManager.onConnect2(_con, true, true);
						} else {
							console.error('DeviceServer::startDevice : cant find connection');
						}
					});
					return true;
				} else {
					console.error('cant find device info')
				}
				info && debug && console.log('driver runs server side, start device! ' + info.toString());
			} else {
				debug && console.error('no such device', options.host + ':' + options.port + ' - id = ' + options.id);
			}
		},
		/**
		 *
		 * Creates a device connection.
		 *
		 * @param options {module:xide/types~DeviceInfo}
		 * @param conn {module:nxapp/model/ClientConnection}
		 * @returns {*}
		 */
		createConnection: function (options, conn) {
			var cm = this.ctx.getConnectionManager();
			var _con = cm.getConnection2(options);
			var serverSide = (options.driverOptions & 1 << types.DRIVER_FLAGS.RUNS_ON_SERVER);
			var isServer = (options.driverOptions & 1 << types.DRIVER_FLAGS.SERVER);
			if (this.profile.user && options.userDirectory) {
				if (this.profile.user !== decodeURIComponent(options.userDirectory)) {
					console.warn("Error in createConnection: the device doesnt belong to this instance:" +
						"\t\n This instance is using " + this.profile.user + " as user directory but your device belongs to " +
						"\t\n " + decodeURIComponent(options.userDirectory) + " : aborting start device command \n");
					return;
				}
			}
			debug && console.log('createConnection ' + options.host + ' : ' + options.port + '@' + options.protocol + ' server side = ' + serverSide + ' device Scope ' + options.deviceScope + ' is server = ' + isServer + ' found connection:' + (_con ? 'true' : 'false') + ' | is external client:' + conn.isExternal());
			if (!_con) {
				debug && console.log('createConnection: connection doesnt exists ' + options.host + ' : ' + options.port + '@' + options.protocol + ' : ' + options.deviceScope + ' id : ' + Connection.generateId(options));
				if (!options[options.driverScope]) {
					console.error('have no driver scope');
					return null;
				}
				return cm.connect(options.host, options.protocol, options.port, options.mqtt, options, conn);
			}
			var deviceConnection = _con;
			deviceConnection.options = options;
			cm.onConnect2(deviceConnection, true, true);
			if (!options.deviceScope) {
				console.error('has no device scope', options);
				throw new Error('has no device scope', options);
			}
			return deviceConnection;
		},
		onFileChanged: function (evt) {
			this.broadCastMessage(types.EVENTS.ON_FILE_CHANGED, {
				path: evt.path,
				event: evt.event
			});
		},
		/**
		 *
		 * @param connection {module:nxapp/model/Connection}
		 * @param data {object}
		 * @param buffer
		 */
		onDeviceMessage: function (connection, data, buffer) {
			if (!connection) {
				console.error('---onDeviceMessage:have no connection object');
				return;

			}
			if (data === null) {
				debug && console.error('---onDeviceMessage:have no data ' + connection.options.id + ' server id = ' + Connection.generateId(connection.options));
				return;
			}
			var broadCastMessage = {
				device: connection.options,
				deviceMessage: data,
				bytes: buffer && buffer.join ? buffer.join(",") : ""
			};
			var event = data.event || types.EVENTS.ON_DEVICE_MESSAGE;
			//if the device is running "server side", we dont publish this to all clients but to the device manager only
			if (connection && connection.isRunAsServer() && event === types.EVENTS.ON_DEVICE_MESSAGE) {
				var deviceManager = this.ctx.getDeviceManager();
				deviceManager.onDeviceServerMessage({
					event: types.EVENTS.ON_DEVICE_MESSAGE,
					data: {
						data: broadCastMessage
					}
				});
				return;
			}
			this.broadCastMessage(data.event || types.EVENTS.ON_DEVICE_MESSAGE, broadCastMessage);
		},
		onLogMessage: function (evt) {
			debugLogging && console.log('on log message ', utils.inspect(evt));
			this.broadCastMessage(types.EVENTS.ON_SERVER_LOG_MESSAGE, evt); //forward to all device server clients
		},
		/**
		 *
		 * @param connection {module:nxapp/model/ClientConnection}
		 */
		onClientDisconnect: function (connection) {
			if (connection.mqtt) {
				connection.mqtt.end();
				delete connection.mqtt;
			}
		},
		/**
		 *
		 * @param connection {module:nxapp/model/ClientConnection}
		 */
		onClientConnection: function (connection) {
			if (!this.mqttManager) {
				return;
			}
			var thiz = this;
			if (!connection) {
				debug && console.error('DeviceServer::onClientConnection connection null');
				return;
			}

			if (!connection.mqtt) {
				var mqtt = this.mqttManager.createMQTTClient(connection.remoteAddress, connection.remotePort);
				mqtt.client = connection;
				connection.mqtt = mqtt;
				var hash = connection.remoteAddress + ':' + connection.remotePort;
				mqtt.on('message', function (topic, message) {
					debugMQTT && console.log('DeviceServer::onClientConnection->MQTT-Client/on/message: onClientConnection: MQTTManager :: LocalMQTT - Client for ' + hash + ' : received topic' + topic + " \n ");
					if (message.sourceHost !== connection.remoteAddress && message.sourcePort !== connection.remotePort) {
						thiz.sendClientMessage(connection, {}, types.EVENTS.ON_MQTT_MESSAGE, {
							topic: topic,
							message: message.toString(),
							host: connection.remoteAddress,
							port: connection.remotePort
						});
					}
				});
			}
		},
		start: function (_options, profile) {
			this.profile = profile;
			this.options = lang.mixin(this.options, _options);
			this.clients = [];

			var thiz = this;

			var port = this.options.port;
			var host = this.options.host;

			this.subscribe(types.EVENTS.ON_SERVER_LOG_MESSAGE, this.onLogMessage, this);


			if (!port) {
				console.error("Port must be provided into options");
				return false;
			} else {

				this.initLogger(profile.debug);
				this.log("Socket server running in " + port, "socket_server");
				var socket_options = {
					heartbeat_delay: 2000
				};

				if (!debug) {
					socket_options.log = function () {};
				}
				var socket = sockjs.createServer(socket_options);
				//console.log('socket',socket);
				//console.log('-----------',global.httpServer);
				var socket_server = global.httpServer || http.createServer(this._handler);
				this.socket = socket_server;
				socket.installHandlers(socket_server);
				socket_server.listen(port, host);
				this._handleSocketEmits(socket);
				this.socketServer = socket_server;
				serverDestroy(this.socketServer);

				// Triggers ON_WEBSOCKET_START
				this.publish(types.EVENTS.ON_WEBSOCKET_START, {
					socket_server: {
						port: port,
						socket_handler: socket
					}
				}, this);

				socket.on('connection', function (conn) {
					var index = thiz.clients.push(conn);
					debug && console.log('on device server client connection ' + index);
					thiz.onClientConnection(conn);
					conn.isExternal = function () {
						return true;
					};
					conn.on('close', function () {
						debug && console.info('close client connection ' + index);
						thiz.onClientDisconnect(conn);
						thiz.clients.remove(conn);
					});
				});

				return true;
			}
		},
		_handleSocketEmits: function (socket) {
			var port = this.options.port;
			var self = this;
			socket.on('connection', function (conn) {
				self.socket = conn;
				// Triggers ON_WEBSOCKET_CONNECTION
				factory.publish(types.EVENTS.ON_WEBSOCKET_CONNECTION, {
					connection: {
						port: port,
						socket_handler: conn
					}
				}, this);
				conn.on('data', function (data) {
					if (_.isString(data)) {
						data = utils.fromJson(data);
					}
					if (data.manager_command) {
						self.handleManagerCommand(data, conn);
					} else {
						self.handleDeviceCommand(data, conn);
					}
				});
			});
		}
	});
});
