/** @module nxapp/server/WebSocket */
define([
	'dcl/dcl',
	'dojo/_base/lang',
	'xide/types',
	'xide/factory',
	'nxapp/server/ServerBase',
	'nxapp/utils/_LogMixin',
	'nxapp/server/_CommandsParserMixin',
	"dojo/node!http",
	"dojo/node!sockjs",
	"dojo/node!colors",
	"xide/utils"
], function (dcl, lang, types, factory, ServerBase, _LogMixin, _CommandsParserMixin, http, sockjs, colors, utils) {
	var debug = false;
    /**
     * @class module:nxapp/server/WebSocket
     */
	return dcl([ServerBase, _LogMixin, _CommandsParserMixin], {
		declaredClass: "nxapp.server.WebSocket",
		_io_socket: null,
		clients: null,
		handleManagerCommand: function (data, conn) {

			var command = data.manager_command;

			switch (command) {

				case types.SOCKET_SERVER_COMMANDS.MANAGER_TEST:
					{
						console.dir("Test Manager command");
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.RUN_SHELL:
					{
						break;
					}

				case types.SOCKET_SERVER_COMMANDS.WATCH:
					{
						var fMgr = this.ctx.getFileManager();
						if (fMgr) {
							fMgr.watch(data.path, data.watch);
						}
						break;
					}

				case types.SOCKET_SERVER_COMMANDS.MQTT_PUBLISH:
					{
						return this.sendMQTTMessage(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.MANAGER_START_DRIVER:
					{
						return this.startDriver(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.GET_DEVICE_VARIABLES:
					{
						return this.getVariables(data, conn);
					}
				case types.SOCKET_SERVER_COMMANDS.WRITE_LOG_MESSAGE:
					{
						this.logClientMessage(data);
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.MANAGER_STOP_DRIVER:
					{
						this.stopDriver(data);
						break;
					}
				case types.SOCKET_SERVER_COMMANDS.MANAGER_STATUS:
					{
						var nbpool = this.options.context.getConnectionManager().getNumberOfConnections();
						var data = {
							message: 'returning status',
							deviceConnections: nbpool
						};
						this.emit(types.SOCKET_SERVER_COMMANDS.SIGNAL_RESPONSE, JSON.stringify(data));
						break;
					}

				case types.SOCKET_SERVER_COMMANDS.MANAGER_CLOSE_ALL:
					{
						this.options.context.getConnectionManager().closeAll();
						break;
					}
			}

		},
		handleDeviceCommand: function (data, conn) {

			var command = data.device_command;
			var ctx = this.options.context;
			var mysocket = this;

			var thiz = this;

			debug = false;

			if (this.showDebugMsg("socket_server")) {
				debug && console.log("Handle device command: " + command + ' | ' + types.SOCKET_SERVER_COMMANDS.CALL_METHOD);
			}


			console.error('handle device command', data);

			var deviceConnection = ctx.getConnectionManager().connect(data.host, data.protocol, data.port, null, null, conn);
			if (!conn) {
				debug && console.error('---have no client connect');
			}

			try {
				ctx.getConnectionManager().registerFeedBack(deviceConnection, function (connection, _data) {
					if (thiz.profile.debug.debugDevices) {
						debug && console.log("Data received from " + connection.host);
					}
					//mysocket.emit(types.SOCKET_SERVER_COMMANDS.SIGNAL_RESPONSE, _data);


					if (_.isString(_data)) {
						data.lastResponse = '' + _data;
					} else if (_.isObject(_data)) {
						try {
							data.lastResponse = JSON.stringify(_data);
						} catch (e) {
							debug && console.error('error serializing data');
						}
					}
					thiz.onDeviceMessage(connection, _data);

				});
			} catch (e) {
				console.error('-register feed back function failed', e);
				console.trace();
			}

			try {
				switch (command) {

					case types.SOCKET_SERVER_COMMANDS.CALL_METHOD:
						{
							this.options.context.getConnectionManager().callMethod(deviceConnection, data, conn);
							break;
						}

					case types.SOCKET_SERVER_COMMANDS.DEVICE_SEND:
						this.options.context.getConnectionManager().send(deviceConnection, data.command, data.options);
						break;
				}
			} catch (e) {
				console.error('error command parser ' + command, e);
			}
		},
        /**
         *
         * @param eventName {string}
         * @param data {object}
         */
		broadCastMessage: function (eventName, data) {
			var length = this.clients.length;
			while (length--) {
				if (this.clients[length] !== undefined) {
					var dataOut = {
						event: eventName,
						data: _.isString(data) ? JSON.parse(data) : data
					};
					if (this.clients[length].readyState === 3) {//Sockjs bug, connection not closed
						this.clients[length].close();
						this.clients.remove(this.clients[length]);
						continue;
					}
					if (debug) {
						var string = 'writing broadcast message ' + dataOut.event + ' have ' + this.clients.length + ' clients ' + ' readystate : ' + this.clients[length].readyState;
						console.log(colors.green(string));
					}
					var out = JSON.stringify(dataOut);
					this.clients[length].write(out);
				}else{
					console.log('invalid data');
				}
			}
		},
		onFileChanged: function (evt) {
			this.broadCastMessage(types.EVENTS.ON_FILE_CHANGED, {
				path: evt.path,
				type: evt.type
			});
		},
		start: function (_options, profile) {

			this.profile = profile;

			this.options = lang.mixin(this.options, _options);

			this.clients = [];

			var thiz = this;
			//factory.subscribe(types.EVENTS.ON_FILE_CHANGED,this.onFileChanged,this);
			var port = this.options.port;
			var host = this.options.host;
			if (!port) {
				console.error("Port must be provided into options");
				return false;
			} else {

				this.initLogger(profile.debug);
				this.log("Socket server running in " + port, "socket_server");
				var socket_options = {};
				if (!this.showDebugMsg("socket_server")) {
					socket_options.log = function () { };
				}
				var socket = sockjs.createServer(socket_options);
				var socket_server = http.createServer(this._handler);
				socket.installHandlers(socket_server);
				socket_server.listen(port, host);
				this._handleSocketEmits(socket);
				//http://127.0.0.1:5555/Code/client/src//lib/util/touch.js
				// Triggers ON_WEBSOCKET_START
				factory.publish(types.EVENTS.ON_WEBSOCKET_START, {
					socket_server: {
						port: port,
						socket_handler: socket
					}
				}, this);

				socket.on('connection', function (conn) {
					var index = thiz.clients.push(conn);
					conn.on('close', function () {
						debug && console.error('close connection :  ' + index);
						delete thiz.clients[index];
					});
				});

				return true;
			}
		},
		emit: function (signal, data) {
			console.log('emit + ' + this.id + '#' + signal, data);
			if (this.showDebugMsg("socket_server")) {
				//debug && console.log("Socket emits: ["+signal+"]");
				if (_.isString(data)) {
					//debug && console.log(data.substr(0, 50));
				} else if (_.isObject(data)) {
					//console.log(data.substr(0, 50));
					data = JSON.stringify(data);
				}
			}
			if (!this.socket) {
				debug && console.error('-------- WebSocket::emit failed. Have no socket! ' + signal + ' id ' + this.id);

			} else {
				//console.log('write + ' + this.id);
				this.socket.write(data);
			}

		},
		_handleSocketEmits: function (socket) {

			var port = this.options.port;
			var delegate = this.options.delegate;
			var self = this;

			socket.on('connection', function (conn) {
				self.socket = conn;
				debug && console.log('client connected');
				// Triggers ON_WEBSOCKET_CONNECTION
				factory.publish(types.EVENTS.ON_WEBSOCKET_CONNECTION, {
					connection: {
						port: port,
						socket_handler: conn
					}
				}, this);
				conn.on('data', function (data) {
					if (lang.isString(data)) {
						data = dojo.fromJson(data);
					}
					if (data.manager_command) {
						self.handleManagerCommand(data, conn);
					} else {
						self.handleDeviceCommand(data, conn);
					}
				});
				conn.on('close', function () {
					console.error('closed!');
				});
			});

		},
		constructor: function () {
			this.id = utils.createUUID();
		}
	});
});
