/** @module nxapp/protocols/Udp */
define([
	'dcl/dcl',
	'nxapp/protocols/ProtocolBase',
	"dojo/node!dgram",
	"xide/utils",
	"dojo/Deferred",
	"xide/types"
], function (dcl, ProtocolBase, dgram, utils, Deferred, types) {
	var debug = true;

    /**
     * Udp protocol client
     * @class module:nxapp/protocols/Udp
     * @extends module:nxapp/protocols/ProtocolBase
     * @implements module:nxapp/protocols/ProtocolBase
     */
	var Module = dcl(ProtocolBase, {
		declaredClass: "nxapp.protocols.Udp",
		_socket: null,
		protocolName: 'udp',
		port: null,
		host: null,
		destroy:function(){
			this._socket.close();
		},
		onData: function (evt) {
			var owner = this;
			var connectionManager = owner.delegate;
			this.isDebug() && console.log('UDP: got data : ' + this.connection.toString(), evt);
			connectionManager.onData(this.connection, evt);
		},
		makeServer: function (server, options) {
			var self = this;
			options.port = options.port || this.port;
			server.bind(options.port, options.ip);
			self.isDebug() && console.log('UDP: Make server, bind socket to ' + options.ip + ' :: ' + options.port);
			server.on('listening', function () {
				var address = server.address();
				self.isDebug() && console.log('\t UDP Server listening on ' + address.address + ":" + address.port);
			});

			server.on('message', function (message, remote) {
				self.isDebug() && console.log("UDP: on server message " + self.connection.toString() + " - " + remote.address + ':' + remote.port + ' - ' + message);
				self.onData(message.toString());
			});
		},
        /**
         *
         * @param _options
         * @returns {nxapp.protocols.Udp}
         */
		connect: function () {
			var options = this.options;
			this.port = this.options.port;
			this.host = this.options.host;
			this.protocol = this.protocolName;
			this._socket = dgram.createSocket('udp4');
			var deviceOptions = utils.getJson(options.options);
            debug || this.isDebug() && console.log('connecting to udp '+this.host + ':'+this.port);
			this._setupEventListeners(this._socket, this.delegate);
			this._socket.writable = true;
			this.connection.connected = true;
			this.delegate.onConnect2(this.connection);
			if (deviceOptions && deviceOptions.server === true) {
				this.makeServer(this._socket, deviceOptions);
			}
			return this;
		},
		send: function (cmd, options) {
			try {
				var self = this;
				var intArray = utils.bufferFromDecString(cmd);
				var buffer = new Buffer(intArray);
				var str = buffer.toString();
				var _debug = debug || this.isDebug();
				_debug && console.log("UDP - " + self.connection.toString() + " Sending command: " + cmd + ' = ' + str + ' l= ' + str.length);
				this._socket.send(buffer, 0, buffer.length, this.port, this.host, function (err, bytes) {
					if (err) {
						self.delegate.onError(err);
					}
					else {
						debug || self.isDebug() && console.log('UDP message sent to ' + self.host + ':' + self.port);
					}
				});
			} catch (e) {
				console.error('crash !');
			}
		},
		close: function () {
			try {
				this._socket.close();
			} catch (e) {
				debug && console.error('Error closing udp connection : ', e);
			}
		}
	});
	Module.options = function (query) {
		try {
			var dfd = new Deferred();
			var ECIType = types.ECIType;
			var NetworkGroup = 'Network';
			var cis = [
				utils.createCI('ip', ECIType.STRING, '', {
					group: NetworkGroup,
					title: 'IP',
					description: "The ip address to bind the server."
				}),
				utils.createCI('port', ECIType.STRING, "", {
					group: NetworkGroup,
					title: 'Port',
					description: "The port to use when its a server"
				}),
				utils.createCI('server', ECIType.BOOL, false, {
					group: NetworkGroup,
					title: 'Make this as server.',
					description: ""
				})
			]
			dfd.resolve(cis);
			return dfd;
		} catch (e) {
			console.error('error', e);
		}
		return dfd;
	}
	return Module;
});
