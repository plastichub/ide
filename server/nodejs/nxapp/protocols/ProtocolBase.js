/** @module nxapp/protocols/ProtocolBase */
define([
    'dcl/dcl',
    'xide/model/Base',
    'xide/types',
    'xide/utils',
    'nxapp/utils/_LogMixin',
    "xide/mixins/ReloadMixin",
    "dojo/Deferred"
],function(dcl,Base,types,utils,_LogMixin,ReloadMixin,Deferred){

    /**
     * Common base class for protocol connections
     * @class module:nxapp/protocols/ProtocolBase
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xide/model/Base
     * @interface module:nxapp/protocols/ProtocolBase
     */
    var Module = dcl([Base.dcl,_LogMixin,ReloadMixin.dcl],{
        onReloaded:function(){
            console.error('on reloaded');
        },
        /**
         * @member options {module:xide/types~DeviceInfo} The device info
         */
        options:null,
        declaredClass:"nxapp.protocols.ProtocolBase",
        protocolName:'undefined',
        debug_connection:'protocol_connection',
        debug_messages:'protocol_messages',
        /**
         * 
         * @param module {string} the Node.JS module
         * @returns {Module}
         */
        getModule:function(module){
            var deferred = new Deferred();
            try {
                require(["dojo/node!" + module], function (module) {
                    deferred.resolve(module);
                });
            }catch(e){
                deferred.reject(e);
            }
            return deferred;
        },
        /**
         * @member delegate {module:nxapp/manager/ConnectionManager}
         */
        delegate:null,
        /**
         * @member connection {module:nxapp/model/Connection}
         */
        connection:null,
        /**
         *
         * @param socket {module:net/Socket}
         * @param handler {module:nxapp/manager/ConnectionManager}
         * @private
         */
        _setupEventListeners:function(socket,handler) {
            
            var self = this;
            
            var connection = self.connection;

            socket.handle = function (data) {
                handler.onHandle(connection,data);
            };

            function toArrayBuffer(buffer) {
                var ab = new ArrayBuffer(buffer.length);
                var view = new Uint8Array(ab);
                for (var i = 0; i < buffer.length; ++i) {
                    view[i] = buffer[i];
                }
                return ab;
            }

            socket.on('data', function(data) {

                /*
                if(data instanceof Buffer){
                    console.error('buffer');
                }else{
                    console.error('not buffer ',data);
                }
                */
                
                handler.onData(connection,data.toString(),data);
                //console.log('socket data');
                /*
                //throw new Error();
                for(var i = 0; i < data.length; i++){
                    //var char = hex.toString(); // i is byte index of hex
                    //console.log('el ' + String.charCodeAt(data[i]));
                    //var b = textString.charCodeAt(i);
                };
                */
                //console.log('received : '+data.length + '  = ' +data.toString());
                for (var i = 0; i < data.length; i++) {
                    //var b = data.charCodeAt(i);
                    //console.log('b = ' + data[i]);
                }
                /*
                console.dir(data);
                console.dir(toArrayBuffer(data));
                */

                /*
                
                console.log('data ' + utils.stringToHex(data.toString()) + ' = ' + util.inspect(data) + ' ascii =' + data.toString('ascii'));
                console.log('binary ' + data.toString('binary') );
                console.log('hex ' + data.toString('hex') );
                console.log('ucs2' + data.toString('ucs2'));
                console.log(data);
                */

            });

            if(!handler){
                console.error('have no handler');
                utils.stack();
            }

            if(!connection){
                console.error('have no connection');
                utils.stack();
            }

            socket.on('error', function(exception){
                self.isDebug() && console.error('socket error : ',exception);
                handler.onError(connection,exception);
            });


            socket.on('drain', function() {
                handler.onDrain(connection);
            });

            socket.on('timeout', function() {
                handler.onTimeout(connection);
            });

            // Add a 'close' event handler for the client client
            socket.on('close', function(data) {
                handler.onClose(connection,data);
            });

        },
        isDebug:function(){
            var connection = this.connection;
            if(connection){
                var options = connection.options;
                if(options) {
                    var driverFlags = options.driverOptions;
                    if(driverFlags){
                        if(driverFlags & 1 << types.DRIVER_FLAGS.DEBUG){
                            return true;
                        }
                    }
                }
            }
            
            return false;
        },
        isServer:function(){
            var connection = this.connection;
            if(connection){
                var options = connection.options;
                if(options) {
                    var driverFlags = options.driverOptions;
                    if(driverFlags){
                        if(driverFlags & 1 << types.DRIVER_FLAGS.SERVER){
                            return true;
                        }
                    }
                }
            }
            return false;
        },
        runAsServer:function(){
            var connection = this.connection;
            if(connection){
                var options = connection.options;
                if(options) {
                    var driverFlags = options.driverOptions;
                    if(driverFlags){
                        if(driverFlags & 1 << types.DRIVER_FLAGS.RUNS_ON_SERVER){
                            return true;
                        }
                    }
                }
            }
            return false;
        },
        _defaultOptions:function(){
            return {
                port:23,
                text_encoding: 'utf8',
                end_of_command: '\r'
            };
        },
        toString:function(cmd,encoding){
            var intArray = utils.bufferFromDecString(cmd);
            var buffer = new Buffer(intArray);
            var str = buffer.toString(encoding);
            return str;
        },
        init:function() {
            this.initLogger(this.options.debug);
            this.initReload();
            var self = this;
            this.subscribe(types.EVENTS.ON_MODULE_RELOADED,function(evt){
                if(self.modulePath && evt.module && self.modulePath === evt.module){
                    self.mergeFunctions(self,evt.newModule.prototype);
                    self.onReloaded();
                }
            });
        },
        mergeFunctions: function (target, source) {
            for (var i in source) {
                var o = source[i];
                if (i === 'constructor' || i === 'inherited') {
                    continue;
                }
                if (_.isFunction(source[i])) {
                    target[i] = null;//be nice
                    target[i] = source[i];//override
                    console.log('update : ' +i);
                }

            }
        },
        logConnection:function(msg) {
            this.log(msg,this.debug_connection);
        },
        logMessages:function(msg) {
            this.log(msg,this.debug_messages);
        },
        /**
         * Send a raw string
         * @param cmd {string} the string to send
         */
        send:function(cmd){},
        onConnect:function(){},
        /**
         * 
         */
        destroy:function(){}
    });
    dcl.chainAfter(Module,"onConnect");
    dcl.chainAfter(Module,"init");
    dcl.chainAfter(Module,"close");
    dcl.chainAfter(Module,"destroy");
    return Module;
});