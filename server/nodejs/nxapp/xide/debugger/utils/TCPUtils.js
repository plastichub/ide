define([
    "dojo/node!net"
], function(net)
{
    var utils={};
    utils.checkPort = function(port, host, callback) {

        var Socket = net.Socket;
        var socket = new Socket(), status = null;

        // Socket connection established, port is open
        socket.on('connect', function() {status = 'open';socket.end();});
        socket.setTimeout(1500);// If no response, assume port is not listening
        socket.on('timeout', function() {status = 'closed';socket.destroy();});
        socket.on('error', function(exception) {status = 'closed';});
        socket.on('close', function(exception) {callback(null, status,host,port);});

        socket.connect(port, host);
    };

    return utils;

});