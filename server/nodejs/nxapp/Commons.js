define([
    'dojo/has',
    'dojo/_base/lang',
    'nxapp/types/Types',
    'xide/factory',
    "xide/factory/Events",
    "xide/factory/Objects",
    "xide/utils/StringUtils",
    "xide/utils/HexUtils",
    "xide/utils/CIUtils",
    "xide/utils/ObjectUtils",
    "xide/utils/StoreUtils",
    "nxapp/utils/FileUtils",
    "xide/utils",
    "nxapp/utils",
    "dojo/node!os",
    "dojo/node!util",
    'nxapp/utils/_console'
],function(has,lang,Types,factory,EventFactory,ObjectFactory,StringUtils,HexUtils,CIUtils,ObjectUtils,StoreUtils,FileUtils,utils,nxUtils,os,util,_console){

    var changeConsole = true;

    var os_suffix='linux';
    if(os.platform() ==='win32'){
        os_suffix = 'windows';
    }else if(os.platform() ==='darwin'){
        os_suffix = 'osx';
    }else if(os.platform() ==='linux' && os.arch()=='arm'){
        os_suffix = 'arm';
    }

    if(os_suffix==='arm'){
        has.add('serialport',function(){
            return true;
        });
    }else{
        has.add('serialport',function(){
            return true;
        });
    }

    utils.mixin(nxUtils,utils);

    utils.mixin(utils,nxUtils);
    
    utils.inspect = function(data){
        return util.inspect(data, {depth: null, colors: true});
    };

    Array.prototype.remove= function(){
        var what, a= arguments, L= a.length, ax;
        while(L && this.length){
            what= a[--L];
            while((ax= this.indexOf(what))!= -1){
                this.splice(ax, 1);
            }
        }
        return this;
    };

    Array.prototype.contains = function(obj) {
        var i = this.length;
        while (i--) {
            if (this[i] == obj) {
                return true;
            }
        }
        return false;
    };

    if (!Array.prototype.indexOfPropertyValue){
        Array.prototype.indexOfPropertyValue = function(prop,value){
            for (var index = 0; index < this.length; index++){
                if (this[index][prop]){
                    if (this[index][prop] == value){
                        return index;
                    }
                }
            }
            return -1;
        };
    }

    if ( typeof String.prototype.startsWith != 'function' ) {
        String.prototype.startsWith = function( str ) {
            return this.substring( 0, str.length ) === str;
        };
    }

    if ( typeof String.prototype.endsWith != 'function' ) {
        String.prototype.endsWith = function( str ) {
            return this.substring( this.length - str.length, this.length ) === str;
        };
    }

    return {
        utils:nxUtils,
        types:Types
    };

});