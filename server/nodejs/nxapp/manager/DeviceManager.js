/** @module nxapp/manager/DeviceManager */
define([
    'dcl/dcl',
    "dojo/node!fs",
    "xcf/manager/DeviceManager",
    "xide/utils",
    "xide/types",
    "nxapp/utils/_console",
    "nxapp/utils",
    "dojo/node!child_process"
], function (dcl, fs, DeviceManager, utils,types,_console, xUtils, child_process) {

    var console = _console;
    var debug = false;
    var FakeConnectionClass = dcl(null, {
        emit: function () {
            console.error('device server client emit', arguments);
        }
    });

    /**
     * 
     * @param startNeedle
     * @param path
     * @returns {*}
     */
    function isItemPath(startNeedle, path) {
        var _start = startNeedle;
        if (path.indexOf(_start) != -1) {

            var libPath = path.substr(path.indexOf(_start) + (_start.length + 1 ), path.length);
            return libPath;
        }
        return null;
    }

    var exec = child_process;
    var _options = {
        stdout: true,
        stderr: true,
        stdin: true,
        failOnError: true,
        stdinRawMode: false,
        killSignal: 'SIGTERM'
    };
    /**
     *
     * @param instance
     * @param device
     * @param code
     * @param args
     * @param source
     * @param options
     * @param block
     */
    function run(instance, device, code, args, source, options,block) {
        args = "";

        var process = exec.exec(code, _options, function (err, stdout, stderr) {

            //not found or whatever
            if(stdout && parseInt(stdout,10)===127){
                console.error('--have error ' + stderr);
                if(block && block.onCommandError){
                    block.onCommandError();
                }
            }

            if(stdout && parseInt(stdout,10)===0){
                console.error('--terminate with ' + stdout);//dlg = yes
            }

            if(stdout && parseInt(stdout,10)===1){
                console.error('--terminate with 1 ' + stdout);//dlg = yes                
            }
            /**
             * <error> response  { '0': null,
                '1': 'apps\nbackupluiz\nbin\nboot\ncore\ndata\ndev\netc\netc.tar\nhome\nhtdocs\ninitrd.img\ninitrd.img.old\ninstall\nlib\nlib32\nlib64\nlibnss3.so\nlibx32\nlost+found\nmc007\nmedia\nmnt\nopt\nPMaster\nproc\nroot\nrun\nsbin\nsdk\nsetgw.sh\nsnap\nsrv\nsys\ntmp\nudev_back\nusr\nvar\nvmlinuz\nvmlinuz.old\nwebmin-setup.out\n',
                '2': '' } (in DeviceManager.js:96)
             */
            console.error('response ', arguments);

        }.bind(this));

    }

    /**
     * DeviceManager server side only part
     * @class module:nxapp/manager/DeviceManager
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xide/manager/ManagerBase
     */
    return dcl(DeviceManager, {
        declaredClass: "nxapp.manager.DeviceManager",
        connection: null,
        runShell: function (method, args,options) {
            var connectionManager = this.getContext().getConnectionManager();
            var process = exec.exec(method, _options, function (err, stdout, stderr) {

                //console.error('did run : ' + method);
                //not found or whatever

                if(stdout && parseInt(stdout,10)===127){
                    console.error('--have error ' + stderr);
                }

                err && console.log('err',err);
                stdout && console.log('stdout',stdout);
                stderr && console.log('stderr',stderr);

                if(stdout && parseInt(stdout,10)===0){
                    console.info('--terminate with 0' + stdout);//dlg = yes
                }

                if(stdout && parseInt(stdout,10)===1){
                    console.info('--terminate with 1 ' + stdout);//dlg = yes
                    try {
                        connectionManager.onData(connectionManager.getConnection2(options), utils.mixin({
                            cmd: method,
                            event: types.EVENTS.ON_COMMAND_FINISH,
                            result:stdout
                        },options),new Buffer(stdout));

                    }catch(e){
                        console.error('onFinish-Error:',e);
                    }
                }
                /**
                 * <error> response  { '0': null,
                '1': 'apps\nbackupluiz\nbin\nboot\ncore\ndata\ndev\netc\netc.tar\nhome\nhtdocs\ninitrd.img\ninitrd.img.old\ninstall\nlib\nlib32\nlib64\nlibnss3.so\nlibx32\nlost+found\nmc007\nmedia\nmnt\nopt\nPMaster\nproc\nroot\nrun\nsbin\nsdk\nsetgw.sh\nsnap\nsrv\nsys\ntmp\nudev_back\nusr\nvar\nvmlinuz\nvmlinuz.old\nwebmin-setup.out\n',
                '2': '' } (in DeviceManager.js:96)
                 */
                //console.error('response ', arguments);

            }.bind(this));

            /*

            var options = instance.options,
                sendOptions = {
                    id: id,
                    src: src
                },
                dataOut = {
                    method: method,
                    args: args,
                    manager_command: 'Run_Shell',
                    host: options.host,
                    port: options.port,
                    protocol: options.protocol,
                    options: sendOptions
                };

            run(instance, {
                host: options.host,
                port: options.port,
                protocol: options.protocol
            }, method, args, src, _options,block);
            */

        },
        runShell2: function (instance, method, args, src, id,block) {
            var options = instance.options,
                sendOptions = {
                    id: id,
                    src: src
                },
                dataOut = {
                    method: method,
                    args: args,
                    manager_command: 'Run_Shell',
                    host: options.host,
                    port: options.port,
                    protocol: options.protocol,
                    options: sendOptions
                };

            run(instance, {
                host: options.host,
                port: options.port,
                protocol: options.protocol
            }, method, args, src, _options,block);

        },
        /**
         * update meta
         * @param dataPath
         * @param shortPath
         */
        onDeviceMetaChanged: function (dataPath, shortPath) {
            var store = this.store;
            if(!store){
                return;
            }
            var data = store.data;
            
            _.each(data, function (item) {
                if (item.path.indexOf(shortPath) !== -1 && fs.existsSync(dataPath)) {
                    var meta = utils.getJson(xUtils.readFile(dataPath));
                    meta && (item.user = meta);
                }
            });
        },
        /**
         * 
         * @param evt
         */
        onFileChanged: function (evt) {
            if (evt.type !== 'changed') {
                return;
            }
            if (evt._did) {
                return;
            }
            evt._did = true;
            var path = utils.replaceAll('\\', '/', evt.path);
            path = utils.replaceAll('//', '/', path);
            path = path.replace(/\\/g, "/");

            var isDevice = isItemPath('system/devices', path);
            if (isDevice && isDevice.indexOf('.meta.json') !== -1) {
                this.onDeviceMetaChanged(path, isDevice);
            }
        },
        /**
         * noob out checkDeviceServerConnection
         */
        checkDeviceServerConnection: function () {},
        /**
         * Device Server managment interface
         * @param cmd
         * @param data
         */
        sendManagerCommand: function (cmd, data) {
            try {
                var deviceServer = this.ctx.getDeviceServer();
                var dataOut = {
                    manager_command: cmd
                };
                utils.mixin(dataOut, data);
                debug && console.error('sendManagerCommand', dataOut);
                deviceServer.handleManagerCommand(dataOut, this.connection);
            } catch (e) {
                console.error('error ' + e.message, e.stack);
            }
        },
        /**
         *
         * @param driverInstance
         * @param data
         * @param src
         * @param id
         */
        sendDeviceCommand: function (driverInstance, data, src, id) {            
            var options = driverInstance.options;
            utils.mixin({
                src:src
            },options);

            var dataOut = {
                command: data,
                device_command: 'Device_Send',
                options:options
            };
            utils.mixin(dataOut.options,{
                params:{
                    src:src,
                    id:id
                }
            });
            debug && console.log("Device.Manager.Send.Message : " + dataOut.command.substr(0, 30), dataOut);//sending device message
            var deviceServer = this.ctx.getDeviceServer();
            deviceServer.handleDeviceCommand(dataOut, this.connection);
        },
        debug:function(){
            console.log('------------stores');
            console.log('have '+_.size(this.stores));
            _.each(this.stores,function(store){

            });
        }
    });
});