/** @module nxapp/manager/ManagerBase */
define([
    "dcl/dcl",
    "xide/model/Base",
    "xide/mixins/EventedMixin"
], function (dcl,Base,EventedMixin){
    /**
     * Common base class for managers.
     * @class module:nxapp/manager/ManagerBase
     * @extends module:xide/mixins/EventedMixin
     * @extends module:xide/model/Base
     */
    var Module = dcl([Base.dcl,EventedMixin.dcl],{
        declaredClass:"nxapp.manager.ManagerBase",
        ctx:null,
        profile:null,
        init:function(profile){
            this.profile = profile;
        }
    });
    dcl.chainAfter(Module,'init');
    return Module;
});