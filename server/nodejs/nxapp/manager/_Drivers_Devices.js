/** @module nxapp/manager/_Drivers_Devices */
define([
    "dcl/dcl",
    "dojo/node!path",
    "dojo/node!fs",
    "nxapp/utils",
    'xide/utils/StringUtils',
    'xide/types',
    "nxapp/utils/_console"
], function (dcl, path, fs, utils, StringUtils, types, _console) {

    var console = _console;
    /**
     * @class module:nxapp/manager/_Drivers_Devices
     */
    return dcl(null, {
        declaredClass: "nxapp.manager._Drivers_Devices",
        profile: null,
        getDevices: function (devicePath,scope,options) {
			require({
                packages: [
                    {
                        name: scope,
                        location: devicePath
                    }
                ]
            });

            var items = [];
            options = options || {};

            var serverSide = options.serverSide;
            function parseDirectory(_path, name) {
                var dirItems = fs.readdirSync(_path);
                if (dirItems.length) {

                    _.each(dirItems, function (file) {
                        if (file.indexOf('.meta.json') != -1) {
                            var meta = StringUtils.getJson(utils.readFile(_path + path.sep + file),true,false);
                            if(!meta){
                                console.error('cant get device meta for '+file + ' path = '+_path + path.sep + file);
                                return;
                            }
                            if(serverSide){
                                var driverOptions = utils.getCIByChainAndName(meta, 0, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
                                if(driverOptions && !(driverOptions.value & (1 << types.DRIVER_FLAGS.RUNS_ON_SERVER))){
                                    driverOptions.value = driverOptions.value | (1 << types.DRIVER_FLAGS.RUNS_ON_SERVER);
                                }
                            }
                            if (!meta) {
                                console.error('device has no meta ' + _path + path.sep + file);
                            } else {
                                var item = {
                                    isDir: false,
                                    path: name + '/' + file,
                                    parentId: name,
                                    scope: scope,
                                    user: meta,
                                    id: utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_ID)
                                };
                                items.push(item);
                            }
                        }
                    });
                }
            }
            function _walk(dir) {

                var results = [];

                if (fs.existsSync(dir)) {

                    var list = fs.readdirSync(dir);

                    list.forEach(function (file) {

                        file = dir + '/' + file;

                        var stat = fs.statSync(file);
                        if (stat) {

                            var root = file.replace(devicePath + '/', '');

                            if (stat.isDirectory()) {
                                var dirItem = {
                                    isDir: true,
                                    parent: root,
                                    name: root,
                                    path: root
                                };
                                items.push(dirItem);
                                parseDirectory(file, root);

                                results.push(file.replace(devicePath + '/', ''));

                                results = results.concat(_walk(file));
                            }
                        } else {
                            console.error('cant get stat for ' + file);
                        }
                    });
                } else {
                    console.error('device path ' + dir + ' doesnt exists');
                }
                return results
            };
            _walk(devicePath);
            return items;

        },
        getDrivers: function (driverPath,scope,options) {
            require({
                packages: [
                    {
                        name: scope,
                        location: driverPath
                    }
                ]
            });
            var items = [];

            function parseDirectory(_path, name) {
                var dirItems = fs.readdirSync(_path);
                if (dirItems.length) {

                    _.each(dirItems, function (file) {
                        if (file.indexOf('.meta.json') != -1) {
                            var meta = StringUtils.getJson(utils.readFile(_path + path.sep + file),true,false);

                            if (!meta) {
                                console.error('cant get driver meta ' + _path + path.sep + file);
                            } else {
                                var id = utils.getCIInputValueByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_ID);
                                var bloxFile = _path + '/' + file.replace('.meta.json', '.xblox');
                                var blox = StringUtils.getJson(utils.readFile(bloxFile),true,false);
                                if(!blox){
                                    console.warn('invalid blocks file for driver ' + name + '/' + file + ' blox path = '+bloxFile);
                                }
                                var item = {
                                    isDir: false,
                                    path: name + '/' + file,
                                    parentId: name,
                                    scope: scope,
                                    user: meta,
                                    id: id,
                                    blox: blox || {},
                                    blockPath: bloxFile
                                };
                                items.push(item);
                            }
                        }
                    });
                }
            }
            function _walk(dir) {

                var results = [];
                if (fs.existsSync(dir)) {
                    var list = fs.readdirSync(dir);
                    list.forEach(function (file) {

                        file = dir + '/' + file;
                        var stat = fs.statSync(file);
                        if (stat) {

                            var root = file.replace(path + '/', '');

                            if (stat.isDirectory()) {

                                var dirItem = {
                                    isDir: true,
                                    parent: root,
                                    name: root,
                                    path: root
                                };
                                items.push(dirItem);

                                parseDirectory(file, root);

                                results.push(file.replace(driverPath + '/', ''));

                                results = results.concat(_walk(file));

                            }
                        }
                    });
                } else {
                    console.error('driver path ' + dir + ' doesnt exists');
                }
                return results;
            }
            _walk(driverPath);
            return items;
        }
    });
});
