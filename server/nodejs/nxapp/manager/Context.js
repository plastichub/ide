/** @module nxapp/manager/Context */
define([
	'dcl/dcl',
	'./ConnectionManager',
	'xide/manager/ContextBase',
	'require',
	"nxapp/utils/_console",
	'xide/types',
	'xide/utils',
	"dojo/node!path"
	//'dojo/node!inquirer'
], function (dcl, ConnectionManager, ContextBase, require, _console, types, utils, path) {
	var console = _console;
	/**
	 * @class module:nxapp/manager/Context
	 * @extends module:xide/manager/ContextBase
	 * @extends module:xcf/manager/Context
	 * @lends module:nxapp/manager/DeviceServerContext
	 */
	return dcl(ContextBase, {
		declaredClass: "nxapp.manager.Context",
		connectionManager: null,
		blockManager: null,
		deviceServer: null,
		logManager: null,
		/**
		 *
		 * @param data {object}
		 * @param data.scope {string} Name of the scope
		 * @param data.path {string} Absolute path to the scope
		 */
		loadDevices: function (data) {
			console.log('load devices', data);
			var deviceManager = this.getDeviceManager();
			var driverManager = this.getDriverManager();
			deviceManager.debug();
			var stores = deviceManager.getStores();
			var store = stores[data.scope];
			var scopePath = data.path;
			if (store) {
				if (store.connected) {
					console.warn('store ' + store.scope + ' is already connected');
					return;
				} else {
					console.info('connecting store ' + store.scope);
					return;
				}
			} else {

				console.info('connecting store ' + data.scope);

				driverManager.createStore({
					items: this.getDrivers(path.resolve(scopePath + '/drivers'), 'user_drivers')
				}, 'user_drivers', true);

				deviceManager.initStore({
					items: this.getDevices(path.resolve(scopePath + '/devices'), 'user_devices')
				}, 'user_devices', true);


				return;
			}
		},
		runFile: function (data) {
			console.log('run file', data);
		},
		broadCastMessage: function (event, data) {
			this.getDeviceServer().broadCastMessage(types.EVENTS.ON_RUN_CLASS_EVENT, utils.mixin({
				event: types.EVENTS.ON_RUN_CLASS_EVENT
			}, data));
		},
		runClass: function (data) {
			var _class = data['class'];
			var self = this;
			try {
				require([_class], function (module) {
					var instance = new module(data.args);
					try {

						var delegate = {
							data: data,
							clear: function () {
								this.data.progress = null;
								this.data.finish = null;
								this.data.error = null;
								this.data.data = null;
							},
							onProgress: function (progress, data) {
								this.clear();
								this.data.progress = progress;
								this.data.data = data;
								self.broadCastMessage(null, this.data);
							},
							onFinish: function (finish, data) {
								this.clear();
								this.data.finish = finish;
								this.data.data = data;
								self.broadCastMessage(null, this.data);
							},
							onError: function (error, data) {
								this.clear();
								this.data.error = error;
								this.data.data = data;
								self.broadCastMessage(null, this.data);
							}
						};

						instance.run(delegate);

					} catch (e) {
						console.error('error running class ' + _class, e);
						data.error = e.message;
						self.broadCastMessage(null, data);
					}
				});
			} catch (e) {
				console.error('error loading class ' + _class, e);
			}



		},
		runAppServerClass: function (data) {
			var _class = data['class'];
			var self = this;
			var app = this.appServer;
			return app.runClass(data, this);
		},
		runAppServerComponentMethod: function (data, client) {
			var _class = data['class'];
			var self = this;
			var app = this.appServer;
			return app.runComponentMethod(data, this, client);
		},
		cancelAppServerComponentMethod: function (data, client) {
			var app = this.appServer;
			return app.cancelComponentMethod(data, this, client);
		},
		answerAppServerComponentMethodInterrupt: function (data, client) {
			var app = this.appServer;
			return app.answerAppServerComponentMethodInterrupt(data, this, client);
		},
		runAppServerClassMethod: function (data, client) {
			var _class = data['class'];
			var self = this;
			var app = this.appServer;
			return app.runClassMethod(data, this, client);
		},
		getDeviceServer: function () {
			return this.deviceServer;
		},
		getConnectionManager: function () {
			return this.connectionManager;
		},
		initManagers: function (profile) {
			this.connectionManager.init(profile);
		},
		constructManagers: function () {
			this.connectionManager = new ConnectionManager({
				ctx: this
			});
		},
		getLogManager: function () {
			return this.logManager;
		}
	});
});
