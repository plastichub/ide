define([
    "dcl/dcl"
], function (dcl) {    
    return dcl(null, {
        declaredClass: "nxapp.utils._LogMixin",
        debug_conf: null,
        logger: null,
        initLogger: function (debug_config) {
            this.debug_conf = debug_config;
        },
        log: function (msg, msg_context, level, data) {
            if (!msg_context) msg_context = this._debugContext()["main"];
            if (!msg_context || this.showDebugMsg(msg_context)) {
                if (this.logger) {
                    level = level || 'info';
                    data.time = new Date().getTime();
                    this.logger.log(level, msg, data);

                }
            }
        },
        logEx: function (msg, level, type, data) {
            if (this.logger) {
                level = level || 'info';
                data = data || {};
                data.type = type || 'Unknown';
                data.time = data.time || new Date().getTime();
                var b = this.logger.log(level, msg, data);

            } else {
                console.error('have no logger!');
            }
        },
        showDebugMsg: function (msg_context) {
            if (this.debug_conf != null) {
                if (this.debug_conf["all"]) {
                    return true;
                }
                else {
                    if (this.debug_conf[msg_context]) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

            } else {
                //console.log("No debug config, showing all.");
                this.debug_conf = {
                    "all": true
                };
                return true;
            }
        }
    });
});