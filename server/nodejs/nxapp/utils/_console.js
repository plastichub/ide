define([
    'dojo/has!host-node?dojo/node!tracer',
    'dojo/has!host-node?dojo/node!util',
    'nxapp/utils',
    'dojo/has'
], function (tracer, util, utils, has) {
    var console = console;
    if(tracer){
        console = tracer.colorConsole({
            format: "<{{title}}> {{message}} " + ( !has('build') ? "(in {{file}}:{{line}})" : ""),
            dateformat : "HH:MM:ss.L"
        });
    }

    if(typeof logError=='undefined'){
        global['logError']=function(e,reason){
            console.error('Error ' + reason,e);
        };
    }

    utils.stack=function(){
        var stack = new Error().stack;
        console.log( stack );
    };
    console.clear = function(){
        util.print("\u001b[2J\u001b[0;0H");
        /*
        var lines = process.stdout.getWindowSize()[1];
        for(var i = 0; i < lines; i++) {
            console.log('\r\n');
        }
        */
    };
    return console;
});
