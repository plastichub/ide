define([
    "dojo/node!net",
    "nxapp/utils",
    'nxapp/server/WebSocket',
    'nxapp/server/DeviceServer',
    'nxapp/server/FileServer'
], function(net, utils,WebSocket,DeviceServer,FileServer)
{
    utils.checkPort = function(port, host, callback) {

        var Socket = net.Socket;
        var socket = new Socket(), status = null;

        // Socket connection established, port is open
        socket.on('connect', function() {status = 'open';socket.end();});
        socket.setTimeout(1500);// If no response, assume port is not listening
        socket.on('timeout', function() {status = 'closed';socket.destroy();});
        socket.on('error', function(exception) {status = 'closed';});
        socket.on('close', function(exception) {callback(null, status,host,port);});

        socket.connect(port, host);
    };
    utils.createSocketServer = function(profile,ctx) {

        var WSocket = new WebSocket();
        WSocket.init({
            options:{
                port:profile.socket_server.port,
                context: ctx,
                host:profile.socket_server.host
            }
        });
        WSocket.ctx=ctx;
        WSocket.start(null,profile);
        return WebSocket;
    };

    /**
     * 
     * @param profile
     * @param ctx
     * @returns {*}
     */
    utils.createDeviceServer = function(profile,ctx) {
        var WSocket = new DeviceServer();
        WSocket.init({
            options:{
                port:profile.socket_server.port,
                context: ctx,
                host:profile.socket_server.host
            }
        });
        WSocket.ctx=ctx;
        //console.log('create device server ' + profile.socket_server.host+':'+profile.socket_server.port);
        WSocket.start(null,profile);
        return WSocket;
    };

    utils.createFileServer = function(profile,ctx) {

        var WSocket = new FileServer();
        WSocket.init({
            options:{
                port:profile.socket_server.port,
                context: ctx,
                host:profile.socket_server.host
            }
        });
        WSocket.ctx=ctx;
        console.error('create file server ' + profile.socket_server.host);
        WSocket.start(null,profile);
        return WebSocket;
    };

    utils.connect=function(host,port){

    };
    return utils;
});