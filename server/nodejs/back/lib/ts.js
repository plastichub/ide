var fs = require('fs');

var stat = fs.statSync('/tmp/node_modules_fs/.git');
console.log('stat : ',stat);
console.log('stat : ' + stat.isFile());
console.log('stat : ' + stat.isDirectory());

/*
stat :  { dev: 64770,
  mode: 16877,
  nlink: 9,
  uid: 1000,
  gid: 1000,
  rdev: 0,
  blksize: 4096,
  ino: 6398168,
  size: 4096,
  blocks: 8,
  atime: 2017-03-06T09:58:43.230Z,
  mtime: 2017-03-02T13:31:24.021Z,
  ctime: 2017-03-02T13:31:24.021Z,
  birthtime: 2017-03-02T13:31:24.021Z }
stat : false
stat : tru
*/
