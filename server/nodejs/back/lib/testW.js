var wsio = require('websocket.io')
	, wamp = require('wamp.io');

var ws = wsio.listen(9000);
var app = wamp.attach(ws);

app.on('call', function (procUri, args, cb) {
	if (procUri === 'isEven') {
		cb(null, args[0] % 2 == 0);
	}
});

const Wampy = require('wampy');
const wampyCra = require('wampy-cra');
const w3cws = require('websocket').w3cwebsocket;
console.log('w');
try {
	var b = new Wampy('ws://0.0.0.0:9000', {
		ws: w3cws,
		realm: 'realm1',
		//authid: 'joe',
		//authmethods: ['wampcra'],
		//transportEncoding: 'msgpack',
		onChallenge: (method, info) => {
			console.log('Requested challenge with ', method, info);
			//return wampyCra.sign('joe secret key or password', info.challenge);
		},
		onConnect: () => {
			console.log('Connected to Router!');
		},
		onClose: () => {
			console.log('close');
		},
		onError: (e) => {
			console.log('err',e.message);
		}
	});
	b.call('call',2);
} catch (e) {
	console.error('error', e);
}
