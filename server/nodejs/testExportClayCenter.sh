#!/usr/bin/env bash


#./server noob --file="./export.js" --user="../../../../user/" --system="../../../../data" --root="../../../../" --target="../../../../myapp2"
rm -rf ../../myapp2/

node ./nxappmain/export.js --windows=false --linux32=false --osx=false --arm=false --user="../../user/claycenter" --system="../../data" --root="../../" --target="../../claycenter" --dist="./dist/" --client="../../dist/all/Code/client" "$@"
cp build/server/nxappmain/serverbuild.js.uncompressed.js ../../claycenter/server/linux_64/nxappmain/serverbuild.js
cp ../../Code/client/src/lib/xapp/build/main_build.js ../../claycenter/www/Code/client/src/lib/xapp/build/main_build.js

cd ../../claycenter
sh start_linux.sh
