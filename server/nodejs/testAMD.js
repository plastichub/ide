var libRoot = '.' + '/' + 'lib/';
var path = require('path');

var libRoot =path.resolve('../../Code/client/src/lib/') + path.sep;

var dojoConfig = {
    cwd:__dirname,
    hasCache: {
        "host-node": 1,
        "host-browser":0,
        "dom":0,
        "dojo-amd-factory-scan":0,
        "dojo-has-api":1,
        "dojo-inject-api":0,
        "dojo-timeout-api":0,
        "dojo-trace-api":0,
        "dojo-log-api":0,
        "dojo-dom-ready-api":0,
        "dojo-publish-privates":1,
        "dojo-config-api":0,
        "dojo-sniff":1,
        "dojo-sync-loader":0,
        "dojo-test-sniff":0,
        "config-deferredInstrumentation":1,
        "config-useDeferredInstrumentation":"report-unhandled-rejections",
        "config-tlmSiblingOfDojo":1,
        'xlog':true,
        'xblox':true,
        'dojo-undef-api': true,
        "debug":true

    },
    trace: 1,
    async: 0,
    baseUrl: ".",
    packages: [
        {
            name: "dojo",
            location:"dojo"
        },
        {
            name: "nxappmain",
            location: "nxappmain"
        },
        {
            name: "node",
            location: "node"
        },
        {
            name: "nxapp",
            location: "nxapp"
        },
        {
            name: "xcf",
            location: libRoot +'xcf'
        },
        {
            name: "dstore",
            location: libRoot + 'dstore'
        },
        {
            name: "xide",
            location: libRoot + 'xide'
        },
        {
            name: "xwire",
            location: libRoot + 'xwire'
        },
        {
            name: "dcl",
            location: libRoot +'dcl'
        },
        {
            name: "xblox",
            location: libRoot + 'xblox'
        },
        {
            name: "xlog",
            location: libRoot + 'xlog'
        },
        {

            name: "xblox",
            location: libRoot + 'xblox'
        },
        {
            name: "dstore",
            location: libRoot + 'dstore'
        },
        {
            name: "dijit",
            location: libRoot + 'dijit'
        },
        {
            name: "xlang",
            location: libRoot + 'xlang'
        },
        {
            name: "xgrid",
            location: libRoot + 'xgrid'
        },
        {
            name: "xaction",
            location: libRoot + 'xaction/src'
        },
        {
            name: "xdojo",
            location: __dirname + '/compat/xdojo'
        }
    ]
};

var require = require("amdrequire");
console.log('__dirname',libRoot);


var paths = {
    'nxapp':__dirname+'/nxapp/',
    'xcf': libRoot +'xcf/',
    'xide': libRoot +'xide'
}


var packages = [
    {
        name: "dojo",
        location:libRoot +'dojo'
    },
    {
        name: "xcf",
        location: libRoot +'xcf'
    },
    {
        name: "dstore",
        location: libRoot + 'dstore'
    },
    {
        name: "xide",
        location: libRoot + 'xide'
    },
    {
        name: "xwire",
        location: libRoot + 'xwire'
    },
    {
        name: "dcl",
        location: libRoot +'dcl'
    },
    {
        name: "xblox",
        location: libRoot + 'xblox'
    },
    {
        name: "xlog",
        location: libRoot + 'xlog'
    },
    {

        name: "xblox",
        location: libRoot + 'xblox'
    },
    {
        name: "dstore",
        location: libRoot + 'dstore'
    },
    {
        name: "dijit",
        location: libRoot + 'dijit'
    },
    {
        name: "xlang",
        location: libRoot + 'xlang'
    },
    {
        name: "xgrid",
        location: libRoot + 'xgrid'
    },
    {
        name: "xaction",
        location: libRoot + 'xaction/src'
    },
    {
        name: "xdojo",
        location: __dirname + '/compat/xdojo'
    }
]

for (var i = 0; i < packages.length; i++) {
    var obj = packages[i];
    paths[obj.name]  = obj.location

}
require.config({
    paths: paths,
    basePath: '/',
    publicPath: '/'

});
var test = require(["nxapp/types/Types"]);
console.log('test',test);
