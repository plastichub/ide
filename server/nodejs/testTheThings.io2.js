var theThingsAPI = require('thethingsio-api');

//http client
var client = theThingsAPI.createClient();
//https client
//var client = theThingsAPI.createSecureClient();

var params = {limit:15}
client.on('ready', function () {
    client.thingRead('voltage', params, function (error, data) {
        console.log(error ? error : data)
    })
})