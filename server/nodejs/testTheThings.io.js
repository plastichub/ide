//thingToken directly

//Usage: write key-value pairs in the console

var mqtt = require('mqtt')

var activationCode = 'MPNxhB6gNZ0YWHOzRA_dRYFwwoI8-pfjhC_vQpSCs30';///you should change this
var thingToken = 'MPNxhB6gNZ0YWHOzRA_dRYFwwoI8-pfjhC_vQpSCs30'; //you can add a thingToken too if don't have an activation code

var client = mqtt.connect({
    host:'mqtt.thethings.io',
    port:1883
})

var activationTopic = 'v2/activations/'+activationCode+'/'+Math.random()
var thingTopic = 'v2/things/'+thingToken;

client.on('connect',function(){
    console.log('connected to thethings.iO!')
    if(!thingToken){
        client.subscribe(activationTopic,function(err,granted){
            console.log('subscribe result for', activationTopic, err, granted)
        })
    }else{
        client.subscribe(thingTopic,function(err,granted){
            console.log('subscribe result for', thingTopic, err, granted)
            console.log('write your key value in the console:')
            console.log('Example:')
            console.log('fun',9000)
        })
    }
})

client.on('message', function (topic, message) {
    message = JSON.parse(message)
    console.log('message',topic, message)
    if(topic === activationTopic){
        console.log('activation')
        if(message.status === 'error'){
            console.error('failed to activate your thing')
            console.error(message.message)
            process.exit(1)
        }
        console.log('thing activated, hurray!')
        thingToken = message.thingToken
        thingTopic = 'v2/things/'+thingToken
        client.subscribe(thingTopic)
        console.log('write your key value in the console:')
        console.log('Example:')
        console.log('fun',9000)
    }
})