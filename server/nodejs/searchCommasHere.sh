#!/usr/bin/env bash
set -x
jshint ./$1 | grep -v "node_modules" | grep "Extra comma"