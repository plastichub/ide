/* jshint node:true */

module.exports = function (grunt) {

	var DIST_ALL = grunt.option('DIST_ALL');
	var DIST_PLATFORM = grunt.option('DIST_PLATFORM');
	var OS = grunt.option('OS');
	var path = require('path');
	var os = require('os');
	var server_exe_suffix = os =='windows' ? '.exe' : '';
	grunt.extendConfig({
		copy: {
			'server-build':{
				src: "./build/server/nxappmain/serverbuild.js",
				dest: DIST_ALL  + "/nxappmain/serverbuild.js"
			}
		}
	});
};
