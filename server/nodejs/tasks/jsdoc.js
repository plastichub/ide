/* jshint node:true */

module.exports = function (grunt) {

	var DIST_ALL = grunt.option('DIST_ALL');
	var DIST_PLATFORM = grunt.option('DIST_PLATFORM');
	var OS = grunt.option('OS');
	var path = require('path');
	var os = require('os');

	grunt.extendConfig({
		"jsdoc-amddcl": {
			"plugins": [
				"plugins/markdown"
			],
			docs: {
				files: [
					{
						src: [
							"!./**/*",
							"nxapp/manager/*.js",
							"nxapp/protocols/*.js",
							"nxapp/types/**",
							"nxapp/model/**",
							//"nxapp/utils/**",
							"!./node_modules",
							"!nxapp/utils/Downloader.js",
							"!testDownload.js",
							"!./GruntFile.js",
							"../../Code/client/src/lib/xcf/types/**",
							"../../Code/client/src/lib/xide/types/**"
						]

					}
				]
			},
			'export': {
				files: [
					{
						args: [
							"-X"
						],
						src:[
							"!./**/*",
							"nxapp/manager/*.js",
							"nxapp/protocols/*.js",
							"nxapp/types/**",
							"nxapp/model/**",
							//"nxapp/utils/**",
							"!./node_modules",
							"!nxapp/utils/Downloader.js",
							"!testDownload.js",
							"!./GruntFile.js",
							"../../Code/client/src/lib/xcf/types/**",
							"../../Code/client/src/lib/xide/types/**"
						],
						dest: "out/doclets.json"
					}
				]
			}
		}
	});
	
	grunt.registerTask("jsdoc", "jsdoc-amddcl");
};
