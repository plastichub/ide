/* jshint node:true */

module.exports = function (grunt) {

	var DIST_ALL = grunt.option('DIST_ALL');
	var DIST_PLATFORM = grunt.option('DIST_PLATFORM');
	var NODE_VERSION = '8.3.0';
	var OS = grunt.option('OS');
	var path = require('path');
	var os = require('os');
	var server_exe_suffix = OS =='win32' ? '.exe' : '';
	var arch = os.arch();
	var is32 = arch!=='x64';
	var targetPath = OS + (is32  ? '_32'  : '_64');
	if(OS=='darwin'){
	    OS = 'osx';
	    targetPath="osx";
	}
	if(OS=='win32'){
		OS = 'windows';
		targetPath="windows";
	}
    if(OS=='windows'){
        OS = 'windows';
        targetPath="windows";
    }

	if(arch=='arm'){
		OS = 'arm';
		targetPath="arm";
	}
	grunt.extendConfig({
		dojo: {
			"server-build": {
				options: {
					ignoreErrors:true,
					profile: './profiles/server.profile.js', // Profile for build
					profiles: [], // Optional: Array of Profiles for build
					releaseDir:path.resolve('./build/server'),
					require: 'nxappmain/main_server.js' // Optional: Module to require for the build (Default: nothing)
				}
			},
			options: {
				ignoreErrors:true,
				// You can also specify options to be used in all your tasks
				dojo: 'dojo/dojo.js', // Path to dojo.js file in dojo source
				load: 'build', // Optional: Utility to bootstrap (Default: 'build')
				profile: 'app.profile.js', // Profile for build
				profiles: [], // Optional: Array of Profiles for build
				appConfigFile: '', // Optional: Config file for dojox/app
				package: '', // Optional: Location to search package.json (Default: nothing)
				packages: [


				], // Optional: Array of locations of package.json (Default: nothing)
				require: '', // Optional: Module to require for the build (Default: nothing)
				requires: [], // Optional: Array of modules to require for the build (Default: nothing)
				action: 'release', // Optional: Build action, release, help. clean has been deprecated.
				cwd: './', // Directory to execute build within
				dojoConfig: path.resolve('./nxappmain/dojoBuildConfig.js'),  // Optional: Location of dojoConfig (Default: null),
				// Optional: Base Path to pass at the command line
				// Takes precedence over other basePaths
				// Default: null
				basePath: '.'
			}
		},
		shell: {
			'server-exe':{
				command:'nexe -i ' + "\"" + path.resolve('./nxappmain/server-nexe-main_ts.js') + "\"" + ' -r ' +NODE_VERSION + ' -o ' + "\"" + path.resolve('./dist/' + targetPath) +path.sep+ 'server' + server_exe_suffix + "\"" + ' -t ' + "\""  + path.resolve('../nexeTemp_'+targetPath) + "\"",
				stderr: false,
				failOnError:false,
				execOptions: {
					cwd: './'
				}
			}
		}
	});

	grunt.registerTask('build-server-dojo', [
		'dojo:server-build', // temp
		'copy:server-build' // serverbuild.js -> dist/all/nxappmain/
	]);

	grunt.registerTask('build-server-all', [
		'build-server-dojo', // build server dojo and copy to dist/all
		'shell:server-exe',    // build platform server exe
		'copy-server'
	]);
};
