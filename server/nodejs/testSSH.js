var Connection = require('ssh2');
var c = new Connection();

var port = 22;
var host = "localhost";

var deviceOptions = {
    username:"mc007",
    password:"214,,asd"
};

var options = {
    config: false,
    host: host,
    username: deviceOptions.username,
    password: deviceOptions.password,
    agent: "",
    agentForward: false,
    port: port,
    proxy: {
        port: port
    },
    ignoreErrors: false,
    minimatch: {},
    pty: {},
    suppressRemoteErrors: false,
    callback: function() {}
};

var commands = ['cd /PMaster','pwd'];

function execCommand() {
    if (commands.length === 0) {
        //c.end();
    } else {
        var command = commands.shift();

        console.log('Executing :: ' + command);

        c.exec(command, options, function (err, stream) {
            if (err) {
                throw err;
            }
            var out;
            stream.on('data', function (data, extended) {
                out = String(data);
                if (extended === 'stderr') {
                    if (!options.suppressRemoteErrors) {
                        console.warn(out);
                    }
                    else {
                        console.warn(out);
                    }
                } else {
                    console.log(out);
                }
            });
            stream.on('end', function () {
                console.log('Stream :: EOF');
                if (out && typeof options.callback === "function") {
                    options.callback(out.trim());
                }
            });
            stream.on('exit', function () {
                console.log('Stream :: exit');
            });
            stream.on('close', function (code, signal) {
                console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                if (!options.ignoreErrors && code !== 0) {
                    console.error('Error executing task ' + command);
                    c.end();
                } else {
                    execCommand();
                }
            });
        });
    }
}
function parseConnectionOptions(options) {
    var connectionOptions = {
        host: options.host,
        port: options.port,
        username: options.username,
        readyTimeout: options.readyTimeout,
        agentForward: options.agentForward
    };

    if (options.privateKey) {
        connectionOptions.privateKey = options.privateKey.trim();

        if (options.passphrase) {
            connectionOptions.passphrase = options.passphrase.trim();
        }
    }
    else if (options.password) {
        connectionOptions.password = options.password;
        if (options.agent) {
            connectionOptions.agent = options.agent;
        }
    } else {
        connectionOptions.agent = options.agent;
    }

    return connectionOptions;
}

var connectionOptions = parseConnectionOptions(options);

c.on('keyboard-interactive', function(){
    var prompts = arguments[3];
    var reply = arguments[4];

    prompts.forEach(function(question){

        var msg = question.prompt.toLowerCase();

        if (msg.indexOf('password') !== -1){
            console.log('send password');
            reply([options.password]);
        }
    });
});

c.on('connect', function () {
    console.log('Connection :: connect');
});
c.on('ready', function () {
    console.log('Connection :: ready');
    execCommand();
});
c.on('error', function (err) {
    console.warn('Connection :: error :: ' + err);
});
c.on('debug', function (message) {
    console.log('Connection :: debug :: ' + message);
});
c.on('end', function () {
    console.log('Connection :: end');
});
c.on('close', function (had_error) {
    console.log('Connection :: close');
    console.log('finishing task');
    //done();
});

c.connect(connectionOptions);