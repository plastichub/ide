mkdir ~/qemu_vms/ -p
#Download/copy Raspbian Wheezy to ~/qemu_vms/
#Download kernel-qemu to ~/qemu_vms/
#wget https://github.com/dhruvvyas90/qemu-rpi-kernel/blob/master/kernel-qemu-4.1.13-jessie?raw=true -O kernel-qemu-4.1.13-jessie
$ sudo apt-get install qemu-system
$ file ~/qemu_vms/2013-02-09-wheezy-raspbian.img
From the output of the file command, take the partition 2 'startsector' value an multiply by 512, and use this figure as the offset value in the mount command below.
$ sudo mount ~/qemu_vms/2013-02-09-wheezy-raspbian.img -o offset=62914560 /mnt
$ sudo nano /mnt/etc/ld.so.preload
Comment out the line in the file (use a # as the first character of the line) and save the file (CTRL+X, then "Y" for yes).
$ sudo umount ~/qemu_vms/2013-02-09-wheezy-raspbian.img /mnt
$ cd ~/qemu_vms/
$ qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -no-reboot -serial stdio -append "root=/dev/sda2 panic=1" -hda ~/qemu_vms/2013-02-09-wheezy-raspbian.img -redir tcp:5022::22
Qemu gives you a root shell, run:
$ fsck /dev/sda2
$ shutdown -r now
Login as pi
Password raspberry