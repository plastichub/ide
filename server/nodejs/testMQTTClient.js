var mqtt    = require('mqtt');

var clientId = 'someClient';
var client  = mqtt.connect('mqtt://localhost',{
    clean:false,
    clientId:clientId
});

client.on('connect', function () {

    client.subscribe('192.168.1.20/23/Variable/PowerState',{
        qos:2
    });
    /*
    client.subscribe('presence');
    client.subscribe('presence');
    client.publish('presence', 'Hello mqtt');*/
});

client.on('message', function (topic, message) {

    // message is Buffer
    console.log('published : ' +topic + "\n"  +message.toString());
    //client.end();
});