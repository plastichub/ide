var os = require('os');
var ifaces = os.networkInterfaces();
var ipaddr = require('ipaddr.js');
var ips = [];

var _ = require('lodash');
var evilscan = require('evilscan');
var util = require('util');

Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;
    ifaces[ifname].forEach(function (iface) {
        if ('IPv4' !== iface.family || iface.internal !== false) {
            // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
            return;
        }

        if (alias >= 1) {
            // this single interface has multiple ipv4 addresses
            ips.push({
                face:ifname + alias,
                ip:iface.address
            })
        } else {
            ips.push({
                face:ifname,
                ip:iface.address
            })
        }
        ++alias;
    });
});

var results = {}

function checkResults(){
    var done = true;
    _.each(results,function(item){
        if(!item.done){
            done=false;
        }
    })
    if(done){
        results = _.filter(results,function(result){
           return result.list.length > 0
        });

        _.each(results,function(item){
            console.log(util.inspect(item.list));
        })
    }

}

_.each(ips,function(ip){
    var range = ipaddr.parse(ip.ip);

    var octets = range.octets;
    octets[octets.length-1] = 0;
    var target = octets.join('.') + '-254';
    //console.log('ip ' + ip.ip,target);

    var testPorts=['8080'];
    var options = {
        target :target,
        port    :testPorts.join(','),
        status  : 'Open', // Timeout, Refused, Open, Unreachable
        timeout : 500,
        banner  : false
    }

    var scanner = new evilscan(options);

    if(!results[target]){
        results[target] = {
            done:false
        };
        results[target].list = [];
    }

    scanner.on('result',function (data) {
        // fired when item is matching options
        //console.log(data);
        if(data.status==='open'){
            results[target].list.push({
                host:data.ip,
                port:data.port,
                description:ip.face + ' ' + ip.ip,
                "interface":ip.face
            });
        }
    });

    scanner.on('error',function (err) {
        //throw new Error(data.toString());
    });
    scanner.on('done',function () {
        //console.log('done');
        results[target].done=true;
        checkResults();
    });
    scanner.run();
})

