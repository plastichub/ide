/*global module */
module.exports = function (grunt) {

    var path = require('path');
    var os = require('os');
    // var stylish = require('jshint-stylish');


    var arch = os.arch();
    var is32 = arch!=='x64';
    var is64 = arch==='x64';

    var os_suffix='linux';

    var platform = os.platform();

    var CLEAN_PLATFORM=null;

    if(platform ==='win32'){
        os_suffix = 'windows';
        platform ="windows";
    }else if(platform ==='darwin'){
        os_suffix = 'osx';
        platform = 'osx';
    }else if(platform ==='linux' && os.arch()=='arm'){
        os_suffix = 'arm';
    }else if(platform ==='linux'){
        if(is32) {
            os_suffix = "linux_32";
        }else if(is64){
            os_suffix = "linux_64";
        }
    }

    var libRoot = path.resolve('../../Code/client/src/lib/')+"/";
    var DIST_ALL = path.resolve('./dist/all') + "/";
    var DIST_FINAL = path.resolve('../../dist/') + "/";
    var DIST_PLATFORM = path.resolve('./dist/' + os_suffix) + path.sep;
    var DIST_WINDOWS = path.resolve('./dist/windows') + path.sep;
    var DIST_WINDOWS_32 = path.resolve('./dist/windows_32') + path.sep;
    var DIST_LINUX = path.resolve('./dist/linux_64') + path.sep;
    var DIST_WEB = path.resolve('./dist/web') + path.sep;
    var DIST_ALL_WEB_OVERRIDE = path.resolve('./dist/web_override') + path.sep;
    var DIST_LINUX_32 = path.resolve('./dist/linux_32') + path.sep;
    var DIST_OSX = path.resolve('./dist/osx_64') + path.sep;
    var DIST_ARM = path.resolve('./dist/arm') + path.sep;
    var DIST_INSTALLER = path.resolve('./dist/installer') + path.sep;

    var IS_MAIN_DEV = grunt.option('dev') !==false;


    grunt.option('DIST_ALL',DIST_ALL);
    grunt.option('OS',platform);
    grunt.option('DIST_PLATFORM',DIST_PLATFORM);

    // Project configuration.
    grunt.initConfig({
        clean:{
            'windows-modules':[
                'dist/windows/node_modules'
            ],
            'linux_64-modules':[
                'dist/linux_64/node_modules'
            ],
            'linux_32-modules':[
                'dist/linux_32/node_modules'
            ],
            'dist-platform':[
                DIST_PLATFORM + '/node_modules/**/*.pdb',
                DIST_PLATFORM + '/node_modules/**/*.obj',
                DIST_PLATFORM + '/node_modules/**/*.lib',
                DIST_PLATFORM + '/node_modules/**/*.exp',
                DIST_PLATFORM + '/node_modules/**/MAKEFILE',
                DIST_PLATFORM + '/node_modules/**/.m4',
                DIST_PLATFORM + '/node_modules/**/.3',
                DIST_PLATFORM + '/node_modules/**/Makefile',
                DIST_PLATFORM + '/node_modules/**/Changlog',
                DIST_PLATFORM + '/node_modules/**/Changlog.*',
                DIST_PLATFORM + '/node_modules/**/*.info',
                DIST_PLATFORM + '/node_modules/**/*.texi',
                DIST_PLATFORM + '/node_modules/**/*.ac',
                DIST_PLATFORM + '/node_modules/**/*.in',
                DIST_PLATFORM + '/node_modules/**/Cakefile',
                DIST_PLATFORM + '/node_modules/**/Gruntfile.js',
                DIST_PLATFORM + '/node_modules/**/Vagrantfile',
                DIST_PLATFORM + '/node_modules/**/.tern-port',
                DIST_PLATFORM + '/node_modules/**/*.cc',
                DIST_PLATFORM + '/node_modules/**/*.c',
                DIST_PLATFORM + '/node_modules/**/*.s', //asm
                DIST_PLATFORM + '/node_modules/**/*.py',
                DIST_PLATFORM + '/node_modules/**/*.pyc',
                DIST_PLATFORM + '/node_modules/**/*.pl',
                DIST_PLATFORM + '/node_modules/**/*.rb',
                DIST_PLATFORM + '/node_modules/**/*.tlog',
                DIST_PLATFORM + '/node_modules/**/*.sln',
                DIST_PLATFORM + '/node_modules/**/.jshintrc',
                DIST_PLATFORM + '/node_modules/**/.lint',
                DIST_PLATFORM + '/node_modules/**/jsl.node.conf',
                DIST_PLATFORM + '/node_modules/**/.eslintrc',
                DIST_PLATFORM + '/node_modules/**/.editorconfig',
                DIST_PLATFORM + '/node_modules/**/.jscs.json',
                DIST_PLATFORM + '/node_modules/**/.npmignore',
                DIST_PLATFORM + '/node_modules/**/.eslintignore',
                DIST_PLATFORM + '/node_modules/**/*.dntrc',
                DIST_PLATFORM + '/node_modules/**/*.cpp',
                DIST_PLATFORM + '/node_modules/**/*.jpg',
                DIST_PLATFORM + '/node_modules/**/*.png',
                DIST_PLATFORM + '/node_modules/**/.gitmodules',
                DIST_PLATFORM + '/node_modules/**/*.h',
                DIST_PLATFORM + '/node_modules/**/*.patch',
                DIST_PLATFORM + '/node_modules/**/_test',
                DIST_PLATFORM + '/node_modules/**/LICENSE',
                DIST_PLATFORM + '/node_modules/**/*LICENSE*',
                DIST_PLATFORM + '/node_modules/**/*LICENCE*',
                DIST_PLATFORM + '/node_modules/**/License',
                DIST_PLATFORM + '/node_modules/**/license',
                //DIST_PLATFORM + '/node_modules/**/*test.js*',
                //DIST_PLATFORM + '/node_modules/**/*-test.js*',
                DIST_PLATFORM + '/node_modules/**/AUTHORS',
                DIST_PLATFORM + '/node_modules/**/LICENSE.txt',
                DIST_PLATFORM + '/node_modules/**/MIT-LICENSE.txt',
                DIST_PLATFORM + '/node_modules/**/README',
                DIST_PLATFORM + '/node_modules/**/README.md',
                DIST_PLATFORM + '/node_modules/**/usage.txt',
                DIST_PLATFORM + '/node_modules/**/*.md',
                DIST_PLATFORM + '/node_modules/**/*.txt',
                DIST_PLATFORM + '/node_modules/**/*.markdown',
                DIST_PLATFORM + '/node_modules/**/Changelog',
                DIST_PLATFORM + '/node_modules/**/CHANGES',
                DIST_PLATFORM + '/node_modules/**/CHANGELOG',
                DIST_PLATFORM + '/node_modules/**/.travis.yml',
                DIST_PLATFORM + '/node_modules/**/appveyor.yml',
                DIST_PLATFORM + '/node_modules/**/robot.html',
                DIST_PLATFORM + '/node_modules/**/examples',
                DIST_PLATFORM + '/node_modules/**/example',
                DIST_PLATFORM + '/node_modules/**/example.js',
                DIST_PLATFORM + '/node_modules/**/screenshots',
                DIST_PLATFORM + '/node_modules/**/.gitattributes',
                DIST_PLATFORM + '/node_modules/**/benchmarks',
                DIST_PLATFORM + '/node_modules/**/*benchmark*',
                DIST_PLATFORM + '/node_modules/**/coverage',
                DIST_PLATFORM + '/node_modules/**/docs',
                DIST_PLATFORM + '/node_modules/**/*.coffee',
                DIST_PLATFORM + '/node_modules/**/tests',
                DIST_PLATFORM + '/node_modules/**/*.vcxproj',
                DIST_PLATFORM + '/node_modules/**/*.vcxproj.filters',
                DIST_PLATFORM + '/node_modules/**/node-pre-gyp',
                DIST_PLATFORM + '/node_modules/**/node-gyp',
                DIST_PLATFORM + '/node_modules/**/gyp',
                DIST_PLATFORM + '/node_modules/**/*.gypi'
            ]
        },
        copy: {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            //  Copy new files into dist/all
            //
            'server-dist-all':{
                src: "./build/server/nxappmain/serverbuild.js",
                dest: DIST_ALL  + "/nxappmain/serverbuild.js"
            },
			'server-build':{
				src: "dist/all/nxappmain/serverbuild.js",
				dest: "./nxappmain/serverbuild.js"
			},
			'ts-server-dist-all':{
            	cwd:"./lib/build",
				src: "**/*.js",
				dest: DIST_ALL  + "/_build/",
				expand: true,
				flatten: false
			},
			'ts-server-views-dist-all':{
            	cwd:"./lib/src/",
				src: ["**/*.ejs","**/*.html","**/*.css","**/*.png"],
				dest: DIST_ALL  + "/_build/",
				expand: true,
				flatten: false
			},
			'ts-server':{
				cwd:"./lib/build",
				src: "**",
				dest: "./_build/",
				expand: true,
				flatten: false
			},

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            //  Copy from dist/all to dist/linux
            //
            'dist-all-linux-64':{
                src: ['**'],
                dest: DIST_PLATFORM,
                cwd: DIST_ALL,
                expand: true,
                flatten: false
            },
            'dist-all-linux-32':{
                src: ['**'],
                dest: DIST_LINUX_32,
                cwd: DIST_ALL,
                expand: true,
                flatten: false
            },
            'dist-all-windows-32':{
                src: ['**'],
                dest: DIST_WINDOWS_32,
                cwd: DIST_ALL,
                expand: true,
                flatten: false
            },
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            //  Copy from dist/all to dist/arm
            //
            'dist-all-arm':{
                src: ['**'],
                dest: DIST_ARM,
                cwd: DIST_ALL,
                expand: true,
                flatten: false
            },
            'dist-all-web':{
                src: ['**'],
                dest: DIST_WEB,
                cwd: DIST_ALL,
                expand: true,
                flatten: false
            },
            'dist-all-web-override':{
                src: ['**'],
                dest: DIST_WEB,
                cwd: DIST_ALL_WEB_OVERRIDE,
                expand: true,
                flatten: false
            },

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            //  Copy from dist/all to dist/windows
            //
            'dist-all-windows':{
                src: ['**'],
                dest: DIST_WINDOWS,
                cwd: DIST_ALL,
                expand: true,
                flatten: false
            },
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            //  Copy from dist/all to dist/osx
            //
            'dist-all-osx':{
                src: ['**'],
                dest: DIST_OSX,
                cwd: DIST_ALL,
                expand: true,
                flatten: false
            },
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            //  Copy from dist/all to dist/installer
            //
            'dist-all-installer':{
                src: ['**'],
                dest: DIST_INSTALLER,
                cwd: DIST_ALL,
                expand: true,
                flatten: false,
                filter:function(what){
                    return what.indexOf('package.json') == -1;
                }
            },
            'dist-windows':{
                src: ['**'],
                dest: DIST_FINAL + '/windows/server/windows/',
                cwd: DIST_WINDOWS,
                expand: true,
                flatten: false
            }
        },
        pkg: grunt.file.readJSON("package.json"),
        sshexec: {
            "build-windows": {
                command: [
                    'build-server.bat'
                ],
                options: {
                    host: 'windows',
                    username: 'mc007',
                    password: 'asdasd'
                }
            },
            "build-linux_32": {
                command: [
                    'build_linux_32.sh'
                ],
                options: {
                    host: 'linux_32',
                    username: 'mc007',
                    password: 'asdasd'
                }
            }
        },
        shell: {
            'build-linux_64':{
                command:'sh ./build-linux_64.sh',
                stderr: true,
                failOnError:true,
                execOptions: {
                    cwd: './'
                }
            },
            'clean-linux_64':{
                command:'sh ./clean-linux_64.sh',
                stderr: true,
                failOnError:true,
                execOptions: {
                    cwd: './'
                }
            },
            'commit-windows':{
                command:'cd dist/windows; git add -A . ; gitc',
                stderr: true,
                failOnError:true
            },
			'build-ts':{
				command:'cd lib; npm run build;',
				stderr: true,
				failOnError:true
			},
            'xide-build-jshint':{
                command:['jshint \"../client/src/lib/xide/\"  | grep \"Extra comma\" || true'].join(' && '),
                stderr: true,
                failOnError:false,
                execOptions: {
                    cwd: './'
                }
            },
            'commit-dist-windows':{
                command:'cd ../../dist/windows; git add -A . ; gitc',
                stderr: true,
                failOnError:true
            }
        },
        dojo: {
            server: {
                options: {

                    profile: './profiles/server.profile.js', // Profile for build
                    profiles: [], // Optional: Array of Profiles for build
                    releaseDir:path.resolve('./build/server'),
                    require: 'nxappmain/main_server.js' // Optional: Module to require for the build (Default: nothing)
                }
            },
            options: {
                // You can also specify options to be used in all your tasks
                dojo: 'dojo/dojo.js', // Path to dojo.js file in dojo source
                load: 'build', // Optional: Utility to bootstrap (Default: 'build')
                profile: 'app.profile.js', // Profile for build
                profiles: [], // Optional: Array of Profiles for build
                appConfigFile: '', // Optional: Config file for dojox/app
                package: '', // Optional: Location to search package.json (Default: nothing)
                packages: [


                ], // Optional: Array of locations of package.json (Default: nothing)
                require: '', // Optional: Module to require for the build (Default: nothing)
                requires: [], // Optional: Array of modules to require for the build (Default: nothing)
                action: 'release', // Optional: Build action, release, help. clean has been deprecated.
                cwd: './', // Directory to execute build within
                dojoConfig: path.resolve('./nxappmain/dojoBuildConfig.js'),  // Optional: Location of dojoConfig (Default: null),
                // Optional: Base Path to pass at the command line
                // Takes precedence over other basePaths
                // Default: null
                basePath: '.'
            }
        }
    });
    grunt.loadNpmTasks("jsdoc-amddcl");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks('grunt-nginx');
    grunt.loadNpmTasks('grunt-ssh');
    grunt.loadNpmTasks('grunt-dojo');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-extend-config");
    grunt.loadTasks('tasks');
    /**
     * The device server
     */
    grunt.registerTask('build-server', [
        'shell:server-exe',
        'copy:serverwindows.exe'
    ]);
    grunt.registerTask('update-server', [
        'dojo:server'
    ]);
    grunt.registerTask('copy-server',"Nada",function(){

		if(platform=='linux') {
			grunt.task.run('shell:build-ts');
			grunt.task.run('copy:ts-server-dist-all');
			grunt.task.run('copy:ts-server-views-dist-all');
			grunt.task.run('copy:server-build');
			grunt.task.run('copy:ts-server');
		}

        platform = grunt.option('platform') || platform;

        if(platform!=='linux'){
            grunt.task.run('copy:dist-all-'+platform);
            if(platform==='web'){
                grunt.task.run('copy:dist-all-web-override');
            }
        }else{

            grunt.task.run('copy:server-dist-all');
            grunt.task.run('copy:dist-all-linux-64');
            grunt.task.run('copy:dist-all-linux-32');
            grunt.task.run('copy:dist-all-arm');
            grunt.task.run('copy:dist-all-windows');
            grunt.task.run('copy:dist-all-osx');
            grunt.task.run('copy:dist-all-web');
            if(platform==='web'){
                grunt.task.run('copy:dist-all-web-override');
            }

        }

    });
    grunt.registerTask('clean-platform',"Clean node_modules of this or a specified platform",function(){

        var platform = CLEAN_PLATFORM || grunt.option('platform') || os_suffix;

        var platformPath = path.resolve('./dist/' + platform) + path.sep;
        console.log('cleaning node modules in ' + platformPath);
        grunt.extendConfig({
            clean: {
                "platform": [
                    platformPath + '/node_modules/**/*.pdb',
                    platformPath + '/node_modules/firmata/node_modules',
                    platformPath + '/node_modules/serialport/node_modules',
                    platformPath + '/node_modules/johnny-five/node_modules',
                    platformPath + '/node_modules/**/*.o.d',
                    platformPath + '/node_modules/**/*.a.d',
                    platformPath + '/node_modules/**/*.obj',
                    platformPath + '/node_modules/**/*.lib',
                    platformPath + '/node_modules/**/*.exp',
                    platformPath + '/node_modules/**/MAKEFILE',
                    platformPath + '/node_modules/**/Makefile',
                    platformPath + '/node_modules/**/*.Makefile',
                    platformPath + '/node_modules/**/*.target.mk',
                    platformPath + '/node_modules/**/*.node.mk',
                    platformPath + '/node_modules/**/Cakefile',
                    platformPath + '/node_modules/**/Gruntfile.js',
                    platformPath + '/node_modules/**/Vagrantfile',
                    platformPath + '/node_modules/**/.tern-port',
                    platformPath + '/node_modules/**/*.cc',
                    platformPath + '/node_modules/**/*.c',
                    platformPath + '/node_modules/**/*.s', //asm
                    platformPath + '/node_modules/**/*.S', //asm
                    platformPath + '/node_modules/**/*.py',
                    platformPath + '/node_modules/**/*.pyc',
                    platformPath + '/node_modules/**/*.pl',
                    platformPath + '/node_modules/**/*.rb',
                    platformPath + '/node_modules/**/*.tlog',
                    platformPath + '/node_modules/**/*.sln',
                    platformPath + '/node_modules/**/.jshintrc',
                    platformPath + '/node_modules/**/.lint',
                    platformPath + '/node_modules/**/jsl.node.conf',
                    platformPath + '/node_modules/**/.eslintrc',
                    platformPath + '/node_modules/**/.editorconfig',
                    platformPath + '/node_modules/**/.jscs.json',
                    platformPath + '/node_modules/**/.npmignore',
                    platformPath + '/node_modules/**/.eslintignore',
                    platformPath + '/node_modules/**/*.dntrc',
                    platformPath + '/node_modules/**/*.cpp',
                    platformPath + '/node_modules/**/*.jpg',
                    platformPath + '/node_modules/**/*.png',
                    platformPath + '/node_modules/**/.gitmodules',
                    platformPath + '/node_modules/**/*.h',
                    platformPath + '/node_modules/**/*.patch',
                    platformPath + '/node_modules/**/_test',
                    platformPath + '/node_modules/**/LICENSE',
                    platformPath + '/node_modules/**/*LICENSE*',
                    platformPath + '/node_modules/**/*LICENCE*',
                    platformPath + '/node_modules/**/License',
                    platformPath + '/node_modules/**/license',
                    //platformPath + '/node_modules/**/*test.js*',
                    //platformPath + '/node_modules/**/*-test.js*',
                    platformPath + '/node_modules/**/AUTHORS',
                    platformPath + '/node_modules/**/LICENSE.txt',
                    platformPath + '/node_modules/**/MIT-LICENSE.txt',
                    platformPath + '/node_modules/**/README',
                    platformPath + '/node_modules/**/README.md',
                    platformPath + '/node_modules/**/usage.txt',
                    platformPath + '/node_modules/**/*.md',
                    platformPath + '/node_modules/**/*.txt',
                    platformPath + '/node_modules/**/*.markdown',
                    platformPath + '/node_modules/**/Changelog',
                    platformPath + '/node_modules/**/CHANGES',
                    platformPath + '/node_modules/**/CHANGELOG',
                    platformPath + '/node_modules/**/.travis.yml',
                    platformPath + '/node_modules/**/appveyor.yml',
                    platformPath + '/node_modules/**/robot.html',
                    platformPath + '/node_modules/**/examples',
                    platformPath + '/node_modules/**/example',
                    platformPath + '/node_modules/**/example.js',
                    platformPath + '/node_modules/**/screenshots',
                    platformPath + '/node_modules/**/.gitattributes',
                    platformPath + '/node_modules/**/benchmarks',
                    platformPath + '/node_modules/**/*benchmark*',
                    platformPath + '/node_modules/**/coverage',
                    platformPath + '/node_modules/**/docs',
                    platformPath + '/node_modules/**/*.coffee',
                    platformPath + '/node_modules/**/tests',
                    platformPath + '/node_modules/**/*.vcxproj',
                    platformPath + '/node_modules/**/*.vcxproj.filters',
                    platformPath + '/node_modules/**/node-pre-gyp',
                    platformPath + '/node_modules/**/node-gyp',
                    platformPath + '/node_modules/**/gyp',
                    platformPath + '/node_modules/**/*.gypi'
                ]
            }
        });
        grunt.task.run('clean:platform');
    });

    grunt.registerTask('build-server-windows', [
        'shell:server-exe',
        'copy:serverwindows.exe'
    ]);

    grunt.registerTask('build-windows', 're-install windows modules and commit',function(){
        grunt.task.run('clean:windows-modules');
        grunt.task.run('sshexec:build-windows');
        CLEAN_PLATFORM = "windows";
        grunt.task.run('clean-platform');
        grunt.task.run('shell:commit-windows');
        grunt.task.run('copy:dist-windows');
        grunt.task.run('shell:commit-dist-windows');
    });

    grunt.registerTask('build-linux_64', 'install linux modules',function(){
        grunt.task.run('clean:linux_64-modules');
        grunt.task.run('shell:build-linux_64');
        CLEAN_PLATFORM = "linux_64";
        grunt.task.run('clean-platform');
        grunt.task.run('shell:clean-linux_64');
    });

    grunt.registerTask('platform','platform independent task',function(){

        platform = grunt.option('platform') || platform;

        if(grunt.option('dojo')!==false && platform==='linux'){
            grunt.task.run('build-server-dojo');
        }
        if(grunt.option('nexe')!==false && platform!=='web'){
            grunt.task.run('shell:server-exe');
        }

        if(grunt.option('copy')!==false){
            grunt.task.run('copy-server');
        }

        if(grunt.option('clean')!==false){
            //grunt.task.run('clean:dist-platform');
        }
    });

};
