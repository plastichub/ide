var five = require("johnny-five"),
    board, potentiometer;

board = new five.Board();

var configs = five.Motor.SHIELD_CONFIGS.ADAFRUIT_V1;

//console.dir(configs);

//var motor1 = new five.Motor(configs.M1);
/*
var motor2 = new five.Motor(configs.M2);
var motor3 = new five.Motor(configs.M3);
var motor4 = new five.Motor(configs.M4);
*/

function stop(nr){
    var motor = new five.Motor(nr);
    motor.stop();
}

/*
#define MOTOR1_PWM 11
#define MOTOR2_PWM 3
#define MOTOR3_PWM 6
#define MOTOR4_PWM 5
#define SERVO1_PWM 10
#define SERVO2_PWM 9
*/
// Connector usage
// ---------------
// The order is different than what you would expect.
// If the Arduino (Uno) board is held with the USB
// connector to the left, the positive (A) side is
// at the top (north), and the negative (B) side is
// the bottom (south) for both headers.
//
//   Connector X1:
//     M1 on outside = MOTOR1_A   (+) north
//     M1 on inside  = MOTOR1_B   (-)
//     middle        = GND
//     M2 on inside  = MOTOR2_A   (+)
//     M2 on outside = MOTOR2_B   (-) south
//
//   Connector X2:
//     M3 on outside = MOTOR3_B   (-) south
//     M3 on inside  = MOTOR3_A   (+)
//     middle        = GND
//     M4 on inside  = MOTOR4_B   (-)
//     M4 on outside = MOTOR4_A   (+) north
//
//
//         -------------------------------
//         | -+s                         |
//         | -+s                         |
//    M1 A |                             | M4 A
//    M1 B |                             | M4 B
//    GND  |                             | GND
//    M2 A |                             | M3 A
//    M2 B |                             | M3 B
//         |                       ..... |
//         -------------------------------
//                + -
// 8-bit bus after the 74HC595 shift register
// (not Arduino pins)
// These are used to set the direction of the bridge driver.
/*
#define MOTOR1_A 2
#define MOTOR1_B 3
#define MOTOR2_A 1
#define MOTOR2_B 4
#define MOTOR3_A 5
#define MOTOR3_B 7
#define MOTOR4_A 0
#define MOTOR4_B 6

// Arduino pins for the PWM signals.
#define MOTOR1_PWM 11
#define MOTOR2_PWM 3
#define MOTOR3_PWM 6
#define MOTOR4_PWM 5
#define SERVO1_PWM 10
#define SERVO2_PWM 9

// Codes for the motor function.
#define FORWARD 1
#define BACKWARD 2
#define BRAKE 3
#define RELEASE 4

 motorA   = MOTOR4_A;
 motorB   = MOTOR4_B;

 case FORWARD:

 #define MOTOR4_A 0
 #define MOTOR4_B 6

 motor_output (motorA, HIGH, speed);
 motor_output (motorB, LOW, -1);

*/
board.on("ready", function() {

    var configs = five.Motor.SHIELD_CONFIGS.ADAFRUIT_V1;

    var motor1 = new five.Motor(configs.M4);
    motor1.stop(100);



    for (var i = 7; i < 15; i++) {
        //stop(i);
    }

    //var motor = new five.Motor(0);

    //motor.start();



/*
    var s = new five.Servo({
        pin: 9,
        center: true
    });
    */


    //s.sweep();




    /*

    var motor = new five.Motor({
         pins:{
             pwm: 10
         }
        }
    );
    motor.stop();
    // Start the motor at maximum speed
    motor.start(100);
    motor.forward(255);
    */

    //var motor2 = new five.Motor(10);
    //motor.start(100);
    // Start the motor at maximum speed
    //motor2.forward(255);
    //motor2.stop();

});