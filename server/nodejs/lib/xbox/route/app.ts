import * as Router from 'koa-router';
import * as path from 'path';
const views = require('co-views');
import { IObjectLiteral } from '../../../interfaces/index';
import { ResourceRenderer } from '../../../resource/Renderer';
import { xbox } from '../../../applications/xbox/index';
import { EEKey } from '../../../applications/Base';
const appRouter = new Router({ prefix: '/app' });
import * as _ from 'lodash';

appRouter.get('/:name', async (ctx: Router.IRouterContext) => {
	const rtConfig = ctx.request.query.debug === 'true' ? 'debug' : 'release';
	const app: xbox = (<xbox> ctx.app);
	const appName = ctx.params.name;
	const config = (<IObjectLiteral> app)['config'];
	const render = views(path.join(config.NODE_ROOT, '/src/views'), { ext: 'ejs' });
	let variables = _.extend({}, config.relativeVariables);
	app.variables(ctx, variables);
	const RESOURCE_OFFSET = rtConfig === 'debug' ? '/lib/' : '';
	const tplParams = {
		// see NODE_ROOT/views/app_name.[debug|release].ejs# <%- HTML_HEADER %>
		// this part contains pretty all HTML-HEAD resources, ei: CSS, external scripts
		HTML_HEADER: new ResourceRenderer(
			// clientRootSource = /Code/client/src/lib/[app_name]/resources-' + [debug|release] + '.json'
			app._env(null, EEKey.CLIENT_ROOT) + RESOURCE_OFFSET + appName + '/resources-' + rtConfig + '.json',
			// variables to replace in resource description '/resources-' + [debug|release]] + '.json
			variables, config.absoluteVariables)
			// render all resources resolved as string
			.renderHeader(),

		// same as HEADER
		BODY_RESOURCES: '',
		// results in <body class="xTheme-default xTheme-<%-THEME%>">
		THEME: variables[EEKey.THEME],
		APP_URL: variables['APP_URL']
	};
	ctx.body = await render(appName + '_' + rtConfig, tplParams);
});
export default appRouter;
