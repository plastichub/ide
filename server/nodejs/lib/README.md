
Experimental re-implementation of xphp in Typescript

## Impacts for Control-Freak/Net-Commander

- No PHP needed anymore: 100 MB less 
- No NGINX needed anymore: 10 MB less

- All virtual file systems are now naturally mounted at /files/mount/...path... instead of
  the API point 'index.php' ?

- Mounting the VFS like above removes a lot of workarounds and dirty hacks and has direct positive impacts for VE/Driver development

- Device server and this new internal web-server will run now as one

- Exported apps will be also much smaller and easier to run

### New other features

- strong security now easily possible for any request but also the currently unprotected device-server

- IDE JSON-RPC-2 will tunneled over Web-Sockets

- IDE can be bundled now into one single exe file since theres is no need for additional binaries anymore

- Much, much faster

### Todos

- Impl./activate missing server methods for xfile/xideve/xcf
- Authentification
- client render offloading to server, streamed and component based
