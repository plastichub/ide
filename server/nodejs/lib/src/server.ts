import * as global from './global';

export default function (root) {
	var commander = require('commander'),
		path = require('path'),
		lodash = require('lodash'),
		clientRoot = path.resolve('../../Code/client/src/');

	var _dojoConfig = require(path.resolve('./nxappmain/dojoConfig.js')),
		defaultProfile = './nxappmain/profile_xcom.json',
		initModule = "./nxappmain/main_server";

	//global.console = console;
	global['_'] = lodash;
	global['nRequire'] = require;
	var dojoConfig = _dojoConfig.createDojoConfig(path.resolve('./logs'), initModule, null, clientRoot);
	global['dojoConfig'] = dojoConfig;
	console.error("run in " + path.resolve('.'));
	dojoConfig.profilePath = defaultProfile;
	dojoConfig.nRequire = require;
	try {
		//console.log('require dojo');
		require("../dojo/dojo");
		console.log('did require dojo');
	} catch (e) {
		console.error('error loading server : ' + e.message, e.stack);
	}

}
