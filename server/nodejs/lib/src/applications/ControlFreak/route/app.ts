import { EEKey, ELayout } from '../../../applications/Base';
import { ControlFreak as app } from '../../../applications/ControlFreak/index';
import { IObjectLiteral } from '../../../interfaces/index';
import { ResourceRenderer } from '../../../resource/Renderer';
import * as views from 'co-views';
import * as Router from 'koa-router';
import * as _ from 'lodash';
import * as path from 'path';

const appRouter = new Router({ prefix: '/app' });

appRouter.get('/:name', async (ctx: Router.IRouterContext) => {
	const rtConfig = ctx.request.query.debug === 'true' ? 'debug' : 'release';
	const app: app = (<app>ctx.app);
	const appName = ctx.params.name;
	const config = (<IObjectLiteral>app)['config'];
	let render = null;
	if (app.options.type === ELayout.OFFLINE_RELEASE) {
		render = views(path.join(app.path(EEKey.NODE_ROOT), '/_build/views'), { ext: 'ejs' });
	} else {
		render = views(path.join(process.cwd(), '/src/views'), { ext: 'ejs' });
	}
	const variables = _.extend({}, config.relativeVariables);
	app.variables(ctx, variables);
	variables['XASWEB'] = variables['ROOT'] + 'Code/client/src/';
	if (app.options.print) {
		console.log('Render with', variables);
	}
	const RESOURCE_OFFSET = rtConfig === 'debug' ? '/lib/' : '';
	const RESOURCE_CONFIG = app._env(null, EEKey.CLIENT_ROOT) + RESOURCE_OFFSET + appName + '/resources-' + rtConfig + '.json';
	if (app.options.print) {
		console.log('Render with resource config: ' + rtConfig + ' | xasweb=' + variables['XASWEB'], RESOURCE_CONFIG);
	}
	// ejs params
	const tplParams = {
		// see srcRoot/views/app_name.[debug|release].ejs# <%- HTML_HEADER %>
		// this part contains pretty all HTML-HEAD resources, ei: CSS, external scripts
		HTML_HEADER: new ResourceRenderer(
			// clientRootSource = /Code/client/src/lib/[app_name]/resources-' + [debug|release] + '.json'
			RESOURCE_CONFIG,
			// variables to replace in resource description '/resources-' + [debug|release]] + '.json
			variables, config.absoluteVariables)
			// render all resources resolved as string
			.renderHeader(),

		// same as HEADER
		BODY_RESOURCES: '',
		// results in <body class="xTheme-default xTheme-<%-THEME%>">
		THEME: variables[EEKey.THEME],

		APP_URL: variables[EEKey.APP_URL]
	};
	ctx.body = await render(appName + '_' + rtConfig, tplParams);
});
export default appRouter;
