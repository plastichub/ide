import { ApplicationBase, EEKey, ESKey, ELayout, IApplication, Options, IInterface } from './../Base';
import { IObjectLiteral, EArch } from '../../interfaces/index';
import { EPersistence } from '../../interfaces/Application';
import { IResourceDriven } from '../../interfaces/Resource';
import { create as ServiceConfigFactory, IServiceConfiguration } from '../../interfaces/Service';
import appRoutes from './route/app';
import smd from '../../route/smd';
import { create as createFileRoute, FileRouter } from '../../route/files';
import { create as createUploadRoute, UploadRouter } from '../../route/uploads';
import { JSON_RPC_2 } from '../../rpc/JSON-RPC-2';
import { DeviceService } from '../../services/Devices';
import { DirectoryService } from './services/Directory';
import { DriverService } from '../../services/Drivers';
import { JSONFileService } from '../../services/JSONFile';
import { MountService } from '../../services/Mounts';
import { NodeService } from '../../services/Services';
import { TrackingService } from '../../services/Tracking';
import { ExternalService } from '../../services/External';
import { Mongod } from '../../services/external/Mongod';
import { XIDEVE } from '../../components/xideve/xideve';
import { XFILE } from '../../components/xfile/xfile';
import { Component } from '../../components/Component';
import { LogsService } from '../../services/Logs';
import { registerService } from '../../services/register';
import * as Koa from 'koa';
import * as convert from 'koa-convert';
import * as bodyParser from 'koa-bodyparser';
import * as serve from 'koa-static';
const cors = require('koa2-cors');

import * as path from 'path';
import * as _ from 'lodash';
import * as os from 'os';
import * as Router from 'koa-router';
import { List } from '../../interfaces/index';
import { BaseService } from '../../services/Base';
import { read } from '../../io/file';
import { deserialize, safe } from '../../io/json';
import * as yargs_parser from 'yargs-parser';
import { console } from '../../console';
import { io } from '../../interfaces/io';

import { create } from './cli';
create();
import open from '../../utils/open';
import { runElectron } from './desktop/app';
const mount = require('koa-mount');
const argv = yargs_parser(process.argv.slice(2));
const util = require('util');
const osTmpdir = require('os-tmpdir');
const mkdirp = require('mkdirp');

const MODULE_ROOT = '../../';
const COMPONENT_ROOT = '../../components/';

// import { serveIndex } from '../../server/index';

// tslint:disable-next-line:interface-name
export class ControlFreak extends ApplicationBase implements IApplication {
	test: number;
	uuid = 'ide';
	config: IObjectLiteral;
	root: string;
	rpc2: any;
	_rpcServices: BaseService[];
	_externalServices: ExternalService[];
	_routes: Router[];
	directoryService: DirectoryService;
	settingsService: JSONFileService;
	options: Options;
	deviceServer: any = null;
	profile: any = null;
	_components: Component[];
	running: IObjectLiteral = {};
	_getProfile(_path: string) {
		let data = null;
		try {
			_path = _path ? path.resolve(_path) : path.join(this.path(EEKey.NODE_ROOT), 'nxappmain/profile_device_server.json');
			console.info('ControlFreak#init : use server profile from ' + _path);
			data = deserialize(read(_path));
		} catch (e) {
			console.error('error reading profile : ', e);
		}
		return data || {
			mongo: {
				port: 27017
			},
			http: {
				port: 5555,
				host: "0.0.0.0"
			}
		};
	}
	runClass(data: IObjectLiteral, deviceServerContext: any) {
		const _class = data['class'];
		const _args = data['args'];
		let _module = null;
		const delegate = {
			data: data,
			clear: function () {
				this.data.progress = null;
				this.data.finish = null;
				this.data.error = null;
				this.data.data = null;
			},
			onProgress: function (progress, data) {
				this.clear();
				this.data.progress = progress;
				this.data.data = data;
				deviceServerContext.broadCastMessage(null, this.data);
			},
			onFinish: function (finish, data) {
				this.clear();
				this.data.finish = finish;
				this.data.data = data;
				deviceServerContext.broadCastMessage(null, this.data);
			},
			onError: function (error, data) {
				this.clear();
				this.data.error = error;
				this.data.data = data;
				deviceServerContext.broadCastMessage(null, this.data);
			}
		};
		try {
			_module = require(MODULE_ROOT + _class);
		} catch (e) {
			console.error('runClass# Error : cant find class ' + _class);
			data['error'] = e.message;
			deviceServerContext.broadCastMessage(null, data);
			return;
		}
		try {
			let instance = new _module.default(_args as any, delegate);
			instance.run();
		} catch (e) {
			data['error'] = e.message;
			deviceServerContext.broadCastMessage(null, data);
		}
	}
	runClassMethod(data: IObjectLiteral, deviceServerContext: any, client: any) {
		const _class = data['class'];
		const _args = data['args'];
		const _method = data['method'];
		let _module = null;
		const delegate = {
			data: data,
			clear: function () {
				this.data.progress = null;
				this.data.finish = null;
				this.data.error = null;
				this.data.data = null;
			},
			onProgress: function (progress, data) {
				this.clear();
				this.data.progress = progress;
				this.data.data = data;
				deviceServerContext.broadCastMessage(null, this.data);
			},
			onFinish: function (finish, data) {
				this.clear();
				this.data.finish = finish;
				this.data.data = data;
				deviceServerContext.broadCastMessage(null, this.data);
			},
			onError: function (error, data) {
				this.clear();
				this.data.error = error;
				this.data.data = data;
				deviceServerContext.broadCastMessage(null, this.data);
			}
		};
		try {
			_module = require(MODULE_ROOT + _class);
		} catch (e) {
			console.error('runClass# Error : cant find class ' + _class);
			data['error'] = e.message;
			deviceServerContext.broadCastMessage(null, data);
			return;
		}
		_args['app'] = this;
		try {
			let instance = new _module.default(_args as any, delegate);
			instance[_method]();
		} catch (e) {
			data['error'] = e.message;
			deviceServerContext.broadCastMessage(null, data);
		}
	}
	getComponent(name: string) {
		if (name === 'xfile') {
			return XFILE;
		}
	}
	cancelComponentMethod(data: IObjectLiteral, deviceServerContext: any, client: any) {
		const _id = data['id'];
		if (this.running[_id]) {
			this.running[_id].cancel();
			delete this.running[_id];
		} else {
			console.error('cant cancel component method: no instance for id ' + _id);
		}
	}
	answerAppServerComponentMethodInterrupt(data: IObjectLiteral, deviceServerContext: any, client: any) {
		const _id = data['id'];
		if (this.running[_id]) {
			this.running[_id].answer(data['answer']);
		} else {
			console.error('cant answer component method interrupt: no instance for id ' + _id, this.running);
		}
	}
	/**
	 * runClass is a back - compat port for nxapp,
	 */
	runComponentMethod(data: IObjectLiteral, deviceServerContext: any, client: any) {
		const componentClass = data['component'];
		const _args = [].concat(data['args']);
		const _options = data['options'];
		const _id = _options['id'];
		const _method = data['method'];
		let instance: Component = null;
		const delegate = {
			data: data,
			clear: function () {
				this.data.progress = null;
				this.data.finish = null;
				this.data.error = null;
				this.data.data = null;
				this.data.interrupt = null;
			},
			onProgress: function (progress, data) {
				this.clear();
				this.data.progress = progress;
				this.data.data = data;
				deviceServerContext.deviceServer.sendClientMessage(client, null, 'onRunClassEvent', JSON.parse(safe(this.data)));
			},
			onFinish: function (finish, data) {
				this.clear();
				this.data.finish = finish;
				this.data.data = data;
				deviceServerContext.deviceServer.sendClientMessage(client, null, 'onRunClassEvent', JSON.parse(safe(this.data)));
			},
			onError: function (error, data) {
				this.clear();
				this.data.error = error;
				this.data.data = data;
				deviceServerContext.deviceServer.sendClientMessage(client, null, 'onRunClassEvent', JSON.parse(safe(this.data)));
				self.running[_id].destroy();
				delete self.running[_id];
			},
			onInterrupt: function (interrupt, data) {
				this.clear();
				this.data.data = data;
				this.data.interrupt = interrupt;
				deviceServerContext.deviceServer.sendClientMessage(client, null, 'onRunClassEvent', JSON.parse(safe(this.data)));
			},
			end: function () {
				self.running[_id].destroy();
				delete self.running[_id];
			}
		};

		_options.delegate = delegate;
		// let _module: Component = this.getComponent(component);
		let _module: any = null;
		try {
			_module = this.getComponent(componentClass);
		} catch (e) {
			console.error('runComponentMethod# Error : cant find component ' + componentClass + ' at ' + path.resolve(COMPONENT_ROOT + componentClass), data);
			data['error'] = e.message;
			deviceServerContext.deviceServer.sendClientMessage(client, null, 'onRunClassEvent', JSON.parse(safe(data)));
			return;
		}
		try {
			instance = new _module(this, this.serviceConfig(), _options);
		} catch (e) {
			data['error'] = e.message;
			deviceServerContext.deviceServer.sendClientMessage(client, null, 'onRunClassEvent', JSON.parse(safe(data)));
			return;
		}
		let abort = false;
		let self = this;
		if (abort) {
			return;
		}

		if (!_module) {
			console.error('runComponentMethod# Error : cant find component' + componentClass);
			data['error'] = 'runComponentMethod# Error : cant find component';
			deviceServerContext.broadCastMessage(null, JSON.parse(safe(data)));
			return;
		}
		if (!instance[_method]) {
			let msg = 'runComponent# Error : component ' + componentClass + ' has no such method : ' + _method;
			console.error(msg);
			data['error'] = msg;
			deviceServerContext.broadCastMessage(null, JSON.parse(safe(data)));
			return;
		}
		try {
			instance[_method].apply(instance, _args);
			this.running[_id] = instance;
		} catch (e) {
			data['error'] = e.message;
			console.error('error running component method :' + componentClass + '#' + _method, e);
			deviceServerContext.broadCastMessage(null, JSON.parse(safe(data)));
		}

		client.on('close', function () {
			self.running[_id].cancel();
			self.running[_id].destroy();
			delete self.running[_id];
		});
	}
	constructor(options: Options) {
		super(options.root);
		this.options = options;
		this.root = options.root;
		this.uuid = options.uuid;
		const APP_ROOT = this.root;
		const CLIENT_ROOT = path.join(APP_ROOT, 'Code/client/src/');
		const NODE_ROOT = options.release ? process.cwd() : path.join(APP_ROOT, 'server/nodejs/');
		const USER_DIRECTORY = options.user || path.join(APP_ROOT, '/user');
		const DATA_ROOT = path.join(APP_ROOT, '/data/');
		let DB_ROOT = null;
		if (this.options.persistence === EPersistence.MONGO) {
			const TMP_PATH = osTmpdir();
			if (argv.mqtt !== 'false') {
				try {
					mkdirp.sync(TMP_PATH + path.sep + '_MONGO' + '_' + this.uuid);
				} catch (e) {
					console.error('error creating MONGO Database path ' + TMP_PATH + path.sep + '_MONGO' + '_' + this.uuid);
				}
			}

			DB_ROOT = path.resolve(TMP_PATH + path.sep + '_MONGO_' + this.uuid);
		}
		const SYSTEM_ROOT = path.join(DATA_ROOT, '/system/');
		const VFS_CONFIG = {
			'workspace': path.join(USER_DIRECTORY, 'workspace'),
			'workspace_user': path.join(USER_DIRECTORY, 'workspace'),
			'docs': path.join(APP_ROOT, 'documentation/docFiles'),
			'system_drivers': path.join(SYSTEM_ROOT, 'drivers'),
			'user_drivers': path.join(USER_DIRECTORY, 'drivers'),
			'system_devices': path.join(SYSTEM_ROOT, 'devices'),
			'user_devices': path.join(USER_DIRECTORY, 'devices'),
			'user': path.join(USER_DIRECTORY),
			'root': options.root
		};

		let params: IObjectLiteral = {
			APP_ROOT: APP_ROOT,
			DB_ROOT: DB_ROOT,
			USER_DIRECTORY: USER_DIRECTORY,
			DATA_ROOT: DATA_ROOT,
			SYSTEM_ROOT: SYSTEM_ROOT,
			NODE_ROOT: NODE_ROOT,
			CLIENT_ROOT: CLIENT_ROOT,
			relativeVariables: {
				'XASWEB': '../Code/client/src/',
				'APP_URL': '../Code/client/src',
				'APP_URL_VE': '../',
				'RPC_URL': '../smd',
				'XCF_SYSTEM_DRIVERS': '""',
				'XCF_USER_DRIVERS': '""',
				'XCF_SYSTEM_DEVICES': '""',
				'XCF_USER_DEVICES': '""',
				'XCF_MOUNTS': '{}',
				'XCF_DRIVER_VFS_CONFIG': '{}',
				'XCF_DEVICE_VFS_CONFIG': '{}',
				'XAPP_PLUGIN_RESOURCES': '{}',
				'THEME': 'white',
				'COMPONENTS': {
					'xfile': true,
					'xnode': true,
					'xideve': { cmdOffset: '/' },
					'xblox': true,
					'x-markdown': true,
					'xtrack': true,
					'protocols': false
				},
				VFS_CONFIG: VFS_CONFIG,
				USER_DIRECTORY: USER_DIRECTORY,
				// back compat
				VFS_GET_URL: '../../files/',
				// required by export
				PLATFORM: os.platform(),
				ARCH: os.arch() === 'x64' ? EArch.x64 : EArch.x32
			},
			absoluteVariables: {
				'XASWEB': path.join(CLIENT_ROOT)
			}
		};

		this.config = params;
		this.config[EEKey.NODE_ROOT] = NODE_ROOT;
		this.profile = this._getProfile(argv.profile);
		if (argv.port) {
			this.profile.http.port = argv.port;
		}
		if (argv.host) {
			this.profile.http.host = argv.host;
		}

		if (argv.print === 'true') {
			console.log('Config', util.inspect(params));
			console.log('\n\n');
			console.log('Options', util.inspect(this.options));
		}
	}
	externalServices(): ExternalService[] {
		if (this.options.persistence === EPersistence.MONGO) {
			let searchPaths = [];
			if (this.options.type === ELayout.OFFLINE_RELEASE) {
				searchPaths.push(path.resolve(
					path.join(this.path(EEKey.APP_ROOT), 'mongo'))
				);
			}
			const mongod: Mongod = new Mongod({
				db: this.path(EEKey.DB_ROOT),
				port: this.profile.mongo.port
			}, searchPaths, this.options.print);
			return [mongod];
		} else {
			return [];
		}
	}
	vfsConfig(): IResourceDriven {
		return {
			configPath: path.join(this.path(EEKey.SYSTEM_ROOT), 'vfs.json'),
			relativeVariables: {},
			absoluteVariables: this.vfsMounts()
		};
	}
	serviceConfig(): IServiceConfiguration {
		return ServiceConfigFactory(this.vfsConfig(), this);
	}
	components(): List<Component> {
		if (!this._components) {
			const serviceConfig = this.serviceConfig();
			this._components = [new XIDEVE(this, serviceConfig) as Component];
		}
		return this._components;
	}
	rpcServices(): BaseService[] {
		if (this._rpcServices) {
			return this._rpcServices;
		}
		const serviceConfig = this.serviceConfig();
		const settingsService = this.settingsService = new JSONFileService(path.join(this.path('USER_DIRECTORY'), 'settings.json'));
		const directoryService = this.directoryService = new DirectoryService(this.vfsConfig());
		const mountService = new MountService(path.join(this.path(EEKey.DATA_ROOT), 'system/vfs.json'));
		const driverService = new DriverService(serviceConfig);
		const devicesService = new DeviceService(serviceConfig);
		const logsService = new LogsService(serviceConfig);
		const nodeService = new NodeService(path.join(this.path(EEKey.DATA_ROOT), 'system/services-debug.json'));
		nodeService.setDeviceServerPort(this.profile.socket_server.port);
		const trackingService = new TrackingService(path.join(this.path('USER_DIRECTORY'), 'meta.json'));
		const components = this.components();
		let componentServices: BaseService[] = [];
		_.each(components, component => {
			componentServices.push(...component.services(serviceConfig));
		});
		this._rpcServices = [directoryService, mountService, driverService, devicesService, logsService, nodeService, settingsService, trackingService];
		this._rpcServices = this._rpcServices.concat(componentServices);
		return this._rpcServices;
	}
	variables(ctx: Router.IRouterContext, dst: IObjectLiteral): IObjectLiteral {
		let origin = ctx.request.origin;
		dst = dst || [];
		_.each([
			EEKey.XAS_WEB,
			EEKey.APP_URL,
			EEKey.RPC_URL], key => {
				this._env(origin, key);
			}, this);

		const baseUrl: Function = this._env(origin, EEKey.XAS_WEB);
		dst[EEKey.BASE_URL] = baseUrl(origin);
		dst[EEKey.APP_URL] = this._env(origin, EEKey.APP_URL)(origin);
		dst['XASWEB'] = this._env(origin, EEKey.APP_URL)(origin);
		dst[EEKey.RPC_URL] = this._env(origin, EEKey.RPC_URL)(origin);
		dst['VFS_URL'] = origin + '/files/';
		dst[EEKey.ROOT] = origin + '/';
		const urlArgs = ctx.request.query;
		let USER_DIRECTORY: string = urlArgs['userDirectory'];
		if (USER_DIRECTORY) {
			dst[EEKey.USER_DIRECTORY] = USER_DIRECTORY;
			const VFS_CONF = this.vfsMounts();
			VFS_CONF[EEKey.USER_DRIVERS] = path.join(USER_DIRECTORY, EEKey.DRIVERS);
			VFS_CONF[EEKey.USER_DEVICES] = path.join(USER_DIRECTORY, EEKey.DEVICES);
			VFS_CONF[EEKey.WORKSPACE] = path.join(USER_DIRECTORY, EEKey.WORKSPACE);
			VFS_CONF['workspace_user'] = path.join(USER_DIRECTORY, EEKey.WORKSPACE);
			dst[EEKey.VFS_CONFIG] = VFS_CONF;
		}
		dst[EEKey.DOJOPACKAGES] = io.serialize(this.packages(origin + '/files/', baseUrl(origin)));
		dst[EEKey.RESOURCE_VARIABLES] = io.serialize(dst);
		const settingsService = this[ESKey.SettingsService];
		if (settingsService) {
			try {
				let theme = ctx.params.theme || 'white';
				const t = _.find(settingsService.get('settings', '.', null, ctx.request)['settings'], { id: 'theme' });
				if (t && t['value']) {
					theme = t['value'];
				}
				dst[EEKey.THEME] = theme;
			} catch (e) {
				console.error('Cant read settings file, user = ' + USER_DIRECTORY, e);
			}
		}
		return dst;
	}
	routes(): List<Router> {
		const filesRoute: FileRouter = createFileRoute(this.directoryService, '/files', this);
		const uploadRoute: UploadRouter = createUploadRoute(this.directoryService, '/upload', this);
		const components = this.components();
		let componentRoutes: Router[] = [];
		_.each(components, component => {
			componentRoutes.push(...component.routes());
		});
		this._routes = [uploadRoute, appRoutes, smd, filesRoute];
		this._routes.push(...componentRoutes);
		return this._routes;
	}
	setup(): void {
		super.setup();
		this.use(cors({
			origin: function (ctx) {
				return '*';
			},
			allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'xapp_debug_data', 'X-Requested-With']
		}));
		// RPC stack
		this.rpc2 = JSON_RPC_2();
		const rpcApp = new Koa();
		rpcApp.use(convert(this.rpc2.app()));
		this.use(convert(mount('/api', rpcApp)));
		// pretty index browser, must be 'used' no later than at this point

		// this.use(serveIndex(this.path(EEKey.APP_ROOT), {
		// 	icons: true,
		// 	view: 'details'
		// }));
		// RPC services
		const services: List<BaseService> = this.rpcServices();
		_.each(services, (service) => registerService(this.rpc2, service, this));

		// Generics
		this.use(convert(bodyParser({
			formLimit: null
		})));

		// Routes
		const routes: List<Router> = this.routes();
		_.each(routes, route => {
			this.use(route.routes());
			this.use(route.allowedMethods({
				throw: true,
				methodNotAllowed: () => {
					console.log('e', arguments);
				}
			}));
		});

		// Extras
		this.use(serve(this.path(EEKey.APP_ROOT)));
	}

	async stop(): Promise<any> {
		this.deviceServer.destroy();
		console.log('stop cf app');
		// @TODO: get rid of 'server-destroy'
		this.server['destroy']();
		const services = this.externalServices();
		let last: ExternalService = null;
		try {
			const res: Array<any> = [];
			for (let index = 0; index < services.length; index++) {
				let service = services[index];
				last = service;
				res.push(await service.stop());
			}
			return Promise.all(res);
		} catch (e) {
			console.error('Error stopping ' + last.label(), e);
		}
	}
	async boot(): Promise<any> {
		this._externalServices = this.externalServices();
		return await Promise.all(this._externalServices.map(async service => {
			try {
				console.info('ControlFreak#boot : run external service: ' + service.label());
				await service.run();
			} catch (e) {
				console.error('error running service ' + service.label(), e);
			}
		}));
	}

	dConfig(clientRoot: string, serverRoot: string, base: string | null, packages: string[] | null): IObjectLiteral {
		let dojoConfig = {
			libRoot: clientRoot,
			clientRoot: clientRoot,
			cwd: serverRoot,
			hasCache: {
				'host-node': 1,
				'host-browser': 0,
				'dom': 0,
				'dojo-amd-factory-scan': 0,
				'dojo-has-api': 1,
				'dojo-inject-api': 0,
				'dojo-timeout-api': 0,
				'dojo-trace-api': 1,
				'dojo-log-api': 0,
				'dojo-dom-ready-api': 0,
				'dojo-publish-privates': 1,
				'dojo-config-api': 1,
				'dojo-sniff': 1,
				'dojo-sync-loader': 0,
				'dojo-test-sniff': 0,
				'config-deferredInstrumentation': 1,
				'config-useDeferredInstrumentation': 'report-unhandled-rejections',
				'config-tlmSiblingOfDojo': 1,
				'xlog': true,
				'xblox': true,
				'dojo-undef-api': true,
				'debug': true,
				'dcl': false,
				'dojo': true
			},
			trace: 0,
			async: 0,
			baseUrl: base || serverRoot || '.',
			packages: [
				{
					name: 'dojo',
					location: 'dojo'
				},
				{
					name: 'nxappmain',
					location: serverRoot + path.sep + 'nxappmain'
				},
				{
					name: 'nxapp',
					location: serverRoot + path.sep + 'nxapp'
				},
				{
					name: 'requirejs-dplugins2',
					location: clientRoot + path.sep + 'xibm/ibm/requirejs-dplugins'
				},
				{
					name: 'xcf',
					location: clientRoot + path.sep + 'xcf'
				},
				{
					name: 'dstore',
					location: clientRoot + path.sep + 'dstore'
				},
				{
					name: 'xide',
					location: clientRoot + path.sep + 'xide'
				},
				{
					name: 'xwire',
					location: clientRoot + path.sep + 'xwire'
				},
				{
					name: 'dcl',
					location: clientRoot + path.sep + 'dcl'
				},
				{
					name: 'xblox',
					location: clientRoot + path.sep + 'xblox'
				},
				{
					name: 'xlog',
					location: clientRoot + path.sep + 'xlog'
				},
				{
					name: 'xblox',
					location: clientRoot + path.sep + 'xblox'
				},
				{
					name: 'dstore',
					location: clientRoot + path.sep + 'dstore'
				},
				{
					name: 'dijit',
					location: clientRoot + path.sep + 'dijit'
				},
				{
					name: 'xlang',
					location: clientRoot + path.sep + 'xlang'
				},
				{
					name: 'xgrid',
					location: clientRoot + path.sep + 'xgrid'
				},
				{
					name: 'xaction',
					location: clientRoot + path.sep + 'xaction/src'
				},
				{
					name: 'xdojo',
					location: clientRoot + path.sep + 'xdojo'
				}
			]
		};
		return dojoConfig;
	}
	getIps(): any[] {
		const ifaces = os.networkInterfaces();
		const ips = [];
		Object.keys(ifaces).forEach(function (ifname) {
			let alias = 0;
			ifaces[ifname].forEach(function (iface) {
				if ('IPv4' !== iface.family || iface.internal !== false) {
					// skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
					return;
				}
				if (alias >= 1) {
					// this single interface has multiple ipv4 addresses
					ips.push({
						face: ifname + alias,
						ip: iface.address
					});
				} else {
					ips.push({
						face: ifname,
						ip: iface.address
					});
				}
				++alias;
			});
		});
		return ips;
	}
	isDev() {
		return __filename.indexOf('.ts') !== -1;
	}
	ready(url?: string): void {
		if (this.options.interface === IInterface.ELECTRON) {
			let appPath = null;
			if (this.isDev()) {
				appPath = path.resolve(path.join(process.cwd(), 'build/applications/ControlFreak/desktop'));
				console.log('ControlFreak#run : start desktop application' + appPath);
				runElectron(process.cwd(), appPath, this.path(EEKey.USER_DIRECTORY));
			}
		}
		if (process.argv.join(' ').indexOf('.ts') === -1) {
			open(url, null);
		}
	}
	async run(deviceServer: boolean = true): Promise<any> {
		process.on('SIGINT', (e) => { return this.stop(); });
		process.on('SIGTERM', (e) => { return this.stop(); });
		// process.on('SIGKILL', (e) => { return this.stop(); });
		process.on('unhandledRejection', (reason) => {
			console.error('Unhandled rejection, reason: ', reason);
		});
		await this.boot();
		return new Promise((resolve, reject) => {
			this.setup();
			super.run();
			console.info('ControlFreak#run : serve www at : ' + this.path(EEKey.APP_ROOT));
			this.use(convert(serve(this.path(EEKey.APP_ROOT), { maxage: 1 })));
			const port = this.profile.http.port || this.options.port || process.env.PORT || 5555;
			let host = this.profile.http.host || this.options.host || process.env.HOST || '0.0.0.0';
			let isWin = /^win/.test(process.platform);
			const ips = this.getIps();
			if (isWin && host === '0.0.0.0') {
				// host = ips[0].ip;
			}
			console.info('ControlFreak#run : create HTTP server at ' + host + ':' + port);
			this.server.listen(port, host);
			if (!deviceServer) {
				return Promise.resolve(true);
			}

			/*
			const clientRoot:string = path.join(this.path(EEKey.CLIENT_ROOT), '/lib');
			const nodeRoot:string = this.path('NODE_ROOT');
			const dConfig = this.dConfig(clientRoot, nodeRoot,null,null);
			dojoRequire.config(dConfig);*/
			// console.log('dconfig ',dConfig);
			// return;
			const amdRequire = require(path.join(process.cwd(), !this.options.release ? '../dojo/dojo-require' : '/dojo/dojo-require'));
			const dojoRequire = amdRequire(path.join(this.path(EEKey.CLIENT_ROOT), '/lib'), this.path('NODE_ROOT'));
			const loader: string = this.options.release ? 'nxappmain/main_server_ts_build' : 'nxappmain/main_server_ts';
			console.info('ControlFreak#run : load device server application');
			console.info('ControlFreak#run : User workspace : ' + this.path(EEKey.USER_DIRECTORY));
			// as we don't really consume/mix AMD modules, we get the data over the xide/Context
			global.process.on('device-server-ready', (context) => {
				// @TODO: v1 context in v2 app?
				this.deviceServer = context;
				context.setAppServer(this);
				console.info('ControlFreak#run : device server ready');
				console.info('ControlFreak	can be accessed at http://' + host + ':' + port + '/app/xcf?userDirectory=' + encodeURIComponent(this.path(EEKey.USER_DIRECTORY)));
				if (host === '0.0.0.0') {
					ips.forEach((ip) => {
						console.info('\t Found iface ' + ip.face + ' \t with IP = ' + ip.ip);
					});
				}
				resolve(context);
				if (isWin && host === '0.0.0.0') {
					host = ips[0].ip;
				}
				this.ready('http://' + host + ':' +
					port + '/app/xcf?userDirectory=' +
					encodeURIComponent(this.path(EEKey.USER_DIRECTORY)));
			});
			try {
				dojoRequire([loader], (_module) => { });
			} catch (e) {
				const message: String = 'Error in nxappmain/' + loader;
				reject(message);
			}
		});
	}
	_package(name: string, prefix: string, suffix: string = '') {
		// @TODO: check TS str interpolators
		let loc: string = (prefix || name);
		!~loc.endsWith('/') && (loc = loc + '/');
		!~suffix.endsWith('/') && (suffix = suffix + '/');
		return {
			name: name,
			location: loc + suffix
		};
	}

	packages(offset: string = '', baseUrl?: string): List<IObjectLiteral> {
		baseUrl = baseUrl || '';
		const result = [
			this._package(EEKey.SYSTEM_DRIVERS, offset, EEKey.SYSTEM_DRIVERS),
			this._package(EEKey.USER_DRIVERS, offset, EEKey.USER_DRIVERS),
			this._package(EEKey.WORKSPACE, offset, EEKey.WORKSPACE),
			this._package('maq-metadata-dojo', baseUrl, '/xideve/metadata/dojo/1.8/'),
			this._package('maq-metadata-html', baseUrl, '/xideve/metadata/html/0.8/'),
			this._package('maq-metadata-delite', baseUrl, '/xideve/metadata/delite/0.8/')
		];
		return result;
	}
}
