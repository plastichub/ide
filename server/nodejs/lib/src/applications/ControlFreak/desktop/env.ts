// Simple module exposes environment variables to rest of the code.

const jetpack = require('fs-jetpack');
let app;
if (process['type'] === 'renderer') {
	app = require('electron').remote.app;
} else {
	app = require('electron').app;
}
let appDir = jetpack.cwd(app.getAppPath());


let manifest = appDir.read('package.json', 'json');

export default manifest.env;
