(function () {
	'use strict';
	let remote = require('electron').remote;
	let Menu = remote.Menu;
	let MenuItem = remote.MenuItem;

	let cut = new MenuItem({
		label: "Cut",
		click: function () {
			document.execCommand("cut");
		}
	});

	let copy = new MenuItem({
		label: "Copy",
		click: function () {
			document.execCommand("copy");
		}
	});

	let paste = new MenuItem({
		label: "Paste",
		click: function () {
			document.execCommand("paste");
		}
	});

	let textMenu = new Menu();
	textMenu.append(cut);
	textMenu.append(copy);
	textMenu.append(paste);

	document.addEventListener('contextmenu', function (e: any) {
		switch (e.target.nodeName) {
			case 'TEXTAREA':
			case 'INPUT':
				e.preventDefault();
				textMenu.popup(remote.getCurrentWindow());
				break;
		}

	}, false);

}());
