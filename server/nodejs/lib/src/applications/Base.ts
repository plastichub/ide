import { IObjectLiteral, List } from '../interfaces/index';
import { EPersistence } from '../interfaces/Application';
import { BaseService } from '../services/Base';
import { ExternalService } from '../services/External';
import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as _ from 'lodash';
const http = require('http');
const compress = require('koa-compress');
const SRC_DIR = '/Code/client/src/';
const LIB_DIR = SRC_DIR + 'lib/';
const destroyable = require('server-destroy');
const io = {
	serialize: JSON.stringify
};

export enum IInterface {
	CLI = <any> 'cli',
	ELECTRON = <any> 'electron'
}

export const ENV = {
	VARIABLES: {
		SRC_DIR: SRC_DIR,
		LIB_DIR: LIB_DIR,
		'XAS_WEB': function (origin) {
			return origin + LIB_DIR;
		},
		'APP_URL': function (origin) {
			return origin + SRC_DIR;
		},
		'RPC_URL': function (origin) {
			return origin + '/smd';
		},
		APP_ROOT: function (app: ApplicationBase) {
			return app.config['APP_ROOT'];
		},
		CLIENT_ROOT: function (app: ApplicationBase) {
			return app.config['CLIENT_ROOT'];
		},
		RELEASE: function (app: ApplicationBase) {
			return app.config['RELEASE'];
		}
	}
};
export const EEKey = {
	'XAS_WEB': 'XAS_WEB',
	'APP_URL': 'APP_URL',
	'RPC_URL': 'RPC_URL',
	'RESOURCE_VARIABLES': 'RESOURCE_VARIABLES',
	'DOJOPACKAGES': 'DOJOPACKAGES',
	'THEME': 'THEME',
	'APP_ROOT': 'APP_ROOT',
	'DATA_ROOT': 'DATA_ROOT',
	'DB_ROOT': 'DB_ROOT',
	'BASE_URL': 'BASE_URL',
	'CLIENT_ROOT': 'CLIENT_ROOT',
	'LIB_DIR': ENV.VARIABLES.LIB_DIR,
	'RELEASE': 'RELEASE',
	'NODE_ROOT': 'NODE_ROOT',
	'ROOT': 'ROOT',
	'VFS_URL': 'VFS_URL',
	'VFS_CONFIG': 'VFS_CONFIG',
	'SYSTEM_ROOT': 'SYSTEM_ROOT',
	'USER_DIRECTORY': 'USER_DIRECTORY',
	'DEVICES': 'devices',
	'DRIVERS': 'drivers',
	'WORKSPACE': 'workspace',
	'USER_DEVICES': 'user_devices',
	'USER_DRIVERS': 'user_drivers',
	'SYSTEM_DRIVERS': 'system_drivers',
	'SYSTEM_DEVICES': 'SYSTEM_DEVICES'
};

export enum ELayout {
	'NODE_JS' = <any> 'NODE_JS',
	'SOURCE' = <any> 'SOURCE',
	'OFFLINE_RELEASE' = <any> 'OFFLINE_RELEASE'
}

export const ESKey = {
	'SettingsService': 'settingsService'
};

export function ENV_VAR(key: string) {
	return ENV.VARIABLES[key];
}
export interface IApplication {
	rpcServices(): List<BaseService>;
	externalServices(): List<ExternalService>;
	setup(): void;
	run(): void;
	config: IObjectLiteral;
	uuid: string;
}
export interface Options {
	host?: string;
	port?: number;
	root: string;
	release: boolean;
	clientRoot?: string;
	type?: ELayout;
	nodeRoot?: string;
	print: boolean;
	uuid: string;
	user: string;
	persistence: EPersistence;
	interface?: any;
	home?: boolean;
}
type StringOrFunction = string & Function;

export class ApplicationBase extends Koa implements IApplication {
	readonly config: IObjectLiteral;
	uuid = 'no uuid';
	server: any;
	externalServices(): List<ExternalService> {
		return [];
	}

	public path(key: string): string {
		return this.config[key];
	}

	public relativeVariable(key: string, value?: string) {
		if (value != null) {
			this.config['relativeVariables'][key] = value;
		}
		return this.config['relativeVariables'][key];
	}

	public vfsMounts(): IObjectLiteral {
		return this.relativeVariable('VFS_CONFIG');
	}

	public _env(origin: string, key: string): StringOrFunction {
		switch (key) {
			case EEKey.APP_ROOT: {
				return this.config[EEKey.APP_ROOT];
			}
			case EEKey.CLIENT_ROOT: {
				return this.config[EEKey.CLIENT_ROOT];
			}
		}
		return ENV.VARIABLES[key];
	}

	variables(ctx: Router.IRouterContext, dst: IObjectLiteral): IObjectLiteral {
		let origin = ctx.request.origin;
		dst = dst || [];
		_.each([
			EEKey.XAS_WEB,
			EEKey.APP_URL,
			EEKey.RPC_URL], key => {
				this._env(origin, key);
			}, this);

		const baseUrl: Function = this._env(origin, EEKey.XAS_WEB);
		dst['BASE_URL'] = baseUrl(origin);
		dst['APP_URL'] = this._env(origin, EEKey.APP_URL)(origin);
		dst[EEKey.XAS_WEB] = this._env(origin, EEKey.APP_URL)(origin);
		dst['RPC_URL'] = this._env(origin, EEKey.RPC_URL)(origin);
		dst['VFS_URL'] = origin + '/files/';
		dst[EEKey.ROOT] = origin + '/';
		dst[EEKey.DOJOPACKAGES] = io.serialize(this.packages(origin + '/files/', baseUrl(origin)));
		dst[EEKey.RESOURCE_VARIABLES] = io.serialize(dst);
		const settingsService = this[ESKey.SettingsService];
		if (settingsService) {
			try {
				const theme = _.find(settingsService.get('settings', '.')['settings'], { id: 'theme' })['value'] || ctx.params.theme || 'white';
				dst[EEKey.THEME] = theme;
			} catch (e) {
				console.error('error reading user settings file');
				dst[EEKey.THEME] = 'white';
			}
		}
		return dst;
	}

	constructor(root: string) {
		super();
	}

	rpcServices(): List<BaseService> {
		return [];
	}

	async boot(): Promise<any> {
		return Promise.resolve(null);
	}

	run(): void {
		const koaHandler = this.callback();
		this.server = http.createServer(koaHandler);
		this.use(async (ctx, next) => {
			ctx.req.socket.setNoDelay(true);
			await next();
		});
		destroyable(this.server);
	}

	setup(): void {
		this.use(compress({
			filter: function (type) {
				return /text/i.test(type) || /json/i.test(type) || /javascript/i.test(type);
			},
			threshold: 2048,
			flush: require('zlib').Z_SYNC_FLUSH
		}));
	}

	packages(offset: string, baseUrl?: string): List<IObjectLiteral> {
		return [];
	}
}
