import * as _ from 'lodash';
import { ApplicationBase, IApplication, Options, ELayout, EEKey } from './../Base';
import { IObjectLiteral } from '../../interfaces/index';
import { IResourceDriven } from '../../interfaces/Resource';
import { create as ServiceConfigFactory, IServiceConfiguration } from '../../interfaces/Service';
import appRoutes from './route/app';
import smd from '../../route/smd';
import { create as createFileRoute, FileRouter } from '../../route/files';
import { create as createUploadRoute, UploadRouter } from '../../route/uploads';
import { JSON_RPC_2 } from '../../rpc/JSON-RPC-2';
import { DirectoryService } from '../../services/Directory';
import { JSONFileService } from '../../services/JSONFile';
import { MountService } from '../../services/Mounts';
import { registerService } from '../../services/register';
import * as AnyPromise from 'any-promise';
import * as Koa from 'koa';
import * as convert from 'koa-convert';
import * as bodyParser from 'koa-bodyparser';
import * as serve from 'koa-static';
import * as path from 'path';
import * as Router from 'koa-router';
import { IDefaultBackend } from '../../acl/interfaces';
import { ACL } from '../../acl/ACLC';
import { File as FileBackend } from '../../acl/data/File';
const mount = require('koa-mount');
const serveStatic = require('koa-static');

import { List } from '../../interfaces/index';
import { BaseService } from '../../services/Base';
import * as yargs from 'yargs-parser';
import { serveIndex } from '../../server/index';
let argv = yargs(process.argv.slice(2));
// tslint:disable-next-line:interface-name
// tslint:disable-next-line:class-name
export class xbox extends ApplicationBase implements IApplication {
	setupAcl2() {

		const backend = new FileBackend() as IDefaultBackend;
		let acl = new ACL(backend, null, {});

		acl.allow('guest', 'blogs', 'view');
		// allow function accepts arrays as any parameter
		acl.allow('member', 'blogs', ['edit', 'view']);

		acl.addUserRoles('joed', 'member');

		acl.isAllowed('joed', 'blogs', 'view', function (err, res) {
			if (res) {
				console.log("User joed is allowed to view blogs");
			} else {
				console.log("User joed is not allowed to view blogs");
			}
		});

		acl.isAllowed('joed', 'blogs', 'delete', function (err, res) {
			if (res) {
				console.log("User joed is allowed to delete blogs");
			} else {
				console.log("User joed is not allowed to delete blogs");
			}
		});
		// const file = path.join(this._env(null, EEKey.APP_ROOT), '/acl.json');
		// (backend as FileBackend).write(file);

	}
	config: IObjectLiteral;
	root: string;
	rpc: any;
	_services: List<BaseService>;
	_routes: List<Router>;
	directoryService: DirectoryService;
	settingsService: JSONFileService;
	options: Options;
	constructor(options: Options) {
		super(options.root);
		this.options = options;
		this.root = options.root;
		const APP_ROOT = this.root;
		let CLIENT_ROOT = options.clientRoot || path.join(APP_ROOT, 'Code/client/src/');
		const NODE_ROOT = options.release === true ? process.cwd() : path.join(APP_ROOT, 'server/nodejs/');
		const DATA_ROOT = path.join(APP_ROOT, '/data/');
		const SYSTEM_ROOT = path.join(DATA_ROOT, '/system/');
		const USER_DIRECTORY = path.join(APP_ROOT, '/user');

		const VFS_CONFIG = {
			'docs': path.join(APP_ROOT, 'documentation/docFiles'),
			'root': argv.fileRoot ? path.resolve(argv.fileRoot) : APP_ROOT
		};


		const isNodeJSType = options.type === ELayout.NODE_JS;
		if (isNodeJSType) {
			CLIENT_ROOT = path.join(NODE_ROOT, '/node_modules/xjs-dist/src/');
		}

		let params: IObjectLiteral = {
			APP_ROOT: APP_ROOT,
			CLIENT_ROOT: CLIENT_ROOT,
			DATA_ROOT: DATA_ROOT,
			SYSTEM_ROOT: SYSTEM_ROOT,
			USER_DIRECTORY: USER_DIRECTORY,
			RELEASE: options.release,
			relativeVariables: {
				'XASWEB': isNodeJSType ? '/node_modules/xjs-dist/src/' : '../Code/client/src/',
				'APP_URL': isNodeJSType ? '/node_modules/xjs-dist/src/' : '../Code/client/src/',
				'APP_URL_VE': '../',
				'RPC_URL': '../smd',
				'XAPP_PLUGIN_RESOURCES': '{}',
				'THEME': 'white',
				"COMPONENTS": {
					"xfile": true,
					"xnode": false,
					"xideve": false,
					"xblox": false,
					"x-markdown": false,
					"xtrack": false,
					"protocols": false
				},
				VFS_CONFIG: VFS_CONFIG,
				VFS_GET_URL: '../../files/'
			},
			absoluteVariables: {
				'XASWEB': path.join(CLIENT_ROOT)
			}
		};
		this.config = params;
		this.config['NODE_ROOT'] = NODE_ROOT;
	}
	vfsConfig(): IResourceDriven {
		return {
			configPath: path.join(this.path(EEKey.SYSTEM_ROOT), 'vfs_xbox.json'),
			relativeVariables: {},
			absoluteVariables: this.vfsMounts()
		};
	}

	serviceConfig(): IServiceConfiguration {
		return ServiceConfigFactory(this.vfsConfig(), this);
	}

	rpcServices(): List<BaseService> {
		if (this._services) {
			return this._services;
		}
		const settingsService = this.settingsService = new JSONFileService(path.join(this.path('USER_DIRECTORY'), 'settings.json'));
		const directoryService = this.directoryService = new DirectoryService(this.vfsConfig());
		const mountService = new MountService(path.join(this.path(EEKey.DATA_ROOT), 'system/vfs_xbox.json'));
		this._services = [directoryService, mountService, settingsService];
		return this._services;
	}

	routes(): List<Router> {
		if (this._routes) {
			return this._routes;
		}
		const filesRoute: FileRouter = createFileRoute(this.directoryService, '/files', this);
		const uploadRoute: UploadRouter = createUploadRoute(this.directoryService, '/upload', this);
		this._routes = [filesRoute, appRoutes, smd, uploadRoute];
		return this._routes;
	}
	setup(): void {
		super.setup();
		// RPC stack
		this.rpc = JSON_RPC_2();
		const rpcApp = new Koa();
		rpcApp.use(convert(this.rpc.app()));
		this.use(convert(mount('/api', rpcApp)));
		//this.setupAcl2();

		// pretty index browser, must be 'used' no later than at this point
		this.use(serveIndex(this.path(EEKey.APP_ROOT), {
			icons: true,
			view: 'details'
		}));

		// RPC services
		const services: List<BaseService> = this.rpcServices();
		_.each(services, service => {
			registerService(this.rpc, service, this);
		});

		// Generics
		this.use(convert(bodyParser({
			formLimit: null
		})));

		// Routes
		const routes: List<Router> = this.routes();
		_.each(routes, route => {
			this.use(route.routes());
			this.use(route.allowedMethods());
		});
		const root = this.options.release ? this.config['NODE_ROOT'] : this.path('APP_ROOT');
		// Extras
		// this.use(convert(serveIndex(root)));
		this.use(serveStatic(root));
	}


	async run(): Promise<any> {
		return new AnyPromise((resolve, reject) => {
			try {
				this.boot();
				this.setup();
				super.run();
				this.use(convert(serve(this.path('APP_ROOT'))));
				this.listen(this.options.port || 5555, '0.0.0.0');
				resolve(true);
			} catch (e) {
				console.error('error', e);
			}
		});
	}
	packages(offset: string = '../../../../../'): List<IObjectLiteral> {
		return [];
	}
}
