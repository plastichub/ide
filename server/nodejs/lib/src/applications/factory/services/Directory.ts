import { DirectoryService as Base } from '../../../services/Directory';
import * as path from 'path';
export class DirectoryService extends Base {
	_userDir(userRoot: string, what: string) {
		return path.resolve(path.join(userRoot + path.sep + what));
	}
	_resolveUserMount(mount: string, request: any, _default?: string): string {
		const user = this._getUser(request);
		if (user) {
			switch (mount) {
				case 'workspace_user': {
					return this._userDir(user, 'workspace');
				}
				case 'user_drivers': {
					return this._userDir(user, 'drivers');
				}
				case 'user': {
					return this._userDir(user, '');
				}
			}
		}
		return _default;
	}
}
