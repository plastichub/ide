var UPnPClient;
var log = console;

function discoverUpnp(options, log, progressCallback, callback) {
    options.upnpTimeout = parseInt(options.upnpTimeout, 10) || 10000;

    UPnPClient = UPnPClient || require('node-ssdp').Client;
    var client = new UPnPClient();

    var result = [];

    var timer = setTimeout(function () {
        timer = null;
        client.stop();
        client = null;
        if (typeof callback === 'function') callback(null, result);
    }, options.upnpTimeout);

    client.on('response', function (headers, statusCode, rinfo) {
        console.log('response',arguments);
        result.push({ _data: rinfo }); //{_addr: , _name: ,...}
        if (isStopping) {
            timer = null;
            client.stop();
            client = null;
            if (typeof callback === 'function') callback(null, result, 'upnp');
        }
    });
    client.on('error', function (err) {
        if (timer) {
            clearTimeout(timer);
            timer = null;
        }
        if (typeof callback === 'function') callback(err, result, 'upnp');
    });

    // get a list of all services on the network
    console.info('Discover UPnP devices...');
    client.search('ssdp:all');
}

discoverUpnp({}, null, function () {
    console.log('e', arguments);
}, function () {
    console.log('d', arguments);
})