import 'reflect-metadata';
import { INameToValueMap } from '../interfaces/index';
import * as _ from 'lodash';

export function CustomComponent(annotation: any) {
	return function (target: Function) {
		var parentTarget = Object.getPrototypeOf(target.prototype).constructor;
		var parentAnnotations = Reflect.getMetadata('annotations', parentTarget);

		var parentAnnotation = parentAnnotations[0];
		Object.keys(parentAnnotation).forEach(key => {
			// if (isPresent(parentAnnotation[key])) {
			// 	annotation[key] = parentAnnotation[key];
			// }
		});

		// var metadata = new ComponentMetadata(annotation);
		// Reflect.defineMetadata('annotations', [metadata], target);
	};
}

function CustomDecorator(data: any) {
	return function (target: Function) {
		var parentTarget = Object.getPrototypeOf(target.prototype).constructor;
		console.log('#', parentTarget);
		var parentAnnotations = Reflect.getMetadata('annotations', parentTarget);
		console.log('parent annotation ', parentAnnotations);
		const desc = Object.getOwnPropertyDescriptor(target, "getRpcMethods");
		console.log('desc', desc);
		// console.log(parentTarget === AbstractClass); // true :)
	};
}


export const RpcMethod = (target: Object, propName: string, propertyDescriptor: any) => {
	// let parentTarget = Object.getPrototypeOf(new (target as any));
	// var parentTarget = Object.getPrototypeOf(target['prototype']).constructor;
	// console.log('parent target ', parentTarget);
	// var parentAnnotations = Reflect.getMetadata('annotations', parentTarget);
	// console.log('parent annotation ', parentAnnotations);

	const desc = Object.getOwnPropertyDescriptor(target, "getRpcMethods");
	if (desc && desc.configurable) {
		Object.defineProperty(target, "getRpcMethods", { value: function () { return this["rpcMethods"]; }, configurable: false });
		Object.defineProperty(target, "rpcMethods", { value: [] });
	}

	target && target["rpcMethods"] && target["rpcMethods"].push(propName);

};

interface IService {
	method: string;
	methods(): any;
}
export class BaseClass implements IService {
	method = "no_method";

	public getRpcMethods(): string[] {
		throw new Error("Should be implemented by decorator");
	}
	public toMethods(methods: Array<string>): any {
		const self = this;
		const result: INameToValueMap = {};
		_.each(methods, function (method: string) {
			result[method] = (<any>self)[method];
		});
		return result;
	}
	methods() {
		const methods = this.getRpcMethods();
		return this.toMethods(methods);
	}
}
