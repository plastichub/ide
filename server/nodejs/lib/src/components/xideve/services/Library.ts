import * as dotProp from 'dot-prop';
import { IObjectLiteral } from '../../../interfaces/index';
import { JSONFileService } from '../../../services/JSONFile';
const defaultFileName = 'styles.json';
/**
 * This service sets/gets data in a json file, utilizing 'dot-prop' to select certain data in the object.
 *
 * @export
 * @class JSONFileService
 * @extends {BaseService}
 * @implements {IStoreIO}
 * @implements {IStoreAccess}
 */
export class LibraryService extends JSONFileService {
	public method = 'Library_Store';
	public root: string;
	public defaultFileName: string = defaultFileName;

	public defaultData: any = {
		user: {
			styles: [
				{
					"id": "white",
					"value": {
						"type": "xblox/StyleState",
						"properties": {
							"style": "background-color: white; background-image: none;"
						}
					}
				}
			]
		}
	};

	constructor(config: string) {
		super(config);
		this.configPath = config;
		this.root = 'user';
		this.defaultData = {
			user: {
				styles: [
					{
						"id": "white",
						"value": {
							"type": "xblox/StyleState",
							"properties": {
								"style": "background-color: white; background-image: none;"
							}
						}
					}
				]
			}
		};
	}
	getDefaults() {
		return this.defaultData;
	}

	public _get(section: string, path: string, query?: any): IObjectLiteral {
		let configPath = this._getConfigPath(arguments);
		let data = this.readConfig(configPath);
		let result: IObjectLiteral = {};
		result[section] = dotProp.get(data, this.root + path + section);
		return result;
	}

	//
	// ─── DECORATORS
	//
	public getRpcMethods(): string[] {
		throw new Error("Should be implemented by decorator");
	}
	methods() {
		return this.toMethods(['get', 'set', 'update', 'remove']);
	}
}
