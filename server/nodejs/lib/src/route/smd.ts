import * as Router from 'koa-router';
import * as services from './services';
const smd = new Router({ prefix: '/smd' });
smd.get('/', async (ctx: Router.IRouterContext) => {
	ctx.body = services.default(ctx.origin + '/api');
});
export default smd;
