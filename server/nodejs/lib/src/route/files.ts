import { ApplicationBase } from '../applications/Base';
import { DirectoryService } from '../services/Directory';
import * as Router from 'koa-router';
import * as send from 'koa-send';
import { EResourceType } from '../interfaces/Resource';
export class FileRouter extends Router {
	constructor(args: any) {
		super(args);
	}
	public directoryService: DirectoryService;
}
export function create(directoryService: DirectoryService, prefix: string = '/files', app: ApplicationBase): FileRouter {
	const filesRouter = new FileRouter({ prefix: prefix });
	filesRouter.directoryService = directoryService;
	filesRouter.get('/:prefix/*', async (ctx, next) => {
		if (!ctx.req.url.startsWith(prefix)) {
			return next();
		}
		const filePath = ctx.params['0'];
		const mount = ctx.params.prefix;
		if (filePath) {
			const resource = directoryService.getResourceByTypeAndName(EResourceType.FILE_PROXY, mount);
			if (resource['vfs']) {
				let data = await directoryService.get(mount + '://' + filePath, false, false, false, ctx.request);
				ctx.body = data;
				return filesRouter;

			}
			await send(ctx, filePath, {
				root: filesRouter.directoryService.resolve(ctx.params.prefix, '', ctx.request)
			});
		} else {
			ctx.body = '';
		}
	});
	return filesRouter;
}
