import { Registry } from 'winreg';
export function get(hive: string, where: string, what: string) {
	const key = new Registry({
		hive: hive,
		key: where
	});
	return new Promise((resolve, reject) => {
		key.get(what, function (err, item) {
			if (err) {
				reject(err);
			}

			if (item) {
				resolve(item.value);
			}
		});
	});
}
