/*define([
	'dojo/_base/lang',
	'dojo/_base/array',
	'dojo/aspect',
	'dojo/has',
	'dojo/when',
	'dojo/Deferred',
	'dojo/_base/declare',
	'./QueryMethod',
	'./Filter',
	'dojo/Evented'
], function (lang, arrayUtil, aspect, has, when, Deferred, declare, QueryMethod, Filter, Evented) {
	*/

// module:
// 		dstore/Store
/* jshint proto: true */
// detect __proto__, and avoid using it on Firefox, as they warn about
// deoptimizations. The watch method is a clear indicator of the Firefox
// JS engine.
//has.add('object-proto', !!{}.__proto__ && !({}).watch);

let hasProto = false;//has('object-proto');

function emitUpdateEvent(type) {
	return function (result, args) {
		let self = this;
		when(result, function (result) {
			let event = { target: result },
				options = args[1] || {};

			if ('beforeId' in options) {
				event.beforeId = options.beforeId;
			}
			self.emit(type, event);
		});

		return result;
	};
}

let base = Evented;
/*=====
base = [ Evented, Collection ];
=====*/

export class Store {
	constructor(options) {
		// perform the mixin
		//options && declare.safeMixin(this, options);

		//if (this.Model && this.Model.createSubclass) {
			// we need a distinct model for each store, so we can
			// save the reference back to this store on it.
			// we always create a new model to be safe.
		//	this.Model = this.Model.createSubclass([]).extend({
				// give a reference back to the store for saving, etc.
		//		_store: this
		//	});
		//}

		// the object the store can use for holding any local data or events
		this.storage = new Evented();
		let store = this;
		if (this.autoEmitEvents) {
			// emit events when modification operations are called
			aspect.after(this, 'add', emitUpdateEvent('add'));
			aspect.after(this, 'put', emitUpdateEvent('update'));
			aspect.after(this, 'remove', function (result, args) {
				when(result, function () {
					store.emit('delete', { id: args[0] });
				});
				return result;
			});
		}
	},

	// autoEmitEvents: Boolean
	// 		Indicates if the events should automatically be fired for put, add, remove
	// 		method calls. Stores may wish to explicitly fire events, to control when
	// 		and which event is fired.
	autoEmitEvents: true,

	// idProperty: String
	// 		Indicates the property to use as the identity property. The values of this
	// 		property should be unique.
	idProperty: 'id',

	// queryAccessors: Boolean
	// 		Indicates if client-side query engine filtering should (if the store property is true)
	// 		access object properties through the get() function (enabling querying by
	// 		computed properties), or if it should (by setting this to false) use direct/raw
	// 		property access (which may more closely follow database querying style).
	queryAccessors: true,

	getIdentity: function (object) {
		// summary:
		// 		Returns an object's identity
		// object: Object
		// 		The object to get the identity from
		// returns: String|Number

		return object.get ? object.get(this.idProperty) : object[this.idProperty];
	},

	_setIdentity: function (object, identityArg) {
		// summary:
		// 		Sets an object's identity
		// description:
		// 		This method sets an object's identity and is useful to override to support
		// 		multi-key identities and object's whose properties are not stored directly on the object.
		// object: Object
		// 		The target object
		// identityArg:
		// 		The argument used to set the identity

		if (object.set) {
			object.set(this.idProperty, identityArg);
		} else {
			object[this.idProperty] = identityArg;
		}
	},

	forEach: function (callback, thisObject) {
		let collection = this;
		return when(this.fetch(), function (data) {
			for (let i = 0, item; (item = data[i]) !== undefined; i++) {
				callback.call(thisObject, item, i, collection);
			}
			return data;
		});
	},
	on: function (type, listener) {
		return this.storage.on(type, listener);
	},
	emit: function (type, event) {
		event = event || {};
		event.type = type;
		try {
			return this.storage.emit(type, event);
		} finally {
			// Return the initial value of event.cancelable because a listener error makes it impossible
			// to know whether the event was actually canceled
			return event.cancelable;
		}
	},

	// parse: Function
	// 		One can provide a parsing function that will permit the parsing of the data. By
	// 		default we assume the provide data is a simple JavaScript array that requires
	// 		no parsing (subclass stores may provide their own default parse function)
	parse: null,

	// stringify: Function
	// 		For stores that serialize data (to send to a server, for example) the stringify
	// 		function can be specified to control how objects are serialized to strings
	stringify: null,

	// Model: Function
	// 		This should be a entity (like a class/constructor) with a 'prototype' property that will be
	// 		used as the prototype for all objects returned from this store. One can set
	// 		this to the Model from dmodel/Model to return Model objects, or leave this
	// 		to null if you don't want any methods to decorate the returned
	// 		objects (this can improve performance by avoiding prototype setting),
	Model: null,

	_restore: function (object, mutateAllowed) {
		// summary:
		// 		Restores a plain raw object, making an instance of the store's model.
		// 		This is called when an object had been persisted into the underlying
		// 		medium, and is now being restored. Typically restored objects will come
		// 		through a phase of deserialization (through JSON.parse, DB retrieval, etc.)
		// 		in which their __proto__ will be set to Object.prototype. To provide
		// 		data model support, the returned object needs to be an instance of the model.
		// 		This can be accomplished by setting __proto__ to the model's prototype
		// 		or by creating a new instance of the model, and copying the properties to it.
		// 		Also, model's can provide their own restore method that will allow for
		// 		custom model-defined behavior. However, one should be aware that copying
		// 		properties is a slower operation than prototype assignment.
		// 		The restore process is designed to be distinct from the create process
		// 		so their is a clear delineation between new objects and restored objects.
		// object: Object
		// 		The raw object with the properties that need to be defined on the new
		// 		model instance
		// mutateAllowed: boolean
		// 		This indicates if restore is allowed to mutate the original object
		// 		(by setting its __proto__). If this isn't true, than the restore should
		// 		copy the object to a new object with the correct type.
		// returns: Object
		// 		An instance of the store model, with all the properties that were defined
		// 		on object. This may or may not be the same object that was passed in.
		let Model = this.Model;
		if (Model && object) {
			let prototype = Model.prototype;
			let restore = prototype._restore;
			if (restore) {
				// the prototype provides its own restore method
				object = restore.call(object, Model, mutateAllowed);
			} else if (hasProto && mutateAllowed) {
				// the fast easy way
				// http://jsperf.com/setting-the-prototype
				object.__proto__ = prototype;
			} else {
				// create a new object with the correct prototype
				object = lang.delegate(prototype, object);
			}
		}
		return object;
	},

	create: function (properties) {
		// summary:
		// 		This creates a new instance from the store's model.
		// 	properties:
		// 		The properties that are passed to the model constructor to
		// 		be copied onto the new instance. Note, that should only be called
		// 		when new objects are being created, not when existing objects
		// 		are being restored from storage.
		return new this.Model(properties);
	},

	_createSubCollection: function (kwArgs) {
		let newCollection = lang.delegate(this.constructor.prototype);

		for (let i in this) {
			if (this._includePropertyInSubCollection(i, newCollection)) {
				newCollection[i] = this[i];
			}
		}

		return declare.safeMixin(newCollection, kwArgs);
	},

	_includePropertyInSubCollection: function (name, subCollection) {
		return !(name in subCollection) || subCollection[name] !== this[name];
	},

	// queryLog: __QueryLogEntry[]
	// 		The query operations represented by this collection
	queryLog: [],	// NOTE: It's ok to define this on the prototype because the array instance is never modified

	filter: new QueryMethod({
		type: 'filter',
		normalizeArguments: function (filter) {
			let Filter = this.Filter;
			if (filter instanceof Filter) {
				return [filter];
			}
			return [new Filter(filter)];
		}
	}),

	Filter: Filter,

	sort: new QueryMethod({
		type: 'sort',
		normalizeArguments: function (property, descending) {
			let sorted;
			if (typeof property === 'function') {
				sorted = [property];
			} else {
				if (property instanceof Array) {
					sorted = property.slice();
				} else if (typeof property === 'object') {
					sorted = [].slice.call(arguments);
				} else {
					sorted = [{ property: property, descending: descending }];
				}

				sorted = arrayUtil.map(sorted, function (sort) {
					// copy the sort object to avoid mutating the original arguments
					sort = lang.mixin({}, sort);
					sort.descending = !!sort.descending;
					return sort;
				});
				// wrap in array because sort objects are a single array argument
				sorted = [sorted];
			}
			return sorted;
		}
	}),

	select: new QueryMethod({
		type: 'select'
	}),

	_getQuerierFactory: function (type) {
		let uppercaseType = type[0].toUpperCase() + type.substr(1);
		return this['_create' + uppercaseType + 'Querier'];
	}
});
