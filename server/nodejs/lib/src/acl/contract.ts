/*
  Design by Contract module (c) OptimalBits 2011.
  Roadmap:
    - Optional parameters. ['(string)', 'array']
    - Variable number of parameters.['number','...']

  api?:

  contract(arguments)
    .params('string', 'array', '...')
    .params('number')
    .end()

*/
let noop: any = {};
import * as _ from 'lodash';
// import { IContract } from './interfaces';

noop.params = function () {
	return this;
};
noop.end = function () { };

export const contract: any = function (args) {
	if (contract.debug === true) {
		contract.fulfilled = false;
		contract.args = _.toArray(args);
		contract.checkedParams = [];
		return contract;
	} else {
		return noop;
	}
};
export function checkParams(args: Array<String>, contract: any): any {
	let fulfilled, types, type, i, j;
	if (args.length !== contract.length) {
		return false;
	} else {
		for (i = 0; i < args.length; i++) {
			try {
				types = contract[i].split('|');
			} catch (e) {
				console.log(e, args);
			}

			type = typeOf(args[i]);
			fulfilled = false;
			for (j = 0; j < types.length; j++) {
				if (type === types[j]) {
					fulfilled = true;
					break;
				}
			}
			if (fulfilled === false) {
				return false;
			}
		}
		return true;
	}
};

contract.params = function () {
	this.fulfilled |= checkParams(this.args, _.toArray(arguments));
	if (this.fulfilled) {
		return noop;
	} else {
		this.checkedParams.push(arguments);
		return this;
	}
};
contract.end = function () {
	if (!this.fulfilled) {
		printParamsError(this.args, this.checkedParams);
		throw new Error('Broke parameter contract');
	}
};

let typeOf = function (obj) {
	return Array.isArray(obj) ? 'array' : typeof obj;
};

export function printParamsError(args, checkedParams) {
	let msg = 'Parameter mismatch.\nInput:\n( ',
		type,
		input,
		i;

	_.each(args, function (input, key: string | number) {
		type = typeOf(input);
		if (key !== 0) {
			msg += ', ';
		}
		msg += input + ': ' + type;
	});

	msg += ')\nAccepted:\n';

	for (i = 0; i < checkedParams.length; i++) {
		msg += '(' + argsToString(checkedParams[i]) + ')\n';
	}

	console.log(msg);
};
export function argsToString(args) {
	let res = "";
	_.each(args, function (arg, key: string | number) {
		if (key !== 0) {
			res += ', ';
		}
		res += arg;
	});
	return res;
}
// exports = module.exports = contract;
