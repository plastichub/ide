define("xfile/views/UploadMixin", [
    'xdojo/declare',
    'xide/utils',
    'xide/types',
    "dojo/promise/all"
], function (declare, utils, types, all) {

    return declare("xfiles.views.UploadMixin", null, {
        cssClass: 'uploadFilesList droparea dropareaHover',
        __currentDragOverRow: null,
        __currentDragOverTimer: null,
        __currentDragOverTimeRow: null,
        buildRendering: function () {
            this.inherited(arguments);
            $(this.domNode).addClass(this.cssClass);
        },
        _createUploadItemTemplate: function (name, progress, obj) {
            var _textClass = obj.failed ? 'text-danger' : 'text-info';
            return '<div class="" style="position: relative;margin: 0.1em;height: auto">' +

                '<div style="margin:0;vertical-align: middle;padding: 1px;" class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">' +
                '<div class="progress-bar progress-bar-info" style="width:' + progress + ';background-color: rgba(102, 161, 179, 0.35);height:auto;magin:0;">' +
                '<span class="' + _textClass + '" style="left: 0px">' + name + '</span>' +
                '</div>' +
                '</div>' +
                '</div>';
        },
        _createUploadItems: function (files) {

            var thiz = this;
            var store = this.collection,
                selectedRenderer = thiz.selectedRenderer;

            var parent = this.getCurrentFolder();
            if (this.__currentDragOverRow) {
                parent = this.__currentDragOverRow.data;
            }
            function createItem(file) {
                var _item = {
                    sizeBytes: utils.humanFileSize(file.size),
                    size: utils.humanFileSize(file.size),
                    name: file.name,
                    type: file.type,
                    path: parent.path + '/' + file.name + '__uploading__' + Math.random(2),
                    isDir: false,
                    mount: parent.mount,
                    parent: parent.path,
                    progress: 0,
                    isUpload: true,
                    modified: file.lastModified / 1000,
                    renderColumn: function (field, value, obj) {

                        if (field === 'name') {
                            return thiz._createUploadItemTemplate(obj.name, obj.progress + '%', obj);
                        }
                        if (field === 'sizeBytes') {
                            if (obj.progress < 100) {
                                return utils.humanFileSize(obj.loaded) + ' of ' + utils.humanFileSize(obj.total);
                            }
                        }
                    }
                }
                //_item._S = store;
                store.putSync(_item);
                _item = store.getSync(_item.path);
                file._item = _item;
                return _item;
            }

            var items = [];
            _.each(files, function (file) {
                items.push(createItem(file));
            });
            return items;

        },
        _onAllUploaded: function (files) {
            var total = 0, items = this._toUploadItems(files),
                thiz = this;
            _.each(items, function (file) {
                var item = file._store ? file : file._item;
                if (item) {
                    item.progress = item.failed ? 0 : 100;
                    item.refresh();
                    setTimeout(function () {
                        item.remove();
                    }, 1);
                }
            });
            setTimeout(function () {
                thiz.runAction(types.ACTION.RELOAD).then(function () {
                });
            }, 1);
        },
        _onBrowserFilesDropped: function (panel, files, view) {
            var fm = this.ctx.getFileManager();
            var parent = panel.getCurrentFolder();
            if (this.__currentDragOverRow) {
                parent = this.__currentDragOverRow.data;
            }
            var dst = parent;
            var items = this._createUploadItems(files);
            return fm.upload(files, dst.mount, dst ? dst.path : './tmp', panel, view);
        },
        _onUploadFailed: function (uploadItem) {
            if (!uploadItem) {
                return;
            }
            uploadItem.failed = true;
            uploadItem.done = true;
        },
        _onSomeFileUploaded: function (uploadItem) {
            var thiz = this
            if (uploadItem) {
                uploadItem.progress = 100;
                uploadItem.done = true;
                uploadItem.renderColumn = null;
                uploadItem.renderRow = null;
                uploadItem.refresh();
                setTimeout(function () {
                    thiz.runAction(types.ACTION.RELOAD).then(function () {
                    });
                    uploadItem.remove();
                }, 1);
            }
            return true;

        },
        _toUploadItems: function (files) {
            var items = [];
            _.each(files, function (_file) {
                _.each(_file, function (file) {
                    items.push(file);
                });
            });
            return items;

        },
        _toUploadStoreItem: function (data) {
            var file = data.file,
                storeItem = file._item;
            return storeItem;

        },
        onBrowserFilesDropped: function (view, files) {
            var dfds = this._onBrowserFilesDropped(this, files, view),
                thiz = this,
                items = thiz._toUploadItems(files),
                isSingleUpload = items.length == 1;
            all(dfds).then(this._onAllUploaded.bind(this, [files]));
            function check() {
                var done = 0;
                _.each(items, function (item) {
                    item.done && done++;
                });
                done == items.length && thiz._onAllUploaded(files);
            }

            _.each(dfds, function (dfd) {
                dfd.then(function (data) {
                    //only refresh item if its a multi upload
                    !isSingleUpload && thiz._onSomeFileUploaded(thiz._toUploadStoreItem(data)) && check();
                }, function (data) {

                    thiz._onUploadFailed(thiz._toUploadStoreItem(data));
                    isSingleUpload && thiz._onAllUploaded(files);
                    !isSingleUpload && check();
                }, function (data) {

                    var computableEvent = data.progress,
                        item = data.item,
                        file = item.file,
                        storeItem = file._item,
                        percentage = Math.round((computableEvent.loaded * 100) / computableEvent.total);

                    if (storeItem) {
                        storeItem.progress = percentage;
                        storeItem.loaded = computableEvent.loaded;
                        storeItem.total = computableEvent.total;
                        storeItem.refresh();
                    }
                });
            });
        },
        onBrowserUrlDropped: function (srcView, url) {
            if (this.delegate && this.delegate.onBrowserUrlDropped) {
                this.delegate.onBrowserUrlDropped(this, url);
            }
        },
        dragEnter: function (e) {
            e.preventDefault();
        },
        dragLeave: function (e) {
        },
        dragOver: function (e) {
            e.preventDefault();
            if (!this.row) {
                return;
            }

            var self = this,
                row = this.row(e),
                oldRow = this.__currentDragOverRow,
                oldItem = oldRow ? oldRow.data : null,
                current = row ? row.data : null,
                isFolder = current ? current.directory == true : false;

            if (this.__currentDragOverRow) {
                $(this.__currentDragOverRow.element).removeClass('dgrid-focus');
            }

            if (row && row.element && row.data && row.data.directory === true) {
                if (!this.__currentDragOverTimer) {
                    self.__currentDragOverTimeRow = row;
                    //set up timer
                    self.__currentDragOverTimer = setTimeout(function () {
                        var _last = self.__currentDragOverTimeRow.data;
                        if (_last && oldItem && _last == oldItem) {
                            self.__currentDragOverTimeRow = null;
                            self.__currentDragOverTimer = null;
                            self.openFolder(_last);
                            return;
                        }
                        self.__currentDragOverTimer = null;
                    }, 1550);
                }
                this.__currentDragOverRow = row;
                $(row.element).addClass('dgrid-focus');
            } else {
                this.__currentDragOverRow = null;
            }
        },
        drop: function (e) {
            e.preventDefault();
            var dt = e.dataTransfer;
            if (dt) {
                var files = dt.files;
                if (files && files.length) {
                    if (this.onBrowserFilesDropped) {
                        this.onBrowserFilesDropped(this, files);
                    } else {
                         0 && console.warn('upload failed, invalid state : have no delegate or delegate::onBrowserFilesDropped doesnt exists');
                    }
                } else {
                    try {
                        var url = e.dataTransfer.getData('text/plain');
                        if (url && url.length) {
                            this.delegate.onBrowserUrlDropped(this, url);
                        }
                    } catch (err) {
                        console.error('dropping url failed ' + err);
                    }
                }
            }
        },
        initDND: function () {
            var thiz = this;
            var dstNode = this.domNode;
            if (dstNode) {
                dstNode.addEventListener("dragover", function (e) {
                    thiz.dragOver(e);
                }, false);
                dstNode.addEventListener("drop", function (e) {
                    e.preventDefault();
                    thiz.drop(e);
                }, false);
            } else {
                console.error('have not drop destination');
            }
        },
        startup: function () {
            if (this._started) {
                return;
            }
            var res = this.inherited(arguments);
            this.initDND();
            var grid = this,
                node = this.domNode;
            /*
             this._on('onAddActions', function (evt) {

             var actions = evt.actions,
             permissions = evt.permissions,
             action = types.ACTION.UPLOAD;
             if(!evt.store.getSync(action)) {
             var _action = grid.createAction('Upload', action, types.ACTION_ICON.UPLOAD, null, 'Home', 'File', 'item|view', null, null, null, null, null, permissions, node, grid);
             if (!_action) {
             return;
             }
             actions.push(_action);
             }
             });
             */
            return res;
        }
    });
});