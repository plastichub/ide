/** @module xgrid/Base **/
define("xfile/views/FileSize", [
    "dcl/dcl",
    "xide/_base/_Widget"

], function (dcl, _XWidget) {
    var Module = dcl(_XWidget,{
        _find:function(path,nodes){
            var res = _.find(nodes,{
                    path:path
                }),
                self = this;
            if(res && res.children && res.children){
                var _child = self._find(path,res.children);
                if(_child){
                    res = _child;
                }
            }
            if(!res){
                _.each(nodes,function(node){
                    var _inner = null;
                    if(node.children && node.children.length){
                        _inner = self._find(path,node.children);
                        if(_inner){
                            res=_inner;
                        }
                    }
                });
            }
            return res;
        },
        select:function(path){
            var d = this._find(path,this.d3Nodes);
            if(d) {
                this.highlight(d);
            }

        },
        direction:'left',
        templateString:'<div class="sizeDisplay tabbable tabs-${!direction}" attachTo="containerNode" style="width: inherit;height: 100%;padding: 4px">' +

        '<ul class="nav nav-tabs tabs-right" role="tablist">'+


        '<li class="active">'+
        '<a href="#_tab-content2_${!id}" data-toggle="tab">Overall</a>'+
        '</li>'+

        '<li>'+
        '<a href="#_tab-content1_${!id}" data-toggle="tab">By Type</a>'+
        '</li>'+



        '</ul>'+
        '<div class="body tab-content">'+

        '<div attachTo="tab2" id="_tab-content2_${!id}" class="tab-pane active clearfix">'+

        '<div class="chart1" attachTo="chart1" style="width: 100%;height: inherit;min-height:250px">'+
        '<div class="" style="" id="detailInfo"></div>'+
        '<div class="widget" id="explanation" >'+
        '<span id="percentage"></span><br/>'+
        '<span id="percentageDetail"></span>'+
        '</div>'+
        '</div>'+

        '</div>'+

        '<div attachTo="tab1" id="_tab-content1_${!id}" class="tab-pane">'+

        '<svg class="chart2" style="width: 100%;height: inherit;min-height:250px">'+

        '</svg>'+
        '<div id="tooltip" class="hidden">'+
        '<span id="chart2Per1">A</span> <br>'+
        '<span id="chart2Per2">100</span>'+
        '</div>'+

        '</div>'+


        '</div>'+

        '</div>',
        data:null,
        owner:self,
        vis:null,
        d3Nodes:null,
        colors : {
            "dir": "#5687d1",
            /*pictures same color*/
            "jpeg":"#9b59b6",
            "jpg":"#9b59b6",
            "png":"#9b59b6",
            "gif":"#9b59b6",
            "psd":"#9b59b6",
            /*audios same color*/
            "mp3":"#3498db",
            "wav":"#3498db",
            "wma":"#3498db",
            /*videos same color*/
            "wmv":"#2ecc71",
            "3gp":"#2ecc71",
            "mp4":"#2ecc71",
            "plv":"#2ecc71",
            "mpg":"#2ecc71",
            "mpeg":"#2ecc71",
            "mkv":"#2ecc71",
            "rm":"#2ecc71",
            "rmvb":"#2ecc71",
            "mov":"#2ecc71",
            /*office products same color*/
            "doc":"#1abc9c",
            "xls":"#1abc9c",
            "ppt":"#1abc9c",
            "docx":"#1abc9c",
            "xlsx":"#1abc9c",
            "pptx":"#1abc9c",
            /*mac products same color*/
            "pages":"#e74c3c",
            "key":"#e74c3c",
            "numbers":"#e74c3c",

            "pdf": "#7b615c",
            "epub": "#7b615c",
            /*programming langs*/
            "c":"#f1c40f",
            "cpp":"#f1c40f",
            "h":"#f1c40f",
            "html":"#f1c40f",
            "js":"#f1c40f",
            "css":"#f1c40f",
            "pl":"#f1c40f",
            "py":"#f1c40f",
            "php":"#f1c40f",
            "sql":"#f1c40f",
            "csv":"#de783b",
            "odp":"#de783b",
            /*zip files*/
            "gz":"#6ab975",
            "tar":"#6ab975",
            "rar":"#6ab975",
            "zip":"#6ab975",
            "7z":"#6ab975",
            "default": "#34495e"
        },
        totalSize : 0,
        result:{},
        overallSize:0,
        buildHierarchy:function(csv) {
            var root = {"name": "root", "children": []};
            for (var i = 0; i < csv.length; i++) {
                var sequence = csv[i][0];

                var size = +csv[i][1];
                if (isNaN(size)) { // e.g. if this is a header row
                    continue;
                }
                var parts = sequence.split("/");
                var currentNode = root;
                for (var j = 0; j < parts.length; j++) {
                    var children = currentNode["children"];
                    var nodeName = parts[j];
                    var childNode;
                    if (j + 1 < parts.length) {
                        // Not yet at the end of the sequence; move down the tree.
                        var foundChild = false;
                        for (var k = 0; k < children.length; k++) {
                            if (children[k]["name"] == nodeName) {
                                childNode = children[k];
                                foundChild = true;
                                break;
                            }
                        }
                        // If we don't already have a child node for this branch, create it.
                        if (!foundChild) {
                            childNode = {"name": nodeName, "type": "dir", "children": [],"path":sequence};
                            children.push(childNode);
                        }
                        currentNode = childNode;
                    } else {
                        var filetype = nodeName.split('.').pop();
                        if (filetype.length < 7) {
                            if (filetype in this.result) {
                                this.result[filetype] += size;
                            }
                            else {
                                this.result[filetype] = size;
                            }
                        }
                        childNode = {"name": nodeName, "type": filetype, "size": size,"path":sequence};
                        this.overallSize += size;
                        children.push(childNode);
                    }
                }
            }
            return root;
        },
        bytesToSize:function(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return 'n/a';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            if (i == 0) return bytes + ' ' + sizes[i];
            return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
        },
        getAncestors:function(node) {
            var path = [];
            var current = node;
            while (current.parent) {
                path.unshift(current);
                current = current.parent;
            }
            return path;
        },
        highlight:function(d){
            if(d){
                var percentage = (100 * d.value / this.totalSize).toPrecision(3);
                var percentageString = percentage + "%";
                if (percentage < 0.1) {
                    percentageString = "< 0.1%";
                }
                var percentageDetail = this.bytesToSize(d.value) + '/' + this.bytesToSize(this.totalSize);
                var colors = this.colors;
                d3.select("#percentage")
                    .text(percentageString);
                d3.select("#percentageDetail")
                    .text(percentageDetail);
                d3.select("#explanation")
                    .style("visibility", "");
                var sequenceArray = this.getAncestors(d);
                var arrayLength = sequenceArray.length;
                var htmlString = '';
                for (var i = 0; i < arrayLength; i++) {
                    var nodeType = sequenceArray[i].type;
                    if (nodeType == 'dir'){
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'/</span>';
                    }
                    else {
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'</span>';
                    }
                }
                $("#detailInfo").html(htmlString);
                // Fade all the segments.
                d3.selectAll("path").style("opacity", 0.3);
                // Then highlight only those that are an ancestor of the current segment.
                var foundNode = this.vis.selectAll("path")
                    .filter(function(node) {
                        return (sequenceArray.indexOf(node) >= 0);
                    })
                    .style("opacity", 1);
            }else{
                console.error('cant find '+path);
            }
        },
        createChart1:function(json,vis,width,height,radius) {
            vis.append("svg:circle")
                .attr("r", radius)
                .style("opacity", 0);

            var colors = this.colors,
                self = this;
            var partition = d3.layout.partition(radius)
                .size([2 * Math.PI, radius * radius])
                .value(function(d) { return d.size; });
            function mouseleave(d) {

                //return;

                // Hide the breadcrumb trail
                d3.select("#trail")
                    .style("visibility", "hidden");

                // Deactivate all segments during transition.
                d3.selectAll("path").on("mouseover", null);

                // Transition each segment to full opacity and then reactivate it.
                d3.selectAll("path")
                    .transition()
                    .duration(1000)
                    .style("opacity", 1)
                    .each("end", function() {
                        d3.select(this).on("mouseover", mouseover);
                    });

                d3.select("#explanation")
                    .transition()
                    .duration(1000)
                    .style("visibility", "hidden");

                $("#detailInfo").html("");

            }
            function mouseover(d) {
                var percentage = (100 * d.value / self.totalSize).toPrecision(3);
                var percentageString = percentage + "%";
                if (percentage < 0.1) {
                    percentageString = "< 0.1%";
                }
                var percentageDetail = self.bytesToSize(d.value) + '/' + self.bytesToSize(self.totalSize);

                d3.select("#percentage")
                    .text(percentageString);
                d3.select("#percentageDetail")
                    .text(percentageDetail);

                d3.select("#explanation")
                    .style("visibility", "");
                var sequenceArray = self.getAncestors(d);
                var arrayLength = sequenceArray.length;
                var htmlString = '';
                for (var i = 0; i < arrayLength; i++) {
                    var nodeType = sequenceArray[i].type;
                    if (nodeType == 'dir'){
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'/</span>';
                    }
                    else {
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'</span>';
                    }
                }
                $("#detailInfo").html(htmlString);

                // Fade all the segments.
                d3.selectAll("path")
                    .style("opacity", 0.3);

                // Then highlight only those that are an ancestor of the current segment.
                vis.selectAll("path")
                    .filter(function(node) {
                        return (sequenceArray.indexOf(node) >= 0);
                    })
                    .style("opacity", 1);
            }

            // For efficiency, filter nodes to keep only those large enough to see.
            var nodes = partition.nodes(json,radius)
                .filter(function(d) {
                    return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
                });

            var arc = d3.svg.arc()
                .startAngle(function(d) { return d.x; })
                .endAngle(function(d) { return d.x + d.dx; })
                .innerRadius(function(d) { return Math.sqrt(d.y); })
                .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

            var path = vis.data([json]).selectAll("path")
                .data(nodes)
                .enter().append("svg:path")
                .attr("display", function(d) { return d.depth ? null : "none"; })
                .attr("d", arc)
                .attr("fill-rule", "evenodd")
                .style("fill", function(d) { return colors[d.type]?colors[d.type]:colors["default"]; })
                .style("opacity", 1)
                .on("mouseover", mouseover);

            // Add the mouseleave handler to the bounding circle.
            d3.select("#container").on("mouseleave", mouseleave);

            // Get total size of the tree = value of root node from partition.
            this.totalSize = path.node().__data__.value;

            return nodes;
        },
        render:function(data){
            var node = $(this.tab2);
            var width = node.height() -50;
            var height = node.height() -50;
            if(width < 400){
                width = 400;
            }
            if(height < 300){
                height = 300;
            }
            var radius = height;
            var visParent = this.chart1;
            var vis = d3.select(visParent).append("svg:svg")
                .attr("width", width)
                .attr("height", height)
                .append("svg:g")
                .attr("id", "container")
                .attr("transform", "translate(" + height/2 + "," + height/2  + ")");

            var nodes = this.createChart1(data,vis,width,height,radius/4,vis);
            var filetypeJSON = [];
            for (var x in this.result) {
                var tempF = this.result[x] / this.overallSize;
                if (tempF < 0.001) {
                    //console.error('skip');
                    continue;
                }
                var tempN = this.bytesToSize(this.result[x]) + '/' + this.bytesToSize(this.overallSize);
                var temp = {filetype: x, "usage": tempF, "detail": tempN};
                filetypeJSON.push(temp);
            }

            this.vis = vis;
            this.d3Nodes = nodes;

            var node2 = $(this.tab1);
            width = node2.width();
            if(width < 400){
                width = 400;
            }
            this.createChart2(filetypeJSON,width,height-100);

        },
        createChart2:function(inputJson,width,height) {
            var margin = {top: 20, right: 30, bottom: 30, left: 40};

            var x = d3.scale.ordinal()
                .rangeRoundBands([0, width], .1);

            var y = d3.scale.linear()
                .range([height, 0]);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");


            var node = this.tab1.children[0],
                colors = this.colors;

            var chart = d3.select(node)
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var data = inputJson;

            x.domain(inputJson.map(function(d) {return d.filetype; }));

            y.domain([0, d3.max(inputJson, function(d) { return d.usage; })]);

            chart.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            chart.append("g")
                .attr("class", "y axis")
                .call(yAxis);

            chart.selectAll(".bar")
                .data(inputJson)
                .enter()
                .append("rect")
                .attr("class", "bar")
                .attr("id", function(d) { return 'bar'+d.filetype;})
                .attr("x", function(d) { return x(d.filetype); })
                .attr("y", function(d) { return y(d.usage); })
                .attr("height", function(d) { return height - y(d.usage); })
                .attr("width", x.rangeBand())
                .attr("fill", function(d) { return colors[d.filetype]?colors[d.filetype]:colors["default"]; })
                .on("mouseover", function(d) {
                    d3.select(this)
                        .transition()
                        .duration(50)
                        .attr("fill", "#7f8c8d");

                    //Get this bar's x/y values, then augment for the tooltip
                    var xPosition = parseFloat(d3.select(this).attr("x")) + x.rangeBand() / 2;
                    var yPosition = parseFloat(d3.select(this).attr("y")) / 2 + height / 2;

                    var usageString = parseFloat(d.usage * 100).toFixed(2) + '%('+ d.filetype +')';
                    //Update the tooltip position and value
                    d3.select("#tooltip")
                        .style("left", xPosition + "px")
                        .style("top", yPosition + "px")
                        .select("#chart2Per2")
                        .text(d.detail);

                    d3.select("#chart2Per1")
                        .text(usageString);

                    //Show the tooltip
                    d3.select("#tooltip").classed("hidden", false);
                })

                .on("mouseout", function() {
                    d3.select(this)
                        .transition()
                        .delay(100)
                        .duration(250)
                        .attr("fill", function(d) { return colors[d.filetype]?colors[d.filetype]:colors["default"]; })

                    //Hide the tooltip
                    d3.select("#tooltip").classed("hidden", true);

                });
        },
        startup:function(){
            if(!this.owner){
                return;
            }
            var owner = this.owner,
                rows = owner.getRows();
            var csv = [];
            _.each(rows,function(row){
                csv.push([row.path.replace('./',''),"" + row.sizeBytes]);
            });
            var json = this.buildHierarchy(csv);
            this.render(json);
            var self = this;
            owner._on('selectionChanged',function(evt){
                if(evt.why=='clear'){
                    return;
                }
                var selection = evt.selection;
                var first = selection ? selection[0] : null;
                var row = owner.row(first);
                var element = row ? row.element : null;
                if(!first || !element){
                    return;
                }
                var path = first.path.replace('./','');
                if(!self.d3Nodes){
                    return;
                }
                self.select(path);
            })
        }
    });
    return Module;
});