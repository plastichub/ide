/** @module xgrid/Base **/
define("xfile/views/FileConsole", [
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "xide/mixins/PersistenceMixin",
    'xide/layout/_TabContainer',
    'xide/views/_Console',
    'xide/views/_ConsoleWidget',
    "xfile/views/FileSize",
    'xide/views/ConsoleView',
    'xide/lodash',
    'dojo/has!xace?xace/views/Editor'
], function (declare, dcl, types,
             utils, PersistenceMixin, _TabContainer, Console, ConsoleWidget, FileSize, ConsoleView, _, Editor) {

    function createConsoleAceWidgetClass() {
        return dcl([ConsoleWidget, PersistenceMixin.dcl], {
            createEditor: function (ctx) {
                return createEditor(this.consoleParent, this.value, this, {
                    options: this.options,
                    ctx: ctx
                });
            }
        });
    }

    function createEditor(root, value, owner, mixin) {
        var item = {
            filePath: '',
            fileName: ''
        };
        var title = "No Title";
        var args = {
            _permissions: [],
            item: item,
            value: value,
            style: 'padding:0px;top:0 !important',
            iconClass: 'fa-code',
            options: utils.mixin(mixin, {
                filePath: item.path,
                fileName: item.name
            }),
            /***
             * Provide a text editor store delegate
             */
            storeDelegate: {},
            title: title
        };
        utils.mixin(args, mixin);
        var editor = utils.addWidget(Editor, args, owner, root, true, null, null, false);
        editor.resize();
        return editor;
    }

    function createShellViewDelegate() {
        return dcl(null, {
            owner: null,
            onServerResponse: function (theConsole, data, addTimes) {

                if (theConsole && data && theConsole.owner && theConsole.owner.onServerResponse) {
                    return theConsole.owner.onServerResponse(data, addTimes);
                }
            },
            runBash: function (theConsole, value, cwd, commandNode) {
                var thiz = this,
                    ctx = thiz.ctx;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['sh', _value, cwd]).then(function (response) {
                    if (commandNode) {
                        $(commandNode).find('.consoleRunningCommand').remove();
                    }
                    thiz.onServerResponse(theConsole, response, false);
                });
            },

            runPHP: function (theConsole, value, cwd) {
                var thiz = this,
                    ctx = thiz.ctx;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['php', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole, response, false);
                });
            },
            runJavascript: function (theConsole, value, context, args) {
                var _function = new Function("{" + value + "; }");
                var response = _function.call(context, args);
                if (response != null) {
                    this.onServerResponse(theConsole, response);
                    return response;
                }
                return value;
            },
            onConsoleCommand: function (data, value) {
                var thiz = this,
                    theConsole = data.console;
                if (theConsole.type === 'sh') {
                    var commandNode = thiz.onServerResponse(theConsole, "<span class='consoleRunningCommand fa-spinner fa-spin'></span><pre style='font-weight: bold'># " + value + "</pre>", true);
                    var dstPath = null;
                    if (this.owner && this.owner.getCurrentFolder) {
                        var cwd = this.owner.getCurrentFolder();
                        if (cwd) {
                            dstPath = utils.buildPath(cwd.mount, cwd.path, false);
                        }
                    }
                    return this.runBash(theConsole, value, dstPath, commandNode);
                }
                if (theConsole.type === 'php') {
                    var dstPath = null;
                    if (theConsole.isLinked()) {
                        dstPath = this.getCurrentPath();
                    }
                    return this.runPHP(theConsole, value, dstPath);
                }

                if (theConsole.type === 'javascript') {
                    return this.runJavascript(theConsole, value);
                }
            },
            onConsoleEnter: function (data, input) {
                return this.onConsoleCommand(data, input);
            }
        });
    }

    function createShellViewClass() {
        return dcl(Console, {
            lazy: true,
            consoleClass: createConsoleAceWidgetClass(),
            getServer: function () {
                return this.server || this.ctx.fileManager;
            },
            log: function (msg, addTimes) {
                utils.destroy(this.progressItem);
                var out = '',
                    isHTML = false;
                if (_.isString(msg)) {

                    if (msg.indexOf('<body') != -1 || /<[a-z][\s\S]*>/i.test(msg)) {
                        isHTML = true;
                        out = msg;
                    } else {
                        out += msg.replace(/\n/g, '<br/>');
                    }

                } else if (_.isObject(msg) || _.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var lastMessageNode = null;
                var thiz = this;
                if (isHTML) {
                    lastMessageNode = dojo.create("div", {
                        className: 'html_response',
                        innerHTML: out
                    });
                    dst.appendChild(lastMessageNode);
                    last = dst.appendChild(dojo.create("div", {
                        innerHTML: '&nbsp;',
                        style: 'height:1px;font-size:1px'
                    }));
                } else {

                    for (var i = 0; i < items.length; i++) {
                        var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');
                        var item = items[i];
                        if (!item || !item.length) {
                            continue;
                        }
                        last = dst.appendChild(dojo.create("div", {
                            className: _class,
                            innerHTML: this._toString(items[i], addTimes)

                        }));
                        this.lastIndex++;
                    }
                }
                if (last) {
                    last.scrollIntoViewIfNeeded();
                }
                return lastMessageNode;
            }
        });
    }

    function createShellViewClass2() {
        var EditorClass = dcl(ConsoleView.Editor, {
            didAddMCompleter: false,
            multiFileCompleter: null,
            blockScope: null,
            driverInstance: null,
            onACEReady: function (editor) {
            },
            onEditorCreated: function (editor, options) {
                this.inherited(arguments);
                this.onACEReady(editor);
            }
        });
        return dcl(ConsoleView, {
            EditorClass: EditorClass,
            onAddEditorActions: dcl.superCall(function (sup) {
                return function (evt) {
                    //grab the result from the handler

                    var res = sup.call(this, evt);


                    var thiz = this;

                    var actions = evt.actions,
                        owner = evt.owner;

                    var save = _.find(actions, {
                        command: 'File/Save'
                    });

                    actions.remove(_.find(actions, {
                        command: 'File/Save'
                    }));

                    actions.remove(_.find(actions, {
                        command: "File/Reload"
                    }));


                    var mixin = {
                        addPermission: true
                    }


                };
            }),
            logTemplate: '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>',
            _parse: function (scope, expression, errorCB) {
                var str = '' + expression;
                if (str.indexOf('{{') > 0 || str.indexOf('}}') > 0) {
                    str = _parser.parse(types.EXPRESSION_PARSER.FILTREX,
                        str, this,
                        {
                            variables: scope.getVariablesAsObject(),
                            delimiters: {
                                begin: '{{',
                                end: '}}'
                            }
                        }
                    );
                     0 && console.timeEnd('parse expression');
                } else {
                    var _text = scope.parseExpression(expression, null, null, null, errorCB);
                    if (_text) {
                        str = _text;
                    }
                }
                return str;
            },
            parse: function (str, errorCB) {
                var driverInstance = this.driverInstance;
                if (driverInstance && driverInstance.blockScope) {
                    return this._parse(driverInstance.blockScope, str, errorCB);
                }
                return str;
            }
        });
    }

    var Module = declare("xfile.views.FileConsole", null, {
        isStatusbarOpen: false,
        resizeToParent: true,
        __bottomTabContainer: null,
        onCloseStatusPanel: function (e) {
        },
        onOpenStatusPanel: function (panel) {
            if (!panel._tabs) {
                var thiz = this;
                var tabContainer = this.__bottomTabContainer || utils.addWidget(_TabContainer, {
                        direction: 'below'
                    }, null, panel, true);

                panel._tabs = tabContainer;
                this.__bottomTabContainer = tabContainer;
                var consoleViewClass = createShellViewClass2(),
                    handlerClass = createShellViewDelegate(),
                    delegate = new handlerClass();

                delegate.ctx = thiz.ctx;

                delegate.owner = this;
                var tab = tabContainer.createTab('Bash', 'fa-terminal'),
                    bashShell = tab.add(consoleViewClass, {
                        type: 'sh',
                        title: 'Bash',
                        icon: 'fa-terminal',
                        ctx: thiz.ctx,
                        owner: thiz,
                        value: 'ls -l --color=always'
                    }, null, true);

                bashShell.delegate = delegate;
                tab._onShown();
                var tab2 = tabContainer.createTab('Javascript', 'fa-code');
                var jsShell = tab2.add(consoleViewClass, {
                    type: 'javascript',
                    title: 'Javascript',
                    ctx: thiz.ctx,
                    value: 'return 2+2;',
                    owner: thiz
                }, null, false);
                jsShell.delegate = delegate;
                var tab3 = tabContainer.createTab('PHP', 'fa-terminal');
                var jsShell = tab3.add(consoleViewClass, {
                    type: 'php',
                    title: 'PHP',
                    ctx: thiz.ctx,
                    value: '<?php \n\n>',
                    owner: thiz
                }, null, false);
                var tab4 = tabContainer.createTab('Sizes', 'fa-bar-chart');
                var fileSize = tab4.add(FileSize, {
                    owner: this,
                    ctx: thiz.ctx
                }, null, false);
                this.__fileSize = fileSize;
                this._on('openedFolder', function (data) {
                    utils.destroy(thiz.__fileSize);
                    tab4.resize();
                    thiz.resize();
                    thiz.__fileSize = null;
                    thiz.__fileSize = tab4.add(FileSize, {
                        owner: thiz,
                        ctx: thiz.ctx
                    }, null, false);
                    utils.resizeTo(thiz.__fileSize, tab4, true, true);
                    thiz.__fileSize.startup();
                });
                panel.add(tabContainer);
            }
            return this.inherited(arguments);
        },
        onStatusbarCollapse: function (collapser) {
            var panel = null;
            if (this.isStatusbarOpen) {
                panel = this.getBottomPanel(false, 0.2);
                panel.collapse();
                this.onCloseStatusPanel(collapser);
                collapser && collapser.removeClass('fa-caret-down');
                collapser && collapser.addClass('fa-caret-up');
            } else {
                panel = this._getBottom();
                if (!panel) {
                    panel = this.getBottomPanel(false, 0.2);
                } else {
                    panel.expand();
                }
                this.onOpenStatusPanel(panel);

                collapser && collapser.removeClass('fa-caret-up');
                collapser && collapser.addClass('fa-caret-down');
            }
            this.isStatusbarOpen = !this.isStatusbarOpen;
            panel.resize();
        }
    });
    Module.createEditor = createEditor;
    Module.createShellViewDelegate = createShellViewDelegate;
    Module.createShellViewClass = createShellViewClass2;

    return Module;
});