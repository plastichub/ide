require({cache:{
'xfile/main':function(){
define([
    "dojo/_base/kernel",
    'dojo/Stateful',
    'dojo/query',
    'dojo/cache',
    'dojo/window',
	'xfile/types',
    'xfile/component',
    'xfile/views/Grid',
    'xfile/views/FileGrid',
    'xfile/FileActions',
    'xfile/Statusbar',
    'xfile/ThumbRenderer',
    'xfile/Breadcrumb',
    'xfile/types',
    'xfile/component',
    'xfile/config',
    'xfile/model/File',
    'xfile/manager/FileManager',
    'xfile/manager/MountManager',
    'xfile/manager/BlockManager',
    'xfile/views/FileConsole',
    'xfile/data/DriverStore',
    'xfile/views/FileGridLight',
    'xfile/manager/Electron'
], function(){

});

},
'xfile/types':function(){
define([
    'xide/utils',
    'xide/types',
    'xide/types/Types',
    'xaction/types'
], function (utils, types) {
    /**
     * Public mime registry setter
     * @param type
     * @param map
     */
    types.registerCustomMimeIconExtension = function (type, map) {
        types['customMimeIcons'][type] = map;
    };

    var ACTION = types.ACTION;

    var DEFAULT_PERMISSIONS = [
        ACTION.EDIT,
        ACTION.COPY,
        ACTION.CLOSE,
        ACTION.MOVE,
        ACTION.RENAME,
        ACTION.DOWNLOAD,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.NEW_FILE,
        ACTION.NEW_DIRECTORY,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.PREVIEW,
        ACTION.OPEN_IN,
        ACTION.GO_UP,
        ACTION.SEARCH,
        ACTION.OPEN_IN_TAB,
        ACTION.TOOLBAR,
        ACTION.STATUSBAR,
        ACTION.UPLOAD,
        ACTION.SIZE_STATS,
        ACTION.CONSOLE,
        ACTION.HEADER,
        'File/Compress',
        'File/New',
        ACTION.CONTEXT_MENU,
        ACTION.SOURCE,
        'File/OpenInOS'
    ];

    types.DEFAULT_FILE_GRID_PERMISSIONS = DEFAULT_PERMISSIONS;

    types.FIELDS = {
        SHOW_ISDIR: 1602,
        SHOW_OWNER: 1604,
        SHOW_MIME: 1608,
        SHOW_SIZE: 1616,
        SHOW_PERMISSIONS: 1632,
        SHOW_TIME: 1633,
        SHOW_FOLDER_SIZE: 1634,
        SHOW_FOLDER_HIDDEN: 1635,
        SHOW_TYPE: 1636,
        SHOW_MEDIA_INFO: 1637
    };

    types.FILE_PANEL_LAYOUT =
        {
            TREE: 1,
            LIST: 2,
            THUMB: 3,
            PREVIEW: 4,
            COVER: 5,
            SPLIT_VERTICAL: 6,
            SPLIT_HORIZONTAL: 7,
            IMAGE_GRID: 8
        };

    types.LAYOUT_PRESET =
        {
            DUAL: 1,
            SINGLE: 2,
            BROWSER: 3,
            PREVIEW: 4,
            GALLERY: 5,
            EDITOR: 6
        };

    types.PANEL_OPTIONS = {
        ALLOW_NEW_TABS: true,
        ALLOW_MULTI_TAB: false,
        ALLOW_INFO_VIEW: true,
        ALLOW_LOG_VIEW: true,
        ALLOW_CONTEXT_MENU: true,
        ALLOW_LAYOUT_SELECTOR: true,
        ALLOW_SOURCE_SELECTOR: true,
        ALLOW_COLUMN_RESIZE: true,
        ALLOW_COLUMN_REORDER: true,
        ALLOW_COLUMN_HIDE: true,
        ALLOW_ACTION_TOOLBAR: true,
        ALLOW_MAIN_MENU: true
    };

    /**
     * @TODO: remove
     * @type {{LAYOUT: number, AUTO_OPEN: boolean}}
     */
    types.FILE_PANEL_OPTIONS_LEFT = {
        LAYOUT: 2,
        AUTO_OPEN: true
    };

    types.FILE_PANEL_OPTIONS_MAIN = {
        LAYOUT: 3,
        AUTO_OPEN: true
    };

    types.FILE_PANEL_OPTIONS_RIGHT = {
        LAYOUT: 3,
        AUTO_OPEN: true
    };
    types.FILE_GRID_COLUMNS =
        {
            NAME: 'name',
            SIZE: 'size',
            MODIFIED: 'modified'
        };
    types.ACTION_TOOLBAR_MODE =
        {
            SELF: 'self'
        };

    utils.mixin(types.ITEM_TYPE, {
        FILE: 'BTFILE'
    });

    /***
     *
     * Extend the core events with xfile specific events
     */
    /**
     * ActionVisibility
     * @enum module:xide/types/EVENTS
     * @memberOf module:xide/types
     */
    utils.mixin(types.EVENTS, {
        STORE_CHANGED: 'onStoreChange',
        BEFORE_STORE_CHANGE: 'onBeforeStoreChange',
        STORE_REFRESHED: 'onStoreRefreshed',
        ON_FILE_STORE_READY: 'onFileStoreReady',
        ON_DID_OPEN_ITEM: 'onDidOpenItem',
        ON_SHOW_PANEL: 'onShowPanel',
        ITEM_SELECTED: 'onItemSelected',
        ERROR: 'fileOperationError',
        STATUS: 'fileOperationStatus',
        IMAGE_LOADED: 'imageLoaded',
        IMAGE_ERROR: 'imageError',
        RESIZE: 'resize',
        ON_UPLOAD_BEGIN: 'onUploadBegin',
        ON_UPLOAD_PROGRESS: 'onUploadProgress',
        ON_UPLOAD_FINISH: 'onUploadFinish',
        ON_UPLOAD_FAILED: 'onUploadFailed',
        ON_CLIPBOARD_COPY: 'onClipboardCopy',
        ON_CLIPBOARD_PASTE: 'onClipboardPaste',
        ON_CLIPBOARD_CUT: 'onClipboardCut',
        ON_CONTEXT_MENU_OPEN: 'onContextMenuOpen',
        ON_PLUGIN_LOADED: 'onPluginLoaded',
        ON_PLUGIN_READY: 'onPluginReady',
        ON_MAIN_VIEW_READY: 'onMainViewReady',
        ON_FILE_CONTENT_CHANGED: 'onFileContentChanged',
        ON_PANEL_CLOSED: 'onPanelClosed',
        ON_PANEL_CREATED: 'onPanelCreated',
        ON_COPY_BEGIN: 'onCopyBegin',
        ON_COPY_END: 'onCopyEnd',
        ON_DOWNLOAD_TO_BEGIN: 'onDownloadToBegin',
        ON_DOWNLOAD_TO_END: 'onDownloadToEnd',
        ON_DELETE_BEGIN: 'onDeleteBegin',
        ON_DELETE_END: 'onDeleteEnd',
        ON_MOVE_BEGIN: 'onMoveBegin',
        ON_MOVE_END: 'onMoveEnd',
        ON_COMPRESS_BEGIN: 'onCompressBegin',
        ON_COMPRESS_END: 'onCompressEnd',
        ON_SOURCE_MENU_OPEN: 'onSourceMenuOpen',
        ON_MOUNT_DATA_READY: 'onMountDataReady',
        ON_XFILE_READY: 'onXFileReady',
        ON_CHANGE_PERSPECTIVE: 'onChangePerspective',
        ON_FILE_PROPERTIES_RENDERED: 'onFilePropertiesRendered'
    });

    /**
     * SELECTION_MODE specfies the possible selection modes for xfile grid views
     * @enum module:xide/types/SELECTION_MODE
     * @memberOf module:xide/types
     */
    types.SELECTION_MODE =
        {
            /** Single
             * @const
             * @type {string}
             */
            SINGLE: 'single',
            /** Multiple
             * @const
             * @type {string}
             */
            MULTI: 'multiple',
            /** Extended
             * @const
             * @type {string}
             */
            EXTENDED: 'extended'
        };

    /**
     * @TODO: remove
     * OPERATION is the string representation of xfile commands
     * @enum module:xide/types/OPERATION
     * @memberOf module:xide/types
     */
    types.OPERATION =
        {

            COPY: 'copy',
            MOVE: 'move',
            RENAME: 'rename',
            DELETE: 'delete',
            OPEN: 'open',
            EDIT: 'edit',
            DOWNLOAD: 'download',
            DOWNLOAD_TO: 'downloadTo',
            INFO: 'info',
            COMPRESS: 'compress',
            RELOAD: 'reload',
            PREVIEW: 'preview',
            INSERT_IMAGE: 'insertImage',
            COPY_PASTE: 'copypaste',
            DND: 'dnd',
            OPTIONS: 'options',
            NEW_FILE: 'mkfile',
            NEW_DIRECTORY: 'mkdir',
            GET_CONTENT: 'get',
            SET_CONTENT: 'set',
            FIND: 'find',
            CUSTOM: 'custom',
            PERMA_LINK: 'permaLink',
            ADD_MOUNT: 'ADD_MOUNT',
            REMOVE_MOUNT: 'REMOVE_MOUNT',
            EDIT_MOUNT: 'EDIT_MOUNT',
            PERSPECTIVE: 'PERSPECTIVE',
            EXTRACT: 'extract'
        };

    /**
     * @TODO: remove
     * OPERATION_INT is the integer version of {xide/types/OPERATION}
     * @enum module:xide/types/OPERATION_INT
     * @memberOf module:xide/types
     */
    types.OPERATION_INT = {
        NONE: 0,
        EDIT: 1,
        COPY: 2,
        MOVE: 3,
        INFO: 4,
        DOWNLOAD: 5,
        COMPRESS: 6,
        DELETE: 7,
        RENAME: 8,
        DND: 9,
        COPY_PASTE: 10,
        OPEN: 11,
        RELOAD: 12,
        PREVIEW: 13,
        INSERT_IMAGE: 15,
        NEW_FILE: 16,
        NEW_DIRECTORY: 17,
        UPLOAD: 18,
        READ: 19,
        WRITE: 20,
        PLUGINS: 21,
        CUSTOM: 22,
        FIND: 23,
        PERMA_LINK: 24,
        ADD_MOUNT: 25,
        REMOVE_MOUNT: 26,
        EDIT_MOUNT: 27,
        PERSPECTIVE: 28,      //change perspective
        CLIPBOARD_COPY: 29,
        CLIPBOARD_CUT: 30,
        CLIPBOARD_PASTE: 31,
        EXTRACT: 32
    };

    types.EResolveMode = {
        "SKIP": 0,
        "OVERWRITE": 1,
        "IF_NEWER": 2,
        "IF_SIZE_DIFFERS": 3,
        "APPEND": 4,
        "THROW": 5,
        "ABORT": 6
    }
    
    types.EResolve = {
        ALWAYS: 0,
        THIS: 1
    }
    
    types.EError = {
        NONE: 'None',
        EXISTS: 'EEXIST',
        PERMISSION: 'EACCES',
        NOEXISTS: 'ENOENT',
        ISDIR:'EISDIR'
    }
    return types;
});
},
'xfile/component':function(){
define([
    "dcl/dcl",
    "xide/model/Component"
], function (dcl,Component) {

    /**
     * @class xfile.component
     * @inheritDoc
     */
    return dcl(Component, {
        /**
         * @inheritDoc
         */
        beanType:'BTFILE',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        getDependencies:function(){
            return [
                "xfile/types",
                "xfile/manager/FileManager",
                "xfile/manager/MountManager",
                "xfile/factory/Store",
                "xfile/views/FileGrid"
            ];
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return 'xfile';
        },
        /**
         * @inheritDoc
         */
        getBeanType:function(){
            return this.beanType;
        }
    });
});


},
'xfile/views/Grid':function(){
/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/ListRenderer',
    'xfile/ThumbRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xfile/FileActions',
    'xfile/Statusbar',
    'xfile/views/UploadMixin',
    'xide/views/_LayoutMixin',
    'xgrid/KeyboardNavigation',
    'xgrid/Search',
    'xgrid/Selection',
    'xide/mixins/_State',
    "xide/widgets/_Widget",
    'xdojo/has!FileConsole?xfile/views/FileConsole',
    'xfile/FolderSize',
    'xide/noob'
], function (declare, types,ListRenderer, ThumbRenderer, TreeRenderer,Grid, MultiRenderer,FileActions,Statusbar,UploadMixin,_LayoutMixin,KeyboardNavigation,Search,Selection,_State,_Widget,FileConsole,FolderSize,noob) {
    
    /**
     * A grid feature
     * @class module:xgrid/GridActions
     */
    var Implementation = {

        },
        renderers = [ListRenderer,ThumbRenderer,TreeRenderer],
        multiRenderer = declare.classFactory('multiRenderer',{},renderers,MultiRenderer.Implementation);

    var GridClass = Grid.createGridClass('xfile.views.Grid', Implementation, {
            SELECTION: {
                CLASS:Selection
            },
            KEYBOARD_SELECTION: true,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD:types.GRID_FEATURES.CLIPBOARD,
            ACTIONS:types.GRID_FEATURES.ACTIONS,
            ITEM_ACTIONS: {
                CLASS:FileActions
            },
            STATUS_BAR: {
                CLASS:Statusbar
            },
            SPLIT:{
                CLASS:_LayoutMixin
            },
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            SEARCH:{
                CLASS:Search
            },
            STATE:{
                CLASS:_State
            },
            UPLOAD: {
                CLASS:UploadMixin
            },
            WIDGET:{
                CLASS:_Widget
            },
            SIZES:{
                CLASS:FolderSize
            }
        },
        {
            RENDERER: multiRenderer
        },
        {
            renderers: renderers,
            selectedRenderer: TreeRenderer
        }
    );
    GridClass.DEFAULT_RENDERERS = renderers;
    GridClass.DEFAULT_MULTI_RENDERER = multiRenderer;
    return GridClass;

});
},
'xfile/ThumbRenderer':function(){
/** @module xfile/ThumbRenderer **/
define([
    "xdojo/declare",
    'xide/utils',
    'dojo/dom-construct',
    'xgrid/ThumbRenderer'
], function (declare,utils,domConstruct,ThumbRenderer) {

    /**
     * The list renderer does nothing since the xgrid/Base is already inherited from
     * dgrid/OnDemandList and its rendering as list already.
     *
     * @class module:xfile/ThumbRenderer
     * @extends module:xfile/Renderer
     */
    return declare('xfile.ThumbRenderer',[ThumbRenderer],{
        thumbSize: "400",
        resizeThumb: true,
        __type:'thumb',
        deactivateRenderer:function(){
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding','');
            this.isThumbGrid = false;

        },
        activateRenderer:function(){
            $(this.domNode.parentNode).addClass('metro');
            $(this.contentNode).css('padding','8px');
            this.isThumbGrid = true;
            this.refresh();
        },

        _renderUpload:function(name,progress,obj){

            progress = parseInt(progress.replace('%',''));
            if(progress==100){
                progress = 90;
            }

            var result = '<div class="tile widget uploadItem"><div class="radial-progress tile widget " data-progress="' + progress + '">'+
                '<div class="circle">'+
                '<div class="mask full">'+
                '<div class="fill"></div>'+
                '</div>'+
                '<div class="mask half">'+
                '<div class="fill"></div>'+
                '<div class="fill fix"></div>'+
                '</div>'+
                '<div class="shadow"></div>'+
                '</div>'+
                '<div class="inset">'+
                '<div class="percentage">'+
                '<div class="numbers"><span>-</span>';

            for(var i= 0 ; i< 99 ; i++){

                result+=('<span>' + i +'%' + '</span>');

            };

            result+='</div>'+
                '</div>'+
                '</div>'+
                '</div></div>';

            return result;

        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            if(obj.isUpload === true){
                return $(this._renderUpload(obj.name, obj.progress + '%',obj))[0];
            }
            if(obj.renderRow){
                var _res = obj.renderRow.apply(this,[obj]);
                if(_res){
                    return _res;
                }
            }
            var thiz = this,
                div = domConstruct.create('div', {
                    className: "tile widget"
                }),
                icon = obj.icon,
                no_access = obj.read === false && obj.write === false,
                isBack = obj.name == '..',
                directory = obj && !!obj.directory,
                useCSS = false,
                label = '',
                imageClass = 'fa fa-folder fa-5x',
                isImage = false;

            this._doubleWidthThumbs = true;
            var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;opacity: 0.7';
            var contentClass = 'icon';
            if (directory) {

                if (isBack) {
                    imageClass = 'fa fa-level-up fa-5x itemFolder';
                    useCSS = true;
                } else if (!no_access) {
                    imageClass = 'fa fa-folder fa-5x itemFolder';
                    useCSS = true;
                } else {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                }

            } else {

                imageClass = 'itemIcon';

                if (no_access) {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                } else {

                    if (utils.isImage(obj.path)) {

                        var url = this.getImageUrl(obj);
                        if (url) {
                            obj.icon = url;
                        } else {
                            obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                        }

                        imageClass = 'imageFile';

                    } else {
                        imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                        useCSS = true;
                    }
                }

            }

            label = obj.name;

            var folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';
            if (this.hideExtensions) {
                label = utils.pathinfo(label).filename;
            }

            if (utils.isImage(obj.path)) {

                var url = this.getImageUrl(obj);
                if (url) {
                    obj.icon = url;
                } else {
                    obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                }

                imageClass = '';
                contentClass = 'image';
                folderContent = '<div style="" class="tile-content image">' +
                    '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                    '</div>';

                useCSS = true;
                isImage = true;

            }
            var label2 = label + '\n' + obj.modified;
            var html = '<div title="' + label2 +'" class="tile-content ' + contentClass +'">'+
                folderContent +
                '</div>'+

                '<div class="brand opacity">'+
                '<span class="thumbText text opacity ellipsis" style="">'+
                label +
                '</span>'+
                '</div>';

            if (useCSS) {
                div.innerHTML = html;
                return div;
            }


            if (directory) {
                div.innerHTML = html;
            } else {
                div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
            }
            return div;

        },
        getImageUrl: function (item) {
            var fileManager = this.ctx.getFileManager();
            if (fileManager && fileManager) {
                var params = null;
                if (this.resizeThumb) {
                    params = {
                        width: this.thumbSize
                    }
                }
                return fileManager.getImageUrl(item, null, params);
            }
            return null;
        }
    });
});
},
'xfile/FileActions':function(){
/** module:xfile/FileActions **/
define([
    'dcl/dcl',
    'xdojo/declare',
    'xdojo/has',
    'dojo/Deferred',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xide/model/Path',
    'xaction/DefaultActions',
    'xide/editor/Registry',
    'xide/editor/Default',
    'dojo/aspect',
    'xfile/views/FileOperationDialog',
    'xfile/views/FilePreview',
    'xide/views/_CIDialog',
    "xide/views/_Dialog",
    "xide/views/_PanelDialog",
    'xfile/views/FilePicker',
    "dojo/text!xfile/newscene.html"
], function (dcl, declare, has, Deferred, utils, types, factory, Path, DefaultActions, Registry, Default, aspect, FileOperationDialog, FilePreview, _CIDialog, _Dialog, _PanelDialog, FilePicker, newscene) {
    var ACTION = types.ACTION;
    var _debug = false;
    var Module = null;

    function defaultCopyOptions() {
        /***
         * gather options
         */
        var result = {
            includes: ['*', '.*'],
            excludes: [],
            mode: 1501
        },
            flags = 4;

        switch (flags) {

            case 1 << 2:
                {
                    result.mode = 1502;//all
                    break;
                }
            case 1 << 4:
                {
                    result.mode = 1501;//none
                    break;
                }
            case 1 << 8:
                {
                    result.mode = 1504;//newer
                    break;
                }
            case 1 << 16:
                {
                    result.mode = 1503;//size
                    break;
                }
        }

        return result;
    }

    /**
     *
     * @param owner
     * @param inValue
     * @param callback
     * @param title
     * @param permissions
     * @param pickerOptions
     * @param dfd
     * @param Module
     * @returns {*}
     */
    function createFilePicker(owner, inValue, callback, title, permissions, pickerOptions, dfd, Module, storeOptions) {
        var dlg = new _PanelDialog({
            size: types.DIALOG_SIZE.SIZE_NORMAL,
            getContentSize: function () {
                return {
                    width: '600px',
                    height: '500px'
                }
            },
            bodyCSS: {
                'height': 'auto',
                'min-height': '500px',
                'padding': '8px',
                'margin-right': '16px'
            },
            picker: null,
            title: title,
            onOk: function () {
                var selected = this.picker._selection;
                if (selected && selected[0]) {
                    callback(selected[0]);
                    dfd && dfd.resolve(selected[0], selected, this.picker);
                }
            },
            onShow: function (panel, contentNode) {
                var picker = utils.addWidget(FilePicker, utils.mixin({
                    ctx: owner.ctx,
                    owner: owner,
                    selection: inValue || '.',
                    resizeToParent: true,
                    storeOptionsMixin: utils.mixin({
                        "includeList": "*,.*",
                        "excludeList": ""
                    }, storeOptions),
                    Module: Module,
                    permissions: permissions || utils.clone(types.DEFAULT_FILE_GRID_PERMISSIONS)
                }, pickerOptions), this, contentNode, true);

                this.picker = picker;
                this.add(picker);
                this.startDfd.resolve();
                return [this.picker];

            }
        });
        dlg.show();
        return dfd;
    }

    function openFilePicker(dlg, fileWidget) {

        var inValue = fileWidget.userData.value || '.';
        var dfd = new Deferred(),
            self = this;

        var result = new _PanelDialog({
            size: types.DIALOG_SIZE.SIZE_NORMAL,
            getContentSize: function () {
                return {
                    width: '600px',
                    height: '500px'
                }
            },
            bodyCSS: {
                'height': 'auto',
                'min-height': '500px',
                'padding': '8px',
                'margin-right': '16px'
            },
            picker: null,
            title: fileWidget.title,
            onOk: function () {
                var selected = this.picker._selection;
                if (selected && selected[0]) {
                    fileWidget.set('value', selected[0].path);
                    dfd.resolve(selected[0].path);
                    result.value = selected[0].path;
                }

            },
            onShow: function (panel, contentNode) {
                var picker = utils.addWidget(FilePicker, {
                    ctx: self.ctx,
                    owner: self,
                    selection: inValue || '.',
                    resizeToParent: true,
                    mount: self.collection.mount,
                    storeOptionsMixin: {
                        "includeList": "*,.*",
                        "excludeList": ""
                    },
                    Module: self.Module,
                    permissions: utils.clone(self.Module.DEFAULT_PERMISSIONS)
                }, this, contentNode, true);

                this.picker = picker;
                this.add(picker, null, false);
                this.startDfd.resolve();
                return [this.picker];

            }
        });
        result.show();
        return dfd;

    }

    /**
     * XFile actions
     * @class module:xfile/FileActions
     * @augments module:xfile/views/FileGrid
     */
    var Implementation = {
        __loading: true,
        /**
         * mkdir version:
         *
         *    1. open dialog with input box (value = currentItem)
         *
         * @TODO : remove leaks
         */
        touch: function () {
            var dfd = new Deferred();
            try {
                var self = this,
                    currentItem = this.getSelectedItem(),
                    currentFolder = this.getCurrentFolder(),
                    startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                    collection = this.collection;

                var CIS = {
                    inputs: [
                        utils.createCI('Name', 13, startValue, {
                            widget: {
                                instant: true,
                                validator: function (value) {
                                    if (currentFolder) {
                                        return collection.getSync(currentFolder.path + '/' + value) == null &&
                                            value.length > 0;
                                    } else {
                                        return true;
                                    }
                                }
                            }
                        })
                    ]
                },
                    defaultDfdArgs = {
                        select: currentItem,
                        focus: true,
                        append: false
                    };

                var dlg = new _CIDialog({
                    cis: CIS,
                    title: 'Create new File',
                    ctx: this.ctx,
                    size: types.DIALOG_SIZE.SIZE_SMALL,
                    bodyCSS: {
                        'height': 'auto',
                        'min-height': '80px'

                    },
                    _onError: function (title, suffix, message) {
                        title = title || this.title;
                        message = message || this.notificationMessage;
                        message && message.update({
                            message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                            type: 'error',
                            actions: false,
                            duration: 15000
                        });
                        this.onError && this.onError(suffix);
                    },
                    onCancel: function () {
                        dfd.resolve();
                    },
                    onOk: function () {
                        var val = this.getField('name');
                        if (val == null) {
                            dfd.resolve(defaultDfdArgs);
                            return;
                        }
                        var currentFolder = self.getCurrentFolder() || { path: '.' },
                            newFolder = currentFolder.path + '/' + val;

                        var fileDfd = self.ctx.getFileManager().mkfile(collection.mount, newFolder, {
                            checkErrors: true,
                            returnProm: false
                        });

                        // call server::mkdir, then reload, then resolve dfd with new newFolder as selection
                        fileDfd.then(function (data) {
                            self.runAction(ACTION.RELOAD).then(function () {
                                collection.getSync(newFolder) && dfd.resolve({
                                    select: newFolder,
                                    append: false,
                                    focus: true
                                });
                            });
                        }, function (e) {
                            dfd.resolve(defaultDfdArgs);
                            logError(e, 'error creating directory');
                        });
                    }
                });
                dlg.show();
            } catch (e) {
                logError(e, 'error creating dialog');
            }
            return dfd;

        },
        newTabArgs: null,
        openInOS: function (item) {
            var thiz = this,
                ctx = thiz.ctx,
                resourceManager = ctx.getResourceManager(),
                vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {};

            var mount = item.mount.replace('/', '');
            if (!vfsConfig[mount]) {
                console.error('open in os failed: have no VFS config for ' + mount);
                return;
            }

            var _require = window['eRequire'];
            if (!_require) {
                console.error('have no electron');
            }


            var os = _require('os');
            var shell = _require('electron').shell;
            var path = _require("path");

            mount = vfsConfig[mount];
            mount = utils.replaceAll(' ', '', mount);

            var itemPath = item.path.replace('./', '/');
            itemPath = utils.replaceAll('/', path.sep, itemPath);
            var realPath = path.resolve(mount + path.sep + itemPath);
            realPath = utils.replaceAll(' ', '', realPath);
            if (os.platform() !== 'win32') {
                shell.openItem(realPath);
            } else {
                shell.openItem('file:///' + realPath);
            }

             0 && console.log('open in system ' + mount + ':' + itemPath + ' = ' + realPath, vfsConfig);

        },
        getFileActions: function (permissions) {
            permissions = permissions || this.permissions;
            var result = [],
                ACTION = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                VISIBILITY = types.ACTION_VISIBILITY,
                thiz = this,
                ctx = thiz.ctx,
                container = thiz.domNode,
                resourceManager = ctx.getResourceManager(),
                vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') : {},//possibly resourceManager not there in some builds
                actionStore = thiz.getActionStore();

            ///////////////////////////////////////////////////
            //
            //  misc
            //
            ///////////////////////////////////////////////////
            result.push(thiz.createAction({
                label: 'Go up',
                command: ACTION.GO_UP,
                icon: ACTION_ICON.GO_UP,
                tab: 'Home',
                group: 'Navigation',
                keycombo: ['backspace'],
                mixin: {
                    quick: true
                }
            }));

            result.push(thiz.createAction({
                label: 'Copy',
                command: ACTION.COPY,
                icon: 'fa-paste',
                tab: 'Home',
                group: 'Organize',
                keycombo: ['f5'],
                shouldDisable: function () {
                    return thiz.getSelectedItem() == null;
                }
            }));

            result.push(thiz.createAction({
                label: 'Close',
                command: ACTION.CLOSE,
                icon: 'fa-close',
                tab: 'View',
                group: 'Organize',
                keycombo: ['ctrl 0']
            }));

            result.push(thiz.createAction({
                label: 'Move',
                command: ACTION.MOVE,
                icon: 'fa-long-arrow-right',
                tab: 'Home',
                group: 'Organize',
                keycombo: ['f6'],
                shouldDisable: function () {
                    return thiz.getSelectedItem() == null;
                }
            }));

            result.push(thiz.createAction({
                label: 'Create New Scene',
                command: 'File/New Scene',
                icon: 'fa-magic',
                tab: 'Home',
                group: 'Organize',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));

            if ((location.href.indexOf('electron=true') !== -1 || has('electronx') &&  true ) && vfsConfig) {
                result.push(thiz.createAction({
                    label: 'Open with System',
                    command: 'File/OpenInOS',
                    icon: 'fa-share',
                    tab: 'Home',
                    group: 'Open',
                    keycombo: ['shift f4'],
                    mixin: {
                        addPermission: true,
                        quick: true
                    },
                    shouldDisable: function () {
                        var item = thiz.getSelectedItem();
                        if (!item) {
                            return;
                        }
                        return false;
                    }
                }));
            }

            result.push(this.createAction('Open In New Tab', 'File/OpenInNewTab', 'fa-share', ['alt enter'], 'Home', 'Open', 'item', null, null, { quick: true }, null, function () {
                var item = thiz.getSelectedItem();
                if (item && item.isDir) {
                    return false;
                }
                return true;
            }, permissions, container, thiz
            ));


            if (ctx && ctx.getMountManager && DefaultActions.hasAction(permissions, ACTION.SOURCE)) {
                permissions.push(ACTION.SOURCE);
                var mountManager = ctx.getMountManager(),
                    mountData = mountManager.getMounts(),
                    i = 0;

                if (mountData && mountData.length > 0) {
                    var currentValue = thiz.collection.mount;
                    result.push(this.createAction({
                        label: 'Source',
                        command: ACTION.SOURCE,
                        icon: 'fa-hdd-o',
                        keyCombo: ['ctrl f1'],
                        tab: 'Home',
                        group: 'Navigation',
                        mixin: {
                            data: mountData,
                            addPermission: true,
                            quick: true
                        },
                        onCreate: function (action) {
                            action.set('value', currentValue);
                        }
                    }));

                    function createSourceAction(item) {
                        var label = item.label;
                        permissions.push(ACTION.SOURCE + '/' + label);
                        var _sourceAction = thiz.createAction({
                            label: label,
                            command: ACTION.SOURCE + '/' + label,
                            icon: 'fa-hdd-o',
                            keyCombo: ['alt f' + i],
                            tab: 'Home',
                            group: 'Navigation',
                            mixin: {
                                data: mountData,
                                item: item,
                                closeOnClick: false,
                                quick: true
                            },
                            onCreate: function (action) {
                                action.set('value', item.name);
                                action._oldIcon = this.icon;
                                action.actionType = types.ACTION_TYPE.SINGLE_TOGGLE;
                            }
                        });
                        result.push(_sourceAction);
                        i++;
                    }
                    _.each(mountData, createSourceAction);
                }
            }

            return result.concat(this.getEditorActions(permissions));
        },
        rename: function (item) {
            var dfd = new Deferred();
            var self = this,
                currentItem = item || this.getSelectedItem(),
                selection = this.getSelection(),
                currentFolder = this.getCurrentFolder(),
                startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).basename : '',
                collection = this.collection,
                defaultDfdArgs = {
                    select: selection,
                    focus: true,
                    append: false
                };

            var CIS = {
                inputs: [
                    utils.createCI('name', types.ECIType.STRING, startValue, {
                        widget: {
                            instant: true,
                            title: 'New Name',
                            validator: function (value) {
                                return collection.getSync(currentFolder.path + '/' + value) == null &&
                                    value.length > 0;
                            }
                        }
                    })
                ]
            };
            var dlg = new _CIDialog({
                cis: CIS,
                title: 'Rename',
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                bodyCSS: {
                    'height': 'auto',
                    'min-height': '80px',
                    'width': '100%',
                    'min-width': '400px'
                },
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {
                    var val = this.getField('name');
                    if (val == null) {
                        dfd.resolve(defaultDfdArgs);
                        return;
                    }
                    var currentFolder = self.getCurrentFolder(),
                        newFolder = val,
                        fileDfd = self.ctx.getFileManager().rename(currentItem.mount, currentItem.path, val, {
                            checkErrors: true,
                            returnProm: false
                        });

                    fileDfd.then(function (data) {

                        self.runAction(ACTION.RELOAD).then(function () {
                            defaultDfdArgs.select = currentFolder.path + '/' + val;
                            dfd.resolve(defaultDfdArgs);
                        });

                    }, function (e) {
                        logError(e, '__error renaming file!');
                        dfd.resolve(defaultDfdArgs);
                    });
                }
            });
            dlg.show();

            return dfd;

        },
        move: function (items) {
            var dfd = new Deferred();
            var self = this,
                currentItem = this.getSelectedItem(),
                selection = this.getSelection(),
                currentFolder = this.getCurrentFolder(),
                startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                collection = this.collection,

                defaultDfdArgs = {
                    select: selection,
                    focus: true,
                    append: false
                };

            //file selected
            if (!currentItem.directory) {
                var _parent = currentItem.getParent();
                if (_parent) {
                    currentItem = _parent;
                    startValue = currentItem ? currentItem.path : './';
                }
            }
            var fileWidgetValue = currentItem.path;
            var CIS = {
                inputs: [
                    utils.createCI('Name', types.ECIType.FILE, fileWidgetValue, {
                        widget: {
                            instant: true,
                            title: 'Target',
                            validator: function (value) {
                                return collection.getSync(currentFolder.path + '/' + value) == null &&
                                    value.length > 0;
                            }
                        }
                    })
                ]
            }

            var dlg = new _CIDialog({
                cis: CIS,
                title: self.localize('Move'),
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {

                    var val = this.getField('name');

                    if (val == null) {
                        dfd.resolve(defaultDfdArgs);
                        return;
                    }

                    var serverArgs = self._buildServerSelection(selection);
                    var dstPathItem = self.collection.getSync(val);
                    var dstPath = dstPathItem ? utils.normalizePath(dstPathItem.mount + '/' + dstPathItem.path) : '';

                    if (dstPathItem) {
                        serverArgs.dstPath = dstPath;
                    }

                    var options = defaultCopyOptions();
                    var currentFolder = self.getCurrentFolder(),
                        newFolder = val,
                        fileDfd = self.ctx.getFileManager().moveItem(serverArgs.selection, serverArgs.dstPath, options.includes, options.excludes, options.mode, {
                            checkErrors: true,
                            returnProm: false
                        });

                    fileDfd.then(function (data) {
                        self.runAction(ACTION.RELOAD).then(function () {
                            dfd.resolve(defaultDfdArgs);
                        });

                    }, function (e) {
                        logError(e, '__error creating file!');
                        dfd.resolve(defaultDfdArgs);
                    })
                }
            });
            dlg._on('widget', function (e) {
                e.widget._onSelect = function (w) {
                    return openFilePicker.apply(self, [dlg, w]);
                }
            });
            dlg.show();
            return dfd;
        },
        copy: function (items) {
            var dfd = new Deferred();
            var self = this,
                currentItem = this.getSelectedItem(),
                selection = this.getSelection(),
                currentFolder = this.getCurrentFolder(),
                startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                collection = this.collection,
                defaultDfdArgs = {
                    select: selection,
                    focus: true,
                    append: false
                };

            //file selected
            if (!currentItem.directory) {
                var _parent = currentItem.getParent();
                if (_parent) {
                    currentItem = _parent;
                    startValue = currentItem ? currentItem.path : './';
                }
            }
            var fileWidgetValue = currentItem.path;
            var CIS = {
                inputs: [
                    utils.createCI('Name', types.ECIType.FILE, fileWidgetValue, {
                        widget: {
                            instant: true,
                            title: 'Target',
                            validator: function (value) {
                                return collection.getSync(currentFolder.path + '/' + value) == null &&
                                    value.length > 0;
                            }
                        }
                    })
                ]
            }
            var dlgClass = dcl([FileOperationDialog, _CIDialog], {});
            var dlg = new dlgClass({
                cis: CIS,
                title: self.localize('Copy'),
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                bodyCSS: {
                    'height': 'auto',
                    'min-height': '80px',
                    'width': '100%',
                    'min-width': '400px'
                },
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {
                    var val = this.getField('name');
                    if (val == null) {
                        dfd.resolve(defaultDfdArgs);
                        return;
                    }
                    var msg = this.showMessage(),
                        thiz = this,
                        serverArgs = self._buildServerSelection(selection),
                        dstPathItem = self.collection.getSync(val),
                        dstPath = dstPathItem ? utils.normalizePath(dstPathItem.mount + '/' + dstPathItem.path) : '';

                    if (dstPathItem) {
                        serverArgs.dstPath = dstPath;
                    } else {
                        serverArgs.dstPath = utils.normalizePath(currentItem.mount + '/' + val);
                    }

                    var currentFolder = self.getCurrentFolder(),
                        newFolder = val,
                        fileDfd = self.ctx.getFileManager().copyItem(serverArgs.selection, serverArgs.dstPath, defaultCopyOptions(), {
                            checkErrors: true,
                            returnProm: false
                        });

                    fileDfd.then(function (result) {
                        thiz._onSuccess(result);
                    }, function (err) {
                        thiz._onError();
                    });

                    fileDfd.then(function (data) {
                        self.runAction(ACTION.RELOAD).then(function () {
                            dfd.resolve(defaultDfdArgs);
                        });

                    }, function (e) {
                        logError(e, '__error creating file!');
                        dfd.resolve(defaultDfdArgs);
                    })
                }
            });
            dlg._on('widget', function (e) {
                e.widget._onSelect = function (w) {
                    return openFilePicker.apply(self, [dlg, w]);
                };
            });
            dlg.show();
            return dfd;


        },
        clipboardPaste: function () {
            var dfd = new Deferred();
            var isCut = this.currentCutSelection,
                items = isCut ? this.currentCutSelection : this.currentCopySelection,
                serverParams = this._buildServerSelection(items),
                serverFunction = isCut ? 'moveItem' : 'copyItem',
                self = this,
                defaultDfdArgs = {
                    select: items,
                    focus: true,
                    append: false
                };
            if (!items) {
                dfd.resolve();
                return dfd;
            }
            var options = defaultCopyOptions();
            self.ctx.getFileManager()[serverFunction](serverParams.selection, serverParams.dstPath, {
                include: options.includes,
                exclude: options.excludes,
                mode: options.mode
            }).then(function (data) {
                self.runAction(types.ACTION.RELOAD).then(function () {
                    dfd.resolve(defaultDfdArgs);
                });
            }, function (err) {
                dfd.resolve(defaultDfdArgs);
            }, function (progress) {
                dfd.resolve(defaultDfdArgs);
            });
            return dfd;
        },
        /**
         * mkdir version:
         *
         *    1. open dialog with input box (value = currentItem)
         *
         * @TODO : remove leaks
         */
        mkdir: function () {
            var dfd = new Deferred();
            try {
                var self = this,
                    currentItem = this.getSelectedItem(),
                    currentFolder = this.getCurrentFolder(),
                    startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                    collection = this.collection;

                var CIS = {
                    inputs: [
                        utils.createCI('Name', 13, startValue, {
                            widget: {
                                instant: true,
                                validator: function (value) {
                                    if (currentFolder) {
                                        return collection.getSync(currentFolder.path + '/' + value) == null &&
                                            value.length > 0;
                                    } else {
                                        return true;
                                    }
                                }
                            }
                        })
                    ]
                },
                    defaultDfdArgs = {
                        select: currentItem,
                        focus: true,
                        append: false
                    };

                var dlg = new _CIDialog({
                    cis: CIS,
                    title: 'Create new Directory',
                    ctx: this.ctx,
                    size: types.DIALOG_SIZE.SIZE_NORMAL,
                    bodyCSS: {
                        'height': 'auto',
                        'min-height': '80px',
                        'width': '100%',
                        'min-width': '400px'
                    },
                    _onError: function (title, suffix, message) {
                        title = title || this.title;
                        message = message || this.notificationMessage;
                        message && message.update({
                            message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                            type: 'error',
                            actions: false,
                            duration: 15000
                        });
                        this.onError && this.onError(suffix);
                    },
                    onCancel: function () {
                        dfd.resolve();
                    },
                    onOk: function () {
                        var val = this.getField('name');
                        if (val == null) {
                            dfd.resolve(defaultDfdArgs);
                            return;
                        }
                        var currentFolder = self.getCurrentFolder() || { path: '.' },
                            newFolder = currentFolder.path + '/' + val;

                        var fileDfd = self.ctx.getFileManager().mkdir(collection.mount, newFolder, {
                            checkErrors: true,
                            returnProm: false
                        });

                        // call server::mkdir, then reload, then resolve dfd with new newFolder as selection
                        fileDfd.then(function (data) {
                            self.runAction(ACTION.RELOAD).then(function () {
                                collection.getSync(newFolder) && dfd.resolve({
                                    select: newFolder,
                                    append: false,
                                    focus: true
                                });
                            });
                        }, function (e) {
                            dfd.resolve(defaultDfdArgs);
                            logError(e, 'error creating directory');
                        })
                    }
                });

                dlg.show();

            } catch (e) {
                logError(e, 'error creating dialog');
            }
            return dfd;

        },
        _buildServerSelection: function (items, dst) {

            !dst && (dst = this.getCurrentFolder());

            //normalize
            if (dst && !dst.directory) {
                dst = dst.getParent();
            }

            if (!_.isArray(items)) {
                items = [items];
            }

            //basics
            var selection = [],
                store = items[0].getStore(),
                dstPath = dst ? utils.normalizePath(dst.mount + '/' + dst.path) : '';

            //build selection
            _.each(items, function (item) {
                var _storeItem = store.getSync(item.path);
                if (_storeItem) {
                    selection.push(utils.normalizePath(item.mount + '/' + item.path));
                }
            });


            //compose output
            return {
                selection: selection,
                store: items[0]._S,
                dstPath: '/' + dstPath + '/',
                firstItem: items[0],
                parent: dst
            }
        },
        openPreview: function (item, parent) {

            var row = this.row(item);

            var el = row.element,
                self = this,
                dfd = new Deferred();

            if (this._preview) {
                this._preview.item = item;
                var _dfd = this._preview.open().then(function () {
                    self._preview.preview.trigger($.Event('update', { file: item }));
                });

                return _dfd;
            }


            var _prev = new FilePreview({
                node: $(el),
                item: item,
                delegate: this.ctx.getFileManager(),
                parent: $(el),
                ctx: self.ctx,
                container: parent ? parent.containerNode : null
            });

            parent ? _prev.buildRenderingEmbedded() : _prev.buildRendering();
            _prev.init();
            _prev.exec();
            this._preview = _prev;
            this._preview.handler = this;
            self._on('selectionChanged', function (e) {
                var _item = self.getSelectedItem();
                if (_item) {

                    _prev.item = _item;
                    _prev.preview.trigger($.Event('update', { file: _item }));
                }
            });

            this.add(_prev, null, false);
            _prev._emit('changeState', function (state) {
                if (state === 0) {
                    dfd.resolve({
                        select: item,
                        focus: true,
                        append: false
                    });
                }
            });

            return dfd;

        },
        deleteSelection: function (selection) {
            selection = selection || this.getSelection();
            var dfd = new Deferred(),
                _next = this.getNext(selection[0], null, true),
                _prev = this.getPrevious(selection[0], null, true),

                next = _next || _prev,
                serverParams = this._buildServerSelection(selection),
                dlgClass = FileOperationDialog,
                title = 'Delete ' + serverParams.selection.length + ' ' + 'items',
                thiz = this;

            var dlg = new dlgClass({
                ctx: thiz.ctx,
                notificationMessage: null,
                title: title,
                type: types.DIALOG_TYPE.DANGER,
                onBeforeOk: function () {

                },
                getOkDfd: function () {
                    var thiz = this;
                    return this.ctx.getFileManager().deleteItems(serverParams.selection, {
                        hints: [1]
                    }, {
                            checkErrors: false,
                            returnProm: false,
                            onError: function (err) {
                                thiz._onError(null, err.message);
                            }
                        });
                },
                onCancel: function () {
                    dfd.resolve({
                        select: selection,
                        focus: true,
                        append: false
                    });
                },
                onSuccess: function () {
                    thiz.runAction(types.ACTION.RELOAD).then(function () {
                        dfd.resolve({
                            select: next,
                            focus: true,
                            append: false
                        });
                    });
                }
            });
            dlg.show();
            return dfd;
        },
        /**
         *
         * @param item {module:xfile/model/File}
         */
        goUp: function (item) {
            item = this._history.getNow();
            _debug &&  0 && console.log('go up ' + item);
            item = _.isString(item) ? this.collection.getSync(item) : item;
            var dfd = new Deferred(),
                self = this;
            var history = this.getHistory();
            var prev = history.getNow();


            if (!item) {
                var rows = this.getRows();
                if (rows[0] && rows[0]._S) {
                    var _parent = rows[0]._S.getParent(rows[0]);
                    if (_parent) {
                        _parent = _parent._S.getParent(_parent);
                        if (_parent) {
                            item = _parent;
                        }
                    }
                }
            }

            if (item) {
                //current folder:
                var _parent = item._S.getParent(item);
                if (_parent) {
                    var _dfd = this.openFolder(_parent, true, false);
                    _dfd && _dfd.then && _dfd.then(function () {
                        if (prev) {
                            //history.pop();
                            var select = self.collection.getSync(prev);
                            if (select) {
                                dfd.resolve({
                                    select: select,
                                    focus: true,
                                    delay: 1
                                });
                            } else {

                                 0 && console.warn('cant find back item ' + prev);
                            }
                        } else {
                             0 && console.warn('cant find back item ' + prev + ' ' + item.path);
                        }
                    });
                }
            } else {
                if (prev) {
                    var select = this.collection.getSync(prev);
                    if (select) {
                        this.select([select], null, true, {
                            focus: true,
                            delay: 1
                        });
                    }
                } else {
                    var rows = this.getRows();
                    if (rows[0] && rows[0]._S) {
                        var _parent = rows[0]._S.getParent(rows[0]);
                        if (_parent) {
                            _parent = _parent._S.getParent(_parent);
                            if (_parent) {
                                this.openFolder(_parent, true);
                            }
                        }
                    }
                }
            }
            return dfd;
        },
        reload: function (item) {
            var dfd = new Deferred(),
                selection = this.getSelection();

            item = item || this.getRows()[0];

            //item could be a non-store item:
            var cwd = /*item.getParent ? item.getParent() : */this.getCurrentFolder() || { path: '.' },
                self = this;

            if (cwd.isBack) {
                cwd = this.collection.getSync(cwd.rPath) || {path:'.'};
            }
            var expanded = false;
            if (this.isExpanded && this.expand && item) {
                expanded = this._isExpanded(item);
                if (expanded) {
                    this.expand(item, null, false);
                }
            }
            self.collection.resetQueryLog();
            this.collection.loadItem(cwd, true).then(function (what) {
                if (cwd.path === '.') {
                    self.collection.resetQueryLog();
                }
                self.refresh().then(function () {
                    var _dfd = self.openItem(cwd, false, false);
                    if (_dfd && _dfd.then) {
                        _dfd.then(function () {
                            dfd.resolve({
                                select: selection,
                                append: false,
                                focus: true,
                                delay: 1
                            });
                            return;
                        });
                    }
                    self.deselectAll();
                    dfd.resolve({
                        select: selection,
                        append: false,
                        focus: true,
                        delay: 2
                    });
                });
            });
            return dfd;
        },
        /**
         * Opens folder
         * @param item {module:xfile.model.File}
         * @param isBack
         */
        toHistory: function (item) {
            var FolderPath = item.getFolder ? new Path(item.getFolder()) : new Path('.');
            var segs = FolderPath.getSegments();
            var addDot = this.collection.addDot;
            var root = this.collection.rootSegment;
            //var _last = addDot ? '.' : "";
            var _last = root;
            if (segs && segs[0] === '.') {
                segs[0] = root;
            }
            var out = [];

            _.each(segs, function (seg) {
                var segPath = seg !== _last ? _last + (_last.endsWith("/") ? "" : "/") + seg : seg;
                out.push(segPath);
                _last = segPath;
            });

            return out;

        },
        openFolder: function (item, isBack, select) {
            var cwd = this.getCurrentFolder(),
                store = this.collection,
                history = this.getHistory(),
                now = history.getNow(),
                prev = history.getNow(),
                self = this;

            if (_.isString(item)) {
                item = store.getSync(item);
            } else {
                item = store.getSync(item.path) || item;
            }

            if (!item) {
                 0 && console.warn('openFolder: no item! set to root:');
                item = store.getRootItem();
            }

            _debug &&  0 && console.log('open folder ' + item.path + ' cwd : ' + this.getCurrentFolder().path);
            var _hItems = self.toHistory(item);

            _debug &&  0 && console.log('history:\n' + _hItems.join('\n'));
            history.set(_hItems);
            if (!item) {
                return this.setQueryEx(store.getRootItem());
            }

            if (cwd && item && isBack && cwd.path === item.path || !item) {

                return;
            }

            this.deselectAll();
            var row = this.row(item),
                node = row ? row.element : null,
                loaded = item._S.isItemLoaded(item),
                iconNode;
            if (!loaded) {
                var _els = $(node).find("span.fa");
                if (_els && _els[0]) {
                    iconNode = $(_els[0]);
                }
                if (iconNode) {
                    iconNode.removeClass('fa fa-folder');
                    iconNode.addClass('fa-spinner fa-spin');

                }
            }
            var head = new Deferred();
            this._emit('openFolder', {
                item: item,
                back: isBack
            });

            var dfd = this.setQueryEx(item, null);
            dfd && dfd.then(function () {
                //remove spinner
                if (iconNode) {
                    iconNode.addClass('fa fa-folder');
                    iconNode.removeClass('fa-spinner fa-spin');
                }
                head.resolve({
                    select: select !== false ? (isBack ? item : self.getRows()[0]) : null,
                    focus: true,
                    append: false,
                    delay: 1
                });
                self._emit('openedFolder', {
                    item: item,
                    back: isBack
                });
            });

            return head;
        },
        changeSource: function (mountData, silent) {
            var dfd = new Deferred(),
                thiz = this,
                ctx = thiz.ctx || ctx,
                fileManager = ctx.getFileManager(),
                store = factory.createFileStore(mountData.name, null, fileManager.config, null, ctx),
                oldStore = this.collection,
                sourceAction = this.getAction(ACTION.SOURCE),
                label = mountData.label || mountData.name,
                mountAction = this.getAction(ACTION.SOURCE + '/' + label);

            oldStore.destroy();

            silent !== true && this._emit('changeSource', mountData);
            this.set('loading', true);
            this.set('collection', store.getDefaultCollection('./'));
            sourceAction.set('value', mountData.name);
            thiz.set('title', 'Files (' + mountData.label + ')');
            var sourceActions = sourceAction.getChildren();
            _.each(sourceActions, function (child) {
                child.set('icon', child._oldIcon);
            });

            sourceAction.set('label', label);
            mountAction && mountAction.set('icon', 'fa-spinner fa-spin');
            this.refresh().then(function () {
                thiz.set('loading', false);
                thiz.set('collection', store.getDefaultCollection('./'));
                mountAction && mountAction.set('icon', 'fa-check');
                silent !== true && thiz._emit('changedSource', mountData);
                thiz.select([0], null, true, {
                    append: false,
                    focus: true
                }).then(function () {
                    dfd.resolve();
                });
            });
            return dfd;

        },
        openInNewTab: function (item) {
            var thiz = this,
                ctx = thiz.ctx,
                wManager = ctx.getWindowManager(),
                dfd = new Deferred(),
                tab = null;

            if (_.isFunction(this.newTarget)) {
                tab = this.newTarget({
                    title: item.name,
                    icon: 'fa-folder',
                    target: thiz._parent,
                    location: null,
                    tabOrientation: null
                });

            } else {
                tab = wManager.createTab(item.name, 'fa-folder', this.newTarget || this);
            }
            var _store = this.collection;
            if (!item.isDir) {
                item = this.getCurrentFolder();
            }
            var store = factory.createFileStore(_store.mount, _store.options, _store.config, null, ctx);
            store.micromatch = _store.micromatch;

            var args = utils.mixin({
                showToolbar: this.showToolbar,
                collection: store,
                selectedRenderer: this.selectedRenderer,
                showHeader: this.showHeader,
                newTarget: this.newTarget,
                style: this.style,
                options: utils.clone(this.options),
                permissions: this.permissions,
                _columns: this._columns,
                attachDirect: true,
                    registerEditors: this.registerEditors,
                    hideExtensions: this.hideExtensions
            }, this.newTabArgs || {}),

                grid = utils.addWidget(this.getClass(), args, null, tab, false);

            dfd.resolve();
            grid.set('loading', true);
            store.initRoot().then(function (d) {
                var _dfd = store.getItem(item.path, true);
                _dfd && _dfd.then(function () {
                    grid.startup();
                    grid._showHeader(thiz.showHeader);
                    grid.openItem(item).then(function () {
                        grid.set('loading', false);
                        grid.select([0], null, true, {
                            focus: true,
                            append: false,
                            delay: 1
                        });

                    });
                    wManager.registerView(grid, true);
                });
            });
            return dfd;
        },
        openItem: function (item, isBack, select) {
            if (!item) {
                var _rows = this.getRows();
                if (_rows[0] && _rows[0].isBack) {
                    item = _rows[0];
                }
            }

            if (item && item.isBack) {
                item._S = null;
                return this.goUp();
            }

            item = _.isString(item) ? this.collection.getSync(item) : item;
            if (!item) {
                return;
            }
            if (item.directory === true) {
                return this.openFolder(item, isBack, select);
            } else {
                var editors = Registry.getEditors(item);
                var defaultEditor = _.find(editors, function (editor) {
                    return editor.isDefault === true;
                });
                if (defaultEditor) {
                    return defaultEditor.onEdit(item, this);
                }
                return this.ctx.getWindowManager().openItem(item, this.newTarget || this, {
                    register: true
                });

            }
        },
        getSelectedItem: function () {
            return this.getSelection()[0];
        },
        openDefaultEditor: function (item) {
            return Default.Implementation.open(item);
        },
        close: function () {
            var panel = this._parent;
            if (panel) {
                var docker = panel.docker();
                if (docker) {
                    this.onAfterAction = null;
                    docker.removePanel(panel);
                }
            }
            return false;
        },
        compress: function (items) {
            items = items || this.getSelection();
            var thiz = this;
            var serverParams = this._buildServerSelection(items);
            if (serverParams && serverParams.store && serverParams.selection) {
                thiz.ctx.getFileManager().compressItem(serverParams.firstItem.mount, serverParams.selection, 'zip').then(function (args) {
                    thiz.reload();
                });
            }

        },
        newScene: function () {
            var dfd = new Deferred();
            try {
                var self = this,
                    currentItem = this.getSelectedItem(),
                    currentFolder = this.getCurrentFolder(),
                    startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                    collection = this.collection;

                startValue = 'my new scene.dhtml';

                var CIS = {
                        inputs: [
                            utils.createCI('Name', 13, startValue, {
                                widget: {
                                    instant: true,
                                    validator: function (value) {
                                        if (currentFolder) {
                                            return collection.getSync(currentFolder.path + '/' + value) == null &&
                                                value.length > 0;
                                        } else {
                                            return true;
                                        }
                                    }
                                }
                            })
                        ]
                    },
                    defaultDfdArgs = {
                        select: currentItem,
                        focus: true,
                        append: false
                    };

                var dlg = new _CIDialog({
                    cis: CIS,
                    title: 'Create new scene',
                    ctx: this.ctx,
                    size: types.DIALOG_SIZE.SIZE_SMALL,
                    bodyCSS: {
                        'height': 'auto',
                        'min-height': '80px'

                    },
                    _onError: function (title, suffix, message) {
                        title = title || this.title;
                        message = message || this.notificationMessage;
                        message && message.update({
                            message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                            type: 'error',
                            actions: false,
                            duration: 15000
                        });
                        this.onError && this.onError(suffix);
                    },
                    onCancel: function () {
                        dfd.resolve();
                    },
                    onOk: function () {
                        var val = this.getField('name');
                        if (val == null) {
                            dfd.resolve(defaultDfdArgs);
                            return;
                        }
                        var currentFolder = self.getCurrentFolder() || {
                                path: '.'
                            },
                            newFolder = currentFolder.path + '/' + val;


                        var fileDfd = self.ctx.getFileManager().mkfile(collection.mount, newFolder, newscene, {
                            checkErrors: true,
                            returnProm: false
                        });

                        // call server::mkdir, then reload, then resolve dfd with new newFolder as selection
                        fileDfd.then(function (data) {
                            self.runAction(ACTION.RELOAD).then(function () {
                                collection.getSync(newFolder) && dfd.resolve({
                                    select: newFolder,
                                    append: false,
                                    focus: true
                                });
                            });
                        }, function (e) {
                            dfd.resolve(defaultDfdArgs);
                            logError(e, 'error creating directory');
                        });
                    }
                });
                dlg.show();
            } catch (e) {
                logError(e, 'error creating dialog');
            }
            return dfd;
        },
        runAction: function (action, _item) {
            _.isString(action) && (action = this.getAction(action));
            if (!action || !action.command) {
                 0 && console.warn('invalid action');
                return;
            }
            var ACTION_TYPE = types.ACTION,
                item = this.getSelectedItem() || _item,
                sel = this.getSelection();

            if (action.command.indexOf(ACTION.SOURCE) != -1) {
                return this.changeSource(action.item);
            }
            switch (action.command) {

                case 'File/Compress':
                    {
                        return this.compress(sel);
                    }
                case ACTION_TYPE.PREVIEW:
                    {
                        return this.openPreview(item);
                    }
                case 'File/OpenInNewTab':
                    {
                        return this.openInNewTab(item || this.collection.getRootItem());
                    }
                case ACTION_TYPE.EDIT:
                    {
                        return this.openItem(item);
                    }
                case 'File/New Scene':
                    {
                        return this.newScene(item);
                    }
                case ACTION_TYPE.NEW_FILE:
                    {
                        return this.touch(item);
                    }
                case ACTION_TYPE.NEW_DIRECTORY:
                    {
                        return this.mkdir(item);
                    }
                case ACTION_TYPE.RELOAD:
                    {
                        return this.reload(item);
                    }
                case ACTION_TYPE.RENAME:
                    {
                        return this.rename(item);
                    }
                case ACTION_TYPE.GO_UP:
                    {
                        return this.goUp(null);
                    }
                case ACTION_TYPE.COPY:
                    {
                        return this.copy(null);
                    }
                case ACTION_TYPE.MOVE:
                    {
                        return this.move(null);
                    }
                case ACTION_TYPE.DELETE:
                    {
                        return this.deleteSelection(null);
                    }
                case ACTION_TYPE.OPEN_IN + '/Default Editor':
                    {
                        return this.openDefaultEditor(item);
                    }
                case ACTION_TYPE.DOWNLOAD:
                    {
                        return this.ctx.getFileManager().download(item);
                    }
                case ACTION_TYPE.CLOSE:
                    {
                        return this.close();
                    }
                case 'File/OpenInOS':
                    {
                        return this.openInOS(item);
                    }
            }

            if (action.command.indexOf(ACTION_TYPE.OPEN_IN + '/') != -1) {
                return action.editor.onEdit(item);
            }

            return this.inherited(arguments);
        },

        getEditorActions: function (permissions) {
            permissions = permissions || this.permissions;
            var result = [],
                ACTION = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                VISIBILITY = types.ACTION_VISIBILITY,
                thiz = this,
                ctx = thiz.ctx,
                container = thiz.domNode,
                actionStore = thiz.getActionStore(),
                openInAction = null,
                openInActionModel = null,
                dirty = false,
                selHandle = null;

            function getItem() {
                return thiz.getSelection()[0];
            }

            function selHandler(event) {

                var selection = event.selection;
                if (!selection || !selection[0]) {
                    return;
                }
                var item = selection[0];
                var permissions = this.permissions;
                var ACTION = types.ACTION,
                    ACTION_ICON = types.ACTION_ICON,
                    VISIBILITY = types.ACTION_VISIBILITY,
                    thiz = this,
                    container = thiz.domNode,
                    actionStore = thiz.getActionStore(),
                    contextMenu = this.getContextMenu ? this.getContextMenu() : null;

                function _wireEditor(editor, action) {
                    action.handler = function () {
                        editor.onEdit(thiz.getSelection()[0]);
                    };
                }
                function getEditorActions(item) {
                    var editors = Registry.getEditors(item) || [],
                        result = [];

                    for (var i = 0; i < editors.length; i++) {
                        var editor = editors[i];
                        if (editor.name === 'Default Editor') {
                            continue;
                        }
                        var editorAction = thiz.createAction(editor.name, ACTION.OPEN_IN + '/' + editor.name, editor.iconClass, null, 'Home', 'Open', 'item', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'Home',
                                editor: editor,
                                custom: true,
                                quick: true
                            }, null, null, permissions, container, thiz
                        );
                        _wireEditor(editor, editorAction);
                        result.push(editorAction);
                        if (editor.editorClass && editor.editorClass.getFileActions) {
                            result = result.concat(editor.editorClass.getFileActions(ctx, item));
                        }
                    }
                    return result;
                }
                if (event.why == 'deselect') {
                    return;
                }
                var action = actionStore.getSync(types.ACTION.OPEN_IN);
                var editorActions = thiz.addActions(getEditorActions(item));

                if (this._lastEditorActions) {
                    _.each(this._lastEditorActions, function (action) {
                        actionStore.removeSync(action.command);
                    });
                    delete this._lastEditorActions;
                }

                contextMenu && contextMenu.removeCustomActions();

                if (editorActions.length > 0) {
                    var newStoredActions = this.addActions(editorActions);
                    actionStore._emit('onActionsAdded', newStoredActions);
                    this._lastEditorActions = newStoredActions;
                }
            }

            if (!this._selHandle) {
                this._selHandle = this._on('selectionChanged', function (evt) {
                    selHandler.apply(thiz, [evt]);
                });
            }
            openInAction = this.createAction('Open In', ACTION.OPEN_IN, ACTION_ICON.EDIT, null, 'Home', 'Open', 'item',
                null,
                function () {
                },
                {
                    addPermission: true,
                    tab: 'Home',
                    quick: true
                }, null, DefaultActions.shouldDisableDefaultFileOnly, permissions, container, thiz
            );

            result.push(openInAction);
            var e = thiz.createAction('Default Editor', ACTION.OPEN_IN + '/Default Editor', 'fa-code', null, 'Home', 'Open', 'item', null,
                function () {
                },
                {
                    addPermission: true,
                    tab: 'Home',
                    forceSubs: true,
                    quick: true

                }, null, null, permissions, container, thiz
            );
            result.push(e);
            return result;
        }
    };

    //package via declare
    var _class = declare('xfile.FileActions', null, Implementation);
    _class.Implementation = Implementation;
    _class.createFilePicker = createFilePicker;
    return _class;

});
},
'xfile/views/FileOperationDialog':function(){
/** @module xfile/views/FileOperationDialog **/
define([
    "dcl/dcl",
    'xide/types',
    "xide/views/_Dialog"
], function (dcl, types, _Dialog) {


    var Module = dcl(_Dialog, {

        title: '',
        type: types.DIALOG_TYPE.INFO,
        size: types.DIALOG_SIZE.SIZE_SMALL,
        bodyCSS: {},
        failedText: ' Failed!',
        successText: ': Success!',
        showSpinner: true,
        spinner: '  <span class="fa-spinner fa-spin"/>',
        notificationMessage: null,
        doOk: function (dfd) {

            this.onBeforeOk && this.onBeforeOk();

            var msg = this.showMessage(),
                thiz = this;

            dfd.then(function (result) {
                thiz._onSuccess(result);
            }, function (err) {
                thiz._onError();
            });

        },
        _onSuccess: function (title, suffix, message) {

            title = title || this.title;
            message = message || this.notificationMessage;

            message && message.update({
                message: title + this.successText + (suffix ? '<br/>' + suffix : ''),
                type: 'info',
                actions: false,
                duration: 1500
            });

            this.onSuccess && this.onSuccess();
        },
        _onError: function (title, suffix, message) {

            title = title || this.title;

            message = message || this.notificationMessage;

            message && message.update({
                message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                type: 'error',
                actions: false,
                duration: 15000
            });


            this.onError && this.onError(suffix);

        },
        onOk: function () {

            var msg = this.showMessage(),
                thiz = this;
            this.doOk(this.getOkDfd());
        },
        showMessage: function (title) {

            if (this.notificationMessage) {
                return this.notificationMessage;
            }
            title = title || this.title;

            var msg = this.ctx.getNotificationManager().postMessage({
                message: title + (this.showSpinner ? this.spinner : ''),
                type: 'info',
                showCloseButton: true,
                duration: 4500
            });

            this.notificationMessage = msg;

            return msg;
        }

    });

    return Module;

});
},
'xfile/views/FilePreview':function(){
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "xide/mixins/EventedMixin",
    "xide/widgets/_Widget",
    "dojo/cache",
    "dojo/Deferred",
    "dijit/registry",
    "xide/editor/Default",
    "xaction/DefaultActions"
], function (dcl, types,
             utils, EventedMixin,
             _Widget, cache, Deferred, registry, Default, DefaultActions) {

    var plugins = [
        /**
         * Images preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var mimes = ['image/jpeg', 'image/png', 'image/gif'],
                preview = ql.preview;

            // what kind of images we can display
            $.each(navigator.mimeTypes, function (i, o) {
                var mime = o.type;

                if (mime.indexOf('image/') === 0 && $.inArray(mime, mimes)) {
                    mimes.push(mime);
                }
            });

            preview.bind('update', function (e) {
                var file = e.file,
                    img;
                if (!ql.opened()) {
                    return;
                }

                if ($.inArray(file.mime, mimes) !== -1) {
                    // this is our file - stop event propagation
                    e.stopImmediatePropagation();
                    var params = null;
                    if (ql.resizeToPreview) {
                        params = {
                            width: preview.width()
                        }
                    }
                    img = $('<img/>')
                        .hide()
                        .appendTo(preview)
                        .load(function () {
                            // timeout - because of strange safari bug -
                            // sometimes cant get image height 0_o
                            setTimeout(function () {
                                var prop = (img.width() / img.height()).toFixed(2);
                                preview.bind('changesize', function () {
                                    var pw = parseInt(preview.width()),
                                        ph = parseInt(preview.height()),
                                        w, h;

                                    if (prop < (pw / ph).toFixed(2)) {
                                        h = ph;
                                        w = Math.floor(h * prop);
                                    } else {
                                        w = pw;
                                        h = Math.floor(w / prop);
                                    }
                                    img.width(w).height(h).css('margin-top', h < ph ? Math.floor((ph - h) / 2) : 0);

                                }).trigger('changesize');

                                // hide info/icon
                                ql.hideinfo();
                                //show image
                                img.fadeIn(100);

                            }, 1)
                        });
                    img.attr('src', ql.fm.url(file, null, params));
                }
            });
        },

        /**
         * HTML preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var mimes = ['text/html', 'application/xhtml+xml'],
                preview = ql.preview,
                fm = ql.fm;
            preview.bind('update', function (e) {
                var file = e.file, jqxhr;

                if (!ql.opened()) {
                    return;
                }

                if ($.inArray(file.mime, mimes) !== -1) {
                    e.stopImmediatePropagation();
                    var ctx = ql.ctx;
                    var fm = ctx.getFileManager();
                    fm.getContent(file.mount, file.path, function (content) {
                        ql.hideinfo();
                        doc = $('<iframe class="elfinder-quicklook-preview-html"/>').appendTo(preview)[0].contentWindow.document;
                        doc.open();
                        doc.write(content);
                        doc.close();
                    });
                }
            })
        },

        /**
         * Texts preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var fm = ql.fm,
                preview = ql.preview;


            var mimes = [
                'application/x-empty',
                'application/javascript',
                'application/xhtml+xml',
                'audio/x-mp3-playlist',
                'application/x-web-config',
                'application/docbook+xml',
                'application/x-php',
                'application/x-perl',
                'application/x-awk',
                'application/x-config',
                'application/x-csh',
                'application/xml',
                'application/x-empty',
                'text/html',
                'text/x-c',
                'text/x-php',
                'text/plain',
                'text/x-c++',
                'text/x-lisp'
            ];

            preview.bind('update', function (e) {
                var file = e.file,
                    mime = file.mime,
                    jqxhr;

                if (!ql.opened()) {
                    return;
                }
                if (mime.indexOf('text/') === 0 || $.inArray(mime, mimes) !== -1) {
                    e.stopImmediatePropagation();
                    if (ql.useAce) {
                        ql.hideinfo();
                        var _node = $('<div class="elfinder-quicklook-preview-text-wrapper"></div>');

                        _node.appendTo(preview);

                        var editor = ql._editor,
                            wasCached = editor;

                        if (editor) {

                        } else {
                            editor = Default.Implementation.open(file, _node[0], ql.editorOptions, false, ql);
                            ql._editor = editor;
                            //add to _widgets
                            ql.add(editor, null, false);

                            if (DefaultActions.hasAction(ql.editorOptions.permissions, types.ACTION.TOOLBAR)) {
                                var toolbar = editor.getToolbar();
                                if (toolbar) {
                                    $(toolbar.domNode).addClass('bg-opaque');
                                }
                            }
                        }
                        if (wasCached) {
                            _node.append(editor.domNode);
                            editor.set('item', file);
                        }

                        preview.bind('changesize', function () {
                            var pw = parseInt(preview.width()),
                                ph = parseInt(preview.height());
                            utils.resizeTo(editor.domNode, preview[0], true, true);
                            editor.resize();
                        });
                    }
                }
            });
        },
        /**
         * PDF preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var fm = ql.fm,
                mime = 'application/pdf',
                preview = ql.preview,
                active = false;

            active = false;
            var isActive = false;
            if (isActive) {
                active = true;
            } else {
                $.each(navigator.plugins, function (i, plugins) {
                    $.each(plugins, function (i, plugin) {
                        if (plugin.type == mime) {
                            return !(active = true);
                        }
                    });
                });
            }
            active && preview.bind('update', function (e) {
                var file = e.file, node;
                if (!ql.opened()) {
                    return;
                }

                if (file.mime == mime) {
                    e.stopImmediatePropagation();
                    preview.one('change', function () {
                        node.unbind('load').remove();
                    });

                    node = $('<iframe class="elfinder-quicklook-preview-pdf"/>')
                        .hide()
                        .appendTo(preview)
                        .load(function () {
                            ql.hideinfo();
                            node.show();
                        })
                        .attr('src', ql.fm.url(file));
                }
            })
        },

        /**
         * Flash preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var fm = ql.fm,
                mime = 'application/x-shockwave-flash',
                preview = ql.preview,
                active = false;

            $.each(navigator.plugins, function (i, plugins) {
                $.each(plugins, function (i, plugin) {
                    if (plugin.type == mime) {
                        return !(active = true);
                    }
                });
            });

            active && preview.bind('update', function (e) {
                var file = e.file,
                    node;
                if (!ql.opened()) {
                    return;
                }
                if (file.mime == mime) {
                    e.stopImmediatePropagation();
                    ql.hideinfo();
                    preview.append((node = $('<embed class="elfinder-quicklook-preview-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="' + fm.url(file) + '" quality="high" type="application/x-shockwave-flash" />')));
                }
            });
        },
        /**
         * HTML5 audio preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var preview = ql.preview,
                autoplay = !!ql.options['autoplay'],
                mimes = {
                    'audio/mpeg': 'mp3',
                    'audio/mpeg3': 'mp3',
                    'audio/mp3': 'mp3',
                    'audio/x-mpeg3': 'mp3',
                    'audio/x-mp3': 'mp3',
                    'audio/x-wav': 'wav',
                    'audio/wav': 'wav',
                    'audio/x-m4a': 'm4a',
                    'audio/aac': 'm4a',
                    'audio/mp4': 'm4a',
                    'audio/x-mp4': 'm4a',
                    'audio/ogg': 'ogg'
                },
                node;

            preview.bind('update', function (e) {
                if (!ql.opened()) {
                    return;
                }
                var file = e.file,
                    type = mimes[file.mime];

                if (ql.support.audio[type]) {
                    e.stopImmediatePropagation();

                    node = $('<audio class="elfinder-quicklook-preview-audio" controls preload="auto" autobuffer><source src="' + ql.fm.url(file) + '" /></audio>')
                        .appendTo(preview);
                    autoplay && node[0].play();
                }
            }).bind('change', function () {
                if (node && node.parent().length) {
                    node[0].pause();
                    node.remove();
                    node = null;
                }
            });
        },

        /**
         * HTML5 video preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {

            var preview = ql.preview,
                autoplay = !!ql.options['autoplay'],
                mimes = {
                    'video/mp4': 'mp4',
                    'video/x-m4v': 'mp4',
                    'video/ogg': 'ogg',
                    'application/ogg': 'ogg',
                    'video/webm': 'webm'
                },
                node;

            preview.bind('update', function (e) {
                if (!ql.opened()) {
                    return;
                }
                var file = e.file,
                    type = mimes[file.mime];

                if (ql.support.video[type]) {
                    e.stopImmediatePropagation();

                    ql.hideinfo();
                    node = $('<video class="elfinder-quicklook-preview-video" controls preload="auto" autobuffer><source src="' + ql.fm.url(file) + '" /></video>').appendTo(preview);
                    autoplay && node[0].play();

                }
            }).bind('change', function () {
                if (node && node.parent().length) {
                    node[0].pause();
                    node.remove();
                    node = null;
                }
            });
        },
        /**
         * Audio/video preview plugin using browser plugins
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var preview = ql.preview,
                mimes = [],
                node;

            if (!ql.opened()) {
                return;
            }

            $.each(navigator.plugins, function (i, plugins) {
                $.each(plugins, function (i, plugin) {
                    (plugin.type.indexOf('audio/') === 0 || plugin.type.indexOf('video/') === 0) && mimes.push(plugin.type);
                });
            });

            preview.bind('update', function (e) {
                if (!ql.opened()) {
                    return;
                }
                var file = e.file,
                    mime = file.mime,
                    video;
                if ($.inArray(file.mime, mimes) !== -1) {
                    e.stopImmediatePropagation();
                    (video = mime.indexOf('video/') === 0) && ql.hideinfo();
                    node = $('<embed src="' + ql.fm.url(file) + '" type="' + mime + '" class="elfinder-quicklook-preview-' + (video ? 'video' : 'audio') + '"/>')
                        .appendTo(preview);
                }
            }).bind('change', function () {
                if (node && node.parent().length) {
                    node.remove();
                    node = null;
                }
            });
        }
    ];
    /**
     * window closed state
     *
     * @type Number
     **/
    var closed = 0,
        /**
         * window opened state
         *
         * @type Number
         **/
        opened = 2,
        /**
         * window animated state
         *
         * @type Number
         **/
        animated = 1;

    var previewClass = dcl([_Widget.dcl, EventedMixin.dcl], {
        resizeToPreview: true,
        useAce: true,
        registerEditors: false,
        editorPermissions: [],
        container: null,
        destroy: function () {
            this.window.empty();
            utils.destroy(this.window[0]);
            this.inherited(arguments);
        },
        /**
         * Init command.
         * Add default plugins and init other plugins
         *
         * @return Object
         **/
        init: function () {
            this.options = {
                autoplay: true,
                jplayer: "extensions/jplayer",
                ui: 'button'
            };
            var o = this.options,
                self = this,
                win = this.window,
                preview = this.preview,
                container = this.container,
                i, p;

            width = o.width > 0 ? parseInt(o.width) : 450;
            height = o.height > 0 ? parseInt(o.height) : 300;
            if (!container) {
                win.appendTo('body').zIndex(20);
            }
            // close window on escape
            $(document).keydown(function (e) {
                e.keyCode == 27 && self.opened() && win.trigger('close')
            });

            if (!container && $.fn.resizable) {
                win.resizable({
                    handles: 'se',
                    minWidth: 350,
                    minHeight: 120,
                    resize: function () {
                        // use another event to avoid recursion in fullscreen mode
                        // may be there is clever solution, but i cant find it :(
                        preview.trigger('changesize');
                    }
                });
            }
            $.each(plugins || [], function (i, plugin) {
                if (typeof(plugin) == 'function') {
                    new plugin(self)
                }
            });

            preview.bind('update', function () {
                self.info.show();
            });
        },
        buildRenderingEmbedded: function () {
            var self = this,
                navicon = this.navicon,
                container = this.container ? $(this.container) : null,
                title = $('<div class="elfinder-quicklook-title" style="display: none">' + self.title + '</div>'),
                icon = $('<div/>'),
                info = $('<div class="elfinder-quicklook-info"/>');//.hide(),


            self.openDfd = new Deferred();

            self.fsicon = $('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrows-alt"/>')
                .mousedown(function (e) {
                    var win = self.window,
                        full = win.is('.' + self.fullscreen),
                        scroll = "scroll.elfinder-finder",
                        $window = $(window);

                    e.stopPropagation();

                    if (full) {
                        win.css(win.data('position')).unbind('mousemove');
                        $window.unbind(scroll).trigger(self.resize).unbind(self.resize);
                        self.navbar.unbind('mouseenter').unbind('mousemove');
                    } else {

                        win.data('position', {
                            left: win.css('left'),
                            top: win.css('top'),
                            width: win.width(),
                            height: win.height()
                        }).css({
                            width: '100%',
                            height: '100%'
                        });

                        $(window).bind(scroll, function () {
                                win.css({
                                    left: parseInt($(window).scrollLeft()) + 'px',
                                    top: parseInt($(window).scrollTop()) + 'px'
                                })
                            })
                            .bind(self.resize, function (e) {
                                self.preview.trigger('changesize');
                            })
                            .trigger(scroll)
                            .trigger(self.resize);

                        win.bind('mousemove', function (e) {
                                self.navbar.stop(true, true).show().delay(3000).fadeOut('slow');
                            })
                            .mousemove();

                        self.navbar.mouseenter(function () {
                                self.navbar.stop(true, true).show();
                            })
                            .mousemove(function (e) {
                                e.stopPropagation();
                            });
                    }

                    self.navbar.attr('style', '').draggable(full ? 'destroy' : {});
                    win.toggleClass(self.fullscreen);
                    $(this).toggleClass(navicon + '-fullscreen-off');
                    if (!parent) {
                        parent = $('.xapp');
                    }
                });

            self.navbar = $('<div class="" style="display: none;"/>');
            self.preview = $('<div class="elfinder-quicklook-preview ui-helper-clearfix"/>')
            // clean info/icon
                .bind('change', function (e) {
                    self.info.attr('style', '').hide();
                    icon.removeAttr('class').attr('style', '');
                    info.html('');

                })
                // update info/icon
                .bind('update', function (e) {
                    var fm = self.fm,
                        preview = self.preview,
                        file = e.file,
                        tpl = '<div class="elfinder-quicklook-info-data">{value}</div>',
                        tmb;

                    if (file) {
                        !file.read && e.stopImmediatePropagation();
                        //self.window.data('hash', file.path);
                        self.preview.unbind('changesize').trigger('change').children().remove();
                        self.info.delay(100).fadeIn(10);

                    } else {
                        e.stopImmediatePropagation();
                    }
                });


            self.info = $('<div class="elfinder-quicklook-info-wrapper"/>')
                .append(icon)
                .append(info);


            self.window = $('<div id="' + self.id + '" class="ui-helper-reset ui-widget elfinder-quicklook widget bg-opaque" style=""/>')
                .click(function (e) {
                    e.stopPropagation();
                })
                .append(self.preview)
                .append(self.info.hide())
                .bind('open', function (e) {
                    self._open = true;
                    var win = self.window,
                        file = self.value,
                        node = self.node;

                    if (self.closed()) {
                        self.update(1, self.value);
                        self.navbar.attr('style', '');
                        self.state = animated;
                        node && node.trigger('scrolltoview');
                        win.css(self.closedCss(node))
                            .show()
                            .animate(self.openedCss(), 550, function () {
                                self.state = opened;
                                self.update(2, self.value);
                                self.openDfd.resolve();
                            });
                    }
                })
                .bind('close', function (e) {
                    var win = self.window,
                        preview = self.preview.trigger('change'),
                        file = self.value,
                        node = {},
                        close = function () {
                            self._open = false;
                            self.state = closed;
                            win.hide();
                            preview.children().remove();
                            self.update(0, self.value);

                        };

                    if (self.opened()) {
                        state = animated;
                        win.is('.' + self.fullscreen) && self.fsicon.mousedown();
                        node.length
                            ? win.animate(self.closedCss(node), 500, close)
                            : close();
                    }
                });

            if (container) {
                container.append(self.window);
            }
            var support = this.supportTest;
            self.support = {

                audio: {
                    ogg: support('audio/ogg; codecs="vorbis"'),
                    mp3: support('audio/mpeg;'),
                    wav: support('audio/wav; codecs="1"'),
                    m4a: support('audio/x-m4a;') || support('audio/aac;')
                },
                video: {
                    ogg: support('video/ogg; codecs="theora"'),
                    webm: support('video/webm; codecs="vp8, vorbis"'),
                    mp4: support('video/mp4; codecs="avc1.42E01E"') || support('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')
                }
            };


        },
        buildRendering: function () {
            var self = this,
                navicon = this.navicon,
                container = this.container ? $(this.container) : null,
                title = $('<div class="elfinder-quicklook-title">' + self.title + '</div>'),
                icon = $('<div/>'),
                info = $('<div class="elfinder-quicklook-info"/>');//.hide(),


            self.openDfd = new Deferred();

            self.fsicon = $('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrows-alt"/>')
                .mousedown(function (e) {
                    var win = self.window,
                        full = win.is('.' + self.fullscreen),
                        scroll = "scroll.elfinder-finder",
                        $window = $(window);

                    e.stopPropagation();

                    if (full) {
                        win.css(win.data('position')).unbind('mousemove');
                        $window.unbind(scroll).trigger(self.resize).unbind(self.resize);
                        self.navbar.unbind('mouseenter').unbind('mousemove');
                    } else {

                        win.data('position', {
                            left: win.css('left'),
                            top: win.css('top'),
                            width: win.width(),
                            height: win.height()
                        }).css({
                            width: '100%',
                            height: '100%'
                        });

                        $(window).bind(scroll, function () {
                                win.css({
                                    left: parseInt($(window).scrollLeft()) + 'px',
                                    top: parseInt($(window).scrollTop()) + 'px'
                                })
                            })
                            .bind(self.resize, function (e) {
                                self.preview.trigger('changesize');
                            })
                            .trigger(scroll)
                            .trigger(self.resize);

                        win.bind('mousemove', function (e) {
                                self.navbar.stop(true, true).show().delay(3000).fadeOut('slow');
                            })
                            .mousemove();

                        self.navbar.mouseenter(function () {
                                self.navbar.stop(true, true).show();
                            })
                            .mousemove(function (e) {
                                e.stopPropagation();
                            });
                    }

                    self.navbar.attr('style', '').draggable(full ? 'destroy' : {});
                    win.toggleClass(self.fullscreen);
                    $(this).toggleClass(navicon + '-fullscreen-off');
                    if (!parent) {
                        parent = $('.xapp');
                    }
                });


            self.navbar = $('<div class="elfinder-quicklook-navbar"/>')
                .append($('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrow-left"/>').mousedown(function () {
                    self.navtrigger(37);
                }))
                .append(self.fsicon)
                .append($('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrow-right"/>').mousedown(function () {
                    self.navtrigger(39);
                }))
                .append('<div class="elfinder-quicklook-navbar-separator"/>')
                .append($('<div class="' + navicon + ' ' + navicon + '-close"/>').mousedown(function () {
                    self.window.trigger('close');
                }));


            self.preview = $('<div class="elfinder-quicklook-preview ui-helper-clearfix"/>')
            // clean info/icon
                .bind('change', function (e) {
                    self.info.attr('style', '').hide();
                    icon.removeAttr('class').attr('style', '');
                    info.html('');
                })
                // update info/icon
                .bind('update', function (e) {
                    var fm = self.fm,
                        preview = self.preview,
                        file = e.file,
                        tpl = '<div class="elfinder-quicklook-info-data">{value}</div>',
                        tmb;

                    if (file) {
                        !file.read && e.stopImmediatePropagation();
                        //self.window.data('hash', file.path);
                        self.preview.unbind('changesize').trigger('change').children().remove();
                        self.info.delay(100).fadeIn(10);

                    } else {
                        e.stopImmediatePropagation();
                    }
                });

            self.info = $('<div class="elfinder-quicklook-info-wrapper"/>')
                .append(icon)
                .append(info);


            self.window = $('<div id="' + self.id + '" class="ui-helper-reset ui-widget elfinder-quicklook widget bg-opaque" style="position:absolute"/>')
                .click(function (e) {
                    e.stopPropagation();
                })
                .append(
                    $('<div class="elfinder-quicklook-titlebar"/>')
                        .append(title)
                        .append($('<span class="fa-close" style="margin-left: 4px; color: white; font-size: 16px; text-align: center; position: absolute; left: 0px; top: 2px;"/>').mousedown(function (e) {
                                e.stopPropagation();
                                self.window.trigger('close');
                            }
                            )
                        )
                )
                .append(self.preview.add(self.navbar))
                .append(self.info.hide())
                .draggable({handle: 'div.elfinder-quicklook-titlebar'})
                .bind('open', function (e) {

                    self._open = true;

                    var win = self.window,
                        file = self.value,
                        node = self.node;

                    if (self.closed()) {
                        self.update(1, self.value);
                        self.navbar.attr('style', '');
                        self.state = animated;
                        node.trigger('scrolltoview');
                        win.css(self.closedCss(node))
                            .show()
                            .animate(self.openedCss(), 550, function () {
                                self.state = opened;
                                self.update(2, self.value);
                                self.openDfd.resolve();
                            });
                    }
                })
                .bind('close', function (e) {

                    var win = self.window,
                        preview = self.preview.trigger('change'),
                        file = self.value,
                        node = {},
                        close = function () {
                            self._open = false;
                            self.state = closed;
                            win.hide();
                            preview.children().remove();
                            self.update(0, self.value);

                        };

                    if (self.opened()) {
                        state = animated;
                        win.is('.' + self.fullscreen) && self.fsicon.mousedown();
                        node.length
                            ? win.animate(self.closedCss(node), 500, close)
                            : close();
                    }
                });


            if (container) {
                container.append(self.window);
            }
            var support = this.supportTest;
            self.support = {

                audio: {
                    ogg: support('audio/ogg; codecs="vorbis"'),
                    mp3: support('audio/mpeg;'),
                    wav: support('audio/wav; codecs="1"'),
                    m4a: support('audio/x-m4a;') || support('audio/aac;')
                },
                video: {
                    ogg: support('video/ogg; codecs="theora"'),
                    webm: support('video/webm; codecs="vp8, vorbis"'),
                    mp4: support('video/mp4; codecs="avc1.42E01E"') || support('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')
                }
            };


        },
        constructor: function (args) {
            utils.mixin(this, args);
        },
        editorOptions: {
            permissions: [
                types.ACTION.TOOLBAR,
                types.ACTION.RELOAD,
                'Editor/Settings',
                'View/Increase Font Size',
                'View/Decrease Font Size',
                'View/Themes',
                'File/Search'
            ]
        },
        title: 'Preview',
        /**
         * Opened window width (from config)
         *
         * @type Number
         **/
        width: 450,
        /**
         * Opened window height (from config)
         *
         * @type Number
         **/
        height: 300,
        fm: null,
        /**
         * window state
         *
         * @type Number
         **/
        state: 0,
        /**
         * navbar icon class
         *
         * @type Number
         **/
        navicon: 'elfinder-quicklook-navbar-icon',
        /**
         * navbar "fullscreen" icon class
         *
         * @type Number
         **/
        fullscreen: 'elfinder-quicklook-fullscreen',
        /**
         * elFinder node
         *
         * @type jQuery
         **/
        parent: null,
        /**
         * elFinder current directory node
         *
         * @type jQuery
         **/

        cwd: null,
        resize: "resize.elfinder-finder",
        /**
         * This command cannot be disable by backend
         *
         * @type Boolean
         **/
        alwaysEnabled: true,
        /**
         * Selected file
         *
         * @type Object
         **/
        value: null,
        handlers: {
            // save selected file
            select: function () {
                this.update(void(0), this.fm.selectedFiles()[0]);
            },
            error: function () {
                self.window.is(':visible') && self.window.data('hash', '').trigger('close');
            },
            'searchshow searchhide': function () {
                this.opened() && this.window.trigger('close');
            }
        },
        shortcuts: [
            {
                pattern: 'space'
            }
        ],
        /**
         * Triger keydown/keypress event with left/right arrow key code
         *
         * @param  Number  left/right arrow key code
         * @return void
         **/
        navtrigger: function (code) {
            $(document).trigger($.Event('keydown', {
                keyCode: code,
                ctrlKey: false,
                shiftKey: false,
                altKey: false,
                metaKey: false
            }));
        },
        /**
         * Return css for closed window
         *
         * @param  jQuery  file node in cwd
         * @return void
         **/
        closedCss: function (node) {
            return {
                opacity: 0,
                width: 20,//node.width(),
                height: 20,
                top: node ? node.offset().top + 'px' : 0,
                left: node ? node.offset().left + 'px' : 0
            }
        },
        /**
         * Return css for opened window
         *
         * @return void
         **/
        openedCss: function () {

            var container = this.container ? $(this.container) : null,
                win = container || $(window);

            var w = container ? "100%" : Math.min(this.width, win.   width() - 10);
            var h = container ? "100%" : Math.min(this.height, win.height() - 80);
            return {
                opacity: 1,
                width: w,
                height: h,
                top: parseInt((win.height() - h - 60) / 2 + win.scrollTop()),
                left: parseInt((win.width() - w) / 2 + win.scrollLeft())
            }
        },
        supportTest: function (codec) {
            var media = document.createElement(codec.substr(0, codec.indexOf('/'))),
                value = false;

            try {
                value = media.canPlayType && media.canPlayType(codec);
            } catch (e) {

            }

            return value && value !== '' && value != 'no';
        },
        changeView: function (item) {
            if (this.currentView) {
                utils.destroy(this.currentView);
                if (this.currentView.destroy) {
                    this.currentView.destroy();
                }
            }
        },
        update: function (state) {
            this._open = state;
            this._emit('changeState', state);
            if (state == animated && this.onAnimate) {
                this.onAnimate();
            }
            if (state == opened && this.onOpened) {
                this.onOpened();
            }
            if (state == closed && this.onClosed) {
                this.onClosed();
            }
        },
        /**
         * Return true if quickLoock window is visible and not animated
         *
         * @return Boolean
         **/
        closed: function () {
            return this.state == closed;
        },
        /**
         * Return true if quickLoock window is hidden
         *
         * @return Boolean
         **/
        opened: function () {
            return this.state == opened;
        },
        /**
         * Attach listener to events
         * To bind to multiply events at once, separate events names by space
         *
         * @param  String  event(s) name(s)
         * @param  Object  event handler
         * @return elFinder
         */
        bind: function (event, callback) {
            var i;
            if (typeof(callback) == 'function') {
                event = ('' + event).toLowerCase().split(/\s+/);

                for (i = 0; i < event.length; i++) {
                    if (listeners[event[i]] === void(0)) {
                        listeners[event[i]] = [];
                    }
                    listeners[event[i]].push(callback);
                }
            }
            return this;
        },
        one: function (event, callback) {
            var self = this,
                h = $.proxy(callback, function (event) {
                    setTimeout(function () {
                        self.unbind(event.type, h);
                    }, 3);
                    return callback.apply(this, arguments);
                });
            return this.bind(event, h);
        },
        open: function (e) {
            var self = this,
                win = self.window,
                file = self.value,
                node = self.node;
            self._open = true;
            self.openDfd = new Deferred();
            if (self.closed()) {

                self.navbar.attr('style', '');
                self._open = true;
                self.state = animated;
                node && node.trigger('scrolltoview');
                win.css(self.closedCss(node))
                    .show()
                    .animate(self.openedCss(), 550, function () {
                        self.state = opened;
                        self.update(1, self.value);
                        self.openDfd.resolve();
                    });
            } else {
                self.openDfd.resolve();
            }
            return self.openDfd;
        },
        close: function (e) {
            this._open = false;
            var self = this,
                win = self.window,
                preview = self.preview.trigger('change'),
                file = self.value,
                node = self.node,
                close = function () {
                    self.state = closed;
                    win.hide();
                    preview.children().remove();
                    self.update(0, self.value);
                    self.closeDfd && self.closeDfd.resolve();
                };

            if (self.opened()) {
                self.state = animated;
                win.is('.' + self.fullscreen) && self.fsicon.mousedown();
                node && node.length
                    ? win.animate(self.closedCss(node), 500, close)
                    : close();
            }


            return self.closeDfd;
        },
        getstate: function () {
            return this.state == opened ? 1 : 0;
        },

        exec: function () {
            this.window.trigger(this.opened() ? 'close' : 'open');
        },
        hideinfo: function () {
            this.info.stop(true).hide();
        }
    });
    var filePreviewClass = dcl(previewClass, {
        destroy: function () {
            this.close();
            this.preview.trigger($.Event('update', {file: null}));
            this.inherited(arguments);
            utils.destroy(this._editor);
            utils.destroy(this.preview);
            utils.destroy(this.window);

        },
        onOpened: function () {
            var self = this;
            self.fm = {
                url: function (what, cache, params) {
                    return self.delegate.getImageUrl(what, cache, params);
                }
            }
            self.preview.trigger($.Event('update', {file: self.item}));
        },
        onClosed: function () {
        },
        onAnimate: function () {
        }
    });

    var editorClass = dcl(filePreviewClass, {
        resize: function () {
            this.preview.trigger('changesize');
        },
        constructor: function (args) {
            this.parent = $(args._parent.containerNode);
            this.container = args._parent.containerNode;
            this.resizeToParent = true;
            this.delegate = args.ctx.getFileManager();
            this.item = args.item;
            this.buildRenderingEmbedded();
             0 && console.log('this',this);
            this.domNode = this.info[0];
        },
        openItem: function (item) {
            this.id = utils.createUUID();
            registry.add(this);
            this.item = item || this.item;

            this.init();
            this.exec();
            this.window.css({
                height: '100%',
                width: '100%'
            });
            this._parent.add(this, null, false);
            this.preview.trigger($.Event('update', {file: this.item}));
            this._parent.resize();
            this.resize();
        }
    });

    filePreviewClass.EditorClass = editorClass;
    return filePreviewClass;

});
},
'xfile/views/FilePicker':function(){
/** @module xfile/views/FilePicker **/
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'module',
    'xide/_base/_Widget',
    'xdocker/Docker2',
    'xfile/ThumbRenderer',
    'dojo/promise/all',
    'xfile/Breadcrumb'
], function (dcl, types, utils, factory,
             module, _Widget, Docker2, ThumbRenderer, all, Breadcrumb) {

    var ACTION = types.ACTION;
    var Module = dcl(_Widget, {
        declaredClass:'xfile.views.FilePicker',
        templateString: '<div class="FilePicker"></div>',
        resizeToParent: true,
        docker: null,
        dockerOptions : {
            resizeToParent: true
        },
        breadcrumb: null,
        leftStore: null,
        rightStore: null,

        storeOptionsMixin: null,
        storeOptions: null,

        leftGrid: null,
        leftGridArgs: null,

        rightGrid: null,
        rightGridArgs: null,
        removePermissions:[
            ACTION.OPEN_IN_TAB
        ],
        defaultGridArgs: {
            registerEditors: false,
            _columns: {
                "Name": true,
                "Path": false,
                "Size": false,
                "Modified": false
            }
        },
        selection:null,
        mount: 'root',
        _selection:null,
        getDocker:function(){
            if(this.docker){
                return this.docker;
            }
            var dockerOptions = this.dockerOptions || {
                resizeToParent: true
            };
            var docker = this.docker || Docker2.createDefault(this.domNode, dockerOptions);
            docker.resizeToParent = true;
            this.add(docker, null, false);

            this.docker = docker;
            return docker;
        },
        createLayout: function () {
            var docker = this.getDocker();
            this.layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                h: 50,
                title: false
            });
            this.layoutTop._minSize.y = 50;
            this.layoutTop._maxSize.y = 50;
            this.layoutTop._scrollable = {
                x: false,
                y: false
            };
            this.breadcrumb = utils.addWidget(Breadcrumb, {}, null, this.layoutTop, true);
            this.layoutLeft = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.BOTTOM, null, {
                w: 100,
                title: false
            });
            this.layoutLeft._minSize.x = 150;
            this.layoutLeft.moveable(true);
            this.layoutMain = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.RIGHT, this.layoutLeft, {
                w: '70%',
                title: false
            });
            this.layoutTop.initSize(100,'100%');
            this.layoutLeft.initSize('100%',150);
            docker.resize();
        },
        setSelection:function(selection){
            this._selection=selection;
        },
        connectGrids:function(src,target){

            var self = this;

            target._on('selectionChanged', function (event) {
                if(event.why !=='deselect') {
                    var selection = target.getSelection(),
                        item = target.getSelectedItem();

                    if(item){
                        self.setSelection(selection);
                    }
                }
            });
            src._on('selectionChanged', function (event) {
                var selection = src.getSelection(),
                    item = src.getSelectedItem();
                if (!item) {
                    item = src.getCurrentFolder();
                }
                if(!item){
                    return;
                }
                var targetFolder = null;
                if (item.directory) {
                    targetFolder = item;
                } else {
                    if(!item.getParent){
                        console.error('item has no item.getParent!');
                    }
                    targetFolder = item.getParent ? item.getParent() : null;
                }
                if (targetFolder) {
                    target.openFolder(targetFolder.path, false);
                }

                if(item){
                    self.setSelection(selection);
                }
            });

        },
        createFileGrid: function (store, parent, args) {
            var GridClass = this.Module /*|| FileGrid*/;
            return GridClass.createDefault(this.ctx, args, parent, false, true, store);
        },
        onLeftGridReady:function(grid){},
        onRightGridReady:function(grid){},
        onError:function(err,where){
            //console.error('File Picker Error: '+ where + ' : ' + err,err);
        },
        startGrids:function(left,right){
            var self = this,
                srcStore = left.collection,
                targetStore = right.collection,
                _selectArgs = {
                    focus: true,
                    append: false
                },
                both = [left.refresh(), right.refresh()];
            var selected = this.selection;// './AA/xfile_last/Selection_002.png';
            var pathInfo = utils.pathinfo(selected, types.PATH_PARTS.ALL);
            var path = pathInfo.dirname || "";
            if(path==='.'){
                path = selected;
            }
            left.showStatusbar && left.showStatusbar(false);

            left._on('changeSource',function(mountdata){
                right.changeSource(mountdata,true);
            });
            left._on('changedSource',function(mountdata){
                left.select([0],null,true,_selectArgs);
            });
            right._on('changeSource',function(mountdata){
                left.changeSource(mountdata,true);
            });
            right._on('changedSource',function(mountdata){
                left.select([0],null,true,_selectArgs);
            });

            all(both).then(function () {
                self.onLeftGridReady(left);
                self.onRightGridReady(right);
                self.getDocker().resize();
                //load the selected item in the store
                all([srcStore.getItem(path, true), targetStore.getItem(path, true)]).then(function (what) {
                    //when loaded, open that folder!
                    all(_.invoke([left], 'openFolder', path)).then(function () {
                        //when opened, select the selected item
                        all(_.invoke([left,right], 'select', [selected], null, true, _selectArgs)).then(function () {
                            //when selected, complete grids
                            selected._selection = selected;
                            self.connectGrids(left, right);
                            //self.setupBreadcrumb(left, right,selected);
                        },function(err){
                            self.onError(err,'selecting items');
                        });

                    },function(err){
                        self.onError(err,'open folder');
                    });
                },function(err){
                    self.onError(err,'loading items');
                });
            },function(error){
                self.onError(err,'refresh grid');
            });
        },
        /**
         * Function called right after left & right grid is ready (opened folders,...)
         * @param src
         * @param target
         */
        setupBreadcrumb:function(src,target,openPath){
            
            var breadcrumb = this.breadcrumb,
                cwd = src.getCurrentFolder(),
                srcStore = src.collection,
                targetStore = target.collection;
            breadcrumb.setSource(src);

            function _onChangeFolder(store,item,grid){
                if(breadcrumb.grid!=grid){
                    breadcrumb.setSource(grid);
                }
                breadcrumb.clear();
                breadcrumb.setPath('.',srcStore.getRootItem(),item.getPath(),store);
            }

            this.addHandle('click',src.on('click',function(){
                breadcrumb.setSource(src);
            }));
            this.addHandle('click',target.on('click',function(){
                breadcrumb.setSource(target);
            }));
            src._on('openFolder', function (evt) {
                _onChangeFolder(srcStore,evt.item,src);
            });
            target._on('openFolder', function (evt) {
                _onChangeFolder(targetStore,evt.item,target);
            });

        },
        destroy:function(){          
            utils.destroy(this.leftStore);
            utils.destroy(this.rightStore);
            utils.destroy(this.leftGrid);
            utils.destroy(this.rightGrid);
            utils.destroy(this.breadcrumb);
            utils.destroy(this.docker);            
        },
        startup: function () {
            this.createLayout();
            var leftParent = this.layoutLeft;
            var rightParent = this.layoutMain;
            var self = this,
                ctx = self.ctx,
                config = ctx.config;

            /**
             * File Stores
             */
            var mount = this.mount || 'root';
            var storeOptions = this.storeOptions;
            var storeOptionsMixin = this.storeOptionsMixin || {
                    "includeList": "*.jpg,*.png",
                    "excludeList": "*"
                };

            var leftStore = this.leftStore || factory.createFileStore(mount, storeOptions, config, storeOptionsMixin, ctx,storeOptionsMixin);
            var rightStore = this.rightStore || factory.createFileStore(mount, storeOptions, config, storeOptionsMixin, ctx,storeOptionsMixin);
            var newTabTarget = this.layoutMain;
            var defaultGridArgs = utils.mixin({},utils.clone(this.defaultGridArgs));
            var removePermissions = this.removePermissions;
            /**
             *  Left - Grid :
             */
            var leftGridArgs = utils.mixin(defaultGridArgs, this.leftGridArgs || {
                    newTarget: newTabTarget
                });
            _.remove(leftGridArgs.permissions, function (permission) {
                return _.indexOf(removePermissions, permission) !== -1;
            });
            //eg: leftGridArgs.permissions.remove(types.ACTION.STATUSBAR);
            var gridLeft = this.leftGrid || this.createFileGrid(leftStore, leftParent, leftGridArgs);
            this.leftGrid = gridLeft;
            /**
             *  Right - Grid :
             */
            var rightGridArgs = utils.mixin(defaultGridArgs,this.rightGridArgs || {
                    selectedRenderer: ThumbRenderer
                });
            _.remove(rightGridArgs.permissions, function (permission) {
                return _.indexOf(removePermissions, permission) !== -1;
            });
            var gridMain = this.rightGrid || this.createFileGrid(rightStore, rightParent, rightGridArgs);
            this.rightGrid = gridMain;
            return this.startGrids(gridLeft,gridMain);
        }
    });

    return Module;

});

},
'xfile/Breadcrumb':function(){
/** @module xfile/Breadcrumb **/
define([
    "dcl/dcl",
    'xide/widgets/TemplatedWidgetBase',
    'xide/model/Path',
    'xide/_base/_Widget'
], function (dcl,TemplatedWidgetBase,Path,_Widget) {

    return dcl(_Widget,{
        templateString:"<ul attachTo='root' style='-webkit-app-region:drag' class='breadcrumb'></ul>",
        grid:null,
        destroy:function(){
            this.clear();
            this.grid = null;
        },
        setSource:function(src){

            /*
            if(!src || (!src.getBreadcrumbPath || !src.collection)){
                return;
            }
            */

            if(this.grid==src){
                return;
            }

            this.grid = src;
            var collection = src.collection
            this.clear();

            var customPath = src.getBreadcrumbPath ? src.getBreadcrumbPath():null;
            if(customPath===false){
                return;
            }
            if(customPath){
                this.setPath(customPath.root, null, customPath, null);
            }else if(collection && src.getCurrentFolder){

                var store = collection,
                    cwdItem = src.getCurrentFolder(),
                    cwd = cwdItem ? cwdItem.path : '';

                this.setPath('.', store.getRootItem(), cwd, store);
            }

        },
        setPath : function(rootLabel,rootItem,path,store,_init){

            this.clear();
            rootItem && rootLabel && this.addSegment(rootLabel, true, rootItem);
            var _path = new Path(path);
            var segs = _path.getSegments();
            var _last = _init || '.';
            _.each(segs, function (seg) {
                var segPath = _last + '/' + seg;
                this.addSegment(seg, true, store ? store.getSync(segPath) : null);
                _last = segPath;
            },this);
        },
        /**
         * Event delegation
         * @param label
         * @param e
         * @param el
         */
        onClick:function(label,e,el){
            this._emit('click',{
                element:el,
                event:e,
                label:label,
                data:el.data
            });
        },
        /**
         * Add a new breadcrumb item
         * @param label
         * @param active
         * @param data
         * @returns {*|jQuery|HTMLElement}
         */
        addSegment:function(label,active,data){

            if(data && this.has(data)){
                return null;
            }

            var _class = active ? 'active' : '',
                self = this,
                el = $("<li class='" +_class + "'><a href='#'>" + label + "</a></li>");

            //clear active state of all previous elements
            _.each(this.all(),function(e){
                e.el.removeClass('active');
            });

            $(this.domNode).append(el);

            if(data) {
                el.on('click', function (e) {
                    self.grid && self.grid.openFolder(data);
                });
                el[0].data = data;
            }

            return el;
        },
        /**
         * Remove last breadcrumb item
         */
        pop:function(){
            this.last().remove();
        },
        /**
         * Return last breadcrumb item
         * @returns {*|jQuery}
         */
        last:function(){
            return $(this.root).children().last();
        },
        /**
         * Returns true when data is found
         * @param data
         * @returns {boolean}
         */
        has:function(data){

            var all = this.all();
            for (var i = 0; i < all.length; i++) {
                if(all[i].data.path === data.path){
                    return true;
                }
            }
            return false;
        },
        /**
         * Return all breadcrumb items
         * @returns {Array}
         */
        all:function(){

            var result = [];

            _.each(this.$root.children(),function(el){
                result.push({
                    el:$(el),
                    label:el.outerText,
                    data:el.data
                })
            });

            return result;

        },
        clear:function(){

            var all = this.all();
            while(all.length){
                var item =all[all.length-1];
                item.el.remove();
                item.data = null;
                all.remove(item);
            }
        },
        /**
         * Removes all breadcrumb items from back to beginning, until data.path matches
         * @param data
         */
        unwind:function(data){
            var rev = this.all().reverse();
            _.each(rev,function(e){
                if(e.data && e.data.path !== (data && data.path) && e.data.path!=='.'){
                    this.pop();
                }
            },this);
        },
        startup:function(){

            this._on('click',function(e){

                var data = e.element[0].data,
                    grid = this.grid;

                if(data) {
                    this.unwind(data);
                    grid && grid.openFolder(data);
                }

            }.bind(this));
        }
    });
});
},
'xfile/Statusbar':function(){
/** @module xfile/Statusbar **/
define([
    "xdojo/declare",
    'xide/types',
    'xfile/Breadcrumb'
], function (declare, types, Breadcrumb) {

    //package via declare
    var _class = declare('xgrid.Statusbar', null, {
        statusbar: null,
        setState: function (state) {
            this.showStatusbar(state.statusBar);
            return this.inherited(arguments);
        },
        getState: function (state) {
            state = this.inherited(arguments) || {};
            state.statusBar = this.statusbar != null;
            return state;
        },
        runAction: function (action) {
            if (action.command == types.ACTION.STATUSBAR) {
                this.showStatusbar(this.statusbar ? false : true);
            }
            return this.inherited(arguments);
        },
        showStatusbar: function (show,priorityChange) {

            if (!show && this.statusbar) {
                this._destroyStatusbar();
            } else if (show) {
                this.getStatusbar();
            }

            this.resize();

            if(this._statusbarAction && priorityChange!==true){
                this._statusbarAction.set('value',show);
            }

        },
        _destroyStatusbar: function () {

            this.statusbarRoot && this.statusbarRoot.remove();
            this.statusbar = null;
            this.statusbarRoot = null;
        },
        _statusbarAction:null,
        buildRendering: function () {

            this.inherited(arguments);

            var self = this,
                node = this.domNode;

            this._on('onAddActions', function (evt) {

                var actions = evt.actions,
                    permissions = evt.permissions,
                    action = types.ACTION.STATUSBAR;

                if (!evt.store.getSync(action)) {

                    var _action = self.createAction({
                        label: 'Statusbar',
                        command: action,
                        icon: types.ACTION_ICON.STATUSBAR,
                        tab: 'View',
                        group: 'Show',
                        mixin:{
                            actionType:'multiToggle'
                        },
                        onCreate:function(action){
                            action.set('value',self.statusbar!==null);
                            self._statusbarAction = action;
                        },
                        onChange:function(property,value,action){
                            self.showStatusbar(value,true);

                        }
                    });

                    actions.push(_action);


                    /*
                    var _action = grid.createAction('Statusbar', action,
                        types.ACTION_ICON.STATUSBAR, null, 'View', 'Show', 'item|view', null,
                        null, null, null, null, permissions, node, grid);
                    if (!_action) {

                        return;
                    }
                    */
                    //actions.push(_action);
                }
            });
        },
        getStatusbar: function () {

            if (this.statusbar) {
                return this.statusbar;
            } else {
                return this.createStatusbar();
            }
        },
        onRenderedStatusBar: function (statusbar, root, text) {

            var bc = this.__bc;
            if (!bc && root && root.append) {
                bc = new Breadcrumb({}, $('<div>'));
                root.append(bc.domNode);

                $(bc.domNode).css({
                    "float": "right",
                    "padding": 0,
                    "margin-right": 10,
                    "top": 0,
                    "right": 50,
                    /*"right":0,*/
                    "position": "absolute"
                });

                this.__bc = bc;
                //bc.startup();
                bc.setSource(this);
            }
            if (bc) {

                bc.clear();


                var store = this.collection,
                    cwdItem = this.getCurrentFolder(),
                    cwd = cwdItem ? cwdItem.path : '';

                bc.setPath('.', store.getRootItem(), cwd, store);
            }

        },
        onStatusbarCollapse:function(e){

        },
        createStatusbar: function (where) {

            where = where = this.footer;
            var statusbar = this.statusbar,
                self = this;

            if (!statusbar) {


                var root = $('<div class="statusbar widget" style="width:inherit;padding: 0;margin:0;padding-left: 4px;"></div>')[0];

                where.appendChild(root);



                statusbar = $('<div class="status-bar-text ellipsis" style="display: inline-block;">0 items selected</div>')[0];

                root.appendChild(statusbar);

                var $collapser = $('<div class="status-bar-collapser fa-caret-up" style="" ></div>');
                $collapser.click(function(e){
                    self.onStatusbarCollapse($collapser);
                });
                var collapser  = $collapser[0];
                root.appendChild(collapser);
                this.statusbar = statusbar;
                this.statusbarCollapse = collapser;
                this.statusbarRoot = root;

                this._emit('createStatusbar', {
                    root: root,
                    statusbar: statusbar,
                    collapser: collapser
                })

            }

            return statusbar;

        },
        startup: function () {

            this.inherited(arguments);


            function bytesToSize(bytes) {
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Byte';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            }


            function calculateSize(items) {


                var bytes = 0;
                var sizeStr = '0 KB';
                _.each(items, function (item) {

                    var _bytes = item.sizeBytes || 0;
                    bytes += _bytes;
                });

                if (bytes > 0) {
                    sizeStr = bytesToSize(bytes);
                }

                return {
                    bytes: bytes,
                    sizeStr: sizeStr
                }


            }

            if (!this.hasPermission(types.ACTION.STATUSBAR)) {
                return;
            }
            var statusbar = this.getStatusbar(),
                self = this;

            if (statusbar) {

                function selectionChanged(evt) {
                    var selection = evt.selection || [],
                        nbItems = selection.length,
                        size = calculateSize(selection),
                        text = nbItems + '  ' +self.localize('selected') + ' (' + size.sizeStr + ')';

                    statusbar.innerHTML = text;

                    if (self.onRenderedStatusBar) {
                        self.onRenderedStatusBar(statusbar, self.statusbarRoot, text);
                    }

                }

                this._on('selectionChanged', selectionChanged);
            }
        }
    });

    return _class;
});
},
'xfile/views/UploadMixin':function(){
define([
    'xdojo/declare',
    'xide/utils',
    'xide/types',
    "dojo/promise/all"
], function (declare, utils, types, all) {

    return declare("xfiles.views.UploadMixin", null, {
        cssClass: 'uploadFilesList droparea dropareaHover',
        __currentDragOverRow: null,
        __currentDragOverTimer: null,
        __currentDragOverTimeRow: null,
        buildRendering: function () {
            this.inherited(arguments);
            $(this.domNode).addClass(this.cssClass);
        },
        _createUploadItemTemplate: function (name, progress, obj) {
            var _textClass = obj.failed ? 'text-danger' : 'text-info';
            return '<div class="" style="position: relative;margin: 0.1em;height: auto">' +

                '<div style="margin:0;vertical-align: middle;padding: 1px;" class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">' +
                '<div class="progress-bar progress-bar-info" style="width:' + progress + ';background-color: rgba(102, 161, 179, 0.35);height:auto;magin:0;">' +
                '<span class="' + _textClass + '" style="left: 0px">' + name + '</span>' +
                '</div>' +
                '</div>' +
                '</div>';
        },
        _createUploadItems: function (files) {

            var thiz = this;
            var store = this.collection,
                selectedRenderer = thiz.selectedRenderer;

            var parent = this.getCurrentFolder();
            if (this.__currentDragOverRow) {
                parent = this.__currentDragOverRow.data;
            }
            function createItem(file) {
                var _item = {
                    sizeBytes: utils.humanFileSize(file.size),
                    size: utils.humanFileSize(file.size),
                    name: file.name,
                    type: file.type,
                    path: parent.path + '/' + file.name + '__uploading__' + Math.random(2),
                    isDir: false,
                    mount: parent.mount,
                    parent: parent.path,
                    progress: 0,
                    isUpload: true,
                    modified: file.lastModified / 1000,
                    renderColumn: function (field, value, obj) {

                        if (field === 'name') {
                            return thiz._createUploadItemTemplate(obj.name, obj.progress + '%', obj);
                        }
                        if (field === 'sizeBytes') {
                            if (obj.progress < 100) {
                                return utils.humanFileSize(obj.loaded) + ' of ' + utils.humanFileSize(obj.total);
                            }
                        }
                    }
                }
                //_item._S = store;
                store.putSync(_item);
                _item = store.getSync(_item.path);
                file._item = _item;
                return _item;
            }

            var items = [];
            _.each(files, function (file) {
                items.push(createItem(file));
            });
            return items;

        },
        _onAllUploaded: function (files) {
            var total = 0, items = this._toUploadItems(files),
                thiz = this;
            _.each(items, function (file) {
                var item = file._store ? file : file._item;
                if (item) {
                    item.progress = item.failed ? 0 : 100;
                    item.refresh();
                    setTimeout(function () {
                        item.remove();
                    }, 1);
                }
            });
            setTimeout(function () {
                thiz.runAction(types.ACTION.RELOAD).then(function () {
                });
            }, 1);
        },
        _onBrowserFilesDropped: function (panel, files, view) {
            var fm = this.ctx.getFileManager();
            var parent = panel.getCurrentFolder();
            if (this.__currentDragOverRow) {
                parent = this.__currentDragOverRow.data;
            }
            var dst = parent;
            var items = this._createUploadItems(files);
            return fm.upload(files, dst.mount, dst ? dst.path : './tmp', panel, view);
        },
        _onUploadFailed: function (uploadItem) {
            if (!uploadItem) {
                return;
            }
            uploadItem.failed = true;
            uploadItem.done = true;
        },
        _onSomeFileUploaded: function (uploadItem) {
            var thiz = this
            if (uploadItem) {
                uploadItem.progress = 100;
                uploadItem.done = true;
                uploadItem.renderColumn = null;
                uploadItem.renderRow = null;
                uploadItem.refresh();
                setTimeout(function () {
                    thiz.runAction(types.ACTION.RELOAD).then(function () {
                    });
                    uploadItem.remove();
                }, 1);
            }
            return true;

        },
        _toUploadItems: function (files) {
            var items = [];
            _.each(files, function (_file) {
                _.each(_file, function (file) {
                    items.push(file);
                });
            });
            return items;

        },
        _toUploadStoreItem: function (data) {
            var file = data.file,
                storeItem = file._item;
            return storeItem;

        },
        onBrowserFilesDropped: function (view, files) {
            var dfds = this._onBrowserFilesDropped(this, files, view),
                thiz = this,
                items = thiz._toUploadItems(files),
                isSingleUpload = items.length == 1;
            all(dfds).then(this._onAllUploaded.bind(this, [files]));
            function check() {
                var done = 0;
                _.each(items, function (item) {
                    item.done && done++;
                });
                done == items.length && thiz._onAllUploaded(files);
            }

            _.each(dfds, function (dfd) {
                dfd.then(function (data) {
                    //only refresh item if its a multi upload
                    !isSingleUpload && thiz._onSomeFileUploaded(thiz._toUploadStoreItem(data)) && check();
                }, function (data) {

                    thiz._onUploadFailed(thiz._toUploadStoreItem(data));
                    isSingleUpload && thiz._onAllUploaded(files);
                    !isSingleUpload && check();
                }, function (data) {

                    var computableEvent = data.progress,
                        item = data.item,
                        file = item.file,
                        storeItem = file._item,
                        percentage = Math.round((computableEvent.loaded * 100) / computableEvent.total);

                    if (storeItem) {
                        storeItem.progress = percentage;
                        storeItem.loaded = computableEvent.loaded;
                        storeItem.total = computableEvent.total;
                        storeItem.refresh();
                    }
                });
            });
        },
        onBrowserUrlDropped: function (srcView, url) {
            if (this.delegate && this.delegate.onBrowserUrlDropped) {
                this.delegate.onBrowserUrlDropped(this, url);
            }
        },
        dragEnter: function (e) {
            e.preventDefault();
        },
        dragLeave: function (e) {
        },
        dragOver: function (e) {
            e.preventDefault();
            if (!this.row) {
                return;
            }

            var self = this,
                row = this.row(e),
                oldRow = this.__currentDragOverRow,
                oldItem = oldRow ? oldRow.data : null,
                current = row ? row.data : null,
                isFolder = current ? current.directory == true : false;

            if (this.__currentDragOverRow) {
                $(this.__currentDragOverRow.element).removeClass('dgrid-focus');
            }

            if (row && row.element && row.data && row.data.directory === true) {
                if (!this.__currentDragOverTimer) {
                    self.__currentDragOverTimeRow = row;
                    //set up timer
                    self.__currentDragOverTimer = setTimeout(function () {
                        var _last = self.__currentDragOverTimeRow.data;
                        if (_last && oldItem && _last == oldItem) {
                            self.__currentDragOverTimeRow = null;
                            self.__currentDragOverTimer = null;
                            self.openFolder(_last);
                            return;
                        }
                        self.__currentDragOverTimer = null;
                    }, 1550);
                }
                this.__currentDragOverRow = row;
                $(row.element).addClass('dgrid-focus');
            } else {
                this.__currentDragOverRow = null;
            }
        },
        drop: function (e) {
            e.preventDefault();
            var dt = e.dataTransfer;
            if (dt) {
                var files = dt.files;
                if (files && files.length) {
                    if (this.onBrowserFilesDropped) {
                        this.onBrowserFilesDropped(this, files);
                    } else {
                         0 && console.warn('upload failed, invalid state : have no delegate or delegate::onBrowserFilesDropped doesnt exists');
                    }
                } else {
                    try {
                        var url = e.dataTransfer.getData('text/plain');
                        if (url && url.length) {
                            this.delegate.onBrowserUrlDropped(this, url);
                        }
                    } catch (err) {
                        console.error('dropping url failed ' + err);
                    }
                }
            }
        },
        initDND: function () {
            var thiz = this;
            var dstNode = this.domNode;
            if (dstNode) {
                dstNode.addEventListener("dragover", function (e) {
                    thiz.dragOver(e);
                }, false);
                dstNode.addEventListener("drop", function (e) {
                    e.preventDefault();
                    thiz.drop(e);
                }, false);
            } else {
                console.error('have not drop destination');
            }
        },
        startup: function () {
            if (this._started) {
                return;
            }
            var res = this.inherited(arguments);
            this.initDND();
            var grid = this,
                node = this.domNode;
            /*
             this._on('onAddActions', function (evt) {

             var actions = evt.actions,
             permissions = evt.permissions,
             action = types.ACTION.UPLOAD;
             if(!evt.store.getSync(action)) {
             var _action = grid.createAction('Upload', action, types.ACTION_ICON.UPLOAD, null, 'Home', 'File', 'item|view', null, null, null, null, null, permissions, node, grid);
             if (!_action) {
             return;
             }
             actions.push(_action);
             }
             });
             */
            return res;
        }
    });
});
},
'xfile/FolderSize':function(){
/** @module xfile/Statusbar **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xfile/Breadcrumb',
    'xide/layout/_TabContainer',
    'xfile/views/FileSize'
], function (declare, types,utils,Breadcrumb,_TabContainer,FileSize) {

    //package via declare

    var _class = declare('xfile.FolderSize', null, {

        __sizeDisplay:null,
        __bottomTabContainer:null,
        __bottomPanel:null,
        __sizesTab:null,

        showStats:function(show){

            var bottomTabContainer = this.getBottomTabContainer(),
                tab = this.__sizesTab,
                display = this.__sizeDisplay,
                self = this,
                bottom = this._getBottom(),
                open = (bottom && bottom.isExpanded());

            if(open==true && bottom){
                show=false;
            }else if(!open && bottom){
                return bottom.expand();
            }


            if(show){


                if(!bottom){
                    bottom = this.getBottomPanel(false, 0.2);
                }

                if(bottom && display){
                    return bottom.expand();
                }

                if(!bottomTabContainer){

                    var tabContainer = utils.addWidget(_TabContainer, {
                        direction: 'below'
                    }, null, bottom, true);

                    this.__bottomTabContainer = bottomTabContainer  = tabContainer;
                    bottom._tabs = tabContainer;
                }

                if(!tab){
                    tab = this.__sizesTab = bottomTabContainer.createTab('Sizes', 'fa-bar-chart');
                }

                if(!display){

                    display = this.__sizeDisplay = tab.add(FileSize, {
                        owner:self
                    }, null, false);

                    display.startup();


                    this._on('openedFolder',function(data){

                        utils.destroy(self.__sizeDisplay);

                        tab.resize();
                        self.resize();
                        self.__sizeDisplay = null;
                        self.__sizeDisplay = utils.addWidget(FileSize, {
                            owner:self
                        }, null,tab,false);

                        utils.resizeTo(self.__sizeDisplay,tab,true,true);
                        self.__sizeDisplay.startup();

                    });
                }




            }else{
                bottom && bottom.collapse();
            }

        },
        _runAction: function (action) {


            var _action = this.getAction(action);
            if (_action && _action.command == types.ACTION.SIZE_STATS) {
                return this.showStats(this.__sizeDisplay ? false : true);
            }
            return this.inherited(arguments);
        },
        buildRendering: function () {

            this.inherited(arguments);

            var grid = this,
                node = this.domNode;


            this._on('onAddActions', function (evt) {

                var actions = evt.actions,
                    permissions = evt.permissions,
                    action = types.ACTION.SIZE_STATS;

                /*
                if (!evt.store.getSync(action) &&typeof d3 !=='undefined') {

                    var _action = grid.createAction('Sizes', action, 'fa-bar-chart', null, 'View', 'Show', 'item|view', null, null, null, null, null, permissions, node, grid);
                    if (!_action) {

                        return;
                    }
                    actions.push(_action);
                }
                */
            });
        }

    });

    return _class;
});
},
'xfile/views/FileSize':function(){
/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xide/_base/_Widget"

], function (dcl, _XWidget) {
    var Module = dcl(_XWidget,{
        _find:function(path,nodes){
            var res = _.find(nodes,{
                    path:path
                }),
                self = this;
            if(res && res.children && res.children){
                var _child = self._find(path,res.children);
                if(_child){
                    res = _child;
                }
            }
            if(!res){
                _.each(nodes,function(node){
                    var _inner = null;
                    if(node.children && node.children.length){
                        _inner = self._find(path,node.children);
                        if(_inner){
                            res=_inner;
                        }
                    }
                });
            }
            return res;
        },
        select:function(path){
            var d = this._find(path,this.d3Nodes);
            if(d) {
                this.highlight(d);
            }

        },
        direction:'left',
        templateString:'<div class="sizeDisplay tabbable tabs-${!direction}" attachTo="containerNode" style="width: inherit;height: 100%;padding: 4px">' +

        '<ul class="nav nav-tabs tabs-right" role="tablist">'+


        '<li class="active">'+
        '<a href="#_tab-content2_${!id}" data-toggle="tab">Overall</a>'+
        '</li>'+

        '<li>'+
        '<a href="#_tab-content1_${!id}" data-toggle="tab">By Type</a>'+
        '</li>'+



        '</ul>'+
        '<div class="body tab-content">'+

        '<div attachTo="tab2" id="_tab-content2_${!id}" class="tab-pane active clearfix">'+

        '<div class="chart1" attachTo="chart1" style="width: 100%;height: inherit;min-height:250px">'+
        '<div class="" style="" id="detailInfo"></div>'+
        '<div class="widget" id="explanation" >'+
        '<span id="percentage"></span><br/>'+
        '<span id="percentageDetail"></span>'+
        '</div>'+
        '</div>'+

        '</div>'+

        '<div attachTo="tab1" id="_tab-content1_${!id}" class="tab-pane">'+

        '<svg class="chart2" style="width: 100%;height: inherit;min-height:250px">'+

        '</svg>'+
        '<div id="tooltip" class="hidden">'+
        '<span id="chart2Per1">A</span> <br>'+
        '<span id="chart2Per2">100</span>'+
        '</div>'+

        '</div>'+


        '</div>'+

        '</div>',
        data:null,
        owner:self,
        vis:null,
        d3Nodes:null,
        colors : {
            "dir": "#5687d1",
            /*pictures same color*/
            "jpeg":"#9b59b6",
            "jpg":"#9b59b6",
            "png":"#9b59b6",
            "gif":"#9b59b6",
            "psd":"#9b59b6",
            /*audios same color*/
            "mp3":"#3498db",
            "wav":"#3498db",
            "wma":"#3498db",
            /*videos same color*/
            "wmv":"#2ecc71",
            "3gp":"#2ecc71",
            "mp4":"#2ecc71",
            "plv":"#2ecc71",
            "mpg":"#2ecc71",
            "mpeg":"#2ecc71",
            "mkv":"#2ecc71",
            "rm":"#2ecc71",
            "rmvb":"#2ecc71",
            "mov":"#2ecc71",
            /*office products same color*/
            "doc":"#1abc9c",
            "xls":"#1abc9c",
            "ppt":"#1abc9c",
            "docx":"#1abc9c",
            "xlsx":"#1abc9c",
            "pptx":"#1abc9c",
            /*mac products same color*/
            "pages":"#e74c3c",
            "key":"#e74c3c",
            "numbers":"#e74c3c",

            "pdf": "#7b615c",
            "epub": "#7b615c",
            /*programming langs*/
            "c":"#f1c40f",
            "cpp":"#f1c40f",
            "h":"#f1c40f",
            "html":"#f1c40f",
            "js":"#f1c40f",
            "css":"#f1c40f",
            "pl":"#f1c40f",
            "py":"#f1c40f",
            "php":"#f1c40f",
            "sql":"#f1c40f",
            "csv":"#de783b",
            "odp":"#de783b",
            /*zip files*/
            "gz":"#6ab975",
            "tar":"#6ab975",
            "rar":"#6ab975",
            "zip":"#6ab975",
            "7z":"#6ab975",
            "default": "#34495e"
        },
        totalSize : 0,
        result:{},
        overallSize:0,
        buildHierarchy:function(csv) {
            var root = {"name": "root", "children": []};
            for (var i = 0; i < csv.length; i++) {
                var sequence = csv[i][0];

                var size = +csv[i][1];
                if (isNaN(size)) { // e.g. if this is a header row
                    continue;
                }
                var parts = sequence.split("/");
                var currentNode = root;
                for (var j = 0; j < parts.length; j++) {
                    var children = currentNode["children"];
                    var nodeName = parts[j];
                    var childNode;
                    if (j + 1 < parts.length) {
                        // Not yet at the end of the sequence; move down the tree.
                        var foundChild = false;
                        for (var k = 0; k < children.length; k++) {
                            if (children[k]["name"] == nodeName) {
                                childNode = children[k];
                                foundChild = true;
                                break;
                            }
                        }
                        // If we don't already have a child node for this branch, create it.
                        if (!foundChild) {
                            childNode = {"name": nodeName, "type": "dir", "children": [],"path":sequence};
                            children.push(childNode);
                        }
                        currentNode = childNode;
                    } else {
                        var filetype = nodeName.split('.').pop();
                        if (filetype.length < 7) {
                            if (filetype in this.result) {
                                this.result[filetype] += size;
                            }
                            else {
                                this.result[filetype] = size;
                            }
                        }
                        childNode = {"name": nodeName, "type": filetype, "size": size,"path":sequence};
                        this.overallSize += size;
                        children.push(childNode);
                    }
                }
            }
            return root;
        },
        bytesToSize:function(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return 'n/a';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            if (i == 0) return bytes + ' ' + sizes[i];
            return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
        },
        getAncestors:function(node) {
            var path = [];
            var current = node;
            while (current.parent) {
                path.unshift(current);
                current = current.parent;
            }
            return path;
        },
        highlight:function(d){
            if(d){
                var percentage = (100 * d.value / this.totalSize).toPrecision(3);
                var percentageString = percentage + "%";
                if (percentage < 0.1) {
                    percentageString = "< 0.1%";
                }
                var percentageDetail = this.bytesToSize(d.value) + '/' + this.bytesToSize(this.totalSize);
                var colors = this.colors;
                d3.select("#percentage")
                    .text(percentageString);
                d3.select("#percentageDetail")
                    .text(percentageDetail);
                d3.select("#explanation")
                    .style("visibility", "");
                var sequenceArray = this.getAncestors(d);
                var arrayLength = sequenceArray.length;
                var htmlString = '';
                for (var i = 0; i < arrayLength; i++) {
                    var nodeType = sequenceArray[i].type;
                    if (nodeType == 'dir'){
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'/</span>';
                    }
                    else {
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'</span>';
                    }
                }
                $("#detailInfo").html(htmlString);
                // Fade all the segments.
                d3.selectAll("path").style("opacity", 0.3);
                // Then highlight only those that are an ancestor of the current segment.
                var foundNode = this.vis.selectAll("path")
                    .filter(function(node) {
                        return (sequenceArray.indexOf(node) >= 0);
                    })
                    .style("opacity", 1);
            }else{
                console.error('cant find '+path);
            }
        },
        createChart1:function(json,vis,width,height,radius) {
            vis.append("svg:circle")
                .attr("r", radius)
                .style("opacity", 0);

            var colors = this.colors,
                self = this;
            var partition = d3.layout.partition(radius)
                .size([2 * Math.PI, radius * radius])
                .value(function(d) { return d.size; });
            function mouseleave(d) {

                //return;

                // Hide the breadcrumb trail
                d3.select("#trail")
                    .style("visibility", "hidden");

                // Deactivate all segments during transition.
                d3.selectAll("path").on("mouseover", null);

                // Transition each segment to full opacity and then reactivate it.
                d3.selectAll("path")
                    .transition()
                    .duration(1000)
                    .style("opacity", 1)
                    .each("end", function() {
                        d3.select(this).on("mouseover", mouseover);
                    });

                d3.select("#explanation")
                    .transition()
                    .duration(1000)
                    .style("visibility", "hidden");

                $("#detailInfo").html("");

            }
            function mouseover(d) {
                var percentage = (100 * d.value / self.totalSize).toPrecision(3);
                var percentageString = percentage + "%";
                if (percentage < 0.1) {
                    percentageString = "< 0.1%";
                }
                var percentageDetail = self.bytesToSize(d.value) + '/' + self.bytesToSize(self.totalSize);

                d3.select("#percentage")
                    .text(percentageString);
                d3.select("#percentageDetail")
                    .text(percentageDetail);

                d3.select("#explanation")
                    .style("visibility", "");
                var sequenceArray = self.getAncestors(d);
                var arrayLength = sequenceArray.length;
                var htmlString = '';
                for (var i = 0; i < arrayLength; i++) {
                    var nodeType = sequenceArray[i].type;
                    if (nodeType == 'dir'){
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'/</span>';
                    }
                    else {
                        htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'</span>';
                    }
                }
                $("#detailInfo").html(htmlString);

                // Fade all the segments.
                d3.selectAll("path")
                    .style("opacity", 0.3);

                // Then highlight only those that are an ancestor of the current segment.
                vis.selectAll("path")
                    .filter(function(node) {
                        return (sequenceArray.indexOf(node) >= 0);
                    })
                    .style("opacity", 1);
            }

            // For efficiency, filter nodes to keep only those large enough to see.
            var nodes = partition.nodes(json,radius)
                .filter(function(d) {
                    return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
                });

            var arc = d3.svg.arc()
                .startAngle(function(d) { return d.x; })
                .endAngle(function(d) { return d.x + d.dx; })
                .innerRadius(function(d) { return Math.sqrt(d.y); })
                .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

            var path = vis.data([json]).selectAll("path")
                .data(nodes)
                .enter().append("svg:path")
                .attr("display", function(d) { return d.depth ? null : "none"; })
                .attr("d", arc)
                .attr("fill-rule", "evenodd")
                .style("fill", function(d) { return colors[d.type]?colors[d.type]:colors["default"]; })
                .style("opacity", 1)
                .on("mouseover", mouseover);

            // Add the mouseleave handler to the bounding circle.
            d3.select("#container").on("mouseleave", mouseleave);

            // Get total size of the tree = value of root node from partition.
            this.totalSize = path.node().__data__.value;

            return nodes;
        },
        render:function(data){
            var node = $(this.tab2);
            var width = node.height() -50;
            var height = node.height() -50;
            if(width < 400){
                width = 400;
            }
            if(height < 300){
                height = 300;
            }
            var radius = height;
            var visParent = this.chart1;
            var vis = d3.select(visParent).append("svg:svg")
                .attr("width", width)
                .attr("height", height)
                .append("svg:g")
                .attr("id", "container")
                .attr("transform", "translate(" + height/2 + "," + height/2  + ")");

            var nodes = this.createChart1(data,vis,width,height,radius/4,vis);
            var filetypeJSON = [];
            for (var x in this.result) {
                var tempF = this.result[x] / this.overallSize;
                if (tempF < 0.001) {
                    //console.error('skip');
                    continue;
                }
                var tempN = this.bytesToSize(this.result[x]) + '/' + this.bytesToSize(this.overallSize);
                var temp = {filetype: x, "usage": tempF, "detail": tempN};
                filetypeJSON.push(temp);
            }

            this.vis = vis;
            this.d3Nodes = nodes;

            var node2 = $(this.tab1);
            width = node2.width();
            if(width < 400){
                width = 400;
            }
            this.createChart2(filetypeJSON,width,height-100);

        },
        createChart2:function(inputJson,width,height) {
            var margin = {top: 20, right: 30, bottom: 30, left: 40};

            var x = d3.scale.ordinal()
                .rangeRoundBands([0, width], .1);

            var y = d3.scale.linear()
                .range([height, 0]);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");


            var node = this.tab1.children[0],
                colors = this.colors;

            var chart = d3.select(node)
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var data = inputJson;

            x.domain(inputJson.map(function(d) {return d.filetype; }));

            y.domain([0, d3.max(inputJson, function(d) { return d.usage; })]);

            chart.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            chart.append("g")
                .attr("class", "y axis")
                .call(yAxis);

            chart.selectAll(".bar")
                .data(inputJson)
                .enter()
                .append("rect")
                .attr("class", "bar")
                .attr("id", function(d) { return 'bar'+d.filetype;})
                .attr("x", function(d) { return x(d.filetype); })
                .attr("y", function(d) { return y(d.usage); })
                .attr("height", function(d) { return height - y(d.usage); })
                .attr("width", x.rangeBand())
                .attr("fill", function(d) { return colors[d.filetype]?colors[d.filetype]:colors["default"]; })
                .on("mouseover", function(d) {
                    d3.select(this)
                        .transition()
                        .duration(50)
                        .attr("fill", "#7f8c8d");

                    //Get this bar's x/y values, then augment for the tooltip
                    var xPosition = parseFloat(d3.select(this).attr("x")) + x.rangeBand() / 2;
                    var yPosition = parseFloat(d3.select(this).attr("y")) / 2 + height / 2;

                    var usageString = parseFloat(d.usage * 100).toFixed(2) + '%('+ d.filetype +')';
                    //Update the tooltip position and value
                    d3.select("#tooltip")
                        .style("left", xPosition + "px")
                        .style("top", yPosition + "px")
                        .select("#chart2Per2")
                        .text(d.detail);

                    d3.select("#chart2Per1")
                        .text(usageString);

                    //Show the tooltip
                    d3.select("#tooltip").classed("hidden", false);
                })

                .on("mouseout", function() {
                    d3.select(this)
                        .transition()
                        .delay(100)
                        .duration(250)
                        .attr("fill", function(d) { return colors[d.filetype]?colors[d.filetype]:colors["default"]; })

                    //Hide the tooltip
                    d3.select("#tooltip").classed("hidden", true);

                });
        },
        startup:function(){
            if(!this.owner){
                return;
            }
            var owner = this.owner,
                rows = owner.getRows();
            var csv = [];
            _.each(rows,function(row){
                csv.push([row.path.replace('./',''),"" + row.sizeBytes]);
            });
            var json = this.buildHierarchy(csv);
            this.render(json);
            var self = this;
            owner._on('selectionChanged',function(evt){
                if(evt.why=='clear'){
                    return;
                }
                var selection = evt.selection;
                var first = selection ? selection[0] : null;
                var row = owner.row(first);
                var element = row ? row.element : null;
                if(!first || !element){
                    return;
                }
                var path = first.path.replace('./','');
                if(!self.d3Nodes){
                    return;
                }
                self.select(path);
            })
        }
    });
    return Module;
});
},
'xfile/views/FileGrid':function(){
/** @module xfile/views/FileGrid **/
define([
    "xdojo/declare",
    'dojo/Deferred',
    'xide/types',
    'xide/utils',
    'xide/views/History',
    'xaction/DefaultActions',
    'xfile/views/Grid',
    'xfile/factory/Store',
    'xide/model/Path',
    'xfile/model/File',
    "xfile/types",
    'xlang/i18',
    'xide/$'
], function (declare, Deferred, types, utils, History, DefaultActions, Grid, factory, Path, File, fTypes, il8,$) {
    var DEFAULT_PERMISSIONS = fTypes.DEFAULT_FILE_GRID_PERMISSIONS;
    /**
     * A grid feature
     * @class module:xfile/views/FileGrid
     */
    var GridClass = declare('xfile.views.FileGrid', Grid, {
        resizeAfterStartup: true,
        formatters:{},
        hideExtensions:false,
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 50,
            'Settings': 20,
            'Navigation': 10,
            'Window': 5
        },
        groupOrder: {
            'Clipboard': 110,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'Navigation': 5,
            'Select': 0
        },
        tabOrder: {
            'Home': 100,
            'View': 50,
            'Settings': 20,
            'Navigation': 10
        },
        /**
         *
         */
        noDataMessage: '<span/>',
        /**
         * history {module:xide/views/History}
         */
        _history: null,
        options: utils.clone(types.DEFAULT_GRID_OPTIONS),
        _columns: {},
        toolbarInitiallyHidden: true,
        itemType: types.ITEM_TYPE.FILE,
        permissions: DEFAULT_PERMISSIONS,
        contextMenuArgs: {
            limitTo: null,
            actionFilter: {
                quick: true
            }
        },
        /**
         *
         * @param state
         * @returns {*}
         */
        setState: function (state) {
            this.inherited(arguments);
            var self = this,
                collection = self.collection,
                path = state.store.path,
                item = collection.getSync(path),
                dfd = self.refresh();

            dfd.then(function () {
                item = collection.getItem(path, true).then(function (item) {
                    self.openFolder(item);
                });
            });
            return dfd;
        },
        postMixInProperties: function () {
            var state = this.state;
            if (state) {
                if (state._columns) {
                    this._columns = state._columns;
                }
            }
            if (!this.columns) {
                this.columns = this.getColumns();
            }
            if (!this.collection && this.state) {
                var _store = this.state.store,
                    ctx = this.ctx,
                    store = factory.createFileStore(_store.mount, _store.storeOptions, ctx.config);
                this.collection = store.getDefaultCollection();
            }
            return this.inherited(arguments);
        },
        /**
         *
         * @param state
         * @returns {object}
         */
        getState: function (state) {
            state = this.inherited(arguments) || {};
            state.store = {
                mount: this.collection.mount,
                path: this.getCurrentFolder().path,
                storeOptions: this.collection.options
            };
            state._columns = {};
            _.each(this._columns, function (c) {
                state._columns[c.label] = !this.isColumnHidden(c.id);
            }, this);
            return state;
        },
        onSaveLayout: function (e) {
            var customData = e.data,
                gridState = this.getState(),
                data = {
                    widget: this.declaredClass,
                    state: gridState
                };
            customData.widgets.push(data);

            return customData;
        },
        formatColumn: function (field, value, obj) {
            var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
            if (renderer.formatColumn) {
                var result = renderer.formatColumn.apply(arguments);
                if (result) {
                    return result;
                }
            }
            if (obj.renderColumn) {
                var rendered = obj.renderColumn.apply(this, arguments);
                if (rendered) {
                    return rendered;
                }
            }
            switch (field) {
                case "fileType": {
                    if (value == 'folder') {
                        return il8.localize('kindFolder');
                    } else {
                        if (obj.mime) {
                            var mime = obj.mime.split('/')[1] || "unknown";
                            var key = 'kind' + mime.toUpperCase();
                            var _translated = il8.localize(key);
                            return key !== _translated ? _translated : value;
                        }
                    }
                }
                case "name": {
                    var directory = obj && obj.directory != null && obj.directory === true;
                    var no_access = obj.read === false && obj.write === false;
                    var isBack = obj.name == '..';
                    var folderClass = 'fa-folder';
                    var icon = '';
                    var imageClass = '';
                    var useCSS = false;
                    if (directory) {
                        if (isBack) {
                            imageClass = 'fa fa-level-up itemFolderList';
                            useCSS = true;
                        } else if (!no_access) {
                            imageClass = 'fa ' + folderClass + ' itemFolderList';

                            useCSS = true;
                        } else {
                            imageClass = 'fa fa-lock itemFolderList';
                            useCSS = true;
                        }
                    } else {
                        if (!no_access) {
                            imageClass = 'itemFolderList fa ' + utils.getIconClass(obj.path);
                            useCSS = true;
                        } else {
                            imageClass = 'fa fa-lock itemFolderList';
                            useCSS = true;
                        }
                    }
                    var label = obj.showPath === true ? obj.path : value;
                        if (this.hideExtensions) {
                            label = utils.pathinfo(label).filename;
                        }

                    if (!useCSS) {
                        return '<img class="fileGridIconCell" src="' + icon + ' "/><span class="fileGridNameCell">' + label + '</span>';
                    } else {
                        return '<span class=\"' + imageClass + '\""></span><span class="name fileGridNameNode" style="vertical-align: middle;padding-top: 0px">' + label + '</span>';
                    }
                }
                case "sizeBytes": {
                    return obj.size;
                }
                case "fileType": {
                    return utils.capitalize(obj.fileType || 'unknown');
                }
                case "mediaInfo": {
                    return obj.mediaInfo || 'unknown';
                }
                case "owner": {
                    if (obj) {
                        var owner = obj.owner;
                        if (owner && owner.user) {
                            return owner.user.name;
                        }
                    }
                    return "";
                }
                case "modified": {
                    if (value === '') {
                        return value;
                    }
                    var directory = !obj.directory == null;
                    if (!directory) {
                        if(il8.translations.dateFormat){
                            return il8.formatDate(value).replace('ms', '');
                        }
                    }
                    return '';
                }
            }
            return value;
        },
        /**
         *
         * @param formatters
         * @returns {object|null}
         */
        getColumns: function (formatters) {
            formatters = formatters || this.formatters;
            var self = this;
            this.columns = [];
            function createColumn(label, field, sortable, hidden) {
                if (self._columns[label] != null) {
                    hidden = !self._columns[label];
                }
                self.columns.push({
                    renderExpando: label === 'Name',
                    label: label,
                    field: field,
                    sortable: sortable,
                    formatter: function (value, obj) {
                        return label in formatters ? formatters[label].apply(self,[field, value, obj]) : self.formatColumn(field, value, obj);
                    },
                    hidden: hidden
                });
            }
            createColumn('Name', 'name', true, false);
            createColumn('Type', 'fileType', true, true);
            createColumn('Path', 'path', true, true);
            createColumn('Size', 'sizeBytes', true, false);
            createColumn('Modified', 'modified', true, false);
            createColumn('Owner', 'owner', true, true);
            createColumn('Media', 'mediaInfo', true, true);
            return this.columns;
        },
        _focus: function () {
            var rows = this.getRows();
            if (rows[0]) {
                this.focus(this.row(rows[0]));
            }
        },
        /**
         *
         * @param item
         * @returns {boolean}
         */
        setQueryEx: function (item) {
            if (!item || !item.directory) {
                return false;
            }
            this._lastPath = item.getPath();
            var self = this, dfd = new Deferred();
            var col = self.collection;
            if (item.path === '.') {
                col.resetQueryLog();
                self.set("collection", col.getDefaultCollection(item.getPath()));
                dfd.resolve();
            } else {
                col.open(item).then(function (items) {
                    col.resetQueryLog();
                    self.set("collection", col.getDefaultCollection(item.getPath()));
                    dfd.resolve(items);
                });
            }
            return dfd;
        },
        getCurrentFolder: function () {
            var renderer = this.getSelectedRenderer();
            if (renderer && renderer.getCurrentFolder) {
                var _result = renderer.getCurrentFolder.apply(this);
                if (_result) {
                    if (_result.isBack) {
                        var __result = this.collection.getSync(_result.rPath);
                        if (__result) {
                            _result = __result;
                        }
                    }
                    return _result;
                }
            }

            var item = this.getRows()[0];
            if (item && (item._S || item._store)) {
                if (item.isBack === true) {
                    var _now = this.getHistory().getNow();
                    if (_now) {
                        return this.collection.getSync(_now);
                    }
                }
                //current folder:
                var _parent = item._S.getParent(item);
                if (_parent) {
                    return _parent;
                }
            }
            return null;
        },
        getClass: function () {
            return GridClass;
        },
        getHistory: function () {
            if (!this._history) {
                this._history = new History();
            }
            return this._history;
        },
        _createBackItem:function(path){
            return {
                name: '..',
                path: '..',
                rPath: path,
                sizeBytes: 0,
                size: '',
                icon: 'fa-level-up',
                isBack: true,
                modified: '',
                _S: this.collection,
                directory: true,
                _EX: true,
                children: [],
                mayHaveChildren: false
            }
        },
        startup: function () {
            if (this._started) {
                return;
            }
            var res = this.inherited(arguments);
            $(this.domNode).addClass('xfileGrid');
            this.set('loading', true);
            if (this.permissions) {
                this.addActions([].concat(DefaultActions.getDefaultActions(this.permissions, this, this).concat(this.getFileActions(this.permissions))));
            }
            this._history = new History();
            var self = this;
            this.subscribe(types.EVENTS.ON_CLIPBOARD_COPY, function (evt) {
                if (evt.type === self.itemType) {
                    self.currentCopySelection = evt.selection;
                    self.refreshActions();
                }
            });
            self._on('noData', function () {
                if (self._total > 0) {
                    return;
                }
                var _history = self._history,
                    now = _history.getNow();

                if (!now || now === './.') {
                    return;
                }
                self.renderArray([this._createBackItem(now)]);
            });
            this.on('dgrid-refresh-complete', function () {
                var rows = self.getRows();
                if (rows && rows.length > 1) {
                    var back = _.find(rows, {
                        isBack: true
                    });
                    if (back) {
                        self.removeRow(back);
                        self.refresh();
                    }
                }
            });

            this._on('openFolder', function (evt) {
                self.set('title', evt.item.name);
            });

            if (self.selectedRenderer) {
                res = this.refresh();
                res && res.then(function () {
                    self.set('loading', false);
                    self.setRenderer(self.selectedRenderer, false);
                });
            }
            this._on('onChangeRenderer',this.refresh);
            this._emit('startup');
            res.then(this.resize.bind(this));
            return res;
        }
    });

    /**
     *
     * @param ctx
     * @param args
     * @param parent
     * @param register
     * @param startup
     * @param store
     * @returns {module:xfile/views/Grid}
     */
    function createDefault(ctx, args, parent, register, startup, store) {
        var defaults = {
            collection: store.getDefaultCollection(),
            _parent: parent,
            Module: GridClass,
            ctx: ctx
        };
        utils.mixin(defaults, args || {});
        var grid = utils.addWidget(GridClass, defaults, null, parent, startup, null, null, true, null);
        register &&ctx.getWindowManager().registerView(grid, false);
        return grid;
    }

    GridClass.prototype.Module = GridClass;
    GridClass.Module = GridClass;
    GridClass.createDefault = createDefault;
    GridClass.DEFAULT_PERMISSIONS = DEFAULT_PERMISSIONS;
    return GridClass;
});
},
'xfile/factory/Store':function(){
define([
    'xide/types',
    'xide/factory',
    'xide/utils',
    'xfile/data/Store'
], function (types, factory,utils,Store){
    /**
     *
     * @param mount
     * @param options
     * @param config
     * @param optionsMixin
     * @param ctx
     * @param args
     * @returns {*}
     */
    factory.createFileStore = function (mount,options,config,optionsMixin,ctx,args){
        var storeClass = Store;
        options = options || {
            fields:
            types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            //types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME |
            types.FIELDS.SHOW_MEDIA_INFO
        };

        utils.mixin(options,optionsMixin);
        var store = new storeClass(utils.mixin({
            data:[],
            ctx:ctx,
            config:config,
            url:config.FILE_SERVICE,
            serviceUrl:config.serviceUrl,
            serviceClass:config.FILES_STORE_SERVICE_CLASS,
            mount:mount,
            options:options
        },args));
        store._state = {
            initiated:false,
            filter:null,
            filterDef:null
        };
        store.reset();
        store.setData([]);
        store.init();
        ctx && ctx.getFileManager().addStore(store);
        return store;
    };
    return factory;

});
},
'xfile/data/Store':function(){
/**
 * @module xfile/data/FileStore
 **/
define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    'dojo/Deferred',
    "xide/mixins/ReloadMixin",
    "xide/manager/ServerActionBase",
    'dstore/Cache',
    'dstore/QueryResults',
    'xide/types',
    'xide/utils',
    'dojo/when',
    'xide/data/TreeMemory',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xfile/model/File',
    'xide/lodash'
], function (declare, lang, Deferred, ReloadMixin, ServerActionBase, Cache, QueryResults, types, utils, when, TreeMemory, Trackable, ObservableStore, File, _) {
    var _debug = false;
    /**
     * Constants
     * @type {string}
     */
    var C_ITEM_EXPANDED = "_EX"; // Attribute indicating if a directory item is fully expanded.
    /**
     *
     * A store based on dstore/Memory and additional dstore mixins, used for all xfile grids.
     *
     * ###General
     *
     * - This store is only fetching file listings from the server! Any other file operation is done in
     * by file manager. However, this store is the central database for all files.
     *
     *
     * ###Common features and remarks
     *
     * - works with for multiple grid instances: lists/thumb or tree grids
     * - caches all directory listings, use _loadPath(path,force=true) to drop the cache
     * - all server methods return dojo/Deferred handles
     * - loads path segments recursivly, ie: getItem('./path/subPath/anotherSub');
     *
     * ### Server related
     * #### Requests

     * This part is done in the {link:xide/manager/ServerActionBase} ServerActionBase mixin, wrapped in here as
     * '_request'. This will take care about the possible transports : JSON-RPC-2.0 with a Dojo-SMD envelope or JSONP.
     * Both requests will be signed upon its payload, using this.config.RPC_PARAMETERS(server controlled), if required.
     * At the time of writing, this library has been developed for a PHP and Java-Struts which both support Dojo - SMD.
     * However, you may adjust to REST based environments yourself. Also, when using the
     * supplied XPHP - JSON-RPC-2.0-SMD implementation you can make use of 'batch-requests' to avoid further requests.
     *
     * #### Responses
     *
     * The response arrives here parsed already, no need to do anything but adding it to the store.
     *
     * @class module:xfile/data/FileStore
     * @augments module:xide/manager/ServerActionBase
     * @augments module:dgrid/Memory
     * @augments module:dgrid/Tree
     * @augments module:dgrid/Cache
     * @augments module:xide/mixins/ReloadMixin
     */
    function Implementation() {
        return {
            addDot: true,
            /**
             * 'recursive' will tell the server to run the directory listing recursive for server method 'ls'
             * @type {boolean}
             */
            recursive: false,
            rootSegment: ".",
            Model: File,
            /**
             * @member idProperty {string} sets the unique identifier for store items is set to the 'path' of a file.
             * @public
             */
            idProperty: "path",
            parentProperty: "path",
            parentField: 'parent',
            /**
             * @member mount {string} sets the 'mount' prefix for a VFS. This is needed to simplify the work
             * with paths and needs to be handled separated. By default any request expects full paths as:
             * - root/_my_path
             * - root://_my_path/even_deeper
             * - dropbox/_my_folder
             *
             * This property is is being used to complete a full file path automatically in the request so that only the
             * actual inner path needs to be specified in all methods.
             * By default xfile supports multiple stores in the same application.
             * Each store is created upon mount + store options hash.
             * @public
             */
            mount: 'path',
            /**
             * @member options{Object[]} specifies the store options. The store options are tied to the server. As the store
             * is only relavant for enumerating files through a server method, there are only a couple of options needed:
             *
             * <b>options.fields</b> {int} A enumeration bitmask which specifies the fields to be added for a file or
             * directory node. This is described in link:xfile/types/Types#Fields and again, this is being processed by
             * the server.
             *
             * options.includeList {string} a comma separated list of allowed file extensions, this has priority the
             * exclusion mask.
             *
             * options.excludeList {string} a comma separated list of excluded file extensions
             *
             * This options are set through xfile/manager/Context#defaultStoreOptions which might be overriden
             * through the server controlled xFileConfiguration.mixins global header script tag.
             *
             * @example {
                "fields": 1663,
                "includeList": "*",
                "excludeList": "*"
            }
             * @public
             */
            options: {
                fields: 1663,
                includeList: "*",
                excludeList: "*" // XPHP actually sets this to ".svn,.git,.idea" which are compatible to PHP's 'glob'
            },
            /**
             * @member serviceUrl {string} the path to the service entry point. There are 2 modes this end-point must
             * provide:
             * - a Dojo compatible SMD
             * - post/get requests with parameters. Currently JSONP & JSON-RPC-1/2.0 is supported.
             *
             * You can set also static SMD (see xide/manager/ServerActionBase) to bypass the SMD output. However,
             * this is being set usually by a server side mixin (in HTML header tag) on the fly!
             * @default null
             * @public
             */
            serviceUrl: null,
            /**
             * @member serviceClass {string} is the server class to be called. By default, this store creates a
             * JSON-RPC-2.0 post request where the methods looks like "XApp_Directory_Service.ls". Other formats are
             * supported in XPHP as well and support also composer autoloaders. If this is a singleton, you can also call
             * this.serviceObject.SERVER_CLASS.SERVER_METHOD(args) through here!
             * @public
             *
             */
            serviceClass: null,
            /**
             * @member singleton {boolean} Sets the ServerActionBase as 'Singleton' at the time of the construction. If there
             * is any other ServerActionBase subclass with the same service url, this will avoid additional requests to
             * fetch a SMD, furthermore, you can call other methods on the server through this here
             * as well: this.serviceObject.SERVER_CLASS.SERVER_METHOD(args)
             * @default true
             * @public
             */
            singleton: true,
            /////////////////////////////////////////////////////////////////////////////
            //
            //  dstore/Tree implementation
            //
            /////////////////////////////////////////////////////////////////////////////
            queryAccessors: true,
            autoEmitEvents: false, // this is handled by the methods themselves
            /*
             * test
             * @param object
             * @returns {boolean}
             */
            mayHaveChildren: function (object) {
                // summary:
                //		Check if an object may have children
                // description:
                //		This method is useful for eliminating the possibility that an object may have children,
                //		allowing collection consumers to determine things like whether to render UI for child-expansion
                //		and whether a query is necessary to retrieve an object's children.
                // object:
                //		The potential parent
                // returns: boolean
                if (object.mayHaveChildren == false) {
                    return false;
                }
                return object.directory === true;
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  Private part, might be trashed
            //
            /////////////////////////////////////////////////////////////////////////////
            _lastFilter: null,
            _lastFilterDef: null,
            _lastFilterItem: null,
            _initiated: {
                value: false
            },
            id: null,
            _state: {
                initiated: false,
                filter: null,
                filterDef: null
            },
            isInitiated: function () {
                return this._state.initiated;
            },
            setInitiated: function (initiated) {
                this._state.initiated = initiated;
            },
            _extraSortProperties: {
                name: {
                    ignoreCase: true
                }
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  Sorting
            //
            /////////////////////////////////////////////////////////////////////////////

            constructor: function () {
                this.id = utils.createUUID();
            },
            onSorted: function (sorted, data) {
                if (sorted.length == 1 && sorted[0].property === 'name') {
                    var upperCaseFirst = true,
                        directoriesFirst = true,
                        descending = sorted[0].descending;

                    if (directoriesFirst) {
                        function _sort(item) {
                            return upperCaseFirst ? item.name : item.name.toLowerCase();
                        }
                        var grouped = _.groupBy(data, function (item) {
                            return item.directory === true;
                        }, this);

                        data = _.sortBy(grouped['true'], _sort);
                        data = data.concat(_.sortBy(grouped['false'], _sort));
                        if (descending) {
                            data.reverse();
                        }
                    }
                }
                var _back = _.find(data, {
                    name: '..'
                });
                if (_back) {
                    data.remove(_back);
                    data.unshift(_back);
                }

                data = this.onAfterSort(data);

                return data;
            },
            onAfterSort: function (data) {
                var micromatch = this.micromatch;
                if (typeof mm !== 'undefined' && micromatch && data && data[0]) {
                    var what = data[0].realPath ? 'realPath' : 'path';
                    what = 'name';
                    var _items = _.pluck(data, what);
                    var matching = mm(_items, micromatch);
                    data = data.filter(function (item) {
                        if (matching.indexOf(item[what]) === -1) {
                            return null;
                        }
                        return item;
                    });
                    if (this._onAfterSort) {
                        data = this._onAfterSort(data);
                    }
                }
                return data;
            },
            /**
             * Overrides dstore method to add support for case in-sensitive sorting. This requires
             * ignoreCase: true in a this.sort(..) call, ie:
             *
             *  return [{property: 'name', descending: false, ignoreCase: true}]
             *
             * this will involve this._extraSortProperties and is being called by this.getDefaultSort().
             *
             * @param sorted
             * @returns {Function}
             * @private
             */
            _createSortQuerier: function (sorted) {
                var thiz = this;
                return function (data) {
                    data = data.slice();
                    data.sort(typeof sorted == 'function' ? sorted : function (a, b) {
                        for (var i = 0; i < sorted.length; i++) {
                            var comparison;
                            if (typeof sorted[i] == 'function') {
                                comparison = sorted[i](a, b);
                            } else {
                                var property = sorted[i].property;
                                if (thiz._extraSortProperties[property]) {
                                    utils.mixin(sorted[i], thiz._extraSortProperties[property]);
                                }
                                var descending = sorted[i].descending;
                                var aValue = a.get ? a.get(property) : a[property];
                                var bValue = b.get ? b.get(property) : b[property];
                                var ignoreCase = !!sorted[i].ignoreCase;
                                aValue != null && (aValue = aValue.valueOf());
                                bValue != null && (bValue = bValue.valueOf());

                                if (ignoreCase) {
                                    aValue.toUpperCase && (aValue = aValue.toUpperCase());
                                    bValue.toUpperCase && (bValue = bValue.toUpperCase());
                                }
                                comparison = aValue === bValue ? 0 : (!!descending === (aValue === null || aValue > bValue) ? -1 : 1);
                            }
                            if (comparison !== 0) {
                                return comparison;
                            }
                        }
                        return 0;
                    });
                    return thiz.onSorted(sorted, data);
                };
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  Public API section
            //
            /////////////////////////////////////////////////////////////////////////////
            _getItem: function (path, allowNonLoaded) {
                //try instant and return when loaded
                //this.getSync(path.replace('./',''))
                if (path === '/') {
                    path = '.';
                }
                var item = this.getSync(path) || this.getSync('./' + path);
                if (item && (this.isItemLoaded(item) || allowNonLoaded == true)) {
                    return item;
                }
                if (path === '.') {
                    return this.getRootItem();
                }
                return null;
            },
            /**
             * Returns a promise or a store item. This works recursively for any path and
             * results in one request per path segment or a single request when batch-requests
             * are enabled on the server.

             * @param path {string} a unique path, ie: ./ | . | ./myFolder | ./myFolder/and_deeper. If the item isn't
             * fully loaded yet, it just returns the item, if you enable 'load' and does the full load.
             * @param load {boolean} load the item if not already
             * @param options {object|null} request options
             *
             * @returns {Object|Deferred|null}
             */
            getItem: function (path, load, options) {
                path = path.replace('./', '');
                if (load == false) {
                    return this._getItem(path);
                } else if (load == true) {
                    //at this point we have to load recursively
                    var parts = path.split('/'),
                        thiz = this,
                        partsToLoad = [],
                        item = thiz.getSync(path);

                    if (item && this.isItemLoaded(item)) {
                        return item;
                    }

                    //new head promise for all underlying this.getItem calls
                    var deferred = new Deferred();
                    var _loadNext = function () {
                        //no additional lodash or array stuff please, keep it simple
                        var isFinish = !_.find(partsToLoad, {
                            loaded: false
                        });
                        if (isFinish) {
                            deferred.resolve(thiz._getItem(path));
                        } else {
                            
                            for (var i = 0; i < partsToLoad.length; i++) {
                                if (!partsToLoad[i].loaded) {
                                    var _item = thiz.getSync(partsToLoad[i].path);
                                    if (_item) {
                                        if (_item.directory === true && thiz.isItemLoaded(_item)) {
                                            partsToLoad[i].loaded = true;
                                            continue;
                                        } else if (_item.directory == null) {
                                            deferred.resolve(_item);
                                            break;
                                        }
                                    }
                                    thiz._loadPath(partsToLoad[i].path, false, options).then(function (items) {
                                        partsToLoad[i].loaded = true;
                                        _loadNext();
                                    }, function (err) {
                                        var _i = Math.abs(Math.min(0, i - 1));
                                        var nextPart = partsToLoad[_i];
                                        var parts = partsToLoad;
                                        if (!nextPart) {
                                            _i = partsToLoad.length - 1;
                                            nextPart = partsToLoad[_i];
                                        }
                                        var _item = thiz.getItem(nextPart.path);
                                        when(thiz.getItem(partsToLoad[_i].path, false), function (_item) {
                                            deferred.resolve(_item, partsToLoad[_i].path);
                                        });
                                    });
                                    break;
                                }
                            }


                            var isFinish = !_.find(partsToLoad, {
                                loaded: false
                            });
                            if (isFinish) {
                                deferred.resolve(thiz._getItem(path));
                            }
                        }
                    };

                    //prepare process array
                    var itemStr = '.';
                    for (var i = 0; i < parts.length; i++) {
                        if (parts[i] == '.') {
                            continue;
                        }
                        if (parts.length > 0) {
                            itemStr += '/';
                        }
                        itemStr += parts[i];
                        partsToLoad.push({
                            path: itemStr,
                            loaded: false
                        });
                    }

                    //fire
                    _loadNext();
                    return deferred;
                }
                if (path === '.') {
                    return this.getRootItem();
                }
                return null;
            },
            /**
             * Return the root item, is actually private
             * @TODO: root item unclear
             * @returns {{path: string, name: string, mount: *, directory: boolean, virtual: boolean, _S: (xfile|data|FileStore), getPath: Function}}
             */
            getRootItem: function () {
                return {
                    _EX: true,
                    path: '.',
                    name: '.',
                    mount: this.mount,
                    directory: true,
                    virtual: true,
                    _S: this,
                    getPath: function () {
                        return this.path + '/';
                    }
                };
            },
            /**
             * back compat, trash
             */
            getItemByPath: function () {
                // 0 && console.log('FILESTORE::getItemByPath',arguments);
            },
            /*
             * */
            getParents: function () {
                return null;
            },
            /**
             * Return parent object in sync mode, default to root item
             * @TODO fix root problem
             * @param mixed {string|object}
             */
            getParent: function (mixed) {
                if (!mixed) {
                    return null;
                }
                var item = mixed,
                    result = null;

                if (_.isString(item)) {
                    item = this.getSync(mixed);
                }

                if (item && item.parent) {
                    result = this.getSync(item.parent);
                }
                return result || this.getRootItem();
            },
            /**
             * Return 'loaded' state
             * @param item
             * @returns {boolean}
             */
            isItemLoaded: function (item) {
                return item && (!item.directory || this._isLoaded(item));
            },
            /**
             * Wrap loadItem
             * @TODO yeah, what?
             * @param item
             * @param force
             * @returns {*}
             */
            loadItem: function (item, force) {
                return this._loadItem(item, force);
            },
            /**
             * Fix an incoming item for our needs, adds the _S(this) attribute and
             * a function to safely return a its path since there are items with fake paths as: './myPath_back_'
             * @param item
             * @private
             */
            _parse: function (item) {
                item._S = this;
                if (!_.isEmpty(item.children)) {
                    _.each(item.children, function (_item) {
                        if (!_item.parent) {
                            _item.parent = item.path;
                        }
                        this._parse(_item);
                    }, this);

                    item._EX = true;
                    item.children = this.addItems(item.children);
                }
                item.getPath = function () {
                    return this.path;
                };
                if (!this.getSync(item.path)) {
                    this.putSync(item);
                }
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  True store impl.
            //
            /////////////////////////////////////////////////////////////////////////////
            /**
             * Here to load an item forcefully (reload/refresh)
             * @param path
             * @param force
             * @param options {object|null}
             * @returns {*}
             * @private
             */
            _loadPath: function (path, force, options) {
                var thiz = this;
                var result = this._request(path, options);
                // 0 && console.log('load path : ' + path);
                result.then(function (items) {
                        // 0 && console.log('got : items for ' + path, items);
                        var _item = thiz._getItem(path, true);
                        if (_item) {
                            if (force) {
                                if (!_.isEmpty(_item.children)) {
                                    thiz.removeItems(_item.children);
                                }
                            }
                            _item._EX = true;
                            thiz.addItems(items, force);
                            _item.children = items;
                            return items;
                        } else {
                            if (options && options.onError) {
                                options.onError('Error Requesting path on server : ' + path);
                            } else {
                                throw new Error('cant get item at ' + path);
                            }
                        }
                    }.bind(this),
                    function (err) {
                        console.error('error in load');
                    });

                return result;

            },
            /**
             * Creates an object, throws an error if the object already exists.
             * @param object {Object} The object to store.
             * @param options {Object} Additional metadata for storing the data.  Includes an 'id' property if a specific
             * id is to be used. dstore/Store.PutDirectives?
             * @returns {Number|Object}
             */
            addSync: function (object, options) {
                (options = options || {}).overwrite = false;
                // call put with overwrite being false
                return this.putSync(object, options);
            },
            /**
             * @TODO: what?
             * @param item
             * @param force
             * @returns {Deferred}
             * @private
             */
            _loadItem: function (item, force) {
                var deferred = new Deferred(),
                    thiz = this;
                if (!item) {
                    deferred.reject('need item');
                    return deferred;
                }
                if (force) {
                    //special case on root
                    if (item.path === '.') {
                        thiz.setInitiated(false);
                        thiz.fetchRange().then(function (items) {
                            deferred.resolve({
                                store: thiz,
                                items: items,
                                item: item
                            });
                        });
                    } else {
                        this._loadPath(item.path, true).then((items)=> {
                            var _item = this.getSync(item.path);
                            deferred.resolve(_item);
                        }, function (err) {
                            console.error('error occured whilst loading items');
                            deferred.reject(err);
                        });
                    }
                }
                return deferred;
            },
            _normalize: function (response) {
                if (response && response.items) {
                    return response.items[0];
                }
                return [];
            },
            _isLoaded: function (item) {
                return item && item[C_ITEM_EXPANDED] === true;
            },
            fetch: function () {

            },
            put: function () {

            },
            add: function (item) {
                var _item = this.getSync(item.path);
                if (!_item) {
                    _item = this.addSync(item);
                    _item._S = this;
                    _item.getPath = function () {
                        return this.path;
                    };
                }
                return _item;
            },
            removeItems: function (items) {
                _.each(items, function (item) {
                    if (this.getSync(item.path)) {
                        this.removeSync(item.path);
                    }
                }, this);
            },
            getSync: function (id) {
                id = id || '';
                var data = this.storage.fullData;
                return data[this.storage.index[id]] || data[this.storage.index[id.replace('./', '')]];
            },
            addItems: function (items) {
                var result = [];
                _.each(items, function (item) {
                    var storeItem = this.getSync(item.path);
                    if (storeItem) {
                        this.removeSync(item.path);
                    }
                    result.push(this.add(item));
                }, this);
                return result;
            },
            open: function (item) {
                var thiz = this;
                if (!this._isLoaded(item)) {
                    item.isLoading = true;
                    return thiz._request(item.path).then(function (items) {
                        item.isLoading = false;
                        item._EX = true;
                        thiz.addItems(items);
                        item.children = items;
                        return items;
                    });
                } else {
                    var deferred = new Deferred();
                    thiz.resetQueryLog();
                    deferred.resolve(item.children);
                    return deferred;
                }
            },
            getDefaultSort: function () {
                return [{
                    property: 'name',
                    descending: false,
                    ignoreCase: true
                }];
            },
            filter: function (data) {
                if (data && typeof data === 'function') {
                    return this.inherited(arguments);
                }
                if (data.parent) {
                    this._state.path = data.parent;
                }
                var item = this.getSync(data.parent);
                if (item) {
                    if (!this.isItemLoaded(item)) {
                        item.isLoading = true;
                        this._state.filterDef = this._loadPath(item.path);
                        this._state.filterDef.then(function () {
                            item.isLoading = false;
                        })
                    } else {
                        /*
                        if(item.children) {
                            var total = new Deferred();
                            total.resolve(item.children);
                            this._state.filterDef = total;
                            this._state.filter = data;
                        }
                        */
                        this._state.filterDef = null;
                    }
                }
                delete this._state.filter;
                this._state.filter = data;
                return this.inherited(arguments);
            },
            _request: function (path, options) {
                var collection = this;
                const onError = ((e) => {
                    if (options && options.displayError === false) {
                        return;
                    }
                    logError(e, 'error in FileStore : ' + this.mount + ' :' + e);
                });
                const promise = this.runDeferred(null, 'ls', {
                        path: path,
                        mount: this.mount,
                        options: this.options,
                        recursive: this.recursive
                    },
                    utils.mixin({
                        checkErrors: false,
                        displayError: true
                    }, options), onError).then(function (response) {
                    var results = collection._normalize(response);
                    collection._parse(results);
                    // support items in the results
                    results = results.children || results;
                    return results;
                }, function (e) {
                    if (options && options.displayError === false) {
                        return;
                    }
                    logError(e, 'error in FileStore : ' + this.mount + ' :' + e);
                });
                return promise;
            },
            fetchRangeSync: function () {
                var data = this.fetchSync();
                var total = new Deferred();
                total.resolve(data.length);
                return new QueryResults(data, {
                    totalLength: total
                });
            },
            reset: function () {
                this._state.filter = null;
                this._state.filterDef = null;
                this.resetQueryLog();
            },
            resetQueryLog: function () {
                this.queryLog = [];
            },
            fetchRange: function () {
                // dstore/Memory#fetchRange always uses fetchSync, which we aren't extending,
                // so we need to extend this as well.
                var results = this._fetchRange();
                return new QueryResults(results.then(function (data) {
                    return data;
                }), {
                    totalLength: results.then(function (data) {
                        return data.length;
                    })
                });
            },
            initRoot: function () {
                //first time load
                var _path = '.';
                var thiz = this;
                //business as usual, root is loaded
                if (!this.isInitiated()) {
                    return thiz._request(_path).then(function (data) {
                        if (!thiz.isInitiated()) {
                            _.each(data, thiz._parse, thiz);
                            thiz.setData(data);
                            thiz.setInitiated(true);
                            thiz.emit('loaded');
                        }
                        return thiz.fetchRangeSync(arguments);
                    }.bind(this));
                }
                var dfd = new Deferred();
                dfd.resolve();
                return dfd;
            },
            _fetchRange: function () {
                //special case for trees
                if (this._state.filter) {
                    var def = this._state.filterDef;
                    if (def) {
                        def.then(function (items) {
                            this.reset();
                            if (def && def.resolve) {
                                def.resolve(items);
                            }
                        }.bind(this));
                        return def;
                    }
                }
                //first time load
                var _path = '.';
                var thiz = this;
                //business as usual, root is loaded
                if (this.isInitiated()) {
                    var _def = thiz.fetchRangeSync(arguments);
                    var resultsDeferred = new Deferred();
                    var totalDeferred = new Deferred();
                    resultsDeferred.resolve(_def);
                    totalDeferred.resolve(_def.length);
                    thiz.emit('loaded');
                    return new QueryResults(resultsDeferred, {
                        totalLength: _def.totalLength
                    });
                }
                return thiz._request(_path).then(function (data) {
                    if (!thiz.isInitiated()) {
                        _.each(data, thiz._parse, thiz);
                        thiz.setData(data);
                        thiz.setInitiated(true);
                        thiz.emit('loaded');
                    }
                    return thiz.fetchRangeSync(arguments);
                }.bind(this));
            },
            getDefaultCollection: function (path) {
                var _sort = this.getDefaultSort();
                if (path == null) {
                    return this.sort(_sort);
                } else {
                    /*
                    return this.filter({
                        parent: path
                    }).sort(_sort);
                    */
                    return this.filter(function (item) {
                        return item.parent === path;
                        return res;
                    }).sort(_sort);
                }
            },
            getChildren: function (object) {
                // summary:
                //		Get a collection of the children of the provided parent object
                // object:
                //		The parent object
                // returns: dstore/Store.Collection
                return this.root.filter({
                    parent: this.getIdentity(object)
                });
            },
            spin: true
        };
    }
    var Module = declare("xfile/data/Store", [TreeMemory, Cache, Trackable, ObservableStore, ServerActionBase.declare, ReloadMixin], Implementation());
    Module.Implementation = Implementation;
    return Module;
});
},
'xfile/model/File':function(){
/** @module xfile/model/File **/
define([
    "dcl/dcl",
    "xide/data/Model",
    "xide/utils",
    "xide/types",
    "xide/lodash"
], function (dcl, Model, utils, types, _) {
    /**
     * @class module:xfile/model/File
     */
    return dcl(Model, {
        declaredClass: 'xfile.model.File',
        getFolder: function () {
            var path = this.getPath();
            if (this.directory) {
                return path;
            }
            return utils.pathinfo(path, types.PATH_PARTS.ALL).dirname;
        },
        getChildren: function () {
            return this.children;
        },
        getParent: function () {
            //current folder:
            var store = this.getStore() || this._S;
            return store.getParent(this);
        },
        getChild: function (path) {
            return _.find(this.getChildren(), {
                path: path
            });
        },
        getStore: function () {
            return this._store || this._S;
        }
    });
});
},
'xfile/config':function(){
define([
    'dojo/_base/declare',
    'xide/types'
],function(declare,types){

    types.config={

        /**
         * Root node id to for xfile main application, can be ignored for embedded mode.
         */
        ROOT_NODE:'xapp',
        /***
         * The absolute url to server rpc end-point
         */
        FILE_SERVICE:'',
        /***
         * Default start path
         */
        START_PATH:'./',
        /***
         * A pointer to a resolved AMD prototype. This should be xfile/data/FileStore
         */
        FILES_STORE_SERVICE_CLASS:'XCOM_Directory_Service',
        /***
         *  Enables file picker controls, obsolete at the moment
         */
        IS_MEDIA_PICKER:null,
        EDITOR_NODE:null,
        EDITOR_AFTER_NODE:null,
        ACTION_TOOLBAR:null,

        ALLOWED_DISPLAY_MODES:{
            TREE:1,
            LIST:1,
            THUMB:1,
            IMAGE_GRID:1
        },

        LAYOUT_PRESET:null,

        beanContextName:null

    };
    return types
});

},
'xfile/manager/FileManager':function(){
/** @module xfile/manager/FileManager */
define([
    'dcl/dcl',
    'dojo/_base/kernel',
    'xide/manager/ServerActionBase',
    'xide/types',
    'xfile/types',
    'xide/utils',
    'xide/encoding/SHA1',
    'xide/manager/RPCService',
    'dojo/Deferred',
    'xdojo/has',
    'xfile/manager/FileManagerActions',
    'require',
    'xfile/factory/Store',
    "xide/lodash",
    'xdojo/has!electron?xfile/manager/Electron'
], function (dcl, dojo, ServerActionBase, types, fTypes, utils, SHA1, RPCService, Deferred, has, FileManagerActions, require, StoreFactory, _, Electron) {
    var bases = [ServerActionBase, FileManagerActions];
    if (has('electronx') && Electron) {
        bases.push(Electron);
    }
    var debug = false;
    /**
     * @class module:xfile.manager.FileManager
     * @extends {module:xide/manager/ServerActionBase}
     * @extends {module:xide/manager/ManagerBase}
     * @augments {module:xide/mixins/EventedMixin}
     */
    return dcl(bases, {
        declaredClass: "xfile.manager.FileManager",
        /**
         * Returns a new name 
         * @param item
         * @param others
         * @returns {*}
         */
        getNewName: function (item, others) {
            var name = item.name.replace('.meta.json', '');
            var found = false;
            var i = 1;
            var newName = null;
            while (!found) {
                newName = name + '-' + i + '.meta.json';
                var colliding = _.find(others, {
                    name: newName
                });

                if (!colliding) {
                    found = true;
                } else {
                    i++;
                }
            }
            return newName;
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Variables
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        _uploadXHR: null,
        store: null,
        config: null,
        filesToUpload: null,
        serviceUrl: "index.php",
        serviceClass: 'XCOM_Directory_Service',
        settingsStore: null,
        stores: [],
        getStore: function (mount, cache) {
            var store = _.find(this.stores, {
                mount: mount
            });
            if (store && cache !== false) {
                return store;
            }
            return StoreFactory.createFileStore(mount, null, this.config, null, this.ctx);
        },
        addStore: function (store) {
            this.stores.push(store);
            store._on('destroy', this.removeStore.bind(this));
        },
        removeStore: function (store) {
            var index = this.stores.indexOf(store);
            if (index) {
                this.stores.remove(store);
            }
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Standard manager interface implementation
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        download: function (src) {
            var selection = [];
            selection.push(src.path);

            if (has('nserver')) {
                window.open('/files/' + src.mount + '/' + src.path + '?userDirectory=' + encodeURIComponent(this.ctx.getUserDirectory()));
                return;
            }

            var thiz = this;
            var downloadUrl = decodeURIComponent(this.serviceUrl);
            downloadUrl = downloadUrl.replace('view=rpc', 'view=smdCall');
            if (downloadUrl.indexOf('?') != -1) {
                downloadUrl += '&';
            } else {
                downloadUrl += '?';
            }
            var serviceClass = this.serviceClass || 'XCOM_Directory_Service';
            var path = utils.buildPath(src.mount, src.path, true);
            path = this.serviceObject.base64_encode(path);
            downloadUrl += 'service=' + serviceClass + '.get&path=' + path + '&callback=asdf';
            if (this.config.DOWNLOAD_URL) {
                downloadUrl = '' + this.config.DOWNLOAD_URL;
                downloadUrl += '&path=' + path + '&callback=asdf';
            }
            downloadUrl += '&raw=html';
            downloadUrl += '&attachment=1';
            var aParams = utils.getUrlArgs(location.href);
            utils.mixin(aParams, {
                "service": serviceClass + ".get",
                "path": path,
                "callback": "asdf",
                "raw": "html",
                "attachment": "1",
                "send": "1"
            });
            delete aParams['theme'];
            delete aParams['debug'];
            delete aParams['width'];
            delete aParams['attachment'];
            delete aParams['send'];
            var pStr = dojo.toJson(JSON.stringify(aParams));
            var signature = SHA1._hmac(pStr, this.config.RPC_PARAMS.rpcSignatureToken, 1);
            downloadUrl += '&' + this.config.RPC_PARAMS.rpcUserField + '=' + this.config.RPC_PARAMS.rpcUserValue;
            downloadUrl += '&' + this.config.RPC_PARAMS.rpcSignatureField + '=' + signature;
            window.open(downloadUrl);
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  File manager only related
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        getImageUrl: function (src, preventCache, extraParams) {
            if (has('nserver')) {
                return ('/files/' + src.mount + '/' + src.path + '?userDirectory=' + encodeURIComponent(this.ctx.getUserDirectory()));
            }
            preventCache = location.href.indexOf('noImageCache') != -1 || preventCache === true || src.dirty === true;
            var downloadUrl = decodeURIComponent(this.serviceUrl);
            downloadUrl = downloadUrl.replace('view=rpc', 'view=smdCall');
            var path = utils.buildPath(src.mount, src.path, true);
            path = this.serviceObject.base64_encode(path);

            var serviceClass = this.ctx.getFileManager().serviceClass || 'XCOM_Directory_Service';
            if (downloadUrl.indexOf('?') != -1) {
                downloadUrl += '&';
            } else {
                downloadUrl += '?';
            }
            downloadUrl += 'service=' + serviceClass + '.get&path=' + path + '&callback=asdf';
            if (this.config.DOWNLOAD_URL) {
                downloadUrl = '' + this.config.DOWNLOAD_URL;
                downloadUrl += '&path=' + path + '&callback=asdf';
            }
            downloadUrl += '&raw=html';
            downloadUrl += '&attachment=0';
            downloadUrl += '&send=1';
            var aParams = utils.getUrlArgs(location.href);
            utils.mixin(aParams, {
                "service": serviceClass + ".get",
                "path": path,
                "callback": "asdf",
                "raw": "html"
            });
            utils.mixin(aParams, extraParams);
            delete aParams['theme'];
            delete aParams['debug'];
            delete aParams['width'];
            var pStr = dojo.toJson(aParams);
            var signature = SHA1._hmac(pStr, this.config.RPC_PARAMS.rpcSignatureToken, 1);
            downloadUrl += '&' + this.config.RPC_PARAMS.rpcUserField + '=' + this.config.RPC_PARAMS.rpcUserValue;
            downloadUrl += '&' + this.config.RPC_PARAMS.rpcSignatureField + '=' + signature;
            if (preventCache) {
                downloadUrl += '&time=' + new Date().getTime();
            }
            if (extraParams) {
                for (var p in extraParams) {
                    downloadUrl += '&' + p + '=' + extraParams[p];
                }
            }
            return downloadUrl;
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Upload related
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        onFileUploadFailed: function (item) {
            var thiz = this,
                eventKeys = types.EVENTS;
            if (item.dfd) {
                item.dfd.reject(item);
            }
            thiz.filesToUpload.remove(item);
            thiz.publish(eventKeys.ON_UPLOAD_FAILED, {
                item: item
            }, thiz);
        },
        onFileUploaded: function (item) {
            var thiz = this,
                eventKeys = types.EVENTS;

            setTimeout(function () {
                var struct1 = {
                    message: '' + item.file.name + ' uploaded to ' + item.dstDir,
                    messageArgs: {}
                };
                thiz.publish(eventKeys.STATUS, struct1, thiz);
                if (item.dfd) {
                    item.dfd.resolve(item);
                }
                thiz.filesToUpload.remove(item);
                thiz.publish(eventKeys.ON_UPLOAD_FINISH, {
                    item: item
                });
            }, 500);
        },
        getUploadUrl: function () {
            if (has('nserver')) {
                return this.serviceUrl.replace('/smd', '/upload/?');
            }
            var url = '' + decodeURIComponent(this.serviceUrl);

            url = url.replace('view=rpc', 'view=upload');
            url = url.replace('../../../../', './');
            url += '&service=';
            url += this.serviceClass;
            url += '.put&callback=nada';
            return url;
        },
        initXHRUpload: function (item, autoRename, dstDir, mount) {
            var xhr = new XMLHttpRequest();
            var uploadUrl = this.getUploadUrl();
            var uri = '' + uploadUrl;
            //uri += '' + encodeURIComponent(mount);
            //uri += '/' + encodeURIComponent(dstDir);
            uri += 'mount=' + encodeURIComponent(mount);
            uri += '&dstDir=' + encodeURIComponent(dstDir);
            var thiz = this;
            var upload = xhr.upload;
            upload.addEventListener("progress", function (e) {
                if (!e.lengthComputable) {
                    thiz.onFileUploaded(item);
                } else {
                    var struct = {
                        item: item,
                        progress: e
                    };
                    item.isLoading = true;
                    item.dfd.progress(struct);
                }
            }.bind(this), false);

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.responseText && xhr.responseText != 'OK') {
                        var error = utils.getJson(xhr.responseText);
                        if (!error && xhr.responseText.indexOf('Fata Error')) {
                            error = {
                                result: [xhr.responseText],
                                code: 1
                            };
                        }
                        if (error && error.result && _.isArray(error.result) && error.result.length > 0) {
                            var _message = null;
                            for (var i = 0; i < error.result.length; i++) {
                                thiz.publish(types.EVENTS.ERROR, 'Error uploading : ' + item.name + ' ' + error.result[i], thiz);
                                _message = error.result[i];
                            }
                            if (_message) {
                                item.error = _message;
                            }
                            thiz.onFileUploadFailed(item);
                            thiz.submitNext();
                            return;
                        }
                        if (error && error.error) {
                            thiz.publish(types.EVENTS.ERROR, 'Error uploading : ' + item.name + ' ' + error.error.message, thiz);
                            thiz.onFileUploadFailed(item);
                        }
                    }
                    thiz.onFileUploaded(item);
                    thiz.submitNext();
                }
            }.bind(this);
            upload.onerror = function () {
                thiz.publish(types.EVENTS.ERROR, 'Error uploading : ' + item.name, thiz);
            };
            xhr.open("POST", uri, true);
            return xhr;
        },
        hasLoadingItem: function () {
            for (var i = 0; i < this.filesToUpload.length; i++) {
                if (this.filesToUpload[i].status == 'loading') {
                    return this.filesToUpload[i];
                }
            }
            return false;
        },
        /**
         *
         * @param files
         * @param mount
         * @param path
         * @param callee
         * @param view
         * @returns {Deferred[]}
         */
        upload: function (files, mount, path, callee, view) {
            var dfds = [];
            for (var i = 0; i < files.length; i++) {
                var uploadStruct = {
                    file: files[i],
                    dstDir: '' + path,
                    mount: '' + mount,
                    callee: callee,
                    view: callee,
                    dfd: new Deferred()
                };
                dfds.push(uploadStruct['dfd']);
                this.filesToUpload.push(uploadStruct);
            }
            this.submitNext();
            return dfds;
        },
        sendFileUsingFormData: function (xhr, file) {
            var formData = new FormData();
            formData.append("userfile_0", file.file);
            xhr.send(formData);
        },
        sendFileMultipart: function (item) {
            var auto_rename = false;
            item.status = 'loading';
            var xhr = this.initXHRUpload(item, (auto_rename ? "auto_rename=true" : ""), item['dstDir'], item['mount']);
            this.publish(types.EVENTS.ON_UPLOAD_BEGIN, {
                item: item,
                name: item.name
            }, this);
            if (window.FormData) {
                this.sendFileUsingFormData(xhr, item);
            }
        },
        submitNext: function () {
            var item = this.getNextUploadItem();
            if (item) {
                this.sendFileMultipart(item);
            }
        },
        getNextUploadItem: function () {
            for (var i = 0; i < this.filesToUpload.length; i++) {
                if (!this.filesToUpload[i].status) {
                    return this.filesToUpload[i];
                }
            }
            return false;
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Error handling
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        onError: function (err) {
            if (err) {
                if (err.code === 1) {
                    if (err.message && _.isArray(err.message)) {
                        this.publish(types.EVENTS.ERROR, err.message.join('<br/>'), this);
                        return;
                    }
                } else if (err.code === 0) {
                    this.publish(types.EVENTS.STATUS, 'Ok', this);
                }
            }
            this.publish(types.EVENTS.ERROR, {
                error: err
            }, this);
        },
        addError: function (def) {
            var thiz = this;
            var _cb = function () {
                thiz.onError();
            };
            def.addCallback(_cb);
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  pre RPC roundup
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        downloadItem: function (src, readyCB) {
            return this.callMethod(types.OPERATION.DOWNLOAD, [src], readyCB, true);
        },
        downloadTo: function (url, mount, dst, readyCB, dstItem) {
            if (dstItem) {
                var thiz = this;
                var _cb = function (result) {
                    var _failed = false;
                    if (result && result.error && result.error.code == 1) {
                        _failed = true;
                    }
                    thiz.publish(types.EVENTS.ON_DOWNLOAD_TO_END, {
                        terminatorItem: dstItem,
                        failed: _failed
                    }, this);

                    readyCB(arguments);
                };
                thiz.publish(types.EVENTS.ON_DOWNLOAD_TO_BEGIN, {
                    dst: dstItem,
                    url: url,
                    items: [dstItem]
                }, this);
            } else {
                 0 && console.log('download from remote url have no dest item');
            }
            return this.callMethod(types.OPERATION.DOWNLOAD_TO, [url, mount, dst], _cb, true);
        },
        find: function (mount, conf, readyCB) {
            try {
                return this.callMethod(types.OPERATION.FIND, [mount, conf], readyCB, true);
            } catch (e) {
                logError(e, 'find');
            }
        },
        getContentSync: function (mount, path, readyCB, emit) {
            /*
            var self = this;

            function resolveAfter2Seconds(x) {
                return new Promise(resolve => {
                    self.getContent(mount, path, function (content) {
                        resolve(content);
                    })
                });
            }

            async function f1() {
                var x = await resolveAfter2Seconds(10);
                 0 && console.log('got ', x); // 10
                return x;
            }
            const content = f1();
            return content;
            */
        },
        getContent: function (mount, path, readyCB, emit) {
            if (this.getContentE) {
                var res = this.getContentE.apply(this, arguments);
                if (res) {
                    return res;
                }
            }
            if (has('php')) {
                var _path = utils.buildPath(mount, path, false);
                return this.callMethod(types.OPERATION.GET_CONTENT, [_path, false, false], readyCB, false);
            } else {
                return this._getText(require.toUrl(mount).replace('main.js', '') + '/' + path, {
                    sync: false,
                    handleAs: 'text'
                }).then(function (res) {
                    try {
                        if (readyCB) {
                            readyCB(res);
                        }
                    } catch (e) {
                        logError(e, 'error running RPC');
                    }
                });
            }
        },
        setContent: function (mount, path, content, readyCB) {
            this.publish(types.EVENTS.ON_CHANGED_CONTENT, {
                'mount': mount,
                'path': path,
                'content': content
            });
            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                text: "Did save file : " + mount + '://' + path
            });
            if (this.setContentE) {
                var res = this.setContentE.apply(this, arguments);
                if (res) {
                    return res;
                }
            }
            return this.callMethod(types.OPERATION.SET_CONTENT, [mount, path, content], readyCB, true);
        },
        onMessages: function (res) {
            var events = utils.getJson(res.events);
            if (events && _.isArray(events)) {
                for (var i = 0; i < events.length; i++) {
                    var struct = {
                        path: events[i].relPath
                    };
                    utils.mixin(struct, events[i]);
                    this.publish(events[i].clientEvent, struct, this);
                }
            }
        },
        onErrors: function (res) {},
        init: function () {
            this.stores = [];
            this.filesToUpload = [];
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  RPC helpers
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        callMethodEx: function (serverClassIn, method, args, readyCB, omitError) {
            /***
             * Check we the RPC method is in the SMD
             */
            var serviceClass = serverClassIn || this.serviceClass;
            var thiz = this;
            if (!this.serviceObject[serviceClass][method]) {
                if (omitError === true) {
                    this.onError({
                        code: 1,
                        message: ['Sorry, server doesnt know ' + method]
                    });
                }
                return null;
            }
            /***
             * Build signature
             */
            var params = {};
            params = utils.mixin(params, this.config.RPC_PARAMS.rpcFixedParams);
            /**
             * Mixin mandatory fields
             */
            params[this.config.RPC_PARAMS.rpcUserField] = this.config.RPC_PARAMS.rpcUserValue;
            this.serviceObject.extraArgs = params;
            this.serviceObject.signatureField = this.config.RPC_PARAMS.rpcSignatureField;
            this.serviceObject.signatureToken = this.config.RPC_PARAMS.rpcSignatureToken;
            this.serviceObject[serviceClass][method](args).then(function (res) {
                try {
                    if (readyCB) {
                        readyCB(res);
                    }
                } catch (e) {
                    console.error('bad news : callback for method ' + method + ' caused a crash in service class ' + serviceClass);
                }
                if (res && res.error && res.error.code == 3) {
                    setTimeout(function () {
                        thiz.onMessages(res.error);
                    }, 50);
                }

                if (res && res.error && res.error && res.error.code !== 0) {
                    thiz.onError(res.error);
                    return;
                }

                thiz.publish(types.EVENTS.STATUS, {
                    message: 'Ok!'
                }, this);

            }, function (err) {
                thiz.onError(err);
            });


        },
        callMethod: function (method, args, readyCB, omitError) {
            var thiz = this;
            /***
             * Check we the RPC method is in the SMD
             */
            var serviceClass = this.serviceClass;
            try {
                if (!this.serviceObject[serviceClass][method]) {
                    if (omitError === true) {
                        this.onError({
                            code: 1,
                            message: ['Sorry, server doesnt know ' + method]
                        });
                    }
                    return null;
                }
                /***
                 * Build signature
                 */
                var params = {};
                params = utils.mixin(params, this.config.RPC_PARAMS.rpcFixedParams);
                /**
                 * Mixin mandatory fields
                 */
                params[this.config.RPC_PARAMS.rpcUserField] = this.config.RPC_PARAMS.rpcUserValue;
                this.serviceObject.extraArgs = params;
                this.serviceObject.signatureField = this.config.RPC_PARAMS.rpcSignatureField;
                this.serviceObject.signatureToken = this.config.RPC_PARAMS.rpcSignatureToken;
                var dfd = this.serviceObject[this.serviceClass][method](args);
                dfd.then(function (res) {
                    try {
                        if (readyCB) {
                            readyCB(res);
                        }
                    } catch (e) {
                        console.error('crashed in ' + method);
                        logError(e, 'error running RPC');

                    }
                    //@TODO: batch, still needed?
                    if (res && res.error && res.error.code == 3) {
                        setTimeout(function () {
                            thiz.onMessages(res.error);
                        }, 50);
                    }

                    if (res && res.error && res.error && res.error.code == 1) {
                        thiz.onError(res.error);
                        return;
                    }
                    if (omitError !== false) {
                        var struct = {
                            message: 'Ok!'
                        };
                        thiz.publish(types.EVENTS.STATUS, struct, this);
                    }
                }, function (err) {
                    thiz.onError(err);
                });
                return dfd;
            } catch (e) {
                console.error('crash calling method' + e, arguments);
                thiz.onError(e);
                logError(e, 'error ');
            }
        },
        __initService: function () {
            this.filesToUpload = [];
            if (!this.serviceObject) {
                if (this.serviceUrl) {
                    this.serviceObject = new RPCService(decodeURIComponent(this.serviceUrl));
                    this.serviceObject.config = this.config;
                }
            }
        }
    });
});
},
'xfile/manager/FileManagerActions':function(){
define([
    'dcl/dcl',
    'xide/types',
    'xide/utils'
], function (dcl, types, utils) {
    /**
     * @class xfile.manager.FileManager
     * @augments module:xfile.manager.FileManager
     */
    return dcl(null, {
        declaredClass: "xfile/manager/FileManagerActions",
        /**
         * Publish a file's operations progress event
         * @param event
         * @param terminator
         * @param items
         * @param failed
         * @param extra
         * @private
         */
        _publishProgress: function (event, terminator, items, failed, extra) {
            var _args = {
                terminatorItem: terminator,
                failed: failed,
                items: items || terminator
            };
            utils.mixin(_args, extra);
            this.publish(event, _args, this);
        },
        /**
         *
         * @param operation
         * @param args
         * @param terminator
         * @param items
         * @returns {*}
         */
        doOperation: function (operation, args, terminator, items, extra, dfdOptions) {
            var thiz = this,
                operationCapitalized = operation.substring(0, 1).toUpperCase() + operation.substring(1),
                beginEvent = 'on' + operationCapitalized + 'Begin', //will evaluate for operation 'delete' to 'onDeleteBegin'
                endEvent = 'on' + operationCapitalized + 'End';

            thiz._publishProgress(beginEvent, terminator, items, false, extra);

            var rpcPromise = this.runDeferred(null, operation, args, dfdOptions).then(function () {
                thiz._publishProgress(endEvent, terminator, items, false, extra);
            }, function (err) {
                thiz._publishProgress(endEvent, terminator, items, true, extra);
            });
            return rpcPromise;
        },
        deleteItems: function (selection, options, dfdOptions) {
            return this.doOperation(types.OPERATION.DELETE, [selection, options, true], selection, selection, null, dfdOptions);
        },
        copyItem: function (selection, dst, options, dfdOptions) {
            return this.doOperation(types.OPERATION.COPY, [selection, dst, options, false], selection, selection, {dst: dst}, dfdOptions);
        },
        mkdir: function (mount, path, dfdOptions) {
            return this.doOperation(types.OPERATION.NEW_DIRECTORY, [mount, path], path, null, null, dfdOptions);
        },
        mkfile: function (mount, path, content) {
            return this.doOperation(types.OPERATION.NEW_FILE, [mount, path, content || ''], path);
        },
        rename: function (mount, src, dst) {
            return this.doOperation(types.OPERATION.RENAME, [mount, src, dst], src);
        },
        moveItem: function (src, dst, include, exclude, mode, dfdOptions) {
            return this.doOperation(types.OPERATION.MOVE, [src, dst, include, exclude, mode], src, null, null, dfdOptions);
        },
        compressItem: function (mount, src, type, readyCB) {
            return this.doOperation(types.OPERATION.COMPRESS, [mount, src, type], src);
        },
        extractItem: function (mount, src, type) {
            return this.doOperation(types.OPERATION.EXTRACT, [mount, src], src);
        }
    });
});
},
'xfile/manager/MountManager':function(){
define([
    'dcl/dcl',
    "dojo/_base/lang",
    "xdojo/has",
    "xide/manager/ResourceManager",
    "xide/mixins/ReloadMixin",
    "xide/mixins/EventedMixin",
    "xide/types",
    'xide/utils',
    'dojo/Deferred'
], function (dcl, lang, has, ResourceManager, ReloadMixin, EventedMixin, types, utils, Deferred) {
    return dcl([ResourceManager, EventedMixin.dcl, ReloadMixin.dcl], {
        declaredClass: "xfile.manager.MountManager",
        serviceClass: "XApp_Resource_Service",
        mountData: null,
        didReload: false,
        editMount: function (mount) {
            /*
            if (!mount) {
                return;
            }
            if (mount.type === 'FILE_PROXY') {
                this.registerLocalMount(mount);
            } else if (mount.type === 'REMOTE_FILE_PROXY') {

                if (has('remote-vfs')) {
                    if (mount.adapter === 'Ftp') {
                        this.registerFTP(mount);
                    }
                    if (mount.adapter === 'Sftp') {
                        this.registerSFTP(mount);
                    }
                    if (mount.adapter === 'Dropbox') {
                        this.registerDropbox(mount);
                    }
                    if (mount.adapter === 'Webdav') {
                        this.registerWebDav(mount);
                    }
                }
            }
            */
        },
        removeMount: function (mount) {
            /*
            if (!mount) {
                return;
            }
            var thiz = this;
            var onOk = function () {
                var _cb = function () {
                    thiz.ls(function (data) {
                        thiz.onMountDataReady(data)
                    });
                };
                thiz.removeResource(mount, false, _cb);

            };

            var dlg = new FileDeleteDialog({
                title: 'Remove ' + mount.name,
                config: this.config,
                delegate: {
                    onOk: function () {
                        onOk();
                    }
                },
                ctx: this.ctx,
                titleBarClass: 'ui-state-error',
                inserts: [{
                    query: '.dijitDialogPaneContent',
                    insert: '<div><span class="fileManagerDialogText">Do you really want to remove this item' + '?</span></div>',
                    place: 'first'
                }]

            });
            domClass.add(dlg.domNode, 'fileOperationDialog');
            dlg.show();
            dlg.addActionButtons();
            dlg.fixHeight();
            dlg.resize();
            setTimeout(function () {
                dlg.resize();
            }, 1000);
            */
        },
        getMounts: function () {
            return this.mountData;
        },
        _onDialogOk: function (dlg, data, mount) {
            var options = utils.toOptions(data);
            var thiz = this;

            var isUpdate = mount.name != null;

            //build resource object
            var resourceObject = {
                "class": "cmx.types.Resource",
                "enabled": true
            };

            //merge options to resource config
            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                if (option.user == null) {
                    resourceObject[option.name] = option.value;//std resource field
                } else {

                    //we put adapter specific fields into the resource's 'config' field
                    if (!resourceObject.config) {
                        resourceObject.config = {};
                    }
                    resourceObject.config[option.name] = option.value;
                }
            }

            //complete resource config
            var label = '';
            if (resourceObject.label) {
                label = resourceObject.label;
            } else if (resourceObject.config.label) {
                label = resourceObject.config.label;
            }

            resourceObject.name = '' + (mount.name != null ? mount.name : label.toLowerCase());

            if (resourceObject.config.type === 'FILE_PROXY') {
                resourceObject.path = '' + resourceObject.path + '';//VFS Local adjustment
                lang.mixin(resourceObject, resourceObject.config);
            } else if (resourceObject.config.type === 'REMOTE_FILE_PROXY') {
                resourceObject.path = resourceObject.name + '://';//VFS Remote adjustment
            }
            var _cb = function () {
                thiz.ls(function (data) {
                    thiz.onMountDataReady(data)
                });
            };
            if (!isUpdate) {
                this.createResource(resourceObject, false, _cb);
            } else {
                this.updateResource(resourceObject, false, _cb);
            }

        },
        registerLocalMount: function (mount) {
            /*
                        var name = mount ? mount.name : '';
                        var path = mount ? mount.path : '';
                        if (mount && mount.config && mount.config.path) {
                            path = mount.config.path;
                        }
                        var thiz = this;
                        var actionDialog = new CIActionDialog({
                            title: name ? 'Edit Mount ' + name : 'New Local Mount',
                            style: 'max-width:400px;min-height:300px',
                            delegate: {
                                onOk: function (dlg, data) {
                                    thiz._onDialogOk(dlg, data, mount);
                                }
                            },
                            cis: [
                                utils.createCI('label', 13, '', {
                                    group: 'Common',
                                    title: 'Name',
                                    value: name
                                }),
                                utils.createCI('path', 13, '', {
                                    group: 'Common',
                                    title: 'Path',
                                    value: path
                                }),
                                utils.createCI('type', 13, '', {
                                    visible: false,
                                    value: "FILE_PROXY"
                                })
                            ]
                        });
                        actionDialog.show();
                        */
        },
        registerFTP: function (mount) {
            /*
                        mount = mount || {};
                        var config = mount.config || {};
            
            
                        var thiz = this;
                        var actionDialog = new CIActionDialog({
                            title: mount.name ? 'Edit Ftp ' + mount.name : 'New Ftp',
                            style: 'max-width:400px',
                            delegate: {
                                onOk: function (dlg, data) {
                                    thiz._onDialogOk(dlg, data, mount);
                                }
                            },
                            cis: [
                                xide.utils.createCI('label', 13, '', {
                                    group: 'Common',
                                    title: 'Name',
                                    value: mount.name
            
                                }),
                                xide.utils.createCI('root', 13, '', {
                                    group: 'Ftp',
                                    title: 'Start Path',
                                    value: config.root,
                                    user: {
                                        config: true
                                    }
            
                                }),
                                xide.utils.createCI('adapter', 13, '', {
                                    visible: false,
                                    value: 'Ftp'
                                }),
                                xide.utils.createCI('host', 13, '', {
                                    group: 'Ftp',
                                    title: 'Host',
                                    user: {
                                        config: true
                                    },
                                    value: config.host
                                }),
                                xide.utils.createCI('username', 13, '', {
                                    group: 'Ftp',
                                    title: 'User',
                                    user: {
                                        config: true
                                    },
                                    value: config.username
                                }),
                                xide.utils.createCI('password', 13, '', {
                                    group: 'Ftp',
                                    title: 'Password',
                                    user: {
                                        config: true
                                    },
                                    value: config.password
                                }),
                                xide.utils.createCI('passive', 0, '', {
                                    group: 'Ftp',
                                    title: 'Passive',
                                    user: {
                                        config: true
                                    },
                                    value: config.passive
                                }),
                                xide.utils.createCI('ssl', 0, '', {
                                    group: 'Ftp',
                                    title: 'SSL',
                                    user: {
                                        config: true
                                    },
                                    value: config.ssl != null ? config.ssl : false
                                }),
                                xide.utils.createCI('port', 13, '', {
                                    group: 'Ftp',
                                    title: 'Port',
                                    user: {
                                        config: true
                                    },
                                    value: config.port != null ? config.port : 21
                                }),
                                xide.utils.createCI('type', 13, '', {
                                    visible: false,
                                    value: "REMOTE_FILE_PROXY"
                                })
            
                            ]
                        });
                        actionDialog.show();
                        */
        },
        registerSFTP: function (mount) {
            /*
            mount = mount || {};
            var config = mount.config || {};


            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: mount.name ? 'Edit Ftp ' + mount.name : 'New Ftp',
                style: 'max-width:400px',
                delegate: {
                    onOk: function (dlg, data) {
                        thiz._onDialogOk(dlg, data, mount);
                    }
                },
                cis: [
                    xide.utils.createCI('label', 13, '', {
                        group: 'Common',
                        title: 'Name',
                        value: mount.name

                    }),
                    xide.utils.createCI('root', 13, '', {
                        group: 'SFtp',
                        title: 'Start Path',
                        value: config.root,
                        user: {
                            config: true
                        }

                    }),
                    xide.utils.createCI('adapter', 13, '', {
                        visible: false,
                        value: 'Sftp'
                    }),
                    xide.utils.createCI('host', 13, '', {
                        group: 'SFtp',
                        title: 'Host',
                        user: {
                            config: true
                        },
                        value: config.host
                    }),
                    xide.utils.createCI('username', 13, '', {
                        group: 'SFtp',
                        title: 'User',
                        user: {
                            config: true
                        },
                        value: config.username
                    }),
                    xide.utils.createCI('password', 13, '', {
                        group: 'SFtp',
                        title: 'Password',
                        user: {
                            config: true
                        },
                        value: config.password
                    }),
                    xide.utils.createCI('port', 13, '', {
                        group: 'SFtp',
                        title: 'Port',
                        user: {
                            config: true
                        },
                        value: config.port != null ? config.port : 22
                    }),
                    xide.utils.createCI('type', 13, '', {
                        visible: false,
                        value: "REMOTE_FILE_PROXY"
                    })

                ]
            });
            actionDialog.show();
            */
        },
        registerDropbox: function (mount) {
            /*
            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: 'New Dropbox',
                style: 'max-width:400px',
                delegate: {
                    onOk: function (dlg, data) {
                        thiz._onDialogOk(dlg, data, mount);
                    }
                },
                cis: [
                    xide.utils.createCI('label', 13, '', {
                        group: 'Common',
                        title: 'Name',
                        value: 'Dropbox1'

                    }),
                    xide.utils.createCI('pathPrefix', 13, '', {
                        group: 'Common',
                        title: 'Start Path',
                        user: {
                            config: true
                        }

                    }),
                    xide.utils.createCI('adapter', 13, '', {
                        visible: false,
                        value: 'Dropbox'
                    }),
                    xide.utils.createCI('token', 13, '', {
                        group: 'Dropbox',
                        title: 'Token',
                        user: {
                            config: true
                        },
                        value: 'h16UVItP7qQAAAAAAAAABP3qmBJFOHj3fA5ffKyaHH-j7HCLvFOceZxhENV0sy24'
                    }),
                    xide.utils.createCI('appname', 13, '', {
                        group: 'Dropbox',
                        title: 'App Name',
                        user: {
                            config: true
                        },
                        value: 'xapp_local'
                    }),
                    xide.utils.createCI('type', 13, '', {
                        visible: false,
                        value: "REMOTE_FILE_PROXY"
                    })

                ]
            });
            actionDialog.show();
            */
        },
        registerWebDav: function () {
            /*
            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: 'New Webdav',
                style: 'max-width:400px',
                delegate: {
                    onOk: function (dlg, data) {
                        thiz._onDialogOk(dlg, data);
                    }
                },
                cis: [
                    xide.utils.createCI('label', 13, '', {
                        group: 'Common',
                        title: 'name'
                    }),
                    xide.utils.createCI('pathPrefix', 13, '', {
                        group: 'Common',
                        title: 'Start Path'
                    }),
                    xide.utils.createCI('adapter', 13, '', {
                        visible: false,
                        value: 'WebDav'
                    }),
                    xide.utils.createCI('host', 13, '', {
                        group: 'WebDav',
                        title: 'Host'
                    }),
                    xide.utils.createCI('baseUri', 13, '', {
                        group: 'WebDav',
                        title: 'Base URI'
                    }),
                    xide.utils.createCI('userName', 13, '', {
                        group: 'WebDav',
                        title: 'User Name'
                    }),
                    xide.utils.createCI('password', 13, '', {
                        group: 'WebDav',
                        title: 'Password'
                    }),
                    xide.utils.createCI('type', 13, '', {
                        visible: false,
                        value: "REMOTE_FILE_PROXY"
                    })
                ]
            });
            actionDialog.show();
            */
        },
        onMountDataReady: function (data) {
            this.mountData = data;
            this.publish(types.EVENTS.ON_MOUNT_DATA_READY, { data: data });
            var thiz = this;
            setTimeout(function () {
                thiz.publish(types.EVENTS.ON_MOUNT_DATA_READY, { data: data });
            }, 4000);
        },
        check: function () {
            if (!this.serviceObject)
                this._initService();
        },
        /**
         * Callback when context initializes us
         */
        init: function () {
            if (this.ctx.getFileManager()) {
                this.serviceObject = this.ctx.getFileManager().serviceObject;
            }
        },
        /////////////////////////////////////////////////////////////////////////////////
        //
        //  Server Methods
        //
        //////////////////////////////////////////////////////////////////////////////////
        createResource: function (resource, test, readyCB) {
            return this.callMethodEx(null, 'createResource', [resource, test], readyCB, true);
        },
        removeResource: function (resource, test, readyCB) {
            return this.callMethodEx(null, 'removeResource', [resource, true], readyCB, true);
        },
        updateResource: function (resource, test, readyCB) {
            return this.callMethodEx(null, 'updateResource', [resource, true], readyCB, true);
        },

        ls: function (readyCB) {
            function data(_data) {
                if (!has('debug')) {
                    _data = _data[0];
                }
                this.mountData = _data;
                this.onMountDataReady(_data);
                if (readyCB) {
                    readyCB(_data);
                }
            }
            if (!_.isEmpty(this.prefetch)) {
                var dfd = new Deferred();
                dfd.resolve(this.prefetch);
                data.apply(this, [this.prefetch]);
                delete this.prefetch;
                return dfd;
            }

            return this.runDeferred(null, 'ls', []).then(data.bind(this));
        }
    });
});
},
'xfile/manager/BlockManager':function(){
define([
    'dcl/dcl',
    'xide/manager/ManagerBase',
    'xide/mixins/ReloadMixin',
    'xblox/manager/BlockManager'
], function (dcl,ManagerBase, ReloadMixin, BlockManager) {
    return dcl([ManagerBase, BlockManager, ReloadMixin.dcl], {
        declaredClass:"xfile/manager/BlockManager",
        currentItem: null,
        /**
         * One time call per blox scope creation. This adds various functions
         * to the blox's owner object. This enables expressions to access the
         * object but also block specific functions like getVariable
         * @param obj
         * @param scope
         * @param owner
         */
        setScriptFunctions: function (obj, scope, owner) {
            this.inherited(arguments);
        }
    });
});
},
'xfile/views/FileConsole':function(){
/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "xide/mixins/PersistenceMixin",
    'xide/layout/_TabContainer',
    'xide/views/_Console',
    'xide/views/_ConsoleWidget',
    "xfile/views/FileSize",
    'xide/views/ConsoleView',
    'xide/lodash',
    'dojo/has!xace?xace/views/Editor'
], function (declare, dcl, types,
             utils, PersistenceMixin, _TabContainer, Console, ConsoleWidget, FileSize, ConsoleView, _, Editor) {

    function createConsoleAceWidgetClass() {
        return dcl([ConsoleWidget, PersistenceMixin.dcl], {
            createEditor: function (ctx) {
                return createEditor(this.consoleParent, this.value, this, {
                    options: this.options,
                    ctx: ctx
                });
            }
        });
    }

    function createEditor(root, value, owner, mixin) {
        var item = {
            filePath: '',
            fileName: ''
        };
        var title = "No Title";
        var args = {
            _permissions: [],
            item: item,
            value: value,
            style: 'padding:0px;top:0 !important',
            iconClass: 'fa-code',
            options: utils.mixin(mixin, {
                filePath: item.path,
                fileName: item.name
            }),
            /***
             * Provide a text editor store delegate
             */
            storeDelegate: {},
            title: title
        };
        utils.mixin(args, mixin);
        var editor = utils.addWidget(Editor, args, owner, root, true, null, null, false);
        editor.resize();
        return editor;
    }

    function createShellViewDelegate() {
        return dcl(null, {
            owner: null,
            onServerResponse: function (theConsole, data, addTimes) {

                if (theConsole && data && theConsole.owner && theConsole.owner.onServerResponse) {
                    return theConsole.owner.onServerResponse(data, addTimes);
                }
            },
            runBash: function (theConsole, value, cwd, commandNode) {
                var thiz = this,
                    ctx = thiz.ctx;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['sh', _value, cwd]).then(function (response) {
                    if (commandNode) {
                        $(commandNode).find('.consoleRunningCommand').remove();
                    }
                    thiz.onServerResponse(theConsole, response, false);
                });
            },

            runPHP: function (theConsole, value, cwd) {
                var thiz = this,
                    ctx = thiz.ctx;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['php', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole, response, false);
                });
            },
            runJavascript: function (theConsole, value, context, args) {
                var _function = new Function("{" + value + "; }");
                var response = _function.call(context, args);
                if (response != null) {
                    this.onServerResponse(theConsole, response);
                    return response;
                }
                return value;
            },
            onConsoleCommand: function (data, value) {
                var thiz = this,
                    theConsole = data.console;
                if (theConsole.type === 'sh') {
                    var commandNode = thiz.onServerResponse(theConsole, "<span class='consoleRunningCommand fa-spinner fa-spin'></span><pre style='font-weight: bold'># " + value + "</pre>", true);
                    var dstPath = null;
                    if (this.owner && this.owner.getCurrentFolder) {
                        var cwd = this.owner.getCurrentFolder();
                        if (cwd) {
                            dstPath = utils.buildPath(cwd.mount, cwd.path, false);
                        }
                    }
                    return this.runBash(theConsole, value, dstPath, commandNode);
                }
                if (theConsole.type === 'php') {
                    var dstPath = null;
                    if (theConsole.isLinked()) {
                        dstPath = this.getCurrentPath();
                    }
                    return this.runPHP(theConsole, value, dstPath);
                }

                if (theConsole.type === 'javascript') {
                    return this.runJavascript(theConsole, value);
                }
            },
            onConsoleEnter: function (data, input) {
                return this.onConsoleCommand(data, input);
            }
        });
    }

    function createShellViewClass() {
        return dcl(Console, {
            lazy: true,
            consoleClass: createConsoleAceWidgetClass(),
            getServer: function () {
                return this.server || this.ctx.fileManager;
            },
            log: function (msg, addTimes) {
                utils.destroy(this.progressItem);
                var out = '',
                    isHTML = false;
                if (_.isString(msg)) {

                    if (msg.indexOf('<body') != -1 || /<[a-z][\s\S]*>/i.test(msg)) {
                        isHTML = true;
                        out = msg;
                    } else {
                        out += msg.replace(/\n/g, '<br/>');
                    }

                } else if (_.isObject(msg) || _.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var lastMessageNode = null;
                var thiz = this;
                if (isHTML) {
                    lastMessageNode = dojo.create("div", {
                        className: 'html_response',
                        innerHTML: out
                    });
                    dst.appendChild(lastMessageNode);
                    last = dst.appendChild(dojo.create("div", {
                        innerHTML: '&nbsp;',
                        style: 'height:1px;font-size:1px'
                    }));
                } else {

                    for (var i = 0; i < items.length; i++) {
                        var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');
                        var item = items[i];
                        if (!item || !item.length) {
                            continue;
                        }
                        last = dst.appendChild(dojo.create("div", {
                            className: _class,
                            innerHTML: this._toString(items[i], addTimes)

                        }));
                        this.lastIndex++;
                    }
                }
                if (last) {
                    last.scrollIntoViewIfNeeded();
                }
                return lastMessageNode;
            }
        });
    }

    function createShellViewClass2() {
        var EditorClass = dcl(ConsoleView.Editor, {
            didAddMCompleter: false,
            multiFileCompleter: null,
            blockScope: null,
            driverInstance: null,
            onACEReady: function (editor) {
            },
            onEditorCreated: function (editor, options) {
                this.inherited(arguments);
                this.onACEReady(editor);
            }
        });
        return dcl(ConsoleView, {
            EditorClass: EditorClass,
            onAddEditorActions: dcl.superCall(function (sup) {
                return function (evt) {
                    //grab the result from the handler

                    var res = sup.call(this, evt);


                    var thiz = this;

                    var actions = evt.actions,
                        owner = evt.owner;

                    var save = _.find(actions, {
                        command: 'File/Save'
                    });

                    actions.remove(_.find(actions, {
                        command: 'File/Save'
                    }));

                    actions.remove(_.find(actions, {
                        command: "File/Reload"
                    }));


                    var mixin = {
                        addPermission: true
                    }


                };
            }),
            logTemplate: '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>',
            _parse: function (scope, expression, errorCB) {
                var str = '' + expression;
                if (str.indexOf('{{') > 0 || str.indexOf('}}') > 0) {
                    str = _parser.parse(types.EXPRESSION_PARSER.FILTREX,
                        str, this,
                        {
                            variables: scope.getVariablesAsObject(),
                            delimiters: {
                                begin: '{{',
                                end: '}}'
                            }
                        }
                    );
                     0 && console.timeEnd('parse expression');
                } else {
                    var _text = scope.parseExpression(expression, null, null, null, errorCB);
                    if (_text) {
                        str = _text;
                    }
                }
                return str;
            },
            parse: function (str, errorCB) {
                var driverInstance = this.driverInstance;
                if (driverInstance && driverInstance.blockScope) {
                    return this._parse(driverInstance.blockScope, str, errorCB);
                }
                return str;
            }
        });
    }

    var Module = declare("xfile.views.FileConsole", null, {
        isStatusbarOpen: false,
        resizeToParent: true,
        __bottomTabContainer: null,
        onCloseStatusPanel: function (e) {
        },
        onOpenStatusPanel: function (panel) {
            if (!panel._tabs) {
                var thiz = this;
                var tabContainer = this.__bottomTabContainer || utils.addWidget(_TabContainer, {
                        direction: 'below'
                    }, null, panel, true);

                panel._tabs = tabContainer;
                this.__bottomTabContainer = tabContainer;
                var consoleViewClass = createShellViewClass2(),
                    handlerClass = createShellViewDelegate(),
                    delegate = new handlerClass();

                delegate.ctx = thiz.ctx;

                delegate.owner = this;
                var tab = tabContainer.createTab('Bash', 'fa-terminal'),
                    bashShell = tab.add(consoleViewClass, {
                        type: 'sh',
                        title: 'Bash',
                        icon: 'fa-terminal',
                        ctx: thiz.ctx,
                        owner: thiz,
                        value: 'ls -l --color=always'
                    }, null, true);

                bashShell.delegate = delegate;
                tab._onShown();
                var tab2 = tabContainer.createTab('Javascript', 'fa-code');
                var jsShell = tab2.add(consoleViewClass, {
                    type: 'javascript',
                    title: 'Javascript',
                    ctx: thiz.ctx,
                    value: 'return 2+2;',
                    owner: thiz
                }, null, false);
                jsShell.delegate = delegate;
                var tab3 = tabContainer.createTab('PHP', 'fa-terminal');
                var jsShell = tab3.add(consoleViewClass, {
                    type: 'php',
                    title: 'PHP',
                    ctx: thiz.ctx,
                    value: '<?php \n\n>',
                    owner: thiz
                }, null, false);
                var tab4 = tabContainer.createTab('Sizes', 'fa-bar-chart');
                var fileSize = tab4.add(FileSize, {
                    owner: this,
                    ctx: thiz.ctx
                }, null, false);
                this.__fileSize = fileSize;
                this._on('openedFolder', function (data) {
                    utils.destroy(thiz.__fileSize);
                    tab4.resize();
                    thiz.resize();
                    thiz.__fileSize = null;
                    thiz.__fileSize = tab4.add(FileSize, {
                        owner: thiz,
                        ctx: thiz.ctx
                    }, null, false);
                    utils.resizeTo(thiz.__fileSize, tab4, true, true);
                    thiz.__fileSize.startup();
                });
                panel.add(tabContainer);
            }
            return this.inherited(arguments);
        },
        onStatusbarCollapse: function (collapser) {
            var panel = null;
            if (this.isStatusbarOpen) {
                panel = this.getBottomPanel(false, 0.2);
                panel.collapse();
                this.onCloseStatusPanel(collapser);
                collapser && collapser.removeClass('fa-caret-down');
                collapser && collapser.addClass('fa-caret-up');
            } else {
                panel = this._getBottom();
                if (!panel) {
                    panel = this.getBottomPanel(false, 0.2);
                } else {
                    panel.expand();
                }
                this.onOpenStatusPanel(panel);

                collapser && collapser.removeClass('fa-caret-up');
                collapser && collapser.addClass('fa-caret-down');
            }
            this.isStatusbarOpen = !this.isStatusbarOpen;
            panel.resize();
        }
    });
    Module.createEditor = createEditor;
    Module.createShellViewDelegate = createShellViewDelegate;
    Module.createShellViewClass = createShellViewClass2;

    return Module;
});
},
'xfile/data/DriverStore':function(){
/**
 * @module xfile/data/DriverStore
 **/
define([
    "dojo/_base/declare",
    'dojo/Deferred',
    'xide/data/ObservableStore',
    'xide/data/TreeMemory',
    "xfile/data/Store",
    "xide/manager/ServerActionBase",
    "xide/utils"
], function (declare, Deferred, ObservableStore, TreeMemory, Store, ServerActionBase, utils) {
    var Implementation = Store.Implementation;

    return declare('driverFileStore', [TreeMemory, ObservableStore, ServerActionBase.declare], utils.mixin(Implementation(), {
        driver: null,
        addDot: false,
        rootSegment: "/",
        getRootItem: function () {
            var root = this._root;
            if (root) {
                return root;
            }
            this._root = {
                _EX: false,
                path: '/',
                name: '/',
                mount: this.mount,
                directory: true,
                virtual: true,
                _S: this,
                getPath: function () {
                    return this.path;
                }
            };
            return this._root;
        },
        _filter: function (items) {
            return items;
        },
        _request: function (path) {
            var collection = this;
            var self = this;
            if (path === '.' || path === '/') {
                var result = new Deferred();
                var dfd = self.driver.callCommand('LSProg', {
                    override: {
                        args: ["/*"]
                    }
                });
                dfd.then(function (data) {
                    var files = data.files;
                    _.each(files, function (file) {
                        file.parent = ".";
                    });
                    files = self._filter(files);
                    var response = {
                        items: [{
                            mount: "root",
                            path: "/",
                            children: files,
                            name: "/",
                            isDir: true
                        }]
                    };

                    var results = collection._normalize(response);
                    self._parse(results);
                    results = results.children || results;
                    result.resolve(results);
                });
                return result;
            } else {
                var result = new Deferred();
                var arg = path + ( self.glob || "/*");
                var dfd = self.driver.callCommand('LSProg', {
                    override: {
                        args: [arg]
                    }
                });
                dfd.then(function (data) {
                    var files = data.files;
                    _.each(files, function (file) {
                        file.parent = path;
                    });
                    files = self._filter(files);
                    var response = {
                        items: [{
                            mount: "root",
                            path: path,
                            children: files,
                            isDir: true
                        }]
                    };
                    var results = collection._normalize(response);
                    self._parse(results);
                    results = results.children || results;
                    result.resolve(results);
                });
                return result;
            }
        }
    }));
});
},
'xfile/views/FileGridLight':function(){
/** @module xfile/views/FileGrid **/
define([
    "xdojo/declare",
    "dojo/dom-class",
    'dojo/Deferred',
    'xide/types',
    'xide/utils',
    'xide/views/History',
    'xaction/DefaultActions',
    'xfile/views/GridLight',
    'xfile/factory/Store',
    'xide/model/Path',
    'xfile/model/File',
    "xfile/types",
    'xlang/i18'
], function (declare, domClass, Deferred, types, utils, History,DefaultActions,GridLight,factory,Path,File,fTypes,il8){
    

    var ACTION = types.ACTION;
    var DEFAULT_PERMISSIONS = fTypes.DEFAULT_FILE_GRID_PERMISSIONS;
    /**
     * A grid feature
     * @class module:xfile/views/FileGrid
     */
    var GridClass = declare('xfile.views.FileGrid', GridLight, {
        resizeAfterStartup:true,
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 50,
            'Settings': 20,
            'Navigation': 10,
            'Window': 5
        },
        groupOrder: {
            'Clipboard': 110,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'Navigation':5,
            'Select': 0
        },
        tabOrder: {
            'Home': 100,
            'View': 50,
            'Settings': 20,
            'Navigation':10
        },
        /**
         *
         */
        noDataMessage: '<span/>',

        /**
         * history {module:xide/views/History}
         */
        _history:null,
        options: utils.clone(types.DEFAULT_GRID_OPTIONS),
        _columns: {},
        toolbarInitiallyHidden:true,
        itemType:types.ITEM_TYPE.FILE,
        permissions: DEFAULT_PERMISSIONS,
        contextMenuArgs:{
            limitTo:null
        },

        /**
         *
         * @param state
         * @returns {*}
         */
        setState:function(state){

            this.inherited(arguments);

            var self = this,
                collection = self.collection,
                path = state.store.path,//'./client/src'
                item = collection.getSync(path),
                dfd = self.refresh();

            try {
                dfd.then(function () {
                    item = collection.getItem(path, true).then(function (item) {
                        self.openFolder(item);
                    });
                });

            }catch(e){
                console.error('error restoring folder state');
            }
            return dfd;
        },
        /**
         *
         * @returns {*|{dir, lang, textDir}|{dir, lang}}
         */
        postMixInProperties: function () {
            var state =this.state;
            if(state){
                if(state._columns){
                    this._columns = state._columns;
                }
            }

            if (!this.columns) {
                this.columns = this.getColumns();
            }

            if(!this.collection && this.state){

                var _store = this.state.store,
                    ctx = this.ctx,
                    store = factory.createFileStore(_store.mount,_store.storeOptions,ctx.config);
                this.collection = store.getDefaultCollection();
            }
            return this.inherited(arguments);
        },
        /**
         *
         * @param state
         * @returns {object}
         */
        getState:function(state) {
            state = this.inherited(arguments) || {};
            state.store = {
                mount:this.collection.mount,
                path:this.getCurrentFolder().path,
                storeOptions:this.collection.options
            };
            state._columns = {};
            _.each(this._columns,function(c){
                state._columns[c.label]= !this.isColumnHidden(c.id);
            },this);
            return state;
        },
        onSaveLayout:function(e){
            var customData = e.data,
                gridState = this.getState(),
                data = {
                    widget:this.declaredClass,
                    state:gridState
                };

            customData.widgets.push(data);
            return customData;
        },
        formatColumn: function (field, value, obj) {
            var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
            if (renderer.formatColumn) {
                var result = renderer.formatColumn.apply(arguments);
                if (result) {
                    return result;
                }
            }
            if(obj.renderColumn){
               var rendered = obj.renderColumn.apply(this,arguments);
                if(rendered){
                    return rendered;
                }

            }
            switch (field) {

                case "fileType":{
                    if(value=='folder'){
                        return il8.localize('kindFolder');
                    }else{
                        if(obj.mime) {
                            var mime = obj.mime.split('/')[1]  ||  "unknown";
                            var key = 'kind' + mime.toUpperCase();
                            var _translated = il8.localize(key);
                            return key !== _translated ? _translated : value;
                        }
                    }
                }
                case "name":{

                    var directory = obj && obj.directory != null && obj.directory === true;
                    var no_access = obj.read === false && obj.write === false;
                    var isBack = obj.name == '..';

                    var folderClass = 'fa-folder';

                    var isLoading = obj.isLoading;
                    if(isLoading){
                        //folderClass = ' fa-spinner fa-spin';
                    }

                    var icon = '';
                    var imageClass = '';
                    var useCSS = false;
                    if (directory) {
                        if (isBack) {
                            imageClass = 'fa fa-level-up itemFolderList';
                            useCSS = true;
                        } else if (!no_access) {
                            imageClass = 'fa ' + folderClass +' itemFolderList';

                            useCSS = true;
                        } else {
                            imageClass = 'fa fa-lock itemFolderList';
                            useCSS = true;
                        }

                    } else {

                        if (!no_access) {
                            imageClass = 'itemFolderList fa ' + utils.getIconClass(obj.path);
                            useCSS = true;
                        } else {
                            imageClass = 'fa fa-lock itemFolderList';
                            useCSS = true;
                        }
                    }
                    var label = obj.showPath === true ? obj.path : value;
                    if (!useCSS) {
                        return '<img class="fileGridIconCell" src="' + icon + ' "/><span class="fileGridNameCell">' + label + '</span>';
                    } else {
                        return '<span class=\"' + imageClass + '\""></span><span class="name fileGridNameNode" style="vertical-align: middle;padding-top: 0px">' + label + '</span>';
                    }
                }
                case "sizeBytes":
                {
                    return obj.size;
                }
                case "fileType":
                {
                    return utils.capitalize(obj.fileType || 'unknown');
                }
                case "mediaInfo":{
                    return obj.mediaInfo || 'unknown';
                }
                case "owner":
                {
                    if(obj) {
                        var owner = obj.owner;
                        if (owner && owner.user) {
                            return owner.user.name;
                        }
                    }
                    return ""
                }
                case "modified":
                {
                    if(value ===''){
                        return value;
                    }
                    var directory = !obj.directory == null;
                    var dateStr = '';
                    if (directory) {

                    } else {
                        var dateFormat = il8.translations.dateFormat;
                        if(dateFormat){
                            var res = il8.formatDate(value);
                            return res.replace('ms','');
                        }
                    }
                    return dateStr;
                }

            }
            return value;
        },
        getColumns: function () {
            var thiz = this;
            this.columns = [];
            function createColumn(label, field, sortable, hidden) {

                if (thiz._columns[label] != null) {
                    hidden = !thiz._columns[label];
                }

                thiz.columns.push({
                    renderExpando: label === 'Name',
                    label: label,
                    field: field,
                    sortable: sortable,
                    formatter: function (value, obj) {
                        return thiz.formatColumn(field, value, obj);
                    },
                    hidden: hidden
                });
            }
            createColumn('Name', 'name', true, false);
            createColumn('Type', 'fileType', true, true);
            createColumn('Path', 'path', true, true);
            createColumn('Size', 'sizeBytes', true, false);
            createColumn('Modified', 'modified', true, false);
            createColumn('Owner', 'owner', true, true);
            createColumn('Media', 'mediaInfo', true, true);
            return this.columns;
        },
        _focus:function(){
            var thiz = this,
                rows = thiz.getRows();
            if(rows[0]){
                var _row = thiz.row(rows[0]);
                thiz.focus(_row.data);
            }
        },
        setQueryEx: function (item, settings) {
            settings = settings || {
                focus: true,
                delay:1
            };
            if (!item) {
                console.error('bad, no item!');
                return false;
            }

            if (!item.directory) {
                return false;
            }

            this._lastPath = item.getPath();
            var thiz = this,
                grid = thiz,
                dfd = new Deferred();

            if (!grid) {
                console.error('have no grid');
                return;
            }
            var col = thiz.collection,
                focusNext;

            if (item.path === '.') {
                col.resetQueryLog();
                grid.set("collection", col.getDefaultCollection(item.getPath()));
                if(dfd.resolve){
                    dfd.resolve();
                }

            } else {
                col.open(item).then(function (items) {
                    col.resetQueryLog();
                    grid.set("collection", col.getDefaultCollection(item.getPath()));
                    if(dfd.resolve) {
                        dfd.resolve(items);
                    }
                });
            }
            return dfd;
        },
        getCurrentFolder:function(){
            var renderer = this.getSelectedRenderer();
            if(renderer && renderer.getCurrentFolder){
                var _result = renderer.getCurrentFolder.apply(this);
                if(_result){
                    if(_result.isBack){
                        var __result = this.collection.getSync(_result.rPath);
                        if(__result){
                            _result = __result;
                        }
                    }
                    return _result;
                }
            }
            var item = this.getRows()[0];
            if(item && (item._S || item._store)) {
                if(item.isBack==true){
                    var _now = this.getHistory().getNow();
                    if(_now){
                        return this.collection.getSync(_now);
                    }
                }
                //current folder:
                var _parent = item._S.getParent(item);
                if(_parent){
                    return _parent;
                }
            }
            return null;
        },
        getClass:function(){
            return GridClass;
        },
        getHistory:function(){
            if(!this._history){
                this._history = new History();
            }
            return this._history;
        },
        renderArray:function(what,data){
            var items = arguments[0];
            var self = this;
            var firstItem = items[0],
                _parent = null;
            var addBack = !_.find(items,{
                isBack:true
            });
            return this.inherited(arguments);
        },
        startup: function () {
            if (this._started) {
                return;
            }
            var res  = this.inherited(arguments);
            domClass.add(this.domNode, 'xfileGrid');
            this.set('loading',true);
            if (this.permissions) {
                var _defaultActions = DefaultActions.getDefaultActions(this.permissions, this,this);
                _defaultActions = _defaultActions.concat(this.getFileActions(this.permissions));
                this.addActions(_defaultActions);
            }
            this._history = new History();
            var self = this;

            this.subscribe(types.EVENTS.ON_CLIPBOARD_COPY,function(evt){
                if(evt.type === self.itemType){
                    self.currentCopySelection = evt.selection;
                    self.refreshActions();
                }
            });
            
            self._on('noData',function(){

                var _rows = self.getRows();
                if(self._total>0){
                    return;
                }
                var _history = self._history,
                    now = _history.getNow();

                if(!now || now ==='./.'){
                    return;
                }
                self.renderArray([
                    {
                        name: '..',
                        path:'..',
                        rPath:now,
                        sizeBytes:0,
                        size:'',
                        icon:'fa-level-up',
                        isBack:true,
                        modified:'',
                        _S:self.collection,
                        directory:true,
                        _EX:true,
                        children:[],
                        mayHaveChildren:false
                    }
                ]);
            });

            this.on('dgrid-refresh-complete',function(){
                var rows = self.getRows();
                if(rows && rows.length>1){
                    var back = _.find(rows,{
                        isBack:true
                    });
                    if(back){
                        self.removeRow(back);
                        self.refresh();
                    }
                }
            })

            this._on('openFolder',function(evt){
                var isBack = evt.back,
                    item = evt.item,
                    path = item.path,
                    history = self._history;
                self.set('title',item.name);
            });

            //initiate
            if(self.selectedRenderer) {
                res = this.refresh();
                res && res.then(function () {
                    self.set('loading',false);
                    self.setRenderer(self.selectedRenderer,false);
                });
            }
            this._on('onChangeRenderer',function(){
                self.refresh();
            });
            setTimeout(function(){
                self.resize();
            },500);
            return res;
        }
    });

    /**
     *
     * @param ctx
     * @param args
     * @param parent
     * @param register
     * @param startup
     * @param store
     * @returns {widgetProto}
     */
    function createDefault(ctx,args,parent,register,startup,store) {

        args = utils.mixin({
            collection: store.getDefaultCollection(),
            _parent: parent,
            Module:GridClass,
            ctx:ctx
        }, args || {});

        var grid = utils.addWidget(GridClass, args, null, parent, startup, null, null, true, null);
        if (register) {
            ctx.getWindowManager().registerView(grid,false);
        }
        return grid;
    }

    GridClass.prototype.Module = GridClass;
    GridClass.Module = GridClass;
    GridClass.createDefault = createDefault;
    GridClass.DEFAULT_PERMISSIONS = DEFAULT_PERMISSIONS;

    return GridClass;

});
},
'xfile/views/GridLight':function(){
/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/ListRenderer',
    'xfile/ThumbRenderer',
    'xgrid/TreeRenderer',
    'xgrid/GridLite',
    'xgrid/MultiRenderer',
    'xfile/FileActions',
    'xide/views/_LayoutMixin',
    'xgrid/KeyboardNavigation',
    'xgrid/Selection',
    'xide/mixins/_State',
    "xide/widgets/_Widget"
], function (declare, types,ListRenderer, ThumbRenderer, TreeRenderer,Grid, MultiRenderer,FileActions,_LayoutMixin,KeyboardNavigation,Selection,_State,_Widget) {
    
    /**
     * A grid feature
     * @class module:xgrid/GridActions
     */
    var Implementation = {

        },
        renderers = [ListRenderer,ThumbRenderer,TreeRenderer],
        multiRenderer = declare.classFactory('multiRenderer',{},renderers,MultiRenderer.Implementation);

    var GridClass = Grid.createGridClass('xfile.views.GridLight', Implementation, {
            SELECTION: {
                CLASS:Selection
            },
            KEYBOARD_SELECTION: true,
            COLUMN_HIDER: true,
            COLUMN_REORDER: false,
            ACTIONS:types.GRID_FEATURES_LITE.ACTIONS,
            ITEM_ACTIONS: {
                CLASS:FileActions
            },
            SPLIT:{
                CLASS:_LayoutMixin
            },
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            STATE:{
                CLASS:_State
            },
            WIDGET:{
                CLASS:_Widget
            }

        },
        {
            RENDERER: multiRenderer
        },
        {
            renderers: renderers,
            selectedRenderer: TreeRenderer
        }
    );
    GridClass.DEFAULT_RENDERERS = renderers;
    GridClass.DEFAULT_MULTI_RENDERER = multiRenderer;
    return GridClass;

});
},
'xfile/manager/Electron':function(){
define([
    'dcl/dcl',
    'xdojo/has',
    'xide/utils',
    'dojo/Deferred'
], function (dcl, has, utils, Deferred) {
    var electron = has('electronx');
    var Module = dcl(null, {
        declaredClass: "xfile.manager.Electron",
        require: function () {
            return window['eRequire'].apply(null, arguments);
        },
        srequire: function () {
            return this.require('electron').remote.require.apply(null, arguments);
        },
        remote: function () {
            var _require = window['eRequire'];
            return _require('electron').remote;
        },
        getContentE: function (_mount, _path, readyCB, emit) {
            var thiz = this,
                item = {
                    path: _path,
                    mount: _mount
                },
                ctx = thiz.ctx || sctx,
                resourceManager = ctx.getResourceManager(),
                vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {},
                dfd = new Deferred();

            var os = this.require('os');
            var shell = this.require('electron').shell;
            var path = this.require("path");
            var fs = this.require("fs");
            var mount = item.mount.replace('/', '');
            if (!vfsConfig[mount]) {
                console.error('open in os failed: have no VFS config for ' + mount);
                return;
            }
            mount = vfsConfig[mount];
            if (!mount) {
                console.error('cant resolve file ' + mount + '/' + _path);
                return false;
            }
            var itemPath = item.path.replace('./', '/');
            itemPath = utils.replaceAll('/', path.sep, itemPath);
            var realPath = path.resolve(mount + path.sep + itemPath);
            if (fs.existsSync(realPath)) {
                var size = fs.statSync(realPath).size,
                    buf = new Buffer(size),
                    fd = fs.openSync(realPath, 'r');
                if (!size) {
                    return "";
                }

                fs.readSync(fd, buf, 0, size, 0);
                fs.closeSync(fd);
                var res = buf.toString();
                dfd.resolve(res);
                if (readyCB) {
                    readyCB(res);
                }

            } else {
                 0 && console.warn('path ' + realPath + ' doesnt exists, fall back to legacy getContent');
                return false;
            }
            return dfd;
        },
        setContentE: function (_mount, _path, content, readyCB) {
            var thiz = this;
            var item = {
                path: _path,
                mount: _mount
            };
            var ctx = thiz.ctx || sctx;
            var resourceManager = ctx.getResourceManager();
            var vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {};
            var dfd = new Deferred();
            var os = this.require('os');
            var path = this.require("path");
            var fs = this.require("fs");
            var mount = item.mount.replace('/', '');
            if (!vfsConfig[mount]) {
                console.error('open in os failed: have no VFS config for ' + mount);
                return;
            }
            mount = vfsConfig[mount];
            if (!mount) {
                 0 && console.warn('cant resolve file mount' + mount + '/' + _path);
                return false;
            }
            var itemPath = item.path.replace('./', '/');
            itemPath = utils.replaceAll('/', path.sep, itemPath);
            var realPath = path.resolve(mount + path.sep + itemPath);
            if (fs.existsSync(realPath)) {
                try {
                    fs.writeFileSync(realPath, content);
                    if (readyCB) {
                        readyCB();
                    }
                    dfd.resolve();
                } catch (e) {
                    console.error('error saving file ' + realPath);
                    return false;
                }
            } else {
                console.error('path ' + realPath + ' doesnt exists, fallback to legacy setContent');
                return false;
            }
            return dfd;
        }
    });
    return Module;
});


}}});
