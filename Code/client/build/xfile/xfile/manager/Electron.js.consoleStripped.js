define("xfile/manager/Electron", [
    'dcl/dcl',
    'xdojo/has',
    'xide/utils',
    'dojo/Deferred'
], function (dcl, has, utils, Deferred) {
    var electron = has('electronx');
    var Module = dcl(null, {
        declaredClass: "xfile.manager.Electron",
        require: function () {
            return window['eRequire'].apply(null, arguments);
        },
        srequire: function () {
            return this.require('electron').remote.require.apply(null, arguments);
        },
        remote: function () {
            var _require = window['eRequire'];
            return _require('electron').remote;
        },
        getContentE: function (_mount, _path, readyCB, emit) {
            var thiz = this,
                item = {
                    path: _path,
                    mount: _mount
                },
                ctx = thiz.ctx || sctx,
                resourceManager = ctx.getResourceManager(),
                vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {},
                dfd = new Deferred();

            var os = this.require('os');
            var shell = this.require('electron').shell;
            var path = this.require("path");
            var fs = this.require("fs");
            var mount = item.mount.replace('/', '');
            if (!vfsConfig[mount]) {
                console.error('open in os failed: have no VFS config for ' + mount);
                return;
            }
            mount = vfsConfig[mount];
            if (!mount) {
                console.error('cant resolve file ' + mount + '/' + _path);
                return false;
            }
            var itemPath = item.path.replace('./', '/');
            itemPath = utils.replaceAll('/', path.sep, itemPath);
            var realPath = path.resolve(mount + path.sep + itemPath);
            if (fs.existsSync(realPath)) {
                var size = fs.statSync(realPath).size,
                    buf = new Buffer(size),
                    fd = fs.openSync(realPath, 'r');
                if (!size) {
                    return "";
                }

                fs.readSync(fd, buf, 0, size, 0);
                fs.closeSync(fd);
                var res = buf.toString();
                dfd.resolve(res);
                if (readyCB) {
                    readyCB(res);
                }

            } else {
                 0 && console.warn('path ' + realPath + ' doesnt exists, fall back to legacy getContent');
                return false;
            }
            return dfd;
        },
        setContentE: function (_mount, _path, content, readyCB) {
            var thiz = this;
            var item = {
                path: _path,
                mount: _mount
            };
            var ctx = thiz.ctx || sctx;
            var resourceManager = ctx.getResourceManager();
            var vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {};
            var dfd = new Deferred();
            var os = this.require('os');
            var path = this.require("path");
            var fs = this.require("fs");
            var mount = item.mount.replace('/', '');
            if (!vfsConfig[mount]) {
                console.error('open in os failed: have no VFS config for ' + mount);
                return;
            }
            mount = vfsConfig[mount];
            if (!mount) {
                 0 && console.warn('cant resolve file mount' + mount + '/' + _path);
                return false;
            }
            var itemPath = item.path.replace('./', '/');
            itemPath = utils.replaceAll('/', path.sep, itemPath);
            var realPath = path.resolve(mount + path.sep + itemPath);
            if (fs.existsSync(realPath)) {
                try {
                    fs.writeFileSync(realPath, content);
                    if (readyCB) {
                        readyCB();
                    }
                    dfd.resolve();
                } catch (e) {
                    console.error('error saving file ' + realPath);
                    return false;
                }
            } else {
                console.error('path ' + realPath + ' doesnt exists, fallback to legacy setContent');
                return false;
            }
            return dfd;
        }
    });
    return Module;
});

