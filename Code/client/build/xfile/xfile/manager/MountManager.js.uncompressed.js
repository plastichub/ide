define("xfile/manager/MountManager", [
    'dcl/dcl',
    "dojo/_base/lang",
    "xdojo/has",
    "xide/manager/ResourceManager",
    "xide/mixins/ReloadMixin",
    "xide/mixins/EventedMixin",
    "xide/types",
    'xide/utils',
    'dojo/Deferred'
], function (dcl, lang, has, ResourceManager, ReloadMixin, EventedMixin, types, utils, Deferred) {
    return dcl([ResourceManager, EventedMixin.dcl, ReloadMixin.dcl], {
        declaredClass: "xfile.manager.MountManager",
        serviceClass: "XApp_Resource_Service",
        mountData: null,
        didReload: false,
        editMount: function (mount) {
            /*
            if (!mount) {
                return;
            }
            if (mount.type === 'FILE_PROXY') {
                this.registerLocalMount(mount);
            } else if (mount.type === 'REMOTE_FILE_PROXY') {

                if (has('remote-vfs')) {
                    if (mount.adapter === 'Ftp') {
                        this.registerFTP(mount);
                    }
                    if (mount.adapter === 'Sftp') {
                        this.registerSFTP(mount);
                    }
                    if (mount.adapter === 'Dropbox') {
                        this.registerDropbox(mount);
                    }
                    if (mount.adapter === 'Webdav') {
                        this.registerWebDav(mount);
                    }
                }
            }
            */
        },
        removeMount: function (mount) {
            /*
            if (!mount) {
                return;
            }
            var thiz = this;
            var onOk = function () {
                var _cb = function () {
                    thiz.ls(function (data) {
                        thiz.onMountDataReady(data)
                    });
                };
                thiz.removeResource(mount, false, _cb);

            };

            var dlg = new FileDeleteDialog({
                title: 'Remove ' + mount.name,
                config: this.config,
                delegate: {
                    onOk: function () {
                        onOk();
                    }
                },
                ctx: this.ctx,
                titleBarClass: 'ui-state-error',
                inserts: [{
                    query: '.dijitDialogPaneContent',
                    insert: '<div><span class="fileManagerDialogText">Do you really want to remove this item' + '?</span></div>',
                    place: 'first'
                }]

            });
            domClass.add(dlg.domNode, 'fileOperationDialog');
            dlg.show();
            dlg.addActionButtons();
            dlg.fixHeight();
            dlg.resize();
            setTimeout(function () {
                dlg.resize();
            }, 1000);
            */
        },
        getMounts: function () {
            return this.mountData;
        },
        _onDialogOk: function (dlg, data, mount) {
            var options = utils.toOptions(data);
            var thiz = this;

            var isUpdate = mount.name != null;

            //build resource object
            var resourceObject = {
                "class": "cmx.types.Resource",
                "enabled": true
            };

            //merge options to resource config
            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                if (option.user == null) {
                    resourceObject[option.name] = option.value;//std resource field
                } else {

                    //we put adapter specific fields into the resource's 'config' field
                    if (!resourceObject.config) {
                        resourceObject.config = {};
                    }
                    resourceObject.config[option.name] = option.value;
                }
            }

            //complete resource config
            var label = '';
            if (resourceObject.label) {
                label = resourceObject.label;
            } else if (resourceObject.config.label) {
                label = resourceObject.config.label;
            }

            resourceObject.name = '' + (mount.name != null ? mount.name : label.toLowerCase());

            if (resourceObject.config.type === 'FILE_PROXY') {
                resourceObject.path = '' + resourceObject.path + '';//VFS Local adjustment
                lang.mixin(resourceObject, resourceObject.config);
            } else if (resourceObject.config.type === 'REMOTE_FILE_PROXY') {
                resourceObject.path = resourceObject.name + '://';//VFS Remote adjustment
            }
            var _cb = function () {
                thiz.ls(function (data) {
                    thiz.onMountDataReady(data)
                });
            };
            if (!isUpdate) {
                this.createResource(resourceObject, false, _cb);
            } else {
                this.updateResource(resourceObject, false, _cb);
            }

        },
        registerLocalMount: function (mount) {
            /*
                        var name = mount ? mount.name : '';
                        var path = mount ? mount.path : '';
                        if (mount && mount.config && mount.config.path) {
                            path = mount.config.path;
                        }
                        var thiz = this;
                        var actionDialog = new CIActionDialog({
                            title: name ? 'Edit Mount ' + name : 'New Local Mount',
                            style: 'max-width:400px;min-height:300px',
                            delegate: {
                                onOk: function (dlg, data) {
                                    thiz._onDialogOk(dlg, data, mount);
                                }
                            },
                            cis: [
                                utils.createCI('label', 13, '', {
                                    group: 'Common',
                                    title: 'Name',
                                    value: name
                                }),
                                utils.createCI('path', 13, '', {
                                    group: 'Common',
                                    title: 'Path',
                                    value: path
                                }),
                                utils.createCI('type', 13, '', {
                                    visible: false,
                                    value: "FILE_PROXY"
                                })
                            ]
                        });
                        actionDialog.show();
                        */
        },
        registerFTP: function (mount) {
            /*
                        mount = mount || {};
                        var config = mount.config || {};
            
            
                        var thiz = this;
                        var actionDialog = new CIActionDialog({
                            title: mount.name ? 'Edit Ftp ' + mount.name : 'New Ftp',
                            style: 'max-width:400px',
                            delegate: {
                                onOk: function (dlg, data) {
                                    thiz._onDialogOk(dlg, data, mount);
                                }
                            },
                            cis: [
                                xide.utils.createCI('label', 13, '', {
                                    group: 'Common',
                                    title: 'Name',
                                    value: mount.name
            
                                }),
                                xide.utils.createCI('root', 13, '', {
                                    group: 'Ftp',
                                    title: 'Start Path',
                                    value: config.root,
                                    user: {
                                        config: true
                                    }
            
                                }),
                                xide.utils.createCI('adapter', 13, '', {
                                    visible: false,
                                    value: 'Ftp'
                                }),
                                xide.utils.createCI('host', 13, '', {
                                    group: 'Ftp',
                                    title: 'Host',
                                    user: {
                                        config: true
                                    },
                                    value: config.host
                                }),
                                xide.utils.createCI('username', 13, '', {
                                    group: 'Ftp',
                                    title: 'User',
                                    user: {
                                        config: true
                                    },
                                    value: config.username
                                }),
                                xide.utils.createCI('password', 13, '', {
                                    group: 'Ftp',
                                    title: 'Password',
                                    user: {
                                        config: true
                                    },
                                    value: config.password
                                }),
                                xide.utils.createCI('passive', 0, '', {
                                    group: 'Ftp',
                                    title: 'Passive',
                                    user: {
                                        config: true
                                    },
                                    value: config.passive
                                }),
                                xide.utils.createCI('ssl', 0, '', {
                                    group: 'Ftp',
                                    title: 'SSL',
                                    user: {
                                        config: true
                                    },
                                    value: config.ssl != null ? config.ssl : false
                                }),
                                xide.utils.createCI('port', 13, '', {
                                    group: 'Ftp',
                                    title: 'Port',
                                    user: {
                                        config: true
                                    },
                                    value: config.port != null ? config.port : 21
                                }),
                                xide.utils.createCI('type', 13, '', {
                                    visible: false,
                                    value: "REMOTE_FILE_PROXY"
                                })
            
                            ]
                        });
                        actionDialog.show();
                        */
        },
        registerSFTP: function (mount) {
            /*
            mount = mount || {};
            var config = mount.config || {};


            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: mount.name ? 'Edit Ftp ' + mount.name : 'New Ftp',
                style: 'max-width:400px',
                delegate: {
                    onOk: function (dlg, data) {
                        thiz._onDialogOk(dlg, data, mount);
                    }
                },
                cis: [
                    xide.utils.createCI('label', 13, '', {
                        group: 'Common',
                        title: 'Name',
                        value: mount.name

                    }),
                    xide.utils.createCI('root', 13, '', {
                        group: 'SFtp',
                        title: 'Start Path',
                        value: config.root,
                        user: {
                            config: true
                        }

                    }),
                    xide.utils.createCI('adapter', 13, '', {
                        visible: false,
                        value: 'Sftp'
                    }),
                    xide.utils.createCI('host', 13, '', {
                        group: 'SFtp',
                        title: 'Host',
                        user: {
                            config: true
                        },
                        value: config.host
                    }),
                    xide.utils.createCI('username', 13, '', {
                        group: 'SFtp',
                        title: 'User',
                        user: {
                            config: true
                        },
                        value: config.username
                    }),
                    xide.utils.createCI('password', 13, '', {
                        group: 'SFtp',
                        title: 'Password',
                        user: {
                            config: true
                        },
                        value: config.password
                    }),
                    xide.utils.createCI('port', 13, '', {
                        group: 'SFtp',
                        title: 'Port',
                        user: {
                            config: true
                        },
                        value: config.port != null ? config.port : 22
                    }),
                    xide.utils.createCI('type', 13, '', {
                        visible: false,
                        value: "REMOTE_FILE_PROXY"
                    })

                ]
            });
            actionDialog.show();
            */
        },
        registerDropbox: function (mount) {
            /*
            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: 'New Dropbox',
                style: 'max-width:400px',
                delegate: {
                    onOk: function (dlg, data) {
                        thiz._onDialogOk(dlg, data, mount);
                    }
                },
                cis: [
                    xide.utils.createCI('label', 13, '', {
                        group: 'Common',
                        title: 'Name',
                        value: 'Dropbox1'

                    }),
                    xide.utils.createCI('pathPrefix', 13, '', {
                        group: 'Common',
                        title: 'Start Path',
                        user: {
                            config: true
                        }

                    }),
                    xide.utils.createCI('adapter', 13, '', {
                        visible: false,
                        value: 'Dropbox'
                    }),
                    xide.utils.createCI('token', 13, '', {
                        group: 'Dropbox',
                        title: 'Token',
                        user: {
                            config: true
                        },
                        value: 'h16UVItP7qQAAAAAAAAABP3qmBJFOHj3fA5ffKyaHH-j7HCLvFOceZxhENV0sy24'
                    }),
                    xide.utils.createCI('appname', 13, '', {
                        group: 'Dropbox',
                        title: 'App Name',
                        user: {
                            config: true
                        },
                        value: 'xapp_local'
                    }),
                    xide.utils.createCI('type', 13, '', {
                        visible: false,
                        value: "REMOTE_FILE_PROXY"
                    })

                ]
            });
            actionDialog.show();
            */
        },
        registerWebDav: function () {
            /*
            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: 'New Webdav',
                style: 'max-width:400px',
                delegate: {
                    onOk: function (dlg, data) {
                        thiz._onDialogOk(dlg, data);
                    }
                },
                cis: [
                    xide.utils.createCI('label', 13, '', {
                        group: 'Common',
                        title: 'name'
                    }),
                    xide.utils.createCI('pathPrefix', 13, '', {
                        group: 'Common',
                        title: 'Start Path'
                    }),
                    xide.utils.createCI('adapter', 13, '', {
                        visible: false,
                        value: 'WebDav'
                    }),
                    xide.utils.createCI('host', 13, '', {
                        group: 'WebDav',
                        title: 'Host'
                    }),
                    xide.utils.createCI('baseUri', 13, '', {
                        group: 'WebDav',
                        title: 'Base URI'
                    }),
                    xide.utils.createCI('userName', 13, '', {
                        group: 'WebDav',
                        title: 'User Name'
                    }),
                    xide.utils.createCI('password', 13, '', {
                        group: 'WebDav',
                        title: 'Password'
                    }),
                    xide.utils.createCI('type', 13, '', {
                        visible: false,
                        value: "REMOTE_FILE_PROXY"
                    })
                ]
            });
            actionDialog.show();
            */
        },
        onMountDataReady: function (data) {
            this.mountData = data;
            this.publish(types.EVENTS.ON_MOUNT_DATA_READY, { data: data });
            var thiz = this;
            setTimeout(function () {
                thiz.publish(types.EVENTS.ON_MOUNT_DATA_READY, { data: data });
            }, 4000);
        },
        check: function () {
            if (!this.serviceObject)
                this._initService();
        },
        /**
         * Callback when context initializes us
         */
        init: function () {
            if (this.ctx.getFileManager()) {
                this.serviceObject = this.ctx.getFileManager().serviceObject;
            }
        },
        /////////////////////////////////////////////////////////////////////////////////
        //
        //  Server Methods
        //
        //////////////////////////////////////////////////////////////////////////////////
        createResource: function (resource, test, readyCB) {
            return this.callMethodEx(null, 'createResource', [resource, test], readyCB, true);
        },
        removeResource: function (resource, test, readyCB) {
            return this.callMethodEx(null, 'removeResource', [resource, true], readyCB, true);
        },
        updateResource: function (resource, test, readyCB) {
            return this.callMethodEx(null, 'updateResource', [resource, true], readyCB, true);
        },

        ls: function (readyCB) {
            function data(_data) {
                if (!has('debug')) {
                    _data = _data[0];
                }
                this.mountData = _data;
                this.onMountDataReady(_data);
                if (readyCB) {
                    readyCB(_data);
                }
            }
            if (!_.isEmpty(this.prefetch)) {
                var dfd = new Deferred();
                dfd.resolve(this.prefetch);
                data.apply(this, [this.prefetch]);
                delete this.prefetch;
                return dfd;
            }

            return this.runDeferred(null, 'ls', []).then(data.bind(this));
        }
    });
});