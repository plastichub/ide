//>>built
define("xfile/component",["dcl/dcl","xide/model/Component"],function(a,b){return a(b,{beanType:"BTFILE",getDependencies:function(){return["xfile/types","xfile/manager/FileManager","xfile/manager/MountManager","xfile/factory/Store","xfile/views/FileGrid"]},getLabel:function(){return"xfile"},getBeanType:function(){return this.beanType}})});
//# sourceMappingURL=component.js.map