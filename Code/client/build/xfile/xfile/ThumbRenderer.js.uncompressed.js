/** @module xfile/ThumbRenderer **/
define("xfile/ThumbRenderer", [
    "xdojo/declare",
    'xide/utils',
    'dojo/dom-construct',
    'xgrid/ThumbRenderer'
], function (declare,utils,domConstruct,ThumbRenderer) {

    /**
     * The list renderer does nothing since the xgrid/Base is already inherited from
     * dgrid/OnDemandList and its rendering as list already.
     *
     * @class module:xfile/ThumbRenderer
     * @extends module:xfile/Renderer
     */
    return declare('xfile.ThumbRenderer',[ThumbRenderer],{
        thumbSize: "400",
        resizeThumb: true,
        __type:'thumb',
        deactivateRenderer:function(){
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding','');
            this.isThumbGrid = false;

        },
        activateRenderer:function(){
            $(this.domNode.parentNode).addClass('metro');
            $(this.contentNode).css('padding','8px');
            this.isThumbGrid = true;
            this.refresh();
        },

        _renderUpload:function(name,progress,obj){

            progress = parseInt(progress.replace('%',''));
            if(progress==100){
                progress = 90;
            }

            var result = '<div class="tile widget uploadItem"><div class="radial-progress tile widget " data-progress="' + progress + '">'+
                '<div class="circle">'+
                '<div class="mask full">'+
                '<div class="fill"></div>'+
                '</div>'+
                '<div class="mask half">'+
                '<div class="fill"></div>'+
                '<div class="fill fix"></div>'+
                '</div>'+
                '<div class="shadow"></div>'+
                '</div>'+
                '<div class="inset">'+
                '<div class="percentage">'+
                '<div class="numbers"><span>-</span>';

            for(var i= 0 ; i< 99 ; i++){

                result+=('<span>' + i +'%' + '</span>');

            };

            result+='</div>'+
                '</div>'+
                '</div>'+
                '</div></div>';

            return result;

        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            if(obj.isUpload === true){
                return $(this._renderUpload(obj.name, obj.progress + '%',obj))[0];
            }
            if(obj.renderRow){
                var _res = obj.renderRow.apply(this,[obj]);
                if(_res){
                    return _res;
                }
            }
            var thiz = this,
                div = domConstruct.create('div', {
                    className: "tile widget"
                }),
                icon = obj.icon,
                no_access = obj.read === false && obj.write === false,
                isBack = obj.name == '..',
                directory = obj && !!obj.directory,
                useCSS = false,
                label = '',
                imageClass = 'fa fa-folder fa-5x',
                isImage = false;

            this._doubleWidthThumbs = true;
            var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;opacity: 0.7';
            var contentClass = 'icon';
            if (directory) {

                if (isBack) {
                    imageClass = 'fa fa-level-up fa-5x itemFolder';
                    useCSS = true;
                } else if (!no_access) {
                    imageClass = 'fa fa-folder fa-5x itemFolder';
                    useCSS = true;
                } else {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                }

            } else {

                imageClass = 'itemIcon';

                if (no_access) {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                } else {

                    if (utils.isImage(obj.path)) {

                        var url = this.getImageUrl(obj);
                        if (url) {
                            obj.icon = url;
                        } else {
                            obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                        }

                        imageClass = 'imageFile';

                    } else {
                        imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                        useCSS = true;
                    }
                }

            }

            label = obj.name;

            var folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';
            if (this.hideExtensions) {
                label = utils.pathinfo(label).filename;
            }

            if (utils.isImage(obj.path)) {

                var url = this.getImageUrl(obj);
                if (url) {
                    obj.icon = url;
                } else {
                    obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                }

                imageClass = '';
                contentClass = 'image';
                folderContent = '<div style="" class="tile-content image">' +
                    '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                    '</div>';

                useCSS = true;
                isImage = true;

            }
            var label2 = label + '\n' + obj.modified;
            var html = '<div title="' + label2 +'" class="tile-content ' + contentClass +'">'+
                folderContent +
                '</div>'+

                '<div class="brand opacity">'+
                '<span class="thumbText text opacity ellipsis" style="">'+
                label +
                '</span>'+
                '</div>';

            if (useCSS) {
                div.innerHTML = html;
                return div;
            }


            if (directory) {
                div.innerHTML = html;
            } else {
                div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
            }
            return div;

        },
        getImageUrl: function (item) {
            var fileManager = this.ctx.getFileManager();
            if (fileManager && fileManager) {
                var params = null;
                if (this.resizeThumb) {
                    params = {
                        width: this.thumbSize
                    }
                }
                return fileManager.getImageUrl(item, null, params);
            }
            return null;
        }
    });
});