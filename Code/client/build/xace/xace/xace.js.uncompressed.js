require({cache:{
'xace/main':function(){
define([
    "dojo/text",
	"dojo/_base/kernel",
    "xace/views/ACEEditor",
    "xace/views/Editor",
    "xace/xace"
], function(){
    return dojo.xace;
});

},
'xace/views/ACEEditor':function(){
/** @module xace/views/ACEEditor **/
define([
    'dcl/dcl',
    'dojo/has',
    'dojo/dom-construct',
    'xide/utils',
    'xide/types',
    'xide/widgets/TemplatedWidgetBase',
    'xace/views/_Split',
    'xaction/DefaultActions',
    'xace/views/_AceMultiDocs'
], function (dcl, has, domConstruct,utils, types, TemplatedWidgetBase, Splitter, DefaultActions, _AceMultiDocs) {
    var _loadedModes = {};//global cache for loaded modes
    var containerClass = dcl([TemplatedWidgetBase], {
        resizeToParent: true,
        templateString: '<div attachTo="aceNode" style="width: 100%;height: 100%" class=""></div>'
    });

    var EditorInterfaceImplementation = dcl(_AceMultiDocs, {
        declaredClass: 'xace/views/EditorInterface',
        editorSession: null,
        _lastValue:null,
        enableMultiDocuments: function () {
            var completer = this.addFileCompleter();
            var text = "var xxxTop = 2*3;";
            var path = "asdf.js";
            completer.addDocument(path, text);
        },
        loadScript: function (url, attributes, readyCB) {
            // DOM: Create the script element
            var jsElm = document.createElement("script");
            // set the type attribute
            jsElm.type = "application/javascript";
            // make the script element load file
            jsElm.src = url;
            jsElm.onload = function () {
                if (readyCB) {
                    readyCB();
                }
            };
            // finally insert the element to the body element in order to load the script
            document.body.appendChild(jsElm);
        },
        setMode: function (mode, _ctx, cb) {
            var ctx = _ctx || this.ctx || window['sctx'];
            if (ctx && ctx.getResourceManager()) {
                var thiz = this;
                if (!_loadedModes[mode]) {
                    var modeFile = null;
                    var aceRoot = ctx.getResourceManager().getVariable(types.RESOURCE_VARIABLES.ACE);
                    if (!aceRoot) {
                        var webRoot = ctx.getResourceManager().getVariable(types.RESOURCE_VARIABLES.APP_URL);
                        if (has('debug') === true) {
                            webRoot += '/xfile/ext/ace/';
                        } else {
                            webRoot += '/xfile/ext/ace/'
                        }
                        modeFile = webRoot + '/mode-' + mode + '.js';
                    } else {
                        modeFile = aceRoot + '/mode-' + mode + '.js';
                    }
                    this.loadScript(modeFile, null, function () {
                        _loadedModes[mode] = true;//update cache
                        thiz.set('mode', mode);
                        cb && cb(mode);

                    });
                } else {
                    thiz.set('mode', mode);
                    cb && cb(mode);
                }

            } else {
                console.error('have no resource manager!');
            }
        },
        get: function (what) {
            if (what === 'value') {
                var self = this,
                    editor = self.getEditor(),

                    session = editor ? editor.session : null;

                return session ? session.getValue() : null;

            }
            return this.inherited(arguments);
        },
        set: function (key, value) {
            var self = this,
                editor = this.getEditor(),
                session = this.editorSession;

            if (key === 'iconClass') {
                var _parent = this._parent;
                if (_parent && _parent.icon) {
                    this._parent.icon(value);
                }
            }

            if (editor && session) {
                //console.log('set ace option ' + key,value);
                var node = editor.container;
                if (key == 'item') {
                    session.setUseWorker(false);
                    self.getContent(value,
                        function (content) {
                            var newMode = self._getMode(value.path);
                            self.set('value', content);
                            self.setMode(newMode);
                            session.setUseWorker(self.options.useWorker);
                        });
                }

                if (key == "value" && value) {
                    session.setValue(value);
                }
                else if (key == "theme") {
                    if (typeof value == "string") {
                        value = "ace/theme/" + value;
                    }
                    editor.setTheme(value);
                    if (this.split) {
                        this.split.setTheme(value);
                    }
                }
                else if (key == "mode") {
                    try {
                        if ( 1 ) {
                            ace.require(["ace/mode/" + value], function (modeModule) {
                                if (modeModule && modeModule.Mode) {
                                    self.split.$editors.forEach(function (editor) {
                                        editor.session.setMode(new modeModule.Mode());
                                    });
                                }
                            });
                        }
                    } catch (e) {
                        console.error('ace mode failed : ' + value);
                    }
                }
                else if (key == "readOnly") {
                    editor.setReadOnly(value);
                }
                else if (key == "tabSize") {
                    session.setTabSize(value);
                }
                else if (key == "softTabs") {
                    session.setUseSoftTabs(value);
                }
                else if (key == "wordWrap") {
                    session.setUseWrapMode(value);
                }
                else if (key == "printMargin") {
                    editor.renderer.setPrintMarginColumn(value);
                }
                else if (key == "showPrintMargin") {
                    editor.setShowPrintMargin(value);
                }
                else if (key == "highlightActiveLine") {
                    editor.setHighlightActiveLine(value);
                }
                else if (key == "fontSize") {
                    $(node).css(key, value);
                }
                else if (key == "showIntentGuides") {
                    editor.setDisplayIndentGuides(value);
                }
                else if (key == "elasticTabstops") {
                    editor.setOption("useElasticTabstops", value);
                }
                else if (key == "useIncrementalSearch") {
                    editor.setOption("useIncrementalSearch", value);
                }
                else if (key == "showGutter") {
                    editor.renderer.setShowGutter(value);
                }
            }
            return this.inherited("set", arguments);
        },
        _getMode: function (fileName) {
            fileName = fileName || this.fileName;
            var ext = 'javascript';
            if (fileName) {

                ext = utils.getFileExtension(fileName) || 'js';

                if (ext === 'js' || ext === 'xblox') {
                    ext = 'javascript';
                }

                if (ext === 'h' || ext === 'cpp') {
                    ext = 'c_cpp';
                }

                if (ext === 'html' || ext === 'cfhtml' || ext === 'dhtml') {
                    ext = 'html';
                }
                if (ext === 'cp') {
                    ext = 'xml';
                }
                if (ext === 'md') {
                    ext = 'markdown';
                }
                if (ext === 'hbs') {
                    ext = 'handlebars';
                }
            }
            return ext;
        },
        setOptions: function (options) {
            this.options = options;
            var self = this;
            _.each(options, function (value, name) {
                self.set(name, value);
            });

            var editor = this.getEditor();
            if (editor && options.aceOptions) {
                editor.setOptions(options.aceOptions);
                editor.session.setUseWorker(options.useWorker);
                editor.setOptions({
                    enableBasicAutocompletion: true,
                    enableSnippets: true,
                    enableLiveAutocompletion: true
                });
                this.setMode(options.mode);
            }
            return this.options;
        },
        getOptions: function () {
            return this.options;
        },
        onContentChange: function (value) {
            if(this._parent && this._parent.set){
                this._parent.set('changed',value!==this.lastSavedContent);
            }
        },
        onDidChange: function () {
            var value = this.get('value');
            if(this._lastValue!==value){
                this._lastValue = value;
                this._emit('change',value);
                this.onContentChange(value);
            }
        },
        getDefaultOptions: function (value, mixin) {
            var thiz = this;
            return utils.mixin({
                region: "center",
                style: "margin: 0; padding: 0; position:relative;overflow: auto;height:inherit;width:inherit;text-align:left;",
                readOnly: false,
                tabSize: 2,
                wordWrap: false,
                showPrintMargin: false,
                highlightActiveLine: true,
                fontSize: 16,
                showGutter: true,
                showLineNumbers: true,
                useWorker: true,
                showInvisibles: false,
                displayIndentGuides: true,
                useSoftTabs: true,
                fileName: 'none',
                mode: 'javascript',
                value: value || this.value || 'No value',
                theme: 'idle_fingers',
                splits: 1,
                useElasticTabstops: false,
                animatedScroll: false,
                highlightActive: true,
                aceOptions: {
                    enableBasicAutocompletion: true,
                    enableSnippets: true,
                    enableLiveAutocompletion: true
                },
                onLoad: function (_editor) {
                    // This is to remove following warning message on console:
                    // Automatically scrolling cursor into view after selection change this will be disabled in the next version
                    // set editor.$blockScrolling = Infinity to disable this message
                    _editor.$blockScrolling = Infinity;
                },
                onDidChange: function () {
                    var value = thiz.get('value');
                    if(value!==thiz.lastSavedContent) {
                        thiz.onContentChange(value);
                    }
                },
                onPrefsChanged: function () {
                    thiz.setPreferences && thiz.setPreferences();
                }
            }, mixin);
        },
        getEditor: function (index) {
            if (this.split) {
                return index == null ? this.split.getCurrentEditor() : this.split.getEditor(index != null ? index : 0);
            } else if (this.editor) {
                return this.editor;
            }
        },
        resize: function (what, target, event) {
            var options = this.options || {};

            this.onResize && this.onResize();

            function _resize() {
                var editor = this.getEditor(),
                    widget = this.split || this.editor;
                if (!editor || !this.aceNode || !editor.container) {
                    this['resize_debounced'].cancel();
                    this['resize_debounced'] = null;
                    return;
                }
                editor && utils.resizeTo(editor.container, this.aceNode, true, true);
                return widget ? widget.resize() : null;
            }
            return this.debounce('resize', _resize.bind(this), options.resizeDelay || 300, null);
            //_resize().bind(this);
        },
        getAce: function () {
            return this.getEditor();
        },
        addBasicCommands: function (editor) {
            editor = editor || this.getEditor();
            var thiz = this,
                whiteSpace = ace.require("ace/ext/whitespace");

            this._whiteSpaceExt = whiteSpace;


            editor.commands.addCommands(whiteSpace.commands);

            editor.commands.addCommands([
                {
                    name: "gotoline",
                    bindKey: {win: "Ctrl-L", mac: "Command-L"},
                    exec: function (editor, line) {
                        if (typeof line == "object") {
                            var arg = this.name + " " + editor.getCursorPosition().row;
                            editor.cmdLine.setValue(arg, 1);
                            editor.cmdLine.focus();
                            return;
                        }
                        line = parseInt(line, 10);
                        if (!isNaN(line))
                            editor.gotoLine(line);
                    },
                    readOnly: true
                },
                {
                    name: "snippet",
                    bindKey: {win: "Alt-C", mac: "Command-Alt-C"},
                    exec: function (editor, needle) {
                        if (typeof needle == "object") {
                            editor.cmdLine.setValue("snippet ", 1);
                            editor.cmdLine.focus();
                            return;
                        }
                        var s = snippetManager.getSnippetByName(needle, editor);
                        if (s)
                            snippetManager.insertSnippet(editor, s.content);
                    },
                    readOnly: true
                },
                {
                    name: "increaseFontSize",
                    bindKey: "Ctrl-+",
                    exec: function (editor) {
                        editor.setFontSize(editor.getFontSize() + 1);
                        thiz.onAfterAction();
                    }
                }, {
                    name: "decreaseFontSize",
                    bindKey: "Ctrl+-",
                    exec: function (editor) {
                        editor.setFontSize(editor.getFontSize() - 1);
                        thiz.onAfterAction();
                    }
                }, {
                    name: "resetFontSize",
                    bindKey: "Ctrl+0",
                    exec: function (editor) {
                        editor.setFontSize(12);
                        thiz.onAfterAction();
                    }
                }
            ]);
        },
        onEditorCreated: function (editor, options) {
            var thiz = this;
            editor.getSelectedText = function () {
                return thiz.editorSession.getTextRange(this.getSelectionRange());
            };
            editor.on('change', function () {
                thiz.onDidChange(arguments);
            });
            this.addBasicCommands(editor);
            editor.setFontSize(options.fontSize);
            editor.$blockScrolling = Infinity;
            //var a = editor.session.doc.getNewLineCharacter();

            if(this._whiteSpaceExt){
                this._whiteSpaceExt.detectIndentation(editor.session);
            }

        },
        destroy: function () {
            var editor = this.getEditor();
            editor && editor.destroy();
            var _resize = this['resize_debounced'];
            if (_resize) {
                _resize.cancel();
            }
        },
        getOptionsMixed: function (_options) {
            var settings = this.getPreferences ? this.getPreferences() : {};
            var options = this.getDefaultOptions(this.value, _options || this.options);
            //apply overrides
            utils.mixin(options, _options);
            //apply settings from persistence store
            utils.mixin(options, settings);
            options.mode = this._getMode(options.fileName);
            return options;
        },
        createEditor: function (_options, value) {

            this.set('iconClass', this.iconClassNormal);

            if (this.editor || this.split) {
                return this.editor || this.split;
            }
            var settings = this.getPreferences ? this.getPreferences() : {};
            var options = this.getDefaultOptions(value);

            //apply overrides
            utils.mixin(options, _options);
            //apply settings from persistence store
            utils.mixin(options, settings);
            options.mode = this._getMode(options.fileName);

            var node = options.targetNode || domConstruct.create('div');
            $(node).css({
                padding: "0",
                margin: "0",
                height: '100%',
                width: '100%'
            });


            this.aceNode.appendChild(node);

            var config = ace.require("ace/config"),
                split = null,
                editor = null;
            ace.require("ace/ext/language_tools");
            try {
                var Split = Splitter.getSplitter();
                split = new Split(node, null, 1);
                this.split = split;
                this._aceConfig = config;
                config.init();
                //ace.require('ace/ext/language_tools');
                this.editor = editor = split.getEditor(0);
                this.editorSession = this.editor.getSession();
                if (value) {
                    this.editorSession.setValue(value);
                }
                split && split.setSplits(options.splits);
            } catch (e) {
                logError(e, 'error creating editor');
            }
            this.setOptions(options);
            this.onEditorCreated(editor, options);
            return editor;
        },
        addAutoCompleter: function (list) {
            var langTools = ace.require("ace/ext/language_tools");
            var rhymeCompleter = {
                getCompletions: function (editor, session, pos, prefix, callback) {
                    if (prefix.length === 0) {
                        callback(null, []);
                        return;
                    }
                    if (!list) {
                        callback(null, []);
                        return;
                    }
                    callback(null, list.map(function (ea) {
                        return {name: ea.value, value: ea.word, meta: ea.meta}
                    }));
                }
            };
            langTools.addCompleter(rhymeCompleter);
        }
    });

    var EditorClass = dcl(null, {
        declaredClass: 'xace/views/ACE',
        onLoaded: function () {
            this.set('iconClass', this.iconClassNormal);
        },
        getKeyTarget: function () {
            return this.aceNode;
        },
        startup: function () {
            this.aceNode.id = utils.createUUID();
            if (this.permissions) {
                var _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
                _defaultActions = _defaultActions.concat(this.getEditorActions(this.permissions));
                this.addActions(_defaultActions);
            }

            //save icon class normal
            this.iconClassNormal = '' + this.iconClass;
            this.set('iconClass', 'fa-spinner fa-spin');
            var self = this,
                options = this.options || {};

            if (!this.item && this.value == null) {
                return;
            }

            function createEditor(options, value) {
                /*
                if(typeof ace === "function"){
                    ace(function(_ace){
                        ACE = _ace;
                        self.createEditor(self.options || options, value);
                    });
                    return;
                }
                */
                self.createEditor(self.options || options, value);
            }




            if (this.value != null) {
                this.lastSavedContent = '' + this.value;
                createEditor(null, this.value);
            } else {
                //we have no content yet, call in _TextEditor::getContent, this will be forwarded
                //to our 'storeDelegate'
                this.getContent(
                    this.item,
                    function (content) {//onSuccess
                        self.lastSavedContent = content;
                        createEditor(options, content);
                    },
                    function (e) {//onError
                        createEditor(null, '');
                        logError(e, 'error loading content from file');
                    }
                );
            }
        }
    });
    var Module = dcl([containerClass, EditorClass, EditorInterfaceImplementation], {});
    Module.EditorImplementation = EditorInterfaceImplementation;
    Module.Editor = EditorClass;
    Module.Container = containerClass;
    Module.Splitter = Splitter;
    return Module;
});
},
'xace/views/_Split':function(){
define([
    'dcl/dcl'
],function (dcl){

    var _splitProto = null;
    var getSplitProto = function() {

        if(_splitProto){
            return _splitProto;
        }



        var require = ace.require;
        var oop = require("ace/lib/oop");
        var lang = require("ace/lib/lang");
        var EventEmitter = require("ace/lib/event_emitter").EventEmitter;

        var Editor = require("ace/editor").Editor;
        var Renderer = require("ace/virtual_renderer").VirtualRenderer;
        var EditSession = require("ace/edit_session").EditSession;
        var UndoManager = require("ace/undomanager").UndoManager;
        var HashHandler = require("ace/keyboard/hash_handler").HashHandler;

        var Split = function (container, theme, splits) {
            this.BELOW = 1;
            this.BESIDE = 0;

            this.$container = container;
            this.$theme = theme;
            this.$splits = 0;
            this.$editorCSS = "";
            this.$editors = [];
            this.$orientation = this.BESIDE;

            this.setSplits(splits || 1);
            this.$cEditor = this.$editors[0];


            this.on("focus", function (editor) {
                this.$cEditor = editor;
            }.bind(this));
        };

        (function () {

            oop.implement(this, EventEmitter);

            this.$createEditor = function () {
                var el = document.createElement("div");
                el.className = this.$editorCSS;
                el.style.cssText = "position: absolute; top:0px; bottom:0px";
                this.$container.appendChild(el);
                var editor = new Editor(new Renderer(el, this.$theme));


                editor.on("focus", function () {
                    this._emit("focus", editor);
                }.bind(this));

                this.$editors.push(editor);

                //var undoManager = editor.session.getUndoManager();
                editor.session.setUndoManager(new UndoManager());

                editor.setFontSize(this.$fontSize);
                return editor;
            };

            this.setSplits = function (splits) {
                var editor;
                if (splits < 1) {
                    throw "The number of splits have to be > 0!";
                }

                if(splits==1){

                }

                if (splits == this.$splits) {
                    return;
                } else if (splits > this.$splits) {
                    while (this.$splits < this.$editors.length && this.$splits < splits) {
                        editor = this.$editors[this.$splits];
                        this.$container.appendChild(editor.container);
                        editor.setFontSize(this.$fontSize);
                        this.$splits++;
                    }
                    while (this.$splits < splits) {
                        this.$createEditor();
                        this.$splits++;
                    }
                } else {
                    while (this.$splits > splits) {
                        editor = this.$editors[this.$splits - 1];
                        this.$container.removeChild(editor.container);
                        this.$splits--;
                    }
                }
                this.resize();
            };

            /**
             *
             * Returns the number of splits.
             * @returns {Number}
             **/
            this.getSplits = function () {
                return this.$splits;
            };

            /**
             * @param {Number} idx The index of the editor you want
             *
             * Returns the editor identified by the index `idx`.
             *
             **/
            this.getEditor = function (idx) {
                return this.$editors[idx];
            };

            /**
             *
             * Returns the current editor.
             * @returns {Editor}
             **/
            this.getCurrentEditor = function () {
                return this.$cEditor;
            };

            /**
             * Focuses the current editor.
             * @related Editor.focus
             **/
            this.focus = function () {
                this.$cEditor.focus();
            };

            /**
             * Blurs the current editor.
             * @related Editor.blur
             **/
            this.blur = function () {
                this.$cEditor.blur();
            };

            this.setSessionOption= function(what,value){
                this.$editors.forEach(function (editor) {

                    var session  =  editor.session;
                    if(what=='mode'){
                        session.setMode(value);
                    }

                });
            };
            /**
             *
             * @param {String} theme The name of the theme to set
             *
             * Sets a theme for each of the available editors.
             * @related Editor.setTheme
             **/
            this.setTheme = function (theme) {
                this.$editors.forEach(function (editor) {
                    editor.setTheme(theme);
                });
            };

            /**
             *
             * @param {String} keybinding
             *
             * Sets the keyboard handler for the editor.
             * @related editor.setKeyboardHandler
             **/
            this.setKeyboardHandler = function (keybinding) {
                this.$editors.forEach(function (editor) {
                    editor.setKeyboardHandler(keybinding);
                });
            };

            /**
             *
             * @param {Function} callback A callback function to execute
             * @param {String} scope The default scope for the callback
             *
             * Executes `callback` on all of the available editors.
             *
             **/
            this.forEach = function (callback, scope) {
                this.$editors.forEach(callback, scope);
            };


            this.$fontSize = "";
            /**
             * @param {Number} size The new font size
             *
             * Sets the font size, in pixels, for all the available editors.
             *
             **/
            this.setFontSize = function (size) {
                this.$fontSize = size;
                this.forEach(function (editor) {
                    editor.setFontSize(size);
                });
            };

            this.$cloneSession = function (session) {
                var s = new EditSession(session.getDocument(), session.getMode());

                var undoManager = session.getUndoManager();
                if (undoManager) {
                    var undoManagerProxy = new UndoManagerProxy(undoManager, s);
                    s.setUndoManager(undoManagerProxy);
                }

                // Overwrite the default $informUndoManager function such that new delas
                // aren't added to the undo manager from the new and the old session.
                s.$informUndoManager = lang.delayedCall(function () {
                    s.$deltas = [];
                });

                // Copy over 'settings' from the session.
                s.setTabSize(session.getTabSize());
                s.setUseSoftTabs(session.getUseSoftTabs());
                s.setOverwrite(session.getOverwrite());
                s.setBreakpoints(session.getBreakpoints());
                s.setUseWrapMode(session.getUseWrapMode());
                s.setUseWorker(session.getUseWorker());
                s.setWrapLimitRange(session.$wrapLimitRange.min,
                    session.$wrapLimitRange.max);
                s.$foldData = session.$cloneFoldData();

                return s;
            };

            /**
             *
             * @param {EditSession} session The new edit session
             * @param {Number} idx The editor's index you're interested in
             *
             * Sets a new [[EditSession `EditSession`]] for the indicated editor.
             * @related Editor.setSession
             **/
            this.setSession = function (session, idx) {
                var editor;
                if (idx == null) {
                    editor = this.$cEditor;
                } else {
                    editor = this.$editors[idx];
                }

                // Check if the session is used already by any of the editors in the
                // split. If it is, we have to clone the session as two editors using
                // the same session can cause terrible side effects (e.g. UndoQueue goes
                // wrong). This also gives the user of Split the possibility to treat
                // each session on each split editor different.
                var isUsed = this.$editors.some(function (editor) {
                    return editor.session === session;
                });

                if (isUsed) {
                    session = this.$cloneSession(session);
                }
                editor.setSession(session);

                // Return the session set on the editor. This might be a cloned one.
                return session;
            };

            /**
             *
             * Returns the orientation.
             * @returns {Number}
             **/
            this.getOrientation = function () {
                return this.$orientation;
            };

            /**
             *
             * Sets the orientation.
             * @param {Number} orientation The new orientation value
             *
             *
             **/
            this.setOrientation = function (orientation) {
                if (this.$orientation == orientation) {
                    return;
                }
                this.$orientation = orientation;
                this.resize();
            };

            /**
             * Resizes the editor.
             **/
            this.resize = function () {
                var width = this.$container.clientWidth;
                var height = this.$container.clientHeight;
                var editor;

                if (this.$orientation == this.BESIDE) {
                    var editorWidth = width / this.$splits;
                    if(this.diffGutter){
                        editorWidth -=60/this.$splits;
                    }


                    for (var i = 0; i < this.$splits; i++) {
                        editor = this.$editors[i];
                        editor.container.style.width = editorWidth + "px";
                        editor.container.style.top = "0px";
                        editor.container.style.left = i * editorWidth + "px";
                        if(i==1 && this.diffGutter){
                            editor.container.style.left = 60 +  (i * editorWidth) + "px";
                            this.diffGutter.style.left = i * editorWidth + "px";
                            /*this.diffGutter.style.height = height + "px";*/
                        }

                        //editor.container.style.height = height + "px";
                        if(!height){
                            return;
                        }
                        $(editor.container).css('height',height + "px");
                        var cNode = $(editor.container).find('.ace_content');
                        editor.resize();
                        cNode.css('height',height + "px");

                    }
                } else {
                    var editorHeight = height / this.$splits;
                    for (var i = 0; i < this.$splits; i++) {
                        editor = this.$editors[i];
                        editor.container.style.width = width + "px";
                        editor.container.style.top = i * editorHeight + "px";
                        editor.container.style.left = "0px";
                        editor.container.style.height = editorHeight + "px";
                        editor.resize();
                    }
                }
            };

        }).call(Split.prototype);


        function UndoManagerProxy(undoManager, session) {
            this.$u = undoManager;
            this.$doc = session;
        }

        (function () {
            this.execute = function (options) {
                this.$u.execute(options);
            };

            this.undo = function () {
                var selectionRange = this.$u.undo(true);
                if (selectionRange) {
                    this.$doc.selection.setSelectionRange(selectionRange);
                }
            };

            this.redo = function () {
                var selectionRange = this.$u.redo(true);
                if (selectionRange) {
                    this.$doc.selection.setSelectionRange(selectionRange);
                }
            };

            this.reset = function () {
                this.$u.reset();
            };

            this.hasUndo = function () {
                return this.$u.hasUndo();
            };

            this.hasRedo = function () {
                return this.$u.hasRedo();
            };
        }).call(UndoManagerProxy.prototype);

        _splitProto = Split;
        /*});*/
        return _splitProto;
    };
    var Module = dcl(null,{
        declaredClass:'xace/views/Split'
    });
    Module.getSplitter = getSplitProto;
    return Module;
});

},
'xace/views/_AceMultiDocs':function(){
define([
    "dcl/dcl",
    "xdojo/declare",
    "module",
    "xace/base_handler",
    "xace/complete_util"
],function (dcl,declare,module,baseLanguageHandler,completeUtil){

    var analysisCache = {}; // path => {identifier: 3, ...}
    var globalWordIndex = {}; // word => frequency
    var globalWordFiles = {}; // word => [path]
    var precachedPath;
    var precachedDoc;

    var completer = module.exports = Object.create(baseLanguageHandler);

    completer.handlesLanguage = function(language) {
        return true;
    };

    completer.handlesEditor = function() {
        return this.HANDLES_ANY;
    };

    completer.getMaxFileSizeSupported = function() {
        return 1000 * 1000;
    };

    function frequencyAnalyzer(path, text, identDict, fileDict) {
        var identifiers = text.split(/[^a-zA-Z_0-9\$]+/);
        for (var i = 0; i < identifiers.length; i++) {
            var ident = identifiers[i];
            if (!ident)
                continue;

            if (Object.prototype.hasOwnProperty.call(identDict, ident)) {
                identDict[ident]++;
                fileDict[ident][path] = true;
            }
            else {
                identDict[ident] = 1;
                fileDict[ident] = {};
                fileDict[ident][path] = true;
            }
        }
        return identDict;
    }

    function removeDocumentFromCache(path) {
        var analysis = analysisCache[path];
        if (!analysis) return;

        for (var id in analysis) {
            globalWordIndex[id] -= analysis[id];
            delete globalWordFiles[id][path];
            if (globalWordIndex[id] === 0) {
                delete globalWordIndex[id];
                delete globalWordFiles[id];
            }
        }
        delete analysisCache[path];
    }

    function analyzeDocument(path, allCode) {
        if (!analysisCache[path]) {
            if (allCode.size > 80 * 10000) {
                delete analysisCache[path];
                return;
            }
            // Delay this slightly, because in Firefox document.value is not immediately filled
            analysisCache[path] = frequencyAnalyzer(path, allCode, {}, {});
            // may be a bit redundant to do this twice, but alright...
            frequencyAnalyzer(path, allCode, globalWordIndex, globalWordFiles);
        }
    }

    completer.onDocumentOpen = function(path, doc, oldPath, callback) {
        if (!analysisCache[path]) {
            analyzeDocument(path, doc.getValue());
        }
        callback();
    };

    completer.addDocument = function(path, value) {
        if (!analysisCache[path]) {
            analyzeDocument(path, value);
        }

    };

    completer.onDocumentClose = function(path, callback) {
        removeDocumentFromCache(path);
        if (path == precachedPath)
            precachedDoc = null;
        callback();
    };

    completer.analyze = function(doc, ast, callback, minimalAnalysis) {
        if (precachedDoc && this.path !== precachedPath) {
            removeDocumentFromCache(precachedPath);
            analyzeDocument(precachedPath, precachedDoc);
            precachedDoc = null;
        }
        precachedPath = this.path;
        precachedDoc = doc;
        callback();
    };

    completer.complete = function(editor, fullAst, pos, currentNode, callback) {

        var doc = editor.getSession();
        var line = doc.getLine(pos.row);
        var identifier = completeUtil.retrievePrecedingIdentifier(line, pos.column, this.$getIdentifierRegex());
        var identDict = globalWordIndex;

        var allIdentifiers = [];
        for (var ident in identDict) {
            allIdentifiers.push(ident);
        }
        var matches = completeUtil.findCompletions(identifier, allIdentifiers);

        var currentPath = this.path;
        matches = matches.filter(function(m) {
            return !globalWordFiles[m][currentPath];
        });

        matches = matches.slice(0, 100); // limits results for performance

        callback(matches.filter(function(m) {
            return !m.match(/^[0-9$_\/]/);
        }).map(function(m) {
            var path = Object.keys(globalWordFiles[m])[0] || "[unknown]";
            var pathParts = path.split("/");
            var foundInFile = pathParts[pathParts.length-1];
            return {
                name: m,
                value: m,
                icon: null,
                score: identDict[m],
                meta: foundInFile,
                priority: 0,
                isGeneric: true
            };
        }));
    };

    completer.getCompletions=function (editor, session, pos, prefix, callback) {
        var completions = null;
        var _c = function(_completions){
            completions = _completions;
        };
        completer.complete(editor,null,pos,null,_c);
        callback(null,completions);
    };

    var Module  = dcl(null,{

        declaredClass:"xide.views._AceMultiDocs",
        didAddMCompleter:false,
        multiFileCompleter:null,
        addFileCompleter:function(){
            var compl = null;
            if(!this.didAddMCompleter) {
                compl = completer;
                this.multiFileCompleter= compl;
                var langTools = ace.require("ace/ext/language_tools");
                langTools.addCompleter(compl);
                this.didAddMCompleter=true;
            }
            return compl;
        }
    });

    Module.completer = completer;

    return Module;
});
},
'xace/base_handler':function(){
/**
 * This module is used as a base class for language handlers.
 * It provides properties, helper functions, and functions that
 * can be overridden by language handlers to implement
 * language services such as code completion.
 *
 * See {@link language} for an example plugin.
 *
 * @class language.base_handler
 */
define(function(require, exports, module) {

    module.exports = {

        /**
         * Indicates the handler handles editors, the immediate window,
         * and anything else.
         */
        HANDLES_ANY: 0,

        /**
         * Indicates the handler only handles editors, not the immediate window.
         */
        HANDLES_EDITOR: 1,

        /**
         * Indicates the handler only handles the immediate window, not editors.
         */
        HANDLES_IMMEDIATE: 2,

        /**
         * Indicates the handler only handles the immediate window, not editors.
         */
        HANDLES_EDITOR_AND_IMMEDIATE: 3,

        /**
         * The language this worker is currently operating on.
         * @type {String}
         */
        language: null,

        /**
         * The path of the file this worker is currently operating on.
         * @type {String}
         */
        path: null,

        /**
         * The current workspace directory.
         * @type {String}
         */
        workspaceDir: null,

        /**
         * The current document this worker is operating on.
         *
         * @type {Document}
         */
        doc: null,

        // UTILITIES

        /**
         * Utility function, used to determine whether a certain feature is enabled
         * in the user's preferences.
         *
         * Should not be overridden by inheritors.
         *
         * @deprecated Use worker_util#isFeatureEnabled instead
         *
         * @param {String} name  The name of the feature, e.g. "unusedFunctionArgs"
         * @return {Boolean}
         */
        isFeatureEnabled: function(name) {
            /*global disabledFeatures*/
            return !disabledFeatures[name];
        },

        /**
         * Utility function, used to determine the identifier regex for the
         * current language, by invoking {@link #getIdentifierRegex} on its handlers.
         *
         * Should not be overridden by inheritors.
         *
         * @deprecated Use worker_util#getIdentifierRegex instead
         *
         * @return {RegExp}
         */
        $getIdentifierRegex: function() {
            return null;
        },

        /**
         * Utility function, used to retrigger completion,
         * in case new information was collected and should
         * be displayed, and assuming the popup is still open.
         *
         * Should not be overridden by inheritors.
         *
         * @deprecated Use worker_util#completeUpdate instead
         *
         * @param {Object} pos   The position to retrigger this update
         * @param {String} line  The line that this update was triggered for
         */
        completeUpdate: function(pos) {
            throw new Error("Use worker_util.completeUpdate instead()"); // implemented by worker.completeUpdate
        },

        // OVERRIDABLE ACCESORS

        /**
         * Returns whether this language handler should be enabled for the given
         * file.
         *
         * Must be overridden by inheritors.
         *
         * @param {String} language   to check the handler against
         * @return {Boolean}
         */
        handlesLanguage: function(language) {
            throw new Error("base_handler.handlesLanguage() is not overridden");
        },

        /**
         * Returns whether this language handler should be used in a
         * particular kind of editor.
         *
         * May be overridden by inheritors; returns {@link #HANDLES_EDITOR}
         * by default.
         *
         * @return {Number} One of {@link #HANDLES_EDITOR},
         *                  {@link #HANDLES_IMMEDIATE}, or
         *                  {@link #HANDLES_EDITOR_AND_IMMEDIATE}, or
         *                  {@link #HANDLES_ANY}.
         */
        handlesEditor: function() {
            return this.HANDLES_EDITOR;
        },

        /**
         * Returns the maximum file size this language handler supports.
         * Should return Infinity if size does not matter.
         * Default is 10.000 lines of 80 characters.
         *
         * May be overridden by inheritors.
         *
         * @return {Number}
         */
        getMaxFileSizeSupported: function() {
            // Moderately conservative default (well, still 800K)
            return 10 * 1000 * 80;
        },

        /**
         * Determine if the language component supports parsing.
         * Assumed to be true if at least one hander for the language reports true.
         *
         * Should be overridden by inheritors.
         *
         * @return {Boolean}
         */
        isParsingSupported: function() {
            return false;
        },

        /**
         * Returns a regular expression for identifiers in the handler's language.
         * If not specified, /[A-Za-z0-9$_]/ is used.
         *
         * Note: to indicate dollars are allowed at the start of identifiers
         * (like with php $variables), include '$$'' in the regex, e.g.
         * /[A-Z0-9$$_]/.
         *
         * Should be overridden by inheritors that implement code completion.
         *
         * @return RegExp
         */
        getIdentifierRegex: function() {
            return null;
        },

        /**
         * Returns a regular expression used to trigger code completion.
         * If a non-null value is returned, it is assumed continous completion
         * is supported for this language.
         *
         * As an example, Java-like languages might want to use: /^\.$/
         *
         * Should be overridden by inheritors that implement code completion.
         * Default implementation returns null.
         *
         * @return RegExp
         */
        getCompletionRegex: function() {
            return null;
        },

        /**
         * Returns a regular expression used to trigger a tooltip.
         * Normally, tooltips after a scheduled analysis has been completed.
         * To avoid delays, this function can be used to trigger
         * analysis & tooltip fetching early.
         *
         * Should be overridden by inheritors that implement tooltips.
         * Default implementation returns null.
         *
         * @return RegExp
         */
        getTooltipRegex: function() {
            return null;
        },

        // PARSING AND ABSTRACT SYNTAX CALLBACKS

        /**
         * Parses the given document.
         *
         * Should be overridden by inheritors that implement parsing
         * (which is, like all features here, optional).
         *
         * @param value {String}   the source the document to analyze
         * @return {Object}        an abstract syntax tree (of any type), or null if not implemented
         */
        parse: function(value, callback) {
            callback();
        },

        /**
         * Finds a tree node at a certain row and column,
         * e.g. using the findNode(pos) function of treehugger.
         *
         * Should be overridden by inheritors that implement parsing.
         *
         * @param {Object} ast                An abstract syntax tree object from {@link #parse}
         * @param {Object} pos                The position of the node to look up
         * @param {Number} pos.row            The position's row
         * @param {Number} pos.column         The position's column
         * @param {Function} callback         The callback for the result
         * @param {Object} [callback.result]  The found node
         */
        findNode: function(ast, pos, callback) {
            callback();
        },

        /**
         * Returns the  a tree node at a certain row and col,
         * e.g. using the node.getPos() function of treehugger.
         *
         * Should be overridden by inheritors that implement parsing.
         *
         * @param {Object} node                The node to look up
         * @param {Function} callback          The callback for the result
         * @param {Object} [callback.result]   The resulting position
         * @param {Number} callback.result.sl  The starting line
         * @param {Number} callback.result.el  The ending line
         * @param {Number} callback.result.sc  The starting column
         * @param {Number} callback.result.ec  The ending column
         */
        getPos: function(node, callback) {
            callback();
        },

        // OTHER CALLBACKS

        /**
         * Initialize this language handler.
         *
         * May be overridden by inheritors.
         *
         * @param callback            The callback; must be called
         */
        init: function(callback) {
            callback();
        },

        /**
         * Invoked when the document has been updated (possibly after a certain delay)
         *
         * May be overridden by inheritors.
         *
         * @param {Document} doc  The current document
         * @param {Function} callback            The callback; must be called
         */
        onUpdate: function(doc, callback) {
            callback();
        },

        /**
         * Invoked when a new document has been opened.
         *
         * May be overridden by inheritors.
         *
         * @param {String} path        The path of the newly opened document
         * @param {String} doc         The Document object representing the source
         * @param {String} oldPath     The path of the document that was active before
         * @param {Function} callback  The callback; must be called
         */
        onDocumentOpen: function(path, doc, oldPath, callback) {
            callback();
        },

        /**
         * Invoked when a document is closed in the IDE.
         *
         * May be overridden by inheritors.
         *
         * @param {String} path the path of the file
         * @param {Function} callback  The callback; must be called
         */
        onDocumentClose: function(path, callback) {
            callback();
        },

        /**
         * Invoked when the cursor has been moved.
         *
         * May be overridden by inheritors that immediately act upon cursor moves.
         *
         * See {@link #tooltip} and {@link #highlightOccurrences}
         * for handler functions that are invoked after the cursor has been moved,
         * the document has been analyzed, and feedback is requested.
         *
         * @param {Document} doc                      Document object representing the source
         * @param {Object} fullAst                    The entire AST of the current file (if parsed already, otherwise null)
         * @param {Object} cursorPos                  The current cursor position
         * @param {Number} cursorPos.row              The current cursor's row
         * @param {Number} cursorPos.column           The current cursor's column
         * @param {Object} currentNode                The AST node the cursor is currently at (if parsed alreadty, and if any)
         * @param {Function} callback                 The callback; must be called
         * @paran {Object} callback.result            An optional result. Supports the same result objects as
         *                                            {@link #tooltip} and {@link #highlightOccurrences}
         */
        onCursorMove: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Invoked when the cursor has been moved inside to a different AST node.
         * Gets a tooltip to display when the cursor is moved to a particular location.
         *
         * Should be overridden by inheritors that implement tooltips.
         *
         * @param {Document} doc                               Document object representing the source
         * @param {Object} fullAst                             The entire AST of the current file (if any)
         * @param {Object} cursorPos                           The current cursor position
         * @param {Number} cursorPos.row                       The current cursor's row
         * @param {Number} cursorPos.column                    The current cursor's column
         * @param {Object} currentNode                         The AST node the cursor is currently at (if any)
         * @param {Function} callback                          The callback; must be called
         * @param {Object} callback.result                     The function's result
         * @param {Object|String} callback.result.hint         An object or HTML string with the tooltip to display
         * @param {Object[]} [callback.result.signatures]      One or more function signatures to show
         * @param {String} callback.result.signatures.name     Function name
         * @param {String} [callback.result.signatures.doc]    Function documentation
         * @param {Object[]} callback.result.signatures.parameters
         *                                                     Function parameters
         * @param {String} callback.result.signatures.parameters.name
         *                                                     Parameter name
         * @param {String} [callback.result.signatures.parameters.type]
         *                                                     Parameter type
         * @param {String} [callback.result.signatures.parameters.doc]
         *                                                     Parameter documentation
         * @param {String} [callback.result.signatures.returnType]
         *                                                     The function return type
         * @param {Object} callback.result.pos                 The position range for which this tooltip is valid
         * @param {Number} callback.result.pos.sl              The starting line
         * @param {Number} callback.result.pos.el              The ending line
         * @param {Number} callback.result.pos.sc              The starting column
         * @param {Number} callback.result.pos.ec              The ending column
         * @param {Object} [callback.result.displayPos]        The position to display this tooltip
         * @param {Number} [callback.result.displayPos.row]    The display position's row
         * @param {Number} [callback.result.displayPos.column] The display position's column
         */
        tooltip: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Gets the instances to highlight when the cursor is moved to a particular location.
         *
         * Should be overridden by inheritors that implement occurrence highlighting.
         *
         * @param {Document} doc                           Document object representing the source
         * @param {Object} fullAst                         The entire AST of the current file (if any)
         * @param {Object} cursorPos                       The current cursor position
         * @param {Number} cursorPos.row                   The current cursor's row
         * @param {Number} cursorPos.column                The current cursor's column
         * @param {Object} currentNode                     The AST node the cursor is currently at (if any)
         * @param {Function} callback                      The callback; must be called
         * @param {Object} callback.result                 The function's result
         * @param {Object[]} [callback.result.markers]     The occurrences to highlight
         * @param {Object} callback.result.markers.pos     The marker's position
         * @param {Number} callback.result.markers.pos.sl  The starting line
         * @param {Number} callback.result.markers.pos.el  The ending line
         * @param {Number} callback.result.markers.pos.sc  The starting column
         * @param {Number} callback.result.markers.pos.ec  The ending column
         * @param {Boolean} callback.result.isGeneric      Indicates this is generic highlighting and should be deferred
         * @param {"occurrence_other"|"occurrence_main"} callback.result.markers.type
         *                                                 The type of occurrence: the main one, or any other one.
         */
        highlightOccurrences: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Determines what refactorings to enable when the cursor is moved to a particular location.
         *
         * Should be overridden by inheritors that implement refactorings.
         *
         * @param {Document} doc                 Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Object} cursorPos             The current cursor position
         * @param {Number} cursorPos.row         The current cursor's row
         * @param {Number} cursorPos.column      The current cursor's column
         * @param {Object} currentNode           The AST node the cursor is currently at (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result
         * @param {String[]} callback.result.refactorings
         *                                       The refactorings to enable, such as "rename"
         * @param {String[]} [callback.result.isGeneric]
         *                                       Whether is a generic answer and should be deferred
         */
        getRefactorings: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Constructs an outline.
         *
         * Example outline object:
         *
         *     {
     *          icon: 'method',
     *          name: "fooMethod",
     *          pos: this.getPos(),
     *          displayPos: { sl: 15, sc: 20 },
     *          items: [ ...items nested under this method... ],
     *          isUnordered: true
     *     }
         *
         * Should be overridden by inheritors that implement an outline.
         *
         * @param {Document} doc                           The Document object representing the source
         * @param {Object} fullAst                         The entire AST of the current file (if any)
         * @param {Function} callback                      The callback; must be called
         * @param {Object} callback.result                 The function's result, a JSON outline structure or null if not supported
         * @param {"event"|"method"|"method2"|"package"|"property"|"property2"|"unknown"|"unknown2"} callback.result.icon
         *                                                 The icon to display for the first outline item
         * @param {String} callback.result.name            The name to display for the first outline item
         * @param {Object} callback.result.pos             The item's range, e.g. the full visible range of a method
         * @param {Number} callback.result.pos.sl          The item's starting row
         * @param {Number} [callback.result.pos.el]        The item's ending row
         * @param {Number} [callback.result.pos.sc]        The item's starting column
         * @param {Number} [callback.result.pos.ec]        The item's ending column
         * @param {Object} [callback.result.displayPos]    The item's position of the text to select when it's picked from the outline
         * @param {Number} callback.result.displayPos.sl   The item's starting row
         * @param {Number} [callback.result.displayPos.el] The item's ending row
         * @param {Number} [callback.result.displayPos.sc] The item's starting column
         * @param {Number} [callback.result.displayPos.ec] The item's ending column
         * @param {Object[]} callback.result.items         Any items nested under the curent item.
         * @param {Boolean} [callback.result.isGeneric]    Indicates that this is a generic, language-independent outline
         * @param {Boolean} [callback.result.isUnordered]  Indicates the outline is not ordered by appearance of the items,
         *                                                 but that they're e.g. grouped as methods, properties, etc.
         */
        outline: function(doc, fullAst, callback) {
            callback();
        },

        /**
         * Constructs a hierarchy.
         *
         * Should be overridden by inheritors that implement a type hierarchy.
         *
         * Not supported right now.
         *
         * @param {Document} doc             The Document object representing the source
         * @param {Object} cursorPos         The current cursor position
         * @param {Number} cursorPos.row     The current cursor's row
         * @param {Number} cursorPos.column  The current cursor's column
         * @param {Function} callback        The callback; must be called
         * @param {Object} callback.result   A JSON hierarchy structure or null if not supported
         */
        hierarchy: function(doc, cursorPos, callback) {
            callback();
        },

        /**
         * Performs code completion for the user based on the current cursor position.
         *
         * Should be overridden by inheritors that implement code completion.
         *
         * Example completion result:
         * {
     *    name        : "foo()",
     *    replaceText : "foo()",
     *    icon        : "method",
     *    meta        : "FooClass",
     *    doc         : "The foo() method",
     *    docHead     : "FooClass.foo",
     *    priority    : 1
     *  };
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Object} pos                   The current cursor position
         * @param {Number} pos.row               The current cursor's row
         * @param {Number} pos.column            The current cursor's column
         * @param {Object} currentNode           The AST node the cursor is currently at (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result, an array of completion matches
         * @param {String} callback.result.name  The full name to show in the completion popup
         * @param {String} [callback.result.id]  The short name that identifies this completion
         * @param {String} callback.result.replaceText
         *                                       The text to replace the selection with
         * @param {"event"|"method"|"method2"|"package"|"property"|"property2"|"unknown"|"unknown2"}
         *        [callback.result.icon]
         *                                       The icon to use
         * @param {String} callback.result.meta  Additional information to show
         * @param {String} callback.result.doc   Documentation to display
         * @param {String} callback.result.docHead
         *                                       Documentation heading to display
         * @param {Number} callback.result.priority
         *                                       Priority of this completion suggestion
         * @param {Boolean} callback.result.isGeneric
         *                                       Indicates that this is a generic, language-independent
         *                                       suggestion
         * @param {Boolean} callback.result.isContextual
         *                                       Indicates that this is a contextual completion,
         *                                       and that any generic completions should not be shown
         */
        complete: function(doc, fullAst, pos, currentNode, callback) {
            callback();
        },

        /**
         * Analyzes an AST or file and annotates it as desired.
         *
         * Example of an annotation to return:
         *
         *     {
     *         pos: { sl: 1, el: 1, sc: 4, ec: 5 },
     *         type: "warning",
     *         message: "Assigning to undeclared variable."
     *     }
         *
         * Should be overridden by inheritors that implement analysis.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result, an array of error and warning markers
         * @param {Boolean} [minimalAnalysis]    Fast, minimal analysis is requested, e.g.
         *                                       for code completion or tooltips.
         */
        analyze: function(value, fullAst, callback, minimalAnalysis) {
            callback();
        },

        /**
         * Gets all positions to select for a rename refactoring.
         *
         * Example result, renaming a 3-character identfier
         * on line 10 that also occurs on line 11 and 12:
         *
         *     {
     *         length: 3,
     *         pos: {
     *             row: 10,
     *             column: 5
     *         },
     *         others: [
     *             { row: 11, column: 5 },
     *             { row: 12, column: 5 }
     *         ]
     *     }
         *
         * Must be overridden by inheritors that implement rename refactoring.
         *
         * @param {Document} doc                          The Document object representing the source
         * @param {Object} ast                            The entire AST of the current file (if any)
         * @param {Object} pos                            The current cursor position
         * @param {Number} pos.row                        The current cursor's row
         * @param {Number} pos.column                     The current cursor's column
         * @param {Object} currentNode                    The AST node the cursor is currently at (if any)
         * @param {Function} callback                     The callback; must be called
         * @param {Object} callback.result                The function's result (see function description).
         * @param {Boolean} callback.result.isGeneric     Indicates this is a generic refactoring and should be deferred.
         * @param {Boolean} callback.result.length        The lenght of the rename identifier
         * @param {Object} callback.result.pos            The position of the current identifier
         * @param {Number} callback.result.pos.row        The row of the current identifier
         * @param {Number} callback.result.pos.column     The column of the current identifier
         * @param {Object[]} callback.result.others       The positions of other identifiers to be renamed
         * @param {Number} callback.result.others.row     The row of another identifier to be renamed
         * @param {Number} callback.result.others.column  The column of another identifier to be renamed
         */
        getRenamePositions: function(doc, ast, pos, currentNode, callback) {
            callback();
        },

        /**
         * Invoked when refactoring is started.
         *
         * May be overridden by inheritors that implement rename refactoring.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Function} callback            The callback; must be called
         */
        onRenameBegin: function(doc, callback) {
            callback();
        },

        /**
         * Confirms that a rename refactoring is valid, before committing it.
         *
         * May be overridden by inheritors that implement rename refactoring.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} oldId                 The old identifier was being renamed
         * @param {Number} oldId.row             The row of the identifier that was being renamed
         * @param {Number} oldId.column          The column of the identifier that was being renamed
         * @param {String} oldId.value           The value of the identifier that was being renamed
         * @param {String} newName               The new name of the element after refactoring
         * @param {Boolean} isGeneric            True if this was a refactoring marked with 'isGeneric' (see {@link #getRenamePositions})
         * @param {Function} callback            The callback; must be called
         * @param {String} callback.err          Null if the refactoring can be committed, or an error message if refactoring failed
         */
        commitRename: function(doc, oldName, newName, isGeneric, callback) {
            callback();
        },

        /**
         * Invoked when a refactor request is cancelled
         *
         * May be overridden by inheritors that implement rename refactoring.
         *
         * @param {Function} callback            The callback; must be called
         */
        onRenameCancel: function(callback) {
            callback();
        },

        /**
         * Performs code formatting.
         *
         * Should be overridden by inheritors that implement code formatting.
         *
         * @param {Document} doc the Document object representing the source
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result
         * @return a string value representing the new source code after formatting or null if not supported
         */
        codeFormat: function(doc, callback) {
            callback();
        },

        /**
         * Performs jumping to a definition.
         *
         * Should be overridden by inheritors that implement jump to definition.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Object} pos                   The current cursor position
         * @param {Number} pos.row               The current cursor's row
         * @param {Number} pos.column            The current cursor's column
         * @param {Function} callback            The callback; must be called
         * @param {Object[]} callback.results    The results
         * @param {String} [callback.results.path]
         *                                       The result path
         * @param {Number} [callback.results.row]
         *                                       The result row
         * @param {Number} [callback.results.column]
         *                                       The result column
         * @param {"event"|"method"|"method2"|"package"|"property"|"property2"|"unknown"|"unknown2"} [callback.results.icon]
         *                                       The icon to display (in case of multiple results)
         * @param {Boolean} [callback.results.isGeneric]
         *                                       Indicates that this is a generic, language-independent
         *                                       suggestion (that should be deferred)
         */
        jumpToDefinition: function(doc, fullAst, pos, currentNode, callback) {
            callback();
        },

        /**
         * Gets marker resolutions for quick fixes.
         *
         * Must be overridden by inheritors that implement quick fixes.
         *
         * See {@link #hasResolution}.
         *
         * @param {Document} doc                        The Document object representing the source
         * @param {Object} fullAst                      The entire AST of the current file (if any)
         * @param {Object} markers                      The markers to get resolutions for
         * @param {Function} callback                   The callback; must be called
         * @param {Object} callback.result              The function's result
         * @return {language.MarkerResolution[]} Resulting resolutions.
         */
        getResolutions: function(doc, fullAst, markers, callback) {
            callback();
        },

        /**
         * Determines if there are marker resolutions for quick fixes.
         *
         * Must be overridden by inheritors that implement quick fixes.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Boolean} callback.result      There is at least one resolution
         */
        hasResolution: function(doc, fullAst, marker, callback) {
            callback();
        },

        /**
         * Given the cursor position and the parsed node at that position,
         * gets the string to send to the debugger for live inspect hovering.
         *
         * Should be overridden by inheritors that implement a debugger
         * with live inspect. If not implemented, the string value based on
         * currentNode's position is used.
         *
         * @param {Document} doc                    The Document object representing the source
         * @param {Object} fullAst                  The entire AST of the current file (if any)
         * @param {Object} pos                      The current cursor position
         * @param {Number} pos.row                  The current cursor's row
         * @param {Number} pos.column               The current cursor's column
         * @param {Function} callback               The callback; must be called
         * @param {Object} callback.result          The resulting expression
         * @param {String} callback.result.value    The string representation of the expression to inspect
         * @param {Object} callback.result.pos      The expression's position
         * @param {Number} callback.result.pos.sl   The expression's starting row
         * @param {Number} callback.result.pos.el   The expression's ending row
         * @param {Number} callback.result.pos.sc   The expression's starting column
         * @param {Number} callback.result.pos.ec   The expression's ending column
         */
        getInspectExpression: function(doc, fullAst, pos, currentNode, callback) {
            callback();
        }
    };

// Mark all abstract/builtin methods for later optimization
    for (f in module.exports) {
        if (typeof module.exports[f] === "function")
            module.exports[f].base_handler = true;
    }

});

},
'xace/complete_util':function(){
/**
 * Completion utilities for language workers.
 * 
 * Import using
 * 
 *     require("plugins/c9.ide.language/complete_util")
 * 
 * @class language.complete_util
 */
define(function(require, exports, module) {

var ID_REGEX = /[a-zA-Z_0-9\$]/;
var REQUIRE_ID_REGEX = /(?!["'])./;
var staticPrefix = "../static/lib";

function retrievePrecedingIdentifier(line, offset, regex) {
    regex = regex || ID_REGEX;
    var buf = [];
    for (var i = offset-1; i >= 0 && line; i--) {
        if (regex.test(line[i]))
            buf.push(line[i]);
        else
            break;
    }
    return buf.reverse().join("");
}

function retrieveFollowingIdentifier(line, offset, regex) {
    regex = regex || ID_REGEX;
    var buf = [];
    for (var i = offset; line && i < line.length; i++) {
        if (regex.test(line[i]))
            buf.push(line[i]);
        else
            break;
    }
    return buf.join("");
}

function prefixBinarySearch(items, prefix) {
    var startIndex = 0;
    var stopIndex = items.length - 1;
    var middle = Math.floor((stopIndex + startIndex) / 2);
    
    while (stopIndex > startIndex && middle >= 0 && items[middle].indexOf(prefix) !== 0) {
        if (prefix < items[middle]) {
            stopIndex = middle - 1;
        }
        else if (prefix > items[middle]) {
            startIndex = middle + 1;
        }
        middle = Math.floor((stopIndex + startIndex) / 2);
    }
    
    // Look back to make sure we haven't skipped any
    while (middle > 0 && items[middle-1].indexOf(prefix) === 0)
        middle--;
    return middle >= 0 ? middle : 0; // ensure we're not returning a negative index
}

function findCompletions(prefix, allIdentifiers) {
    allIdentifiers.sort();
    var startIdx = prefixBinarySearch(allIdentifiers, prefix);
    var matches = [];
    for (var i = startIdx; i < allIdentifiers.length && allIdentifiers[i].indexOf(prefix) === 0; i++)
        matches.push(allIdentifiers[i]);
    return matches;
}

function fetchText(path) {
    var xhr = new XMLHttpRequest();
    var _url = staticPrefix + "/" + path;
    console.error('fetch text : ' + _url);
    xhr.open('GET', _url, false);
    try {
        xhr.send();
    }
    // Likely we got a cross-script error (equivalent with a 404 in our cloud setup)
    catch (e) {
        return false;
    }
    if (xhr.status === 200 || xhr.responseText) // when loading from file:// status is always 0
        return xhr.responseText;
    else
        return false;
}

function setStaticPrefix(url) {
    //console.error('setStaticPrefix : ' + url);
    staticPrefix = url;
}

/**
 * Determine if code completion results triggered for oldLine/oldPos
 * would still be applicable for newLine/newPos
 * (assuming you would filter them for things that no longer apply).
 */
function canCompleteForChangedLine(oldLine, newLine, oldPos, newPos, identifierRegex) {
    if (oldPos.row !== newPos.row)
        return false;
        
    if (newLine.indexOf(oldLine) !== 0)
        return false;
        
    var oldPrefix = retrievePrecedingIdentifier(oldLine, oldPos.column, identifierRegex);
    var newPrefix = retrievePrecedingIdentifier(newLine, newPos.column, identifierRegex);
    return newLine.substr(0, newLine.length - newPrefix.length) === oldLine.substr(0, oldLine.length - oldPrefix.length);
}

function precededByIdentifier(line, column, postfix, ace) {
    var id = retrievePrecedingIdentifier(line, column);
    if (postfix) id += postfix;
    return id !== "" && !(id[0] >= '0' && id[0] <= '9') 
        && (inCompletableCodeContext(line, column, id, ace) 
        || isRequireJSCall(line, column, id, ace));
}

function isRequireJSCall(line, column, identifier, ace, noQuote) {
    if (["javascript", "jsx"].indexOf(ace.getSession().syntax) === -1)
        return false;
    var id = identifier == null ? retrievePrecedingIdentifier(line, column, REQUIRE_ID_REGEX) : identifier;
    var LENGTH = 'require("'.length - (noQuote ? 1 : 0);
    var start = column - id.length - LENGTH;
    var substr = line.substr(start, LENGTH) + (noQuote ? '"' : '');

    return start >= 0 && substr.match(/require\(["']/)
        || line.substr(start + 1, LENGTH).match(/require\(["']/);
}

/**
 * Ensure that code completion is not triggered in comments and such.
 * Right now this only returns false when in a JavaScript regular expression.
 */
function inCompletableCodeContext(line, column, id, ace) {
    if (["javascript", "jsx"].indexOf(ace.getSession().syntax) === -1)
        return true;
    var isJavaScript = true;
    var inMode = null;
    for (var i = 0; i < column; i++) {
        if (line[i] === '"' && !inMode)
            inMode = '"';
        else if (line[i] === '"' && inMode === '"' && line[i-1] !== "\\")
            inMode = null;
        else if (line[i] === "'" && !inMode)
            inMode = "'";
        else if (line[i] === "'" && inMode === "'" && line[i-1] !== "\\")
            inMode = null;
        else if (line[i] === "/" && line[i+1] === "/") {
            inMode = '//';
            i++;
        }
        else if (line[i] === "/" && line[i+1] === "*" && !inMode) {
            if (line.substr(i + 2, 6) === "global")
                continue;
            inMode = '/*';
            i++;
        }
        else if (line[i] === "*" && line[i+1] === "/" && inMode === "/*") {
            inMode = null;
            i++;
        }
        else if (line[i] === "/" && !inMode && isJavaScript)
            inMode = "/";
        else if (line[i] === "/" && inMode === "/" && line[i-1] !== "\\")
            inMode = null;
    }
    return inMode != "/";
}

/**
 * @ignore
 * @return {Boolean}
 */
exports.precededByIdentifier = precededByIdentifier;

/**
 * @ignore
 */
exports.isRequireJSCall = isRequireJSCall;

/**
 * @internal Use {@link worker_util#getPrecedingIdentifier() instead.
 */
exports.retrievePrecedingIdentifier = retrievePrecedingIdentifier;

/**
 * @internal Use {@link worker_util#getFollowingIdentifier() instead. 
 */
exports.retrieveFollowingIdentifier = retrieveFollowingIdentifier;

/**
 * @ignore
 */
exports.findCompletions = findCompletions;

/**
 * @ignore
 */
exports.fetchText = fetchText;

/**
 * @ignore
 */
exports.setStaticPrefix = setStaticPrefix;

/**
 * @ignore
 */
exports.DEFAULT_ID_REGEX = ID_REGEX;

/**
 * @ignore
 */
exports.canCompleteForChangedLine = canCompleteForChangedLine;
});
},
'xace/views/Editor':function(){
/** @module xace/views/Editor **/
define([
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xaction/ActionProvider',
    'xace/views/ACEEditor',
    'xace/views/_Actions',
    'xaction/Toolbar',
    "xide/mixins/PersistenceMixin"
], function (dcl, types, utils, ActionProvider, ACEEditor, _Actions, Toolbar, PersistenceMixin) {

    var Persistence = dcl([PersistenceMixin.dcl], {
        declaredClass: 'xace.views.EditorPersistence',
        defaultPrefenceTheme: 'idle_fingers',
        defaultPrefenceFontSize: 14,
        saveValueInPreferences: true,
        getDefaultPreferences: function () {
            return utils.mixin(
                {
                    theme: this.defaultPrefenceTheme,
                    fontSize: this.defaultPrefenceFontSize
                },
                this.saveValueInPreferences ? {value: this.get('value')} : null);
        },
        onAfterAction: function (action) {
            var _theme = this.getEditor().getTheme();
            this.savePreferences({
                theme: _theme.replace('ace/theme/', ''),
                fontSize: this.getEditor().getFontSize()
            });
            return this.inherited(arguments);
        },
        /**
         * Override id for pref store:
         * know factors:
         *
         * - IDE theme
         * - per bean description and context
         * - by container class string
         * - app / plugins | product / package or whatever this got into
         * -
         **/
        toPreferenceId: function (prefix) {
            prefix = prefix;
            if (!prefix) {
                var body = $('body'), prefix = 'xTheme-', search;
                _.each(['blue', 'gray', 'white', 'transparent'], function (theme) {
                    search = prefix + theme
                    if (body.hasClass(search)) {
                        prefix = search;
                    }
                });
            }
            return (prefix || this.cookiePrefix || '') + '_xace';
        },
        getDefaultOptions: function () {
            //take our defaults, then mix with prefs from store,
            var _super = this.inherited(arguments),
                _prefs = this.loadPreferences(null);
            (_prefs && utils.mixin(_super, _prefs) ||
            //else store defaults
            this.savePreferences(this.getDefaultPreferences()));
            return _super;
        }
    });
    /**
     * Default Editor with all extras added : Actions, Toolbar and ACE-Features
     @class module:xgrid/Base
     */
    var Module = dcl([_Actions, ACEEditor, ActionProvider.dcl, Persistence, Toolbar.dcl], {
        toolbarArgs:{
            actionFilter:{}
        },
        getBreadcrumbPath: function () {
            if (this.item) {
                return {
                    path: utils.replaceAll('/', '', this.item.mount) + ':/' + this.item.path.replace('./', '/')
                }
            }
        },
        tabOrder: {
            'Home': 100,
            'View': 50,
            'Settings': 20
        },
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Editor': 9,
            'Step': 5,
            'New': 4,
            'Window': 3,
            'Help': 2
        },
        declaredClass: 'xace/views/Editor',
        options: null,
        /**
         * The icon class when doing any storage operation
         * @member loadingIcon {string}
         */
        loadingIcon: 'fa-spinner fa-spin',
        /**
         * The original icon class
         * @member iconClassNormal {string}
         */
        iconClassNormal: 'fa-code',
        templateString: '<div attachTo="template" class="grid-template widget" style="width: 100%;height: 100%;overflow: hidden !important;position: relative;padding: 0px;margin: 0px">' +
        '<div attachTo="header" class="view-header row bg-opaque" style="height: auto;width:inherit;height:auto;min-height: 33px"></div>' +
        '<div attachTo="aceNode" class="view-body row" style="height:100%;width: 100%;position: relative;"></div>' +
        '<div attachTo="footer" class="view-footer" style="position: absolute;bottom: 0px;width: 100%"></div></div>',
        getContent: function (item, onSuccess, onError) {
            if (!this.storeDelegate) {
                onError && onError('Editor::getContent : Have no store delegate!');
            } else {
                this.storeDelegate.getContent(function (content) {
                    onSuccess(content);
                }, item || this.item);
            }
        },
        saveContent: function (value, onSuccess, onError) {
            var thiz = this;
            this.set('iconClass', 'fa-spinner fa-spin');
            var _value = value || this.get('value');
            if (!_value) {
                console.warn('Editor::saveContent : Have nothing to save, editor seems empty');
            }
            if (!this.storeDelegate) {
                if (onError) {
                    onError('Editor::saveContent : Have no store delegate!');
                }
                return false;
            } else {
                return this.storeDelegate.saveContent(_value, function () {
                    thiz.set('iconClass', thiz.iconClassNormal);
                    thiz.lastSavedContent = _value;
                    thiz.onContentChange(_value);
                    var struct = {
                        path: thiz.options.filePath,
                        item: thiz.item,
                        content: _value,
                        editor: thiz
                    };
                    thiz.publish(types.EVENTS.ON_FILE_CONTENT_CHANGED, struct, thiz);
                }, null, thiz.item);
            }
        },
        addCommands: function () {
            var aceEditor = this.getAce(),
                thiz = this;

            var config = ace.require("ace/config");
            config.init();
            var commands = [];
            if (this.hasHelp) {
                commands.push({
                    name: "showKeyboardShortcuts",
                    bindKey: {win: "Ctrl-Alt-h", mac: "Command-Alt-h"},
                    exec: function (editor) {
                        thiz.showHelp(editor);
                    }
                });
            }
            if (this.hasConsole) {
                commands.push({
                    name: "gotoline",
                    bindKey: {win: "Ctrl-L", mac: "Command-L"},
                    exec: function (editor, line) {
                        if (typeof line == "object") {
                            var arg = this.name + " " + editor.getCursorPosition().row;
                            editor.cmdLine.setValue(arg, 1);
                            editor.cmdLine.focus();
                            return;
                        }
                        line = parseInt(line, 10);
                        if (!isNaN(line))
                            editor.gotoLine(line);
                    },
                    readOnly: true
                });
            }
            aceEditor.commands.addCommands(commands);
        },
        getWebRoot: function () {
            return this.ctx.getResourceManager().getVariable(types.RESOURCE_VARIABLES.APP_URL);
        },
        resize: function () {
            return this._resize();
        },
        onResize: function () {
            return this._resize();
        },
        _resize: function () {
            var parent = this.getParent();
            if (!this._isMaximized) {
                parent && utils.resizeTo(this, parent, true, true);
            } else {
                utils.resizeTo(this, this._maximizeContainer, true, true);
            }

            var thiz = this,
                toolbar = this.getToolbar(),
                noToolbar = false,
                topOffset = 0,
                aceNode = $(this.aceNode);

            if (this._isMaximized && toolbar) {
                utils.resizeTo(toolbar, this.header, true, true);
            }

            if (!toolbar || (toolbar && toolbar.isEmpty())) {
                noToolbar = true;
            } else {
                if (toolbar) {
                    utils.resizeTo(toolbar, this.header, false,true);
                    toolbar.resize();
                    utils.resizeTo(this.header,toolbar,true,false);
                }
            }

            var totalHeight = $(thiz.domNode).height(),
                topHeight = noToolbar == true ? 0 : $(thiz.header).height(),
                footerHeight = $(thiz.footer).height(),
                finalHeight = totalHeight - topHeight - footerHeight;

            if (toolbar) {
                finalHeight -= 4;
            }
            if (finalHeight > 50) {
                aceNode.height(finalHeight + 'px');
            } else {
                aceNode.height('inherited');
            }
        },
        __set: function (what, value) {
            var _res = this.inherited(arguments);
            if (what === 'iconClass') {
                var _parent = this._parent;
                if (_parent && _parent.icon) {
                    this._parent.icon(value);
                }
            }
            return _res;
        },
        __get: function (what) {
            if (what === 'value') {
                var self = this,
                    editor = self.getEditor(),
                    session = editor ? editor.session : null;
                return session ? session.getValue() : null;
            }
            return this.inherited(arguments);
        }
    });

    //pass through defaults
    Module.DEFAULT_PERMISSIONS = _Actions.DEFAULT_PERMISSIONS;

    return Module;

});
},
'xace/views/_Actions':function(){
/** @module xace/views/Editor **/
define([
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'xide/types/Types', //  <--important for build
    'xaction/types', //  <--important for build
    'xaction/ActionProvider',
    'xace/views/ACEEditor',
    'xaction/Toolbar',
    'xaction/DefaultActions',
    'dojo/Deferred',
    'xace/formatters'
], function (dcl, utils, types, Types,aTypes,ActionProvider,ACEEditor,Toolbar, DefaultActions,Deferred,formatters) {

        var ACTION = types.ACTION,
        EDITOR_SETTINGS = 'Editor/Settings',
        INCREASE_FONT_SIZE = 'View/Increase Font Size',
        DECREASE_FONT_SIZE = 'View/Decrease Font Size',
        EDITOR_HELP = 'Help/Editor Shortcuts',
        EDITOR_THEMES = 'View/Themes',
        SNIPPETS = 'Editor/Snippets',
        EDITOR_CONSOLE = 'Editor/Console',
        KEYBOARD = 'Editor/Keyboard',
        LAYOUT = 'View/Layout',
        FORMAT = 'Edit/Format',
        SPLIT_MODE = types.VIEW_SPLIT_MODE,
        DEFAULT_PERMISSIONS = [
            ACTION.RELOAD,
            ACTION.SAVE,
            ACTION.FIND,
            ACTION.TOOLBAR,
            KEYBOARD,
            INCREASE_FONT_SIZE,
            DECREASE_FONT_SIZE,
            EDITOR_THEMES,
            'Help/Editor Shortcuts',
            SNIPPETS,
            EDITOR_CONSOLE,
            EDITOR_SETTINGS,
            ACTION.FULLSCREEN,
            LAYOUT,
            FORMAT
        ];
    /**
     * Default Editor with all extras added : Actions, Toolbar and ACE-Features
     @class module:xgrid/Base
     */
    var Module = dcl([ACEEditor, Toolbar.dcl, ActionProvider.dcl], {
            permissions: DEFAULT_PERMISSIONS,
            _searchBoxOpen: false,
            onSingleView: function () {

            },
            setSplitMode: function (mode) {
                this.splitMode = mode;
                if (!this.doSplit) {
                    if (mode == 'Diff') {
                        this.doDiff();
                        return;
                    }
                    var isSplit = mode == SPLIT_MODE.SPLIT_HORIZONTAL || mode == SPLIT_MODE.SPLIT_VERTICAL;
                    var _ed = this.getEditor();
                    var sp = this.split;
                    if (isSplit) {
                        var newEditor = (sp.getSplits() == 1);
                        sp.setOrientation(mode == SPLIT_MODE.SPLIT_HORIZONTAL ? sp.BELOW : sp.BESIDE);
                        sp.setSplits(2);
                        if (newEditor) {
                            var session = sp.getEditor(0).session;
                            var newSession = sp.setSession(session, 1);
                            newSession.name = session.name;
                            var options = _ed.getOptions();
                            sp.getEditor(1).setOptions(options);
                        }
                    } else {
                        sp.setSplits(1);
                        this.onSingleView();
                    }
                }
            },
            onMaximized: function (maximized) {
                var parent = this.getParent();
                if (maximized === false) {
                    if (parent && parent.resize) {
                        parent.resize();
                    }
                }
                var toolbar = this.getToolbar();
                if (toolbar) {
                    if (maximized) {
                        $(toolbar.domNode).addClass('bg-opaque');
                    } else {
                        $(toolbar.domNode).removeClass('bg-opaque');
                    }
                }
                if (maximized === false) {
                    this.resize();
                    parent && utils.resizeTo(this, parent, true, true);
                    this.publish(types.EVENTS.ON_VIEW_MAXIMIZE_END);
                }
                this.getEditor().focus();
            },
            maximize: function () {
                var node = this.domNode,
                    $node = $(node),
                    _toolbar = this.getToolbar();

                if (!this._isMaximized) {
                    this.publish(types.EVENTS.ON_VIEW_MAXIMIZE_START);
                    this._isMaximized = true;
                    var vp = $(this.domNode.ownerDocument);
                    var root = $('body')[0];
                    var container = utils.create('div', {
                        className: 'ACEContainer bg-opaque',
                        style: 'z-index:300;height:100%;width:100%'
                    });

                    this._maximizeContainer = container;
                    root.appendChild(container);
                    $(node).addClass('AceEditorPaneFullScreen');
                    $(node).css('width', vp.width());
                    $(node).css('height', vp.height());
                    this.resize();
                    this._lastParent = node.parentNode;
                    container.appendChild(node);
                    $(container).addClass('bg-opaque');
                    $(container).css('width', vp.width());
                    $(container).css('height', vp.height());
                    $(container).css({
                        position: "absolute",
                        left: "0px",
                        top: "0px",
                        border: 'none medium',
                        width: '100%',
                        height: '100%'
                    });

                } else {
                    this._isMaximized = false;
                    $node.removeClass('AceEditorPaneFullScreen');
                    this._lastParent.appendChild(node);
                    utils.destroy(this._maximizeContainer);
                }
                this.onMaximized(this._isMaximized);
                return true;
            },
            save: function (item) {
                var value = this.get('value');
                var res = this.saveContent(this.get('value'), item);
                var thiz = this;
                this._emit(types.EVENTS.ON_FILE_CONTENT_CHANGED,{
                    content:value,
                    item:item
                });
                setTimeout(function () {
                    var _ed = thiz.getEditor();
                    if (_ed) {
                        _ed.focus();
                    }
                }, 600);
                return res;
            },
            reload:function(){
                var self = this;
                var dfd = new Deferred();
                this.getContent(
                    this.item,
                    function (content) {//onSuccess
                        self.lastSavedContent = content;
                        self.set('value',content);
                        dfd.resolve(content);
                    },
                    function (e) {//onError
                        logError(e, 'error loading content from file');
                        dfd.reject(e);
                    }
                );
                return dfd;
            },
            runAction: function (action) {
                action = this.getAction(action);
                if (!action) {
                    return false;
                }

                var self = this,
                    command = action.command,
                    ACTION = types.ACTION,
                    editor = this.getEditor(),
                    session = this.editorSession,
                    result = false;

                if (command.indexOf(LAYOUT) != -1) {
                    self.setSplitMode(action.option, null);
                }

                switch (command) {
                    case ACTION.RELOAD:
                    {
                        return this.reload();
                    }
                    case INCREASE_FONT_SIZE:
                    {
                        editor.setFontSize(editor.getFontSize() + 1);
                        return true;
                    }
                    case DECREASE_FONT_SIZE:
                    {
                        editor.setFontSize(editor.getFontSize() - 1);
                        return true;
                    }
                    case ACTION.FULLSCREEN:
                    {
                        return this.maximize();
                    }
                    case EDITOR_HELP:
                    {
                        self.showHelp();
                        break;
                    }
                    case ACTION.SAVE:
                    {
                        result = self.save(this.item);
                        break;
                    }
                    case ACTION.FIND:
                    {
                        var net = ace.require("ace/lib/net");
                        var webRoot = this.getWebRoot();
                        var sb = editor.searchBox;
                        function _search(sb) {
                            var shown = self._searchBoxOpen;
                            if (!shown) {
                                sb.show(editor.session.getTextRange(), null);
                                self._searchBoxOpen = true;
                            } else {
                                sb.hide();
                                self._searchBoxOpen = false;
                            }
                        }
                        if (sb) {
                            _search(sb);
                        } else {
                            net.loadScript(webRoot + '/xfile/ext/ace/ext-searchbox.js', function (what) {
                                var sbm = ace.require("ace/ext/searchbox");
                                _search(new sbm.SearchBox(editor));
                            });
                        }
                        return true;
                    }
                }

                //themes
                if (command.indexOf(EDITOR_THEMES) !==-1) {
                    self.set('theme', action.theme);
                    var parentAction = action.getParent ?  action.getParent() : null;
                    //action._originEvent = 'change';
                    if(parentAction) {
                        var rendererActions = parentAction.getChildren();
                        _.each(rendererActions, function (child) {
                            child.set('icon', child._oldIcon);
                        });
                    }
                    action.set('icon', 'fa fa-check');
                }
                //formatters :
                if (command.indexOf(FORMAT) !==-1) {
                     if (editor) {
                         var _value = formatters.format(editor, action.formatter);
                         self.set('value',_value);
                     }
                }
                /*
                if (command.indexOf(KEYBOARD) !==-1) {
                    var option = action.option,
                        keybindings = {
                        ace: null, // Null = use "default" keymapping
                        vim: ace.require("ace/keyboard/vim").handler,
                        emacs: "ace/keyboard/emacs"
                    };
                    editor.setKeyboardHandler(keybindings[action.option]);
                    return true;
                }
                */

                if (command.indexOf(EDITOR_SETTINGS) !==-1) {
                    var key = action.option,
                        option = editor.getOption(action.option),
                        isBoolean = _.isBoolean(option);
                    if (key === 'highlightActive') {
                        editor.setHighlightActiveLine(!editor.getHighlightActiveLine());
                        return;
                    }
                    if (isBoolean) {
                        editor.setOption(action.option, !option);
                    } else {
                        if (key === 'wordWrap') {
                            var mode = session.getUseWrapMode();
                            this.set('wordWrap', !mode);
                            return true;
                        }
                        if (option === 'off' || option === 'on') {
                            editor.setOption(key, option === 'off' ? 'on' : 'off');
                        } else {
                            editor.setOption(action.option, false);
                        }
                    }
                    return true;
                }

                return this.inherited(arguments);
            },
            getEditorActions: function (permissions) {

                var actions = [],
                    self = this,
                    ACTION = types.ACTION,
                    ICON = types.ACTION_ICON;

                /* @TODO: reactivate reload action
                actions.push(this.createAction({
                    label: 'Reload',
                    command: ACTION.RELOAD,
                    icon: ICON.RELOAD,
                    keycombo: 'ctrl r'
                }));
                */
                actions.push(this.createAction({
                    label: 'Save',
                    command: ACTION.SAVE,
                    icon: ICON.SAVE,
                    keycombo: 'ctrl s',
                    group: 'File'
                }));

                actions.push(this.createAction({
                    label: 'Find',
                    command: ACTION.FIND,
                    icon: ICON.SEARCH,
                    keycombo: 'ctrl f',
                    group: 'Search'
                }));

                actions.push(this.createAction({
                    label: 'Fullscreen',
                    command: ACTION.FULLSCREEN,
                    icon: ICON.MAXIMIZE,
                    keycombo: 'ctrl f11',
                    group: 'View'
                }));


                actions.push(this.createAction({
                    label: 'Increase Fontsize',
                    command: INCREASE_FONT_SIZE,
                    icon: 'fa-text-height',
                    group: 'View'
                }));

                actions.push(this.createAction({
                    label: 'Decrease Fontsize',
                    command: DECREASE_FONT_SIZE,
                    icon: 'fa-text-height',
                    group: 'View'
                }));

                if (DefaultActions.hasAction(permissions, EDITOR_THEMES)) {
                    actions.push(this.createAction({
                        label: 'Themes',
                        command: EDITOR_THEMES,
                        icon: 'fa-paint-brush',
                        group: 'View',
                        mixin:{
                            closeOnClick:false,
                            value:this.defaultPrefenceTheme
                        },
                        onCreate:function(action){
                            var options = self.getDefaultOptions();
                            action.set('value',options.theme);
                        }
                    }));

                    self._addThemes && self._addThemes(actions);
                }

                actions.push(this.createAction({
                    label: 'Help',
                    command: EDITOR_HELP,
                    icon: 'fa-question',
                    keycombo: 'f1'
                }));

                ///editor settings
                actions.push(this.createAction({
                    label: 'Settings',
                    command: EDITOR_SETTINGS,
                    icon: 'fa-cogs',
                    group: "Settings"
                }));

                function _createSettings(label, command, icon, option, mixin, group, actionType, params) {
                    command = command || EDITOR_SETTINGS + '/' + label;
                    mixin = mixin || {};
                    command = command || EDITOR_SETTINGS + '/' + label;
                    mixin = mixin || {};
                    var action = self.createAction(utils.mixin({
                        label: label,
                        command: command,
                        icon: icon || 'fa-cogs',
                        group: group || "Settings",
                        mixin: utils.mixin({
                            addPermission: true,
                            option: option,
                            actionType: actionType,
                            owner: self
                        }, mixin)
                    }, params));
                    actions.push(action);
                    return action;
                }
                var _params = {
                    onCreate: function (action) {
                        var optionValue = self.getOptionsMixed()[this.option];
                        if (optionValue !== null) {
                            action.set('value', optionValue);
                        }
                    },
                    onChange: function (property, value) {
                        this.value = value;
                        self.runAction(this);
                    }
                };


                _createSettings('Show Gutters', null, null, 'showGutter', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Show Print Margin', null, null, 'showPrintMargin', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Display Intend Guides', null, null, 'displayIndentGuides', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Show Line Numbers', null, null, 'showLineNumbers', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Show Indivisibles', null, null, 'showInvisibles', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Use Soft Tabs', null, null, 'useSoftTabs', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Use Elastic Tab Stops', null, null, 'useElasticTabstops', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                //_createSettings('Use Elastic Tab Stops', null, null, 'useElasticTabstops');
                _createSettings('Animated Scroll', null, null, 'animatedScroll', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Word Wrap', null, null, 'wordWrap', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Highlight Active Line', null, null, 'highlightActive', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);

                /*
                 var keybindings = {
                 ace: null, // Null = use "default" keymapping
                 vim: ace.require("ace/keyboard/vim").handler,
                 emacs: "ace/keyboard/emacs"
                 };
                 */

                /*
                 actions.push(this.createAction({
                 label: 'Keyboard',
                 command: KEYBOARD,
                 icon: 'fa-keyboard-o',
                 group: "Settings"
                 }));

                 if(DefaultActions.hasAction(permissions,KEYBOARD)){
                 _createSettings('Default', KEYBOARD + '/Default', null, 'ace');
                 _createSettings('Vim', KEYBOARD + '/Vim', null, 'vim');
                 _createSettings('EMacs', KEYBOARD + '/EMacs', null, 'emacs');
                 }
                 */
                var VISIBILITY = types.ACTION_VISIBILITY;
                if (DefaultActions.hasAction(permissions, FORMAT)) {
                    actions.push(this.createAction({
                        label: 'Format',
                        command: 'Edit/Format',
                        icon: 'fa-indent',
                        group: "Edit"
                    }));

                    var modes = formatters.modes;
                    var creatorFn = function (label, icon, value) {

                        var head = self.createAction({
                            label: label,
                            command: 'Edit/Format/'+label,
                            icon: 'fa-indent',
                            group: "Edit",
                            mixin:{
                                addPermission: true,
                                formatter:value
                            },
                            onCreate:function(action){
                                /*
                                action.setVisibility(VISIBILITY.ACTION_TOOLBAR, {label: ''}).
                                setVisibility(VISIBILITY.MAIN_MENU, {show: false}).
                                setVisibility(VISIBILITY.CONTEXT_MENU, null);*/
                            }
                        });

                        actions.push(head);

                        /*
                        return Action.create(label, icon, 'Edit/Format/' + label, false, null, 'TEXT', 'viewActions', null, false, function () {
                            formatCode(value);
                        });
                        */
                    };

                    for (var _f in modes) {
                        actions.push(creatorFn(modes[_f], '', _f));
                    }
                    /*
                    var format = Action.createDefault('Format', 'fa-indent', 'Edit/Format', '_a', null, {
                        dummy: true
                    }).setVisibility(VISIBILITY.ACTION_TOOLBAR, {label: ''}).
                    setVisibility(VISIBILITY.MAIN_MENU, {show: false}).
                    setVisibility(VISIBILITY.CONTEXT_MENU, null);

                    this.addAction(actions,format);

                    for (var _f in modes) {
                        actions.push(creatorFn(modes[_f], '', _f));
                    }
                    */


                    /*

                    //layout
                    actions.push(_createSettings('None', 'View/Layout/None', 'fa-columns', SPLIT_MODE.SOURCE, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Horizontal', 'View/Layout/Horizontal', 'layoutIcon-horizontalSplit', SPLIT_MODE.SPLIT_HORIZONTAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Vertical', 'View/Layout/Vertical', 'layoutIcon-layout293', SPLIT_MODE.SPLIT_VERTICAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    */
                    //actions.push(_createSettings('Diff', 'View/Layout/Diff', 'fa-columns', 'Diff', null, 'View'));
                }

                if (DefaultActions.hasAction(permissions, LAYOUT)) {
                    actions.push(this.createAction({
                        label: 'Split',
                        command: 'View/Layout',
                        icon: 'fa-columns',
                        group: "View"
                    }));
                    //layout
                    actions.push(_createSettings('None', 'View/Layout/None', 'fa-columns', SPLIT_MODE.SOURCE, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Horizontal', 'View/Layout/Horizontal', 'layoutIcon-horizontalSplit', SPLIT_MODE.SPLIT_HORIZONTAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Vertical', 'View/Layout/Vertical', 'layoutIcon-layout293', SPLIT_MODE.SPLIT_VERTICAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    //actions.push(_createSettings('Diff', 'View/Layout/Diff', 'fa-columns', 'Diff', null, 'View'));
                }
                return actions;
            },
            _addThemes: function (actions) {
                var themes = this.getThemeData(),
                    thiz = this;

                var creatorFn = function (label, icon, value) {
                    return thiz.createAction({
                        label: label,
                        command: EDITOR_THEMES + '/' + label,
                        group: 'View',
                        icon: icon,
                        mixin: {
                            addPermission: true,
                            value:value,
                            theme: value,
                            closeOnClick:false
                        },
                        onCreate:function(action) {
                            action._oldIcon = icon;
                            action.set('value', value);
                            action.actionType = types.ACTION_TYPE.SINGLE_TOGGLE;
                        }
                    });
                };

                //clean and complete theme data
                for (var i = 0; i < themes.length; i++) {
                    var data = themes[i];
                    var name = data[1] || data[0].replace(/ /g, "_").toLowerCase();
                    var theme = creatorFn(data[0], ' ', name);//@TODO: _MenuMixin not creating icon node, use white space for now
                    actions.push(theme);
                }
            },
            showHelp: function (editor) {
                editor = editor || this.getEditor();
                var config = ace.require("ace/config");
                config.loadModule("ace/ext/keybinding_menu", function (module) {
                    module.init(editor);
                    editor.showKeyboardShortcuts();
                });
            },
            getThemeData: function () {
                return [
                    ["Chrome"],
                    ["Clouds"],
                    ["Crimson Editor"],
                    ["Dawn"],
                    ["Dreamweaver"],
                    ["Eclipse"],
                    ["GitHub"],
                    ["Solarized Light"],
                    ["TextMate"],
                    ["Tomorrow"],
                    ["XCode"],
                    ["Kuroir"],
                    ["KatzenMilch"],
                    ["Ambiance", "ambiance", "dark"],
                    ["Day", "cloud9_day"],
                    ["Night", "cloud9_night"],
                    ["Chaos", "chaos", "dark"],
                    ["Midnight", "clouds_midnight", "dark"],
                    ["Cobalt", "cobalt", "dark"],
                    ["idle Fingers", "idle_fingers", "dark"],
                    ["krTheme", "kr_theme", "dark"],
                    ["Merbivore", "merbivore", "dark"],
                    ["Merbivore-Soft", "merbivore_soft", "dark"],
                    ["Mono Industrial", "mono_industrial", "dark"],
                    ["Monokai", "monokai", "dark"],
                    ["Pastel on dark", "pastel_on_dark", "dark"],
                    ["Solarized Dark", "solarized_dark", "dark"],
                    ["Terminal", "terminal", "dark"],
                    ["Tomorrow-Night", "tomorrow_night", "dark"],
                    ["Tomorrow-Night-Blue", "tomorrow_night_blue", "dark"],
                    ["Tomorrow-Night-Bright", "tomorrow_night_bright", "dark"],
                    ["Tomorrow-Night-80s", "tomorrow_night_eighties", "dark"],
                    ["Twilight", "twilight", "dark"],
                    ["Vibrant Ink", "vibrant_ink", "dark"]
                ];
            }
        }
    );
    Module.DEFAULT_PERMISSIONS = DEFAULT_PERMISSIONS;
    dcl.chainAfter('runAction',Module);
    return Module;
});
},
'xace/formatters':function(){
define([
    'xace/lib_jsbeautify'
    ], function (jsbeautify) {


    function formatCode(editor, mode){
        if (this.disabled === true)
            return;

        var ace = editor;
        var sel = ace.selection;
        var session = ace.session;
        var range = sel.getRange();

        /*
        session.diffAndReplace = function(range, text) {
            var doc = this.doc;
            var start = doc.positionToIndex(range.start);
            var oldText = doc.getTextRange(range);
            merge.patchAce(oldText, text, doc, {
                offset: start,
                method: "quick"
            });
            var dl = text.replace(/\r\n|\r|\n/g, doc.getNewLineCharacter()).length;
            return doc.indexToPosition(start + dl);
        };*/

        // Load up current settings data
        /*
         var options = {
         space_before_conditional: settings.getBool("user/format/jsbeautify/@space_before_conditional"),
         keep_array_indentation: settings.getBool("user/format/jsbeautify/@keeparrayindentation"),
         preserve_newlines: settings.getBool("user/format/jsbeautify/@preserveempty"),
         unescape_strings: settings.getBool("user/format/jsbeautify/@unescape_strings"),
         jslint_happy: settings.getBool("user/format/jsbeautify/@jslinthappy"),
         brace_style: settings.get("user/format/jsbeautify/@braces")
         };
         */

        var options = {
            space_before_conditional: true,
            keep_array_indentation: false,
            preserve_newlines: true,
            unescape_strings: true,
            jslint_happy: false,
            brace_style: "end-expand"
        };
        var useSoftTabs = true;
        if (useSoftTabs) {
            options.indent_char = " ";
            options.indent_size = session.getTabSize();
        } else {
            options.indent_char = "\t";
            options.indent_size = 1;
        }

        var line = session.getLine(range.start.row);
        var indent = line.match(/^\s*/)[0];
        var trim = false;

        if (range.start.column < indent.length)
            range.start.column = 0;
        else
            trim = true;

        var value = session.getTextRange(range);
        if(value.length==0){
            value = session.getValue();
        }
        var type = null;

        if (mode == "javascript" || mode == "json") {
            type = "js";
        } else if (mode == "css" || mode == "less"){
            type = "css";
        } else if (/^\s*<!?\w/.test(value)) {
            type = "html";
        } else if (mode == "xml") {
            type = "html";
        } else if (mode == "html") {
            if (/[^<]+?{[\s\-\w]+:[^}]+;/.test(value))
                type = "css";
            else if (/<\w+[ \/>]/.test(value))
                type = "html";
            else
                type = "js";
        } else if (mode == "handlebars") {
            options.indent_handlebars = true;
            type = "html";
        }


        try {
            value = jsbeautify[type + "_beautify"](value, options);
            if (trim)
                value = value.replace(/^/gm, indent).trim();
            if (range.end.column === 0)
                value += "\n" + indent;
        }
        catch (e) {
            return false;
        }

        //var end = session.diffAndReplace(range, value);

        //sel.setSelectionRange(Range.fromPoints(range.start, end));

        return value;
    }

    return {
        format:formatCode,
        modes : {
            "javascript" : "Javascript (JS Beautify)",
            "html"       : "HTML (JS Beautify)",
            "css"        : "CSS (JS Beautify)",
            "less"       : "Less (JS Beautify)",
            "xml"        : "XML (JS Beautify)",
            "json"       : "JSON (JS Beautify)",
            "handlebars" : "Handlebars (JS Beautify)"
        }
    }
});
},
'xace/lib_jsbeautify':function(){
define(['require', 'exports', 'module'], function(_r, _e, module) {
var define, window = module.exports = {};
/*jshint curly:true, eqeqeq:true, laxbreak:true, noempty:false */
/*

  The MIT License (MIT)

  Copyright (c) 2007-2013 Einar Lielmanis and contributors.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

 JS Beautifier
---------------


  Written by Einar Lielmanis, <einar@jsbeautifier.org>
      http://jsbeautifier.org/

  Originally converted to javascript by Vital, <vital76@gmail.com>
  "End braces on own line" added by Chris J. Shull, <chrisjshull@gmail.com>
  Parsing improvements for brace-less statements by Liam Newman <bitwiseman@gmail.com>


  Usage:
    js_beautify(js_source_text);
    js_beautify(js_source_text, options);

  The options are:
    indent_size (default 4)          - indentation size,
    indent_char (default space)      - character to indent with,
    preserve_newlines (default true) - whether existing line breaks should be preserved,
    max_preserve_newlines (default unlimited) - maximum number of line breaks to be preserved in one chunk,

    jslint_happy (default false) - if true, then jslint-stricter mode is enforced.

            jslint_happy        !jslint_happy
            ---------------------------------
            function ()         function()

            switch () {         switch() {
            case 1:               case 1:
              break;                break;
            }                   }

    space_after_anon_function (default false) - should the space before an anonymous function's parens be added, "function()" vs "function ()",
          NOTE: This option is overriden by jslint_happy (i.e. if jslint_happy is true, space_after_anon_function is true by design)

    brace_style (default "collapse") - "collapse" | "expand" | "end-expand" | "none"
            put braces on the same line as control statements (default), or put braces on own line (Allman / ANSI style), or just put end braces on own line, or attempt to keep them where they are.

    space_before_conditional (default true) - should the space before conditional statement be added, "if(true)" vs "if (true)",

    unescape_strings (default false) - should printable characters in strings encoded in \xNN notation be unescaped, "example" vs "\x65\x78\x61\x6d\x70\x6c\x65"

    wrap_line_length (default unlimited) - lines should wrap at next opportunity after this number of characters.
          NOTE: This is not a hard limit. Lines will continue until a point where a newline would
                be preserved if it were present.

    end_with_newline (default false)  - end output with a newline


    e.g

    js_beautify(js_source_text, {
      'indent_size': 1,
      'indent_char': '\t'
    });

*/

(function() {

    var acorn = {};
    (function (exports) {
      // This section of code is taken from acorn.
      //
      // Acorn was written by Marijn Haverbeke and released under an MIT
      // license. The Unicode regexps (for identifiers and whitespace) were
      // taken from [Esprima](http://esprima.org) by Ariya Hidayat.
      //
      // Git repositories for Acorn are available at
      //
      //     http://marijnhaverbeke.nl/git/acorn
      //     https://github.com/marijnh/acorn.git

      // ## Character categories

      // Big ugly regular expressions that match characters in the
      // whitespace, identifier, and identifier-start categories. These
      // are only applied when a character is found to actually have a
      // code point above 128.

      var nonASCIIwhitespace = /[\u1680\u180e\u2000-\u200a\u202f\u205f\u3000\ufeff]/;
      var nonASCIIidentifierStartChars = "\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc";
      var nonASCIIidentifierChars = "\u0300-\u036f\u0483-\u0487\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u0620-\u0649\u0672-\u06d3\u06e7-\u06e8\u06fb-\u06fc\u0730-\u074a\u0800-\u0814\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0840-\u0857\u08e4-\u08fe\u0900-\u0903\u093a-\u093c\u093e-\u094f\u0951-\u0957\u0962-\u0963\u0966-\u096f\u0981-\u0983\u09bc\u09be-\u09c4\u09c7\u09c8\u09d7\u09df-\u09e0\u0a01-\u0a03\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a66-\u0a71\u0a75\u0a81-\u0a83\u0abc\u0abe-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ae2-\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b3c\u0b3e-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5f-\u0b60\u0b66-\u0b6f\u0b82\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62-\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0cbc\u0cbe-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0ce2-\u0ce3\u0ce6-\u0cef\u0d02\u0d03\u0d46-\u0d48\u0d57\u0d62-\u0d63\u0d66-\u0d6f\u0d82\u0d83\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e34-\u0e3a\u0e40-\u0e45\u0e50-\u0e59\u0eb4-\u0eb9\u0ec8-\u0ecd\u0ed0-\u0ed9\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f41-\u0f47\u0f71-\u0f84\u0f86-\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u1029\u1040-\u1049\u1067-\u106d\u1071-\u1074\u1082-\u108d\u108f-\u109d\u135d-\u135f\u170e-\u1710\u1720-\u1730\u1740-\u1750\u1772\u1773\u1780-\u17b2\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u1920-\u192b\u1930-\u193b\u1951-\u196d\u19b0-\u19c0\u19c8-\u19c9\u19d0-\u19d9\u1a00-\u1a15\u1a20-\u1a53\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1b46-\u1b4b\u1b50-\u1b59\u1b6b-\u1b73\u1bb0-\u1bb9\u1be6-\u1bf3\u1c00-\u1c22\u1c40-\u1c49\u1c5b-\u1c7d\u1cd0-\u1cd2\u1d00-\u1dbe\u1e01-\u1f15\u200c\u200d\u203f\u2040\u2054\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2d81-\u2d96\u2de0-\u2dff\u3021-\u3028\u3099\u309a\ua640-\ua66d\ua674-\ua67d\ua69f\ua6f0-\ua6f1\ua7f8-\ua800\ua806\ua80b\ua823-\ua827\ua880-\ua881\ua8b4-\ua8c4\ua8d0-\ua8d9\ua8f3-\ua8f7\ua900-\ua909\ua926-\ua92d\ua930-\ua945\ua980-\ua983\ua9b3-\ua9c0\uaa00-\uaa27\uaa40-\uaa41\uaa4c-\uaa4d\uaa50-\uaa59\uaa7b\uaae0-\uaae9\uaaf2-\uaaf3\uabc0-\uabe1\uabec\uabed\uabf0-\uabf9\ufb20-\ufb28\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\uff10-\uff19\uff3f";
      var nonASCIIidentifierStart = new RegExp("[" + nonASCIIidentifierStartChars + "]");
      var nonASCIIidentifier = new RegExp("[" + nonASCIIidentifierStartChars + nonASCIIidentifierChars + "]");

      // Whether a single character denotes a newline.

      var newline = exports.newline = /[\n\r\u2028\u2029]/;

      // Matches a whole line break (where CRLF is considered a single
      // line break). Used to count lines.

      var lineBreak = /\r\n|[\n\r\u2028\u2029]/g;

      // Test whether a given character code starts an identifier.

      var isIdentifierStart = exports.isIdentifierStart = function(code) {
        if (code < 65) return code === 36;
        if (code < 91) return true;
        if (code < 97) return code === 95;
        if (code < 123)return true;
        return code >= 0xaa && nonASCIIidentifierStart.test(String.fromCharCode(code));
      };

      // Test whether a given character is part of an identifier.

      var isIdentifierChar = exports.isIdentifierChar = function(code) {
        if (code < 48) return code === 36;
        if (code < 58) return true;
        if (code < 65) return false;
        if (code < 91) return true;
        if (code < 97) return code === 95;
        if (code < 123)return true;
        return code >= 0xaa && nonASCIIidentifier.test(String.fromCharCode(code));
      };
    })(acorn);

    function in_array(what, arr) {
        for (var i = 0; i < arr.length; i += 1) {
            if (arr[i] === what) {
                return true;
            }
        }
        return false;
    }

    function trim(s) {
        return s.replace(/^\s+|\s+$/g, '');
    }

    function js_beautify(js_source_text, options) {
        "use strict";
        var beautifier = new Beautifier(js_source_text, options);
        return beautifier.beautify();
    }

    var MODE = {
            BlockStatement: 'BlockStatement', // 'BLOCK'
            Statement: 'Statement', // 'STATEMENT'
            ObjectLiteral: 'ObjectLiteral', // 'OBJECT',
            ArrayLiteral: 'ArrayLiteral', //'[EXPRESSION]',
            ForInitializer: 'ForInitializer', //'(FOR-EXPRESSION)',
            Conditional: 'Conditional', //'(COND-EXPRESSION)',
            Expression: 'Expression' //'(EXPRESSION)'
        };

    function Beautifier(js_source_text, options) {
        "use strict";
        var output
        var tokens = [], token_pos;
        var Tokenizer;
        var current_token;
        var last_type, last_last_text, indent_string;
        var flags, previous_flags, flag_store;
        var prefix;

        var handlers, opt;
        var baseIndentString = '';

        handlers = {
            'TK_START_EXPR': handle_start_expr,
            'TK_END_EXPR': handle_end_expr,
            'TK_START_BLOCK': handle_start_block,
            'TK_END_BLOCK': handle_end_block,
            'TK_WORD': handle_word,
            'TK_RESERVED': handle_word,
            'TK_SEMICOLON': handle_semicolon,
            'TK_STRING': handle_string,
            'TK_EQUALS': handle_equals,
            'TK_OPERATOR': handle_operator,
            'TK_COMMA': handle_comma,
            'TK_BLOCK_COMMENT': handle_block_comment,
            'TK_INLINE_COMMENT': handle_inline_comment,
            'TK_COMMENT': handle_comment,
            'TK_DOT': handle_dot,
            'TK_UNKNOWN': handle_unknown,
            'TK_EOF': handle_eof
        };

        function create_flags(flags_base, mode) {
            var next_indent_level = 0;
            if (flags_base) {
                next_indent_level = flags_base.indentation_level;
                if (!output.just_added_newline() &&
                    flags_base.line_indent_level > next_indent_level) {
                    next_indent_level = flags_base.line_indent_level;
                }
            }

            var next_flags = {
                mode: mode,
                parent: flags_base,
                last_text: flags_base ? flags_base.last_text : '', // last token text
                last_word: flags_base ? flags_base.last_word : '', // last 'TK_WORD' passed
                declaration_statement: false,
                declaration_assignment: false,
                multiline_frame: false,
                if_block: false,
                else_block: false,
                do_block: false,
                do_while: false,
                in_case_statement: false, // switch(..){ INSIDE HERE }
                in_case: false, // we're on the exact line with "case 0:"
                case_body: false, // the indented case-action block
                indentation_level: next_indent_level,
                line_indent_level: flags_base ? flags_base.line_indent_level : next_indent_level,
                start_line_index: output.get_line_number(),
                ternary_depth: 0
            };
            return next_flags;
        }

        // Some interpreters have unexpected results with foo = baz || bar;
        options = options ? options : {};
        opt = {};

        // compatibility
        if (options.braces_on_own_line !== undefined) { //graceful handling of deprecated option
            opt.brace_style = options.braces_on_own_line ? "expand" : "collapse";
        }
        opt.brace_style = options.brace_style ? options.brace_style : (opt.brace_style ? opt.brace_style : "collapse");

        // graceful handling of deprecated option
        if (opt.brace_style === "expand-strict") {
            opt.brace_style = "expand";
        }


        opt.indent_size = options.indent_size ? parseInt(options.indent_size, 10) : 4;
        opt.indent_char = options.indent_char ? options.indent_char : ' ';
        opt.preserve_newlines = (options.preserve_newlines === undefined) ? true : options.preserve_newlines;
        opt.break_chained_methods = (options.break_chained_methods === undefined) ? false : options.break_chained_methods;
        opt.max_preserve_newlines = (options.max_preserve_newlines === undefined) ? 0 : parseInt(options.max_preserve_newlines, 10);
        opt.space_in_paren = (options.space_in_paren === undefined) ? false : options.space_in_paren;
        opt.space_in_empty_paren = (options.space_in_empty_paren === undefined) ? false : options.space_in_empty_paren;
        opt.jslint_happy = (options.jslint_happy === undefined) ? false : options.jslint_happy;
        opt.space_after_anon_function = (options.space_after_anon_function === undefined) ? false : options.space_after_anon_function;
        opt.keep_array_indentation = (options.keep_array_indentation === undefined) ? false : options.keep_array_indentation;
        opt.space_before_conditional = (options.space_before_conditional === undefined) ? true : options.space_before_conditional;
        opt.unescape_strings = (options.unescape_strings === undefined) ? false : options.unescape_strings;
        opt.wrap_line_length = (options.wrap_line_length === undefined) ? 0 : parseInt(options.wrap_line_length, 10);
        opt.e4x = (options.e4x === undefined) ? false : options.e4x;
        opt.end_with_newline = (options.end_with_newline === undefined) ? false : options.end_with_newline;


        // force opt.space_after_anon_function to true if opt.jslint_happy
        if(opt.jslint_happy) {
            opt.space_after_anon_function = true;
        }

        if(options.indent_with_tabs){
            opt.indent_char = '\t';
            opt.indent_size = 1;
        }

        //----------------------------------
        indent_string = '';
        while (opt.indent_size > 0) {
            indent_string += opt.indent_char;
            opt.indent_size -= 1;
        }

        var preindent_index = 0;
        if(js_source_text && js_source_text.length) {
            while ( (js_source_text.charAt(preindent_index) === ' ' ||
                    js_source_text.charAt(preindent_index) === '\t')) {
                baseIndentString += js_source_text.charAt(preindent_index);
                preindent_index += 1;
            }
            js_source_text = js_source_text.substring(preindent_index);
        }

        last_type = 'TK_START_BLOCK'; // last token type
        last_last_text = ''; // pre-last token text
        output = new Output(indent_string, baseIndentString);


        // Stack of parsing/formatting states, including MODE.
        // We tokenize, parse, and output in an almost purely a forward-only stream of token input
        // and formatted output.  This makes the beautifier less accurate than full parsers
        // but also far more tolerant of syntax errors.
        //
        // For example, the default mode is MODE.BlockStatement. If we see a '{' we push a new frame of type
        // MODE.BlockStatement on the the stack, even though it could be object literal.  If we later
        // encounter a ":", we'll switch to to MODE.ObjectLiteral.  If we then see a ";",
        // most full parsers would die, but the beautifier gracefully falls back to
        // MODE.BlockStatement and continues on.
        flag_store = [];
        set_mode(MODE.BlockStatement);

        this.beautify = function() {

            /*jshint onevar:true */
            var local_token, sweet_code;
            Tokenizer = new tokenizer(js_source_text, opt, indent_string);
            tokens = Tokenizer.tokenize();
            token_pos = 0;

            while (local_token = get_token()) {
                for(var i = 0; i < local_token.comments_before.length; i++) {
                    // The cleanest handling of inline comments is to treat them as though they aren't there.
                    // Just continue formatting and the behavior should be logical.
                    // Also ignore unknown tokens.  Again, this should result in better behavior.
                    handle_token(local_token.comments_before[i]);
                }
                handle_token(local_token);

                last_last_text = flags.last_text;
                last_type = local_token.type;
                flags.last_text = local_token.text;

                token_pos += 1;
            }

            sweet_code = output.get_code();
            if (opt.end_with_newline) {
                sweet_code += '\n';
            }

            return sweet_code;
        };

        function handle_token(local_token) {
            var newlines = local_token.newlines;
            var keep_whitespace = opt.keep_array_indentation && is_array(flags.mode);

            if (keep_whitespace) {
                for (i = 0; i < newlines; i += 1) {
                    print_newline(i > 0);
                }
            } else {
                if (opt.max_preserve_newlines && newlines > opt.max_preserve_newlines) {
                    newlines = opt.max_preserve_newlines;
                }

                if (opt.preserve_newlines) {
                    if (local_token.newlines > 1) {
                        print_newline();
                        for (var i = 1; i < newlines; i += 1) {
                            print_newline(true);
                        }
                    }
                }
            }

            current_token = local_token;
            handlers[current_token.type]();
        }

        // we could use just string.split, but
        // IE doesn't like returning empty strings

        function split_newlines(s) {
            //return s.split(/\x0d\x0a|\x0a/);

            s = s.replace(/\x0d/g, '');
            var out = [],
                idx = s.indexOf("\n");
            while (idx !== -1) {
                out.push(s.substring(0, idx));
                s = s.substring(idx + 1);
                idx = s.indexOf("\n");
            }
            if (s.length) {
                out.push(s);
            }
            return out;
        }

        function allow_wrap_or_preserved_newline(force_linewrap) {
            force_linewrap = (force_linewrap === undefined) ? false : force_linewrap;

            // Never wrap the first token on a line
            if (output.just_added_newline()) {
                return
            }

            if ((opt.preserve_newlines && current_token.wanted_newline) || force_linewrap) {
                print_newline(false, true);
            } else if (opt.wrap_line_length) {
                var proposed_line_length = output.current_line.get_character_count() + current_token.text.length +
                    (output.space_before_token ? 1 : 0);
                if (proposed_line_length >= opt.wrap_line_length) {
                    print_newline(false, true);
                }
            }
        }

        function print_newline(force_newline, preserve_statement_flags) {
            if (!preserve_statement_flags) {
                if (flags.last_text !== ';' && flags.last_text !== ',' && flags.last_text !== '=' && last_type !== 'TK_OPERATOR') {
                    while (flags.mode === MODE.Statement && !flags.if_block && !flags.do_block) {
                        restore_mode();
                    }
                }
            }

            if (output.add_new_line(force_newline)) {
                flags.multiline_frame = true;
            }
        }

        function print_token_line_indentation() {
            if (output.just_added_newline()) {
                if (opt.keep_array_indentation && is_array(flags.mode) && current_token.wanted_newline) {
                    output.current_line.push(current_token.whitespace_before);
                    output.space_before_token = false;
                } else if (output.set_indent(flags.indentation_level)) {
                    flags.line_indent_level = flags.indentation_level;
                }
            }
        }

        function print_token(printable_token) {
            printable_token = printable_token || current_token.text;
            print_token_line_indentation();
            output.add_token(printable_token);
        }

        function indent() {
            flags.indentation_level += 1;
        }

        function deindent() {
            if (flags.indentation_level > 0 &&
                ((!flags.parent) || flags.indentation_level > flags.parent.indentation_level))
                flags.indentation_level -= 1;
        }

        function set_mode(mode) {
            if (flags) {
                flag_store.push(flags);
                previous_flags = flags;
            } else {
                previous_flags = create_flags(null, mode);
            }

            flags = create_flags(previous_flags, mode);
        }

        function is_array(mode) {
            return mode === MODE.ArrayLiteral;
        }

        function is_expression(mode) {
            return in_array(mode, [MODE.Expression, MODE.ForInitializer, MODE.Conditional]);
        }

        function restore_mode() {
            if (flag_store.length > 0) {
                previous_flags = flags;
                flags = flag_store.pop();
                if (previous_flags.mode === MODE.Statement) {
                    output.remove_redundant_indentation(previous_flags);
                }
            }
        }

        function start_of_object_property() {
            return flags.parent.mode === MODE.ObjectLiteral && flags.mode === MODE.Statement && (
                (flags.last_text === ':' && flags.ternary_depth === 0) || (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['get', 'set'])));
        }

        function start_of_statement() {
            if (
                    (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['var', 'let', 'const']) && current_token.type === 'TK_WORD') ||
                    (last_type === 'TK_RESERVED' && flags.last_text === 'do') ||
                    (last_type === 'TK_RESERVED' && flags.last_text === 'return' && !current_token.wanted_newline) ||
                    (last_type === 'TK_RESERVED' && flags.last_text === 'else' && !(current_token.type === 'TK_RESERVED' && current_token.text === 'if')) ||
                    (last_type === 'TK_END_EXPR' && (previous_flags.mode === MODE.ForInitializer || previous_flags.mode === MODE.Conditional)) ||
                    (last_type === 'TK_WORD' && flags.mode === MODE.BlockStatement
                        && !flags.in_case
                        && !(current_token.text === '--' || current_token.text === '++')
                        && current_token.type !== 'TK_WORD' && current_token.type !== 'TK_RESERVED') ||
                    (flags.mode === MODE.ObjectLiteral && (
                        (flags.last_text === ':' && flags.ternary_depth === 0) || (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['get', 'set']))))
                ) {

                set_mode(MODE.Statement);
                indent();

                if (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['var', 'let', 'const']) && current_token.type === 'TK_WORD') {
                    flags.declaration_statement = true;
                }

                // Issue #276:
                // If starting a new statement with [if, for, while, do], push to a new line.
                // if (a) if (b) if(c) d(); else e(); else f();
                if (!start_of_object_property()) {
                    allow_wrap_or_preserved_newline(
                        current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['do', 'for', 'if', 'while']));
                }

                return true;
            }
            return false;
        }

        function all_lines_start_with(lines, c) {
            for (var i = 0; i < lines.length; i++) {
                var line = trim(lines[i]);
                if (line.charAt(0) !== c) {
                    return false;
                }
            }
            return true;
        }

        function each_line_matches_indent(lines, indent) {
            var i = 0,
                len = lines.length,
                line;
            for (; i < len; i++) {
                line = lines[i];
                // allow empty lines to pass through
                if (line && line.indexOf(indent) !== 0) {
                    return false;
                }
            }
            return true;
        }

        function is_special_word(word) {
            return in_array(word, ['case', 'return', 'do', 'if', 'throw', 'else']);
        }

        function get_token(offset) {
            var index = token_pos + (offset || 0);
            return (index < 0 || index >= tokens.length) ? null : tokens[index];
        }

        function handle_start_expr() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            var next_mode = MODE.Expression;
            if (current_token.text === '[') {

                if (last_type === 'TK_WORD' || flags.last_text === ')') {
                    // this is array index specifier, break immediately
                    // a[x], fn()[x]
                    if (last_type === 'TK_RESERVED' && in_array(flags.last_text, Tokenizer.line_starters)) {
                        output.space_before_token = true;
                    }
                    set_mode(next_mode);
                    print_token();
                    indent();
                    if (opt.space_in_paren) {
                        output.space_before_token = true;
                    }
                    return;
                }

                next_mode = MODE.ArrayLiteral;
                if (is_array(flags.mode)) {
                    if (flags.last_text === '[' ||
                        (flags.last_text === ',' && (last_last_text === ']' || last_last_text === '}'))) {
                        // ], [ goes to new line
                        // }, [ goes to new line
                        if (!opt.keep_array_indentation) {
                            print_newline();
                        }
                    }
                }

            } else {
                if (last_type === 'TK_RESERVED' && flags.last_text === 'for') {
                    next_mode = MODE.ForInitializer;
                } else if (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['if', 'while'])) {
                    next_mode = MODE.Conditional;
                } else {
                    // next_mode = MODE.Expression;
                }
            }

            if (flags.last_text === ';' || last_type === 'TK_START_BLOCK') {
                print_newline();
            } else if (last_type === 'TK_END_EXPR' || last_type === 'TK_START_EXPR' || last_type === 'TK_END_BLOCK' || flags.last_text === '.') {
                // TODO: Consider whether forcing this is required.  Review failing tests when removed.
                allow_wrap_or_preserved_newline(current_token.wanted_newline);
                // do nothing on (( and )( and ][ and ]( and .(
            } else if (!(last_type === 'TK_RESERVED' && current_token.text === '(') && last_type !== 'TK_WORD' && last_type !== 'TK_OPERATOR') {
                output.space_before_token = true;
            } else if ((last_type === 'TK_RESERVED' && (flags.last_word === 'function' || flags.last_word === 'typeof')) ||
                (flags.last_text === '*' && last_last_text === 'function')) {
                // function() vs function ()
                if (opt.space_after_anon_function) {
                    output.space_before_token = true;
                }
            } else if (last_type === 'TK_RESERVED' && (in_array(flags.last_text, Tokenizer.line_starters) || flags.last_text === 'catch')) {
                if (opt.space_before_conditional) {
                    output.space_before_token = true;
                }
            }

            // Support of this kind of newline preservation.
            // a = (b &&
            //     (c || d));
            if (current_token.text === '(') {
                if (last_type === 'TK_EQUALS' || last_type === 'TK_OPERATOR') {
                    if (!start_of_object_property()) {
                        allow_wrap_or_preserved_newline();
                    }
                }
            }

            set_mode(next_mode);
            print_token();
            if (opt.space_in_paren) {
                output.space_before_token = true;
            }

            // In all cases, if we newline while inside an expression it should be indented.
            indent();
        }

        function handle_end_expr() {
            // statements inside expressions are not valid syntax, but...
            // statements must all be closed when their container closes
            while (flags.mode === MODE.Statement) {
                restore_mode();
            }

            if (flags.multiline_frame) {
                allow_wrap_or_preserved_newline(current_token.text === ']' && is_array(flags.mode) && !opt.keep_array_indentation);
            }

            if (opt.space_in_paren) {
                if (last_type === 'TK_START_EXPR' && ! opt.space_in_empty_paren) {
                    // () [] no inner space in empty parens like these, ever, ref #320
                    output.trim();
                    output.space_before_token = false;
                } else {
                    output.space_before_token = true;
                }
            }
            if (current_token.text === ']' && opt.keep_array_indentation) {
                print_token();
                restore_mode();
            } else {
                restore_mode();
                print_token();
            }
            output.remove_redundant_indentation(previous_flags);

            // do {} while () // no statement required after
            if (flags.do_while && previous_flags.mode === MODE.Conditional) {
                previous_flags.mode = MODE.Expression;
                flags.do_block = false;
                flags.do_while = false;

            }
        }

        function handle_start_block() {
            // Check if this is should be treated as a ObjectLiteral
            var next_token = get_token(1)
            var second_token = get_token(2)
            if (second_token && (
                    (second_token.text === ':' && in_array(next_token.type, ['TK_STRING', 'TK_WORD', 'TK_RESERVED']))
                    || (in_array(next_token.text, ['get', 'set']) && in_array(second_token.type, ['TK_WORD', 'TK_RESERVED']))
                )) {
                // We don't support TypeScript,but we didn't break it for a very long time.
                // We'll try to keep not breaking it.
                if (!in_array(last_last_text, ['class','interface'])) {
                    set_mode(MODE.ObjectLiteral);
                } else {
                    set_mode(MODE.BlockStatement);
                }
            } else {
                set_mode(MODE.BlockStatement);
            }

            var empty_braces = !next_token.comments_before.length &&  next_token.text === '}';
            var empty_anonymous_function = empty_braces && flags.last_word === 'function' &&
                last_type === 'TK_END_EXPR';

            if (opt.brace_style === "expand" ||
                (opt.brace_style === "none" && current_token.wanted_newline)) {
                if (last_type !== 'TK_OPERATOR' &&
                    (empty_anonymous_function ||
                        last_type === 'TK_EQUALS' ||
                        (last_type === 'TK_RESERVED' && is_special_word(flags.last_text) && flags.last_text !== 'else'))) {
                    output.space_before_token = true;
                } else {
                    print_newline(false, true);
                }
            } else { // collapse
                if (last_type !== 'TK_OPERATOR' && last_type !== 'TK_START_EXPR') {
                    if (last_type === 'TK_START_BLOCK') {
                        print_newline();
                    } else {
                        output.space_before_token = true;
                    }
                } else {
                    // if TK_OPERATOR or TK_START_EXPR
                    if (is_array(previous_flags.mode) && flags.last_text === ',') {
                        if (last_last_text === '}') {
                            // }, { in array context
                            output.space_before_token = true;
                        } else {
                            print_newline(); // [a, b, c, {
                        }
                    }
                }
            }
            print_token();
            indent();
        }

        function handle_end_block() {
            // statements must all be closed when their container closes
            while (flags.mode === MODE.Statement) {
                restore_mode();
            }
            var empty_braces = last_type === 'TK_START_BLOCK';

            if (opt.brace_style === "expand") {
                if (!empty_braces) {
                    print_newline();
                }
            } else {
                // skip {}
                if (!empty_braces) {
                    if (is_array(flags.mode) && opt.keep_array_indentation) {
                        // we REALLY need a newline here, but newliner would skip that
                        opt.keep_array_indentation = false;
                        print_newline();
                        opt.keep_array_indentation = true;

                    } else {
                        print_newline();
                    }
                }
            }
            restore_mode();
            print_token();
        }

        function handle_word() {
            if (current_token.type === 'TK_RESERVED' && flags.mode !== MODE.ObjectLiteral &&
                in_array(current_token.text, ['set', 'get'])) {
                current_token.type = 'TK_WORD';
            }

            if (current_token.type === 'TK_RESERVED' && flags.mode === MODE.ObjectLiteral) {
                var next_token = get_token(1);
                if (next_token.text == ':') {
                    current_token.type = 'TK_WORD';
                }
            }

            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            } else if (current_token.wanted_newline && !is_expression(flags.mode) &&
                (last_type !== 'TK_OPERATOR' || (flags.last_text === '--' || flags.last_text === '++')) &&
                last_type !== 'TK_EQUALS' &&
                (opt.preserve_newlines || !(last_type === 'TK_RESERVED' && in_array(flags.last_text, ['var', 'let', 'const', 'set', 'get'])))) {

                print_newline();
            }

            if (flags.do_block && !flags.do_while) {
                if (current_token.type === 'TK_RESERVED' && current_token.text === 'while') {
                    // do {} ## while ()
                    output.space_before_token = true;
                    print_token();
                    output.space_before_token = true;
                    flags.do_while = true;
                    return;
                } else {
                    // do {} should always have while as the next word.
                    // if we don't see the expected while, recover
                    print_newline();
                    flags.do_block = false;
                }
            }

            // if may be followed by else, or not
            // Bare/inline ifs are tricky
            // Need to unwind the modes correctly: if (a) if (b) c(); else d(); else e();
            if (flags.if_block) {
                if (!flags.else_block && (current_token.type === 'TK_RESERVED' && current_token.text === 'else')) {
                    flags.else_block = true;
                } else {
                    while (flags.mode === MODE.Statement) {
                        restore_mode();
                    }
                    flags.if_block = false;
                    flags.else_block = false;
                }
            }

            if (current_token.type === 'TK_RESERVED' && (current_token.text === 'case' || (current_token.text === 'default' && flags.in_case_statement))) {
                print_newline();
                if (flags.case_body || opt.jslint_happy) {
                    // switch cases following one another
                    deindent();
                    flags.case_body = false;
                }
                print_token();
                flags.in_case = true;
                flags.in_case_statement = true;
                return;
            }

            if (current_token.type === 'TK_RESERVED' && current_token.text === 'function') {
                if (in_array(flags.last_text, ['}', ';']) || (output.just_added_newline() && ! in_array(flags.last_text, ['[', '{', ':', '=', ',']))) {
                    // make sure there is a nice clean space of at least one blank line
                    // before a new function definition
                    if ( !output.just_added_blankline() && !current_token.comments_before.length) {
                        print_newline();
                        print_newline(true);
                    }
                }
                if (last_type === 'TK_RESERVED' || last_type === 'TK_WORD') {
                    if (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['get', 'set', 'new', 'return', 'export'])) {
                        output.space_before_token = true;
                    } else if (last_type === 'TK_RESERVED' && flags.last_text === 'default' && last_last_text === 'export') {
                        output.space_before_token = true;
                    } else {
                        print_newline();
                    }
                } else if (last_type === 'TK_OPERATOR' || flags.last_text === '=') {
                    // foo = function
                    output.space_before_token = true;
                } else if (!flags.multiline_frame && (is_expression(flags.mode) || is_array(flags.mode))) {
                    // (function
                } else {
                    print_newline();
                }
            }

            if (last_type === 'TK_COMMA' || last_type === 'TK_START_EXPR' || last_type === 'TK_EQUALS' || last_type === 'TK_OPERATOR') {
                if (!start_of_object_property()) {
                    allow_wrap_or_preserved_newline();
                }
            }

            if (current_token.type === 'TK_RESERVED' &&  in_array(current_token.text, ['function', 'get', 'set'])) {
                print_token();
                flags.last_word = current_token.text;
                return;
            }

            prefix = 'NONE';

            if (last_type === 'TK_END_BLOCK') {
                if (!(current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['else', 'catch', 'finally']))) {
                    prefix = 'NEWLINE';
                } else {
                    if (opt.brace_style === "expand" ||
                        opt.brace_style === "end-expand" ||
                        (opt.brace_style === "none" && current_token.wanted_newline)) {
                        prefix = 'NEWLINE';
                    } else {
                        prefix = 'SPACE';
                        output.space_before_token = true;
                    }
                }
            } else if (last_type === 'TK_SEMICOLON' && flags.mode === MODE.BlockStatement) {
                // TODO: Should this be for STATEMENT as well?
                prefix = 'NEWLINE';
            } else if (last_type === 'TK_SEMICOLON' && is_expression(flags.mode)) {
                prefix = 'SPACE';
            } else if (last_type === 'TK_STRING') {
                prefix = 'NEWLINE';
            } else if (last_type === 'TK_RESERVED' || last_type === 'TK_WORD' ||
                (flags.last_text === '*' && last_last_text === 'function')) {
                prefix = 'SPACE';
            } else if (last_type === 'TK_START_BLOCK') {
                prefix = 'NEWLINE';
            } else if (last_type === 'TK_END_EXPR') {
                output.space_before_token = true;
                prefix = 'NEWLINE';
            }

            if (current_token.type === 'TK_RESERVED' && in_array(current_token.text, Tokenizer.line_starters) && flags.last_text !== ')') {
                if (flags.last_text === 'else' || flags.last_text === 'export') {
                    prefix = 'SPACE';
                } else {
                    prefix = 'NEWLINE';
                }

            }

            if (current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['else', 'catch', 'finally'])) {
                if (last_type !== 'TK_END_BLOCK' ||
                    opt.brace_style === "expand" ||
                    opt.brace_style === "end-expand" ||
                    (opt.brace_style === "none" && current_token.wanted_newline)) {
                    print_newline();
                } else {
                    output.trim(true);
                    var line = output.current_line;
                    // If we trimmed and there's something other than a close block before us
                    // put a newline back in.  Handles '} // comment' scenario.
                    if (line.last() !== '}') {
                        print_newline();
                    }
                    output.space_before_token = true;
                }
            } else if (prefix === 'NEWLINE') {
                if (last_type === 'TK_RESERVED' && is_special_word(flags.last_text)) {
                    // no newline between 'return nnn'
                    output.space_before_token = true;
                } else if (last_type !== 'TK_END_EXPR') {
                    if ((last_type !== 'TK_START_EXPR' || !(current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['var', 'let', 'const']))) && flags.last_text !== ':') {
                        // no need to force newline on 'var': for (var x = 0...)
                        if (current_token.type === 'TK_RESERVED' && current_token.text === 'if' && flags.last_text === 'else') {
                            // no newline for } else if {
                            output.space_before_token = true;
                        } else {
                            print_newline();
                        }
                    }
                } else if (current_token.type === 'TK_RESERVED' && in_array(current_token.text, Tokenizer.line_starters) && flags.last_text !== ')') {
                    print_newline();
                }
            } else if (flags.multiline_frame && is_array(flags.mode) && flags.last_text === ',' && last_last_text === '}') {
                print_newline(); // }, in lists get a newline treatment
            } else if (prefix === 'SPACE') {
                output.space_before_token = true;
            }
            print_token();
            flags.last_word = current_token.text;

            if (current_token.type === 'TK_RESERVED' && current_token.text === 'do') {
                flags.do_block = true;
            }

            if (current_token.type === 'TK_RESERVED' && current_token.text === 'if') {
                flags.if_block = true;
            }
        }

        function handle_semicolon() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
                // Semicolon can be the start (and end) of a statement
                output.space_before_token = false;
            }
            while (flags.mode === MODE.Statement && !flags.if_block && !flags.do_block) {
                restore_mode();
            }
            print_token();
        }

        function handle_string() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
                // One difference - strings want at least a space before
                output.space_before_token = true;
            } else if (last_type === 'TK_RESERVED' || last_type === 'TK_WORD') {
                output.space_before_token = true;
            } else if (last_type === 'TK_COMMA' || last_type === 'TK_START_EXPR' || last_type === 'TK_EQUALS' || last_type === 'TK_OPERATOR') {
                if (!start_of_object_property()) {
                    allow_wrap_or_preserved_newline();
                }
            } else {
                print_newline();
            }
            print_token();
        }

        function handle_equals() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            if (flags.declaration_statement) {
                // just got an '=' in a var-line, different formatting/line-breaking, etc will now be done
                flags.declaration_assignment = true;
            }
            output.space_before_token = true;
            print_token();
            output.space_before_token = true;
        }

        function handle_comma() {
            if (flags.declaration_statement) {
                if (is_expression(flags.parent.mode)) {
                    // do not break on comma, for(var a = 1, b = 2)
                    flags.declaration_assignment = false;
                }

                print_token();

                if (flags.declaration_assignment) {
                    flags.declaration_assignment = false;
                    print_newline(false, true);
                } else {
                    output.space_before_token = true;
                }
                return;
            }

            print_token();
            if (flags.mode === MODE.ObjectLiteral ||
                (flags.mode === MODE.Statement && flags.parent.mode === MODE.ObjectLiteral)) {
                if (flags.mode === MODE.Statement) {
                    restore_mode();
                }
                print_newline();
            } else {
                // EXPR or DO_BLOCK
                output.space_before_token = true;
            }

        }

        function handle_operator() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            if (last_type === 'TK_RESERVED' && is_special_word(flags.last_text)) {
                // "return" had a special handling in TK_WORD. Now we need to return the favor
                output.space_before_token = true;
                print_token();
                return;
            }

            // hack for actionscript's import .*;
            if (current_token.text === '*' && last_type === 'TK_DOT') {
                print_token();
                return;
            }

            if (current_token.text === ':' && flags.in_case) {
                flags.case_body = true;
                indent();
                print_token();
                print_newline();
                flags.in_case = false;
                return;
            }

            if (current_token.text === '::') {
                // no spaces around exotic namespacing syntax operator
                print_token();
                return;
            }

            // http://www.ecma-international.org/ecma-262/5.1/#sec-7.9.1
            // if there is a newline between -- or ++ and anything else we should preserve it.
            if (current_token.wanted_newline && (current_token.text === '--' || current_token.text === '++')) {
                print_newline(false, true);
            }

            // Allow line wrapping between operators
            if (last_type === 'TK_OPERATOR') {
                allow_wrap_or_preserved_newline();
            }

            var space_before = true;
            var space_after = true;

            if (in_array(current_token.text, ['--', '++', '!', '~']) || (in_array(current_token.text, ['-', '+']) && (in_array(last_type, ['TK_START_BLOCK', 'TK_START_EXPR', 'TK_EQUALS', 'TK_OPERATOR']) || in_array(flags.last_text, Tokenizer.line_starters) || flags.last_text === ','))) {
                // unary operators (and binary +/- pretending to be unary) special cases

                space_before = false;
                space_after = false;

                if (flags.last_text === ';' && is_expression(flags.mode)) {
                    // for (;; ++i)
                    //        ^^^
                    space_before = true;
                }

                if (last_type === 'TK_RESERVED' || last_type === 'TK_END_EXPR') {
                    space_before = true;
                } else if (last_type === 'TK_OPERATOR') {
                    space_before =
                        (in_array(current_token.text, ['--', '-']) && in_array(flags.last_text, ['--', '-'])) ||
                        (in_array(current_token.text, ['++', '+']) && in_array(flags.last_text, ['++', '+']));
                }

                if ((flags.mode === MODE.BlockStatement || flags.mode === MODE.Statement) && (flags.last_text === '{' || flags.last_text === ';')) {
                    // { foo; --i }
                    // foo(); --bar;
                    print_newline();
                }
            } else if (current_token.text === ':') {
                if (flags.ternary_depth === 0) {
                    // Colon is invalid javascript outside of ternary and object, but do our best to guess what was meant.
                    space_before = false;
                } else {
                    flags.ternary_depth -= 1;
                }
            } else if (current_token.text === '?') {
                flags.ternary_depth += 1;
            } else if (current_token.text === '*' && last_type === 'TK_RESERVED' && flags.last_text === 'function') {
                space_before = false;
                space_after = false;
            }
            output.space_before_token = output.space_before_token || space_before;
            print_token();
            output.space_before_token = space_after;
        }

        function handle_block_comment() {
            var lines = split_newlines(current_token.text);
            var j; // iterator for this case
            var javadoc = false;
            var starless = false;
            var lastIndent = current_token.whitespace_before;
            var lastIndentLength = lastIndent.length;

            // block comment starts with a new line
            print_newline(false, true);
            if (lines.length > 1) {
                if (all_lines_start_with(lines.slice(1), '*')) {
                    javadoc = true;
                }
                else if (each_line_matches_indent(lines.slice(1), lastIndent)) {
                    starless = true;
                }
            }

            // first line always indented
            print_token(lines[0]);
            for (j = 1; j < lines.length; j++) {
                print_newline(false, true);
                if (javadoc) {
                    // javadoc: reformat and re-indent
                    print_token(' ' + trim(lines[j]));
                } else if (starless && lines[j].length > lastIndentLength) {
                    // starless: re-indent non-empty content, avoiding trim
                    print_token(lines[j].substring(lastIndentLength));
                } else {
                    // normal comments output raw
                    output.add_token(lines[j]);
                }
            }

            // for comments of more than one line, make sure there's a new line after
            print_newline(false, true);
        }

        function handle_inline_comment() {
            output.space_before_token = true;
            print_token();
            output.space_before_token = true;
        }

        function handle_comment() {
            if (current_token.wanted_newline) {
                print_newline(false, true);
            } else {
                output.trim(true);
            }

            output.space_before_token = true;
            print_token();
            print_newline(false, true);
        }

        function handle_dot() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            if (last_type === 'TK_RESERVED' && is_special_word(flags.last_text)) {
                output.space_before_token = true;
            } else {
                // allow preserved newlines before dots in general
                // force newlines on dots after close paren when break_chained - for bar().baz()
                allow_wrap_or_preserved_newline(flags.last_text === ')' && opt.break_chained_methods);
            }

            print_token();
        }

        function handle_unknown() {
            print_token();

            if (current_token.text[current_token.text.length - 1] === '\n') {
                print_newline();
            }
        }

        function handle_eof() {
            // Unwind any open statements
            while (flags.mode === MODE.Statement) {
                restore_mode();
            }
        }
    }


    function OutputLine(parent) {
        var _character_count = 0;
        // use indent_count as a marker for lines that have preserved indentation
        var _indent_count = -1;

        var _items = [];
        var _empty = true;

        this.set_indent = function(level) {
            _character_count = parent.baseIndentLength + level * parent.indent_length
            _indent_count = level;
        }

        this.get_character_count = function() {
            return _character_count;
        }

        this.is_empty = function() {
            return _empty;
        }

        this.last = function() {
            if (!this._empty) {
              return _items[_items.length - 1];
            } else {
              return null;
            }
        }

        this.push = function(input) {
            _items.push(input);
            _character_count += input.length;
            _empty = false;
        }

        this.remove_indent = function() {
            if (_indent_count > 0) {
                _indent_count -= 1;
                _character_count -= parent.indent_length
            }
        }

        this.trim = function() {
            while (this.last() === ' ') {
                var item = _items.pop();
                _character_count -= 1;
            }
            _empty = _items.length === 0;
        }

        this.toString = function() {
            var result = '';
            if (!this._empty) {
                if (_indent_count >= 0) {
                    result = parent.indent_cache[_indent_count];
                }
                result += _items.join('')
            }
            return result;
        }
    }

    function Output(indent_string, baseIndentString) {
        baseIndentString = baseIndentString || '';
        this.indent_cache = [ baseIndentString ];
        this.baseIndentLength = baseIndentString.length;
        this.indent_length = indent_string.length;

        var lines =[];
        this.baseIndentString = baseIndentString;
        this.indent_string = indent_string;
        this.current_line = null;
        this.space_before_token = false;

        this.get_line_number = function() {
            return lines.length;
        }

        // Using object instead of string to allow for later expansion of info about each line
        this.add_new_line = function(force_newline) {
            if (this.get_line_number() === 1 && this.just_added_newline()) {
                return false; // no newline on start of file
            }

            if (force_newline || !this.just_added_newline()) {
                this.current_line = new OutputLine(this);
                lines.push(this.current_line);
                return true;
            }

            return false;
        }

        // initialize
        this.add_new_line(true);

        this.get_code = function() {
            var sweet_code = lines.join('\n').replace(/[\r\n\t ]+$/, '');
            return sweet_code;
        }

        this.set_indent = function(level) {
            // Never indent your first output indent at the start of the file
            if (lines.length > 1) {
                while(level >= this.indent_cache.length) {
                    this.indent_cache.push(this.indent_cache[this.indent_cache.length - 1] + this.indent_string);
                }

                this.current_line.set_indent(level);
                return true;
            }
            this.current_line.set_indent(0);
            return false;
        }

        this.add_token = function(printable_token) {
            this.add_space_before_token();
            this.current_line.push(printable_token);
        }

        this.add_space_before_token = function() {
            if (this.space_before_token && !this.just_added_newline()) {
                this.current_line.push(' ');
            }
            this.space_before_token = false;
        }

        this.remove_redundant_indentation = function (frame) {
            // This implementation is effective but has some issues:
            //     - can cause line wrap to happen too soon due to indent removal
            //           after wrap points are calculated
            // These issues are minor compared to ugly indentation.

            if (frame.multiline_frame ||
                frame.mode === MODE.ForInitializer ||
                frame.mode === MODE.Conditional) {
                return;
            }

            // remove one indent from each line inside this section
            var index = frame.start_line_index;
            var line;

            var output_length = lines.length;
            while (index < output_length) {
                lines[index].remove_indent();
                index++;
            }
        }

        this.trim = function(eat_newlines) {
            eat_newlines = (eat_newlines === undefined) ? false : eat_newlines;

            this.current_line.trim(indent_string, baseIndentString);

            while (eat_newlines && lines.length > 1 &&
                this.current_line.is_empty()) {
                lines.pop();
                this.current_line = lines[lines.length - 1]
                this.current_line.trim();
            }
        }

        this.just_added_newline = function() {
            return this.current_line.is_empty();
        }

        this.just_added_blankline = function() {
            if (this.just_added_newline()) {
                if (lines.length === 1) {
                    return true; // start of the file and newline = blank
                }

                var line = lines[lines.length - 2];
                return line.is_empty();
            }
            return false;
        }
    }


    var Token = function(type, text, newlines, whitespace_before, mode, parent) {
        this.type = type;
        this.text = text;
        this.comments_before = [];
        this.newlines = newlines || 0;
        this.wanted_newline = newlines > 0;
        this.whitespace_before = whitespace_before || '';
        this.parent = null;
    }

    function tokenizer(input, opts, indent_string) {

        var whitespace = "\n\r\t ".split('');
        var digit = /[0-9]/;

        var punct = ('+ - * / % & ++ -- = += -= *= /= %= == === != !== > < >= <= >> << >>> >>>= >>= <<= && &= | || ! ~ , : ? ^ ^= |= :: =>'
                +' <%= <% %> <?= <? ?>').split(' '); // try to be a good boy and try not to break the markup language identifiers

        // words which should always start on new line.
        this.line_starters = 'continue,try,throw,return,var,let,const,if,switch,case,default,for,while,break,function,yield,import,export'.split(',');
        var reserved_words = this.line_starters.concat(['do', 'in', 'else', 'get', 'set', 'new', 'catch', 'finally', 'typeof']);

        var n_newlines, whitespace_before_token, in_html_comment, tokens, parser_pos;
        var input_length;

        this.tokenize = function() {
            // cache the source's length.
            input_length = input.length
            parser_pos = 0;
            in_html_comment = false
            tokens = [];

            var next, last;
            var token_values;
            var open = null;
            var open_stack = [];
            var comments = [];

            while (!(last && last.type === 'TK_EOF')) {
                token_values = tokenize_next();
                next = new Token(token_values[1], token_values[0], n_newlines, whitespace_before_token);
                while(next.type === 'TK_INLINE_COMMENT' || next.type === 'TK_COMMENT' ||
                    next.type === 'TK_BLOCK_COMMENT' || next.type === 'TK_UNKNOWN') {
                    comments.push(next);
                    token_values = tokenize_next();
                    next = new Token(token_values[1], token_values[0], n_newlines, whitespace_before_token);
                }

                if (comments.length) {
                    next.comments_before = comments;
                    comments = [];
                }

                if (next.type === 'TK_START_BLOCK' || next.type === 'TK_START_EXPR') {
                    next.parent = last;
                    open = next;
                    open_stack.push(next);
                }  else if ((next.type === 'TK_END_BLOCK' || next.type === 'TK_END_EXPR') &&
                    (open && (
                        (next.text === ']' && open.text === '[') ||
                        (next.text === ')' && open.text === '(') ||
                        (next.text === '}' && open.text === '}')))) {
                    next.parent = open.parent;
                    open = open_stack.pop();
                }

                tokens.push(next);
                last = next;
            }

            return tokens;
        }

        function tokenize_next() {
            var i, resulting_string;
            var whitespace_on_this_line = [];

            n_newlines = 0;
            whitespace_before_token = '';

            if (parser_pos >= input_length) {
                return ['', 'TK_EOF'];
            }

            var last_token;
            if (tokens.length) {
                last_token = tokens[tokens.length-1];
            } else {
                // For the sake of tokenizing we can pretend that there was on open brace to start
                last_token = new Token('TK_START_BLOCK', '{');
            }


            var c = input.charAt(parser_pos);
            parser_pos += 1;

            while (in_array(c, whitespace)) {

                if (c === '\n') {
                    n_newlines += 1;
                    whitespace_on_this_line = [];
                } else if (n_newlines) {
                    if (c === indent_string) {
                        whitespace_on_this_line.push(indent_string);
                    } else if (c !== '\r') {
                        whitespace_on_this_line.push(' ');
                    }
                }

                if (parser_pos >= input_length) {
                    return ['', 'TK_EOF'];
                }

                c = input.charAt(parser_pos);
                parser_pos += 1;
            }

            if(whitespace_on_this_line.length) {
                whitespace_before_token = whitespace_on_this_line.join('');
            }

            if (digit.test(c)) {
                var allow_decimal = true;
                var allow_e = true;
                var local_digit = digit;

                if (c === '0' && parser_pos < input_length && /[Xx]/.test(input.charAt(parser_pos))) {
                    // switch to hex number, no decimal or e, just hex digits
                    allow_decimal = false;
                    allow_e = false;
                    c += input.charAt(parser_pos);
                    parser_pos += 1;
                    local_digit = /[0123456789abcdefABCDEF]/
                } else {
                    // we know this first loop will run.  It keeps the logic simpler.
                    c = '';
                    parser_pos -= 1
                }

                // Add the digits
                while (parser_pos < input_length && local_digit.test(input.charAt(parser_pos))) {
                    c += input.charAt(parser_pos);
                    parser_pos += 1;

                    if (allow_decimal && parser_pos < input_length && input.charAt(parser_pos) === '.') {
                        c += input.charAt(parser_pos);
                        parser_pos += 1;
                        allow_decimal = false;
                    }

                    if (allow_e && parser_pos < input_length && /[Ee]/.test(input.charAt(parser_pos))) {
                        c += input.charAt(parser_pos);
                        parser_pos += 1;

                        if (parser_pos < input_length && /[+-]/.test(input.charAt(parser_pos))) {
                            c += input.charAt(parser_pos);
                            parser_pos += 1;
                        }

                        allow_e = false;
                        allow_decimal = false;
                    }
                }

                return [c, 'TK_WORD'];
            }

            if (acorn.isIdentifierStart(input.charCodeAt(parser_pos-1))) {
                if (parser_pos < input_length) {
                    while (acorn.isIdentifierChar(input.charCodeAt(parser_pos))) {
                        c += input.charAt(parser_pos);
                        parser_pos += 1;
                        if (parser_pos === input_length) {
                            break;
                        }
                    }
                }

                if (!(last_token.type === 'TK_DOT' ||
                        (last_token.type === 'TK_RESERVED' && in_array(last_token.text, ['set', 'get'])))
                    && in_array(c, reserved_words)) {
                    if (c === 'in') { // hack for 'in' operator
                        return [c, 'TK_OPERATOR'];
                    }
                    return [c, 'TK_RESERVED'];
                }

                return [c, 'TK_WORD'];
            }

            if (c === '(' || c === '[') {
                return [c, 'TK_START_EXPR'];
            }

            if (c === ')' || c === ']') {
                return [c, 'TK_END_EXPR'];
            }

            if (c === '{') {
                return [c, 'TK_START_BLOCK'];
            }

            if (c === '}') {
                return [c, 'TK_END_BLOCK'];
            }

            if (c === ';') {
                return [c, 'TK_SEMICOLON'];
            }

            if (c === '/') {
                var comment = '';
                // peek for comment /* ... */
                var inline_comment = true;
                if (input.charAt(parser_pos) === '*') {
                    parser_pos += 1;
                    if (parser_pos < input_length) {
                        while (parser_pos < input_length && !(input.charAt(parser_pos) === '*' && input.charAt(parser_pos + 1) && input.charAt(parser_pos + 1) === '/')) {
                            c = input.charAt(parser_pos);
                            comment += c;
                            if (c === "\n" || c === "\r") {
                                inline_comment = false;
                            }
                            parser_pos += 1;
                            if (parser_pos >= input_length) {
                                break;
                            }
                        }
                    }
                    parser_pos += 2;
                    if (inline_comment && n_newlines === 0) {
                        return ['/*' + comment + '*/', 'TK_INLINE_COMMENT'];
                    } else {
                        return ['/*' + comment + '*/', 'TK_BLOCK_COMMENT'];
                    }
                }
                // peek for comment // ...
                if (input.charAt(parser_pos) === '/') {
                    comment = c;
                    while (input.charAt(parser_pos) !== '\r' && input.charAt(parser_pos) !== '\n') {
                        comment += input.charAt(parser_pos);
                        parser_pos += 1;
                        if (parser_pos >= input_length) {
                            break;
                        }
                    }
                    return [comment, 'TK_COMMENT'];
                }

            }

            if (c === '`' || c === "'" || c === '"' || // string
                (
                    (c === '/') || // regexp
                    (opts.e4x && c === "<" && input.slice(parser_pos - 1).match(/^<([-a-zA-Z:0-9_.]+|{[^{}]*}|!\[CDATA\[[\s\S]*?\]\])\s*([-a-zA-Z:0-9_.]+=('[^']*'|"[^"]*"|{[^{}]*})\s*)*\/?\s*>/)) // xml
                ) && ( // regex and xml can only appear in specific locations during parsing
                    (last_token.type === 'TK_RESERVED' && in_array(last_token.text , ['return', 'case', 'throw', 'else', 'do', 'typeof', 'yield'])) ||
                    (last_token.type === 'TK_END_EXPR' && last_token.text === ')' &&
                        last_token.parent && last_token.parent.type === 'TK_RESERVED' && in_array(last_token.parent.text, ['if', 'while', 'for'])) ||
                    (in_array(last_token.type, ['TK_COMMENT', 'TK_START_EXPR', 'TK_START_BLOCK',
                        'TK_END_BLOCK', 'TK_OPERATOR', 'TK_EQUALS', 'TK_EOF', 'TK_SEMICOLON', 'TK_COMMA'
                    ]))
                )) {

                var sep = c,
                    esc = false,
                    has_char_escapes = false;

                resulting_string = c;

                if (sep === '/') {
                    //
                    // handle regexp
                    //
                    var in_char_class = false;
                    while (parser_pos < input_length &&
                            ((esc || in_char_class || input.charAt(parser_pos) !== sep) &&
                            !acorn.newline.test(input.charAt(parser_pos)))) {
                        resulting_string += input.charAt(parser_pos);
                        if (!esc) {
                            esc = input.charAt(parser_pos) === '\\';
                            if (input.charAt(parser_pos) === '[') {
                                in_char_class = true;
                            } else if (input.charAt(parser_pos) === ']') {
                                in_char_class = false;
                            }
                        } else {
                            esc = false;
                        }
                        parser_pos += 1;
                    }
                } else if (opts.e4x && sep === '<') {
                    //
                    // handle e4x xml literals
                    //
                    var xmlRegExp = /<(\/?)([-a-zA-Z:0-9_.]+|{[^{}]*}|!\[CDATA\[[\s\S]*?\]\])\s*([-a-zA-Z:0-9_.]+=('[^']*'|"[^"]*"|{[^{}]*})\s*)*(\/?)\s*>/g;
                    var xmlStr = input.slice(parser_pos - 1);
                    var match = xmlRegExp.exec(xmlStr);
                    if (match && match.index === 0) {
                        var rootTag = match[2];
                        var depth = 0;
                        while (match) {
                            var isEndTag = !! match[1];
                            var tagName = match[2];
                            var isSingletonTag = ( !! match[match.length - 1]) || (tagName.slice(0, 8) === "![CDATA[");
                            if (tagName === rootTag && !isSingletonTag) {
                                if (isEndTag) {
                                    --depth;
                                } else {
                                    ++depth;
                                }
                            }
                            if (depth <= 0) {
                                break;
                            }
                            match = xmlRegExp.exec(xmlStr);
                        }
                        var xmlLength = match ? match.index + match[0].length : xmlStr.length;
                        parser_pos += xmlLength - 1;
                        return [xmlStr.slice(0, xmlLength), "TK_STRING"];
                    }
                } else {
                    //
                    // handle string
                    //
                    // Template strings can travers lines without escape characters.
                    // Other strings cannot
                    while (parser_pos < input_length &&
                            (esc || (input.charAt(parser_pos) !== sep &&
                            (sep === '`' || !acorn.newline.test(input.charAt(parser_pos)))))) {
                        resulting_string += input.charAt(parser_pos);
                        if (esc) {
                            if (input.charAt(parser_pos) === 'x' || input.charAt(parser_pos) === 'u') {
                                has_char_escapes = true;
                            }
                            esc = false;
                        } else {
                            esc = input.charAt(parser_pos) === '\\';
                        }
                        parser_pos += 1;
                    }

                }

                if (has_char_escapes && opts.unescape_strings) {
                    resulting_string = unescape_string(resulting_string);
                }

                if (parser_pos < input_length && input.charAt(parser_pos) === sep) {
                    resulting_string += sep;
                    parser_pos += 1;

                    if (sep === '/') {
                        // regexps may have modifiers /regexp/MOD , so fetch those, too
                        // Only [gim] are valid, but if the user puts in garbage, do what we can to take it.
                        while (parser_pos < input_length && acorn.isIdentifierStart(input.charCodeAt(parser_pos))) {
                            resulting_string += input.charAt(parser_pos);
                            parser_pos += 1;
                        }
                    }
                }
                return [resulting_string, 'TK_STRING'];
            }

            if (c === '#') {

                if (tokens.length === 0 && input.charAt(parser_pos) === '!') {
                    // shebang
                    resulting_string = c;
                    while (parser_pos < input_length && c !== '\n') {
                        c = input.charAt(parser_pos);
                        resulting_string += c;
                        parser_pos += 1;
                    }
                    return [trim(resulting_string) + '\n', 'TK_UNKNOWN'];
                }



                // Spidermonkey-specific sharp variables for circular references
                // https://developer.mozilla.org/En/Sharp_variables_in_JavaScript
                // http://mxr.mozilla.org/mozilla-central/source/js/src/jsscan.cpp around line 1935
                var sharp = '#';
                if (parser_pos < input_length && digit.test(input.charAt(parser_pos))) {
                    do {
                        c = input.charAt(parser_pos);
                        sharp += c;
                        parser_pos += 1;
                    } while (parser_pos < input_length && c !== '#' && c !== '=');
                    if (c === '#') {
                        //
                    } else if (input.charAt(parser_pos) === '[' && input.charAt(parser_pos + 1) === ']') {
                        sharp += '[]';
                        parser_pos += 2;
                    } else if (input.charAt(parser_pos) === '{' && input.charAt(parser_pos + 1) === '}') {
                        sharp += '{}';
                        parser_pos += 2;
                    }
                    return [sharp, 'TK_WORD'];
                }
            }

            if (c === '<' && input.substring(parser_pos - 1, parser_pos + 3) === '<!--') {
                parser_pos += 3;
                c = '<!--';
                while (input.charAt(parser_pos) !== '\n' && parser_pos < input_length) {
                    c += input.charAt(parser_pos);
                    parser_pos++;
                }
                in_html_comment = true;
                return [c, 'TK_COMMENT'];
            }

            if (c === '-' && in_html_comment && input.substring(parser_pos - 1, parser_pos + 2) === '-->') {
                in_html_comment = false;
                parser_pos += 2;
                return ['-->', 'TK_COMMENT'];
            }

            if (c === '.') {
                return [c, 'TK_DOT'];
            }

            if (in_array(c, punct)) {
                while (parser_pos < input_length && in_array(c + input.charAt(parser_pos), punct)) {
                    c += input.charAt(parser_pos);
                    parser_pos += 1;
                    if (parser_pos >= input_length) {
                        break;
                    }
                }

                if (c === ',') {
                    return [c, 'TK_COMMA'];
                } else if (c === '=') {
                    return [c, 'TK_EQUALS'];
                } else {
                    return [c, 'TK_OPERATOR'];
                }
            }

            return [c, 'TK_UNKNOWN'];
        }


        function unescape_string(s) {
            var esc = false,
                out = '',
                pos = 0,
                s_hex = '',
                escaped = 0,
                c;

            while (esc || pos < s.length) {

                c = s.charAt(pos);
                pos++;

                if (esc) {
                    esc = false;
                    if (c === 'x') {
                        // simple hex-escape \x24
                        s_hex = s.substr(pos, 2);
                        pos += 2;
                    } else if (c === 'u') {
                        // unicode-escape, \u2134
                        s_hex = s.substr(pos, 4);
                        pos += 4;
                    } else {
                        // some common escape, e.g \n
                        out += '\\' + c;
                        continue;
                    }
                    if (!s_hex.match(/^[0123456789abcdefABCDEF]+$/)) {
                        // some weird escaping, bail out,
                        // leaving whole string intact
                        return s;
                    }

                    escaped = parseInt(s_hex, 16);

                    if (escaped >= 0x00 && escaped < 0x20) {
                        // leave 0x00...0x1f escaped
                        if (c === 'x') {
                            out += '\\x' + s_hex;
                        } else {
                            out += '\\u' + s_hex;
                        }
                        continue;
                    } else if (escaped === 0x22 || escaped === 0x27 || escaped === 0x5c) {
                        // single-quote, apostrophe, backslash - escape these
                        out += '\\' + String.fromCharCode(escaped);
                    } else if (c === 'x' && escaped > 0x7e && escaped <= 0xff) {
                        // we bail out on \x7f..\xff,
                        // leaving whole string escaped,
                        // as it's probably completely binary
                        return s;
                    } else {
                        out += String.fromCharCode(escaped);
                    }
                } else if (c === '\\') {
                    esc = true;
                } else {
                    out += c;
                }
            }
            return out;
        }

    }


    if (typeof define === "function" && define.amd) {
        // Add support for AMD ( https://github.com/amdjs/amdjs-api/wiki/AMD#defineamd-property- )
        define([], function() {
            return { js_beautify: js_beautify };
        });
    } else if (typeof exports !== "undefined") {
        // Add support for CommonJS. Just put this file somewhere on your require.paths
        // and you will be able to `var js_beautify = require("beautify").js_beautify`.
        exports.js_beautify = js_beautify;
    } else if (typeof window !== "undefined") {
        // If we're running a web page and don't have either of the above, add our one global
        window.js_beautify = js_beautify;
    } else if (typeof global !== "undefined") {
        // If we don't even have window, try global.
        global.js_beautify = js_beautify;
    }

}());
/*jshint curly:true, eqeqeq:true, laxbreak:true, noempty:false */
/*

  The MIT License (MIT)

  Copyright (c) 2007-2013 Einar Lielmanis and contributors.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.


 CSS Beautifier
---------------

    Written by Harutyun Amirjanyan, (amirjanyan@gmail.com)

    Based on code initially developed by: Einar Lielmanis, <einar@jsbeautifier.org>
        http://jsbeautifier.org/

    Usage:
        css_beautify(source_text);
        css_beautify(source_text, options);

    The options are (default in brackets):
        indent_size (4)                   — indentation size,
        indent_char (space)               — character to indent with,
        selector_separator_newline (true) - separate selectors with newline or
                                            not (e.g. "a,\nbr" or "a, br")
        end_with_newline (false)          - end with a newline

    e.g

    css_beautify(css_source_text, {
      'indent_size': 1,
      'indent_char': '\t',
      'selector_separator': ' ',
      'end_with_newline': false,
    });
*/

// http://www.w3.org/TR/CSS21/syndata.html#tokenization
// http://www.w3.org/TR/css3-syntax/

(function() {
    function css_beautify(source_text, options) {
        options = options || {};
        var indentSize = options.indent_size || 4;
        var indentCharacter = options.indent_char || ' ';
        var selectorSeparatorNewline = (options.selector_separator_newline === undefined) ? true : options.selector_separator_newline;
        var end_with_newline = (options.end_with_newline === undefined) ? false : options.end_with_newline;

        // compatibility
        if (typeof indentSize === "string") {
            indentSize = parseInt(indentSize, 10);
        }


        // tokenizer
        var whiteRe = /^\s+$/;
        var wordRe = /[\w$\-_]/;

        var pos = -1,
            ch;

        function next() {
            ch = source_text.charAt(++pos);
            return ch || '';
        }

        function peek(skipWhitespace) {
            var prev_pos = pos;
            if (skipWhitespace) {
                eatWhitespace();
            }
            result = source_text.charAt(pos + 1) || '';
            pos = prev_pos - 1;
            next();
            return result;
        }

        function eatString(endChars) {
            var start = pos;
            while (next()) {
                if (ch === "\\") {
                    next();
                } else if (endChars.indexOf(ch) !== -1) {
                    break;
                } else if (ch === "\n") {
                    break;
                }
            }
            return source_text.substring(start, pos + 1);
        }

        function peekString(endChar) {
            var prev_pos = pos;
            var str = eatString(endChar);
            pos = prev_pos - 1;
            next();
            return str;
        }

        function eatWhitespace() {
            var result = '';
            while (whiteRe.test(peek())) {
                next()
                result += ch;
            }
            return result;
        }

        function skipWhitespace() {
            var result = '';
            if (ch && whiteRe.test(ch)) {
                result = ch;
            }
            while (whiteRe.test(next())) {
                result += ch
            }
            return result;
        }

        function eatComment(singleLine) {
            var start = pos;
            var singleLine = peek() === "/";
            next();
            while (next()) {
                if (!singleLine && ch === "*" && peek() === "/") {
                    next();
                    break;
                } else if (singleLine && ch === "\n") {
                    return source_text.substring(start, pos);
                }
            }

            return source_text.substring(start, pos) + ch;
        }


        function lookBack(str) {
            return source_text.substring(pos - str.length, pos).toLowerCase() ===
                str;
        }

        // Nested pseudo-class if we are insideRule
        // and the next special character found opens
        // a new block
        function foundNestedPseudoClass() {
            for (var i = pos + 1; i < source_text.length; i++){
                var ch = source_text.charAt(i);
                if (ch === "{"){
                    return true;
                } else if (ch === ";" || ch === "}" || ch === ")") {
                    return false;
                }
            }
            return false;
        }

        // printer
        var basebaseIndentString = source_text.match(/^[\t ]*/)[0];
        var singleIndent = new Array(indentSize + 1).join(indentCharacter);
        var indentLevel = 0;
        var nestedLevel = 0;

        function indent() {
            indentLevel++;
            basebaseIndentString += singleIndent;
        }

        function outdent() {
            indentLevel--;
            basebaseIndentString = basebaseIndentString.slice(0, -indentSize);
        }

        var print = {};
        print["{"] = function(ch) {
            print.singleSpace();
            output.push(ch);
            print.newLine();
        };
        print["}"] = function(ch) {
            print.newLine();
            output.push(ch);
            print.newLine();
        };

        print._lastCharWhitespace = function() {
            return whiteRe.test(output[output.length - 1]);
        };

        print.newLine = function(keepWhitespace) {
            if (!keepWhitespace) {
                print.trim();
            }

            if (output.length) {
                output.push('\n');
            }
            if (basebaseIndentString) {
                output.push(basebaseIndentString);
            }
        };
        print.singleSpace = function() {
            if (output.length && !print._lastCharWhitespace()) {
                output.push(' ');
            }
        };

        print.trim = function() {
            while (print._lastCharWhitespace()) {
                output.pop();
            }
        };

        
        var output = [];
        if (basebaseIndentString) {
            output.push(basebaseIndentString);
        }
        /*_____________________--------------------_____________________*/

        var insideRule = false;
        var enteringConditionalGroup = false;
        var top_ch = '';
        var last_top_ch = '';

        while (true) {
            var whitespace = skipWhitespace();
            var isAfterSpace = whitespace !== '';
            var isAfterNewline = whitespace.indexOf('\n') !== -1;
            var last_top_ch = top_ch;
            var top_ch = ch;

            if (!ch) {
                break;
            } else if (ch === '/' && peek() === '*') { /* css comment */
                var header = lookBack("");
                print.newLine();
                output.push(eatComment());
                print.newLine();
                if (header) {
                    print.newLine(true);
                }
            } else if (ch === '/' && peek() === '/') { // single line comment
                if (!isAfterNewline && last_top_ch !== '{') {
                    print.trim();
                }
                print.singleSpace();
                output.push(eatComment());
                print.newLine();
            } else if (ch === '@') {
                // pass along the space we found as a separate item
                if (isAfterSpace) {
                    print.singleSpace();
                }
                output.push(ch);

                // strip trailing space, if present, for hash property checks
                var variableOrRule = peekString(": ,;{}()[]/='\"").replace(/\s$/, '');

                // might be a nesting at-rule
                if (variableOrRule in css_beautify.NESTED_AT_RULE) {
                    nestedLevel += 1;
                    if (variableOrRule in css_beautify.CONDITIONAL_GROUP_RULE) {
                        enteringConditionalGroup = true;
                    }
                } else if (': '.indexOf(variableOrRule[variableOrRule.length -1]) >= 0) {
                    //we have a variable, add it and insert one space before continuing
                    next();
                    variableOrRule = eatString(": ").replace(/\s$/, '');
                    output.push(variableOrRule);
                    print.singleSpace();
                }
            } else if (ch === '{') {
                if (peek(true) === '}') {
                    eatWhitespace();
                    next();
                    print.singleSpace();
                    output.push("{}");
                } else {
                    indent();
                    print["{"](ch);
                    // when entering conditional groups, only rulesets are allowed
                    if (enteringConditionalGroup) {
                        enteringConditionalGroup = false;
                        insideRule = (indentLevel > nestedLevel);
                    } else {
                        // otherwise, declarations are also allowed
                        insideRule = (indentLevel >= nestedLevel);
                    }
                }
            } else if (ch === '}') {
                outdent();
                print["}"](ch);
                insideRule = false;
                if (nestedLevel) {
                    nestedLevel--;
                }
            } else if (ch === ":") {
                eatWhitespace();
                if ((insideRule || enteringConditionalGroup) && 
                        !(lookBack("&") || foundNestedPseudoClass())) {
                    // 'property: value' delimiter
                    // which could be in a conditional group query
                    output.push(':');
                    print.singleSpace();
                } else {
                    // sass/less parent reference don't use a space
                    // sass nested pseudo-class don't use a space
                    if (peek() === ":") {
                        // pseudo-element
                        next();
                        output.push("::");
                    } else {
                        // pseudo-class
                        output.push(':');
                    }
                }
            } else if (ch === '"' || ch === '\'') {
                if (isAfterSpace) {
                    print.singleSpace();
                }
                output.push(eatString(ch));
            } else if (ch === ';') {
                output.push(ch);
                print.newLine();
            } else if (ch === '(') { // may be a url
                if (lookBack("url")) {
                    output.push(ch);
                    eatWhitespace();
                    if (next()) {
                        if (ch !== ')' && ch !== '"' && ch !== '\'') {
                            output.push(eatString(')'));
                        } else {
                            pos--;
                        }
                    }
                } else {
                    if (isAfterSpace) {
                        print.singleSpace();
                    }
                    output.push(ch);
                    eatWhitespace();
                }
            } else if (ch === ')') {
                output.push(ch);
            } else if (ch === ',') {
                output.push(ch);
                eatWhitespace();
                if (!insideRule && selectorSeparatorNewline) {
                    print.newLine();
                } else {
                    print.singleSpace();
                }
            } else if (ch === ']') {
                output.push(ch);
            } else if (ch === '[') {
                if (isAfterSpace) {
                    print.singleSpace();
                }
                output.push(ch);
            } else if (ch === '=') { // no whitespace before or after
                eatWhitespace();
                output.push(ch);
            } else {
                if (isAfterSpace) {
                    print.singleSpace();
                }

                output.push(ch);
            }
        }


        var sweetCode = output.join('').replace(/[\r\n\t ]+$/, '');

        // establish end_with_newline
        if (end_with_newline) {
            sweetCode += "\n";
        }

        return sweetCode;
    }

    // https://developer.mozilla.org/en-US/docs/Web/CSS/At-rule
    css_beautify.NESTED_AT_RULE = {
        "@page": true,
        "@font-face": true,
        "@keyframes": true,
        // also in CONDITIONAL_GROUP_RULE below
        "@media": true,
        "@supports": true,
        "@document": true
    };
    css_beautify.CONDITIONAL_GROUP_RULE = {
        "@media": true,
        "@supports": true,
        "@document": true
    };

    /*global define */
    if (typeof define === "function" && define.amd) {
        // Add support for AMD ( https://github.com/amdjs/amdjs-api/wiki/AMD#defineamd-property- )
        define([], function() {
            return {
                css_beautify: css_beautify
            };
        });
    } else if (typeof exports !== "undefined") {
        // Add support for CommonJS. Just put this file somewhere on your require.paths
        // and you will be able to `var html_beautify = require("beautify").html_beautify`.
        exports.css_beautify = css_beautify;
    } else if (typeof window !== "undefined") {
        // If we're running a web page and don't have either of the above, add our one global
        window.css_beautify = css_beautify;
    } else if (typeof global !== "undefined") {
        // If we don't even have window, try global.
        global.css_beautify = css_beautify;
    }

}());
/*jshint curly:true, eqeqeq:true, laxbreak:true, noempty:false */
/*

  The MIT License (MIT)

  Copyright (c) 2007-2013 Einar Lielmanis and contributors.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.


 Style HTML
---------------

  Written by Nochum Sossonko, (nsossonko@hotmail.com)

  Based on code initially developed by: Einar Lielmanis, <einar@jsbeautifier.org>
    http://jsbeautifier.org/

  Usage:
    style_html(html_source);

    style_html(html_source, options);

  The options are:
    indent_inner_html (default false)  — indent <head> and <body> sections,
    indent_size (default 4)          — indentation size,
    indent_char (default space)      — character to indent with,
    wrap_line_length (default 250)            -  maximum amount of characters per line (0 = disable)
    brace_style (default "collapse") - "collapse" | "expand" | "end-expand" | "none"
            put braces on the same line as control statements (default), or put braces on own line (Allman / ANSI style), or just put end braces on own line, or attempt to keep them where they are.
    unformatted (defaults to inline tags) - list of tags, that shouldn't be reformatted
    indent_scripts (default normal)  - "keep"|"separate"|"normal"
    preserve_newlines (default true) - whether existing line breaks before elements should be preserved
                                        Only works before elements, not inside tags or for text.
    max_preserve_newlines (default unlimited) - maximum number of line breaks to be preserved in one chunk
    indent_handlebars (default false) - format and indent {{#foo}} and {{/foo}}
    end_with_newline (false)          - end with a newline


    e.g.

    style_html(html_source, {
      'indent_inner_html': false,
      'indent_size': 2,
      'indent_char': ' ',
      'wrap_line_length': 78,
      'brace_style': 'expand',
      'unformatted': ['a', 'sub', 'sup', 'b', 'i', 'u'],
      'preserve_newlines': true,
      'max_preserve_newlines': 5,
      'indent_handlebars': false
    });
*/

(function() {

    function trim(s) {
        return s.replace(/^\s+|\s+$/g, '');
    }

    function ltrim(s) {
        return s.replace(/^\s+/g, '');
    }

    function rtrim(s) {
        return s.replace(/\s+$/g,'');
    }

    function style_html(html_source, options, js_beautify, css_beautify) {
        //Wrapper function to invoke all the necessary constructors and deal with the output.

        var multi_parser,
            indent_inner_html,
            indent_size,
            indent_character,
            wrap_line_length,
            brace_style,
            unformatted,
            preserve_newlines,
            max_preserve_newlines,
            indent_handlebars,
            end_with_newline;

        options = options || {};

        // backwards compatibility to 1.3.4
        if ((options.wrap_line_length === undefined || parseInt(options.wrap_line_length, 10) === 0) &&
                (options.max_char !== undefined && parseInt(options.max_char, 10) !== 0)) {
            options.wrap_line_length = options.max_char;
        }

        indent_inner_html = (options.indent_inner_html === undefined) ? false : options.indent_inner_html;
        indent_size = (options.indent_size === undefined) ? 4 : parseInt(options.indent_size, 10);
        indent_character = (options.indent_char === undefined) ? ' ' : options.indent_char;
        brace_style = (options.brace_style === undefined) ? 'collapse' : options.brace_style;
        wrap_line_length =  parseInt(options.wrap_line_length, 10) === 0 ? 32786 : parseInt(options.wrap_line_length || 250, 10);
        unformatted = options.unformatted || ['a', 'span', 'img', 'bdo', 'em', 'strong', 'dfn', 'code', 'samp', 'kbd', 'var', 'cite', 'abbr', 'acronym', 'q', 'sub', 'sup', 'tt', 'i', 'b', 'big', 'small', 'u', 's', 'strike', 'font', 'ins', 'del', 'pre', 'address', 'dt', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
        preserve_newlines = (options.preserve_newlines === undefined) ? true : options.preserve_newlines;
        max_preserve_newlines = preserve_newlines ?
            (isNaN(parseInt(options.max_preserve_newlines, 10)) ? 32786 : parseInt(options.max_preserve_newlines, 10))
            : 0;
        indent_handlebars = (options.indent_handlebars === undefined) ? false : options.indent_handlebars;
        end_with_newline = (options.end_with_newline === undefined) ? false : options.end_with_newline;

        function Parser() {

            this.pos = 0; //Parser position
            this.token = '';
            this.current_mode = 'CONTENT'; //reflects the current Parser mode: TAG/CONTENT
            this.tags = { //An object to hold tags, their position, and their parent-tags, initiated with default values
                parent: 'parent1',
                parentcount: 1,
                parent1: ''
            };
            this.tag_type = '';
            this.token_text = this.last_token = this.last_text = this.token_type = '';
            this.newlines = 0;
            this.indent_content = indent_inner_html;

            this.Utils = { //Uilities made available to the various functions
                whitespace: "\n\r\t ".split(''),
                single_token: 'br,input,link,meta,!doctype,basefont,base,area,hr,wbr,param,img,isindex,?xml,embed,?php,?,?='.split(','), //all the single tags for HTML
                extra_liners: 'head,body,/html'.split(','), //for tags that need a line of whitespace before them
                in_array: function(what, arr) {
                    for (var i = 0; i < arr.length; i++) {
                        if (what === arr[i]) {
                            return true;
                        }
                    }
                    return false;
                }
            };

            // Return true iff the given text is composed entirely of
            // whitespace.
            this.is_whitespace = function(text) {
                for (var n = 0; n < text.length; text++) {
                    if (!this.Utils.in_array(text.charAt(n), this.Utils.whitespace)) {
                        return false;
                    }
                }
                return true;
            }

            this.traverse_whitespace = function() {
                var input_char = '';

                input_char = this.input.charAt(this.pos);
                if (this.Utils.in_array(input_char, this.Utils.whitespace)) {
                    this.newlines = 0;
                    while (this.Utils.in_array(input_char, this.Utils.whitespace)) {
                        if (preserve_newlines && input_char === '\n' && this.newlines <= max_preserve_newlines) {
                            this.newlines += 1;
                        }

                        this.pos++;
                        input_char = this.input.charAt(this.pos);
                    }
                    return true;
                }
                return false;
            };

            // Append a space to the given content (string array) or, if we are
            // at the wrap_line_length, append a newline/indentation.
            this.space_or_wrap = function(content) {
                if (this.line_char_count >= this.wrap_line_length) { //insert a line when the wrap_line_length is reached
                    this.print_newline(false, content);
                    this.print_indentation(content);
                } else {
                    this.line_char_count++;
                    content.push(' ');
                }
            };

            this.get_content = function() { //function to capture regular content between tags
                var input_char = '',
                    content = [],
                    space = false; //if a space is needed

                while (this.input.charAt(this.pos) !== '<') {
                    if (this.pos >= this.input.length) {
                        return content.length ? content.join('') : ['', 'TK_EOF'];
                    }

                    if (this.traverse_whitespace()) {
                        this.space_or_wrap(content);
                        continue;
                    }

                    if (indent_handlebars) {
                        // Handlebars parsing is complicated.
                        // {{#foo}} and {{/foo}} are formatted tags.
                        // {{something}} should get treated as content, except:
                        // {{else}} specifically behaves like {{#if}} and {{/if}}
                        var peek3 = this.input.substr(this.pos, 3);
                        if (peek3 === '{{#' || peek3 === '{{/') {
                            // These are tags and not content.
                            break;
                        } else if (this.input.substr(this.pos, 2) === '{{') {
                            if (this.get_tag(true) === '{{else}}') {
                                break;
                            }
                        }
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;
                    this.line_char_count++;
                    content.push(input_char); //letter at-a-time (or string) inserted to an array
                }
                return content.length ? content.join('') : '';
            };

            this.get_contents_to = function(name) { //get the full content of a script or style to pass to js_beautify
                if (this.pos === this.input.length) {
                    return ['', 'TK_EOF'];
                }
                var input_char = '';
                var content = '';
                var reg_match = new RegExp('</' + name + '\\s*>', 'igm');
                reg_match.lastIndex = this.pos;
                var reg_array = reg_match.exec(this.input);
                var end_script = reg_array ? reg_array.index : this.input.length; //absolute end of script
                if (this.pos < end_script) { //get everything in between the script tags
                    content = this.input.substring(this.pos, end_script);
                    this.pos = end_script;
                }
                return content;
            };

            this.record_tag = function(tag) { //function to record a tag and its parent in this.tags Object
                if (this.tags[tag + 'count']) { //check for the existence of this tag type
                    this.tags[tag + 'count']++;
                    this.tags[tag + this.tags[tag + 'count']] = this.indent_level; //and record the present indent level
                } else { //otherwise initialize this tag type
                    this.tags[tag + 'count'] = 1;
                    this.tags[tag + this.tags[tag + 'count']] = this.indent_level; //and record the present indent level
                }
                this.tags[tag + this.tags[tag + 'count'] + 'parent'] = this.tags.parent; //set the parent (i.e. in the case of a div this.tags.div1parent)
                this.tags.parent = tag + this.tags[tag + 'count']; //and make this the current parent (i.e. in the case of a div 'div1')
            };

            this.retrieve_tag = function(tag) { //function to retrieve the opening tag to the corresponding closer
                if (this.tags[tag + 'count']) { //if the openener is not in the Object we ignore it
                    var temp_parent = this.tags.parent; //check to see if it's a closable tag.
                    while (temp_parent) { //till we reach '' (the initial value);
                        if (tag + this.tags[tag + 'count'] === temp_parent) { //if this is it use it
                            break;
                        }
                        temp_parent = this.tags[temp_parent + 'parent']; //otherwise keep on climbing up the DOM Tree
                    }
                    if (temp_parent) { //if we caught something
                        this.indent_level = this.tags[tag + this.tags[tag + 'count']]; //set the indent_level accordingly
                        this.tags.parent = this.tags[temp_parent + 'parent']; //and set the current parent
                    }
                    delete this.tags[tag + this.tags[tag + 'count'] + 'parent']; //delete the closed tags parent reference...
                    delete this.tags[tag + this.tags[tag + 'count']]; //...and the tag itself
                    if (this.tags[tag + 'count'] === 1) {
                        delete this.tags[tag + 'count'];
                    } else {
                        this.tags[tag + 'count']--;
                    }
                }
            };

            this.indent_to_tag = function(tag) {
                // Match the indentation level to the last use of this tag, but don't remove it.
                if (!this.tags[tag + 'count']) {
                    return;
                }
                var temp_parent = this.tags.parent;
                while (temp_parent) {
                    if (tag + this.tags[tag + 'count'] === temp_parent) {
                        break;
                    }
                    temp_parent = this.tags[temp_parent + 'parent'];
                }
                if (temp_parent) {
                    this.indent_level = this.tags[tag + this.tags[tag + 'count']];
                }
            };

            this.get_tag = function(peek) { //function to get a full tag and parse its type
                var input_char = '',
                    content = [],
                    comment = '',
                    space = false,
                    tag_start, tag_end,
                    tag_start_char,
                    orig_pos = this.pos,
                    orig_line_char_count = this.line_char_count;

                peek = peek !== undefined ? peek : false;

                do {
                    if (this.pos >= this.input.length) {
                        if (peek) {
                            this.pos = orig_pos;
                            this.line_char_count = orig_line_char_count;
                        }
                        return content.length ? content.join('') : ['', 'TK_EOF'];
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;

                    if (this.Utils.in_array(input_char, this.Utils.whitespace)) { //don't want to insert unnecessary space
                        space = true;
                        continue;
                    }

                    if (input_char === "'" || input_char === '"') {
                        input_char += this.get_unformatted(input_char);
                        space = true;

                    }

                    if (input_char === '=') { //no space before =
                        space = false;
                    }

                    if (content.length && content[content.length - 1] !== '=' && input_char !== '>' && space) {
                        //no space after = or before >
                        this.space_or_wrap(content);
                        space = false;
                    }

                    if (indent_handlebars && tag_start_char === '<') {
                        // When inside an angle-bracket tag, put spaces around
                        // handlebars not inside of strings.
                        if ((input_char + this.input.charAt(this.pos)) === '{{') {
                            input_char += this.get_unformatted('}}');
                            if (content.length && content[content.length - 1] !== ' ' && content[content.length - 1] !== '<') {
                                input_char = ' ' + input_char;
                            }
                            space = true;
                        }
                    }

                    if (input_char === '<' && !tag_start_char) {
                        tag_start = this.pos - 1;
                        tag_start_char = '<';
                    }

                    if (indent_handlebars && !tag_start_char) {
                        if (content.length >= 2 && content[content.length - 1] === '{' && content[content.length - 2] == '{') {
                            if (input_char === '#' || input_char === '/') {
                                tag_start = this.pos - 3;
                            } else {
                                tag_start = this.pos - 2;
                            }
                            tag_start_char = '{';
                        }
                    }

                    this.line_char_count++;
                    content.push(input_char); //inserts character at-a-time (or string)

                    if (content[1] && content[1] === '!') { //if we're in a comment, do something special
                        // We treat all comments as literals, even more than preformatted tags
                        // we just look for the appropriate close tag
                        content = [this.get_comment(tag_start)];
                        break;
                    }

                    if (indent_handlebars && tag_start_char === '{' && content.length > 2 && content[content.length - 2] === '}' && content[content.length - 1] === '}') {
                        break;
                    }
                } while (input_char !== '>');

                var tag_complete = content.join('');
                var tag_index;
                var tag_offset;

                if (tag_complete.indexOf(' ') !== -1) { //if there's whitespace, thats where the tag name ends
                    tag_index = tag_complete.indexOf(' ');
                } else if (tag_complete[0] === '{') {
                    tag_index = tag_complete.indexOf('}');
                } else { //otherwise go with the tag ending
                    tag_index = tag_complete.indexOf('>');
                }
                if (tag_complete[0] === '<' || !indent_handlebars) {
                    tag_offset = 1;
                } else {
                    tag_offset = tag_complete[2] === '#' ? 3 : 2;
                }
                var tag_check = tag_complete.substring(tag_offset, tag_index).toLowerCase();
                if (tag_complete.charAt(tag_complete.length - 2) === '/' ||
                    this.Utils.in_array(tag_check, this.Utils.single_token)) { //if this tag name is a single tag type (either in the list or has a closing /)
                    if (!peek) {
                        this.tag_type = 'SINGLE';
                    }
                } else if (indent_handlebars && tag_complete[0] === '{' && tag_check === 'else') {
                    if (!peek) {
                        this.indent_to_tag('if');
                        this.tag_type = 'HANDLEBARS_ELSE';
                        this.indent_content = true;
                        this.traverse_whitespace();
                    }
                } else if (this.is_unformatted(tag_check, unformatted)) { // do not reformat the "unformatted" tags
                    comment = this.get_unformatted('</' + tag_check + '>', tag_complete); //...delegate to get_unformatted function
                    content.push(comment);
                    tag_end = this.pos - 1;
                    this.tag_type = 'SINGLE';
                } else if (tag_check === 'script' &&
                    (tag_complete.search('type') === -1 ||
                    (tag_complete.search('type') > -1 &&
                    tag_complete.search(/\b(text|application)\/(x-)?(javascript|ecmascript|jscript|livescript)/) > -1))) {
                    if (!peek) {
                        this.record_tag(tag_check);
                        this.tag_type = 'SCRIPT';
                    }
                } else if (tag_check === 'style' &&
                    (tag_complete.search('type') === -1 ||
                    (tag_complete.search('type') > -1 && tag_complete.search('text/css') > -1))) {
                    if (!peek) {
                        this.record_tag(tag_check);
                        this.tag_type = 'STYLE';
                    }
                } else if (tag_check.charAt(0) === '!') { //peek for <! comment
                    // for comments content is already correct.
                    if (!peek) {
                        this.tag_type = 'SINGLE';
                        this.traverse_whitespace();
                    }
                } else if (!peek) {
                    if (tag_check.charAt(0) === '/') { //this tag is a double tag so check for tag-ending
                        this.retrieve_tag(tag_check.substring(1)); //remove it and all ancestors
                        this.tag_type = 'END';
                    } else { //otherwise it's a start-tag
                        this.record_tag(tag_check); //push it on the tag stack
                        if (tag_check.toLowerCase() !== 'html') {
                            this.indent_content = true;
                        }
                        this.tag_type = 'START';
                    }

                    // Allow preserving of newlines after a start or end tag
                    if (this.traverse_whitespace()) {
                        this.space_or_wrap(content);
                    }

                    if (this.Utils.in_array(tag_check, this.Utils.extra_liners)) { //check if this double needs an extra line
                        this.print_newline(false, this.output);
                        if (this.output.length && this.output[this.output.length - 2] !== '\n') {
                            this.print_newline(true, this.output);
                        }
                    }
                }

                if (peek) {
                    this.pos = orig_pos;
                    this.line_char_count = orig_line_char_count;
                }

                return content.join(''); //returns fully formatted tag
            };

            this.get_comment = function(start_pos) { //function to return comment content in its entirety
                // this is will have very poor perf, but will work for now.
                var comment = '',
                    delimiter = '>',
                    matched = false;

                this.pos = start_pos;
                input_char = this.input.charAt(this.pos);
                this.pos++;

                while (this.pos <= this.input.length) {
                    comment += input_char;

                    // only need to check for the delimiter if the last chars match
                    if (comment[comment.length - 1] === delimiter[delimiter.length - 1] &&
                        comment.indexOf(delimiter) !== -1) {
                        break;
                    }

                    // only need to search for custom delimiter for the first few characters
                    if (!matched && comment.length < 10) {
                        if (comment.indexOf('<![if') === 0) { //peek for <![if conditional comment
                            delimiter = '<![endif]>';
                            matched = true;
                        } else if (comment.indexOf('<![cdata[') === 0) { //if it's a <[cdata[ comment...
                            delimiter = ']]>';
                            matched = true;
                        } else if (comment.indexOf('<![') === 0) { // some other ![ comment? ...
                            delimiter = ']>';
                            matched = true;
                        } else if (comment.indexOf('<!--') === 0) { // <!-- comment ...
                            delimiter = '-->';
                            matched = true;
                        }
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;
                }

                return comment;
            };

            this.get_unformatted = function(delimiter, orig_tag) { //function to return unformatted content in its entirety

                if (orig_tag && orig_tag.toLowerCase().indexOf(delimiter) !== -1) {
                    return '';
                }
                var input_char = '';
                var content = '';
                var min_index = 0;
                var space = true;
                do {

                    if (this.pos >= this.input.length) {
                        return content;
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;

                    if (this.Utils.in_array(input_char, this.Utils.whitespace)) {
                        if (!space) {
                            this.line_char_count--;
                            continue;
                        }
                        if (input_char === '\n' || input_char === '\r') {
                            content += '\n';
                            /*  Don't change tab indention for unformatted blocks.  If using code for html editing, this will greatly affect <pre> tags if they are specified in the 'unformatted array'
                for (var i=0; i<this.indent_level; i++) {
                  content += this.indent_string;
                }
                space = false; //...and make sure other indentation is erased
                */
                            this.line_char_count = 0;
                            continue;
                        }
                    }
                    content += input_char;
                    this.line_char_count++;
                    space = true;

                    if (indent_handlebars && input_char === '{' && content.length && content[content.length - 2] === '{') {
                        // Handlebars expressions in strings should also be unformatted.
                        content += this.get_unformatted('}}');
                        // These expressions are opaque.  Ignore delimiters found in them.
                        min_index = content.length;
                    }
                } while (content.toLowerCase().indexOf(delimiter, min_index) === -1);
                return content;
            };

            this.get_token = function() { //initial handler for token-retrieval
                var token;

                if (this.last_token === 'TK_TAG_SCRIPT' || this.last_token === 'TK_TAG_STYLE') { //check if we need to format javascript
                    var type = this.last_token.substr(7);
                    token = this.get_contents_to(type);
                    if (typeof token !== 'string') {
                        return token;
                    }
                    return [token, 'TK_' + type];
                }
                if (this.current_mode === 'CONTENT') {
                    token = this.get_content();
                    if (typeof token !== 'string') {
                        return token;
                    } else {
                        return [token, 'TK_CONTENT'];
                    }
                }

                if (this.current_mode === 'TAG') {
                    token = this.get_tag();
                    if (typeof token !== 'string') {
                        return token;
                    } else {
                        var tag_name_type = 'TK_TAG_' + this.tag_type;
                        return [token, tag_name_type];
                    }
                }
            };

            this.get_full_indent = function(level) {
                level = this.indent_level + level || 0;
                if (level < 1) {
                    return '';
                }

                return Array(level + 1).join(this.indent_string);
            };

            this.is_unformatted = function(tag_check, unformatted) {
                //is this an HTML5 block-level link?
                if (!this.Utils.in_array(tag_check, unformatted)) {
                    return false;
                }

                if (tag_check.toLowerCase() !== 'a' || !this.Utils.in_array('a', unformatted)) {
                    return true;
                }

                //at this point we have an  tag; is its first child something we want to remain
                //unformatted?
                var next_tag = this.get_tag(true /* peek. */ );

                // test next_tag to see if it is just html tag (no external content)
                var tag = (next_tag || "").match(/^\s*<\s*\/?([a-z]*)\s*[^>]*>\s*$/);

                // if next_tag comes back but is not an isolated tag, then
                // let's treat the 'a' tag as having content
                // and respect the unformatted option
                if (!tag || this.Utils.in_array(tag, unformatted)) {
                    return true;
                } else {
                    return false;
                }
            };

            this.printer = function(js_source, indent_character, indent_size, wrap_line_length, brace_style) { //handles input/output and some other printing functions

                this.input = js_source || ''; //gets the input for the Parser
                this.output = [];
                this.indent_character = indent_character;
                this.indent_string = '';
                this.indent_size = indent_size;
                this.brace_style = brace_style;
                this.indent_level = 0;
                this.wrap_line_length = wrap_line_length;
                this.line_char_count = 0; //count to see if wrap_line_length was exceeded

                for (var i = 0; i < this.indent_size; i++) {
                    this.indent_string += this.indent_character;
                }

                this.print_newline = function(force, arr) {
                    this.line_char_count = 0;
                    if (!arr || !arr.length) {
                        return;
                    }
                    if (force || (arr[arr.length - 1] !== '\n')) { //we might want the extra line
                        if ((arr[arr.length - 1] !== '\n')) {
                            arr[arr.length - 1] = rtrim(arr[arr.length - 1]);
                        }
                        arr.push('\n');
                    }
                };

                this.print_indentation = function(arr) {
                    for (var i = 0; i < this.indent_level; i++) {
                        arr.push(this.indent_string);
                        this.line_char_count += this.indent_string.length;
                    }
                };

                this.print_token = function(text) {
                    // Avoid printing initial whitespace.
                    if (this.is_whitespace(text) && !this.output.length) {
                        return;
                    }
                    if (text || text !== '') {
                        if (this.output.length && this.output[this.output.length - 1] === '\n') {
                            this.print_indentation(this.output);
                            text = ltrim(text);
                        }
                    }
                    this.print_token_raw(text);
                };

                this.print_token_raw = function(text) {
                    // If we are going to print newlines, truncate trailing
                    // whitespace, as the newlines will represent the space.
                    if (this.newlines > 0) {
                        text = rtrim(text);
                    }

                    if (text && text !== '') {
                        if (text.length > 1 && text[text.length - 1] === '\n') {
                            // unformatted tags can grab newlines as their last character
                            this.output.push(text.slice(0, -1));
                            this.print_newline(false, this.output);
                        } else {
                            this.output.push(text);
                        }
                    }

                    for (var n = 0; n < this.newlines; n++) {
                        this.print_newline(n > 0, this.output);
                    }
                    this.newlines = 0;
                };

                this.indent = function() {
                    this.indent_level++;
                };

                this.unindent = function() {
                    if (this.indent_level > 0) {
                        this.indent_level--;
                    }
                };
            };
            return this;
        }

        /*_____________________--------------------_____________________*/

        multi_parser = new Parser(); //wrapping functions Parser
        multi_parser.printer(html_source, indent_character, indent_size, wrap_line_length, brace_style); //initialize starting values

        while (true) {
            var t = multi_parser.get_token();
            multi_parser.token_text = t[0];
            multi_parser.token_type = t[1];

            if (multi_parser.token_type === 'TK_EOF') {
                break;
            }

            switch (multi_parser.token_type) {
                case 'TK_TAG_START':
                    multi_parser.print_newline(false, multi_parser.output);
                    multi_parser.print_token(multi_parser.token_text);
                    if (multi_parser.indent_content) {
                        multi_parser.indent();
                        multi_parser.indent_content = false;
                    }
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_STYLE':
                case 'TK_TAG_SCRIPT':
                    multi_parser.print_newline(false, multi_parser.output);
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_END':
                    //Print new line only if the tag has no content and has child
                    if (multi_parser.last_token === 'TK_CONTENT' && multi_parser.last_text === '') {
                        var tag_name = multi_parser.token_text.match(/\w+/)[0];
                        var tag_extracted_from_last_output = null;
                        if (multi_parser.output.length) {
                            tag_extracted_from_last_output = multi_parser.output[multi_parser.output.length - 1].match(/(?:<|{{#)\s*(\w+)/);
                        }
                        if (tag_extracted_from_last_output === null ||
                            tag_extracted_from_last_output[1] !== tag_name) {
                            multi_parser.print_newline(false, multi_parser.output);
                        }
                    }
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_SINGLE':
                    // Don't add a newline before elements that should remain unformatted.
                    var tag_check = multi_parser.token_text.match(/^\s*<([a-z-]+)/i);
                    if (!tag_check || !multi_parser.Utils.in_array(tag_check[1], unformatted)) {
                        multi_parser.print_newline(false, multi_parser.output);
                    }
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_HANDLEBARS_ELSE':
                    multi_parser.print_token(multi_parser.token_text);
                    if (multi_parser.indent_content) {
                        multi_parser.indent();
                        multi_parser.indent_content = false;
                    }
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_CONTENT':
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'TAG';
                    break;
                case 'TK_STYLE':
                case 'TK_SCRIPT':
                    if (multi_parser.token_text !== '') {
                        multi_parser.print_newline(false, multi_parser.output);
                        var text = multi_parser.token_text,
                            _beautifier,
                            script_indent_level = 1;
                        if (multi_parser.token_type === 'TK_SCRIPT') {
                            _beautifier = typeof js_beautify === 'function' && js_beautify;
                        } else if (multi_parser.token_type === 'TK_STYLE') {
                            _beautifier = typeof css_beautify === 'function' && css_beautify;
                        }

                        if (options.indent_scripts === "keep") {
                            script_indent_level = 0;
                        } else if (options.indent_scripts === "separate") {
                            script_indent_level = -multi_parser.indent_level;
                        }

                        var indentation = multi_parser.get_full_indent(script_indent_level);
                        if (_beautifier) {
                            // call the Beautifier if avaliable
                            text = _beautifier(text.replace(/^\s*/, indentation), options);
                        } else {
                            // simply indent the string otherwise
                            var white = text.match(/^\s*/)[0];
                            var _level = white.match(/[^\n\r]*$/)[0].split(multi_parser.indent_string).length - 1;
                            var reindent = multi_parser.get_full_indent(script_indent_level - _level);
                            text = text.replace(/^\s*/, indentation)
                                .replace(/\r\n|\r|\n/g, '\n' + reindent)
                                .replace(/\s+$/, '');
                        }
                        if (text) {
                            multi_parser.print_token_raw(text);
                            multi_parser.print_newline(true, multi_parser.output);
                        }
                    }
                    multi_parser.current_mode = 'TAG';
                    break;
                default:
                    // We should not be getting here but we don't want to drop input on the floor
                    // Just output the text and move on
                    if (multi_parser.token_text !== '') {
                        multi_parser.print_token(multi_parser.token_text);
                    }
                    break;
            }
            multi_parser.last_token = multi_parser.token_type;
            multi_parser.last_text = multi_parser.token_text;
        }
        var sweet_code = multi_parser.output.join('').replace(/[\r\n\t ]+$/, '');
        if (end_with_newline) {
            sweet_code += '\n';
        }
        return sweet_code;
    }

    if (typeof define === "function" && define.amd) {
        // Add support for AMD ( https://github.com/amdjs/amdjs-api/wiki/AMD#defineamd-property- )
        define(["require", "./beautify", "./beautify-css"], function(requireamd) {
            var js_beautify =  requireamd("./beautify");
            var css_beautify =  requireamd("./beautify-css");

            return {
              html_beautify: function(html_source, options) {
                return style_html(html_source, options, js_beautify.js_beautify, css_beautify.css_beautify);
              }
            };
        });
    } else if (typeof exports !== "undefined") {
        // Add support for CommonJS. Just put this file somewhere on your require.paths
        // and you will be able to `var html_beautify = require("beautify").html_beautify`.
        var js_beautify = require('./beautify.js');
        var css_beautify = require('./beautify-css.js');

        exports.html_beautify = function(html_source, options) {
            return style_html(html_source, options, js_beautify.js_beautify, css_beautify.css_beautify);
        };
    } else if (typeof window !== "undefined") {
        // If we're running a web page and don't have either of the above, add our one global
        window.html_beautify = function(html_source, options) {
            return style_html(html_source, options, window.js_beautify, window.css_beautify);
        };
    } else if (typeof global !== "undefined") {
        // If we don't even have window, try global.
        global.html_beautify = function(html_source, options) {
            return style_html(html_source, options, global.js_beautify, global.css_beautify);
        };
    }

}());
});

}}});
