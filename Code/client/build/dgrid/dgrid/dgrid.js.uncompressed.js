require({cache:{
'dgrid/main':function(){
define([
    "dgrid/Tree",
    "dgrid/Selection",
    "dgrid/OnDemandList",
    "dgrid/OnDemandGrid",
    "dgrid/extensions/ColumnReorder",
    "dgrid/extensions/ColumnResizer",
    "dgrid/extensions/CompoundColumns",
    "dgrid/extensions/Pagination",
    "dgrid/extensions/DnD",
    "dgrid/Editor",
    "dstore/Store",
    "dstore/Memory",
    "dstore/Tree",
    "dstore/Cache",
    "dstore/Trackable",
    "dstore/legacy/DstoreAdapter"
], function(){

});

},
'dgrid/Tree':function(){
define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/_base/array',
	'dojo/aspect',
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/on',
	'dojo/query',
	'dojo/when',
	'./util/has-css3',
	'./Grid',
	'./util/touch'
], function (declare, lang, arrayUtil, aspect, domConstruct, domClass, on, querySelector, when, has, Grid, touchUtil) {

	return declare(null, {
		// collapseOnRefresh: Boolean
		//		Whether to collapse all expanded nodes any time refresh is called.
		collapseOnRefresh: false,

		// enableTreeTransitions: Boolean
		//		Enables/disables all expand/collapse CSS transitions.
		enableTreeTransitions: false,

		// treeIndentWidth: Number
		//		Width (in pixels) of each level of indentation.
		treeIndentWidth: 9,

		constructor: function () {
			this._treeColumnListeners = [];
		},

		shouldExpand: function (row, level, previouslyExpanded) {
			// summary:
			//		Function called after each row is inserted to determine whether
			//		expand(rowElement, true) should be automatically called.
			//		The default implementation re-expands any rows that were expanded
			//		the last time they were rendered (if applicable).

			return previouslyExpanded;
		},

		expand: function (target, expand, noTransition) {
			// summary:
			//		Expands the row corresponding to the given target.
			// target: Object
			//		Row object (or something resolvable to one) to expand/collapse.
			// expand: Boolean?
			//		If specified, designates whether to expand or collapse the row;
			//		if unspecified, toggles the current state.

			if (!this._treeColumn) {
				return;
			}

			var grid = this,
				row = target.element ? target : this.row(target),
				isExpanded = !!this._expanded[row.id],
				hasTransitionend = has('transitionend'),
				promise, collection = this.collection;

			target = row.element;
			if (!target) {
				return;
			}
			target = target.className.indexOf('dgrid-expando-icon') > -1 ? target :
				querySelector('.dgrid-expando-icon', target)[0];

			noTransition = noTransition || !this.enableTreeTransitions;

			if (target && target.mayHaveChildren && (noTransition || expand !== isExpanded)) {
				// toggle or set expand/collapsed state based on optional 2nd argument
				var expanded = expand === undefined ? !this._expanded[row.id] : expand;

				if (collection.spin === true && expand) {
					domClass.remove(target, 'ui-icon-triangle-1-e');
					domClass.remove(target, 'ui-icon-triangle-1-se');
					domClass.add(target, 'fa fa-spinner fa-spin dgrid-cell-loading');
				} else {
					domClass.replace(target, 'ui-icon-triangle-1-' + (expanded ? 'se' : 'e'),
						'ui-icon-triangle-1-' + (expanded ? 'e' : 'se'));
				}


				domClass.toggle(row.element, 'dgrid-row-expanded', expanded);
				var rowElement = row.element,
					container = rowElement.connected,
					containerStyle,
					scrollHeight,
					options = {};

				if (!container) {
					// if the children have not been created, create a container, a preload node and do the
					// query for the children
					container = options.container = rowElement.connected =
						domConstruct.create('div', { className: 'dgrid-tree-container' }, rowElement, 'after');

					var query = function (options) {
						var childCollection = grid._renderedCollection.getChildren(row.data),
							results;
						if (grid.sort) {
							childCollection = childCollection.sort(grid.sort);
						}
						if (childCollection.track && grid.shouldTrackCollection) {
							container._rows = options.rows = [];

							childCollection = childCollection.track();

							// remember observation handles so they can be removed when the parent row is destroyed
							container._handles = [
								childCollection.tracking,
								grid._observeCollection(childCollection, container, options)
							];
						}
						if ('start' in options) {
							var rangeArgs = {
								start: options.start,
								end: options.start + options.count
							};
							results = childCollection.fetchRange(rangeArgs);
						} else {
							results = childCollection.fetch();
						}
						return results;
					};
					// Include level information on query for renderQuery case
					if ('level' in target) {
						query.level = target.level;
					}

					// Add the query to the promise chain
					if (this.renderQuery) {
						promise = this.renderQuery(query, options);
					}
					else {
						// If not using OnDemandList, we don't need preload nodes,
						// but we still need a beforeNode to pass to renderArray,
						// so create a temporary one
						var firstChild = domConstruct.create('div', null, container);
						promise = this._trackError(function () {
							return grid.renderQueryResults(
								query(options),
								firstChild,
								lang.mixin({ rows: options.rows },
									'level' in query ? { queryLevel: query.level } : null
								)
							).then(function (rows) {
								domConstruct.destroy(firstChild);
								return rows;
							});
						});
					}

					if (hasTransitionend) {
						// Update height whenever a collapse/expand transition ends.
						// (This handler is only registered when each child container is first created.)
						on(container, hasTransitionend, this._onTreeTransitionEnd);
					}
				}

				// Show or hide all the children.

				container.hidden = !expanded;
				containerStyle = container.style;

				// make sure it is visible so we can measure it
				if (!hasTransitionend || noTransition) {
					containerStyle.display = expanded ? 'block' : 'none';
					containerStyle.height = '';
				}
				else {
					if (expanded) {
						containerStyle.display = 'block';
						scrollHeight = container.scrollHeight;
						containerStyle.height = '0px';
					}
					else {
						// if it will be hidden we need to be able to give a full height
						// without animating it, so it has the right starting point to animate to zero
						domClass.add(container, 'dgrid-tree-resetting');
						containerStyle.height = container.scrollHeight + 'px';
					}
					// Perform a transition for the expand or collapse.
					setTimeout(function () {
						domClass.remove(container, 'dgrid-tree-resetting');
						containerStyle.height =
							expanded ? (scrollHeight ? scrollHeight + 'px' : 'auto') : '0px';
					}, 0);
				}

				// Update _expanded map.
				if (expanded) {
					this._expanded[row.id] = true;
				}
				else {
					delete this._expanded[row.id];
				}
			}
			function clear(target) {
				if (collection.spin === true && target.mayHaveChildren) {
					domClass.remove(target, 'fa-spin fa-spinner dgrid-cell-loading');
					domClass.replace(target, 'ui-icon-triangle-1-' + (expanded ? 'se' : 'e'),
						'ui-icon-triangle-1-' + (expanded ? 'e' : 'se'));
				}
			}
			if (promise && promise.then) {
				promise.then(function () {
					clear(target);
				});
			} else {
				clear(target);
			}

			// Always return a promise
			return when(promise);
		},

		_configColumns: function () {
			var columnArray = this.inherited(arguments);

			// Set up hash to store IDs of expanded rows (here rather than in
			// _configureTreeColumn so nothing breaks if no column has renderExpando)
			this._expanded = {};

			for (var i = 0, l = columnArray.length; i < l; i++) {
				if (columnArray[i].renderExpando) {
					this._configureTreeColumn(columnArray[i]);
					break; // Allow only one tree column.
				}
			}
			return columnArray;
		},

		insertRow: function (object) {
			var rowElement = this.inherited(arguments);

			// Auto-expand (shouldExpand) considerations
			var row = this.row(rowElement),
				expanded = this.shouldExpand(row, this._currentLevel, this._expanded[row.id]);

			if (expanded) {
				this.expand(rowElement, true, true);
			}

			if (expanded || (!this.collection.mayHaveChildren || this.collection.mayHaveChildren(object))) {
				domClass.add(rowElement, 'dgrid-row-expandable');
			}

			return rowElement; // pass return value through
		},

		removeRow: function (rowElement, preserveDom) {
			var connected = rowElement.connected,
				childOptions = {};
			if (connected) {
				if (connected._handles) {
					arrayUtil.forEach(connected._handles, function (handle) {
						handle.remove();
					});
					delete connected._handles;
				}

				if (connected._rows) {
					childOptions.rows = connected._rows;
				}

				querySelector('>.dgrid-row', connected).forEach(function (element) {
					this.removeRow(element, true, childOptions);
				}, this);

				if (connected._rows) {
					connected._rows.length = 0;
					delete connected._rows;
				}

				if (!preserveDom) {
					domConstruct.destroy(connected);
				}
			}

			this.inherited(arguments);
		},

		_refreshCellFromItem: function (cell, item) {
			if (!cell.column.renderExpando) {
				return this.inherited(arguments);
			}

			this.inherited(arguments, [cell, item, {
				queryLevel: querySelector('.dgrid-expando-icon', cell.element)[0].level - 1
			}]);
		},

		cleanup: function () {
			this.inherited(arguments);

			if (this.collapseOnRefresh) {
				// Clear out the _expanded hash on each call to cleanup
				// (which generally coincides with refreshes, as well as destroy)
				this._expanded = {};
			}
		},

		_destroyColumns: function () {
			this.inherited(arguments);
			var listeners = this._treeColumnListeners;

			for (var i = listeners.length; i--;) {
				listeners[i].remove();
			}
			this._treeColumnListeners = [];
			this._treeColumn = null;
		},

		_calcRowHeight: function (rowElement) {
			// Override this method to provide row height measurements that
			// include the children of a row
			var connected = rowElement.connected;
			// if connected, need to consider this in the total row height
			return this.inherited(arguments) + (connected ? connected.offsetHeight : 0);
		},

		_configureTreeColumn: function (column) {
			// summary:
			//		Adds tree navigation capability to a column.

			var grid = this;
			var collection = this.collection;
			var colSelector = '.dgrid-content .dgrid-column-' + column.id;
			var clicked; // tracks row that was clicked (for expand dblclick event handling)

			this._treeColumn = column;
			if (!column._isConfiguredTreeColumn) {
				var originalRenderCell = column.renderCell || this._defaultRenderCell;
				column._isConfiguredTreeColumn = true;
				column.renderCell = function (object, value, td, options) {
					// summary:
					//		Renders a cell that can be expanded, creating more rows

					if (!collection) {
						return;
					}

					var level = Number(options && options.queryLevel) + 1,
						mayHaveChildren = !collection.mayHaveChildren || collection.mayHaveChildren(object),
						expando, node;

					level = grid._currentLevel = isNaN(level) ? 0 : level;

					expando = column.renderExpando(level, mayHaveChildren,
						grid._expanded[collection.getIdentity(object)], object);

					expando.level = level;
					expando.mayHaveChildren = mayHaveChildren;

					node = originalRenderCell.call(column, object, value, td, options);
					if (node && node.nodeType) {
						td.appendChild(expando);
						td.appendChild(node);
					}
					else {
						td.insertBefore(expando, td.firstChild);
					}
				};

				if (typeof column.renderExpando !== 'function') {
					column.renderExpando = this._defaultRenderExpando;
				}
			}

			var treeColumnListeners = this._treeColumnListeners;
			if (treeColumnListeners.length === 0) {
				// Set up the event listener once and use event delegation for better memory use.
				treeColumnListeners.push(this.on(column.expandOn ||
					'.dgrid-expando-icon:click,' + colSelector + ':dblclick,' + colSelector + ':keydown',
					function (event) {
						var row = grid.row(event);
						if ((!grid.collection.mayHaveChildren || grid.collection.mayHaveChildren(row.data)) &&
							(event.type !== 'keydown' || event.keyCode === 32) && !(event.type === 'dblclick' &&
								clicked && clicked.count > 1 && row.id === clicked.id &&
								event.target.className.indexOf('dgrid-expando-icon') > -1)) {
							grid.expand(row);
						}

						// If the expando icon was clicked, update clicked object to prevent
						// potential over-triggering on dblclick (all tested browsers but IE < 9).
						if (event.target.className.indexOf('dgrid-expando-icon') > -1) {
							if (clicked && clicked.id === grid.row(event).id) {
								clicked.count++;
							}
							else {
								clicked = {
									id: grid.row(event).id,
									count: 1
								};
							}
						}
					})
				);

				if (has('touch')) {
					// Also listen on double-taps of the cell.
					treeColumnListeners.push(this.on(touchUtil.selector(colSelector, touchUtil.dbltap),
						function () {
							grid.expand(this);
						}));
				}
			}

			/*
			 column.renderCell = function (object, value, td, options) {
			 // summary:
			 //		Renders a cell that can be expanded, creating more rows

			 var grid = column.grid,
			 level = Number(options && options.queryLevel) + 1,
			 mayHaveChildren = !grid.collection.mayHaveChildren || grid.collection.mayHaveChildren(object),
			 expando, node;

			 level = grid._currentLevel = isNaN(level) ? 0 : level;
			 expando = column.renderExpando(level, mayHaveChildren,
			 grid._expanded[grid.collection.getIdentity(object)], object);
			 expando.level = level;
			 expando.mayHaveChildren = mayHaveChildren;

			 node = originalRenderCell.call(column, object, value, td, options);
			 if (node && node.nodeType) {
			 td.appendChild(expando);
			 td.appendChild(node);
			 }
			 else {
			 td.insertBefore(expando, td.firstChild);
			 }
			 };
			 *//*
   if (!column.originalRenderCell)
   {
	   column.originalRenderCell = column.renderCell || this._defaultRenderCell;
	   column.renderCell = function (object, value, td, options) {
		   // summary:
		   //              Renders a cell that can be expanded, creating more rows
		   var grid = column.grid,
				   level = Number(options && options.queryLevel) + 1,
				   mayHaveChildren = !grid.collection.mayHaveChildren || grid.collection.mayHaveChildren(object),
				   expando,
				   node;

		   level = grid._currentLevel = isNaN(level) ? 0 : level;
		   expando = column.renderExpando(level, mayHaveChildren,
				   grid._expanded[grid.collection.getIdentity(object)], object);
		   expando.level = level;
		   expando.mayHaveChildren = mayHaveChildren;
		   node = column.originalRenderCell(object, value, td, options);
		   if (node && node.nodeType) {
			   td.appendChild(expando);
			   td.appendChild(node);
		   }
		   else {
			   td.insertBefore(expando, td.firstChild);
		   }
	   };
   }
   */
		},

		_defaultRenderExpando: function (level, hasChildren, expanded) {
			// summary:
			//		Default implementation for column.renderExpando.
			//		NOTE: Called in context of the column definition object.
			// level: Number
			//		Level of indentation for this row (0 for top-level)
			// hasChildren: Boolean
			//		Whether this item may have children (in most cases this determines
			//		whether an expando icon should be rendered)
			// expanded: Boolean
			//		Whether this item is currently in expanded state
			// object: Object
			//		The item that this expando pertains to

			var dir = this.grid.isRTL ? 'right' : 'left',
				cls = 'dgrid-expando-icon';
			if (hasChildren) {
				cls += ' ui-icon ui-icon-triangle-1-' + (expanded ? 'se' : 'e');
			}
			return domConstruct.create('div', {
				className: cls,
				innerHTML: '&nbsp;',
				style: 'margin-' + dir + ': ' + (level * this.grid.treeIndentWidth) + 'px; float: ' + dir + ';'
			});
		},

		_onNotification: function (rows, event) {
			if (event.type === 'delete') {
				delete this._expanded[event.id];
			}
			this.inherited(arguments);
		},

		_onTreeTransitionEnd: function (event) {
			var container = this,
				height = this.style.height;
			if (height) {
				// After expansion, ensure display is correct;
				// after collapse, set display to none to improve performance
				this.style.display = height === '0px' ? 'none' : 'block';
			}

			// Reset height to be auto, so future height changes (from children
			// expansions, for example), will expand to the right height.
			if (event) {
				// For browsers with CSS transition support, setting the height to
				// auto or "" will cause an animation to zero height for some
				// reason, so temporarily set the transition to be zero duration
				domClass.add(this, 'dgrid-tree-resetting');
				setTimeout(function () {
					// Turn off the zero duration transition after we have let it render
					domClass.remove(container, 'dgrid-tree-resetting');
				}, 0);
			}
			// Now set the height to auto
			this.style.height = '';
		}
	});
});
},
'dgrid/util/has-css3':function(){
define([
	'dojo/has'
], function (has) {
	// This module defines feature tests for CSS3 features such as transitions.
	// The css-transitions, css-transforms, and css-transforms3d has-features
	// can report either boolean or string:
	// * false indicates no support
	// * true indicates prefix-less support
	// * string indicates the vendor prefix under which the feature is supported

	var cssPrefixes = ['ms', 'O', 'Moz', 'Webkit'];

	function testStyle(element, property) {
		var style = element.style,
			i;

		if (property in style) {
			// Standard, no prefix
			return true;
		}
		property = property.slice(0, 1).toUpperCase() + property.slice(1);
		for (i = cssPrefixes.length; i--;) {
			if ((cssPrefixes[i] + property) in style) {
				// Vendor-specific css property prefix
				return cssPrefixes[i];
			}
		}

		// Otherwise, not supported
		return false;
	}

	has.add('css-transitions', function (global, doc, element) {
		return testStyle(element, 'transitionProperty');
	});

	has.add('css-transforms', function (global, doc, element) {
		return testStyle(element, 'transform');
	});

	has.add('css-transforms3d', function (global, doc, element) {
		return testStyle(element, 'perspective');
	});

	has.add('transitionend', function () {
		// Infer transitionend event name based on CSS transitions has-feature.
		var tpfx = has('css-transitions');
		if (!tpfx) {
			return false;
		}
		if (tpfx === true) {
			return 'transitionend';
		}
		return {
			ms: 'MSTransitionEnd',
			O: 'oTransitionEnd',
			Moz: 'transitionend',
			Webkit: 'webkitTransitionEnd'
		}[tpfx];
	});

	return has;
});

},
'dgrid/Grid':function(){
define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/on',
	'dojo/has',
	'./List',
	'./util/misc',
	'dojo/_base/sniff'
], function (declare, lang, domConstruct, domClass, listen, has, List, miscUtil) {
	function appendIfNode(parent, subNode) {
		if (subNode && subNode.nodeType) {
			parent.appendChild(subNode);
		}
	}

	function replaceInvalidChars(str) {
		// Replaces invalid characters for a CSS identifier with hyphen,
		// as dgrid does for field names / column IDs when adding classes.
		return miscUtil.escapeCssIdentifier(str, '-');
	}

	var Grid = declare(List, {
		columns: null,

		// hasNeutralSort: Boolean
		//		Determines behavior of toggling sort on the same column.
		//		If false, sort toggles between ascending and descending and cannot be
		//		reset to neutral without sorting another column.
		//		If true, sort toggles between ascending, descending, and neutral.
		hasNeutralSort: false,

		// cellNavigation: Boolean
		//		This indicates that focus is at the cell level. This may be set to false to cause
		//		focus to be at the row level, which is useful if you want only want row-level
		//		navigation.
		cellNavigation: true,

		tabableHeader: true,
		showHeader: true,
		column: function (target) {
			// summary:
			//		Get the column object by node, or event, or a columnId
			if (typeof target !== 'object') {
				return this.columns[target];
			}
			else {
				return this.cell(target).column;
			}
		},
		listType: 'grid',
		cell: function (target, columnId) {
			// summary:
			//		Get the cell object by node, or event, id, plus a columnId

			if (target.column && target.element) {
				return target;
			}

			if (target.target && target.target.nodeType) {
				// event
				target = target.target;
			}
			var element;
			if (target.nodeType) {
				do {
					if (this._rowIdToObject[target.id]) {
						break;
					}
					var colId = target.columnId;
					if (colId) {
						columnId = colId;
						element = target;
						break;
					}
					target = target.parentNode;
				} while (target && target !== this.domNode);
			}
			if (!element && typeof columnId !== 'undefined') {
				var row = this.row(target),
					rowElement = row && row.element;
				if (rowElement) {
					var elements = rowElement.getElementsByTagName('td');
					for (var i = 0; i < elements.length; i++) {
						if (elements[i].columnId === columnId) {
							element = elements[i];
							break;
						}
					}
				}
			}
			if (target != null) {
				return {
					row: row || this.row(target),
					column: columnId && this.column(columnId),
					element: element
				};
			}
		},

		createRowCells: function (tag, createCell, subRows, item, options) {
			// summary:
			//		Generates the grid for each row (used by renderHeader and and renderRow)
			var row = domConstruct.create('table', {
					className: 'dgrid-row-table',
					role: 'presentation'
				}),
				// IE < 9 needs an explicit tbody; other browsers do not
				tbody = (has('ie') < 9) ? domConstruct.create('tbody', null, row) : row,
				tr,
				si, sl, i, l, // iterators
				subRow, column, id, extraClasses, className,
				cell, colSpan, rowSpan; // used inside loops

			// Allow specification of custom/specific subRows, falling back to
			// those defined on the instance.
			subRows = subRows || this.subRows;

			for (si = 0, sl = subRows.length; si < sl; si++) {
				subRow = subRows[si];
				// for single-subrow cases in modern browsers, TR can be skipped
				// http://jsperf.com/table-without-trs
				tr = domConstruct.create('tr', null, tbody);
				if (subRow.className) {
					tr.className = subRow.className;
				}

				for (i = 0, l = subRow.length; i < l; i++) {
					// iterate through the columns
					column = subRow[i];
					id = column.id;

					extraClasses = column.field ?
						' field-' + replaceInvalidChars(column.field) :
						'';
					className = typeof column.className === 'function' ?
						column.className(item) : column.className;
					if (className) {
						extraClasses += ' ' + className;
					}

					cell = domConstruct.create(tag, {
						className: 'dgrid-cell' +
							(id ? ' dgrid-column-' + replaceInvalidChars(id) : '') + extraClasses,
						role: tag === 'th' ? 'columnheader' : 'gridcell'
					});
					cell.columnId = id;
					colSpan = column.colSpan;
					if (colSpan) {
						cell.colSpan = colSpan;
					}
					rowSpan = column.rowSpan;
					if (rowSpan) {
						cell.rowSpan = rowSpan;
					}
					createCell(cell, column, item, options);
					// add the td to the tr at the end for better performance
					tr.appendChild(cell);
				}
			}
			return row;
		},

		_createBodyRowCell: function (cellElement, column, item, options) {
			var cellData = item;

			// Support get function or field property (similar to DataGrid)
			if (column.get) {
				cellData = column.get(item);
			}
			else if ('field' in column && column.field !== '_item') {
				cellData = item[column.field];
			}

			if (column.renderCell) {
				// A column can provide a renderCell method to do its own DOM manipulation,
				// event handling, etc.
				appendIfNode(cellElement, column.renderCell(item, cellData, cellElement, options));
			}
			else {
				this._defaultRenderCell.call(column, item, cellData, cellElement, options);
			}
		},

		_createHeaderRowCell: function (cellElement, column) {
			var contentNode = column.headerNode = cellElement;
			var field = column.field;
			if (field) {
				cellElement.field = field;
			}
			// allow for custom header content manipulation
			if (column.renderHeaderCell) {
				appendIfNode(contentNode, column.renderHeaderCell(contentNode));
			}
			else if ('label' in column || column.field) {
				contentNode.appendChild(document.createTextNode(
					'label' in column ? column.label : column.field));
			}
			if (column.sortable !== false && field && field !== '_item') {
				cellElement.sortable = true;
				cellElement.className += ' dgrid-sortable';
			}
		},

		left: function (cell, steps) {
			if (!cell.element) {
				cell = this.cell(cell);
			}
			return this.cell(this._move(cell, -(steps || 1), 'dgrid-cell'));
		},
		right: function (cell, steps) {
			if (!cell.element) {
				cell = this.cell(cell);
			}
			return this.cell(this._move(cell, steps || 1, 'dgrid-cell'));
		},

		_defaultRenderCell: function (object, value, td) {
			// summary:
			//		Default renderCell implementation.
			//		NOTE: Called in context of column definition object.
			// object: Object
			//		The data item for the row currently being rendered
			// value: Mixed
			//		The value of the field applicable to the current cell
			// td: DOMNode
			//		The cell element representing the current item/field
			// options: Object?
			//		Any additional options passed through from renderRow

			if (this.formatter) {
				// Support formatter, with or without formatterScope
				var formatter = this.formatter,
					formatterScope = this.grid.formatterScope;
				td.innerHTML = typeof formatter === 'string' && formatterScope ?
					formatterScope[formatter](value, object) : this.formatter(value, object);
			}
			else if (value != null) {
				td.appendChild(document.createTextNode(value));
			}
		},

		renderRow: function (item, options) {
			var row = this.createRowCells('td', lang.hitch(this, '_createBodyRowCell'),
				options && options.subRows, item, options);

			// row gets a wrapper div for a couple reasons:
			// 1. So that one can set a fixed height on rows (heights can't be set on <table>'s AFAICT)
			// 2. So that outline style can be set on a row when it is focused,
			// and Safari's outline style is broken on <table>
			var div = domConstruct.create('div', { role: 'row' });
			div.appendChild(row);
			return div;
		},

		renderHeader: function () {
			// summary:
			//		Setup the headers for the grid
			var grid = this,
				headerNode = this.headerNode;

			headerNode.setAttribute('role', 'row');

			// clear out existing header in case we're resetting
			domConstruct.empty(headerNode);

			var row = this.createRowCells('th', lang.hitch(this, '_createHeaderRowCell'),
				this.subRows && this.subRows.headerRows);
			this._rowIdToObject[row.id = this.id + '-header'] = this.columns;
			headerNode.appendChild(row);

			// If the columns are sortable, re-sort on clicks.
			// Use a separate listener property to be managed by renderHeader in case
			// of subsequent calls.
			if (this._sortListener) {
				this._sortListener.remove();
			}
			this._sortListener = listen(row, 'click,keydown', function (event) {
				// respond to click, space keypress, or enter keypress
				if (event.type === 'click' || event.keyCode === 32 ||
						(!has('opera') && event.keyCode === 13)) {
					var target = event.target;
					var field;
					var sort;
					var newSort;
					var eventObj;

					do {
						if (target.sortable) {
							field = target.field || target.columnId;
							sort = grid.sort[0];
							if (!grid.hasNeutralSort || !sort || sort.property !== field || !sort.descending) {
								// If the user toggled the same column as the active sort,
								// reverse sort direction
								newSort = [{
									property: field,
									descending: sort && sort.property === field &&
										!sort.descending
								}];
							}
							else {
								// If the grid allows neutral sort and user toggled an already-descending column,
								// clear sort entirely
								newSort = [];
							}

							// Emit an event with the new sort
							eventObj = {
								bubbles: true,
								cancelable: true,
								grid: grid,
								parentType: event.type,
								sort: newSort
							};

							if (listen.emit(event.target, 'dgrid-sort', eventObj)) {
								// Stash node subject to DOM manipulations,
								// to be referenced then removed by sort()
								grid._sortNode = target;
								grid.set('sort', newSort);
							}

							break;
						}
					} while ((target = target.parentNode) && target !== headerNode);
				}
			});
		},

		resize: function () {
			// extension of List.resize to allow accounting for
			// column sizes larger than actual grid area
			if(!this.headerNode){
				return;
			}
			var headerTableNode = this.headerNode.firstChild,
				contentNode = this.contentNode,
				width;

			this.inherited(arguments);

			// Force contentNode width to match up with header width.
			contentNode.style.width = ''; // reset first
			if (contentNode && headerTableNode) {
				if ((width = headerTableNode.offsetWidth) > contentNode.offsetWidth) {
					// update size of content node if necessary (to match size of rows)
					// (if headerTableNode can't be found, there isn't much we can do)
					contentNode.style.width = width + 'px';
				}
			}
		},

		destroy: function () {
			// Run _destroyColumns first to perform any column plugin tear-down logic.
			this._destroyColumns();
			if (this._sortListener) {
				this._sortListener.remove();
			}

			this.inherited(arguments);
		},

		_setSort: function () {
			// summary:
			//		Extension of List.js sort to update sort arrow in UI

			// Normalize sort first via inherited logic, then update the sort arrow
			this.inherited(arguments);
			this.updateSortArrow(this.sort);
		},

		_findSortArrowParent: function (field) {
			// summary:
			//		Method responsible for finding cell that sort arrow should be
			//		added under.  Called by updateSortArrow; separated for extensibility.

			var columns = this.columns;
			for (var i in columns) {
				var column = columns[i];
				if (column.field === field) {
					return column.headerNode;
				}
			}
		},

		updateSortArrow: function (sort, updateSort) {
			// summary:
			//		Method responsible for updating the placement of the arrow in the
			//		appropriate header cell.  Typically this should not be called (call
			//		set("sort", ...) when actually updating sort programmatically), but
			//		this method may be used by code which is customizing sort (e.g.
			//		by reacting to the dgrid-sort event, canceling it, then
			//		performing logic and calling this manually).
			// sort: Array
			//		Standard sort parameter - array of object(s) containing property name
			//		and optional descending flag
			// updateSort: Boolean?
			//		If true, will update this.sort based on the passed sort array
			//		(i.e. to keep it in sync when custom logic is otherwise preventing
			//		it from being updated); defaults to false

			// Clean up UI from any previous sort
			if (this._lastSortedArrow) {
				// Remove the sort classes from the parent node
				domClass.remove(this._lastSortedArrow.parentNode, 'dgrid-sort-up dgrid-sort-down');
				// Destroy the lastSortedArrow node
				domConstruct.destroy(this._lastSortedArrow);
				delete this._lastSortedArrow;
			}

			if (updateSort) {
				this.sort = sort;
			}
			if (!sort[0]) {
				return; // Nothing to do if no sort is specified
			}

			var prop = sort[0].property,
				desc = sort[0].descending,
				// if invoked from header click, target is stashed in _sortNode
				target = this._sortNode || this._findSortArrowParent(prop),
				arrowNode;

			delete this._sortNode;

			// Skip this logic if field being sorted isn't actually displayed
			if (target) {
				target = target.contents || target;
				// Place sort arrow under clicked node, and add up/down sort class
				arrowNode = this._lastSortedArrow = domConstruct.create('div', {
					className: 'dgrid-sort-arrow ui-icon',
					innerHTML: '&nbsp;',
					role: 'presentation'
				}, target, 'first');
				domClass.add(target, 'dgrid-sort-' + (desc ? 'down' : 'up'));
				// Call resize in case relocation of sort arrow caused any height changes
				this.resize();
			}
		},

		styleColumn: function (colId, css) {
			// summary:
			//		Dynamically creates a stylesheet rule to alter a column's style.

			return this.addCssRule('#' + miscUtil.escapeCssIdentifier(this.domNode.id) +
				' .dgrid-column-' + replaceInvalidChars(colId), css);
		},

		/*=====
		_configColumn: function (column, rowColumns, prefix) {
			// summary:
			//		Method called when normalizing base configuration of a single
			//		column.  Can be used as an extension point for behavior requiring
			//		access to columns when a new configuration is applied.
		},=====*/

		_configColumns: function (prefix, rowColumns) {
			// configure the current column
			var subRow = [],
				isArray = rowColumns instanceof Array;

			function configColumn(column, columnId) {
				if (typeof column === 'string') {
					rowColumns[columnId] = column = { label: column };
				}
				if (!isArray && !column.field) {
					column.field = columnId;
				}
				columnId = column.id = column.id || (isNaN(columnId) ? columnId : (prefix + columnId));
				// allow further base configuration in subclasses
				if (this._configColumn) {
					this._configColumn(column, rowColumns, prefix);
					// Allow the subclasses to modify the column id.
					columnId = column.id;
				}
				if (isArray) {
					this.columns[columnId] = column;
				}

				// add grid reference to each column object for potential use by plugins
				column.grid = this;
				subRow.push(column); // make sure it can be iterated on
			}

			miscUtil.each(rowColumns, configColumn, this);
			return isArray ? rowColumns : subRow;
		},

		_destroyColumns: function () {
			// summary:
			//		Extension point for column-related cleanup.  This is called
			//		immediately before configuring a new column structure,
			//		and when the grid is destroyed.

			// First remove rows (since they'll be refreshed after we're done),
			// so that anything temporarily extending removeRow can run.
			// (cleanup will end up running again, but with nothing to iterate.)
			this.cleanup();
		},

		configStructure: function () {
			// configure the columns and subRows
			var subRows = this.subRows,
				columns = this._columns = this.columns;

			// Reset this.columns unless it was already passed in as an object
			this.columns = !columns || columns instanceof Array ? {} : columns;

			if (subRows) {
				// Process subrows, which will in turn populate the this.columns object
				for (var i = 0; i < subRows.length; i++) {
					subRows[i] = this._configColumns(i + '-', subRows[i]);
				}
			}
			else {
				this.subRows = [this._configColumns('', columns)];
			}
		},

		_getColumns: function () {
			// _columns preserves what was passed to set("columns"), but if subRows
			// was set instead, columns contains the "object-ified" version, which
			// was always accessible in the past, so maintain that accessibility going
			// forward.
			return this._columns || this.columns;
		},
		_setColumns: function (columns) {
			this._destroyColumns();
			// reset instance variables
			this.subRows = null;
			this.columns = columns;
			// re-run logic
			this._updateColumns();
		},

		_setSubRows: function (subrows) {
			this._destroyColumns();
			this.subRows = subrows;
			this._updateColumns();
		},

		_updateColumns: function () {
			// summary:
			//		Called when columns, subRows, or columnSets are reset

			this.configStructure();
			this.renderHeader();

			this.refresh();
			// re-render last collection if present
			this._lastCollection && this.renderArray(this._lastCollection);

			// After re-rendering the header, re-apply the sort arrow if needed.
			if (this._started) {
				if (this.sort.length) {
					this._lastSortedArrow = null;
					this.updateSortArrow(this.sort);
				} else {
					// Only call resize directly if we didn't call updateSortArrow,
					// since that calls resize itself when it updates.
					this.resize();
				}
			}
		}
	});

	Grid.appendIfNode = appendIfNode;

	return Grid;
});

},
'dgrid/List':function(){
/** @module dgrid/List **/
define([
    'dojo/_base/declare',
    'dojo/dom-construct',
    'dojo/dom-class',
    'dojo/on',
    'dojo/has',
    './util/misc',
    'xide/$'
], function (declare, domConstruct, domClass, listen, has, miscUtil, $) {
    // Add user agent/feature CSS classes needed for structural CSS
    var featureClasses = [];
    if (has('mozilla')) {
        featureClasses.push('has-mozilla');
    }
    if (has('touch')) {
        featureClasses.push('has-touch');
    }
    domClass.add(document.documentElement, featureClasses);

    // Add a feature test for pointer (only Dojo 1.10 has pointer-events and MSPointer tests)
    has.add('pointer', function (global) {
        return 'PointerEvent' in global ? 'pointer' :
            'MSPointerEvent' in global ? 'MSPointer' : false;
    });

    var oddClass = 'dgrid-row-odd',
        evenClass = 'dgrid-row-even',
        scrollbarWidth, scrollbarHeight;

    function byId(id) {
        return document.getElementById(id);
    }

    function cleanupTestElement(element) {
        element.className = '';
        if (element.parentNode) {
            document.body.removeChild(element);
        }
    }

    function getScrollbarSize(element, dimension) {
        // Used by has tests for scrollbar width/height
        element.className = 'dgrid-scrollbar-measure';
        document.body.appendChild(element);
        var size = element['offset' + dimension] - element['client' + dimension];
        cleanupTestElement(element);
        return size;
    }

    has.add('dom-scrollbar-width', function (global, doc, element) {
        return getScrollbarSize(element, 'Width');
    });
    has.add('dom-scrollbar-height', function (global, doc, element) {
        return getScrollbarSize(element, 'Height');
    });

    has.add('dom-rtl-scrollbar-left', function (global, doc, element) {
        var div = document.createElement('div'),
            isLeft;

        element.className = 'dgrid-scrollbar-measure';
        element.setAttribute('dir', 'rtl');
        element.appendChild(div);
        document.body.appendChild(element);

        // position: absolute makes modern IE and Edge always report child's offsetLeft as 0,
        // but other browsers factor in the position of the scrollbar if it is to the left.
        // All versions of IE and Edge are known to move the scrollbar to the left side for rtl.
        isLeft = !!has('ie') || !!has('trident') || /\bEdge\//.test(navigator.userAgent) ||
            div.offsetLeft >= has('dom-scrollbar-width');
        cleanupTestElement(element);
        domConstruct.destroy(div);
        element.removeAttribute('dir');
        return isLeft;
    });

    // var and function for autogenerating ID when one isn't provided
    var autoId = 0;

    function generateId() {
        return List.autoIdPrefix + autoId++;
    }

    // common functions for class and className setters/getters
    // (these are run in instance context)
    function setClass(cls) {
        // TODO: unit test
        domClass.replace(this.domNode, cls, this._class || '');

        // Store for later retrieval/removal.
        this._class = cls;
    }

    function getClass() {
        return this._class;
    }

    // window resize event handler, run in context of List instance
    //xhack no longer needed
    //var winResizeHandler = function () {
    //	console.log('reis');
    //	if (this._started) {
    //		this.resize();
    //	}
    //};

    /**
     * Grid base class
     * @class module:dgrid/List
     */
    var List = declare(null, {
        tabableHeader: false,

        // showHeader: Boolean
        //		Whether to render header (sub)rows.
        showHeader: false,

        // showFooter: Boolean
        //		Whether to render footer area.  Extensions which display content
        //		in the footer area should set this to true.
        showFooter: false,

        // maintainOddEven: Boolean
        //		Whether to maintain the odd/even classes when new rows are inserted.
        //		This can be disabled to improve insertion performance if odd/even styling is not employed.
        maintainOddEven: true,

        // cleanAddedRules: Boolean
        //		Whether to track rules added via the addCssRule method to be removed
        //		when the list is destroyed.  Note this is effective at the time of
        //		the call to addCssRule, not at the time of destruction.
        cleanAddedRules: true,

        // addUiClasses: Boolean
        //		Whether to add jQuery UI classes to various elements in dgrid's DOM.
        addUiClasses: false,

        // highlightDuration: Integer
        //		The amount of time (in milliseconds) that a row should remain
        //		highlighted after it has been updated.
        highlightDuration: 250,

        postscript: function (params, srcNodeRef) {
            // perform setup and invoke create in postScript to allow descendants to
            // perform logic before create/postCreate happen (a la dijit/_WidgetBase)
            var grid = this;

            (this._Row = function (id, object, element) {
                this.id = id;
                this.data = object;
                this.element = element;
            }).prototype.remove = function () {
                grid.removeRow(this.element);
            };

            if (srcNodeRef) {
                // normalize srcNodeRef and store on instance during create process.
                // Doing this in postscript is a bit earlier than dijit would do it,
                // but allows subclasses to access it pre-normalized during create.
                this.srcNodeRef = srcNodeRef =
                    srcNodeRef.nodeType ? srcNodeRef : byId(srcNodeRef);
            }
            this.create(params, srcNodeRef);
        },
        listType: 'list',

        create: function (params, srcNodeRef) {
            var domNode = this.domNode = srcNodeRef || document.createElement('div'),
                cls;

            if (params) {
                this.params = params;
                declare.safeMixin(this, params);

                // Check for initial class or className in params or on domNode
                cls = params['class'] || params.className || domNode.className;
            }

            // ensure arrays and hashes are initialized
            this.sort = this.sort || [];
            this._listeners = [];
            this._rowIdToObject = {};

            this.postMixInProperties && this.postMixInProperties();

            // Apply id to widget and domNode,
            // from incoming node, widget params, or autogenerated.
            this.id = domNode.id = domNode.id || this.id || generateId();

            // Perform initial rendering, and apply classes if any were specified.
            this.buildRendering();
            if (cls) {
                setClass.call(this, cls);
            }

            this.postCreate();

            // remove srcNodeRef instance property post-create
            delete this.srcNodeRef;
            // to preserve "it just works" behavior, call startup if we're visible
            //xhack, non of your business
            //if (this.domNode.offsetHeight) {
            //	//this.startup();
            //}
        },
        buildRendering: function () {
            var domNode = this.domNode,
                addUiClasses = this.addUiClasses,
                self = this,
                headerNode,
                bodyNode,
                footerNode,
                isRTL;

            // Detect RTL on html/body nodes; taken from dojo/dom-geometry
            isRTL = this.isRTL = (document.body.dir || document.documentElement.dir ||
                document.body.style.direction).toLowerCase() === 'rtl';

            // Clear out className (any pre-applied classes will be re-applied via the
            // class / className setter), then apply standard classes/attributes
            domNode.className = '';

            domNode.setAttribute('role', 'grid');
            domClass.add(domNode, 'dgrid dgrid-' + this.listType +
                (addUiClasses ? ' ui-widget' : ''));

            // Place header node (initially hidden if showHeader is false).
            headerNode = this.headerNode = domConstruct.create('div', {
                tabIndex: -1,
                className: 'dgrid-header dgrid-header-row' + (addUiClasses ? ' ui-widget-header' : '') +
                (this.showHeader ? '' : ' dgrid-header-hidden')
            }, domNode);

            bodyNode = this.bodyNode = domConstruct.create('div', {
                className: 'dgrid-scroller'
            }, domNode);

            // Firefox 4+ adds overflow: auto elements to the tab index by default;
            // force them to not be tabbable, but restrict this to Firefox,
            // since it breaks accessibility support in other browsers
            if (has('ff')) {
                bodyNode.tabIndex = -1;
            }

            this.headerScrollNode = domConstruct.create('div', {
                className: 'dgrid-header dgrid-header-scroll dgrid-scrollbar-width' +
                (addUiClasses ? ' widget' : '')
            }, domNode);

            // Place footer node (initially hidden if showFooter is false).
            this.footerNode = domConstruct.create('div', {
                className: 'dgrid-footer' + (this.showFooter ? '' : ' dgrid-footer-hidden')
            }, domNode);

            if (isRTL) {
                domNode.className += ' dgrid-rtl' +
                    (has('dom-rtl-scrollbar-left') ? ' dgrid-rtl-swap' : '');
            }
            listen(bodyNode, 'scroll', function (event) {
                if (self.showHeader) {
                    // keep the header aligned with the body
                    headerNode.scrollLeft = event.scrollLeft || bodyNode.scrollLeft;
                }
                // re-fire, since browsers are not consistent about propagation here
                event.stopPropagation();
                listen.emit(domNode, 'scroll', {scrollTarget: bodyNode});
            });
            this.configStructure();
            this.renderHeader();
            this.contentNode = this.touchNode = domConstruct.create('div', {
                className: 'dgrid-content' + (addUiClasses ? ' widget' : '')
            }, this.bodyNode);
        },
        startup: function () {
            // summary:
            //		Called automatically after postCreate if the component is already
            //		visible; otherwise, should be called manually once placed.

            if (this._started) {
                return;
            }
            this.inherited(arguments);
            this._started = true;
            //xhack: not needed this.resize();
            // apply sort (and refresh) now that we're ready to render
            this.set('sort', this.sort);
        },

        configStructure: function () {
            // does nothing in List, this is more of a hook for the Grid
        },
        resize: function () {
            var bodyNode = this.bodyNode,
                headerNode = this.headerNode,
                footerNode = this.footerNode,
                headerHeight = headerNode.offsetHeight,
                footerHeight = this.showFooter ? footerNode.offsetHeight : 0;

            this.headerScrollNode.style.height = bodyNode.style.marginTop = headerHeight + 'px';
            bodyNode.style.marginBottom = footerHeight + 'px';

            if (!scrollbarWidth) {
                // Measure the browser's scrollbar width using a DIV we'll delete right away
                scrollbarWidth = has('dom-scrollbar-width');
                scrollbarHeight = has('dom-scrollbar-height');

                // Avoid issues with certain widgets inside in IE7, and
                // ColumnSet scroll issues with all supported IE versions
                if (has('ie')) {
                    scrollbarWidth++;
                    scrollbarHeight++;
                }

                // add rules that can be used where scrollbar width/height is needed
                //miscUtil.addCssRule('.dgrid-scrollbar-width', 'width: ' + scrollbarWidth + 'px');
                //miscUtil.addCssRule('.dgrid-scrollbar-height', 'height: ' + scrollbarHeight + 'px');

                if (scrollbarWidth !== 17) {
                    // for modern browsers, we can perform a one-time operation which adds
                    // a rule to account for scrollbar width in all grid headers.
                    //miscUtil.addCssRule('.dgrid-header-row', 'right: ' + scrollbarWidth + 'px');
                    // add another for RTL grids
                    miscUtil.addCssRule('.dgrid-rtl-swap .dgrid-header-row', 'left: ' + scrollbarWidth + 'px');
                }
            }
        },

        addCssRule: function (selector, css) {
            // summary:
            //		Version of util/misc.addCssRule which tracks added rules and removes
            //		them when the List is destroyed.

            var rule = miscUtil.addCssRule(selector, css);
            if (this.cleanAddedRules) {
                // Although this isn't a listener, it shares the same remove contract
                this._listeners.push(rule);
            }
            return rule;
        },

        on: function (eventType, listener) {
            // delegate events to the domNode
            var signal = listen(this.domNode, eventType, listener);
            if (!has('dom-addeventlistener')) {
                this._listeners.push(signal);
            }
            return signal;
        },

        cleanup: function () {
            // summary:
            //		Clears out all rows currently in the list.

            var i;
            for (i in this._rowIdToObject) {
                if (this._rowIdToObject[i] !== this.columns) {
                    var rowElement = byId(i);
                    if (rowElement) {
                        this.removeRow(rowElement, true);
                    }
                }
            }
        },
        destroy: function () {
            // summary:
            //		Destroys this grid

            // Remove any event listeners and other such removables
            if (this._listeners) { // Guard against accidental subsequent calls to destroy
                for (var i = this._listeners.length; i--;) {
                    this._listeners[i].remove();
                }
                this._listeners = null;
            }

            this._started = false;
            this.cleanup();
            // destroy DOM
            $(this.domNode).remove();
        },
        refresh: function () {
            // summary:
            //		refreshes the contents of the grid
            this.cleanup();
            this._rowIdToObject = {};
            this._autoRowId = 0;

            // make sure all the content has been removed so it can be recreated
            $(this.contentNode).empty();
            //xhack: Ensure scroll position always resets. no, we don't! this is causing side effects in xide
            //this.scrollTo({ x: 0, y: 0 });
        },
        adjustRowIndices: function (firstRow) {
            // this traverses through rows to maintain odd/even classes on the rows when indexes shift;
            var next = firstRow;
            var rowIndex = next.rowIndex;
            if (rowIndex > -1) { // make sure we have a real number in case this is called on a non-row
                do {
                    // Skip non-numeric, non-rows
                    if (next.rowIndex > -1) {
                        if (this.maintainOddEven) {
                            if (domClass.contains(next, 'dgrid-row')) {
                                domClass.replace(next, (rowIndex % 2 === 1 ? oddClass : evenClass),
                                    (rowIndex % 2 === 0 ? oddClass : evenClass));
                            }
                        }
                        next.rowIndex = rowIndex++;
                    }
                } while ((next = next.nextSibling) && next.rowIndex !== rowIndex);
            }
        },
        renderArray: function (results, beforeNode, options) {
            // summary:
            //		Renders an array of objects as rows, before the given node.

            options = options || {};
            var self = this,
                start = options.start || 0,
                rowsFragment = document.createDocumentFragment(),
                rows = [],
                container,
                i = 0,
                len = results.length;

            if (!beforeNode) {
                this._lastCollection = results;
            }

            // Insert a row for each item into the document fragment
            while (i < len) {
                rows[i] = this.insertRow(results[i], rowsFragment, null, start++, options);
                i++;
            }

            // Insert the document fragment into the appropriate position
            container = beforeNode ? beforeNode.parentNode : self.contentNode;
            if (container && container.parentNode && (container !== self.contentNode || len)) {
                container.insertBefore(rowsFragment, beforeNode || null);
                if (len) {
                    self.adjustRowIndices(rows[len - 1]);
                }
            }

            return rows;
        },

        renderHeader: function () {
            // no-op in a plain list
        },

        _autoRowId: 0,
        insertRow: function (object, parent, beforeNode, i, options) {
            // summary:
            //		Creates a single row in the grid.

            // Include parentId within row identifier if one was specified in options.
            // (This is used by tree to allow the same object to appear under
            // multiple parents.)
            var id = this.id + '-row-' + ((this.collection && this.collection.getIdentity) ?
                        this.collection.getIdentity(object) : this._autoRowId++),
                row = byId(id),
                previousRow = row && row.previousSibling;

            if (row) {
                // If it existed elsewhere in the DOM, we will remove it, so we can recreate it
                if (row === beforeNode) {
                    beforeNode = (beforeNode.connected || beforeNode).nextSibling;
                }
                this.removeRow(row, false, options);
            }
            row = this.renderRow(object, options);
            row.className = (row.className || '') + ' dgrid-row ' + (i % 2 === 1 ? oddClass : evenClass);
            // Get the row id for easy retrieval
            this._rowIdToObject[row.id = id] = object;
            parent.insertBefore(row, beforeNode || null);

            row.rowIndex = i;
            if (previousRow && previousRow.rowIndex !== (row.rowIndex - 1)) {
                // In this case, we are pulling the row from another location in the grid,
                // and we need to readjust the rowIndices from the point it was removed
                this.adjustRowIndices(previousRow);
            }
            return row;
        },
        renderRow: function (value) {
            // summary:
            //		Responsible for returning the DOM for a single row in the grid.
            // value: Mixed
            //		Value to render
            // options: Object?
            //		Optional object with additional options

            var div = document.createElement('div');
            div.appendChild(document.createTextNode(value));
            return div;
        },
        removeRow: function (rowElement, preserveDom) {
            // summary:
            //		Simply deletes the node in a plain List.
            //		Column plugins may aspect this to implement their own cleanup routines.
            // rowElement: Object|DOMNode
            //		Object or element representing the row to be removed.
            // preserveDom: Boolean?
            //		If true, the row element will not be removed from the DOM; this can
            //		be used by extensions/plugins in cases where the DOM will be
            //		massively cleaned up at a later point in time.
            // options: Object?
            //		May be specified with a `rows` property for the purpose of
            //		cleaning up collection tracking (used by `_StoreMixin`).

            rowElement = rowElement.element || rowElement;
            delete this._rowIdToObject[rowElement.id];
            if (!preserveDom) {
                //#xhack, cleanup node data right
                $(rowElement).remove();
            }
        },

        row: function (target) {
            // summary:
            //		Get the row object by id, object, node, or event
            var id;

            if (target instanceof this._Row) {
                return target; // No-op; already a row
            }

            if (!target) {
                return null;
            }
            if (target.target && target.target.nodeType) {
                // Event
                target = target.target;
            }
            if (target.nodeType) {
                // Row element, or child of a row element
                var object;
                do {
                    var rowId = target.id;
                    if ((object = this._rowIdToObject[rowId])) {
                        return new this._Row(rowId.substring(this.id.length + 5), object, target);
                    }
                    target = target.parentNode;
                } while (target && target !== this.domNode);
                return;
            }

            if (typeof target === 'object') {
                // Assume target represents a collection item
                id = this.collection.getIdentity(target);
            }
            else {
                // Assume target is a row ID
                id = target;
                target = this._rowIdToObject[this.id + '-row-' + id];
            }
            return new this._Row(id, target, byId(this.id + '-row-' + id));
        },
        cell: function (target) {
            // this doesn't do much in a plain list
            return {
                row: this.row(target)
            };
        },

        _move: function (item, steps, targetClass, visible) {
            var nextSibling, current, element;
            // Start at the element indicated by the provided row or cell object.
            element = current = item.element;
            steps = steps || 1;

            do {
                // Outer loop: move in the appropriate direction.
                if ((nextSibling = current[steps < 0 ? 'previousSibling' : 'nextSibling'])) {
                    do {
                        // Inner loop: advance, and dig into children if applicable.
                        current = nextSibling;
                        if (current && (current.className + ' ').indexOf(targetClass + ' ') > -1) {
                            // Element with the appropriate class name; count step, stop digging.
                            element = current;
                            steps += steps < 0 ? 1 : -1;
                            break;
                        }
                        // If the next sibling isn't a match, drill down to search, unless
                        // visible is true and children are hidden.
                    } while ((nextSibling = (!visible || !current.hidden) &&
                        current[steps < 0 ? 'lastChild' : 'firstChild']));
                }
                else {
                    current = current.parentNode;
                    if (!current || current === this.bodyNode || current === this.headerNode) {
                        // Break out if we step out of the navigation area entirely.
                        break;
                    }
                }
            } while (steps);
            // Return the final element we arrived at, which might still be the
            // starting element if we couldn't navigate further in that direction.
            return element;
        },

        up: function (row, steps, visible) {
            // summary:
            //		Returns the row that is the given number of steps (1 by default)
            //		above the row represented by the given object.
            // row:
            //		The row to navigate upward from.
            // steps:
            //		Number of steps to navigate up from the given row; default is 1.
            // visible:
            //		If true, rows that are currently hidden (i.e. children of
            //		collapsed tree rows) will not be counted in the traversal.
            // returns:
            //		A row object representing the appropriate row.  If the top of the
            //		list is reached before the given number of steps, the first row will
            //		be returned.
            if (!row.element) {
                row = this.row(row);
            }
            return this.row(this._move(row, -(steps || 1), 'dgrid-row', visible));
        },
        down: function (row, steps, visible) {
            // summary:
            //		Returns the row that is the given number of steps (1 by default)
            //		below the row represented by the given object.
            // row:
            //		The row to navigate downward from.
            // steps:
            //		Number of steps to navigate down from the given row; default is 1.
            // visible:
            //		If true, rows that are currently hidden (i.e. children of
            //		collapsed tree rows) will not be counted in the traversal.
            // returns:
            //		A row object representing the appropriate row.  If the bottom of the
            //		list is reached before the given number of steps, the last row will
            //		be returned.
            if (!row.element) {
                row = this.row(row);
            }
            return this.row(this._move(row, steps || 1, 'dgrid-row', visible));
        },

        scrollTo: function (options) {
            if (typeof options.x !== 'undefined') {
                this.bodyNode.scrollLeft = options.x;
            }
            if (typeof options.y !== 'undefined') {
                this.bodyNode.scrollTop = options.y;
            }
        },

        getScrollPosition: function () {
            return {
                x: this.bodyNode.scrollLeft,
                y: this.bodyNode.scrollTop
            };
        },

        get: function (/*String*/ name /*, ... */) {
            // summary:
            //		Get a property on a List instance.
            //	name:
            //		The property to get.
            //	returns:
            //		The property value on this List instance.
            // description:
            //		Get a named property on a List object. The property may
            //		potentially be retrieved via a getter method in subclasses. In the base class
            //		this just retrieves the object's property.

            var fn = '_get' + name.charAt(0).toUpperCase() + name.slice(1);

            if (typeof this[fn] === 'function') {
                return this[fn].apply(this, [].slice.call(arguments, 1));
            }
            return this[name];
        },

        set: function (/*String*/ name, /*Object*/ value /*, ... */) {
            //	summary:
            //		Set a property on a List instance
            //	name:
            //		The property to set.
            //	value:
            //		The value to set in the property.
            //	returns:
            //		The function returns this List instance.
            //	description:
            //		Sets named properties on a List object.
            //		A programmatic setter may be defined in subclasses.
            //
            //		set() may also be called with a hash of name/value pairs, ex:
            //	|	myObj.set({
            //	|		foo: "Howdy",
            //	|		bar: 3
            //	|	})
            //		This is equivalent to calling set(foo, "Howdy") and set(bar, 3)

            if (typeof name === 'object') {
                for (var k in name) {
                    this.set(k, name[k]);
                }
            }
            else {
                var fn = '_set' + name.charAt(0).toUpperCase() + name.slice(1);
                if (typeof this[fn] === 'function') {
                    this[fn].apply(this, [].slice.call(arguments, 1));
                }
                else {
                    this[name] = value;
                }
            }

            return this;
        },

        // Accept both class and className programmatically to set domNode class.
        _getClass: getClass,
        _setClass: setClass,
        _getClassName: getClass,
        _setClassName: setClass,

        _setSort: function (property, descending) {
            // summary:
            //		Sort the content
            // property: String|Array
            //		String specifying field to sort by, or actual array of objects
            //		with property and descending properties
            // descending: boolean
            //		In the case where property is a string, this argument
            //		specifies whether to sort ascending (false) or descending (true)

            this.sort = typeof property !== 'string' ? property :
                [{property: property, descending: descending}];

            this._applySort();
        },

        _applySort: function () {
            // summary:
            //		Applies the current sort
            // description:
            //		This is an extension point to allow specializations to apply the sort differently

            this.refresh();

            if (this._lastCollection) {
                var sort = this.sort;
                if (sort && sort.length > 0) {
                    var property = sort[0].property,
                        descending = !!sort[0].descending;
                    this._lastCollection.sort(function (a, b) {
                        var aVal = a[property], bVal = b[property];
                        // fall back undefined values to "" for more consistent behavior
                        if (aVal === undefined) {
                            aVal = '';
                        }
                        if (bVal === undefined) {
                            bVal = '';
                        }
                        return aVal === bVal ? 0 : (aVal > bVal !== descending ? 1 : -1);
                    });
                }
                this.renderArray(this._lastCollection);
            }
        },

        _setShowHeader: function (show) {
            // this is in List rather than just in Grid, primarily for two reasons:
            // (1) just in case someone *does* want to show a header in a List
            // (2) helps address IE < 8 header display issue in List

            var headerNode = this.headerNode;

            this.showHeader = show;

            // add/remove class which has styles for "hiding" header
            domClass.toggle(headerNode, 'dgrid-header-hidden', !show);

            this.renderHeader();
            this.resize(); // resize to account for (dis)appearance of header

            if (show) {
                // Update scroll position of header to make sure it's in sync.
                headerNode.scrollLeft = this.getScrollPosition().x;
            }
        },

        _setShowFooter: function (show) {
            this.showFooter = show;

            // add/remove class which has styles for hiding footer
            domClass.toggle(this.footerNode, 'dgrid-footer-hidden', !show);

            this.resize(); // to account for (dis)appearance of footer
        }
    });

    List.autoIdPrefix = 'dgrid_';

    return List;
});

},
'dgrid/util/misc':function(){
define([
	'dojo/has'
], function (has) {
	// summary:
	//		This module defines miscellaneous utility methods for purposes of
	//		adding styles, and throttling/debouncing function calls.

	has.add('dom-contains', function (global, doc, element) {
		return !!element.contains; // not supported by FF < 9
	});

	// establish an extra stylesheet which addCssRule calls will use,
	// plus an array to track actual indices in stylesheet for removal
	var extraRules = [],
		extraSheet,
		removeMethod,
		rulesProperty,
		invalidCssChars = /([^A-Za-z0-9_\u00A0-\uFFFF-])/g;

	function removeRule(index) {
		// Function called by the remove method on objects returned by addCssRule.
		var realIndex = extraRules[index],
			i, l;
		if (realIndex === undefined) {
			return; // already removed
		}

		// remove rule indicated in internal array at index
		extraSheet[removeMethod](realIndex);

		// Clear internal array item representing rule that was just deleted.
		// NOTE: we do NOT splice, since the point of this array is specifically
		// to negotiate the splicing that occurs in the stylesheet itself!
		extraRules[index] = undefined;

		// Then update array items as necessary to downshift remaining rule indices.
		// Can start at index + 1, since array is sparse but strictly increasing.
		for (i = index + 1, l = extraRules.length; i < l; i++) {
			if (extraRules[i] > realIndex) {
				extraRules[i]--;
			}
		}
	}

	var util = {
		// Throttle/debounce functions


		defaultDelay: 15,
		throttle: function (cb, context, delay) {
			// summary:
			//		Returns a function which calls the given callback at most once per
			//		delay milliseconds.  (Inspired by plugd)
			var ran = false;
			delay = delay || util.defaultDelay;
			return function () {
				if (ran) {
					return;
				}
				ran = true;
				cb.apply(context, arguments);
				setTimeout(function () {
					ran = false;
				}, delay);
			};
		},
		throttleDelayed: function (cb, context, delay) {
			// summary:
			//		Like throttle, except that the callback runs after the delay,
			//		rather than before it.
			var ran = false;
			delay = delay || util.defaultDelay;
			return function () {
				if (ran) {
					return;
				}
				ran = true;
				var a = arguments;
				setTimeout(function () {
					ran = false;
					cb.apply(context, a);
				}, delay);
			};
		},
		debounce: function (cb, context, delay) {
			// summary:
			//		Returns a function which calls the given callback only after a
			//		certain time has passed without successive calls.  (Inspired by plugd)
			var timer;
			delay = delay || util.defaultDelay;
			return function () {
				if (timer) {
					clearTimeout(timer);
					timer = null;
				}
				var a = arguments;
				timer = setTimeout(function () {
					cb.apply(context, a);
				}, delay);
			};
		},

		// Iterative functions

		each: function (arrayOrObject, callback, context) {
			// summary:
			//		Given an array or object, iterates through its keys.
			//		Does not use hasOwnProperty (since even Dojo does not
			//		consistently use it), but will iterate using a for or for-in
			//		loop as appropriate.

			var i, len;

			if (!arrayOrObject) {
				return;
			}

			if (typeof arrayOrObject.length === 'number') {
				for (i = 0, len = arrayOrObject.length; i < len; i++) {
					callback.call(context, arrayOrObject[i], i, arrayOrObject);
				}
			}
			else {
				for (i in arrayOrObject) {
					callback.call(context, arrayOrObject[i], i, arrayOrObject);
				}
			}
		},

		// DOM-related functions

		contains: function (parent, node) {
			// summary:
			//		Checks to see if an element is contained in another element.

			if (has('dom-contains')) {
				return parent.contains(node);
			}
			else {
				return parent.compareDocumentPosition(node) & /* DOCUMENT_POSITION_CONTAINS */ 8;
			}
		},

		// CSS-related functions

		addCssRule: function (selector, css) {
			// summary:
			//		Dynamically adds a style rule to the document.  Returns an object
			//		with a remove method which can be called to later remove the rule.

			if (!extraSheet) {
				// First time, create an extra stylesheet for adding rules
				extraSheet = document.createElement('style');
				document.getElementsByTagName('head')[0].appendChild(extraSheet);
				// Keep reference to actual StyleSheet object (`styleSheet` for IE < 9)
				extraSheet = extraSheet.sheet || extraSheet.styleSheet;
				// Store name of method used to remove rules (`removeRule` for IE < 9)
				removeMethod = extraSheet.deleteRule ? 'deleteRule' : 'removeRule';
				// Store name of property used to access rules (`rules` for IE < 9)
				rulesProperty = extraSheet.cssRules ? 'cssRules' : 'rules';
			}

			var index = extraRules.length;
			extraRules[index] = (extraSheet.cssRules || extraSheet.rules).length;
			extraSheet.addRule ?
				extraSheet.addRule(selector, css) :
				extraSheet.insertRule(selector + '{' + css + '}', extraRules[index]);

			return {
				get: function (prop) {
					return extraSheet[rulesProperty][extraRules[index]].style[prop];
				},
				set: function (prop, value) {
					if (typeof extraRules[index] !== 'undefined') {
						extraSheet[rulesProperty][extraRules[index]].style[prop] = value;
					}
				},
				remove: function () {
					removeRule(index);
				}
				
			};
		},

		escapeCssIdentifier: function (id, replace) {
			// summary:
			//		Escapes normally-invalid characters in a CSS identifier (such as . or :);
			//		see http://www.w3.org/TR/CSS2/syndata.html#value-def-identifier
			// id: String
			//		CSS identifier (e.g. tag name, class, or id) to be escaped
			// replace: String?
			//		If specified, indicates that invalid characters should be
			//		replaced by the given string rather than being escaped

			return typeof id === 'string' ? id.replace(invalidCssChars, replace || '\\$1') : id;
		}
	};
	return util;
});
},
'dgrid/util/touch':function(){
define([
	'dojo/on'
	//'dojo/query'
], function (on, query) {
	// This module exposes useful functions for working with touch devices.

	var util = {
		// Overridable defaults related to extension events defined below.
		tapRadius: 10,
		dbltapTime: 250,

		selector: function (selector, eventType, children) {
			// summary:
			//		Reimplementation of on.selector, taking an iOS quirk into account
			return function (target, listener) {
				var bubble = eventType.bubble;
				if (bubble) {
					// the event type doesn't naturally bubble, but has a bubbling form, use that
					eventType = bubble;
				}
				else if (children !== false) {
					// for normal bubbling events we default to allowing children of the selector
					children = true;
				}
				return on(target, eventType, function (event) {
					var eventTarget = event.target;

					// iOS tends to report the text node an event was fired on, rather than
					// the top-level element; this may end up causing errors in selector engines
					if (eventTarget.nodeType === 3) {
						eventTarget = eventTarget.parentNode;
					}

					// there is a selector, so make sure it matches
					while (!query.matches(eventTarget, selector, target)) {
						//debugger;
						if (eventTarget === target || !children || !(eventTarget = eventTarget.parentNode)) {
							return;
						}
					}
					return listener.call(eventTarget, event);
				});
			};
		},

		countCurrentTouches: function (evt, node) {
			// summary:
			//		Given a touch event and a DOM node, counts how many current touches
			//		presently lie within that node.  Useful in cases where an accurate
			//		count is needed but tracking changedTouches won't suffice because
			//		other handlers stop events from bubbling high enough.

			if (!('touches' in evt)) {
				// Not a touch event (perhaps called from a mouse event on a
				// platform supporting touch events)
				return -1;
			}

			var i, numTouches, touch;
			for (i = 0, numTouches = 0; (touch = evt.touches[i]); ++i) {
				if (node.contains(touch.target)) {
					++numTouches;
				}
			}
			return numTouches;
		}
	};

	function handleTapStart(target, listener, evt, prevent) {
		// Common function for handling tap detection.
		// The passed listener will only be fired when and if a touchend is fired
		// which confirms the overall gesture resembled a tap.

		if (evt.targetTouches.length > 1) {
			return; // ignore multitouch
		}

		var start = evt.changedTouches[0],
			startX = start.screenX,
			startY = start.screenY;

		prevent && evt.preventDefault();

		var endListener = on(target, 'touchend', function (evt) {
			var end = evt.changedTouches[0];
			if (!evt.targetTouches.length) {
				// only call listener if this really seems like a tap
				if (Math.abs(end.screenX - startX) < util.tapRadius &&
						Math.abs(end.screenY - startY) < util.tapRadius) {
					prevent && evt.preventDefault();
					listener.call(this, evt);
				}
				endListener.remove();
			}
		});
	}

	function tap(target, listener) {
		// Function usable by dojo/on as a synthetic tap event.
		return on(target, 'touchstart', function (evt) {
			handleTapStart(target, listener, evt);
		});
	}

	function dbltap(target, listener) {
		// Function usable by dojo/on as a synthetic double-tap event.
		var first, timeout;

		return on(target, 'touchstart', function (evt) {
			if (!first) {
				// first potential tap: detect as usual, but with specific logic
				handleTapStart(target, function (evt) {
					first = evt.changedTouches[0];
					timeout = setTimeout(function () {
						first = timeout = null;
					}, util.dbltapTime);
				}, evt);
			}
			else {
				handleTapStart(target, function (evt) {
					// bail out if first was cleared between 2nd touchstart and touchend
					if (!first) {
						return;
					}
					var second = evt.changedTouches[0];
					// only call listener if both taps occurred near the same place
					if (Math.abs(second.screenX - first.screenX) < util.tapRadius &&
							Math.abs(second.screenY - first.screenY) < util.tapRadius) {
						timeout && clearTimeout(timeout);
						first = timeout = null;
						listener.call(this, evt);
					}
				}, evt, true);
			}
		});
	}

	util.tap = tap;
	util.dbltap = dbltap;

	return util;
});
},
'dgrid/Selection':function(){
define([
	'dojo/_base/declare',
	'dojo/dom-class',
	'dojo/on',
	'dojo/has',
	'dojo/aspect',
	'./List',
	'dgrid/util/touch',
	'dojo/query',
	'dojo/_base/sniff',
	'dojo/dom'
], function (declare, domClass, on, has, aspect, List, touchUtil) {

	has.add('dom-comparedocumentposition', function (global, doc, element) {
		return !!element.compareDocumentPosition;
	});

	// Add a feature test for the onselectstart event, which offers a more
	// graceful fallback solution than node.unselectable.
	has.add('dom-selectstart', typeof document.onselectstart !== 'undefined');

	var ctrlEquiv = has('mac') ? 'metaKey' : 'ctrlKey',
		hasUserSelect = has('css-user-select'),
		hasPointer = has('pointer'),
		hasMSPointer = hasPointer && hasPointer.slice(0, 2) === 'MS',
		downType = hasPointer ? hasPointer + (hasMSPointer ? 'Down' : 'down') : 'mousedown',
		upType = hasPointer ? hasPointer + (hasMSPointer ? 'Up' : 'up') : 'mouseup';

	if (hasUserSelect === 'WebkitUserSelect' && typeof document.documentElement.style.msUserSelect !== 'undefined') {
		// Edge defines both webkit and ms prefixes, rendering feature detects as brittle as UA sniffs...
		hasUserSelect = false;
	}

	function makeUnselectable(node, unselectable) {
		// Utility function used in fallback path for recursively setting unselectable
		var value = node.unselectable = unselectable ? 'on' : '',
			elements = node.getElementsByTagName('*'),
			i = elements.length;

		while (--i) {
			if (elements[i].tagName === 'INPUT' || elements[i].tagName === 'TEXTAREA') {
				continue; // Don't prevent text selection in text input fields.
			}
			elements[i].unselectable = value;
		}
	}

	function setSelectable(grid, selectable) {
		// Alternative version of dojo/dom.setSelectable based on feature detection.

		// For FF < 21, use -moz-none, which will respect -moz-user-select: text on
		// child elements (e.g. form inputs).  In FF 21, none behaves the same.
		// See https://developer.mozilla.org/en-US/docs/CSS/user-select
		var node = grid.bodyNode,
			value = selectable ? 'text' : has('ff') < 21 ? '-moz-none' : 'none';

		// In IE10+, -ms-user-select: none will block selection from starting within the
		// element, but will not block an existing selection from entering the element.
		// When using a modifier key, IE will select text inside of the element as well
		// as outside of the element, because it thinks the selection started outside.
		// Therefore, fall back to other means of blocking selection for IE10+.
		// Newer versions of Dojo do not even report msUserSelect (see https://github.com/dojo/dojo/commit/7ae2a43).
		if (hasUserSelect && hasUserSelect !== 'msUserSelect') {
			node.style[hasUserSelect] = value;
		}
		else if (has('dom-selectstart')) {
			// For browsers that don't support user-select but support selectstart (IE<10),
			// we can hook up an event handler as necessary.  Since selectstart bubbles,
			// it will handle any child elements as well.
			// Note, however, that both this and the unselectable fallback below are
			// incapable of preventing text selection from outside the targeted node.
			if (!selectable && !grid._selectstartHandle) {
				grid._selectstartHandle = on(node, 'selectstart', function (evt) {
					var tag = evt.target && evt.target.tagName;

					// Prevent selection except where a text input field is involved.
					if (tag !== 'INPUT' && tag !== 'TEXTAREA') {
						evt.preventDefault();
					}
				});
			}
			else if (selectable && grid._selectstartHandle) {
				grid._selectstartHandle.remove();
				delete grid._selectstartHandle;
			}
		}
		else {
			// For browsers that don't support either user-select or selectstart (Opera),
			// we need to resort to setting the unselectable attribute on all nodes
			// involved.  Since this doesn't automatically apply to child nodes, we also
			// need to re-apply it whenever rows are rendered.
			makeUnselectable(node, !selectable);
			if (!selectable && !grid._unselectableHandle) {
				grid._unselectableHandle = aspect.after(grid, 'renderRow', function (row) {
					makeUnselectable(row, true);
					return row;
				});
			}
			else if (selectable && grid._unselectableHandle) {
				grid._unselectableHandle.remove();
				delete grid._unselectableHandle;
			}
		}
	}

	return declare(null, {
		// summary:
		//		Add selection capabilities to a grid. The grid will have a selection property and
		//		fire "dgrid-select" and "dgrid-deselect" events.

		// selectionDelegate: String
		//		Selector to delegate to as target of selection events.
		selectionDelegate: '.dgrid-row',

		// selectionEvents: String|Function
		//		Event (or comma-delimited events, or extension event) to listen on
		//		to trigger select logic.
		selectionEvents: downType + ',' + upType + ',dgrid-cellfocusin',

		// selectionTouchEvents: String|Function
		//		Event (or comma-delimited events, or extension event) to listen on
		//		in addition to selectionEvents for touch devices.
		selectionTouchEvents: has('touch') ? touchUtil.tap : null,

		// deselectOnRefresh: Boolean
		//		If true, the selection object will be cleared when refresh is called.
		deselectOnRefresh: true,

		// allowSelectAll: Boolean
		//		If true, allow ctrl/cmd+A to select all rows.
		//		Also consulted by the selector plugin for showing select-all checkbox.
		allowSelectAll: false,

		// selection:
		//		An object where the property names correspond to
		//		object ids and values are true or false depending on whether an item is selected
		selection: {},

		// selectionMode: String
		//		The selection mode to use, can be "none", "multiple", "single", or "extended".
		selectionMode: 'extended',

		// allowTextSelection: Boolean
		//		Whether to still allow text within cells to be selected.  The default
		//		behavior is to allow text selection only when selectionMode is none;
		//		setting this property to either true or false will explicitly set the
		//		behavior regardless of selectionMode.
		allowTextSelection: undefined,

		// _selectionTargetType: String
		//		Indicates the property added to emitted events for selected targets;
		//		overridden in CellSelection
		_selectionTargetType: 'rows',

		create: function () {
			this.selection = {};
			return this.inherited(arguments);
		},
		postCreate: function () {
			this.inherited(arguments);

			this._initSelectionEvents();

			// Force selectionMode setter to run
			var selectionMode = this.selectionMode;
			this.selectionMode = '';
			this._setSelectionMode(selectionMode);
		},

		destroy: function () {
			this.inherited(arguments);

			// Remove any extra handles added by Selection.
			if (this._selectstartHandle) {
				this._selectstartHandle.remove();
			}
			if (this._unselectableHandle) {
				this._unselectableHandle.remove();
			}
			if (this._removeDeselectSignals) {
				this._removeDeselectSignals();
			}
		},

		_setSelectionMode: function (mode) {
			// summary:
			//		Updates selectionMode, resetting necessary variables.

			if (mode === this.selectionMode) {
				return;
			}

			// Start selection fresh when switching mode.
			this.clearSelection();

			this.selectionMode = mode;

			// Compute name of selection handler for this mode once
			// (in the form of _fooSelectionHandler)
			this._selectionHandlerName = '_' + mode + 'SelectionHandler';

			// Also re-run allowTextSelection setter in case it is in automatic mode.
			this._setAllowTextSelection(this.allowTextSelection);
		},

		_setAllowTextSelection: function (allow) {
			if (typeof allow !== 'undefined') {
				setSelectable(this, allow);
			}
			else {
				setSelectable(this, this.selectionMode === 'none');
			}
			this.allowTextSelection = allow;
		},

		_handleSelect: function (event, target) {
			// Don't run if selection mode doesn't have a handler (incl. "none"), target can't be selected,
			// or if coming from a dgrid-cellfocusin from a mousedown
			if (!this[this._selectionHandlerName] || !this.allowSelect(this.row(target)) ||
					(event.type === 'dgrid-cellfocusin' && event.parentType === 'mousedown') ||
					(event.type === upType && target !== this._waitForMouseUp)) {
				return;
			}
			this._waitForMouseUp = null;
			this._selectionTriggerEvent = event;

			// Don't call select handler for ctrl+navigation
			if (!event.keyCode || !event.ctrlKey || event.keyCode === 32) {
				// If clicking a selected item, wait for mouseup so that drag n' drop
				// is possible without losing our selection
				if (!event.shiftKey && event.type === downType && this.isSelected(target)) {
					this._waitForMouseUp = target;
				}
				else {
					this[this._selectionHandlerName](event, target);
				}
			}
			this._selectionTriggerEvent = null;
		},

		_singleSelectionHandler: function (event, target) {
			// summary:
			//		Selection handler for "single" mode, where only one target may be
			//		selected at a time.

			var ctrlKey = event.keyCode ? event.ctrlKey : event[ctrlEquiv];
			if (this._lastSelected === target) {
				// Allow ctrl to toggle selection, even within single select mode.
				this.select(target, null, !ctrlKey || !this.isSelected(target));
			}
			else {
				this.clearSelection();
				this.select(target);
				this._lastSelected = target;
			}
		},

		_multipleSelectionHandler: function (event, target) {
			// summary:
			//		Selection handler for "multiple" mode, where shift can be held to
			//		select ranges, ctrl/cmd can be held to toggle, and clicks/keystrokes
			//		without modifier keys will add to the current selection.

			var lastRow = this._lastSelected,
				ctrlKey = event.keyCode ? event.ctrlKey : event[ctrlEquiv],
				value;

			if (!event.shiftKey) {
				// Toggle if ctrl is held; otherwise select
				value = ctrlKey ? null : true;
				lastRow = null;
			}

			this.select(target, lastRow, value,null,event.type.indexOf('mouse')!==-1 ? 'mouse' : event.type);

			if (!lastRow) {
				// Update reference for potential subsequent shift+select
				// (current row was already selected above)
				this._lastSelected = target;
			}
		},

		_extendedSelectionHandler: function (event, target) {
			// summary:
			//		Selection handler for "extended" mode, which is like multiple mode
			//		except that clicks/keystrokes without modifier keys will clear
			//		the previous selection.

			// Clear selection first for right-clicks outside selection and non-ctrl-clicks;
			// otherwise, extended mode logic is identical to multiple mode
			if (event.button === 2 ? !this.isSelected(target) :
					!(event.keyCode ? event.ctrlKey : event[ctrlEquiv])) {
				this.clearSelection(null, true);
			}
			this._multipleSelectionHandler(event, target);
		},

		_toggleSelectionHandler: function (event, target) {
			// summary:
			//		Selection handler for "toggle" mode which simply toggles the selection
			//		of the given target.  Primarily useful for touch input.

			this.select(target, null, null);
		},

		_initSelectionEvents: function () {
			// summary:
			//		Performs first-time hookup of event handlers containing logic
			//		required for selection to operate.

			var grid = this,
				contentNode = this.contentNode,
				selector = this.selectionDelegate;

			this._selectionEventQueues = {
				deselect: [],
				select: []
			};

			if (has('touch') && !has('pointer') && this.selectionTouchEvents) {
				// Listen for taps, and also for mouse/keyboard, making sure not
				// to trigger both for the same interaction
				on(contentNode, touchUtil.selector(selector, this.selectionTouchEvents), function (evt) {
					grid._handleSelect(evt, this);
					grid._ignoreMouseSelect = this;
				});
				on(contentNode, on.selector(selector, this.selectionEvents), function (event) {
					if (grid._ignoreMouseSelect !== this) {
						grid._handleSelect(event, this);
					}
					else if (event.type === upType) {
						grid._ignoreMouseSelect = null;
					}
				});
			}
			else {
				// Listen for mouse/keyboard actions that should cause selections
				on(contentNode, on.selector(selector, this.selectionEvents), function (event) {
					grid._handleSelect(event, this);
				});
			}

			// Also hook up spacebar (for ctrl+space)
			if (this.addKeyHandler) {
				this.addKeyHandler(32, function (event) {
					grid._handleSelect(event, event.target);
				});
			}

			// If allowSelectAll is true, bind ctrl/cmd+A to (de)select all rows,
			// unless the event was received from an editor component.
			// (Handler further checks against _allowSelectAll, which may be updated
			// if selectionMode is changed post-init.)
			if (this.allowSelectAll) {
				this.on('keydown', function (event) {
					if (event[ctrlEquiv] && event.keyCode === 65 &&
							!/\bdgrid-input\b/.test(event.target.className)) {
						event.preventDefault();
						grid[grid.allSelected ? 'clearSelection' : 'selectAll']();
					}
				});
			}

			// Update aspects if there is a collection change
			if (this._setCollection) {
				aspect.before(this, '_setCollection', function (collection) {
					grid._updateDeselectionAspect(collection);
				});
			}
			this._updateDeselectionAspect();
		},

		_updateDeselectionAspect: function (collection) {
			// summary:
			//		Hooks up logic to handle deselection of removed items.
			//		Aspects to a trackable collection's notify method if applicable,
			//		or to the list/grid's removeRow method otherwise.

			var self = this,
				signals;

			function ifSelected(rowArg, methodName,why) {
				// Calls a method if the row corresponding to the object is selected.
				var row = self.row(rowArg),
					selection = row && self.selection[row.id];
				// Is the row currently in the selection list.
				if (selection) {
					self[methodName](row,null,true,null,why);
				}
			}

			// Remove anything previously configured
			if (this._removeDeselectSignals) {
				this._removeDeselectSignals();
			}

			if (collection && collection.track && this._observeCollection) {
				signals = [
					aspect.before(this, '_observeCollection', function (collection) {
						signals.push(
							collection.on('delete', function (event) {
								if (typeof event.index === 'undefined') {
									// Call deselect on the row if the object is being removed.  This allows the
									// deselect event to reference the row element while it still exists in the DOM.
									ifSelected(event.id, 'deselect');
								}
							})
						);
					}),
					aspect.after(this, '_observeCollection', function (collection) {
						signals.push(
							collection.on('update', function (event) {
								if (typeof event.index !== 'undefined') {
									// When List updates an item, the row element is removed and a new one inserted.
									// If at this point the object is still in grid.selection,
									// then call select on the row so the element's CSS is updated.
									ifSelected(collection.getIdentity(event.target), 'select','update');
								}
							})
						);
					}, true)
				];
			}
			else {
				signals = [
					aspect.before(this, 'removeRow', function (rowElement, preserveDom) {
						var row;
						if (!preserveDom) {
							row = this.row(rowElement);
							// if it is a real row removal for a selected item, deselect it
							if (row && (row.id in this.selection)) {
								this.deselect(row);
							}
						}
					})
				];
			}

			this._removeDeselectSignals = function () {
				for (var i = signals.length; i--;) {
					signals[i].remove();
				}
				signals = [];
			};
		},

		allowSelect: function () {
			// summary:
			//		A method that can be overriden to determine whether or not a row (or
			//		cell) can be selected. By default, all rows (or cells) are selectable.
			// target: Object
			//		Row object (for Selection) or Cell object (for CellSelection) for the
			//		row/cell in question
			return true;
		},

		_fireSelectionEvent: function (type) {
			// summary:
			//		Fires an event for the accumulated rows once a selection
			//		operation is finished (whether singular or for a range)

			var queue = this._selectionEventQueues[type],
				triggerEvent = this._selectionTriggerEvent,
				eventObject;

			eventObject = {
				bubbles: true,
				grid: this
			};
			if (triggerEvent) {
				eventObject.parentType = triggerEvent.type;
			}
			eventObject[this._selectionTargetType] = queue;

			// Clear the queue so that the next round of (de)selections starts anew
			this._selectionEventQueues[type] = [];

			on.emit(this.contentNode, 'dgrid-' + type, eventObject);
		},

		_fireSelectionEvents: function () {
			var queues = this._selectionEventQueues,
				type;

			for (type in queues) {
				if (queues[type].length) {
					this._fireSelectionEvent(type);
				}
			}
		},

		_select: function (row, toRow, value) {
			// summary:
			//		Contains logic for determining whether to select targets, but
			//		does not emit events.  Called from select, deselect, selectAll,
			//		and clearSelection.

			var selection,
				previousValue,
				element,
				toElement,
				direction;

			if (typeof value === 'undefined') {
				// default to true
				value = true;
			}
			if (!row.element) {
				row = this.row(row);
			}

			// Check whether we're allowed to select the given row before proceeding.
			// If a deselect operation is being performed, this check is skipped,
			// to avoid errors when changing column definitions, and since disabled
			// rows shouldn't ever be selected anyway.
			if (value === false || this.allowSelect(row)) {
				selection = this.selection;
				previousValue = !!selection[row.id];
				if (value === null) {
					// indicates a toggle
					value = !previousValue;
				}
				element = row.element;
				if (!value && !this.allSelected) {
					delete this.selection[row.id];
				}
				else {
					selection[row.id] = value;
				}
				if (element) {
					// add or remove classes as appropriate
					if (value) {
						domClass.add(element, 'dgrid-selected' +
							(this.addUiClasses ? ' ui-state-active' : ''));
					}
					else {
						domClass.remove(element, 'dgrid-selected ui-state-active');
					}
				}
				if (value !== previousValue && element) {
					// add to the queue of row events
					this._selectionEventQueues[(value ? '' : 'de') + 'select'].push(row);
				}

				if (toRow) {
					if (!toRow.element) {
						toRow = this.row(toRow);
					}

					if (!toRow) {
						this._lastSelected = element;
						console.warn('The selection range has been reset because the ' +
							'beginning of the selection is no longer in the DOM. ' +
							'If you are using OnDemandList, you may wish to increase ' +
							'farOffRemoval to avoid this, but note that keeping more nodes ' +
							'in the DOM may impact performance.');
						return;
					}

					toElement = toRow.element;
					if (toElement) {
						direction = this._determineSelectionDirection(element, toElement);
						if (!direction) {
							// The original element was actually replaced
							toElement = document.getElementById(toElement.id);
							direction = this._determineSelectionDirection(element, toElement);
						}
						while (row.element !== toElement && (row = this[direction](row))) {
							this._select(row, null, value);
						}
					}
				}
			}
		},

		// Implement _determineSelectionDirection differently based on whether the
		// browser supports element.compareDocumentPosition; use sourceIndex for IE<9
		_determineSelectionDirection: has('dom-comparedocumentposition') ? function (from, to) {
			var result = to.compareDocumentPosition(from);
			if (result & 1) {
				return false; // Out of document
			}
			return result === 2 ? 'down' : 'up';
		} : function (from, to) {
			if (to.sourceIndex < 1) {
				return false; // Out of document
			}
			return to.sourceIndex > from.sourceIndex ? 'down' : 'up';
		},

		select: function (row, toRow, value) {
			// summary:
			//		Selects or deselects the given row or range of rows.
			// row: Mixed
			//		Row object (or something that can resolve to one) to (de)select
			// toRow: Mixed
			//		If specified, the inclusive range between row and toRow will
			//		be (de)selected
			// value: Boolean|Null
			//		Whether to select (true/default), deselect (false), or toggle
			//		(null) the row

			this._select(row, toRow, value);
			this._fireSelectionEvents();
		},
		deselect: function (row, toRow) {
			// summary:
			//		Deselects the given row or range of rows.
			// row: Mixed
			//		Row object (or something that can resolve to one) to deselect
			// toRow: Mixed
			//		If specified, the inclusive range between row and toRow will
			//		be deselected

			this.select(row, toRow, false);
		},

		clearSelection: function (exceptId, dontResetLastSelected) {
			// summary:
			//		Deselects any currently-selected items.
			// exceptId: Mixed?
			//		If specified, the given id will not be deselected.

			this.allSelected = false;
			for (var id in this.selection) {
				if (exceptId !== id) {
					this._select(id, null, false);
				}
			}
			if (!dontResetLastSelected) {
				this._lastSelected = null;
			}
			this._fireSelectionEvents();
		},
		selectAll: function () {
			this.allSelected = true;
			this.selection = {}; // we do this to clear out pages from previous sorts
			for (var i in this._rowIdToObject) {
				var row = this.row(this._rowIdToObject[i]);
				this._select(row.id, null, true);
			}
			this._fireSelectionEvents();
		},

		isSelected: function (object) {
			// summary:
			//		Returns true if the indicated row is selected.

			if (typeof object === 'undefined' || object === null) {
				return false;
			}
			if (!object.element) {
				object = this.row(object);
			}
/*
            if (typeof object === 'undefined' || object === null) {
                return false;
            }*/


			// First check whether the given row is indicated in the selection hash;
			// failing that, check if allSelected is true (testing against the
			// allowSelect method if possible)
			return (object.id in this.selection) ? !!this.selection[object.id] :
				this.allSelected && (!object.data || this.allowSelect(object));
		},

		refresh: function () {
			if (this.deselectOnRefresh) {
				this.clearSelection();
			}
			this._lastSelected = null;
			return this.inherited(arguments);
		},

		renderArray: function () {
			var rows = this.inherited(arguments),
				selection = this.selection,
				i,
				row,
				selected;

			for (i = 0; i < rows.length; i++) {
				row = this.row(rows[i]);
				selected = row.id in selection ? selection[row.id] : this.allSelected;
				if (selected) {
					this.select(row, null, selected,null,'renderArray');
				}
			}
			this._fireSelectionEvents();
			return rows;
		}
	});
});

},
'dgrid/OnDemandList':function(){
define([
	'./List',
	'./_StoreMixin',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/dom-construct',
	'dojo/on',
	'dojo/when',
	'./util/misc'
], function (List, _StoreMixin, declare, lang, domConstruct, on, when, miscUtil) {

	return declare([ List, _StoreMixin ], {
		// summary:
		//		Extends List to include virtual scrolling functionality, querying a
		//		dojo/store instance for the appropriate range when the user scrolls.

		// minRowsPerPage: Integer
		//		The minimum number of rows to request at one time.
		minRowsPerPage: 2500,

		// maxRowsPerPage: Integer
		//		The maximum number of rows to request at one time.
		maxRowsPerPage: 250,

		// maxEmptySpace: Integer
		//		Defines the maximum size (in pixels) of unrendered space below the
		//		currently-rendered rows. Setting this to less than Infinity can be useful if you
		//		wish to limit the initial vertical scrolling of the grid so that the scrolling is
		// 		not excessively sensitive. With very large grids of data this may make scrolling
		//		easier to use, albiet it can limit the ability to instantly scroll to the end.
		maxEmptySpace: Infinity,

		// bufferRows: Integer
		//	  The number of rows to keep ready on each side of the viewport area so that the user can
		//	  perform local scrolling without seeing the grid being built. Increasing this number can
		//	  improve perceived performance when the data is being retrieved over a slow network.
		bufferRows: 10,

		// farOffRemoval: Integer
		//		Defines the minimum distance (in pixels) from the visible viewport area
		//		rows must be in order to be removed.  Setting to Infinity causes rows
		//		to never be removed.
		farOffRemoval: 2000,

		// queryRowsOverlap: Integer
		//		Indicates the number of rows to overlap queries. This helps keep
		//		continuous data when underlying data changes (and thus pages don't
		//		exactly align)
		queryRowsOverlap: 0,

		// pagingMethod: String
		//		Method (from dgrid/util/misc) to use to either throttle or debounce
		//		requests.  Default is "debounce" which will cause the grid to wait until
		//		the user pauses scrolling before firing any requests; can be set to
		//		"throttleDelayed" instead to progressively request as the user scrolls,
		//		which generally incurs more overhead but might appear more responsive.
		pagingMethod: 'debounce',

		// pagingDelay: Integer
		//		Indicates the delay (in milliseconds) imposed upon pagingMethod, to wait
		//		before paging in more data on scroll events. This can be increased to
		//		reduce client-side overhead or the number of requests sent to a server.
		pagingDelay: miscUtil.defaultDelay,

		// keepScrollPosition: Boolean
		//		When refreshing the list, controls whether the scroll position is
		//		preserved, or reset to the top.  This can also be overridden for
		//		specific calls to refresh.
		keepScrollPosition: true,

		// rowHeight: Number
		//		Average row height, computed in renderQuery during the rendering of
		//		the first range of data.
		rowHeight: 0,

		postCreate: function () {
			this.inherited(arguments);
			var self = this;
			// check visibility on scroll events
			on(this.bodyNode, 'scroll',
				miscUtil[this.pagingMethod](function (event) {
					self._processScroll(event);
				}, null, this.pagingDelay)
			);
		},

		destroy: function () {
			this.inherited(arguments);
			if (this._refreshTimeout) {
				clearTimeout(this._refreshTimeout);
			}
		},

		renderQuery: function (query, options) {
			// summary:
			//		Creates a preload node for rendering a query into, and executes the query
			//		for the first page of data. Subsequent data will be downloaded as it comes
			//		into view.
			// query: Function
			//		Function to be called when requesting new data.
			// options: Object?
			//		Optional object containing the following:
			//		* container: Container to build preload nodes within; defaults to this.contentNode

			var self = this,
				container = (options && options.container) || this.contentNode,
				preload = {
					query: query,
					count: 0
				},
				preloadNode,
				priorPreload = this.preload;

			// Initial query; set up top and bottom preload nodes
			var topPreload = {
				node: domConstruct.create('div', {
					className: 'dgrid-preload',
					style: { height: '0' }
				}, container),
				count: 0,
				query: query,
				next: preload
			};
			topPreload.node.rowIndex = 0;
			preload.node = preloadNode = domConstruct.create('div', {
				className: 'dgrid-preload'
			}, container);
			preload.previous = topPreload;

			// this preload node is used to represent the area of the grid that hasn't been
			// downloaded yet
			preloadNode.rowIndex = this.minRowsPerPage;

			if (priorPreload) {
				// the preload nodes (if there are multiple) are represented as a linked list, need to insert it
				if ((preload.next = priorPreload.next) &&
						// is this preload node below the prior preload node?
						preloadNode.offsetTop >= priorPreload.node.offsetTop) {
					// the prior preload is above/before in the linked list
					preload.previous = priorPreload;
				}
				else {
					// the prior preload is below/after in the linked list
					preload.next = priorPreload;
					preload.previous = priorPreload.previous;
				}
				// adjust the previous and next links so the linked list is proper
				preload.previous.next = preload;
				preload.next.previous = preload;
			}
			else {
				this.preload = preload;
			}

			var loadingNode = domConstruct.create('div', {
					className: 'dgrid-loading'
				}, preloadNode, 'before'),
				innerNode = domConstruct.create('div', {
					className: 'dgrid-below'
				}, loadingNode);
			innerNode.innerHTML = this.loadingMessage;

			// Establish query options, mixing in our own.
			options = lang.mixin({ start: 0, count: this.minRowsPerPage },
				'level' in query ? { queryLevel: query.level } : null);

			// Protect the query within a _trackError call, but return the resulting collection
			return this._trackError(function () {
				var results = query(options);

				// Render the result set
				return self.renderQueryResults(results, preloadNode, options).then(function (trs) {
					return results.totalLength.then(function (total) {
						var trCount = trs.length,
							parentNode = preloadNode.parentNode,
							noDataNode = self.noDataNode;

						if (self._rows) {
							self._rows.min = 0;
							self._rows.max = trCount === total ? Infinity : trCount - 1;
						}

						domConstruct.destroy(loadingNode);
						if (!('queryLevel' in options)) {
							self._total = total;
						}
						// now we need to adjust the height and total count based on the first result set
						if (total === 0 && parentNode) {
							if (noDataNode) {
								domConstruct.destroy(noDataNode);
								delete self.noDataNode;
							}
							self.noDataNode = noDataNode = domConstruct.create('div', {
								className: 'dgrid-no-data',
								innerHTML: self.noDataMessage
							});
							self._emit('noData');
							parentNode.insertBefore(noDataNode, self._getFirstRowSibling(parentNode));
						}
						var height = 0;
						for (var i = 0; i < trCount; i++) {
							height += self._calcRowHeight(trs[i]);
						}
						// only update rowHeight if we actually got results and are visible
						if (trCount && height) {
							self.rowHeight = height / trCount;
						}

						total -= trCount;
						preload.count = total;
						preloadNode.rowIndex = trCount;
						if (total) {
							preloadNode.style.height = Math.min(total * self.rowHeight, self.maxEmptySpace) + 'px';
						}
						else {
							preloadNode.style.display = 'none';
						}

						if (self._previousScrollPosition) {
							// Restore position after a refresh operation w/ keepScrollPosition
							self.scrollTo(self._previousScrollPosition);
							delete self._previousScrollPosition;
						}

						// Redo scroll processing in case the query didn't fill the screen,
						// or in case scroll position was restored
						return when(self._processScroll()).then(function () {
							return trs;
						});
					});
				}).otherwise(function (err) {
					// remove the loadingNode and re-throw
					domConstruct.destroy(loadingNode);
					throw err;
				});
			});
		},

		refresh: function (options) {
			// summary:
			//		Refreshes the contents of the grid.
			// options: Object?
			//		Optional object, supporting the following parameters:
			//		* keepScrollPosition: like the keepScrollPosition instance property;
			//			specifying it in the options here will override the instance
			//			property's value for this specific refresh call only.

			var self = this,
				keep = (options && options.keepScrollPosition);

			// Fall back to instance property if option is not defined
			if (typeof keep === 'undefined') {
				//keep = this.keepScrollPosition;
			}

			// Store scroll position to be restored after new total is received
			if (keep) {
				this._previousScrollPosition = this.getScrollPosition();
			}

			this.inherited(arguments);
			if (this._renderedCollection) {
				// render the query

				// renderQuery calls _trackError internally
				return this.renderQuery(function (queryOptions) {
					return self._renderedCollection.fetchRange({
						start: queryOptions.start,
						end: queryOptions.start + queryOptions.count
					});
				}).then(function () {
					// Emit on a separate turn to enable event to be used consistently for
					// initial render, regardless of whether the backing store is async
					self._refreshTimeout = setTimeout(function () {
						on.emit(self.domNode, 'dgrid-refresh-complete', {
							bubbles: true,
							cancelable: false,
							grid: self
						});
						self._refreshTimeout = null;
					}, 0);
				});
			}
		},

		resize: function () {
			this.inherited(arguments);
			this._processScroll();
		},

		cleanup: function () {
			this.inherited(arguments);
			this.preload = null;
		},

		renderQueryResults: function (results) {
			var rows = this.inherited(arguments);
			var collection = this._renderedCollection;

			if (collection && collection.releaseRange) {
				rows.then(function (resolvedRows) {
					if (resolvedRows[0] && !resolvedRows[0].parentNode.tagName) {
						// Release this range, since it was never actually rendered;
						// need to wait until totalLength promise resolves, since
						// Trackable only adds the range then to begin with
						results.totalLength.then(function () {
							collection.releaseRange(resolvedRows[0].rowIndex,
								resolvedRows[resolvedRows.length - 1].rowIndex + 1);
						});
					}
				});
			}

			return rows;
		},

		_getFirstRowSibling: function (container) {
			// summary:
			//		Returns the DOM node that a new row should be inserted before
			//		when there are no other rows in the current result set.
			//		In the case of OnDemandList, this will always be the last child
			//		of the container (which will be a trailing preload node).
			return container.lastChild;
		},

		_calcRowHeight: function (rowElement) {
			// summary:
			//		Calculate the height of a row. This is a method so it can be overriden for
			//		plugins that add connected elements to a row, like the tree

			var sibling = rowElement.nextSibling;

			// If a next row exists, compare the top of this row with the
			// next one (in case "rows" are actually rendering side-by-side).
			// If no next row exists, this is either the last or only row,
			// in which case we count its own height.
			if (sibling && !/\bdgrid-preload\b/.test(sibling.className)) {
				return sibling.offsetTop - rowElement.offsetTop;
			}

			return rowElement.offsetHeight;
		},

		lastScrollTop: 0,
		_processScroll: function (evt) {
			// summary:
			//		Checks to make sure that everything in the viewable area has been
			//		downloaded, and triggering a request for the necessary data when needed.

			if (!this.rowHeight) {
				return;
			}

			var grid = this,
				scrollNode = grid.bodyNode,
				// grab current visible top from event if provided, otherwise from node
				visibleTop = (evt && evt.scrollTop) || this.getScrollPosition().y,
				visibleBottom = scrollNode.offsetHeight + visibleTop,
				priorPreload, preloadNode, preload = grid.preload,
				lastScrollTop = grid.lastScrollTop,
				requestBuffer = grid.bufferRows * grid.rowHeight,
				searchBuffer = requestBuffer - grid.rowHeight, // Avoid rounding causing multiple queries
				// References related to emitting dgrid-refresh-complete if applicable
				lastRows,
				preloadSearchNext = true;

			// XXX: I do not know why this happens.
			// munging the actual location of the viewport relative to the preload node by a few pixels in either
			// direction is necessary because at least WebKit on Windows seems to have an error that causes it to
			// not quite get the entire element being focused in the viewport during keyboard navigation,
			// which means it becomes impossible to load more data using keyboard navigation because there is
			// no more data to scroll to to trigger the fetch.
			// 1 is arbitrary and just gets it to work correctly with our current test cases; don’t wanna go
			// crazy and set it to a big number without understanding more about what is going on.
			// wondering if it has to do with border-box or something, but changing the border widths does not
			// seem to make it break more or less, so I do not know…
			var mungeAmount = 1;

			grid.lastScrollTop = visibleTop;

			function removeDistantNodes(preload, distanceOff, traversal, below) {
				// we check to see the the nodes are "far off"
				var farOffRemoval = grid.farOffRemoval,
					preloadNode = preload.node;
				// by checking to see if it is the farOffRemoval distance away
				if (distanceOff > 2 * farOffRemoval) {
					// there is a preloadNode that is far off;
					// remove rows until we get to in the current viewport
					var row;
					var nextRow = preloadNode[traversal];
					var reclaimedHeight = 0;
					var count = 0;
					var toDelete = [];
					var firstRowIndex = nextRow && nextRow.rowIndex;
					var lastRowIndex;

					while ((row = nextRow)) {
						var rowHeight = grid._calcRowHeight(row);
						if (reclaimedHeight + rowHeight + farOffRemoval > distanceOff ||
								(nextRow.className.indexOf('dgrid-row') < 0 &&
									nextRow.className.indexOf('dgrid-loading') < 0)) {
							// we have reclaimed enough rows or we have gone beyond grid rows
							break;
						}

						nextRow = row[traversal];
						reclaimedHeight += rowHeight;
						count += row.count || 1;
						// Just do cleanup here, as we will do a more efficient node destruction in a setTimeout below
						grid.removeRow(row, true);
						toDelete.push(row);

						if ('rowIndex' in row) {
							lastRowIndex = row.rowIndex;
						}
					}

					if (grid._renderedCollection.releaseRange &&
							typeof firstRowIndex === 'number' && typeof lastRowIndex === 'number') {
						// Note that currently child rows in Tree structures are never unrendered;
						// this logic will need to be revisited when that is addressed.

						// releaseRange is end-exclusive, and won't remove anything if start >= end.
						if (below) {
							grid._renderedCollection.releaseRange(lastRowIndex, firstRowIndex + 1);
						}
						else {
							grid._renderedCollection.releaseRange(firstRowIndex, lastRowIndex + 1);
						}

						grid._rows[below ? 'max' : 'min'] = lastRowIndex;
						if (grid._rows.max >= grid._total - 1) {
							grid._rows.max = Infinity;
						}
					}
					// now adjust the preloadNode based on the reclaimed space
					preload.count += count;
					if (below) {
						preloadNode.rowIndex -= count;
						adjustHeight(preload);
					}
					else {
						// if it is above, we can calculate the change in exact row changes,
						// which we must do to not mess with the scroll position
						preloadNode.style.height = (preloadNode.offsetHeight + reclaimedHeight) + 'px';
					}
					// we remove the elements after expanding the preload node so that
					// the contraction doesn't alter the scroll position
					var trashBin = document.createElement('div');
					for (var i = toDelete.length; i--;) {
						trashBin.appendChild(toDelete[i]);
					}
					setTimeout(function () {
						// we can defer the destruction until later
						domConstruct.destroy(trashBin);
					}, 1);
				}
			}

			function adjustHeight(preload, noMax) {
				preload.node.style.height = Math.min(preload.count * grid.rowHeight,
					noMax ? Infinity : grid.maxEmptySpace) + 'px';
			}
			function traversePreload(preload, moveNext) {
				// Skip past preloads that are not currently connected
				do {
					preload = moveNext ? preload.next : preload.previous;
				} while (preload && !preload.node.offsetWidth);
				return preload;
			}
			while (preload && !preload.node.offsetWidth) {
				// skip past preloads that are not currently connected
				preload = preload.previous;
			}
			// there can be multiple preloadNodes (if they split, or multiple queries are created),
			//	so we can traverse them until we find whatever is in the current viewport, making
			//	sure we don't backtrack
			while (preload && preload !== priorPreload) {
				priorPreload = grid.preload;
				grid.preload = preload;
				preloadNode = preload.node;
				var preloadTop = preloadNode.offsetTop;
				var preloadHeight;

				if (visibleBottom + mungeAmount + searchBuffer < preloadTop) {
					// the preload is below the line of sight
					preload = traversePreload(preload, (preloadSearchNext = false));
				}
				else if (visibleTop - mungeAmount - searchBuffer >
						(preloadTop + (preloadHeight = preloadNode.offsetHeight))) {
					// the preload is above the line of sight
					preload = traversePreload(preload, (preloadSearchNext = true));
				}
				else {
					// the preload node is visible, or close to visible, better show it
					var offset = ((preloadNode.rowIndex ? visibleTop - requestBuffer :
						visibleBottom) - preloadTop) / grid.rowHeight;
					var count = (visibleBottom - visibleTop + 2 * requestBuffer) / grid.rowHeight;
					// utilize momentum for predictions
					var momentum = Math.max(
						Math.min((visibleTop - lastScrollTop) * grid.rowHeight, grid.maxRowsPerPage / 2),
						grid.maxRowsPerPage / -2);
					count += Math.min(Math.abs(momentum), 10);
					if (preloadNode.rowIndex === 0) {
						// at the top, adjust from bottom to top
						offset -= count;
					}
					offset = Math.max(offset, 0);
					if (offset < 10 && offset > 0 && count + offset < grid.maxRowsPerPage) {
						// connect to the top of the preloadNode if possible to avoid excessive adjustments
						count += Math.max(0, offset);
						offset = 0;
					}
					count = Math.min(Math.max(count, grid.minRowsPerPage),
										grid.maxRowsPerPage, preload.count);

					if (count === 0) {
						preload = traversePreload(preload, preloadSearchNext);
						continue;
					}

					count = Math.ceil(count);
					offset = Math.min(Math.floor(offset), preload.count - count);

					var options = {};
					preload.count -= count;
					var beforeNode = preloadNode,
						keepScrollTo, queryRowsOverlap = grid.queryRowsOverlap,
						below = (preloadNode.rowIndex > 0 || preloadNode.offsetTop > visibleTop) && preload;
					if (below) {
						// add new rows below
						var previous = preload.previous;
						if (previous) {
							removeDistantNodes(previous,
								visibleTop - (previous.node.offsetTop + previous.node.offsetHeight),
								'nextSibling');
							if (offset > 0 && previous.node === preloadNode.previousSibling) {
								// all of the nodes above were removed
								offset = Math.min(preload.count, offset);
								preload.previous.count += offset;
								adjustHeight(preload.previous, true);
								preloadNode.rowIndex += offset;
								queryRowsOverlap = 0;
							}
							else {
								count += offset;
							}
							preload.count -= offset;
						}
						options.start = preloadNode.rowIndex - queryRowsOverlap;
						options.count = Math.min(count + queryRowsOverlap, grid.maxRowsPerPage);
						preloadNode.rowIndex = options.start + options.count;
					}
					else {
						// add new rows above
						if (preload.next) {
							// remove out of sight nodes first
							removeDistantNodes(preload.next, preload.next.node.offsetTop - visibleBottom,
								'previousSibling', true);
							beforeNode = preloadNode.nextSibling;
							if (beforeNode === preload.next.node) {
								// all of the nodes were removed, can position wherever we want
								preload.next.count += preload.count - offset;
								preload.next.node.rowIndex = offset + count;
								adjustHeight(preload.next);
								preload.count = offset;
								queryRowsOverlap = 0;
							}
							else {
								keepScrollTo = true;
							}

						}
						options.start = preload.count;
						options.count = Math.min(count + queryRowsOverlap, grid.maxRowsPerPage);
					}
					if (keepScrollTo && beforeNode && beforeNode.offsetWidth) {
						keepScrollTo = beforeNode.offsetTop;
					}

					adjustHeight(preload);

					// use the query associated with the preload node to get the next "page"
					if ('level' in preload.query) {
						options.queryLevel = preload.query.level;
					}

					// Avoid spurious queries (ideally this should be unnecessary...)
					if (!('queryLevel' in options) && (options.start > grid._total || options.count < 0)) {
						continue;
					}

					// create a loading node as a placeholder while the data is loaded
					var loadingNode = domConstruct.create('div', {
						className: 'dgrid-loading',
						style: { height: count * grid.rowHeight + 'px' }
					}, beforeNode, 'before');
					domConstruct.create('div', {
						className: 'dgrid-' + (below ? 'below' : 'above'),
						innerHTML: grid.loadingMessage
					}, loadingNode);
					loadingNode.count = count;

					// Query now to fill in these rows.
					grid._trackError(function () {
						// Use function to isolate the variables in case we make multiple requests
						// (which can happen if we need to render on both sides of an island of already-rendered rows)
						(function (loadingNode, below, keepScrollTo) {
							/* jshint maxlen: 122 */
							var rangeResults = preload.query(options);
							lastRows = grid.renderQueryResults(rangeResults, loadingNode, options).then(function (rows) {
								var gridRows = grid._rows;
								if (gridRows && !('queryLevel' in options) && rows.length) {
									// Update relevant observed range for top-level items
									if (below) {
										if (gridRows.max <= gridRows.min) {
											// All rows were removed; update start of rendered range as well
											gridRows.min = rows[0].rowIndex;
										}
										gridRows.max = rows[rows.length - 1].rowIndex;
									}
									else {
										if (gridRows.max <= gridRows.min) {
											// All rows were removed; update end of rendered range as well
											gridRows.max = rows[rows.length - 1].rowIndex;
										}
										gridRows.min = rows[0].rowIndex;
									}
								}

								// can remove the loading node now
								beforeNode = loadingNode.nextSibling;
								domConstruct.destroy(loadingNode);
								// beforeNode may have been removed if the query results loading node was removed
								// as a distant node before rendering
								if (keepScrollTo && beforeNode && beforeNode.offsetWidth) {
									// if the preload area above the nodes is approximated based on average
									// row height, we may need to adjust the scroll once they are filled in
									// so we don't "jump" in the scrolling position
									var pos = grid.getScrollPosition();
									grid.scrollTo({
										// Since we already had to query the scroll position,
										// include x to avoid TouchScroll querying it again on its end.
										x: pos.x,
										y: pos.y + beforeNode.offsetTop - keepScrollTo,
										// Don't kill momentum mid-scroll (for TouchScroll only).
										preserveMomentum: true
									});
								}

								rangeResults.totalLength.then(function (total) {
									if (!('queryLevel' in options)) {
										grid._total = total;
										if (grid._rows && grid._rows.max >= grid._total - 1) {
											grid._rows.max = Infinity;
										}
									}
									if (below) {
										// if it is below, we will use the total from the collection to update
										// the count of the last preload in case the total changes as
										// later pages are retrieved

										// recalculate the count
										below.count = total - below.node.rowIndex;
										// readjust the height
										adjustHeight(below);
									}
								});

								// make sure we have covered the visible area
								grid._processScroll();
								return rows;
							}, function (e) {
								domConstruct.destroy(loadingNode);
								throw e;
							});
						})(loadingNode, below, keepScrollTo);
					});

					preload = preload.previous;

				}
			}

			// return the promise from the last render
			return lastRows;
		}
	});

});

},
'dgrid/_StoreMixin':function(){
define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/Deferred',
	'dojo/aspect',
	'dojo/dom-construct',
	'dojo/has',
	'dojo/on',
	'dojo/when'
], function (declare, lang, Deferred, aspect, domConstruct, has, on, when) {
	// This module isolates the base logic required by store-aware list/grid
	// components, e.g. OnDemandList/Grid and the Pagination extension.

	function emitError(err) {
		// called by _trackError in context of list/grid, if an error is encountered
		if (typeof err !== 'object') {
			// Ensure we actually have an error object, so we can attach a reference.
			err = new Error(err);
		}
		else if (err.dojoType === 'cancel') {
			// Don't fire dgrid-error events for errors due to canceled requests
			// (unfortunately, the Deferred instrumentation will still log them)
			return;
		}

		var event = on.emit(this.domNode, 'dgrid-error', {
			grid: this,
			error: err,
			cancelable: true,
			bubbles: true
		});
		if (event) {
			console.error(err);
		}
	}

	return declare(null, {
		// collection: Object
		//		The base object collection (implementing the dstore/api/Store API) before being sorted
		//		or otherwise processed by the grid. Use it for general purpose store operations such as
		//		`getIdentity` and `get`, `add`, `put`, and `remove`.
		collection: null,

		// _renderedCollection: Object
		//		The object collection from which data is to be fetched. This is the sorted collection.
		//		Use it when retrieving data to be rendered by the grid.
		_renderedCollection: null,

		// _rows: Array
		//		Sparse array of row nodes, used to maintain the grid in response to events from a tracked collection.
		//		Each node's index corresponds to the index of its data object in the collection.
		_rows: null,

		// _observerHandle: Object
		//		The observer handle for the current collection, if trackable.
		_observerHandle: null,

		// shouldTrackCollection: Boolean
		//		Whether this instance should track any trackable collection it is passed.
		shouldTrackCollection: false,

		// getBeforePut: boolean
		//		If true, a get request will be performed to the store before each put
		//		as a baseline when saving; otherwise, existing row data will be used.
		getBeforePut: true,

		// noDataMessage: String
		//		Message to be displayed when no results exist for a collection, whether at
		//		the time of the initial query or upon subsequent observed changes.
		//		Defined by _StoreMixin, but to be implemented by subclasses.
		noDataMessage: '',

		// loadingMessage: String
		//		Message displayed when data is loading.
		//		Defined by _StoreMixin, but to be implemented by subclasses.
		loadingMessage: '',

		_total: 0,

		constructor: function () {
			// Create empty objects on each instance, not the prototype
			this.dirty = {};
			this._updating = {}; // Tracks rows that are mid-update
			this._columnsWithSet = {};

			// Reset _columnsWithSet whenever column configuration is reset
			aspect.before(this, 'configStructure', lang.hitch(this, function () {
				this._columnsWithSet = {};
			}));
		},

		destroy: function () {
			this.inherited(arguments);

			if (this._renderedCollection) {
				this._cleanupCollection();
			}
		},

		_configColumn: function (column) {
			// summary:
			//		Implements extension point provided by Grid to store references to
			//		any columns with `set` methods, for use during `save`.
			if (column.set) {
				this._columnsWithSet[column.field] = column;
			}
			this.inherited(arguments);
		},

		_setCollection: function (collection) {
			// summary:
			//		Assigns a new collection to the list/grid, sets up tracking
			//		if applicable, and tells the list/grid to refresh.

			if (this._renderedCollection) {
				this.cleanup();
				this._cleanupCollection({
					// Only clear the dirty hash if the collection being used is actually from a different store
					// (i.e. not just a re-sorted / re-filtered version of the same store)
					shouldRevert: !collection || collection.storage !== this._renderedCollection.storage
				});
			}

			this.collection = collection;

			// Avoid unnecessary rendering and processing before the grid has started up
			if (this._started) {
				// Once startup is called, List.startup sets the sort property which calls _StoreMixin._applySort
				// which sets the collection property again.  So _StoreMixin._applySort will be executed again
				// after startup is called.
				if (collection) {
					var renderedCollection = collection;
					if (this.sort && this.sort.length > 0) {
						renderedCollection = collection.sort(this.sort);
					}

					if (renderedCollection.track && this.shouldTrackCollection) {
						renderedCollection = renderedCollection.track();
						this._rows = [];

						this._observerHandle = this._observeCollection(
							renderedCollection,
							this.contentNode,
							{ rows: this._rows }
						);
					}

					this._renderedCollection = renderedCollection;
				}
				this.refresh();
			}
		},

		_setStore: function () {
			if (!this.collection) {
				console.debug('set(\'store\') call detected, but you probably meant set(\'collection\')');
			}
		},

		_getTotal: function () {
			// summary:
			//		Retrieves the currently-tracked total (as updated by
			//		subclasses after store queries, or by _StoreMixin in response to
			//		updated totalLength in events)

			return this._total;
		},

		_cleanupCollection: function (options) {
			// summary:
			//		Handles cleanup duty for the previous collection;
			//		called during _setCollection and destroy.
			// options: Object?
			//		* shouldRevert: Whether to clear the dirty hash

			options = options || {};

			if (this._renderedCollection.tracking) {
				this._renderedCollection.tracking.remove();
			}

			// Remove observer and existing rows so any sub-row observers will be cleaned up
			if (this._observerHandle) {
				this._observerHandle.remove();
				this._observerHandle = this._rows = null;
			}

			// Discard dirty map, as it applied to a previous collection
			if (options.shouldRevert !== false) {
				this.dirty = {};
			}

			this._renderedCollection = this.collection = null;
		},

		_applySort: function () {
			if (this.collection) {
				this.set('collection', this.collection);
			}
		},

		row: function () {
			// Extend List#row with more appropriate lookup-by-id logic
			var row = this.inherited(arguments);
			if (row && row.data && typeof row.id !== 'undefined') {

				if(this.collection) {
					row.id = this.collection.getIdentity(row.data);
				}else{
					console.error('_StoreMixin:have no collection!');
				}

			}
			return row;
		},

		refresh: function () {
			var result = this.inherited(arguments);

			if (!this.collection) {
				
				this.noDataNode = domConstruct.create('div', {
					className: 'dgrid-no-data',
					innerHTML: this.noDataMessage
				}, this.contentNode);
				
				this._emit('noData');
			}
			//{"values":[{"key":"Marantz-Power","value":"%%PowerState%%"}]}
			return result;
		},

		refreshCell: function (cell) {
			/*
			 this.inherited(arguments);
			 var row = cell.row;
			 var self = this;
			 */
			if (!this.collection || !this._createBodyRowCell) {
				//throw new Error('refreshCell requires a Grid with a collection.');
				return false;
			}

			if(!cell.column){
				return;
			}
			if (cell.column && cell.column.selector) {
				return (new Deferred()).resolve();
			}
			this.inherited(arguments);
			return this.collection.get(cell.row.id).then(lang.hitch(this, '_refreshCellFromItem', cell));

/*
			return this.collection.get(row.id).then(function (item) {

				var cellElement = cell.element;

				if(cellElement) {

					if (cellElement.widget) {
						cellElement.widget.destroyRecursive();
					}
					domConstruct.empty(cellElement);

					var dirtyItem = self.dirty && self.dirty[row.id];
					if (dirtyItem) {
						item = lang.delegate(item, dirtyItem);
					}

					self._createBodyRowCell(cellElement, cell.column, item);
				}
			});
			*/
		},
		_refreshCellFromItem: function (cell, item, options) {
			if(!cell || !cell.element){
				return;
			}
			var cellElement = cell.element;
			if (cellElement.widget) {
				cellElement.widget.destroyRecursive();
			}
			domConstruct.empty(cellElement);

			var dirtyItem = this.dirty && this.dirty[cell.row.id];
			if (dirtyItem) {
				item = lang.delegate(item, dirtyItem);
			}

			this._createBodyRowCell(cellElement, cell.column, item, options);
		},
		renderArray: function () {
			var rows = this.inherited(arguments);

			if (!this.collection) {
				if (rows.length && this.noDataNode) {
					domConstruct.destroy(this.noDataNode);
				}
			}
			return rows;
		},

		insertRow: function (object, parent, beforeNode, i, options) {
			var store = this.collection,
				dirty = this.dirty,
				id = store && store.getIdentity(object),
				dirtyObj,
				row;

			if (id in dirty && !(id in this._updating)) {
				dirtyObj = dirty[id];
			}
			if (dirtyObj) {
				// restore dirty object as delegate on top of original object,
				// to provide protection for subsequent changes as well
				object = lang.delegate(object, dirtyObj);
			}

			row = this.inherited(arguments);

			if (options && options.rows) {
				options.rows[i] = row;
			}

			// Remove no data message when a new row appears.
			// Run after inherited logic to prevent confusion due to noDataNode
			// no longer being present as a sibling.
			if (this.noDataNode) {
				domConstruct.destroy(this.noDataNode);
				this.noDataNode = null;
			}

			return row;
		},

		updateDirty: function (id, field, value) {
			// summary:
			//		Updates dirty data of a field for the item with the specified ID.
			var dirty = this.dirty,
				dirtyObj = dirty[id];

			if (!dirtyObj) {
				dirtyObj = dirty[id] = {};
			}
			dirtyObj[field] = value;
		},

		save: function () {
			// Keep track of the store and puts
			var self = this,
				store = this.collection,
				dirty = this.dirty,
				dfd = new Deferred(),
				results = {},
				getFunc = function (id) {
					// returns a function to pass as a step in the promise chain,
					// with the id variable closured
					var data;
					return (self.getBeforePut || !(data = self.row(id).data)) ?
						function () {
							return store.get(id);
						} :
						function () {
							return data;
						};
				};

			// function called within loop to generate a function for putting an item
			function putter(id, dirtyObj) {
				// Return a function handler
				return function (object) {
					var colsWithSet = self._columnsWithSet,
						updating = self._updating,
						key, data;

					if (typeof object.set === 'function') {
						object.set(dirtyObj);
					} else {
						// Copy dirty props to the original, applying setters if applicable
						for (key in dirtyObj) {
							object[key] = dirtyObj[key];
						}
					}

					// Apply any set methods in column definitions.
					// Note that while in the most common cases column.set is intended
					// to return transformed data for the key in question, it is also
					// possible to directly modify the object to be saved.
					for (key in colsWithSet) {
						data = colsWithSet[key].set(object);
						if (data !== undefined) {
							object[key] = data;
						}
					}

					updating[id] = true;
					// Put it in the store, returning the result/promise
					return store.put(object).then(function (result) {
						// Clear the item now that it's been confirmed updated
						delete dirty[id];
						delete updating[id];
						results[id] = result;
						return results;
					});
				};
			}

			var promise = dfd.then(function () {
				// Ensure empty object is returned even if nothing was dirty, for consistency
				return results;
			});

			// For every dirty item, grab the ID
			for (var id in dirty) {
				// Create put function to handle the saving of the the item
				var put = putter(id, dirty[id]);

				// Add this item onto the promise chain,
				// getting the item from the store first if desired.
				promise = promise.then(getFunc(id)).then(put);
			}

			// Kick off and return the promise representing all applicable get/put ops.
			// If the success callback is fired, all operations succeeded; otherwise,
			// save will stop at the first error it encounters.
			dfd.resolve();
			return promise;
		},

		revert: function () {
			// summary:
			//		Reverts any changes since the previous save.
			this.dirty = {};
			this.refresh();
		},

		_trackError: function (func) {
			// summary:
			//		Utility function to handle emitting of error events.
			// func: Function|String
			//		A function which performs some store operation, or a String identifying
			//		a function to be invoked (sans arguments) hitched against the instance.
			//		If sync, it can return a value, but may throw an error on failure.
			//		If async, it should return a promise, which would fire the error
			//		callback on failure.
			// tags:
			//		protected

			if (typeof func === 'string') {
				func = lang.hitch(this, func);
			}

			var self = this,
				promise;

			try {
				promise = when(func());
			} catch (err) {
				// report sync error
				var dfd = new Deferred();
				dfd.reject(err);
				promise = dfd.promise;
			}

			promise.otherwise(function (err) {
				emitError.call(self, err);
			});
			return promise;
		},

		removeRow: function (rowElement, preserveDom, options) {
			var row = {element: rowElement};
			// Check to see if we are now empty...
			if (!preserveDom && this.noDataMessage &&
					(this.up(row).element === rowElement) &&
					(this.down(row).element === rowElement)) {
				// ...we are empty, so show the no data message.
				this.noDataNode = domConstruct.create('div', {
					className: 'dgrid-no-data',
					innerHTML: this.noDataMessage
				}, this.contentNode);
				this._emit('noData');
			}

			var rows = (options && options.rows) || this._rows;
			if (rows) {
				delete rows[rowElement.rowIndex];
			}

			return this.inherited(arguments);
		},

		renderQueryResults: function (results, beforeNode, options) {
			// summary:
			//		Renders objects from QueryResults as rows, before the given node.

			options = lang.mixin({ rows: this._rows }, options);
			var self = this;

			if (! 1 ) {
				// Check for null/undefined totalResults to help diagnose faulty services/stores
				results.totalLength.then(function (total) {
					if (total == null) {
						console.warn('Store reported null or undefined totalLength. ' +
							'Make sure your store (and service, if applicable) are reporting total correctly!');
					}
				});
			}

			return results.then(function (resolvedResults) {
				var resolvedRows = self.renderArray(resolvedResults, beforeNode, options);
				delete self._lastCollection; // used only for non-store List/Grid
				return resolvedRows;
			});
		},

		_observeCollection: function (collection, container, options) {
			var self = this,
				rows = options.rows,
				row;

			var handles = [
				collection.on('delete, update', function (event) {
					var from = event.previousIndex;
					var to = event.index;

					if (from !== undefined && rows[from]) {
						if ('max' in rows && (to === undefined || to < rows.min || to > rows.max)) {
							rows.max--;
						}

						row = rows[from];

						// check to make the sure the node is still there before we try to remove it
						// (in case it was moved to a different place in the DOM)
						if (row.parentNode === container) {
							self.removeRow(row, false, options);
						}

						// remove the old slot
						rows.splice(from, 1);

						if (event.type === 'delete' ||
								(event.type === 'update' && (from < to || to === undefined))) {
							// adjust the rowIndex so adjustRowIndices has the right starting point
							rows[from] && rows[from].rowIndex--;
						}
					}
					if (event.type === 'delete') {
						// Reset row in case this is later followed by an add;
						// only update events should retain the row variable below
						row = null;
					}
				}),

				collection.on('add, update', function (event) {
					var from = event.previousIndex;
					var to = event.index;
					var nextNode;

					function advanceNext() {
						nextNode = (nextNode.connected || nextNode).nextSibling;
					}

					// When possible, restrict observations to the actually rendered range
					if (to !== undefined && (!('max' in rows) || (to >= rows.min && to <= rows.max))) {
						if ('max' in rows && (from === undefined || from < rows.min || from > rows.max)) {
							rows.max++;
						}
						// Add to new slot (either before an existing row, or at the end)
						// First determine the DOM node that this should be placed before.
						if (rows.length) {
							nextNode = rows[to];
							if (!nextNode) {
								nextNode = rows[to - 1];
								if (nextNode) {
									// Make sure to skip connected nodes, so we don't accidentally
									// insert a row in between a parent and its children.
									advanceNext();
								}
							}
						}
						else {
							// There are no rows.  Allow for subclasses to insert new rows somewhere other than
							// at the end of the parent node.
							nextNode = self._getFirstRowSibling && self._getFirstRowSibling(container);
						}
						// Make sure we don't trip over a stale reference to a
						// node that was removed, or try to place a node before
						// itself (due to overlapped queries)
						if (row && nextNode && row.id === nextNode.id) {
							advanceNext();
						}
						if (nextNode && !nextNode.parentNode) {
							nextNode = document.getElementById(nextNode.id);
						}
						rows.splice(to, 0, undefined);
						row = self.insertRow(event.target, container, nextNode, to, options);
						self.highlightRow(row);
					}
					// Reset row so it doesn't get reused on the next event
					row = null;
				}),

				collection.on('add, delete, update', function (event) {
					var from = (typeof event.previousIndex !== 'undefined') ? event.previousIndex : Infinity,
						to = (typeof event.index !== 'undefined') ? event.index : Infinity,
						adjustAtIndex = Math.min(from, to);
					from !== to && rows[adjustAtIndex] && self.adjustRowIndices(rows[adjustAtIndex]);

					// the removal of rows could cause us to need to page in more items
					if (from !== Infinity && self._processScroll && (rows[from] || rows[from - 1])) {
						self._processScroll();
					}

					// Fire _onNotification, even for out-of-viewport notifications,
					// since some things may still need to update (e.g. Pagination's status/navigation)
					self._onNotification(rows, event, collection);

					// Update _total after _onNotification so that it can potentially
					// decide whether to perform actions based on whether the total changed
					if (collection === self._renderedCollection && 'totalLength' in event) {
						self._total = event.totalLength;
					}
				})
			];

			return {
				remove: function () {
					while (handles.length > 0) {
						handles.pop().remove();
					}
				}
			};
		},

		_onNotification: function () {
			// summary:
			//		Protected method called whenever a store notification is observed.
			//		Intended to be extended as necessary by mixins/extensions.
			// rows: Array
			//		A sparse array of row nodes corresponding to data objects in the collection.
			// event: Object
			//		The notification event
			// collection: Object
			//		The collection that the notification is relevant to.
			//		Useful for distinguishing child-level from top-level notifications.
		}
	});
});

},
'dgrid/OnDemandGrid':function(){
define([
	'dojo/_base/declare',
	'./Grid',
	'./OnDemandList'
], function (declare, Grid, OnDemandList) {
	return declare([ Grid, OnDemandList ], {});
});
},
'dgrid/extensions/ColumnReorder':function(){
define([
	'dojo/_base/lang',
	'dojo/_base/declare',
	'dojo/_base/array',
	'dojo/dom-class',
	'dojo/on',
	'dojo/query',
	'dojo/dnd/Source'
], function (lang, declare, arrayUtil, domClass, on, DndSource) {
	var dndTypeRx = /(\d+)(?:-(\d+))?$/; // used to determine subrow from dndType

	// The following 2 functions are used by onDropInternal logic for
	// retrieving/modifying a given subRow.  The `match` variable in each is
	// expected to be the result of executing dndTypeRx on a subRow ID.

	function getMatchingSubRow(grid, match) {
		var hasColumnSets = match[2],
			rowOrSet = grid[hasColumnSets ? 'columnSets' : 'subRows'][match[1]];

		return hasColumnSets ? rowOrSet[match[2]] : rowOrSet;
	}

	function setMatchingSubRow(grid, match, subRow) {
		if (match[2]) {
			grid.columnSets[match[1]][match[2]] = subRow;
		}
		else {
			grid.subRows[match[1]] = subRow;
		}
	}

	// Builds a prefix for a dndtype value based on a grid id.
	function makeDndTypePrefix(gridId) {
		return 'dgrid-' + gridId + '-';
	}

	// Removes the grid id prefix from a dndtype value.  This allows the grid id to contain
	// a dash-number suffix.  This works only if a column is dropped on the grid from which it
	// originated.  Otherwise, a dash-number suffix will cause the regex to match on the wrong values.
	function stripIdPrefix(gridId, dndtype) {
		return dndtype.slice(makeDndTypePrefix(gridId).length);
	}

	var ColumnDndSource = declare(DndSource, {
		// summary:
		//		Custom dojo/dnd source extension configured specifically for
		//		dgrid column reordering.

		copyState: function () {
			return false; // never copy
		},

		checkAcceptance: function (source) {
			return source === this; // self-accept only
		},

		_legalMouseDown: function (evt) {
			// Overridden to prevent blocking ColumnResizer resize handles.
			return evt.target.className.indexOf('dgrid-resize-handle') > -1 ? false :
				this.inherited(arguments);
		},

		onDropInternal: function (nodes) {
			var grid = this.grid,
				match = dndTypeRx.exec(stripIdPrefix(grid.id, nodes[0].getAttribute('dndType'))),
				structureProperty = match[2] ? 'columnSets' : 'subRows',
				oldSubRow = getMatchingSubRow(grid, match),
				columns = grid.columns;

			// First, allow original DnD logic to place node in new location.
			this.inherited(arguments);

			if (!match) {
				return;
			}

			// Then, iterate through the header cells in their new order,
			// to populate a new row array to assign as a new sub-row to the grid.
			// (Wait until the next turn to avoid errors in Opera.)
			setTimeout(function () {
				var newSubRow = arrayUtil.map(nodes[0].parentNode.childNodes, function (col) {
						return columns[col.columnId];
					}),
					eventObject;

				setMatchingSubRow(grid, match, newSubRow);

				eventObject = {
					grid: grid,
					subRow: newSubRow,
					column: columns[nodes[0].columnId],
					bubbles: true,
					cancelable: true,
					// Set parentType to indicate this is the result of user interaction.
					parentType: 'dnd'
				};
				// Set columnSets or subRows depending on which the grid is using.
				eventObject[structureProperty] = grid[structureProperty];

				// Emit a custom event which passes the new structure.
				// Allow calling preventDefault() to cancel the reorder operation.
				if (on.emit(grid.domNode, 'dgrid-columnreorder', eventObject)) {
					// Event was not canceled - force processing of modified structure.
					grid.set(structureProperty, grid[structureProperty]);
				}
				else {
					// Event was canceled - revert the structure and re-render the header
					// (since the inherited logic invoked above will have shifted cells).
					setMatchingSubRow(grid, match, oldSubRow);
					grid.renderHeader();
					// After re-rendering the header, re-apply the sort arrow if needed.
					if (grid.sort.length) {
						grid.updateSortArrow(grid.sort);
					}
				}
			}, 0);
		}
	});

	var ColumnReorder = declare('dgrid.ColumnReorder',null,{
		// summary:
		//		Extension allowing reordering of columns in a grid via drag'n'drop.
		//		Reordering of columns within the same subrow or columnset is also
		//		supported; between different ones is not.

		// columnDndConstructor: Function
		//		Constructor to call for instantiating DnD sources within the grid's
		//		header.
		columnDndConstructor: ColumnDndSource,

		_initSubRowDnd: function (subRow, dndType) {
			// summary:
			//		Initializes a dojo/dnd source for one subrow of a grid;
			//		this could be its only subrow, one of several, or a subrow within a
			//		columnset.

			var dndParent, c, len, col, th;

			for (c = 0, len = subRow.length; c < len; c++) {
				col = subRow[c];
				if (col.reorderable === false) {
					continue;
				}

				th = col.headerNode;
				// Add dojoDndItem class, and a dndType unique to this subrow.
				domClass.add(th, 'dojoDndItem');
				th.setAttribute('dndType', dndType);

				if (!dndParent) {
					dndParent = th.parentNode;
				}
			}

			if (dndParent) {
				this._columnDndSources.push(new this.columnDndConstructor(dndParent, {
					horizontal: true,
					grid: this
				}));
			}
			// (If dndParent wasn't set, no columns are draggable)
		},

		renderHeader: function () {
			var dndTypePrefix = makeDndTypePrefix(this.id),
				csLength, cs;

			this.inherited(arguments);

			// After header is rendered, set up a dnd source on each of its subrows.

			this._columnDndSources = [];

			if (this.columnSets) {
				// Iterate columnsets->subrows->columns.
				for (cs = 0, csLength = this.columnSets.length; cs < csLength; cs++) {
					arrayUtil.forEach(this.columnSets[cs], function (subRow, sr) {
						this._initSubRowDnd(subRow, dndTypePrefix + cs + '-' + sr);
					}, this);
				}
			}
			else {
				// Iterate subrows->columns.
				arrayUtil.forEach(this.subRows, function (subRow, sr) {
					this._initSubRowDnd(subRow, dndTypePrefix + sr);
				}, this);
			}
		},

		_destroyColumns: function () {
			if (this._columnDndSources) {
				// Destroy old dnd sources.
				arrayUtil.forEach(this._columnDndSources, function (source) {
					source && source.destroy && source.destroy();
				});
			}
			this.inherited(arguments);
		}
	});

	ColumnReorder.ColumnDndSource = ColumnDndSource;
	return ColumnReorder;
});

},
'dgrid/extensions/ColumnResizer':function(){
define([
	'dojo/_base/declare',
	'dojo/on',
	'dojo/query',
	'dojo/_base/lang',
	'dojo/dom',
	'dojo/dom-construct',
	'dojo/dom-geometry',
	'dojo/has',
	'../util/misc',
	'dojo/_base/html'
], function (declare, listen, query, lang, dom, domConstruct, geom, has, miscUtil) {

	function addRowSpan(table, span, startRow, column, id) {
		// loop through the rows of the table and add this column's id to
		// the rows' column
		for (var i = 1; i < span; i++) {
			table[startRow + i][column] = id;
		}
	}
	function subRowAssoc(subRows) {
		// Take a sub-row structure and output an object with key=>value pairs
		// The keys will be the column id's; the values will be the first-row column
		// that column's resizer should be associated with.

		var i = subRows.length,
			l = i,
			numCols = subRows[0].length,
			table = new Array(i);

		// create table-like structure in an array so it can be populated
		// with row-spans and col-spans
		while (i--) {
			table[i] = new Array(numCols);
		}

		var associations = {};

		for (i = 0; i < l; i++) {
			var row = table[i],
				subRow = subRows[i];

			// j: counter for table columns
			// js: counter for subrow structure columns
			for (var j = 0, js = 0; j < numCols; j++) {
				var cell = subRow[js], k;

				// if something already exists in the table (row-span), skip this
				// spot and go to the next
				if (typeof row[j] !== 'undefined') {
					continue;
				}
				row[j] = cell.id;

				if (cell.rowSpan && cell.rowSpan > 1) {
					addRowSpan(table, cell.rowSpan, i, j, cell.id);
				}

				// colSpans are only applicable in the second or greater rows
				// and only if the colSpan is greater than 1
				if (i > 0 && cell.colSpan && cell.colSpan > 1) {
					for (k = 1; k < cell.colSpan; k++) {
						// increment j and assign the id since this is a span
						row[++j] = cell.id;
						if (cell.rowSpan && cell.rowSpan > 1) {
							addRowSpan(table, cell.rowSpan, i, j, cell.id);
						}
					}
				}
				associations[cell.id] = subRows[0][j].id;
				js++;
			}
		}

		return associations;
	}

	function resizeColumnWidth(grid, colId, width, parentType, doResize) {
		// don't react to widths <= 0, e.g. for hidden columns
		if (width <= 0) {
			return;
		}

		var column = grid.columns[colId],
			event,
			rule;

		if (!column) {
			return;
		}

		event = {
			grid: grid,
			columnId: colId,
			width: width,
			bubbles: true,
			cancelable: true
		};

		if (parentType) {
			event.parentType = parentType;
		}

		if (!grid._resizedColumns || listen.emit(grid.headerNode, 'dgrid-columnresize', event)) {
			// Update width on column object, then convert value for CSS
			if (width === 'auto') {
				delete column.width;
			}
			else {
				column.width = width;
				width += 'px';
			}

			rule = grid._columnSizes[colId];

			if (rule) {
				// Modify existing, rather than deleting + adding
				rule.set('width', width);
			}
			else {
				// Use miscUtil function directly, since we clean these up ourselves anyway
				rule = miscUtil.addCssRule('#' + miscUtil.escapeCssIdentifier(grid.domNode.id) +
					' .dgrid-column-' + miscUtil.escapeCssIdentifier(colId, '-'),
					'width: ' + width + ';');
			}

			// keep a reference for future removal
			grid._columnSizes[colId] = rule;

			if (doResize !== false) {
				grid.resize();
			}

			return true;
		}
	}

	// Functions for shared resizer node

	var resizerNode, // DOM node for resize indicator, reused between instances
		resizableCount = 0; // Number of ColumnResizer-enabled grid instances
	var resizer = {
		// This object contains functions for manipulating the shared resizerNode
		create: function () {
			resizerNode = domConstruct.create('div', { className: 'dgrid-column-resizer' });
		},
		destroy: function () {
			domConstruct.destroy(resizerNode);
			resizerNode = null;
		},
		show: function (grid) {
			var pos = geom.position(grid.domNode, true);
			resizerNode.style.top = pos.y + 'px';
			resizerNode.style.height = pos.h + 'px';
			document.body.appendChild(resizerNode);
		},
		move: function (x) {
			resizerNode.style.left = x + 'px';
		},
		hide: function () {
			resizerNode.parentNode.removeChild(resizerNode);
		}
	};

	return declare('dgrid.ColumnResizer',null, {
		resizeNode: null,

		// minWidth: Number
		//		Minimum column width, in px.
		minWidth: 40,

		// adjustLastColumn: Boolean
		//		If true, adjusts the last column's width to "auto" at times where the
		//		browser would otherwise stretch all columns to span the grid.
		adjustLastColumn: true,

		_resizedColumns: false, // flag indicating if resizer has converted column widths to px

		buildRendering: function () {
			this.inherited(arguments);

			// Create resizerNode when first grid w/ ColumnResizer is created
			if (!resizableCount) {
				resizer.create();
			}
			resizableCount++;
		},

		destroy: function () {
			this.inherited(arguments);

			// Remove any applied column size styles since we're tracking them directly
			for (var name in this._columnSizes) {
				this._columnSizes[name].remove();
			}

			// If this is the last grid on the page with ColumnResizer, destroy the
			// shared resizerNode
			if (!--resizableCount) {
				resizer.destroy();
			}
		},

		resizeColumnWidth: function (colId, width) {
			// Summary:
			//      calls grid's styleColumn function to add a style for the column
			// colId: String
			//      column id
			// width: Integer
			//      new width of the column
			return resizeColumnWidth(this, colId, width);
		},

		configStructure: function () {
			var oldSizes = this._oldColumnSizes = lang.mixin({}, this._columnSizes), // shallow clone
				k;

			this._resizedColumns = false;
			this._columnSizes = {};

			this.inherited(arguments);

			// Remove old column styles that are no longer relevant; this is specifically
			// done *after* calling inherited so that _columnSizes will contain keys
			// for all columns in the new structure that were assigned widths.
			for (k in oldSizes) {
				if (!(k in this._columnSizes)) {
					oldSizes[k].remove();
				}
			}
			delete this._oldColumnSizes;
		},

		_configColumn: function (column) {
			this.inherited(arguments);

			var colId = column.id,
				rule;

			if ('width' in column) {
				// Update or add a style rule for the specified width
				if ((rule = this._oldColumnSizes[colId])) {
					rule.set('width', column.width + 'px');
				}
				else {
					rule = miscUtil.addCssRule('#' + miscUtil.escapeCssIdentifier(this.domNode.id) +
						' .dgrid-column-' + miscUtil.escapeCssIdentifier(colId, '-'),
						'width: ' + column.width + 'px;');
				}
				this._columnSizes[colId] = rule;
			}
		},

		renderHeader: function () {
			this.inherited(arguments);

			var grid = this;

			var assoc;
			if (this.columnSets && this.columnSets.length) {
				var csi = this.columnSets.length;
				while (csi--) {
					assoc = lang.mixin(assoc || {}, subRowAssoc(this.columnSets[csi]));
				}
			}
			else if (this.subRows && this.subRows.length > 1) {
				assoc = subRowAssoc(this.subRows);
			}

			var colNodes = query('.dgrid-cell', grid.headerNode),
				i = colNodes.length;
			while (i--) {
				var colNode = colNodes[i],
					id = colNode.columnId,
					col = grid.columns[id],
					childNodes = colNode.childNodes,
					resizeHandle;

				if (!col || col.resizable === false) {
					continue;
				}

				var headerTextNode = domConstruct.create('div', { className: 'dgrid-resize-header-container' });
				colNode.contents = headerTextNode;

				// move all the children to the header text node
				while (childNodes.length > 0) {
					headerTextNode.appendChild(childNodes[0]);
				}

				resizeHandle = domConstruct.create('div', {
					className: 'dgrid-resize-handle resizeNode-' + miscUtil.escapeCssIdentifier(id, '-')
				}, headerTextNode);
				colNode.appendChild(headerTextNode);
				resizeHandle.columnId = assoc && assoc[id] || id;
			}

			if (!grid.mouseMoveListen) {
				// establish listeners for initiating, dragging, and finishing resize
				listen(grid.headerNode,
					'.dgrid-resize-handle:mousedown' +
						(has('touch') ? ',.dgrid-resize-handle:touchstart' : ''),
					function (e) {
						grid._resizeMouseDown(e, this);
						grid.mouseMoveListen.resume();
						grid.mouseUpListen.resume();
					}
				);
				grid._listeners.push(grid.mouseMoveListen =
					listen.pausable(document,
						'mousemove' + (has('touch') ? ',touchmove' : ''),
						miscUtil.throttleDelayed(function (e) {
							grid._updateResizerPosition(e);
						})
				));
				grid._listeners.push(grid.mouseUpListen = listen.pausable(document,
					'mouseup' + (has('touch') ? ',touchend' : ''),
					function (e) {
						grid._resizeMouseUp(e);
						grid.mouseMoveListen.pause();
						grid.mouseUpListen.pause();
					}
				));
				// initially pause the move/up listeners until a drag happens
				grid.mouseMoveListen.pause();
				grid.mouseUpListen.pause();
			}
		}, // end renderHeader

		_resizeMouseDown: function (e, target) {
			// Summary:
			//      called when mouse button is pressed on the header
			// e: Object
			//      mousedown event object

			// preventDefault actually seems to be enough to prevent browser selection
			// in all but IE < 9.  setSelectable works for those.
			e.preventDefault();
			dom.setSelectable(this.domNode, false);
			this._startX = this._getResizeMouseLocation(e); //position of the target

			this._targetCell = query('.dgrid-column-' + miscUtil.escapeCssIdentifier(target.columnId, '-'),
				this.headerNode)[0];

			// Show resizerNode after initializing its x position
			this._updateResizerPosition(e);
			resizer.show(this);
		},
		_resizeMouseUp: function (e) {
			// Summary:
			//      called when mouse button is released
			// e: Object
			//      mouseup event object

			var columnSizes = this._columnSizes,
				colNodes, colWidths, gridWidth;

			if (this.adjustLastColumn) {
				// For some reason, total column width needs to be 1 less than this
				gridWidth = this.headerNode.clientWidth - 1;
			}

			//This is used to set all the column widths to a static size
			if (!this._resizedColumns) {
				colNodes = query('.dgrid-cell', this.headerNode);

				if (this.columnSets && this.columnSets.length) {
					colNodes = colNodes.filter(function (node) {
						var idx = node.columnId.split('-');
						return idx[0] === '0' && !(node.columnId in columnSizes);
					});
				}
				else if (this.subRows && this.subRows.length > 1) {
					colNodes = colNodes.filter(function (node) {
						return node.columnId.charAt(0) === '0' && !(node.columnId in columnSizes);
					});
				}

				// Get a set of sizes before we start mutating, to avoid
				// weird disproportionate measures if the grid has set
				// column widths, but no full grid width set
				colWidths = colNodes.map(function (colNode) {
					return colNode.offsetWidth;
				});

				// Set a baseline size for each column based on
				// its original measure
				colNodes.forEach(function (colNode, i) {
					resizeColumnWidth(this, colNode.columnId, colWidths[i], null, false);
				}, this);

				this._resizedColumns = true;
			}
			dom.setSelectable(this.domNode, true);


			var cell = this._targetCell,
				delta = this._getResizeMouseLocation(e) - this._startX, //final change in position of resizer
				newWidth = cell.offsetWidth + delta, //the new width after resize
				obj = this._getResizedColumnWidths(),//get current total column widths before resize
				totalWidth = obj.totalWidth,
				lastCol = obj.lastColId,
				lastColWidth = query('.dgrid-column-' + miscUtil.escapeCssIdentifier(lastCol, '-'),
					this.headerNode)[0].offsetWidth;

			if (newWidth < this.minWidth) {
				//enforce minimum widths
				newWidth = this.minWidth;
			}

			if (resizeColumnWidth(this, cell.columnId, newWidth, e.type)) {
				if (cell.columnId !== lastCol && this.adjustLastColumn) {
					if (totalWidth + delta < gridWidth) {
						//need to set last column's width to auto
						resizeColumnWidth(this, lastCol, 'auto', e.type);
					}
					else if (lastColWidth - delta <= this.minWidth) {
						//change last col width back to px, unless it is the last column itself being resized...
						resizeColumnWidth(this, lastCol, this.minWidth, e.type);
					}
				}
			}
			resizer.hide();

			// Clean up after the resize operation
			delete this._startX;
			delete this._targetCell;
		},

		_updateResizerPosition: function (e) {
			// Summary:
			//      updates position of resizer bar as mouse moves
			// e: Object
			//      mousemove event object

			if (!this._targetCell) {
				return; // Release event was already processed
			}

			var mousePos = this._getResizeMouseLocation(e),
				delta = mousePos - this._startX, //change from where user clicked to where they drag
				width = this._targetCell.offsetWidth,
				left = mousePos;
			if (width + delta < this.minWidth) {
				left = this._startX - (width - this.minWidth);
			}
			resizer.move(left);
		},

		_getResizeMouseLocation: function (e) {
			//Summary:
			//      returns position of mouse relative to the left edge
			// e: event object
			//      mouse move event object
			var posX = 0;
			if (e.pageX) {
				posX = e.pageX;
			}
			else if (e.clientX) {
				posX = e.clientX + document.body.scrollLeft +
					document.documentElement.scrollLeft;
			}
			return posX;
		},
		_getResizedColumnWidths: function () {
			//Summary:
			//      returns object containing new column width and column id
			var totalWidth = 0,
				colNodes = query(
					(this.columnSets ? '.dgrid-column-set-cell ' : '') + 'tr:first-child .dgrid-cell',
					this.headerNode);

			var i = colNodes.length;
			if (!i) {
				return {};
			}

			var lastColId = colNodes[i - 1].columnId;

			while (i--) {
				totalWidth += colNodes[i].offsetWidth;
			}
			return {totalWidth: totalWidth, lastColId: lastColId};
		}
	});
});

},
'dgrid/extensions/CompoundColumns':function(){
define([
	'dojo/_base/lang',
	'dojo/_base/declare',
	'dojo/sniff',
	'dojo/query',
	'../util/misc'
], function (lang, declare, has, query, miscUtil) {
	return declare(null, {
		// summary:
		//		Extension allowing for specification of columns with additional
		//		header rows spanning multiple columns for strictly display purposes.
		//		Only works on `columns` arrays, not `columns` objects or `subRows`
		//		(nor ColumnSets).
		// description:
		//		CompoundColumns allows nested header cell configurations, wherein the
		//		higher-level headers may span multiple columns and are for
		//		display purposes only.
		//		These nested header cells are configured using a special recursive
		//		`children` property in the column definition, where only the deepest
		//		children are ultimately rendered in the grid as actual columns.
		//		In addition, the deepest child columns may be rendered without
		//		individual headers by specifying `showChildHeaders: false` on the parent.

		configStructure: function () {
			// create a set of sub rows for the header row so we can do compound columns
			// the first row is a special spacer row
			var columns = (this.subRows && this.subRows[0]) || this.columns,
				headerRows = [[]],
				topHeaderRow = headerRows[0],
				contentColumns = [];
			// This first row is spacer row that will be made invisible (zero height)
			// with CSS, but it must be rendered as the first row since that is what
			// the table layout is driven by.
			headerRows[0].className = 'dgrid-spacer-row';

			function processColumns(columns, level, hasLabel, parent) {
				var numColumns = 0,
					noop = function () {},
					children,
					hasChildLabels;

				function processColumn(column, i) {
					// Handle the column config when it is an object rather
					// than an array.
					if (typeof column === 'string') {
						column = {label: column};
					}
					if (!(columns instanceof Array) && !column.field) {
						column.field = i;
					}
					children = column.children;
					hasChildLabels = children && (column.showChildHeaders !== false);
					// Set a reference to the parent column so later the children's ids can
					// be updated to indicate the parent-child relationship.
					column.parentColumn = parent;
					if (children) {
						// it has children
						// make sure the column has an id
						if (column.id == null) {
							column.id = ((parent && parent.id) || level - 1) + '-' + topHeaderRow.length;
						}
						else if (parent && parent.id) {
							// Make sure nested compound columns have ids that are prefixed with
							// their parent's ids.
							column.id = parent.id + '-' + column.id;
						}
					}
					else {
						// it has no children, it is a normal header, add it to the content columns
						contentColumns.push(column);
						// add each one to the first spacer header row for proper layout of the header cells
						topHeaderRow.push(lang.delegate(column, {renderHeaderCell: noop}));
						numColumns++;
					}
					if (!hasChildLabels) {
						// create a header version of the column where we can define a specific rowSpan
						// we define the rowSpan as a negative, the number of levels less than the
						// total number of rows, which we don't know yet
						column = lang.delegate(column, {rowSpan: -level});
					}

					if (children) {
						// Recursively process the children; this is specifically
						// performed *after* any potential lang.delegate calls
						// so the parent reference will receive additional info
						numColumns += (column.colSpan =
							processColumns(children, level + 1, hasChildLabels, column));
					}

					// add the column to the header rows at the appropriate level
					if (hasLabel) {
						(headerRows[level] || (headerRows[level] = [])).push(column);
					}
				}

				miscUtil.each(columns, processColumn, this);
				return numColumns;
			}

			processColumns(columns, 1, true);

			var numHeaderRows = headerRows.length,
				i, j, headerRow, headerColumn;
			// Now go back through and increase the rowSpans of the headers to be
			// total rows minus the number of levels they are at.
			for (i = 0; i < numHeaderRows; i++) {
				headerRow = headerRows[i];
				for (j = 0; j < headerRow.length; j++) {
					headerColumn = headerRow[j];
					if (headerColumn.rowSpan < 1) {
						headerColumn.rowSpan += numHeaderRows;
					}
				}
			}
			// we need to set this to be used for subRows, so we make it a single row
			contentColumns = [contentColumns];
			// set our header rows so that the grid will use the alternate header row
			// configuration for rendering the headers
			contentColumns.headerRows = headerRows;
			this.subRows = contentColumns;
			this.inherited(arguments);
		},

		renderHeader: function () {
			var i,
				columns = this.subRows[0],
				headerColumns = this.subRows.headerRows[0];

			this.inherited(arguments);

			// The object delegation performed in configStructure unfortunately
			// "protects" the original column definition objects (referenced by
			// columns and subRows) from obtaining headerNode information, so
			// copy them back in.
			for (i = columns.length; i--;) {
				columns[i].headerNode = headerColumns[i].headerNode;
			}
		},

		_findSortArrowParent: function () {
			var parent = this.inherited(arguments),
				columnId,
				nodes;

			if (parent && miscUtil.contains(query('.dgrid-spacer-row', this.headerNode)[0], parent)) {
				// Determine column that the sort arrow is in
				// (fallback is for the padding node in IE < 8)
				columnId = parent.columnId || parent.parentNode.columnId;
				nodes = query('.dgrid-column-' + columnId, this.headerNode);
				return nodes[nodes.length - 1];
			}
		},

		_configColumn: function (column, rowColumns, prefix) {
			// Updates the id on a column definition that is a child to include
			// the parent's id.
			var parent = column.parentColumn;
			var columnId = column.id;
			if (parent) {
				// Adjust the id to incorporate the parent's id.
				// Remove the prefix if it was used to create the id
				var id = columnId.indexOf(prefix) === 0 ? columnId.substring(prefix.length) : columnId;
				prefix = parent.id + '-';
				columnId = column.id = prefix + id;
			}
			this.inherited(arguments, [column, rowColumns, prefix]);
		},

		cell: function (target, columnId) {
			// summary:
			//		Get the cell object by node, event, or id, plus a columnId.
			//		This extension prefixes children's column ids with the parents' column ids,
			//		so cell takes that into account when looking for a column id.

			if (typeof columnId !== 'object') {
				// Find the columnId that corresponds with the provided id.
				// The provided id may be a suffix of the actual id.
				var column = this.column(columnId);
				if (column) {
					columnId = column.id;
				}
			}
			return this.inherited(arguments, [target, columnId]);
		},

		column: function (target) {
			// summary:
			//		Get the column object by node, event, or column id.  Take into account parent column id
			//		prefixes that may be added by this extension.
			var results = this.inherited(arguments);
			if (results == null && typeof target !== 'object') {
				// Find a column id that ends with the provided column id.  This will locate a child column
				// by an id that was provided in the original column configuration.  For example, if a compound column
				// was given the id "compound" and a child column was given the id "child", this will find the column
				// using only "child".  If "compound-child" was being searched for, the inherited call
				// above would have found the cell.
				var suffix = '-' + target,
					suffixLength = suffix.length;
				for (var completeId in this.columns) {
					if (completeId.indexOf(suffix, completeId.length - suffixLength) !== -1) {
						return this.columns[completeId];
					}
				}
			}
			return results;
		},

		_updateCompoundHiddenStates: function (id, hidden) {
			// summary:
			//		Called from _hideColumn and _showColumn (for ColumnHider)
			//		to adjust parent header cells

			var column = this.columns[id],
				colSpan;

			if (column && column.hidden === hidden) {
				// Avoid redundant processing (since it would cause colSpan skew)
				return;
			}

			// column will be undefined when this is called for parents
			while (column && column.parentColumn) {
				// Update colSpans / hidden state of parents
				column = column.parentColumn;
				colSpan = column.colSpan = column.colSpan + (hidden ? -1 : 1);

				if (colSpan) {
					column.headerNode.colSpan = colSpan;
				}
				if (colSpan === 1 && !hidden) {
					this._showColumn(column.id);
				}
				else if (!colSpan && hidden) {
					this._hideColumn(column.id);
				}
			}
		},

		_hideColumn: function (id) {
			var self = this;

			this._updateCompoundHiddenStates(id, true);
			this.inherited(arguments);

			if (has('ff')) {
				// Firefox causes display quirks in certain situations;
				// avoid them by forcing reflow of the header
				this.headerNode.style.display = 'none';
				setTimeout(function () {
					self.headerNode.style.display = '';
					self.resize();
				}, 0);
			}
		},

		_showColumn: function (id) {
			this._updateCompoundHiddenStates(id, false);
			this.inherited(arguments);
		},

		_getResizedColumnWidths: function () {
			// Overrides ColumnResizer method to report the total width and
			// last column correctly for CompoundColumns structures

			var total = 0,
				columns = this.columns,
				id;

			for (id in columns) {
				total += columns[id].headerNode.offsetWidth;
			}

			return {
				totalWidth: total,
				lastColId: this.subRows[0][this.subRows[0].length - 1].id
			};
		}
	});
});

},
'dgrid/extensions/Pagination':function(){
define([
	'../_StoreMixin',
	'dojo/_base/declare',
	'dojo/_base/array',
	'dojo/_base/lang',
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/on',
	'dojo/query',
	'dojo/string',
	'dojo/has',
	'dojo/when',
	'../util/misc',
	'dojo/_base/sniff'
], function (_StoreMixin, declare, arrayUtil, lang, domConstruct, domClass, on, query, string, has, when,
		miscUtil) {


	function cleanupContent(grid) {
		// Remove any currently-rendered rows, or noDataMessage
		if (grid.noDataNode) {
			domConstruct.destroy(grid.noDataNode);
			delete grid.noDataNode;
		}
		else {
			grid.cleanup();
		}
		grid.contentNode.innerHTML = '';
	}
	function cleanupLoading(grid) {
		if (grid.loadingNode) {
			domConstruct.destroy(grid.loadingNode);
			delete grid.loadingNode;
		}
		else if (grid._oldPageNodes) {
			// If cleaning up after a load w/ showLoadingMessage: false,
			// be careful to only clean up rows from the old page, not the new one
			for (var id in grid._oldPageNodes) {
				grid.removeRow(grid._oldPageNodes[id]);
			}
			delete grid._oldPageNodes;
		}
		delete grid._isLoading;
	}

	return declare(_StoreMixin, {
		// summary:
		//		An extension for adding discrete pagination to a List or Grid.

		// rowsPerPage: Number
		//		Number of rows (items) to show on a given page.
		rowsPerPage: 10,

		// pagingTextBox: Boolean
		//		Indicates whether or not to show a textbox for paging.
		pagingTextBox: false,
		// previousNextArrows: Boolean
		//		Indicates whether or not to show the previous and next arrow links.
		previousNextArrows: true,
		// firstLastArrows: Boolean
		//		Indicates whether or not to show the first and last arrow links.
		firstLastArrows: false,

		// pagingLinks: Number
		//		The number of page links to show on each side of the current page
		//		Set to 0 (or false) to disable page links.
		pagingLinks: 2,
		// pageSizeOptions: Array[Number]
		//		This provides options for different page sizes in a drop-down.
		//		If it is empty (default), no page size drop-down will be displayed.
		pageSizeOptions: null,

		// showLoadingMessage: Boolean
		//		If true, clears previous data and displays loading node when requesting
		//		another page; if false, leaves previous data in place until new data
		//		arrives, then replaces it immediately.
		showLoadingMessage: true,

		// i18nPagination: Object
		//		This object contains all of the internationalized strings as
		//		key/value pairs.
		i18nPagination: {
			status: '${start} - ${end} of ${total} results',
			gotoFirst: 'Go to first page',
			gotoNext: 'Go to next page',
			gotoPrev: 'Go to previous page',
			gotoLast: 'Go to last page',
			gotoPage: 'Go to page',
			jumpPage: 'Jump to page',
			rowsPerPage: 'Number of rows per page'
		},

		showFooter: true,
		_currentPage: 1,

		buildRendering: function () {
			this.inherited(arguments);

			// add pagination to footer
			var grid = this,
				paginationNode = this.paginationNode =
					domConstruct.create('div', { className: 'dgrid-pagination' }, this.footerNode),
				statusNode = this.paginationStatusNode =
					domConstruct.create('div', { className: 'dgrid-status' }, paginationNode),
				i18n = this.i18nPagination,
				navigationNode,
				node;

			statusNode.tabIndex = 0;

            if(this.addUiClasses){
                domClass.add(this.footerNode,'ui-widget-content');
            }

			// Initialize UI based on pageSizeOptions and rowsPerPage
			this._updatePaginationSizeSelect();
			this._updateRowsPerPageOption();

			// initialize some content into paginationStatusNode, to ensure
			// accurate results on initial resize call
			this._updatePaginationStatus(this._total);

			navigationNode = this.paginationNavigationNode =
				domConstruct.create('div', { className: 'dgrid-navigation' }, paginationNode);

			if (this.firstLastArrows) {
				// create a first-page link
				node = this.paginationFirstNode = domConstruct.create('span', {
					'aria-label': i18n.gotoFirst,
					className: 'dgrid-first dgrid-page-link',
					innerHTML: '«',
					tabIndex: 0
				}, navigationNode);
			}
			if (this.previousNextArrows) {
				// create a previous link
				node = this.paginationPreviousNode = domConstruct.create('span', {
					'aria-label': i18n.gotoPrev,
					className: 'dgrid-previous dgrid-page-link',
					innerHTML: '‹',
					tabIndex: 0
				}, navigationNode);
			}

			this.paginationLinksNode = domConstruct.create('span', {
				className: 'dgrid-pagination-links'
			}, navigationNode);

			if (this.previousNextArrows) {
				// create a next link
				node = this.paginationNextNode = domConstruct.create('span', {
					'aria-label': i18n.gotoNext,
					className: 'dgrid-next dgrid-page-link',
					innerHTML: '›',
					tabIndex: 0
				}, navigationNode);
			}
			if (this.firstLastArrows) {
				// create a last-page link
				node = this.paginationLastNode = domConstruct.create('span', {
					'aria-label': i18n.gotoLast,
					className: 'dgrid-last dgrid-page-link',
					innerHTML: '»',
					tabIndex: 0
				}, navigationNode);
			}

			/* jshint maxlen: 121 */
			this._listeners.push(on(navigationNode, '.dgrid-page-link:click,.dgrid-page-link:keydown', function (event) {
				// For keyboard events, only respond to enter
				if (event.type === 'keydown' && event.keyCode !== 13) {
					return;
				}

				var cls = this.className,
					curr, max;

				if (grid._isLoading || cls.indexOf('dgrid-page-disabled') > -1) {
					return;
				}

				curr = grid._currentPage;
				max = Math.ceil(grid._total / grid.rowsPerPage);

				// determine navigation target based on clicked link's class
				if (this === grid.paginationPreviousNode) {
					grid.gotoPage(curr - 1);
				}
				else if (this === grid.paginationNextNode) {
					grid.gotoPage(curr + 1);
				}
				else if (this === grid.paginationFirstNode) {
					grid.gotoPage(1);
				}
				else if (this === grid.paginationLastNode) {
					grid.gotoPage(max);
				}
				else if (cls === 'dgrid-page-link') {
					grid.gotoPage(+this.innerHTML); // the innerHTML has the page number
				}
			}));
		},

		destroy: function () {
			this.inherited(arguments);
			if (this._pagingTextBoxHandle) {
				this._pagingTextBoxHandle.remove();
			}
		},

		_updatePaginationSizeSelect: function () {
			// summary:
			//		Creates or repopulates the pagination size selector based on
			//		the values in pageSizeOptions. Called from buildRendering
			//		and _setPageSizeOptions.

			var pageSizeOptions = this.pageSizeOptions,
				paginationSizeSelect = this.paginationSizeSelect,
				handle;

			if (pageSizeOptions && pageSizeOptions.length) {
				if (!paginationSizeSelect) {
					// First time setting page options; create the select
					paginationSizeSelect = this.paginationSizeSelect = domConstruct.create('select', {
						'aria-label': this.i18nPagination.rowsPerPage,
						className: 'dgrid-page-size'
					}, this.paginationNode);

					handle = this._paginationSizeChangeHandle =
						on(paginationSizeSelect, 'change', lang.hitch(this, function () {
							this.set('rowsPerPage', +this.paginationSizeSelect.value);
						}));
					this._listeners.push(handle);
				}

				// Repopulate options
				paginationSizeSelect.options.length = 0;
				for (var i = 0; i < pageSizeOptions.length; i++) {
					domConstruct.create('option', {
						innerHTML: pageSizeOptions[i],
						selected: this.rowsPerPage === pageSizeOptions[i],
						value: pageSizeOptions[i]
					}, paginationSizeSelect);
				}
				// Ensure current rowsPerPage value is in options
				this._updateRowsPerPageOption();
			}
			else if (!(pageSizeOptions && pageSizeOptions.length) && paginationSizeSelect) {
				// pageSizeOptions was removed; remove/unhook the drop-down
				domConstruct.destroy(paginationSizeSelect);
				this.paginationSizeSelect = null;
				this._paginationSizeChangeHandle.remove();
			}
		},

		_setPageSizeOptions: function (pageSizeOptions) {
			this.pageSizeOptions = pageSizeOptions && pageSizeOptions.sort(function (a, b) {
				return a - b;
			});
			this._updatePaginationSizeSelect();
		},

		_updateRowsPerPageOption: function () {
			// summary:
			//		Ensures that an option for rowsPerPage's value exists in the
			//		paginationSizeSelect drop-down (if one is rendered).
			//		Called from buildRendering and _setRowsPerPage.

			var rowsPerPage = this.rowsPerPage,
				pageSizeOptions = this.pageSizeOptions,
				paginationSizeSelect = this.paginationSizeSelect;

			if (paginationSizeSelect) {
				if (arrayUtil.indexOf(pageSizeOptions, rowsPerPage) < 0) {
					this._setPageSizeOptions(pageSizeOptions.concat([rowsPerPage]));
				}
				else {
					paginationSizeSelect.value = '' + rowsPerPage;
				}
			}
		},

		_setRowsPerPage: function (rowsPerPage) {
			this.rowsPerPage = rowsPerPage;
			this._updateRowsPerPageOption();
			this.gotoPage(1);
		},

		_updateNavigation: function (total) {
			// summary:
			//		Update status and navigation controls based on total count from query

			var grid = this,
				i18n = this.i18nPagination,
				linksNode = this.paginationLinksNode,
				currentPage = this._currentPage,
				pagingLinks = this.pagingLinks,
				paginationNavigationNode = this.paginationNavigationNode,
				end = Math.ceil(total / this.rowsPerPage),
				pagingTextBoxHandle = this._pagingTextBoxHandle,
				focused = document.activeElement,
				focusedPage,
				lastFocusablePageLink,
				focusableNodes;

			function pageLink(page, addSpace) {
				var link;
				var disabled;
				if (grid.pagingTextBox && page === currentPage && end > 1) {
					// use a paging text box if enabled instead of just a number
					link = domConstruct.create('input', {
						'aria-label': i18n.jumpPage,
						className: 'dgrid-page-input',
						type: 'text',
						value: currentPage
					}, linksNode);
					grid._pagingTextBoxHandle = on(link, 'change', function () {
						var value = +this.value;
						if (!isNaN(value) && value > 0 && value <= end) {
							grid.gotoPage(+this.value);
						}
					});
					if (focused && focused.tagName === 'INPUT') {
						link.focus();
					}
				}
				else {
					// normal link
					disabled = page === currentPage;
					link = domConstruct.create('span', {
						'aria-label': i18n.gotoPage,
						className: 'dgrid-page-link' + (disabled ? ' dgrid-page-disabled' : ''),
						innerHTML: page + (addSpace ? ' ' : ''),
						tabIndex: disabled ? -1 : 0
					}, linksNode);

					// Try to restore focus if applicable;
					// if we need to but can't, try on the previous or next page,
					// depending on whether we're at the end
					if (focusedPage === page) {
						if (!disabled) {
							link.focus();
						}
						else if (page < end) {
							focusedPage++;
						}
						else {
							lastFocusablePageLink.focus();
						}
					}

					if (!disabled) {
						lastFocusablePageLink = link;
					}
				}
			}

			function setDisabled(link, disabled) {
				domClass.toggle(link, 'dgrid-page-disabled', disabled);
				link.tabIndex = disabled ? -1 : 0;
			}

			function addSkipNode() {
				// Adds visual indication of skipped page numbers in navigation area
				domConstruct.create('span', {
					className: 'dgrid-page-skip',
					innerHTML: '...'
				}, linksNode);
			}

			if (!focused || !miscUtil.contains(this.paginationNavigationNode, focused)) {
				focused = null;
			}
			else if (focused.className === 'dgrid-page-link') {
				focusedPage = +focused.innerHTML;
			}

			if (pagingTextBoxHandle) {
				pagingTextBoxHandle.remove();
			}
			linksNode.innerHTML = '';
			query('.dgrid-first, .dgrid-previous', paginationNavigationNode).forEach(function (link) {
				setDisabled(link, currentPage === 1);
			});
			query('.dgrid-last, .dgrid-next', paginationNavigationNode).forEach(function (link) {
				setDisabled(link, currentPage >= end);
			});

			if (pagingLinks && end > 0) {
				// always include the first page (back to the beginning)
				pageLink(1, true);
				var start = currentPage - pagingLinks;
				if (start > 2) {
					addSkipNode();
				}
				else {
					start = 2;
				}
				// now iterate through all the page links we should show
				for (var i = start; i < Math.min(currentPage + pagingLinks + 1, end); i++) {
					pageLink(i, true);
				}
				if (currentPage + pagingLinks + 1 < end) {
					addSkipNode();
				}
				// last link
				if (end > 1) {
					pageLink(end);
				}
			}
			else if (grid.pagingTextBox) {
				// The pageLink function is also used to create the paging textbox.
				pageLink(currentPage);
			}

			if (focused && focused.tabIndex === -1) {
				// One of the first/last or prev/next links was focused but
				// is now disabled, so find something focusable
				focusableNodes = query('[tabindex="0"]', this.paginationNavigationNode);
				if (focused === this.paginationPreviousNode || focused === this.paginationFirstNode) {
					focused = focusableNodes[0];
				}
				else if (focusableNodes.length) {
					focused = focusableNodes[focusableNodes.length - 1];
				}
				if (focused) {
					focused.focus();
				}
			}
		},

		_updatePaginationStatus: function (total) {
			var count = this.rowsPerPage;
			var start = Math.min(total, (this._currentPage - 1) * count + 1);
			this.paginationStatusNode.innerHTML = string.substitute(this.i18nPagination.status, {
				start: start,
				end: Math.min(total, start + count - 1),
				total: total
			});
		},

		refresh: function (options) {
			// summary:
			//		Re-renders the first page of data, or the current page if
			//		options.keepCurrentPage is true.

			var self = this;
			var page = options && options.keepCurrentPage ?
				Math.min(this._currentPage, Math.ceil(this._total / this.rowsPerPage)) : 1;

			this.inherited(arguments);

			// Reset to first page and return promise from gotoPage
			return this.gotoPage(page).then(function (results) {
				// Emit on a separate turn to enable event to be used consistently for
				// initial render, regardless of whether the backing store is async
				setTimeout(function () {
					on.emit(self.domNode, 'dgrid-refresh-complete', {
						bubbles: true,
						cancelable: false,
						grid: self
					});
				}, 0);

				return results;
			});
		},

		_onNotification: function (rows, event, collection) {
			var rowsPerPage = this.rowsPerPage;
			var pageEnd = this._currentPage * rowsPerPage;
			var needsRefresh = (event.type === 'add' && event.index < pageEnd) ||
				(event.type === 'delete' && event.previousIndex < pageEnd) ||
				(event.type === 'update' &&
					Math.floor(event.index / rowsPerPage) !== Math.floor(event.previousIndex / rowsPerPage));

			if (needsRefresh) {
				// Refresh the current page to maintain correct number of rows on page
				this.gotoPage(Math.min(this._currentPage, Math.ceil(event.totalLength / this.rowsPerPage)));
			}
			// If we're not updating the whole page, check if we at least need to update status/navigation
			else if (collection === this._renderedCollection && event.totalLength !== this._total) {
				this._updatePaginationStatus(event.totalLength);
				this._updateNavigation(event.totalLength);
			}
		},

		renderQueryResults: function (results, beforeNode) {
			var grid = this,
				rows = this.inherited(arguments);

			if (!beforeNode) {
				if (this._topLevelRequest) {
					// Cancel previous async request that didn't finish
					this._topLevelRequest.cancel();
					delete this._topLevelRequest;
				}

				if (typeof rows.cancel === 'function') {
					// Store reference to new async request in progress
					this._topLevelRequest = rows;
				}

				rows.then(function () {
					if (grid._topLevelRequest) {
						// Remove reference to request now that it's finished
						delete grid._topLevelRequest;
					}
				});
			}

			return rows;
		},

		insertRow: function () {
			var oldNodes = this._oldPageNodes,
				row = this.inherited(arguments);

			if (oldNodes && row === oldNodes[row.id]) {
				// If the previous row was reused, avoid removing it in cleanup
				delete oldNodes[row.id];
			}

			return row;
		},

		gotoPage: function (page) {
			// summary:
			//		Loads the given page.  Note that page numbers start at 1.
			var grid = this,
				start = (this._currentPage - 1) * this.rowsPerPage;

			if (!this._renderedCollection) {
				console.warn('Pagination requires a collection to operate.');
				return when([]);
			}

			if (this._renderedCollection.releaseRange) {
				this._renderedCollection.releaseRange(start, start + this.rowsPerPage);
			}

			return this._trackError(function () {
				var count = grid.rowsPerPage,
					start = (page - 1) * count,
					options = {
						start: start,
						count: count
					},
					results,
					contentNode = grid.contentNode,
					loadingNode,
					oldNodes,
					children,
					i,
					len;

				if (grid.showLoadingMessage) {
					cleanupContent(grid);
					loadingNode = grid.loadingNode = domConstruct.create('div', {
						className: 'dgrid-loading',
						innerHTML: grid.loadingMessage
					}, contentNode);
				}
				else {
					// Reference nodes to be cleared later, rather than now;
					// iterate manually since IE < 9 doesn't like slicing HTMLCollections
					grid._oldPageNodes = oldNodes = {};
					children = contentNode.children;
					for (i = 0, len = children.length; i < len; i++) {
						oldNodes[children[i].id] = children[i];
					}
				}

				// set flag to deactivate pagination event handlers until loaded
				grid._isLoading = true;

				results = grid._renderedCollection.fetchRange({
					start: start,
					end: start + count
				});

				return grid.renderQueryResults(results, null, options).then(function (rows) {
					cleanupLoading(grid);
					// Reset scroll Y-position now that new page is loaded.
					grid.scrollTo({ y: 0 });

					if (grid._rows) {
						grid._rows.min = start;
						grid._rows.max = start + count - 1;
					}

					results.totalLength.then(function (total) {
						if (!total) {
							if (grid.noDataNode) {
								domConstruct.destroy(grid.noDataNode);
								delete grid.noDataNode;
							}
							// If there are no results, display the no data message.
							grid.noDataNode = domConstruct.create('div', {
								className: 'dgrid-no-data',
								innerHTML: grid.noDataMessage
							}, grid.contentNode);
						}

						// Update status text based on now-current page and total.
						grid._total = total;
						grid._currentPage = page;
						grid._rowsOnPage = rows.length;
						grid._updatePaginationStatus(total);

						// It's especially important that _updateNavigation is called only
						// after renderQueryResults is resolved as well (to prevent jumping).
						grid._updateNavigation(total);
					});

					return results;
				}, function (error) {
					cleanupLoading(grid);
					throw error;
				});
			});
		}
	});
});

},
'dgrid/extensions/DnD':function(){
define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/_base/array',
	'dojo/aspect',
	'dojo/dom-class',
	'dojo/on',
	'dojo/topic',
	'dojo/has',
	'dojo/when',
	'dojo/dnd/Source',
	'dojo/dnd/Manager',
	'dojo/_base/NodeList',
	'../Selection',
	'./util/touch',
	'./_DnD-touch-autoscroll'
], function (declare, lang, arrayUtil, aspect, domClass, on, topic, has, when, DnDSource,
		DnDManager, NodeList, Selection, touchUtil) {
	// Requirements
	// * requires a store (sounds obvious, but not all Lists/Grids have stores...)
	// * must support options.before in put calls
	//   (if undefined, put at end)
	// * should support copy
	//   (copy should also support options.before as above)

	// TODOs
	// * consider sending items rather than nodes to onDropExternal/Internal
	// * consider emitting store errors via OnDemandList._trackError

	var GridDnDSource = declare(DnDSource, {
		grid: null,
		getObject: function (node) {
			// summary:
			//		getObject is a method which should be defined on any source intending
			//		on interfacing with dgrid DnD.

			var grid = this.grid;
			// Extract item id from row node id (gridID-row-*).
			return grid.collection.get(node.id.slice(grid.id.length + 5));
		},
		_legalMouseDown: function (evt) {
			// Fix _legalMouseDown to only allow starting drag from an item
			// (not from bodyNode outside contentNode).
			var legal = this.inherited(arguments);
			return legal && evt.target !== this.grid.bodyNode;
		},

		// DnD method overrides
		onDrop: function (sourceSource, nodes, copy) {
			var targetSource = this,
				targetRow = this._targetAnchor = this.targetAnchor, // save for Internal
				grid = this.grid,
				store = grid.collection;

			if (!this.before && targetRow) {
				// target before next node if dropped within bottom half of this node
				// (unless there's no node to target at all)
				targetRow = targetRow.nextSibling;
			}
			targetRow = targetRow && grid.row(targetRow);

			when(targetRow && store.get(targetRow.id), function (target) {
				// Note: if dropping after the last row, or into an empty grid,
				// target will be undefined.  Thus, it is important for store to place
				// item last in order if options.before is undefined.

				// Delegate to onDropInternal or onDropExternal for rest of logic.
				// These are passed the target item as an additional argument.
				if (targetSource !== sourceSource) {
					targetSource.onDropExternal(sourceSource, nodes, copy, target);
				}
				else {
					targetSource.onDropInternal(nodes, copy, target);
				}
			});
		},
		onDropInternal: function (nodes, copy, targetItem) {
			var grid = this.grid,
				store = grid.collection,
				targetSource = this,
				anchor = targetSource._targetAnchor,
				targetRow,
				nodeRow;

			if (anchor) { // (falsy if drop occurred in empty space after rows)
				targetRow = this.before ? anchor.previousSibling : anchor.nextSibling;
			}

			// Don't bother continuing if the drop is really not moving anything.
			// (Don't need to worry about edge first/last cases since dropping
			// directly on self doesn't fire onDrop, but we do have to worry about
			// dropping last node into empty space beyond rendered rows.)
			nodeRow = grid.row(nodes[0]);
			if (!copy && (targetRow === nodes[0] ||
					(!targetItem && nodeRow && grid.down(nodeRow).element === nodes[0]))) {
				return;
			}

			nodes.forEach(function (node) {
				when(targetSource.getObject(node), function (object) {
					var id = store.getIdentity(object);

					// For copy DnD operations, copy object, if supported by store;
					// otherwise settle for put anyway.
					// (put will relocate an existing item with the same id, i.e. move).
					store[copy && store.copy ? 'copy' : 'put'](object, {
						beforeId: targetItem ? store.getIdentity(targetItem) : null
					});

					// Self-drops won't cause the dgrid-select handler to re-fire,
					// so update the cached node manually
					if (targetSource._selectedNodes[id]) {
						targetSource._selectedNodes[id] = grid.row(id).element;
					}
				});
			});
		},
		onDropExternal: function (sourceSource, nodes, copy, targetItem) {
			// Note: this default implementation expects that two grids do not
			// share the same store.  There may be more ideal implementations in the
			// case of two grids using the same store (perhaps differentiated by
			// query), dragging to each other.
			var store = this.grid.collection,
				sourceGrid = sourceSource.grid;

			// TODO: bail out if sourceSource.getObject isn't defined?
			nodes.forEach(function (node, i) {
				when(sourceSource.getObject(node), function (object) {
					if (!copy) {
						if (sourceGrid) {
							// Remove original in the case of inter-grid move.
							// (Also ensure dnd source is cleaned up properly)
							when(sourceGrid.collection.getIdentity(object), function (id) {
								!i && sourceSource.selectNone(); // deselect all, one time
								sourceSource.delItem(node.id);
								sourceGrid.collection.remove(id);
							});
						}
						else {
							sourceSource.deleteSelectedNodes();
						}
					}
					// Copy object, if supported by store; otherwise settle for put
					// (put will relocate an existing item with the same id).
					// Note that we use store.copy if available even for non-copy dnd:
					// since this coming from another dnd source, always behave as if
					// it is a new store item if possible, rather than replacing existing.
					store[store.copy ? 'copy' : 'put'](object, {
						beforeId: targetItem ? store.getIdentity(targetItem) : null
					});
				});
			});
		},

		onDndStart: function (source) {
			// Listen for start events to apply style change to avatar.

			this.inherited(arguments); // DnDSource.prototype.onDndStart.apply(this, arguments);
			if (source === this) {
				// If TouchScroll is in use, cancel any pending scroll operation.
				if (this.grid.cancelTouchScroll) {
					this.grid.cancelTouchScroll();
				}

				// Set avatar width to half the grid's width.
				// Kind of a naive default, but prevents ridiculously wide avatars.
				DnDManager.manager().avatar.node.style.width =
					this.grid.domNode.offsetWidth / 2 + 'px';
			}
		},

		onMouseDown: function (evt) {
			// Cancel the drag operation on presence of more than one contact point.
			// (This check will evaluate to false under non-touch circumstances.)
			if (has('touch') && this.isDragging &&
					touchUtil.countCurrentTouches(evt, this.grid.touchNode) > 1) {
				topic.publish('/dnd/cancel');
				DnDManager.manager().stopDrag();
			}
			else {
				this.inherited(arguments);
			}
		},

		onMouseMove: function (evt) {
			// If we're handling touchmove, only respond to single-contact events.
			if (!has('touch') || touchUtil.countCurrentTouches(evt, this.grid.touchNode) <= 1) {
				this.inherited(arguments);
			}
		},

		checkAcceptance: function (source) {
			// Augment checkAcceptance to block drops from sources without getObject.
			return source.getObject &&
				DnDSource.prototype.checkAcceptance.apply(this, arguments);
		},
		getSelectedNodes: function () {
			// If dgrid's Selection mixin is in use, synchronize with it, using a
			// map of node references (updated on dgrid-[de]select events).

			if (!this.grid.selection) {
				return this.inherited(arguments);
			}
			var t = new NodeList(),
				id;
			for (id in this.grid.selection) {
				t.push(this._selectedNodes[id]);
			}
			return t;	// NodeList
		}
		// TODO: could potentially also implement copyState to jive with default
		// onDrop* implementations (checking whether store.copy is available);
		// not doing that just yet until we're sure about default impl.
	});

	// Mix in Selection for more resilient dnd handling, particularly when part
	// of the selection is scrolled out of view and unrendered (which we
	// handle below).
	var DnD = declare(Selection, {
		// dndSourceType: String
		//		Specifies the type which will be set for DnD items in the grid,
		//		as well as what will be accepted by it by default.
		dndSourceType: 'dgrid-row',

		// dndParams: Object
		//		Object containing params to be passed to the DnD Source constructor.
		dndParams: null,

		// dndConstructor: Function
		//		Constructor from which to instantiate the DnD Source.
		//		Defaults to the GridSource constructor defined/exposed by this module.
		dndConstructor: GridDnDSource,

		postMixInProperties: function () {
			this.inherited(arguments);
			// ensure dndParams is initialized
			this.dndParams = lang.mixin({ accept: [this.dndSourceType] }, this.dndParams);
		},

		postCreate: function () {
			this.inherited(arguments);

			// Make the grid's content a DnD source/target.
			var Source = this.dndConstructor || GridDnDSource;
			this.dndSource = new Source(
				this.bodyNode,
				lang.mixin(this.dndParams, {
					// add cross-reference to grid for potential use in inter-grid drop logic
					grid: this,
					dropParent: this.contentNode
				})
			);

			// Set up select/deselect handlers to maintain references, in case selected
			// rows are scrolled out of view and unrendered, but then dragged.
			var selectedNodes = this.dndSource._selectedNodes = {};

			function selectRow(row) {
				selectedNodes[row.id] = row.element;
			}
			function deselectRow(row) {
				delete selectedNodes[row.id];
				// Re-sync dojo/dnd UI classes based on deselection
				// (unfortunately there is no good programmatic hook for this)
				domClass.remove(row.element, 'dojoDndItemSelected dojoDndItemAnchor');
			}

			this.on('dgrid-select', function (event) {
				arrayUtil.forEach(event.rows, selectRow);
			});
			this.on('dgrid-deselect', function (event) {
				arrayUtil.forEach(event.rows, deselectRow);
			});

			aspect.after(this, 'destroy', function () {
				delete this.dndSource._selectedNodes;
				selectedNodes = null;
				this.dndSource.destroy();
			}, true);
		},

		insertRow: function (object) {
			// override to add dojoDndItem class to make the rows draggable
			var row = this.inherited(arguments),
				type = typeof this.getObjectDndType === 'function' ?
					this.getObjectDndType(object) : [this.dndSourceType];

			domClass.add(row, 'dojoDndItem');
			this.dndSource.setItem(row.id, {
				data: object,
				type: type instanceof Array ? type : [type]
			});
			return row;
		},

		removeRow: function (rowElement) {
			this.dndSource.delItem(this.row(rowElement));
			this.inherited(arguments);
		}
	});
	DnD.GridSource = GridDnDSource;

	return DnD;
});

},
'dgrid/extensions/_DnD-touch-autoscroll':function(){
define([
	'dojo/aspect',
	'dojo/dom-geometry',
	'dojo/dnd/autoscroll',
	'../List'
], function (aspect, domGeometry, autoscroll, List) {
	// summary:
	//		This module patches the autoScrollNodes function from the
	//		dojo/dnd/autoscroll module, in order to behave properly for
	//		dgrid TouchScroll components.

	var original = autoscroll.autoScrollNodes,
		instances, findEnclosing;

	// In order to properly detect autoscroll cases for dgrid+TouchScroll
	// instances, we need to register instances so that we can look them up based
	// on child nodes later.

	instances = {};
	aspect.after(List.prototype, 'postCreate', function (r) {
		var id = this.id;
		// Since this code is only hooked in some cases, don't throw an error here,
		// but do warn since duplicate IDs or improper destruction are likely going
		// to lead to unintended consequences.
		if (instances[id]) {
			console.warn('dgrid instance registered with duplicate id "' + id + '"');
		}
		instances[id] = this;
		return r;
	});
	aspect.after(List.prototype, 'destroy', function (r) {
		delete instances[this.id];
		return r;
	});
	findEnclosing = function (node) {
		var id, instance;
		while (node) {
			if ((id = node.id) && (instance = instances[id])) {
				return instance;
			}
			node = node.parentNode;
		}
	};

	autoscroll.autoScrollNodes = function (evt) {
		var node = evt.target,
			list = findEnclosing(node),
			pos, nodeX, nodeY, thresholdX, thresholdY, dx, dy, oldScroll, newScroll;

		if (list) {
			// We're inside a dgrid component with TouchScroll; handle using the
			// getScrollPosition and scrollTo APIs instead of scrollTop/Left.
			// All logic here is designed to be functionally equivalent to the
			// existing logic in the original dojo/dnd/autoscroll function.

			node = list.touchNode.parentNode;
			pos = domGeometry.position(node, true);
			nodeX = evt.pageX - pos.x;
			nodeY = evt.pageY - pos.y;
			// Use standard threshold, unless element is too small to warrant it.
			thresholdX = Math.min(autoscroll.H_TRIGGER_AUTOSCROLL, pos.w / 2);
			thresholdY = Math.min(autoscroll.V_TRIGGER_AUTOSCROLL, pos.h / 2);

			// Check whether event occurred beyond threshold in any given direction.
			// If so, we will scroll by an amount equal to the calculated threshold.
			if (nodeX < thresholdX) {
				dx = -thresholdX;
			}
			else if (nodeX > pos.w - thresholdX) {
				dx = thresholdX;
			}

			if (nodeY < thresholdY) {
				dy = -thresholdY;
			}
			else if (nodeY > pos.h - thresholdY) {
				dy = thresholdY;
			}

			// Perform any warranted scrolling.
			if (dx || dy) {
				oldScroll = list.getScrollPosition();
				newScroll = {};
				if (dx) {
					newScroll.x = oldScroll.x + dx;
				}
				if (dy) {
					newScroll.y = oldScroll.y + dy;
				}

				list.scrollTo(newScroll);
				return;
			}
		}
		// If we're not inside a dgrid component with TouchScroll, fall back to
		// the original logic to handle scroll on other elements and the document.
		original.call(this, evt);
	};

	return autoscroll;
});

},
'dgrid/Editor':function(){
define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Deferred',
    'dojo/dom-construct',
    'dojo/dom-class',
    'dojo/on',
    'dojo/has',
    'dojo/query',
    './Grid',
    'dojo/_base/sniff'
], function (declare, lang, Deferred, domConstruct, domClass, on, has, query, Grid) {

    return declare(null, {
        constructor: function () {
            this._editorInstances = {};
            // Tracks shared editor dismissal listeners, and editor click/change listeners for old IE
            this._editorColumnListeners = [];
            // Tracks always-on editor listeners for old IE, or listeners for triggering shared editors
            this._editorCellListeners = {};
            this._editorsPendingStartup = [];
        },

        postCreate: function () {
            var self = this;

            this.inherited(arguments);

            this.on('.dgrid-input:focusin', function () {
                self._focusedEditorCell = self.cell(this);
            });
            this._editorFocusoutHandle = on.pausable(this.domNode, '.dgrid-input:focusout', function () {
                self._focusedEditorCell = null;
            });
            this._listeners.push(this._editorFocusoutHandle);
        },

        insertRow: function () {
            this._editorRowListeners = {};
            var rowElement = this.inherited(arguments);
            var row = this.row(rowElement);
            var rowListeners = this._editorCellListeners[rowElement.id] =
                this._editorCellListeners[rowElement.id] || {};

            for (var key in this._editorRowListeners) {
                rowListeners[key] = this._editorRowListeners[key];
            }
            // Null this out so that _createEditor can tell whether the editor being created is
            // an individual cell editor at insertion time, vs. a cell being refreshed
            this._editorRowListeners = null;

            var previouslyFocusedCell = this._previouslyFocusedEditorCell;

            if (previouslyFocusedCell && previouslyFocusedCell.row.id === row.id) {
                this.edit(this.cell(row, previouslyFocusedCell.column.id));
            }
            return rowElement;
        },

        refresh: function () {
            for (var id in this._editorInstances) {
                var editorInstanceDomNode = this._editorInstances[id].domNode;
                if (editorInstanceDomNode && editorInstanceDomNode.parentNode) {
                    // Remove any editor widgets from the DOM before List destroys it, to avoid issues in IE (#1100)
                    editorInstanceDomNode.parentNode.removeChild(editorInstanceDomNode);
                }
            }

            this.inherited(arguments);
        },

        removeRow: function (rowElement) {
            var self = this;
            var focusedCell = this._focusedEditorCell;

            if (focusedCell && focusedCell.row.id === this.row(rowElement).id) {
                this._previouslyFocusedEditorCell = focusedCell;
                // Pause the focusout handler until after this row has had
                // time to re-render, if this removal is part of an update.
                // A setTimeout is used here instead of resuming in insertRow,
                // since if a row were actually removed (not updated) while
                // editing, the handler would not be properly hooked up again
                // for future occurrences.
                this._editorFocusoutHandle.pause();
                setTimeout(function () {
                    self._editorFocusoutHandle.resume();
                    self._previouslyFocusedEditorCell = null;
                }, 0);
            }

            if (this._editorCellListeners[rowElement.id]) {
                for (var columnId in this._editorCellListeners[rowElement.id]) {
                    this._editorCellListeners[rowElement.id][columnId].remove();
                }
                delete this._editorCellListeners[rowElement.id];
            }

            for (var i = this._alwaysOnWidgetColumns.length; i--;) {
                // Destroy always-on editor widgets during the row removal operation,
                // but don't trip over loading nodes from incomplete requests
                var cellElement = this.cell(rowElement, this._alwaysOnWidgetColumns[i].id).element,
                    widget = cellElement && (cellElement.contents || cellElement).widget;
                if (widget) {
                    this._editorFocusoutHandle.pause();
                    widget.destroyRecursive();
                }
            }

            return this.inherited(arguments);
        },

        renderArray: function () {
            var rows = this.inherited(arguments);
            if (rows.length) {
                // Finish processing any pending editors that are now displayed
                this._startupPendingEditors();
            }
            else {
                this._editorsPendingStartup = [];
            }
            return rows;
        },

        _onNotification: function () {
            this.inherited(arguments);
            this._startupPendingEditors();
        },

        _destroyColumns: function () {
            this._editorStructureCleanup();
            this.inherited(arguments);
        },

        _editorStructureCleanup: function () {
            var editorInstances = this._editorInstances;
            var listeners = this._editorColumnListeners;

            if (this._editTimer) {
                clearTimeout(this._editTimer);
            }
            // Do any clean up of previous column structure.
            for (var columnId in editorInstances) {
                var editor = editorInstances[columnId];
                if (editor.domNode) {
                    // The editor is a widget
                    editor.destroyRecursive();
                }
            }
            this._editorInstances = {};

            for (var i = listeners.length; i--;) {
                listeners[i].remove();
            }

            for (var rowId in this._editorCellListeners) {
                for (columnId in this._editorCellListeners[rowId]) {
                    this._editorCellListeners[rowId][columnId].remove();
                }
            }

            for (i = 0; i < this._editorColumnListeners.length; i++) {
                this._editorColumnListeners[i].remove();
            }

            this._editorCellListeners = {};
            this._editorColumnListeners = [];
            this._editorsPendingStartup = [];
        },

        _configColumns: function () {
            var columnArray = this.inherited(arguments);
            this._alwaysOnWidgetColumns = [];
            for (var i = 0, l = columnArray.length; i < l; i++) {
                if (columnArray[i].editor) {
                    this._configureEditorColumn(columnArray[i]);
                }
            }
            return columnArray;
        },

        _configureEditorColumn: function (column) {
            // summary:
            //		Adds editing capability to a column's cells.

            var editor = column.editor;
            var self = this;

            var originalRenderCell = column.renderCell || this._defaultRenderCell;
            var editOn = column.editOn;
            var isWidget = typeof editor !== 'string';

            if (editOn) {
                // Create one shared widget/input to be swapped into the active cell.
                this._editorInstances[column.id] = this._createSharedEditor(column, originalRenderCell);
            }
            else if (isWidget) {
                // Append to array iterated in removeRow
                this._alwaysOnWidgetColumns.push(column);
            }

            column.renderCell = editOn ? function (object, value, cell, options) {
                    // TODO: Consider using event delegation
                    // (Would require using dgrid's focus events for activating on focus,
                    // which we already advocate in docs for optimal use)

                    if (!options || !options.alreadyHooked) {
                        var listener = on(cell, editOn, function () {
                            self._activeOptions = options;
                            self.edit(this);
                        });
                        if (self._editorRowListeners) {
                            self._editorRowListeners[column.id] = listener;
                        }
                        else {
                            // We're in refreshCell since _editorRowListeners doesn't exist,
                            // so the row should exist
                            var row = self.row(object);
                            self._editorCellListeners[row.element.id][column.id] = listener;
                        }
                    }

                    // initially render content in non-edit mode
                    return originalRenderCell.call(column, object, value, cell, options);

                } : function (object, value, cell, options) {
                    // always-on: create editor immediately upon rendering each cell
                    if (!column.canEdit || column.canEdit(object, value)) {
                        var cmp = self._createEditor(column);
                        //dgrid-hack
                        cmp.object = object;//how to release this?
                        self._showEditor(cmp, column, cell, value);
                        // Maintain reference for later use.
                        cell[isWidget ? 'widget' : 'input'] = cmp;
                    }
                    else {
                        return originalRenderCell.call(column, object, value, cell, options);
                    }
                };
        },

        edit: function (cell) {
            // summary:
            //		Shows/focuses the editor for a given grid cell.
            // cell: Object
            //		Cell (or something resolvable by grid.cell) to activate editor on.
            // returns:
            //		If the cell is editable, returns a promise resolving to the editor
            //		input/widget when the cell editor is focused.
            //		If the cell is not editable, returns null.

            var self = this;
            var column;
            var cellElement;
            var dirty;
            var field;
            var value;
            var cmp;
            var dfd;

            function showEditor(dfd) {
                self._activeCell = cellElement;
                self._showEditor(cmp, column, cellElement, value);

                // focus / blur-handler-resume logic is surrounded in a setTimeout
                // to play nice with Keyboard's dgrid-cellfocusin as an editOn event
                self._editTimer = setTimeout(function () {
                    // focus the newly-placed control (supported by form widgets and HTML inputs)
                    if (cmp.focus) {
                        cmp.focus();
                    }
                    // resume blur handler once editor is focused
                    if (column._editorBlurHandle) {
                        column._editorBlurHandle.resume();
                    }
                    self._editTimer = null;
                    dfd.resolve(cmp);
                }, 0);
            }

            if (!cell.column) {
                cell = this.cell(cell);
            }
            if (!cell || !cell.element) {
                return null;
            }

            column = cell.column;
            field = column.field;
            cellElement = cell.element.contents || cell.element;

            if ((cmp = this._editorInstances[column.id])) {
                // Shared editor (editOn used)
                if (this._activeCell !== cellElement) {
                    // Get the cell value
                    var row = cell.row;
                    dirty = this.dirty && this.dirty[row.id];
                    value = (dirty && field in dirty) ? dirty[field] :
                        column.get ? column.get(row.data) : row.data[field];
                    // Check to see if the cell can be edited
                    if (!column.canEdit || column.canEdit(cell.row.data, value)) {
                        dfd = new Deferred();

                        // In some browsers, moving a DOM node causes a blur event to fire which in this case,
                        // is a bad time for the blur handler to run.  Blur the input node first.
                        var node = cmp.domNode || cmp;
                        if (node.offsetWidth) {
                            // The editor is visible.  Blur it.
                            node.blur();
                            // In IE, the blur does not complete immediately.
                            // Push showing of the editor to the next turn.
                            // (dfd will be resolved within showEditor)
                            setTimeout(function () {
                                showEditor(dfd);
                            }, 0);
                        } else {
                            showEditor(dfd);
                        }

                        return dfd.promise;
                    }
                }
            }
            else if (column.editor) {
                // editor but not shared; always-on
                cmp = cellElement.widget || cellElement.input;
                if (cmp) {
                    dfd = new Deferred();
                    if (cmp.focus) {
                        cmp.focus();
                    }
                    dfd.resolve(cmp);
                    return dfd.promise;
                }
            }
            return null;
        },

        refreshCell: function (cell) {
            var rowElementId = cell.row.element.id;
            var columnId = cell.column.id;
            if (this._editorCellListeners[rowElementId] && this._editorCellListeners[rowElementId][columnId]) {
                this._editorCellListeners[rowElementId][columnId].remove();
                this._editorCellListeners[rowElementId][columnId] = null;
            }
            return this.inherited(arguments);
        },

        _showEditor: function (cmp, column, cellElement, value) {
            // Places a shared editor into the newly-active cell in the column.
            // Also called when rendering an editor in an "always-on" editor column.

            var isWidget = cmp.domNode;
            // for regular inputs, we can update the value before even showing it
            if (!isWidget) {
                this._updateInputValue(cmp, value);
            }

            cellElement.innerHTML = '';
            domClass.add(cellElement, 'dgrid-cell-editing');

            // If a shared editor is a validation widget, reset it to clear validation state
            // (The value will be preserved since it is explicitly set in _startupEditor)
            if (isWidget && column.editOn && cmp.validate && cmp.reset) {
                cmp.reset();
            }

            cellElement.appendChild(cmp.domNode || cmp);

            if (isWidget && !column.editOn) {
                // Queue arguments to be run once editor is in DOM
                this._editorsPendingStartup.push([cmp, column, cellElement, value]);
            }
            else {
                this._startupEditor(cmp, column, cellElement, value);
            }
        },

        _startupEditor: function (cmp, column, cellElement, value) {
            // summary:
            //		Handles editor widget startup logic and updates the editor's value.

            if (cmp.domNode) {
                // For widgets, ensure startup is called before setting value, to maximize compatibility
                // with flaky widgets like dijit/form/Select.
                if (!cmp._started) {
                    cmp.startup();
                }

                // Set value, but ensure it isn't processed as a user-generated change.
                // (Clear flag on a timeout to wait for delayed onChange to fire first)
                cmp._dgridIgnoreChange = true;
                cmp.set('value', value);
                setTimeout(function () {
                    cmp._dgridIgnoreChange = false;
                }, 0);
            }

            // track previous value for short-circuiting or in case we need to revert
            cmp._dgridLastValue = value;
            // if this is an editor with editOn, also update _activeValue
            // (_activeOptions will have been updated previously)
            if (this._activeCell) {
                this._activeValue = value;
                // emit an event immediately prior to placing a shared editor
                on.emit(cellElement, 'dgrid-editor-show', {
                    grid: this,
                    cell: this.cell(cellElement),
                    column: column,
                    editor: cmp,
                    bubbles: true,
                    cancelable: false
                });
            }
        },

        _startupPendingEditors: function () {
            var args = this._editorsPendingStartup;
            for (var i = args.length; i--;) {
                this._startupEditor.apply(this, args[i]);
            }
            this._editorsPendingStartup = [];
        },

        _handleEditorChange: function (evt, column) {
            var target = evt.target;
            if ('_dgridLastValue' in target && target.className.indexOf('dgrid-input') > -1) {
                this._updatePropertyFromEditor(column || this.cell(target).column, target, evt);
            }
        },

        _createEditor: function (column) {
            // Creates an editor instance based on column definition properties,
            // and hooks up events.
            var editor = column.editor,
                editOn = column.editOn,
                self = this,
                Widget = typeof editor !== 'string' && editor,
                args,
                cmp,
                node,
                tagName,
                tagArgs = {};

            args = column.editorArgs || {};
            if (typeof args === 'function') {
                args = args.call(this, column);
            }

            if (Widget) {
                cmp = new Widget(args);
                node = cmp.focusNode || cmp.domNode;

                // Add dgrid-input to className to make consistent with HTML inputs.
                node.className += ' dgrid-input';

                // For editOn editors, connect to onBlur rather than onChange, since
                // the latter is delayed by setTimeouts in Dijit and will fire too late.
                cmp.on(editOn ? 'blur' : 'change', function () {
                    if (!cmp._dgridIgnoreChange) {
                        self._updatePropertyFromEditor(column, this, {type: 'widget'});
                    }
                });
            }
            else {
                // considerations for standard HTML form elements
                if (!this._hasInputListener) {
                    // register one listener at the top level that receives events delegated
                    this._hasInputListener = true;
                    this.on('change', function (evt) {
                        self._handleEditorChange(evt);
                    });
                    // also register a focus listener
                }

                if (editor === 'textarea') {
                    tagName = 'textarea';
                }
                else {
                    tagName = 'input';
                    tagArgs.type = editor;
                }
                cmp = node = domConstruct.create(tagName, lang.mixin(tagArgs, {
                    className: 'dgrid-input',
                    name: column.field,
                    tabIndex: isNaN(column.tabIndex) ? -1 : column.tabIndex
                }, args));
                
            }

            if (column.autoSelect) {
                var selectNode = cmp.focusNode || cmp;
                if (selectNode.select) {
                    on(selectNode, 'focus', function () {
                        // setTimeout is needed for always-on editors on WebKit,
                        // otherwise selection is reset immediately afterwards
                        setTimeout(function () {
                            selectNode.select();
                        }, 0);
                    });
                }
            }

            return cmp;
        },

        _createSharedEditor: function (column) {
            // Creates an editor instance with additional considerations for
            // shared usage across an entire column (for columns with editOn specified).

            var cmp = this._createEditor(column),
                self = this,
                isWidget = cmp.domNode,
                node = cmp.domNode || cmp,
                focusNode = cmp.focusNode || node,
                reset = isWidget ?
                    function () {
                        cmp.set('value', cmp._dgridLastValue);
                    } :
                    function () {
                        self._updateInputValue(cmp, cmp._dgridLastValue);
                        // Update property again in case we need to revert a previous change
                        self._updatePropertyFromEditor(column, cmp);
                    };

            function blur() {
                var element = self._activeCell;
                focusNode.blur();

                if (typeof self.focus === 'function') {
                    // Dijit form widgets don't end up dismissed until the next turn,
                    // so wait before calling focus (otherwise Keyboard will focus the
                    // input again).  IE<9 needs to wait longer, otherwise the cell loses
                    // focus after we've set it.
                    setTimeout(function () {
                        self.focus(element);
                    }, isWidget && has('ie') < 9 ? 15 : 0);
                }
            }

            function onblur() {
                var parentNode = node.parentNode,
                    options = {alreadyHooked: true},
                    cell = self.cell(node);

                // emit an event immediately prior to removing an editOn editor
                on.emit(cell.element, 'dgrid-editor-hide', {
                    grid: self,
                    cell: cell,
                    column: column,
                    editor: cmp,
                    bubbles: true,
                    cancelable: false
                });
                column._editorBlurHandle.pause();
                // Remove the editor from the cell, to be reused later.
                parentNode.removeChild(node);

                if (cell.row) {
                    // If the row is still present (i.e. we didn't blur due to removal),
                    // clear out the rest of the cell's contents, then re-render with new value.
                    domClass.remove(cell.element, 'dgrid-cell-editing');
                    domConstruct.empty(parentNode);
                    Grid.appendIfNode(parentNode, column.renderCell(cell.row.data, self._activeValue, parentNode,
                        self._activeOptions ? lang.delegate(options, self._activeOptions) : options));
                }

                // Reset state now that editor is deactivated;
                // reset _focusedEditorCell as well since some browsers will not
                // trigger the focusout event handler in this case
                self._focusedEditorCell = self._activeCell = self._activeValue = self._activeOptions = null;
            }

            function dismissOnKey(evt) {
                // Contains logic for reacting to enter/escape keypresses to save/cancel edits.
                // Calls `focusNode.blur()` in cases where field should be dismissed.
                var key = evt.keyCode || evt.which;

                if (key === 27) {
                    // Escape: revert + dismiss
                    reset();
                    self._activeValue = cmp._dgridLastValue;
                    blur();
                }
                else if (key === 13 && column.dismissOnEnter !== false) {
                    // Enter: dismiss
                    blur();
                }
            }

            // hook up enter/esc key handling
            this._editorColumnListeners.push(on(focusNode, 'keydown', dismissOnKey));

            // hook up blur handler, but don't activate until widget is activated
            (column._editorBlurHandle = on.pausable(cmp, 'blur', onblur)).pause();
            this._editorColumnListeners.push(column._editorBlurHandle);

            return cmp;
        },

        _updatePropertyFromEditor: function (column, cmp, triggerEvent) {
            var value,
                id,
                editedRow;

            if (!cmp.isValid || cmp.isValid()) {
                value = this._updateProperty((cmp.domNode || cmp).parentNode,
                    this._activeCell ? this._activeValue : cmp._dgridLastValue,
                    this._retrieveEditorValue(column, cmp), triggerEvent);

                if (this._activeCell) { // for editors with editOn defined
                    this._activeValue = value;
                }
                else { // for always-on editors, update _dgridLastValue immediately
                    cmp._dgridLastValue = value;
                }

                if (cmp.type === 'radio' && cmp.name && !column.editOn && column.field) {
                    editedRow = this.row(cmp);

                    // Update all other rendered radio buttons in the group
                    query('input[type=radio][name=' + cmp.name + ']', this.contentNode).forEach(function (radioBtn) {
                        var row = this.row(radioBtn);
                        // Only update _dgridLastValue and the dirty data if it exists
                        // and is not already false
                        if (radioBtn !== cmp && radioBtn._dgridLastValue) {
                            radioBtn._dgridLastValue = false;
                            if (this.updateDirty) {
                                this.updateDirty(row.id, column.field, false);
                            }
                            else {
                                // update store-less grid
                                row.data[column.field] = false;
                            }
                        }
                    }, this);

                    // Also update dirty data for rows that are not currently rendered
                    for (id in this.dirty) {
                        if (editedRow.id.toString() !== id && this.dirty[id][column.field]) {
                            this.updateDirty(id, column.field, false);
                        }
                    }
                }
            }
        },

        _updateProperty: function (cellElement, oldValue, value, triggerEvent) {
            // Updates dirty hash and fires dgrid-datachange event for a changed value.
            var self = this;

            // test whether old and new values are inequal, with coercion (e.g. for Dates)
            if ((oldValue && oldValue.valueOf()) !== (value && value.valueOf())) {
                var cell = this.cell(cellElement);
                var row = cell.row;
                var column = cell.column;
                // Re-resolve cellElement in case the passed element was nested
                cellElement = cell.element;

                if (column.field && row) {
                    var eventObject = {
                        grid: this,
                        cell: cell,
                        oldValue: oldValue,
                        value: value,
                        bubbles: true,
                        cancelable: true
                    };
                    if (triggerEvent && triggerEvent.type) {
                        eventObject.parentType = triggerEvent.type;
                    }

                    if (on.emit(cellElement, 'dgrid-datachange', eventObject)) {
                        if (this.updateDirty) {
                            // for OnDemandGrid: update dirty data, and save if autoSave is true
                            this.updateDirty(row.id, column.field, value);
                            // perform auto-save (if applicable) in next tick to avoid
                            // unintentional mishaps due to order of handler execution
                            if (column.autoSave) {
                                setTimeout(function () {
                                    self._trackError('save');
                                }, 0);
                            }
                        }
                        else {
                            // update store-less grid
                            row.data[column.field] = value;
                        }
                    }
                    else {
                        // Otherwise keep the value the same
                        // For the sake of always-on editors, need to manually reset the value
                        var cmp;
                        if ((cmp = cellElement.widget)) {
                            // set _dgridIgnoreChange to prevent an infinite loop in the
                            // onChange handler and prevent dgrid-datachange from firing
                            // a second time
                            cmp._dgridIgnoreChange = true;
                            cmp.set('value', oldValue);
                            setTimeout(function () {
                                cmp._dgridIgnoreChange = false;
                            }, 0);
                        }
                        else if ((cmp = cellElement.input)) {
                            this._updateInputValue(cmp, oldValue);
                        }

                        return oldValue;
                    }
                }
            }
            return value;
        },

        _updateInputValue: function (input, value) {
            // summary:
            //		Updates the value of a standard input, updating the
            //		checked state if applicable.

            input.value = value;
            if (input.type === 'radio' || input.type === 'checkbox') {
                input.checked = input.defaultChecked = !!value;
            }
        },

        _retrieveEditorValue: function (column, cmp) {
            // summary:
            //		Intermediary between _convertEditorValue and
            //		_updatePropertyFromEditor.

            if (typeof cmp.get === 'function') { // widget
                return this._convertEditorValue(cmp.get('value'));
            }
            else { // HTML input
                return this._convertEditorValue(
                    cmp[cmp.type === 'checkbox' || cmp.type === 'radio' ? 'checked' : 'value']);
            }
        },

        _convertEditorValue: function (value, oldValue) {
            // summary:
            //		Contains default logic for translating values from editors;
            //		tries to preserve type if possible.

            if (typeof oldValue === 'number') {
                value = isNaN(value) ? value : parseFloat(value);
            }
            else if (typeof oldValue === 'boolean') {
                value = value === 'true' ? true : value === 'false' ? false : value;
            }
            else if (oldValue instanceof Date) {
                var asDate = new Date(value);
                value = isNaN(asDate.getTime()) ? value : asDate;
            }
            return value;
        }
    });
});

},
'dstore/Store':function(){
/** @module dstore/Store **/
define([
	'dojo/_base/lang',
	'dojo/_base/array',
	'dojo/aspect',
	'dojo/has',
	'dojo/when',
    'dojo/_base/declare',
	'dstore/QueryMethod',
	'dstore/Filter',
	'dojo/Evented'
], function (lang, arrayUtil, aspect, has, when, declare, QueryMethod, Filter, Evented) {

	// module:
	//		dstore/Store
	/* jshint proto: true */
	// detect __proto__, and avoid using it on Firefox, as they warn about
	// deoptimizations. The watch method is a clear indicator of the Firefox
	// JS engine.
	has.add('object-proto', !!{}.__proto__ && !({}).watch);
	var hasProto = has('object-proto');

	function emitUpdateEvent(type) {
		return function (result, args) {
			var self = this;
			when(result, function (result) {
				var event = { target: result },
					options = args[1] || {};
				if ('beforeId' in options) {
					event.beforeId = options.beforeId;
				}
				self.emit(type, event);
			});

			return result;
		};
	}

	/**
     * Base store class
     * @class module:dstore/Store
     * @extends module:dojo/Evented
     */
	return declare(Evented, {
		constructor: function (options) {
			// perform the mixin
			options && declare.safeMixin(this, options);
            if (this.Model && this.Model.createSubclass) {

				// we need a distinct model for each store, so we can
				// save the reference back to this store on it.
				// we always create a new model to be safe.
                var self = this;
                this.Model = this.Model.createSubclass([],{

                }).extend({
					// give a reference back to the store for saving, etc.
                    _store:this
				});
			}

			// the object the store can use for holding any local data or events
			this.storage = new Evented();
			var store = this;
			if (this.autoEmitEvents) {
				// emit events when modification operations are called
				aspect.after(this, 'add', emitUpdateEvent('add'));
				aspect.after(this, 'put', emitUpdateEvent('update'));
				aspect.after(this, 'remove', function (result, args) {
					when(result, function () {
						store.emit('delete', {id: args[0]});
					});
					return result;
				});
			}
		},

		// autoEmitEvents: Boolean
		//		Indicates if the events should automatically be fired for put, add, remove
		//		method calls. Stores may wish to explicitly fire events, to control when
		//		and which event is fired.
		autoEmitEvents: true,

		// idProperty: String
		//		Indicates the property to use as the identity property. The values of this
		//		property should be unique.
		idProperty: 'id',

		// queryAccessors: Boolean
		//		Indicates if client-side query engine filtering should (if the store property is true)
		//		access object properties through the get() function (enabling querying by
		//		computed properties), or if it should (by setting this to false) use direct/raw
		// 		property access (which may more closely follow database querying style).
		queryAccessors: true,

		getIdentity: function (object) {
			// summary:
			//		Returns an object's identity
			// object: Object
			//		The object to get the identity from
			// returns: String|Number

			return object.get ? object.get(this.idProperty) : object[this.idProperty];
		},

		_setIdentity: function (object, identityArg) {
			// summary:
			//		Sets an object's identity
			// description:
			//		This method sets an object's identity and is useful to override to support
			//		multi-key identities and object's whose properties are not stored directly on the object.
			// object: Object
			//		The target object
			// identityArg:
			//		The argument used to set the identity

			if (object.set) {
				object.set(this.idProperty, identityArg);
			} else {
				object[this.idProperty] = identityArg;
			}
		},

		forEach: function (callback, thisObject) {
			var collection = this;
			return when(this.fetch(), function (data) {
				for (var i = 0, item; (item = data[i]) !== undefined; i++) {
					callback.call(thisObject, item, i, collection);
				}
				return data;
			});
		},
		on: function (type, listener) {
			return this.storage.on(type, listener);
		},
		emit: function (type, event) {
			event = event || {};
			event.type = type;
			try {
				return this.storage.emit(type, event);
			} finally {
				// Return the initial value of event.cancelable because a listener error makes it impossible
				// to know whether the event was actually canceled
				return event.cancelable;
			}
		},

		// parse: Function
		//		One can provide a parsing function that will permit the parsing of the data. By
		//		default we assume the provide data is a simple JavaScript array that requires
		//		no parsing (subclass stores may provide their own default parse function)
		parse: null,

		// stringify: Function
		//		For stores that serialize data (to send to a server, for example) the stringify
		//		function can be specified to control how objects are serialized to strings
		stringify: null,

		// Model: Function
		//		This should be a entity (like a class/constructor) with a 'prototype' property that will be
		//		used as the prototype for all objects returned from this store. One can set
		//		this to the Model from dmodel/Model to return Model objects, or leave this
		//		to null if you don't want any methods to decorate the returned
		//		objects (this can improve performance by avoiding prototype setting),
		Model: null,

		_restore: function (object, mutateAllowed) {
			// summary:
			//		Restores a plain raw object, making an instance of the store's model.
			//		This is called when an object had been persisted into the underlying
			//		medium, and is now being restored. Typically restored objects will come
			//		through a phase of deserialization (through JSON.parse, DB retrieval, etc.)
			//		in which their __proto__ will be set to Object.prototype. To provide
			//		data model support, the returned object needs to be an instance of the model.
			//		This can be accomplished by setting __proto__ to the model's prototype
			//		or by creating a new instance of the model, and copying the properties to it.
			//		Also, model's can provide their own restore method that will allow for
			//		custom model-defined behavior. However, one should be aware that copying
			//		properties is a slower operation than prototype assignment.
			//		The restore process is designed to be distinct from the create process
			//		so their is a clear delineation between new objects and restored objects.
			// object: Object
			//		The raw object with the properties that need to be defined on the new
			//		model instance
			// mutateAllowed: boolean
			//		This indicates if restore is allowed to mutate the original object
			//		(by setting its __proto__). If this isn't true, than the restore should
			//		copy the object to a new object with the correct type.
			// returns: Object
			//		An instance of the store model, with all the properties that were defined
			//		on object. This may or may not be the same object that was passed in.
			var Model = this.Model;
			if (Model && object) {
				var prototype = Model.prototype;
				var restore = prototype._restore;
				if (restore) {
					// the prototype provides its own restore method
					object = restore.call(object, Model, mutateAllowed);
				} else if (hasProto && mutateAllowed) {
					// the fast easy way
					// http://jsperf.com/setting-the-prototype
					object.__proto__ = prototype;
				} else {
					// create a new object with the correct prototype
					object = lang.delegate(prototype, object);
				}
			}
			return object;
		},

		create: function (properties) {
			// summary:
			//		This creates a new instance from the store's model.
			//	properties:
			//		The properties that are passed to the model constructor to
			//		be copied onto the new instance. Note, that should only be called
			//		when new objects are being created, not when existing objects
			//		are being restored from storage.
			return new this.Model(properties);
		},

		_createSubCollection: function (kwArgs) {
			var newCollection = lang.delegate(this.constructor.prototype);

			for (var i in this) {
				if (this._includePropertyInSubCollection(i, newCollection)) {
					newCollection[i] = this[i];
				}
			}

			return declare.safeMixin(newCollection, kwArgs);
		},

		_includePropertyInSubCollection: function (name, subCollection) {
			return !(name in subCollection) || subCollection[name] !== this[name];
		},

		// queryLog: __QueryLogEntry[]
		//		The query operations represented by this collection
		queryLog: [],	// NOTE: It's ok to define this on the prototype because the array instance is never modified

		filter: new QueryMethod({
			type: 'filter',
			normalizeArguments: function (filter) {
				var Filter = this.Filter;
				if (filter instanceof Filter) {
					return [filter];
				}
				return [new Filter(filter)];
			}
		}),

		Filter: Filter,

		sort: new QueryMethod({
			type: 'sort',
			normalizeArguments: function (property, descending) {
				var sorted;
				if (typeof property === 'function') {
					sorted = [ property ];
				} else {
					if (property instanceof Array) {
						sorted = property.slice();
					} else if (typeof property === 'object') {
						sorted = [].slice.call(arguments);
					} else {
						sorted = [{ property: property, descending: descending }];
					}

					sorted = arrayUtil.map(sorted, function (sort) {
						// copy the sort object to avoid mutating the original arguments
						sort = lang.mixin({}, sort);
						sort.descending = !!sort.descending;
						return sort;
					});
					// wrap in array because sort objects are a single array argument
					sorted = [ sorted ];
				}
				return sorted;
			}
		}),

		select: new QueryMethod({
			type: 'select'
		}),

		_getQuerierFactory: function (type) {
			var uppercaseType = type[0].toUpperCase() + type.substr(1);
			return this['_create' + uppercaseType + 'Querier'];
		}

/*====,
		get: function (id) {
			// summary:
			//		Retrieves an object by its identity
			// id: Number
			//		The identity to use to lookup the object
			// returns: Object
			//		The object in the store that matches the given id.
		},
		put: function (object, directives) {
			// summary:
			//		Stores an object
			// object: Object
			//		The object to store.
			// directives: dstore/Store.PutDirectives?
			//		Additional directives for storing objects.
			// returns: Object
			//		The object that was stored, with any changes that were made by
			//		the storage system (like generated id)
		},
		add: function (object, directives) {
			// summary:
			//		Creates an object, throws an error if the object already exists
			// object: Object
			//		The object to store.
			// directives: dstore/Store.PutDirectives?
			//		Additional directives for creating objects.
			// returns: Object
			//		The object that was stored, with any changes that were made by
			//		the storage system (like generated id)
		},
		remove: function (id) {
			// summary:
			//		Deletes an object by its identity
			// id: Number
			//		The identity to use to delete the object
		},
		transaction: function () {
			// summary:
			//		Starts a new transaction.
			//		Note that a store user might not call transaction() prior to using put,
			//		delete, etc. in which case these operations effectively could be thought of
			//		as "auto-commit" style actions.
			// returns: dstore/Store.Transaction
			//		This represents the new current transaction.
		},
		getChildren: function (parent) {
			// summary:
			//		Retrieves the children of an object.
			// parent: Object
			//		The object to find the children of.
			// returns: dstore/Store.Collection
			//		A result set of the children of the parent object.
		}
====*/
	});
});


/*====
	var Collection = declare(null, {
		// summary:
		//		This is an abstract API for a collection of objects, which can be filtered,
		//		sorted, and sliced to create new collections. This is considered to be base
		//		interface for all stores and  query results in dstore. Note that the objects in the
		//		collection may not be immediately retrieved from the underlying data
		//		storage until they are actually accessed through forEach() or fetch().

		filter: function (query) {
			// summary:
			//		Filters the collection, returning a new subset collection
			// query: String|Object|Function
			//		The query to use for retrieving objects from the store.
			// returns: Collection
		},
		sort: function (property, descending) {
			// summary:
			//		Sorts the current collection into a new collection, reordering the objects by the provided sort order.
			// property: String|Function
			//		The property to sort on. Alternately a function can be provided to sort with
			// descending?: Boolean
			//		Indicate if the sort order should be descending (defaults to ascending)
			// returns: Collection
		},
		fetchRange: function (kwArgs) {
			// summary:
			//		Retrieves a range of objects from the collection, returning a promise to an array.
			// kwArgs.start: Number
			//		The starting index of objects to return (0-indexed)
			// kwArgs.end: Number
			//		The exclusive end of objects to return
			// returns: Collection
		},
		forEach: function (callback, thisObject) {
			// summary:
			//		Iterates over the query results, based on
			//		https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Objects/Array/forEach.
			//		Note that this may executed asynchronously (in which case it will return a promise),
			//		and the callback may be called after this function returns.
			// callback:
			//		Function that is called for each object in the query results
			// thisObject:
			//		The object to use as |this| in the callback.
			// returns:
			//		undefined|Promise
		},
		fetch: function () {
			// summary:
			//		This can be called to materialize and request the data behind this collection.
			//		Often collections may be lazy, and won't retrieve their underlying data until
			//		forEach or fetch is called. This returns an array, or for asynchronous stores,
			//		this will return a promise, resolving to an array of objects, once the
			//		operation is complete.
			//	returns Array|Promise
		},
		on: function (type, listener) {
			// summary:
			//		This registers a callback for notification of when data is modified in the query results.
			// type: String
			//		There are four types of events defined in this API:
			//		- add - A new object was added
			//		- update - An object was updated
			//		- delete - An object was deleted
			// listener: Function
			//		The listener function is called when objects in the query results are modified
			//		to affect the query result. The listener function is called with a single event object argument:
			//		| listener(event);
			//
			//		- The event object as the following properties:
			//		- type - The event type (of the four above)
			//		- target - This indicates the object that was create or modified.
			//		- id - If an object was removed, this indicates the object that was removed.
			//		The next two properties will only be available if array tracking is employed,
			//		which is usually provided by dstore/Trackable
			//		- previousIndex - The previousIndex parameter indicates the index in the result array where
			//		the object used to be. If the value is -1, then the object is an addition to
			//		this result set (due to a new object being created, or changed such that it
			//		is a part of the result set).
			//		- index - The inex parameter indicates the index in the result array where
			//		the object should be now. If the value is -1, then the object is a removal
			//		from this result set (due to an object being deleted, or changed such that it
			//		is not a part of the result set).

		}
	});

	Collection.SortInformation = declare(null, {
		// summary:
		//		An object describing what property to sort on, and the direction of the sort.
		// property: String
		//		The name of the property to sort on.
		// descending: Boolean
		//		The direction of the sort.  Default is false.
	});
	Store.Collection = Collection;

	Store.PutDirectives = declare(null, {
		// summary:
		//		Directives passed to put() and add() handlers for guiding the update and
		//		creation of stored objects.
		// id: String|Number?
		//		Indicates the identity of the object if a new object is created
		// beforeId: String?
		//		If the collection of objects in the store has a natural ordering,
		//		this indicates that the created or updated object should be placed before the
		//		object whose identity is specified as the value of this property. A value of null indicates that the
		//		object should be last.
		// parent: Object?,
		//		If the store is hierarchical (with single parenting) this property indicates the
		//		new parent of the created or updated object.
		// overwrite: Boolean?
		//		If this is provided as a boolean it indicates that the object should or should not
		//		overwrite an existing object. A value of true indicates that a new object
		//		should not be created, the operation should update an existing object. A
		//		value of false indicates that an existing object should not be updated, a new
		//		object should be created (which is the same as an add() operation). When
		//		this property is not provided, either an update or creation is acceptable.
	});

	Store.Transaction = declare(null, {
		// summary:
		//		This is an object returned from transaction() calls that represents the current
		//		transaction.

		commit: function () {
			// summary:
			//		Commits the transaction. This may throw an error if it fails. Of if the operation
			//		is asynchronous, it may return a promise that represents the eventual success
			//		or failure of the commit.
		},
		abort: function (callback, thisObject) {
			// summary:
			//		Aborts the transaction. This may throw an error if it fails. Of if the operation
			//		is asynchronous, it may return a promise that represents the eventual success
			//		or failure of the abort.
		}
	});

	var __QueryLogEntry = {
		type: String
			The query type
		arguments: Array
			The original query arguments
		normalizedArguments: Array
			The normalized query arguments
		querier: Function?
			A client-side implementation of the query that takes an item array and returns an item array
	};
====*/

},
'dstore/QueryMethod':function(){
define([], function () {
	/*=====
	var __QueryMethodArgs = {
		// type: String
		//		The type of the query. This identifies the query's type in the query log
		//		and the name of the corresponding query engine method.
		// normalizeArguments: Function?
		//		A function that normalizes arguments for consumption by a query engine
		// applyQuery: Function?
		//		A function that takes the query's new subcollection and the query's log entry
		//		and applies it to the new subcollection. This is useful for collections that need
		//		to both declare and implement new query methods.
		// querierFactory: Function?
		//		A factory function that provides a default querier implementation to use when
		//		a collection does not define its own querier factory method for this query type.
	};
	=====*/
	return function QueryMethod(/*__QueryMethodArgs*/ kwArgs) {
		// summary:
		//		The constructor for a dstore collection query method
		// description:
		//		This is the constructor for a collection query method. It encapsulates the following:
		//		* Creating a new subcollection for the query results
		//		* Logging the query in the collection's `queryLog`
		//		* Normalizing query arguments
		//		* Applying the query engine
		// kwArgs:
		//		The properties that define the query method
		// returns: Function
		//		Returns a function that takes query arguments and returns a new collection with
		//		the query associated with it.

		var type = kwArgs.type,
			normalizeArguments = kwArgs.normalizeArguments,
			applyQuery = kwArgs.applyQuery,
			defaultQuerierFactory = kwArgs.querierFactory;

		return function () {
			// summary:
			//		A query method whose arguments are determined by the query type
			// returns: dstore/Collection
			//		A collection representing the query results

			var originalArguments = Array.prototype.slice.call(arguments),
				normalizedArguments = normalizeArguments
					? normalizeArguments.apply(this, originalArguments)
					: originalArguments,
				logEntry = {
					type: type,
					arguments: originalArguments,
					normalizedArguments: normalizedArguments
				},
				querierFactory = this._getQuerierFactory(type) || defaultQuerierFactory;

			if (querierFactory) {
				// Call the query factory in store context to support things like
				// mapping a filter query's string argument to a custom filter method on the collection
				logEntry.querier = querierFactory.apply(this, normalizedArguments);
			}

			var newCollection = this._createSubCollection({
				queryLog: this.queryLog.concat(logEntry)
			});

			return applyQuery ? applyQuery.call(this, newCollection, logEntry) : newCollection;
		};
	};
});

},
'dstore/Filter':function(){
define(['dojo/_base/declare'], function (declare) {
	// a Filter builder
	function filterCreator(type) {
		// constructs a new filter based on type, used to create each comparison method
		return function newFilter() {
			var Filter = this.constructor;
			var filter = new Filter();
			filter.type = type;
			// ensure args is array so we can concat, slice, unshift
			filter.args = Array.prototype.slice.call(arguments);
			if (this.type) {
				// we are chaining, so combine with an and operator
				return filterCreator('and').call(Filter.prototype, this, filter);
			}
			return filter;
		};
	}
	function logicalOperatorCreator(type) {
		// constructs a new logical operator 'filter', used to create each logical operation method
		return function newLogicalOperator() {
			var Filter = this.constructor;
			var argsArray = [];
			for (var i = 0; i < arguments.length; i++) {
				var arg = arguments[i];
				argsArray.push(arg instanceof Filter ? arg : new Filter(arg));
			}
			var filter = new Filter();
			filter.type = type;
			filter.args = argsArray;
			if (this.type === type) {
				// chaining, same type
				// combine arguments
				filter.args = this.args.concat(argsArray);
			} else if (this.type) {
				// chaining, different type
				// add this filter to start of arguments
				argsArray.unshift(this);
			} else if (argsArray.length === 1) {
				// not chaining and only one argument
				// returned filter is the same as the single argument
				filter.type = argsArray[0].type;
				filter.args = argsArray[0].args.slice();
			}
			return filter;
		};
	}
	var Filter = declare(null, {
		constructor: function (filterArg) {
			var argType = typeof filterArg;
			switch (argType) {
				case 'object':
					var filter = this;
					// construct a filter based on the query object
					for (var key in filterArg){
						var value = filterArg[key];
						if (value instanceof this.constructor) {
							// fully construct the filter from the single arg
							filter = filter[value.type](key, value.args[0]);
						} else if (value && value.test) {
							// support regex
							filter = filter.match(key, value);
						} else {
							filter = filter.eq(key, value);
						}
					}
					this.type = filter.type;
					this.args = filter.args;
					break;
				case 'function': case 'string':
					// allow string and function args as well
					this.type = argType;
					this.args = [filterArg];
			}
		},
		// define our operators
		and: logicalOperatorCreator('and'),
		or: logicalOperatorCreator('or'),
		eq: filterCreator('eq'),
		ne: filterCreator('ne'),
		lt: filterCreator('lt'),
		lte: filterCreator('lte'),
		gt: filterCreator('gt'),
		gte: filterCreator('gte'),
		contains: filterCreator('contains'),
		'in': filterCreator('in'),
		match: filterCreator('match')
	});
	Filter.filterCreator = filterCreator;
	Filter.logicalOperatorCreator = logicalOperatorCreator;
	return Filter;
});
},
'dstore/Memory':function(){
/** @module dstore/Memory **/
define([
	'dojo/_base/declare',
	'dojo/_base/array',
	'./Store',
	'./Promised',
	'./SimpleQuery',
	'./QueryResults'
], function (declare, arrayUtil, Store, Promised, SimpleQuery, QueryResults) {
    /**
     * @class module:dstore/Memory
     */
	return declare([Store, Promised, SimpleQuery ], {
		constructor: function () {
			// summary:
			//		Creates a memory object store.
			// options: dstore/Memory
			//		This provides any configuration information that will be mixed into the store.
			//		This should generally include the data property to provide the starting set of data.

			// Add a version property so subcollections can detect when they're using stale data
			this.storage.version = 0;
		},

		postscript: function () {
			this.inherited(arguments);

			// Set the data in `postscript` so subclasses can override `data` in their constructors
			// (e.g., a LocalStorage store that retrieves its data from localStorage)
			this.setData(this.data || []);
		},

		// data: Array
		//		The array of all the objects in the memory store
		data: null,

		autoEmitEvents: false, // this is handled by the methods themselves

		getSync: function (id) {
			// summary:
			//		Retrieves an object by its identity
			// id: Number
			//		The identity to use to lookup the object
			// returns: Object
			//		The object in the store that matches the given id.
			return this.storage.fullData[this.storage.index[id]];
		},
		putSync: function (object, options) {
			// summary:
			//		Stores an object
			// object: Object
			//		The object to store.
			// options: dstore/Store.PutDirectives?
			//		Additional metadata for storing the data.  Includes an 'id'
			//		property if a specific id is to be used.
			// returns: Number

			options = options || {};

			var storage = this.storage,
				index = storage.index,
				data = storage.fullData;

			var Model = this.Model;
			if (Model && !(object instanceof Model)) {
				// if it is not the correct type, restore a
				// properly typed version of the object. Note that we do not allow
				// mutation here
				object = this._restore(object);
			}
			var id = this.getIdentity(object);
			if (id == null) {
				this._setIdentity(object, ('id' in options) ? options.id : Math.random());
				id = this.getIdentity(object);
			}
			storage.version++;

			var eventType = id in index ? 'update' : 'add',
				event = { target: object },
				previousIndex,
				defaultDestination;
			if (eventType === 'update') {
				if (options.overwrite === false) {
					throw new Error('Object already exists');
				} else {
					data.splice(previousIndex = index[id], 1);
					defaultDestination = previousIndex;
				}
			} else {
				defaultDestination = this.defaultNewToStart ? 0 : data.length;
			}

			var destination;
			if ('beforeId' in options) {
				var beforeId = options.beforeId;

				if (beforeId === null) {
					destination = data.length;
				} else {
					destination = index[beforeId];

					// Account for the removed item
					if (previousIndex < destination) {
						--destination;
					}
				}

				if (destination !== undefined) {
					event.beforeId = beforeId;
				} else {
					console.error('options.beforeId was specified but no corresponding index was found');
					destination = defaultDestination;
				}
			} else {
				destination = defaultDestination;
			}
			data.splice(destination, 0, object);

			// the fullData has been changed, so the index needs updated
			var i = isFinite(previousIndex) ? Math.min(previousIndex, destination) : destination;
			for (var l = data.length; i < l; ++i) {
				index[this.getIdentity(data[i])] = i;
			}

			this.emit(eventType, event);

			return object;
		},
		addSync: function (object, options) {
			// summary:
			//		Creates an object, throws an error if the object already exists
			// object: Object
			//		The object to store.
			// options: dstore/Store.PutDirectives?
			//		Additional metadata for storing the data.  Includes an 'id'
			//		property if a specific id is to be used.
			// returns: Number
			(options = options || {}).overwrite = false;
			// call put with overwrite being false
			return this.putSync(object, options);
		},
		removeSync: function (id) {
			// summary:
			//		Deletes an object by its identity
			// id: Number
			//		The identity to use to delete the object
			// returns: Boolean
			//		Returns true if an object was removed, falsy (undefined) if no object matched the id
			var storage = this.storage;
			var index = storage.index;
			var data = storage.fullData;
			if (id in index) {
				var removed = data.splice(index[id], 1)[0];
				// now we have to reindex
				this._reindex();
				this._ignoreChangeEvents !==true && this.emit('delete', {id: id, target: removed});
				return true;
			}
		},
		setData: function (data) {
			// summary:
			//		Sets the given data as the source for this store, and indexes it
			// data: Object[]
			//		An array of objects to use as the source of data. Note that this
			//		array will not be copied, it is used directly and mutated as
			//		data changes.

			if (this.parse) {
				data = this.parse(data);
			}
			if (data.items) {
				// just for convenience with the data format ItemFileReadStore expects
				this.idProperty = data.identifier || this.idProperty;
				data = data.items;
			}
			var storage = this.storage;
			storage.fullData = this.data = data;
			this._reindex();
		},

		_reindex: function () {
			var storage = this.storage;
			var index = storage.index = {};
			var data = storage.fullData;
			var Model = this.Model;
			var ObjectPrototype = Object.prototype;
			for (var i = 0, l = data.length; i < l; i++) {
				var object = data[i];
				if (Model && !(object instanceof Model)) {
					var restoredObject = this._restore(object,
							// only allow mutation if it is a plain object
							// (which is generally the expected input),
							// if "typed" objects are actually passed in, we will
							// respect that, and leave the original alone
							object.__proto__ === ObjectPrototype);
					if (object !== restoredObject) {
						// a new object was generated in the restoration process,
						// so we have to update the item in the data array.
						data[i] = object = restoredObject;
					}
				}
				index[this.getIdentity(object)] = i;
			}
			storage.version++;
		},

		fetchSync: function () {
			var data = this.data;
			if (!data || data._version !== this.storage.version) {
				// our data is absent or out-of-date, so we requery from the root
				// start with the root data
				data = this.storage.fullData;
				var queryLog = this.queryLog;
				// iterate through the query log, applying each querier
				for (var i = 0, l = queryLog.length; i < l; i++) {
					data = queryLog[i].querier(data);
				}
				// store it, with the storage version stamp
				data._version = this.storage.version;
				this.data = data;
			}
			return new QueryResults(data);
		},

		fetchRangeSync: function (kwArgs) {
			var data = this.fetchSync(),
				start = kwArgs.start,
				end = kwArgs.end;
			return new QueryResults(data.slice(start, end), {
				totalLength: data.length
			});
		},

		_includePropertyInSubCollection: function (name) {
			return name !== 'data' && this.inherited(arguments);
		}
	});
});

},
'dstore/Promised':function(){
define([
	'dojo/_base/declare',
	'dojo/Deferred',
	'./QueryResults',
	'dojo/when'
], function (declare, Deferred, QueryResults, when) {
	// module:
	//		this is a mixin that can be used to provide async methods,
	// 		by implementing their sync counterparts
	function promised(method, query) {
		return function() {
			var deferred = new Deferred();
			try {
				deferred.resolve(this[method].apply(this, arguments));
			} catch (error) {
				deferred.reject(error);
			}
			if (query) {
				// need to create a QueryResults and ensure the totalLength is
				// a promise.
				var queryResults = new QueryResults(deferred.promise);
				queryResults.totalLength = when(queryResults.totalLength);
				return queryResults;
			}
			return deferred.promise;
		};
	}
	return declare(null, {
		get: promised('getSync'),
		put: promised('putSync'),
		add: promised('addSync'),
		remove: promised('removeSync'),
		fetch: promised('fetchSync', true),
		fetchRange: promised('fetchRangeSync', true)
	});
});

},
'dstore/QueryResults':function(){
define(['dojo/_base/lang', 'dojo/when'], function (lang, when) {
	function forEach(callback, instance) {
		return when(this, function(data) {
			for (var i = 0, l = data.length; i < l; i++){
				callback.call(instance, data[i], i, data);
			}
		});
	}
	return function (data, options) {
		var hasTotalLength = options && 'totalLength' in options;
		if(data.then) {
			data = lang.delegate(data);
			// a promise for the eventual realization of the totalLength, in
			// case it comes from the resolved data
			var totalLengthPromise = data.then(function (data) {
				// calculate total length, now that we have access to the resolved data
				var totalLength = hasTotalLength ? options.totalLength :
						data.totalLength || data.length;
				// make it available on the resolved data
				data.totalLength = totalLength;
				// don't return the totalLength promise unless we need to, to avoid
				// triggering a lazy promise
				return !hasTotalLength && totalLength;
			});
			// make the totalLength available on the promise (whether through the options or the enventual
			// access to the resolved data)
			data.totalLength = hasTotalLength ? options.totalLength : totalLengthPromise;
			// make the response available as well
			data.response = options && options.response;
		} else {
			data.totalLength = hasTotalLength ? options.totalLength : data.length;
		}

		data.forEach = forEach;

		return data;
	};
});

},
'dstore/SimpleQuery':function(){
define([
	'dojo/_base/declare',
	'dojo/_base/array'
], function (declare, arrayUtil) {

	// module:
	//		dstore/SimpleQuery

	function makeGetter(property, queryAccessors) {
		if (property.indexOf('.') > -1) {
			var propertyPath = property.split('.');
			var pathLength = propertyPath.length;
			return function (object) {
				for (var i = 0; i < pathLength; i++) {
					object = object && (queryAccessors && object.get ? object.get(propertyPath[i]) : object[propertyPath[i]]);
				}
				return object;
			};
		}
		// else
		return function (object) {
			return object.get ? object.get(property) : object[property];
		};
	}

	var comparators = {
		eq: function (value, required) {
			return value === required;
		},
		'in': function(value, required) {
			// allow for a collection of data
			return arrayUtil.indexOf(required.data || required, value) > -1;
		},
		ne: function (value, required) {
			return value !== required;
		},
		lt: function (value, required) {
			return value < required;
		},
		lte: function (value, required) {
			return value <= required;
		},
		gt: function (value, required) {
			return value > required;
		},
		gte: function (value, required) {
			return value >= required;
		},
		match: function (value, required, object) {
			return required.test(value, object);
		},
		contains: function (value, required, object, key) {
			var collection = this;
			return arrayUtil.every(required.data || required, function (requiredValue) {
				if (typeof requiredValue === 'object' && requiredValue.type) {
					var comparator = collection._getFilterComparator(requiredValue.type);
					return arrayUtil.some(value, function (item) {
						return comparator.call(collection, item, requiredValue.args[1], object, key);
					});
				}
				return arrayUtil.indexOf(value, requiredValue) > -1;
			});
		}
	};

	return declare(null, {
		// summary:
		//		Mixin providing querier factories for core query types

		_createFilterQuerier: function (filter) {
			// create our matching filter function
			var queryAccessors = this.queryAccessors;
			var collection = this;
			var querier = getQuerier(filter);

			function getQuerier(filter) {
				var type = filter.type;
				var args = filter.args;
				var comparator = collection._getFilterComparator(type);
				if (comparator) {
					// it is a comparator
					var firstArg = args[0];
					var getProperty = makeGetter(firstArg, queryAccessors);
					var secondArg = args[1];
					if (secondArg && secondArg.fetchSync) {
						// if it is a collection, fetch the contents (for `in` and `contains` operators)
						secondArg = secondArg.fetchSync();
					}
					return function (object) {
						// get the value for the property and compare to expected value
						return comparator.call(collection, getProperty(object), secondArg, object, firstArg);
					};
				}
				switch (type) {
					case 'and': case 'or':
						for (var i = 0, l = args.length; i < l; i++) {
							// combine filters, using and or or
							var nextQuerier = getQuerier(args[i]);
							if (querier) {
								// combine the last querier with a new one
								querier = (function(a, b) {
									return type === 'and' ?
										function(object) {
											return a(object) && b(object);
										} :
										function(object) {
											return a(object) || b(object);

										};
								})(querier, nextQuerier);
							} else {
								querier = nextQuerier;
							}
						}
						return querier;
					case 'function':
						return args[0];
					case 'string':
						// named filter
						var filterFunction = collection[args[0]];
						if (!filterFunction) {
							throw new Error('No filter function ' + args[0] + ' was found in the collection');
						}
						return filterFunction;
					case undefined:
						return function () {
							return true;
						};
					default:
						throw new Error('Unknown filter operation "' + type + '"');
				}
			}
			return function (data) {
				return arrayUtil.filter(data, querier);
			};
		},

		_getFilterComparator: function (type) {
			// summary:
			//		Get the comparator for the specified type
			// returns: Function?

			return comparators[type] || this.inherited(arguments);
		},

		_createSelectQuerier: function (properties) {
			return function (data) {
				var l = properties.length;
				return arrayUtil.map(data, properties instanceof Array ?
					// array of properties
					function (object) {
						var selectedObject = {};
						for (var i = 0; i < l; i++) {
							var property = properties[i];
							selectedObject[property] = object[property];
						}
						return selectedObject;
					} :
					// single property
					function (object) {
						return object[properties];
					});
			};
		},

		_createSortQuerier: function (sorted) {
			var queryAccessors = this.queryAccessors;
			return function (data) {
				data = data.slice();
				data.sort(typeof sorted == 'function' ? sorted : function (a, b) {
					for (var i = 0; i < sorted.length; i++) {
						var comparison;
						var sorter = sorted[i];
						if (typeof sorter == 'function') {
							comparison = sorter(a, b);
						} else {
							var getProperty = sorter.get || (sorter.get = makeGetter(sorter.property, queryAccessors));
							var descending = sorter.descending;
							var aValue = getProperty(a);
							var bValue = getProperty(b);

							aValue != null && (aValue = aValue.valueOf());
							bValue != null && (bValue = bValue.valueOf());

							comparison = aValue === bValue
								? 0
								: (!!descending === (aValue === null || aValue > bValue && bValue !== null) ? -1 : 1);
						}

						if (comparison !== 0) {
							return comparison;
						}
					}
					return 0;
				});
				return data;
			};
		}
	});
});

},
'dstore/Tree':function(){
define([
	'dojo/_base/declare'
	/*=====, 'dstore/Store'=====*/
], function (declare /*=====, Store=====*/) {
	return declare(null, {
		constructor: function () {
			this.root = this;
		},

		mayHaveChildren: function (object) {
			// summary:
			//		Check if an object may have children
			// description:
			//		This method is useful for eliminating the possibility that an object may have children,
			//		allowing collection consumers to determine things like whether to render UI for child-expansion
			//		and whether a query is necessary to retrieve an object's children.
			// object:
			//		The potential parent
			// returns: boolean

			return 'hasChildren' in object ? object.hasChildren : true;
		},

		getRootCollection: function () {
			// summary:
			//		Get the collection of objects with no parents
			// returns: dstore/Store.Collection

			return this.root.filter({ parent: null });
		},

		getChildren: function (object) {
			// summary:
			//		Get a collection of the children of the provided parent object
			// object:
			//		The parent object
			// returns: dstore/Store.Collection

			return this.root.filter({ parent: this.getIdentity(object) });
		}
	});
});

},
'dstore/Cache':function(){
define([
	'dojo/_base/array',
	'dojo/when',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'./Store',
	'./Memory',
	'./QueryResults'
], function (arrayUtil, when, declare, lang, Store, Memory, QueryResults) {

	// module:
	//		dstore/Cache


	function cachingQuery(type) {
		// ensure querying creates a parallel caching store query
		return function () {
			var subCollection = this.inherited(arguments);
			var cachingCollection = this.cachingCollection || this.cachingStore;
			subCollection.cachingCollection = cachingCollection[type].apply(cachingCollection, arguments);
			subCollection.isValidFetchCache = this.canCacheQuery === true || this.canCacheQuery(type, arguments);
			return subCollection;
		};
	}

	function init (store) {
		if (!store.cachingStore) {
			store.cachingStore = new Memory();
		}

		store.cachingStore.Model = store.Model;
		store.cachingStore.idProperty = store.idProperty;
	}
	var CachePrototype = {
		cachingStore: null,
		constructor: function () {
			init(this);
		},
		canCacheQuery: function (method, args) {
			// summary:
			//		Indicates if a queried (filter, sort, etc.) collection should using caching
			return false;
		},
		isAvailableInCache: function () {
			// summary:
			//		Indicates if the collection's cachingCollection is a viable source
			//		for a fetch
			return (this.isValidFetchCache && (this.allLoaded || this.fetchRequest)) ||
					this._parent && this._parent.isAvailableInCache();
		},
		fetch: function () {
			return this._fetch(arguments);
		},
		fetchRange: function () {
			return this._fetch(arguments, true);
		},
		_fetch: function (args, isRange) {
			// if the data is available in the cache (via any parent), we use fetch from the caching store
			var cachingStore = this.cachingStore;
			var cachingCollection = this.cachingCollection || cachingStore;
			var store = this;
			var available = this.isAvailableInCache();
			if (available) {
				return new QueryResults(when(available, function () {
					// need to double check to make sure the flag hasn't been cleared
					// and we really have all data loaded
					if (store.isAvailableInCache()) {
						return isRange ?
							cachingCollection.fetchRange(args[0]) :
							cachingCollection.fetch();
					} else {
						return store.inherited(args);
					}
				}));
			}
			var results = this.fetchRequest = this.inherited(args);
			when(results, function (results) {
				var allLoaded = !isRange;
				store.fetchRequest = null;
				// store each object before calling the callback
				arrayUtil.forEach(results, function (object) {
					// store each object before calling the callback
					if (!store.isLoaded || store.isLoaded(object)) {
						cachingStore.put(object);
					} else {
						// if anything is not loaded, we can't consider them all loaded
						allLoaded = false;
					}
				});
				if (allLoaded) {
					store.allLoaded = true;
				}

				return results;
			});
			return results;
		},
		// TODO: for now, all forEach() calls delegate to fetch(), but that may be different
		// with IndexedDB, so we may need to intercept forEach as well (and hopefully not
		// double load elements.
		// isValidFetchCache: boolean
		//		This flag indicates if a previous fetch can be used as a cache for subsequent
		//		fetches (in this collection, or downstream).
		isValidFetchCache: false,
		get: function (id, directives) {
			var cachingStore = this.cachingStore;
			var masterGet = this.getInherited(arguments);
			var masterStore = this;
			// if everything is being loaded, we always wait for that to finish
			return when(this.fetchRequest, function () {
				return when(cachingStore.get(id), function (result) {
					if (result !== undefined) {
						return result;
					} else if (masterGet) {
						return when(masterGet.call(masterStore, id, directives), function (result) {
							if (result) {
								cachingStore.put(result, {id: id});
							}
							return result;
						});
					}
				});
			});
		},
		add: function (object, directives) {
			var cachingStore = this.cachingStore;
			return when(this.inherited(arguments), function (result) {
				// now put result in cache (note we don't do add, because add may have
				// called put() and already added it)
				var cachedPutResult =
					cachingStore.put(result && typeof result === 'object' ? result : object, directives);
				// the result from the add should be dictated by the master store and be unaffected by the cachingStore,
				// unless the master store doesn't implement add
				return result || cachedPutResult;
			});
		},
		put: function (object, directives) {
			// first remove from the cache, so it is empty until we get a response from the master store
			var cachingStore = this.cachingStore;
			cachingStore.remove((directives && directives.id) || this.getIdentity(object));
			return when(this.inherited(arguments), function (result) {
				// now put result in cache
				var cachedPutResult =
					cachingStore.put(result && typeof result === 'object' ? result : object, directives);
				// the result from the put should be dictated by the master store and be unaffected by the cachingStore,
				// unless the master store doesn't implement put
				return result || cachedPutResult;
			});
		},
		remove: function (id, directives) {
			var cachingStore = this.cachingStore;
			return when(this.inherited(arguments), function (result) {
				return when(cachingStore.remove(id, directives), function () {
					return result;
				});
			});
		},
		evict: function (id) {
			// summary:
			//		Evicts an object from the cache
			// any eviction means that we don't have everything loaded anymore
			this.allLoaded = false;
			return this.cachingStore.remove(id);
		},
		invalidate: function () {
			// summary:
			//		Invalidates this collection's cache as being a valid source of
			//		future fetches
			this.allLoaded = false;
		},
		_createSubCollection: function () {
			var subCollection = this.inherited(arguments);
			subCollection._parent = this;
			return subCollection;
		},

		sort: cachingQuery('sort'),
		filter: cachingQuery('filter'),
		select: cachingQuery('select'),

		_getQuerierFactory: function (type) {
			var cachingStore = this.cachingStore;
			return this.inherited(arguments) || lang.hitch(cachingStore, cachingStore._getQuerierFactory(type));
		}
	};
	var Cache = declare(null, CachePrototype);
	Cache.create = function (target, properties) {
		// create a delegate of an existing store with caching
		// functionality mixed in
		target = declare.safeMixin(lang.delegate(target), CachePrototype);
		declare.safeMixin(target, properties);
		// we need to initialize it since the constructor won't have been called
		init(target);
		return target;
	};
	return Cache;
});

},
'dstore/Trackable':function(){
define([
	'dojo/_base/lang',
	'dojo/_base/declare',
	'dojo/aspect',
	'dojo/when',
	'dojo/promise/all',
	'dojo/_base/array',
	'dojo/on'
	/*=====, './api/Store' =====*/
], function (lang, declare, aspect, when, whenAll, arrayUtil, on /*=====, Store =====*/) {

	// module:
	//		dstore/Trackable
	var revision = 0;

	function createRange(newStart, newEnd) {
		return {
			start: newStart,
			count: newEnd - newStart
		};
	}

	function registerRange(ranges, newStart, newEnd) {
		for (var i = ranges.length - 1; i >= 0; --i) {
			var existingRange = ranges[i],
				existingStart = existingRange.start,
				existingEnd = existingStart + existingRange.count;

			if (newStart > existingEnd) {
				// existing range completely precedes new range. we are done.
				ranges.splice(i + 1, 0, createRange(newStart, newEnd));
				return;
			} else if (newEnd >= existingStart) {
				// the ranges overlap and must be merged into a single range
				newStart = Math.min(newStart, existingStart);
				newEnd = Math.max(newEnd, existingEnd);
				ranges.splice(i, 1);
			}
		}

		ranges.unshift(createRange(newStart, newEnd));
	}

	function unregisterRange(ranges, start, end) {
		for (var i = 0, range; (range = ranges[i]); ++i) {
			var existingStart = range.start,
				existingEnd = existingStart + range.count;

			if (start <= existingStart) {
				if (end >= existingEnd) {
					// The existing range is within the forgotten range
					ranges.splice(i, 1);
				} else {
					// The forgotten range overlaps the beginning of the existing range
					range.start = end;
					range.count = existingEnd - range.start;

					// Since the forgotten range ends before the existing range,
					// there are no more ranges to update, and we are done
					return;
				}
			} else if (start < existingEnd) {
				if (end > existingStart) {
					// The forgotten range is within the existing range
					ranges.splice(i, 1, createRange(existingStart, start), createRange(end, existingEnd));

					// We are done because the existing range bounded the forgotten range
					return;
				} else {
					// The forgotten range overlaps the end of the existing range
					range.count = start - range.start;
				}
			}
		}
	}

	var trackablePrototype = {
		track: function () {
			var store = this.store || this;

			// monitor for updates by listening to these methods
			var handles = [];
			var eventTypes = {add: 1, update: 1, 'delete': 1};
			// register to listen for updates
			for (var type in eventTypes) {
				handles.push(
					this.on(type, (function (type) {
						return function (event) {
							notify(type, event);
						};
					})(type))
				);
			}

			function makeFetch() {
				return function () {
					var self = this;
					var fetchResults = this.inherited(arguments);
					when(fetchResults, function (results) {
						results = self._results = results.slice();
						if (self._partialResults) {
							// clean this up, as we don't need this anymore
							self._partialResults = null;
						}
						self._ranges = [];
						registerRange(self._ranges, 0, results.length);
					});
					return fetchResults;
				};
			}
			function makeFetchRange() {
				return function (kwArgs) {
					var self = this,
						start = kwArgs.start,
						end = kwArgs.end,
						fetchResults = this.inherited(arguments);
					// only use this if we don't have all the data
					if (!this._results) {
						when(fetchResults, function (results) {
							return when(results.totalLength, function (totalLength) {
								var partialResults = self._partialResults || (self._partialResults = []);
								end = Math.min(end, start + results.length);

								partialResults.length = totalLength;

								// copy the new ranged data into the parent partial data set
								var spliceArgs = [ start, end - start ].concat(results);
								partialResults.splice.apply(partialResults, spliceArgs);
								registerRange(self._ranges, start, end);

								return results;
							});
						});
					}
					return fetchResults;
				};
			}

			// delegate rather than call _createSubCollection because we are not ultimately creating
			// a new collection, just decorating an existing collection with item index tracking.
			// If we use _createSubCollection, it will return a new collection that may exclude
			// important, defining properties from the tracked collection.
			var observed = declare.safeMixin(lang.delegate(this), {
				_ranges: [],

				fetch: makeFetch(),
				fetchRange: makeFetchRange(),

				releaseRange: function (start, end) {
					if (this._partialResults) {
						unregisterRange(this._ranges, start, end);

						for (var i = start; i < end; ++i) {
							delete this._partialResults[i];
						}
					}
				},

				on: function (type, listener) {
					var self = this,
						inheritedOn = this.getInherited(arguments);
					return on.parse(observed, type, listener, function (target, type) {
						return type in eventTypes ?
							aspect.after(observed, 'on_tracked' + type, listener, true) :
							inheritedOn.call(self, type, listener);
					});
				},

				tracking: {
					remove: function () {
						while (handles.length > 0) {
							handles.pop().remove();
						}

						this.remove = function () {};
					}
				},
				// make sure track isn't called twice
				track: null
			});
			if (this.fetchSync) {
				// only add these if we extending a sync-capable store
				declare.safeMixin(observed, {
					fetchSync: makeFetch(),
					fetchRangeSync: makeFetchRange()
				});

				// we take the presence of fetchSync to indicate that the results can be
				// retrieved cheaply, and then we can just automatically fetch and start
				// tracking results
				observed.fetchSync();
			}

			// Create a function that applies all queriers in the query log
			// in order to determine whether a new or updated item belongs
			// in the results and at what position.
			var queryExecutor;
			arrayUtil.forEach(this.queryLog, function (entry) {
				var existingQuerier = queryExecutor,
					querier = entry.querier;

				if (querier) {
					queryExecutor = existingQuerier
						? function (data) { return querier(existingQuerier(data)); }
						: querier;
				}
			});

			var defaultEventProps = {
					'add': { index: undefined },
					'update': { previousIndex: undefined, index: undefined },
					'delete': { previousIndex: undefined }
				},
				findObject = function (data, id, start, end) {
					start = start !== undefined ? start : 0;
					end = end !== undefined ? end : data.length;
					for (var i = start; i < end; ++i) {
						if (store.getIdentity(data[i]) === id) {
							return i;
						}
					}
					return -1;
				};

			function notify(type, event) {

				revision++;
				var target = event.target;
				event = lang.delegate(event, defaultEventProps[type]);

				when(observed._results || observed._partialResults, function (resultsArray) {
					/* jshint maxcomplexity: 32 */

					function emitEvent() {
						// TODO: Eventually we will want to aggregate all the listener events
						// in an event turn, but we will wait until we have a reliable, performant queueing
						// mechanism for this (besides setTimeout)
						var method = observed['on_tracked' + type];
						method && method.call(observed, event);
					}

					if (!resultsArray) {
						// without data, we have no way to determine the indices effected by the change,
						// so just pass along the event and return.
						emitEvent();
						return;
					}

					var i, j, l, ranges = observed._ranges, range;
					/*if(++queryRevision != revision){
						throw new Error('Query is out of date, you must observe() the' +
						' query prior to any data modifications');
					}*/

					var targetId = 'id' in event ? event.id : store.getIdentity(target);
					var removedFrom = -1,
						removalRangeIndex = -1,
						insertedInto = -1,
						insertionRangeIndex = -1;
					if (type === 'delete' || type === 'update') {
						// remove the old one
						for (i = 0; removedFrom === -1 && i < ranges.length; ++i) {
							range = ranges[i];
							for (j = range.start, l = j + range.count; j < l; ++j) {
								var object = resultsArray[j];
								// often ids can be converted strings (if they are used as keys in objects),
								// so we do a coercive equality check
								/* jshint eqeqeq: false */
								if (store.getIdentity(object) == targetId) {
									removedFrom = event.previousIndex = j;
									removalRangeIndex = i;
									resultsArray.splice(removedFrom, 1);

									range.count--;
									for (j = i + 1; j < ranges.length; ++j) {
										ranges[j].start--;
									}

									break;
								}
							}
						}
					}

					if (type === 'add' || type === 'update') {
						if (queryExecutor) {
							// with a queryExecutor, we can determine the correct sorted index for the change

							if (queryExecutor([target]).length) {
								var begin = 0,
									end = ranges.length - 1,
									sampleArray,
									candidateIndex = -1,
									sortedIndex,
									adjustedIndex;
								while (begin <= end && insertedInto === -1) {
									// doing a binary search for the containing range
									i = begin + Math.round((end - begin) / 2);
									range = ranges[i];

									sampleArray = resultsArray.slice(range.start, range.start + range.count);

									if ('beforeId' in event) {
										candidateIndex = event.beforeId === null
											? sampleArray.length
											: findObject(sampleArray, event.beforeId);
									}

									if (candidateIndex === -1) {
										// If the original index came from this range, put back in the original slot
										// so it doesn't move unless it needs to (relying on a stable sort below)
										if (removedFrom >= Math.max(0, range.start - 1)
											&& removedFrom <= (range.start + range.count)) {
											candidateIndex = removedFrom;
										} else {
											candidateIndex = store.defaultNewToStart ? 0 : sampleArray.length;
										}
									}
									sampleArray.splice(candidateIndex, 0, target);

									sortedIndex = arrayUtil.indexOf(queryExecutor(sampleArray), target);
									adjustedIndex = range.start + sortedIndex;

									if (sortedIndex === 0 && range.start !== 0) {
										end = i - 1;
									} else if (sortedIndex >= (sampleArray.length - 1) &&
											adjustedIndex < resultsArray.length) {
										begin = i + 1;
									} else {
										insertedInto = adjustedIndex;
										insertionRangeIndex = i;
									}
								}
								if (insertedInto === -1 && begin > 0 && begin < ranges.length) {
									var betweenRanges = true;
								}
							}
						} else {
							// we don't have a queryExecutor, so we can't provide any information
							// about where it was inserted or moved to. If it is an update, we leave
							// its position alone. otherwise, we at least indicate a new object

							var range,
								possibleRangeIndex = -1;
							if ('beforeId' in event) {
								if (event.beforeId === null) {
									insertedInto = resultsArray.length;
									possibleRangeIndex = ranges.length - 1;
								} else {
									for (i = 0, l = ranges.length; insertionRangeIndex === -1 && i < l; ++i) {
										range = ranges[i];

										insertedInto = findObject(
											resultsArray,
											event.beforeId,
											range.start,
											range.start + range.count
										);

										if (insertedInto !== -1) {
											insertionRangeIndex = i;
										}
									}
								}
							} else {
								if (type === 'update') {
									insertedInto = removedFrom;
									insertionRangeIndex = removalRangeIndex;
								} else {
									if (store.defaultNewToStart) {
										insertedInto = 0;
										possibleRangeIndex = 0;
									} else {
										// default to the bottom
										insertedInto = resultsArray.length;
										possibleRangeIndex = ranges.length - 1;
									}
								}
							}

							if (possibleRangeIndex !== -1 && insertionRangeIndex === -1) {
								range = ranges[possibleRangeIndex];
								if (range && range.start <= insertedInto
									&& insertedInto <= (range.start + range.count)) {
									insertionRangeIndex = possibleRangeIndex;
								}
							}
						}

						// an item only truly has a known index if it is in a known range
						if (insertedInto > -1 && insertionRangeIndex > -1) {
							event.index = insertedInto;
							resultsArray.splice(insertedInto, 0, target);

							// update the count and start of the appropriate ranges
							ranges[insertionRangeIndex].count++;
							for (i = insertionRangeIndex + 1; i < ranges.length; ++i) {
								ranges[i].start++;
							}
						} else if (betweenRanges) {
							// the begin index will be after the inserted item, and is
							// where we can begin incrementing start values
							event.beforeIndex = ranges[begin].start;
							for (i = begin; i < ranges.length; ++i) {
								ranges[i].start++;
							}
						}
					}
					// update the total
					event.totalLength = resultsArray.length;

					emitEvent();
				});
			}

			return observed;
		}
	};

	var Trackable =  declare(null, trackablePrototype);

	Trackable.create = function (target, properties) {
		// create a delegate of an existing store with trackability functionality mixed in
		target = declare.safeMixin(lang.delegate(target), trackablePrototype);
		declare.safeMixin(target, properties);
		return target;
	};
	return Trackable;
});

},
'dstore/legacy/DstoreAdapter':function(){
define([
	'dojo/_base/declare',
	'dojo/_base/array',
	'dojo/store/util/QueryResults'
	/*=====, "dstore/api/Store" =====*/
], function (declare, arrayUtil, QueryResults /*=====, Store =====*/) {
// module:
//		An adapter mixin that makes a dstore store object look like a legacy Dojo object store.

	function passthrough(data) {
		return data;
	}

	// No base class, but for purposes of documentation, the base class is dstore/api/Store
	var base = null;
	/*===== base = Store; =====*/

	var adapterPrototype = {

		// store:
		//		The dstore store that is wrapped as a Dojo object store
		store: null,

		constructor: function (store) {
			this.store = store;

			if (store._getQuerierFactory('filter') || store._getQuerierFactory('sort')) {
				this.queryEngine = function (query, options) {
					options = options || {};

					var filterQuerierFactory = store._getQuerierFactory('filter');
					var filter = filterQuerierFactory ? filterQuerierFactory(query) : passthrough;

					var sortQuerierFactory = store._getQuerierFactory('sort');
					var sort = passthrough;
					if (sortQuerierFactory) {
						sort = sortQuerierFactory(arrayUtil.map(options.sort, function (criteria) {
							return {
								property: criteria.attribute,
								descending: criteria.descending
							};
						}));
					}

					var range = passthrough;
					if (!isNaN(options.start) || !isNaN(options.count)) {
						range = function (data) {
							var start = options.start || 0,
								count = options.count || Infinity;

							var results = data.slice(start, start + count);
							results.total = data.length;
							return results;
						};
					}

					return function (data) {
						return range(sort(filter(data)));
					};
				};
			}
			var objectStore = this;
			// we call notify on events to mimic the old dojo/store/Trackable
			store.on('add,update,delete', function (event) {
				var type = event.type;
				var target = event.target;
				objectStore.notify(
					(type === 'add' || type === 'update') ? target : undefined,
					(type === 'delete' || type === 'update') ?
						('id' in event ? event.id : store.getIdentity(target)) : undefined);
			});
		},
		labelAttr:'title',
		getLabel:function(item){
			return this.store.getLabel(item);
		},
		query: function (query, options) {
			// summary:
			//		Queries the store for objects. This does not alter the store, but returns a
			//		set of data from the store.
			// query: String|Object|Function
			//		The query to use for retrieving objects from the store.
			// options: dstore/api/Store.QueryOptions
			//		The optional arguments to apply to the resultset.
			// returns: dstore/api/Store.QueryResults
			//		The results of the query, extended with iterative methods.
			//
			// example:
			//		Given the following store:
			//
			//	...find all items where "prime" is true:
			//
			//	|	store.query({ prime: true }).forEach(function(object){
			//	|		// handle each object
			//	|	});
			options = options || {};

			var results = this.store.filter(query);
			var queryResults;
			var tracked;
			var total;

			// Apply sorting
			var sort = options.sort;
			if (sort) {
				if (Object.prototype.toString.call(sort) === '[object Array]') {
					var sortOptions;
					while ((sortOptions = sort.pop())) {
						results = results.sort(sortOptions.attribute, sortOptions.descending);
					}
				} else {
					results = results.sort(sort);
				}
			}

			if (results.track && !results.tracking) {
				// if it is trackable, always track, so that observe can
				// work properly.
				results = results.track();
				tracked = true;
			}
			if ('start' in options) {
				// Apply a range
				var start = options.start || 0;
				// object stores support sync results, so try that if available
				queryResults = results[results.fetchRangeSync ? 'fetchRangeSync' : 'fetchRange']({
					start: start,
					end: options.count ? (start + options.count) : Infinity
				});
			}
			queryResults = queryResults || results[results.fetchSync ? 'fetchSync' : 'fetch']();
			total = queryResults.totalLength;
			queryResults = new QueryResults(queryResults);
			queryResults.total = total;
			queryResults.observe = function (callback, includeObjectUpdates) {
				// translate observe to event listeners
				function convertUndefined(value) {
					if (value === undefined && tracked) {
						return -1;
					}
					return value;
				}
				var addHandle = results.on('add', function (event) {
					callback(event.target, -1, convertUndefined(event.index));
				});
				var updateHandle = results.on('update', function (event) {
					if (includeObjectUpdates || event.previousIndex !== event.index || !isFinite(event.index)) {
						callback(event.target, convertUndefined(event.previousIndex), convertUndefined(event.index));
					}
				});
				var removeHandle = results.on('delete', function (event) {
					callback(event.target, convertUndefined(event.previousIndex), -1);
				});
				var handle = {
					remove: function () {
						addHandle.remove();
						updateHandle.remove();
						removeHandle.remove();
					}
				};
				handle.cancel = handle.remove;
				return handle;
			};
			return queryResults;
		},
		notify: function () {

		}
	};

	var delegatedMethods = [ 'get', 'put', 'add', 'remove', 'getIdentity' ];
	arrayUtil.forEach(delegatedMethods, function (methodName) {
		adapterPrototype[methodName] = function () {
			var store = this.store;
			// try sync first, since dojo object stores support that directly
			return (store[methodName + 'Sync'] || store[methodName]).apply(store, arguments);
		};
	});

	return declare(base, adapterPrototype);
});

}}});
