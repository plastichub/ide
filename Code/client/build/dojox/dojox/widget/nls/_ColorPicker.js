//>>built
define("dojox/widget/nls/_ColorPicker",[],function(){return{redLabel:"r",greenLabel:"g",blueLabel:"b",hueLabel:"h",saturationLabel:"s",valueLabel:"v",degLabel:"\u00b0",hexLabel:"hex",huePickerTitle:"Hue Selector",saturationPickerTitle:"Saturation Selector"}});
//# sourceMappingURL=_ColorPicker.js.map