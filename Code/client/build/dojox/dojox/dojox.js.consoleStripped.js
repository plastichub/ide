require({cache:{
'dojox/main':function(){
define([
	"dojo/_base/kernel",
	"dijit/dijit",
	"dojox/html/entities",
	"dojox/html/ellipsis",
	"dojox/html/_base",
	"dojox/widget/ColorPicker"
], function(dojo) {
	return dojo.dojox;
});
},
'dojox/html/entities':function(){
define(["dojo/_base/lang"], function(lang) {
	// dojox.html.entities.html [public] Array
	//		Entity characters for HTML, represented as an array of
	//		character code, entity name (minus & and ; wrapping.
	//		The function wrapper is to fix global leking with the build tools.
	var dhe = lang.getObject("dojox.html.entities",true);	
	
	var _applyEncodingMap = function(str, map){
		// summary:
		//		Private internal function for performing encoding of entity characters.
		// tags:
		//		private
	
		// Check to see if we have genned and cached a regexp for this map yet
		// If we have, use it, if not, gen it, cache, then use.
		var mapper, regexp;
		if(map._encCache &&
			map._encCache.regexp &&
			map._encCache.mapper &&
			map.length == map._encCache.length){
			mapper = map._encCache.mapper;
			regexp = map._encCache.regexp;
		}else{
			mapper = {};
			regexp = ["["];
			var i;
			for(i = 0; i < map.length; i++){
				mapper[map[i][0]] = "&" + map[i][1] + ";";
				regexp.push(map[i][0]);
			}
			regexp.push("]");
			regexp = new RegExp(regexp.join(""), "g");
			map._encCache = {
				mapper: mapper,
				regexp: regexp,
				length: map.length
			};
		}
		str = str.replace(regexp, function(c){
			return mapper[c];
		});
		return str;
	};
	
	var _applyDecodingMap = function(str, map){
		// summary:
		//		Private internal function for performing decoding of entity characters.
		// tags:
		//		private
		var mapper, regexp;
		if(map._decCache &&
			map._decCache.regexp &&
			map._decCache.mapper &&
			map.length == map._decCache.length){
			mapper = map._decCache.mapper;
			regexp = map._decCache.regexp;
		}else{
			mapper = {};
			regexp = ["("];
			var i;
			for(i = 0; i < map.length; i++){
				var e = "&" + map[i][1] + ";";
				if(i){regexp.push("|");}
				mapper[e] = map[i][0];
				regexp.push(e);
			}
			regexp.push(")");
			regexp = new RegExp(regexp.join(""), "g");
			map._decCache = {
				mapper: mapper,
				regexp: regexp,
				length: map.length
			};
		}
		str = str.replace(regexp, function(c){
			return mapper[c];
		});
		return str;
	};

	dhe.html = [
		["\u0026","amp"], ["\u0022","quot"],["\u003C","lt"], ["\u003E","gt"],
		["\u00A0","nbsp"]
	];
	
	// dojox.html.entities.latin [public] Array
	//		Entity characters for Latin characters and similar, represented as an array of
	//		character code, entity name (minus & and ; wrapping.
	dhe.latin = [
		["\u00A1","iexcl"],["\u00A2","cent"],["\u00A3","pound"],["\u20AC","euro"],
		["\u00A4","curren"],["\u00A5","yen"],["\u00A6","brvbar"],["\u00A7","sect"],
		["\u00A8","uml"],["\u00A9","copy"],["\u00AA","ordf"],["\u00AB","laquo"],
		["\u00AC","not"],["\u00AD","shy"],["\u00AE","reg"],["\u00AF","macr"],
		["\u00B0","deg"],["\u00B1","plusmn"],["\u00B2","sup2"],["\u00B3","sup3"],
		["\u00B4","acute"],["\u00B5","micro"],["\u00B6","para"],["\u00B7","middot"],
		["\u00B8","cedil"],["\u00B9","sup1"],["\u00BA","ordm"],["\u00BB","raquo"],
		["\u00BC","frac14"],["\u00BD","frac12"],["\u00BE","frac34"],["\u00BF","iquest"],
		["\u00C0","Agrave"],["\u00C1","Aacute"],["\u00C2","Acirc"],["\u00C3","Atilde"],
		["\u00C4","Auml"],["\u00C5","Aring"],["\u00C6","AElig"],["\u00C7","Ccedil"],
		["\u00C8","Egrave"],["\u00C9","Eacute"],["\u00CA","Ecirc"],["\u00CB","Euml"],
		["\u00CC","Igrave"],["\u00CD","Iacute"],["\u00CE","Icirc"],["\u00CF","Iuml"],
		["\u00D0","ETH"],["\u00D1","Ntilde"],["\u00D2","Ograve"],["\u00D3","Oacute"],
		["\u00D4","Ocirc"],["\u00D5","Otilde"],["\u00D6","Ouml"],["\u00D7","times"],
		["\u00D8","Oslash"],["\u00D9","Ugrave"],["\u00DA","Uacute"],["\u00DB","Ucirc"],
		["\u00DC","Uuml"],["\u00DD","Yacute"],["\u00DE","THORN"],["\u00DF","szlig"],
		["\u00E0","agrave"],["\u00E1","aacute"],["\u00E2","acirc"],["\u00E3","atilde"],
		["\u00E4","auml"],["\u00E5","aring"],["\u00E6","aelig"],["\u00E7","ccedil"],
		["\u00E8","egrave"],["\u00E9","eacute"],["\u00EA","ecirc"],["\u00EB","euml"],
		["\u00EC","igrave"],["\u00ED","iacute"],["\u00EE","icirc"],["\u00EF","iuml"],
		["\u00F0","eth"],["\u00F1","ntilde"],["\u00F2","ograve"],["\u00F3","oacute"],
		["\u00F4","ocirc"],["\u00F5","otilde"],["\u00F6","ouml"],["\u00F7","divide"],
		["\u00F8","oslash"],["\u00F9","ugrave"],["\u00FA","uacute"],["\u00FB","ucirc"],
		["\u00FC","uuml"],["\u00FD","yacute"],["\u00FE","thorn"],["\u00FF","yuml"],
		["\u0192","fnof"],["\u0391","Alpha"],["\u0392","Beta"],["\u0393","Gamma"],
		["\u0394","Delta"],["\u0395","Epsilon"],["\u0396","Zeta"],["\u0397","Eta"],
		["\u0398","Theta"], ["\u0399","Iota"],["\u039A","Kappa"],["\u039B","Lambda"],
		["\u039C","Mu"],["\u039D","Nu"],["\u039E","Xi"],["\u039F","Omicron"],
		["\u03A0","Pi"],["\u03A1","Rho"],["\u03A3","Sigma"],["\u03A4","Tau"],
		["\u03A5","Upsilon"],["\u03A6","Phi"],["\u03A7","Chi"],["\u03A8","Psi"],
		["\u03A9","Omega"],["\u03B1","alpha"],["\u03B2","beta"],["\u03B3","gamma"],
		["\u03B4","delta"],["\u03B5","epsilon"],["\u03B6","zeta"],["\u03B7","eta"],
		["\u03B8","theta"],["\u03B9","iota"],["\u03BA","kappa"],["\u03BB","lambda"],
		["\u03BC","mu"],["\u03BD","nu"],["\u03BE","xi"],["\u03BF","omicron"],
		["\u03C0","pi"],["\u03C1","rho"],["\u03C2","sigmaf"],["\u03C3","sigma"],
		["\u03C4","tau"],["\u03C5","upsilon"],["\u03C6","phi"],["\u03C7","chi"],
		["\u03C8","psi"],["\u03C9","omega"],["\u03D1","thetasym"],["\u03D2","upsih"],
		["\u03D6","piv"],["\u2022","bull"],["\u2026","hellip"],["\u2032","prime"],
		["\u2033","Prime"],["\u203E","oline"],["\u2044","frasl"],["\u2118","weierp"],
		["\u2111","image"],["\u211C","real"],["\u2122","trade"],["\u2135","alefsym"],
		["\u2190","larr"],["\u2191","uarr"],["\u2192","rarr"],["\u2193","darr"],
		["\u2194","harr"],["\u21B5","crarr"],["\u21D0","lArr"],["\u21D1","uArr"],
		["\u21D2","rArr"],["\u21D3","dArr"],["\u21D4","hArr"],["\u2200","forall"],
		["\u2202","part"],["\u2203","exist"],["\u2205","empty"],["\u2207","nabla"],
		["\u2208","isin"],["\u2209","notin"],["\u220B","ni"],["\u220F","prod"],
		["\u2211","sum"],["\u2212","minus"],["\u2217","lowast"],["\u221A","radic"],
		["\u221D","prop"],["\u221E","infin"],["\u2220","ang"],["\u2227","and"],
		["\u2228","or"],["\u2229","cap"],["\u222A","cup"],["\u222B","int"],
		["\u2234","there4"],["\u223C","sim"],["\u2245","cong"],["\u2248","asymp"],
		["\u2260","ne"],["\u2261","equiv"],["\u2264","le"],["\u2265","ge"],
		["\u2282","sub"],["\u2283","sup"],["\u2284","nsub"],["\u2286","sube"],
		["\u2287","supe"],["\u2295","oplus"],["\u2297","otimes"],["\u22A5","perp"],
		["\u22C5","sdot"],["\u2308","lceil"],["\u2309","rceil"],["\u230A","lfloor"],
		["\u230B","rfloor"],["\u2329","lang"],["\u232A","rang"],["\u25CA","loz"],
		["\u2660","spades"],["\u2663","clubs"],["\u2665","hearts"],["\u2666","diams"],
		["\u0152","OElig"],["\u0153","oelig"],["\u0160","Scaron"],["\u0161","scaron"],
		["\u0178","Yuml"],["\u02C6","circ"],["\u02DC","tilde"],["\u2002","ensp"],
		["\u2003","emsp"],["\u2009","thinsp"],["\u200C","zwnj"],["\u200D","zwj"],
		["\u200E","lrm"],["\u200F","rlm"],["\u2013","ndash"],["\u2014","mdash"],
		["\u2018","lsquo"],["\u2019","rsquo"],["\u201A","sbquo"],["\u201C","ldquo"],
		["\u201D","rdquo"],["\u201E","bdquo"],["\u2020","dagger"],["\u2021","Dagger"],
		["\u2030","permil"],["\u2039","lsaquo"],["\u203A","rsaquo"]
	];
	
	dhe.encode = function(str/*string*/, m /*array?*/){
		// summary:
		//		Function to obtain an entity encoding for a specified character
		// str:
		//		The string to process for possible entity encoding.
		// m:
		//		An optional list of character to entity name mappings (array of
		//		arrays).  If not provided, it uses the and Latin entities as the
		//		set to map and escape.
		// tags:
		//		public
		if(str){
			if(!m){
				// Apply the basic mappings.  HTML should always come first when decoding
				// as well.
				str = _applyEncodingMap(str, dhe.html);
				str = _applyEncodingMap(str, dhe.latin);
	
			}else{
				str = _applyEncodingMap(str, m);
			}
		}
		return str;
	};
	
	dhe.decode = function(str/*string*/, m /*array?*/){
		// summary:
		//		Function to obtain an entity encoding for a specified character
		// str:
		//		The string to process for possible entity encoding to decode.
		// m:
		//		An optional list of character to entity name mappings (array of
		//		arrays).  If not provided, it uses the HTML and Latin entities as the
		//		set to map and decode.
		// tags:
		//		public
		if(str){
			if(!m){
				// Apply the basic mappings.  HTML should always come first when decoding
				// as well.
				str = _applyDecodingMap(str, dhe.html);
				str = _applyDecodingMap(str, dhe.latin);
	
			}else{
				str = _applyDecodingMap(str, m);
			}
		}
		return str;
	};
	return dhe;
});


},
'dojox/html/ellipsis':function(){
define("dojox/html/ellipsis",["dojo/_base/kernel", "dojo/_base/lang", "dojo/_base/array", "dojo/_base/Color", "dojo/colors"], function(d){
	/*=====
	return {
		// summary:
		//		offers cross-browser support for text-overflow: ellipsis
		// description:
		//		Add "dojoxEllipsis" on any node that you want to ellipsis-ize. In order to function properly,
		//		the node with the dojoxEllipsis class set on it should be a child of a node with a defined width.
		//		It should also be a block-level element (i.e. `<div>`) - it will not work on td elements.
		//		NOTE: When using the dojoxEllipsis class within tables, the table needs to have the table-layout: fixed style
	};
	=====*/
	
	if(d.isFF < 7){ //TODO: feature detect text-overflow in computed style?
		// The delay (in ms) to wait so that we don't keep querying when many
		// changes happen at once - set config "dojoxFFEllipsisDelay" if you
		// want a different value
		var delay = 1;
		if("dojoxFFEllipsisDelay" in d.config){
			delay = Number(d.config.dojoxFFEllipsisDelay);
			if(isNaN(delay)){
				delay = 1;
			}
		}
		try{
			var createXULEllipsis = (function(){
				// Create our stub XUL elements for cloning later
				// NOTE: this no longer works as of FF 4.0:
				// https://developer.mozilla.org/En/Firefox_4_for_developers#Remote_XUL_support_removed
				var sNS = 'http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul';
				var xml = document.createElementNS(sNS, 'window');
				var label = document.createElementNS(sNS, 'description');
				label.setAttribute('crop', 'end');
				xml.appendChild(label);

				return function(/* Node */ n){
					// Summary:
					//		Given a node, it creates the XUL and sets its
					//		content so that it will have an ellipsis
					var x = xml.cloneNode(true);
					x.firstChild.setAttribute('value', n.textContent);
					n.innerHTML = '';
					n.appendChild(x);
				};
			})();
		}catch(e){}
		
		// Create our iframe elements for cloning later
		var create = d.create;
		var dd = d.doc;
		var dp = d.place;
		var iFrame = create("iframe", {className: "dojoxEllipsisIFrame",
					src: "javascript:'<html><head><script>if(\"loadFirebugConsole\" in window){window.loadFirebugConsole();}</script></head><body></body></html>'", style: {display: "none"}});
		var rollRange = function(/* W3C Range */ r, /* int? */ cnt){
			// summary:
			//		Rolls the given range back one character from the end
			// r: W3C Range
			//		The range to roll back
			// cnt: int?
			//		An optional number of times to roll back (defaults 1)
			if(r.collapsed){
				// Do nothing - we are already collapsed
				return;
			}
			if(cnt > 0){
				do{
					rollRange(r);
					cnt--;
				}while(cnt);
				return;
			}
			if(r.endContainer.nodeType == 3 && r.endOffset > 0){
				r.setEnd(r.endContainer, r.endOffset - 1);
			}else if(r.endContainer.nodeType == 3){
				r.setEndBefore(r.endContainer);
				rollRange(r);
				return;
			}else if(r.endOffset && r.endContainer.childNodes.length >= r.endOffset){
				var nCont = r.endContainer.childNodes[r.endOffset - 1];
				if(nCont.nodeType == 3){
					r.setEnd(nCont, nCont.length - 1);
				}else if(nCont.childNodes.length){
					r.setEnd(nCont, nCont.childNodes.length);
					rollRange(r);
					return;
				}else{
					r.setEndBefore(nCont);
					rollRange(r);
					return;
				}
			}else{
				r.setEndBefore(r.endContainer);
				rollRange(r);
				return;
			}
		};
		var createIFrameEllipsis = function(/* Node */ n){
			// summary:
			//		Given a node, it creates an iframe and and ellipsis div and
			//		sets up the connections so that they will work correctly.
			//		This function is used when createXULEllipsis is not able
			//		to be used (because there is markup within the node) - it's
			//		a bit slower, but does the trick
			var c = create("div", {className: "dojoxEllipsisContainer"});
			var e = create("div", {className: "dojoxEllipsisShown", style: {display: "none"}});
			n.parentNode.replaceChild(c, n);
			c.appendChild(n);
			c.appendChild(e);
			var i = iFrame.cloneNode(true);
			var ns = n.style;
			var es = e.style;
			var ranges;
			var resizeNode = function(){
				ns.display = "";
				es.display = "none";
				if(n.scrollWidth <= n.offsetWidth){ return; }
				var r = dd.createRange();
				r.selectNodeContents(n);
				ns.display = "none";
				es.display = "";
				var done = false;
				do{
					var numRolls = 1;
					dp(r.cloneContents(), e, "only");
					var sw = e.scrollWidth, ow = e.offsetWidth;
					done = (sw <= ow);
					var pct = (1 - ((ow * 1) / sw));
					if(pct > 0){
						numRolls = Math.max(Math.round(e.textContent.length * pct) - 1, 1);
					}
					rollRange(r, numRolls);
				}while(!r.collapsed && !done);
			};
			i.onload = function(){
				i.contentWindow.onresize = resizeNode;
				resizeNode();
			};
			c.appendChild(i);
		};

		// Function for updating the ellipsis
		var hc = d.hasClass;
		var doc = d.doc;
		var s, fn, opt;
		if(doc.querySelectorAll){
			s = doc;
			fn = "querySelectorAll";
			opt = ".dojoxEllipsis";
		}else if(doc.getElementsByClassName){
			s = doc;
			fn = "getElementsByClassName";
			opt = "dojoxEllipsis";
		}else{
			s = d;
			fn = "query";
			opt = ".dojoxEllipsis";
		}
		fx = function(){
			d.forEach(s[fn].apply(s, [opt]), function(n){
				if(!n || n._djx_ellipsis_done){ return; }
				n._djx_ellipsis_done = true;
				if(createXULEllipsis && n.textContent == n.innerHTML && !hc(n, "dojoxEllipsisSelectable")){
					// We can do the faster XUL version, instead of calculating
					createXULEllipsis(n);
				}else{
					createIFrameEllipsis(n);
				}
			});
		};
		
		d.addOnLoad(function(){
			// Apply our initial stuff
			var t = null;
			var c = null;
			var connFx = function(){
				if(c){
					// disconnect us - so we don't fire anymore
					d.disconnect(c);
					c = null;
				}
				if(t){ clearTimeout(t); }
				t = setTimeout(function(){
					t = null;
					fx();
					// Connect to the modified function so that we can catch
					// our next change
					c = d.connect(d.body(), "DOMSubtreeModified", connFx);
				}, delay);
			};
			connFx();
		});
	}
});

},
'dojox/html/_base':function(){
define([
	"dojo/_base/declare",
	"dojo/Deferred",
	"dojo/dom-construct",
	"dojo/html",
	"dojo/_base/kernel",
	"dojo/_base/lang",
	"dojo/ready",
	"dojo/_base/sniff",
	"dojo/_base/url",
	"dojo/_base/xhr",
	"dojo/when",
	"dojo/_base/window"
], function(declare, Deferred, domConstruct, htmlUtil, kernel, lang, ready, has, _Url, xhrUtil, when, windowUtil){

/*
	Status: don't know where this will all live exactly
	Need to pull in the implementation of the various helper methods
	Some can be static method, others maybe methods of the ContentSetter (?)

	Gut the ContentPane, replace its _setContent with our own call to dojox.html.set()


*/
	var html = kernel.getObject("dojox.html", true);

	if(has("ie")){
		var alphaImageLoader = /(AlphaImageLoader\([^)]*?src=(['"]))(?![a-z]+:|\/)([^\r\n;}]+?)(\2[^)]*\)\s*[;}]?)/g;
	}

	// css at-rules must be set before any css declarations according to CSS spec
	// match:
	// @import 'http://dojotoolkit.org/dojo.css';
	// @import 'you/never/thought/' print;
	// @import url("it/would/work") tv, screen;
	// @import url(/did/you/now.css);
	// but not:
	// @namespace dojo "http://dojotoolkit.org/dojo.css"; /* namespace URL should always be a absolute URI */
	// @charset 'utf-8';
	// @media print{ #menuRoot {display:none;} }

	// we adjust all paths that dont start on '/' or contains ':'
	//(?![a-z]+:|\/)

	var cssPaths = /(?:(?:@import\s*(['"])(?![a-z]+:|\/)([^\r\n;{]+?)\1)|url\(\s*(['"]?)(?![a-z]+:|\/)([^\r\n;]+?)\3\s*\))([a-z, \s]*[;}]?)/g;

	var adjustCssPaths = html._adjustCssPaths = function(cssUrl, cssText){
		// summary:
		//		adjusts relative paths in cssText to be relative to cssUrl
		//		a path is considered relative if it doesn't start with '/' and not contains ':'
		// description:
		//		Say we fetch a HTML page from level1/page.html
		//		It has some inline CSS:
		//	|		@import "css/page.css" tv, screen;
		//	|		...
		//	|		background-image: url(images/aplhaimage.png);
		//
		//		as we fetched this HTML and therefore this CSS
		//		from level1/page.html, these paths needs to be adjusted to:
		//	|		@import 'level1/css/page.css' tv, screen;
		//	|		...
		//	|		background-image: url(level1/images/alphaimage.png);
		//
		//		In IE it will also adjust relative paths in AlphaImageLoader()
		//	|		filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/alphaimage.png');
		//		will be adjusted to:
		//	|		filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='level1/images/alphaimage.png');
		//
		//		Please note that any relative paths in AlphaImageLoader in external css files wont work, as
		//		the paths in AlphaImageLoader is MUST be declared relative to the HTML page,
		//		not relative to the CSS file that declares it

		if(!cssText || !cssUrl){ return; }

		// support the ImageAlphaFilter if it exists, most people use it in IE 6 for transparent PNGs
		// We are NOT going to kill it in IE 7 just because the PNGs work there. Somebody might have
		// other uses for it.
		// If user want to disable css filter in IE6  he/she should
		// unset filter in a declaration that just IE 6 doesn't understands
		// like * > .myselector { filter:none; }
		if(alphaImageLoader){
			cssText = cssText.replace(alphaImageLoader, function(ignore, pre, delim, url, post){
				return pre + (new _Url(cssUrl, './'+url).toString()) + post;
			});
		}

		return cssText.replace(cssPaths, function(ignore, delimStr, strUrl, delimUrl, urlUrl, media){
			if(strUrl){
				return '@import "' + (new _Url(cssUrl, './'+strUrl).toString()) + '"' + media;
			}else{
				return 'url(' + (new _Url(cssUrl, './'+urlUrl).toString()) + ')' + media;
			}
		});
	};

	// attributepaths one tag can have multiple paths, example:
	// <input src="..." style="url(..)"/> or <a style="url(..)" href="..">
	// <img style='filter:progid...AlphaImageLoader(src="noticeTheSrcHereRunsThroughHtmlSrc")' src="img">
	var htmlAttrPaths = /(<[a-z][a-z0-9]*\s[^>]*)(?:(href|src)=(['"]?)([^>]*?)\3|style=(['"]?)([^>]*?)\5)([^>]*>)/gi;

	var adjustHtmlPaths = html._adjustHtmlPaths = function(htmlUrl, cont){
		var url = htmlUrl || "./";

		return cont.replace(htmlAttrPaths,
			function(tag, start, name, delim, relUrl, delim2, cssText, end){
				return start + (name ?
							(name + '=' + delim + (new _Url(url, relUrl).toString()) + delim)
						: ('style=' + delim2 + adjustCssPaths(url, cssText) + delim2)
				) + end;
			}
		);
	};

	var snarfStyles = html._snarfStyles = function	(/*String*/cssUrl, /*String*/cont, /*Array*/styles){
		/****************  cut out all <style> and <link rel="stylesheet" href=".."> **************/
		// also return any attributes from this tag (might be a media attribute)
		// if cssUrl is set it will adjust paths accordingly
		styles.attributes = [];

		cont = cont.replace(/<[!][-][-](.|\s)*?[-][-]>/g,
			function(comment){
				return comment.replace(/<(\/?)style\b/ig,"&lt;$1Style").replace(/<(\/?)link\b/ig,"&lt;$1Link").replace(/@import "/ig,"@ import \"");
			}
		);
		return cont.replace(/(?:<style([^>]*)>([\s\S]*?)<\/style>|<link\s+(?=[^>]*rel=['"]?stylesheet)([^>]*?href=(['"])([^>]*?)\4[^>\/]*)\/?>)/gi,
			function(ignore, styleAttr, cssText, linkAttr, delim, href){
				// trim attribute
				var i, attr = (styleAttr||linkAttr||"").replace(/^\s*([\s\S]*?)\s*$/i, "$1");
				if(cssText){
					i = styles.push(cssUrl ? adjustCssPaths(cssUrl, cssText) : cssText);
				}else{
					i = styles.push('@import "' + href + '";');
					attr = attr.replace(/\s*(?:rel|href)=(['"])?[^\s]*\1\s*/gi, ""); // remove rel=... and href=...
				}
				if(attr){
					attr = attr.split(/\s+/);// split on both "\n", "\t", " " etc
					var atObj = {}, tmp;
					for(var j = 0, e = attr.length; j < e; j++){
						tmp = attr[j].split('='); // split name='value'
						atObj[tmp[0]] = tmp[1].replace(/^\s*['"]?([\s\S]*?)['"]?\s*$/, "$1"); // trim and remove ''
					}
					styles.attributes[i - 1] = atObj;
				}
				return "";
			}
		);
	};

	var snarfScripts = html._snarfScripts = function(cont, byRef){
		// summary:
		//		strips out script tags from cont
		// byRef:
		//		byRef = {errBack:function(){/*add your download error code here*/, downloadRemote: true(default false)}}
		//		byRef will have {code: 'jscode'} when this scope leaves
		byRef.code = "";

		//Update script tags nested in comments so that the script tag collector doesn't pick
		//them up.
		cont = cont.replace(/<[!][-][-](.|\s)*?[-][-]>/g,
			function(comment){
				return comment.replace(/<(\/?)script\b/ig,"&lt;$1Script");
			}
		);

		function download(src){
			if(byRef.downloadRemote){
				//  0 && console.debug('downloading',src);
				//Fix up src, in case there were entity character encodings in it.
				//Probably only need to worry about a subset.
				src = src.replace(/&([a-z0-9#]+);/g, function(m, name) {
					switch(name) {
						case "amp"	: return "&";
						case "gt"	: return ">";
						case "lt"	: return "<";
						default:
							return name.charAt(0)=="#" ? String.fromCharCode(name.substring(1)) : "&"+name+";";
					}
				});
				xhrUtil.get({
					url: src,
					sync: true,
					load: function(code){
						byRef.code += code+";";
					},
					error: byRef.errBack
				});
			}
		}

		// match <script>, <script type="text/..., but not <script type="dojo(/method)...
		return cont.replace(/<script\s*(?![^>]*type=['"]?(?:dojo\/|text\/html\b))[^>]*?(?:src=(['"]?)([^>]*?)\1[^>]*)?>([\s\S]*?)<\/script>/gi,
			function(ignore, delim, src, code){
				if(src){
					download(src);
				}else{
					byRef.code += code;
				}
				return "";
			}
		);
	};

	var evalInGlobal = html.evalInGlobal = function(code, appendNode){
		// we do our own eval here as dojo.eval doesn't eval in global crossbrowser
		// This work X browser but but it relies on a DOM
		// plus it doesn't return anything, thats unrelevant here but not for dojo core
		appendNode = appendNode || windowUtil.doc.body;
		var n = appendNode.ownerDocument.createElement('script');
		n.type = "text/javascript";
		appendNode.appendChild(n);
		n.text = code; // DOM 1 says this should work
	};

	html._ContentSetter = declare(/*===== "dojox.html._ContentSetter", =====*/ htmlUtil._ContentSetter, {
		// adjustPaths: Boolean
		//		Adjust relative paths in html string content to point to this page
		//		Only useful if you grab content from a another folder than the current one
		adjustPaths: false,
		referencePath: ".",
		renderStyles: false,

		executeScripts: false,
		scriptHasHooks: false,
		scriptHookReplacement: null,

		_renderStyles: function(styles){
			// insert css from content into document head
			this._styleNodes = [];
			var st, att, cssText, doc = this.node.ownerDocument;
			var head = doc.getElementsByTagName('head')[0];

			for(var i = 0, e = styles.length; i < e; i++){
				cssText = styles[i]; att = styles.attributes[i];
				st = doc.createElement('style');
				st.setAttribute("type", "text/css"); // this is required in CSS spec!

				for(var x in att){
					st.setAttribute(x, att[x]);
				}

				this._styleNodes.push(st);
				head.appendChild(st); // must insert into DOM before setting cssText

				if(st.styleSheet){ // IE
					st.styleSheet.cssText = cssText;
				}else{ // w3c
					st.appendChild(doc.createTextNode(cssText));
				}
			}
		},

		empty: function() {
			this.inherited("empty", arguments);

			// empty out the styles array from any previous use
			this._styles = [];
		},

		onBegin: function() {
			// summary:
			//		Called after instantiation, but before set();
			//		It allows modification of any of the object properties - including the node and content
			//		provided - before the set operation actually takes place
			//		This implementation extends that of dojo.html._ContentSetter
			//		to add handling for adjustPaths, renderStyles on the html string content before it is set
			this.inherited("onBegin", arguments);

			var cont = this.content,
				node = this.node;

			var styles = this._styles;// init vars

			if(lang.isString(cont)){
				if(this.adjustPaths && this.referencePath){
					cont = adjustHtmlPaths(this.referencePath, cont);
				}

				if(this.renderStyles || this.cleanContent){
					cont = snarfStyles(this.referencePath, cont, styles);
				}

				// because of a bug in IE, script tags that is first in html hierarchy doesnt make it into the DOM
				//	when content is innerHTML'ed, so we can't use dojo.query to retrieve scripts from DOM
				if(this.executeScripts){
					var _t = this;
					var byRef = {
						downloadRemote: true,
						errBack:function(e){
							_t._onError.call(_t, 'Exec', 'Error downloading remote script in "'+_t.id+'"', e);
						}
					};
					cont = snarfScripts(cont, byRef);
					this._code = byRef.code;
				}
			}
			this.content = cont;
		},

		onEnd: function() {
			// summary:
			//		Called after set(), when the new content has been pushed into the node
			//		It provides an opportunity for post-processing before handing back the node to the caller
			//		This implementation extends that of dojo.html._ContentSetter

			var code = this._code,
				styles = this._styles;

			// clear old stylenodes from the DOM
			// these were added by the last set call
			// (in other words, if you dont keep and reuse the ContentSetter for a particular node
			// .. you'll have no practical way to do this)
			if(this._styleNodes && this._styleNodes.length){
				while(this._styleNodes.length){
					domConstruct.destroy(this._styleNodes.pop());
				}
			}
			// render new style nodes
			if(this.renderStyles && styles && styles.length){
				this._renderStyles(styles);
			}

			// Deferred to signal when this function is complete
			var d = new Deferred();

			// Setup function to call onEnd() in the superclass, for parsing, and resolve the above Deferred when
			// parsing is complete.
			var superClassOnEndMethod = this.getInherited(arguments),
				args = arguments,
				callSuperclass = lang.hitch(this, function(){
					superClassOnEndMethod.apply(this, args);

					// If parser ran (parseContent == true), wait for it to finish, otherwise call d.resolve() immediately
					when(this.parseDeferred, function(){ d.resolve(); });
				});

			if(this.executeScripts && code){
				// Evaluate any <script> blocks in the content
				if(this.cleanContent){
					// clean JS from html comments and other crap that browser
					// parser takes care of in a normal page load
					code = code.replace(/(<!--|(?:\/\/)?-->|<!\[CDATA\[|\]\]>)/g, '');
				}
				if(this.scriptHasHooks){
					// replace _container_ with this.scriptHookReplace()
					// the scriptHookReplacement can be a string
					// or a function, which when invoked returns the string you want to substitute in
					code = code.replace(/_container_(?!\s*=[^=])/g, this.scriptHookReplacement);
				}
				try{
					evalInGlobal(code, this.node);
				}catch(e){
					this._onError('Exec', 'Error eval script in '+this.id+', '+e.message, e);
				}

				// Finally, use ready() to wait for any require() calls from the <script> blocks to complete,
				// then call onEnd() in the superclass, for parsing, and when that is done resolve the Deferred.
				// For 2.0, remove the call to ready() (or this whole if() branch?) since the parser can do loading for us.
				ready(callSuperclass);
			}else{
				// There were no <script>'s to execute, so immediately call onEnd() in the superclass, and
				// when the parser finishes running, resolve the Deferred.
				callSuperclass();
			}

			// Return a promise that resolves after the ready() call completes, and after the parser finishes running.
			return d.promise;
		},

		tearDown: function() {
			this.inherited(arguments);
			delete this._styles;
			// only tear down -or another set() - will explicitly throw away the
			// references to the style nodes we added
			if(this._styleNodes && this._styleNodes.length){
				while(this._styleNodes.length){
					domConstruct.destroy(this._styleNodes.pop());
				}
			}
			delete this._styleNodes;
			// reset the defaults from the prototype
			// XXX: not sure if this is the correct intended behaviour, it was originally
			// dojo.getObject(this.declaredClass).prototype which will not work with anonymous
			// modules
			lang.mixin(this, html._ContentSetter.prototype);
		}

	});

	html.set = function(/* DomNode */ node, /* String|DomNode|NodeList */ cont, /* Object? */ params){
		// TODO: add all the other options
			// summary:
			//		inserts (replaces) the given content into the given node
			// node:
			//		the parent element that will receive the content
			// cont:
			//		the content to be set on the parent element.
			//		This can be an html string, a node reference or a NodeList, dojo/NodeList, Array or other enumerable list of nodes
			// params:
			//		Optional flags/properties to configure the content-setting. See dojo.html._ContentSetter
			// example:
			//		A safe string/node/nodelist content replacement/injection with hooks for extension
			//		Example Usage:
			//	|	dojo.html.set(node, "some string");
			//	|	dojo.html.set(node, contentNode, {options});
			//	|	dojo.html.set(node, myNode.childNodes, {options});

		if(!params){
			// simple and fast
			return htmlUtil._setNodeContent(node, cont, true);
		}else{
			// more options but slower
			var op = new html._ContentSetter(lang.mixin(
					params,
					{ content: cont, node: node }
			));
			return op.set();
		}
	};

	return html;
});
},
'dojox/widget/ColorPicker':function(){
define([
    "dojo/_base/kernel",
    "dojo/_base/declare", "dojo/_base/lang", "dojo/_base/array",
    "dojo/_base/html", "dojo/_base/connect", "dojo/_base/sniff", "dojo/_base/window",
    "dojo/_base/event", "dojo/dom", "dojo/dom-class", "dojo/keys", "dojo/fx", "dojo/dnd/move",
    "dijit/registry", "dijit/_base/focus", "dijit/form/_FormWidget",
    "dijit/typematic",
    "dojox/color",
    "dojo/i18n",
    "dojo/text!./ColorPicker/ColorPicker.html"
], function (kernel, declare, lang, ArrayUtil, html, Hub, has, win, Event, DOM, DOMClass, Keys, fx, move,
             registry, FocusManager, FormWidget, Typematic, color,
             i18n,
             template) {

    var bundle1 = {
        redLabel: "r",
            greenLabel: "g",
        blueLabel: "b",
        hueLabel: "h",
        saturationLabel: "s",
        valueLabel: "v", /* aka intensity or brightness */
        degLabel: "\u00B0",
        hexLabel: "hex",
        huePickerTitle: "Hue Selector",
        saturationPickerTitle: "Saturation Selector"
    }


    var webSafeFromHex = function (hex) {
        // stub, this is planned later:
        return hex;
    };

    // TODO: shouldn't this extend _FormValueWidget?
    return declare("dojox.widget.ColorPicker", FormWidget, {
        // summary:
        //		a HSV color picker - similar to Photoshop picker
        // description:
        //		Provides an interactive HSV ColorPicker similar to
        //		PhotoShop's color selction tool. This is an enhanced
        //		version of the default dijit.ColorPalette, though provides
        //		no accessibility.
        // example:
        // |	var picker = new dojox.widget.ColorPicker({
        // |		// a couple of example toggles:
        // |		animatePoint:false,
        // |		showHsv: false,
        // |		webSafe: false,
        // |		showRgb: false
        // |	});
        // example:
        // |	<!-- markup: -->
        // |	<div dojoType="dojox.widget.ColorPicker"></div>

        // showRgb: Boolean
        //		show/update RGB input nodes
        showRgb: true,

        // showHsv: Boolean
        //		show/update HSV input nodes
        showHsv: true,

        // showHex: Boolean
        //		show/update Hex value field
        showHex: true,

        // webSafe: Boolean
        //		deprecated? or just use a toggle to show/hide that node, too?
        webSafe: true,

        // animatePoint: Boolean
        //		toggle to use slideTo (true) or just place the cursor (false) on click
        animatePoint: true,

        // slideDuration: Integer
        //		time in ms picker node will slide to next location (non-dragging) when animatePoint=true
        slideDuration: 250,

        // liveUpdate: Boolean
        //		Set to true to fire onChange in an indeterminate way
        liveUpdate: false,

        // PICKER_HUE_H: int
        //		Height of the hue picker, used to calculate positions
        PICKER_HUE_H: 150,

        // PICKER_SAT_VAL_H: int
        //		Height of the 2d picker, used to calculate positions
        PICKER_SAT_VAL_H: 150,

        // PICKER_SAT_VAL_W: int
        //		Width of the 2d picker, used to calculate positions
        PICKER_SAT_VAL_W: 150,

        // PICKER_HUE_SELECTOR_H: int
        //		Height of the hue selector DOM node, used to calc offsets so that selection
        //		is center of the image node.
        PICKER_HUE_SELECTOR_H: 8,

        // PICKER_SAT_SELECTOR_H: int
        //		Height of the saturation selector DOM node, used to calc offsets so that selection
        //		is center of the image node.
        PICKER_SAT_SELECTOR_H: 10,

        // PICKER_SAT_SELECTOR_W: int
        //		Width of the saturation selector DOM node, used to calc offsets so that selection
        //		is center of the image node.
        PICKER_SAT_SELECTOR_W: 10,

        // value: String
        //		Default color for this component. Only hex values are accepted as incoming/returned
        //		values. Adjust this value with `.attr`, eg: dijit.byId("myPicker").attr("value", "#ededed");
        //		to cause the points to adjust and the values to reflect the current color.
        value: "#ffffff",

        _underlay: require.toUrl("dojox/widget/ColorPicker/images/underlay.png"),

        _hueUnderlay: require.toUrl("dojox/widget/ColorPicker/images/hue.png"),

        _pickerPointer: require.toUrl("dojox/widget/ColorPicker/images/pickerPointer.png"),

        _huePickerPointer: require.toUrl("dojox/widget/ColorPicker/images/hueHandle.png"),

        _huePickerPointerAlly: require.toUrl("dojox/widget/ColorPicker/images/hueHandleA11y.png"),

        templateString: template,

        postMixInProperties: function () {
            this._uId = registry.getUniqueId(this.id);
            lang.mixin(this, bundle1);
            lang.mixin(this, i18n.getLocalization("dojo.cldr", "number"));
            this.inherited(arguments);
        },

        postCreate: function () {
            // summary:
            //		As quickly as we can, set up ie6 alpha-filter support for our
            //		underlay.  we don't do image handles (done in css), just the 'core'
            //		of this widget: the underlay.
            this.inherited(arguments);
            if (has("ie") < 7) {
                this.colorUnderlay.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this._underlay + "', sizingMethod='scale')";
                this.colorUnderlay.src = this._blankGif.toString();
            }
            // hide toggle-able nodes:
            if (!this.showRgb) {
                this.rgbNode.style.visibility = "hidden";
            }
            if (!this.showHsv) {
                this.hsvNode.style.visibility = "hidden";
            }
            if (!this.showHex) {
                this.hexNode.style.visibility = "hidden";
            }
            if (!this.webSafe) {
                this.safePreviewNode.style.visibility = "hidden";
            }
        },

        startup: function () {
            if (this._started) {
                return;
            }
            this._started = true;
            this.set("value", this.value);
            this._mover = new move.boxConstrainedMoveable(this.cursorNode, {
                box: {
                    t: -(this.PICKER_SAT_SELECTOR_H / 2),
                    l: -(this.PICKER_SAT_SELECTOR_W / 2),
                    w: this.PICKER_SAT_VAL_W,
                    h: this.PICKER_SAT_VAL_H
                }
            });

            this._hueMover = new move.boxConstrainedMoveable(this.hueCursorNode, {
                box: {
                    t: -(this.PICKER_HUE_SELECTOR_H / 2),
                    l: 0,
                    w: 0,
                    h: this.PICKER_HUE_H
                }
            });

            this._subs = [];
            // no dnd/move/move published ... use a timer:
            this._subs.push(Hub.subscribe("/dnd/move/stop", lang.hitch(this, "_clearTimer")));
            this._subs.push(Hub.subscribe("/dnd/move/start", lang.hitch(this, "_setTimer")));

            // Bind to up, down, left and right  arrows on the hue and saturation nodes.
            this._keyListeners = [];
            this._connects.push(Typematic.addKeyListener(this.hueCursorNode, {
                charOrCode: Keys.UP_ARROW,
                shiftKey: false,
                metaKey: false,
                ctrlKey: false,
                altKey: false
            }, this, lang.hitch(this, this._updateHueCursorNode), 25, 25));
            this._connects.push(Typematic.addKeyListener(this.hueCursorNode, {
                charOrCode: Keys.DOWN_ARROW,
                shiftKey: false,
                metaKey: false,
                ctrlKey: false,
                altKey: false
            }, this, lang.hitch(this, this._updateHueCursorNode), 25, 25));
            this._connects.push(Typematic.addKeyListener(this.cursorNode, {
                charOrCode: Keys.UP_ARROW,
                shiftKey: false,
                metaKey: false,
                ctrlKey: false,
                altKey: false
            }, this, lang.hitch(this, this._updateCursorNode), 25, 25));
            this._connects.push(Typematic.addKeyListener(this.cursorNode, {
                charOrCode: Keys.DOWN_ARROW,
                shiftKey: false,
                metaKey: false,
                ctrlKey: false,
                altKey: false
            }, this, lang.hitch(this, this._updateCursorNode), 25, 25));
            this._connects.push(Typematic.addKeyListener(this.cursorNode, {
                charOrCode: Keys.LEFT_ARROW,
                shiftKey: false,
                metaKey: false,
                ctrlKey: false,
                altKey: false
            }, this, lang.hitch(this, this._updateCursorNode), 25, 25));
            this._connects.push(Typematic.addKeyListener(this.cursorNode, {
                charOrCode: Keys.RIGHT_ARROW,
                shiftKey: false,
                metaKey: false,
                ctrlKey: false,
                altKey: false
            }, this, lang.hitch(this, this._updateCursorNode), 25, 25));
        },

        _setValueAttr: function (value, fireOnChange) {
            if (!this._started) {
                return;
            }
            this.setColor(value, fireOnChange);
        },

        setColor: function (/* String */col, force) {
            // summary:
            //		Set a color on a picker. Usually used to set
            //		initial color as an alternative to passing defaultColor option
            //		to the constructor.
            col = color.fromString(col);
            this._updatePickerLocations(col);
            this._updateColorInputs(col);
            this._updateValue(col, force);
        },

        _setTimer: function (/* dojo/dnd/Mover */mover) {
            if (mover.node != this.cursorNode && mover.node != this.hueCursorNode) {
                return;
            }
            // FIXME: should I assume this? focus on mouse down so on mouse up
            FocusManager.focus(mover.node);
            DOM.setSelectable(this.domNode, false);
            this._timer = setInterval(lang.hitch(this, "_updateColor"), 45);
        },

        _clearTimer: function (/* dojo/dnd/Mover */mover) {
            if (!this._timer) {
                return;
            }
            clearInterval(this._timer);
            this._timer = null;
            this.onChange(this.value);
            DOM.setSelectable(this.domNode, true);
        },

        _setHue: function (/* Float */h) {
            // summary:
            //		Sets a natural color background for the
            //		underlay image against closest hue value (full saturation)
            // h:
            //		0..360
            html.style(this.colorUnderlay, "backgroundColor", color.fromHsv(h, 100, 100).toHex());

        },

        _updateHueCursorNode: function (count, node, e) {
            // summary:
            //		Function used by the typematic code to handle cursor position and update
            //		via keyboard.
            // count: Number
            //		-1 means stop, anything else is just how many times it was called.
            // node: DomNode
            //		The node generating the event.
            // e: Event
            //		The event.
            if (count !== -1) {
                var y = html.style(this.hueCursorNode, "top");
                var selCenter = this.PICKER_HUE_SELECTOR_H / 2;

                // Account for our offset
                y += selCenter;
                var update = false;
                if (e.charOrCode == Keys.UP_ARROW) {
                    if (y > 0) {
                        y -= 1;
                        update = true;
                    }
                } else if (e.charOrCode == Keys.DOWN_ARROW) {
                    if (y < this.PICKER_HUE_H) {
                        y += 1;
                        update = true;
                    }
                }
                y -= selCenter;
                if (update) {
                    html.style(this.hueCursorNode, "top", y + "px");
                }
            } else {
                this._updateColor(true);
            }
        },

        _updateCursorNode: function (count, node, e) {
            // summary:
            //		Function used by the typematic code to handle cursor position and update
            //		via keyboard.
            // count:
            //		-1 means stop, anything else is just how many times it was called.
            // node:
            //		The node generating the event.
            // e:
            //		The event.
            var selCenterH = this.PICKER_SAT_SELECTOR_H / 2;
            var selCenterW = this.PICKER_SAT_SELECTOR_W / 2;

            if (count !== -1) {
                var y = html.style(this.cursorNode, "top");
                var x = html.style(this.cursorNode, "left");

                // Account for our offsets to center
                y += selCenterH;
                x += selCenterW;

                var update = false;
                if (e.charOrCode == Keys.UP_ARROW) {
                    if (y > 0) {
                        y -= 1;
                        update = true;
                    }
                } else if (e.charOrCode == Keys.DOWN_ARROW) {
                    if (y < this.PICKER_SAT_VAL_H) {
                        y += 1;
                        update = true;
                    }
                } else if (e.charOrCode == Keys.LEFT_ARROW) {
                    if (x > 0) {
                        x -= 1;
                        update = true;
                    }
                } else if (e.charOrCode == Keys.RIGHT_ARROW) {
                    if (x < this.PICKER_SAT_VAL_W) {
                        x += 1;
                        update = true;
                    }
                }
                if (update) {
                    // Account for our offsets to center
                    y -= selCenterH;
                    x -= selCenterW;
                    html.style(this.cursorNode, "top", y + "px");
                    html.style(this.cursorNode, "left", x + "px");
                }
            } else {
                this._updateColor(true);
            }
        },

        _updateColor: function (fireChange) {
            // summary:
            //		update the previewNode color, and input values [optional]

            var hueSelCenter = this.PICKER_HUE_SELECTOR_H / 2,
                satSelCenterH = this.PICKER_SAT_SELECTOR_H / 2,
                satSelCenterW = this.PICKER_SAT_SELECTOR_W / 2;

            var _huetop = html.style(this.hueCursorNode, "top") + hueSelCenter,
                _pickertop = html.style(this.cursorNode, "top") + satSelCenterH,
                _pickerleft = html.style(this.cursorNode, "left") + satSelCenterW,
                h = Math.round(360 - (_huetop / this.PICKER_HUE_H * 360)),
                col = color.fromHsv(h, _pickerleft / this.PICKER_SAT_VAL_W * 100, 100 - (_pickertop / this.PICKER_SAT_VAL_H * 100))
                ;

            this._updateColorInputs(col);
            this._updateValue(col, fireChange);

            // update hue, not all the pickers
            if (h != this._hue) {
                this._setHue(h);
            }
        },

        _colorInputChange: function (e) {
            // summary:
            //		updates picker position and inputs
            //		according to rgb, hex or hsv input changes
            var col, hasit = false;
            switch (e.target) {
                //transform to hsv to pixels

                case this.hexCode:
                    col = color.fromString(e.target.value);
                    hasit = true;

                    break;
                case this.Rval:
                case this.Gval:
                case this.Bval:
                    col = color.fromArray([this.Rval.value, this.Gval.value, this.Bval.value]);
                    hasit = true;
                    break;
                case this.Hval:
                case this.Sval:
                case this.Vval:
                    col = color.fromHsv(this.Hval.value, this.Sval.value, this.Vval.value);
                    hasit = true;
                    break;
            }

            if (hasit) {
                this._updatePickerLocations(col);
                this._updateColorInputs(col);
                this._updateValue(col, true);
            }

        },

        _updateValue: function (/* dojox/color/Color */col, /* Boolean */fireChange) {
            // summary:
            //		updates the value of the widget
            //		can cancel reverse onChange by specifying second param
            var hex = col.toHex();

            this.value = this.valueNode.value = hex;

            // anytime we muck with the color, fire onChange?
            if (fireChange && (!this._timer || this.liveUpdate)) {
                this.onChange(hex);
            }
        },

        _updatePickerLocations: function (/* dojox/color/Color */col) {
            // summary:
            //		update handles on the pickers acording to color values

            var hueSelCenter = this.PICKER_HUE_SELECTOR_H / 2,
                satSelCenterH = this.PICKER_SAT_SELECTOR_H / 2,
                satSelCenterW = this.PICKER_SAT_SELECTOR_W / 2;

            var hsv = col.toHsv(),
                ypos = Math.round(this.PICKER_HUE_H - hsv.h / 360 * this.PICKER_HUE_H) - hueSelCenter,
                newLeft = Math.round(hsv.s / 100 * this.PICKER_SAT_VAL_W) - satSelCenterW,
                newTop = Math.round(this.PICKER_SAT_VAL_H - hsv.v / 100 * this.PICKER_SAT_VAL_H) - satSelCenterH
                ;

            if (this.animatePoint) {
                fx.slideTo({
                    node: this.hueCursorNode,
                    duration: this.slideDuration,
                    top: ypos,
                    left: 0
                }).play();

                fx.slideTo({
                    node: this.cursorNode,
                    duration: this.slideDuration,
                    top: newTop,
                    left: newLeft
                }).play();

            }
            else {
                html.style(this.hueCursorNode, "top", ypos + "px");
                html.style(this.cursorNode, {
                    left: newLeft + "px",
                    top: newTop + "px"
                });
            }

            // limit hue calculations to only when it changes
            if (hsv.h != this._hue) {
                this._setHue(hsv.h);
            }

        },

        _updateColorInputs: function (/* dojox/color/Color */ col) {
            // summary:
            //		updates color inputs that were changed through other inputs
            //		or by clicking on the picker

            var hex = col.toHex();

            if (this.showRgb) {
                this.Rval.value = col.r;
                this.Gval.value = col.g;
                this.Bval.value = col.b;
            }

            if (this.showHsv) {
                var hsv = col.toHsv();
                this.Hval.value = Math.round((hsv.h)); // convert to 0..360
                this.Sval.value = Math.round(hsv.s);
                this.Vval.value = Math.round(hsv.v);
            }

            if (this.showHex) {
                this.hexCode.value = hex;
            }

            this.previewNode.style.backgroundColor = hex;

            if (this.webSafe) {
                this.safePreviewNode.style.backgroundColor = webSafeFromHex(hex);
            }
        },

        _setHuePoint: function (/* Event */evt) {
            // summary:
            //		set the hue picker handle on relative y coordinates
            var selCenter = this.PICKER_HUE_SELECTOR_H / 2;
            var ypos = evt.layerY - selCenter;
            if (this.animatePoint) {
                fx.slideTo({
                    node: this.hueCursorNode,
                    duration: this.slideDuration,
                    top: ypos,
                    left: 0,
                    onEnd: lang.hitch(this, function () {
                        this._updateColor(false);
                        FocusManager.focus(this.hueCursorNode);
                    })
                }).play();
            } else {
                html.style(this.hueCursorNode, "top", ypos + "px");
                this._updateColor(false);
            }
        },

        _setPoint: function (/* Event */evt) {
            // summary:
            //		set our picker point based on relative x/y coordinates

            //	evt.preventDefault();
            var satSelCenterH = this.PICKER_SAT_SELECTOR_H / 2;
            var satSelCenterW = this.PICKER_SAT_SELECTOR_W / 2;
            var newTop = evt.layerY - satSelCenterH;
            var newLeft = evt.layerX - satSelCenterW;

            if (evt) {
                FocusManager.focus(evt.target);
            }

            if (this.animatePoint) {
                fx.slideTo({
                    node: this.cursorNode,
                    duration: this.slideDuration,
                    top: newTop,
                    left: newLeft,
                    onEnd: lang.hitch(this, function () {
                        this._updateColor(true);
                        FocusManager.focus(this.cursorNode);
                    })
                }).play();
            } else {
                html.style(this.cursorNode, {
                    left: newLeft + "px",
                    top: newTop + "px"
                });
                this._updateColor(false);
            }
        },

        _handleKey: function (/* Event */e) {
            // TODO: not implemented YET
            // var keys = d.keys;
        },

        focus: function () {
            // summary:
            //		Put focus on this widget, only if focus isn't set on it already.
            if (!this.focused) {
                FocusManager.focus(this.focusNode);
            }
        },

        _stopDrag: function (e) {
            // summary:
            //		Function to halt the mouse down default
            //		to disable dragging of images out of the color
            //		picker.
            Event.stop(e);
        },

        destroy: function () {
            // summary:
            //		Over-ride to clean up subscriptions, etc.
            this.inherited(arguments);
            ArrayUtil.forEach(this._subs, function (sub) {
                Hub.unsubscribe(sub);
            });
            delete this._subs;
        }
    });
});

},
'dojox/color':function(){
define(["./color/_base"], function(dxcolor){
	/*=====
	 return {
	 // summary:
	 //		Deprecated.  Should require dojox/color modules directly rather than trying to access them through
	 //		this module.
	 };
	 =====*/
	return dxcolor;
});

},
'dojox/color/_base':function(){
define(["../main", "dojo/_base/lang", "dojo/_base/Color", "dojo/colors"],
	function(dojox, lang, Color, colors){

var cx = lang.getObject("color", true, dojox);
/*===== cx = dojox.color =====*/
		
//	alias all the dojo.Color mechanisms
cx.Color=Color;
cx.blend=Color.blendColors;
cx.fromRgb=Color.fromRgb;
cx.fromHex=Color.fromHex;
cx.fromArray=Color.fromArray;
cx.fromString=Color.fromString;

//	alias the dojo.colors mechanisms
cx.greyscale=colors.makeGrey;

lang.mixin(cx,{
	fromCmy: function(/* Object|Array|int */cyan, /*int*/magenta, /*int*/yellow){
		// summary:
		//		Create a dojox.color.Color from a CMY defined color.
		//		All colors should be expressed as 0-100 (percentage)
	
		if(lang.isArray(cyan)){
			magenta=cyan[1], yellow=cyan[2], cyan=cyan[0];
		} else if(lang.isObject(cyan)){
			magenta=cyan.m, yellow=cyan.y, cyan=cyan.c;
		}
		cyan/=100, magenta/=100, yellow/=100;
	
		var r=1-cyan, g=1-magenta, b=1-yellow;
		return new Color({ r:Math.round(r*255), g:Math.round(g*255), b:Math.round(b*255) });	//	dojox.color.Color
	},
	
	fromCmyk: function(/* Object|Array|int */cyan, /*int*/magenta, /*int*/yellow, /*int*/black){
		// summary:
		//		Create a dojox.color.Color from a CMYK defined color.
		//		All colors should be expressed as 0-100 (percentage)
	
		if(lang.isArray(cyan)){
			magenta=cyan[1], yellow=cyan[2], black=cyan[3], cyan=cyan[0];
		} else if(lang.isObject(cyan)){
			magenta=cyan.m, yellow=cyan.y, black=cyan.b, cyan=cyan.c;
		}
		cyan/=100, magenta/=100, yellow/=100, black/=100;
		var r,g,b;
		r = 1-Math.min(1, cyan*(1-black)+black);
		g = 1-Math.min(1, magenta*(1-black)+black);
		b = 1-Math.min(1, yellow*(1-black)+black);
		return new Color({ r:Math.round(r*255), g:Math.round(g*255), b:Math.round(b*255) });	//	dojox.color.Color
	},
		
	fromHsl: function(/* Object|Array|int */hue, /* int */saturation, /* int */luminosity){
		// summary:
		//		Create a dojox.color.Color from an HSL defined color.
		//		hue from 0-359 (degrees), saturation and luminosity 0-100.
	
		if(lang.isArray(hue)){
			saturation=hue[1], luminosity=hue[2], hue=hue[0];
		} else if(lang.isObject(hue)){
			saturation=hue.s, luminosity=hue.l, hue=hue.h;
		}
		saturation/=100;
		luminosity/=100;
	
		while(hue<0){ hue+=360; }
		while(hue>=360){ hue-=360; }
		
		var r, g, b;
		if(hue<120){
			r=(120-hue)/60, g=hue/60, b=0;
		} else if (hue<240){
			r=0, g=(240-hue)/60, b=(hue-120)/60;
		} else {
			r=(hue-240)/60, g=0, b=(360-hue)/60;
		}
		
		r=2*saturation*Math.min(r, 1)+(1-saturation);
		g=2*saturation*Math.min(g, 1)+(1-saturation);
		b=2*saturation*Math.min(b, 1)+(1-saturation);
		if(luminosity<0.5){
			r*=luminosity, g*=luminosity, b*=luminosity;
		}else{
			r=(1-luminosity)*r+2*luminosity-1;
			g=(1-luminosity)*g+2*luminosity-1;
			b=(1-luminosity)*b+2*luminosity-1;
		}
		return new Color({ r:Math.round(r*255), g:Math.round(g*255), b:Math.round(b*255) });	//	dojox.color.Color
	}
});
	
cx.fromHsv = function(/* Object|Array|int */hue, /* int */saturation, /* int */value){
	// summary:
	//		Create a dojox.color.Color from an HSV defined color.
	//		hue from 0-359 (degrees), saturation and value 0-100.

	if(lang.isArray(hue)){
		saturation=hue[1], value=hue[2], hue=hue[0];
	} else if (lang.isObject(hue)){
		saturation=hue.s, value=hue.v, hue=hue.h;
	}
	
	if(hue==360){ hue=0; }
	saturation/=100;
	value/=100;
	
	var r, g, b;
	if(saturation==0){
		r=value, b=value, g=value;
	}else{
		var hTemp=hue/60, i=Math.floor(hTemp), f=hTemp-i;
		var p=value*(1-saturation);
		var q=value*(1-(saturation*f));
		var t=value*(1-(saturation*(1-f)));
		switch(i){
			case 0:{ r=value, g=t, b=p; break; }
			case 1:{ r=q, g=value, b=p; break; }
			case 2:{ r=p, g=value, b=t; break; }
			case 3:{ r=p, g=q, b=value; break; }
			case 4:{ r=t, g=p, b=value; break; }
			case 5:{ r=value, g=p, b=q; break; }
		}
	}
	return new Color({ r:Math.round(r*255), g:Math.round(g*255), b:Math.round(b*255) });	//	dojox.color.Color
};
lang.extend(Color,{
	toCmy: function(){
		// summary:
		//		Convert this Color to a CMY definition.
		var cyan=1-(this.r/255), magenta=1-(this.g/255), yellow=1-(this.b/255);
		return { c:Math.round(cyan*100), m:Math.round(magenta*100), y:Math.round(yellow*100) };		//	Object
	},
		
	toCmyk: function(){
		// summary:
		//		Convert this Color to a CMYK definition.
		var cyan, magenta, yellow, black;
		var r=this.r/255, g=this.g/255, b=this.b/255;
		black = Math.min(1-r, 1-g, 1-b);
		cyan = (1-r-black)/(1-black);
		magenta = (1-g-black)/(1-black);
		yellow = (1-b-black)/(1-black);
		return { c:Math.round(cyan*100), m:Math.round(magenta*100), y:Math.round(yellow*100), b:Math.round(black*100) };	//	Object
	},
		
	toHsl: function(){
		// summary:
		//		Convert this Color to an HSL definition.
		var r=this.r/255, g=this.g/255, b=this.b/255;
		var min = Math.min(r, b, g), max = Math.max(r, g, b);
		var delta = max-min;
		var h=0, s=0, l=(min+max)/2;
		if(l>0 && l<1){
			s = delta/((l<0.5)?(2*l):(2-2*l));
		}
		if(delta>0){
			if(max==r && max!=g){
				h+=(g-b)/delta;
			}
			if(max==g && max!=b){
				h+=(2+(b-r)/delta);
			}
			if(max==b && max!=r){
				h+=(4+(r-g)/delta);
			}
			h*=60;
		}
		return { h:h, s:Math.round(s*100), l:Math.round(l*100) };	//	Object
	},
	
	toHsv: function(){
		// summary:
		//		Convert this Color to an HSV definition.
		var r=this.r/255, g=this.g/255, b=this.b/255;
		var min = Math.min(r, b, g), max = Math.max(r, g, b);
		var delta = max-min;
		var h = null, s = (max==0)?0:(delta/max);
		if(s==0){
			h = 0;
		}else{
			if(r==max){
				h = 60*(g-b)/delta;
			}else if(g==max){
				h = 120 + 60*(b-r)/delta;
			}else{
				h = 240 + 60*(r-g)/delta;
			}
	
			if(h<0){ h+=360; }
		}
		return { h:h, s:Math.round(s*100), v:Math.round(max*100) };	//	Object
	}
});

return cx;
});

}}});
