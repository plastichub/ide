require({cache:{
'xgrid/main':function(){
define([
    'xide/data/ObservableStore',
    'xide/data/Reference',
    'xide/data/Source',
    'xgrid/Actions',
    'xgrid/Base',
    'xgrid/Clipboard',
    'xgrid/ColumnHider',
    'xgrid/ContextMenu',
    'xgrid/Defaults',
    'xgrid/DnD',
    'xgrid/Focus',
    'xgrid/Grid',
    'xgrid/Keyboard',
    'xgrid/KeyboardNavigation',
    'xgrid/Layout',
    'xgrid/ListRenderer',
    'xgrid/MultiRenderer',
    'xgrid/Renderer',
    'xgrid/Search',
    'xgrid/ThumbRenderer',
    'xgrid/component',
    'xgrid/Toolbar',
    'xgrid/TreeRenderer',
    'xgrid/types',
    'xgrid/Noob',
    'xgrid/GridLite'
], function () {
});
},
'xgrid/Actions':function(){
/** module xgrid/actions **/
define([
    "xdojo/declare",
    'xide/types',
    'xaction/ActionProvider',
    'xaction/DefaultActions',
    'xide/lodash',
    'xide/$',
    'xide/console'
], function (declare, types, ActionProvider, DefaultActions, _, $, console) {
    var _debug = false;
    /**
     * @class module:xgrid/Actions
     * @lends module:xide/mixins/EventedMixin
     *
     * All about actions:
     * 1. implements std before and after actions:
     * 1.1 on onAfterAction its restoring focus and selection automatically
     * 2. handles and forwards click, contextmenu and onAddActions     *
     */
    var Implementation = {
        _ActionContextState: null,
        onActivateActionContext: function (context, e) {
            return;
            /*
            var state = this._ActionContextState;
            if (this._isRestoring) {
                return;
            }
            this._isRestoring = true;
            if (e != null && e.selection && state) {
                state.selection = e != null ? e.selection : state.selection;
            }
            var self = this;
            _debug && console.log('onActivateActionContext', e);
            //@TODO Fixme

                var dfd = self._restoreSelection(state, 0, false, 'onActivateActionContext');
                if (dfd && dfd.then) {
                    dfd.then(function (e) {
                        self._isRestoring = false;
                    });
                } else {
                    self._isRestoring = false;
                }

                */
        },
        onDeactivateActionContext: function (context, event) {
            //_debug && console.log('onDeactivateActionContext ' + this.id, event);
            //this._ActionContextState = this._preserveSelection();
        },
        /**
         * Callback when action is performed:before (xide/widgets/_MenuMixin)
         * @param action {module:xaction/Action}
         */
        onBeforeAction: function (action) {
        },
        /**
         * Callback when action is performed: after (xide/widgets/_MenuMixin)
         *
         * @TODO Run the post selection only when we are active!
         *
         *
         * @param action {module:xaction/Action}
         */
        onAfterAction: function (action, actionDfdResult) {
            action = this.getAction(action);
            _debug && console.log('on after ' + action.command, actionDfdResult);
            if (actionDfdResult != null) {
                if (_.isObject(actionDfdResult)) {
                    // post work: selection & focus
                    var select = actionDfdResult.select,
                        focus = actionDfdResult.focus || true;
                    if (select) {
                        var options = {
                            append: actionDfdResult.append,
                            focus: focus,
                            delay: actionDfdResult.delay || 1,
                            expand: actionDfdResult.expand
                        };
                        //focus == true ? null : this.focus();
                        return this.select(select, null, true, options);
                    }
                }
            }
            this._emit(types.EVENTS.ON_AFTER_ACTION, action);
        },
        hasPermission: function (permission) {
            return DefaultActions.hasAction(this.permissions, permission);
        },
        /**
         *
         * @param where
         * @param action
         * @returns {boolean}
         */
        addAction: function (where, action) {
            if (action.keyCombo && _.isArray(action.keyCombo)) {
                if (action.keyCombo.indexOf('dblclick') !== -1) {
                    var thiz = this;
                    function handler(e) {
                        // @TODO: weird dblclick event duplicate in xgrid/Actions
                        if (e._inp) {
                            return;
                        }
                        e._inp = true;
                        var row = thiz.row(e);
                        row && thiz.runAction(action, row.data);
                    }
                    this.addHandle('dbclick', this.on('dblclick', handler));
                }
            }
            return this.inherited(arguments);
        },
        /**
         * Callback when selection changed, refreshes all actions
         * @param evt
         * @private
         */
        _onSelectionChanged: function (evt) {
            this.inherited(arguments);
            this.refreshActions();
        },
        ////////////////////////////////////////////////////////////////////////////
        //
        //  Original ActionMixin
        //
        ///////////////////////////////////////////////////////////////////////////
        /**
         *
         * @param provider
         * @param target
         */
        updateActions: function (provider, target) {
            var actions,
                actionsFiltered,
                selection = this.getSelection();

            if (provider && target) {
                actions = provider.getItemActions();
                actionsFiltered = this._filterActions(selection, actions, provider);
                target.setItemActions({}, actionsFiltered);
            }
        },
        startup: function () {
            if (this._started) {
                return;
            }
            var thiz = this;
            thiz.domNode.tabIndex = -1;
            function clickHandler(evt) {
                //container
                if (evt && evt.target) {
                    var $target = $(evt.target);
                    if ($target.hasClass('dgrid-content') || $target.hasClass('dgrid-extra')) {
                        thiz.select([], null, false);
                        thiz.deselectAll();
                        if (evt.type !== 'contextmenu') {
                            setTimeout(function () {
                                thiz.domNode.focus();
                                document.activeElement = thiz.domNode;
                                $(thiz.domNode).focus();
                            }, 1);
                        }
                    }
                }
            }
            this.on("contextmenu", clickHandler.bind(this));
            this._on('selectionChanged', function (evt) {
                this._onSelectionChanged(evt);
            }.bind(this));

            this._on('onAddActions', function (evt) {
                var actions = evt.actions,
                    action = types.ACTION.HEADER;

                if (!thiz.getAction(action)) {
                    actions.push(thiz.createAction({
                        label: 'Header',
                        command: action,
                        icon: 'fa-hdd-o',
                        tab: 'View',
                        group: 'Show',
                        mixin: {
                            actionType: 'multiToggle'
                        },
                        onCreate: function (action) {
                            action.set('value', thiz.showHeader);
                        },
                        onChange: function (property, value) {
                            thiz._setShowHeader(value);
                            thiz.showHeader = value;
                            thiz.onAfterAction(types.ACTION.HEADER);
                        }
                    }));
                }
            });
            return this.inherited(arguments);
        }
    };
    //package via declare
    var _class = declare('xgrid.Actions', ActionProvider, Implementation);
    _class.Implementation = Implementation;
    return _class;
});
},
'xgrid/Base':function(){
/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/types',
    'xide/utils/ObjectUtils', //possibly not loaded yet
    'xide/utils',
    'dgrid/OnDemandGrid',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'xgrid/ListRenderer',
    'xgrid/ThumbRenderer',
    'xgrid/TreeRenderer',
    'dgrid/util/misc'

], function (declare, types,
    xTypes, ObjectUtils, utils,
    OnDemandGrid, Defaults, Layout, Focus,
    ListRenderer, ThumbRenderer, TreeRenderer,
    miscUtil) {

    var BASE_CLASSES = ['EVENTED', 'GRID', 'EDITOR', 'RENDERER', 'DEFAULTS', 'LAYOUT', 'FOCUS', 'i18'];
    var DEFAULT_GRID_FEATURES = types.DEFAULT_GRID_FEATURES;
    var GRID_BASES = types.GRID_BASES;
    var DEFAULT_GRID_OPTIONS = types.DEFAULT_GRID_OPTIONS;

    /**
     * Short hand version of declare.classFactory for our base grid
     * @param name
     * @param bases
     * @param extraClasses
     * @param implementation
     * @private
     * @returns {*}
     */
    function classFactory(name, bases, extraClasses, implementation) {
        return declare.classFactory(name, bases, extraClasses, implementation, GRID_BASES);
    }
    /**
     * Default implementation
     * @class module:xgrid/Base
     * @extends module:dgrid/List
     * @extends module:xide/mixins/EventedMixin
     */
    var Implementation = {
        _isHighlighting: false,
        _featureMap: {},
        options: utils.clone(types.DEFAULT_GRID_OPTIONS),
        getContextMenu: function () {},
        getToolbar: function () {},
        /**
         * Returns true if there is anything rendered.
         * @param item {obj|null}
         * @returns {boolean}
         */
        isRendered: function (item) {
            if (!item) {
                return this.bodyNode != null;
            }
            item = this._normalize(item);
            var collection = this.collection;
            if (item) {
                var itemData = item.data;
                var idProp = collection['idProperty'];
                var nodes = this.getRows(true);
                if (nodes) {
                    for (var i = 0; i < nodes.length; i++) {
                        var node = nodes[i];
                        var row = this.row(node);
                        if (row && row.data && row.data && itemData && row.data[idProp] === itemData[idProp]) {
                            return true;
                        }
                    }
                }

            }
            return false;
        },
        /**
         * highlightRow in dgrid/List leaks and is anyway not needed.
         */
        highlightRow: function () {},
        getParent: function () {
            return this._parent;
        },
        get: function (what) {
            var parent = this.getParent();
            if (what === 'iconClass') {
                //docker:
                if (parent && parent.icon) {
                    return parent.icon();
                }
            }
            return this.inherited(arguments);
        },
        set: function (what, value) {
            var parent = this.getParent();
            if (what === 'iconClass') {
                var _set = parent.set;
                if (_set) {
                    _set.apply(parent, [what, value]);
                } else if (parent && parent.icon) {
                    parent.icon(value);
                    return true;
                }
            }
            if (what === 'title' && value && parent) {
                var _set = parent.set;
                if (_set) {
                    _set.apply(parent, [what, value]);
                } else if (parent && parent.title) {
                    parent.title(value);
                }
            }

            if (what === 'loading') {
                this.__loading = value;
                if (parent) {
                    //docker:
                    if (parent.startLoading) {
                        var icon = parent._options.icon;
                        if (value === true) {
                            parent.startLoading('', 0.5);
                            parent.icon('fa-spinner fa-spin');
                        } else {
                            parent.finishLoading();
                            parent.icon(icon);
                        }
                        return true;
                    } else if (parent.set) {
                        parent.set('loading', value);
                    }
                }
            }
            return this.inherited(arguments);
        },
        runAction: function (action) {
            if (action.command == types.ACTION.HEADER) {
                this._setShowHeader(!this.showHeader);
            }
            return this.inherited(arguments);
        },
        highlight: function (highlight) {
            var node = $(this.domNode.parentNode);
            if (highlight) {
                if (this._isHighlighting) {
                    return;
                }
                this._isHighlighting = true;
                node.addClass('highlight');
            } else {

                this._isHighlighting = false;
                node.removeClass('highlight');
            }
        },
        getState: function (state) {
            state = this.inherited(arguments) || {};
            state.showHeader = this.showHeader;
            return state;
        },
        postMixInProperties: function () {
            var state = this.state;
            if (state) {
                this.showHeader = state.showHeader;
            }
            return this.inherited(arguments);
        },
        renderArray: function (array) {
            if (this.__loading) {
                return [];
            }
            this._lastData = array;
            return this.inherited(arguments);
        },
        getData: function () {
            return this._lastData;
        },
        refreshItem: function (item, silent) {
            if (silent) {
                this._muteSelectionEvents = true;
            }
            this.collection.emit('update', {
                target: item
            });
            if (silent) {
                this._muteSelectionEvents = false;
            }
        },
        onShow: function () {
            this._emit(types.EVENTS.ON_VIEW_SHOW, this);
            return this.inherited(arguments);
        },
        isActive: function (testNode) {
            return utils.isDescendant(this.domNode, testNode || document.activeElement);
        },
        _showHeader: function (show) {
            $(this.domNode).find('.dgrid-header').each(function (i, el) {
                $(el).css('display', show ? '' : 'none');
            });

            $(this.domNode).find('.dgrid-scroller').each(function (i, el) {
                $(el).css('margin-top', show ? 26 : 0);
            });

        },
        destroy: function () {
            this._emit('destroy', this);
            return this.inherited(arguments);
        },
        hasFeature: function (name) {
            return _contains(['name'], _.keys(this._featureMap));
        },
        _rowCount: function () {
            return this._lastRenderedArray.length;
        },
        /**
         * Return current row's elements or data
         * @param domNodes {boolean} return dom instead of data. Default false.
         * @param filterFunction
         * @returns {*}
         */
        getRows: function (domNodes, filterFunction) {
            var result = [],
                self = this;
            var nodes = $(self.domNode).find('.dgrid-row') || [];
            _.each(nodes, (node) => {
                var _row = self.row(node);
                if (_row && _row.element) {
                    result.push(_row[domNodes ? 'element' : 'data']);
                }
            });
            if (filterFunction) {
                return result.filter(filterFunction);
            }
            return result;
        },
        startup: function () {
            var result = this.inherited(arguments);
            if (this.columns) {
                _.each(this.columns, function (column) {
                    if (column.width) {
                        this.styleColumn(parseInt(column.id), 'width:' + column.width);
                    }
                }, this);
            }

            var self = this;
            this.showExtraSpace && this.on('dgrid-refresh-complete', function () {
                var rows = self.getRows();
                var _extra = $(self.contentNode).find('.dgrid-extra');
                if (!rows.length) {
                    return;
                }

                if (!_extra.length) {
                    _extra = $('<div class="dgrid-extra" style="width:100%;height:80px"></div>');
                    $(self.contentNode).append(_extra);
                    _extra.on('click', function () {
                        self.deselectAll();
                    });
                    _extra.on('contextmenu', function () {
                        self.deselectAll();
                    })
                }
            });

            return result;
        },
        removeRow: function () {
            var res = this.inherited(arguments);
            if (this.showExtraSpace) {
                if (!this._lastRenderedArray || this._lastRenderedArray.length == 0) {
                    var _extra = $(self.contentNode).find('.dgrid-extra');
                    _extra.remove();
                }
            }
            return res;
        }
    };
    /**
     * Create root class with declare and default implementation
     */
    var _default = declare('xgrid.Default', null, Implementation);

    /**
     * 2-dim array search
     * @param left {string[]}
     * @param keys {string[]}
     * @returns {boolean}
     * @private
     */
    function _contains(left, keys) {
        return keys.some((v) => {
            return left.indexOf(v) >= 0;
        });
    }

    /**
     * Find default keys in a feature struct and recompse user feature
     * @param feature {object} feature struct
     * @param defaultFeature {object}
     * @returns {object} recomposed feature
     */
    function getFeature(feature, defaultFeature) {
        //is new feature, return the mix of default props and customized version
        if (_contains(['CLASS', 'CLASSES', 'IMPLEMENTATION'], _.keys(feature))) {
            return utils.mixin(utils.cloneKeys(defaultFeature), feature);
        }
        return defaultFeature;
    }

    /**
     * Grid class factory
     * @param name {string} A name for the class created
     * @param baseClass {object} the actual implementation (default root class, declared above)
     * @param features {object} the feature map override
     * @param gridClasses {object} the base grid classes map override
     * @param args {object} root class override
     * @param _defaultBases {object}
     * @memberOf module:xgrid/Base
     * @returns {module:xgrid/Base}
     */
    function createGridClass(name, baseClass, features, gridClasses, args, _defaultBases) {
        var _isNewBaseClass = false;
        baseClass = baseClass || _default;
        //simple case, no base class and no features
        if (!baseClass && !features) {
            return _default;
        }
        if (baseClass) {
            _isNewBaseClass = _contains(BASE_CLASSES, _.keys(gridClasses));
            var defaultBases = utils.cloneKeys(_defaultBases || GRID_BASES);
            if (_isNewBaseClass) {
                utils.mixin(defaultBases, gridClasses);
                //remove empty
                defaultBases = _.pick(defaultBases, _.identity);
            }
            //recompose base class
            baseClass = classFactory(name, defaultBases, [_default], baseClass);
        }

        var newFeatures = [],
            featureMap = {};

        //case: base class and features
        if (baseClass && features) {
            var _defaultFeatures = utils.cloneKeys(DEFAULT_GRID_FEATURES);
            utils.mixin(_defaultFeatures, features);

            for (var featureName in _defaultFeatures) {
                var feature = _defaultFeatures[featureName];
                if (!_defaultFeatures[featureName]) {
                    continue;
                }
                var newFeature = null;
                if (feature === true) {
                    //is a base feature
                    newFeature = DEFAULT_GRID_FEATURES[featureName];
                } else if (DEFAULT_GRID_FEATURES[featureName]) {
                    //is new/extra feature
                    newFeature = getFeature(feature, DEFAULT_GRID_FEATURES[featureName]);
                } else {
                    //go on
                    newFeature = feature;
                }
                if (newFeature) {
                    var featureClass = classFactory(featureName, newFeature['CLASSES'] || [], [newFeature['CLASS']], newFeature['IMPLEMENTATION']);
                    newFeatures.push(featureClass);
                    featureMap[featureName] = featureClass;
                }
            }
            //recompose
            if (newFeatures.length > 0) {
                baseClass = classFactory(name, [baseClass], newFeatures, args);
            }
            //complete
            baseClass.prototype._featureMap = featureMap;
        }
        return baseClass;
    }


    var Module = createGridClass('xgrid/Base', {
            options: utils.clone(DEFAULT_GRID_OPTIONS)
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            COLUMN_HIDER: false
        },
        //bases, no modification
        null, {

        });

    Module.createGridClass = createGridClass;

    //track defaults on module
    Module.classFactory = classFactory;
    Module.DEFAULT_GRID_FEATURES = DEFAULT_GRID_FEATURES;
    Module.DEFAULT_GRID_BASES = GRID_BASES;
    Module.DEFAULT_GRID_OPTIONS = DEFAULT_GRID_OPTIONS;
    Module.DEFAULT_GRID_OPTION_KEYS = types.DEFAULT_GRID_OPTION_KEYS;

    return Module;

});
},
'xgrid/types':function(){
/** @module xgrid/types **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/ColumnHider',
    'dgrid/extensions/ColumnReorder', //@todo : fork!
    'dgrid/extensions/ColumnResizer', //@todo : fork!
    'dgrid/extensions/Pagination',    //@todo : fork!
    'xgrid/Selection',
    'xgrid/Toolbar',
    'xgrid/ContextMenu',
    'xgrid/Keyboard',
    'xide/mixins/EventedMixin',
    'dgrid/OnDemandGrid',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'xgrid/ListRenderer',
    'xgrid/Clipboard',
    'xgrid/Actions',
    'xlang/i18'
], function (declare,types,
             ColumnHider, ColumnReorder, ColumnResizer,
             Pagination,
             Selection,Toolbar,ContextMenu,_GridKeyboardSelection,
             EventedMixin, OnDemandGrid,Defaults,Layout,Focus,
             ListRenderer,
             Clipboard,Actions,i18)
{
    /**
     * Grid Bases
     * @enum module:xgrid/types/GRID_BASES
     * @memberOf module:xgrid/types
     */
    types.GRID_BASES = {
        GRID: OnDemandGrid,
        LAYOUT:Layout,
        DEFAULTS: Defaults,
        RENDERER: ListRenderer,
        EVENTED: EventedMixin,
        FOCUS:Focus,
        i18:i18
    };
    /**
     * Default Grid Options
     * @deprecated
     * @enum module:xgrid/types/DEFAULT_GRID_OPTIONS
     * @memberOf module:xgrid/types
     */
    types.DEFAULT_GRID_OPTIONS = {
        /**
         * Instruct the grid to add jQuery theme classes
         * @default true
         * @type {bool}
         * @constant
         */
        USE_JQUERY_CSS: true,
        /**
         * Behaviour flag to deselect an item when its already selected
         * @default true
         * @type {bool}
         * @constant
         */
        DESELECT_SELECTED: true,
        /**
         * Behaviour flag to clear selection when clicked on the container node
         * @default true
         * @type {bool}
         * @constant
         */
        CLEAR_SELECTION_ON_CLICK: true,
        /**
         * Item actions
         * @default true
         * @type {object}
         * @constant
         */
        ITEM_ACTIONS: {},
        /**
         * Grid actions (sort, hide column, layout)
         * @default true
         * @type {object}
         * @constant
         */
        GRID_ACTIONS: {},
        /**
         * Publish selection change globally
         * @default true
         * @type {boolean}
         * @constant
         */
        PUBLISH_SELECTION: false
    };
    /**
     * Grid option keys
     * @enum module:xgrid/types/GRID_OPTION
     * @memberOf module:xgrid/types
     */
    types.GRID_OPTION = {
        /**
         * Instruct the grid to add jQuery theme classes
         * @default true
         * @type {string}
         * @constant
         */
        USE_JQUERY_CSS: 'USE_JQUERY_CSS',
        /**
         * Behaviour flag to deselect an item when its already selected
         * @default true
         * @type {string}
         * @constant
         */
        DESELECT_SELECTED: 'DESELECT_SELECTED',
        /**
         * Behaviour flag to deselect an item when its already selected
         * @default true
         * @type {string}
         * @constant
         */
        CLEAR_SELECTION_ON_CLICK:'CLEAR_SELECTION_ON_CLICK',
        /**
         * Actions
         * @default true
         * @type {string}
         * @constant
         */
        ITEM_ACTIONS:'ITEM_ACTIONS',
        /**
         * Actions
         * @default true
         * @type {string}
         * @constant
         */
        GRID_ACTIONS:'GRID_ACTIONS'
    };
    /**
     * All grid default features
     * @enum module:xgrid/types/GRID_DEFAULT_FEATURES
     * @memberOf module:xgrid/types
     */
    types.DEFAULT_GRID_FEATURES = {
        SELECTION: {
            CLASS: Selection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        KEYBOARD_SELECTION: {
            CLASS: _GridKeyboardSelection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        COLUMN_HIDER: {
            CLASS: ColumnHider,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        COLUMN_REORDER: {
            CLASS: ColumnReorder,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        COLUMN_RESIZER: {
            CLASS: ColumnResizer,
            IMPLEMENTATION: {},
            CLASSES: null
        }
    };
    /**
     * All Grid Features for easy access
     * @enum module:xgrid/types/GRID_FEATURES
     * @memberOf module:xgrid/types
     */
    types.GRID_FEATURES = {
        SELECTION: {
            CLASS: Selection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        KEYBOARD_SELECTION: {
            CLASS: _GridKeyboardSelection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        COLUMN_HIDER: {
            CLASS: ColumnHider,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        COLUMN_REORDER: {
            CLASS: ColumnReorder,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        COLUMN_RESIZER: {
            CLASS: ColumnResizer,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        PAGINATION: {
            CLASS: Pagination,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        TOOLBAR: {
            CLASS: Toolbar,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        CONTEXT_MENU: {
            CLASS: ContextMenu,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        ACTIONS: {
            CLASS: Actions,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        CLIPBOARD:{
            CLASS:Clipboard,
            IMPLEMENTATION:{},
            CLASSES:null
        }
    };
    return declare(null,[],{});
});
},
'xgrid/ColumnHider':function(){
define([
	'xdojo/declare',
    'dojo/has',
    'dgrid/util/misc',
    'xide/types',
    'xide/utils'
], function (declare, has, misc,types,utils) {

    /*
     *	Column Hider plugin for dgrid
     *	Originally contributed by TRT 2011-09-28
     *
     *	A dGrid plugin that attaches a menu to a dgrid, along with a way of opening it,
     *	that will allow you to show and hide columns.  A few caveats:
     *
     *	1. Menu placement is entirely based on CSS definitions.
     *	2. If you want columns initially hidden, you must add "hidden: true" to your
     *		column definition.
     *	3. This implementation does NOT support ColumnSet, and has not been tested
     *		with multi-subrow records.
     *	4. Column show/hide is controlled via straight up HTML checkboxes.  If you
     *		are looking for something more fancy, you'll probably need to use this
     *		definition as a template to write your own plugin.
     *
     */
	return declare('xgrid.ColumnHider',null, {
        columnHiderActionRootCommand:'View/Columns',
		// i18nColumnHider: Object
		//		This object contains all of the internationalized strings for
		//		the ColumnHider extension as key/value pairs.
		i18nColumnHider: {},

		// _columnHiderRules: Object
		//		Hash containing handles returned from addCssRule.
		_columnHiderRules: null,
        _runAction:function(action,update,value){
            if(action && action.command.indexOf(this.columnHiderActionRootCommand)!=-1 ){
                var col = action.column;
                var isHidden = this.isColumnHidden(col.id);
                this.showColumn(col.id,isHidden);
                update!==false && action.set('value', !this.isColumnHidden(col.id));
            }
            return this.inherited(arguments);
        },
        /**
         *
         * @param permissions
         * @param actions
         * @returns {Array}
         */
		getColumnHiderActions:function(permissions,actions){
            var root = this.columnHiderActionRootCommand,
                thiz = this,
                columnActions = [],
                VISIBILITY = types.ACTION_VISIBILITY,
                node = this.domNode;

            actions = actions || [];
            var rootAction = _.find(actions,{
                command:root
            });
            if(!rootAction) {
                columnActions.push(this.createAction({
                    label:'Columns',
                    command:root,
                    icon:'fa-columns',
                    tab:'View',
                    group:'Columns',
                    toggleGroup:thiz.id + 'Columns',
                    onCreate:function(action){
                        action.setVisibility(VISIBILITY.RIBBON,{
                            expand:true
                        }).setVisibility(VISIBILITY.ACTION_TOOLBAR, false);
                    }
                }));
            }
            /**
             *
             * @param col
             * @private
             */
            function _createEntry(col) {

                var id = col.id,
                    label = 'Show ' + ( col.label || col.field || ''),
                    icon = col.icon || 'fa-cogs';

                // Allow cols to opt out of the hider (e.g. for selector column).
                if (col.unhidable) {
                    return;
                }
                var _action = thiz.createAction(label, root + '/' + label , icon, null, 'View', 'Columns', 'item|view',

                    //oncreate
                    function(action){

                        var widgetImplementation = {
                            postMixInProperties: function() {
                                this.inherited(arguments);
                                this.checked = this.item.get('value') === true;
                            },
                            startup:function(){
                                this.inherited(arguments);
                                this.on('change',function(val){
                                    thiz.showColumn(id,val);
                                });
                            }
                        };
                        var widgetArgs  ={
                            checked:!col.hidden,
                            iconClass:icon,
                            style:'float:inherit;'
                        };


                        var _visibilityMixin = {
                            //widgetClass:declare.classFactory('_Checked', [CheckedMenuItem,_ActionValueWidgetMixin], null, widgetImplementation ,null),
                            widgetArgs:widgetArgs,
                            actionType : 'multiToggle'
                        };

                        action.actionType = 'multiToggle';


                        action.setVisibility(types.ACTION_VISIBILITY_ALL,utils.cloneKeys(_visibilityMixin,false));

                        label = action.label.replace('Show ','');


                        //for ribbons we collapse into 'Checkboxes'
                        /*
                        action.setVisibility(VISIBILITY.RIBBON,{
                            widgetClass:declare.classFactory('_CheckedGroup', [ActionValueWidget], null,{
                                iconClass:"",
                                postMixInProperties: function() {
                                    this.inherited(arguments);
                                    this.checked = this.item.get('value') == true;
                                },
                                startup:function(){
                                    this.inherited(arguments);
                                    this.widget.on('change', function (val) {
                                        thiz.showColumn(id,val);
                                    }.bind(this));
                                }
                            } ,null),
                            widgetArgs:{
                                renderer:CheckBox,
                                checked:!col.hidden,
                                label:action.label.replace('Show ','')
                            }
                        });
                        */

                    }, /*handler*/ null ,
                    {
                        column:col,
                        filterGroup:"item|view",
                        tab:'View',
                        value:!col.hidden,
                        addPermission:true
                    },
                    null, null, permissions, node,thiz,thiz);

                if(_action){
                    columnActions.push(_action);
                }

                /**

                columnActions.push(_ActionMixin.createActionParameters(label, root + '/' + label, 'Columns', icon, function () {
                    console.log('handler');

                }, '', null, null, thiz, thiz, {
                    column:col,
                    filterGroup:"item|view",
                    tab:'View',
                    value:!col.hidden,
                    onCreate:function(action){

                        var _action = this;

                        action.owner = thiz;

                        var widgetImplementation = {
                            postMixInProperties: function() {
                                this.inherited(arguments);
                                this.checked = this.item.get('value') == true;
                            },
                            startup:function(){
                                this.inherited(arguments);
                                this.on('change',function(val){
                                    thiz.showColumn(id,val);
                                })
                            },
                            destroy:function(){

                                this.inherited(arguments);
                            }
                        };
                        var widgetArgs  ={
                            checked:!col.hidden,
                            iconClass:icon,
                            style:'float:inherit;'
                        };

                        var _visibilityMixin = {
                            widgetClass:declare.classFactory('_Checked', [CheckedMenuItem,_ActionValueWidgetMixin], null, widgetImplementation ,null),
                            widgetArgs:widgetArgs
                        };

                        action.setVisibility(types.ACTION_VISIBILITY_ALL,_visibilityMixin);

                        label = action.label.replace('Show ','');


                        //for ribbons we collapse into 'Checkboxes'
                        action.setVisibility(VISIBILITY.RIBBON,{
                            widgetClass:declare.classFactory('_CheckedGroup', [ActionValueWidget], null,{
                                iconClass:"",
                                postMixInProperties: function() {
                                    this.inherited(arguments);
                                    this.checked = this.item.get('value') == true;
                                },
                                startup:function(){
                                    this.inherited(arguments);
                                    this.widget.on('change', function (val) {
                                        thiz.showColumn(id,val);
                                    }.bind(this));
                                }
                            } ,null),
                            widgetArgs:{
                                renderer:CheckBox,
                                checked:!col.hidden,
                                label:action.label.replace('Show ','')
                            }
                        });

                    }
                }));

                */

            }
            var subRows = this.subRows,
                first = true,
                srLength, cLength, sr, c;
            for (sr = 0, srLength = subRows.length; sr < srLength; sr++) {
                for (c = 0, cLength = subRows[sr].length; c < cLength; c++) {
                    _createEntry(subRows[sr][c]);
                    if (first) {
                        first = false;
                    }
                }
            }
            return columnActions;

        },
        resize:function(){
            this.inherited(arguments);
            this._checkHiddenColumns();
        },
        _checkHiddenColumns:function(){
            var subRows = this.subRows,
                srLength, cLength, sr, c,
                totalWidth = $(this.domNode).width();

            for (sr = 0, srLength = subRows.length; sr < srLength; sr++) {
                for (c = 0, cLength = subRows[sr].length; c < cLength; c++) {
                    var col = subRows[sr][c];
                    if(col.minWidth){
                        if(totalWidth < col.minWidth){
                            if(!col.unhidable) {
                                this.showColumn(col.id,false);
                            }
                        }else{
                            this.showColumn(col.id,true);
                        }
                    }
                }
            }
        },
        startup:function(){
            if(this._started){
                return;
            }

            this._columnHiderCheckboxes = {};
            this._columnHiderRules = {};
            var res = this.inherited(arguments);
            this._checkHiddenColumns();
            var subRows = this.subRows,
                srLength, cLength, sr, c,
                thiz = this;

            for (sr = 0, srLength = subRows.length; sr < srLength; sr++) {
                for (c = 0, cLength = subRows[sr].length; c < cLength; c++) {

                    var col = subRows[sr][c],
                        id = col.id;

                    if (col.hidden===true) {
                        // Hide the column (reset first to avoid short-circuiting logic)
                        col.hidden = false;
                        thiz._hideColumn(id);
                        col.hidden = true;
                    }
                }
            }
            if(this.getActionStore){
                this.getActionStore().on('update',function(evt){
                    var action = evt.target;
                    if(action.command.indexOf('View/Columns')!==-1){
                        var col = action.column;
                        thiz.showColumn(col.id,action.get('value'));
                        thiz.onAfterAction(action);

                    }
                });
            }
            return res;

        },
		left: function (cell, steps) {
			return this.right(cell, -steps);
		},
		right: function (cell, steps) {
			if (!cell.element) {
				cell = this.cell(cell);
			}
			var nextCell = this.inherited(arguments),
				prevCell = cell;

			// Skip over hidden cells
			while (nextCell.column.hidden) {
				nextCell = this.inherited(arguments, [nextCell, steps > 0 ? 1 : -1]);
				if (prevCell.element === nextCell.element) {
					// No further visible cell found - return original
					return cell;
				}
				prevCell = nextCell;
			}
			return nextCell;
		},
		isColumnHidden: function (id) {
			// summary:
			//		Convenience method to determine current hidden state of a column
			return !!this._columnHiderRules[id];
		},
		_hideColumn: function (id) {
			// summary:
			//		Hides the column indicated by the given id.

			// Use misc function directly, since we clean these up ourselves anyway
			var grid = this,
                domId = this.template ? this.template.id : this.domNode.id,
                selectorPrefix = '#' + misc.escapeCssIdentifier(domId) + ' .dgrid-column-',
				tableRule; // used in IE8 code path

			if (this._columnHiderRules[id]) {
				return;
			}

			this._columnHiderRules[id] = misc.addCssRule(selectorPrefix + misc.escapeCssIdentifier(id, '-'), 'display: none;');
            
			if (has('ie') === 8 || has('ie') === 10) {
				// Work around IE8 display issue and IE10 issue where
				// header/body cells get out of sync when ColumnResizer is also used
				tableRule = misc.addCssRule('.dgrid-row-table', 'display: inline-table;');
				window.setTimeout(function () {
					tableRule.remove();
					grid.resize();
				}, 0);
			}
		},
		_showColumn: function (id) {
			// summary:
			//		Shows the column indicated by the given id
			//		(by removing the rule responsible for hiding it).

			if (this._columnHiderRules[id]) {
				this._columnHiderRules[id].remove();
				delete this._columnHiderRules[id];
			}
		},
        showColumn:function(id,show){
            if(this.isColumnHidden(id)){
                if(show) {
                    this._showColumn(id);
                }
            }else if(!show){
                this._hideColumn(id);
            }
        }
	});
});

},
'xgrid/Selection':function(){
/** @module xgrid/Selection **/
define([
    "xdojo/declare",
    "xdojo/has",
    'xide/types',
    'xide/utils',
    'dgrid/Selection',
    'dojo/dom-class',
    'dojo/on',
    'dojo/Deferred',
    'xide/lodash',
    'xide/$'
], function (declare, has, types, utils, Selection, domClass, on, Deferred, _, $) {

    /////////////////////////////////////////////////////////////////////
    //
    //  Utils
    //
    //
    /**
     * Event filter
     * @param event
     * @returns {string|boolean}
     */
    function handledEvent(event) {
        // Text boxes and other inputs that can use direction keys should be ignored
        // and not affect cell/row navigation
        var target = event.target;
        return target.type && (event.keyCode === 32);
    }

    /**
     *
     * @param selection to ids
     * @returns {string[]}
     */
    function rows(selection) {
        var result = [];
        if (selection && selection.rows) {
            selection.rows.forEach(function (row) {
                result.push(row.id);
            });
        }
        return result;
    }

    /**
     *
     * @param arrays
     * @returns {*|Array}
     */
    function allArraysAlike(arrays) {
        return _.all(arrays, function (array) {
            return array.length == arrays[0].length && _.difference(array, arrays[0]).length == 0;
        });
    }

    /**
     *
     * @param lastSelection
     * @param newSelection
     * @returns {*|Array}
     */
    function equals(lastSelection, newSelection) {
        var cSelected = rows(lastSelection);
        var nSelected = rows(newSelection);
        return allArraysAlike([cSelected, nSelected]);
    }

    /**
     *
     * @param items
     * @param now
     * @param idProperty
     * @returns {boolean}
     */
    function isSame(items, now, idProperty) {
        var newSelection = items ? items.map(function (item) {
            return item ? item.data || item : {};
        }) : [];
        var idsNew = newSelection.map(function (x) { return x[idProperty]; });
        var idsNow = now.map(function (x) { return x[idProperty]; });
        return (idsNew.join(',') === idsNow.join(','));
    }

    /**
     *
     * @param self {module:xgrid/Base}
     */
    function clearFocused(self) {
        $(self.domNode).find('.dgrid-focus').each(function (i, el) {
            $(el).removeClass('dgrid-focus');
        });
    }

    var _debug = false;
    var debugSelect = false;
    /**
     * @class module:xgrid/Selection
     * @lends module:xgrid/Base
     */
    var Implementation = {
        _lastSelection: null,
        _lastFocused: null,
        _refreshInProgress: null,
        __lastLast: null,
        __lastFirst: null,
        /**
         * Mute any selection events.
         */
        _muteSelectionEvents: true,
        selectAll: function (filter) {
            this.select(this.getRows(filter), null, true, {
                append: false,
                delay: 1
            });
        },
        /**
         *
         * @param state
         * @returns {object}
         */
        setState: function (state) {
            state && state.selection && state.selection.selection && this.select(state.selection.selection, null, true, {
                expand: true,
                append: false,
                scrollInto: true
            }, 'restore');
            return this.inherited(arguments);
        },
        /**
         *
         * @param state
         * @returns {object}
         */
        getState: function (state) {
            state = this.inherited(arguments) || {};
            var selection = this._preserveSelection();
            var thisState = {
                selection: []
            };
            var collection = this.collection;
            var idProp = collection.idProperty;
            if (selection.selection && idProp) {
                _.each(selection.selection, function (item) {
                    if (item && item[idProp]) {
                        thisState.selection.push(item[idProp]);
                    }
                });
            }
            if (selection.focused) {
                thisState.focused = selection.focused.path;
            }
            state.selection = thisState;
            return state;
        },
        /**
         *
         * @param restoreSelection
         * @returns {*}
         */
        refresh: function (restoreSelection) {
            if (!this.isRendered()) {
                return false;
            }
            if (this._refreshInProgress) {
                return this._refreshInProgress;
            }

            var _restore = restoreSelection !== false ? this._preserveSelection() : null,
                thiz = this,
                active = this.isActive(),
                res = this.inherited(arguments);

            this._refreshInProgress = res;

            res && res.then && res.then(function () {
                thiz._refreshInProgress = null;
                active && _restore && thiz._restoreSelection(_restore, 1, !active, 'restore');
            });
            return res;
        },
        /**
         * Normalize an item
         * @param what
         * @returns {*}
         * @private
         */
        _normalize: function (what) {
            if (!what) {
                return null;
            }
            if (!what.element) {
                what = this.cell(what);
            }
            if (what && what.row) {
                what = what.row;
            }
            return what;
        },
        /**
         * save deselect
         */
        deselectAll: function () {
            if (!this._lastSelection) {
                return;
            }
            this.clearSelection();
            this._lastSelection = null;
            this._lastFocused = null;
            $(this.domNode).find('.dgrid-focus').each(function (i, el) {
                $(el).removeClass('dgrid-focus');
            });
            this._emit('selectionChanged', {
                selection: [],
                why: "clear",
                source: 'code'
            });
        },
        invertSelection: function (items) {
            var selection = items || this._getSelection() || [];
            var newSelection = [],
                all = this.getRows();
            _.each(all, function (data) {
                if (selection.indexOf(data) === -1) {
                    newSelection.push(data);
                }
            });
            return this.select(newSelection, null, true, {
                append: false
            });
        },
        runAction: function (action) {
            if (_.isString(action)) {
                action = this.getActionStore().getSync(action);
            }
            if (action.command === 'File/Select/None') {
                this.deselectAll();
                return true;
            }
            if (action.command === 'File/Select/All') {
                this.selectAll();
                return true;
            }
            if (action.command === 'File/Select/Invert') {
                return this.invertSelection();
            }
            return this.inherited(arguments);
        },
        _preserveSelection: function () {
            this.__lastSelection = this._getSelection();
            this._lastFocused = this.getFocused();
            return {
                selection: this._getSelection(),
                focused: this.getFocused()
            };
        },
        _restoreSelection: function (what, delay, silent, reason) {
            var lastFocused = what ? what.focused : this._lastFocused;
            var lastSelection = what ? what.selection : this.__lastSelection;
            if (_.isEmpty(lastSelection)) {
                lastFocused = null;
                this._lastFocused = null;
            } else {
                //restore:
                var dfd = this.select(lastSelection, null, true, {
                    silent: silent != null ? silent : true,
                    append: false,
                    delay: delay != null ? delay : 0
                }, reason);

                if (lastFocused && this.isActive()) {
                    this.focus(this.row(lastFocused));
                }
                return dfd;
            }
        },
        /**
         * get previous item
         * @param from
         * @param domNode
         * @param skipSelected
         * @returns {*}
         */
        getPrevious: function (from, domNode, skipSelected) {
            from = from || this.getFocused(domNode);
            from = this._normalize(from);
            var nextNode = this.cell(this._move(from, -1, "dgrid-row"));
            if (nextNode && nextNode.row) {
                nextNode = nextNode.row[domNode ? 'element' : 'data'];
                if (skipSelected === true) {
                    if (this.isSelected(nextNode)) {
                        //nothing previous here
                        if (from && from.data && from.data == nextNode) {
                            return null;
                        }
                        var _nextNode = this.getPrevious(nextNode, domNode, skipSelected);
                        if (_nextNode) {
                            return _nextNode;
                        }
                    }
                }
            }
            return nextNode;
        },
        /**
         * get next item
         * @param from
         * @param domNode
         * @param skipSelected
         * @returns {*}
         */
        getNext: function (from, domNode, skipSelected) {
            from = from || this.getFocused(domNode);
            from = this._normalize(from);
            var nextNode = this.cell(this._move(from, 1, "dgrid-row"));
            if (nextNode && nextNode.row) {
                nextNode = nextNode.row[domNode ? 'element' : 'data'];
                if (skipSelected === true) {
                    if (this.isSelected(nextNode)) {
                        //nothing previous here
                        if (from && from.data && from.data == nextNode) {
                            return null;
                        }
                        var _nextNode = this.getNext(nextNode, domNode, skipSelected);
                        if (_nextNode) {
                            return _nextNode;
                        }
                    }
                }
            }
            return nextNode;
        },
        /**
         *
         * @param filterFunction
         * @returns selection {Object[] | NULL }
         */
        getSelection: function (filterFunction) {
            return this._getSelection(filterFunction);
        },
        /**
         *
         * @param filterFunction
         * @returns selection {Object[] | NULL }
         */
        _getSelection: function (filterFunction) {
            var result = [];
            var collection = this.collection;
            if (collection) {
                for (var id in this.selection) {
                    var item = this.collection.getSync(id);
                    item && result.push(item);
                }
                if (filterFunction) {
                    return result.filter(filterFunction);
                }
            }
            return result;
        },
        /**
         *
         * @param filterFunction
         * @returns selection {Object[] | NULL }
         */
        _getSelected: function () {
            return $('.dgrid-selected', this.domNode);
        },
        /**
         *
         * @param filter
         * @returns {*}
         */
        getSelectedItem: function (filter) {
            var _selection = this.getSelection(filter);
            if (_selection.length === 1) {
                return _selection[0];
            }
            return null;
        },
        /**
         * Override std::postCreate
         * @returns {*}
         */
        postCreate: function () {
            var thiz = this;
            if (this.options[types.GRID_OPTION.CLEAR_SELECTION_ON_CLICK] === true) {
                var clickHandler = function (evt) {
                    if (evt && evt.target && domClass.contains(evt.target, 'dgrid-content')) {
                        this.deselectAll();
                    }
                }.bind(this);
                this.on("click", function (evt) {
                    clickHandler(evt);
                }.bind(this));
            }
            this.on("dgrid-select", function (data) {
                if (!equals(thiz._lastSelection, data)) {
                    delete thiz._lastSelection;
                    thiz._lastSelection = data;
                    thiz._emit('selectionChanged', {
                        selection: thiz._getSelection(),
                        why: "select",
                        source: data.parentType
                    })
                }
            });
            return this.inherited(arguments);
        },
        /**
         * Override dgrid/Selection::_fireSelectionEvents
         * @returns {*}
         * @private
         */
        _fireSelectionEvents: function () {
            if (this._muteSelectionEvents === true) {
                return;
            }
            return this.inherited(arguments);
        },
        __select: function (items, toRow, select, dfd, reason) {
            _.each(items, function (item) {
                if (item) {
                    var _row = this.row(item);
                    if (_row) {
                        this._select(_row, toRow, select);
                    }
                }
            }, this);
            dfd && dfd.resolve(items);
            this._muteSelectionEvents = false;
            this._fireSelectionEvents();
            var rows = this.getRows();
            if (rows && rows.length && items && items.length && select && reason && reason !== 'mouse') {
                //trigger bounce if we hit
                var _last = items[items.length - 1];
                if (rows[rows.length - 1] == _last) {
                    if (this.__lastLast && this.__lastLast == _last) {
                        reason.indexOf('pointer') === -1 && this._emit('bounced', {
                            direction: 1,
                            item: _last
                        });
                        return;
                    }
                    this.__lastLast = _last;
                } else {
                    this.__lastLast = null;
                }


                var _first = items[0];
                if (rows[0] == _first) {
                    if (this.__lastFirst && this.__lastFirst == _first) {
                        reason.indexOf('pointer') === -1 && this._emit('bounced', {
                            direction: -1,
                            item: _first
                        })
                        return;
                    }
                    this.__lastFirst = _first;
                } else {
                    this.__lastFirst = null;
                }
            } else {
                this.__lastFirst = null;
            }
        },
        /**
         * Overrides dgrid selection
         * @param mixed
         * @param toRow {object|null} preserve super
         * @param select {boolean|null} preserve super
         * @param options {object}
         * @param options.focus {boolean}
         * @param options.silent {boolean}
         * @param options.append {boolean}
         * @param options.expand {boolean}
         * @param options.scrollInto {boolean}
         * @param reason {string} the origin event's type
         * returns dojo/Deferred
         */
        select: function (mixed, toRow, select, options, reason) {
            clearTimeout(this._selectTimer);
            this._selectTimer = null;
            var isMouse = reason === 'mouse',
                isPrioritySelect = isMouse || reason === 'update',
                isActive = this.isActive(),
                def = new Deferred();

            reason = reason || '';

            //sanitize/defaults
            options = options || {};

            if (isPrioritySelect) {
                isActive = true;
            }
            if (isMouse) {
                options.focus = true;
            }
            select = select === null ? true : select;
            var delay = options.delay || 0,
                self = this,
                coll = this.collection,
                idProperty = coll.idProperty;

            //silence selection change (batch or state restoring job)
            if (options.silent === true) {
                self._muteSelectionEvents = true;
            }

            //normalize to array
            var items = utils.isArray(mixed) ? mixed : [mixed];
            if (_.isEmpty(items)) {
                return;
            }
            var _newItems = [];

            //indices to items
            if (_.isNumber(items[0])) {
                var rows = self.getRows();
                _.each(items, function (item) {
                    _newItems.push(rows[item]);
                });
                items = _newItems;
            } else if (_.isString(items[0])) {
                _.each(items, function (item) {
                    var _item = coll.getSync(item);
                    if (_item) {
                        _newItems.push(_item);
                    }
                });

                items = _newItems;
            } else if (items && items[0] && items[0].tagName) {
                _.each(items, function (item) {
                    _newItems.push(self.row(item).data);
                });
                items = _newItems;
            }

            if (!items.length) {
                if (has('debug')) {
                    _debug && console.log('nothing to select!');
                }
                def.resolve();
                return def;
            }


            if (has('debug')) {
                debugSelect && console.log('selected : ', _.map(items, "name"));
            }

            var _last = this._lastSelection ? this._lastSelection.rows : [];
            var now = _last.map(function (x) { return x.data; });

            var isEqual = isSame(items, now, idProperty);

            //store update
            if (reason === 'update' && select) {
                options.focus = true;
                options.append = false;
                options.delay = 1;
                //this.focus();
            }

            if (reason === 'dgrid-cellfocusin') {
                options.focus = true;
            }

            //clear previous selection
            if (options.append === false && select && !isEqual) {
                self.clearSelection(items);
                clearFocused(self);
            }

            if (isEqual && (reason === 'update' || reason === 'dgrid-cellfocusin')) {
                if (options.focus) {
                    clearFocused(self);
                    self.focus(items[0]);
                }
                return;
            }

            //focus
            if (options.focus === true) {
                if (options.expand) {
                    if (!self.isRendered(items[0])) {
                        self._expandTo(items[0]);
                    }
                }
            }
            if (options.expand) {
                if (!self.isRendered(items[0])) {
                    self._expandTo(items[0]);
                }
            }
            if (options.scrollInto && reason !== 'restore') {
                var row = this.row(items[0]);
                if (row.element) {
                    row.element.scrollIntoView();
                }
            }

            if (delay && items.length) {
                this._selectTimer = setTimeout(function () {
                    if (self.destroyed || !self.collection) {
                        return;
                    }
                    if (options.append === false) {
                        self.clearSelection();
                    }
                    clearFocused(self);
                    self.focus(items[0], false);
                    self.__select(items, toRow, select, def, reason);
                }, delay);
            } else {
                self.__select(items, toRow, select, def, reason);
            }
            return def;
        },

        _setLast: function (selection) {
            var _ids = [];
            for (var i = 0; i < selection.length; i++) {
                var obj = selection[i];
                _ids.push(this.collection.getIdentity(obj));
            }
        },
        isExpanded: function (item) {
            item = this._normalize('root');
            return !!this._expanded[item.id];
        },
        _expandTo: function (item) {
            if (!item) {
                return;
            }
            var store = this.collection;
            if (_.isString(item)) {
                item = store.getSync(item);
            }
            var parent = store.getSync(item[store.parentField]) || item.getParent ? item.getParent() : null;
            if (parent) {
                if (!this.isRendered(parent)) {
                    this._expandTo(parent);
                } else {
                    if (!this.isExpanded(parent)) {
                        this.expand(parent, true, true);
                    }
                    if (!this.isExpanded(item)) {
                        this.expand(item, true, true);
                    }
                }
            }
        },
        startup: function () {
            var result = this.inherited(arguments);
            //we want keyboard navigation also when nothing is selected
            this.addHandle('keyup', on(this.domNode, 'keyup', function (event) {
                // For now, don't squash browser-specific functionality by letting
                // ALT and META function as they would natively
                if (event.metaKey || event.altKey) {
                    return;
                }
                var handler = this['keyMap'][event.keyCode];
                // Text boxes and other inputs that can use direction keys should be ignored
                // and not affect cell/row navigation
                if (handler && !handledEvent(event) && this._getSelection().length == 0) {
                    handler.call(this, event);
                }
            }.bind(this)));
            return result;
        }
    };
    //package via declare
    var _class = declare('xgrid.Selection', Selection, Implementation);
    _class.Implementation = Implementation;

    return _class;
});
},
'xgrid/Toolbar':function(){
/** @module xgrid/Toolbar **/
define([
    "xdojo/declare",
    'xide/utils',
    'xide/types',
    'xide/widgets/ActionToolbar'
], function (declare,utils,types,ActionToolbar) {
    /**
     *
     * @class module:xgrid/Toolbar
     */
    var Implementation = {
        _toolbar:null,
        toolbarClass:null,
        toolbarInitiallyHidden:false,
        runAction:function(action){
            if(action.command==types.ACTION.TOOLBAR){
                this.showToolbar(this._toolbar==null);
                return true;
            }
            return this.inherited(arguments);
        },
        getToolbar:function(){
            return this._toolbar;
        },
        /**
         *
         * @param show
         * @param toolbarClass
         * @param where
         * @param setEmitter
         * @param args
         * @returns {null}
         */
        showToolbar:function(show,toolbarClass,where,setEmitter,args){
            //remember toolbar class
            toolbarClass = toolbarClass || this.toolbarClass;
            if(toolbarClass) {
                this.toolbarClass = toolbarClass;
            }
            if(show==null){
                show = this._toolbar==null;
            }
            if(show && !this._toolbar){
                var toolbar = utils.addWidget(toolbarClass || ActionToolbar ,utils.mixin({
                        style:'min-height:30px;height:auto;width:100%'
                    },args),this,where||this.header,true);

                if(setEmitter !==false) {
                    toolbar.addActionEmitter(this);
                    //at this point the actions are rendered!
                    toolbar.setActionEmitter(this);
                    this.refreshActions && this.refreshActions();
                }
                this._toolbar = toolbar;
                this.add && this.add(toolbar);
                this._emit('showToolbar',toolbar);
            }
            if(!show && this._toolbar){
                utils.destroy(this._toolbar,true,this);
                $(where||this.header).css('height','auto');
            }
            this.resize();
            return this._toolbar;
        },
        getState:function(state) {
            state = this.inherited(arguments) || {};
            state.toolbar = this._toolbar!==null;
            return state;
        },
        setState:function(state) {
            if(state && state.toolbar){
                this.showToolbar(state.toolbar);
            }
            return this.inherited(arguments);
        },
        startup:function(){
            var thiz = this;
            if(this._started){
                return;
            }
            this._on('onAddActions', function (evt) {
                var actions = evt.actions,
                    action = types.ACTION.TOOLBAR;
                if(!evt.store.getSync(action) && this.hasPermission(action)) {
                    actions.push(thiz.createAction({
                        label: 'Toolbar',
                        command: action,
                        icon: types.ACTION_ICON.TOOLBAR,
                        tab: 'View',
                        group: 'Show',
                        keycombo:['ctrl b'],
                        mixin:{
                            actionType:'multiToggle',
                            value:false,
                            id:utils.createUUID()
                        },
                        onCreate:function(action){
                            action.value = thiz._toolbar!==null;
                        },
                        onChange:function(property,value){
                            thiz.showToolbar(value);
                            thiz.onAfterAction(types.ACTION.TOOLBAR);
                        }
                    }));
                }
            });
            this.inherited(arguments);
            this.showToolbar(!this.toolbarInitiallyHidden);
        }
    };
    //package via declare
    var _class = declare('xgrid.Toolbar',null,Implementation);
    _class.Implementation = Implementation;
    return _class;
});
},
'xgrid/ContextMenu':function(){
/** module:xgrid/ContextMenu **/
define([
    'dojo/_base/declare',
    'xide/utils',
    'xide/widgets/ContextMenu',
    'xide/types'
], function (declare, utils, ContextMenu, types) {
    return declare("xgrid.ContextMenu", null, {
        contextMenu: null,
        getContextMenu: function () {
            return this.contextMenu;
        },
        _createContextMenu: function () {
            var _ctorArgs = this.contextMenuArgs || {};
            var node = this.contentNode;
            var mixin = {
                owner: this,
                delegate: this,
                _actionFilter: {
                    quick: true
                }
            };
            utils.mixin(_ctorArgs, mixin);
            var contextMenu = new ContextMenu(_ctorArgs, node);
            contextMenu.openTarget = node;
            //@TODO: remove back dijit compat
            //contextMenu.limitTo=null;
            contextMenu.init({preventDoubleContext: false});
            contextMenu._registerActionEmitter(this);
            $(node).one('contextmenu',function(e){
                e.preventDefault();
                if(!this.store) {
                    contextMenu.setActionStore(this.getActionStore(), this);
                }
            }.bind(this));
            this.contextMenu = contextMenu;
            this.add(contextMenu);
        },
        startup: function () {
            if (this._started) {
                return;
            }
            this.inherited(arguments);
            if (this.hasPermission(types.ACTION.CONTEXT_MENU)) {
                this._createContextMenu();
            }
        }
    });
});
},
'xgrid/Keyboard':function(){
define([
	'dojo/_base/declare',
	'dojo/aspect',
	'dojo/dom-class',
	'dojo/on',
	'dojo/_base/lang',
	'dojo/has',
	'dgrid/util/misc',
	'dojo/_base/sniff',
	'dcl/dcl'
], function (declare, aspect, domClass, on, lang, has, miscUtil,dcl) {

	var delegatingInputTypes = {
			checkbox: 1,
			radio: 1,
			button: 1
		},
		hasGridCellClass = /\bdgrid-cell\b/,
		hasGridRowClass = /\bdgrid-row\b/,
		_debug = false;

    has.add("dom-contains", function(global, doc, element){
        return !!element.contains; // not supported by FF < 9
    });

    function contains(parent, node){
        // summary:
        //		Checks to see if an element is contained by another element.

        if(has("dom-contains")){
            return parent.contains(node);
        }else{
            return parent.compareDocumentPosition(node) & 8 /* DOCUMENT_POSITION_CONTAINS */;
        }
    }

	var _upDownSelect = function(event,who,steps) {

		var prev     = steps < 0,
			selector = prev ? 'first:' : 'last',
			s, n, sib, top, left;

		var _current = who.row(event).element;
		var sel = $(_current); // header reports row as undefined

		var clDisabled = 'ui-state-disabled';
		function sibling(n, direction) {
			return n[direction+'All']('[id]:not(.'+clDisabled+'):not(.dgrid-content-parent):first');
		}
		var hasLeftRight=false;
		if (sel.length) {
			var next = who.up(who._focusedNode,1, true);
			s = sel;
			sib = $(next.element);
			if (!sib.length) {
				// there is no sibling on required side - do not move selection
				n = s;
			} else if (hasLeftRight) {//done somewhere else
				n = sib;
			} else {
				// find up/down side file in icons view
				top = s.position().top;
				left = s.position().left;
				n = s;
				if (prev) {
					do {
						n = n.prev('[id]');
					} while (n.length && !(n.position().top < top && n.position().left <= left));

					if (n.is('.'+clDisabled)) {
						n = sibling(n, 'next');
					}
				} else {
					do {
						n = n.next('[id]');
					} while (n.length && !(n.position().top > top && n.position().left >= left));

					if (n.is('.'+clDisabled)) {
						n = sibling(n, 'prev');
					}
				}
			}
		}
		return n;
	};
	var _rightLeftSelect = function(event,who,steps) {

		var prev     = steps < 0,
			selector = prev ? 'first:' : 'last',
			s, n, sib, top, left;

		var _current = who.row(event).element;
		var sel = $(_current); // header reports row as undefined

		var clDisabled = 'ui-state-disabled';
		function sibling(n, direction) {
			return n[direction+'All']('[id]:not(.'+clDisabled+'):not(.dgrid-content-parent):first');
		}
		var hasLeftRight=true;
		if (sel.length) {
			var next = who.up(who._focusedNode,1, true);
			s = sel;
			sib = $(next.element);
			if (!sib.length) {
				// there is no sibling on required side - do not move selection
				n = s;
			} else if (hasLeftRight) {//done somewhere else
				n = sib;
			} else {
				// find up/down side file in icons view
				top = s.position().top;
				left = s.position().left;
				n = s;
				if (prev) {
					do {
						n = n.prev('[id]');
					} while (n.length && !(n.position().top < top && n.position().left <= left));

					if (n.is('.'+clDisabled)) {
						n = sibling(n, 'next');
					}
				} else {
					do {
						n = n.next('[id]');
					} while (n.length && !(n.position().top > top && n.position().left >= left));

					if (n.is('.'+clDisabled)) {
						n = sibling(n, 'prev');
					}
				}
			}
		}
		return n;
	};

	var Implementation = {
		// summary:
		//		Adds keyboard navigation capability to a list or grid.

		// pageSkip: Number
		//		Number of rows to jump by when page up or page down is pressed.
		pageSkip: 10,

		tabIndex: -1,

		// keyMap: Object
		//		Hash which maps key codes to functions to be executed (in the context
		//		of the instance) for key events within the grid's body.
		keyMap: null,

		// headerKeyMap: Object
		//		Hash which maps key codes to functions to be executed (in the context
		//		of the instance) for key events within the grid's header row.
		headerKeyMap: null,

		postMixInProperties: function () {
			this.inherited(arguments);

			if (!this.keyMap) {
				this.keyMap = lang.mixin({}, Implementation.defaultKeyMap);
			}
			if (!this.headerKeyMap) {
				this.headerKeyMap = lang.mixin({}, Implementation.defaultHeaderKeyMap);
			}
		},

		postCreate: function () {
			this.inherited(arguments);
			var grid = this;

			function handledEvent(event) {
				// Text boxes and other inputs that can use direction keys should be ignored
				// and not affect cell/row navigation
				var target = event.target;
				return target.type && (!delegatingInputTypes[target.type] || event.keyCode === 32);
			}

			function enableNavigation(areaNode) {

				var cellNavigation = grid.cellNavigation,
					isFocusableClass = cellNavigation ? hasGridCellClass : hasGridRowClass,
					isHeader = areaNode === grid.headerNode,
					initialNode = areaNode;

				function initHeader() {
					if (grid._focusedHeaderNode) {
						// Remove the tab index for the node that previously had it.
						grid._focusedHeaderNode.tabIndex = -1;
					}
					if (grid.showHeader) {
						if (cellNavigation) {
							// Get the focused element. Ensure that the focused element
							// is actually a grid cell, not a column-set-cell or some
							// other cell that should not be focused
							var elements = grid.headerNode.getElementsByTagName('th');
							for (var i = 0, element; (element = elements[i]); ++i) {
								if (isFocusableClass.test(element.className)) {
									grid._focusedHeaderNode = initialNode = element;
									break;
								}
							}
						}
						else {
							grid._focusedHeaderNode = initialNode = grid.headerNode;
						}

						// Set the tab index only if the header is visible.
						if (initialNode) {
							initialNode.tabIndex = grid.tabIndex;
						}
					}
				}

				if (isHeader) {
					// Initialize header now (since it's already been rendered),
					// and aspect after future renderHeader calls to reset focus.
					initHeader();
					aspect.after(grid, 'renderHeader', initHeader, true);
				}
				else {
					aspect.after(grid, 'renderArray', function (rows) {
						// summary:
						//		Ensures the first element of a grid is always keyboard selectable after data has been
						//		retrieved if there is not already a valid focused element.

						var focusedNode = grid._focusedNode || initialNode;

						// do not update the focused element if we already have a valid one
						if (isFocusableClass.test(focusedNode.className) && miscUtil.contains(areaNode, focusedNode)) {
							return rows;
						}

						// ensure that the focused element is actually a grid cell, not a
						// dgrid-preload or dgrid-content element, which should not be focusable,
						// even when data is loaded asynchronously
						var elements = areaNode.getElementsByTagName('*');
						for (var i = 0, element; (element = elements[i]); ++i) {
							if (isFocusableClass.test(element.className)) {
								focusedNode = grid._focusedNode = element;
								break;
							}
						}

						focusedNode.tabIndex = grid.tabIndex;
						return rows;
					});
				}

				grid._listeners.push(on(areaNode, 'mousedown', function (event) {
					if (!handledEvent(event)) {
						grid._focusOnNode(event.target, isHeader, event);
					}
				}));

				grid._listeners.push(on(areaNode, 'keydown', function (event) {
					//console.log('keyboardkey down : ',event);
					// For now, don't squash browser-specific functionalities by letting
					// ALT and META function as they would natively
					if (event.metaKey || event.altKey) {
						return;
					}

					var handler = grid[isHeader ? 'headerKeyMap' : 'keyMap'][event.keyCode];

					// Text boxes and other inputs that can use direction keys should be ignored
					// and not affect cell/row navigation
					if (handler && !handledEvent(event)) {
						handler.call(grid, event);
					}
				}));
			}

			if (this.tabableHeader) {
				enableNavigation(this.headerNode);
				on(this.headerNode, 'dgrid-cellfocusin', function () {
					grid.scrollTo({ x: this.scrollLeft });
				});
			}
			enableNavigation(this.contentNode);

			this._debouncedEnsureScroll = miscUtil.debounce(this._ensureScroll, this);
		},

		removeRow: function (rowElement) {
			if (!this._focusedNode) {
				// Nothing special to do if we have no record of anything focused
				return this.inherited(arguments);
			}

			var self = this,
				isActive = document.activeElement === this._focusedNode,

					focusedTarget = this[this.cellNavigation ? 'cell' : 'row'](this._focusedNode);

            if(!focusedTarget){
                console.error('no focus target');
                return this.inherited(arguments);
            }


				var focusedRow = focusedTarget.row || focusedTarget,
				sibling;
			rowElement = rowElement.element || rowElement;

			// If removed row previously had focus, temporarily store information
			// to be handled in an immediately-following insertRow call, or next turn
			if (rowElement === focusedRow.element) {
				sibling = this.down(focusedRow, true);

				// Check whether down call returned the same row, or failed to return
				// any (e.g. during a partial unrendering)
				if (!sibling || sibling.element === rowElement) {
					sibling = this.up(focusedRow, true);
				}

				this._removedFocus = {
					active: isActive,
					rowId: focusedRow.id,
					columnId: focusedTarget.column && focusedTarget.column.id,
					siblingId: !sibling || sibling.element === rowElement ? undefined : sibling.id
				};

				// Call _restoreFocus on next turn, to restore focus to sibling
				// if no replacement row was immediately inserted.
				// Pass original row's id in case it was re-inserted in a renderArray
				// call (and thus was found, but couldn't be focused immediately)
				setTimeout(function () {
					if (self._removedFocus) {
						self._restoreFocus(focusedRow.id);
					}
				}, 0);

				// Clear _focusedNode until _restoreFocus is called, to avoid
				// needlessly re-running this logic
				this._focusedNode = null;
			}

			this.inherited(arguments);
		},

		insertRow: function () {
			var rowElement = this.inherited(arguments);
			if (this._removedFocus && !this._removedFocus.wait) {
				this._restoreFocus(rowElement);
			}
			return rowElement;
		},

		_restoreFocus: function (row) {
			// summary:
			//		Restores focus to the newly inserted row if it matches the
			//		previously removed row, or to the nearest sibling otherwise.

			var focusInfo = this._removedFocus,
				newTarget,
				cell;

			row = row && this.row(row);
			newTarget = row && row.element && row.id === focusInfo.rowId ? row :
				typeof focusInfo.siblingId !== 'undefined' && this.row(focusInfo.siblingId);

			if (newTarget && newTarget.element) {
				if (!newTarget.element.parentNode.parentNode) {
					// This was called from renderArray, so the row hasn't
					// actually been placed in the DOM yet; handle it on the next
					// turn (called from removeRow).
					focusInfo.wait = true;
					return;
				}
				// Should focus be on a cell?
				if (typeof focusInfo.columnId !== 'undefined') {
					cell = this.cell(newTarget, focusInfo.columnId);
					if (cell && cell.element) {
						newTarget = cell;
					}
				}
				if (focusInfo.active && newTarget.element.offsetHeight !== 0) {
					// Row/cell was previously focused and is visible, so focus the new one immediately
					this._focusOnNode(newTarget, false, null);
				}
				else {
					// Row/cell was not focused or is not visible, but we still need to
					// update _focusedNode and the element's tabIndex/class
					domClass.add(newTarget.element, 'dgrid-focus');
					newTarget.element.tabIndex = this.tabIndex;
					this._focusedNode = newTarget.element;
				}
			}

			delete this._removedFocus;
		},

		addKeyHandler: function (key, callback, isHeader) {
			// summary:
			//		Adds a handler to the keyMap on the instance.
			//		Supports binding additional handlers to already-mapped keys.
			// key: Number
			//		Key code representing the key to be handled.
			// callback: Function
			//		Callback to be executed (in instance context) when the key is pressed.
			// isHeader: Boolean
			//		Whether the handler is to be added for the grid body (false, default)
			//		or the header (true).

			// Aspects may be about 10% slower than using an array-based appraoch,
			// but there is significantly less code involved (here and above).
			return aspect.after( // Handle
				this[isHeader ? 'headerKeyMap' : 'keyMap'], key, callback, true);
		},

		_ensureRowScroll: function (rowElement) {
			// summary:
			//		Ensures that the entire row is visible within the viewport.
			//		Called for cell navigation in complex structures.

			var scrollY = this.getScrollPosition().y;
			if (scrollY > rowElement.offsetTop) {
				// Row starts above the viewport
				this.scrollTo({ y: rowElement.offsetTop });
			}
			else if (scrollY + this.contentNode.offsetHeight < rowElement.offsetTop + rowElement.offsetHeight) {
				// Row ends below the viewport
				this.scrollTo({ y: rowElement.offsetTop - this.contentNode.offsetHeight + rowElement.offsetHeight });
			}
		},

		_ensureColumnScroll: function (cellElement) {
			// summary:
			//		Ensures that the entire cell is visible in the viewport.
			//		Called in cases where the grid can scroll horizontally.

			var scrollX = this.getScrollPosition().x;
			var cellLeft = cellElement.offsetLeft;
			if (scrollX > cellLeft) {
				this.scrollTo({ x: cellLeft });
			}
			else {
				var bodyWidth = this.bodyNode.clientWidth;
				var cellWidth = cellElement.offsetWidth;
				var cellRight = cellLeft + cellWidth;
				if (scrollX + bodyWidth < cellRight) {
					// Adjust so that the right side of the cell and grid body align,
					// unless the cell is actually wider than the body - then align the left sides
					this.scrollTo({ x: bodyWidth > cellWidth ? cellRight - bodyWidth : cellLeft });
				}
			}
		},

		_ensureScroll: function (cell, isHeader) {
			// summary:
			//		Corrects scroll based on the position of the newly-focused row/cell
			//		as necessary based on grid configuration and dimensions.

			if(this.cellNavigation && (this.columnSets || this.subRows.length > 1) && !isHeader){
				this._ensureRowScroll(cell.row.element);
			}
			if(this.bodyNode.clientWidth < this.contentNode.offsetWidth){
				this._ensureColumnScroll(cell.element);
			}
		},

		_focusOnNode: function (element,isHeader,event,emit) {
			var focusedNodeProperty = '_focused' + (isHeader ? 'Header' : '') + 'Node',
				focusedNode = this[focusedNodeProperty],
				cellOrRowType = this.cellNavigation ? 'cell' : 'row',
				cell = this[cellOrRowType](element),
				inputs,
				input,
				numInputs,
				inputFocused,
				i;

			element = cell && cell.element;

			if (!element /*|| element==this._focusedNode*/) {
				//console.error('same el');
				return;
			}

			if (this.cellNavigation) {
				inputs = element.getElementsByTagName('input');
				for (i = 0, numInputs = inputs.length; i < numInputs; i++) {
					input = inputs[i];
					if ((input.tabIndex !== -1 || '_dgridLastValue' in input) && !input.disabled) {
						input.focus();
						inputFocused = true;
						break;
					}
				}
			}

			// Set up event information for dgrid-cellfocusout/in events.
			// Note that these events are not fired for _restoreFocus.
			if (event !== null) {
				event = lang.mixin({ grid: this }, event);
				if (event.type) {
					event.parentType = event.type;
				}
				if (!event.bubbles) {
					// IE doesn't always have a bubbles property already true.
					// Opera throws if you try to set it to true if it is already true.
					event.bubbles = true;
				}
			}

			if (focusedNode) {
				// Clean up previously-focused element
				// Remove the class name and the tabIndex attribute
				domClass.remove(focusedNode, 'dgrid-focus');
				focusedNode.removeAttribute('tabindex');

				// Expose object representing focused cell or row losing focus, via
				// event.cell or event.row; which is set depends on cellNavigation.
				if (event) {
					event[cellOrRowType] = this[cellOrRowType](focusedNode);
					on.emit(focusedNode, 'dgrid-cellfocusout', event);
				}
			}
			focusedNode = this[focusedNodeProperty] = element;

			if (event) {
				// Expose object representing focused cell or row gaining focus, via
				// event.cell or event.row; which is set depends on cellNavigation.
				// Note that yes, the same event object is being reused; on.emit
				// performs a shallow copy of properties into a new event object.
				event[cellOrRowType] = cell;
			}

			var isFocusableClass = this.cellNavigation ? hasGridCellClass : hasGridRowClass;
			if (!inputFocused && isFocusableClass.test(element.className)) {

				element.tabIndex = this.tabIndex;
				element.focus();
			}
			domClass.add(element, 'dgrid-focus');


			if (event && emit!==false) {
				on.emit(focusedNode, 'dgrid-cellfocusin', event);
			}

			this._debouncedEnsureScroll(cell, isHeader);
		},

		focusHeader: function (element) {
			this._focusOnNode(element || this._focusedHeaderNode, true);
		},

		focus: function (element,emit) {
			_debug && console.log('focuse : ' + (element ? element.id : ''));
			var node = element || this._focusedNode;
			if (node) {
				if (element==this._focusedNode) {
					//console.error('same el');
					//return;
				}
				this._focusOnNode(node, false,null,emit);
			}
			else {
				this.contentNode.focus();
			}
		}
	};

	// Common functions used in default keyMap (called in instance context)

	var moveFocusVertical = Implementation.moveFocusVertical = function (event, steps) {
		if(this.isThumbGrid){
			var next = _upDownSelect(event,this,steps);
			if(next && next.length){
				this._focusOnNode(next[0], false, event);
				event.preventDefault();
				return;
			}
		}
		var cellNavigation = this.cellNavigation,
			target = this[cellNavigation ? 'cell' : 'row'](event),
			columnId = cellNavigation && target.column.id,
			next = this.down(this._focusedNode, steps, true);

		// Navigate within same column if cell navigation is enabled
		if (cellNavigation) {
			next = this.cell(next, columnId);
		}
		this._focusOnNode(next, false, event);

		event.preventDefault();
	};

	var moveFocusUp = Implementation.moveFocusUp = function (event) {
		moveFocusVertical.call(this, event, -1);
	};

	var moveFocusDown = Implementation.moveFocusDown = function (event) {
		moveFocusVertical.call(this, event, 1);
	};

	var moveFocusPageUp = Implementation.moveFocusPageUp = function (event) {
		moveFocusVertical.call(this, event, -this.pageSkip);
	};

	var moveFocusPageDown = Implementation.moveFocusPageDown = function (event) {
		moveFocusVertical.call(this, event, this.pageSkip);
	};

	var moveFocusHorizontal = Implementation.moveFocusHorizontal = function (event, steps) {

		if (!this.cellNavigation && this.isThumbGrid!==true) {
			return;
		}

		var isHeader = !this.row(event), // header reports row as undefined
			currentNode = this['_focused' + (isHeader ? 'Header' : '') + 'Node'];

		//var _row = this.row(event);
		if(this.isThumbGrid==true){

			var cellNavigation = this.cellNavigation,
				next = this.down(this._focusedNode, steps, true);

			// Navigate within same column if cell navigation is enabled
			this._focusOnNode(next, false, event);
			event.preventDefault();
			return ;
		}

		this._focusOnNode(this.right(currentNode, steps), isHeader, event);
		event.preventDefault();
	};

	var moveFocusLeft = Implementation.moveFocusLeft = function (event) {
		moveFocusHorizontal.call(this, event, -1);
	};

	var moveFocusRight = Implementation.moveFocusRight = function (event) {
		moveFocusHorizontal.call(this, event, 1);
	};

	var moveHeaderFocusEnd = Implementation.moveHeaderFocusEnd = function (event, scrollToBeginning) {
		// Header case is always simple, since all rows/cells are present
		var nodes;
		if (this.cellNavigation) {
			nodes = this.headerNode.getElementsByTagName('th');
			this._focusOnNode(nodes[scrollToBeginning ? 0 : nodes.length - 1], true, event);
		}
		// In row-navigation mode, there's nothing to do - only one row in header

		// Prevent browser from scrolling entire page
		event.preventDefault();
	};

	var moveHeaderFocusHome = Implementation.moveHeaderFocusHome = function (event) {
		moveHeaderFocusEnd.call(this, event, true);
	};

	var moveFocusEnd = Implementation.moveFocusEnd = function (event, scrollToTop) {
		// summary:
		//		Handles requests to scroll to the beginning or end of the grid.

		// Assume scrolling to top unless event is specifically for End key
		var cellNavigation = this.cellNavigation,
			contentNode = this.contentNode,
			contentPos = scrollToTop ? 0 : contentNode.scrollHeight,
			scrollPos = contentNode.scrollTop + contentPos,
			endChild = contentNode[scrollToTop ? 'firstChild' : 'lastChild'];

		if(endChild.className.indexOf('dgrid-extra') > -1){
			endChild = endChild['previousSibling'];
		}

		var	hasPreload = endChild.className.indexOf('dgrid-preload') > -1,
			endTarget = hasPreload ? endChild[(scrollToTop ? 'next' : 'previous') + 'Sibling'] : endChild,
			endPos = endTarget.offsetTop + (scrollToTop ? 0 : endTarget.offsetHeight),
			handle;

		if (hasPreload) {
			// Find the nearest dgrid-row to the relevant end of the grid
			while (endTarget && endTarget.className.indexOf('dgrid-row') < 0) {
				endTarget = endTarget[(scrollToTop ? 'next' : 'previous') + 'Sibling'];
			}
			// If none is found, there are no rows, and nothing to navigate
			if (!endTarget) {
				return;
			}
		}

		// Grid content may be lazy-loaded, so check if content needs to be
		// loaded first
		if (!hasPreload || endChild.offsetHeight < 1) {
			// End row is loaded; focus the first/last row/cell now
			if (cellNavigation) {
				// Preserve column that was currently focused
				endTarget = this.cell(endTarget, this.cell(event).column.id);
			}
			this._focusOnNode(endTarget, false, event);
		}
		else {
			// In IE < 9, the event member references will become invalid by the time
			// _focusOnNode is called, so make a (shallow) copy up-front
			if (!has('dom-addeventlistener')) {
				event = lang.mixin({}, event);
			}

			// If the topmost/bottommost row rendered doesn't reach the top/bottom of
			// the contentNode, we are using OnDemandList and need to wait for more
			// data to render, then focus the first/last row in the new content.
			handle = aspect.after(this, 'renderArray', function (rows) {
				var target = rows[scrollToTop ? 0 : rows.length - 1];
				if (cellNavigation) {
					// Preserve column that was currently focused
					target = this.cell(target, this.cell(event).column.id);
				}
				this._focusOnNode(target, false, event);
				handle.remove();
				return rows;
			});
		}

		if (scrollPos === endPos) {
			// Grid body is already scrolled to end; prevent browser from scrolling
			// entire page instead
			event.preventDefault();
		}
	};

	var moveFocusHome = Implementation.moveFocusHome = function (event) {
		moveFocusEnd.call(this, event, true);
	};

	function preventDefault(event) {
		event.preventDefault();
	}

	Implementation.defaultKeyMap = {
		32: preventDefault, // space
		33: moveFocusPageUp, // page up
		34: moveFocusPageDown, // page down
		35: moveFocusEnd, // end
		36: moveFocusHome, // home
		37: moveFocusLeft, // left
		38: moveFocusUp, // up
		39: moveFocusRight, // right
		40: moveFocusDown // down
	};

	// Header needs fewer default bindings (no vertical), so bind it separately
	Implementation.defaultHeaderKeyMap = {
		32: preventDefault, // space
		35: moveHeaderFocusEnd, // end
		36: moveHeaderFocusHome, // home
		37: moveFocusLeft, // left
		39: moveFocusRight // right
	};

	var Module = declare(null,Implementation);
	Module.dcl = dcl(null,Implementation);
	return Module;
});

},
'xgrid/Defaults':function(){
/** @module xgrid/Defaults **/
define([
    'xdojo/declare'
], function (declare) {
    /**
     * xGrid defaults
     * */
    return declare('xgrid/Defaults', null, {
        minRowsPerPage: 100,
        keepScrollPosition: true,
        rowsPerPage: 30,
        deselectOnRefresh: false,
        cellNavigation: false,
        _skipFirstRender: false,
        loadingMessage: null,
        preload: null,
        childSelector: ".dgrid-row",
        addUiClasses: false,
        noDataMessage: '<span class="textWarning">No data....</span>',
        showExtraSpace:true,
        expandOnClick:true
    });
});

},
'xgrid/Layout':function(){
/** @module xgrid/Layout **/
define([
    "xdojo/declare",
    'xide/utils',
    'xide/widgets/TemplatedWidgetBase',
    'xide/registry',
    'xide/$'
], function (declare, utils, TemplatedWidgetBase, registry, $) {
    var template = '<div tabindex="-1" attachTo="template" class="grid-template" style="width: 100%;height: 100%;overflow: hidden;position: relative;padding: 0px;margin: 0px">' +
        '<div tabindex="-1" attachTo="header" class="grid-header row" style="width: 100%;height: auto"></div>' +
        '<div tabindex="0" attachTo="grid" class="grid-body row"></div>' +
        '<div attachTo="footer" class="grid-footer" style="position: absolute;bottom: 0px;width: 100%"></div>' +
        '</div>';
    /**
     *
     * @class module:xgrid/Layout
     */
    var Implementation = {
        template: null,
        attachDirect: true,
        destroy: function () {
            // important,remove us from our temp. template.
            this.template && this.template.remove(this) && utils.destroy(this.template, true, this);
            return this.inherited(arguments);
        },
        getTemplateNode: function () {
            return this.template.domNode;
        },
        getHeaderNode: function () {
            return this.template.header;
        },
        getBodyNode: function () {
            return this.template.grid;
        },
        getFooterNode: function () {
            return this.template.footer;
        },
        resize: function () {
            this.inherited(arguments);
            var thiz = this,
                mainNode = thiz.template ? thiz.template.domNode : this.domNode,
                isRerooted = false;

            if (this.__masterPanel) {
                mainNode = this.__masterPanel.containerNode;
                isRerooted = true;
            }
            var totalHeight = $(mainNode).height();
            var template = thiz.template;
            if (!template) {
                return;
            }
            var $header = $(template.header);
            var topHeight = $header ? $header.height() : 0;
            var _toolbarHeight = this._toolbar ? this._toolbar._height : 0;
            if (_toolbarHeight > 0 && topHeight === 0) {
                topHeight += _toolbarHeight;
            }
            if (_toolbarHeight && $header) {
                $header.css('height', 'auto');
            }
            var footerHeight = template.footer ? $(template.footer).height() : 0;
            var finalHeight = totalHeight - topHeight - (footerHeight);

            if (finalHeight > 50) {
                $(template.grid).height(finalHeight + 'px');
                isRerooted && $(template.domNode).width($(mainNode).width());
            } else {
                $(template.grid).height('inherited');
            }


        },
        buildRendering: function () {
            if (this.template) {
                return;
            }
            this._domNode = this.domNode;
            var templated = utils.addWidget(TemplatedWidgetBase, {
                templateString: template,
                declaredClass: 'xgrid/_BaseParent_' + this.declaredClass
            }, null, this.domNode, true);

            $(templated.domNode).attr('tabIndex', -1);

            this.template = templated;
            this.header = templated.header;
            this.footer = templated.footer;
            this.gridBody = templated.grid;
            this.domNode = templated.grid;
            this.id = this.template.id;
            this.domNode.id = this.id;
            templated.domNode.id = this.id;
            registry._hash[this.id] = this;
            templated.add(this);
            return this.inherited(arguments);
        }
    };

    //package via declare
    var _class = declare('xgrid.Layout', null, Implementation);
    _class.Implementation = Implementation;
    return _class;
});
},
'xgrid/Focus':function(){
define([
    "xdojo/declare",
    "xide/types",
    "xide/utils"
], function (declare,types,utils) {

    var Implementation = {
        _focused:false,
        _detectFocus:null,
        _detectBlur:null,
        destroy:function(){
            this.inherited(arguments);
            window.removeEventListener('focus', this._detectFocus, true);
            window.removeEventListener('blur', this._detectBlur, true);
        },
        _onBlur:function(){
        },
        set:function(what,value){
            if(what=='focused'){
                this._onFocusChanged(value);
              }
            return this.inherited(arguments);
        },
        _onFocusChanged:function(focused,type){
            if(this._focused && !focused){
                this._onBlur();
            }
            if(!this._focused && focused){
                this._emit(types.EVENTS.ON_VIEW_SHOW,this);
            }
            this._focused = focused;
            this.highlight  && this.highlight(focused);
        },
        getFocused:function(domNode){
            if(this._focusedNode){
                var row = this.row(this._focusedNode);
                if(row){
                    return row[domNode? 'element' : 'data' ];
                }
            }
            return null;
        },
        startup:function(){
            this.inherited(arguments);
            var thiz = this,
                node = thiz.domNode.parentNode;


            //@TODO: /active=true
            this.headerNode.tabIndex=-1;
            this._focused  = true;
        }

    };
    //package via declare
    var _class = declare('xgrid.Focus',null,Implementation);
    _class.Implementation = Implementation;
    return _class;
});
},
'xgrid/ListRenderer':function(){
/** @module xgrid/ListRenderer **/
define([
    "xdojo/declare",
    './Renderer',
    'dojo/dom-construct',
    'dgrid/Grid'
], function (declare,Renderer,domConstruct,Grid) {

    /**
     * The list renderer does nothing since the xgrid/Base is already inherited from
     * dgrid/OnDemandList and its rendering as list already.
     *
     * @class module:xgrid/ListRenderer
     * @extends module:xgrid/Renderer
     */
    var Implementation = {

        _getLabel:function(){ return "List"; },
        _getIcon:function(){ return "fa-th-list"; },
        activateRenderer:function(renderer){
            this._showHeader(true);
        },
        deactivateRenderer:function(renderer){},
        _configColumns: function () {
            return Grid.prototype._configColumns.apply(this, arguments);
        },
        insertRow:function(object,options) {
            return Grid.prototype.insertRow.apply(this, arguments);
        },
        renderRow:function(object,options){

            var self = this;
            var row = this.createRowCells('td', function (td, column) {
                var data = object;
                // Support get function or field property (similar to DataGrid)
                if (column.get) {
                    data = column.get(object);
                }
                else if ('field' in column && column.field !== '_item') {
                    data = data[column.field];
                }
                self._defaultRenderCell.call(column, object, data, td, options);
            }, options && options.subRows, object);
            // row gets a wrapper div for a couple reasons:
            // 1. So that one can set a fixed height on rows (heights can't be set on <table>'s AFAICT)
            // 2. So that outline style can be set on a row when it is focused,
            // and Safari's outline style is broken on <table>
            var div = domConstruct.create('div', { role: 'row' });
            div.appendChild(row);
            return div;
        }
    };

    //package via declare
    var _class = declare('xgrid.ListRenderer',[Renderer],Implementation);
    _class.Implementation = Implementation;

    return _class;
});
},
'xgrid/Renderer':function(){
/** @module xgrid/Renderer **/
define([
    "xdojo/declare"
], function (declare) {
    var Implementation = {
        _renderIndex: 0,
        _lastRenderedArray: null,
        publishRendering: false,
        _getLabel:function(){return ''},
        activateRenderer:function(renderer){},
        deactivateRenderer:function(renderer){},
        runAction:function(action){},
        /**
         * Placeholder
         */
        delegate: {
            onDidRenderCollection: function () {}
        },
        /**
         * Override render row to enable model side rendering
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            if (obj.render) {
                return obj.render(obj, this.inherited);
            }
            return this.inherited(arguments);
        },
        /**
         * Override renderArray in dgrid/List to track the
         * last rendered array
         * @returns {HTMLElement[]}
         */
        renderArray: function () {
            this._lastRenderedArray = this.inherited(arguments);
            this._onDidRenderCollection(arguments);
            return this._lastRenderedArray;
        },
        /**
         * Callback for dgrid/List#refresh promise, used to publish
         * the last rendered collection
         *
         */
        _onDidRenderCollection: function () {
            var info = {
                collection: this._renderedCollection,
                elements: this._lastRenderedArray,
                grid: this
            };
            this._renderIndex++;
        },
        /**
         * Return that this grid has actually rendered anything.
         * @returns {boolean}
         */
        didRender: function () {
            return this._renderIndex >= 0;
        }
    };

    //package via declare
    var _class = declare('xgrid.Renderer',null,Implementation);
    _class.Implementation = Implementation;

    return _class;
});
},
'xgrid/Clipboard':function(){
/** @module xgrid/Clipboard **/
define([
    "xdojo/declare",
    'xide/types'
], function (declare,types) {

    var Implementation = {
        runAction:function(action){
            switch (action.command){
                case types.ACTION.CLIPBOARD_COPY:{
                    this.clipboardCopy();
                    this.refreshActions();
                    return true;
                }
                case types.ACTION.CLIPBOARD_PASTE:{
                    return this.clipboardPaste();
                }
                case types.ACTION.CLIPBOARD_CUT:{
                    this.clipboardCut();
                    return true;
                }
            }
            return this.inherited(arguments);
        },
        clipboardPaste:function(){
            return this.inherited(arguments);
        },
        /**
         * Clipboard/Copy action
         */

        clipboardCopy:function(){
            this.currentCutSelection=null;
            this.currentCopySelection=this.getSelection();
            this.publish(types.EVENTS.ON_CLIPBOARD_COPY,{
                selection:this.currentCopySelection,
                owner:this,
                type:this.itemType
            });
        },
        clipboardCut:function(){
            this.currentCopySelection = null;
            this.currentCutSelection = this.getSelection();
        },
        getClipboardActions:function(addAction){
            var thiz = this,
                actions = [],
                ACTION = types.ACTION;

            function _selection(){
                var selection = thiz.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                var item = selection[0];
                if(!item){
                    console.error('have no item');
                    return null;
                }
                return selection;
            }

            function isItem() {
                var selection = _selection();
                if (!selection) {
                    return true;
                }
                return false;
            }

            function disable(){
                switch (this.title){
                    case 'Cut':
                    case 'Copy':{
                        return isItem()!==false;
                    }
                    case 'Paste':{
                        return thiz.currentCopySelection==null;
                    }
                }
                return false;
            }
            function _createEntry(label,command,icon,keyCombo) {
                actions.push(thiz.createAction({
                    label: label,
                    command: command,
                    icon: icon,
                    tab: 'Home',
                    group: 'Clipboard',
                    keycombo: keyCombo,
                    mixin: {
                        addPermission:true,
                        quick:true
                    },
                    shouldDisable:disable
                }));
            }
            _createEntry('Copy',ACTION.CLIPBOARD_COPY,'fa-copy','ctrl c');
            _createEntry('Paste',ACTION.CLIPBOARD_PASTE,'fa-paste','ctrl v');
            _createEntry('Cut',ACTION.CLIPBOARD_CUT,'fa-cut','ctrl x');
            return actions;
        }
    };

    //package via declare
    var _class = declare('xgrid.Clipboard',null,Implementation);
    _class.Implementation = Implementation;

    return _class;
});
},
'xgrid/ThumbRenderer':function(){
/** @module xgrid/ThumbRenderer **/
define([
    "xdojo/declare",
    'dojo/dom-construct',
    './Renderer'
], function (declare,domConstruct,Renderer) {
    /**
     * The list renderer does nothing since the xgrid/Base is already inherited from
     * dgrid/OnDemandList and its rendering as list already.
     *
     * @class module:xgrid/ThumbRenderer
     * @extends module:xgrid/Renderer
     */
    var Implementation = {
        isThumbGrid:false,
        _getLabel:function(){ return "Thumb"; },
        _getIcon:function(){ return "fa-th-large"; },
        activateRenderer:function(renderer){
            this._showHeader(false);
            this.isThumbGrid = true;
        },
        deactivateRenderer:function(renderer){
            this.isThumbGrid = false;
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            if (obj.render) {
                return obj.render(obj, this.inherited);
            }
            return domConstruct.create('span', {
                className: "fileGridCell",
                innerHTML: '<span class=\"' + 'fa-cube fa-5x' + '\""></span> <div class="name">' + obj.name + '</div>',
                style: 'color:blue;max-width:200px;float:left;margin:18px;padding:18px;'
            });
        }
    };

    //package via declare
    var _class = declare('xgrid.ThumbRenderer',[Renderer],Implementation);
    _class.Implementation = Implementation;

    return _class;
});
},
'xgrid/TreeRenderer':function(){
/** @module xgrid/TreeRenderer **/
define([
    "xdojo/declare",
    'xgrid/Renderer',
    'dgrid/Tree',
    "dojo/keys",
    "dojo/on",
    "xide/$"
], function (declare, Renderer, Tree, keys, on, $) {

    function KEYBOARD_HANDLER(evt) {
        this.onTreeKey(evt);
        var thiz = this;
        if (thiz.isThumbGrid) {
            return;
        }
        if (evt.keyCode == keys.LEFT_ARROW || evt.keyCode == keys.RIGHT_ARROW || evt.keyCode == keys.HOME || evt.keyCode == keys.END) {
        } else {
            return;
        }
        var target = evt.target;
        if (target) {
            if (target.className.indexOf('InputInner') != -1 || target.className.indexOf('input') != -1 || evt.target.type === 'text') {
                return;
            }
        }

        var row = this.row(evt);
        if (!row || !row.data) {
            return;
        }
        var data = row.data,
            isExpanded = this._isExpanded(data),
            store = this.collection,
            storeItem = store.getSync(data[store.idProperty]);

        //old back compat: var children = data.getChildren ? data.getChildren() :  storeItem && storeItem.children ? null : store.children ? store.children(storeItem) : null;
        var children = data.getChildren ? data.getChildren() : storeItem && storeItem.children ? storeItem.children : null;

        //xideve hack
        var wasStoreBased = false;
        if (children == null && store.getChildrenSync && storeItem) {
            children = store.getChildrenSync(storeItem);
            if (children && children.length) {
                wasStoreBased = true;
            } else {
                children = null;
            }
        }

        var isFolder = storeItem ? (storeItem.isDir || storeItem.directory || storeItem.group) : false;
        if (!isFolder && wasStoreBased && children) {
            isFolder = true;
        }
        //xideve hack end

        var firstChild = children ? children[0] : false,
            focused = this._focusedNode,
            last = focused ? this.down(focused, children ? children.length : 0, true) : null,
            loaded = (storeItem._EX === true || storeItem._EX == null);

        var selection = this.getSelection ? this.getSelection() : [storeItem];
        //var selection2 = this.getSelection ? this._getSelected() : [storeItem];

        var down = this.down(focused, -1, true),
            up = this.down(focused, 1, true),
            defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 1
            };

        if (firstChild && firstChild._reference) {
            var _b = store.getSync(firstChild._reference);
            if (_b) {
                firstChild = _b;
            }
        }
        if (evt.keyCode == keys.END) {
            if (isExpanded && isFolder && last && last.element !== focused) {
                this.select(last, null, true, defaultSelectArgs);
                return;
            }
        }

        function expand(what, expand) {
            _.each(what, function (item) {
                var _row = thiz.row(item);
                if (_row && _row.element) {
                    thiz.expand(_row, expand, true);
                }
            });
        }

        if (evt.keyCode == keys.LEFT_ARROW) {
            evt.preventDefault();
            if (data[store.parentField]) {
                var item = row.data;
                if (!isExpanded) {
                    var parent = store.getSync(item[store.parentField]);
                    var parentRow = parent ? this.row(parent) : null;
                    //we select the parent only if its rendered at all
                    if (parent && parentRow.element) {
                        return this.select([parent], null, true, defaultSelectArgs);
                    } else {
                        if (down) {
                            return this.select(down, null, true, defaultSelectArgs);
                        } else {
                            on.emit(this.contentNode, "keydown", { keyCode: 36, force: true });
                        }
                    }
                }
            }
            if (row) {
                if (isExpanded) {
                    expand(selection, false);
                } else {
                    this.select(down, null, true, defaultSelectArgs);
                }
            }
        }

        if (evt.keyCode == keys.RIGHT_ARROW) {
            evt.preventDefault();
            // empty folder:
            if (isFolder && loaded && isExpanded && !firstChild) {
                //go to next
                if (up) {
                    return this.select(up, null, true, defaultSelectArgs);
                }
            }

            if (loaded && isExpanded) {
                firstChild && this.select([firstChild], null, true, defaultSelectArgs);
            } else {
                //has children or not loaded yet
                if (firstChild || !loaded || isFolder) {
                    expand(selection, true);
                } else {
                    //case on an cell without no children: select
                    up && this.select(up, null, true, defaultSelectArgs);
                }
            }
        }
    }

    /**
     *
     * @class module:xgrid/TreeRenderer
     * @extends module:xgrid/Renderer
     */
    var Implementation = {
        _expandOnClickHandle: null,
        _patchTreeClick:function(){
            this._expandOnClickHandle = this.on("click", this.onTreeClick.bind(this));
            $(this.domNode).addClass('openTreeOnClick');
        },
        _getLabel: function () {
            return "Tree";
        },
        _getIcon: function () {
            return "fa-tree";
        },
        deactivateRenderer: function (renderer) {
            this._expandOnClickHandle && this._expandOnClickHandle.remove();
            if (this.expandOnClick) {
                $(this.domNode).removeClass('openTreeOnClick');
            }
        },
        activateRenderer: function () {
            this._showHeader(true);
            if (this.expandOnClick) {
                this._expandOnClickHandle = this.on("click", this.onTreeClick.bind(this));
                $(this.domNode).addClass('openTreeOnClick');
            }
        },
        __getParent: function (item) {
            if (item && item.getParent) {
                var _parent = item.getParent();
                if (_parent) {
                    var row = this.row(_parent);
                    if (row.element) {
                        return this.__getParent(_parent);
                    } else {
                        return _parent || item;
                    }
                }
            }
            return item;
        },
        /**
         * @TODO: move to xfile
         */
        getCurrentFolder: function () {
            return this.__getParent(this.getRows()[0]);
        },
        _isExpanded: function (item) {
            return !!this._expanded[this.row(item).id];
        },
        onTreeKey: function () {
            this.inherited(arguments);
        },
        onTreeClick: function (e) {
            if(e.target.className.indexOf('expando')!==-1){
                return;
            }
            var row = this.row(e);
            row && this.expand(row, !this._isExpanded(row.data), true);
        },
        startup: function () {
            if (this._started) {
                return;
            }
            var res = this.inherited(arguments);
            this.on("keydown", KEYBOARD_HANDLER.bind(this));
            if (!this.renderers) {
                //we are the only renderer
                this.activateRenderer();
            }
            return res;
        }
    };

    //package via declare
    var Module = declare('xgrid.TreeRenderer', [Renderer, Tree], Implementation);
    Module.Implementation = Implementation;
    Module.KEYBOARD_HANDLER = KEYBOARD_HANDLER;
    return Module;
});
},
'xgrid/DnD':function(){
/** @module xgrid/DnD **/
define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/array',
    'dojo/aspect',
    'dojo/dom-class',
    'dojo/topic',
    'dojo/has',
    'dojo/when',
    'dojo/dnd/Source',
    'dojo/dnd/Manager',
    'dojo/_base/NodeList',
    'dojo/dnd/Manager',
    'dojo/dnd/common',
    'dojo/dom-geometry',
    'xide/lodash',
    'xide/$',
    'dojo/has!touch?../util/touch'
], function (declare, lang, arrayUtil, aspect, domClass, topic, has, when, DnDSource,
             DnDManager, NodeList, Manager, dnd, domGeom,_,$,touchUtil) {
    /**
     * @TODO
     * - consider sending items rather than nodes to onDropExternal/Internal
     * - overriding _markTargetAnchor is incomplete and doesn't treat a redunant before==after and thus cause a pretty ugly flickering
     * - could potentially also implement copyState to jive with default
     * - onDrop* implementations (checking whether store.copy is available);
     * - not doing that just yet until we're sure about default impl.
     */
    /**
     * @class module:xgrid/DnD/GridDnDSource
     * @extends module:dojo/dnd/Source
     */
    var GridDnDSource = declare(DnDSource, {
        /**
         * @type {module:xgrid/Base|dgrid/List}
         */
        grid: null,
        /**
         * @type {string} the CSS class needed to recognize the 'on center' state.
         */
        centerClass: 'dgrid-cell',
        /**
         * Function to determine the the drag source is over a target's center area (and not 'before' or 'after' )
         * This is called by a dojo/dnd/Source#onMouseMove variant.
         * @param e {MouseEvent}
         * @returns {boolean}
         */
        isCenter: function (e) {
            return $(e.target).parent().hasClass(this.centerClass);
        },
        /**
         * Method which should be defined on any source intending on interfacing with dgrid DnD.
         * @param node {HTMLElement}
         * @returns {module:xide/data/Model|*}
         */
        getObject: function (node) {
            var grid = this.grid;
            // Extract item id from row node id (gridID-row-*).
            return grid._trackError(function () {
                return grid.collection.get(node.id.slice(grid.id.length + 5));
            });
        },
        _legalMouseDown: function (evt) {
            // Fix _legalMouseDown to only allow starting drag from an item
            // (not from bodyNode outside contentNode).
            var legal = this.inherited(arguments);
            return legal && evt.target !== this.grid.bodyNode;
        },
        /**
         * @param sourceSource {module:dojo/dnd/Source}
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         */
        onDrop: function (sourceSource, nodes, copy) {
            var targetSource = this,
                targetRow = this._targetAnchor = this.targetAnchor, // save for Internal
                targetGrid = this.grid,
                targetStore = targetGrid.collection,
                sourceGrid = sourceSource.grid,
                sourceStore = sourceGrid.collection;

            if (!this.before && targetRow) {
                // target before next node if dropped within bottom half of this node
                // (unless there's no node to target at all)
                targetRow = targetRow.nextSibling;
            }
            targetRow = targetRow && targetGrid.row(targetRow);
            when(targetRow && targetStore.get(targetRow.id), function (target) {
                // Note: if dropping after the last row, or into an empty grid,
                // target will be undefined.  Thus, it is important for store to place
                // item last in order if options.before is undefined.

                // Delegate to onDropInternal or onDropExternal for rest of logic.
                // These are passed the target item as an additional argument.
                if (targetSource !== sourceSource && sourceStore.root != targetStore.root) {
                    targetSource.onDropExternal(sourceSource, nodes, copy, target);
                }
                else {
                    targetSource.onDropInternal(nodes, copy, target);
                }
            });
        },
        /**
         *
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:xide/data/Model}
         */
        onDropInternal: function (nodes, copy, targetItem) {
            var grid = this.grid,
                store = grid.collection,
                targetSource = this,
                anchor = targetSource._targetAnchor,
                targetRow,
                nodeRow;

            if (anchor) { // (falsy if drop occurred in empty space after rows)
                targetRow = this.before ? anchor.previousSibling : anchor.nextSibling;
            }

            // Don't bother continuing if the drop is really not moving anything.
            // (Don't need to worry about edge first/last cases since dropping
            // directly on self doesn't fire onDrop, but we do have to worry about
            // dropping last node into empty space beyond rendered rows.)
            nodeRow = grid.row(nodes[0]);
            if (!copy && (targetRow === nodes[0] ||
                (!targetItem && nodeRow && grid.down(nodeRow).element === nodes[0]))) {
                return;
            }

            nodes.forEach(function (node) {
                when(targetSource.getObject(node), function (object) {
                    var id = store.getIdentity(object);

                    // For copy DnD operations, copy object, if supported by store;
                    // otherwise settle for put anyway.
                    // (put will relocate an existing item with the same id, i.e. move).
                    grid._trackError(function () {
                        return store[copy && store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {
                            // Self-drops won't cause the dgrid-select handler to re-fire,
                            // so update the cached node manually
                            if (targetSource._selectedNodes[id]) {
                                targetSource._selectedNodes[id] = grid.row(id).element;
                            }
                        });
                    });
                });
            });
        },
        /**
         * @param sourceSource {module:dojo/dnd/Source}
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:dojo/dnd/Target}
         */
        onDropExternal: function (sourceSource, nodes, copy, targetItem) {
            // Note: this default implementation expects that two grids do not
            // share the same store.  There may be more ideal implementations in the
            // case of two grids using the same store (perhaps differentiated by
            // query), dragging to each other.
            var grid = this.grid,
                store = this.grid.collection,
                sourceGrid = sourceSource.grid;

            // TODO: bail out if sourceSource.getObject isn't defined?
            nodes.forEach(function (node, i) {
                when(sourceSource.getObject(node), function (object) {
                    // Copy object, if supported by store; otherwise settle for put
                    // (put will relocate an existing item with the same id).
                    // Note that we use store.copy if available even for non-copy dnd:
                    // since this coming from another dnd source, always behave as if
                    // it is a new store item if possible, rather than replacing existing.
                    grid._trackError(function () {
                        return store[store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {
                            if (!copy) {
                                if (sourceGrid) {
                                    // Remove original in the case of inter-grid move.
                                    // (Also ensure dnd source is cleaned up properly)
                                    var id = sourceGrid.collection.getIdentity(object);
                                    !i && sourceSource.selectNone(); // Deselect all, one time
                                    sourceSource.delItem(node.id);
                                    return sourceGrid.collection.remove(id);
                                }
                                else {
                                    sourceSource.deleteSelectedNodes();
                                }
                            }
                        });
                    });
                });
            });
        },
        /**
         * @param source {module:dojo/dnd/Source}
         */
        onDndStart: function (source) {
            // Listen for start events to apply style change to avatar.
            this.inherited(arguments); // DnDSource.prototype.onDndStart.apply(this, arguments);
            if (source === this) {
                // Set avatar width to half the grid's width.
                // Kind of a naive default, but prevents ridiculously wide avatars.
                DnDManager.manager().avatar.node.style.width =
                    this.grid.domNode.offsetWidth / 2 + 'px';
            }
        },
        /**
         * @param evt {MouseEvent}
         */
        onMouseDown: function (e) {
            if (e.target.tagName=='INPUT') {
                return;
            }
            // Cancel the drag operation on presence of more than one contact point.
            // (This check will evaluate to false under non-touch circumstances.)
            if (has('touch') && this.isDragging &&
                touchUtil.countCurrentTouches(e, this.grid.touchNode) > 1) {
                topic.publish('/dnd/cancel');
                DnDManager.manager().stopDrag();
            }
            else {
                this.inherited(arguments);
            }
        },
        /**
         * Event processor for onmousemove.
         * @param e {MouseEvent}
         */
        onMouseMove: function (e) {
            if (this.isDragging && this.targetState == 'Disabled') {
                return;
            }
            // skip inline edits and the like
            if (e.target.tagName=='INPUT') {
                return;
            }

            var m = Manager.manager();
            if (!this.isDragging) {
                if (this.mouseDown && this.isSource &&
                    (Math.abs(e.pageX - this._lastX) > this.delay || Math.abs(e.pageY - this._lastY) > this.delay)) {
                    var nodes = this.getSelectedNodes();
                    if (nodes.length) {
                        m.startDrag(this, nodes, this.copyState(dnd.getCopyKeyState(e), true));
                    }
                }
            }

            if (this.isDragging) {
                // calculate before/center/after
                var before = false;
                var center = false;
                if (this.current) {
                    if (!this.targetBox || this.targetAnchor != this.current) {
                        this.targetBox = domGeom.position(this.current, true);
                    }
                    if (this.horizontal) {
                        // In LTR mode, the left part of the object means "before", but in RTL mode it means "after".
                        before = (e.pageX - this.targetBox.x < this.targetBox.w / 2) == domGeom.isBodyLtr(this.current.ownerDocument);
                    } else {
                        before = (e.pageY - this.targetBox.y) < (this.targetBox.h / 2);
                    }

                }
                center = this.isCenter(e);
                if (this.current != this.targetAnchor || before != this.before || center != this.center) {
                    var grid = this.grid;
                    var canDrop = !this.current || m.source != this || !(this.current.id in this.selection);
                    var source = grid._sourceToModel(m.source), target = grid._sourceToModel(this.current);
                    if (source && target && target.canAdd && center) {
                        var targetCan = target.canAdd(source);
                        if (targetCan === false) {
                            canDrop = targetCan;
                        }
                    }
                    this._markTargetAnchor(before, center, canDrop);
                    m.canDrop(canDrop);
                }
            }
        },
        checkAcceptance: function (source) {
            // Augment checkAcceptance to block drops from sources without getObject.
            return source.getObject && DnDSource.prototype.checkAcceptance.apply(this, arguments);
        },
        getSelectedNodes: function () {
            // If dgrid's Selection mixin is in use, synchronize with it, using a
            // map of node references (updated on dgrid-[de]select events).

            if (!this.grid.selection) {
                return this.inherited(arguments);
            }
            var t = new NodeList(),
                id;
            for (id in this.grid.selection) {
                t.push(this._selectedNodes[id]);
            }
            return t;	// NodeList
        }
    });

    // Mixin Selection for more resilient dnd handling, particularly when part
    // of the selection is scrolled out of view and unrendered (which we
    // handle below).
    /**
     *
     * Requirements
     * - requires a store (sounds obvious, but not all Lists/Grids have stores...)
     * - must support options.before in put calls (if undefined, put at end)
     * - should support copy (copy should also support options.before as above)
     * @class module:xgrid/DnD
     */
    var DnD = declare('xgrid/views/DnD', null, {
        // dndSourceType: String
        //		Specifies the type which will be set for DnD items in the grid,
        //		as well as what will be accepted by it by default.
        dndSourceType: 'dgrid-row',

        // dndParams: Object
        //		Object containing params to be passed to the DnD Source constructor.
        dndParams: null,

        // dndConstructor: Function
        //		Constructor from which to instantiate the DnD Source.
        //		Defaults to the GridSource constructor defined/exposed by this module.
        dndConstructor: GridDnDSource,
        postMixInProperties: function () {
            this.inherited(arguments);
            // ensure dndParams is initialized
            this.dndParams = lang.mixin({accept: [this.dndSourceType]}, this.dndParams);
        },
        __dndNodesToModel: function (nodes) {
            return _.map(nodes, function (n) {
                return (this.row(n) || {}).data;
            }, this);
        },
        /**
         *
         * @param source {module:xgrid/DnD/GridDnDSource}
         * @returns {module:xide/data/Model}
         * @returns {module:xgrid/Base}
         * @private
         */
        _sourceToModel: function (source, grid) {
            var result = null;
            if (source) {
                var anchor = source._targetAnchor || source.anchor || source;
                grid = grid || source.grid || this;
                if (!anchor || !anchor.ownerDocument) {
                    return null;
                }
                var row = grid.row(anchor);
                if (row) {
                    return row.data;
                }
                if (anchor) {
                    result = grid.collection.getSync(anchor.id.slice(grid.id.length + 5));
                    if (!result) {
                        result = grid.row(anchor);
                    }
                }
            }
            return result;
        },
        postCreate: function () {
            this.inherited(arguments);
            // Make the grid's content a DnD source/target.
            var Source = this.dndConstructor || GridDnDSource;
            var dndParams = lang.mixin(this.dndParams, {
                // add cross-reference to grid for potential use in inter-grid drop logic
                grid: this,
                dropParent: this.contentNode
            });
            if (typeof this.expand === 'function') {
                // If the Tree mixin is being used, allowNested needs to be set to true for DnD to work properly
                // with the child rows.  Without it, child rows will always move to the last child position.
                dndParams.allowNested = true;
            }
            this.dndSource = new Source(this.bodyNode, dndParams);

            // Set up select/deselect handlers to maintain references, in case selected
            // rows are scrolled out of view and unrendered, but then dragged.
            var selectedNodes = this.dndSource._selectedNodes = {};
            function selectRow(row) {
                selectedNodes[row.id] = row.element;
            }
            function deselectRow(row) {
                delete selectedNodes[row.id];
                // Re-sync dojo/dnd UI classes based on deselection
                // (unfortunately there is no good programmatic hook for this)
                domClass.remove(row.element, 'dojoDndItemSelected dojoDndItemAnchor');
            }
            this.on('dgrid-select', function (event) {
                arrayUtil.forEach(event.rows, selectRow);
            });
            this.on('dgrid-deselect', function (event) {
                arrayUtil.forEach(event.rows, deselectRow);
            });
            aspect.after(this, 'destroy', function () {
                delete this.dndSource._selectedNodes;
                selectedNodes = null;
                this.dndSource.destroy();
            }, true);
        },
        insertRow: function (object) {
            // override to add dojoDndItem class to make the rows draggable
            var row = this.inherited(arguments),
                type = typeof this.getObjectDndType === 'function' ?
                    this.getObjectDndType(object) : [this.dndSourceType];

            domClass.add(row, 'dojoDndItem');
            this.dndSource.setItem(row.id, {
                data: object,
                type: type instanceof Array ? type : [type]
            });
            return row;
        },
        removeRow: function (rowElement) {
            this.dndSource.delItem(this.row(rowElement));
            this.inherited(arguments);
        }
    });
    DnD.GridSource = GridDnDSource;
    return DnD;
});
},
'xgrid/Grid':function(){
/** @module xgrid/Grid **/
define([
    'dojo/_base/declare',
    'xide/types',
    './Base'
],function (declare,types,Base) {
    /**
     *
     * Please read {@link module:xgrid/types}
     *
     * @class module:xgrid/Grid
     * @augments module:xgrid/Base
     */
    var grid = declare('xgrid/Grid',Base,{});

    grid.createGridClass = Base.createGridClass;

    //track defaults on module
    grid.classFactory = Base.classFactory;
    grid.DEFAULT_GRID_FEATURES = types.DEFAULT_GRID_FEATURES;
    grid.DEFAULT_GRID_BASES = Base.DEFAULT_GRID_BASES;
    grid.DEFAULT_GRID_OPTIONS = types.DEFAULT_GRID_OPTIONS;
    grid.DEFAULT_GRID_OPTION_KEYS = types.DEFAULT_GRID_OPTION_KEYS;

    return grid;
});
},
'xgrid/KeyboardNavigation':function(){
define([
	"xdojo/declare", // declare
	"dojo/keys", // keys.END keys.HOME, keys.LEFT_ARROW etc.
	"dojo/_base/lang", // hitch
	"dojo/on",
	"xide/utils"
], function(declare, keys, lang, on, utils){

	//@TODO: port hitch
	var hitch = lang.hitch;
	//@TODO: port utils.find
	var find = utils.find;
	
	return declare('xgrid/KeyboardNavigation',null, {
		// summary:
		//		A mixin to allow arrow key and letter key navigation of child or descendant widgets.
		//		It can be used by dijit/_Container based widgets with a flat list of children,
		//		or more complex widgets like dijit/Tree.
		//
		//		To use this mixin, the subclass must:
		//
		//			- Implement  _getNext(), _getFirst(), _getLast(), _onLeftArrow(), _onRightArrow()
		//			  _onDownArrow(), _onUpArrow() methods to handle home/end/left/right/up/down keystrokes.
		//			  Next and previous in this context refer to a linear ordering of the descendants used
		//			  by letter key search.
		//			- Set all descendants' initial tabIndex to "-1"; both initial descendants and any
		//			  descendants added later, by for example addChild()
		//			- Define childSelector to a function or string that identifies focusable descendant widgets
		//
		//		Also, child widgets must implement a focus() method.

		/*=====
		 // focusedChild: [protected readonly] Widget
		 //		The currently focused child widget, or null if there isn't one
		 focusedChild: null,

		 // _keyNavCodes: Object
		 //		Hash mapping key code (arrow keys and home/end key) to functions to handle those keys.
		 //		Usually not used directly, as subclasses can instead override _onLeftArrow() etc.
		 _keyNavCodes: {},
		 =====*/

		// childSelector: [protected abstract] Function||String
		//		Selector (passed to on.selector()) used to identify what to treat as a child widget.   Used to monitor
		//		focus events and set this.focusedChild.   Must be set by implementing class.   If this is a string
		//		(ex: "> *") then the implementing class must require dojo/query.
		childSelector: ".dgrid-row",
		defer: function(fcn, delay){
			// summary:
			//		Wrapper to setTimeout to avoid deferred functions executing
			//		after the originating widget has been destroyed.
			//		Returns an object handle with a remove method (that returns null) (replaces clearTimeout).
			// fcn: function reference
			// delay: Optional number (defaults to 0)
			// tags:
			//		protected.
			var timer = setTimeout(hitch(this,
					function(){
						timer = null;
						if(!this._destroyed){
							hitch(this, fcn)();
						}
					}),
				delay || 0
			);
			return {
				remove:	function(){
					if(timer){
						clearTimeout(timer);
						timer = null;
					}
					return null; // so this works well: handle = handle.remove();
				}
			};
		},
		buildRendering:function(){
			this.inherited(arguments);
			// Set tabIndex on this.domNode.  Will be automatic after #7381 is fixed.
			//domAttr.set(this.domNode, "tabIndex", this.tabIndex);
			if(!this._keyNavCodes){
				var keyCodes = this._keyNavCodes = {};
				keyCodes[keys.UP_ARROW] = hitch(this, "_onUpArrow");
				keyCodes[keys.DOWN_ARROW] = hitch(this, "_onDownArrow");
			}

			var self = this,
				childSelector = typeof this.childSelector == "string" ? this.childSelector : hitch(this, "childSelector"),
				node = this.domNode;

			this.__on(node, "keypress",null,hitch(this, "_onContainerKeypress"));
			this.__on(node, "keydown",null,hitch(this, "_onContainerKeydown"));
		},
		_onLeftArrow: function(){
			// summary:
			//		Called on left arrow key, or right arrow key if widget is in RTL mode.
			//		Should go back to the previous child in horizontal container widgets like Toolbar.
			// tags:
			//		extension
		},

		_onRightArrow: function(){
			// summary:
			//		Called on right arrow key, or left arrow key if widget is in RTL mode.
			//		Should go to the next child in horizontal container widgets like Toolbar.
			// tags:
			//		extension
		},

		_onUpArrow: function(){
			// summary:
			//		Called on up arrow key. Should go to the previous child in vertical container widgets like Menu.
			// tags:
			//		extension
		},

		_onDownArrow: function(){
			// summary:
			//		Called on down arrow key. Should go to the next child in vertical container widgets like Menu.
			// tags:
			//		extension
		},

		___focus: function(){
			// summary:
			//		Default focus() implementation: focus the first child.
			this.focusFirstChild();
		},

		_getFirstFocusableChild: function(){
			// summary:
			//		Returns first child that can be focused.

			// Leverage _getNextFocusableChild() to skip disabled children
			return this._getNextFocusableChild(null, 1);	// dijit/_WidgetBase
		},

		_getLastFocusableChild: function(){
			// summary:
			//		Returns last child that can be focused.

			// Leverage _getNextFocusableChild() to skip disabled children
			return this._getNextFocusableChild(null, -1);	// dijit/_WidgetBase
		},
		_searchString: "",
		// multiCharSearchDuration: Number
		//		If multiple characters are typed where each keystroke happens within
		//		multiCharSearchDuration of the previous keystroke,
		//		search for nodes matching all the keystrokes.
		//
		//		For example, typing "ab" will search for entries starting with
		//		"ab" unless the delay between "a" and "b" is greater than multiCharSearchDuration.
		multiCharSearchDuration: 1000,

		onKeyboardSearch: function(/*dijit/_WidgetBase*/ item, /*Event*/ evt, /*String*/ searchString, /*Number*/ numMatches){
			// summary:
			//		When a key is pressed that matches a child item,
			//		this method is called so that a widget can take appropriate action is necessary.
			// tags:
			//		protected
			if(item){
				this.deselectAll();
				this.select([this.row(item).data],null,true,{
					focus:true,
					delay:10,
					append:true
				})
			}
		},
		getSearchableText:function(data){
			return data.message || data.name  || '';
		},
		_keyboardSearchCompare: function(/*dijit/_WidgetBase*/ item, /*String*/ searchString){
			// summary:
			//		Compares the searchString to the widget's text label, returning:
			//
			//			* -1: a high priority match  and stop searching
			//		 	* 0: not a match
			//		 	* 1: a match but keep looking for a higher priority match
			// tags:
			//		private
			var element = item;
			if(item && !item.data){
				var row= this.row(item);
				if(row){
					item['data']=row.data;
				}
			}

			//var text = item.label || (element.focusNode ? element.focusNode.label : '') || element.innerText || element.textContent || "";
			var text = item ? item.data ? this.getSearchableText(item.data) : '' : '';
			if(text) {
				text = text.toLowerCase();
				//try starts with first:
				var currentString = text.replace(/^\s+/, '').substr(0, searchString.length).toLowerCase();
				var res = (!!searchString.length && currentString == searchString) ? -1 : 0; // stop searching after first match by default

				var contains = text.replace(/^\s+/, '').indexOf(searchString.toLowerCase())!=-1;
				if(res==0 && searchString.length>1 && contains){
					return 1;
				}
				return res;
			}
		},

		_onContainerKeydown: function(evt){
			// summary:
			//		When a key is pressed, if it's an arrow key etc. then it's handled here.
			// tags:
			//		private

			if((evt.target && evt.target.className.indexOf('input') != -1)){
				return;
			}

			var func = this._keyNavCodes[evt.keyCode];
			if(func){
				func(evt, this.focusedChild);
				evt.stopPropagation();
				evt.preventDefault();
				this._searchString = ''; // so a DOWN_ARROW b doesn't search for ab
			}else if(evt.keyCode == keys.SPACE && this._searchTimer && !(evt.ctrlKey || evt.altKey || evt.metaKey)){
				evt.stopImmediatePropagation(); // stop _HasDropDown from processing the SPACE as well
				evt.preventDefault(); // stop default actions like page scrolling on SPACE, but also keypress unfortunately
				on.emit(this.domNode, "keypress", {
					charCode: keys.SPACE,
					cancelable: true,
					bubbles: true
				});
			}
		},

		_onContainerKeypress: function(evt){
			if(this.editing){
				return;
			}
			if((evt.target && evt.target.className.indexOf('input') != -1)){
				return;
			}
			// summary:
			//		When a printable key is pressed, it's handled here, searching by letter.
			// tags:
			//		private

			if(evt.charCode < 32){
				// Avoid duplicate events on firefox (this is an arrow key that will be handled by keydown handler)
				return;
			}

			if(evt.ctrlKey || evt.altKey){
				return;
			}

			var
				matchedItem = null,
				searchString,
				numMatches = 0,
				search = hitch(this, function(){
					if(this._searchTimer){
						this._searchTimer.remove();
					}
					this._searchString += keyChar;
					var allSameLetter = /^(.)\1*$/.test(this._searchString);
					var searchLen = allSameLetter ? 1 : this._searchString.length;
					searchString = this._searchString.substr(0, searchLen);
					// commented out code block to search again if the multichar search fails after a smaller timeout
					//this._searchTimer = this.defer(function(){ // this is the "failure" timeout
					//	this._typingSlowly = true; // if the search fails, then treat as a full timeout
					//	this._searchTimer = this.defer(function(){ // this is the "success" timeout
					//		this._searchTimer = null;
					//		this._searchString = '';
					//	}, this.multiCharSearchDuration >> 1);
					//}, this.multiCharSearchDuration >> 1);
					this._searchTimer = this.defer(function(){ // this is the "success" timeout
						this._searchTimer = null;
						this._searchString = '';

					}, this.multiCharSearchDuration);
					var currentItem = this.focusedChildNode ||this.focusedChild || null;
					if(searchLen == 1 || !currentItem){
						currentItem = this._getNextFocusableChild(currentItem, 1); // skip current
						if(!currentItem){
							return;
						} // no items
					}
					var stop = currentItem;
					var idx=0;
					do{
						var rc = this._keyboardSearchCompare(currentItem, searchString);
						if(!!rc && numMatches++ == 0){
							matchedItem = currentItem;
						}
						if(rc == -1){ // priority match
							numMatches = -1;
							break;
						}
						currentItem = this._getNextFocusableChild(currentItem, 1);
					}while(currentItem != stop);
					// commented out code block to search again if the multichar search fails after a smaller timeout
					//if(!numMatches && (this._typingSlowly || searchLen == 1)){
					//	this._searchString = '';
					//	if(searchLen > 1){
					//		// if no matches and they're typing slowly, then go back to first letter searching
					//		search();
					//	}
					//}
				}),
				keyChar = String.fromCharCode(evt.charCode).toLowerCase();


			evt.preventDefault();
			evt.stopPropagation();
			search();

			// commented out code block to search again if the multichar search fails after a smaller timeout
			//this._typingSlowly = false;
			this.onKeyboardSearch(matchedItem, evt, searchString, numMatches);
		},

		_onChildBlur: function(/*dijit/_WidgetBase*/ /*===== widget =====*/){
			// summary:
			//		Called when focus leaves a child widget to go
			//		to a sibling widget.
			//		Used to be used by MenuBase.js (remove for 2.0)
			// tags:
			//		protected
		},

		_getNextFocusableChild: function(child, dir){
			// summary:
			//		Returns the next or previous focusable descendant, compared to "child".
			//		Implements and extends _KeyNavMixin._getNextFocusableChild() for a _Container.
			// child: Widget
			//		The current widget
			// dir: Integer
			//		- 1 = after
			//		- -1 = before
			// tags:
			//		abstract extension
			var wrappedValue = child;
			do{
				if(!child){
					child = this[dir > 0 ? "_getFirst" : "_getLast"]();
					if(!child){
						break;
					}
				}else{
					if(child && child.node){
						var innerNode = utils.find('.dgrid-cell',child.node,true);
						if(innerNode){
							child=innerNode;
						}
					}
					child = this._getNext(child, dir);
				}
				if(child != null && child != wrappedValue){
					return child;
				}
			}while(child != wrappedValue);
		},

		_getFirst: function(){
			var innerNode = utils.find('.dgrid-row', this.domNode,true);
			if(innerNode){
				var innerNode0 = utils.find('.dgrid-cell', innerNode,true);
				if(innerNode0){
					return innerNode0;
				}
			}
			return innerNode;
		},

		_getLast: function(){
			var innerNode = utils.find('.dgrid-row', this.domNode,false);
			if(innerNode){
				var innerNode0 = utils.find('.dgrid-cell', innerNode,true);
				if(innerNode0){
					return innerNode0;
				}
			}
			return null;
		},
		_getPrev: function(child, dir){
			// summary:
			//		Returns the next descendant, compared to "child".
			// child: Widget
			//		The current widget
			// dir: Integer
			//		- 1 = after
			//		- -1 = before
			// tags:
			//		abstract extension
			if(child){
				var w= this.up(child,1,true);
				if(w){
					var data = null;
					if(w.data){
						data= w.data;
					}

					if(w.element){
						w= w.element;
					}
					var innerNode = utils.find('.dgrid-cell', w,true);
					if(innerNode){
						if(!innerNode.data){
							innerNode['data']=data;
						}
						return innerNode;
					}
					return w;
				}
			}
		},
		_getNext: function(child, dir){
			// summary:
			//		Returns the next descendant, compared to "child".
			// child: Widget
			//		The current widget
			// dir: Integer
			//		- 1 = after
			//		- -1 = before
			// tags:
			//		abstract extension
			if(child){
				var w= this.down(child,1,true);
				if(w){
					if(w.element){
						w= w.element;
					}
					var innerNode = utils.find('.dgrid-cell', w,true);
					if(innerNode){
						return innerNode;
					}
					return w;
				}
			}
		}
	});
});

},
'xgrid/MultiRenderer':function(){
/** @module xgrid/MultiRenderer **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/Renderer',
    'dojo/_base/kernel'
], function (declare, types, Renderer,dojo) {
    /**
     * @class module:xgrid/MultiRenderer
     * @extends module:xgrid/Renderer
     */
    var Implementation = {
        renderers: null,
        selectedRenderer: null,
        lastRenderer: null,
        rendererActionRootCommand: 'View/Layout',
        runAction:function(action){
            action = this.getAction(action);
            if(action.command.indexOf(this.rendererActionRootCommand)!==-1){
                var parentAction = action.getParent ?  action.getParent() : null;
                action._originEvent = 'change';
                this.setRenderer(action.value);
                if(parentAction) {
                    parentAction.set('icon', action.get('icon'));
                    var rendererActions = parentAction.getChildren();
                    _.each(rendererActions, function (child) {
                        child._oldIcon && child.set('icon', child._oldIcon);
                    });
                }
                action.set && action.set('icon', 'fa-check');
                return true;
            }
            return this.inherited(arguments);
        },
        /**
         * Impl. set state
         * @param state
         * @returns {object|null}
         */
        setState:function(state){
            var renderer = state.selectedRenderer ? dojo.getObject(state.selectedRenderer) : null;
            if(renderer){
                this.setRenderer(renderer);
                this.set('collection',this.collection.getDefaultCollection());

            }
            return this.inherited(arguments);
        },
        /**
         * Impl. get state
         * @param state
         * @returns {object}
         */
        getState:function(state){
            state = this.inherited(arguments) || {};
            if(this.selectedRenderer) {
                state.selectedRenderer = this.getSelectedRenderer.declaredClass;
            }
            return state;
        },
        getRendererActions: function (_renderers, actions) {
            var root = this.rendererActionRootCommand,
                thiz = this,
                renderActions = [],
                renderers = _renderers || this.getRenderers(),
                VISIBILITY = types.ACTION_VISIBILITY,
                index = 1;

            actions = actions || [];

            //root
            renderActions.push(this.createAction({
                label: 'Layout',
                command: root,
                icon: 'fa-laptop',
                tab: 'View',
                group: 'Layout',
                mixin:{
                    closeOnClick:false
                },
                onCreate:function(action){
                    action.value = thiz.selectedRenderer;

                    action.setVisibility(VISIBILITY.ACTION_TOOLBAR, false).
                    setVisibility(VISIBILITY.RIBBON,{expand:true});
                }
            }));
            /**
             *
             * @param col
             * @private
             */
            function createEntry(label, icon, Renderer) {
                var selected = Renderer == thiz.selectedRenderer;
                /*
                var mapping = {
                    "change":{
                        //action to widget mapping
                        input:ActionValueWidget.createTriggerSetting('value','checked',function(event,value,mapping){
                            //return this.actionValue;
                            return value;
                        }),

                        //widget to action mapping
                        output:utils.mixin(ActionValueWidget.createTriggerSetting('checked','value',function(){
                            return this.actionValue;
                        }),{
                            ignore:function(event,value){
                                return value === false;
                            }
                        })
                    }
                };
                */

                /*
                var widgetArgs = {
                    actionValue:Renderer,
                    mapping:mapping,
                    checked: selected,
                    label:label
                };
                */

                var keycombo = 'shift f' + index;
                index++;

                var _renderer = Renderer;
                var _action = null;
                var ACTION = null;

                _action = thiz.createAction({
                    label: label,
                    command: root + '/' + label,
                    icon: icon,
                    tab: 'View',
                    group: 'Layout',
                    mixin:{
                        value:Renderer,
                        addPermission:true,
                        closeOnClick:false
                    },
                    keycombo:[keycombo],
                    onCreate:function(action){
                        action._oldIcon = icon;
                        action.actionType = types.ACTION_TYPE.SINGLE_TOGGLE;
                        //action.set('value',Renderer);
                        action.value = Renderer;
                        /*
                        var _visibilityMixin = {
                            widgetArgs: {
                                actionValue:Renderer,
                                mapping:mapping,
                                group: thiz.id+'_renderer_all',
                                checked: selected,
                                label:label,
                                iconClass: null,
                                title:'test'
                            }
                        };
                        action.setVisibility(types.ACTION_VISIBILITY_ALL,_visibilityMixin);
                        */

                    }
                });
                renderActions.push(_action);
                return renderActions;
            }

            _.each(renderers,function (Renderer) {
                var impl = Renderer.Implementation || Renderer.prototype;
                if (impl._getLabel) {
                    createEntry(impl._getLabel(), impl._getIcon(), Renderer);
                }
            });
            return renderActions;
        },
        getSelectedRenderer:function(){
            return this.selectedRenderer.prototype;
        },
        startup: function () {
            var thiz = this;
            this._on('onAddGridActions', function (evt) {
                var renderActions = thiz.getRendererActions(thiz.getRenderers(), evt.actions);
                renderActions.forEach(function (action) {
                    evt.actions.push(action);
                });
            });
            this.inherited(arguments);
            //add new root class
            this.selectedRenderer && $(this.domNode).addClass(this.getSelectedRenderer()._getLabel());
        },
        getRenderers: function () {
            return this.renderers;
        },
        setRenderer: function (renderer,_focus) {
            //track focus and selection
            var self = this,
                selection = self.getSelection(),
                focused = self.getFocused(),
                selected = self.getSelectedRenderer();

            var args = {
                'new': renderer,
                'old': self.selectedRenderer
            };
            var node$ = $(this.domNode);
            //remove renderer root css class
            node$.removeClass(selected._getLabel());
            //call renderer API
            selected.deactivateRenderer.apply(this, args);

            //tell everyone
            this._emit('onChangeRenderer', args);

            //update locals
            this.lastRenderer = this.selectedRenderer;
            this.selectedRenderer = renderer;

            //?
            this.selectedRendererClass = renderer.prototype.declaredClass;

            //add new root class
            node$.addClass(renderer.prototype._getLabel());

            //call  API
            renderer.prototype.activateRenderer.apply(this, args);

            //reset store
            this.collection.reset();

            //refresh, then restore sel/focus
            var refresh = this.refresh();

            refresh && refresh.then && refresh.then(function(){
                self._emit('onChangedRenderer', args);
            });
            return refresh;
        }
    };


    /**
     * Forward custom renderer method
     * @param who
     * @param method
     */
    function forward(who,method){
        Implementation[method]=function(){
            var parent = this.getSelectedRenderer();
            if (parent[method]) {
                return parent[method].apply(this, arguments);
            }
            return this.inherited(arguments);
        };
    }

    //@TODO: this should be all public methods in dgrid/List ?
    _.each(['row','removeRow','renderRow','insertRow','activateRenderer','deactivateRenderer'],function(method){
        forward(Implementation,method);
    });


    //package via declare
    var _class = declare('xgrid.MultiRenderer', null, Implementation);
    _class.Implementation = Implementation;

    return _class;
});
},
'xgrid/Search':function(){
/** @module xgrid/Search **/
define([
    'xdojo/declare',
    'xide/widgets/_Search',
    'dojo/on',
    'xide/Keyboard',
    'xide/types'
], function (declare,Search,on,Keyboard,types) {
    /**
     * @class module:xGrid/Search
     * */
    return declare('xgrid/Search', null,{
        _searchText:null,
        _search:null,
        runAction:function(action){
            if(action && action.command==types.ACTION.SEARCH){
                if(this._search) {
                    if(this._search.isHidden()) {
                        this._search.show('', false);
                    }else{
                        this._search.hide();
                    }
                }
            }
            return this.inherited(arguments);
        },
        buildRendering: function () {

            this.inherited(arguments);

            var grid = this,
                node = grid.domNode.parentNode,
                search = new Search({});

            search.find = function(){
                grid._searchText = this.searchInput.value;
                grid.set("collection", grid.collection);
            };
            search.showSearchBox(node);
            search.show('',false);
            search.hide();

            this._search = search;
            on(search.searchInput,'keydown',function(e){
                if(e.code ==='Escape'){
                    search.hide();
                    grid.focus();
                }
            });

            var mapping = Keyboard.defaultMapping(['ctrl f'], function(){
                search.show('',false);
            }, types.KEYBOARD_PROFILE.DEFAULT, grid.domNode, grid,null);

            this.registerKeyboardMapping(mapping);
            this._on('onAddActions',function(evt){
                var actions = evt.actions,
                    permissions = evt.permissions;
                    action = types.ACTION.SEARCH;

                if(!evt.store.getSync(action)) {
                    var _action = grid.createAction('Search', action, types.ACTION_ICON.SEARCH, ['ctrl f'], 'Home', 'File', 'item|view', null, null, null, null, null, permissions, node, grid);
                    if (!_action) {
                        return;
                    }
                    actions.push(_action);
                }
            });
        },
        _setCollection: function (collection) {
            var res = this.inherited(arguments);
            var value = this._searchText;
            var renderedCollection = this._renderedCollection;
            if (renderedCollection && value) {
                var rootFilter = new renderedCollection.Filter();
                var re = new RegExp(value, "i");
                var columns = this.columns;
                var matchFilters = [];
                for (var p in columns) {
                    if (columns.hasOwnProperty(p)) {

                        var what = columns[p].searchText || columns[p].field;

                        matchFilters.push(rootFilter.match(what, re));
                    }
                }
                var combined = rootFilter.or.apply(rootFilter, matchFilters);
                this._renderedCollection = renderedCollection.filter(combined);
                this.refresh();
            }
            return res;
        }
    });
});

},
'xgrid/component':function(){
define([
    "dojo/_base/declare",
    "xide/model/Component"
], function (declare,Component) {
    /**
     * @class xfile.component
     * @inheritDoc
     */
    return declare("xfile.component",Component, {
        /**
         * @inheritDoc
         */
        beanType:'',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        getDependencies:function(){
            return [

            ];
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return 'xgrid';
        },
        /**
         * @inheritDoc
         */
        getBeanType:function(){
            return this.beanType;
        }
    });
});


},
'xgrid/Noob':function(){
define(['dojo/_base/declare'], function (declare) {
    return declare('xgrid.Noob',null,{});
});
},
'xgrid/GridLite':function(){
/** @module xgrid/Grid **/
define([
    'dojo/_base/declare',
    'xide/types',
    './BaseLite'
],function (declare,types,Base) {
    /**
     *
     * Please read {@link module:xgrid/types}
     *
     * @class module:xgrid/Grid
     * @augments module:xgrid/Base
     */
    var grid = declare('xgrid/Grid',Base,{});

    grid.createGridClass = Base.createGridClass;

    //track defaults on module
    grid.classFactory = Base.classFactory;
    grid.DEFAULT_GRID_FEATURES = types.DEFAULT_GRID_FEATURES_LITE;
    grid.DEFAULT_GRID_BASES = Base.DEFAULT_GRID_BASES;
    grid.DEFAULT_GRID_OPTIONS = types.DEFAULT_GRID_OPTIONS;
    grid.DEFAULT_GRID_OPTION_KEYS = types.DEFAULT_GRID_OPTION_KEYS;

    return grid;
});
},
'xgrid/BaseLite':function(){
/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/typesLite',
    'xide/utils/ObjectUtils',   //possibly not loaded yet
    'xide/utils',
    'dgrid/OnDemandGrid',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'xgrid/ListRenderer',
    'xgrid/ThumbRenderer',
    'xgrid/TreeRenderer',
    'dgrid/util/misc'
], function (declare,types,
             xTypes,ObjectUtils,utils,
             OnDemandGrid,Defaults,Layout,Focus,
             ListRenderer,ThumbRenderer,TreeRenderer,
             miscUtil){

    var BASE_CLASSES = ['EVENTED','GRID','EDITOR','RENDERER','DEFAULTS','LAYOUT','FOCUS','i18'];
    var DEFAULT_GRID_FEATURES = types.DEFAULT_GRID_FEATURES_LITE;
    var GRID_BASES = types.GRID_BASES;
    var DEFAULT_GRID_OPTIONS = types.DEFAULT_GRID_OPTIONS;

    /**
     * Short hand version of declare.classFactory for our base grid
     * @param name
     * @param bases
     * @param extraClasses
     * @param implementation
     * @private
     * @returns {*}
     */
    function classFactory(name, bases, extraClasses,implementation) {
        return declare.classFactory(name, bases, extraClasses, implementation,GRID_BASES);
    }
    /**
     * Default implementation
     * @class module:xgrid/Base
     * @extends module:dgrid/List
     * @extends module:xide/mixins/EventedMixin
     */
    var Implementation = {
        _isHighlighting:false,
        _featureMap:{},
        options: utils.clone(types.DEFAULT_GRID_OPTIONS),
        getContextMenu:function(){},
        getToolbar:function(){},
        /**
         * Returns true if there is anything rendered.
         * @param item {obj|null}
         * @returns {boolean}
         */
        isRendered:function(item){
            if(!item){
                return this.bodyNode!=null;
            }
            item = this._normalize(item);
            var collection = this.collection;
            if(item){
                var itemData = item.data;
                var idProp = collection['idProperty'];
                var nodes = this.getRows(true);
                if(nodes) {
                    for (var i = 0; i < nodes.length; i++) {
                        var node = nodes[i];
                        var row = this.row(node);
                        if (row && row.data && row.data && itemData && row.data[idProp] === itemData[idProp]) {
                            return true;
                        }
                    }
                }

            }
            return false;
        },
        /**
         * highlightRow in dgrid/List leaks and is anyway not needed.
         */
        highlightRow:function(){},
        getParent:function(){
            return this._parent;
        },
        get:function(what){
            var parent = this.getParent();
            if(what==='iconClass') {
                //docker:
                if (parent && parent.icon) {
                    return parent.icon();
                }
            }
            return this.inherited(arguments);
        },
        set:function(what,value){
            var parent = this.getParent();
            if(what==='iconClass'){
                var _set = parent.set;
                if(_set) {
                    _set.apply(parent, [what, value]);
                }else if(parent && parent.icon){
                    parent.icon(value);
                    return true;
                }
            }
            if(what==='title' && value && parent){
                var _set = parent.set;
                if(_set){
                    _set.apply(parent,[what,value]);
                }else if(parent && parent.title){
                    parent.title(value);
                }
            }

            if(what==='loading'){            
                this.__loading = value;
                if(parent){
                    //docker:
                    if(parent.startLoading) {
                        var icon = parent._options.icon;
                        if (value === true) {
                            parent.startLoading('', 0.5);
                            parent.icon('fa-spinner fa-spin');
                        } else {
                            parent.finishLoading();
                            parent.icon(icon);
                        }
                        return true;
                    }else if(parent.set){
                        parent.set('loading',value);
                    }
                }
            }
            return this.inherited(arguments);
        },
        runAction:function(action){
            if(action.command==types.ACTION.HEADER){
                this._setShowHeader(!this.showHeader);
            }
            return this.inherited(arguments);
        },
        highlight:function(highlight){
            var node = $(this.domNode.parentNode);
            if(highlight){
                if(this._isHighlighting){
                    return;
                }
                this._isHighlighting = true;
                node.addClass('highlight');
            }else{

                this._isHighlighting=false;
                node.removeClass('highlight');
            }
        },
        getState:function(state) {
            state = this.inherited(arguments) || {};
            state.showHeader = this.showHeader;
            return state;
        },
        postMixInProperties: function () {
            var state = this.state;
            if (state) {
                this.showHeader = state.showHeader;
            }
            return this.inherited(arguments);
        },
        renderArray:function(array){
            if(this.__loading){
                return [];
            }
            this._lastData = array;            
            return this.inherited(arguments);
        },
        getData:function(){
            return this._lastData;
        },
        refreshItem:function(item,silent){
            if (silent) {
                this._muteSelectionEvents = true;
            }
            this.collection.emit('update', {
                target: item
            });
            if (silent) {
                this._muteSelectionEvents = false;
            }
        },
        onShow:function(){
            this._emit(types.EVENTS.ON_VIEW_SHOW,this);
            return this.inherited(arguments);
        },
        isActive:function(testNode){
            return utils.isDescendant(this.domNode,testNode || document.activeElement);
        },
        _showHeader:function(show){
            $(this.domNode).find('.dgrid-header').each(function(i,el){
                $(el).css('display',show ? '' : 'none' );
            });

            $(this.domNode).find('.dgrid-scroller').each(function(i,el){
                $(el).css('margin-top',show ? 26 : 0 );
            });

        },
        destroy:function(){
            this._emit('destroy',this);
            return this.inherited(arguments);
        },
        hasFeature:function(name){
            return _contains(['name'],_.keys(this._featureMap));
        },
        /**
         * Return current row's elements or data
         * @param domNodes {boolean} return dom instead of data. Default false.
         * @param filterFunction
         * @returns {*}
         */
        getRows:function(domNodes,filterFunction){
            var result = [],
                self = this;
            var nodes = $(self.domNode).find('.dgrid-row');
            _.each(nodes,function(node){
                var _row = self.row(node);
                if(_row && _row.element){
                    result.push(_row[domNodes ? 'element' : 'data']);
                }
            });
            if (filterFunction) {
                return result.filter(filterFunction);
            }
            return result;
        },
        startup:function(){
            var result = this.inherited(arguments);
            if(this.columns) {
                _.each(this.columns,function(column){
                    if (column.width) {
                        this.styleColumn(parseInt(column.id), 'width:' + column.width);
                    }
                },this);
            }

            var self = this;
            this.showExtraSpace && this.on('dgrid-refresh-complete',function(){
                var rows = self.getRows();
                var _extra = $(self.contentNode).find('.dgrid-extra');
                if(!rows.length){
                    return;
                }

                if(!_extra.length){
                    _extra = $('<div class="dgrid-extra" style="width:100%;height:80px"></div>');
                    $(self.contentNode).append(_extra);
                    _extra.on('click',function(){
                        self.deselectAll();
                    });
                    _extra.on('contextmenu',function(){
                        self.deselectAll();
                    })
                }
            });

            return result;
        },
        removeRow:function(){
            var res = this.inherited(arguments);
            var self = this;
            if(this.showExtraSpace) {
                var rows = self.getRows();
                var _extra = $(self.contentNode).find('.dgrid-extra');
                if (!rows.length) {
                    _extra.remove();
                }
            }
            return res;
        }
    };
    /**
     * Create root class with declare and default implementation
     */
    var _default = declare('xgrid.DefaultLite', null, Implementation);

    /**
     * 2-dim array search
     * @param left {string[]}
     * @param keys {string[]}
     * @returns {boolean}
     * @private
     */
    function _contains(left, keys) {
        return keys.some(function (v) {
            return left.indexOf(v) >= 0;
        });
    }

    /**
     * Find default keys in a feature struct and recompse user feature
     * @param feature {object} feature struct
     * @param defaultFeature {object}
     * @returns {object} recomposed feature
     */
    function getFeature(feature, defaultFeature) {
        //is new feature, return the mix of default props and customized version
        if (_contains(['CLASS','CLASSES','IMPLEMENTATION'],_.keys(feature))) {
            return utils.mixin(utils.cloneKeys(defaultFeature),feature);
        }
        return defaultFeature;
    }

    /**
     * Grid class factory
     * @param name {string} A name for the class created
     * @param baseClass {object} the actual implementation (default root class, declared above)
     * @param features {object} the feature map override
     * @param gridClasses {object} the base grid classes map override
     * @param args {object} root class override
     * @param _defaultBases {object}
     * @memberOf module:xgrid/Base
     * @returns {module:xgrid/Base}
     */
    function createGridClass(name, baseClass, features, gridClasses, args,_defaultBases) {
        var _isNewBaseClass = false;
        baseClass = baseClass || _default;
        //simple case, no base class and no features
        if (!baseClass && !features) {
            return _default;
        }
        if (baseClass) {
            _isNewBaseClass = _contains(BASE_CLASSES,_.keys(gridClasses));
            var defaultBases = utils.cloneKeys(_defaultBases || GRID_BASES);
            if (_isNewBaseClass) {
                utils.mixin(defaultBases, gridClasses);
                //remove empty
                defaultBases = _.pick(defaultBases, _.identity);
            }
            //recompose base class
            baseClass = classFactory(name, defaultBases, [_default], baseClass);
        }

        var newFeatures = [],
            featureMap = {};

        //case: base class and features
        if (baseClass && features) {
            var _defaultFeatures = utils.cloneKeys(DEFAULT_GRID_FEATURES);
            utils.mixin(_defaultFeatures, features);

            for (var featureName in _defaultFeatures) {
                var feature = _defaultFeatures[featureName];
                if (!_defaultFeatures[featureName]) {
                    continue;
                }
                var newFeature = null;
                if (feature === true) {
                    //is a base feature
                    newFeature = DEFAULT_GRID_FEATURES[featureName];
                } else if (DEFAULT_GRID_FEATURES[featureName]) {
                    //is new/extra feature
                    newFeature = getFeature(feature, DEFAULT_GRID_FEATURES[featureName]);
                } else {
                    //go on
                    newFeature = feature;
                }
                if (newFeature) {
                    var featureClass = classFactory(featureName, newFeature['CLASSES'] || [], [newFeature['CLASS']], newFeature['IMPLEMENTATION']);
                    newFeatures.push(featureClass);
                    featureMap[featureName]=featureClass;
                }
            }
            //recompose
            if (newFeatures.length > 0) {
                baseClass = classFactory(name, [baseClass], newFeatures, args);
            }
            //complete
            baseClass.prototype._featureMap = featureMap;
        }
        return baseClass;
    }


    var Module = createGridClass('xgrid/Base',{
            options: utils.clone(DEFAULT_GRID_OPTIONS)
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            COLUMN_HIDER: false
        },
        //bases, no modification
        null,
        {

        });

    Module.createGridClass = createGridClass;

    //track defaults on module
    Module.classFactory = classFactory;
    Module.DEFAULT_GRID_FEATURES = DEFAULT_GRID_FEATURES;
    Module.DEFAULT_GRID_BASES = GRID_BASES;
    Module.DEFAULT_GRID_OPTIONS = DEFAULT_GRID_OPTIONS;
    Module.DEFAULT_GRID_OPTION_KEYS = types.DEFAULT_GRID_OPTION_KEYS;

    return Module;

});
},
'xgrid/typesLite':function(){
/** @module xgrid/types **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/Selection',
    'xgrid/Keyboard',
    'xgrid/ColumnHider',
    'xide/mixins/EventedMixin',
    'dgrid/OnDemandGrid',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'xgrid/ListRenderer',
    'xgrid/Clipboard',
    'xgrid/Actions',
    'xlang/i18'
], function (declare,types,
             Selection,_GridKeyboardSelection,ColumnHider,
             EventedMixin, OnDemandGrid,Defaults,Layout,Focus,
             ListRenderer,
             Clipboard,Actions,i18)
{
    /**
     * Grid Bases
     * @enum module:xgrid/types/GRID_BASES
     * @memberOf module:xgrid/types
     */
    types.GRID_BASES = {
        GRID: OnDemandGrid,
        LAYOUT:Layout,
        DEFAULTS: Defaults,
        RENDERER: ListRenderer,
        EVENTED: EventedMixin,
        FOCUS:Focus,
        i18:i18
    };
    /**
     * Default Grid Options
     * @deprecated
     * @enum module:xgrid/types/DEFAULT_GRID_OPTIONS
     * @memberOf module:xgrid/types
     */
    types.DEFAULT_GRID_OPTIONS = {
        /**
         * Instruct the grid to add jQuery theme classes
         * @default true
         * @type {bool}
         * @constant
         */
        USE_JQUERY_CSS: false,
        /**
         * Behaviour flag to deselect an item when its already selected
         * @default true
         * @type {bool}
         * @constant
         */
        DESELECT_SELECTED: true,
        /**
         * Behaviour flag to clear selection when clicked on the container node
         * @default true
         * @type {bool}
         * @constant
         */
        CLEAR_SELECTION_ON_CLICK: true,
        /**
         * Item actions
         * @default true
         * @type {object}
         * @constant
         */
        ITEM_ACTIONS: {},
        /**
         * Grid actions (sort, hide column, layout)
         * @default true
         * @type {object}
         * @constant
         */
        GRID_ACTIONS: {},
        /**
         * Publish selection change globally
         * @default true
         * @type {boolean}
         * @constant
         */
        PUBLISH_SELECTION: false
    };
    /**
     * Grid option keys
     * @enum module:xgrid/types/GRID_OPTION
     * @memberOf module:xgrid/types
     */
    types.GRID_OPTION = {
        /**
         * Instruct the grid to add jQuery theme classes
         * @default true
         * @type {string}
         * @constant
         */
        USE_JQUERY_CSS: 'USE_JQUERY_CSS',
        /**
         * Behaviour flag to deselect an item when its already selected
         * @default true
         * @type {string}
         * @constant
         */
        DESELECT_SELECTED: 'DESELECT_SELECTED',
        /**
         * Behaviour flag to deselect an item when its already selected
         * @default true
         * @type {string}
         * @constant
         */
        CLEAR_SELECTION_ON_CLICK:'CLEAR_SELECTION_ON_CLICK',
        /**
         * Actions
         * @default true
         * @type {string}
         * @constant
         */
        ITEM_ACTIONS:'ITEM_ACTIONS',
        /**
         * Actions
         * @default true
         * @type {string}
         * @constant
         */
        GRID_ACTIONS:'GRID_ACTIONS'
    };
    /**
     * All grid default features
     * @enum module:xgrid/types/GRID_DEFAULT_FEATURES
     * @memberOf module:xgrid/types
     */
    types.DEFAULT_GRID_FEATURES_LITE = {
        SELECTION: {
            CLASS: Selection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        KEYBOARD_SELECTION: {
            CLASS: _GridKeyboardSelection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        COLUMN_HIDER: {
            CLASS: ColumnHider,
            IMPLEMENTATION: {},
            CLASSES: null
        }

    };
    /**
     * All Grid Features for easy access
     * @enum module:xgrid/types/GRID_FEATURES
     * @memberOf module:xgrid/types
     */
    types.GRID_FEATURES_LITE = {
        SELECTION: {
            CLASS: Selection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        KEYBOARD_SELECTION: {
            CLASS: _GridKeyboardSelection,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        ACTIONS: {
            CLASS: Actions,
            IMPLEMENTATION: {},
            CLASSES: null
        },
        CLIPBOARD:{
            CLASS:Clipboard,
            IMPLEMENTATION:{},
            CLASSES:null
        },
        COLUMN_HIDER: {
            CLASS: ColumnHider,
            IMPLEMENTATION: {},
            CLASSES: null
        }
    };
    return declare(null,[],{});
});
}}});
/** @module xgrid/Base **/
define("xgrid/xgrid", [
], function (){
    return null;
});