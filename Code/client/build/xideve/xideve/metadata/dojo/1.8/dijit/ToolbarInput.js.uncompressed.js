define("xideve/metadata/dojo/1.8/dijit/ToolbarInput", [
	"dojo/_base/declare",
	"./layout/ContainerInput"
], function(
	declare,
	ContainerInput
) {

return declare(ContainerInput, {

	propertyName: "label",
	supportsHTML: true
	
});

});