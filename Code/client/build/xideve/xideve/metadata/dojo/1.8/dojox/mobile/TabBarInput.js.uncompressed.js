define("xideve/metadata/dojo/1.8/dojox/mobile/TabBarInput", [
	"dojo/_base/declare",
	"../../dijit/layout/ContainerInput"
], function(
	declare,
	ContainerInput
) {

return declare(ContainerInput, {

	propertyName: "label"
	
});

});