//>>built
define("xideve/metadata/html/0.8/html/table/TableCellInput",["dojo/_base/declare","davinci/ve/input/SmartInput"],function(d,e){return d(e,{property:"textContent",displayOnCreate:"false",constructor:function(){},serialize:function(a,b,c){a=c;"\x26nbsp;"==c.trim()&&(a="");b(a)},parse:function(a){var b=a;""==a.trim()&&(b="\x26nbsp;");return b}})});
//# sourceMappingURL=TableCellInput.js.map