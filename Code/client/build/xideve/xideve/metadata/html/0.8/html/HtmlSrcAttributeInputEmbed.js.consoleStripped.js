define("xideve/metadata/html/0.8/html/HtmlSrcAttributeInputEmbed", [
	"dojo/_base/declare",
	"./HtmlSrcAttributeInput"
], function(declare, HtmlSrcAttributeInput){

return declare(HtmlSrcAttributeInput, {

	constructor: function(/*Object*/args) {
		this.supportsAltText = false;
	}
});
});