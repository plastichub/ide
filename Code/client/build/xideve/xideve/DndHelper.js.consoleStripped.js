define("xideve/DndHelper", [
    'dojo/_base/declare',
    "davinci/ui/dnd/DragManager",   //this creates already an instance of DragManager
    "davinci/ve/utils/GeomUtils",
    "davinci/ui/dnd/DragSource"


],function(declare,DragManager,GeomUtils,DragSource)
{
    /**
     * This class makes leg work to add custom parts into a ve document
     *
     * @class xide/DndHelper
     */
    var dndHelper =declare("xideve/DndHelper",null,{

        _onDragStart: function(e){
            var data = e.dragSource.data;
             0 && console.log('drag start ',data);

        },
        _onDragEnd: function(e){
            var data = e.dragSource.data;
             0 && console.log('drag end ',data);
        },
        _updateDragManager:function(context){
            DragManager.document = context.getDocument();
            var frameNode = context.frameNode;
            if(frameNode){
                var coords = dojo.coords(frameNode);
                var containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
            }
        }

    });

    var _dragStart = function(from,node,item,context,e){

        //this.selectedItem = null;
        var data = e.dragSource.data;
        Metadata.getHelper(data.type, 'tool').then(function(ToolCtor) {
// Copy the data in case something modifies it downstream -- what types can data.data be?
            var tool = new (ToolCtor || CreateTool)(dojo.clone(data.data),data.userData);
            this._context.setActiveTool(tool);
        }.bind(this));
// Sometimes blockChange doesn't get cleared, force a clear upon starting a widget drag operation
        this._context.blockChange(false);
// Place an extra DIV onto end of dragCloneDiv to allow
// posting a list of possible parent widgets for the new widget
// and register the dragClongDiv with Context
        if(e._dragClone){
            domClass.add(e._dragClone, 'paletteDragContainer');
            dojo.create('div',{className:'maqCandidateParents'}, e._dragClone);
        }
//FIXME: Attach dragClone and event listeners to tool instead of context?
        this._context.setActiveDragDiv(e._dragClone);
        this._dragKeyDownListener = dojo.connect(document, 'onkeydown', dojo.hitch(this,function(event){
            var tool = this._context.getActiveTool();
            if(tool && tool.onKeyDown){
                tool.onKeyDown(event);
            }
        }));
        this._dragKeyUpListener = dojo.connect(document, 'onkeyup', dojo.hitch(this,function(event){
            var tool = this._context.getActiveTool();
            if(tool && tool.onKeyUp){
                tool.onKeyUp(event);
            }
        }));
    };


    var makeDND = function(from,node,item,context){

        var clone = node.domNode;

        var ds = new DragSource(node.domNode, "component", node, clone);

        ds.targetShouldShowCaret = true;
        ds.returnCloneOnFailure = false;

        /**
         * outer handlers:
         */
        from.connect(ds, "onDragStart", dojo.hitch(from,function(e){
             0 && console.log('on drag start ');
            _dragStart(e);
        })); // move start

        from.connect(ds, "onDragEnd", dojo.hitch(from,function(e){
             0 && console.log('on drag end');
            from.onDragEnd(e);
        })); // move end


        /**
         * now the inner handlers
         */
        node.connect(node.domNode, "onmouseover", function(e){
            node._mouseover = true;
            var div = node.domNode;
            //  0 && console.log('mouse over',e);
        });

        node.connect(node.domNode, "onmouseout", function(e){
            node._mouseover = false;
            var div = node.domNode;
            // 0 && console.log('mouse out');

        });


        node.connect(node.domNode, "onmousedown", function(e){

             0 && console.log('mouse down');
            DragManager.document = context.getDocument();
            var frameNode = context.frameNode;
            if(frameNode){
                var coords = dojo.coords(frameNode);
                var containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
                 0 && console.log('drag manager updated ' , DragManager);
            }
        });

    };

    dndHelper.makeDND = makeDND;


    return dndHelper;


});