/** @module xideve/views/VisualEditor **/
define("xideve/views/VisualEditor.bak", [
    'dcl/dcl',
    "dojo/dom-construct",
    "dojo/Deferred",
    'dojo/promise/all',
    "require",
    'xide/layout/ContentPane',
    'xide/layout/_Accordion',
    'xide/utils',
    'xide/types',
    'xide/factory',
    "davinci/ve/PageEditor",
    "davinci/ve/palette/Palette",
    "davinci/Runtime",
    "davinci/Workbench",
    "dojo/_base/connect",
    "dojo/dom-class",
    "davinci/ve/metadata",
    'davinci/ve/views/SwitchingStyleView',
    'xideve/views/OutlineView',
    'xideve/views/OutlineGridView',
    'xide/registry',
    'xide/layout/_TabContainer',
    'xideve/views/VisualEditorPalette',
    'xideve/views/VisualEditorSourceEditor',
    'xideve/views/VisualEditorLayout',
    'xideve/views/VisualEditorAction',
    'xideve/views/VisualEditorTools',
    'xaction/Action',
    "davinci/ve/widget",
    'xide/editor/Default',
    'xide/_base/_Widget',
    'xide/mixins/ReloadMixin',
    "xblox/model/html/SetStyle",
    "xblox/model/html/SetCSS",
    "xblox/model/html/SetState",
    "wcDocker/splitter",
    "xdocker/Splitter2",
    "wcDocker/types"
], function (dcl, domConstruct, Deferred, all, require, ContentPane, AccordionContainer, utils, types, factory, PageEditor, Palette, Runtime, Workbench, connect, domClass,
    Metadata, SwitchingStyleView, OutlineView, OutlineGridView, registry,
    _TabContainer, VisualEditorPalette, VisualEditorSourceEditor, VisualEditorLayout,
    VisualEditorAction, VisualEditorTools, Action, widgetUtils, Default, _Widget, ReloadMixin,
    SetStyle, SetCSS, SetState, splitter, Splitter2, wcDocker) {


    $.fn.inlineStyle = function (prop) {
        var styles = this.attr("style"),
            value;
        styles && styles.split(";").forEach(function (e) {
            var style = e.split(":");
            if ($.trim(style[0]) === prop) {
                value = style[1];
            }
        });
        return value;
    };

    var debugFileChanges = false;
    var debugBlocks = false;
    var debugCSSEditor = false;


    if (sctx) {

        window.updateBodyFrame = function (container, context, frame) {


            var self = this;

            console.clear();

            /*
             function getWindowScroll(el) {
             var elWin = getElementWindow(el)
             , elHtml = elWin.document.documentElement || html
             return { x: elWin.pageXOffset || elHtml.scrollLeft, y: elWin.pageYOffset || elHtml.scrollTop }
             }

             function getElementWindow(el) {
             if (el.contentWindow) return el.contentWindow
             if (elDoc = el.ownerDocument) return elDoc.defaultView || elDoc.parentWindow
             return win
             }

             */
            var pos = container.offset();

            var top = pos.top;
            var left = pos.left;


            var rootWidget = context ? $(context.rootNode) : null;
            var bodyElement = context ? $(context.getDocumentElement().getChildElement("body")) : null;
            var width = container.css('width');
            var height = container.css('height');
            context.rootNode.scrollLeft = context.rootNode.parentNode.scrollLeft;

            var sizeBodyH = false;
            var sizeBodyW = false;

            if (bodyElement) {
                var maxWidth = bodyElement.inlineStyle('max-width');
                if (maxWidth && maxWidth !== 'none') {
                    //  width = maxWidth;
                }
                var maxHeight = bodyElement.inlineStyle('max-height');
                if (maxHeight && maxHeight !== 'none') {
                    // height = maxHeight;
                }
                //var _width2 = bodyElement).inlineStyle('width');
                var _width = bodyElement.inlineStyle('width');
                if (_width) {
                    //  width = maxWidth;
                    sizeBodyW = _width;
                }
                var _height = bodyElement.inlineStyle('height');
                if (_height) {
                    // height = maxHeight;
                    sizeBodyH = _height;
                }

            }

            $(frame).css('display', (self._hidden ? 'none' : 'block'));


            if (top == 0 && left == 0) {
                return;
            }
            frame.css('top', top);
            frame.css('left', left);

            if (width !== 'none') {

                frame.css('width', width);
                frame.css('height', height);
                var body = $(self.getEditorContext().rootNode);
                //frame.css('width', width);
                //frame.css('height', height);

                var html = body.parent();

                if (sizeBodyW) {

                    container.css('overflow', 'auto');
                    frame.css('width', width);
                    frame.css('height', height);
                    html.css('width', width);
                    html.css('height', height);
                    //sizeBodyH && html.css('height', height);
                    //sizeBodyW && html.css('width', width);
                     0 && console.log('setting body w ' + sizeBodyW);
                    body.css('height', width);
                    body.css('width', height);
                    //sizeBodyH && body.css('height', sizeBodyH);
                    //sizeBodyW && body.css('width', sizeBodyW);
                }
                //sizeBodyH && body.css('height', height);
                //sizeBodyW && body.css('width', width);
                if (!sizeBodyH && !sizeBodyW) {

                    //body.css('width', width);
                    //body.css('height', height);
                }

            }

        }
    }

    Splitter2.prototype.collapse2 = function (side, pos) {

        this._isCollapsed = true;

        this._pane[0].collapsed = side == 0;
        this._pane[1].collapsed = side == 1;

        this._saveProp('pos');

        //setFramePropsMin(this._pane[0],side==0);
        //setFramePropsMin(this._pane[1],side==1);
        this._lastCollapsed = side;

        console.error('collapse ' + side + ' pos : ' + pos);
        this._lastCollapsed = side;
        this.pos(pos);

        /*
         setFramePropsMin(this._pane[0],side==0);
         setFramePropsMin(this._pane[1],side==1);

         this.pos( pos!=null ? pos : 1);
         */
    };
    Splitter2.prototype.expand2 = function () {
        this._isCollapsed = false;
        this._pane[0].collapsed = false;
        this._pane[1].collapsed = false;
        this._restoreProp('pos');
        //console.error('collapse '+side + ' pos : ' +pos);
        /*
         restoreFrameProps(this._pane[0]);
         this._restoreProp('pos');
         restoreFrameProps(this._pane[1]);
         this._lastCollapsed = null;
         */
    }
    // Updates the size of the splitter.
    function __update(opt_dontMove) {
        var width = this.$container.outerWidth();
        var height = this.$container.outerHeight();

        var minSize = this.__minPos();
        var maxSize = this.__maxPos();

        var collapsed = this._pane[0].collapsed === true || this._pane[1].collapsed === true;


        if (collapsed) {
            minSize = {
                x: 0,
                y: 0
            }
        }


        if (this._findBestPos) {
            this._findBestPos = false;

            var size1;
            var size2;
            if (this._pane[0] && typeof this._pane[0].initSize === 'function') {
                size1 = this._pane[0].initSize();
                if (size1) {
                    if (size1.x < 0) {
                        size1.x = width / 2;
                    }
                    if (size1.y < 0) {
                        size1.y = height / 2;
                    }
                }
            }

            if (this._pane[1] && typeof this._pane[1].initSize === 'function') {
                size2 = this._pane[1].initSize();
                if (size2) {
                    if (size2.x < 0) {
                        size2.x = width / 2;
                    }
                    if (size2.y < 0) {
                        size2.y = height / 2;
                    }

                    size2.x = width - size2.x;
                    size2.y = height - size2.y;
                }
            }

            var size;
            if (size1 && size2) {
                size = {
                    x: Math.min(size1.x, size2.x),
                    y: Math.min(size1.y, size2.y)
                };
            } else if (size1) {
                size = size1;
            } else if (size2) {
                size = size2;
            }

            if (size) {
                if (this._orientation) {
                    this._pos = size.x / width;
                } else {
                    this._pos = size.y / height;
                }
            }
        }

        this.$bar.toggleClass('wcSplitterBarStatic', this.__isStatic());

        if (this._orientation === wcDocker.ORIENTATION.HORIZONTAL) {
            var barSize = this.$bar.outerWidth() / 2;
            var barBorder = parseInt(this.$bar.css('border-top-width')) + parseInt(this.$bar.css('border-bottom-width'));
            if (opt_dontMove) {
                var offset = this._pixelPos - (this.$container.offset().left + parseInt(this.$container.css('border-left-width'))) - this.$bar.outerWidth() / 2;
                this._pos = offset / (width - this.$bar.outerWidth());
            }

            this._pos = Math.min(Math.max(this._pos, 0), 1);
            var size = (width - this.$bar.outerWidth()) * this._pos + barSize;
            if (minSize) {
                size = Math.max(minSize.x, size);
            }
            if (maxSize) {
                size = Math.min(maxSize.x, size);
            }

            var top = 0;
            var bottom = 0;
            if (this._parent && this._parent.declaredClass === 'wcCollapser') {
                var $outer = this.docker().$container;
                var $inner = this._parent.$container;

                top = $inner.offset().top - $outer.offset().top;
                bottom = ($outer.offset().top + $outer.outerHeight()) - ($inner.offset().top + $inner.outerHeight());
            }

            // Bar is top to bottom
            this.$bar.css('left', size - barSize);
            this.$bar.css('top', top);
            this.$bar.css('height', height - barBorder - bottom);

            this.$pane[0].css('width', size - barSize);
            this.$pane[0].css('left', '0px');
            this.$pane[0].css('right', '');
            this.$pane[0].css('top', top);
            this.$pane[0].css('bottom', bottom);

            this.$pane[1].css('left', '');
            this.$pane[1].css('right', '0px');
            this.$pane[1].css('width', width - size - barSize - parseInt(this.$container.css('border-left-width')) * 2);
            this.$pane[1].css('top', top);
            this.$pane[1].css('bottom', bottom);

            this._pixelPos = this.$bar.offset().left + barSize;
        } else {
            var barSize = this.$bar.outerHeight() / 2;
            var barBorder = parseInt(this.$bar.css('border-left-width')) + parseInt(this.$bar.css('border-right-width'));
            if (opt_dontMove) {
                var offset = this._pixelPos - (this.$container.offset().top + parseInt(this.$container.css('border-top-width'))) - this.$bar.outerHeight() / 2;
                this._pos = offset / (height - this.$bar.outerHeight());
            }

            this._pos = Math.min(Math.max(this._pos, 0), 1);
            var size = (height - this.$bar.outerHeight()) * this._pos + barSize;
            if (minSize) {
                size = Math.max(minSize.y, size);
            }
            if (maxSize) {
                size = Math.min(maxSize.y, size);
            }

            var left = 0;
            var right = 0;
            if (this._parent && this._parent.declaredClass === 'wcCollapser') {
                var $outer = this.docker().$container;
                var $inner = this._parent.$container;

                left = $inner.offset().left - $outer.offset().left;
                right = ($outer.offset().left + $outer.outerWidth()) - ($inner.offset().left + $inner.outerWidth());
            }

            // Bar is left to right
            this.$bar.css('top', size - barSize);
            this.$bar.css('left', left);
            this.$bar.css('width', width - barBorder - bottom);
            this.$pane[0].css('height', size - barSize);
            this.$pane[0].css('top', '0px');
            this.$pane[0].css('bottom', '');
            this.$pane[0].css('left', left);
            this.$pane[0].css('right', right);
            this.$pane[1].css('top', '');
            this.$pane[1].css('bottom', '0px');
            this.$pane[1].css('height', height - size - barSize - parseInt(this.$container.css('border-top-width')) * 2);
            this.$pane[1].css('left', left);
            this.$pane[1].css('right', right);

            this._pixelPos = this.$bar.offset().top + barSize;
        }

        if (opt_dontMove === undefined) {
            opt_dontMove = true;
        }
        this._pane[0] && this._pane[0].__update(opt_dontMove);
        this._pane[1] && this._pane[1].__update(opt_dontMove);
        // 0 && console.log('pos ' + this.pos());
    }

    /**
     * * @augments module:xideve/views/VisualEditorPalette
     * @augments module:xideve/views/VisualEditorSourceEditor
     * @augments module:xideve/views/VisualEditorTools
     * @augments module:xideve/views/VisualEditorLayout
     * @augments module:xideve/views/VisualEditorAction
     */

    /**
     * @class module:xideve/views/VisualEditor
     * @extends module:xide/_base/_Widget
     */
    var Module = dcl([_Widget, VisualEditorPalette, VisualEditorSourceEditor, VisualEditorLayout, VisualEditorAction, VisualEditorTools, ReloadMixin.dcl], {
        updateFrame: function () {

            var ctx = this.getEditorContext();
            if (ctx && ctx.isResizing) {
                return;
            }

            var pageEditor = this.editor,
                frame = this.getFrame(),
                self = this;



            function follow(what, target) {

                var frame = $(what);
                var container = $(target);
                var pos = container.offset();
                var top = pos.top;
                var left = pos.left;
                var context = self.getEditorContext();

                var rootWidget = context ? $(context.rootNode) : null;
                var bodyElement = context ? $(context.getDocumentElement().getChildElement("body")) : null;
                var width = container.css('width');
                var height = container.css('height');

                var sizeBodyH = false;
                var sizeBodyW = false;

                if (bodyElement) {
                    var maxWidth = bodyElement.inlineStyle('max-width');
                    if (maxWidth && maxWidth !== 'none') {
                        //  width = maxWidth;
                    }
                    var maxHeight = bodyElement.inlineStyle('max-height');
                    if (maxHeight && maxHeight !== 'none') {
                        // height = maxHeight;
                    }
                    //var _width2 = bodyElement).inlineStyle('width');
                    var _width = bodyElement.inlineStyle('width');
                    if (_width) {
                        //  width = maxWidth;
                        sizeBodyW = _width;
                    }
                    var _height = bodyElement.inlineStyle('height');
                    if (_height) {
                        // height = maxHeight;
                        sizeBodyH = _height;
                    }

                }

                $(frame).css('display', (self._hidden ? 'none' : 'block'));


                if (top == 0 && left == 0) {
                    return;
                }
                frame.css('top', top);
                frame.css('left', left);
                if (width !== 'none') {
                    frame.css('width', width);
                    frame.css('height', height);
                    var body = $(self.getEditorContext().rootNode);
                    frame.css('width', width);
                    frame.css('height', height);

                    sizeBodyH && body.css('height', sizeBodyH);
                    sizeBodyW && body.css('width', sizeBodyW);
                    if (!sizeBodyH && !sizeBodyW) {
                        body.css('width', width);
                        body.css('height', height);
                    }

                }
            }

            if (frame) {
                if (!frame._followTimer) {
                    frame._followTimer = setInterval(function () {
                        follow(frame, pageEditor._designCP.containerNode);
                    }, 500);
                }
            }

        },
        doSplit: function (mode) {
            var pe = this.getPageEditor();
            var newMode = 'design';

            var sourcePane = pe.getSourcePane();
            var designPane = pe.getSourcePane();

            var splitter2 = sourcePane.getSplitter();
            var splitter = designPane.getSplitter();
            switch (mode) {


                case types.VIEW_SPLIT_MODE.DESIGN:
                    {
                        console.error('splitter ', splitter);
                        console.error('splitter ', splitter2);
                        if (splitter) {
                            if (splitter.isCollapsed()) {
                                splitter.expand();
                            } else {
                                splitter.collapse(0, 0.9);
                            }
                        }

                        break;
                    }
                case types.VIEW_SPLIT_MODE.SOURCE:
                    {

                        splitter._pane[0].collapsed = true;
                        splitter._pane[1].collapsed = false;

                        if (splitter) {
                            if (splitter.isCollapsed()) {
                                splitter.expand();
                            } else {
                                splitter.collapse(1, 0.1);
                            }
                        }
                        break;
                    }
                case types.VIEW_SPLIT_MODE.SPLIT_VERTICAL:
                    {
                        newMode = 'splitVertical';
                        break;
                    }
                case types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL:
                    {
                        newMode = 'splitHorizontal';
                        break;
                    }
            }

            return;

            //this.editor.switchDisplayMode(newMode);
            var thiz = this;
            var pe = thiz.getPageEditor();


        },
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Widget': 80,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Step': 5,
            'New': 4,
            'Window': 1
        },
        declaredClass: "xideve/views/VisualEditor",
        templateString: "<div style='height: 100%;width: 100%' attachTo='containerNode'></div>",
        isVisualEditor: true,
        resizeToParent: true,
        showLESS: true,
        showBehaviours: false,
        showStyleView: true,
        blockScopes: null,
        onReload: function () {},
        /**
         * leftLayoutContainer is passed by xfile::PanelManager::createEditor.
         * its a stacked container.
         */
        leftLayoutContainer: null,
        /**
         * rightLayoutContainer is passed by xfile::PanelManager::createEditor.
         * its a stacked container.
         */
        rightLayoutContainer: null,

        /** Palette */
        palette: null,
        paletteContainer: null,
        showPalette: true,

        /** Style */
        styleView: null,
        styleViewContainer: null,

        /** Outline **/
        addOutline: true, //enable outline view

        outlineView: null,
        outlineContainer: null,

        /** Outline Grid View **/
        outlineGridView: null,
        outlineGridContainer: null,
        /**
         * Instance to the actual visual editor
         *
         * @type {module:davinci/ve/PageEditor}
         * */
        editor: null,
        _designMode: true,
        _tempLeftContainer: null,
        textEditor: null,

        /**
         * is editor for a deliteful scene
         */
        delite: false,

        /**
         * @type {xideve/Template}
         */
        template: null,

        contextInfo: null,
        didAddSheetEditors: false,
        beanType: types.ITEM_TYPE.WIDGET,

        _didInitial: false,
        ready: false,
        destroyed: false,
        _skipTextEditorChange: false,
        _getHTMLBlocks: function (scope, owner, target, group, items) {
            items.push({
                name: 'HTML',
                iconClass: 'fa-paint-brush',
                items: [{
                        name: 'Set Style',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetStyle,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    },
                    {
                        name: 'Set CSS',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetCSS,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    },
                    {
                        name: 'Set State',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetState,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    }
                ]
            });
            return items;
        },
        onBuildBlockInfoListEnd: function (evt) {
            var owner = evt.owner,
                ve = owner ? owner.visualEditor : null;
            if (!ve || ve !== this) {
                return;
            }
            this._getHTMLBlocks(evt.scope, evt.view, evt.item, evt.group, evt.items);
        },
        __reloadModule: function () {
            //console.error('reload module');
            Reload.reload().then(function (data) {
                var component = data.component;
                component.registerEditors();
            });
        },
        updateFocus: function () {
            var ctx = this.getEditorContext();
            if (ctx) {
                if (ctx.isRezing) {
                    return;
                }
                ctx.clearCachedWidgetBounds();
                ctx.updateFocusAll();
            }
        },
        set: function (key, value) {
            if (key === 'focused') {
                this.updateFocus();
            }
            return this.inherited(arguments);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Layout
        //
        /////////////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Selection
        //
        /////////////////////////////////////////////////////////////////////////////////////
        select: function (mixed, append) {
            mixed = _.isArray(mixed) ? mixed : [mixed];
            var first = mixed[0];

            if (!first) {
                return;
            }

            var context = this.getEditorContext();
            if (!context) {
                return;
            }

            if (_.isString(first)) {

                var _resolved = [];
                _.each(mixed, function (str) {
                    var widget = context.getWidget(str);
                    widget && _resolved.push(widget);
                });
                mixed = _resolved;
            }


            if (append !== true) {
                context.deselect();
            }

            _.each(mixed, function (widget) {
                context.select(widget, true, false);
            });
        },
        selectAll: function () {
            var context = this.getEditorContext(),
                widgets = context.getAllWidgets();
            _.each(widgets, function (widget) {
                context.select(widget, true, false);
            });
        },
        updateToolbars: function () {},
        getTemplateDirectoryPathItem: function () {
            return {
                mount: 'workspace',
                path: './templates'
            }
        },
        getSelection: function () {
            var _ctx = this.getEditorContext();
            var out = [];
            if (_ctx) {
                var selection = _ctx.getSelection();
                for (var i = 0; i < selection.length; i++) {
                    out.push(selection[i]);

                }
            }
            return out;
        },
        ensureSceneFiles: function (item) {
            var dfd = new Deferred();
            var chain = [];
            var store = item._S;
            var mount = item.mount;
            var fileExtension = utils.getFileExtension(item.path);
            var fileManager = this.ctx.getFileManager();

            var templateDirectoryItem = this.getTemplateDirectoryPathItem();

            var parentDirectoryItem = store.getParent(item);
            var parentDirectoryItemPath = parentDirectoryItem.getPath();

            var ensureItem = function (item, extension) {

                var itemDfd = new Deferred();

                var itemPath = item.getPath().replace('.' + fileExtension, '.' + extension);

                var item = store.getSync(itemPath);
                if (item) {
                    itemDfd.resolve(item);
                } else {

                    var newItemPath = './' + itemPath;

                    newItemPath = newItemPath.replace('././', './');
                    var newItemTemplatePath = templateDirectoryItem.path + '/newDHTML.' + extension;

                    //create the file
                    fileManager.mkfile(mount, newItemPath).then(function (result) {

                        //copy from template content over
                        fileManager.getContent(mount, newItemTemplatePath, function (content) {

                             0 && console.log('write from template to: ' + newItemPath);
                            fileManager.setContent(mount, newItemPath, content, function (content) {
                                itemDfd.resolve();
                            });
                        });
                    });
                }
                return itemDfd;
            };


            chain.push(ensureItem(item, 'css'));
            //chain.push(ensureItem(item,'less'));
            chain.push(ensureItem(item, 'xblox'));
            chain.push(ensureItem(item, 'js'));
            all(chain).then(function (results) {
                dfd.resolve();
            });
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Widget Instances
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Bean protocol implementation
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getActiveItem: function () {
            return null;
        },
        hasItemActions: function () {
            return this.ready;
        },
        getItem: function () {
            return this.currentItem;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        /////////////////////////////////////////////////////////////////////////////////////
        onReloaded: function () {},
        _createLeftContainer: function () {
            var main = this.getMainView();
            var pane = utils.addWidget(ContentPane, {
                closable: true,
                style: 'padding:0px;width:200px',
                region: 'left',
                cssClass: 'layoutLeft',
                splitter: true,
                parentContainer: main.layoutMain
            }, this, main.layoutMain, true, 'ui-widget-content');

            return pane;

        },
        getLeftContainer: function () {
            var main = this.getMainView();
            var container = main.leftLayoutContainer;
            if (container) {
                return container;
            }
            if (main.leftTabContainer) {
                return main.leftTabContainer;
            }

            this._tempLeftContainer = this._createLeftContainer();

            this.leftLayoutContainer = utils.addWidget(AccordionContainer, {
                persist: true,
                doLayout: true,
                cssClass: 'ui-widget-content'
            }, this, this._tempLeftContainer.containerNode, true);

            return this.leftLayoutContainer;

        },
        /**
         * This editor got active/visisble again, restore our panels in the main-view
         * @returns {*}
         */
        restoreStyleView: function () {

            return registry.byId('html_style_view_parent');
            if (!this.styleViewContainer) {
                return;
            }

            var styleView = registry.byId('html_style_view_parent');
            if (styleView) {
                var main = this.getMainView();
                /*if (styleView.isHidden) {*/

                var pe = this.getPageEditor();

                this.rightLayoutContainer = pe.getPropertyPane();
                utils.addChild(styleView, this.rightLayoutContainer);
                //this.rightLayoutContainer.addChild(styleView);


                styleView.isHidden = false;

                /*}*/
                return styleView;
            }
            return null;
        },
        /**
         * When user selects another bean, hide our panels instead of destroying them
         * @returns {*}
         */
        suspendStyleView: function () {
            return;
            if (!this.styleViewContainer) {
                return;
            }
            if (!this.styleViewContainer.isHidden) {
                try {
                    this.rightLayoutContainer.removeChild(this.styleViewContainer);
                } catch (e) {
                    console.error('error suspend style view  ' + e, e);
                }
                var widgetCache = dojo.byId('widgetCache');
                dojo.place(this.styleViewContainer.domNode, widgetCache);
                this.styleViewContainer.isHidden = true;
            }
        },
        /////////////////////////////////////////////////
        //
        //  Lifecycle & cleanup methods
        //
        onLastEditorRemoved: function () {
            // 0 && console.log('onLastEditorRemoved');
            this.suspendStyleView();

            //return;
            var stylePaneTC = registry.byId('html_style_view_parent');
            if (stylePaneTC && stylePaneTC.parentContainer) {
                stylePaneTC.parentContainer.removeChild(stylePaneTC);
                utils.destroy(stylePaneTC);
            }


            var stylePane = registry.byId('html_style_view');
            if (stylePane) {
                utils.destroy(stylePane);
            }

            var tc2 = registry.byId('palette-tabcontainer-right-top');
            if (tc2) {
                utils.destroy(tc2);
            }


        },
        clearAll: function () {
            //deselect all, otherwise focus-frame remains active
            if (this.editor) {
                this.editor.visualEditor.context.deselect();
            }
            Workbench._switchEditor(null);
            Workbench.currentEditor = null;
            Runtime.currentEditor = null;
            this.destroyed = true;
            if (this.blockScopes) {
                for (var i = 0; i < this.blockScopes.length; i++) {
                    var scope = this.blockScopes[i];
                    this.ctx.getBlockManager().removeScope(scope.id);
                }
                delete this.blockScopes;
            }
            this.outlineGridContainer = null;
            this.outlineGridView = null;
            this.onLastEditorRemoved();
        },
        getFrame: function () {
            var ctx = this.getEditorContext();
            if (ctx && ctx.frameNode) {
                return ctx.frameNode;
            }
            return null;
        },
        destroy: function () {

            var otherEditors = this.getEditors(),
                isLast = otherEditors.length == 0;

            var main = this.getMainView(),
                left = this.getLeftContainer(),
                ctx = this.getEditorContext();
            clearInterval(this._frameTimer);
            utils.destroy(this.getFrame());

            var isEmbedded = this.leftLayoutContainer == null || (left.canDestroy && !left.canDestroy());
            if (!isEmbedded) {
                utils.destroy(this.leftLayoutContainer, false);
                main.layoutMain && main.layoutMain.removeChild && main.layoutMain.removeChild(this._tempLeftContainer);
                utils.destroy(this.paletteContainer, false);
            } else {
                utils.destroy(this.paletteContainer, false);
                //utils.destroy(this.outlineContainer,false);
                utils.destroy(this._cBPalettePane, false);
                //left.removeChild(this.outlineContainer);
                //this.outlineView.parentContainer = null;
                //utils.destroy(this.outlineView,true);
                try {
                    if (this.outlineGridContainer) {
                        left.removeChild(this.outlineGridContainer);
                        this.outlineGridView.parentContainer = null;
                    }
                    //this.outlineGridView.reset();
                    utils.destroy(this.outlineGridView, true);
                    //this.outlineView.destroy();
                    utils.destroy(this.palette);
                } catch (e) {
                    debugger;
                }
            }

            if (this && this.styleViewContainer) {
                this.styleViewContainer.keep = false;
            }

            utils.destroy(this.styleView, true, this);
            utils.destroy(this.tabContainer, true, this);
            utils.destroy(this.styleViewContainer, true, this);

            this.publish(types.EVENTS.ON_CONTEXT_DESTROYED, this.getEditorContext());
            //clear property panel for true
            //main.getRightTopTarget(true, false);

            /*
             var sview = registry.byId('html_style_view');
             if(sview){
             console.error('sview still alive!');
             }
             */

            /*
             try {
             //utils.destroy(this.styleViewContainer);
             //utils.destroy(this.styleView,true,this);
             }catch(e){
             debugger;
             }
             */

            try {
                this.clearAll();
            } catch (e) {
                logError(e);
            }
            if (isLast) {
                try {
                    utils.destroy(this.editor, true, this);
                } catch (e) {
                    logError(e);
                }
                try {
                    /*utils.destroy(this.leftLayoutContainer,true,this);
                     main.layoutMain.removeChild(this._tempLeftContainer);*/
                } catch (e) {
                    debugger;
                }
            }


            /*

             if (this.palette) {
             utils.destroy(this.palette);
             }

             if (this.leftLayoutContainer) {

             this.leftLayoutContainer.removeChild(this.paletteContainer);
             if (this.outlineView) {
             utils.destroy(this.outlineView);
             this.leftLayoutContainer.removeChild(this.outlineContainer);
             }
             }



             try {
             //remove the accordion
             if (this._tempLeftContainer) {
             utils.empty(this.leftLayoutContainer);
             main.layoutMain.removeChild(this._tempLeftContainer);
             }
             } catch (e) {
             console.error('fucked');
             }


             this.suspendStyleView();

             utils.destroy(this.editor, true, this);

             this._tempLeftContainer = null;
             this.outlineView = null;
             this.outlineContainer = null;
             this.palette = null;
             this.paletteContainer = null;
             */
            /*
             this.styleView =null;
             this.styleViewContainer = null;
             */
            //utils.destroy(this.getEditorContext().frameNode);
            //this.inherited(arguments);
        },
        /////////////////////////////////////////////////
        //
        //  Sub-Widget creation
        //
        getMainView: function () {
            return this.mainView;
        },
        /**
         *
         * @param context
         */
        addStyleView: function (context) {
            var self = this;
            var mainView = this.ctx.getApplication().mainView;
            var pe = this.getPageEditor();
            var propPane = pe.getPropertyPane();

            //#xo propPane._open();
            if (this.styleView) {
                return this.styleView;
            }

            function wirePane(pane) {
                pane._on('splitterMoveStart', function (e) {
                    self.hideFrame(true);
                });
                pane._on('splitterMoveEnd', function (e) {
                    self.hideFrame(false);
                });
            }

            wirePane(propPane);
            wirePane(pe.getDesignPane());

            var tabContainer = utils.addWidget(_TabContainer, {
                region: 'center',
                id: 'palette-tabcontainer-right-top',
                tabPosition: 'right' + '-h',
                tabStrip: false,
                'class': "davinciPalette davinciTopPalette widget",
                style: '',
                splitter: false,
                direction: 'right'
            }, null, propPane.containerNode, true);

            domClass.add(tabContainer.domNode, "davinciPalette davinciTopPalette");

            this.tabContainer = tabContainer;
            this.styleViewContainer = propPane;
            var _sTab = tabContainer.createTab('Widget', 'fa-cogs');
            try {
                this.styleView = utils.addWidget(SwitchingStyleView, {
                    toolbarDiv: propPane.containerNode,
                    context: context,
                    _editor: this.editor,
                    parentContainer: tabContainer,
                    tabContainer: tabContainer
                }, null, _sTab.containerNode, true);

            } catch (e) {
                console.error('html_style_view crash ' + e);

            }
            this.styleView._initialPerspectiveReady();
        },
        /**
         *
         * @param context
         */
        addPalette: function (context) {
            this.palette = registry.byId('html_palette');

            if (this.palette) {
                return;
            }
            var ctx = this.ctx;
            var leftContainer = ctx.mainView.leftLayoutContainer;
            var pane = leftContainer.createTab('Palette', 'fa-cube', true, null, {
                parentContainer: leftContainer,
                open: false
            });

            this.paletteContainer = pane;
            try {
                this.palette = utils.addWidget(Palette, {
                    toolbarDiv: pane.containerNode,
                    context: context,
                    //id: 'html_palette',
                    style: 'padding:0px;'
                }, null, pane.containerNode, true);
            } catch (e) {
                console.error('palette crash ' + e);
            }
            try {
                this.palette.descriptors = "dijit,dojox,html,OpenAjax"; // FIXME: parameterize this in plugin data?
                this.palette.setContext(context);
            } catch (e) {
                console.error('palette crash ' + e);
            }

            this.add(pane, null, false);
            this.add(this.palette, null, false);

        },
        addOutlineGridView: function (context) {

            var leftContainer = this.getLeftContainer();
            if (this.outlineGridView || !leftContainer) {
                return;
            }
            var pane = leftContainer.createTab('Outline', 'fa-cube', true, null, {
                parentContainer: leftContainer
            });
            this.outlineGridContainer = pane;
            var self = this;
            try {
                this.outlineGridView = utils.addWidget(OutlineGridView, {
                    context: context,
                    resizeToParent: true,
                    style: 'padding:0px;'
                }, null, pane.containerNode, true);
                pane.add(this.outlineGridView);

                setTimeout(function () {
                    pane.resize();
                    self.outlineGridView && self.outlineGridView.resize();
                }, 1000)

            } catch (e) {
                logError(e, 'outline crash ')
            }
            this.outlineGridView.startup();
            this.add(pane);
        },
        /**
         * Adds a outline view for HTML nodes
         * @param context
         */
        addOutlineView: function (context) {
            this.outlineView = registry.byId('html_outline_view');
            var mainView = this.ctx.getApplication().mainView;
            var leftContainer = mainView.getLeftLayoutBottomContainer() || this.getLeftContainer();

            if (this.outlineView || !leftContainer) {
                return;
            }

            var pane = utils.addWidget(ContentPane, {
                title: 'Outline',
                closable: true,
                iconClass: 'fa-cube',
                parentContainer: leftContainer,
                style: 'padding:0px;'
            }, this, leftContainer, true);

            this.outlineContainer = pane;
            try {
                this.outlineView = new OutlineView({
                    context: context,
                    id: 'html_outline_view',
                    style: 'padding:0px;'
                });
            } catch (e) {
                console.error('palette crash ' + e);
            }
            pane.containerNode.appendChild(this.outlineView.domNode);
            this.outlineView.startup();
        },
        /**
         *
         * @param e
         * @param isGlobal
         * @private
         */
        _handleKeyDown: function (e, isGlobal) {
            var handled = this._handleKey(e, isGlobal);
            // now pass to runtime if unhandled so global key listeners can take a stab
            // make sure to not pass global events back up
            if (!handled && !isGlobal) {
                Runtime.handleKeyEvent(e);
            }
        },
        /**
         *
         * @param e
         * @param isGlobal
         * @returns {*}
         * @private
         */
        _handleKey: function (e, isGlobal) {
            //catch control key
            /*
             if (e.keyCode == 17) {
             this.setDesignMode(e.type === 'keyup', true);
             return;
             }*/

            if (!this.keyBindings) {
                return false;
            }

            var stopEvent = this.keyBindings.some(function (globalBinding) {
                if (isGlobal && !globalBinding.keyBinding.allowGlobal) {
                    return;
                }

                if (Runtime.isKeyEqualToEvent(globalBinding.keyBinding, e)) {
                    var Workbench = require("davinci/Workbench");
                    Workbench._runAction(globalBinding.action, this.editor, globalBinding.action.id);
                    return true;
                }
            }, this);

            if (stopEvent) {
                dojo.stopEvent(e);
            }

            return stopEvent;
        },
        /**
         *
         * @param editor
         * @private
         */
        _setupKeyboardHandler: function (editor) {

            var pushBinding = function (o) {
                if (!this.keyBindings) {
                    this.keyBindings = [];
                }
                this.keyBindings.push(o);
            }.bind(this);


            this._getViewActions().forEach(function (actionSet) {
                actionSet.actions.forEach(function (action) {
                    if (action.keyBinding) {
                        pushBinding({
                            keyBinding: action.keyBinding,
                            action: action
                        });
                    }
                    if (action.menu) {
                        action.menu.forEach(function (menuItemObj) {
                            if (menuItemObj.keyBinding) {
                                pushBinding({
                                    keyBinding: menuItemObj.keyBinding,
                                    action: menuItemObj
                                });
                            }
                        }, this);
                    }
                }, this);
            }, this);

            connect.connect(editor, "handleKeyEvent", this, "_handleKeyDown");
        },
        /////////////////////////////////////////////////
        //
        //  Public main entries
        //
        /**
         * Enable/disable various items on the editor toolbar and action bar
         */
        setDirty: function () {

        },
        shouldUpdateTextEditor: function () {
            if (this._skipTextEditorChange) {
                this._skipTextEditorChange = true;
                return false
            }
            //var is = this.splitMode ==  types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL || this.splitMode == types.VIEW_SPLIT_MODE.SPLIT_VERTICAL;
            return true;
        },
        addPageEditor: function (item) {
            if (this.editor) {
                return;
            }
            try {
                var thiz = this;

                var _createTextEditor = function (dstNode, fileName) {
                    return thiz.createHTMLEditor(dstNode, fileName, item);
                };
                var _additionalEditors = function () {

                    var blockEditor = {
                        title: 'Blocks'
                    };

                    return [blockEditor];

                };

                //this.editor = new PageEditor(dojo.create('div', {}), item.getPath(), item, _createTextEditor, this.template,_additionalEditors());
                var _PE = require('davinci/ve/PageEditor');
                this.editor = new _PE(dojo.create('div', {}), item.getPath(), item, _createTextEditor, this.template, _additionalEditors());
                this.editor.ctx = this.ctx;

                this.editor.editorContainer = this;
                this.editor.delegate = this;
                this.containerNode.appendChild(this.editor.domNode);
                this.declaredClass = this.editor.declaredClass; //important
                this.editor.editorID = 'davinci.ve.HTMLPageEditor';

                Runtime.currentEditor = this.editor;
                Workbench.currentEditor = this.editor;


                this.add(this.editor, null, false);
                this._setupKeyboardHandler(this.editor);
                this.addTextEditor(this.editor.htmlEditor);
                Workbench._switchEditor(this.editor);
                setTimeout(function () {
                    dojo.publish("/davinci/ui/editorSelected", [{
                        editor: thiz.editor,
                        oldEditor: null
                    }]);
                }, 2000);

                return this.editor;

            } catch (e) {
                console.error('crash in visual editor ' + e);
                logError(e, 'crash in visual editor ');
            }


        },
        ///////////////////////////////////////////////////////////////////
        //
        //  UI-Callbacks
        //
        _widgetSelectionChanged: function (args) {

            if (args.isFake == true) {
                return;
            }
            var selection = args.selection;
            var selected = selection.length == 1 ? selection[0] : null;
            var item = selected || {
                dummy: true
            };

            this.currentItem = selected || null;

            var self = this;

            this.publish(types.EVENTS.ON_ITEM_SELECTED, {
                item: item,
                owner: this,
                beanType: 'widget'
            });
            this._emit('selectionChanged', {
                item: item,
                owner: this
            })
        },
        _resume: function () {
            this.restoreStyleView();
        },
        _suspend: function () {
            this.suspendStyleView();
        },
        hideFrame: function (hide) {
            this._hidden = hide;
            var _frame = this.getFrame();
            if (_frame) {
                $(_frame).css('display', (hide ? 'none' : 'block'));
            }
        },
        onHide: function () {
            this.inherited(arguments);
            this.hideFrame(true);
            this.updateFocus();
            if (this.editor) {
                Workbench._switchEditor(null);
                this.publish("/davinci/ui/editorSelected", [{
                    editor: null,
                    oldEditor: this.editor
                }]);
            }
            this._suspend();
        },
        onShow: function () {
            this.inherited(arguments);
            this.hideFrame(false);
            if (this.editor) {
                this.updateFocus();
                Workbench._switchEditor(this.editor);
                var thiz = this;
                setTimeout(function () {
                    var z = this;
                    thiz.publish("/davinci/ui/editorSelected", [{
                        editor: thiz.editor,
                        oldEditor: null
                    }]);
                }, 1000);
            }
            this._resume();
        },
        ///////////////////////////////////////////////////////////////////
        //
        //  Main entry
        //
        openItem: function (item) {
            var thiz = this,
                dfd = new Deferred();

            thiz.item = item;

            function load(item) {
                thiz.ensureSceneFiles(item).then(function () {
                    if (!thiz.editor) {
                        thiz.addPageEditor(item);
                    }
                    thiz.hideFrame(false);
                    thiz.updateFrame();
                    var onReady = function (content) {
                        thiz.editor.setContent(item.path, content);
                        //var _ctx = thiz.editor.visualEditor.context;
                        thiz.showPalette && thiz.addPalette(thiz.editor.visualEditor.context);
                        try {
                            if (!thiz.styleView && thiz.showStyleView !== false) {
                                thiz.addStyleView(thiz.getEditorContext());
                            }
                        } catch (e) {
                            console.error('error style view ' + e);
                            logError(e, 'error style view ');
                        }

                        dfd.resolve();
                    };

                    thiz.ctx.getFileManager().getContent(item.mount, item.path, onReady);
                });
            }

            if (this.template && !this.template.isLoaded()) {

                this.template.load().then(function () {
                    load(item);
                });

            } else {
                load(item);
            }

            return dfd;
        },
        onReady: function () {
            this.ready = true;
            this.hideFrame(false);
            this.updateFrame();
        },
        onEditorContextLoaded: function () {
            var thiz = this;
            if (this._didInitial) {
                setTimeout(function () {
                    if (thiz.outlineGridView) {
                        thiz.outlineGridView.editorChanged({
                            editor: null,
                            oldEditor: thiz.editor
                        });
                        thiz.outlineGridView.editorChanged({
                            editor: thiz.editor,
                            oldEditor: null
                        })
                    }
                }, 800);
                return;
            }
            this._didInitial = true;
            var _ctx = thiz.editor.visualEditor.context;
            if (thiz.addOutline) {
                try {
                    setTimeout(function () {
                        thiz.addOutlineGridView(_ctx);
                        //init context & other views by changing the current editor
                        dojo.publish("/davinci/ui/editorSelected", [{
                            editor: thiz.editor,
                            oldEditor: null
                        }]);
                        thiz.onReady();
                    }, 800);
                } catch (e) {
                    console.error('outline creation crashed ' + e);
                    debugger;
                }
            }
        },
        getNullItem: function () {
            if (this._nullItem) {
                return this._nullItem;
            }
            this._nullItem = {
                owner: this,
                beanType: 'widget'
            };
            return this._nullItem;
        },
        _editors: null,
        addTextEditor: function (editor) {
            !this._editors && (this._editors = []);

            this._editors.push(editor);

        },
        publishCSSRules: function (content) {
            var parser = new CSSParser();
            var sheet = parser.parse(content, false, true);
            var names = [];
            _.each(sheet.cssRules, function (rule) {
                if (rule.mSelectorText && rule.mSelectorText !== 'body') {
                    names.push(rule.mSelectorText.replace('.', ''))
                }
            });

            this.publish('CSSRulesUpdated', {
                names: names,
                editor: this
            })
        },
        loadSceneCSSFile: function (file) {
            var thiz = this;
            this.ctx.getFileManager().getContent(file.mount, file.path, function (content) {
                thiz.publishCSSRules(content);
            });
        },
        /**
         * Event when an editor context and its containing application has been fully
         * loaded. This is used to extend the left palette system for 'shared blocks'
         *
         * @param evt {Object} event data
         * @param evt.context {davinci/ve/Context} the davinci context
         * @param evt.appContext {xapp/manager/Context} the xapp context
         * @param evt.settings {Object} the application's settings
         * @param evt.ctx {xide/manager/Context} the root context (IDE context)
         * @param evt.global {Object} the loaded document's global
         * @param evt.blockScopes {xblox/model/Scope[]} the loaded xblox scopes
         *
         */
        onContextReady: function (evt) {

            //filter
            if (evt.context != this.getEditorContext()) {
                return;
            }

            this.contextInfo = evt;
            var thiz = this;
            var pe = this.getPageEditor();
            var sheets = evt.context._extraSheets;
            var peTabs = pe.bottomTabContainer;
            var store = this.item._S;
            this.ctx.getWindowManager().registerView(this, true);
            if (this.showLESS !== false && store && !this.didAddSheetEditors) {
                this.didAddSheetEditors = true;
                _.each(sheets, function (props, path) {
                    var sheetPath = path; //path.replace('.css','.less');
                    sheetPath = sheetPath.replace('././', './');
                    path = path.replace('././', './');
                    var fileItem = store.getSync(sheetPath);
                    debugCSSEditor &&  0 && console.log('add sheet editor ' + sheetPath, fileItem);
                    if (fileItem) {
                        props.storeItem = fileItem;
                        props.cssItem = store.getSync(path);
                    } else {
                        debugCSSEditor && console.error('couldnt find file : ', sheetPath);
                    }

                    if (fileItem) {
                        var editorArgs = {
                            sheetProps: props,
                            closeable: false,
                            emmet: true,
                            resizeToParent: true
                        };
                        var DOCKER = types.DOCKER;
                        var editorPane = pe.createTab(null, {
                            title: fileItem.name,
                            icon: 'fa-paint',
                            tabOrientation: DOCKER.TAB.TOP,
                            location: DOCKER.DOCK.STACKED,
                            target: pe.getSourcePane()
                        });

                        thiz.cssPane = editorPane;

                        var editor = Default.Implementation.open(fileItem, editorPane, editorArgs, true);
                        props.edior = editor;
                        thiz.addTextEditor(editor);
                        editorPane.add(editor);
                        thiz.loadSceneCSSFile(fileItem);
                        editor._on(types.EVENTS.ON_FILE_CONTENT_CHANGED, function (evt) {
                            thiz.publishCSSRules(evt.content);
                        });
                    } else {
                        console.error('couldnt find file : ', sheetPath);
                    }
                });

            }
            this.hideFrame(false);
            var scopes = evt.blockScopes;
            //nothing to do ?
            if (!scopes || !scopes.length) {
                return;
            }
            this.showBehaviours && this._buildBlockPalette(evt);
            evt.context.eventHandler = {
                'click': function (e) {
                    thiz.publish(types.EVENTS.ON_ITEM_SELECTED, {
                        item: thiz.currentItem || thiz.getNullItem(),
                        owner: thiz,
                        beanType: 'widget'
                    });
                    thiz._emit(types.EVENTS.ON_VIEW_SHOW, {
                        view: thiz
                    });
                }
            };
        },
        onAppReady: function (evt) {
            setTimeout(function () {
                if (this.outlineView && evt.context) {
                    this.outlineView.reset();
                    this.outlineView.createTree();
                }
            }.bind(this), 2000);
        },
        getTextEditor: function () {
            return this.editor.htmlEditor;
        },
        getEditor: function (_mount, _path) {
            var editor = null;
            _.each(this._editors, function (_editor) {
                var item = _editor.item;
                var mount = item.mount.replace('/', '');
                var path = item.path.replace('./', '');
                if (mount === _mount && path === _path) {
                    editor = _editor;
                }
            });

            return editor;
        },
        /**
         * triggered by intern editor
         * @param evt
         * @returns {*}
         */
        onEditorContentChanged: function (evt) {
            var self = this;
            this.__internUpdate = true;
            setTimeout(function () {
                self.__internUpdate = false;
            }, 5000);
            return this.reload(evt.content);
        },
        isSaving: false,
        save: function (pageEditor, visualEditor, model, text) {

            if (this.isSaving) {
                return;
            }


            this.isSaving = true;
            this.__internUpdate = true;
            var textEditor = this.getTextEditor();
            if (textEditor) {
                textEditor.set('value', text);
            }

            //tell everybody
            this.publish(types.EVENTS.ON_VISUAL_EDITOR_SAVE, {
                pageEditor: pageEditor,
                visualEditor: visualEditor,
                model: model,
                text: text,
                item: this.item
            });

            var thiz = this;
            //this will also publish ON_FILE_CHANGED
            this.ctx.getFileManager().setContent(this.item.mount, this.item.path, text, function () {
                //thiz.__internUpdate = false;
                thiz.isSaving = false;
            });
        },
        fileChanged: function (evt) {
            if (this.__internUpdate) {
                return;
            }
            var path = evt.data.path;
            var self = this;
            var _start = 'workspace_user';

            function reload(editor) {
                if (editor == self.getTextEditor()) {
                     0 && console.log('reload = same text editor');
                    editor.reload().then(function (content) {
                        self.reload(content);
                    });
                }

                 0 && console.log('reload');
                editor.reload().then(function () {
                    self.onFileChanged({
                        editor: editor
                    });
                });

            }

            if (path.indexOf(_start) != -1) {
                var libPath = path.substr(path.indexOf(_start) + (_start.length + 1), path.length);
                var editor = this.getEditor(_start, libPath);
                if (editor) {
                    return reload(editor);
                }
            }
            var vfsPath = this.ctx.toVFSShort(path, 'workspace_user');
            if (vfsPath) {
                var editor = this.getEditor('workspace_user', vfsPath);
                if (editor) {
                    reload(editor);
                }
            }

        },
        onFileChanged: function (evt) {
            if (evt.__did) {
                return;
            }
            evt.__did = true;
            // 0 && console.log('intern: on file changed',evt);
            //scene css files ?
            if (evt && evt.editor && evt.editor.sheetProps) {

                var sheetProps = evt.editor.sheetProps;
                var doc = this.contextInfo.document;
                var thiz = this;
                dojo.withDoc(doc, function () {
                    //remove
                    var head = doc.getElementsByTagName('head')[0];
                    try {
                        // 0 && console.log('did destroy css');
                        if (sheetProps.link) {
                            head.removeChild(sheetProps.link);
                            sheetProps.link = null;
                        }
                    } catch (e) {
                        dojo.destroy(sheetProps.link);
                    }

                    var newUrl = thiz.ctx.getFileManager().getImageUrl(sheetProps.cssItem, true, false);
                    // 0 && console.log('do re-add css ' + newUrl);
                    //re-add
                    var newLink = domConstruct.create('link', {
                        rel: 'stylesheet',
                        type: 'text/css',
                        href: newUrl
                    });
                    head.appendChild(newLink);
                    dojo.place(newLink, head, 'last');
                    sheetProps.link = newLink;
                });
            }
        },
        onUseActionStore: function (store) {
            return;
            var actions = store.query();
            _.each(actions, function (action) {
                action.addPermission = true;
                !action.tab && (action.tab = 'Home');
            });
        },

        getBlockEditor: function () {
            var pe = this.getPageEditor();
            return pe.getBlockEditor();
        },
        resize: function () {
            this.updateFrame();
            this.updateFocus();
            var pe = this.getPageEditor();
            var ctx = this.getEditorContext();
            if (pe) {
                utils.resizeTo(pe.domNode, this.containerNode, true, true, null, {});
                pe.resize();
            }
        },
        startup: function () {
            var self = this,
                EVENTS = types.EVENTS;
            this._frameTimer = setInterval(() => {
                var ctx = this.getEditorContext();
                if (ctx) {
                    if (ctx.isRezing) {
                        return;
                    }
                }
                this.updateFrame();
            }, 1000);
            //this.addActions(this.getItemActions());
            //this.initReload();
            //warm up meta library for wizards
            Metadata.getSmartInput('xblox/RunScript');
            this.subscribe([
                EVENTS.ON_BUILD_BLOCK_INFO_LIST_END,
                //EVENTS.ON_REMOVE_CONTAINER,
                EVENTS.ON_ACTION_CONTEXT_CHANGED,
                EVENTS.ON_ACTION_CHANGE_CONTEXT,
                EVENTS.ON_ACTION_CHANGE_CONTEXT,
                EVENTS.ON_FILE_CHANGED,
                EVENTS.ON_CONTEXT_READY, //track it
                EVENTS.ON_APP_READY, //reloads, text-editor changes
                {
                    //update editor content on file changes
                    key: types.EVENTS.ON_FILE_CONTENT_CHANGED,
                    handler: function (evt) {
                        var item = evt.item;
                        //@TODO: wtf
                        self._skipTextEditorChange = true;
                        if (item == self.item && evt.callee == self.textEditor) {
                            self.editor.setContent(self.item.getPath(), evt.content);
                        } else {
                            self.onFileChanged(evt);
                        }
                    }
                },
                {
                    key: '/davinci/ui/widgetSelected',
                    handler: self._widgetSelectionChanged
                },
                {
                    //update editor content on file changes
                    key: '/davinci/ui/context/loaded',
                    handler: self.onEditorContextLoaded
                }
            ]);
            this.initReload();
            setTimeout(function () {
                self.resize();
            }, 2000);

        },
        /**
         *
         * @param designMode {boolean}
         * @param updateWidget {boolean}
         */
        setDesignMode: function (designMode, updateWidget) {
            this._designMode = designMode;
            if (updateWidget === true) {
                var action = this._designToggleAction;
                if (action) {
                    var widget = action.getVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR).widget;
                    if (widget) {
                        widget.set('checked', designMode);
                    }
                }
            }
            var context = this.getEditorContext();
            if (context) {
                context.setDesignMode(designMode);
            }
        },
        /**
         *
         * @param evt
         */
        _didHide: false,
        getSettings: function () {
            var settingsManager = this.ctx.getSettingsManager();

            var settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };
            var props = settingsStore.getSync('ve') || {
                value: {
                    showEditors: false,
                    selected: 'HTML'
                }
            };
            return props.value;
        },
        saveSettings: function () {
            var settingsManager = this.ctx.getSettingsManager();
            var settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };

            var props = settingsStore.getSync('ve') || {
                value: this.getSettings()
            };

            var value = props.value;
            value.showEditors = this.showCode;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            var selected = 'HTML';
            _.each(docker.getPanels(), (p) => {
                if (p._title === 'Blocks' && p.selected === true) {
                    selected = 'Blocks';
                }
                if (p._title.indexOf && p._title.indexOf('.css') !== -1 && p.selected === true) {
                    selected = p._title;
                }
            });
            value.selected = selected;
            settingsManager.write2(null, '.', {
                id: 've'
            }, {
                value: value
            }, true, null).then(function () {

            });
        },
        restoreSettings: function () {
            var settings = this.getSettings()
            if (!settings.showEditors) {
                setTimeout(() => {
                    if (this._didHide) {
                        var pe = this.getPageEditor();
                        var docker = pe.getDocker();
                        return;
                    }
                    this._didHide = true;
                    this.toogleCode();
                }, 1500);
            }
            var selected = settings.selected;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            var panel = _.find(docker.getPanels(), {
                _title: selected
            });
            if (panel) {
                panel.select();
            }
        },
        didinitSettings: false,
        initSettings: function () {
            if (this.didinitSettings) {
                return;
            }
            this.didinitSettings = true;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            docker._on(types.DOCKER.EVENT.SELECT, (panel) => {
                var selected = panel.title();
                this.getSettings().selected = selected;
                this.saveSettings();
            });
        },
        onSceneBlocksLoaded: function (evt) {
            this.blockScopes = evt.blockScopes;
            debugBlocks &&  0 && console.log('onSceneBlocksLoaded : ', evt);
            var pe = this.getPageEditor();
            window['vee'] = this;
            pe.onSceneBlocksLoaded(evt, this);
            this.initSettings();
            setTimeout(() => {
                this.restoreSettings();
            }, 1500);

        }
    });

    OutlineGridView.prototype.reloadModule = Module.prototype.reloadModule;
    OutlineView.prototype.reloadModule = Module.prototype.reloadModule;
    PageEditor.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorAction.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorPalette.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorSourceEditor.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorLayout.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorAction.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorTools.prototype.reloadModule = Module.prototype.reloadModule;

    Module.getFileActions = function (ctx, item) {
        return [
            ctx.createAction({
                label: 'Preview in Browser',
                command: 'File/Preview',
                tab: 'Home',
                icon: 'fa-eye',
                tab: 'Home',
                mixin: {
                    quick: true,
                    addPermission: true,
                    handler: function () {
                        window.open(ctx.getContextManager().getViewUrl(item));
                    }
                },
                custom: true
            })
        ]
    }
    return Module;
});