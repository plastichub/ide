define("xideve/views/OutlineGridView", [
    'dcl/dcl',
    "dojo/dom-class",
    'xide/utils',
    'xide/types',
    "dgrid/Editor",
    'xide/data/TreeMemory',
    "davinci/ve/widget",
    "xide/_base/_Widget",
    "xide/mixins/ReloadMixin",
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    "davinci/ve/States",
    "davinci/ve/commands/StyleCommand",
    'xideve/views/VisualEditorAction'
], function (dcl, domClass,
    utils, types, Editor, TreeMemory,
    Widget, _Widget, ReloadMixin, Grid,
    TreeRenderer, KeyboardNavigation, _Widget2, States, StyleCommand, VisualEditorAction) {
    var debug = false;
    var ACTION = types.ACTION;
    var gridClass = Grid.createGridClass('OutlineGridView', {
            permissions: [
                ACTION.RENAME,
                ACTION.UNDO,
                ACTION.REDO,
                ACTION.RELOAD,
                ACTION.DELETE,
                ACTION.CLIPBOARD,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.SAVE,
                ACTION.SEARCH,
                ACTION.TOOLBAR,
                'File/Properties',
                'File/Edit',
                ACTION.GO_UP,
                ACTION.HEADER,
                ACTION.EDIT,
                ACTION.COPY,
                ACTION.CLOSE,
                ACTION.MOVE,
                ACTION.RENAME,
                ACTION.RELOAD,
                ACTION.CLIPBOARD,
                ACTION.LAYOUT,
                ACTION.SELECTION,
                ACTION.SEARCH,
                ACTION.TOOLBAR,
                ACTION.STATUSBAR,
                ACTION.SOURCE,
                ACTION.CLIPBOARD_COPY,
                ACTION.CLIPBOARD_PASTE,
                ACTION.CLIPBOARD_CUT
            ],
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Widget': 80,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            getEditorContext: function () {
                return this.context;
            },
            startup: function () {                
                this.inherited(arguments);
                this._createContextMenu();
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            //CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            WIDGET: {
                CLASS: _Widget2
            },
            EDITOR: {
                CLASS: Editor
            },
            EXTRAS: {
                CLASS: VisualEditorAction.GridClass
            }
        }, {
            RENDERER: TreeRenderer
        }, null,
        null);


    return dcl([_Widget, ReloadMixin.dcl], {
        templateString: '<div attachTo="domNode"></div>',
        declaredClass: 'xideve.views.OutlineGridView',
        rootWidget: null,
        _lastEvent: null,
        useRichTextLabel: false,
        resizeToParent: true,
        onItemSelected: function (item, node) {
            debug && console.log('-widget selected', item);
            var context = this.getContext();
            var store = this.getStore();
            if (!context) {
                return;
            }
            var newSelection = store.model._getWidget(item);
            var oldSelection = context.getSelection();

            // deselect any olds not in new
            dojo.forEach(oldSelection, dojo.hitch(this, function (item) {
                /*if (newSelection.indexOf(item) == -1) {*/
                context.deselect(item);
                /*}*/
            }));
            context.select(newSelection, true);
        },
        _toggle: function (widget, on, node) {
            var visible = !on;
            var value = visible ? "" : "none";
            var state;
            var statesFocus = States.getFocus(widget.domNode.ownerDocument.body);
            if (statesFocus) {
                state = statesFocus.state;
            } else {
                var currentStatesList = States.getStatesListCurrent(widget.domNode);
                for (var i = 0; i < currentStatesList.length; i++) {
                    if (currentStatesList[i]) {
                        state = currentStatesList[i];
                        break;
                    }
                }
            }
            var command = new StyleCommand(widget, [{
                display: value
            }], state);
            var context = this.getContext();
            context.getCommandStack().execute(command);
        },
        getLabel: function (item) {
            if (item.id == "myapp") {
                return this.getContext().model.fileName;
            }

            var label = '';
            var widget = this._getWidget(item);
            /*
                 /\{([^}]+)\}/
    
                 /        - delimiter
                 \{       - opening literal brace escaped because it is a special character used for quantifiers eg {2,3}
                 (        - start capturing
                 [^}]     - character class consisting of
                 ^    - not
                 }    - a closing brace (no escaping necessary because special characters in a character class are different)
                 +        - one or more of the character class
                 )        - end capturing
                 \}       - the closing literal brace
                 /
                 */

            if (item.type === 'xblox/RunScript' && widget) {
                var _w = widget.domNode;

                //event to target property
                if (_w.mode == 1 && _w.targetproperty) {

                    if (_w._targetBlock) {
                        return _w._targetBlock.name + '->' + _w.targetproperty;
                    }
                }


                if (_w && _w.script && _w.script !== 'null') {
                    return 'XBlox::' + _w.script;
                }

                if (_w.targetEvent && _w.targetEvent.length > 0) {
                    return 'XBlox::' + _w.targetEvent;
                }

                return 'XBlox';

            }
            if (item.type === 'xblox/CSSState' && widget) {
                var _w = widget.domNode;
                return "CSS State/" + _w.name;
            }

            if (item.type === 'xblox/StyleState' && widget) {
                var _w = widget.domNode;
                return "Style State/" + _w.name;
            }

            if (widget.isWidget) {
                label = Widget.getLabel(widget);
                return label;
            }

            var type = widget.type;

            if (!type) {
                return;
            }

            var lt = this.useRichTextLabel ? "&lt;" : "<";
            var gt = this.useRichTextLabel ? "&gt;" : ">";

            if (type.indexOf("html.") === 0) {
                label = lt + type.substring("html.".length) + gt;
            } else if (type.indexOf("OpenAjax.") === 0) {
                label = lt + type.substring("OpenAjax.".length) + gt;
            } else if (type.indexOf(".") > 0) {
                label = type.substring(type.lastIndexOf(".") + 1);
            } else {
                label = widget.label || type;
            }

            var widget = Widget.byId(widget.id, this.getContext().getDocument());
            if (widget) {
                var id = widget.getId();
                if (id) {
                    label += " id=" + id;
                }
            }
            return label;
        },
        getColumns: function () {

            var thiz = this;
            return [
                /*
                                    {

                                        label: "Visibile",
                                        field: "visible",
                                        sortable: false,
                                        set: function (item, val, val2) {
                                            console.log('set by checkbox', arguments);
                                        },
                                        get: function (item) {
                                            // ensure initial rendering matches up with widget behavior
                                            var widget = thiz._getWidget(item);
                                            if (widget) {
                                                return !(widget.domNode.style.display === 'none');
                                            }

                                            return true;
                                        },
                                        editorArgs: {

                                        }
                                    },*/
                {
                    renderExpando: true,
                    label: "Name",
                    field: "type",
                    sortable: false,
                    formatter: function (name, item) {

                        var label = thiz.getLabel(item);
                        if (item.type == 'xblox/RunScript') {
                            var widget = thiz._getWidget(item);
                            if (widget) {
                                var proxy = widget.domNode; //xblox/RunScript
                                if (proxy) {
                                    var block = proxy._targetBlock;
                                    if (block) {
                                        if (block.declaredClass.indexOf('Command') != -1) {
                                            return '<span class="grid-icon ' + block.getIconClass() + '"></span>' + '' + block.name + ' (' + proxy.targetevent + ')';
                                        } else if (block.declaredClass.indexOf('Variable') != -1) {}
                                    }
                                }
                            }
                        }
                        return label;
                    }
                }
            ]
        },
        createGrid: function () {
            utils.destroy(this.outlineTree);
            dojo.empty(this.domNode);
            var thiz = this;
            var _ctorArgs = {
                features: {},
                className: 'outlineGridView dgrid dgrid-dgrid',
                cellNavigation: false,
                collection: this.store.filter(this.getRootFilter()),
                deselectOnRefresh: false,
                showHeader: false,
                owner: this,
                resizeToParent: true,
                columns: this.getColumns()
            };
            var grid = utils.addWidget(gridClass, _ctorArgs, null, this, true);
            this.grid = grid;
            this.grid.editor = this.editor;
            this.grid.set('collection', this.store.filter({
                type: 'html.body'
            }));
            this.outlineTree = grid;
            grid.refresh();
            this.add(grid);

            if (this._lastState) {
                grid.setState(this._lastState);
                delete this._lastState;
            }
            grid._on('selectionChanged', function (selection) {
                thiz.onItemSelected(selection.selection[0]);
            });
            grid.on('dgrid-datachange', function (evt) {
                var row = grid.row(evt.target);
                var value = evt.value;
                var context = thiz.getContext();
                var store = thiz.getStore();
                if (!context) {
                    return;
                }
                var widget = store.model._getWidget(row.data);
                if (widget) {
                    thiz._toggle(widget, !value);
                }
            });

        },
        initStore: function (data) {
            var self = this;
            this.store = new TreeMemory({
                data: data.items,
                model: data.model,
                provider: data.provider,
                idProperty: 'id',
                parentProperty: 'parent',
                getSync: function (id) {
                    var res = this.inherited(arguments);
                    var model = this.provider.getModel();
                    if (!res) {
                        var _w = model._getWidget({
                            id: id
                        });
                        if (_w) {
                            this.putSync(_w);
                            res = _w;
                        }
                    }
                    return res;
                },
                getChildrenSync: function (parent) {
                    var thiz = this,
                        _items = [];

                    var _cb = function (items) {
                        for (var i = 0; i < items.length; i++) {
                            var obj = items[i];
                            thiz.putSync(obj);
                        }
                        _items = items;
                    };

                    this.model.getChildren(parent, _cb);
                    return _items;
                },
                getChildren: function (parent, options) {
                    var thiz = this,
                        _items = [];
                    var _cb = function (items) {
                        for (var i = 0; i < items.length; i++) {
                            var obj = items[i];
                            thiz.putSync(obj);
                        }
                        _items = items;
                    };
                    this.model.getChildren(parent, _cb);
                    return this.inherited(arguments);
                },
                mayHaveChildren: function (parent) {
                    return this.model.mayHaveChildren(parent);
                }

            });
            return this.store;
        },
        /**
         *
         * @returns {*}
         */
        getContext: function () {
            if (this.currentEditor) {
                return this.currentEditor.getContext();
            }
            return null;
        },
        _getWidget: function (item) {
            return Widget.byId(item.id);
        },
        getStore: function () {
            return this.store;
        },
        getModel: function () {
            return this.outlineModel;
        },
        getRootFilter: function () {
            return {
                id: this.rootWidget ? this.rootWidget.id : ''
            }
        },
        hasItemActions: function () {
            return false;
        },
        _getViewContext: function () {
            return this.outlineProvider;
        },
        _getViewActions: function () {
            return [];
        },
        onWidgetChanged: function (evt) {
            debug && console.log('widget changed', evt);
            var type = evt.type;
            var ctx = this.getContext();
            if (type === ctx.WIDGET_ADDED) {
                this.getStore().putSync(evt.widget);
            } else if (type === ctx.WIDGET_REMOVED) {
                this.getStore().removeSync(evt.widget);
            }
            if (this.grid) {
                this.grid.refresh();
            }
        },
        onWidgetSelected: function (evt) {
            return;
            console.log('widget selected');

            if (evt.outline) {
                return;
            }

            var selection = evt.selection;
            if (!selection || !selection.length) {
                return;
            }

            var _sel = [];
            _.each(selection, function (item) {
                _sel.push(item.id);
            });
            this.grid.select(_sel, null, true, {
                focus: false,
                append: false,
                expand: true,
                delay: 1
            });

        },
        startup: function () {
            domClass.remove(this.domNode, 'ui-widget-content');
            this.subscribe("/davinci/ui/editorSelected", this.editorChanged);
            //this.subscribe("/davinci/ui/selectionChanged", this.selectionChanged);
            //this.subscribe("/davinci/ui/modelChanged", this.modelChanged);
            this.subscribe("/davinci/ui/context/pagerebuilt", this._pageRebuilt);
            this.subscribe(types.EVENTS.ON_WIDGET_CHANGED);
            this.subscribe("/davinci/ui/widgetSelected", this.onWidgetSelected);
            this.initReload();
        },
        onReloaded: function () {
            this.reset();
            if (this._lastEvent) {
                this.editorChanged(this._lastEvent);
            }
        },
        reset: function () {
            this.currentEditor = null;
            delete this.outlineProvider;
            delete this.outlineTree;
        },
        editorChanged: function (changeEvent) {
            debug && console.log('editorChanged', changeEvent);
            this._lastEvent = changeEvent;
            var editor = changeEvent.editor;
            if (this.currentEditor) {
                if (this.currentEditor == editor) {
                    return;
                }

                if (this.outlineTree) {
                    var state = this.outlineTree.getState();
                    this._lastState = state;
                    this.outlineTree.destroy();

                }
                if (this.store) {
                    this.store.destroy();
                    delete this.store;
                }
                delete this.outlineProvider;
                delete this.outlineTree;
            }

            this.currentEditor = editor;
            if (!editor) {
                return;
            }

            if (editor.getOutline) {
                this.outlineProvider = editor.getOutline();
            }

            if (this.outlineProvider) {
                this.createTree();
            } else {
                this.containerNode.innerHTML = 'no outline aviable';
            }

        },
        createTree: function () {
            if (this.popup) {
                this.popup.destroyRecursive();
            }
            if (!this.outlineProvider && this.currentEditor) {
                this.outlineProvider = this.currentEditor.getOutline();
                this.outlineProvider._outlineView = this;
            }
            if (this.outlineProvider && this.outlineProvider.getModel) {
                this.outlineModel = this.outlineProvider.getModel(this.currentEditor);
            }
            var model = this.outlineModel;
            var provider = this.outlineProvider;
            var root = provider && provider._context ? provider._context.rootWidget : null;
            if (!root) {
                console.error('have no root')
            }
            var outlineActionsID = (this.outlineProvider.getActionsID && this.outlineProvider.getActionsID()) || 'davinci.ui.outline';
            this.initStore({
                items: [root],
                model: model,
                provider: provider
            });
            this.createGrid();
        },
        selectionChanged: function (selection) {
            if (!this.publishing["/davinci/ui/selectionChanged"] &&
                selection.length && selection[0].model && this.outlineTree) {
                this.outlineTree.selectNode([selection[0].model]);
            }
        },
        _pageRebuilt: function () {
            if (this.outlineTree) {
                var paths = this.outlineTree.get("paths");
                this.createTree();
                this.outlineTree.set("paths", paths);
            }
        },
        _modelChanged: function (modelChanges) {
            console.log('_modelChanged', selection);
            if (this.outlineModel && this.outlineModel.refresh) {
                this.outlineModel.refresh();
            } else if (this.outlineProvider && this.outlineProvider.getStore) {
                this.outlineModel.store = this.outlineProvider.getStore();
                this.outlineModel.onChange(this.outlineProvider._rootItem);
            }
        }
    });
});