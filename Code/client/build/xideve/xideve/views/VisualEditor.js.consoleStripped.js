/** @module xideve/views/VisualEditor **/
define("xideve/views/VisualEditor", [
    'dcl/dcl',
    "dojo/dom-construct",
    "dojo/Deferred",
    'dojo/promise/all',
    "require",
    'xide/layout/ContentPane',
    'xide/layout/_Accordion',
    'xide/utils',
    'xide/types',
    "davinci/ve/PageEditor",
    "davinci/Runtime",
    "davinci/Workbench",
    "dojo/_base/connect",
    "dojo/dom-class",
    "davinci/ve/metadata",
    'davinci/ve/views/SwitchingStyleView',
    'xideve/views/OutlineView',
    'xideve/views/OutlineGridView',
    'xide/registry',
    'xide/layout/_TabContainer',
    'xideve/views/VisualEditorSourceEditor',
    'xideve/views/VisualEditorLayout',
    'xideve/views/VisualEditorAction',
    'xideve/views/VisualEditorTools',
    'xide/editor/Default',
    'xide/_base/_Widget',
    'xide/mixins/ReloadMixin',
    "xblox/model/html/SetStyle",
    "xblox/model/html/SetCSS",
    "xblox/model/html/SetState",
    "wcDocker/splitter",
    "xideve/palette/Palette",

    'xdojo/declare',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'dstore/Trackable',
    'xide/data/TreeMemory',
    'xaction/DefaultActions',
    "xide/form/Checkbox",
    "dgrid/Editor",
    'xide/factory',
    "davinci/ve/tools/CreateTool",
    "xide/views/SimpleCIDialog",
    "xide/widgets/TemplatedWidgetBase"
], function (dcl, domConstruct, Deferred, all, require,
    ContentPane, AccordionContainer, utils, types, PageEditor,
    Runtime, Workbench, connect, domClass,
    Metadata, SwitchingStyleView, OutlineView, OutlineGridView, registry,
    _TabContainer,
    // VisualEditorPalette, 
    VisualEditorSourceEditor, VisualEditorLayout,
    VisualEditorAction, VisualEditorTools, Default, _Widget, ReloadMixin,
    SetStyle, SetCSS, SetState, splitter, Palette2,
    declare, Grid, TreeRenderer, KeyboardNavigation, __Widget, Trackable, TreeMemory, DefaultActions,
    Checkbox, Editor, factory,
    CreateTool, SimpleCIDialog, TemplatedWidgetBase
) {

    var debugFileChanges = false;
    var debugBlocks = false;
    var debugCSSEditor = false;

    console.clear();
    if (window.vee) {
        vee.onReload();
    }

    const updateWidget = (widget, editor, props) => {
        const widgetClass = dcl(TemplatedWidgetBase, {
            templateString: '<div class="CIActionWidget"></div>',
            widgetClass: ''
        });

        const ctx = editor.getEditorContext();
        const command = new davinci.ve.commands.ModifyCommand(widget, props, null);
        dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
            source: ctx.editor_id,
            command: command
        }]);
    }

    const editWidget = (widget, editor, done) => {
        const node = widget.domNode;
        const ctx = editor.getEditorContext();
        const cis = [
            utils.createCI('Name', types.ECIType.STRING, node.name, {
                group: 'General'
            }),
            utils.createCI('Style', types.ECIType.DOM_PROPERTIES, '', {
                group: 'Style',
                widget: {
                    open: true,
                    widget: widget,
                    value: $(node).attr('style'),
                    ctx: editor.ctx
                }
            })
        ]
        const nameDlg = SimpleCIDialog.create('Edit widget', cis, (changed) => {
            const ctrToolData = {
                context: ctx,
                type: 'xblox/StyleState',
                properties: {
                    name: cis[0].value || 'noname',
                    style: cis[1].value
                },
                userData: {}
            }
            updateWidget(widget, editor, {
                name: cis[0].value || 'noname',
                style: cis[1].value
            });
            done();
        });
        nameDlg.show();
    }
    window.editWidget = editWidget;
    // --- GRID
    const gridClass = Grid.createGridClass('StatesView', {
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Instance': 3,
                'Window': 2
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            getRootFilter: function () {
                return {
                    parentId: '.'
                };
            },
            postMixInProperties: function () {
                // this.collection = this.collection.filter(this.getRootFilter());
                this.columns = [{

                        label: "Visibile",
                        field: "visible",
                        sortable: false,
                        get: function (item) {
                            return item.widget.isActive(item.state.name);
                        },
                        editorArgs: {
                            title: ""
                        },
                        width: "30px",
                        editor: Checkbox
                    },
                    {
                        renderExpando: true,
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            },
            runAction: function (action) {
                const selection = this.getSelection();

                // 0 && console.log('run ac ' + action.command, selection);

                const _createStyleState = (widget, editor) => {
                    const ctx = editor.getEditorContext();
                    const cis = [
                        utils.createCI('Name', types.ECIType.STRING, 'noname', {
                            group: 'General'
                        }),
                        utils.createCI('Style', types.ECIType.DOM_PROPERTIES, '', {
                            group: 'Style',
                            widget: {
                                open: true
                            }
                        })
                    ]
                    const nameDlg = SimpleCIDialog.create('Create New Style State', cis, (changed) => {
                        const ctrToolData = {
                            context: ctx,
                            type: 'xblox/StyleState',
                            properties: {
                                name: cis[0].value || 'noname',
                                style: cis[1].value
                            },
                            userData: {}
                        }
                        const meta = Metadata.getMetadata(ctrToolData.type);
                        Metadata.getHelper(widget.type, 'tool').then(function (ToolCtor) {
                            // Copy the data in case something modifies it downstream -- what types can data.data be?
                            const tool = new(ToolCtor || CreateTool)(ctrToolData, {});
                            tool._context = ctx;
                            ctx.setActiveTool(tool);
                            tool._create({
                                position: {
                                    x: 0,
                                    y: 0
                                },
                                parent: widget,
                                userData: meta
                            });
                            ctx.setActiveTool(null);
                        }.bind(this));
                    });
                    nameDlg.show();

                }
                const editor = this.editor;
                const context = editor.getEditorContext();
                const _removeStyleState = (widget, editor, widgetStateNode) => {
                    const node = widget;
                    let stateWidget = context.toWidget(widgetStateNode);
                    node.removeState(widgetStateNode);
                    context.select(stateWidget, null, null, true);
                    editor.runAction('Edit/Delete');
                    context.select(context.toWidget(widget));
                }
                const _saveToLibrary = (state) => {
                    const cis = [
                        utils.createCI('Name', types.ECIType.STRING, state.name, {
                            group: 'General'
                        })
                    ]
                    const nameDlg = SimpleCIDialog.create('Edit widget', cis, (changed) => {
                        const libMgr = editor.ctx.getLibraryManager();
                        const style = {
                            "type": "xblox/StyleState",
                            "properties": {
                                "style": $(state).attr('style')
                            }
                        }
                        libMgr.setSetting(cis[0].value || state.name, style);
                    });
                    nameDlg.show();                    
                }
                switch (action.command) {
                    case 'File/Edit':
                        {
                            if (!selection.length) {
                                return;
                            }
                            return window.editWidget(context.toWidget(selection[0].state), editor, function () {
                                context.select(context.toWidget(selection[0].widget));
                            });
                        }
                    case 'File/Delete':
                        {
                            if (!selection.length) {
                                return;
                            }
                            return _removeStyleState(selection[0].widget, this.editor, selection[0].state);
                        }
                    case 'New/State':
                        {
                            return _createStyleState(this.editor.getSelection()[0], this.editor);
                        }
                    case 'File/Save':
                        {
                            if (!selection.length) {
                                return;
                            }
                            return _saveToLibrary(selection[0].state);
                        }
                }
                return this.inherited(arguments);
            },
            startup: function () {
                var defaultMixin = {
                    addPermission: true,
                    quick: true
                };
                const actions = [];
                const libMgr = this.editor.ctx.getLibraryManager();
                const store = libMgr.getStore();
                store.on('update', () => {
                    this.refresh();
                });

                actions.push(this.createAction({
                    label: 'New State',
                    command: 'New/State',
                    icon: 'fa-magic',
                    tab: 'Home',
                    group: 'View',
                    mixin: defaultMixin
                }));
                actions.push(this.createAction({
                    label: 'Save to Palette',
                    command: 'File/Save',
                    icon: 'fa-save',
                    tab: 'Home',
                    group: 'View',
                    mixin: defaultMixin
                }));
                this.addActions(actions);
                const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
                this.addActions(_defaultActions);
                this.inherited(arguments);
                this._showHeader(false);

                const updateState = (widget, value) => {
                    const ctx = this.editor.getEditorContext();
                    const valuesObject = {};
                    valuesObject['state'] = value;
                    const command = new davinci.ve.commands.ModifyCommand(widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: ctx.editor_id,
                        command: command
                    }]);
                    /*
                    let widgetStateNode = widget.domNode.getState(value);
                    if (widgetStateNode) {
                        let stateWidget = ctx.toWidget(widgetStateNode);
                        if (stateWidget) {
                            ctx.select(stateWidget);
                        }
                    }
                    */
                }

                this.on('dgrid-datachange', (evt) => {
                    var row = this.row(evt.target);
                    var value = evt.value;
                    const state = row.data.state;
                    if (!this.widgetSelection.length) {
                        return;
                    }
                    const widget = this.widgetSelection[0];

                    const widgetNode = widget.domNode;
                    if (!value) {
                        updateState(widget, widgetNode.disableState(state));
                    } else {
                        updateState(widget, widgetNode.enableState(state));
                    }
                });
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            _WIDGET: {
                CLASS: __Widget
            },
            EDITOR: {
                CLASS: Editor
            }
        }, {
            RENDERER: TreeRenderer
        },
        null,
        null);

    const ACTION = types.ACTION;
    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        // ACTION.CLIPBOARD,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.SEARCH,
        ACTION.CONTEXT_MENU
    ];

    const createStatesStore = () => {
        const storeClass = declare('paletteStore', [TreeMemory, Trackable], {});
        const data = [];
        return new storeClass({
            data: data,
            idProperty: 'id',
            parentProperty: 'parentId',
            id: utils.createUUID(),
            ctx: sctx,
            mayHaveChildren: function () {
                return false;
            },
            /**
             * Return the root item, is actually private
             * @TODO: root item unclear
             * @returns {{path: string, name: string, mount: *, directory: boolean, virtual: boolean, _S: (xfile|data|FileStore), getPath: Function}}
             */
            getRootItem: function () {
                return {
                    _EX: true,
                    id: '.',
                    name: '.',
                    group: true,
                    virtual: true,
                    _S: this,
                    getPath: function () {
                        return this.id;
                    }
                };
            },
            getParent: function (mixed) {
                if (!mixed) {
                    return null;
                }
                var item = mixed,
                    result = null;

                if (_.isString(item)) {
                    item = this.getSync(mixed);
                }

                if (item && item.parentId) {
                    result = this.getSync(item.parentId);
                }
                return result || this.getRootItem();
            }
        });
    }

    const createStatesView = (editor) => {

        const pe = editor.getPageEditor();
        const target = pe.statesTab;
        let view = editor.statesView;

        const comptype = 'delite';
        const store = editor.statesStore || createStatesStore();
        editor.statesStore = store;

        if (view) {
            utils.destroy(view, true);
            utils.destroy(view.template);
            $(view.template).remove();
            editor.statesView = null;
        }
        view = utils.addWidget(gridClass, {
            ctx: sctx,
            open: true,
            expandOnClick: true,
            _parent: target,
            collection: store,
            permissions: DefaultPermissions,
            editor: editor,
            widgetSelection: []
        }, null, target, true);

        editor.statesView = view;
        view.set('collection', store.filter({
            parentId: '.'
        }));
        view.refresh();
        view.resize();
        view.showToolbar(true);
        target.add(view);
    }

    window.createStatesView = createStatesView;
    const clearStatesStore = (editor) => {
        editor.statesStore.setData([]);
    }
    const refreshStatesView = (editor, selection) => {
        editor.statesView.set('collection', editor.statesStore.filter({
            parentId: '.'
        }))
        editor.statesView.widgetSelection = selection;
        editor.statesView.editor = editor;
    }
    const updateStatesView = (editor) => {

        const selectables = editor.getStatableWidgetsSelection();
        clearStatesStore(editor);

        if (!selectables.length) {
            refreshStatesView(editor, selectables);
            return;
        }

        const widget = selectables[0].domNode;

        const state = (_state, widget) => {
            return {
                id: _state.name,
                name: _state.name,
                parentId: '.',
                state: _state,
                widget: widget
            }
        };


        const states = widget.getStates();
        states.forEach((wState) => {
            editor.statesStore.putSync(state(wState, widget));
        });
        refreshStatesView(editor, selectables);
    }
    window.updateStatesView = updateStatesView;

    //--------------------------------------
    function addPreviewToggle() {

        const pe = this.getPageEditor();
        const design = pe.getDesignPane();
        const buttonBar = design.getFrame().$buttonBar;
        utils.destroy(window._cb);
        const cb = utils.addWidget(Checkbox, {
            title: 'Preview',
            userData: {
                value: false
            }
        }, null, buttonBar[0], true);

        $(cb.domNode).css('float', 'right');
        const node = $(cb.domNode).find('.checkbox');
        node.css('margin-top', '3px');
        node.css('margin-right', '10px');
        node.css('margin-bottom', '0');

        cb._on('change', (val) => {
            if (val) {
                this.runAction('View/Show/Preview');
            } else {
                this.runAction('View/Show/Design');
            }
        });
        this.add(cb);
        this._designToggle = cb;
        window._cb = cb;
    }

    if (window.vee) {
        addPreviewToggle.call(window.vee);
    }

    /**
     * @class module:xideve/views/VisualEditor
     * @augments module:xideve/views/VisualEditorPalette
     * @augments module:xideve/views/VisualEditorSourceEditor
     * @augments module:xideve/views/VisualEditorTools
     * @augments module:xideve/views/VisualEditorLayout
     * @augments module:xideve/views/VisualEditorAction
     * @extends module:xide/_base/_Widget
     */
    var Module = dcl([_Widget,
        VisualEditorSourceEditor,
        VisualEditorLayout,
        VisualEditorAction,
        VisualEditorTools,
        ReloadMixin.dcl
    ], {
        updateFrame: function () {
            var ctx = this.getEditorContext();
            if (ctx && ctx.isResizing) {
                return;
            }

            var pageEditor = this.editor,
                frame = this.getFrame(),
                self = this;

            function follow(what, target) {

                var frame = $(what);
                var container = $(target);
                var pos = container.offset();
                var top = pos.top;
                var left = pos.left;
                var context = self.getEditorContext();

                var rootWidget = context ? $(context.rootNode) : null;
                var bodyElement = context ? $(context.getDocumentElement().getChildElement("body")) : null;
                var width = container.css('width');
                var height = container.css('height');

                var sizeBodyH = false;
                var sizeBodyW = false;

                if (bodyElement) {
                    var maxWidth = bodyElement.inlineStyle('max-width');
                    if (maxWidth && maxWidth !== 'none') {
                        //  width = maxWidth;
                    }
                    var maxHeight = bodyElement.inlineStyle('max-height');
                    if (maxHeight && maxHeight !== 'none') {
                        // height = maxHeight;
                    }
                    //var _width2 = bodyElement).inlineStyle('width');
                    var _width = bodyElement.inlineStyle('width');
                    if (_width) {
                        //  width = maxWidth;
                        sizeBodyW = _width;
                    }
                    var _height = bodyElement.inlineStyle('height');
                    if (_height) {
                        // height = maxHeight;
                        sizeBodyH = _height;
                    }

                }

                $(frame).css('display', (self._hidden ? 'none' : 'block'));


                if (top == 0 && left == 0) {
                    return;
                }
                frame.css('top', top);
                frame.css('left', left);
                if (width !== 'none') {
                    frame.css('width', width);
                    frame.css('height', height);
                    var body = $(self.getEditorContext().rootNode);
                    frame.css('width', width);
                    frame.css('height', height);

                    sizeBodyH && body.css('height', sizeBodyH);
                    sizeBodyW && body.css('width', sizeBodyW);
                    if (!sizeBodyH && !sizeBodyW) {
                        body.css('width', width);
                        body.css('height', height);
                    }

                }
            }

            if (frame) {
                if (!frame._followTimer) {
                    frame._followTimer = setInterval(function () {
                        follow(frame, pageEditor._designCP.containerNode);
                    }, 500);
                }
            }

        },
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Widget': 80,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Step': 5,
            'New': 4,
            'Window': 1
        },
        declaredClass: "xideve/views/VisualEditor",
        templateString: "<div style='height: 100%;width: 100%' attachTo='containerNode'></div>",
        isVisualEditor: true,
        resizeToParent: true,
        showLESS: true,
        showBehaviours: false,
        showStyleView: true,
        blockScopes: null,
        onReload: function () {
            window.createStatesView(this);
            const pe = this.getPageEditor();
            const design = pe.getDesignPane();
            const buttonBar = design.getFrame().$buttonBar;
        },
        /**
         * leftLayoutContainer is passed by xfile::PanelManager::createEditor.
         * its a stacked container.
         */
        leftLayoutContainer: null,
        /**
         * rightLayoutContainer is passed by xfile::PanelManager::createEditor.
         * its a stacked container.
         */
        rightLayoutContainer: null,

        /** Palette */
        palette: null,
        paletteContainer: null,
        showPalette: true,

        /** Style */
        styleView: null,
        styleViewContainer: null,

        /** Outline **/
        addOutline: true, //enable outline view

        outlineView: null,
        outlineContainer: null,

        /** Outline Grid View **/
        outlineGridView: null,
        outlineGridContainer: null,
        /**
         * Instance to the actual visual editor
         *
         * @type {module:davinci/ve/PageEditor}
         * */
        editor: null,
        _designMode: true,
        _tempLeftContainer: null,
        textEditor: null,

        /**
         * is editor for a deliteful scene
         */
        delite: false,

        /**
         * @type {xideve/Template}
         */
        template: null,

        contextInfo: null,
        didAddSheetEditors: false,
        beanType: types.ITEM_TYPE.WIDGET,

        _didInitial: false,
        ready: false,
        destroyed: false,
        _skipTextEditorChange: false,
        _getHTMLBlocks: function (scope, owner, target, group, items) {
            items.push({
                name: 'HTML',
                iconClass: 'fa-paint-brush',
                items: [{
                        name: 'Set Style',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetStyle,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    },
                    {
                        name: 'Set CSS',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetCSS,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    },
                    {
                        name: 'Set State',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetState,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    }
                ]
            });
            return items;
        },
        onBuildBlockInfoListEnd: function (evt) {
            var owner = evt.owner,
                ve = owner ? owner.visualEditor : null;
            //if (!ve || ve !== this) {
            //   return;
            //}
            this._getHTMLBlocks(evt.scope, evt.view, evt.item, evt.group, evt.items);
        },
        __reloadModule: function () {
            Reload.reload().then(function (data) {
                var component = data.component;
                component.registerEditors();
            });
        },
        updateFocus: function () {
            var ctx = this.getEditorContext();
            if (ctx) {
                if (ctx.isRezing) {
                    return;
                }
                ctx.clearCachedWidgetBounds();
                ctx.updateFocusAll();
            }
        },
        set: function (key, value) {
            if (key === 'focused') {
                this.updateFocus();
            }
            return this.inherited(arguments);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Layout
        //
        /////////////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Selection
        //
        /////////////////////////////////////////////////////////////////////////////////////
        select: function (mixed, append) {
            mixed = _.isArray(mixed) ? mixed : [mixed];
            var first = mixed[0];
            if (!first) {
                return;
            }
            var context = this.getEditorContext();
            if (!context) {
                return;
            }
            if (_.isString(first)) {
                var _resolved = [];
                _.each(mixed, function (str) {
                    var widget = context.getWidget(str);
                    widget && _resolved.push(widget);
                });
                mixed = _resolved;
            }
            if (append !== true) {
                context.deselect();
            }
            _.each(mixed, function (widget) {
                context.select(widget, true, false);
            });
        },
        selectAll: function () {
            var context = this.getEditorContext(),
                widgets = context.getAllWidgets();
            _.each(widgets, function (widget) {
                context.select(widget, true, false);
            });
        },
        updateToolbars: function () {},
        getTemplateDirectoryPathItem: function () {
            return {
                mount: 'workspace',
                path: './templates'
            }
        },
        getSelection: function () {
            var _ctx = this.getEditorContext();
            var out = [];
            if (_ctx) {
                var selection = _ctx.getSelection();
                for (var i = 0; i < selection.length; i++) {
                    out.push(selection[i]);

                }
            }
            return out;
        },
        ensureSceneFiles: function (item) {
            var dfd = new Deferred();
            var chain = [];
            var store = item._S;
            var mount = item.mount;
            var fileExtension = utils.getFileExtension(item.path);
            var fileManager = this.ctx.getFileManager();

            var templateDirectoryItem = this.getTemplateDirectoryPathItem();

            var parentDirectoryItem = store.getParent(item);
            var parentDirectoryItemPath = parentDirectoryItem.getPath();

            var ensureItem = function (item, extension) {

                var itemDfd = new Deferred();

                var itemPath = item.getPath().replace('.' + fileExtension, '.' + extension);

                var item = store.getSync(itemPath);
                if (item) {
                    itemDfd.resolve(item);
                } else {

                    var newItemPath = './' + itemPath;

                    newItemPath = newItemPath.replace('././', './');
                    var newItemTemplatePath = templateDirectoryItem.path + '/newDHTML.' + extension;

                    //create the file
                    fileManager.mkfile(mount, newItemPath).then(function (result) {

                        //copy from template content over
                        fileManager.getContent(mount, newItemTemplatePath, function (content) {

                             0 && console.log('write from template to: ' + newItemPath);
                            fileManager.setContent(mount, newItemPath, content, function (content) {
                                itemDfd.resolve();
                            });
                        });
                    });
                }
                return itemDfd;
            };


            chain.push(ensureItem(item, 'css'));
            //chain.push(ensureItem(item,'less'));
            chain.push(ensureItem(item, 'xblox'));
            chain.push(ensureItem(item, 'js'));
            all(chain).then(function (results) {
                dfd.resolve();
            });
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Widget Instances
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Bean protocol implementation
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getActiveItem: function () {
            return null;
        },
        hasItemActions: function () {
            return this.ready;
        },
        getItem: function () {
            return this.currentItem;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        /////////////////////////////////////////////////////////////////////////////////////
        _createLeftContainer: function () {
            var main = this.getMainView();
            var pane = utils.addWidget(ContentPane, {
                closable: true,
                style: 'padding:0px;width:200px',
                region: 'left',
                cssClass: 'layoutLeft',
                splitter: true,
                parentContainer: main.layoutMain
            }, this, main.layoutMain, true, 'ui-widget-content');
            return pane;
        },
        getLeftContainer: function () {
            var main = this.getMainView();
            var container = main.leftLayoutContainer;
            if (container) {
                return container;
            }
            if (main.leftTabContainer) {
                return main.leftTabContainer;
            }

            this._tempLeftContainer = this._createLeftContainer();

            this.leftLayoutContainer = utils.addWidget(AccordionContainer, {
                persist: true,
                doLayout: true,
                cssClass: 'ui-widget-content'
            }, this, this._tempLeftContainer.containerNode, true);

            return this.leftLayoutContainer;

        },
        /**
         * This editor got active/visisble again, restore our panels in the main-view
         * @returns {*}
         */
        restoreStyleView: function () {

            return registry.byId('html_style_view_parent');
            if (!this.styleViewContainer) {
                return;
            }

            var styleView = registry.byId('html_style_view_parent');
            if (styleView) {
                var main = this.getMainView();
                /*if (styleView.isHidden) {*/

                var pe = this.getPageEditor();

                this.rightLayoutContainer = pe.getPropertyPane();
                utils.addChild(styleView, this.rightLayoutContainer);
                //this.rightLayoutContainer.addChild(styleView);


                styleView.isHidden = false;

                /*}*/
                return styleView;
            }
            return null;
        },
        /**
         * When user selects another bean, hide our panels instead of destroying them
         * @returns {*}
         */
        suspendStyleView: function () {
            return;
            if (!this.styleViewContainer) {
                return;
            }
            if (!this.styleViewContainer.isHidden) {
                try {
                    this.rightLayoutContainer.removeChild(this.styleViewContainer);
                } catch (e) {
                    console.error('error suspend style view  ' + e, e);
                }
                var widgetCache = dojo.byId('widgetCache');
                dojo.place(this.styleViewContainer.domNode, widgetCache);
                this.styleViewContainer.isHidden = true;
            }
        },
        /////////////////////////////////////////////////
        //
        //  Lifecycle & cleanup methods
        //
        onLastEditorRemoved: function () {
            // 0 && console.log('onLastEditorRemoved');
            this.suspendStyleView();

            //return;
            var stylePaneTC = registry.byId('html_style_view_parent');
            if (stylePaneTC && stylePaneTC.parentContainer) {
                stylePaneTC.parentContainer.removeChild(stylePaneTC);
                utils.destroy(stylePaneTC);
            }


            var stylePane = registry.byId('html_style_view');
            if (stylePane) {
                utils.destroy(stylePane);
            }

            var tc2 = registry.byId('palette-tabcontainer-right-top');
            if (tc2) {
                utils.destroy(tc2);
            }


        },
        clearAll: function () {
            //deselect all, otherwise focus-frame remains active
            if (this.editor) {
                this.editor.visualEditor.context.deselect();
            }
            Workbench._switchEditor(null);
            Workbench.currentEditor = null;
            Runtime.currentEditor = null;
            this.destroyed = true;
            if (this.blockScopes) {
                for (var i = 0; i < this.blockScopes.length; i++) {
                    var scope = this.blockScopes[i];
                    this.ctx.getBlockManager().removeScope(scope.id);
                }
                delete this.blockScopes;
            }
            this.outlineGridContainer = null;
            this.outlineGridView = null;
            this.onLastEditorRemoved();
        },
        getFrame: function () {
            var ctx = this.getEditorContext();
            if (ctx && ctx.frameNode) {
                return ctx.frameNode;
            }
            return null;
        },
        destroy: function () {

            var otherEditors = this.getEditors(),
                isLast = otherEditors.length == 0;

            var main = this.getMainView(),
                left = this.getLeftContainer(),
                ctx = this.getEditorContext();
            clearInterval(this._frameTimer);
            utils.destroy(this.getFrame());
            utils.destroy(this.statesView);
            var isEmbedded = this.leftLayoutContainer == null || (left.canDestroy && !left.canDestroy());
            if (!isEmbedded) {
                utils.destroy(this.leftLayoutContainer, false);
                main.layoutMain && main.layoutMain.removeChild && main.layoutMain.removeChild(this._tempLeftContainer);
                utils.destroy(this.paletteContainer, false);
            } else {
                utils.destroy(this.paletteContainer, false);
                //utils.destroy(this.outlineContainer,false);
                utils.destroy(this._cBPalettePane, false);
                //left.removeChild(this.outlineContainer);
                //this.outlineView.parentContainer = null;
                //utils.destroy(this.outlineView,true);
                try {
                    if (this.outlineGridContainer) {
                        left.removeChild(this.outlineGridContainer);
                        this.outlineGridView.parentContainer = null;
                    }
                    //this.outlineGridView.reset();
                    utils.destroy(this.outlineGridView, true);
                    //this.outlineView.destroy();
                    utils.destroy(this.palette);
                } catch (e) {
                    debugger;
                }
            }

            if (this && this.styleViewContainer) {
                this.styleViewContainer.keep = false;
            }

            utils.destroy(this.styleView, true, this);
            utils.destroy(this.tabContainer, true, this);
            utils.destroy(this.styleViewContainer, true, this);

            this.publish(types.EVENTS.ON_CONTEXT_DESTROYED, this.getEditorContext());
            //clear property panel for true
            //main.getRightTopTarget(true, false);

            /*
             var sview = registry.byId('html_style_view');
             if(sview){
             console.error('sview still alive!');
             }
             */

            /*
             try {
             //utils.destroy(this.styleViewContainer);
             //utils.destroy(this.styleView,true,this);
             }catch(e){
             debugger;
             }
             */

            try {
                this.clearAll();
            } catch (e) {
                logError(e);
            }
            if (isLast) {
                try {
                    utils.destroy(this.editor, true, this);
                } catch (e) {
                    logError(e);
                }
                try {
                    /*utils.destroy(this.leftLayoutContainer,true,this);
                     main.layoutMain.removeChild(this._tempLeftContainer);*/
                } catch (e) {
                    debugger;
                }
            }


            /*

             if (this.palette) {
             utils.destroy(this.palette);
             }

             if (this.leftLayoutContainer) {

             this.leftLayoutContainer.removeChild(this.paletteContainer);
             if (this.outlineView) {
             utils.destroy(this.outlineView);
             this.leftLayoutContainer.removeChild(this.outlineContainer);
             }
             }



             try {
             //remove the accordion
             if (this._tempLeftContainer) {
             utils.empty(this.leftLayoutContainer);
             main.layoutMain.removeChild(this._tempLeftContainer);
             }
             } catch (e) {
             console.error('fucked');
             }


             this.suspendStyleView();

             utils.destroy(this.editor, true, this);

             this._tempLeftContainer = null;
             this.outlineView = null;
             this.outlineContainer = null;
             this.palette = null;
             this.paletteContainer = null;
             */
            /*
             this.styleView =null;
             this.styleViewContainer = null;
             */
            //utils.destroy(this.getEditorContext().frameNode);
            //this.inherited(arguments);
        },
        /////////////////////////////////////////////////
        //
        //  Sub-Widget creation
        //
        getMainView: function () {
            return this.mainView;
        },
        addStyleView: function (context) {
            var self = this;
            var mainView = this.ctx.getApplication().mainView;
            var pe = this.getPageEditor();
            var propPane = pe.getPropertyPane();

            //#xo propPane._open();
            if (this.styleView) {
                return this.styleView;
            }

            function wirePane(pane) {
                pane._on('splitterMoveStart', function (e) {
                    self.hideFrame(true);
                });
                pane._on('splitterMoveEnd', function (e) {
                    self.hideFrame(false);
                });
            }

            wirePane(propPane);
            wirePane(pe.getDesignPane());

            var tabContainer = utils.addWidget(_TabContainer, {
                region: 'center',
                id: 'palette-tabcontainer-right-top',
                tabPosition: 'right' + '-h',
                tabStrip: false,
                'class': "davinciPalette davinciTopPalette widget",
                style: '',
                splitter: false,
                direction: 'right'
            }, null, propPane.containerNode, true);

            domClass.add(tabContainer.domNode, "davinciPalette davinciTopPalette");

            this.tabContainer = tabContainer;
            this.styleViewContainer = propPane;
            var _sTab = tabContainer.createTab('Widget', 'fa-cogs');
            try {
                this.styleView = utils.addWidget(SwitchingStyleView, {
                    toolbarDiv: propPane.containerNode,
                    context: context,
                    _editor: this.editor,
                    parentContainer: tabContainer,
                    tabContainer: tabContainer
                }, null, _sTab.containerNode, true);

            } catch (e) {
                console.error('html_style_view crash ' + e);

            }
            this.styleView._initialPerspectiveReady();
        },
        /**
         *
         * @param context
         */
        addOutlineGridView: function (context) {
            const settings = this.getSettings();
            var leftContainer = this.getLeftContainer();
            if (this.outlineGridView || !leftContainer) {
                return;
            }
            var pane = leftContainer.createTab('Outline', 'fa-cube', true, null, {
                parentContainer: leftContainer,
                open: settings.outlineOpen
            });
            this.outlineGridContainer = pane;
            var self = this;
            try {
                this.outlineGridView = utils.addWidget(OutlineGridView, {
                    context: context,
                    resizeToParent: true,
                    style: 'padding:0px;',
                    editor: this
                }, null, pane.containerNode, true);

                pane.add(this.outlineGridView);
                this.addNavigationGrid(this.outlineGridView);

                setTimeout(() => {
                    pane.resize();
                    this.outlineGridView && this.outlineGridView.resize();
                }, 500);

            } catch (e) {
                logError(e, 'outline crash ')
            }
            this.outlineGridView.startup();
            this.add(pane);
            window.outline = this.outlineGridView;
        },
        _handleKeyDown: function (e, isGlobal) {
            var handled = this._handleKey(e, isGlobal);
            // now pass to runtime if unhandled so global key listeners can take a stab
            // make sure to not pass global events back up
            if (!handled && !isGlobal) {
                Runtime.handleKeyEvent(e);
            }
        },
        /**
         *
         * @param e
         * @param isGlobal
         * @returns {*}
         * @private
         */
        _handleKey: function (e, isGlobal) {
            if (!this.keyBindings) {
                return false;
            }

            var stopEvent = this.keyBindings.some(function (globalBinding) {
                if (isGlobal && !globalBinding.keyBinding.allowGlobal) {
                    return;
                }

                if (Runtime.isKeyEqualToEvent(globalBinding.keyBinding, e)) {
                    var Workbench = require("davinci/Workbench");
                    Workbench._runAction(globalBinding.action, this.editor, globalBinding.action.id);
                    return true;
                }
            }, this);

            if (stopEvent) {
                dojo.stopEvent(e);
            }

            return stopEvent;
        },
        _setupKeyboardHandler: function (editor) {

            var pushBinding = function (o) {
                if (!this.keyBindings) {
                    this.keyBindings = [];
                }
                this.keyBindings.push(o);
            }.bind(this);


            this._getViewActions().forEach(function (actionSet) {
                actionSet.actions.forEach(function (action) {
                    if (action.keyBinding) {
                        pushBinding({
                            keyBinding: action.keyBinding,
                            action: action
                        });
                    }
                    if (action.menu) {
                        action.menu.forEach(function (menuItemObj) {
                            if (menuItemObj.keyBinding) {
                                pushBinding({
                                    keyBinding: menuItemObj.keyBinding,
                                    action: menuItemObj
                                });
                            }
                        }, this);
                    }
                }, this);
            }, this);

            connect.connect(editor, "handleKeyEvent", this, "_handleKeyDown");
        },
        /////////////////////////////////////////////////
        //
        //  Public main entries
        //
        /**
         * Enable/disable various items on the editor toolbar and action bar
         */
        setDirty: function () {},
        shouldUpdateTextEditor: function () {
            if (this._skipTextEditorChange) {
                this._skipTextEditorChange = true;
                return false
            }
            //var is = this.splitMode ==  types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL || this.splitMode == types.VIEW_SPLIT_MODE.SPLIT_VERTICAL;
            return true;
        },
        addPageEditor: function (item) {
            if (this.editor) {
                return;
            }
            try {
                var thiz = this;

                var _createTextEditor = function (dstNode, fileName) {
                    return thiz.createHTMLEditor(dstNode, fileName, item);
                };
                var _additionalEditors = function () {
                    var blockEditor = {
                        title: 'Blocks'
                    };
                    return [blockEditor];
                };

                //this.editor = new PageEditor(dojo.create('div', {}), item.getPath(), item, _createTextEditor, this.template,_additionalEditors());
                var _PE = require('davinci/ve/PageEditor');
                this.editor = new _PE(dojo.create('div', {}), item.getPath(), item, _createTextEditor, this.template, _additionalEditors());
                this.editor.ctx = this.ctx;

                this.editor.editorContainer = this;
                this.editor.delegate = this;
                this.containerNode.appendChild(this.editor.domNode);
                this.declaredClass = this.editor.declaredClass; //important
                this.editor.editorID = 'davinci.ve.HTMLPageEditor';

                Runtime.currentEditor = this.editor;
                Workbench.currentEditor = this.editor;


                this.add(this.editor, null, false);
                this._setupKeyboardHandler(this.editor);
                this.addTextEditor(this.editor.htmlEditor);
                Workbench._switchEditor(this.editor);
                setTimeout(function () {
                    dojo.publish("/davinci/ui/editorSelected", [{
                        editor: thiz.editor,
                        oldEditor: null
                    }]);
                }, 2000);

                return this.editor;

            } catch (e) {
                console.error('crash in visual editor ' + e);
                logError(e, 'crash in visual editor ');
            }


        },
        ///////////////////////////////////////////////////////////////////
        //
        //  UI-Callbacks
        //
        _widgetSelectionChanged: function (args) {
            const ctx = this.getEditorContext();
            if (ctx != args.context) {
                return;
            }

            if (args.isFake == true) {
                return;
            }
            var selection = args.selection;

            window.updateStatesView(this);

            var selected = selection.length == 1 ? selection[0] : null;
            var item = selected || {
                dummy: true
            };

            this.currentItem = selected || null;

            var self = this;

            this.publish(types.EVENTS.ON_ITEM_SELECTED, {
                item: item,
                owner: this,
                beanType: 'widget'
            });
            this._emit('selectionChanged', {
                item: item,
                owner: this
            })
        },
        _resume: function () {
            this.restoreStyleView();
        },
        _suspend: function () {
            this.suspendStyleView();
        },
        hideFrame: function (hide) {
            this._hidden = hide;
            var _frame = this.getFrame();
            if (_frame) {
                $(_frame).css('display', (hide ? 'none' : 'block'));
            }
        },
        onHide: function () {
            this.inherited(arguments);
            this.hideFrame(true);
            this.updateFocus();
            if (this.editor) {
                Workbench._switchEditor(null);
                this.publish("/davinci/ui/editorSelected", [{
                    editor: null,
                    oldEditor: this.editor
                }]);
            }
            this._suspend();
        },
        onShow: function () {
            this.inherited(arguments);
            this.hideFrame(false);
            if (this.editor) {
                this.updateFocus();
                Workbench._switchEditor(this.editor);
                var thiz = this;
                setTimeout(function () {
                    var z = this;
                    thiz.publish("/davinci/ui/editorSelected", [{
                        editor: thiz.editor,
                        oldEditor: null
                    }]);
                }, 1000);
            }
            this._resume();
        },
        addPalette2: function (context, store, palette) {
            this.palette = registry.byId('html_palette');
            if (this.palette) {
                return;
            }
            const head = new Deferred();
            const p = new Palette2.Palette(this.ctx);
            var ctx = this.ctx;
            p.initMeta(ctx);
            const settings = this.getSettings();
            setTimeout(() => {
                var leftContainer = ctx.mainView.leftLayoutContainer;
                var pane = leftContainer.createTab('Palette', 'fa-cube', true, null, {
                    parentContainer: leftContainer,
                    open: settings.paletteOpen
                });
                this.paletteContainer = pane;
                try {

                    p.init();
                    const store = p.createStore();
                    this.palette = utils.addWidget(Palette2.Grid, {
                        ctx: ctx,
                        attachDirect: true,
                        collection: store,
                        open: true,
                        expandOnClick: true,
                        sources: [],
                        palette: p
                    }, null, pane, true);
                    $(this.palette.bodyNode).addClass('dojoyPalette');
                    $(this.palette.bodyNode).addClass('paletteLayoutIcons');

                    this.palette.set('collection', store.filter({
                        parentId: '.'
                    }));
                    this.add(pane, null, false);
                    this.add(this.palette, null, false);
                    pane.add(this.palette);
                    this.addNavigationGrid(pane);
                    setTimeout(() => {
                        this.palette.set('collection', store.filter({
                            parentId: '.'
                        }))
                    }, 1000);
                } catch (e) {
                    console.error('palette crash ' + e);
                }


                try {
                    // this.palette.descriptors = "dijit,dojox,html,OpenAjax"; // FIXME: parameterize this in plugin data?
                    // this.palette.setContext(context);
                } catch (e) {
                    console.error('palette crash ' + e);
                }

            }, 1000);
        },
        ///////////////////////////////////////////////////////////////////
        //
        //  Main entry
        //
        openItem: function (item) {
            var thiz = this,
                dfd = new Deferred();

            thiz.item = item;

            function load(item) {
                //                thiz._createPalette().then((data) => {
                thiz.ensureSceneFiles(item).then(function () {
                    if (!thiz.editor) {
                        thiz.addPageEditor(item);
                    }
                    thiz.hideFrame(false);
                    thiz.updateFrame();

                    var onReady = function (content) {
                        thiz.editor.setContent(item.path, content);
                        //var _ctx = thiz.editor.visualEditor.context;
                        // thiz.showPalette && thiz.addPalette(thiz.editor.visualEditor.context);
                        thiz.showPalette && thiz.addPalette2(thiz.editor.visualEditor.context);
                        try {
                            if (!thiz.styleView && thiz.showStyleView !== false) {
                                thiz.addStyleView(thiz.getEditorContext());
                            }
                        } catch (e) {
                            console.error('error style view ' + e);
                            logError(e, 'error style view ');
                        }

                        dfd.resolve();
                    };

                    thiz.ctx.getFileManager().getContent(item.mount, item.path, onReady);
                });

                //                });
            }
            if (this.template && !this.template.isLoaded()) {
                this.template.load().then(function () {
                    load(item);
                });

            } else {
                load(item);
            }

            return dfd;
        },
        onReady: function () {
            this.ready = true;
            this.hideFrame(false);
            this.updateFrame();
            addPreviewToggle.call(this);
        },
        addStatesView: function () {
            return window.createStatesView(this);
        },
        onEditorContextLoaded: function () {
            var thiz = this;
            this.addStatesView();
            if (this._didInitial) {
                setTimeout(function () {
                    if (thiz.outlineGridView) {
                        thiz.outlineGridView.editorChanged({
                            editor: null,
                            oldEditor: thiz.editor
                        });
                        thiz.outlineGridView.editorChanged({
                            editor: thiz.editor,
                            oldEditor: null
                        })
                    }
                }, 800);
                return;
            }
            this._didInitial = true;
            var _ctx = thiz.editor.visualEditor.context;
            if (thiz.addOutline) {
                try {
                    setTimeout(function () {
                        thiz.addOutlineGridView(_ctx);
                        //init context & other views by changing the current editor
                        dojo.publish("/davinci/ui/editorSelected", [{
                            editor: thiz.editor,
                            oldEditor: null
                        }]);
                        thiz.onReady();
                    }, 500);
                } catch (e) {
                    console.error('outline creation crashed ' + e);
                    debugger;
                }
            }
        },
        getNullItem: function () {
            if (this._nullItem) {
                return this._nullItem;
            }
            this._nullItem = {
                owner: this,
                beanType: 'widget'
            };
            return this._nullItem;
        },
        _editors: null,
        addTextEditor: function (editor) {
            !this._editors && (this._editors = []);

            this._editors.push(editor);

        },
        publishCSSRules: function (content) {
            var parser = new CSSParser();
            var sheet = parser.parse(content, false, true);
            var names = [];
            if (sheet) {
                _.each(sheet.cssRules, function (rule) {
                    if (rule.mSelectorText && rule.mSelectorText !== 'body') {
                        names.push(rule.mSelectorText.replace('.', ''))
                    }
                });
            }
            this.publish('CSSRulesUpdated', {
                names: names,
                editor: this
            })
        },
        loadSceneCSSFile: function (file) {
            var thiz = this;
            this.ctx.getFileManager().getContent(file.mount, file.path, function (content) {
                thiz.publishCSSRules(content);
            });
        },
        /**
         * Event when an editor context and its containing application has been fully
         * loaded. This is used to extend the left palette system for 'shared blocks'
         *
         * @param evt {Object} event data
         * @param evt.context {davinci/ve/Context} the davinci context
         * @param evt.appContext {xapp/manager/Context} the xapp context
         * @param evt.settings {Object} the application's settings
         * @param evt.ctx {xide/manager/Context} the root context (IDE context)
         * @param evt.global {Object} the loaded document's global
         * @param evt.blockScopes {xblox/model/Scope[]} the loaded xblox scopes
         *
         */
        onContextReady: function (evt) {
            //filter
            if (evt.context != this.getEditorContext()) {
                return;
            }

            this.contextInfo = evt;

            var thiz = this;
            var pe = this.getPageEditor();
            var sheets = evt.context._extraSheets;
            var peTabs = pe.bottomTabContainer;
            var store = this.item._S;
            this.ctx.getWindowManager().registerView(this, true);
            if (this.showLESS !== false && store && !this.didAddSheetEditors) {
                this.didAddSheetEditors = true;
                _.each(sheets, function (props, path) {
                    var sheetPath = path; //path.replace('.css','.less');
                    sheetPath = sheetPath.replace('././', './');
                    path = path.replace('././', './');
                    var fileItem = store.getSync(sheetPath);
                    debugCSSEditor &&  0 && console.log('add sheet editor ' + sheetPath, fileItem);
                    if (fileItem) {
                        props.storeItem = fileItem;
                        props.cssItem = store.getSync(path);
                    } else {
                        debugCSSEditor && console.error('couldnt find file : ', sheetPath);
                    }

                    if (fileItem) {
                        var editorArgs = {
                            sheetProps: props,
                            closeable: false,
                            emmet: true,
                            resizeToParent: true
                        };
                        var DOCKER = types.DOCKER;
                        var editorPane = pe.createTab(null, {
                            title: fileItem.name,
                            icon: 'fa-paint',
                            tabOrientation: DOCKER.TAB.TOP,
                            location: DOCKER.DOCK.STACKED,
                            target: pe.getSourcePane()
                        });

                        thiz.cssPane = editorPane;

                        var editor = Default.Implementation.open(fileItem, editorPane, editorArgs, true);
                        props.edior = editor;
                        thiz.addTextEditor(editor);
                        editorPane.add(editor);
                        thiz.loadSceneCSSFile(fileItem);
                        editor._on(types.EVENTS.ON_FILE_CONTENT_CHANGED, function (evt) {
                            thiz.publishCSSRules(evt.content);
                        });
                    } else {
                        console.error('couldnt find file : ', sheetPath);
                    }
                });

            }
            this.hideFrame(false);
            var scopes = evt.blockScopes;
            //nothing to do ?
            if (!scopes || !scopes.length) {
                return;
            }
            this.showBehaviours && this._buildBlockPalette(evt);
            evt.context.eventHandler = {
                'click': function (e) {
                    thiz.publish(types.EVENTS.ON_ITEM_SELECTED, {
                        item: thiz.currentItem || thiz.getNullItem(),
                        owner: thiz,
                        beanType: 'widget'
                    });
                    thiz._emit(types.EVENTS.ON_VIEW_SHOW, {
                        view: thiz
                    });
                }
            };
        },
        onAppReady: function (evt) {
            setTimeout(function () {
                if (this.outlineView && evt.context) {
                    this.outlineView.reset();
                    this.outlineView.createTree();
                }
            }.bind(this), 1000);
        },
        getTextEditor: function () {
            return this.editor.htmlEditor;
        },
        getEditor: function (_mount, _path) {
            var editor = null;
            _.each(this._editors, function (_editor) {
                var item = _editor.item;
                var mount = item.mount.replace('/', '');
                var path = item.path.replace('./', '');
                if (mount === _mount && path === _path) {
                    editor = _editor;
                }
            });
            return editor;
        },
        /**
         * triggered by intern editor
         * @param evt
         * @returns {*}
         */
        onEditorContentChanged: function (evt) {
            var self = this;
            this.__internUpdate = true;
            setTimeout(function () {
                self.__internUpdate = false;
            }, 5000);
            return this.reload(evt.content);
        },
        isSaving: false,
        save: function (pageEditor, visualEditor, model, text) {

            if (this.isSaving) {
                return;
            }


            this.isSaving = true;
            this.__internUpdate = true;
            var textEditor = this.getTextEditor();
            if (textEditor) {
                textEditor.set('value', text);
            }

            //tell everybody
            this.publish(types.EVENTS.ON_VISUAL_EDITOR_SAVE, {
                pageEditor: pageEditor,
                visualEditor: visualEditor,
                model: model,
                text: text,
                item: this.item
            });

            var thiz = this;
            //this will also publish ON_FILE_CHANGED
            this.ctx.getFileManager().setContent(this.item.mount, this.item.path, text, function () {
                //thiz.__internUpdate = false;
                thiz.isSaving = false;
            });
        },
        fileChanged: function (evt) {
            if (this.__internUpdate) {
                return;
            }
            var path = evt.data.path;
            var self = this;
            var _start = 'workspace_user';

            function reload(editor) {
                if (editor == self.getTextEditor()) {
                     0 && console.log('reload = same text editor');
                    editor.reload().then(function (content) {
                        self.reload(content);
                    });
                }

                editor.reload().then(function () {
                    self.onFileChanged({
                        editor: editor
                    });
                });

            }

            if (path.indexOf(_start) != -1) {
                var libPath = path.substr(path.indexOf(_start) + (_start.length + 1), path.length);
                var editor = this.getEditor(_start, libPath);
                if (editor) {
                    return reload(editor);
                }
            }
            var vfsPath = this.ctx.toVFSShort(path, 'workspace_user');
            if (vfsPath) {
                var editor = this.getEditor('workspace_user', vfsPath);
                if (editor) {
                    reload(editor);
                }
            }

        },
        onFileChanged: function (evt) {
            if (evt.__did) {
                return;
            }
            evt.__did = true;
            // 0 && console.log('intern: on file changed',evt);
            //scene css files ?
            if (evt && evt.editor && evt.editor.sheetProps) {

                var sheetProps = evt.editor.sheetProps;
                var doc = this.contextInfo.document;
                var thiz = this;
                dojo.withDoc(doc, function () {
                    //remove
                    var head = doc.getElementsByTagName('head')[0];
                    try {
                        // 0 && console.log('did destroy css');
                        if (sheetProps.link) {
                            head.removeChild(sheetProps.link);
                            sheetProps.link = null;
                        }
                    } catch (e) {
                        dojo.destroy(sheetProps.link);
                    }

                    var newUrl = thiz.ctx.getFileManager().getImageUrl(sheetProps.cssItem, true, false);
                    // 0 && console.log('do re-add css ' + newUrl);
                    //re-add
                    var newLink = domConstruct.create('link', {
                        rel: 'stylesheet',
                        type: 'text/css',
                        href: newUrl
                    });
                    head.appendChild(newLink);
                    dojo.place(newLink, head, 'last');
                    sheetProps.link = newLink;
                });
            }
        },
        onUseActionStore: function (store) {
            return;
            var actions = store.query();
            _.each(actions, function (action) {
                action.addPermission = true;
                !action.tab && (action.tab = 'Home');
            });
        },
        getBlockEditor: function () {
            var pe = this.getPageEditor();
            return pe.getBlockEditor();
        },
        resize: function () {
            this.updateFrame();
            this.updateFocus();
            var pe = this.getPageEditor();
            var ctx = this.getEditorContext();
            if (pe) {
                utils.resizeTo(pe.domNode, this.containerNode, true, true, null, {});
                pe.resize();
            }
        },
        startup: function () {
            var self = this,
                EVENTS = types.EVENTS;
            this._frameTimer = setInterval(() => {
                var ctx = this.getEditorContext();
                if (ctx) {
                    if (ctx.isRezing) {
                        return;
                    }
                }
                this.updateFrame();
            }, 1000);
            //this.addActions(this.getItemActions());
            //this.initReload();
            //warm up meta library for wizards
            Metadata.getSmartInput('xblox/RunScript');
            this.subscribe([
                EVENTS.ON_BUILD_BLOCK_INFO_LIST_END,
                EVENTS.ON_ACTION_CONTEXT_CHANGED,
                EVENTS.ON_ACTION_CHANGE_CONTEXT,
                EVENTS.ON_ACTION_CHANGE_CONTEXT,
                EVENTS.ON_FILE_CHANGED,
                EVENTS.ON_CONTEXT_READY, //track it
                EVENTS.ON_APP_READY, //reloads, text-editor changes
                {
                    //update editor content on file changes
                    key: types.EVENTS.ON_FILE_CONTENT_CHANGED,
                    handler: function (evt) {
                        var item = evt.item;
                        //@TODO: wtf
                        self._skipTextEditorChange = true;
                        if (item == self.item && evt.callee == self.textEditor) {
                            self.editor.setContent(self.item.getPath(), evt.content);
                        } else {
                            self.onFileChanged(evt);
                        }
                    }
                },
                {
                    key: '/davinci/ui/widgetSelected',
                    handler: self._widgetSelectionChanged
                },
                {
                    //update editor content on file changes
                    key: '/davinci/ui/context/loaded',
                    handler: self.onEditorContextLoaded
                }
            ]);
            this.initReload();
            setTimeout(function () {
                self.resize();
            }, 2000);

        },
        /**
         *
         * @param designMode {boolean}
         * @param updateWidget {boolean}
         */
        setDesignMode: function (designMode, updateWidget) {
            this._designMode = designMode;
            if (updateWidget === true) {
                var action = this._designToggleAction;
                if (action) {
                    var widget = action.getVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR).widget;
                    if (widget) {
                        widget.set('checked', designMode);
                    }
                }
            }
            var context = this.getEditorContext();
            if (context) {
                context.setDesignMode(designMode);
            }
        },
        _didHide: false,
        getSettings: function () {
            var settingsManager = this.ctx.getSettingsManager();

            var settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };
            var props = settingsStore.getSync('ve') || {
                value: {
                    showEditors: false,
                    selected: 'HTML'
                }
            };
            return props.value;
        },
        saveSettings: function () {
            var settingsManager = this.ctx.getSettingsManager();
            var settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };

            var props = settingsStore.getSync('ve') || {
                value: this.getSettings()
            };

            var value = props.value;
            value.showEditors = this.showCode;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            var selected = 'HTML';
            _.each(docker.getPanels(), (p) => {
                if (p._title === 'Blocks' && p.selected === true) {
                    selected = 'Blocks';
                }
                if (p._title.indexOf && p._title.indexOf('.css') !== -1 && p.selected === true) {
                    selected = p._title;
                }
            });
            value.selected = selected;
            value.outlineOpen = this.outlineGridView.open;
            value.paletteOpen = this.palette._parent.open;
            //  0 && console.log('save settings', value);
            settingsManager.write2(null, '.', {
                id: 've'
            }, {
                value: value
            }, true, null).then(function () {

            });
        },
        restoreSettings: function () {
            var settings = this.getSettings()
            if (!settings.showEditors) {
                setTimeout(() => {
                    if (this._didHide) {
                        var pe = this.getPageEditor();
                        var docker = pe.getDocker();
                        return;
                    }
                    this._didHide = true;
                    this.toogleCode();
                }, 500);
            }
            var selected = settings.selected;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            var panel = _.find(docker.getPanels(), {
                _title: selected
            });
            if (panel) {
                panel.select();
            }
        },
        didinitSettings: false,
        addNavigationGrid: function (grid) {
            grid._on(['hide', 'show'], () => {
                this.saveSettings();
            });
        },
        initSettings: function () {
            if (this.didinitSettings) {
                return;
            }
            this.didinitSettings = true;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            docker._on(types.DOCKER.EVENT.SELECT, (panel) => {
                var selected = panel.title();
                this.getSettings().selected = selected;
                this.saveSettings();
            });
        },
        onSceneBlocksLoaded: function (evt) {
            this.blockScopes = evt.blockScopes;
            debugBlocks &&  0 && console.log('onSceneBlocksLoaded : ', evt);
            var pe = this.getPageEditor();
            window['vee'] = this;
            pe.onSceneBlocksLoaded(evt, this);
            this.initSettings();
            setTimeout(() => {
                this.restoreSettings();
            }, 500);

        }
    });

    OutlineGridView.prototype.reloadModule = Module.prototype.reloadModule;
    OutlineView.prototype.reloadModule = Module.prototype.reloadModule;
    PageEditor.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorAction.prototype.reloadModule = Module.prototype.reloadModule;
    // VisualEditorPalette.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorSourceEditor.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorLayout.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorAction.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorTools.prototype.reloadModule = Module.prototype.reloadModule;

    Module.getFileActions = function (ctx, item) {
        return [
            ctx.createAction({
                label: 'Preview in Browser',
                command: 'File/Preview',
                tab: 'Home',
                icon: 'fa-eye',
                tab: 'Home',
                mixin: {
                    quick: true,
                    addPermission: true,
                    handler: function () {
                        window.open(ctx.getContextManager().getViewUrl(item));
                    }
                },
                custom: true
            })
        ]
    }
    return Module;
});