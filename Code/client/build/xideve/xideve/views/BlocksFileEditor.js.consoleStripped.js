define("xideve/views/BlocksFileEditor", [
    'dcl/dcl',
    "dojo/on",
    'xblox/views/BlocksFileEditor',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin',
    'davinci/ve/widget',
    'xide/types',
    'xide/utils'
], function (dcl, on, BlocksFileEditor, ReloadMixin, EventedMixin, Widget, types, utils) {
    var debugRun = false;
    var debugWire = false;
    var debugSelection = false;
    if (BlocksFileEditor !== 'not-a-module') {
        return dcl([BlocksFileEditor, ReloadMixin.dcl], {
            suspendedViews: null,
            destroy: function () {
                if (this.suspendedViews) {
                    _.each(this.suspendedViews, utils.destroy);
                    delete this.suspendedViews;
                }
            },
            getSuspendedView: function () {
                if (!this.suspendedViews) {
                    return;
                }
                var last = _.last(this.suspendedViews);
                if (last) {
                    this.suspendedViews.remove(last);
                }
                return last;
            },
            createGroupedBlockView: function (container, group, scope, extra, gridBaseClass) {

                var thiz = this;
                gridBaseClass = gridBaseClass || this.gridClass;
                var store = scope.blockStore;

                if (this._lastTab && !this.__right) {
                    this.__right = this.getRightPanel(null, null, 'DefaultFixed', {
                        target: this._lastTab
                    });

                    this.__right.maxSize(400, null);
                }
                var gridArgs = {
                    __right: this.__right,
                    ctx: this.ctx,
                    blockScope: scope,
                    blockGroup: group,
                    attachDirect: true,
                    resizeToParent: true,
                    collection: store.filter({
                        group: group
                    }),
                    _parent: container,
                    getPropertyStruct: this.getPropertyStruct,
                    setPropertyStruct: this.setPropertyStruct,
                    owner: this
                };

                utils.mixin(gridArgs, extra);

                var suspendedView = this.getSuspendedView();
                if (suspendedView) {
                    utils.mixin(suspendedView, gridArgs);
                    if (!container._widgets) {
                        container._widgets = [];
                    }
                    container.add(suspendedView);
                    container.grid = suspendedView;
                    utils.addChild(container, suspendedView.template.template, false);
                    suspendedView.set('collection', store.filter({
                        group: group
                    }));
                    return suspendedView;
                }

                var view = utils.addWidget(gridBaseClass, gridArgs, null, container, false);
                if (!view.__editorActions) {
                    view.__editorActions = true;
                    this.addGridActions(view, view);
                }
                container.grid = view;
                view._on('selectionChanged', function (evt) {
                    thiz._emit('selectionChanged', evt);
                });

                view._on('onAfterAction', function (e) {
                    thiz.onGridAction(e);
                });
                container.on(types.DOCKER.EVENT.VISIBILITY_CHANGED, function (visible) {
                    if (visible) {
                        thiz.setVisibleTab(container);
                        view.onShow();
                        var right = thiz.getRightPanel();
                        var splitter = right.getSplitter();
                        if (!splitter.isCollapsed()) {
                            view.showProperties(view.getSelection()[0]);
                        }
                        if (view.__last) {
                            view._restoreSelection();
                            view.focus(view.row(view.__last.focused));
                        }
                    } else {
                        view.__last = view._preserveSelection();
                    }
                    if (visible && this.grid && !this.grid._started) {}
                });
                this.registerGrids && this.ctx.getWindowManager().registerView(view, false);
                if (!container._widgets) {
                    container._widgets = [];
                }
                container.add(view);
                return view;
            },
            suspendView: function (tab) {
                if (!this.suspendedViews) {
                    this.suspendedViews = [];
                }
                if (!this.cacheNode) {
                    this.cacheNode = $("<div style='display: none'></div>");
                    $(this.domNode).append(this.cacheNode);
                    this.cacheNode = this.cacheNode[0];
                }
                var grid = tab.grid;
                if (!grid) {
                    return;
                }
                tab.remove(grid);
                tab.grid = null;
                this.cacheNode.appendChild(grid.template.template);
                this.suspendedViews.push(grid);
            },
            clearGroupViews: function (all) {
                var thiz = this;
                _.each(this.tabs, function (tab) {
                    thiz.suspendView(tab);
                });
                _.each(this.tabs, function (tab) {
                    tab.destroy();
                });
                delete this.tabs;
                this.destroyWidgets();
            },
            declaredClass: "xideve.views.BlocksFileEditor",
            widget: null,
            registerView: true,
            createLinkedBlock: function (_block, group, scope, proxy) {
                if (scope.getBlockById(_block.id)) {
                    return;
                }
                //
                var blockSerialized = scope.blockToJson(_block);
                //change group:
                blockSerialized.group = group;

                var device = _block.scope.resolveDevice(proxy.block);
                var deviceName = device ? device.name : "";


                //add it to the scope
                var block = scope.blockFromJson(blockSerialized);
                block.icon = 'text-success fa-link';
                if (device) {
                    block.name = deviceName + '->' + block.name;
                }

                block.virtual = block;
                block.serializeMe = false;
                block.enabled = false;
                block.canEdit = function () {
                    return false;
                };
                block.hasActions = function () {
                    return false;
                };
                block.canMove = function () {
                    return false;
                };
                block.canEdit = function () {
                    return false;
                };
                block.canAdd = function () {
                    return false;
                };
                block.mayHaveChildren = function () {
                    return false;
                };
                proxy.onDestroy = function () {
                    block.destroy();
                    scope.blockStore.removeSync(block.id);
                };
            },
            getModel: function () {
                var ve = this.visualEditor;
                var outlineGridView = ve.outlineGridView;
                if (outlineGridView) {

                    var model = outlineGridView.getModel();
                    if (model) {
                        return model;
                    }
                }
                return null;
            },
            _getWidget: function (widget) {
                return Widget.byId(widget.id);
            },
            getWidgetChildren: function (item) {
                var widgets = [];
                var widget = this._getWidget(item);
                if (widget && widget.getChildren) {
                    widgets = this._getWidget(item).getChildren();
                }
                return widgets;

            },
            /**
             * find x-script nodes within the widget (dragged from the device)
             * problem: _targetBlock might not be there when the device is
             *          not connected!
             * @param widget
             */
            getWidgetBlocks: function (widget) {
                var veWidget = Widget.byId(widget.id);
                var children = this.getWidgetChildren(widget);
                var blocks = [];
                for (var i = 0; i < children.length; i++) {

                    var child = children[i];
                    if (child.type === 'xblox/RunScript') {
                        if (child.domNode) {
                            var xScript = child.domNode;

                            var event = null;
                            if (xScript.targetevent && !xScript.sourceevent) {
                                event = xScript.targetevent;
                            } else if (xScript.targetevent && xScript.sourceevent && xScript.bidirectional === true) {
                                event = xScript.targetevent;
                            }

                            blocks.push({
                                group: event,
                                block: xScript._targetBlock,
                                proxy: xScript
                            });
                        }
                    }
                }
                return blocks;

            },
            /**
             *
             * @param widget
             * @param event
             * @param block
             */
            wireNode: function (widget, event, block) {
                var run = function (event, value, block, widget) {
                    if (!block.enabled) {
                        return;
                    }
                    //setup variables
                    var context = widget,
                        result;
                    if (block && context) {
                        result = block.solve(block.scope);
                        debugRun &&  0 && console.log('run ' + block.name + ' for event ' + event, result + ' for ' + this.id, context);
                    }
                };

                //patch the target
                if (!widget.subscribe) {
                    utils.mixin(widget, EventedMixin.prototype);
                }


                var _target = widget.domNode || widget,
                    _event = event,
                    _isWidget = widget.declaredClass || widget.startup,
                    _hasWidgetCallback = widget.on != null && widget['on' + utils.capitalize(_event)] != null,
                    _handle = null,
                    _isDelite = _target.render != null && _target.on != null,
                    thiz = this;


                block._targetReference = _target;


                if (_isWidget &&
                    //dijit
                    (widget.baseClass && widget.baseClass.indexOf('dijitContentPane') != -1)
                    //delite
                    ||
                    widget.render != null || widget.on != null) {
                    _isWidget = false; //use on
                }

                if (_target) {
                    if (!_isDelite && (!_hasWidgetCallback || !_isWidget)) {
                        _handle = on(_target, event, function (evt) {
                            run(event);
                        }.bind(this));

                    } else {
                        _target = widget;
                        var useOn = true;
                        if (useOn) {
                            if (!_isDelite) {
                                var _e = 'on' + utils.capitalize(_event);
                                widget[_e] = function (val, nada) {
                                    if (_target.ignore !== true) {
                                        run(event, val);
                                    }
                                }
                            } else {
                                if (utils.isSystemEvent(event)) {
                                    _handle = _target.subscribe(event, function (evt) {
                                        run(event, evt, block);
                                    }.bind(this), widget);

                                } else {

                                    if (utils.isNativeEvent(event)) {
                                        event = event.replace('on', '');
                                    }
                                    _handle = _target.on(event, function (evt) {
                                        run(event, evt.currentTarget.value, block, widget);
                                    }.bind(this));
                                }
                            }

                        } else {
                            widget['on' + utils.capitalize(_event)] = function (val) {
                                if (_target.ignore !== true) {
                                    run(event, val);
                                }
                            }
                        }
                    }
                    if (_handle && widget.addHandle) {
                        widget.addHandle(event, _handle);
                    }
                }
            },
            wireWidget: function (editorContext, ctx, scope, widget, node, event, group) {
                var blocks = scope.getBlocks({
                    group: group
                });

                if (widget._destroyHandles) {
                    widget._destroyHandles();
                }
                for (var i = 0; i < blocks.length; i++) {
                    var block = blocks[i];
                    debugWire &&  0 && console.log('wire node to block : ' + block.title + ' for ' + event, block);
                    this.wireNode(widget.domNode, event, block);
                }
            },
            wireScope: function (editorContext, ctx, scope) {
                var allGroups = this.blockScope.allGroups();
                for (var i = 0; i < allGroups.length; i++) {
                    var group = allGroups[i];
                    var event = null;
                    var widgetId = null;
                    var parts = group.split('__');
                    if (parts.length == 2) {
                        event = parts[1];
                        widgetId = parts[0];
                    }
                    if (event && widgetId) {
                        var widget = Widget.byId(widgetId);
                        if (widget) {
                            this.wireWidget(editorContext, ctx, scope, widget, widget.domNode, event, group);
                        }
                    }
                }
            },
            /**
             *
             * @returns {module:davinci/ve/PageEditor}
             */
            getPageEditor: function () {
                return this.delegate;
            },
            getRootWidget: function () {
                var context = this.getPageEditor().getVisualEditor().getContext();
                return context.getRootWidget();
            },
            showWidgetBlocks: function (widget) {
                if (!widget) {
                    return;
                }
                if(!widget.getId){
                  return;                            
                }
                var widgetId = widget.getId();
                var blockGroup = widget.domNode.getAttribute('blockGroup'),
                    thiz = this;

                /**
                 * if the widget has a variable mapping
                 */
                var widgetBlocks = this.getWidgetBlocks(widget);
                debugSelection &&  0 && console.log('show block groups ' + widgetId + ' with block group: ' + blockGroup, widgetBlocks);
                //nothing set yet:
                if (!widgetId && !blockGroup) {
                    //use temp_id:
                    widgetId = widget.id;
                }
                //clear all views
                this.clearGroupViews(true);
                var groups = [];
                var isBlockGroup = function (str, group) {
                    //test by
                    if (group.startsWith(str)) {
                        return true;
                    }
                };
                var newGroupPrefix = '';
                var ctx = this.ctx;
                var deviceManager = ctx.getDeviceManager();

                //we have widget id but no block group
                if (widgetId && !blockGroup) {
                    newGroupPrefix = widgetId;
                    var all = this.blockScope.allGroups();
                    for (var i = 0; i < all.length; i++) {
                        var group = all[i];
                        var testString = '' + widgetId;
                        if (isBlockGroup(testString, group)) {
                            groups.push(group);
                        }
                    }
                }
                this.newGroupPrefix = newGroupPrefix + '__';
                //render xscript-groups
                var xScriptGroups = {};
                if (widgetBlocks) {
                    xScriptGroups = _.groupBy(widgetBlocks, function (block) {
                        var _item = block.block || block.proxy;
                        if (block.proxy) {
                            if (block.proxy.targetevent) {
                                return block.proxy.targetevent;
                            }
                        }

                        if (block.block) {
                            return block.block.group
                        }

                        return block.group;
                    });

                    if (!_.isEmpty(xScriptGroups)) {
                        _.each(xScriptGroups, function (blocks, group) {
                            var realGroup = thiz.newGroupPrefix + group;
                            _.each(blocks, function (item) {

                                var _item = item.block;
                                var proxy = item.proxy;
                                if (!_item && item.proxy) {
                                    if (item.proxy._targetBlock) {
                                        _item = item.proxy._targetBlock;
                                    } else {
                                        var block = item.proxy.block;
                                        if (block.indexOf('://') !== -1) {
                                            var _block = deviceManager.getBlock(block);
                                            if (_block) {
                                                _item = _block;
                                            }
                                        }
                                    }
                                }
                                if (!_item) {
                                     0 && console.warn('have no block or item!', item);
                                } else {
                                    thiz.createLinkedBlock(_item, realGroup, thiz.blockScope, proxy);
                                }
                            });

                            if (groups.indexOf(realGroup) == -1) {
                                groups.push(realGroup);
                            }
                        });
                    }
                }
                if (groups.indexOf('Variables') == -1) {
                    groups.push('Variables');
                }
                this.renderGroups(groups, this.blockScope); //does: this.createNewTab();
            },
            showGlobalBlocks: function () {
                //clear all views
                this.clearGroupViews(true);
                this.newGroupPrefix = '';
                this.initWithScope(this.blockScope);
            },
            onItemRemoved: function () {},
            /**
             *
             * @param args
             * @private
             */
            _widgetSelectionChanged: function (args) {
                if (args && args.isFake) {
                    return;
                }
                this._mute = true;
                var selection = args.selection;
                if (selection.length == 1) {
                    this.widget = selection[0];
                    this.showWidgetBlocks(selection[0]);
                } else {
                    this.showWidgetBlocks(this.getRootWidget());
                }
                _.each(this.tabs, function (tab) {
                    tab._emit = function () {};
                    tab.publish = function () {};
                });
                this.resize();
            },
            onWidgetChanged: function (evt) {
                var type = evt.type;
                var ctx = this.editorContext;
                var widget = evt.widget;
                if (type === ctx.WIDGET_MODIFIED) {
                    var contextManager = this.ctx.getContextManager();
                    contextManager.wireWidget2(ctx, this.ctx, this.blockScope, ctx.appContext, widget);
                    //we fire app ready to re-init child widgets
                    ctx.appContext.publish(types.EVENTS.ON_APP_READY, {
                        context: ctx.appContext
                    });
                }
                if (type === ctx.WIDGET_ADDED) {
                    if (widget.type === 'xblox/RunScript') {
                        widget.domNode.onAppReady({
                            context: ctx.appContext
                        });
                    }
                } else if (type === ctx.WIDGET_REMOVED) {
                    if (widget.type === 'xblox/RunScript') {
                        widget.domNode.destroy();
                    }
                }
            },
            startup: function () {
                this.subscribe('/davinci/ui/widgetSelected', this._widgetSelectionChanged);
                this.subscribe(types.EVENTS.ON_WIDGET_CHANGED);
                this.initReload();
            }
        });
    }
});