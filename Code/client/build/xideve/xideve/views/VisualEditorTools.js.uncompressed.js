/** @module xideve/views/VisualEditorTools **/
define("xideve/views/VisualEditorTools", [
    "dcl/dcl",
    "davinci/ve/widget"
], function (dcl,widgetUtils) {
    $.fn.inlineStyle = function (prop) {
        var styles = this.attr("style"),
            value;
        styles && styles.split(";").forEach(function (e) {
            var style = e.split(":");
            if ($.trim(style[0]) === prop) {
                value = style[1];
            }
        });
        return value;
    };
    /**
     *
     * @mixin module:xideve/views/VisualEditorTools
     * @lends module:xideve/views/VisualEditor
     */
    return dcl(null,{
        declaredClass:"xideve/views/VisualEditorTools",
        /**
         * Returns all instances withing our parent tab container
         * @returns {Array}
         */
        getEditors: function () {
            var result = [];
            if (this.parentContainer) {

                var children = this.parentContainer.getChildren();
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    if (child.declaredClass === "davinci.ve.PageEditor") {
                        result.push(child);
                    }
                }
            }

            return result;
        },
        getEditorContext: function () {
            var context = null;
            if (this.editor && this.editor.currentEditor && this.editor.currentEditor.context) {
                context = this.editor.currentEditor.context;
            }
            return context;
        },

        getPageEditor:function(){
            return this.editor;
        },
        onWidgetAction:function(selection){

            /*
            var context = this.getEditorContext();
            var command = this.getContext().getCommandForStyleChange(value); //#23
            if (command) {
                this.getContext().getCommandStack().execute(command);
                if (command._newId) {
                    var widget = widgetUtils.byId(command._newId, context.getDocument());
                    this.context.select(widget);
                }
                this._srcChanged();
                dojo.publish("/davinci/ui/widgetValuesChanged", [value]);
            }
            */

        },
        _sameSize:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = $(selection[0].domNode);

            var cWidth = firstItem.css('width'),
                cHeight = firstItem.css('height');

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {


                var target = selection[i];
                var _target = $(target.domNode),
                    values = {
                        width:cWidth,
                        height:cHeight

                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23

                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _sameWidth:function(){

            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = $(selection[0].domNode);

            var cWidth = firstItem.css('width'),
                cHeight = firstItem.css('height');

            //pop first
            selection.shift();
            for (var i = 0; i < selection.length; i++) {


                var target = selection[i];
                var _target = $(target.domNode),
                    values = {
                        width:cWidth
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };

                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }

        },
        _sameHeight:function(){


            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = $(selection[0].domNode);

            var cWidth = firstItem.css('width'),
                cHeight = firstItem.css('height');

            //pop first
            selection.shift();


            var _n2 = this.getSelection();

            for (var i = 0; i < selection.length; i++) {


                var target = selection[i];
                var _target = $(target.domNode),
                    values = {
                        height:cHeight
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command,true);
                    var _n = this.getSelection();
                    if (command._newId) {
                        var widget = widgetUtils.byId(command._newId, context.getDocument());
                    }
                }
            }
        },
        _alignBottomHorizontal:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "bottom right",
                    "targetAt": "bottom left",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        top: cTop + (offset.moveBy.y * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }

        },
        _alignTopHorizontal:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "top right",
                    "targetAt": "top left",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        top: cTop + (offset.moveBy.y * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _alignRightVertical:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();
            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "bottom right",
                    "targetAt": "top right",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        left:cLeft +(offset.moveBy.x * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _alignCenterVertical:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();
            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "bottom center",
                    "targetAt": "top center",
                    boundary:$(document.body)
                });

                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        left:cLeft +(offset.moveBy.x * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _alignLeftVertical:function(){

            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    itemAt: "bottom left",
                    targetAt: "top left",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        left:cLeft +(offset.moveBy.x * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command,true);
                }
            }
        }
    });
});
