/** @module xideve/views/VisualEditorAction **/
define("xideve/views/_VisualEditorAction", [
    "dcl/dcl",
    "xdojo/has",
    "require",
    'xaction/Action',
    'xide/types',
    'xide/utils',
    'xide/factory',
    "davinci/Runtime",
    "davinci/Workbench",
    "davinci/ve/metadata",
    "xaction/ActionProvider",
    'xaction/DefaultActions',
    'dojo/Deferred',
    'xdojo/declare',
    "davinci/ve/tools/CreateTool",
    "xide/views/SimpleCIDialog",
    "xide/widgets/TemplatedWidgetBase"
], function (dcl, has, require, Action, types, utils, factory, Runtime, Workbench, Metadata, ActionProvider, DefaultActions, Deferred, declare, CreateTool, SimpleCIDialog,
    TemplatedWidgetBase) {
    var ACTION = types.ACTION;
    var debug = false;
    const Impl = {
        _float: function () {
            var thiz = this;
            var pe = thiz.getPageEditor();
            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getSourcePane();
            var propertyPane = pe.getPropertyPane();
            _float(docker, sourcePane, 300, 300);
            docker.movePanel(propertyPane, types.DOCKER.DOCK.STACKED, sourcePane);
            docker.movePanel(pe.getBlocksPane(), types.DOCKER.DOCK.STACKED, sourcePane);
            docker.movePanel(this.cssPane, types.DOCKER.DOCK.STACKED, sourcePane);
            propertyPane.maxSize(null, null);
            propertyPane.minSize(null, null);
            this.cssPane.select();
        },
        _dock: function () {

            var thiz = this;

            var pe = thiz.getPageEditor();

            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getDesignPane();
            var propertyPane = pe.getPropertyPane();


            docker.movePanel(propertyPane, types.DOCKER.DOCK.RIGHT, designPane, {
                tabOrientation: types.DOCKER.TAB.TOP,
                location: types.DOCKER.TAB.RIGHT
            });
            propertyPane.maxSize(400);
            propertyPane.minSize(350);

            docker.movePanel(sourcePane, types.DOCKER.DOCK.BOTTOM, null, {
                tabOrientation: types.DOCKER.TAB.TOP
            });
            docker.movePanel(pe.getBlocksPane(), types.DOCKER.DOCK.STACKED, sourcePane, {});
            docker.movePanel(this.cssPane, types.DOCKER.DOCK.STACKED, sourcePane, {});

            pe.resize();
            docker.resize();
            setTimeout(function () {
                sourcePane.getSplitter().pos(0.5);
                propertyPane.maxSize(500);
                designPane.getSplitter().pos(0.8);
                thiz.cssPane.select();
            }, 1500);
        },
        getMaqettaActions: function () {
            return this._maqActions;
        },
        getMaqettaAction: function (id, items, recursive) {

            items = items || this.getMaqettaActions();

            var action = _.find(items, {
                id: id
            });

            if (!action) {
                action = _.find(items, {
                    command: id
                });
            }

            if (action || (recursive === false && action)) {
                return action;
            }

            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.menu) {
                    action = this.getMaqettaAction(id, item.menu, false);
                    if (action) {
                        return action;
                    }
                }
            }
            return action;
        },
        showInOutline: function (widget) {
            if (!widget) {
                return;
            }
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];


            widget = widget || firstItem;
            var outline = this.outlineGridView;
            outline.grid.select(widget.id, null, true, {
                focus: true,
                append: false,
                expand: true,
                delay: 1
            });
        },
        showCode: true,
        toogleCode: function () {
            if (this.showCode && this.getPageEditor()) {
                this.getPageEditor()._srcCP.getSplitter().collapse();
            } else {
                this.getPageEditor()._srcCP.getSplitter().expand();
            }
            this.showCode = !this.showCode;
            this.saveSettings();
        },
        _getIconClass: function (iconClassIn) {
            if (!iconClassIn) {
                return '';
            }

            switch (iconClassIn) {

                case "editActionIcon undoIcon":
                    {
                        return 'text-info fa-undo';
                    }
                case "editActionIcon redoIcon":
                    {
                        return 'text-info fa-repeat';
                    }

                case "editActionIcon editCutIcon":
                    {
                        return 'text-warning fa-scissors';
                    }
                case "editActionIcon editCopyIcon":
                    {
                        return 'text-warning fa-copy';
                    }
                case "editActionIcon editPasteIcon":
                    {
                        return 'text-warning fa-paste';
                    }
                case "editActionIcon editDeleteIcon":
                    {
                        return 'text-danger fa-remove';
                    }
            }
            return iconClassIn;
        },
        _toCommand: function (action) {
            switch (action) {
                case 'Undo':
                    return 'Edit/Undo';
                case 'Redo':
                    return 'Edit/Redo';
                case 'Cut':
                    return 'Edit/Cut';
                case 'Copy':
                    return 'Edit/Copy';
                case 'Paste':
                    return 'Edit/Paste';
                case 'Delete':
                    return 'Edit/Delete';
            }
        },
        __addAction: function (action) {
            switch (action.id) {
                case "openBrowser":
                    {
                        return false;
                    }
                case "documentSettings":
                    {
                        return false;
                    }
                case "stickynote":
                    {
                        return false;
                    }

                case "tableCommands":
                    {
                        return false;
                    }
            }
            return true;
        },
        _createEditToggleAction: function () {
            var thiz = this;
            var _toggle = Action.createDefault('Design', 'fa-toggle-on', 'View/Design Mode', 'viewActions', null, {
                widgetClass: 'xide/widgets/ToggleButton',
                widgetArgs: {
                    icon1: 'fa-toggle-on',
                    icon2: 'fa-toggle-off',
                    delegate: this,
                    checked: thiz._designMode
                },
                handler: function (command, item, owner, button) {
                    thiz._designMode = !thiz._designMode;
                    button.set('checked', thiz._designMode);
                    thiz.setDesignMode(thiz._designMode);

                }
            }).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null).
            setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                label: '',
                permanent: function () {
                    return thiz.destroyed == false;
                },
                widgetArgs: {
                    style: 'text-align:right;float:right;font-size:120%;'
                }
            }).
            setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {
                show: false
            });

            this._designToggleAction = _toggle;

            return _toggle;
        },
        getFlowActions: function () {
            var result = [],
                self = this,

                root = 'View/Layout Mode';

            var defaultMixin = {
                addPermission: true,
                quick: true
            }
            result.push(this.createAction({
                label: 'Flow',
                command: root,
                icon: 'fa-crosshairs',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Relative',
                command: root + '/Relative',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Absolute',
                command: root + '/Absolute',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));
            return result;
        },
        getFloatActions: function () {
            var result = [];
            result.push(this.createAction({
                label: 'Float',
                command: 'View/Show/Float',
                icon: 'fa-compress',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            result.push(this.createAction({
                label: 'Dock',
                command: 'View/Show/Dock',
                icon: 'fa-expand',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            result.push(this.createAction({
                label: 'View/Hide Code',
                command: 'View/Show/Code',
                icon: 'fa-code',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            return result;
        },
        getEditToggleActions: function () {
            var thiz = this;

            var result = [],
                self = this,

                root = 'View/Show';

            var defaultMixin = {
                addPermission: true,
                quick: true
            }
            /*
            result.push(this.createAction({
                label: 'Mode',
                command: root,
                icon: 'fa-eye',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));*/

            result.push(this.createAction({
                label: 'Edit',
                command: root + '/Design',
                icon: 'fa-edit',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Preview',
                command: root + '/Preview',
                icon: 'fa-eye',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));
            /*
                        result.push(this.createAction({
                            label: 'Preview',
                            command: 'File' + '/Preview',
                            icon: 'fa-eye',
                            tab: 'Home',
                            group: 'View',
                            mixin: defaultMixin
                        }));*/

            /*

             var _toggle = Action.createDefault('Design', 'fa-toggle-on', 'View/Design Mode', 'viewActions', null, {
             widgetClass: 'xide/widgets/ToggleButton',
             widgetArgs: {
             icon1: 'fa-toggle-on',
             icon2: 'fa-toggle-off',
             delegate: this,
             checked: thiz._designMode
             },
             handler: function (command, item, owner, button) {
             thiz._designMode = !thiz._designMode;
             button.set('checked', thiz._designMode);
             thiz.setDesignMode(thiz._designMode);

             }
             }).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null).
             setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
             label: '',
             permanent:function(){
             return thiz.destroyed==false;
             },
             widgetArgs:{
             style:'text-align:right;float:right;font-size:120%;'
             }
             }).
             setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {show: false});

             this._designToggleAction = _toggle;
             */

            return result;
        },
        _toAction: function (action) {
            //var _default = Action.create(action.label, this._getIconClass(action.iconClass), this._toCommand(action.label), false, null, types.ITEM_TYPE.WIDGET, 'editActions', null, false);
            var cmd = action.command || this._toCommand(action.label) || action.id;
            if (!cmd) {
                return null;
            }
            var self = this;
            var _default = this.createAction({
                label: action.label,
                group: action.group || 'Ungrouped',
                icon: this._getIconClass(action.iconClass) || action.iconClass,
                command: cmd,
                tab: action.tab || 'Home',
                mixin: {
                    action: action,
                    addPermission: true,
                    quick: true,
                    actionId: action.id,
                    localize: false
                },
                shouldDisable: function () {
                    var ctx = self.getEditorContext();
                    if (ctx && action.action && action.action.isEnabled) {
                        return !action.action.isEnabled(ctx);
                    }
                    return false;
                }
            });

            switch (action.id) {
                case "documentSettings":
                    {
                        return null;
                    }
                case 'outline':
                    {
                        _default.group = 'Widget';
                        return _default;
                    }
                case 'undo':
                case 'redo':
                case 'cut':
                case 'copy':
                case 'paste':
                    {
                        _default.group = 'Edit';
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {show: false});
                        return _default;
                    }
                case 'delete':
                    {
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''});
                        _default.group = 'Organize';
                        return _default;

                    }
                case 'theme':
                case 'rotateDevice':
                case 'chooseDevice':
                case 'stickynote':
                case 'savecombo':
                case 'showWidgetsPalette':
                case 'layout':
                case 'sourcecombo':
                case 'design':
                case 'closeactiveeditor':
                case 'tableCommands':
                    {
                        return null;
                    }
            }
            debug &&  0 && console.log('unknown action : ' + action.id, action);
            //_default.command=action.id;
            return _default;
        },
        getWidgetLayoutActions: function (actions) {
            var thiz = this;
            var result = [];

            function add(label, icon, command, handler) {
                result.push(thiz.createAction({
                    label: label,
                    icon: icon,
                    group: 'Widget',
                    tab: 'Home',
                    command: 'Widget/Align/' + command,
                    mixin: {
                        addPermission: true,
                        quick: true
                    }

                }));
            }

            var defaultMixin = {
                addPermission: true,
                quick: true,
                localize: false
            };

            result.push(this.createAction({
                label: "Align",
                command: "Widget/Align",
                icon: 'fa-arrows',
                group: 'Widget',
                tab: 'Home',
                mixin: defaultMixin,
                shouldDisable: function () {

                    var ctx = thiz.getEditorContext();
                    if (!ctx) {
                        return true;
                    }
                    var selection = thiz.getSelection();
                    return selection.length == 0 || selection.length < 2;
                }
            }));

            add('Align Left', 'layoutIcon-alignLeftVertical', 'Align Left', thiz._alignLeftVertical);
            add('Align Center', 'layoutIcon-alignCenterVertical', 'Align Center', thiz._alignCenterVertical);
            add('Align Right', 'layoutIcon-alignRightVertical', 'Align Right', thiz._alignRightVertical);
            add('Align Top', 'layoutIcon-alignTopHorizontal', 'Align Top', thiz._alignTopHorizontal);
            add('Align Bottom', 'layoutIcon-alignBottomHorizontal', 'Align Bottom', thiz._alignBottomHorizontal);
            add('Same Size', 'layoutIcon-sameSize', 'Same Size', thiz._sameSize);
            add('Same Width', 'layoutIcon-sameWidth', 'Same Width', thiz._sameWidth);
            add('Same Height', 'layoutIcon-sameHeight', 'Same Height', thiz._sameHeight);
            return result;
        },
        /**
         * _getViewActions extracts a xide compatible actions from maqetta's own action impl.
         * @returns {Array}
         * @private
         */
        _getViewActions: function () {
            var editorID = 'davinci.ve.HTMLPageEditor'; //this.editorExtension.id;
            var editorActions = [];
            var extensions = Runtime.getExtensions('davinci.editorActions', function (ext) {
                if (editorID == ext.editorContribution.targetID) {
                    editorActions.push(ext.editorContribution);
                    return true;
                }
            });
            if (editorActions.length == 0) {
                var extensions = Runtime.getExtension('davinci.defaultEditorActions', function (ext) {
                    editorActions.push(ext.editorContribution);
                    return true;
                });
            }
            var libraryActions = Metadata.getLibraryActions('davinci.editorActions', editorID);
            // Clone editorActions, otherwise, library actions repeatedly get appended to original plugin object
            editorActions = dojo.clone(editorActions);
            if (editorActions.length > 0 && libraryActions.length) {
                // We want to augment the action list, so let's clone the
                // action set before pushing new items onto the end of the
                // array
                dojo.forEach(libraryActions, function (libraryAction) {
                    var Workbench = require("davinci/Workbench");
                    if (libraryAction.action) {
                        Workbench._loadActionClass(libraryAction);
                    }
                    if (libraryAction.menu) {
                        for (var i = 0; i < libraryAction.menu.length; i++) {
                            var subAction = libraryAction.menu[0];
                            if (subAction.action) {
                                Workbench._loadActionClass(subAction);
                            }
                        }
                    }
                    editorActions[0].actions.push(libraryAction);
                });
            }
            return editorActions;
        },
    }
    
    const Module = dcl(ActionProvider.dcl, Impl);
    Module.GridClass = declare('Actions', [ActionProvider], Impl);
    Module.Impl = Impl;
    return Module;
});