/** @module xideve/views/VisualEditorSourceEditor **/
define("xideve/views/VisualEditorSourceEditor", [
    "dcl/dcl",
    'xace/views/Editor',
    'xide/utils',
    "davinci/model/Factory"
], function (dcl,Editor, utils, Factory) {
    /**
     *
     * @mixin module:xideve/views/VisualEditorSourceEditor
     * @lends module:xideve/views/VisualEditor
     */
    return dcl(null, {
        declaredClass:"xideve/views/VisualEditorSourceEditor",
        createHTMLEditor:function(dstNode,fileName,item){
            var thiz = this;
            var editor = utils.addWidget(Editor, {
                __permissions:[],
                options:{
                    filePath: item.path
                },
                config: thiz.config,
                delegate: thiz,
                owner: thiz,
                parentContainer: dstNode,
                ctx: thiz.ctx,
                style: 'padding:0px;',
                item: item,
                resizeToParent:true,
                model: Factory.newHTML({url: fileName}),
                //registerView:false,
                selectModel: function (model) {
                    // 0 && console.log('select model:');
                    /*
                    if(model && model.length && model[0]['model'] && model[0]['model'].getText) {
                         0 && console.log('select model:  ' +model[0].model.getText(), model);
                    }*/
                },
                setVisible: function () {

                },//dummy
                setContent: function (filename, content) {
                    this.model.ctx = thiz.ctx;
                    this.model.fileName = filename;
                    this.model.setText(content);
                },
                store: null,
                /***
                 * Provide a text editor store delegate
                 */
                storeDelegate: {
                    getContent: function (onSuccess) {
                        thiz.ctx.getFileManager().getContent(item.mount, item.path, onSuccess);
                    },
                    saveContent: function (value, onSuccess, onError) {
                        thiz.ctx.getFileManager().setContent(item.mount, item.path, value, onSuccess);
                    }
                },
                title: 'notitle',
                closable: true
            }, this, dstNode, true);

            thiz.textEditor = editor;
            dstNode.add(editor);
            return editor;
        }
    });
});