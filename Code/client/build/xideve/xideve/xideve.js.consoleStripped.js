require({cache:{
'xideve/types':function(){
/** @module xide/types
 *  @description All the package's constants and enums in C style structures.
*/
 define([
     'xide/types',
     'xide/utils'
], function (types,utils) {
     utils.mixin(types,{
         VE_LAYOUT_RIGHT_CENTER_BOTTOM:'VE_LAYOUT_RIGHT_CENTER_BOTTOM',
         VE_LAYOUT_CENTER_BOTTOM:'VE_LAYOUT_CENTER_BOTTOM',
         VE_LAYOUT_CENTER_RIGHT:'VE_LAYOUT_CENTER_RIGHT',
         VE_LAYOUT_LEFT_CENTER_RIGHT:'VE_LAYOUT_LEFT_CENTER_RIGHT',
         VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM:'VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM'
     });

     /***
      * Extend core event keys for widget related events
      *
      */
     utils.mixin(types.EVENTS,{
         WIDGET_PROPERTIES_RENDERED: 'OnWidgetPropertiesRendered',
         WIDGET_PROPERTY_RENDERED: 'onWidgetPropertyRendered',
         ON_SET_WIDGET_PROPERTY_ACTIONS: 'onSetWidgetPropertyActions',
         ON_GET_LOADER_PACKAGES: 'onGetLoaderPackages',
         ON_VISUAL_EDITOR_SAVE: 'onVisualEditorSave',
         ON_BUILD_HEADER: 'onBuildHeader',
         ON_BUILD_DOJO_CONFIG: 'onBuildDojoConfig',
         ON_SET_DOJO_URL: 'onSetDojoUrl',
         ON_SET_SOURCE_DATA: 'onSetSourceData',
         ON_EVENT_SELECTION_RENDERED: 'onEventSelectionRendered',
         ON_WIDGET_PROPERTY_CHANGED: 'onWidgetPropertyChanged',
         ON_LIBRARY_CONTAINER_CREATED:'onLibraryContainerCreated',
         ON_CONTEXT_READY:'onContextReady',
         ON_CONTEXT_DESTROYED:'onContextDestroyed',
         ON_BUILD_WIDGET_BLOCK_LIBRARY:'onBuildWidgetBlockLibrary',
         ON_SHOW_BLOCK_SMART_INPUT:'onShowBlockSmartInput',
         ON_WIDGET_CHANGED:'onWidgetChanged'
     });
     return types;
});
},
'xideve/views/VisualEditor':function(){
/** @module xideve/views/VisualEditor **/
define([
    'dcl/dcl',
    "dojo/dom-construct",
    "dojo/Deferred",
    'dojo/promise/all',
    "require",
    'xide/layout/ContentPane',
    'xide/layout/_Accordion',
    'xide/utils',
    'xide/types',
    "davinci/ve/PageEditor",
    "davinci/Runtime",
    "davinci/Workbench",
    "dojo/_base/connect",
    "dojo/dom-class",
    "davinci/ve/metadata",
    'davinci/ve/views/SwitchingStyleView',
    'xideve/views/OutlineView',
    'xideve/views/OutlineGridView',
    'xide/registry',
    'xide/layout/_TabContainer',
    'xideve/views/VisualEditorSourceEditor',
    'xideve/views/VisualEditorLayout',
    'xideve/views/VisualEditorAction',
    'xideve/views/VisualEditorTools',
    'xide/editor/Default',
    'xide/_base/_Widget',
    'xide/mixins/ReloadMixin',
    "xblox/model/html/SetStyle",
    "xblox/model/html/SetCSS",
    "xblox/model/html/SetState",
    "wcDocker/splitter",
    "xideve/palette/Palette",

    'xdojo/declare',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'dstore/Trackable',
    'xide/data/TreeMemory',
    'xaction/DefaultActions',
    "xide/form/Checkbox",
    "dgrid/Editor",
    'xide/factory',
    "davinci/ve/tools/CreateTool",
    "xide/views/SimpleCIDialog",
    "xide/widgets/TemplatedWidgetBase"
], function (dcl, domConstruct, Deferred, all, require,
    ContentPane, AccordionContainer, utils, types, PageEditor,
    Runtime, Workbench, connect, domClass,
    Metadata, SwitchingStyleView, OutlineView, OutlineGridView, registry,
    _TabContainer,
    // VisualEditorPalette, 
    VisualEditorSourceEditor, VisualEditorLayout,
    VisualEditorAction, VisualEditorTools, Default, _Widget, ReloadMixin,
    SetStyle, SetCSS, SetState, splitter, Palette2,
    declare, Grid, TreeRenderer, KeyboardNavigation, __Widget, Trackable, TreeMemory, DefaultActions,
    Checkbox, Editor, factory,
    CreateTool, SimpleCIDialog, TemplatedWidgetBase
) {

    var debugFileChanges = false;
    var debugBlocks = false;
    var debugCSSEditor = false;

    console.clear();
    if (window.vee) {
        vee.onReload();
    }

    const updateWidget = (widget, editor, props) => {
        const widgetClass = dcl(TemplatedWidgetBase, {
            templateString: '<div class="CIActionWidget"></div>',
            widgetClass: ''
        });

        const ctx = editor.getEditorContext();
        const command = new davinci.ve.commands.ModifyCommand(widget, props, null);
        dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
            source: ctx.editor_id,
            command: command
        }]);
    }

    const editWidget = (widget, editor, done) => {
        const node = widget.domNode;
        const ctx = editor.getEditorContext();
        const cis = [
            utils.createCI('Name', types.ECIType.STRING, node.name, {
                group: 'General'
            }),
            utils.createCI('Style', types.ECIType.DOM_PROPERTIES, '', {
                group: 'Style',
                widget: {
                    open: true,
                    widget: widget,
                    value: $(node).attr('style'),
                    ctx: editor.ctx
                }
            })
        ]
        const nameDlg = SimpleCIDialog.create('Edit widget', cis, (changed) => {
            const ctrToolData = {
                context: ctx,
                type: 'xblox/StyleState',
                properties: {
                    name: cis[0].value || 'noname',
                    style: cis[1].value
                },
                userData: {}
            }
            updateWidget(widget, editor, {
                name: cis[0].value || 'noname',
                style: cis[1].value
            });
            done();
        });
        nameDlg.show();
    }
    window.editWidget = editWidget;
    // --- GRID
    const gridClass = Grid.createGridClass('StatesView', {
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Instance': 3,
                'Window': 2
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            getRootFilter: function () {
                return {
                    parentId: '.'
                };
            },
            postMixInProperties: function () {
                // this.collection = this.collection.filter(this.getRootFilter());
                this.columns = [{

                        label: "Visibile",
                        field: "visible",
                        sortable: false,
                        get: function (item) {
                            return item.widget.isActive(item.state.name);
                        },
                        editorArgs: {
                            title: ""
                        },
                        width: "30px",
                        editor: Checkbox
                    },
                    {
                        renderExpando: true,
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            },
            runAction: function (action) {
                const selection = this.getSelection();

                // 0 && console.log('run ac ' + action.command, selection);

                const _createStyleState = (widget, editor) => {
                    const ctx = editor.getEditorContext();
                    const cis = [
                        utils.createCI('Name', types.ECIType.STRING, 'noname', {
                            group: 'General'
                        }),
                        utils.createCI('Style', types.ECIType.DOM_PROPERTIES, '', {
                            group: 'Style',
                            widget: {
                                open: true
                            }
                        })
                    ]
                    const nameDlg = SimpleCIDialog.create('Create New Style State', cis, (changed) => {
                        const ctrToolData = {
                            context: ctx,
                            type: 'xblox/StyleState',
                            properties: {
                                name: cis[0].value || 'noname',
                                style: cis[1].value
                            },
                            userData: {}
                        }
                        const meta = Metadata.getMetadata(ctrToolData.type);
                        Metadata.getHelper(widget.type, 'tool').then(function (ToolCtor) {
                            // Copy the data in case something modifies it downstream -- what types can data.data be?
                            const tool = new(ToolCtor || CreateTool)(ctrToolData, {});
                            tool._context = ctx;
                            ctx.setActiveTool(tool);
                            tool._create({
                                position: {
                                    x: 0,
                                    y: 0
                                },
                                parent: widget,
                                userData: meta
                            });
                            ctx.setActiveTool(null);
                        }.bind(this));
                    });
                    nameDlg.show();

                }
                const editor = this.editor;
                const context = editor.getEditorContext();
                const _removeStyleState = (widget, editor, widgetStateNode) => {
                    const node = widget;
                    let stateWidget = context.toWidget(widgetStateNode);
                    node.removeState(widgetStateNode);
                    context.select(stateWidget, null, null, true);
                    editor.runAction('Edit/Delete');
                    context.select(context.toWidget(widget));
                }
                const _saveToLibrary = (state) => {
                    const cis = [
                        utils.createCI('Name', types.ECIType.STRING, state.name, {
                            group: 'General'
                        })
                    ]
                    const nameDlg = SimpleCIDialog.create('Edit widget', cis, (changed) => {
                        const libMgr = editor.ctx.getLibraryManager();
                        const style = {
                            "type": "xblox/StyleState",
                            "properties": {
                                "style": $(state).attr('style')
                            }
                        }
                        libMgr.setSetting(cis[0].value || state.name, style);
                    });
                    nameDlg.show();                    
                }
                switch (action.command) {
                    case 'File/Edit':
                        {
                            if (!selection.length) {
                                return;
                            }
                            return window.editWidget(context.toWidget(selection[0].state), editor, function () {
                                context.select(context.toWidget(selection[0].widget));
                            });
                        }
                    case 'File/Delete':
                        {
                            if (!selection.length) {
                                return;
                            }
                            return _removeStyleState(selection[0].widget, this.editor, selection[0].state);
                        }
                    case 'New/State':
                        {
                            return _createStyleState(this.editor.getSelection()[0], this.editor);
                        }
                    case 'File/Save':
                        {
                            if (!selection.length) {
                                return;
                            }
                            return _saveToLibrary(selection[0].state);
                        }
                }
                return this.inherited(arguments);
            },
            startup: function () {
                var defaultMixin = {
                    addPermission: true,
                    quick: true
                };
                const actions = [];
                const libMgr = this.editor.ctx.getLibraryManager();
                const store = libMgr.getStore();
                store.on('update', () => {
                    this.refresh();
                });

                actions.push(this.createAction({
                    label: 'New State',
                    command: 'New/State',
                    icon: 'fa-magic',
                    tab: 'Home',
                    group: 'View',
                    mixin: defaultMixin
                }));
                actions.push(this.createAction({
                    label: 'Save to Palette',
                    command: 'File/Save',
                    icon: 'fa-save',
                    tab: 'Home',
                    group: 'View',
                    mixin: defaultMixin
                }));
                this.addActions(actions);
                const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
                this.addActions(_defaultActions);
                this.inherited(arguments);
                this._showHeader(false);

                const updateState = (widget, value) => {
                    const ctx = this.editor.getEditorContext();
                    const valuesObject = {};
                    valuesObject['state'] = value;
                    const command = new davinci.ve.commands.ModifyCommand(widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: ctx.editor_id,
                        command: command
                    }]);
                    /*
                    let widgetStateNode = widget.domNode.getState(value);
                    if (widgetStateNode) {
                        let stateWidget = ctx.toWidget(widgetStateNode);
                        if (stateWidget) {
                            ctx.select(stateWidget);
                        }
                    }
                    */
                }

                this.on('dgrid-datachange', (evt) => {
                    var row = this.row(evt.target);
                    var value = evt.value;
                    const state = row.data.state;
                    if (!this.widgetSelection.length) {
                        return;
                    }
                    const widget = this.widgetSelection[0];

                    const widgetNode = widget.domNode;
                    if (!value) {
                        updateState(widget, widgetNode.disableState(state));
                    } else {
                        updateState(widget, widgetNode.enableState(state));
                    }
                });
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            _WIDGET: {
                CLASS: __Widget
            },
            EDITOR: {
                CLASS: Editor
            }
        }, {
            RENDERER: TreeRenderer
        },
        null,
        null);

    const ACTION = types.ACTION;
    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        // ACTION.CLIPBOARD,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.SEARCH,
        ACTION.CONTEXT_MENU
    ];

    const createStatesStore = () => {
        const storeClass = declare('paletteStore', [TreeMemory, Trackable], {});
        const data = [];
        return new storeClass({
            data: data,
            idProperty: 'id',
            parentProperty: 'parentId',
            id: utils.createUUID(),
            ctx: sctx,
            mayHaveChildren: function () {
                return false;
            },
            /**
             * Return the root item, is actually private
             * @TODO: root item unclear
             * @returns {{path: string, name: string, mount: *, directory: boolean, virtual: boolean, _S: (xfile|data|FileStore), getPath: Function}}
             */
            getRootItem: function () {
                return {
                    _EX: true,
                    id: '.',
                    name: '.',
                    group: true,
                    virtual: true,
                    _S: this,
                    getPath: function () {
                        return this.id;
                    }
                };
            },
            getParent: function (mixed) {
                if (!mixed) {
                    return null;
                }
                var item = mixed,
                    result = null;

                if (_.isString(item)) {
                    item = this.getSync(mixed);
                }

                if (item && item.parentId) {
                    result = this.getSync(item.parentId);
                }
                return result || this.getRootItem();
            }
        });
    }

    const createStatesView = (editor) => {

        const pe = editor.getPageEditor();
        const target = pe.statesTab;
        let view = editor.statesView;

        const comptype = 'delite';
        const store = editor.statesStore || createStatesStore();
        editor.statesStore = store;

        if (view) {
            utils.destroy(view, true);
            utils.destroy(view.template);
            $(view.template).remove();
            editor.statesView = null;
        }
        view = utils.addWidget(gridClass, {
            ctx: sctx,
            open: true,
            expandOnClick: true,
            _parent: target,
            collection: store,
            permissions: DefaultPermissions,
            editor: editor,
            widgetSelection: []
        }, null, target, true);

        editor.statesView = view;
        view.set('collection', store.filter({
            parentId: '.'
        }));
        view.refresh();
        view.resize();
        view.showToolbar(true);
        target.add(view);
    }

    window.createStatesView = createStatesView;
    const clearStatesStore = (editor) => {
        editor.statesStore.setData([]);
    }
    const refreshStatesView = (editor, selection) => {
        editor.statesView.set('collection', editor.statesStore.filter({
            parentId: '.'
        }))
        editor.statesView.widgetSelection = selection;
        editor.statesView.editor = editor;
    }
    const updateStatesView = (editor) => {

        const selectables = editor.getStatableWidgetsSelection();
        clearStatesStore(editor);

        if (!selectables.length) {
            refreshStatesView(editor, selectables);
            return;
        }

        const widget = selectables[0].domNode;

        const state = (_state, widget) => {
            return {
                id: _state.name,
                name: _state.name,
                parentId: '.',
                state: _state,
                widget: widget
            }
        };


        const states = widget.getStates();
        states.forEach((wState) => {
            editor.statesStore.putSync(state(wState, widget));
        });
        refreshStatesView(editor, selectables);
    }
    window.updateStatesView = updateStatesView;

    //--------------------------------------
    function addPreviewToggle() {

        const pe = this.getPageEditor();
        const design = pe.getDesignPane();
        const buttonBar = design.getFrame().$buttonBar;
        utils.destroy(window._cb);
        const cb = utils.addWidget(Checkbox, {
            title: 'Preview',
            userData: {
                value: false
            }
        }, null, buttonBar[0], true);

        $(cb.domNode).css('float', 'right');
        const node = $(cb.domNode).find('.checkbox');
        node.css('margin-top', '3px');
        node.css('margin-right', '10px');
        node.css('margin-bottom', '0');

        cb._on('change', (val) => {
            if (val) {
                this.runAction('View/Show/Preview');
            } else {
                this.runAction('View/Show/Design');
            }
        });
        this.add(cb);
        this._designToggle = cb;
        window._cb = cb;
    }

    if (window.vee) {
        addPreviewToggle.call(window.vee);
    }

    /**
     * @class module:xideve/views/VisualEditor
     * @augments module:xideve/views/VisualEditorPalette
     * @augments module:xideve/views/VisualEditorSourceEditor
     * @augments module:xideve/views/VisualEditorTools
     * @augments module:xideve/views/VisualEditorLayout
     * @augments module:xideve/views/VisualEditorAction
     * @extends module:xide/_base/_Widget
     */
    var Module = dcl([_Widget,
        VisualEditorSourceEditor,
        VisualEditorLayout,
        VisualEditorAction,
        VisualEditorTools,
        ReloadMixin.dcl
    ], {
        updateFrame: function () {
            var ctx = this.getEditorContext();
            if (ctx && ctx.isResizing) {
                return;
            }

            var pageEditor = this.editor,
                frame = this.getFrame(),
                self = this;

            function follow(what, target) {

                var frame = $(what);
                var container = $(target);
                var pos = container.offset();
                var top = pos.top;
                var left = pos.left;
                var context = self.getEditorContext();

                var rootWidget = context ? $(context.rootNode) : null;
                var bodyElement = context ? $(context.getDocumentElement().getChildElement("body")) : null;
                var width = container.css('width');
                var height = container.css('height');

                var sizeBodyH = false;
                var sizeBodyW = false;

                if (bodyElement) {
                    var maxWidth = bodyElement.inlineStyle('max-width');
                    if (maxWidth && maxWidth !== 'none') {
                        //  width = maxWidth;
                    }
                    var maxHeight = bodyElement.inlineStyle('max-height');
                    if (maxHeight && maxHeight !== 'none') {
                        // height = maxHeight;
                    }
                    //var _width2 = bodyElement).inlineStyle('width');
                    var _width = bodyElement.inlineStyle('width');
                    if (_width) {
                        //  width = maxWidth;
                        sizeBodyW = _width;
                    }
                    var _height = bodyElement.inlineStyle('height');
                    if (_height) {
                        // height = maxHeight;
                        sizeBodyH = _height;
                    }

                }

                $(frame).css('display', (self._hidden ? 'none' : 'block'));


                if (top == 0 && left == 0) {
                    return;
                }
                frame.css('top', top);
                frame.css('left', left);
                if (width !== 'none') {
                    frame.css('width', width);
                    frame.css('height', height);
                    var body = $(self.getEditorContext().rootNode);
                    frame.css('width', width);
                    frame.css('height', height);

                    sizeBodyH && body.css('height', sizeBodyH);
                    sizeBodyW && body.css('width', sizeBodyW);
                    if (!sizeBodyH && !sizeBodyW) {
                        body.css('width', width);
                        body.css('height', height);
                    }

                }
            }

            if (frame) {
                if (!frame._followTimer) {
                    frame._followTimer = setInterval(function () {
                        follow(frame, pageEditor._designCP.containerNode);
                    }, 500);
                }
            }

        },
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Widget': 80,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Step': 5,
            'New': 4,
            'Window': 1
        },
        declaredClass: "xideve/views/VisualEditor",
        templateString: "<div style='height: 100%;width: 100%' attachTo='containerNode'></div>",
        isVisualEditor: true,
        resizeToParent: true,
        showLESS: true,
        showBehaviours: false,
        showStyleView: true,
        blockScopes: null,
        onReload: function () {
            window.createStatesView(this);
            const pe = this.getPageEditor();
            const design = pe.getDesignPane();
            const buttonBar = design.getFrame().$buttonBar;
        },
        /**
         * leftLayoutContainer is passed by xfile::PanelManager::createEditor.
         * its a stacked container.
         */
        leftLayoutContainer: null,
        /**
         * rightLayoutContainer is passed by xfile::PanelManager::createEditor.
         * its a stacked container.
         */
        rightLayoutContainer: null,

        /** Palette */
        palette: null,
        paletteContainer: null,
        showPalette: true,

        /** Style */
        styleView: null,
        styleViewContainer: null,

        /** Outline **/
        addOutline: true, //enable outline view

        outlineView: null,
        outlineContainer: null,

        /** Outline Grid View **/
        outlineGridView: null,
        outlineGridContainer: null,
        /**
         * Instance to the actual visual editor
         *
         * @type {module:davinci/ve/PageEditor}
         * */
        editor: null,
        _designMode: true,
        _tempLeftContainer: null,
        textEditor: null,

        /**
         * is editor for a deliteful scene
         */
        delite: false,

        /**
         * @type {xideve/Template}
         */
        template: null,

        contextInfo: null,
        didAddSheetEditors: false,
        beanType: types.ITEM_TYPE.WIDGET,

        _didInitial: false,
        ready: false,
        destroyed: false,
        _skipTextEditorChange: false,
        _getHTMLBlocks: function (scope, owner, target, group, items) {
            items.push({
                name: 'HTML',
                iconClass: 'fa-paint-brush',
                items: [{
                        name: 'Set Style',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetStyle,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    },
                    {
                        name: 'Set CSS',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetCSS,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    },
                    {
                        name: 'Set State',
                        owner: owner,
                        iconClass: 'fa-paint-brush',
                        proto: SetState,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group
                        }
                    }
                ]
            });
            return items;
        },
        onBuildBlockInfoListEnd: function (evt) {
            var owner = evt.owner,
                ve = owner ? owner.visualEditor : null;
            //if (!ve || ve !== this) {
            //   return;
            //}
            this._getHTMLBlocks(evt.scope, evt.view, evt.item, evt.group, evt.items);
        },
        __reloadModule: function () {
            Reload.reload().then(function (data) {
                var component = data.component;
                component.registerEditors();
            });
        },
        updateFocus: function () {
            var ctx = this.getEditorContext();
            if (ctx) {
                if (ctx.isRezing) {
                    return;
                }
                ctx.clearCachedWidgetBounds();
                ctx.updateFocusAll();
            }
        },
        set: function (key, value) {
            if (key === 'focused') {
                this.updateFocus();
            }
            return this.inherited(arguments);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Layout
        //
        /////////////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Selection
        //
        /////////////////////////////////////////////////////////////////////////////////////
        select: function (mixed, append) {
            mixed = _.isArray(mixed) ? mixed : [mixed];
            var first = mixed[0];
            if (!first) {
                return;
            }
            var context = this.getEditorContext();
            if (!context) {
                return;
            }
            if (_.isString(first)) {
                var _resolved = [];
                _.each(mixed, function (str) {
                    var widget = context.getWidget(str);
                    widget && _resolved.push(widget);
                });
                mixed = _resolved;
            }
            if (append !== true) {
                context.deselect();
            }
            _.each(mixed, function (widget) {
                context.select(widget, true, false);
            });
        },
        selectAll: function () {
            var context = this.getEditorContext(),
                widgets = context.getAllWidgets();
            _.each(widgets, function (widget) {
                context.select(widget, true, false);
            });
        },
        updateToolbars: function () {},
        getTemplateDirectoryPathItem: function () {
            return {
                mount: 'workspace',
                path: './templates'
            }
        },
        getSelection: function () {
            var _ctx = this.getEditorContext();
            var out = [];
            if (_ctx) {
                var selection = _ctx.getSelection();
                for (var i = 0; i < selection.length; i++) {
                    out.push(selection[i]);

                }
            }
            return out;
        },
        ensureSceneFiles: function (item) {
            var dfd = new Deferred();
            var chain = [];
            var store = item._S;
            var mount = item.mount;
            var fileExtension = utils.getFileExtension(item.path);
            var fileManager = this.ctx.getFileManager();

            var templateDirectoryItem = this.getTemplateDirectoryPathItem();

            var parentDirectoryItem = store.getParent(item);
            var parentDirectoryItemPath = parentDirectoryItem.getPath();

            var ensureItem = function (item, extension) {

                var itemDfd = new Deferred();

                var itemPath = item.getPath().replace('.' + fileExtension, '.' + extension);

                var item = store.getSync(itemPath);
                if (item) {
                    itemDfd.resolve(item);
                } else {

                    var newItemPath = './' + itemPath;

                    newItemPath = newItemPath.replace('././', './');
                    var newItemTemplatePath = templateDirectoryItem.path + '/newDHTML.' + extension;

                    //create the file
                    fileManager.mkfile(mount, newItemPath).then(function (result) {

                        //copy from template content over
                        fileManager.getContent(mount, newItemTemplatePath, function (content) {

                             0 && console.log('write from template to: ' + newItemPath);
                            fileManager.setContent(mount, newItemPath, content, function (content) {
                                itemDfd.resolve();
                            });
                        });
                    });
                }
                return itemDfd;
            };


            chain.push(ensureItem(item, 'css'));
            //chain.push(ensureItem(item,'less'));
            chain.push(ensureItem(item, 'xblox'));
            chain.push(ensureItem(item, 'js'));
            all(chain).then(function (results) {
                dfd.resolve();
            });
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Widget Instances
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Bean protocol implementation
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getActiveItem: function () {
            return null;
        },
        hasItemActions: function () {
            return this.ready;
        },
        getItem: function () {
            return this.currentItem;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        /////////////////////////////////////////////////////////////////////////////////////
        _createLeftContainer: function () {
            var main = this.getMainView();
            var pane = utils.addWidget(ContentPane, {
                closable: true,
                style: 'padding:0px;width:200px',
                region: 'left',
                cssClass: 'layoutLeft',
                splitter: true,
                parentContainer: main.layoutMain
            }, this, main.layoutMain, true, 'ui-widget-content');
            return pane;
        },
        getLeftContainer: function () {
            var main = this.getMainView();
            var container = main.leftLayoutContainer;
            if (container) {
                return container;
            }
            if (main.leftTabContainer) {
                return main.leftTabContainer;
            }

            this._tempLeftContainer = this._createLeftContainer();

            this.leftLayoutContainer = utils.addWidget(AccordionContainer, {
                persist: true,
                doLayout: true,
                cssClass: 'ui-widget-content'
            }, this, this._tempLeftContainer.containerNode, true);

            return this.leftLayoutContainer;

        },
        /**
         * This editor got active/visisble again, restore our panels in the main-view
         * @returns {*}
         */
        restoreStyleView: function () {

            return registry.byId('html_style_view_parent');
            if (!this.styleViewContainer) {
                return;
            }

            var styleView = registry.byId('html_style_view_parent');
            if (styleView) {
                var main = this.getMainView();
                /*if (styleView.isHidden) {*/

                var pe = this.getPageEditor();

                this.rightLayoutContainer = pe.getPropertyPane();
                utils.addChild(styleView, this.rightLayoutContainer);
                //this.rightLayoutContainer.addChild(styleView);


                styleView.isHidden = false;

                /*}*/
                return styleView;
            }
            return null;
        },
        /**
         * When user selects another bean, hide our panels instead of destroying them
         * @returns {*}
         */
        suspendStyleView: function () {
            return;
            if (!this.styleViewContainer) {
                return;
            }
            if (!this.styleViewContainer.isHidden) {
                try {
                    this.rightLayoutContainer.removeChild(this.styleViewContainer);
                } catch (e) {
                    console.error('error suspend style view  ' + e, e);
                }
                var widgetCache = dojo.byId('widgetCache');
                dojo.place(this.styleViewContainer.domNode, widgetCache);
                this.styleViewContainer.isHidden = true;
            }
        },
        /////////////////////////////////////////////////
        //
        //  Lifecycle & cleanup methods
        //
        onLastEditorRemoved: function () {
            // 0 && console.log('onLastEditorRemoved');
            this.suspendStyleView();

            //return;
            var stylePaneTC = registry.byId('html_style_view_parent');
            if (stylePaneTC && stylePaneTC.parentContainer) {
                stylePaneTC.parentContainer.removeChild(stylePaneTC);
                utils.destroy(stylePaneTC);
            }


            var stylePane = registry.byId('html_style_view');
            if (stylePane) {
                utils.destroy(stylePane);
            }

            var tc2 = registry.byId('palette-tabcontainer-right-top');
            if (tc2) {
                utils.destroy(tc2);
            }


        },
        clearAll: function () {
            //deselect all, otherwise focus-frame remains active
            if (this.editor) {
                this.editor.visualEditor.context.deselect();
            }
            Workbench._switchEditor(null);
            Workbench.currentEditor = null;
            Runtime.currentEditor = null;
            this.destroyed = true;
            if (this.blockScopes) {
                for (var i = 0; i < this.blockScopes.length; i++) {
                    var scope = this.blockScopes[i];
                    this.ctx.getBlockManager().removeScope(scope.id);
                }
                delete this.blockScopes;
            }
            this.outlineGridContainer = null;
            this.outlineGridView = null;
            this.onLastEditorRemoved();
        },
        getFrame: function () {
            var ctx = this.getEditorContext();
            if (ctx && ctx.frameNode) {
                return ctx.frameNode;
            }
            return null;
        },
        destroy: function () {

            var otherEditors = this.getEditors(),
                isLast = otherEditors.length == 0;

            var main = this.getMainView(),
                left = this.getLeftContainer(),
                ctx = this.getEditorContext();
            clearInterval(this._frameTimer);
            utils.destroy(this.getFrame());
            utils.destroy(this.statesView);
            var isEmbedded = this.leftLayoutContainer == null || (left.canDestroy && !left.canDestroy());
            if (!isEmbedded) {
                utils.destroy(this.leftLayoutContainer, false);
                main.layoutMain && main.layoutMain.removeChild && main.layoutMain.removeChild(this._tempLeftContainer);
                utils.destroy(this.paletteContainer, false);
            } else {
                utils.destroy(this.paletteContainer, false);
                //utils.destroy(this.outlineContainer,false);
                utils.destroy(this._cBPalettePane, false);
                //left.removeChild(this.outlineContainer);
                //this.outlineView.parentContainer = null;
                //utils.destroy(this.outlineView,true);
                try {
                    if (this.outlineGridContainer) {
                        left.removeChild(this.outlineGridContainer);
                        this.outlineGridView.parentContainer = null;
                    }
                    //this.outlineGridView.reset();
                    utils.destroy(this.outlineGridView, true);
                    //this.outlineView.destroy();
                    utils.destroy(this.palette);
                } catch (e) {
                    debugger;
                }
            }

            if (this && this.styleViewContainer) {
                this.styleViewContainer.keep = false;
            }

            utils.destroy(this.styleView, true, this);
            utils.destroy(this.tabContainer, true, this);
            utils.destroy(this.styleViewContainer, true, this);

            this.publish(types.EVENTS.ON_CONTEXT_DESTROYED, this.getEditorContext());
            //clear property panel for true
            //main.getRightTopTarget(true, false);

            /*
             var sview = registry.byId('html_style_view');
             if(sview){
             console.error('sview still alive!');
             }
             */

            /*
             try {
             //utils.destroy(this.styleViewContainer);
             //utils.destroy(this.styleView,true,this);
             }catch(e){
             debugger;
             }
             */

            try {
                this.clearAll();
            } catch (e) {
                logError(e);
            }
            if (isLast) {
                try {
                    utils.destroy(this.editor, true, this);
                } catch (e) {
                    logError(e);
                }
                try {
                    /*utils.destroy(this.leftLayoutContainer,true,this);
                     main.layoutMain.removeChild(this._tempLeftContainer);*/
                } catch (e) {
                    debugger;
                }
            }


            /*

             if (this.palette) {
             utils.destroy(this.palette);
             }

             if (this.leftLayoutContainer) {

             this.leftLayoutContainer.removeChild(this.paletteContainer);
             if (this.outlineView) {
             utils.destroy(this.outlineView);
             this.leftLayoutContainer.removeChild(this.outlineContainer);
             }
             }



             try {
             //remove the accordion
             if (this._tempLeftContainer) {
             utils.empty(this.leftLayoutContainer);
             main.layoutMain.removeChild(this._tempLeftContainer);
             }
             } catch (e) {
             console.error('fucked');
             }


             this.suspendStyleView();

             utils.destroy(this.editor, true, this);

             this._tempLeftContainer = null;
             this.outlineView = null;
             this.outlineContainer = null;
             this.palette = null;
             this.paletteContainer = null;
             */
            /*
             this.styleView =null;
             this.styleViewContainer = null;
             */
            //utils.destroy(this.getEditorContext().frameNode);
            //this.inherited(arguments);
        },
        /////////////////////////////////////////////////
        //
        //  Sub-Widget creation
        //
        getMainView: function () {
            return this.mainView;
        },
        addStyleView: function (context) {
            var self = this;
            var mainView = this.ctx.getApplication().mainView;
            var pe = this.getPageEditor();
            var propPane = pe.getPropertyPane();

            //#xo propPane._open();
            if (this.styleView) {
                return this.styleView;
            }

            function wirePane(pane) {
                pane._on('splitterMoveStart', function (e) {
                    self.hideFrame(true);
                });
                pane._on('splitterMoveEnd', function (e) {
                    self.hideFrame(false);
                });
            }

            wirePane(propPane);
            wirePane(pe.getDesignPane());

            var tabContainer = utils.addWidget(_TabContainer, {
                region: 'center',
                id: 'palette-tabcontainer-right-top',
                tabPosition: 'right' + '-h',
                tabStrip: false,
                'class': "davinciPalette davinciTopPalette widget",
                style: '',
                splitter: false,
                direction: 'right'
            }, null, propPane.containerNode, true);

            domClass.add(tabContainer.domNode, "davinciPalette davinciTopPalette");

            this.tabContainer = tabContainer;
            this.styleViewContainer = propPane;
            var _sTab = tabContainer.createTab('Widget', 'fa-cogs');
            try {
                this.styleView = utils.addWidget(SwitchingStyleView, {
                    toolbarDiv: propPane.containerNode,
                    context: context,
                    _editor: this.editor,
                    parentContainer: tabContainer,
                    tabContainer: tabContainer
                }, null, _sTab.containerNode, true);

            } catch (e) {
                console.error('html_style_view crash ' + e);

            }
            this.styleView._initialPerspectiveReady();
        },
        /**
         *
         * @param context
         */
        addOutlineGridView: function (context) {
            const settings = this.getSettings();
            var leftContainer = this.getLeftContainer();
            if (this.outlineGridView || !leftContainer) {
                return;
            }
            var pane = leftContainer.createTab('Outline', 'fa-cube', true, null, {
                parentContainer: leftContainer,
                open: settings.outlineOpen
            });
            this.outlineGridContainer = pane;
            var self = this;
            try {
                this.outlineGridView = utils.addWidget(OutlineGridView, {
                    context: context,
                    resizeToParent: true,
                    style: 'padding:0px;',
                    editor: this
                }, null, pane.containerNode, true);

                pane.add(this.outlineGridView);
                this.addNavigationGrid(this.outlineGridView);

                setTimeout(() => {
                    pane.resize();
                    this.outlineGridView && this.outlineGridView.resize();
                }, 500);

            } catch (e) {
                logError(e, 'outline crash ')
            }
            this.outlineGridView.startup();
            this.add(pane);
            window.outline = this.outlineGridView;
        },
        _handleKeyDown: function (e, isGlobal) {
            var handled = this._handleKey(e, isGlobal);
            // now pass to runtime if unhandled so global key listeners can take a stab
            // make sure to not pass global events back up
            if (!handled && !isGlobal) {
                Runtime.handleKeyEvent(e);
            }
        },
        /**
         *
         * @param e
         * @param isGlobal
         * @returns {*}
         * @private
         */
        _handleKey: function (e, isGlobal) {
            if (!this.keyBindings) {
                return false;
            }

            var stopEvent = this.keyBindings.some(function (globalBinding) {
                if (isGlobal && !globalBinding.keyBinding.allowGlobal) {
                    return;
                }

                if (Runtime.isKeyEqualToEvent(globalBinding.keyBinding, e)) {
                    var Workbench = require("davinci/Workbench");
                    Workbench._runAction(globalBinding.action, this.editor, globalBinding.action.id);
                    return true;
                }
            }, this);

            if (stopEvent) {
                dojo.stopEvent(e);
            }

            return stopEvent;
        },
        _setupKeyboardHandler: function (editor) {

            var pushBinding = function (o) {
                if (!this.keyBindings) {
                    this.keyBindings = [];
                }
                this.keyBindings.push(o);
            }.bind(this);


            this._getViewActions().forEach(function (actionSet) {
                actionSet.actions.forEach(function (action) {
                    if (action.keyBinding) {
                        pushBinding({
                            keyBinding: action.keyBinding,
                            action: action
                        });
                    }
                    if (action.menu) {
                        action.menu.forEach(function (menuItemObj) {
                            if (menuItemObj.keyBinding) {
                                pushBinding({
                                    keyBinding: menuItemObj.keyBinding,
                                    action: menuItemObj
                                });
                            }
                        }, this);
                    }
                }, this);
            }, this);

            connect.connect(editor, "handleKeyEvent", this, "_handleKeyDown");
        },
        /////////////////////////////////////////////////
        //
        //  Public main entries
        //
        /**
         * Enable/disable various items on the editor toolbar and action bar
         */
        setDirty: function () {},
        shouldUpdateTextEditor: function () {
            if (this._skipTextEditorChange) {
                this._skipTextEditorChange = true;
                return false
            }
            //var is = this.splitMode ==  types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL || this.splitMode == types.VIEW_SPLIT_MODE.SPLIT_VERTICAL;
            return true;
        },
        addPageEditor: function (item) {
            if (this.editor) {
                return;
            }
            try {
                var thiz = this;

                var _createTextEditor = function (dstNode, fileName) {
                    return thiz.createHTMLEditor(dstNode, fileName, item);
                };
                var _additionalEditors = function () {
                    var blockEditor = {
                        title: 'Blocks'
                    };
                    return [blockEditor];
                };

                //this.editor = new PageEditor(dojo.create('div', {}), item.getPath(), item, _createTextEditor, this.template,_additionalEditors());
                var _PE = require('davinci/ve/PageEditor');
                this.editor = new _PE(dojo.create('div', {}), item.getPath(), item, _createTextEditor, this.template, _additionalEditors());
                this.editor.ctx = this.ctx;

                this.editor.editorContainer = this;
                this.editor.delegate = this;
                this.containerNode.appendChild(this.editor.domNode);
                this.declaredClass = this.editor.declaredClass; //important
                this.editor.editorID = 'davinci.ve.HTMLPageEditor';

                Runtime.currentEditor = this.editor;
                Workbench.currentEditor = this.editor;


                this.add(this.editor, null, false);
                this._setupKeyboardHandler(this.editor);
                this.addTextEditor(this.editor.htmlEditor);
                Workbench._switchEditor(this.editor);
                setTimeout(function () {
                    dojo.publish("/davinci/ui/editorSelected", [{
                        editor: thiz.editor,
                        oldEditor: null
                    }]);
                }, 2000);

                return this.editor;

            } catch (e) {
                console.error('crash in visual editor ' + e);
                logError(e, 'crash in visual editor ');
            }


        },
        ///////////////////////////////////////////////////////////////////
        //
        //  UI-Callbacks
        //
        _widgetSelectionChanged: function (args) {
            const ctx = this.getEditorContext();
            if (ctx != args.context) {
                return;
            }

            if (args.isFake == true) {
                return;
            }
            var selection = args.selection;

            window.updateStatesView(this);

            var selected = selection.length == 1 ? selection[0] : null;
            var item = selected || {
                dummy: true
            };

            this.currentItem = selected || null;

            var self = this;

            this.publish(types.EVENTS.ON_ITEM_SELECTED, {
                item: item,
                owner: this,
                beanType: 'widget'
            });
            this._emit('selectionChanged', {
                item: item,
                owner: this
            })
        },
        _resume: function () {
            this.restoreStyleView();
        },
        _suspend: function () {
            this.suspendStyleView();
        },
        hideFrame: function (hide) {
            this._hidden = hide;
            var _frame = this.getFrame();
            if (_frame) {
                $(_frame).css('display', (hide ? 'none' : 'block'));
            }
        },
        onHide: function () {
            this.inherited(arguments);
            this.hideFrame(true);
            this.updateFocus();
            if (this.editor) {
                Workbench._switchEditor(null);
                this.publish("/davinci/ui/editorSelected", [{
                    editor: null,
                    oldEditor: this.editor
                }]);
            }
            this._suspend();
        },
        onShow: function () {
            this.inherited(arguments);
            this.hideFrame(false);
            if (this.editor) {
                this.updateFocus();
                Workbench._switchEditor(this.editor);
                var thiz = this;
                setTimeout(function () {
                    var z = this;
                    thiz.publish("/davinci/ui/editorSelected", [{
                        editor: thiz.editor,
                        oldEditor: null
                    }]);
                }, 1000);
            }
            this._resume();
        },
        addPalette2: function (context, store, palette) {
            this.palette = registry.byId('html_palette');
            if (this.palette) {
                return;
            }
            const head = new Deferred();
            const p = new Palette2.Palette(this.ctx);
            var ctx = this.ctx;
            p.initMeta(ctx);
            const settings = this.getSettings();
            setTimeout(() => {
                var leftContainer = ctx.mainView.leftLayoutContainer;
                var pane = leftContainer.createTab('Palette', 'fa-cube', true, null, {
                    parentContainer: leftContainer,
                    open: settings.paletteOpen
                });
                this.paletteContainer = pane;
                try {

                    p.init();
                    const store = p.createStore();
                    this.palette = utils.addWidget(Palette2.Grid, {
                        ctx: ctx,
                        attachDirect: true,
                        collection: store,
                        open: true,
                        expandOnClick: true,
                        sources: [],
                        palette: p
                    }, null, pane, true);
                    $(this.palette.bodyNode).addClass('dojoyPalette');
                    $(this.palette.bodyNode).addClass('paletteLayoutIcons');

                    this.palette.set('collection', store.filter({
                        parentId: '.'
                    }));
                    this.add(pane, null, false);
                    this.add(this.palette, null, false);
                    pane.add(this.palette);
                    this.addNavigationGrid(pane);
                    setTimeout(() => {
                        this.palette.set('collection', store.filter({
                            parentId: '.'
                        }))
                    }, 1000);
                } catch (e) {
                    console.error('palette crash ' + e);
                }


                try {
                    // this.palette.descriptors = "dijit,dojox,html,OpenAjax"; // FIXME: parameterize this in plugin data?
                    // this.palette.setContext(context);
                } catch (e) {
                    console.error('palette crash ' + e);
                }

            }, 1000);
        },
        ///////////////////////////////////////////////////////////////////
        //
        //  Main entry
        //
        openItem: function (item) {
            var thiz = this,
                dfd = new Deferred();

            thiz.item = item;

            function load(item) {
                //                thiz._createPalette().then((data) => {
                thiz.ensureSceneFiles(item).then(function () {
                    if (!thiz.editor) {
                        thiz.addPageEditor(item);
                    }
                    thiz.hideFrame(false);
                    thiz.updateFrame();

                    var onReady = function (content) {
                        thiz.editor.setContent(item.path, content);
                        //var _ctx = thiz.editor.visualEditor.context;
                        // thiz.showPalette && thiz.addPalette(thiz.editor.visualEditor.context);
                        thiz.showPalette && thiz.addPalette2(thiz.editor.visualEditor.context);
                        try {
                            if (!thiz.styleView && thiz.showStyleView !== false) {
                                thiz.addStyleView(thiz.getEditorContext());
                            }
                        } catch (e) {
                            console.error('error style view ' + e);
                            logError(e, 'error style view ');
                        }

                        dfd.resolve();
                    };

                    thiz.ctx.getFileManager().getContent(item.mount, item.path, onReady);
                });

                //                });
            }
            if (this.template && !this.template.isLoaded()) {
                this.template.load().then(function () {
                    load(item);
                });

            } else {
                load(item);
            }

            return dfd;
        },
        onReady: function () {
            this.ready = true;
            this.hideFrame(false);
            this.updateFrame();
            addPreviewToggle.call(this);
        },
        addStatesView: function () {
            return window.createStatesView(this);
        },
        onEditorContextLoaded: function () {
            var thiz = this;
            this.addStatesView();
            if (this._didInitial) {
                setTimeout(function () {
                    if (thiz.outlineGridView) {
                        thiz.outlineGridView.editorChanged({
                            editor: null,
                            oldEditor: thiz.editor
                        });
                        thiz.outlineGridView.editorChanged({
                            editor: thiz.editor,
                            oldEditor: null
                        })
                    }
                }, 800);
                return;
            }
            this._didInitial = true;
            var _ctx = thiz.editor.visualEditor.context;
            if (thiz.addOutline) {
                try {
                    setTimeout(function () {
                        thiz.addOutlineGridView(_ctx);
                        //init context & other views by changing the current editor
                        dojo.publish("/davinci/ui/editorSelected", [{
                            editor: thiz.editor,
                            oldEditor: null
                        }]);
                        thiz.onReady();
                    }, 500);
                } catch (e) {
                    console.error('outline creation crashed ' + e);
                    debugger;
                }
            }
        },
        getNullItem: function () {
            if (this._nullItem) {
                return this._nullItem;
            }
            this._nullItem = {
                owner: this,
                beanType: 'widget'
            };
            return this._nullItem;
        },
        _editors: null,
        addTextEditor: function (editor) {
            !this._editors && (this._editors = []);

            this._editors.push(editor);

        },
        publishCSSRules: function (content) {
            var parser = new CSSParser();
            var sheet = parser.parse(content, false, true);
            var names = [];
            if (sheet) {
                _.each(sheet.cssRules, function (rule) {
                    if (rule.mSelectorText && rule.mSelectorText !== 'body') {
                        names.push(rule.mSelectorText.replace('.', ''))
                    }
                });
            }
            this.publish('CSSRulesUpdated', {
                names: names,
                editor: this
            })
        },
        loadSceneCSSFile: function (file) {
            var thiz = this;
            this.ctx.getFileManager().getContent(file.mount, file.path, function (content) {
                thiz.publishCSSRules(content);
            });
        },
        /**
         * Event when an editor context and its containing application has been fully
         * loaded. This is used to extend the left palette system for 'shared blocks'
         *
         * @param evt {Object} event data
         * @param evt.context {davinci/ve/Context} the davinci context
         * @param evt.appContext {xapp/manager/Context} the xapp context
         * @param evt.settings {Object} the application's settings
         * @param evt.ctx {xide/manager/Context} the root context (IDE context)
         * @param evt.global {Object} the loaded document's global
         * @param evt.blockScopes {xblox/model/Scope[]} the loaded xblox scopes
         *
         */
        onContextReady: function (evt) {
            //filter
            if (evt.context != this.getEditorContext()) {
                return;
            }

            this.contextInfo = evt;

            var thiz = this;
            var pe = this.getPageEditor();
            var sheets = evt.context._extraSheets;
            var peTabs = pe.bottomTabContainer;
            var store = this.item._S;
            this.ctx.getWindowManager().registerView(this, true);
            if (this.showLESS !== false && store && !this.didAddSheetEditors) {
                this.didAddSheetEditors = true;
                _.each(sheets, function (props, path) {
                    var sheetPath = path; //path.replace('.css','.less');
                    sheetPath = sheetPath.replace('././', './');
                    path = path.replace('././', './');
                    var fileItem = store.getSync(sheetPath);
                    debugCSSEditor &&  0 && console.log('add sheet editor ' + sheetPath, fileItem);
                    if (fileItem) {
                        props.storeItem = fileItem;
                        props.cssItem = store.getSync(path);
                    } else {
                        debugCSSEditor && console.error('couldnt find file : ', sheetPath);
                    }

                    if (fileItem) {
                        var editorArgs = {
                            sheetProps: props,
                            closeable: false,
                            emmet: true,
                            resizeToParent: true
                        };
                        var DOCKER = types.DOCKER;
                        var editorPane = pe.createTab(null, {
                            title: fileItem.name,
                            icon: 'fa-paint',
                            tabOrientation: DOCKER.TAB.TOP,
                            location: DOCKER.DOCK.STACKED,
                            target: pe.getSourcePane()
                        });

                        thiz.cssPane = editorPane;

                        var editor = Default.Implementation.open(fileItem, editorPane, editorArgs, true);
                        props.edior = editor;
                        thiz.addTextEditor(editor);
                        editorPane.add(editor);
                        thiz.loadSceneCSSFile(fileItem);
                        editor._on(types.EVENTS.ON_FILE_CONTENT_CHANGED, function (evt) {
                            thiz.publishCSSRules(evt.content);
                        });
                    } else {
                        console.error('couldnt find file : ', sheetPath);
                    }
                });

            }
            this.hideFrame(false);
            var scopes = evt.blockScopes;
            //nothing to do ?
            if (!scopes || !scopes.length) {
                return;
            }
            this.showBehaviours && this._buildBlockPalette(evt);
            evt.context.eventHandler = {
                'click': function (e) {
                    thiz.publish(types.EVENTS.ON_ITEM_SELECTED, {
                        item: thiz.currentItem || thiz.getNullItem(),
                        owner: thiz,
                        beanType: 'widget'
                    });
                    thiz._emit(types.EVENTS.ON_VIEW_SHOW, {
                        view: thiz
                    });
                }
            };
        },
        onAppReady: function (evt) {
            setTimeout(function () {
                if (this.outlineView && evt.context) {
                    this.outlineView.reset();
                    this.outlineView.createTree();
                }
            }.bind(this), 1000);
        },
        getTextEditor: function () {
            return this.editor.htmlEditor;
        },
        getEditor: function (_mount, _path) {
            var editor = null;
            _.each(this._editors, function (_editor) {
                var item = _editor.item;
                var mount = item.mount.replace('/', '');
                var path = item.path.replace('./', '');
                if (mount === _mount && path === _path) {
                    editor = _editor;
                }
            });
            return editor;
        },
        /**
         * triggered by intern editor
         * @param evt
         * @returns {*}
         */
        onEditorContentChanged: function (evt) {
            var self = this;
            this.__internUpdate = true;
            setTimeout(function () {
                self.__internUpdate = false;
            }, 5000);
            return this.reload(evt.content);
        },
        isSaving: false,
        save: function (pageEditor, visualEditor, model, text) {

            if (this.isSaving) {
                return;
            }


            this.isSaving = true;
            this.__internUpdate = true;
            var textEditor = this.getTextEditor();
            if (textEditor) {
                textEditor.set('value', text);
            }

            //tell everybody
            this.publish(types.EVENTS.ON_VISUAL_EDITOR_SAVE, {
                pageEditor: pageEditor,
                visualEditor: visualEditor,
                model: model,
                text: text,
                item: this.item
            });

            var thiz = this;
            //this will also publish ON_FILE_CHANGED
            this.ctx.getFileManager().setContent(this.item.mount, this.item.path, text, function () {
                //thiz.__internUpdate = false;
                thiz.isSaving = false;
            });
        },
        fileChanged: function (evt) {
            if (this.__internUpdate) {
                return;
            }
            var path = evt.data.path;
            var self = this;
            var _start = 'workspace_user';

            function reload(editor) {
                if (editor == self.getTextEditor()) {
                     0 && console.log('reload = same text editor');
                    editor.reload().then(function (content) {
                        self.reload(content);
                    });
                }

                editor.reload().then(function () {
                    self.onFileChanged({
                        editor: editor
                    });
                });

            }

            if (path.indexOf(_start) != -1) {
                var libPath = path.substr(path.indexOf(_start) + (_start.length + 1), path.length);
                var editor = this.getEditor(_start, libPath);
                if (editor) {
                    return reload(editor);
                }
            }
            var vfsPath = this.ctx.toVFSShort(path, 'workspace_user');
            if (vfsPath) {
                var editor = this.getEditor('workspace_user', vfsPath);
                if (editor) {
                    reload(editor);
                }
            }

        },
        onFileChanged: function (evt) {
            if (evt.__did) {
                return;
            }
            evt.__did = true;
            // 0 && console.log('intern: on file changed',evt);
            //scene css files ?
            if (evt && evt.editor && evt.editor.sheetProps) {

                var sheetProps = evt.editor.sheetProps;
                var doc = this.contextInfo.document;
                var thiz = this;
                dojo.withDoc(doc, function () {
                    //remove
                    var head = doc.getElementsByTagName('head')[0];
                    try {
                        // 0 && console.log('did destroy css');
                        if (sheetProps.link) {
                            head.removeChild(sheetProps.link);
                            sheetProps.link = null;
                        }
                    } catch (e) {
                        dojo.destroy(sheetProps.link);
                    }

                    var newUrl = thiz.ctx.getFileManager().getImageUrl(sheetProps.cssItem, true, false);
                    // 0 && console.log('do re-add css ' + newUrl);
                    //re-add
                    var newLink = domConstruct.create('link', {
                        rel: 'stylesheet',
                        type: 'text/css',
                        href: newUrl
                    });
                    head.appendChild(newLink);
                    dojo.place(newLink, head, 'last');
                    sheetProps.link = newLink;
                });
            }
        },
        onUseActionStore: function (store) {
            return;
            var actions = store.query();
            _.each(actions, function (action) {
                action.addPermission = true;
                !action.tab && (action.tab = 'Home');
            });
        },
        getBlockEditor: function () {
            var pe = this.getPageEditor();
            return pe.getBlockEditor();
        },
        resize: function () {
            this.updateFrame();
            this.updateFocus();
            var pe = this.getPageEditor();
            var ctx = this.getEditorContext();
            if (pe) {
                utils.resizeTo(pe.domNode, this.containerNode, true, true, null, {});
                pe.resize();
            }
        },
        startup: function () {
            var self = this,
                EVENTS = types.EVENTS;
            this._frameTimer = setInterval(() => {
                var ctx = this.getEditorContext();
                if (ctx) {
                    if (ctx.isRezing) {
                        return;
                    }
                }
                this.updateFrame();
            }, 1000);
            //this.addActions(this.getItemActions());
            //this.initReload();
            //warm up meta library for wizards
            Metadata.getSmartInput('xblox/RunScript');
            this.subscribe([
                EVENTS.ON_BUILD_BLOCK_INFO_LIST_END,
                EVENTS.ON_ACTION_CONTEXT_CHANGED,
                EVENTS.ON_ACTION_CHANGE_CONTEXT,
                EVENTS.ON_ACTION_CHANGE_CONTEXT,
                EVENTS.ON_FILE_CHANGED,
                EVENTS.ON_CONTEXT_READY, //track it
                EVENTS.ON_APP_READY, //reloads, text-editor changes
                {
                    //update editor content on file changes
                    key: types.EVENTS.ON_FILE_CONTENT_CHANGED,
                    handler: function (evt) {
                        var item = evt.item;
                        //@TODO: wtf
                        self._skipTextEditorChange = true;
                        if (item == self.item && evt.callee == self.textEditor) {
                            self.editor.setContent(self.item.getPath(), evt.content);
                        } else {
                            self.onFileChanged(evt);
                        }
                    }
                },
                {
                    key: '/davinci/ui/widgetSelected',
                    handler: self._widgetSelectionChanged
                },
                {
                    //update editor content on file changes
                    key: '/davinci/ui/context/loaded',
                    handler: self.onEditorContextLoaded
                }
            ]);
            this.initReload();
            setTimeout(function () {
                self.resize();
            }, 2000);

        },
        /**
         *
         * @param designMode {boolean}
         * @param updateWidget {boolean}
         */
        setDesignMode: function (designMode, updateWidget) {
            this._designMode = designMode;
            if (updateWidget === true) {
                var action = this._designToggleAction;
                if (action) {
                    var widget = action.getVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR).widget;
                    if (widget) {
                        widget.set('checked', designMode);
                    }
                }
            }
            var context = this.getEditorContext();
            if (context) {
                context.setDesignMode(designMode);
            }
        },
        _didHide: false,
        getSettings: function () {
            var settingsManager = this.ctx.getSettingsManager();

            var settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };
            var props = settingsStore.getSync('ve') || {
                value: {
                    showEditors: false,
                    selected: 'HTML'
                }
            };
            return props.value;
        },
        saveSettings: function () {
            var settingsManager = this.ctx.getSettingsManager();
            var settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };

            var props = settingsStore.getSync('ve') || {
                value: this.getSettings()
            };

            var value = props.value;
            value.showEditors = this.showCode;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            var selected = 'HTML';
            _.each(docker.getPanels(), (p) => {
                if (p._title === 'Blocks' && p.selected === true) {
                    selected = 'Blocks';
                }
                if (p._title.indexOf && p._title.indexOf('.css') !== -1 && p.selected === true) {
                    selected = p._title;
                }
            });
            value.selected = selected;
            value.outlineOpen = this.outlineGridView.open;
            value.paletteOpen = this.palette._parent.open;
            //  0 && console.log('save settings', value);
            settingsManager.write2(null, '.', {
                id: 've'
            }, {
                value: value
            }, true, null).then(function () {

            });
        },
        restoreSettings: function () {
            var settings = this.getSettings()
            if (!settings.showEditors) {
                setTimeout(() => {
                    if (this._didHide) {
                        var pe = this.getPageEditor();
                        var docker = pe.getDocker();
                        return;
                    }
                    this._didHide = true;
                    this.toogleCode();
                }, 500);
            }
            var selected = settings.selected;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            var panel = _.find(docker.getPanels(), {
                _title: selected
            });
            if (panel) {
                panel.select();
            }
        },
        didinitSettings: false,
        addNavigationGrid: function (grid) {
            grid._on(['hide', 'show'], () => {
                this.saveSettings();
            });
        },
        initSettings: function () {
            if (this.didinitSettings) {
                return;
            }
            this.didinitSettings = true;
            var pe = this.getPageEditor();
            var docker = pe.getDocker();
            docker._on(types.DOCKER.EVENT.SELECT, (panel) => {
                var selected = panel.title();
                this.getSettings().selected = selected;
                this.saveSettings();
            });
        },
        onSceneBlocksLoaded: function (evt) {
            this.blockScopes = evt.blockScopes;
            debugBlocks &&  0 && console.log('onSceneBlocksLoaded : ', evt);
            var pe = this.getPageEditor();
            window['vee'] = this;
            pe.onSceneBlocksLoaded(evt, this);
            this.initSettings();
            setTimeout(() => {
                this.restoreSettings();
            }, 500);

        }
    });

    OutlineGridView.prototype.reloadModule = Module.prototype.reloadModule;
    OutlineView.prototype.reloadModule = Module.prototype.reloadModule;
    PageEditor.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorAction.prototype.reloadModule = Module.prototype.reloadModule;
    // VisualEditorPalette.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorSourceEditor.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorLayout.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorAction.prototype.reloadModule = Module.prototype.reloadModule;
    VisualEditorTools.prototype.reloadModule = Module.prototype.reloadModule;

    Module.getFileActions = function (ctx, item) {
        return [
            ctx.createAction({
                label: 'Preview in Browser',
                command: 'File/Preview',
                tab: 'Home',
                icon: 'fa-eye',
                tab: 'Home',
                mixin: {
                    quick: true,
                    addPermission: true,
                    handler: function () {
                        window.open(ctx.getContextManager().getViewUrl(item));
                    }
                },
                custom: true
            })
        ]
    }
    return Module;
});
},
'xideve/views/OutlineView':function(){
define([
    "dcl/dcl",
    "dojo/dom-class",
    "davinci/Workbench",
    "davinci/ui/widgets/OutlineTree",
    "xide/layout/ContentPane"
], function (dcl,domClass,Workbench, OutlineTree, ContentPane) {

    return dcl([ContentPane], {
        declaredClass:'davinci.workbench.OutlineView',
        startup:function(){
            domClass.remove(this.domNode,'ui-widget-content');

        },
        reset:function(){
            if (this.outlineTree) {
                this.outlineTree.destroy();
            }
            delete this.outlineProvider;
            delete this.outlineTree;

        },
        constructor: function (params, srcNodeRef) {
            this.subscribe("/davinci/ui/editorSelected", this.editorChanged);
            //this.subscribe("/davinci/ui/selectionChanged", this.selectionChanged);
            //this.subscribe("/davinci/ui/modelChanged", this.modelChanged);
            this.subscribe("/davinci/ui/context/pagerebuilt", this._pageRebuilt);
        },
        _getViewActions: function () {
            return [];
        },

        editorChanged: function (changeEvent) {
            var editor = changeEvent.editor;

            if (this.currentEditor) {
                if (this.currentEditor == editor) {
                    return;
                }
//			this.currentEditor.removeChangeListener(this.modelStore);
                if (this.outlineTree) {
                    //this.removeContent();
                    this.outlineTree.destroy();
                }
                delete this.outlineProvider;
                delete this.outlineTree;
                //delete this._toolbarID;
            }
            this.currentEditor = editor;
            if (!editor) {
                return;
            }

            if (editor.getOutline) {
                this.outlineProvider = editor.getOutline();
            }
            var iconFunction;
            //this.toolbarDiv.innerHTML="";
            if (this.outlineProvider) {
                this.outlineProvider._outlineView = this;

                /*if (this.outlineProvider.toolbarID) {
                 this.toolbarID=this.outlineProvider.toolbarID;
                 this._createToolbar();
                 }*/

                this.createTree();
            } else {
                this.containerNode.innerHTML = 'no outline aviable';
            }
        },

        createTree: function () {
            if (this.outlineTree) {
                this.removeContent();
            }

            if (this.popup) {
                this.popup.destroyRecursive();
            }

            if (!this.outlineProvider && this.currentEditor) {
                this.outlineProvider = this.currentEditor.getOutline();
                this.outlineProvider._outlineView = this;
            }


            if (this.outlineProvider && this.outlineProvider.getModel) {
                this.outlineModel = this.outlineProvider.getModel(this.currentEditor);
            }

            // create tree
            var treeArgs = {
                model: this.outlineModel,
                showRoot: this.outlineModel.showRoot,
                betweenThreshold: this.outlineModel.betweenThreshold,
                checkItemAcceptance: this.outlineModel.checkItemAcceptance,
                isMultiSelect: true,
                persist: false,
                style:'height:inherit;'
            };

            if (this.outlineProvider.getIconClass) {
                treeArgs.getIconClass = this.outlineProvider.getIconClass;
            }

            if (this.currentEditor.getContext) {
                treeArgs.context = this.currentEditor.getContext()
            }

            // #2256 - dijit tree cannot have a null dndController
            if (this.outlineModel.dndController) {
                treeArgs.dndController = this.outlineModel.dndController;
            }

            this.outlineTree = new OutlineTree(treeArgs);

            // BEGIN TEMPORARY HACK for bug 5277: Surround tree with content pane and subcontainer div overflow: auto set to workaround spurious dnd events.
            // Workaround should be removed after the following dojo bug is fixed: http://bugs.dojotoolkit.org/ticket/10585
            this.container = new ContentPane({style: "padding:0"});
            this.subcontainer = dojo.doc.createElement('div');
            this.subcontainer.id = "dvOutlineSubcontainer";
            this.subcontainer.appendChild(this.outlineTree.domNode);
            this.container.domNode.appendChild(this.subcontainer);
            this.setContent(this.container);
            //this.attachToolbar();
            // END HACK: Original code commented out below:

            this.outlineTree.startup();

            if (this.outlineProvider) {
                this.outlineProvider._tree = this.outlineTree;
            }

            var outlineActionsID = (this.outlineProvider.getActionsID && this.outlineProvider.getActionsID()) || 'davinci.ui.outline';

            this.popup = Workbench.createPopup({
                partID: outlineActionsID,
                domNode: this.outlineTree.domNode,
                openCallback: this.outlineTree.getMenuOpenCallback ? this.outlineTree.getMenuOpenCallback() : null
            });
        },

        _getViewContext: function () {
            return this.outlineProvider;
        },

        selectionChanged: function (selection) {
            if (!this.publishing["/davinci/ui/selectionChanged"] &&
                selection.length && selection[0].model && this.outlineTree) {
                this.outlineTree.selectNode([selection[0].model]);
            }
        },

        _pageRebuilt: function () {
            if (this.outlineTree) {
                var paths = this.outlineTree.get("paths");
                this.createTree();
                this.outlineTree.set("paths", paths);
            }
        },

        modelChanged: function (modelChanges) {
            if (this.outlineModel && this.outlineModel.refresh) {
                this.outlineModel.refresh();
            } else if (this.outlineProvider && this.outlineProvider.getStore) {
                this.outlineModel.store = this.outlineProvider.getStore();
                this.outlineModel.onChange(this.outlineProvider._rootItem);
            }
        }
    });
});


},
'xideve/views/OutlineGridView':function(){
define([
    'dcl/dcl',
    "dojo/dom-class",
    'xide/utils',
    'xide/types',
    "dgrid/Editor",
    'xide/data/TreeMemory',
    "davinci/ve/widget",
    "xide/_base/_Widget",
    "xide/mixins/ReloadMixin",
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    "davinci/ve/States",
    "davinci/ve/commands/StyleCommand",
    'xideve/views/VisualEditorAction'
], function (dcl, domClass,
    utils, types, Editor, TreeMemory,
    Widget, _Widget, ReloadMixin, Grid,
    TreeRenderer, KeyboardNavigation, _Widget2, States, StyleCommand, VisualEditorAction) {
    var debug = false;
    var ACTION = types.ACTION;
    var gridClass = Grid.createGridClass('OutlineGridView', {
            permissions: [
                ACTION.RENAME,
                ACTION.UNDO,
                ACTION.REDO,
                ACTION.RELOAD,
                ACTION.DELETE,
                ACTION.CLIPBOARD,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.SAVE,
                ACTION.SEARCH,
                ACTION.TOOLBAR,
                'File/Properties',
                'File/Edit',
                ACTION.GO_UP,
                ACTION.HEADER,
                ACTION.EDIT,
                ACTION.COPY,
                ACTION.CLOSE,
                ACTION.MOVE,
                ACTION.RENAME,
                ACTION.RELOAD,
                ACTION.CLIPBOARD,
                ACTION.LAYOUT,
                ACTION.SELECTION,
                ACTION.SEARCH,
                ACTION.TOOLBAR,
                ACTION.STATUSBAR,
                ACTION.SOURCE,
                ACTION.CLIPBOARD_COPY,
                ACTION.CLIPBOARD_PASTE,
                ACTION.CLIPBOARD_CUT
            ],
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Widget': 80,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            getEditorContext: function () {
                return this.context;
            },
            startup: function () {                
                this.inherited(arguments);
                this._createContextMenu();
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            //CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            WIDGET: {
                CLASS: _Widget2
            },
            EDITOR: {
                CLASS: Editor
            },
            EXTRAS: {
                CLASS: VisualEditorAction.GridClass
            }
        }, {
            RENDERER: TreeRenderer
        }, null,
        null);


    return dcl([_Widget, ReloadMixin.dcl], {
        templateString: '<div attachTo="domNode"></div>',
        declaredClass: 'xideve.views.OutlineGridView',
        rootWidget: null,
        _lastEvent: null,
        useRichTextLabel: false,
        resizeToParent: true,
        onItemSelected: function (item, node) {
            debug &&  0 && console.log('-widget selected', item);
            var context = this.getContext();
            var store = this.getStore();
            if (!context) {
                return;
            }
            var newSelection = store.model._getWidget(item);
            var oldSelection = context.getSelection();

            // deselect any olds not in new
            dojo.forEach(oldSelection, dojo.hitch(this, function (item) {
                /*if (newSelection.indexOf(item) == -1) {*/
                context.deselect(item);
                /*}*/
            }));
            context.select(newSelection, true);
        },
        _toggle: function (widget, on, node) {
            var visible = !on;
            var value = visible ? "" : "none";
            var state;
            var statesFocus = States.getFocus(widget.domNode.ownerDocument.body);
            if (statesFocus) {
                state = statesFocus.state;
            } else {
                var currentStatesList = States.getStatesListCurrent(widget.domNode);
                for (var i = 0; i < currentStatesList.length; i++) {
                    if (currentStatesList[i]) {
                        state = currentStatesList[i];
                        break;
                    }
                }
            }
            var command = new StyleCommand(widget, [{
                display: value
            }], state);
            var context = this.getContext();
            context.getCommandStack().execute(command);
        },
        getLabel: function (item) {
            if (item.id == "myapp") {
                return this.getContext().model.fileName;
            }

            var label = '';
            var widget = this._getWidget(item);
            /*
                 /\{([^}]+)\}/
    
                 /        - delimiter
                 \{       - opening literal brace escaped because it is a special character used for quantifiers eg {2,3}
                 (        - start capturing
                 [^}]     - character class consisting of
                 ^    - not
                 }    - a closing brace (no escaping necessary because special characters in a character class are different)
                 +        - one or more of the character class
                 )        - end capturing
                 \}       - the closing literal brace
                 /
                 */

            if (item.type === 'xblox/RunScript' && widget) {
                var _w = widget.domNode;

                //event to target property
                if (_w.mode == 1 && _w.targetproperty) {

                    if (_w._targetBlock) {
                        return _w._targetBlock.name + '->' + _w.targetproperty;
                    }
                }


                if (_w && _w.script && _w.script !== 'null') {
                    return 'XBlox::' + _w.script;
                }

                if (_w.targetEvent && _w.targetEvent.length > 0) {
                    return 'XBlox::' + _w.targetEvent;
                }

                return 'XBlox';

            }
            if (item.type === 'xblox/CSSState' && widget) {
                var _w = widget.domNode;
                return "CSS State/" + _w.name;
            }

            if (item.type === 'xblox/StyleState' && widget) {
                var _w = widget.domNode;
                return "Style State/" + _w.name;
            }

            if (widget.isWidget) {
                label = Widget.getLabel(widget);
                return label;
            }

            var type = widget.type;

            if (!type) {
                return;
            }

            var lt = this.useRichTextLabel ? "&lt;" : "<";
            var gt = this.useRichTextLabel ? "&gt;" : ">";

            if (type.indexOf("html.") === 0) {
                label = lt + type.substring("html.".length) + gt;
            } else if (type.indexOf("OpenAjax.") === 0) {
                label = lt + type.substring("OpenAjax.".length) + gt;
            } else if (type.indexOf(".") > 0) {
                label = type.substring(type.lastIndexOf(".") + 1);
            } else {
                label = widget.label || type;
            }

            var widget = Widget.byId(widget.id, this.getContext().getDocument());
            if (widget) {
                var id = widget.getId();
                if (id) {
                    label += " id=" + id;
                }
            }
            return label;
        },
        getColumns: function () {

            var thiz = this;
            return [
                /*
                                    {

                                        label: "Visibile",
                                        field: "visible",
                                        sortable: false,
                                        set: function (item, val, val2) {
                                             0 && console.log('set by checkbox', arguments);
                                        },
                                        get: function (item) {
                                            // ensure initial rendering matches up with widget behavior
                                            var widget = thiz._getWidget(item);
                                            if (widget) {
                                                return !(widget.domNode.style.display === 'none');
                                            }

                                            return true;
                                        },
                                        editorArgs: {

                                        }
                                    },*/
                {
                    renderExpando: true,
                    label: "Name",
                    field: "type",
                    sortable: false,
                    formatter: function (name, item) {

                        var label = thiz.getLabel(item);
                        if (item.type == 'xblox/RunScript') {
                            var widget = thiz._getWidget(item);
                            if (widget) {
                                var proxy = widget.domNode; //xblox/RunScript
                                if (proxy) {
                                    var block = proxy._targetBlock;
                                    if (block) {
                                        if (block.declaredClass.indexOf('Command') != -1) {
                                            return '<span class="grid-icon ' + block.getIconClass() + '"></span>' + '' + block.name + ' (' + proxy.targetevent + ')';
                                        } else if (block.declaredClass.indexOf('Variable') != -1) {}
                                    }
                                }
                            }
                        }
                        return label;
                    }
                }
            ]
        },
        createGrid: function () {
            utils.destroy(this.outlineTree);
            dojo.empty(this.domNode);
            var thiz = this;
            var _ctorArgs = {
                features: {},
                className: 'outlineGridView dgrid dgrid-dgrid',
                cellNavigation: false,
                collection: this.store.filter(this.getRootFilter()),
                deselectOnRefresh: false,
                showHeader: false,
                owner: this,
                resizeToParent: true,
                columns: this.getColumns()
            };
            var grid = utils.addWidget(gridClass, _ctorArgs, null, this, true);
            this.grid = grid;
            this.grid.editor = this.editor;
            this.grid.set('collection', this.store.filter({
                type: 'html.body'
            }));
            this.outlineTree = grid;
            grid.refresh();
            this.add(grid);

            if (this._lastState) {
                grid.setState(this._lastState);
                delete this._lastState;
            }
            grid._on('selectionChanged', function (selection) {
                thiz.onItemSelected(selection.selection[0]);
            });
            grid.on('dgrid-datachange', function (evt) {
                var row = grid.row(evt.target);
                var value = evt.value;
                var context = thiz.getContext();
                var store = thiz.getStore();
                if (!context) {
                    return;
                }
                var widget = store.model._getWidget(row.data);
                if (widget) {
                    thiz._toggle(widget, !value);
                }
            });

        },
        initStore: function (data) {
            var self = this;
            this.store = new TreeMemory({
                data: data.items,
                model: data.model,
                provider: data.provider,
                idProperty: 'id',
                parentProperty: 'parent',
                getSync: function (id) {
                    var res = this.inherited(arguments);
                    var model = this.provider.getModel();
                    if (!res) {
                        var _w = model._getWidget({
                            id: id
                        });
                        if (_w) {
                            this.putSync(_w);
                            res = _w;
                        }
                    }
                    return res;
                },
                getChildrenSync: function (parent) {
                    var thiz = this,
                        _items = [];

                    var _cb = function (items) {
                        for (var i = 0; i < items.length; i++) {
                            var obj = items[i];
                            thiz.putSync(obj);
                        }
                        _items = items;
                    };

                    this.model.getChildren(parent, _cb);
                    return _items;
                },
                getChildren: function (parent, options) {
                    var thiz = this,
                        _items = [];
                    var _cb = function (items) {
                        for (var i = 0; i < items.length; i++) {
                            var obj = items[i];
                            thiz.putSync(obj);
                        }
                        _items = items;
                    };
                    this.model.getChildren(parent, _cb);
                    return this.inherited(arguments);
                },
                mayHaveChildren: function (parent) {
                    return this.model.mayHaveChildren(parent);
                }

            });
            return this.store;
        },
        /**
         *
         * @returns {*}
         */
        getContext: function () {
            if (this.currentEditor) {
                return this.currentEditor.getContext();
            }
            return null;
        },
        _getWidget: function (item) {
            return Widget.byId(item.id);
        },
        getStore: function () {
            return this.store;
        },
        getModel: function () {
            return this.outlineModel;
        },
        getRootFilter: function () {
            return {
                id: this.rootWidget ? this.rootWidget.id : ''
            }
        },
        hasItemActions: function () {
            return false;
        },
        _getViewContext: function () {
            return this.outlineProvider;
        },
        _getViewActions: function () {
            return [];
        },
        onWidgetChanged: function (evt) {
            debug &&  0 && console.log('widget changed', evt);
            var type = evt.type;
            var ctx = this.getContext();
            if (type === ctx.WIDGET_ADDED) {
                this.getStore().putSync(evt.widget);
            } else if (type === ctx.WIDGET_REMOVED) {
                this.getStore().removeSync(evt.widget);
            }
            if (this.grid) {
                this.grid.refresh();
            }
        },
        onWidgetSelected: function (evt) {
            return;
             0 && console.log('widget selected');

            if (evt.outline) {
                return;
            }

            var selection = evt.selection;
            if (!selection || !selection.length) {
                return;
            }

            var _sel = [];
            _.each(selection, function (item) {
                _sel.push(item.id);
            });
            this.grid.select(_sel, null, true, {
                focus: false,
                append: false,
                expand: true,
                delay: 1
            });

        },
        startup: function () {
            domClass.remove(this.domNode, 'ui-widget-content');
            this.subscribe("/davinci/ui/editorSelected", this.editorChanged);
            //this.subscribe("/davinci/ui/selectionChanged", this.selectionChanged);
            //this.subscribe("/davinci/ui/modelChanged", this.modelChanged);
            this.subscribe("/davinci/ui/context/pagerebuilt", this._pageRebuilt);
            this.subscribe(types.EVENTS.ON_WIDGET_CHANGED);
            this.subscribe("/davinci/ui/widgetSelected", this.onWidgetSelected);
            this.initReload();
        },
        onReloaded: function () {
            this.reset();
            if (this._lastEvent) {
                this.editorChanged(this._lastEvent);
            }
        },
        reset: function () {
            this.currentEditor = null;
            delete this.outlineProvider;
            delete this.outlineTree;
        },
        editorChanged: function (changeEvent) {
            debug &&  0 && console.log('editorChanged', changeEvent);
            this._lastEvent = changeEvent;
            var editor = changeEvent.editor;
            if (this.currentEditor) {
                if (this.currentEditor == editor) {
                    return;
                }

                if (this.outlineTree) {
                    var state = this.outlineTree.getState();
                    this._lastState = state;
                    this.outlineTree.destroy();

                }
                if (this.store) {
                    this.store.destroy();
                    delete this.store;
                }
                delete this.outlineProvider;
                delete this.outlineTree;
            }

            this.currentEditor = editor;
            if (!editor) {
                return;
            }

            if (editor.getOutline) {
                this.outlineProvider = editor.getOutline();
            }

            if (this.outlineProvider) {
                this.createTree();
            } else {
                this.containerNode.innerHTML = 'no outline aviable';
            }

        },
        createTree: function () {
            if (this.popup) {
                this.popup.destroyRecursive();
            }
            if (!this.outlineProvider && this.currentEditor) {
                this.outlineProvider = this.currentEditor.getOutline();
                this.outlineProvider._outlineView = this;
            }
            if (this.outlineProvider && this.outlineProvider.getModel) {
                this.outlineModel = this.outlineProvider.getModel(this.currentEditor);
            }
            var model = this.outlineModel;
            var provider = this.outlineProvider;
            var root = provider && provider._context ? provider._context.rootWidget : null;
            if (!root) {
                console.error('have no root')
            }
            var outlineActionsID = (this.outlineProvider.getActionsID && this.outlineProvider.getActionsID()) || 'davinci.ui.outline';
            this.initStore({
                items: [root],
                model: model,
                provider: provider
            });
            this.createGrid();
        },
        selectionChanged: function (selection) {
            if (!this.publishing["/davinci/ui/selectionChanged"] &&
                selection.length && selection[0].model && this.outlineTree) {
                this.outlineTree.selectNode([selection[0].model]);
            }
        },
        _pageRebuilt: function () {
            if (this.outlineTree) {
                var paths = this.outlineTree.get("paths");
                this.createTree();
                this.outlineTree.set("paths", paths);
            }
        },
        _modelChanged: function (modelChanges) {
             0 && console.log('_modelChanged', selection);
            if (this.outlineModel && this.outlineModel.refresh) {
                this.outlineModel.refresh();
            } else if (this.outlineProvider && this.outlineProvider.getStore) {
                this.outlineModel.store = this.outlineProvider.getStore();
                this.outlineModel.onChange(this.outlineProvider._rootItem);
            }
        }
    });
});
},
'xideve/views/VisualEditorAction':function(){
/** @module xideve/views/VisualEditorAction **/
define([
    "dcl/dcl",
    "xdojo/has",
    "require",
    'xaction/Action',
    'xide/types',
    'xide/utils',
    'xide/factory',
    "davinci/Runtime",
    "davinci/Workbench",
    "davinci/ve/metadata",
    "xaction/ActionProvider",
    'xaction/DefaultActions',
    'dojo/Deferred',
    'xdojo/declare',
    "davinci/ve/tools/CreateTool",
    "xide/views/SimpleCIDialog",
    "xide/widgets/TemplatedWidgetBase"
], function (dcl, has, require, Action, types, utils, factory, Runtime, Workbench, Metadata, ActionProvider, DefaultActions, Deferred, declare, CreateTool, SimpleCIDialog,
    TemplatedWidgetBase) {
    var ACTION = types.ACTION;

    var debug = false;
    const __selected = (editor) => {
        if (editor.getSelection) {
            //  0 && console.log('selected', editor.getSelection());
            const widget = editor.getSelection()[0];
            if (widget) {
                const context = editor.getEditorContext();
                const w = context.getWidgetById(widget.id);
                //  0 && console.log('widget', w._srcElement.getText(context));
            }
        } else {
            // debugger;
        }
    }

    window.__selected = __selected;


    const _createStyleState = (widget, editor) => {
        const ctx = editor.getEditorContext();
        const cis = [
            utils.createCI('Name', types.ECIType.STRING, 'noname', {
                group: 'General'
            }),
            utils.createCI('Style', types.ECIType.DOM_PROPERTIES, '', {
                group: 'Style',
                widget: {
                    open: true
                }
            })
        ]
        const nameDlg = SimpleCIDialog.create('Create New Style State', cis, (changed) => {
            const ctrToolData = {
                context: ctx,
                type: 'xblox/StyleState',
                properties: {
                    name: cis[0].value || 'noname',
                    style: cis[1].value
                },
                userData: {}
            }
            const meta = Metadata.getMetadata(ctrToolData.type);
            Metadata.getHelper(widget.type, 'tool').then(function (ToolCtor) {
                // Copy the data in case something modifies it downstream -- what types can data.data be?
                const tool = new(ToolCtor || CreateTool)(ctrToolData, {});
                tool._context = ctx;
                ctx.setActiveTool(tool);
                tool._create({
                    position: {
                        x: 0,
                        y: 0
                    },
                    parent: widget,
                    userData: meta
                });
                ctx.setActiveTool(null);
            }.bind(this));
        });
        nameDlg.show();

    }
    const _removeStyleState = (widget, editor, ctx) => {
        const node = widget.domNode;
        const state = node.state;
        const context = editor.getEditorContext();
        const widgets = context.widgetHash;
        const states = node.getStates();
        let widgetStateNode = node.getState(state);
        let stateWidget = context.toWidget(widgetStateNode);
        node.removeState(widgetStateNode);
        context.select(stateWidget);
        editor.runAction('Edit/Delete');
        context.select(widget);
    }
    window._createStyleState = _createStyleState;

    if (window.sctx && window.vee) {
        var editor = window.vee;
        if (window._veeHandle) {
            window._veeHandle.remove();
        }

        window._runAction = function (action, editor) {

        };
        const _onchange = function (e) {
            return;
             0 && console.log('change');
            const item = e.item;
            if (item && item.dummy) {
                return;
            }
            var ctx = this.ctx;
            var actionStore = this.getActionStore();
            var mainToolbar = ctx.getWindowManager().getToolbar();
            var itemActions = item && item.getActions ? item.getActions() : null;
            var globals = actionStore.query({
                global: true
            });
            if (globals && !itemActions) {
                itemActions = [];
            }
            _.each(globals, (globalAction) => {
                var globalItemActions = globalAction.getActions ? globalAction.getActions(this.permissions, this) : null;
                if (globalItemActions && globalItemActions.length) {
                    itemActions = itemActions.concat(globalItemActions);
                }
            });
            // contextMenu && contextMenu.removeCustomActions();
            mainToolbar && mainToolbar.removeCustomActions();
            var result = [];
            var mixin = {
                addPermission: true,
                custom: true,
                quick: true
            };
            /*
            result.push(this.createAction({
                label: 'State',
                command: 'Widget/State',
                icon: 'fa-magic',
                group: 'Widget',
                tab: 'Widget',
                mixin: mixin
            }));
            */

            var newStoredActions = this.addActions(result);
            actionStore._emit('onActionsAdded', newStoredActions);
        };
        window._veeHandle = vee._on('selectionChanged', _onchange.bind(editor));
    }

    function _float(docker, panel, x, y, w, h) {

        docker.movePanel(panel, types.DOCKER.DOCK.FLOAT, null, {
            x: 300,
            y: 300,
            w: 600,
            h: 400
        });
        var frame = panel._parent;
        if (frame && frame.instanceOf('wcFrame')) {
            frame.pos(x || 300, y || 300, true);
            frame._size.x = w || 600;
            frame._size.y = h || 400;
        }
        frame.__update();

        if (frame && frame.instanceOf('wcFrame')) {
            frame.pos(x || 300, y || 300, true);
        }
        frame.__update();
        docker.__focus(frame);
    }
    const Impl = {
        declaredClass: "xideve/views/VisualEditorAction",
        _layoutActions: null,
        _designToggleAction: null,
        _maqActions: null,
        _float: function () {
            var thiz = this;
            var pe = thiz.getPageEditor();
            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getSourcePane();
            var propertyPane = pe.getPropertyPane();
            _float(docker, sourcePane, 300, 300);
            docker.movePanel(propertyPane, types.DOCKER.DOCK.STACKED, sourcePane);
            docker.movePanel(pe.getBlocksPane(), types.DOCKER.DOCK.STACKED, sourcePane);
            docker.movePanel(this.cssPane, types.DOCKER.DOCK.STACKED, sourcePane);
            propertyPane.maxSize(null, null);
            propertyPane.minSize(null, null);
            this.cssPane.select();
        },
        _dock: function () {

            var thiz = this;

            var pe = thiz.getPageEditor();

            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getDesignPane();
            var propertyPane = pe.getPropertyPane();


            docker.movePanel(propertyPane, types.DOCKER.DOCK.RIGHT, designPane, {
                tabOrientation: types.DOCKER.TAB.TOP,
                location: types.DOCKER.TAB.RIGHT
            });
            propertyPane.maxSize(400);
            propertyPane.minSize(350);

            docker.movePanel(sourcePane, types.DOCKER.DOCK.BOTTOM, null, {
                tabOrientation: types.DOCKER.TAB.TOP
            });
            docker.movePanel(pe.getBlocksPane(), types.DOCKER.DOCK.STACKED, sourcePane, {});
            docker.movePanel(this.cssPane, types.DOCKER.DOCK.STACKED, sourcePane, {});

            pe.resize();
            docker.resize();
            setTimeout(function () {
                sourcePane.getSplitter().pos(0.5);
                propertyPane.maxSize(500);
                designPane.getSplitter().pos(0.8);
                thiz.cssPane.select();
            }, 1500);
        },
        getWidgetBlockActions: function () {
            var result = [],
                thiz = this;
            var defaultMixin = {
                addPermission: true,
                quick: true
            }
            result.push(this.createAction({
                label: "Add Block",
                command: "Widget/Add Block",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-magic',
                mixin: defaultMixin,
                shouldDisable: function () {
                    var ctx = thiz.getEditorContext();
                    if (!ctx) {
                        return true;
                    }
                    var selection = thiz.getSelection();
                    return selection.length !== 1;
                }
            }));
            return result;
        },
        getMaqettaActions: function () {
            return this._maqActions;
        },
        getMaqettaAction: function (id, items, recursive) {

            items = items || this.getMaqettaActions();

            var action = _.find(items, {
                id: id
            });

            if (!action) {
                action = _.find(items, {
                    command: id
                });
            }

            if (action || (recursive === false && action)) {
                return action;
            }

            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.menu) {
                    action = this.getMaqettaAction(id, item.menu, false);
                    if (action) {
                        return action;
                    }
                }
            }

            return action;

        },
        onReload: function () {
            var info = this.contextInfo;
            var ctx = info.appContext.destroy();
        },
        reload: function (content) {
            var thiz = this;
            var dfd = new Deferred();
            this.onReload();
            var context = this.getEditorContext();
            //var context = this.getEditor()
            var rootWidget = context ? $(context.rootNode) : null;
            if (rootWidget && rootWidget[0] && rootWidget[0]._destroyHandles) {
                rootWidget[0].__didEmitLoad = false;
                rootWidget[0]._destroyHandles();
            }

            var appContext = context.appContext;
            if (appContext) {
                appContext.didWireWidgets = false;
            }

            if (thiz.blockScopes) {
                for (var i = 0; i < thiz.blockScopes.length; i++) {
                    var scope = thiz.blockScopes[i];
                    thiz.ctx.getBlockManager().removeScope(scope.id);
                }
                delete this.blockScopes;
            }
            var onReady = function (content) {
                thiz.editor.setContent(thiz.item.path, content);
                dfd.resolve(content);
            };
            if (!content) {
                thiz.ctx.getFileManager().getContent(thiz.item.mount, thiz.item.path, onReady);
            } else {
                onReady(content);
            }

            return dfd;
        },
        showInOutline: function (widget) {
            if (!widget) {
                return;
            }
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];


            widget = widget || firstItem;
            var outline = this.outlineGridView;
            outline.grid.select(widget.id, null, true, {
                focus: true,
                append: false,
                expand: true,
                delay: 1
            });
        },
        showCode: true,
        toogleCode: function () {
            return;
            if (this.showCode && this.getPageEditor()) {
                this.getPageEditor()._srcCP._parent.getSplitter().collapse();
            } else {
                this.getPageEditor()._srcCP._parent.getSplitter().expand();
            }
            this.showCode = !this.showCode;
            this.saveSettings();
        },
        runAction: function (action) {

            action = _.isString(action) ? this.getAction(action) : action;
            if (!action) {
                return;
            }
            debug &&  0 && console.log('run VE - action ' + action.command, action);

            var self = this,
                command = action.command,
                parts = command.split('/'),
                last = parts[parts.length - 1],
                ctx = this.getEditorContext();

            if (command.indexOf('Widget/State/') !== -1) {
                switch (command) {
                    case 'Widget/State/Add':
                        {
                            const command = action.command;
                            const ctx = this.getEditorContext();
                            const selection = this.getSelection();
                            const fn = window._createStyleState || _createStyleState;
                            return fn(selection[0], editor, ctx);
                        }
                }
            }
            if (command.indexOf('View/Show/Float') !== -1) {
                return this._float();
            }
            if (command.indexOf('View/Show/Dock') !== -1) {
                return this._dock();
            }
            if (command.indexOf('View/Show/Code') !== -1) {
                return this.toogleCode();
            }

            if (command.indexOf('Widget/Focus') !== -1) {
                var selection = this.getSelection(),
                    firstItem = selection ? selection[0].domNode : null;
                if (firstItem) {
                    firstItem.scrollIntoView();
                }
                return
            }

            if (command.indexOf('View/Split/') !== -1) {
                this.doSplit(parseInt(last));
                this.updateFrame();
                return true;
            }

            if (command === "Widget/Add Block") {
                var bEditor = this.getBlockEditor();
                if (bEditor) {
                    return bEditor.runAction({
                        command: 'File/New Group'
                    });
                }
            }
            if (command === 'View/Layout Mode/Relative') {
                return self.getPageEditor().selectLayoutFlow();
            }

            if (command === 'File/Reload') {
                return this.reload();
            }
            if (command === 'Widget/Show In Outline') {
                return this.showInOutline();
            }
            if (command === 'View/Layout Mode/Absolute') {
                return self.getPageEditor().selectLayoutAbsolute();
            }
            if (command === 'View/Show/Preview') {
                self._designMode = false;
                self.getPageEditor().getDesignPane().title("<span class='text-warning'>Preview</span>");
                self.getPageEditor().getDesignPane().icon('text-warning fa-eye');
                this._designToggle.set('value', true);
                return self.setDesignMode(self._designMode);
            }
            if (command === 'View/Show/Design') {
                self._designMode = true;
                self.getPageEditor().getDesignPane().title("Design");
                self.getPageEditor().getDesignPane().icon('text-info fa-edit');
                this._designToggle.set('value', false);
                return self.setDesignMode(self._designMode);
            }

            if (command === 'File/Open In Browser' || command === 'File/Preview') {

                var forceRelease = false;
                var css = this.item.name.replace('.dhtml', '.css');
                css = this.item.getParent().path + '/' + css;
                //console.error('css : ' + css);
                var VFS_GET_URL = this.editor.ctx.getResourceManager().getVariable('VFS_GET_URL');

                //subs['APP_CSS'] = VFS_GET_URL + '' + item.mount + '&path=' + encodeURIComponent(subs.SCENE_CSS);

                css = this.editor.ctx.getFileManager().base64_encode(VFS_GET_URL + '' + this.item.mount + '&path=' + css);

                //http://localhost/projects/x4mm/Code/xapp/xcf/index.php?view=smdCall&debug=true&run=run-release-debug&protocols=true&xideve=true&drivers=true&plugins=false&xblox=debug&files=true&dijit=debug&xdocker=debug&xfile=debug&davinci=debug&dgrid=debug&xgrid=debug&xace=debug&service=XCOM_Directory_Service.get2&callback=asdf&raw=html&attachment=0&send=1&mount=/workspace_user&path=default.css

                var params = {
                    template: has('debug') && forceRelease == false ? 'view.template.html' : 'view.template.release.html',
                    css: css,
                    baseOffset: encodeURIComponent(this.item.getParent().path.replace('./', '')),
                    userDirectory: encodeURIComponent(this.editor.ctx.getResourceManager().getVariable('USER_DIRECTORY'))
                };

                var url = this.ctx.getContextManager().getViewUrl(this.item, null, params);
                 0 && console.log('open scene with system : ' + url);
                if (has('electronx')) {
                    var _require = window['eRequire'];
                    if (!_require) {
                        console.error('have no electron');
                    }
                    var shell = _require("electron").shell;
                    return shell.openExternal(url);

                } else {
                    return window.open(url);
                }
            }

            if (command.indexOf('Widget/Align/') !== -1) {
                switch (command) {
                    case 'Widget/Align/Align Left':
                        return self._alignLeftVertical();
                    case 'Widget/Align/Align Center':
                        return self._alignCenterVertical();
                    case 'Widget/Align/Align Right':
                        return self._alignRightVertical();
                    case 'Widget/Align/Align Top':
                        return self._alignTopHorizontal();
                    case 'Widget/Align/Same Size':
                        return self._sameSize();
                    case 'Widget/Align/Same Width':
                        return self._sameWidth();
                    case 'Widget/Align/Same Height':
                        return self._sameHeight();
                }
                return true;
            }
            var mAction = action.action;
            if (!mAction) {
                switch (command) {
                    case 'File/Delete':
                    case 'Edit/Delete':
                        {
                            mAction = this.getMaqettaAction('delete');
                            break;
                        }
                    case 'Edit/Undo':
                        {
                            mAction = this.getMaqettaAction('undo');
                            break;
                        }
                    case 'Edit/Redo':
                        {
                            mAction = this.getMaqettaAction('redo');
                            break;
                        }
                    case 'Edit/Copy':
                        {
                            mAction = this.getMaqettaAction('copy');
                            break;
                        }
                    case 'Edit/Paste':
                        {
                            mAction = this.getMaqettaAction('paste');
                            break;
                        }
                    case 'Edit/Cut':
                        {
                            mAction = this.getMaqettaAction('cut');
                            break;
                        }
                    case "File/ManageStates":
                        {
                            mAction = this.getMaqettaAction('manageStates');
                            break;
                        }
                    case "File/Save As Widget":
                        {

                            mAction = this.getMaqettaAction('saveasdijit');
                            break;

                        }
                    case "Widget/SourroundWithDIV":
                        {
                            mAction = this.getMaqettaAction('Widget/SourroundWithDIV');
                            break;
                        }
                }
            }
            if (!mAction) {
                mAction = this.getMaqettaAction(command);
            }
            if (mAction) {
                var result = Workbench._runAction(mAction, ctx);
                return result;
            } else {
                 0 && console.warn('cant find maqetta action ' + command, [_.pluck(this._maqActions, 'id'), this._maqActions]);
            }
            return this.inherited(arguments);
        },
        _getIconClass: function (iconClassIn) {
            if (!iconClassIn) {
                return '';
            }

            switch (iconClassIn) {

                case "editActionIcon undoIcon":
                    {
                        return 'text-info fa-undo';
                    }
                case "editActionIcon redoIcon":
                    {
                        return 'text-info fa-repeat';
                    }

                case "editActionIcon editCutIcon":
                    {
                        return 'text-warning fa-scissors';
                    }
                case "editActionIcon editCopyIcon":
                    {
                        return 'text-warning fa-copy';
                    }
                case "editActionIcon editPasteIcon":
                    {
                        return 'text-warning fa-paste';
                    }
                case "editActionIcon editDeleteIcon":
                    {
                        return 'text-danger fa-remove';
                    }
            }
            return iconClassIn;
        },
        _toCommand: function (action) {
            switch (action) {
                case 'Undo':
                    return 'Edit/Undo';
                case 'Redo':
                    return 'Edit/Redo';
                case 'Cut':
                    return 'Edit/Cut';
                case 'Copy':
                    return 'Edit/Copy';
                case 'Paste':
                    return 'Edit/Paste';
                case 'Delete':
                    return 'Edit/Delete';
            }
        },
        __addAction: function (action) {
            switch (action.id) {
                case "openBrowser":
                    {
                        return false;
                    }
                case "documentSettings":
                    {
                        return false;
                    }
                case "stickynote":
                    {
                        return false;
                    }

                case "tableCommands":
                    {
                        return false;
                    }
            }
            return true;
        },
        _createEditToggleAction: function () {
            var thiz = this;
            var _toggle = Action.createDefault('Design', 'fa-toggle-on', 'View/Design Mode', 'viewActions', null, {
                widgetClass: 'xide/widgets/ToggleButton',
                widgetArgs: {
                    icon1: 'fa-toggle-on',
                    icon2: 'fa-toggle-off',
                    delegate: this,
                    checked: thiz._designMode
                },
                handler: function (command, item, owner, button) {
                    thiz._designMode = !thiz._designMode;
                    button.set('checked', thiz._designMode);
                    thiz.setDesignMode(thiz._designMode);

                }
            }).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null).
            setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                label: '',
                permanent: function () {
                    return thiz.destroyed == false;
                },
                widgetArgs: {
                    style: 'text-align:right;float:right;font-size:120%;'
                }
            }).
            setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {
                show: false
            });

            this._designToggleAction = _toggle;

            return _toggle;
        },
        _getItemActions: function () {
            try {

                if (!this.editor || !this.ready) {
                    return [];
                }

                this.restoreStyleView();

                var _actions = this._getViewActions();
                if (_actions && _actions[0]) {
                    _actions = _actions[0].actions;
                }


                var actions = [],
                    thiz = this;

                this._maqActions = _actions;

                debug &&  0 && console.log('ve-------------    get item actions', _actions);

                actions.push(this.getSplitViewAction());

                if (!this._designToggleAction) {
                    actions.push(this._createEditToggleAction());
                }

                var _runFunction = function (item) {
                    Workbench._runAction(item, context);
                };

                var _wireAction = function (action) {
                    action.handler = function () {
                        try {
                            _runFunction(action.action);
                        } catch (e) {
                            console.error('action failed ' + e, action);
                        }
                    };
                };

                var context = null;
                var layoutAction = null;
                if (this.editor && this.editor.currentEditor && this.editor.currentEditor.context) {
                    context = this.editor.currentEditor.context;
                }
                for (var i = 0; i < _actions.length; i++) {
                    var action = _actions[i];
                    if (_.isString(action.action)) {
                        Workbench._loadActionClass(action);
                    }
                    var disabled = false;
                    if (action.action && action.action.isEnabled) {
                        disabled = !action.action.isEnabled(context);
                    }
                    if (action.id === 'layout') {
                        layoutAction = action;
                    }
                    var _a = this._toAction(action);
                    if (!_a) {
                        continue;
                    }
                    _a.action = action;
                    _wireAction(_a);
                    actions.push(_a);
                }
                actions.push(Action.createDefault('Reload', 'fa-refresh', 'File/Reload', 'viewActions', null, {
                    addPermission: true,
                    handler: function () {

                        if (thiz.blockScopes) {
                            for (var i = 0; i < thiz.blockScopes.length; i++) {
                                var scope = thiz.blockScopes[i];
                                thiz.ctx.getBlockManager().removeScope(scope.id);
                            }
                            delete this.blockScopes;
                        }

                        var onReady = function (content) {
                            thiz.editor.setContent(thiz.item.path, content);
                        };
                        thiz.ctx.getFileManager().getContent(thiz.item.mount, thiz.item.path, onReady);
                    }
                }));
                var _ctx = this.getEditorContext();
                var selection = _ctx.getSelection();
                if (selection.length) {
                    var widget = selection[0];
                    Metadata.getSmartInput(widget.type).then(function (inline) {
                        if (inline && inline.show) {

                            actions.push(Action.createDefault('Wizard', 'fa-magic', 'Edit/Wizard', 'viewActions', null, {
                                handler: function () {
                                    inline.show(widget.id);
                                }
                            }));
                        }
                    });
                    actions = actions.concat(this.getWidgetLayoutActions(actions));
                }

                if (layoutAction) {
                    var root = Action.create('Layout', 'fa-crosshairs', 'View/Layout Mode', false, null, types.ITEM_TYPE.WIDGET, 'docActions', null, false, function () {}, {
                        dummy: true
                    });
                    actions.push(root);
                    var relative = Action.create('Relative', '', 'View/Layout Mode/Relative', false, null, types.ITEM_TYPE.WIDGET, 'docActions', null, false, function () {
                        //
                        thiz.getPageEditor().selectLayoutFlow();
                    });
                    actions.push(relative);
                    var absolute = Action.create('Absolute', '', 'View/Layout Mode/Absolute', false, null, types.ITEM_TYPE.WIDGET, 'docActions', null, false, function () {
                        thiz.getPageEditor().selectLayoutAbsolute();
                    });
                    actions.push(absolute);
                }

                if (!this._layoutActions) {
                    actions = actions.concat(this.getLayoutActions());
                }
                actions.push(Action.createDefault('Preview', 'text-success fa-eye', 'File/Open In Browser', 'viewActions', null, {
                    /*
                    handler: function () {
                        window.open(thiz.ctx.getContextManager().getViewUrl(thiz.item));
                    },*/
                    mixin: {
                        quick: true
                    }
                }).setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                    label: '',
                    widgetArgs: {
                        style: 'text-align:right;float:right;font-size:120%;'
                    }
                }));
                return actions;
            } catch (e) {
                return [];
            }
        },
        getSplitViewActions: function () {

            var result = [];
            return result;
            /*
                        var self = this,
                            root = 'View/Split',
                            VIEW_SPLIT_MODE = types.VIEW_SPLIT_MODE;

                        var modes = [{
                                mode: VIEW_SPLIT_MODE.DESIGN,
                                label: 'Design',
                                icon: 'fa-eye'
                            },
                            {
                                mode: VIEW_SPLIT_MODE.SOURCE,
                                label: 'Code',
                                icon: 'fa-code'
                            },
                            {
                                mode: VIEW_SPLIT_MODE.SPLIT_HORIZONTAL,
                                label: 'Horizontal',
                                icon: 'layoutIcon-horizontalSplit'
                            },
                            {
                                mode: VIEW_SPLIT_MODE.SPLIT_VERTICAL,
                                label: 'Vertical',
                                icon: 'layoutIcon-layout293'
                            }
                        ];
                        _.each(modes, function (mode) {
                            result.push(self.createAction({
                                label: mode.label,
                                command: root + '/' + mode.mode,
                                icon: mode.icon,
                                tab: 'Home',
                                group: 'View',
                                //keycombo:['ctrl '+mode.mode],
                                mixin: {
                                    addPermission: true,
                                    quick: true
                                }
                            }));
                        });

                        return result;
                        */

        },
        getStatableSelection() {
            const ret = [];
            var selection = this.getSelection();
            if (selection && selection.length === 1) {
                const widget = selection[0].domNode;
                if (widget && widget.getStates) {
                    ret.push(widget);
                }
            }
            return ret;
        },
        getStatableWidgetsSelection() {
            return this.getSelection().filter((widget) => {
                if (widget && widget.domNode && widget.domNode.getStates) {
                    return widget.domNode.getStates;
                }
            });
        },
        getFlowActions: function () {
            var result = [],
                self = this,

                root = 'View/Layout Mode';

            var defaultMixin = {
                addPermission: true,
                quick: true
            }
            result.push(this.createAction({
                label: 'Flow',
                command: root,
                icon: 'fa-crosshairs',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Relative',
                command: root + '/Relative',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Absolute',
                command: root + '/Absolute',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));
            return result;
        },
        getFloatActions: function () {
            var result = [];
            result.push(this.createAction({
                label: 'Float',
                command: 'View/Show/Float',
                icon: 'fa-compress',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            result.push(this.createAction({
                label: 'Dock',
                command: 'View/Show/Dock',
                icon: 'fa-expand',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            result.push(this.createAction({
                label: 'View/Hide Code',
                command: 'View/Show/Code',
                icon: 'fa-code',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            return result;
        },
        getItemActions: function (defaultActions) {
            var result = [],
                self = this,
                ctx = this.getEditorContext();

            result = result.concat(this.getSplitViewActions());
            result = result.concat(this.getFloatActions());
            result = result.concat(this.getWidgetBlockActions());

            var defaultMixin = {
                addPermission: true,
                quick: true,
                localize: false
            };
            result.push(this.createAction({
                label: "Save",
                command: "File/Save",
                icon: 'fa-save',
                group: 'File',
                tab: 'Home',
                keycombo: ['ctrl s'],
                mixin: defaultMixin
            }));
            /*
                        result.push(this.createAction({
                            label: 'State',
                            command: 'Widget/State',
                            icon: 'fa-magic',
                            group: 'Widget',
                            tab: 'Home',
                            mixin: defaultMixin
                        }));
                        result.push(this.createAction({
                            label: 'Add State',
                            command: 'Widget/State/Add',
                            icon: 'fa-magic',
                            group: 'Widget',
                            tab: 'Home',
                            mixin: defaultMixin,
                            owner: this
                        }));
                        */



            /*
            result.push(this.createAction({
                label:"Show Properties",
                command:"View/Show/Properties",
                icon:'fa-cogs',
                group:'View',
                tab:'Home',
                keycombo:['ctrl enter'],
                mixin:defaultMixin
            }));
            */
            result.push(this.createAction({
                label: "Select",
                command: "Widget/Select",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-hand-o-up',
                mixin: defaultMixin
            }));


            result.push(this.createAction({
                label: "Show In Outline",
                command: "Widget/Show In Outline",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-crosshairs',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: "Move",
                command: "Widget/Move",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-reorder',
                mixin: defaultMixin
            }));


            result = result.concat(this.getWidgetLayoutActions());
            result = result.concat(this.getEditToggleActions());
            result = result.concat(this.getFlowActions());
            result.push(this.createAction({
                label: "Focus",
                command: "Widget/Focus",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-crosshairs',
                mixin: defaultMixin
            }));
            ///////////////////////////////////////////////////////////
            //
            //  Normal view actions
            //
            //
            var _viewActions = this._getViewActions(defaultActions);
            if (_viewActions && _viewActions[0]) {
                _viewActions = _viewActions[0].actions;
            }

            this._maqActions = _viewActions;

            debug &&  0 && console.log('maq actions ', _viewActions);
            var context = this.getEditorContext();
            if (this.editor && this.editor.currentEditor && this.editor.currentEditor.context) {
                context = this.editor.currentEditor.context;
            }

            function completeAndAddAction(action) {
                var _a = self._toAction(action);
                if (!_a || !_a.command) {
                    return;
                }
                _a.tab = 'Home';
                _a.mixin = utils.mixin({}, defaultMixin);
                if (_a.command.indexOf("Widget/SourroundWith") !== -1) {
                    _a.mixin.quick = false;
                }
                _a.action = action;
                result.push(_a);

            }
            for (var i = 0; i < _viewActions.length; i++) {
                var action = _viewActions[i];
                Workbench._loadActionClass(action);
                var parms = {
                    showLabel: false /*, id:(id + "_toolbar")*/
                };
                ['label', 'showLabel', 'iconClass'].forEach(function (prop) {
                    if (action.hasOwnProperty(prop)) {
                        parms[prop] = action[prop];
                    }
                });
                if (action.className) {
                    parms['class'] = action.className;
                }
                if (action.menu && (action.type == 'DropDownButton' || action.type == 'ComboButton')) {
                    for (var ddIndex = 0; ddIndex < action.menu.length; ddIndex++) {
                        var menuItemObj = action.menu[ddIndex];
                        Workbench._loadActionClass(menuItemObj);
                        var menuItemParms = {};
                        var props = ['label', 'iconClass'];
                        props.forEach(function (prop) {
                            if (menuItemObj[prop]) {
                                menuItemParms[prop] = menuItemObj[prop];
                            }
                        });

                        completeAndAddAction(menuItemObj);
                    }

                } else if (action.toggle || action.radioGroup) {

                }
                var disabled = false;
                if (action.id === 'layout') {
                    layoutAction = action;
                }
                completeAndAddAction(action);

            }




            const widgetClass = dcl(TemplatedWidgetBase, {
                templateString: '<div class="CIActionWidget"></div>',
                widgetClass: ''
            });
            const updateState = (widget, value) => {
                const ctx = this.getEditorContext();
                const valuesObject = {};
                valuesObject['state'] = value;
                const command = new davinci.ve.commands.ModifyCommand(widget, valuesObject, null);
                dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                    source: ctx.editor_id,
                    command: command
                }]);

                let widgetStateNode = widget.domNode.getState(value);
                if (widgetStateNode) {
                    let stateWidget = ctx.toWidget(widgetStateNode);
                    if (stateWidget) {
                        ctx.select(stateWidget);
                    }
                }
            }

            const createActionWidgetClass = (action) => {
                return dcl(widgetClass, {
                    cis: [],
                    action: action,
                    startup: function () {
                        factory.renderCIS(this.cis, this.domNode, this).then((widgets) => {
                            widgets.forEach((w) => {
                                action.addReference(w);
                                w._on('change', (what) => {
                                    action.set('value', what);
                                    action.refresh();
                                    const selection = self.getStatableWidgetsSelection();
                                    selection.forEach((widget) => {
                                        if (what === 'No State') {
                                            //widget.domNode.setState('');
                                            updateState(widget, '');
                                        } else if (what === 'Add State' || what === 'Remove State') {

                                            if (what === 'Add State') {
                                                return _createStyleState(selection[0], self);
                                            }

                                            if (what === 'Remove State') {
                                                return _removeStyleState(selection[0], self);
                                            }

                                        } else {
                                            // widget.setState(what);
                                            updateState(widget, what);
                                        }
                                    });

                                });
                            });
                        });

                        setTimeout(() => {
                            action.refresh();
                        }, 1000)
                    },
                    render: function (data, $menu) {
                         0 && console.log('render');
                    }
                })
            }
            result.push(this.createAction({
                label: 'State',
                command: 'Widget/State',
                icon: 'fa-magic',
                group: 'Widget',
                tab: 'Widget',
                mixin: defaultMixin,
                onCreate: function (action) {
                    const clz = createActionWidgetClass(action);
                    const viz = types.ACTION_VISIBILITY.QUICK_LAUNCH;
                    action.setVisibility(viz, {
                        widgetClass: clz,
                        closeOnClick: false,
                        widgetArgs: {
                            cis: [
                                utils.createCI('', types.ECIType.ENUMERATION, '', {
                                    widget: {
                                        inline: true,
                                        options: [{
                                            value: 'No state support',
                                            label: 'No state support'
                                        }],
                                        widgetClass: "",
                                        action: action
                                    }
                                })
                            ]
                        }
                    });
                    action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, false);
                    action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, false);
                }
            }));

            return result;
        },
        getEditToggleActions: function () {
            var thiz = this;

            var result = [],
                self = this,

                root = 'View/Show';

            var _createStyleStatedefaultMixin = {
                addPermission: true,
                quick: true
            }
            
            /*
            result.push(this.createAction({
                label: 'Mode',
                command: root,
                icon: 'fa-eye',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));*/
            var defaultMixin = {
                addPermission: true,
                quick: true,
                localize: false
            };
            
            result.push(this.createAction({
                label: 'Edit',
                command: root + '/Design',
                icon: 'fa-edit',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Preview',
                command: root + '/Preview',
                icon: 'fa-eye',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));
            /*
                        result.push(this.createAction({
                            label: 'Preview',
                            command: 'File' + '/Preview',
                            icon: 'fa-eye',
                            tab: 'Home',
                            group: 'View',
                            mixin: defaultMixin
                        }));*/

            /*

             var _toggle = Action.createDefault('Design', 'fa-toggle-on', 'View/Design Mode', 'viewActions', null, {
             widgetClass: 'xide/widgets/ToggleButton',
             widgetArgs: {
             icon1: 'fa-toggle-on',
             icon2: 'fa-toggle-off',
             delegate: this,
             checked: thiz._designMode
             },
             handler: function (command, item, owner, button) {
             thiz._designMode = !thiz._designMode;
             button.set('checked', thiz._designMode);
             thiz.setDesignMode(thiz._designMode);

             }
             }).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null).
             setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
             label: '',
             permanent:function(){
             return thiz.destroyed==false;
             },
             widgetArgs:{
             style:'text-align:right;float:right;font-size:120%;'
             }
             }).
             setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {show: false});

             this._designToggleAction = _toggle;
             */

            return result;
        },
        _toAction: function (action) {
            //var _default = Action.create(action.label, this._getIconClass(action.iconClass), this._toCommand(action.label), false, null, types.ITEM_TYPE.WIDGET, 'editActions', null, false);
            var cmd = action.command || this._toCommand(action.label) || action.id;
            if (!cmd) {
                return null;
            }
            var self = this;
            var _default = this.createAction({
                label: action.label,
                group: action.group || 'Ungrouped',
                icon: this._getIconClass(action.iconClass) || action.iconClass,
                command: cmd,
                tab: action.tab || 'Home',
                mixin: {
                    action: action,
                    addPermission: true,
                    quick: true,
                    actionId: action.id,
                    localize: false
                },
                shouldDisable: function () {
                    var ctx = self.getEditorContext();
                    if (ctx && action.action && action.action.isEnabled) {
                        return !action.action.isEnabled(ctx);
                    }
                    return false;
                }
            });

            switch (action.id) {
                case "documentSettings":
                    {
                        return null;
                    }
                case 'outline':
                    {
                        _default.group = 'Widget';
                        return _default;
                    }
                case 'undo':
                case 'redo':
                case 'cut':
                case 'copy':
                case 'paste':
                    {
                        _default.group = 'Edit';
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {show: false});
                        return _default;
                    }
                case 'delete':
                    {
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''});
                        _default.group = 'Organize';
                        return _default;

                    }
                case 'theme':
                case 'rotateDevice':
                case 'chooseDevice':
                case 'stickynote':
                case 'savecombo':
                case 'showWidgetsPalette':
                case 'layout':
                case 'sourcecombo':
                case 'design':
                case 'closeactiveeditor':
                case 'tableCommands':
                    {
                        return null;
                    }
            }
            debug &&  0 && console.log('unknown action : ' + action.id, action);
            //_default.command=action.id;
            return _default;
        },
        getWidgetLayoutActions: function (actions) {
            var thiz = this;
            var result = [];

            function add(label, icon, command, handler) {
                result.push(thiz.createAction({
                    label: label,
                    icon: icon,
                    group: 'Widget',
                    tab: 'Home',
                    command: 'Widget/Align/' + command,
                    mixin: {
                        addPermission: true,
                        quick: true
                    }

                }));
            }

            var defaultMixin = {
                addPermission: true,
                quick: true,
                localize: false
            };

            result.push(this.createAction({
                label: "Align",
                command: "Widget/Align",
                icon: 'fa-arrows',
                group: 'Widget',
                tab: 'Home',
                mixin: defaultMixin,
                shouldDisable: function () {

                    var ctx = thiz.getEditorContext();
                    if (!ctx) {
                        return true;
                    }
                    var selection = thiz.getSelection();
                    return selection.length == 0 || selection.length < 2;
                }
            }));

            add('Align Left', 'layoutIcon-alignLeftVertical', 'Align Left', thiz._alignLeftVertical);
            add('Align Center', 'layoutIcon-alignCenterVertical', 'Align Center', thiz._alignCenterVertical);
            add('Align Right', 'layoutIcon-alignRightVertical', 'Align Right', thiz._alignRightVertical);
            add('Align Top', 'layoutIcon-alignTopHorizontal', 'Align Top', thiz._alignTopHorizontal);
            add('Align Bottom', 'layoutIcon-alignBottomHorizontal', 'Align Bottom', thiz._alignBottomHorizontal);
            add('Same Size', 'layoutIcon-sameSize', 'Same Size', thiz._sameSize);
            add('Same Width', 'layoutIcon-sameWidth', 'Same Width', thiz._sameWidth);
            add('Same Height', 'layoutIcon-sameHeight', 'Same Height', thiz._sameHeight);
            return result;
        },
        /**
         * _getViewActions extracts a xide compatible actions from maqetta's own action impl.
         * @returns {Array}
         * @private
         */
        _getViewActions: function () {
            var editorID = 'davinci.ve.HTMLPageEditor'; //this.editorExtension.id;
            var editorActions = [];
            var extensions = Runtime.getExtensions('davinci.editorActions', function (ext) {
                if (editorID == ext.editorContribution.targetID) {
                    editorActions.push(ext.editorContribution);
                    return true;
                }
            });
            if (editorActions.length == 0) {
                var extensions = Runtime.getExtension('davinci.defaultEditorActions', function (ext) {
                    editorActions.push(ext.editorContribution);
                    return true;
                });
            }
            var libraryActions = Metadata.getLibraryActions('davinci.editorActions', editorID);
            // Clone editorActions, otherwise, library actions repeatedly get appended to original plugin object
            editorActions = dojo.clone(editorActions);
            if (editorActions.length > 0 && libraryActions.length) {
                // We want to augment the action list, so let's clone the
                // action set before pushing new items onto the end of the
                // array
                dojo.forEach(libraryActions, function (libraryAction) {
                    var Workbench = require("davinci/Workbench");
                    if (libraryAction.action) {
                        Workbench._loadActionClass(libraryAction);
                    }
                    if (libraryAction.menu) {
                        for (var i = 0; i < libraryAction.menu.length; i++) {
                            var subAction = libraryAction.menu[0];
                            if (subAction.action) {
                                Workbench._loadActionClass(subAction);
                            }
                        }
                    }
                    editorActions[0].actions.push(libraryAction);
                });
            }


            return editorActions;
        },
        permissions: [
            ACTION.RENAME,
            ACTION.UNDO,
            ACTION.REDO,
            ACTION.RELOAD,
            // ACTION.DELETE,
            ACTION.CLIPBOARD,
            ACTION.LAYOUT,
            ACTION.COLUMNS,
            ACTION.SELECTION,
            ACTION.SAVE,
            ACTION.SEARCH,
            ACTION.TOOLBAR,
            'File/Properties',
            ACTION.GO_UP,
            ACTION.HEADER,
            ACTION.COPY,
            ACTION.CLOSE,
            ACTION.MOVE,
            ACTION.RENAME,
            ACTION.RELOAD,
            ACTION.CLIPBOARD,
            ACTION.LAYOUT,
            ACTION.SELECTION,
            ACTION.SEARCH,
            ACTION.OPEN_IN_TAB,
            ACTION.TOOLBAR,
            ACTION.SOURCE,
            ACTION.CLIPBOARD_COPY,
            ACTION.CLIPBOARD_PASTE,
            ACTION.CLIPBOARD_CUT
        ],
        _stateOptions: function (widgetStates) {
            const option = (label, value) => {
                return {
                    label: label,
                    value: value || label
                }
            };
            const all = [
                option('No State'),
                option('Add State'),
                option('Remove State')
            ]
            widgetStates.forEach((state) => {
                all.push(option(state.name));
            })
            return all;
        },
        refreshStateAction() {
            var action = this.getAction('Widget/State');
            var selection = this.getSelection() || [];
            const refs = action.getReferences();
            if (selection.length === 1) {
                const widget = selection[0].domNode;
                if (widget && widget.getStates) {
                    const states = widget.getStates();
                    const options = this._stateOptions(states);
                    refs.forEach((ref) => {
                        ref.set('options', options);
                    });
                    const state = widget.state || 'No State';
                    const found = _.find(options, {
                        value: state
                    })

                    refs.forEach((ref) => {
                        ref.set('value', found ? state : 'No State');
                    });

                } else {
                    refs.forEach((ref) => {
                        ref.set('options', [{
                            label: 'No state support',
                            value: -1
                        }]);
                        ref.set('value', -1);
                    });
                }
            } else {
                refs.forEach((ref) => {
                    ref.set('options', [{
                        label: 'No state support',
                        value: -1
                    }]);
                    ref.set('value', -1);
                    ref.set('disabled', true);
                });
            }
            // debugger;
        },
        startup: function () {
            var self = this;
            if (this._started) {
                //return;
            }
            // this._started = true;
            var _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
            _defaultActions = _defaultActions.concat(this.getItemActions(_defaultActions));

            // here to create the root branch ! 
            _defaultActions.push(this.createAction({
                label: 'Edit',
                command: 'Edit',
                icon: 'fa-edit',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));

            var _newActions = this.addActions(_defaultActions);
            debug &&  0 && console.log('add actions ', [_defaultActions, _newActions]);
            debug &&  0 && console.dir(_.pluck(_newActions, 'command'));

            this._on('selectionChanged', (e) => {
                window._vees = e;
                if (e.owner && e.owner.editor && e.owner.editor.getSelection) {
                    window.__selected(e.owner.editor);
                } else {
                    window.__selected(self);
                }
                this.refreshActions();
                this.refreshStateAction();
                if (this.updateFocus) {
                    setTimeout(() => {
                        this.updateFocus();
                    }, 500);
                }
            });

            if (this.inherited) {
                return this.inherited(arguments);
            }
        }
    }
    /*
        window['floatVE']=function(editor){

            var thiz = editor;
            var pe = thiz.getPageEditor();
            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getSourcePane();
            var propertyPane = pe.getPropertyPane();

            _float(docker,sourcePane,300,300);
            docker.movePanel(propertyPane,types.DOCKER.DOCK.STACKED,sourcePane);
            docker.movePanel(pe.getBlocksPane(),types.DOCKER.DOCK.STACKED,sourcePane);
            docker.movePanel(editor.cssPane,types.DOCKER.DOCK.STACKED,sourcePane);
            propertyPane.maxSize(null,null);
            propertyPane.minSize(null,null);
            //docker.movePanel(sourcePane,types.DOCKER.DOCK.STACKED,propertyPane);
        }

        window['dockVE']=function(editor) {

            var thiz = editor;
            var pe = thiz.getPageEditor();
            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getDesignPane();
            var propertyPane = pe.getPropertyPane();


            docker.movePanel(propertyPane,types.DOCKER.DOCK.RIGHT,designPane,{
                tabOrientation: types.DOCKER.TAB.TOP,
                location: types.DOCKER.TAB.RIGHT
            });
            propertyPane.maxSize(400);
            propertyPane.minSize(350);

            docker.movePanel(sourcePane,types.DOCKER.DOCK.BOTTOM,null,{
                tabOrientation: types.DOCKER.TAB.TOP
            });
            docker.movePanel(pe.getBlocksPane(),types.DOCKER.DOCK.STACKED,sourcePane,{});
            docker.movePanel(editor.cssPane,types.DOCKER.DOCK.STACKED,sourcePane,{});

            pe.resize();
            docker.resize();
            setTimeout(function(){
                sourcePane.getSplitter().pos(0.5);
                propertyPane.maxSize(500);
                designPane.getSplitter().pos(0.8);
            },1500);
        };
        */
    /**
     * @mixin module:xideve/views/VisualEditorAction
     */
    const Module = dcl(ActionProvider.dcl, Impl);
    Module.GridClass = declare('Actions', [ActionProvider], Impl);
    Module.Impl = Impl;
    return Module;
});
},
'xideve/views/VisualEditorSourceEditor':function(){
/** @module xideve/views/VisualEditorSourceEditor **/
define([
    "dcl/dcl",
    'xace/views/Editor',
    'xide/utils',
    "davinci/model/Factory"
], function (dcl,Editor, utils, Factory) {
    /**
     *
     * @mixin module:xideve/views/VisualEditorSourceEditor
     * @lends module:xideve/views/VisualEditor
     */
    return dcl(null, {
        declaredClass:"xideve/views/VisualEditorSourceEditor",
        createHTMLEditor:function(dstNode,fileName,item){
            var thiz = this;
            var editor = utils.addWidget(Editor, {
                __permissions:[],
                options:{
                    filePath: item.path
                },
                config: thiz.config,
                delegate: thiz,
                owner: thiz,
                parentContainer: dstNode,
                ctx: thiz.ctx,
                style: 'padding:0px;',
                item: item,
                resizeToParent:true,
                model: Factory.newHTML({url: fileName}),
                //registerView:false,
                selectModel: function (model) {
                    // 0 && console.log('select model:');
                    /*
                    if(model && model.length && model[0]['model'] && model[0]['model'].getText) {
                         0 && console.log('select model:  ' +model[0].model.getText(), model);
                    }*/
                },
                setVisible: function () {

                },//dummy
                setContent: function (filename, content) {
                    this.model.ctx = thiz.ctx;
                    this.model.fileName = filename;
                    this.model.setText(content);
                },
                store: null,
                /***
                 * Provide a text editor store delegate
                 */
                storeDelegate: {
                    getContent: function (onSuccess) {
                        thiz.ctx.getFileManager().getContent(item.mount, item.path, onSuccess);
                    },
                    saveContent: function (value, onSuccess, onError) {
                        thiz.ctx.getFileManager().setContent(item.mount, item.path, value, onSuccess);
                    }
                },
                title: 'notitle',
                closable: true
            }, this, dstNode, true);

            thiz.textEditor = editor;
            dstNode.add(editor);
            return editor;
        }
    });
});
},
'xace/views/Editor':function(){
/** @module xace/views/Editor **/
define([
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xaction/ActionProvider',
    'xace/views/ACEEditor',
    'xace/views/_Actions',
    'xaction/Toolbar',
    "xide/mixins/PersistenceMixin"
], function (dcl, types, utils, ActionProvider, ACEEditor, _Actions, Toolbar, PersistenceMixin) {

    var Persistence = dcl([PersistenceMixin.dcl], {
        declaredClass: 'xace.views.EditorPersistence',
        defaultPrefenceTheme: 'idle_fingers',
        defaultPrefenceFontSize: 14,
        saveValueInPreferences: true,
        getDefaultPreferences: function () {
            return utils.mixin(
                {
                    theme: this.defaultPrefenceTheme,
                    fontSize: this.defaultPrefenceFontSize
                },
                this.saveValueInPreferences ? {value: this.get('value')} : null);
        },
        onAfterAction: function (action) {
            var _theme = this.getEditor().getTheme();
            this.savePreferences({
                theme: _theme.replace('ace/theme/', ''),
                fontSize: this.getEditor().getFontSize()
            });
            return this.inherited(arguments);
        },
        /**
         * Override id for pref store:
         * know factors:
         *
         * - IDE theme
         * - per bean description and context
         * - by container class string
         * - app / plugins | product / package or whatever this got into
         * -
         **/
        toPreferenceId: function (prefix) {
            prefix = prefix;
            if (!prefix) {
                var body = $('body'), prefix = 'xTheme-', search;
                _.each(['blue', 'gray', 'white', 'transparent'], function (theme) {
                    search = prefix + theme
                    if (body.hasClass(search)) {
                        prefix = search;
                    }
                });
            }
            return (prefix || this.cookiePrefix || '') + '_xace';
        },
        getDefaultOptions: function () {
            //take our defaults, then mix with prefs from store,
            var _super = this.inherited(arguments),
                _prefs = this.loadPreferences(null);
            (_prefs && utils.mixin(_super, _prefs) ||
            //else store defaults
            this.savePreferences(this.getDefaultPreferences()));
            return _super;
        }
    });
    /**
     * Default Editor with all extras added : Actions, Toolbar and ACE-Features
     @class module:xgrid/Base
     */
    var Module = dcl([_Actions, ACEEditor, ActionProvider.dcl, Persistence, Toolbar.dcl], {
        toolbarArgs:{
            actionFilter:{}
        },
        getBreadcrumbPath: function () {
            if (this.item) {
                return {
                    path: utils.replaceAll('/', '', this.item.mount) + ':/' + this.item.path.replace('./', '/')
                }
            }
        },
        tabOrder: {
            'Home': 100,
            'View': 50,
            'Settings': 20
        },
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Editor': 9,
            'Step': 5,
            'New': 4,
            'Window': 3,
            'Help': 2
        },
        declaredClass: 'xace/views/Editor',
        options: null,
        /**
         * The icon class when doing any storage operation
         * @member loadingIcon {string}
         */
        loadingIcon: 'fa-spinner fa-spin',
        /**
         * The original icon class
         * @member iconClassNormal {string}
         */
        iconClassNormal: 'fa-code',
        templateString: '<div attachTo="template" class="grid-template widget" style="width: 100%;height: 100%;overflow: hidden !important;position: relative;padding: 0px;margin: 0px">' +
        '<div attachTo="header" class="view-header row bg-opaque" style="height: auto;width:inherit;height:auto;min-height: 33px"></div>' +
        '<div attachTo="aceNode" class="view-body row" style="height:100%;width: 100%;position: relative;"></div>' +
        '<div attachTo="footer" class="view-footer" style="position: absolute;bottom: 0px;width: 100%"></div></div>',
        getContent: function (item, onSuccess, onError) {
            if (!this.storeDelegate) {
                onError && onError('Editor::getContent : Have no store delegate!');
            } else {
                this.storeDelegate.getContent(function (content) {
                    onSuccess(content);
                }, item || this.item);
            }
        },
        saveContent: function (value, onSuccess, onError) {
            var thiz = this;
            this.set('iconClass', 'fa-spinner fa-spin');
            var _value = value || this.get('value');
            if (!_value) {
                 0 && console.warn('Editor::saveContent : Have nothing to save, editor seems empty');
            }
            if (!this.storeDelegate) {
                if (onError) {
                    onError('Editor::saveContent : Have no store delegate!');
                }
                return false;
            } else {
                return this.storeDelegate.saveContent(_value, function () {
                    thiz.set('iconClass', thiz.iconClassNormal);
                    thiz.lastSavedContent = _value;
                    thiz.onContentChange(_value);
                    var struct = {
                        path: thiz.options.filePath,
                        item: thiz.item,
                        content: _value,
                        editor: thiz
                    };
                    thiz.publish(types.EVENTS.ON_FILE_CONTENT_CHANGED, struct, thiz);
                }, null, thiz.item);
            }
        },
        addCommands: function () {
            var aceEditor = this.getAce(),
                thiz = this;

            var config = ace.require("ace/config");
            config.init();
            var commands = [];
            if (this.hasHelp) {
                commands.push({
                    name: "showKeyboardShortcuts",
                    bindKey: {win: "Ctrl-Alt-h", mac: "Command-Alt-h"},
                    exec: function (editor) {
                        thiz.showHelp(editor);
                    }
                });
            }
            if (this.hasConsole) {
                commands.push({
                    name: "gotoline",
                    bindKey: {win: "Ctrl-L", mac: "Command-L"},
                    exec: function (editor, line) {
                        if (typeof line == "object") {
                            var arg = this.name + " " + editor.getCursorPosition().row;
                            editor.cmdLine.setValue(arg, 1);
                            editor.cmdLine.focus();
                            return;
                        }
                        line = parseInt(line, 10);
                        if (!isNaN(line))
                            editor.gotoLine(line);
                    },
                    readOnly: true
                });
            }
            aceEditor.commands.addCommands(commands);
        },
        getWebRoot: function () {
            return this.ctx.getResourceManager().getVariable(types.RESOURCE_VARIABLES.APP_URL);
        },
        resize: function () {
            return this._resize();
        },
        onResize: function () {
            return this._resize();
        },
        _resize: function () {
            var parent = this.getParent();
            if (!this._isMaximized) {
                parent && utils.resizeTo(this, parent, true, true);
            } else {
                utils.resizeTo(this, this._maximizeContainer, true, true);
            }

            var thiz = this,
                toolbar = this.getToolbar(),
                noToolbar = false,
                topOffset = 0,
                aceNode = $(this.aceNode);

            if (this._isMaximized && toolbar) {
                utils.resizeTo(toolbar, this.header, true, true);
            }

            if (!toolbar || (toolbar && toolbar.isEmpty())) {
                noToolbar = true;
            } else {
                if (toolbar) {
                    utils.resizeTo(toolbar, this.header, false,true);
                    toolbar.resize();
                    utils.resizeTo(this.header,toolbar,true,false);
                }
            }

            var totalHeight = $(thiz.domNode).height(),
                topHeight = noToolbar == true ? 0 : $(thiz.header).height(),
                footerHeight = $(thiz.footer).height(),
                finalHeight = totalHeight - topHeight - footerHeight;

            if (toolbar) {
                finalHeight -= 4;
            }
            if (finalHeight > 50) {
                aceNode.height(finalHeight + 'px');
            } else {
                aceNode.height('inherited');
            }
        },
        __set: function (what, value) {
            var _res = this.inherited(arguments);
            if (what === 'iconClass') {
                var _parent = this._parent;
                if (_parent && _parent.icon) {
                    this._parent.icon(value);
                }
            }
            return _res;
        },
        __get: function (what) {
            if (what === 'value') {
                var self = this,
                    editor = self.getEditor(),
                    session = editor ? editor.session : null;
                return session ? session.getValue() : null;
            }
            return this.inherited(arguments);
        }
    });

    //pass through defaults
    Module.DEFAULT_PERMISSIONS = _Actions.DEFAULT_PERMISSIONS;

    return Module;

});
},
'xace/views/ACEEditor':function(){
/** @module xace/views/ACEEditor **/
define([
    'dcl/dcl',
    'dojo/has',
    'dojo/dom-construct',
    'xide/utils',
    'xide/types',
    'xide/widgets/TemplatedWidgetBase',
    'xace/views/_Split',
    'xaction/DefaultActions',
    'xace/views/_AceMultiDocs'
], function (dcl, has, domConstruct,utils, types, TemplatedWidgetBase, Splitter, DefaultActions, _AceMultiDocs) {
    var _loadedModes = {};//global cache for loaded modes
    var containerClass = dcl([TemplatedWidgetBase], {
        resizeToParent: true,
        templateString: '<div attachTo="aceNode" style="width: 100%;height: 100%" class=""></div>'
    });

    var EditorInterfaceImplementation = dcl(_AceMultiDocs, {
        declaredClass: 'xace/views/EditorInterface',
        editorSession: null,
        _lastValue:null,
        enableMultiDocuments: function () {
            var completer = this.addFileCompleter();
            var text = "var xxxTop = 2*3;";
            var path = "asdf.js";
            completer.addDocument(path, text);
        },
        loadScript: function (url, attributes, readyCB) {
            // DOM: Create the script element
            var jsElm = document.createElement("script");
            // set the type attribute
            jsElm.type = "application/javascript";
            // make the script element load file
            jsElm.src = url;
            jsElm.onload = function () {
                if (readyCB) {
                    readyCB();
                }
            };
            // finally insert the element to the body element in order to load the script
            document.body.appendChild(jsElm);
        },
        setMode: function (mode, _ctx, cb) {
            var ctx = _ctx || this.ctx || window['sctx'];
            if (ctx && ctx.getResourceManager()) {
                var thiz = this;
                if (!_loadedModes[mode]) {
                    var modeFile = null;
                    var aceRoot = ctx.getResourceManager().getVariable(types.RESOURCE_VARIABLES.ACE);
                    if (!aceRoot) {
                        var webRoot = ctx.getResourceManager().getVariable(types.RESOURCE_VARIABLES.APP_URL);
                        if (has('debug') === true) {
                            webRoot += '/xfile/ext/ace/';
                        } else {
                            webRoot += '/xfile/ext/ace/'
                        }
                        modeFile = webRoot + '/mode-' + mode + '.js';
                    } else {
                        modeFile = aceRoot + '/mode-' + mode + '.js';
                    }
                    this.loadScript(modeFile, null, function () {
                        _loadedModes[mode] = true;//update cache
                        thiz.set('mode', mode);
                        cb && cb(mode);

                    });
                } else {
                    thiz.set('mode', mode);
                    cb && cb(mode);
                }

            } else {
                console.error('have no resource manager!');
            }
        },
        get: function (what) {
            if (what === 'value') {
                var self = this,
                    editor = self.getEditor(),

                    session = editor ? editor.session : null;

                return session ? session.getValue() : null;

            }
            return this.inherited(arguments);
        },
        set: function (key, value) {
            var self = this,
                editor = this.getEditor(),
                session = this.editorSession;

            if (key === 'iconClass') {
                var _parent = this._parent;
                if (_parent && _parent.icon) {
                    this._parent.icon(value);
                }
            }

            if (editor && session) {
                // 0 && console.log('set ace option ' + key,value);
                var node = editor.container;
                if (key == 'item') {
                    session.setUseWorker(false);
                    self.getContent(value,
                        function (content) {
                            var newMode = self._getMode(value.path);
                            self.set('value', content);
                            self.setMode(newMode);
                            session.setUseWorker(self.options.useWorker);
                        });
                }

                if (key == "value" && value) {
                    session.setValue(value);
                }
                else if (key == "theme") {
                    if (typeof value == "string") {
                        value = "ace/theme/" + value;
                    }
                    editor.setTheme(value);
                    if (this.split) {
                        this.split.setTheme(value);
                    }
                }
                else if (key == "mode") {
                    try {
                        if ( 1 ) {
                            ace.require(["ace/mode/" + value], function (modeModule) {
                                if (modeModule && modeModule.Mode) {
                                    self.split.$editors.forEach(function (editor) {
                                        editor.session.setMode(new modeModule.Mode());
                                    });
                                }
                            });
                        }
                    } catch (e) {
                        console.error('ace mode failed : ' + value);
                    }
                }
                else if (key == "readOnly") {
                    editor.setReadOnly(value);
                }
                else if (key == "tabSize") {
                    session.setTabSize(value);
                }
                else if (key == "softTabs") {
                    session.setUseSoftTabs(value);
                }
                else if (key == "wordWrap") {
                    session.setUseWrapMode(value);
                }
                else if (key == "printMargin") {
                    editor.renderer.setPrintMarginColumn(value);
                }
                else if (key == "showPrintMargin") {
                    editor.setShowPrintMargin(value);
                }
                else if (key == "highlightActiveLine") {
                    editor.setHighlightActiveLine(value);
                }
                else if (key == "fontSize") {
                    $(node).css(key, value);
                }
                else if (key == "showIntentGuides") {
                    editor.setDisplayIndentGuides(value);
                }
                else if (key == "elasticTabstops") {
                    editor.setOption("useElasticTabstops", value);
                }
                else if (key == "useIncrementalSearch") {
                    editor.setOption("useIncrementalSearch", value);
                }
                else if (key == "showGutter") {
                    editor.renderer.setShowGutter(value);
                }
            }
            return this.inherited("set", arguments);
        },
        _getMode: function (fileName) {
            fileName = fileName || this.fileName;
            var ext = 'javascript';
            if (fileName) {

                ext = utils.getFileExtension(fileName) || 'js';

                if (ext === 'js' || ext === 'xblox') {
                    ext = 'javascript';
                }

                if (ext === 'h' || ext === 'cpp') {
                    ext = 'c_cpp';
                }

                if (ext === 'html' || ext === 'cfhtml' || ext === 'dhtml') {
                    ext = 'html';
                }
                if (ext === 'cp') {
                    ext = 'xml';
                }
                if (ext === 'md') {
                    ext = 'markdown';
                }
                if (ext === 'hbs') {
                    ext = 'handlebars';
                }
            }
            return ext;
        },
        setOptions: function (options) {
            this.options = options;
            var self = this;
            _.each(options, function (value, name) {
                self.set(name, value);
            });

            var editor = this.getEditor();
            if (editor && options.aceOptions) {
                editor.setOptions(options.aceOptions);
                editor.session.setUseWorker(options.useWorker);
                editor.setOptions({
                    enableBasicAutocompletion: true,
                    enableSnippets: true,
                    enableLiveAutocompletion: true
                });
                this.setMode(options.mode);
            }
            return this.options;
        },
        getOptions: function () {
            return this.options;
        },
        onContentChange: function (value) {
            if(this._parent && this._parent.set){
                this._parent.set('changed',value!==this.lastSavedContent);
            }
        },
        onDidChange: function () {
            var value = this.get('value');
            if(this._lastValue!==value){
                this._lastValue = value;
                this._emit('change',value);
                this.onContentChange(value);
            }
        },
        getDefaultOptions: function (value, mixin) {
            var thiz = this;
            return utils.mixin({
                region: "center",
                style: "margin: 0; padding: 0; position:relative;overflow: auto;height:inherit;width:inherit;text-align:left;",
                readOnly: false,
                tabSize: 2,
                wordWrap: false,
                showPrintMargin: false,
                highlightActiveLine: true,
                fontSize: 16,
                showGutter: true,
                showLineNumbers: true,
                useWorker: true,
                showInvisibles: false,
                displayIndentGuides: true,
                useSoftTabs: true,
                fileName: 'none',
                mode: 'javascript',
                value: value || this.value || 'No value',
                theme: 'idle_fingers',
                splits: 1,
                useElasticTabstops: false,
                animatedScroll: false,
                highlightActive: true,
                aceOptions: {
                    enableBasicAutocompletion: true,
                    enableSnippets: true,
                    enableLiveAutocompletion: true
                },
                onLoad: function (_editor) {
                    // This is to remove following warning message on console:
                    // Automatically scrolling cursor into view after selection change this will be disabled in the next version
                    // set editor.$blockScrolling = Infinity to disable this message
                    _editor.$blockScrolling = Infinity;
                },
                onDidChange: function () {
                    var value = thiz.get('value');
                    if(value!==thiz.lastSavedContent) {
                        thiz.onContentChange(value);
                    }
                },
                onPrefsChanged: function () {
                    thiz.setPreferences && thiz.setPreferences();
                }
            }, mixin);
        },
        getEditor: function (index) {
            if (this.split) {
                return index == null ? this.split.getCurrentEditor() : this.split.getEditor(index != null ? index : 0);
            } else if (this.editor) {
                return this.editor;
            }
        },
        resize: function (what, target, event) {
            var options = this.options || {};

            this.onResize && this.onResize();

            function _resize() {
                var editor = this.getEditor(),
                    widget = this.split || this.editor;
                if (!editor || !this.aceNode || !editor.container) {
                    this['resize_debounced'].cancel();
                    this['resize_debounced'] = null;
                    return;
                }
                editor && utils.resizeTo(editor.container, this.aceNode, true, true);
                return widget ? widget.resize() : null;
            }
            return this.debounce('resize', _resize.bind(this), options.resizeDelay || 300, null);
            //_resize().bind(this);
        },
        getAce: function () {
            return this.getEditor();
        },
        addBasicCommands: function (editor) {
            editor = editor || this.getEditor();
            var thiz = this,
                whiteSpace = ace.require("ace/ext/whitespace");

            this._whiteSpaceExt = whiteSpace;


            editor.commands.addCommands(whiteSpace.commands);

            editor.commands.addCommands([
                {
                    name: "gotoline",
                    bindKey: {win: "Ctrl-L", mac: "Command-L"},
                    exec: function (editor, line) {
                        if (typeof line == "object") {
                            var arg = this.name + " " + editor.getCursorPosition().row;
                            editor.cmdLine.setValue(arg, 1);
                            editor.cmdLine.focus();
                            return;
                        }
                        line = parseInt(line, 10);
                        if (!isNaN(line))
                            editor.gotoLine(line);
                    },
                    readOnly: true
                },
                {
                    name: "snippet",
                    bindKey: {win: "Alt-C", mac: "Command-Alt-C"},
                    exec: function (editor, needle) {
                        if (typeof needle == "object") {
                            editor.cmdLine.setValue("snippet ", 1);
                            editor.cmdLine.focus();
                            return;
                        }
                        var s = snippetManager.getSnippetByName(needle, editor);
                        if (s)
                            snippetManager.insertSnippet(editor, s.content);
                    },
                    readOnly: true
                },
                {
                    name: "increaseFontSize",
                    bindKey: "Ctrl-+",
                    exec: function (editor) {
                        editor.setFontSize(editor.getFontSize() + 1);
                        thiz.onAfterAction();
                    }
                }, {
                    name: "decreaseFontSize",
                    bindKey: "Ctrl+-",
                    exec: function (editor) {
                        editor.setFontSize(editor.getFontSize() - 1);
                        thiz.onAfterAction();
                    }
                }, {
                    name: "resetFontSize",
                    bindKey: "Ctrl+0",
                    exec: function (editor) {
                        editor.setFontSize(12);
                        thiz.onAfterAction();
                    }
                }
            ]);
        },
        onEditorCreated: function (editor, options) {
            var thiz = this;
            editor.getSelectedText = function () {
                return thiz.editorSession.getTextRange(this.getSelectionRange());
            };
            editor.on('change', function () {
                thiz.onDidChange(arguments);
            });
            this.addBasicCommands(editor);
            editor.setFontSize(options.fontSize);
            editor.$blockScrolling = Infinity;
            //var a = editor.session.doc.getNewLineCharacter();

            if(this._whiteSpaceExt){
                this._whiteSpaceExt.detectIndentation(editor.session);
            }

        },
        destroy: function () {
            var editor = this.getEditor();
            editor && editor.destroy();
            var _resize = this['resize_debounced'];
            if (_resize) {
                _resize.cancel();
            }
        },
        getOptionsMixed: function (_options) {
            var settings = this.getPreferences ? this.getPreferences() : {};
            var options = this.getDefaultOptions(this.value, _options || this.options);
            //apply overrides
            utils.mixin(options, _options);
            //apply settings from persistence store
            utils.mixin(options, settings);
            options.mode = this._getMode(options.fileName);
            return options;
        },
        createEditor: function (_options, value) {

            this.set('iconClass', this.iconClassNormal);

            if (this.editor || this.split) {
                return this.editor || this.split;
            }
            var settings = this.getPreferences ? this.getPreferences() : {};
            var options = this.getDefaultOptions(value);

            //apply overrides
            utils.mixin(options, _options);
            //apply settings from persistence store
            utils.mixin(options, settings);
            options.mode = this._getMode(options.fileName);

            var node = options.targetNode || domConstruct.create('div');
            $(node).css({
                padding: "0",
                margin: "0",
                height: '100%',
                width: '100%'
            });


            this.aceNode.appendChild(node);

            var config = ace.require("ace/config"),
                split = null,
                editor = null;
            ace.require("ace/ext/language_tools");
            try {
                var Split = Splitter.getSplitter();
                split = new Split(node, null, 1);
                this.split = split;
                this._aceConfig = config;
                config.init();
                //ace.require('ace/ext/language_tools');
                this.editor = editor = split.getEditor(0);
                this.editorSession = this.editor.getSession();
                if (value) {
                    this.editorSession.setValue(value);
                }
                split && split.setSplits(options.splits);
            } catch (e) {
                logError(e, 'error creating editor');
            }
            this.setOptions(options);
            this.onEditorCreated(editor, options);
            return editor;
        },
        addAutoCompleter: function (list) {
            var langTools = ace.require("ace/ext/language_tools");
            var rhymeCompleter = {
                getCompletions: function (editor, session, pos, prefix, callback) {
                    if (prefix.length === 0) {
                        callback(null, []);
                        return;
                    }
                    if (!list) {
                        callback(null, []);
                        return;
                    }
                    callback(null, list.map(function (ea) {
                        return {name: ea.value, value: ea.word, meta: ea.meta}
                    }));
                }
            };
            langTools.addCompleter(rhymeCompleter);
        }
    });

    var EditorClass = dcl(null, {
        declaredClass: 'xace/views/ACE',
        onLoaded: function () {
            this.set('iconClass', this.iconClassNormal);
        },
        getKeyTarget: function () {
            return this.aceNode;
        },
        startup: function () {
            this.aceNode.id = utils.createUUID();
            if (this.permissions) {
                var _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
                _defaultActions = _defaultActions.concat(this.getEditorActions(this.permissions));
                this.addActions(_defaultActions);
            }

            //save icon class normal
            this.iconClassNormal = '' + this.iconClass;
            this.set('iconClass', 'fa-spinner fa-spin');
            var self = this,
                options = this.options || {};

            if (!this.item && this.value == null) {
                return;
            }

            function createEditor(options, value) {
                /*
                if(typeof ace === "function"){
                    ace(function(_ace){
                        ACE = _ace;
                        self.createEditor(self.options || options, value);
                    });
                    return;
                }
                */
                self.createEditor(self.options || options, value);
            }




            if (this.value != null) {
                this.lastSavedContent = '' + this.value;
                createEditor(null, this.value);
            } else {
                //we have no content yet, call in _TextEditor::getContent, this will be forwarded
                //to our 'storeDelegate'
                this.getContent(
                    this.item,
                    function (content) {//onSuccess
                        self.lastSavedContent = content;
                        createEditor(options, content);
                    },
                    function (e) {//onError
                        createEditor(null, '');
                        logError(e, 'error loading content from file');
                    }
                );
            }
        }
    });
    var Module = dcl([containerClass, EditorClass, EditorInterfaceImplementation], {});
    Module.EditorImplementation = EditorInterfaceImplementation;
    Module.Editor = EditorClass;
    Module.Container = containerClass;
    Module.Splitter = Splitter;
    return Module;
});
},
'xace/views/_Split':function(){
define([
    'dcl/dcl'
],function (dcl){

    var _splitProto = null;
    var getSplitProto = function() {

        if(_splitProto){
            return _splitProto;
        }



        var require = ace.require;
        var oop = require("ace/lib/oop");
        var lang = require("ace/lib/lang");
        var EventEmitter = require("ace/lib/event_emitter").EventEmitter;

        var Editor = require("ace/editor").Editor;
        var Renderer = require("ace/virtual_renderer").VirtualRenderer;
        var EditSession = require("ace/edit_session").EditSession;
        var UndoManager = require("ace/undomanager").UndoManager;
        var HashHandler = require("ace/keyboard/hash_handler").HashHandler;

        var Split = function (container, theme, splits) {
            this.BELOW = 1;
            this.BESIDE = 0;

            this.$container = container;
            this.$theme = theme;
            this.$splits = 0;
            this.$editorCSS = "";
            this.$editors = [];
            this.$orientation = this.BESIDE;

            this.setSplits(splits || 1);
            this.$cEditor = this.$editors[0];


            this.on("focus", function (editor) {
                this.$cEditor = editor;
            }.bind(this));
        };

        (function () {

            oop.implement(this, EventEmitter);

            this.$createEditor = function () {
                var el = document.createElement("div");
                el.className = this.$editorCSS;
                el.style.cssText = "position: absolute; top:0px; bottom:0px";
                this.$container.appendChild(el);
                var editor = new Editor(new Renderer(el, this.$theme));


                editor.on("focus", function () {
                    this._emit("focus", editor);
                }.bind(this));

                this.$editors.push(editor);

                //var undoManager = editor.session.getUndoManager();
                editor.session.setUndoManager(new UndoManager());

                editor.setFontSize(this.$fontSize);
                return editor;
            };

            this.setSplits = function (splits) {
                var editor;
                if (splits < 1) {
                    throw "The number of splits have to be > 0!";
                }

                if(splits==1){

                }

                if (splits == this.$splits) {
                    return;
                } else if (splits > this.$splits) {
                    while (this.$splits < this.$editors.length && this.$splits < splits) {
                        editor = this.$editors[this.$splits];
                        this.$container.appendChild(editor.container);
                        editor.setFontSize(this.$fontSize);
                        this.$splits++;
                    }
                    while (this.$splits < splits) {
                        this.$createEditor();
                        this.$splits++;
                    }
                } else {
                    while (this.$splits > splits) {
                        editor = this.$editors[this.$splits - 1];
                        this.$container.removeChild(editor.container);
                        this.$splits--;
                    }
                }
                this.resize();
            };

            /**
             *
             * Returns the number of splits.
             * @returns {Number}
             **/
            this.getSplits = function () {
                return this.$splits;
            };

            /**
             * @param {Number} idx The index of the editor you want
             *
             * Returns the editor identified by the index `idx`.
             *
             **/
            this.getEditor = function (idx) {
                return this.$editors[idx];
            };

            /**
             *
             * Returns the current editor.
             * @returns {Editor}
             **/
            this.getCurrentEditor = function () {
                return this.$cEditor;
            };

            /**
             * Focuses the current editor.
             * @related Editor.focus
             **/
            this.focus = function () {
                this.$cEditor.focus();
            };

            /**
             * Blurs the current editor.
             * @related Editor.blur
             **/
            this.blur = function () {
                this.$cEditor.blur();
            };

            this.setSessionOption= function(what,value){
                this.$editors.forEach(function (editor) {

                    var session  =  editor.session;
                    if(what=='mode'){
                        session.setMode(value);
                    }

                });
            };
            /**
             *
             * @param {String} theme The name of the theme to set
             *
             * Sets a theme for each of the available editors.
             * @related Editor.setTheme
             **/
            this.setTheme = function (theme) {
                this.$editors.forEach(function (editor) {
                    editor.setTheme(theme);
                });
            };

            /**
             *
             * @param {String} keybinding
             *
             * Sets the keyboard handler for the editor.
             * @related editor.setKeyboardHandler
             **/
            this.setKeyboardHandler = function (keybinding) {
                this.$editors.forEach(function (editor) {
                    editor.setKeyboardHandler(keybinding);
                });
            };

            /**
             *
             * @param {Function} callback A callback function to execute
             * @param {String} scope The default scope for the callback
             *
             * Executes `callback` on all of the available editors.
             *
             **/
            this.forEach = function (callback, scope) {
                this.$editors.forEach(callback, scope);
            };


            this.$fontSize = "";
            /**
             * @param {Number} size The new font size
             *
             * Sets the font size, in pixels, for all the available editors.
             *
             **/
            this.setFontSize = function (size) {
                this.$fontSize = size;
                this.forEach(function (editor) {
                    editor.setFontSize(size);
                });
            };

            this.$cloneSession = function (session) {
                var s = new EditSession(session.getDocument(), session.getMode());

                var undoManager = session.getUndoManager();
                if (undoManager) {
                    var undoManagerProxy = new UndoManagerProxy(undoManager, s);
                    s.setUndoManager(undoManagerProxy);
                }

                // Overwrite the default $informUndoManager function such that new delas
                // aren't added to the undo manager from the new and the old session.
                s.$informUndoManager = lang.delayedCall(function () {
                    s.$deltas = [];
                });

                // Copy over 'settings' from the session.
                s.setTabSize(session.getTabSize());
                s.setUseSoftTabs(session.getUseSoftTabs());
                s.setOverwrite(session.getOverwrite());
                s.setBreakpoints(session.getBreakpoints());
                s.setUseWrapMode(session.getUseWrapMode());
                s.setUseWorker(session.getUseWorker());
                s.setWrapLimitRange(session.$wrapLimitRange.min,
                    session.$wrapLimitRange.max);
                s.$foldData = session.$cloneFoldData();

                return s;
            };

            /**
             *
             * @param {EditSession} session The new edit session
             * @param {Number} idx The editor's index you're interested in
             *
             * Sets a new [[EditSession `EditSession`]] for the indicated editor.
             * @related Editor.setSession
             **/
            this.setSession = function (session, idx) {
                var editor;
                if (idx == null) {
                    editor = this.$cEditor;
                } else {
                    editor = this.$editors[idx];
                }

                // Check if the session is used already by any of the editors in the
                // split. If it is, we have to clone the session as two editors using
                // the same session can cause terrible side effects (e.g. UndoQueue goes
                // wrong). This also gives the user of Split the possibility to treat
                // each session on each split editor different.
                var isUsed = this.$editors.some(function (editor) {
                    return editor.session === session;
                });

                if (isUsed) {
                    session = this.$cloneSession(session);
                }
                editor.setSession(session);

                // Return the session set on the editor. This might be a cloned one.
                return session;
            };

            /**
             *
             * Returns the orientation.
             * @returns {Number}
             **/
            this.getOrientation = function () {
                return this.$orientation;
            };

            /**
             *
             * Sets the orientation.
             * @param {Number} orientation The new orientation value
             *
             *
             **/
            this.setOrientation = function (orientation) {
                if (this.$orientation == orientation) {
                    return;
                }
                this.$orientation = orientation;
                this.resize();
            };

            /**
             * Resizes the editor.
             **/
            this.resize = function () {
                var width = this.$container.clientWidth;
                var height = this.$container.clientHeight;
                var editor;

                if (this.$orientation == this.BESIDE) {
                    var editorWidth = width / this.$splits;
                    if(this.diffGutter){
                        editorWidth -=60/this.$splits;
                    }


                    for (var i = 0; i < this.$splits; i++) {
                        editor = this.$editors[i];
                        editor.container.style.width = editorWidth + "px";
                        editor.container.style.top = "0px";
                        editor.container.style.left = i * editorWidth + "px";
                        if(i==1 && this.diffGutter){
                            editor.container.style.left = 60 +  (i * editorWidth) + "px";
                            this.diffGutter.style.left = i * editorWidth + "px";
                            /*this.diffGutter.style.height = height + "px";*/
                        }

                        //editor.container.style.height = height + "px";
                        if(!height){
                            return;
                        }
                        $(editor.container).css('height',height + "px");
                        var cNode = $(editor.container).find('.ace_content');
                        editor.resize();
                        cNode.css('height',height + "px");

                    }
                } else {
                    var editorHeight = height / this.$splits;
                    for (var i = 0; i < this.$splits; i++) {
                        editor = this.$editors[i];
                        editor.container.style.width = width + "px";
                        editor.container.style.top = i * editorHeight + "px";
                        editor.container.style.left = "0px";
                        editor.container.style.height = editorHeight + "px";
                        editor.resize();
                    }
                }
            };

        }).call(Split.prototype);


        function UndoManagerProxy(undoManager, session) {
            this.$u = undoManager;
            this.$doc = session;
        }

        (function () {
            this.execute = function (options) {
                this.$u.execute(options);
            };

            this.undo = function () {
                var selectionRange = this.$u.undo(true);
                if (selectionRange) {
                    this.$doc.selection.setSelectionRange(selectionRange);
                }
            };

            this.redo = function () {
                var selectionRange = this.$u.redo(true);
                if (selectionRange) {
                    this.$doc.selection.setSelectionRange(selectionRange);
                }
            };

            this.reset = function () {
                this.$u.reset();
            };

            this.hasUndo = function () {
                return this.$u.hasUndo();
            };

            this.hasRedo = function () {
                return this.$u.hasRedo();
            };
        }).call(UndoManagerProxy.prototype);

        _splitProto = Split;
        /*});*/
        return _splitProto;
    };
    var Module = dcl(null,{
        declaredClass:'xace/views/Split'
    });
    Module.getSplitter = getSplitProto;
    return Module;
});

},
'xace/views/_AceMultiDocs':function(){
define([
    "dcl/dcl",
    "xdojo/declare",
    "module",
    "xace/base_handler",
    "xace/complete_util"
],function (dcl,declare,module,baseLanguageHandler,completeUtil){

    var analysisCache = {}; // path => {identifier: 3, ...}
    var globalWordIndex = {}; // word => frequency
    var globalWordFiles = {}; // word => [path]
    var precachedPath;
    var precachedDoc;

    var completer = module.exports = Object.create(baseLanguageHandler);

    completer.handlesLanguage = function(language) {
        return true;
    };

    completer.handlesEditor = function() {
        return this.HANDLES_ANY;
    };

    completer.getMaxFileSizeSupported = function() {
        return 1000 * 1000;
    };

    function frequencyAnalyzer(path, text, identDict, fileDict) {
        var identifiers = text.split(/[^a-zA-Z_0-9\$]+/);
        for (var i = 0; i < identifiers.length; i++) {
            var ident = identifiers[i];
            if (!ident)
                continue;

            if (Object.prototype.hasOwnProperty.call(identDict, ident)) {
                identDict[ident]++;
                fileDict[ident][path] = true;
            }
            else {
                identDict[ident] = 1;
                fileDict[ident] = {};
                fileDict[ident][path] = true;
            }
        }
        return identDict;
    }

    function removeDocumentFromCache(path) {
        var analysis = analysisCache[path];
        if (!analysis) return;

        for (var id in analysis) {
            globalWordIndex[id] -= analysis[id];
            delete globalWordFiles[id][path];
            if (globalWordIndex[id] === 0) {
                delete globalWordIndex[id];
                delete globalWordFiles[id];
            }
        }
        delete analysisCache[path];
    }

    function analyzeDocument(path, allCode) {
        if (!analysisCache[path]) {
            if (allCode.size > 80 * 10000) {
                delete analysisCache[path];
                return;
            }
            // Delay this slightly, because in Firefox document.value is not immediately filled
            analysisCache[path] = frequencyAnalyzer(path, allCode, {}, {});
            // may be a bit redundant to do this twice, but alright...
            frequencyAnalyzer(path, allCode, globalWordIndex, globalWordFiles);
        }
    }

    completer.onDocumentOpen = function(path, doc, oldPath, callback) {
        if (!analysisCache[path]) {
            analyzeDocument(path, doc.getValue());
        }
        callback();
    };

    completer.addDocument = function(path, value) {
        if (!analysisCache[path]) {
            analyzeDocument(path, value);
        }

    };

    completer.onDocumentClose = function(path, callback) {
        removeDocumentFromCache(path);
        if (path == precachedPath)
            precachedDoc = null;
        callback();
    };

    completer.analyze = function(doc, ast, callback, minimalAnalysis) {
        if (precachedDoc && this.path !== precachedPath) {
            removeDocumentFromCache(precachedPath);
            analyzeDocument(precachedPath, precachedDoc);
            precachedDoc = null;
        }
        precachedPath = this.path;
        precachedDoc = doc;
        callback();
    };

    completer.complete = function(editor, fullAst, pos, currentNode, callback) {

        var doc = editor.getSession();
        var line = doc.getLine(pos.row);
        var identifier = completeUtil.retrievePrecedingIdentifier(line, pos.column, this.$getIdentifierRegex());
        var identDict = globalWordIndex;

        var allIdentifiers = [];
        for (var ident in identDict) {
            allIdentifiers.push(ident);
        }
        var matches = completeUtil.findCompletions(identifier, allIdentifiers);

        var currentPath = this.path;
        matches = matches.filter(function(m) {
            return !globalWordFiles[m][currentPath];
        });

        matches = matches.slice(0, 100); // limits results for performance

        callback(matches.filter(function(m) {
            return !m.match(/^[0-9$_\/]/);
        }).map(function(m) {
            var path = Object.keys(globalWordFiles[m])[0] || "[unknown]";
            var pathParts = path.split("/");
            var foundInFile = pathParts[pathParts.length-1];
            return {
                name: m,
                value: m,
                icon: null,
                score: identDict[m],
                meta: foundInFile,
                priority: 0,
                isGeneric: true
            };
        }));
    };

    completer.getCompletions=function (editor, session, pos, prefix, callback) {
        var completions = null;
        var _c = function(_completions){
            completions = _completions;
        };
        completer.complete(editor,null,pos,null,_c);
        callback(null,completions);
    };

    var Module  = dcl(null,{

        declaredClass:"xide.views._AceMultiDocs",
        didAddMCompleter:false,
        multiFileCompleter:null,
        addFileCompleter:function(){
            var compl = null;
            if(!this.didAddMCompleter) {
                compl = completer;
                this.multiFileCompleter= compl;
                var langTools = ace.require("ace/ext/language_tools");
                langTools.addCompleter(compl);
                this.didAddMCompleter=true;
            }
            return compl;
        }
    });

    Module.completer = completer;

    return Module;
});
},
'xace/base_handler':function(){
/**
 * This module is used as a base class for language handlers.
 * It provides properties, helper functions, and functions that
 * can be overridden by language handlers to implement
 * language services such as code completion.
 *
 * See {@link language} for an example plugin.
 *
 * @class language.base_handler
 */
define(function(require, exports, module) {

    module.exports = {

        /**
         * Indicates the handler handles editors, the immediate window,
         * and anything else.
         */
        HANDLES_ANY: 0,

        /**
         * Indicates the handler only handles editors, not the immediate window.
         */
        HANDLES_EDITOR: 1,

        /**
         * Indicates the handler only handles the immediate window, not editors.
         */
        HANDLES_IMMEDIATE: 2,

        /**
         * Indicates the handler only handles the immediate window, not editors.
         */
        HANDLES_EDITOR_AND_IMMEDIATE: 3,

        /**
         * The language this worker is currently operating on.
         * @type {String}
         */
        language: null,

        /**
         * The path of the file this worker is currently operating on.
         * @type {String}
         */
        path: null,

        /**
         * The current workspace directory.
         * @type {String}
         */
        workspaceDir: null,

        /**
         * The current document this worker is operating on.
         *
         * @type {Document}
         */
        doc: null,

        // UTILITIES

        /**
         * Utility function, used to determine whether a certain feature is enabled
         * in the user's preferences.
         *
         * Should not be overridden by inheritors.
         *
         * @deprecated Use worker_util#isFeatureEnabled instead
         *
         * @param {String} name  The name of the feature, e.g. "unusedFunctionArgs"
         * @return {Boolean}
         */
        isFeatureEnabled: function(name) {
            /*global disabledFeatures*/
            return !disabledFeatures[name];
        },

        /**
         * Utility function, used to determine the identifier regex for the
         * current language, by invoking {@link #getIdentifierRegex} on its handlers.
         *
         * Should not be overridden by inheritors.
         *
         * @deprecated Use worker_util#getIdentifierRegex instead
         *
         * @return {RegExp}
         */
        $getIdentifierRegex: function() {
            return null;
        },

        /**
         * Utility function, used to retrigger completion,
         * in case new information was collected and should
         * be displayed, and assuming the popup is still open.
         *
         * Should not be overridden by inheritors.
         *
         * @deprecated Use worker_util#completeUpdate instead
         *
         * @param {Object} pos   The position to retrigger this update
         * @param {String} line  The line that this update was triggered for
         */
        completeUpdate: function(pos) {
            throw new Error("Use worker_util.completeUpdate instead()"); // implemented by worker.completeUpdate
        },

        // OVERRIDABLE ACCESORS

        /**
         * Returns whether this language handler should be enabled for the given
         * file.
         *
         * Must be overridden by inheritors.
         *
         * @param {String} language   to check the handler against
         * @return {Boolean}
         */
        handlesLanguage: function(language) {
            throw new Error("base_handler.handlesLanguage() is not overridden");
        },

        /**
         * Returns whether this language handler should be used in a
         * particular kind of editor.
         *
         * May be overridden by inheritors; returns {@link #HANDLES_EDITOR}
         * by default.
         *
         * @return {Number} One of {@link #HANDLES_EDITOR},
         *                  {@link #HANDLES_IMMEDIATE}, or
         *                  {@link #HANDLES_EDITOR_AND_IMMEDIATE}, or
         *                  {@link #HANDLES_ANY}.
         */
        handlesEditor: function() {
            return this.HANDLES_EDITOR;
        },

        /**
         * Returns the maximum file size this language handler supports.
         * Should return Infinity if size does not matter.
         * Default is 10.000 lines of 80 characters.
         *
         * May be overridden by inheritors.
         *
         * @return {Number}
         */
        getMaxFileSizeSupported: function() {
            // Moderately conservative default (well, still 800K)
            return 10 * 1000 * 80;
        },

        /**
         * Determine if the language component supports parsing.
         * Assumed to be true if at least one hander for the language reports true.
         *
         * Should be overridden by inheritors.
         *
         * @return {Boolean}
         */
        isParsingSupported: function() {
            return false;
        },

        /**
         * Returns a regular expression for identifiers in the handler's language.
         * If not specified, /[A-Za-z0-9$_]/ is used.
         *
         * Note: to indicate dollars are allowed at the start of identifiers
         * (like with php $variables), include '$$'' in the regex, e.g.
         * /[A-Z0-9$$_]/.
         *
         * Should be overridden by inheritors that implement code completion.
         *
         * @return RegExp
         */
        getIdentifierRegex: function() {
            return null;
        },

        /**
         * Returns a regular expression used to trigger code completion.
         * If a non-null value is returned, it is assumed continous completion
         * is supported for this language.
         *
         * As an example, Java-like languages might want to use: /^\.$/
         *
         * Should be overridden by inheritors that implement code completion.
         * Default implementation returns null.
         *
         * @return RegExp
         */
        getCompletionRegex: function() {
            return null;
        },

        /**
         * Returns a regular expression used to trigger a tooltip.
         * Normally, tooltips after a scheduled analysis has been completed.
         * To avoid delays, this function can be used to trigger
         * analysis & tooltip fetching early.
         *
         * Should be overridden by inheritors that implement tooltips.
         * Default implementation returns null.
         *
         * @return RegExp
         */
        getTooltipRegex: function() {
            return null;
        },

        // PARSING AND ABSTRACT SYNTAX CALLBACKS

        /**
         * Parses the given document.
         *
         * Should be overridden by inheritors that implement parsing
         * (which is, like all features here, optional).
         *
         * @param value {String}   the source the document to analyze
         * @return {Object}        an abstract syntax tree (of any type), or null if not implemented
         */
        parse: function(value, callback) {
            callback();
        },

        /**
         * Finds a tree node at a certain row and column,
         * e.g. using the findNode(pos) function of treehugger.
         *
         * Should be overridden by inheritors that implement parsing.
         *
         * @param {Object} ast                An abstract syntax tree object from {@link #parse}
         * @param {Object} pos                The position of the node to look up
         * @param {Number} pos.row            The position's row
         * @param {Number} pos.column         The position's column
         * @param {Function} callback         The callback for the result
         * @param {Object} [callback.result]  The found node
         */
        findNode: function(ast, pos, callback) {
            callback();
        },

        /**
         * Returns the  a tree node at a certain row and col,
         * e.g. using the node.getPos() function of treehugger.
         *
         * Should be overridden by inheritors that implement parsing.
         *
         * @param {Object} node                The node to look up
         * @param {Function} callback          The callback for the result
         * @param {Object} [callback.result]   The resulting position
         * @param {Number} callback.result.sl  The starting line
         * @param {Number} callback.result.el  The ending line
         * @param {Number} callback.result.sc  The starting column
         * @param {Number} callback.result.ec  The ending column
         */
        getPos: function(node, callback) {
            callback();
        },

        // OTHER CALLBACKS

        /**
         * Initialize this language handler.
         *
         * May be overridden by inheritors.
         *
         * @param callback            The callback; must be called
         */
        init: function(callback) {
            callback();
        },

        /**
         * Invoked when the document has been updated (possibly after a certain delay)
         *
         * May be overridden by inheritors.
         *
         * @param {Document} doc  The current document
         * @param {Function} callback            The callback; must be called
         */
        onUpdate: function(doc, callback) {
            callback();
        },

        /**
         * Invoked when a new document has been opened.
         *
         * May be overridden by inheritors.
         *
         * @param {String} path        The path of the newly opened document
         * @param {String} doc         The Document object representing the source
         * @param {String} oldPath     The path of the document that was active before
         * @param {Function} callback  The callback; must be called
         */
        onDocumentOpen: function(path, doc, oldPath, callback) {
            callback();
        },

        /**
         * Invoked when a document is closed in the IDE.
         *
         * May be overridden by inheritors.
         *
         * @param {String} path the path of the file
         * @param {Function} callback  The callback; must be called
         */
        onDocumentClose: function(path, callback) {
            callback();
        },

        /**
         * Invoked when the cursor has been moved.
         *
         * May be overridden by inheritors that immediately act upon cursor moves.
         *
         * See {@link #tooltip} and {@link #highlightOccurrences}
         * for handler functions that are invoked after the cursor has been moved,
         * the document has been analyzed, and feedback is requested.
         *
         * @param {Document} doc                      Document object representing the source
         * @param {Object} fullAst                    The entire AST of the current file (if parsed already, otherwise null)
         * @param {Object} cursorPos                  The current cursor position
         * @param {Number} cursorPos.row              The current cursor's row
         * @param {Number} cursorPos.column           The current cursor's column
         * @param {Object} currentNode                The AST node the cursor is currently at (if parsed alreadty, and if any)
         * @param {Function} callback                 The callback; must be called
         * @paran {Object} callback.result            An optional result. Supports the same result objects as
         *                                            {@link #tooltip} and {@link #highlightOccurrences}
         */
        onCursorMove: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Invoked when the cursor has been moved inside to a different AST node.
         * Gets a tooltip to display when the cursor is moved to a particular location.
         *
         * Should be overridden by inheritors that implement tooltips.
         *
         * @param {Document} doc                               Document object representing the source
         * @param {Object} fullAst                             The entire AST of the current file (if any)
         * @param {Object} cursorPos                           The current cursor position
         * @param {Number} cursorPos.row                       The current cursor's row
         * @param {Number} cursorPos.column                    The current cursor's column
         * @param {Object} currentNode                         The AST node the cursor is currently at (if any)
         * @param {Function} callback                          The callback; must be called
         * @param {Object} callback.result                     The function's result
         * @param {Object|String} callback.result.hint         An object or HTML string with the tooltip to display
         * @param {Object[]} [callback.result.signatures]      One or more function signatures to show
         * @param {String} callback.result.signatures.name     Function name
         * @param {String} [callback.result.signatures.doc]    Function documentation
         * @param {Object[]} callback.result.signatures.parameters
         *                                                     Function parameters
         * @param {String} callback.result.signatures.parameters.name
         *                                                     Parameter name
         * @param {String} [callback.result.signatures.parameters.type]
         *                                                     Parameter type
         * @param {String} [callback.result.signatures.parameters.doc]
         *                                                     Parameter documentation
         * @param {String} [callback.result.signatures.returnType]
         *                                                     The function return type
         * @param {Object} callback.result.pos                 The position range for which this tooltip is valid
         * @param {Number} callback.result.pos.sl              The starting line
         * @param {Number} callback.result.pos.el              The ending line
         * @param {Number} callback.result.pos.sc              The starting column
         * @param {Number} callback.result.pos.ec              The ending column
         * @param {Object} [callback.result.displayPos]        The position to display this tooltip
         * @param {Number} [callback.result.displayPos.row]    The display position's row
         * @param {Number} [callback.result.displayPos.column] The display position's column
         */
        tooltip: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Gets the instances to highlight when the cursor is moved to a particular location.
         *
         * Should be overridden by inheritors that implement occurrence highlighting.
         *
         * @param {Document} doc                           Document object representing the source
         * @param {Object} fullAst                         The entire AST of the current file (if any)
         * @param {Object} cursorPos                       The current cursor position
         * @param {Number} cursorPos.row                   The current cursor's row
         * @param {Number} cursorPos.column                The current cursor's column
         * @param {Object} currentNode                     The AST node the cursor is currently at (if any)
         * @param {Function} callback                      The callback; must be called
         * @param {Object} callback.result                 The function's result
         * @param {Object[]} [callback.result.markers]     The occurrences to highlight
         * @param {Object} callback.result.markers.pos     The marker's position
         * @param {Number} callback.result.markers.pos.sl  The starting line
         * @param {Number} callback.result.markers.pos.el  The ending line
         * @param {Number} callback.result.markers.pos.sc  The starting column
         * @param {Number} callback.result.markers.pos.ec  The ending column
         * @param {Boolean} callback.result.isGeneric      Indicates this is generic highlighting and should be deferred
         * @param {"occurrence_other"|"occurrence_main"} callback.result.markers.type
         *                                                 The type of occurrence: the main one, or any other one.
         */
        highlightOccurrences: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Determines what refactorings to enable when the cursor is moved to a particular location.
         *
         * Should be overridden by inheritors that implement refactorings.
         *
         * @param {Document} doc                 Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Object} cursorPos             The current cursor position
         * @param {Number} cursorPos.row         The current cursor's row
         * @param {Number} cursorPos.column      The current cursor's column
         * @param {Object} currentNode           The AST node the cursor is currently at (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result
         * @param {String[]} callback.result.refactorings
         *                                       The refactorings to enable, such as "rename"
         * @param {String[]} [callback.result.isGeneric]
         *                                       Whether is a generic answer and should be deferred
         */
        getRefactorings: function(doc, fullAst, cursorPos, currentNode, callback) {
            callback();
        },

        /**
         * Constructs an outline.
         *
         * Example outline object:
         *
         *     {
     *          icon: 'method',
     *          name: "fooMethod",
     *          pos: this.getPos(),
     *          displayPos: { sl: 15, sc: 20 },
     *          items: [ ...items nested under this method... ],
     *          isUnordered: true
     *     }
         *
         * Should be overridden by inheritors that implement an outline.
         *
         * @param {Document} doc                           The Document object representing the source
         * @param {Object} fullAst                         The entire AST of the current file (if any)
         * @param {Function} callback                      The callback; must be called
         * @param {Object} callback.result                 The function's result, a JSON outline structure or null if not supported
         * @param {"event"|"method"|"method2"|"package"|"property"|"property2"|"unknown"|"unknown2"} callback.result.icon
         *                                                 The icon to display for the first outline item
         * @param {String} callback.result.name            The name to display for the first outline item
         * @param {Object} callback.result.pos             The item's range, e.g. the full visible range of a method
         * @param {Number} callback.result.pos.sl          The item's starting row
         * @param {Number} [callback.result.pos.el]        The item's ending row
         * @param {Number} [callback.result.pos.sc]        The item's starting column
         * @param {Number} [callback.result.pos.ec]        The item's ending column
         * @param {Object} [callback.result.displayPos]    The item's position of the text to select when it's picked from the outline
         * @param {Number} callback.result.displayPos.sl   The item's starting row
         * @param {Number} [callback.result.displayPos.el] The item's ending row
         * @param {Number} [callback.result.displayPos.sc] The item's starting column
         * @param {Number} [callback.result.displayPos.ec] The item's ending column
         * @param {Object[]} callback.result.items         Any items nested under the curent item.
         * @param {Boolean} [callback.result.isGeneric]    Indicates that this is a generic, language-independent outline
         * @param {Boolean} [callback.result.isUnordered]  Indicates the outline is not ordered by appearance of the items,
         *                                                 but that they're e.g. grouped as methods, properties, etc.
         */
        outline: function(doc, fullAst, callback) {
            callback();
        },

        /**
         * Constructs a hierarchy.
         *
         * Should be overridden by inheritors that implement a type hierarchy.
         *
         * Not supported right now.
         *
         * @param {Document} doc             The Document object representing the source
         * @param {Object} cursorPos         The current cursor position
         * @param {Number} cursorPos.row     The current cursor's row
         * @param {Number} cursorPos.column  The current cursor's column
         * @param {Function} callback        The callback; must be called
         * @param {Object} callback.result   A JSON hierarchy structure or null if not supported
         */
        hierarchy: function(doc, cursorPos, callback) {
            callback();
        },

        /**
         * Performs code completion for the user based on the current cursor position.
         *
         * Should be overridden by inheritors that implement code completion.
         *
         * Example completion result:
         * {
     *    name        : "foo()",
     *    replaceText : "foo()",
     *    icon        : "method",
     *    meta        : "FooClass",
     *    doc         : "The foo() method",
     *    docHead     : "FooClass.foo",
     *    priority    : 1
     *  };
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Object} pos                   The current cursor position
         * @param {Number} pos.row               The current cursor's row
         * @param {Number} pos.column            The current cursor's column
         * @param {Object} currentNode           The AST node the cursor is currently at (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result, an array of completion matches
         * @param {String} callback.result.name  The full name to show in the completion popup
         * @param {String} [callback.result.id]  The short name that identifies this completion
         * @param {String} callback.result.replaceText
         *                                       The text to replace the selection with
         * @param {"event"|"method"|"method2"|"package"|"property"|"property2"|"unknown"|"unknown2"}
         *        [callback.result.icon]
         *                                       The icon to use
         * @param {String} callback.result.meta  Additional information to show
         * @param {String} callback.result.doc   Documentation to display
         * @param {String} callback.result.docHead
         *                                       Documentation heading to display
         * @param {Number} callback.result.priority
         *                                       Priority of this completion suggestion
         * @param {Boolean} callback.result.isGeneric
         *                                       Indicates that this is a generic, language-independent
         *                                       suggestion
         * @param {Boolean} callback.result.isContextual
         *                                       Indicates that this is a contextual completion,
         *                                       and that any generic completions should not be shown
         */
        complete: function(doc, fullAst, pos, currentNode, callback) {
            callback();
        },

        /**
         * Analyzes an AST or file and annotates it as desired.
         *
         * Example of an annotation to return:
         *
         *     {
     *         pos: { sl: 1, el: 1, sc: 4, ec: 5 },
     *         type: "warning",
     *         message: "Assigning to undeclared variable."
     *     }
         *
         * Should be overridden by inheritors that implement analysis.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result, an array of error and warning markers
         * @param {Boolean} [minimalAnalysis]    Fast, minimal analysis is requested, e.g.
         *                                       for code completion or tooltips.
         */
        analyze: function(value, fullAst, callback, minimalAnalysis) {
            callback();
        },

        /**
         * Gets all positions to select for a rename refactoring.
         *
         * Example result, renaming a 3-character identfier
         * on line 10 that also occurs on line 11 and 12:
         *
         *     {
     *         length: 3,
     *         pos: {
     *             row: 10,
     *             column: 5
     *         },
     *         others: [
     *             { row: 11, column: 5 },
     *             { row: 12, column: 5 }
     *         ]
     *     }
         *
         * Must be overridden by inheritors that implement rename refactoring.
         *
         * @param {Document} doc                          The Document object representing the source
         * @param {Object} ast                            The entire AST of the current file (if any)
         * @param {Object} pos                            The current cursor position
         * @param {Number} pos.row                        The current cursor's row
         * @param {Number} pos.column                     The current cursor's column
         * @param {Object} currentNode                    The AST node the cursor is currently at (if any)
         * @param {Function} callback                     The callback; must be called
         * @param {Object} callback.result                The function's result (see function description).
         * @param {Boolean} callback.result.isGeneric     Indicates this is a generic refactoring and should be deferred.
         * @param {Boolean} callback.result.length        The lenght of the rename identifier
         * @param {Object} callback.result.pos            The position of the current identifier
         * @param {Number} callback.result.pos.row        The row of the current identifier
         * @param {Number} callback.result.pos.column     The column of the current identifier
         * @param {Object[]} callback.result.others       The positions of other identifiers to be renamed
         * @param {Number} callback.result.others.row     The row of another identifier to be renamed
         * @param {Number} callback.result.others.column  The column of another identifier to be renamed
         */
        getRenamePositions: function(doc, ast, pos, currentNode, callback) {
            callback();
        },

        /**
         * Invoked when refactoring is started.
         *
         * May be overridden by inheritors that implement rename refactoring.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Function} callback            The callback; must be called
         */
        onRenameBegin: function(doc, callback) {
            callback();
        },

        /**
         * Confirms that a rename refactoring is valid, before committing it.
         *
         * May be overridden by inheritors that implement rename refactoring.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} oldId                 The old identifier was being renamed
         * @param {Number} oldId.row             The row of the identifier that was being renamed
         * @param {Number} oldId.column          The column of the identifier that was being renamed
         * @param {String} oldId.value           The value of the identifier that was being renamed
         * @param {String} newName               The new name of the element after refactoring
         * @param {Boolean} isGeneric            True if this was a refactoring marked with 'isGeneric' (see {@link #getRenamePositions})
         * @param {Function} callback            The callback; must be called
         * @param {String} callback.err          Null if the refactoring can be committed, or an error message if refactoring failed
         */
        commitRename: function(doc, oldName, newName, isGeneric, callback) {
            callback();
        },

        /**
         * Invoked when a refactor request is cancelled
         *
         * May be overridden by inheritors that implement rename refactoring.
         *
         * @param {Function} callback            The callback; must be called
         */
        onRenameCancel: function(callback) {
            callback();
        },

        /**
         * Performs code formatting.
         *
         * Should be overridden by inheritors that implement code formatting.
         *
         * @param {Document} doc the Document object representing the source
         * @param {Function} callback            The callback; must be called
         * @param {Object} callback.result       The function's result
         * @return a string value representing the new source code after formatting or null if not supported
         */
        codeFormat: function(doc, callback) {
            callback();
        },

        /**
         * Performs jumping to a definition.
         *
         * Should be overridden by inheritors that implement jump to definition.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Object} pos                   The current cursor position
         * @param {Number} pos.row               The current cursor's row
         * @param {Number} pos.column            The current cursor's column
         * @param {Function} callback            The callback; must be called
         * @param {Object[]} callback.results    The results
         * @param {String} [callback.results.path]
         *                                       The result path
         * @param {Number} [callback.results.row]
         *                                       The result row
         * @param {Number} [callback.results.column]
         *                                       The result column
         * @param {"event"|"method"|"method2"|"package"|"property"|"property2"|"unknown"|"unknown2"} [callback.results.icon]
         *                                       The icon to display (in case of multiple results)
         * @param {Boolean} [callback.results.isGeneric]
         *                                       Indicates that this is a generic, language-independent
         *                                       suggestion (that should be deferred)
         */
        jumpToDefinition: function(doc, fullAst, pos, currentNode, callback) {
            callback();
        },

        /**
         * Gets marker resolutions for quick fixes.
         *
         * Must be overridden by inheritors that implement quick fixes.
         *
         * See {@link #hasResolution}.
         *
         * @param {Document} doc                        The Document object representing the source
         * @param {Object} fullAst                      The entire AST of the current file (if any)
         * @param {Object} markers                      The markers to get resolutions for
         * @param {Function} callback                   The callback; must be called
         * @param {Object} callback.result              The function's result
         * @return {language.MarkerResolution[]} Resulting resolutions.
         */
        getResolutions: function(doc, fullAst, markers, callback) {
            callback();
        },

        /**
         * Determines if there are marker resolutions for quick fixes.
         *
         * Must be overridden by inheritors that implement quick fixes.
         *
         * @param {Document} doc                 The Document object representing the source
         * @param {Object} fullAst               The entire AST of the current file (if any)
         * @param {Function} callback            The callback; must be called
         * @param {Boolean} callback.result      There is at least one resolution
         */
        hasResolution: function(doc, fullAst, marker, callback) {
            callback();
        },

        /**
         * Given the cursor position and the parsed node at that position,
         * gets the string to send to the debugger for live inspect hovering.
         *
         * Should be overridden by inheritors that implement a debugger
         * with live inspect. If not implemented, the string value based on
         * currentNode's position is used.
         *
         * @param {Document} doc                    The Document object representing the source
         * @param {Object} fullAst                  The entire AST of the current file (if any)
         * @param {Object} pos                      The current cursor position
         * @param {Number} pos.row                  The current cursor's row
         * @param {Number} pos.column               The current cursor's column
         * @param {Function} callback               The callback; must be called
         * @param {Object} callback.result          The resulting expression
         * @param {String} callback.result.value    The string representation of the expression to inspect
         * @param {Object} callback.result.pos      The expression's position
         * @param {Number} callback.result.pos.sl   The expression's starting row
         * @param {Number} callback.result.pos.el   The expression's ending row
         * @param {Number} callback.result.pos.sc   The expression's starting column
         * @param {Number} callback.result.pos.ec   The expression's ending column
         */
        getInspectExpression: function(doc, fullAst, pos, currentNode, callback) {
            callback();
        }
    };

// Mark all abstract/builtin methods for later optimization
    for (f in module.exports) {
        if (typeof module.exports[f] === "function")
            module.exports[f].base_handler = true;
    }

});

},
'xace/complete_util':function(){
/**
 * Completion utilities for language workers.
 * 
 * Import using
 * 
 *     require("plugins/c9.ide.language/complete_util")
 * 
 * @class language.complete_util
 */
define(function(require, exports, module) {

var ID_REGEX = /[a-zA-Z_0-9\$]/;
var REQUIRE_ID_REGEX = /(?!["'])./;
var staticPrefix = "../static/lib";

function retrievePrecedingIdentifier(line, offset, regex) {
    regex = regex || ID_REGEX;
    var buf = [];
    for (var i = offset-1; i >= 0 && line; i--) {
        if (regex.test(line[i]))
            buf.push(line[i]);
        else
            break;
    }
    return buf.reverse().join("");
}

function retrieveFollowingIdentifier(line, offset, regex) {
    regex = regex || ID_REGEX;
    var buf = [];
    for (var i = offset; line && i < line.length; i++) {
        if (regex.test(line[i]))
            buf.push(line[i]);
        else
            break;
    }
    return buf.join("");
}

function prefixBinarySearch(items, prefix) {
    var startIndex = 0;
    var stopIndex = items.length - 1;
    var middle = Math.floor((stopIndex + startIndex) / 2);
    
    while (stopIndex > startIndex && middle >= 0 && items[middle].indexOf(prefix) !== 0) {
        if (prefix < items[middle]) {
            stopIndex = middle - 1;
        }
        else if (prefix > items[middle]) {
            startIndex = middle + 1;
        }
        middle = Math.floor((stopIndex + startIndex) / 2);
    }
    
    // Look back to make sure we haven't skipped any
    while (middle > 0 && items[middle-1].indexOf(prefix) === 0)
        middle--;
    return middle >= 0 ? middle : 0; // ensure we're not returning a negative index
}

function findCompletions(prefix, allIdentifiers) {
    allIdentifiers.sort();
    var startIdx = prefixBinarySearch(allIdentifiers, prefix);
    var matches = [];
    for (var i = startIdx; i < allIdentifiers.length && allIdentifiers[i].indexOf(prefix) === 0; i++)
        matches.push(allIdentifiers[i]);
    return matches;
}

function fetchText(path) {
    var xhr = new XMLHttpRequest();
    var _url = staticPrefix + "/" + path;
    console.error('fetch text : ' + _url);
    xhr.open('GET', _url, false);
    try {
        xhr.send();
    }
    // Likely we got a cross-script error (equivalent with a 404 in our cloud setup)
    catch (e) {
        return false;
    }
    if (xhr.status === 200 || xhr.responseText) // when loading from file:// status is always 0
        return xhr.responseText;
    else
        return false;
}

function setStaticPrefix(url) {
    //console.error('setStaticPrefix : ' + url);
    staticPrefix = url;
}

/**
 * Determine if code completion results triggered for oldLine/oldPos
 * would still be applicable for newLine/newPos
 * (assuming you would filter them for things that no longer apply).
 */
function canCompleteForChangedLine(oldLine, newLine, oldPos, newPos, identifierRegex) {
    if (oldPos.row !== newPos.row)
        return false;
        
    if (newLine.indexOf(oldLine) !== 0)
        return false;
        
    var oldPrefix = retrievePrecedingIdentifier(oldLine, oldPos.column, identifierRegex);
    var newPrefix = retrievePrecedingIdentifier(newLine, newPos.column, identifierRegex);
    return newLine.substr(0, newLine.length - newPrefix.length) === oldLine.substr(0, oldLine.length - oldPrefix.length);
}

function precededByIdentifier(line, column, postfix, ace) {
    var id = retrievePrecedingIdentifier(line, column);
    if (postfix) id += postfix;
    return id !== "" && !(id[0] >= '0' && id[0] <= '9') 
        && (inCompletableCodeContext(line, column, id, ace) 
        || isRequireJSCall(line, column, id, ace));
}

function isRequireJSCall(line, column, identifier, ace, noQuote) {
    if (["javascript", "jsx"].indexOf(ace.getSession().syntax) === -1)
        return false;
    var id = identifier == null ? retrievePrecedingIdentifier(line, column, REQUIRE_ID_REGEX) : identifier;
    var LENGTH = 'require("'.length - (noQuote ? 1 : 0);
    var start = column - id.length - LENGTH;
    var substr = line.substr(start, LENGTH) + (noQuote ? '"' : '');

    return start >= 0 && substr.match(/require\(["']/)
        || line.substr(start + 1, LENGTH).match(/require\(["']/);
}

/**
 * Ensure that code completion is not triggered in comments and such.
 * Right now this only returns false when in a JavaScript regular expression.
 */
function inCompletableCodeContext(line, column, id, ace) {
    if (["javascript", "jsx"].indexOf(ace.getSession().syntax) === -1)
        return true;
    var isJavaScript = true;
    var inMode = null;
    for (var i = 0; i < column; i++) {
        if (line[i] === '"' && !inMode)
            inMode = '"';
        else if (line[i] === '"' && inMode === '"' && line[i-1] !== "\\")
            inMode = null;
        else if (line[i] === "'" && !inMode)
            inMode = "'";
        else if (line[i] === "'" && inMode === "'" && line[i-1] !== "\\")
            inMode = null;
        else if (line[i] === "/" && line[i+1] === "/") {
            inMode = '//';
            i++;
        }
        else if (line[i] === "/" && line[i+1] === "*" && !inMode) {
            if (line.substr(i + 2, 6) === "global")
                continue;
            inMode = '/*';
            i++;
        }
        else if (line[i] === "*" && line[i+1] === "/" && inMode === "/*") {
            inMode = null;
            i++;
        }
        else if (line[i] === "/" && !inMode && isJavaScript)
            inMode = "/";
        else if (line[i] === "/" && inMode === "/" && line[i-1] !== "\\")
            inMode = null;
    }
    return inMode != "/";
}

/**
 * @ignore
 * @return {Boolean}
 */
exports.precededByIdentifier = precededByIdentifier;

/**
 * @ignore
 */
exports.isRequireJSCall = isRequireJSCall;

/**
 * @internal Use {@link worker_util#getPrecedingIdentifier() instead.
 */
exports.retrievePrecedingIdentifier = retrievePrecedingIdentifier;

/**
 * @internal Use {@link worker_util#getFollowingIdentifier() instead. 
 */
exports.retrieveFollowingIdentifier = retrieveFollowingIdentifier;

/**
 * @ignore
 */
exports.findCompletions = findCompletions;

/**
 * @ignore
 */
exports.fetchText = fetchText;

/**
 * @ignore
 */
exports.setStaticPrefix = setStaticPrefix;

/**
 * @ignore
 */
exports.DEFAULT_ID_REGEX = ID_REGEX;

/**
 * @ignore
 */
exports.canCompleteForChangedLine = canCompleteForChangedLine;
});
},
'xace/views/_Actions':function(){
/** @module xace/views/Editor **/
define([
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'xide/types/Types', //  <--important for build
    'xaction/types', //  <--important for build
    'xaction/ActionProvider',
    'xace/views/ACEEditor',
    'xaction/Toolbar',
    'xaction/DefaultActions',
    'dojo/Deferred',
    'xace/formatters'
], function (dcl, utils, types, Types,aTypes,ActionProvider,ACEEditor,Toolbar, DefaultActions,Deferred,formatters) {

        var ACTION = types.ACTION,
        EDITOR_SETTINGS = 'Editor/Settings',
        INCREASE_FONT_SIZE = 'View/Increase Font Size',
        DECREASE_FONT_SIZE = 'View/Decrease Font Size',
        EDITOR_HELP = 'Help/Editor Shortcuts',
        EDITOR_THEMES = 'View/Themes',
        SNIPPETS = 'Editor/Snippets',
        EDITOR_CONSOLE = 'Editor/Console',
        KEYBOARD = 'Editor/Keyboard',
        LAYOUT = 'View/Layout',
        FORMAT = 'Edit/Format',
        SPLIT_MODE = types.VIEW_SPLIT_MODE,
        DEFAULT_PERMISSIONS = [
            ACTION.RELOAD,
            ACTION.SAVE,
            ACTION.FIND,
            ACTION.TOOLBAR,
            KEYBOARD,
            INCREASE_FONT_SIZE,
            DECREASE_FONT_SIZE,
            EDITOR_THEMES,
            'Help/Editor Shortcuts',
            SNIPPETS,
            EDITOR_CONSOLE,
            EDITOR_SETTINGS,
            ACTION.FULLSCREEN,
            LAYOUT,
            FORMAT
        ];
    /**
     * Default Editor with all extras added : Actions, Toolbar and ACE-Features
     @class module:xgrid/Base
     */
    var Module = dcl([ACEEditor, Toolbar.dcl, ActionProvider.dcl], {
            permissions: DEFAULT_PERMISSIONS,
            _searchBoxOpen: false,
            onSingleView: function () {

            },
            setSplitMode: function (mode) {
                this.splitMode = mode;
                if (!this.doSplit) {
                    if (mode == 'Diff') {
                        this.doDiff();
                        return;
                    }
                    var isSplit = mode == SPLIT_MODE.SPLIT_HORIZONTAL || mode == SPLIT_MODE.SPLIT_VERTICAL;
                    var _ed = this.getEditor();
                    var sp = this.split;
                    if (isSplit) {
                        var newEditor = (sp.getSplits() == 1);
                        sp.setOrientation(mode == SPLIT_MODE.SPLIT_HORIZONTAL ? sp.BELOW : sp.BESIDE);
                        sp.setSplits(2);
                        if (newEditor) {
                            var session = sp.getEditor(0).session;
                            var newSession = sp.setSession(session, 1);
                            newSession.name = session.name;
                            var options = _ed.getOptions();
                            sp.getEditor(1).setOptions(options);
                        }
                    } else {
                        sp.setSplits(1);
                        this.onSingleView();
                    }
                }
            },
            onMaximized: function (maximized) {
                var parent = this.getParent();
                if (maximized === false) {
                    if (parent && parent.resize) {
                        parent.resize();
                    }
                }
                var toolbar = this.getToolbar();
                if (toolbar) {
                    if (maximized) {
                        $(toolbar.domNode).addClass('bg-opaque');
                    } else {
                        $(toolbar.domNode).removeClass('bg-opaque');
                    }
                }
                if (maximized === false) {
                    this.resize();
                    parent && utils.resizeTo(this, parent, true, true);
                    this.publish(types.EVENTS.ON_VIEW_MAXIMIZE_END);
                }
                this.getEditor().focus();
            },
            maximize: function () {
                var node = this.domNode,
                    $node = $(node),
                    _toolbar = this.getToolbar();

                if (!this._isMaximized) {
                    this.publish(types.EVENTS.ON_VIEW_MAXIMIZE_START);
                    this._isMaximized = true;
                    var vp = $(this.domNode.ownerDocument);
                    var root = $('body')[0];
                    var container = utils.create('div', {
                        className: 'ACEContainer bg-opaque',
                        style: 'z-index:300;height:100%;width:100%'
                    });

                    this._maximizeContainer = container;
                    root.appendChild(container);
                    $(node).addClass('AceEditorPaneFullScreen');
                    $(node).css('width', vp.width());
                    $(node).css('height', vp.height());
                    this.resize();
                    this._lastParent = node.parentNode;
                    container.appendChild(node);
                    $(container).addClass('bg-opaque');
                    $(container).css('width', vp.width());
                    $(container).css('height', vp.height());
                    $(container).css({
                        position: "absolute",
                        left: "0px",
                        top: "0px",
                        border: 'none medium',
                        width: '100%',
                        height: '100%'
                    });

                } else {
                    this._isMaximized = false;
                    $node.removeClass('AceEditorPaneFullScreen');
                    this._lastParent.appendChild(node);
                    utils.destroy(this._maximizeContainer);
                }
                this.onMaximized(this._isMaximized);
                return true;
            },
            save: function (item) {
                var value = this.get('value');
                var res = this.saveContent(this.get('value'), item);
                var thiz = this;
                this._emit(types.EVENTS.ON_FILE_CONTENT_CHANGED,{
                    content:value,
                    item:item
                });
                setTimeout(function () {
                    var _ed = thiz.getEditor();
                    if (_ed) {
                        _ed.focus();
                    }
                }, 600);
                return res;
            },
            reload:function(){
                var self = this;
                var dfd = new Deferred();
                this.getContent(
                    this.item,
                    function (content) {//onSuccess
                        self.lastSavedContent = content;
                        self.set('value',content);
                        dfd.resolve(content);
                    },
                    function (e) {//onError
                        logError(e, 'error loading content from file');
                        dfd.reject(e);
                    }
                );
                return dfd;
            },
            runAction: function (action) {
                action = this.getAction(action);
                if (!action) {
                    return false;
                }

                var self = this,
                    command = action.command,
                    ACTION = types.ACTION,
                    editor = this.getEditor(),
                    session = this.editorSession,
                    result = false;

                if (command.indexOf(LAYOUT) != -1) {
                    self.setSplitMode(action.option, null);
                }

                switch (command) {
                    case ACTION.RELOAD:
                    {
                        return this.reload();
                    }
                    case INCREASE_FONT_SIZE:
                    {
                        editor.setFontSize(editor.getFontSize() + 1);
                        return true;
                    }
                    case DECREASE_FONT_SIZE:
                    {
                        editor.setFontSize(editor.getFontSize() - 1);
                        return true;
                    }
                    case ACTION.FULLSCREEN:
                    {
                        return this.maximize();
                    }
                    case EDITOR_HELP:
                    {
                        self.showHelp();
                        break;
                    }
                    case ACTION.SAVE:
                    {
                        result = self.save(this.item);
                        break;
                    }
                    case ACTION.FIND:
                    {
                        var net = ace.require("ace/lib/net");
                        var webRoot = this.getWebRoot();
                        var sb = editor.searchBox;
                        function _search(sb) {
                            var shown = self._searchBoxOpen;
                            if (!shown) {
                                sb.show(editor.session.getTextRange(), null);
                                self._searchBoxOpen = true;
                            } else {
                                sb.hide();
                                self._searchBoxOpen = false;
                            }
                        }
                        if (sb) {
                            _search(sb);
                        } else {
                            net.loadScript(webRoot + '/xfile/ext/ace/ext-searchbox.js', function (what) {
                                var sbm = ace.require("ace/ext/searchbox");
                                _search(new sbm.SearchBox(editor));
                            });
                        }
                        return true;
                    }
                }

                //themes
                if (command.indexOf(EDITOR_THEMES) !==-1) {
                    self.set('theme', action.theme);
                    var parentAction = action.getParent ?  action.getParent() : null;
                    //action._originEvent = 'change';
                    if(parentAction) {
                        var rendererActions = parentAction.getChildren();
                        _.each(rendererActions, function (child) {
                            child.set('icon', child._oldIcon);
                        });
                    }
                    action.set('icon', 'fa fa-check');
                }
                //formatters :
                if (command.indexOf(FORMAT) !==-1) {
                     if (editor) {
                         var _value = formatters.format(editor, action.formatter);
                         self.set('value',_value);
                     }
                }
                /*
                if (command.indexOf(KEYBOARD) !==-1) {
                    var option = action.option,
                        keybindings = {
                        ace: null, // Null = use "default" keymapping
                        vim: ace.require("ace/keyboard/vim").handler,
                        emacs: "ace/keyboard/emacs"
                    };
                    editor.setKeyboardHandler(keybindings[action.option]);
                    return true;
                }
                */

                if (command.indexOf(EDITOR_SETTINGS) !==-1) {
                    var key = action.option,
                        option = editor.getOption(action.option),
                        isBoolean = _.isBoolean(option);
                    if (key === 'highlightActive') {
                        editor.setHighlightActiveLine(!editor.getHighlightActiveLine());
                        return;
                    }
                    if (isBoolean) {
                        editor.setOption(action.option, !option);
                    } else {
                        if (key === 'wordWrap') {
                            var mode = session.getUseWrapMode();
                            this.set('wordWrap', !mode);
                            return true;
                        }
                        if (option === 'off' || option === 'on') {
                            editor.setOption(key, option === 'off' ? 'on' : 'off');
                        } else {
                            editor.setOption(action.option, false);
                        }
                    }
                    return true;
                }

                return this.inherited(arguments);
            },
            getEditorActions: function (permissions) {

                var actions = [],
                    self = this,
                    ACTION = types.ACTION,
                    ICON = types.ACTION_ICON;

                /* @TODO: reactivate reload action
                actions.push(this.createAction({
                    label: 'Reload',
                    command: ACTION.RELOAD,
                    icon: ICON.RELOAD,
                    keycombo: 'ctrl r'
                }));
                */
                actions.push(this.createAction({
                    label: 'Save',
                    command: ACTION.SAVE,
                    icon: ICON.SAVE,
                    keycombo: 'ctrl s',
                    group: 'File'
                }));

                actions.push(this.createAction({
                    label: 'Find',
                    command: ACTION.FIND,
                    icon: ICON.SEARCH,
                    keycombo: 'ctrl f',
                    group: 'Search'
                }));

                actions.push(this.createAction({
                    label: 'Fullscreen',
                    command: ACTION.FULLSCREEN,
                    icon: ICON.MAXIMIZE,
                    keycombo: 'ctrl f11',
                    group: 'View'
                }));


                actions.push(this.createAction({
                    label: 'Increase Fontsize',
                    command: INCREASE_FONT_SIZE,
                    icon: 'fa-text-height',
                    group: 'View'
                }));

                actions.push(this.createAction({
                    label: 'Decrease Fontsize',
                    command: DECREASE_FONT_SIZE,
                    icon: 'fa-text-height',
                    group: 'View'
                }));

                if (DefaultActions.hasAction(permissions, EDITOR_THEMES)) {
                    actions.push(this.createAction({
                        label: 'Themes',
                        command: EDITOR_THEMES,
                        icon: 'fa-paint-brush',
                        group: 'View',
                        mixin:{
                            closeOnClick:false,
                            value:this.defaultPrefenceTheme
                        },
                        onCreate:function(action){
                            var options = self.getDefaultOptions();
                            action.set('value',options.theme);
                        }
                    }));

                    self._addThemes && self._addThemes(actions);
                }

                actions.push(this.createAction({
                    label: 'Help',
                    command: EDITOR_HELP,
                    icon: 'fa-question',
                    keycombo: 'f1'
                }));

                ///editor settings
                actions.push(this.createAction({
                    label: 'Settings',
                    command: EDITOR_SETTINGS,
                    icon: 'fa-cogs',
                    group: "Settings"
                }));

                function _createSettings(label, command, icon, option, mixin, group, actionType, params) {
                    command = command || EDITOR_SETTINGS + '/' + label;
                    mixin = mixin || {};
                    command = command || EDITOR_SETTINGS + '/' + label;
                    mixin = mixin || {};
                    var action = self.createAction(utils.mixin({
                        label: label,
                        command: command,
                        icon: icon || 'fa-cogs',
                        group: group || "Settings",
                        mixin: utils.mixin({
                            addPermission: true,
                            option: option,
                            actionType: actionType,
                            owner: self
                        }, mixin)
                    }, params));
                    actions.push(action);
                    return action;
                }
                var _params = {
                    onCreate: function (action) {
                        var optionValue = self.getOptionsMixed()[this.option];
                        if (optionValue !== null) {
                            action.set('value', optionValue);
                        }
                    },
                    onChange: function (property, value) {
                        this.value = value;
                        self.runAction(this);
                    }
                };


                _createSettings('Show Gutters', null, null, 'showGutter', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Show Print Margin', null, null, 'showPrintMargin', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Display Intend Guides', null, null, 'displayIndentGuides', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Show Line Numbers', null, null, 'showLineNumbers', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Show Indivisibles', null, null, 'showInvisibles', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Use Soft Tabs', null, null, 'useSoftTabs', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Use Elastic Tab Stops', null, null, 'useElasticTabstops', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                //_createSettings('Use Elastic Tab Stops', null, null, 'useElasticTabstops');
                _createSettings('Animated Scroll', null, null, 'animatedScroll', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Word Wrap', null, null, 'wordWrap', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);
                _createSettings('Highlight Active Line', null, null, 'highlightActive', null, null, types.ACTION_TYPE.MULTI_TOGGLE, _params);

                /*
                 var keybindings = {
                 ace: null, // Null = use "default" keymapping
                 vim: ace.require("ace/keyboard/vim").handler,
                 emacs: "ace/keyboard/emacs"
                 };
                 */

                /*
                 actions.push(this.createAction({
                 label: 'Keyboard',
                 command: KEYBOARD,
                 icon: 'fa-keyboard-o',
                 group: "Settings"
                 }));

                 if(DefaultActions.hasAction(permissions,KEYBOARD)){
                 _createSettings('Default', KEYBOARD + '/Default', null, 'ace');
                 _createSettings('Vim', KEYBOARD + '/Vim', null, 'vim');
                 _createSettings('EMacs', KEYBOARD + '/EMacs', null, 'emacs');
                 }
                 */
                var VISIBILITY = types.ACTION_VISIBILITY;
                if (DefaultActions.hasAction(permissions, FORMAT)) {
                    actions.push(this.createAction({
                        label: 'Format',
                        command: 'Edit/Format',
                        icon: 'fa-indent',
                        group: "Edit"
                    }));

                    var modes = formatters.modes;
                    var creatorFn = function (label, icon, value) {

                        var head = self.createAction({
                            label: label,
                            command: 'Edit/Format/'+label,
                            icon: 'fa-indent',
                            group: "Edit",
                            mixin:{
                                addPermission: true,
                                formatter:value
                            },
                            onCreate:function(action){
                                /*
                                action.setVisibility(VISIBILITY.ACTION_TOOLBAR, {label: ''}).
                                setVisibility(VISIBILITY.MAIN_MENU, {show: false}).
                                setVisibility(VISIBILITY.CONTEXT_MENU, null);*/
                            }
                        });

                        actions.push(head);

                        /*
                        return Action.create(label, icon, 'Edit/Format/' + label, false, null, 'TEXT', 'viewActions', null, false, function () {
                            formatCode(value);
                        });
                        */
                    };

                    for (var _f in modes) {
                        actions.push(creatorFn(modes[_f], '', _f));
                    }
                    /*
                    var format = Action.createDefault('Format', 'fa-indent', 'Edit/Format', '_a', null, {
                        dummy: true
                    }).setVisibility(VISIBILITY.ACTION_TOOLBAR, {label: ''}).
                    setVisibility(VISIBILITY.MAIN_MENU, {show: false}).
                    setVisibility(VISIBILITY.CONTEXT_MENU, null);

                    this.addAction(actions,format);

                    for (var _f in modes) {
                        actions.push(creatorFn(modes[_f], '', _f));
                    }
                    */


                    /*

                    //layout
                    actions.push(_createSettings('None', 'View/Layout/None', 'fa-columns', SPLIT_MODE.SOURCE, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Horizontal', 'View/Layout/Horizontal', 'layoutIcon-horizontalSplit', SPLIT_MODE.SPLIT_HORIZONTAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Vertical', 'View/Layout/Vertical', 'layoutIcon-layout293', SPLIT_MODE.SPLIT_VERTICAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    */
                    //actions.push(_createSettings('Diff', 'View/Layout/Diff', 'fa-columns', 'Diff', null, 'View'));
                }

                if (DefaultActions.hasAction(permissions, LAYOUT)) {
                    actions.push(this.createAction({
                        label: 'Split',
                        command: 'View/Layout',
                        icon: 'fa-columns',
                        group: "View"
                    }));
                    //layout
                    actions.push(_createSettings('None', 'View/Layout/None', 'fa-columns', SPLIT_MODE.SOURCE, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Horizontal', 'View/Layout/Horizontal', 'layoutIcon-horizontalSplit', SPLIT_MODE.SPLIT_HORIZONTAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    actions.push(_createSettings('Vertical', 'View/Layout/Vertical', 'layoutIcon-layout293', SPLIT_MODE.SPLIT_VERTICAL, null, 'View', types.ACTION_TYPE.SINGLE_TOGGLE));
                    //actions.push(_createSettings('Diff', 'View/Layout/Diff', 'fa-columns', 'Diff', null, 'View'));
                }
                return actions;
            },
            _addThemes: function (actions) {
                var themes = this.getThemeData(),
                    thiz = this;

                var creatorFn = function (label, icon, value) {
                    return thiz.createAction({
                        label: label,
                        command: EDITOR_THEMES + '/' + label,
                        group: 'View',
                        icon: icon,
                        mixin: {
                            addPermission: true,
                            value:value,
                            theme: value,
                            closeOnClick:false
                        },
                        onCreate:function(action) {
                            action._oldIcon = icon;
                            action.set('value', value);
                            action.actionType = types.ACTION_TYPE.SINGLE_TOGGLE;
                        }
                    });
                };

                //clean and complete theme data
                for (var i = 0; i < themes.length; i++) {
                    var data = themes[i];
                    var name = data[1] || data[0].replace(/ /g, "_").toLowerCase();
                    var theme = creatorFn(data[0], ' ', name);//@TODO: _MenuMixin not creating icon node, use white space for now
                    actions.push(theme);
                }
            },
            showHelp: function (editor) {
                editor = editor || this.getEditor();
                var config = ace.require("ace/config");
                config.loadModule("ace/ext/keybinding_menu", function (module) {
                    module.init(editor);
                    editor.showKeyboardShortcuts();
                });
            },
            getThemeData: function () {
                return [
                    ["Chrome"],
                    ["Clouds"],
                    ["Crimson Editor"],
                    ["Dawn"],
                    ["Dreamweaver"],
                    ["Eclipse"],
                    ["GitHub"],
                    ["Solarized Light"],
                    ["TextMate"],
                    ["Tomorrow"],
                    ["XCode"],
                    ["Kuroir"],
                    ["KatzenMilch"],
                    ["Ambiance", "ambiance", "dark"],
                    ["Day", "cloud9_day"],
                    ["Night", "cloud9_night"],
                    ["Chaos", "chaos", "dark"],
                    ["Midnight", "clouds_midnight", "dark"],
                    ["Cobalt", "cobalt", "dark"],
                    ["idle Fingers", "idle_fingers", "dark"],
                    ["krTheme", "kr_theme", "dark"],
                    ["Merbivore", "merbivore", "dark"],
                    ["Merbivore-Soft", "merbivore_soft", "dark"],
                    ["Mono Industrial", "mono_industrial", "dark"],
                    ["Monokai", "monokai", "dark"],
                    ["Pastel on dark", "pastel_on_dark", "dark"],
                    ["Solarized Dark", "solarized_dark", "dark"],
                    ["Terminal", "terminal", "dark"],
                    ["Tomorrow-Night", "tomorrow_night", "dark"],
                    ["Tomorrow-Night-Blue", "tomorrow_night_blue", "dark"],
                    ["Tomorrow-Night-Bright", "tomorrow_night_bright", "dark"],
                    ["Tomorrow-Night-80s", "tomorrow_night_eighties", "dark"],
                    ["Twilight", "twilight", "dark"],
                    ["Vibrant Ink", "vibrant_ink", "dark"]
                ];
            }
        }
    );
    Module.DEFAULT_PERMISSIONS = DEFAULT_PERMISSIONS;
    dcl.chainAfter('runAction',Module);
    return Module;
});
},
'xace/formatters':function(){
define([
    'xace/lib_jsbeautify'
    ], function (jsbeautify) {


    function formatCode(editor, mode){
        if (this.disabled === true)
            return;

        var ace = editor;
        var sel = ace.selection;
        var session = ace.session;
        var range = sel.getRange();

        /*
        session.diffAndReplace = function(range, text) {
            var doc = this.doc;
            var start = doc.positionToIndex(range.start);
            var oldText = doc.getTextRange(range);
            merge.patchAce(oldText, text, doc, {
                offset: start,
                method: "quick"
            });
            var dl = text.replace(/\r\n|\r|\n/g, doc.getNewLineCharacter()).length;
            return doc.indexToPosition(start + dl);
        };*/

        // Load up current settings data
        /*
         var options = {
         space_before_conditional: settings.getBool("user/format/jsbeautify/@space_before_conditional"),
         keep_array_indentation: settings.getBool("user/format/jsbeautify/@keeparrayindentation"),
         preserve_newlines: settings.getBool("user/format/jsbeautify/@preserveempty"),
         unescape_strings: settings.getBool("user/format/jsbeautify/@unescape_strings"),
         jslint_happy: settings.getBool("user/format/jsbeautify/@jslinthappy"),
         brace_style: settings.get("user/format/jsbeautify/@braces")
         };
         */

        var options = {
            space_before_conditional: true,
            keep_array_indentation: false,
            preserve_newlines: true,
            unescape_strings: true,
            jslint_happy: false,
            brace_style: "end-expand"
        };
        var useSoftTabs = true;
        if (useSoftTabs) {
            options.indent_char = " ";
            options.indent_size = session.getTabSize();
        } else {
            options.indent_char = "\t";
            options.indent_size = 1;
        }

        var line = session.getLine(range.start.row);
        var indent = line.match(/^\s*/)[0];
        var trim = false;

        if (range.start.column < indent.length)
            range.start.column = 0;
        else
            trim = true;

        var value = session.getTextRange(range);
        if(value.length==0){
            value = session.getValue();
        }
        var type = null;

        if (mode == "javascript" || mode == "json") {
            type = "js";
        } else if (mode == "css" || mode == "less"){
            type = "css";
        } else if (/^\s*<!?\w/.test(value)) {
            type = "html";
        } else if (mode == "xml") {
            type = "html";
        } else if (mode == "html") {
            if (/[^<]+?{[\s\-\w]+:[^}]+;/.test(value))
                type = "css";
            else if (/<\w+[ \/>]/.test(value))
                type = "html";
            else
                type = "js";
        } else if (mode == "handlebars") {
            options.indent_handlebars = true;
            type = "html";
        }


        try {
            value = jsbeautify[type + "_beautify"](value, options);
            if (trim)
                value = value.replace(/^/gm, indent).trim();
            if (range.end.column === 0)
                value += "\n" + indent;
        }
        catch (e) {
            return false;
        }

        //var end = session.diffAndReplace(range, value);

        //sel.setSelectionRange(Range.fromPoints(range.start, end));

        return value;
    }

    return {
        format:formatCode,
        modes : {
            "javascript" : "Javascript (JS Beautify)",
            "html"       : "HTML (JS Beautify)",
            "css"        : "CSS (JS Beautify)",
            "less"       : "Less (JS Beautify)",
            "xml"        : "XML (JS Beautify)",
            "json"       : "JSON (JS Beautify)",
            "handlebars" : "Handlebars (JS Beautify)"
        }
    }
});
},
'xace/lib_jsbeautify':function(){
define(['require', 'exports', 'module'], function(_r, _e, module) {
var define, window = module.exports = {};
/*jshint curly:true, eqeqeq:true, laxbreak:true, noempty:false */
/*

  The MIT License (MIT)

  Copyright (c) 2007-2013 Einar Lielmanis and contributors.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

 JS Beautifier
---------------


  Written by Einar Lielmanis, <einar@jsbeautifier.org>
      http://jsbeautifier.org/

  Originally converted to javascript by Vital, <vital76@gmail.com>
  "End braces on own line" added by Chris J. Shull, <chrisjshull@gmail.com>
  Parsing improvements for brace-less statements by Liam Newman <bitwiseman@gmail.com>


  Usage:
    js_beautify(js_source_text);
    js_beautify(js_source_text, options);

  The options are:
    indent_size (default 4)          - indentation size,
    indent_char (default space)      - character to indent with,
    preserve_newlines (default true) - whether existing line breaks should be preserved,
    max_preserve_newlines (default unlimited) - maximum number of line breaks to be preserved in one chunk,

    jslint_happy (default false) - if true, then jslint-stricter mode is enforced.

            jslint_happy        !jslint_happy
            ---------------------------------
            function ()         function()

            switch () {         switch() {
            case 1:               case 1:
              break;                break;
            }                   }

    space_after_anon_function (default false) - should the space before an anonymous function's parens be added, "function()" vs "function ()",
          NOTE: This option is overriden by jslint_happy (i.e. if jslint_happy is true, space_after_anon_function is true by design)

    brace_style (default "collapse") - "collapse" | "expand" | "end-expand" | "none"
            put braces on the same line as control statements (default), or put braces on own line (Allman / ANSI style), or just put end braces on own line, or attempt to keep them where they are.

    space_before_conditional (default true) - should the space before conditional statement be added, "if(true)" vs "if (true)",

    unescape_strings (default false) - should printable characters in strings encoded in \xNN notation be unescaped, "example" vs "\x65\x78\x61\x6d\x70\x6c\x65"

    wrap_line_length (default unlimited) - lines should wrap at next opportunity after this number of characters.
          NOTE: This is not a hard limit. Lines will continue until a point where a newline would
                be preserved if it were present.

    end_with_newline (default false)  - end output with a newline


    e.g

    js_beautify(js_source_text, {
      'indent_size': 1,
      'indent_char': '\t'
    });

*/

(function() {

    var acorn = {};
    (function (exports) {
      // This section of code is taken from acorn.
      //
      // Acorn was written by Marijn Haverbeke and released under an MIT
      // license. The Unicode regexps (for identifiers and whitespace) were
      // taken from [Esprima](http://esprima.org) by Ariya Hidayat.
      //
      // Git repositories for Acorn are available at
      //
      //     http://marijnhaverbeke.nl/git/acorn
      //     https://github.com/marijnh/acorn.git

      // ## Character categories

      // Big ugly regular expressions that match characters in the
      // whitespace, identifier, and identifier-start categories. These
      // are only applied when a character is found to actually have a
      // code point above 128.

      var nonASCIIwhitespace = /[\u1680\u180e\u2000-\u200a\u202f\u205f\u3000\ufeff]/;
      var nonASCIIidentifierStartChars = "\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc";
      var nonASCIIidentifierChars = "\u0300-\u036f\u0483-\u0487\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u0620-\u0649\u0672-\u06d3\u06e7-\u06e8\u06fb-\u06fc\u0730-\u074a\u0800-\u0814\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0840-\u0857\u08e4-\u08fe\u0900-\u0903\u093a-\u093c\u093e-\u094f\u0951-\u0957\u0962-\u0963\u0966-\u096f\u0981-\u0983\u09bc\u09be-\u09c4\u09c7\u09c8\u09d7\u09df-\u09e0\u0a01-\u0a03\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a66-\u0a71\u0a75\u0a81-\u0a83\u0abc\u0abe-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ae2-\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b3c\u0b3e-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5f-\u0b60\u0b66-\u0b6f\u0b82\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62-\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0cbc\u0cbe-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0ce2-\u0ce3\u0ce6-\u0cef\u0d02\u0d03\u0d46-\u0d48\u0d57\u0d62-\u0d63\u0d66-\u0d6f\u0d82\u0d83\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e34-\u0e3a\u0e40-\u0e45\u0e50-\u0e59\u0eb4-\u0eb9\u0ec8-\u0ecd\u0ed0-\u0ed9\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f41-\u0f47\u0f71-\u0f84\u0f86-\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u1029\u1040-\u1049\u1067-\u106d\u1071-\u1074\u1082-\u108d\u108f-\u109d\u135d-\u135f\u170e-\u1710\u1720-\u1730\u1740-\u1750\u1772\u1773\u1780-\u17b2\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u1920-\u192b\u1930-\u193b\u1951-\u196d\u19b0-\u19c0\u19c8-\u19c9\u19d0-\u19d9\u1a00-\u1a15\u1a20-\u1a53\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1b46-\u1b4b\u1b50-\u1b59\u1b6b-\u1b73\u1bb0-\u1bb9\u1be6-\u1bf3\u1c00-\u1c22\u1c40-\u1c49\u1c5b-\u1c7d\u1cd0-\u1cd2\u1d00-\u1dbe\u1e01-\u1f15\u200c\u200d\u203f\u2040\u2054\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2d81-\u2d96\u2de0-\u2dff\u3021-\u3028\u3099\u309a\ua640-\ua66d\ua674-\ua67d\ua69f\ua6f0-\ua6f1\ua7f8-\ua800\ua806\ua80b\ua823-\ua827\ua880-\ua881\ua8b4-\ua8c4\ua8d0-\ua8d9\ua8f3-\ua8f7\ua900-\ua909\ua926-\ua92d\ua930-\ua945\ua980-\ua983\ua9b3-\ua9c0\uaa00-\uaa27\uaa40-\uaa41\uaa4c-\uaa4d\uaa50-\uaa59\uaa7b\uaae0-\uaae9\uaaf2-\uaaf3\uabc0-\uabe1\uabec\uabed\uabf0-\uabf9\ufb20-\ufb28\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\uff10-\uff19\uff3f";
      var nonASCIIidentifierStart = new RegExp("[" + nonASCIIidentifierStartChars + "]");
      var nonASCIIidentifier = new RegExp("[" + nonASCIIidentifierStartChars + nonASCIIidentifierChars + "]");

      // Whether a single character denotes a newline.

      var newline = exports.newline = /[\n\r\u2028\u2029]/;

      // Matches a whole line break (where CRLF is considered a single
      // line break). Used to count lines.

      var lineBreak = /\r\n|[\n\r\u2028\u2029]/g;

      // Test whether a given character code starts an identifier.

      var isIdentifierStart = exports.isIdentifierStart = function(code) {
        if (code < 65) return code === 36;
        if (code < 91) return true;
        if (code < 97) return code === 95;
        if (code < 123)return true;
        return code >= 0xaa && nonASCIIidentifierStart.test(String.fromCharCode(code));
      };

      // Test whether a given character is part of an identifier.

      var isIdentifierChar = exports.isIdentifierChar = function(code) {
        if (code < 48) return code === 36;
        if (code < 58) return true;
        if (code < 65) return false;
        if (code < 91) return true;
        if (code < 97) return code === 95;
        if (code < 123)return true;
        return code >= 0xaa && nonASCIIidentifier.test(String.fromCharCode(code));
      };
    })(acorn);

    function in_array(what, arr) {
        for (var i = 0; i < arr.length; i += 1) {
            if (arr[i] === what) {
                return true;
            }
        }
        return false;
    }

    function trim(s) {
        return s.replace(/^\s+|\s+$/g, '');
    }

    function js_beautify(js_source_text, options) {
        "use strict";
        var beautifier = new Beautifier(js_source_text, options);
        return beautifier.beautify();
    }

    var MODE = {
            BlockStatement: 'BlockStatement', // 'BLOCK'
            Statement: 'Statement', // 'STATEMENT'
            ObjectLiteral: 'ObjectLiteral', // 'OBJECT',
            ArrayLiteral: 'ArrayLiteral', //'[EXPRESSION]',
            ForInitializer: 'ForInitializer', //'(FOR-EXPRESSION)',
            Conditional: 'Conditional', //'(COND-EXPRESSION)',
            Expression: 'Expression' //'(EXPRESSION)'
        };

    function Beautifier(js_source_text, options) {
        "use strict";
        var output
        var tokens = [], token_pos;
        var Tokenizer;
        var current_token;
        var last_type, last_last_text, indent_string;
        var flags, previous_flags, flag_store;
        var prefix;

        var handlers, opt;
        var baseIndentString = '';

        handlers = {
            'TK_START_EXPR': handle_start_expr,
            'TK_END_EXPR': handle_end_expr,
            'TK_START_BLOCK': handle_start_block,
            'TK_END_BLOCK': handle_end_block,
            'TK_WORD': handle_word,
            'TK_RESERVED': handle_word,
            'TK_SEMICOLON': handle_semicolon,
            'TK_STRING': handle_string,
            'TK_EQUALS': handle_equals,
            'TK_OPERATOR': handle_operator,
            'TK_COMMA': handle_comma,
            'TK_BLOCK_COMMENT': handle_block_comment,
            'TK_INLINE_COMMENT': handle_inline_comment,
            'TK_COMMENT': handle_comment,
            'TK_DOT': handle_dot,
            'TK_UNKNOWN': handle_unknown,
            'TK_EOF': handle_eof
        };

        function create_flags(flags_base, mode) {
            var next_indent_level = 0;
            if (flags_base) {
                next_indent_level = flags_base.indentation_level;
                if (!output.just_added_newline() &&
                    flags_base.line_indent_level > next_indent_level) {
                    next_indent_level = flags_base.line_indent_level;
                }
            }

            var next_flags = {
                mode: mode,
                parent: flags_base,
                last_text: flags_base ? flags_base.last_text : '', // last token text
                last_word: flags_base ? flags_base.last_word : '', // last 'TK_WORD' passed
                declaration_statement: false,
                declaration_assignment: false,
                multiline_frame: false,
                if_block: false,
                else_block: false,
                do_block: false,
                do_while: false,
                in_case_statement: false, // switch(..){ INSIDE HERE }
                in_case: false, // we're on the exact line with "case 0:"
                case_body: false, // the indented case-action block
                indentation_level: next_indent_level,
                line_indent_level: flags_base ? flags_base.line_indent_level : next_indent_level,
                start_line_index: output.get_line_number(),
                ternary_depth: 0
            };
            return next_flags;
        }

        // Some interpreters have unexpected results with foo = baz || bar;
        options = options ? options : {};
        opt = {};

        // compatibility
        if (options.braces_on_own_line !== undefined) { //graceful handling of deprecated option
            opt.brace_style = options.braces_on_own_line ? "expand" : "collapse";
        }
        opt.brace_style = options.brace_style ? options.brace_style : (opt.brace_style ? opt.brace_style : "collapse");

        // graceful handling of deprecated option
        if (opt.brace_style === "expand-strict") {
            opt.brace_style = "expand";
        }


        opt.indent_size = options.indent_size ? parseInt(options.indent_size, 10) : 4;
        opt.indent_char = options.indent_char ? options.indent_char : ' ';
        opt.preserve_newlines = (options.preserve_newlines === undefined) ? true : options.preserve_newlines;
        opt.break_chained_methods = (options.break_chained_methods === undefined) ? false : options.break_chained_methods;
        opt.max_preserve_newlines = (options.max_preserve_newlines === undefined) ? 0 : parseInt(options.max_preserve_newlines, 10);
        opt.space_in_paren = (options.space_in_paren === undefined) ? false : options.space_in_paren;
        opt.space_in_empty_paren = (options.space_in_empty_paren === undefined) ? false : options.space_in_empty_paren;
        opt.jslint_happy = (options.jslint_happy === undefined) ? false : options.jslint_happy;
        opt.space_after_anon_function = (options.space_after_anon_function === undefined) ? false : options.space_after_anon_function;
        opt.keep_array_indentation = (options.keep_array_indentation === undefined) ? false : options.keep_array_indentation;
        opt.space_before_conditional = (options.space_before_conditional === undefined) ? true : options.space_before_conditional;
        opt.unescape_strings = (options.unescape_strings === undefined) ? false : options.unescape_strings;
        opt.wrap_line_length = (options.wrap_line_length === undefined) ? 0 : parseInt(options.wrap_line_length, 10);
        opt.e4x = (options.e4x === undefined) ? false : options.e4x;
        opt.end_with_newline = (options.end_with_newline === undefined) ? false : options.end_with_newline;


        // force opt.space_after_anon_function to true if opt.jslint_happy
        if(opt.jslint_happy) {
            opt.space_after_anon_function = true;
        }

        if(options.indent_with_tabs){
            opt.indent_char = '\t';
            opt.indent_size = 1;
        }

        //----------------------------------
        indent_string = '';
        while (opt.indent_size > 0) {
            indent_string += opt.indent_char;
            opt.indent_size -= 1;
        }

        var preindent_index = 0;
        if(js_source_text && js_source_text.length) {
            while ( (js_source_text.charAt(preindent_index) === ' ' ||
                    js_source_text.charAt(preindent_index) === '\t')) {
                baseIndentString += js_source_text.charAt(preindent_index);
                preindent_index += 1;
            }
            js_source_text = js_source_text.substring(preindent_index);
        }

        last_type = 'TK_START_BLOCK'; // last token type
        last_last_text = ''; // pre-last token text
        output = new Output(indent_string, baseIndentString);


        // Stack of parsing/formatting states, including MODE.
        // We tokenize, parse, and output in an almost purely a forward-only stream of token input
        // and formatted output.  This makes the beautifier less accurate than full parsers
        // but also far more tolerant of syntax errors.
        //
        // For example, the default mode is MODE.BlockStatement. If we see a '{' we push a new frame of type
        // MODE.BlockStatement on the the stack, even though it could be object literal.  If we later
        // encounter a ":", we'll switch to to MODE.ObjectLiteral.  If we then see a ";",
        // most full parsers would die, but the beautifier gracefully falls back to
        // MODE.BlockStatement and continues on.
        flag_store = [];
        set_mode(MODE.BlockStatement);

        this.beautify = function() {

            /*jshint onevar:true */
            var local_token, sweet_code;
            Tokenizer = new tokenizer(js_source_text, opt, indent_string);
            tokens = Tokenizer.tokenize();
            token_pos = 0;

            while (local_token = get_token()) {
                for(var i = 0; i < local_token.comments_before.length; i++) {
                    // The cleanest handling of inline comments is to treat them as though they aren't there.
                    // Just continue formatting and the behavior should be logical.
                    // Also ignore unknown tokens.  Again, this should result in better behavior.
                    handle_token(local_token.comments_before[i]);
                }
                handle_token(local_token);

                last_last_text = flags.last_text;
                last_type = local_token.type;
                flags.last_text = local_token.text;

                token_pos += 1;
            }

            sweet_code = output.get_code();
            if (opt.end_with_newline) {
                sweet_code += '\n';
            }

            return sweet_code;
        };

        function handle_token(local_token) {
            var newlines = local_token.newlines;
            var keep_whitespace = opt.keep_array_indentation && is_array(flags.mode);

            if (keep_whitespace) {
                for (i = 0; i < newlines; i += 1) {
                    print_newline(i > 0);
                }
            } else {
                if (opt.max_preserve_newlines && newlines > opt.max_preserve_newlines) {
                    newlines = opt.max_preserve_newlines;
                }

                if (opt.preserve_newlines) {
                    if (local_token.newlines > 1) {
                        print_newline();
                        for (var i = 1; i < newlines; i += 1) {
                            print_newline(true);
                        }
                    }
                }
            }

            current_token = local_token;
            handlers[current_token.type]();
        }

        // we could use just string.split, but
        // IE doesn't like returning empty strings

        function split_newlines(s) {
            //return s.split(/\x0d\x0a|\x0a/);

            s = s.replace(/\x0d/g, '');
            var out = [],
                idx = s.indexOf("\n");
            while (idx !== -1) {
                out.push(s.substring(0, idx));
                s = s.substring(idx + 1);
                idx = s.indexOf("\n");
            }
            if (s.length) {
                out.push(s);
            }
            return out;
        }

        function allow_wrap_or_preserved_newline(force_linewrap) {
            force_linewrap = (force_linewrap === undefined) ? false : force_linewrap;

            // Never wrap the first token on a line
            if (output.just_added_newline()) {
                return
            }

            if ((opt.preserve_newlines && current_token.wanted_newline) || force_linewrap) {
                print_newline(false, true);
            } else if (opt.wrap_line_length) {
                var proposed_line_length = output.current_line.get_character_count() + current_token.text.length +
                    (output.space_before_token ? 1 : 0);
                if (proposed_line_length >= opt.wrap_line_length) {
                    print_newline(false, true);
                }
            }
        }

        function print_newline(force_newline, preserve_statement_flags) {
            if (!preserve_statement_flags) {
                if (flags.last_text !== ';' && flags.last_text !== ',' && flags.last_text !== '=' && last_type !== 'TK_OPERATOR') {
                    while (flags.mode === MODE.Statement && !flags.if_block && !flags.do_block) {
                        restore_mode();
                    }
                }
            }

            if (output.add_new_line(force_newline)) {
                flags.multiline_frame = true;
            }
        }

        function print_token_line_indentation() {
            if (output.just_added_newline()) {
                if (opt.keep_array_indentation && is_array(flags.mode) && current_token.wanted_newline) {
                    output.current_line.push(current_token.whitespace_before);
                    output.space_before_token = false;
                } else if (output.set_indent(flags.indentation_level)) {
                    flags.line_indent_level = flags.indentation_level;
                }
            }
        }

        function print_token(printable_token) {
            printable_token = printable_token || current_token.text;
            print_token_line_indentation();
            output.add_token(printable_token);
        }

        function indent() {
            flags.indentation_level += 1;
        }

        function deindent() {
            if (flags.indentation_level > 0 &&
                ((!flags.parent) || flags.indentation_level > flags.parent.indentation_level))
                flags.indentation_level -= 1;
        }

        function set_mode(mode) {
            if (flags) {
                flag_store.push(flags);
                previous_flags = flags;
            } else {
                previous_flags = create_flags(null, mode);
            }

            flags = create_flags(previous_flags, mode);
        }

        function is_array(mode) {
            return mode === MODE.ArrayLiteral;
        }

        function is_expression(mode) {
            return in_array(mode, [MODE.Expression, MODE.ForInitializer, MODE.Conditional]);
        }

        function restore_mode() {
            if (flag_store.length > 0) {
                previous_flags = flags;
                flags = flag_store.pop();
                if (previous_flags.mode === MODE.Statement) {
                    output.remove_redundant_indentation(previous_flags);
                }
            }
        }

        function start_of_object_property() {
            return flags.parent.mode === MODE.ObjectLiteral && flags.mode === MODE.Statement && (
                (flags.last_text === ':' && flags.ternary_depth === 0) || (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['get', 'set'])));
        }

        function start_of_statement() {
            if (
                    (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['var', 'let', 'const']) && current_token.type === 'TK_WORD') ||
                    (last_type === 'TK_RESERVED' && flags.last_text === 'do') ||
                    (last_type === 'TK_RESERVED' && flags.last_text === 'return' && !current_token.wanted_newline) ||
                    (last_type === 'TK_RESERVED' && flags.last_text === 'else' && !(current_token.type === 'TK_RESERVED' && current_token.text === 'if')) ||
                    (last_type === 'TK_END_EXPR' && (previous_flags.mode === MODE.ForInitializer || previous_flags.mode === MODE.Conditional)) ||
                    (last_type === 'TK_WORD' && flags.mode === MODE.BlockStatement
                        && !flags.in_case
                        && !(current_token.text === '--' || current_token.text === '++')
                        && current_token.type !== 'TK_WORD' && current_token.type !== 'TK_RESERVED') ||
                    (flags.mode === MODE.ObjectLiteral && (
                        (flags.last_text === ':' && flags.ternary_depth === 0) || (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['get', 'set']))))
                ) {

                set_mode(MODE.Statement);
                indent();

                if (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['var', 'let', 'const']) && current_token.type === 'TK_WORD') {
                    flags.declaration_statement = true;
                }

                // Issue #276:
                // If starting a new statement with [if, for, while, do], push to a new line.
                // if (a) if (b) if(c) d(); else e(); else f();
                if (!start_of_object_property()) {
                    allow_wrap_or_preserved_newline(
                        current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['do', 'for', 'if', 'while']));
                }

                return true;
            }
            return false;
        }

        function all_lines_start_with(lines, c) {
            for (var i = 0; i < lines.length; i++) {
                var line = trim(lines[i]);
                if (line.charAt(0) !== c) {
                    return false;
                }
            }
            return true;
        }

        function each_line_matches_indent(lines, indent) {
            var i = 0,
                len = lines.length,
                line;
            for (; i < len; i++) {
                line = lines[i];
                // allow empty lines to pass through
                if (line && line.indexOf(indent) !== 0) {
                    return false;
                }
            }
            return true;
        }

        function is_special_word(word) {
            return in_array(word, ['case', 'return', 'do', 'if', 'throw', 'else']);
        }

        function get_token(offset) {
            var index = token_pos + (offset || 0);
            return (index < 0 || index >= tokens.length) ? null : tokens[index];
        }

        function handle_start_expr() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            var next_mode = MODE.Expression;
            if (current_token.text === '[') {

                if (last_type === 'TK_WORD' || flags.last_text === ')') {
                    // this is array index specifier, break immediately
                    // a[x], fn()[x]
                    if (last_type === 'TK_RESERVED' && in_array(flags.last_text, Tokenizer.line_starters)) {
                        output.space_before_token = true;
                    }
                    set_mode(next_mode);
                    print_token();
                    indent();
                    if (opt.space_in_paren) {
                        output.space_before_token = true;
                    }
                    return;
                }

                next_mode = MODE.ArrayLiteral;
                if (is_array(flags.mode)) {
                    if (flags.last_text === '[' ||
                        (flags.last_text === ',' && (last_last_text === ']' || last_last_text === '}'))) {
                        // ], [ goes to new line
                        // }, [ goes to new line
                        if (!opt.keep_array_indentation) {
                            print_newline();
                        }
                    }
                }

            } else {
                if (last_type === 'TK_RESERVED' && flags.last_text === 'for') {
                    next_mode = MODE.ForInitializer;
                } else if (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['if', 'while'])) {
                    next_mode = MODE.Conditional;
                } else {
                    // next_mode = MODE.Expression;
                }
            }

            if (flags.last_text === ';' || last_type === 'TK_START_BLOCK') {
                print_newline();
            } else if (last_type === 'TK_END_EXPR' || last_type === 'TK_START_EXPR' || last_type === 'TK_END_BLOCK' || flags.last_text === '.') {
                // TODO: Consider whether forcing this is required.  Review failing tests when removed.
                allow_wrap_or_preserved_newline(current_token.wanted_newline);
                // do nothing on (( and )( and ][ and ]( and .(
            } else if (!(last_type === 'TK_RESERVED' && current_token.text === '(') && last_type !== 'TK_WORD' && last_type !== 'TK_OPERATOR') {
                output.space_before_token = true;
            } else if ((last_type === 'TK_RESERVED' && (flags.last_word === 'function' || flags.last_word === 'typeof')) ||
                (flags.last_text === '*' && last_last_text === 'function')) {
                // function() vs function ()
                if (opt.space_after_anon_function) {
                    output.space_before_token = true;
                }
            } else if (last_type === 'TK_RESERVED' && (in_array(flags.last_text, Tokenizer.line_starters) || flags.last_text === 'catch')) {
                if (opt.space_before_conditional) {
                    output.space_before_token = true;
                }
            }

            // Support of this kind of newline preservation.
            // a = (b &&
            //     (c || d));
            if (current_token.text === '(') {
                if (last_type === 'TK_EQUALS' || last_type === 'TK_OPERATOR') {
                    if (!start_of_object_property()) {
                        allow_wrap_or_preserved_newline();
                    }
                }
            }

            set_mode(next_mode);
            print_token();
            if (opt.space_in_paren) {
                output.space_before_token = true;
            }

            // In all cases, if we newline while inside an expression it should be indented.
            indent();
        }

        function handle_end_expr() {
            // statements inside expressions are not valid syntax, but...
            // statements must all be closed when their container closes
            while (flags.mode === MODE.Statement) {
                restore_mode();
            }

            if (flags.multiline_frame) {
                allow_wrap_or_preserved_newline(current_token.text === ']' && is_array(flags.mode) && !opt.keep_array_indentation);
            }

            if (opt.space_in_paren) {
                if (last_type === 'TK_START_EXPR' && ! opt.space_in_empty_paren) {
                    // () [] no inner space in empty parens like these, ever, ref #320
                    output.trim();
                    output.space_before_token = false;
                } else {
                    output.space_before_token = true;
                }
            }
            if (current_token.text === ']' && opt.keep_array_indentation) {
                print_token();
                restore_mode();
            } else {
                restore_mode();
                print_token();
            }
            output.remove_redundant_indentation(previous_flags);

            // do {} while () // no statement required after
            if (flags.do_while && previous_flags.mode === MODE.Conditional) {
                previous_flags.mode = MODE.Expression;
                flags.do_block = false;
                flags.do_while = false;

            }
        }

        function handle_start_block() {
            // Check if this is should be treated as a ObjectLiteral
            var next_token = get_token(1)
            var second_token = get_token(2)
            if (second_token && (
                    (second_token.text === ':' && in_array(next_token.type, ['TK_STRING', 'TK_WORD', 'TK_RESERVED']))
                    || (in_array(next_token.text, ['get', 'set']) && in_array(second_token.type, ['TK_WORD', 'TK_RESERVED']))
                )) {
                // We don't support TypeScript,but we didn't break it for a very long time.
                // We'll try to keep not breaking it.
                if (!in_array(last_last_text, ['class','interface'])) {
                    set_mode(MODE.ObjectLiteral);
                } else {
                    set_mode(MODE.BlockStatement);
                }
            } else {
                set_mode(MODE.BlockStatement);
            }

            var empty_braces = !next_token.comments_before.length &&  next_token.text === '}';
            var empty_anonymous_function = empty_braces && flags.last_word === 'function' &&
                last_type === 'TK_END_EXPR';

            if (opt.brace_style === "expand" ||
                (opt.brace_style === "none" && current_token.wanted_newline)) {
                if (last_type !== 'TK_OPERATOR' &&
                    (empty_anonymous_function ||
                        last_type === 'TK_EQUALS' ||
                        (last_type === 'TK_RESERVED' && is_special_word(flags.last_text) && flags.last_text !== 'else'))) {
                    output.space_before_token = true;
                } else {
                    print_newline(false, true);
                }
            } else { // collapse
                if (last_type !== 'TK_OPERATOR' && last_type !== 'TK_START_EXPR') {
                    if (last_type === 'TK_START_BLOCK') {
                        print_newline();
                    } else {
                        output.space_before_token = true;
                    }
                } else {
                    // if TK_OPERATOR or TK_START_EXPR
                    if (is_array(previous_flags.mode) && flags.last_text === ',') {
                        if (last_last_text === '}') {
                            // }, { in array context
                            output.space_before_token = true;
                        } else {
                            print_newline(); // [a, b, c, {
                        }
                    }
                }
            }
            print_token();
            indent();
        }

        function handle_end_block() {
            // statements must all be closed when their container closes
            while (flags.mode === MODE.Statement) {
                restore_mode();
            }
            var empty_braces = last_type === 'TK_START_BLOCK';

            if (opt.brace_style === "expand") {
                if (!empty_braces) {
                    print_newline();
                }
            } else {
                // skip {}
                if (!empty_braces) {
                    if (is_array(flags.mode) && opt.keep_array_indentation) {
                        // we REALLY need a newline here, but newliner would skip that
                        opt.keep_array_indentation = false;
                        print_newline();
                        opt.keep_array_indentation = true;

                    } else {
                        print_newline();
                    }
                }
            }
            restore_mode();
            print_token();
        }

        function handle_word() {
            if (current_token.type === 'TK_RESERVED' && flags.mode !== MODE.ObjectLiteral &&
                in_array(current_token.text, ['set', 'get'])) {
                current_token.type = 'TK_WORD';
            }

            if (current_token.type === 'TK_RESERVED' && flags.mode === MODE.ObjectLiteral) {
                var next_token = get_token(1);
                if (next_token.text == ':') {
                    current_token.type = 'TK_WORD';
                }
            }

            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            } else if (current_token.wanted_newline && !is_expression(flags.mode) &&
                (last_type !== 'TK_OPERATOR' || (flags.last_text === '--' || flags.last_text === '++')) &&
                last_type !== 'TK_EQUALS' &&
                (opt.preserve_newlines || !(last_type === 'TK_RESERVED' && in_array(flags.last_text, ['var', 'let', 'const', 'set', 'get'])))) {

                print_newline();
            }

            if (flags.do_block && !flags.do_while) {
                if (current_token.type === 'TK_RESERVED' && current_token.text === 'while') {
                    // do {} ## while ()
                    output.space_before_token = true;
                    print_token();
                    output.space_before_token = true;
                    flags.do_while = true;
                    return;
                } else {
                    // do {} should always have while as the next word.
                    // if we don't see the expected while, recover
                    print_newline();
                    flags.do_block = false;
                }
            }

            // if may be followed by else, or not
            // Bare/inline ifs are tricky
            // Need to unwind the modes correctly: if (a) if (b) c(); else d(); else e();
            if (flags.if_block) {
                if (!flags.else_block && (current_token.type === 'TK_RESERVED' && current_token.text === 'else')) {
                    flags.else_block = true;
                } else {
                    while (flags.mode === MODE.Statement) {
                        restore_mode();
                    }
                    flags.if_block = false;
                    flags.else_block = false;
                }
            }

            if (current_token.type === 'TK_RESERVED' && (current_token.text === 'case' || (current_token.text === 'default' && flags.in_case_statement))) {
                print_newline();
                if (flags.case_body || opt.jslint_happy) {
                    // switch cases following one another
                    deindent();
                    flags.case_body = false;
                }
                print_token();
                flags.in_case = true;
                flags.in_case_statement = true;
                return;
            }

            if (current_token.type === 'TK_RESERVED' && current_token.text === 'function') {
                if (in_array(flags.last_text, ['}', ';']) || (output.just_added_newline() && ! in_array(flags.last_text, ['[', '{', ':', '=', ',']))) {
                    // make sure there is a nice clean space of at least one blank line
                    // before a new function definition
                    if ( !output.just_added_blankline() && !current_token.comments_before.length) {
                        print_newline();
                        print_newline(true);
                    }
                }
                if (last_type === 'TK_RESERVED' || last_type === 'TK_WORD') {
                    if (last_type === 'TK_RESERVED' && in_array(flags.last_text, ['get', 'set', 'new', 'return', 'export'])) {
                        output.space_before_token = true;
                    } else if (last_type === 'TK_RESERVED' && flags.last_text === 'default' && last_last_text === 'export') {
                        output.space_before_token = true;
                    } else {
                        print_newline();
                    }
                } else if (last_type === 'TK_OPERATOR' || flags.last_text === '=') {
                    // foo = function
                    output.space_before_token = true;
                } else if (!flags.multiline_frame && (is_expression(flags.mode) || is_array(flags.mode))) {
                    // (function
                } else {
                    print_newline();
                }
            }

            if (last_type === 'TK_COMMA' || last_type === 'TK_START_EXPR' || last_type === 'TK_EQUALS' || last_type === 'TK_OPERATOR') {
                if (!start_of_object_property()) {
                    allow_wrap_or_preserved_newline();
                }
            }

            if (current_token.type === 'TK_RESERVED' &&  in_array(current_token.text, ['function', 'get', 'set'])) {
                print_token();
                flags.last_word = current_token.text;
                return;
            }

            prefix = 'NONE';

            if (last_type === 'TK_END_BLOCK') {
                if (!(current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['else', 'catch', 'finally']))) {
                    prefix = 'NEWLINE';
                } else {
                    if (opt.brace_style === "expand" ||
                        opt.brace_style === "end-expand" ||
                        (opt.brace_style === "none" && current_token.wanted_newline)) {
                        prefix = 'NEWLINE';
                    } else {
                        prefix = 'SPACE';
                        output.space_before_token = true;
                    }
                }
            } else if (last_type === 'TK_SEMICOLON' && flags.mode === MODE.BlockStatement) {
                // TODO: Should this be for STATEMENT as well?
                prefix = 'NEWLINE';
            } else if (last_type === 'TK_SEMICOLON' && is_expression(flags.mode)) {
                prefix = 'SPACE';
            } else if (last_type === 'TK_STRING') {
                prefix = 'NEWLINE';
            } else if (last_type === 'TK_RESERVED' || last_type === 'TK_WORD' ||
                (flags.last_text === '*' && last_last_text === 'function')) {
                prefix = 'SPACE';
            } else if (last_type === 'TK_START_BLOCK') {
                prefix = 'NEWLINE';
            } else if (last_type === 'TK_END_EXPR') {
                output.space_before_token = true;
                prefix = 'NEWLINE';
            }

            if (current_token.type === 'TK_RESERVED' && in_array(current_token.text, Tokenizer.line_starters) && flags.last_text !== ')') {
                if (flags.last_text === 'else' || flags.last_text === 'export') {
                    prefix = 'SPACE';
                } else {
                    prefix = 'NEWLINE';
                }

            }

            if (current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['else', 'catch', 'finally'])) {
                if (last_type !== 'TK_END_BLOCK' ||
                    opt.brace_style === "expand" ||
                    opt.brace_style === "end-expand" ||
                    (opt.brace_style === "none" && current_token.wanted_newline)) {
                    print_newline();
                } else {
                    output.trim(true);
                    var line = output.current_line;
                    // If we trimmed and there's something other than a close block before us
                    // put a newline back in.  Handles '} // comment' scenario.
                    if (line.last() !== '}') {
                        print_newline();
                    }
                    output.space_before_token = true;
                }
            } else if (prefix === 'NEWLINE') {
                if (last_type === 'TK_RESERVED' && is_special_word(flags.last_text)) {
                    // no newline between 'return nnn'
                    output.space_before_token = true;
                } else if (last_type !== 'TK_END_EXPR') {
                    if ((last_type !== 'TK_START_EXPR' || !(current_token.type === 'TK_RESERVED' && in_array(current_token.text, ['var', 'let', 'const']))) && flags.last_text !== ':') {
                        // no need to force newline on 'var': for (var x = 0...)
                        if (current_token.type === 'TK_RESERVED' && current_token.text === 'if' && flags.last_text === 'else') {
                            // no newline for } else if {
                            output.space_before_token = true;
                        } else {
                            print_newline();
                        }
                    }
                } else if (current_token.type === 'TK_RESERVED' && in_array(current_token.text, Tokenizer.line_starters) && flags.last_text !== ')') {
                    print_newline();
                }
            } else if (flags.multiline_frame && is_array(flags.mode) && flags.last_text === ',' && last_last_text === '}') {
                print_newline(); // }, in lists get a newline treatment
            } else if (prefix === 'SPACE') {
                output.space_before_token = true;
            }
            print_token();
            flags.last_word = current_token.text;

            if (current_token.type === 'TK_RESERVED' && current_token.text === 'do') {
                flags.do_block = true;
            }

            if (current_token.type === 'TK_RESERVED' && current_token.text === 'if') {
                flags.if_block = true;
            }
        }

        function handle_semicolon() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
                // Semicolon can be the start (and end) of a statement
                output.space_before_token = false;
            }
            while (flags.mode === MODE.Statement && !flags.if_block && !flags.do_block) {
                restore_mode();
            }
            print_token();
        }

        function handle_string() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
                // One difference - strings want at least a space before
                output.space_before_token = true;
            } else if (last_type === 'TK_RESERVED' || last_type === 'TK_WORD') {
                output.space_before_token = true;
            } else if (last_type === 'TK_COMMA' || last_type === 'TK_START_EXPR' || last_type === 'TK_EQUALS' || last_type === 'TK_OPERATOR') {
                if (!start_of_object_property()) {
                    allow_wrap_or_preserved_newline();
                }
            } else {
                print_newline();
            }
            print_token();
        }

        function handle_equals() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            if (flags.declaration_statement) {
                // just got an '=' in a var-line, different formatting/line-breaking, etc will now be done
                flags.declaration_assignment = true;
            }
            output.space_before_token = true;
            print_token();
            output.space_before_token = true;
        }

        function handle_comma() {
            if (flags.declaration_statement) {
                if (is_expression(flags.parent.mode)) {
                    // do not break on comma, for(var a = 1, b = 2)
                    flags.declaration_assignment = false;
                }

                print_token();

                if (flags.declaration_assignment) {
                    flags.declaration_assignment = false;
                    print_newline(false, true);
                } else {
                    output.space_before_token = true;
                }
                return;
            }

            print_token();
            if (flags.mode === MODE.ObjectLiteral ||
                (flags.mode === MODE.Statement && flags.parent.mode === MODE.ObjectLiteral)) {
                if (flags.mode === MODE.Statement) {
                    restore_mode();
                }
                print_newline();
            } else {
                // EXPR or DO_BLOCK
                output.space_before_token = true;
            }

        }

        function handle_operator() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            if (last_type === 'TK_RESERVED' && is_special_word(flags.last_text)) {
                // "return" had a special handling in TK_WORD. Now we need to return the favor
                output.space_before_token = true;
                print_token();
                return;
            }

            // hack for actionscript's import .*;
            if (current_token.text === '*' && last_type === 'TK_DOT') {
                print_token();
                return;
            }

            if (current_token.text === ':' && flags.in_case) {
                flags.case_body = true;
                indent();
                print_token();
                print_newline();
                flags.in_case = false;
                return;
            }

            if (current_token.text === '::') {
                // no spaces around exotic namespacing syntax operator
                print_token();
                return;
            }

            // http://www.ecma-international.org/ecma-262/5.1/#sec-7.9.1
            // if there is a newline between -- or ++ and anything else we should preserve it.
            if (current_token.wanted_newline && (current_token.text === '--' || current_token.text === '++')) {
                print_newline(false, true);
            }

            // Allow line wrapping between operators
            if (last_type === 'TK_OPERATOR') {
                allow_wrap_or_preserved_newline();
            }

            var space_before = true;
            var space_after = true;

            if (in_array(current_token.text, ['--', '++', '!', '~']) || (in_array(current_token.text, ['-', '+']) && (in_array(last_type, ['TK_START_BLOCK', 'TK_START_EXPR', 'TK_EQUALS', 'TK_OPERATOR']) || in_array(flags.last_text, Tokenizer.line_starters) || flags.last_text === ','))) {
                // unary operators (and binary +/- pretending to be unary) special cases

                space_before = false;
                space_after = false;

                if (flags.last_text === ';' && is_expression(flags.mode)) {
                    // for (;; ++i)
                    //        ^^^
                    space_before = true;
                }

                if (last_type === 'TK_RESERVED' || last_type === 'TK_END_EXPR') {
                    space_before = true;
                } else if (last_type === 'TK_OPERATOR') {
                    space_before =
                        (in_array(current_token.text, ['--', '-']) && in_array(flags.last_text, ['--', '-'])) ||
                        (in_array(current_token.text, ['++', '+']) && in_array(flags.last_text, ['++', '+']));
                }

                if ((flags.mode === MODE.BlockStatement || flags.mode === MODE.Statement) && (flags.last_text === '{' || flags.last_text === ';')) {
                    // { foo; --i }
                    // foo(); --bar;
                    print_newline();
                }
            } else if (current_token.text === ':') {
                if (flags.ternary_depth === 0) {
                    // Colon is invalid javascript outside of ternary and object, but do our best to guess what was meant.
                    space_before = false;
                } else {
                    flags.ternary_depth -= 1;
                }
            } else if (current_token.text === '?') {
                flags.ternary_depth += 1;
            } else if (current_token.text === '*' && last_type === 'TK_RESERVED' && flags.last_text === 'function') {
                space_before = false;
                space_after = false;
            }
            output.space_before_token = output.space_before_token || space_before;
            print_token();
            output.space_before_token = space_after;
        }

        function handle_block_comment() {
            var lines = split_newlines(current_token.text);
            var j; // iterator for this case
            var javadoc = false;
            var starless = false;
            var lastIndent = current_token.whitespace_before;
            var lastIndentLength = lastIndent.length;

            // block comment starts with a new line
            print_newline(false, true);
            if (lines.length > 1) {
                if (all_lines_start_with(lines.slice(1), '*')) {
                    javadoc = true;
                }
                else if (each_line_matches_indent(lines.slice(1), lastIndent)) {
                    starless = true;
                }
            }

            // first line always indented
            print_token(lines[0]);
            for (j = 1; j < lines.length; j++) {
                print_newline(false, true);
                if (javadoc) {
                    // javadoc: reformat and re-indent
                    print_token(' ' + trim(lines[j]));
                } else if (starless && lines[j].length > lastIndentLength) {
                    // starless: re-indent non-empty content, avoiding trim
                    print_token(lines[j].substring(lastIndentLength));
                } else {
                    // normal comments output raw
                    output.add_token(lines[j]);
                }
            }

            // for comments of more than one line, make sure there's a new line after
            print_newline(false, true);
        }

        function handle_inline_comment() {
            output.space_before_token = true;
            print_token();
            output.space_before_token = true;
        }

        function handle_comment() {
            if (current_token.wanted_newline) {
                print_newline(false, true);
            } else {
                output.trim(true);
            }

            output.space_before_token = true;
            print_token();
            print_newline(false, true);
        }

        function handle_dot() {
            if (start_of_statement()) {
                // The conditional starts the statement if appropriate.
            }

            if (last_type === 'TK_RESERVED' && is_special_word(flags.last_text)) {
                output.space_before_token = true;
            } else {
                // allow preserved newlines before dots in general
                // force newlines on dots after close paren when break_chained - for bar().baz()
                allow_wrap_or_preserved_newline(flags.last_text === ')' && opt.break_chained_methods);
            }

            print_token();
        }

        function handle_unknown() {
            print_token();

            if (current_token.text[current_token.text.length - 1] === '\n') {
                print_newline();
            }
        }

        function handle_eof() {
            // Unwind any open statements
            while (flags.mode === MODE.Statement) {
                restore_mode();
            }
        }
    }


    function OutputLine(parent) {
        var _character_count = 0;
        // use indent_count as a marker for lines that have preserved indentation
        var _indent_count = -1;

        var _items = [];
        var _empty = true;

        this.set_indent = function(level) {
            _character_count = parent.baseIndentLength + level * parent.indent_length
            _indent_count = level;
        }

        this.get_character_count = function() {
            return _character_count;
        }

        this.is_empty = function() {
            return _empty;
        }

        this.last = function() {
            if (!this._empty) {
              return _items[_items.length - 1];
            } else {
              return null;
            }
        }

        this.push = function(input) {
            _items.push(input);
            _character_count += input.length;
            _empty = false;
        }

        this.remove_indent = function() {
            if (_indent_count > 0) {
                _indent_count -= 1;
                _character_count -= parent.indent_length
            }
        }

        this.trim = function() {
            while (this.last() === ' ') {
                var item = _items.pop();
                _character_count -= 1;
            }
            _empty = _items.length === 0;
        }

        this.toString = function() {
            var result = '';
            if (!this._empty) {
                if (_indent_count >= 0) {
                    result = parent.indent_cache[_indent_count];
                }
                result += _items.join('')
            }
            return result;
        }
    }

    function Output(indent_string, baseIndentString) {
        baseIndentString = baseIndentString || '';
        this.indent_cache = [ baseIndentString ];
        this.baseIndentLength = baseIndentString.length;
        this.indent_length = indent_string.length;

        var lines =[];
        this.baseIndentString = baseIndentString;
        this.indent_string = indent_string;
        this.current_line = null;
        this.space_before_token = false;

        this.get_line_number = function() {
            return lines.length;
        }

        // Using object instead of string to allow for later expansion of info about each line
        this.add_new_line = function(force_newline) {
            if (this.get_line_number() === 1 && this.just_added_newline()) {
                return false; // no newline on start of file
            }

            if (force_newline || !this.just_added_newline()) {
                this.current_line = new OutputLine(this);
                lines.push(this.current_line);
                return true;
            }

            return false;
        }

        // initialize
        this.add_new_line(true);

        this.get_code = function() {
            var sweet_code = lines.join('\n').replace(/[\r\n\t ]+$/, '');
            return sweet_code;
        }

        this.set_indent = function(level) {
            // Never indent your first output indent at the start of the file
            if (lines.length > 1) {
                while(level >= this.indent_cache.length) {
                    this.indent_cache.push(this.indent_cache[this.indent_cache.length - 1] + this.indent_string);
                }

                this.current_line.set_indent(level);
                return true;
            }
            this.current_line.set_indent(0);
            return false;
        }

        this.add_token = function(printable_token) {
            this.add_space_before_token();
            this.current_line.push(printable_token);
        }

        this.add_space_before_token = function() {
            if (this.space_before_token && !this.just_added_newline()) {
                this.current_line.push(' ');
            }
            this.space_before_token = false;
        }

        this.remove_redundant_indentation = function (frame) {
            // This implementation is effective but has some issues:
            //     - can cause line wrap to happen too soon due to indent removal
            //           after wrap points are calculated
            // These issues are minor compared to ugly indentation.

            if (frame.multiline_frame ||
                frame.mode === MODE.ForInitializer ||
                frame.mode === MODE.Conditional) {
                return;
            }

            // remove one indent from each line inside this section
            var index = frame.start_line_index;
            var line;

            var output_length = lines.length;
            while (index < output_length) {
                lines[index].remove_indent();
                index++;
            }
        }

        this.trim = function(eat_newlines) {
            eat_newlines = (eat_newlines === undefined) ? false : eat_newlines;

            this.current_line.trim(indent_string, baseIndentString);

            while (eat_newlines && lines.length > 1 &&
                this.current_line.is_empty()) {
                lines.pop();
                this.current_line = lines[lines.length - 1]
                this.current_line.trim();
            }
        }

        this.just_added_newline = function() {
            return this.current_line.is_empty();
        }

        this.just_added_blankline = function() {
            if (this.just_added_newline()) {
                if (lines.length === 1) {
                    return true; // start of the file and newline = blank
                }

                var line = lines[lines.length - 2];
                return line.is_empty();
            }
            return false;
        }
    }


    var Token = function(type, text, newlines, whitespace_before, mode, parent) {
        this.type = type;
        this.text = text;
        this.comments_before = [];
        this.newlines = newlines || 0;
        this.wanted_newline = newlines > 0;
        this.whitespace_before = whitespace_before || '';
        this.parent = null;
    }

    function tokenizer(input, opts, indent_string) {

        var whitespace = "\n\r\t ".split('');
        var digit = /[0-9]/;

        var punct = ('+ - * / % & ++ -- = += -= *= /= %= == === != !== > < >= <= >> << >>> >>>= >>= <<= && &= | || ! ~ , : ? ^ ^= |= :: =>'
                +' <%= <% %> <?= <? ?>').split(' '); // try to be a good boy and try not to break the markup language identifiers

        // words which should always start on new line.
        this.line_starters = 'continue,try,throw,return,var,let,const,if,switch,case,default,for,while,break,function,yield,import,export'.split(',');
        var reserved_words = this.line_starters.concat(['do', 'in', 'else', 'get', 'set', 'new', 'catch', 'finally', 'typeof']);

        var n_newlines, whitespace_before_token, in_html_comment, tokens, parser_pos;
        var input_length;

        this.tokenize = function() {
            // cache the source's length.
            input_length = input.length
            parser_pos = 0;
            in_html_comment = false
            tokens = [];

            var next, last;
            var token_values;
            var open = null;
            var open_stack = [];
            var comments = [];

            while (!(last && last.type === 'TK_EOF')) {
                token_values = tokenize_next();
                next = new Token(token_values[1], token_values[0], n_newlines, whitespace_before_token);
                while(next.type === 'TK_INLINE_COMMENT' || next.type === 'TK_COMMENT' ||
                    next.type === 'TK_BLOCK_COMMENT' || next.type === 'TK_UNKNOWN') {
                    comments.push(next);
                    token_values = tokenize_next();
                    next = new Token(token_values[1], token_values[0], n_newlines, whitespace_before_token);
                }

                if (comments.length) {
                    next.comments_before = comments;
                    comments = [];
                }

                if (next.type === 'TK_START_BLOCK' || next.type === 'TK_START_EXPR') {
                    next.parent = last;
                    open = next;
                    open_stack.push(next);
                }  else if ((next.type === 'TK_END_BLOCK' || next.type === 'TK_END_EXPR') &&
                    (open && (
                        (next.text === ']' && open.text === '[') ||
                        (next.text === ')' && open.text === '(') ||
                        (next.text === '}' && open.text === '}')))) {
                    next.parent = open.parent;
                    open = open_stack.pop();
                }

                tokens.push(next);
                last = next;
            }

            return tokens;
        }

        function tokenize_next() {
            var i, resulting_string;
            var whitespace_on_this_line = [];

            n_newlines = 0;
            whitespace_before_token = '';

            if (parser_pos >= input_length) {
                return ['', 'TK_EOF'];
            }

            var last_token;
            if (tokens.length) {
                last_token = tokens[tokens.length-1];
            } else {
                // For the sake of tokenizing we can pretend that there was on open brace to start
                last_token = new Token('TK_START_BLOCK', '{');
            }


            var c = input.charAt(parser_pos);
            parser_pos += 1;

            while (in_array(c, whitespace)) {

                if (c === '\n') {
                    n_newlines += 1;
                    whitespace_on_this_line = [];
                } else if (n_newlines) {
                    if (c === indent_string) {
                        whitespace_on_this_line.push(indent_string);
                    } else if (c !== '\r') {
                        whitespace_on_this_line.push(' ');
                    }
                }

                if (parser_pos >= input_length) {
                    return ['', 'TK_EOF'];
                }

                c = input.charAt(parser_pos);
                parser_pos += 1;
            }

            if(whitespace_on_this_line.length) {
                whitespace_before_token = whitespace_on_this_line.join('');
            }

            if (digit.test(c)) {
                var allow_decimal = true;
                var allow_e = true;
                var local_digit = digit;

                if (c === '0' && parser_pos < input_length && /[Xx]/.test(input.charAt(parser_pos))) {
                    // switch to hex number, no decimal or e, just hex digits
                    allow_decimal = false;
                    allow_e = false;
                    c += input.charAt(parser_pos);
                    parser_pos += 1;
                    local_digit = /[0123456789abcdefABCDEF]/
                } else {
                    // we know this first loop will run.  It keeps the logic simpler.
                    c = '';
                    parser_pos -= 1
                }

                // Add the digits
                while (parser_pos < input_length && local_digit.test(input.charAt(parser_pos))) {
                    c += input.charAt(parser_pos);
                    parser_pos += 1;

                    if (allow_decimal && parser_pos < input_length && input.charAt(parser_pos) === '.') {
                        c += input.charAt(parser_pos);
                        parser_pos += 1;
                        allow_decimal = false;
                    }

                    if (allow_e && parser_pos < input_length && /[Ee]/.test(input.charAt(parser_pos))) {
                        c += input.charAt(parser_pos);
                        parser_pos += 1;

                        if (parser_pos < input_length && /[+-]/.test(input.charAt(parser_pos))) {
                            c += input.charAt(parser_pos);
                            parser_pos += 1;
                        }

                        allow_e = false;
                        allow_decimal = false;
                    }
                }

                return [c, 'TK_WORD'];
            }

            if (acorn.isIdentifierStart(input.charCodeAt(parser_pos-1))) {
                if (parser_pos < input_length) {
                    while (acorn.isIdentifierChar(input.charCodeAt(parser_pos))) {
                        c += input.charAt(parser_pos);
                        parser_pos += 1;
                        if (parser_pos === input_length) {
                            break;
                        }
                    }
                }

                if (!(last_token.type === 'TK_DOT' ||
                        (last_token.type === 'TK_RESERVED' && in_array(last_token.text, ['set', 'get'])))
                    && in_array(c, reserved_words)) {
                    if (c === 'in') { // hack for 'in' operator
                        return [c, 'TK_OPERATOR'];
                    }
                    return [c, 'TK_RESERVED'];
                }

                return [c, 'TK_WORD'];
            }

            if (c === '(' || c === '[') {
                return [c, 'TK_START_EXPR'];
            }

            if (c === ')' || c === ']') {
                return [c, 'TK_END_EXPR'];
            }

            if (c === '{') {
                return [c, 'TK_START_BLOCK'];
            }

            if (c === '}') {
                return [c, 'TK_END_BLOCK'];
            }

            if (c === ';') {
                return [c, 'TK_SEMICOLON'];
            }

            if (c === '/') {
                var comment = '';
                // peek for comment /* ... */
                var inline_comment = true;
                if (input.charAt(parser_pos) === '*') {
                    parser_pos += 1;
                    if (parser_pos < input_length) {
                        while (parser_pos < input_length && !(input.charAt(parser_pos) === '*' && input.charAt(parser_pos + 1) && input.charAt(parser_pos + 1) === '/')) {
                            c = input.charAt(parser_pos);
                            comment += c;
                            if (c === "\n" || c === "\r") {
                                inline_comment = false;
                            }
                            parser_pos += 1;
                            if (parser_pos >= input_length) {
                                break;
                            }
                        }
                    }
                    parser_pos += 2;
                    if (inline_comment && n_newlines === 0) {
                        return ['/*' + comment + '*/', 'TK_INLINE_COMMENT'];
                    } else {
                        return ['/*' + comment + '*/', 'TK_BLOCK_COMMENT'];
                    }
                }
                // peek for comment // ...
                if (input.charAt(parser_pos) === '/') {
                    comment = c;
                    while (input.charAt(parser_pos) !== '\r' && input.charAt(parser_pos) !== '\n') {
                        comment += input.charAt(parser_pos);
                        parser_pos += 1;
                        if (parser_pos >= input_length) {
                            break;
                        }
                    }
                    return [comment, 'TK_COMMENT'];
                }

            }

            if (c === '`' || c === "'" || c === '"' || // string
                (
                    (c === '/') || // regexp
                    (opts.e4x && c === "<" && input.slice(parser_pos - 1).match(/^<([-a-zA-Z:0-9_.]+|{[^{}]*}|!\[CDATA\[[\s\S]*?\]\])\s*([-a-zA-Z:0-9_.]+=('[^']*'|"[^"]*"|{[^{}]*})\s*)*\/?\s*>/)) // xml
                ) && ( // regex and xml can only appear in specific locations during parsing
                    (last_token.type === 'TK_RESERVED' && in_array(last_token.text , ['return', 'case', 'throw', 'else', 'do', 'typeof', 'yield'])) ||
                    (last_token.type === 'TK_END_EXPR' && last_token.text === ')' &&
                        last_token.parent && last_token.parent.type === 'TK_RESERVED' && in_array(last_token.parent.text, ['if', 'while', 'for'])) ||
                    (in_array(last_token.type, ['TK_COMMENT', 'TK_START_EXPR', 'TK_START_BLOCK',
                        'TK_END_BLOCK', 'TK_OPERATOR', 'TK_EQUALS', 'TK_EOF', 'TK_SEMICOLON', 'TK_COMMA'
                    ]))
                )) {

                var sep = c,
                    esc = false,
                    has_char_escapes = false;

                resulting_string = c;

                if (sep === '/') {
                    //
                    // handle regexp
                    //
                    var in_char_class = false;
                    while (parser_pos < input_length &&
                            ((esc || in_char_class || input.charAt(parser_pos) !== sep) &&
                            !acorn.newline.test(input.charAt(parser_pos)))) {
                        resulting_string += input.charAt(parser_pos);
                        if (!esc) {
                            esc = input.charAt(parser_pos) === '\\';
                            if (input.charAt(parser_pos) === '[') {
                                in_char_class = true;
                            } else if (input.charAt(parser_pos) === ']') {
                                in_char_class = false;
                            }
                        } else {
                            esc = false;
                        }
                        parser_pos += 1;
                    }
                } else if (opts.e4x && sep === '<') {
                    //
                    // handle e4x xml literals
                    //
                    var xmlRegExp = /<(\/?)([-a-zA-Z:0-9_.]+|{[^{}]*}|!\[CDATA\[[\s\S]*?\]\])\s*([-a-zA-Z:0-9_.]+=('[^']*'|"[^"]*"|{[^{}]*})\s*)*(\/?)\s*>/g;
                    var xmlStr = input.slice(parser_pos - 1);
                    var match = xmlRegExp.exec(xmlStr);
                    if (match && match.index === 0) {
                        var rootTag = match[2];
                        var depth = 0;
                        while (match) {
                            var isEndTag = !! match[1];
                            var tagName = match[2];
                            var isSingletonTag = ( !! match[match.length - 1]) || (tagName.slice(0, 8) === "![CDATA[");
                            if (tagName === rootTag && !isSingletonTag) {
                                if (isEndTag) {
                                    --depth;
                                } else {
                                    ++depth;
                                }
                            }
                            if (depth <= 0) {
                                break;
                            }
                            match = xmlRegExp.exec(xmlStr);
                        }
                        var xmlLength = match ? match.index + match[0].length : xmlStr.length;
                        parser_pos += xmlLength - 1;
                        return [xmlStr.slice(0, xmlLength), "TK_STRING"];
                    }
                } else {
                    //
                    // handle string
                    //
                    // Template strings can travers lines without escape characters.
                    // Other strings cannot
                    while (parser_pos < input_length &&
                            (esc || (input.charAt(parser_pos) !== sep &&
                            (sep === '`' || !acorn.newline.test(input.charAt(parser_pos)))))) {
                        resulting_string += input.charAt(parser_pos);
                        if (esc) {
                            if (input.charAt(parser_pos) === 'x' || input.charAt(parser_pos) === 'u') {
                                has_char_escapes = true;
                            }
                            esc = false;
                        } else {
                            esc = input.charAt(parser_pos) === '\\';
                        }
                        parser_pos += 1;
                    }

                }

                if (has_char_escapes && opts.unescape_strings) {
                    resulting_string = unescape_string(resulting_string);
                }

                if (parser_pos < input_length && input.charAt(parser_pos) === sep) {
                    resulting_string += sep;
                    parser_pos += 1;

                    if (sep === '/') {
                        // regexps may have modifiers /regexp/MOD , so fetch those, too
                        // Only [gim] are valid, but if the user puts in garbage, do what we can to take it.
                        while (parser_pos < input_length && acorn.isIdentifierStart(input.charCodeAt(parser_pos))) {
                            resulting_string += input.charAt(parser_pos);
                            parser_pos += 1;
                        }
                    }
                }
                return [resulting_string, 'TK_STRING'];
            }

            if (c === '#') {

                if (tokens.length === 0 && input.charAt(parser_pos) === '!') {
                    // shebang
                    resulting_string = c;
                    while (parser_pos < input_length && c !== '\n') {
                        c = input.charAt(parser_pos);
                        resulting_string += c;
                        parser_pos += 1;
                    }
                    return [trim(resulting_string) + '\n', 'TK_UNKNOWN'];
                }



                // Spidermonkey-specific sharp variables for circular references
                // https://developer.mozilla.org/En/Sharp_variables_in_JavaScript
                // http://mxr.mozilla.org/mozilla-central/source/js/src/jsscan.cpp around line 1935
                var sharp = '#';
                if (parser_pos < input_length && digit.test(input.charAt(parser_pos))) {
                    do {
                        c = input.charAt(parser_pos);
                        sharp += c;
                        parser_pos += 1;
                    } while (parser_pos < input_length && c !== '#' && c !== '=');
                    if (c === '#') {
                        //
                    } else if (input.charAt(parser_pos) === '[' && input.charAt(parser_pos + 1) === ']') {
                        sharp += '[]';
                        parser_pos += 2;
                    } else if (input.charAt(parser_pos) === '{' && input.charAt(parser_pos + 1) === '}') {
                        sharp += '{}';
                        parser_pos += 2;
                    }
                    return [sharp, 'TK_WORD'];
                }
            }

            if (c === '<' && input.substring(parser_pos - 1, parser_pos + 3) === '<!--') {
                parser_pos += 3;
                c = '<!--';
                while (input.charAt(parser_pos) !== '\n' && parser_pos < input_length) {
                    c += input.charAt(parser_pos);
                    parser_pos++;
                }
                in_html_comment = true;
                return [c, 'TK_COMMENT'];
            }

            if (c === '-' && in_html_comment && input.substring(parser_pos - 1, parser_pos + 2) === '-->') {
                in_html_comment = false;
                parser_pos += 2;
                return ['-->', 'TK_COMMENT'];
            }

            if (c === '.') {
                return [c, 'TK_DOT'];
            }

            if (in_array(c, punct)) {
                while (parser_pos < input_length && in_array(c + input.charAt(parser_pos), punct)) {
                    c += input.charAt(parser_pos);
                    parser_pos += 1;
                    if (parser_pos >= input_length) {
                        break;
                    }
                }

                if (c === ',') {
                    return [c, 'TK_COMMA'];
                } else if (c === '=') {
                    return [c, 'TK_EQUALS'];
                } else {
                    return [c, 'TK_OPERATOR'];
                }
            }

            return [c, 'TK_UNKNOWN'];
        }


        function unescape_string(s) {
            var esc = false,
                out = '',
                pos = 0,
                s_hex = '',
                escaped = 0,
                c;

            while (esc || pos < s.length) {

                c = s.charAt(pos);
                pos++;

                if (esc) {
                    esc = false;
                    if (c === 'x') {
                        // simple hex-escape \x24
                        s_hex = s.substr(pos, 2);
                        pos += 2;
                    } else if (c === 'u') {
                        // unicode-escape, \u2134
                        s_hex = s.substr(pos, 4);
                        pos += 4;
                    } else {
                        // some common escape, e.g \n
                        out += '\\' + c;
                        continue;
                    }
                    if (!s_hex.match(/^[0123456789abcdefABCDEF]+$/)) {
                        // some weird escaping, bail out,
                        // leaving whole string intact
                        return s;
                    }

                    escaped = parseInt(s_hex, 16);

                    if (escaped >= 0x00 && escaped < 0x20) {
                        // leave 0x00...0x1f escaped
                        if (c === 'x') {
                            out += '\\x' + s_hex;
                        } else {
                            out += '\\u' + s_hex;
                        }
                        continue;
                    } else if (escaped === 0x22 || escaped === 0x27 || escaped === 0x5c) {
                        // single-quote, apostrophe, backslash - escape these
                        out += '\\' + String.fromCharCode(escaped);
                    } else if (c === 'x' && escaped > 0x7e && escaped <= 0xff) {
                        // we bail out on \x7f..\xff,
                        // leaving whole string escaped,
                        // as it's probably completely binary
                        return s;
                    } else {
                        out += String.fromCharCode(escaped);
                    }
                } else if (c === '\\') {
                    esc = true;
                } else {
                    out += c;
                }
            }
            return out;
        }

    }


    if (typeof define === "function" && define.amd) {
        // Add support for AMD ( https://github.com/amdjs/amdjs-api/wiki/AMD#defineamd-property- )
        define([], function() {
            return { js_beautify: js_beautify };
        });
    } else if (typeof exports !== "undefined") {
        // Add support for CommonJS. Just put this file somewhere on your require.paths
        // and you will be able to `var js_beautify = require("beautify").js_beautify`.
        exports.js_beautify = js_beautify;
    } else if (typeof window !== "undefined") {
        // If we're running a web page and don't have either of the above, add our one global
        window.js_beautify = js_beautify;
    } else if (typeof global !== "undefined") {
        // If we don't even have window, try global.
        global.js_beautify = js_beautify;
    }

}());
/*jshint curly:true, eqeqeq:true, laxbreak:true, noempty:false */
/*

  The MIT License (MIT)

  Copyright (c) 2007-2013 Einar Lielmanis and contributors.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.


 CSS Beautifier
---------------

    Written by Harutyun Amirjanyan, (amirjanyan@gmail.com)

    Based on code initially developed by: Einar Lielmanis, <einar@jsbeautifier.org>
        http://jsbeautifier.org/

    Usage:
        css_beautify(source_text);
        css_beautify(source_text, options);

    The options are (default in brackets):
        indent_size (4)                   — indentation size,
        indent_char (space)               — character to indent with,
        selector_separator_newline (true) - separate selectors with newline or
                                            not (e.g. "a,\nbr" or "a, br")
        end_with_newline (false)          - end with a newline

    e.g

    css_beautify(css_source_text, {
      'indent_size': 1,
      'indent_char': '\t',
      'selector_separator': ' ',
      'end_with_newline': false,
    });
*/

// http://www.w3.org/TR/CSS21/syndata.html#tokenization
// http://www.w3.org/TR/css3-syntax/

(function() {
    function css_beautify(source_text, options) {
        options = options || {};
        var indentSize = options.indent_size || 4;
        var indentCharacter = options.indent_char || ' ';
        var selectorSeparatorNewline = (options.selector_separator_newline === undefined) ? true : options.selector_separator_newline;
        var end_with_newline = (options.end_with_newline === undefined) ? false : options.end_with_newline;

        // compatibility
        if (typeof indentSize === "string") {
            indentSize = parseInt(indentSize, 10);
        }


        // tokenizer
        var whiteRe = /^\s+$/;
        var wordRe = /[\w$\-_]/;

        var pos = -1,
            ch;

        function next() {
            ch = source_text.charAt(++pos);
            return ch || '';
        }

        function peek(skipWhitespace) {
            var prev_pos = pos;
            if (skipWhitespace) {
                eatWhitespace();
            }
            result = source_text.charAt(pos + 1) || '';
            pos = prev_pos - 1;
            next();
            return result;
        }

        function eatString(endChars) {
            var start = pos;
            while (next()) {
                if (ch === "\\") {
                    next();
                } else if (endChars.indexOf(ch) !== -1) {
                    break;
                } else if (ch === "\n") {
                    break;
                }
            }
            return source_text.substring(start, pos + 1);
        }

        function peekString(endChar) {
            var prev_pos = pos;
            var str = eatString(endChar);
            pos = prev_pos - 1;
            next();
            return str;
        }

        function eatWhitespace() {
            var result = '';
            while (whiteRe.test(peek())) {
                next()
                result += ch;
            }
            return result;
        }

        function skipWhitespace() {
            var result = '';
            if (ch && whiteRe.test(ch)) {
                result = ch;
            }
            while (whiteRe.test(next())) {
                result += ch
            }
            return result;
        }

        function eatComment(singleLine) {
            var start = pos;
            var singleLine = peek() === "/";
            next();
            while (next()) {
                if (!singleLine && ch === "*" && peek() === "/") {
                    next();
                    break;
                } else if (singleLine && ch === "\n") {
                    return source_text.substring(start, pos);
                }
            }

            return source_text.substring(start, pos) + ch;
        }


        function lookBack(str) {
            return source_text.substring(pos - str.length, pos).toLowerCase() ===
                str;
        }

        // Nested pseudo-class if we are insideRule
        // and the next special character found opens
        // a new block
        function foundNestedPseudoClass() {
            for (var i = pos + 1; i < source_text.length; i++){
                var ch = source_text.charAt(i);
                if (ch === "{"){
                    return true;
                } else if (ch === ";" || ch === "}" || ch === ")") {
                    return false;
                }
            }
            return false;
        }

        // printer
        var basebaseIndentString = source_text.match(/^[\t ]*/)[0];
        var singleIndent = new Array(indentSize + 1).join(indentCharacter);
        var indentLevel = 0;
        var nestedLevel = 0;

        function indent() {
            indentLevel++;
            basebaseIndentString += singleIndent;
        }

        function outdent() {
            indentLevel--;
            basebaseIndentString = basebaseIndentString.slice(0, -indentSize);
        }

        var print = {};
        print["{"] = function(ch) {
            print.singleSpace();
            output.push(ch);
            print.newLine();
        };
        print["}"] = function(ch) {
            print.newLine();
            output.push(ch);
            print.newLine();
        };

        print._lastCharWhitespace = function() {
            return whiteRe.test(output[output.length - 1]);
        };

        print.newLine = function(keepWhitespace) {
            if (!keepWhitespace) {
                print.trim();
            }

            if (output.length) {
                output.push('\n');
            }
            if (basebaseIndentString) {
                output.push(basebaseIndentString);
            }
        };
        print.singleSpace = function() {
            if (output.length && !print._lastCharWhitespace()) {
                output.push(' ');
            }
        };

        print.trim = function() {
            while (print._lastCharWhitespace()) {
                output.pop();
            }
        };

        
        var output = [];
        if (basebaseIndentString) {
            output.push(basebaseIndentString);
        }
        /*_____________________--------------------_____________________*/

        var insideRule = false;
        var enteringConditionalGroup = false;
        var top_ch = '';
        var last_top_ch = '';

        while (true) {
            var whitespace = skipWhitespace();
            var isAfterSpace = whitespace !== '';
            var isAfterNewline = whitespace.indexOf('\n') !== -1;
            var last_top_ch = top_ch;
            var top_ch = ch;

            if (!ch) {
                break;
            } else if (ch === '/' && peek() === '*') { /* css comment */
                var header = lookBack("");
                print.newLine();
                output.push(eatComment());
                print.newLine();
                if (header) {
                    print.newLine(true);
                }
            } else if (ch === '/' && peek() === '/') { // single line comment
                if (!isAfterNewline && last_top_ch !== '{') {
                    print.trim();
                }
                print.singleSpace();
                output.push(eatComment());
                print.newLine();
            } else if (ch === '@') {
                // pass along the space we found as a separate item
                if (isAfterSpace) {
                    print.singleSpace();
                }
                output.push(ch);

                // strip trailing space, if present, for hash property checks
                var variableOrRule = peekString(": ,;{}()[]/='\"").replace(/\s$/, '');

                // might be a nesting at-rule
                if (variableOrRule in css_beautify.NESTED_AT_RULE) {
                    nestedLevel += 1;
                    if (variableOrRule in css_beautify.CONDITIONAL_GROUP_RULE) {
                        enteringConditionalGroup = true;
                    }
                } else if (': '.indexOf(variableOrRule[variableOrRule.length -1]) >= 0) {
                    //we have a variable, add it and insert one space before continuing
                    next();
                    variableOrRule = eatString(": ").replace(/\s$/, '');
                    output.push(variableOrRule);
                    print.singleSpace();
                }
            } else if (ch === '{') {
                if (peek(true) === '}') {
                    eatWhitespace();
                    next();
                    print.singleSpace();
                    output.push("{}");
                } else {
                    indent();
                    print["{"](ch);
                    // when entering conditional groups, only rulesets are allowed
                    if (enteringConditionalGroup) {
                        enteringConditionalGroup = false;
                        insideRule = (indentLevel > nestedLevel);
                    } else {
                        // otherwise, declarations are also allowed
                        insideRule = (indentLevel >= nestedLevel);
                    }
                }
            } else if (ch === '}') {
                outdent();
                print["}"](ch);
                insideRule = false;
                if (nestedLevel) {
                    nestedLevel--;
                }
            } else if (ch === ":") {
                eatWhitespace();
                if ((insideRule || enteringConditionalGroup) && 
                        !(lookBack("&") || foundNestedPseudoClass())) {
                    // 'property: value' delimiter
                    // which could be in a conditional group query
                    output.push(':');
                    print.singleSpace();
                } else {
                    // sass/less parent reference don't use a space
                    // sass nested pseudo-class don't use a space
                    if (peek() === ":") {
                        // pseudo-element
                        next();
                        output.push("::");
                    } else {
                        // pseudo-class
                        output.push(':');
                    }
                }
            } else if (ch === '"' || ch === '\'') {
                if (isAfterSpace) {
                    print.singleSpace();
                }
                output.push(eatString(ch));
            } else if (ch === ';') {
                output.push(ch);
                print.newLine();
            } else if (ch === '(') { // may be a url
                if (lookBack("url")) {
                    output.push(ch);
                    eatWhitespace();
                    if (next()) {
                        if (ch !== ')' && ch !== '"' && ch !== '\'') {
                            output.push(eatString(')'));
                        } else {
                            pos--;
                        }
                    }
                } else {
                    if (isAfterSpace) {
                        print.singleSpace();
                    }
                    output.push(ch);
                    eatWhitespace();
                }
            } else if (ch === ')') {
                output.push(ch);
            } else if (ch === ',') {
                output.push(ch);
                eatWhitespace();
                if (!insideRule && selectorSeparatorNewline) {
                    print.newLine();
                } else {
                    print.singleSpace();
                }
            } else if (ch === ']') {
                output.push(ch);
            } else if (ch === '[') {
                if (isAfterSpace) {
                    print.singleSpace();
                }
                output.push(ch);
            } else if (ch === '=') { // no whitespace before or after
                eatWhitespace();
                output.push(ch);
            } else {
                if (isAfterSpace) {
                    print.singleSpace();
                }

                output.push(ch);
            }
        }


        var sweetCode = output.join('').replace(/[\r\n\t ]+$/, '');

        // establish end_with_newline
        if (end_with_newline) {
            sweetCode += "\n";
        }

        return sweetCode;
    }

    // https://developer.mozilla.org/en-US/docs/Web/CSS/At-rule
    css_beautify.NESTED_AT_RULE = {
        "@page": true,
        "@font-face": true,
        "@keyframes": true,
        // also in CONDITIONAL_GROUP_RULE below
        "@media": true,
        "@supports": true,
        "@document": true
    };
    css_beautify.CONDITIONAL_GROUP_RULE = {
        "@media": true,
        "@supports": true,
        "@document": true
    };

    /*global define */
    if (typeof define === "function" && define.amd) {
        // Add support for AMD ( https://github.com/amdjs/amdjs-api/wiki/AMD#defineamd-property- )
        define([], function() {
            return {
                css_beautify: css_beautify
            };
        });
    } else if (typeof exports !== "undefined") {
        // Add support for CommonJS. Just put this file somewhere on your require.paths
        // and you will be able to `var html_beautify = require("beautify").html_beautify`.
        exports.css_beautify = css_beautify;
    } else if (typeof window !== "undefined") {
        // If we're running a web page and don't have either of the above, add our one global
        window.css_beautify = css_beautify;
    } else if (typeof global !== "undefined") {
        // If we don't even have window, try global.
        global.css_beautify = css_beautify;
    }

}());
/*jshint curly:true, eqeqeq:true, laxbreak:true, noempty:false */
/*

  The MIT License (MIT)

  Copyright (c) 2007-2013 Einar Lielmanis and contributors.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.


 Style HTML
---------------

  Written by Nochum Sossonko, (nsossonko@hotmail.com)

  Based on code initially developed by: Einar Lielmanis, <einar@jsbeautifier.org>
    http://jsbeautifier.org/

  Usage:
    style_html(html_source);

    style_html(html_source, options);

  The options are:
    indent_inner_html (default false)  — indent <head> and <body> sections,
    indent_size (default 4)          — indentation size,
    indent_char (default space)      — character to indent with,
    wrap_line_length (default 250)            -  maximum amount of characters per line (0 = disable)
    brace_style (default "collapse") - "collapse" | "expand" | "end-expand" | "none"
            put braces on the same line as control statements (default), or put braces on own line (Allman / ANSI style), or just put end braces on own line, or attempt to keep them where they are.
    unformatted (defaults to inline tags) - list of tags, that shouldn't be reformatted
    indent_scripts (default normal)  - "keep"|"separate"|"normal"
    preserve_newlines (default true) - whether existing line breaks before elements should be preserved
                                        Only works before elements, not inside tags or for text.
    max_preserve_newlines (default unlimited) - maximum number of line breaks to be preserved in one chunk
    indent_handlebars (default false) - format and indent {{#foo}} and {{/foo}}
    end_with_newline (false)          - end with a newline


    e.g.

    style_html(html_source, {
      'indent_inner_html': false,
      'indent_size': 2,
      'indent_char': ' ',
      'wrap_line_length': 78,
      'brace_style': 'expand',
      'unformatted': ['a', 'sub', 'sup', 'b', 'i', 'u'],
      'preserve_newlines': true,
      'max_preserve_newlines': 5,
      'indent_handlebars': false
    });
*/

(function() {

    function trim(s) {
        return s.replace(/^\s+|\s+$/g, '');
    }

    function ltrim(s) {
        return s.replace(/^\s+/g, '');
    }

    function rtrim(s) {
        return s.replace(/\s+$/g,'');
    }

    function style_html(html_source, options, js_beautify, css_beautify) {
        //Wrapper function to invoke all the necessary constructors and deal with the output.

        var multi_parser,
            indent_inner_html,
            indent_size,
            indent_character,
            wrap_line_length,
            brace_style,
            unformatted,
            preserve_newlines,
            max_preserve_newlines,
            indent_handlebars,
            end_with_newline;

        options = options || {};

        // backwards compatibility to 1.3.4
        if ((options.wrap_line_length === undefined || parseInt(options.wrap_line_length, 10) === 0) &&
                (options.max_char !== undefined && parseInt(options.max_char, 10) !== 0)) {
            options.wrap_line_length = options.max_char;
        }

        indent_inner_html = (options.indent_inner_html === undefined) ? false : options.indent_inner_html;
        indent_size = (options.indent_size === undefined) ? 4 : parseInt(options.indent_size, 10);
        indent_character = (options.indent_char === undefined) ? ' ' : options.indent_char;
        brace_style = (options.brace_style === undefined) ? 'collapse' : options.brace_style;
        wrap_line_length =  parseInt(options.wrap_line_length, 10) === 0 ? 32786 : parseInt(options.wrap_line_length || 250, 10);
        unformatted = options.unformatted || ['a', 'span', 'img', 'bdo', 'em', 'strong', 'dfn', 'code', 'samp', 'kbd', 'var', 'cite', 'abbr', 'acronym', 'q', 'sub', 'sup', 'tt', 'i', 'b', 'big', 'small', 'u', 's', 'strike', 'font', 'ins', 'del', 'pre', 'address', 'dt', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
        preserve_newlines = (options.preserve_newlines === undefined) ? true : options.preserve_newlines;
        max_preserve_newlines = preserve_newlines ?
            (isNaN(parseInt(options.max_preserve_newlines, 10)) ? 32786 : parseInt(options.max_preserve_newlines, 10))
            : 0;
        indent_handlebars = (options.indent_handlebars === undefined) ? false : options.indent_handlebars;
        end_with_newline = (options.end_with_newline === undefined) ? false : options.end_with_newline;

        function Parser() {

            this.pos = 0; //Parser position
            this.token = '';
            this.current_mode = 'CONTENT'; //reflects the current Parser mode: TAG/CONTENT
            this.tags = { //An object to hold tags, their position, and their parent-tags, initiated with default values
                parent: 'parent1',
                parentcount: 1,
                parent1: ''
            };
            this.tag_type = '';
            this.token_text = this.last_token = this.last_text = this.token_type = '';
            this.newlines = 0;
            this.indent_content = indent_inner_html;

            this.Utils = { //Uilities made available to the various functions
                whitespace: "\n\r\t ".split(''),
                single_token: 'br,input,link,meta,!doctype,basefont,base,area,hr,wbr,param,img,isindex,?xml,embed,?php,?,?='.split(','), //all the single tags for HTML
                extra_liners: 'head,body,/html'.split(','), //for tags that need a line of whitespace before them
                in_array: function(what, arr) {
                    for (var i = 0; i < arr.length; i++) {
                        if (what === arr[i]) {
                            return true;
                        }
                    }
                    return false;
                }
            };

            // Return true iff the given text is composed entirely of
            // whitespace.
            this.is_whitespace = function(text) {
                for (var n = 0; n < text.length; text++) {
                    if (!this.Utils.in_array(text.charAt(n), this.Utils.whitespace)) {
                        return false;
                    }
                }
                return true;
            }

            this.traverse_whitespace = function() {
                var input_char = '';

                input_char = this.input.charAt(this.pos);
                if (this.Utils.in_array(input_char, this.Utils.whitespace)) {
                    this.newlines = 0;
                    while (this.Utils.in_array(input_char, this.Utils.whitespace)) {
                        if (preserve_newlines && input_char === '\n' && this.newlines <= max_preserve_newlines) {
                            this.newlines += 1;
                        }

                        this.pos++;
                        input_char = this.input.charAt(this.pos);
                    }
                    return true;
                }
                return false;
            };

            // Append a space to the given content (string array) or, if we are
            // at the wrap_line_length, append a newline/indentation.
            this.space_or_wrap = function(content) {
                if (this.line_char_count >= this.wrap_line_length) { //insert a line when the wrap_line_length is reached
                    this.print_newline(false, content);
                    this.print_indentation(content);
                } else {
                    this.line_char_count++;
                    content.push(' ');
                }
            };

            this.get_content = function() { //function to capture regular content between tags
                var input_char = '',
                    content = [],
                    space = false; //if a space is needed

                while (this.input.charAt(this.pos) !== '<') {
                    if (this.pos >= this.input.length) {
                        return content.length ? content.join('') : ['', 'TK_EOF'];
                    }

                    if (this.traverse_whitespace()) {
                        this.space_or_wrap(content);
                        continue;
                    }

                    if (indent_handlebars) {
                        // Handlebars parsing is complicated.
                        // {{#foo}} and {{/foo}} are formatted tags.
                        // {{something}} should get treated as content, except:
                        // {{else}} specifically behaves like {{#if}} and {{/if}}
                        var peek3 = this.input.substr(this.pos, 3);
                        if (peek3 === '{{#' || peek3 === '{{/') {
                            // These are tags and not content.
                            break;
                        } else if (this.input.substr(this.pos, 2) === '{{') {
                            if (this.get_tag(true) === '{{else}}') {
                                break;
                            }
                        }
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;
                    this.line_char_count++;
                    content.push(input_char); //letter at-a-time (or string) inserted to an array
                }
                return content.length ? content.join('') : '';
            };

            this.get_contents_to = function(name) { //get the full content of a script or style to pass to js_beautify
                if (this.pos === this.input.length) {
                    return ['', 'TK_EOF'];
                }
                var input_char = '';
                var content = '';
                var reg_match = new RegExp('</' + name + '\\s*>', 'igm');
                reg_match.lastIndex = this.pos;
                var reg_array = reg_match.exec(this.input);
                var end_script = reg_array ? reg_array.index : this.input.length; //absolute end of script
                if (this.pos < end_script) { //get everything in between the script tags
                    content = this.input.substring(this.pos, end_script);
                    this.pos = end_script;
                }
                return content;
            };

            this.record_tag = function(tag) { //function to record a tag and its parent in this.tags Object
                if (this.tags[tag + 'count']) { //check for the existence of this tag type
                    this.tags[tag + 'count']++;
                    this.tags[tag + this.tags[tag + 'count']] = this.indent_level; //and record the present indent level
                } else { //otherwise initialize this tag type
                    this.tags[tag + 'count'] = 1;
                    this.tags[tag + this.tags[tag + 'count']] = this.indent_level; //and record the present indent level
                }
                this.tags[tag + this.tags[tag + 'count'] + 'parent'] = this.tags.parent; //set the parent (i.e. in the case of a div this.tags.div1parent)
                this.tags.parent = tag + this.tags[tag + 'count']; //and make this the current parent (i.e. in the case of a div 'div1')
            };

            this.retrieve_tag = function(tag) { //function to retrieve the opening tag to the corresponding closer
                if (this.tags[tag + 'count']) { //if the openener is not in the Object we ignore it
                    var temp_parent = this.tags.parent; //check to see if it's a closable tag.
                    while (temp_parent) { //till we reach '' (the initial value);
                        if (tag + this.tags[tag + 'count'] === temp_parent) { //if this is it use it
                            break;
                        }
                        temp_parent = this.tags[temp_parent + 'parent']; //otherwise keep on climbing up the DOM Tree
                    }
                    if (temp_parent) { //if we caught something
                        this.indent_level = this.tags[tag + this.tags[tag + 'count']]; //set the indent_level accordingly
                        this.tags.parent = this.tags[temp_parent + 'parent']; //and set the current parent
                    }
                    delete this.tags[tag + this.tags[tag + 'count'] + 'parent']; //delete the closed tags parent reference...
                    delete this.tags[tag + this.tags[tag + 'count']]; //...and the tag itself
                    if (this.tags[tag + 'count'] === 1) {
                        delete this.tags[tag + 'count'];
                    } else {
                        this.tags[tag + 'count']--;
                    }
                }
            };

            this.indent_to_tag = function(tag) {
                // Match the indentation level to the last use of this tag, but don't remove it.
                if (!this.tags[tag + 'count']) {
                    return;
                }
                var temp_parent = this.tags.parent;
                while (temp_parent) {
                    if (tag + this.tags[tag + 'count'] === temp_parent) {
                        break;
                    }
                    temp_parent = this.tags[temp_parent + 'parent'];
                }
                if (temp_parent) {
                    this.indent_level = this.tags[tag + this.tags[tag + 'count']];
                }
            };

            this.get_tag = function(peek) { //function to get a full tag and parse its type
                var input_char = '',
                    content = [],
                    comment = '',
                    space = false,
                    tag_start, tag_end,
                    tag_start_char,
                    orig_pos = this.pos,
                    orig_line_char_count = this.line_char_count;

                peek = peek !== undefined ? peek : false;

                do {
                    if (this.pos >= this.input.length) {
                        if (peek) {
                            this.pos = orig_pos;
                            this.line_char_count = orig_line_char_count;
                        }
                        return content.length ? content.join('') : ['', 'TK_EOF'];
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;

                    if (this.Utils.in_array(input_char, this.Utils.whitespace)) { //don't want to insert unnecessary space
                        space = true;
                        continue;
                    }

                    if (input_char === "'" || input_char === '"') {
                        input_char += this.get_unformatted(input_char);
                        space = true;

                    }

                    if (input_char === '=') { //no space before =
                        space = false;
                    }

                    if (content.length && content[content.length - 1] !== '=' && input_char !== '>' && space) {
                        //no space after = or before >
                        this.space_or_wrap(content);
                        space = false;
                    }

                    if (indent_handlebars && tag_start_char === '<') {
                        // When inside an angle-bracket tag, put spaces around
                        // handlebars not inside of strings.
                        if ((input_char + this.input.charAt(this.pos)) === '{{') {
                            input_char += this.get_unformatted('}}');
                            if (content.length && content[content.length - 1] !== ' ' && content[content.length - 1] !== '<') {
                                input_char = ' ' + input_char;
                            }
                            space = true;
                        }
                    }

                    if (input_char === '<' && !tag_start_char) {
                        tag_start = this.pos - 1;
                        tag_start_char = '<';
                    }

                    if (indent_handlebars && !tag_start_char) {
                        if (content.length >= 2 && content[content.length - 1] === '{' && content[content.length - 2] == '{') {
                            if (input_char === '#' || input_char === '/') {
                                tag_start = this.pos - 3;
                            } else {
                                tag_start = this.pos - 2;
                            }
                            tag_start_char = '{';
                        }
                    }

                    this.line_char_count++;
                    content.push(input_char); //inserts character at-a-time (or string)

                    if (content[1] && content[1] === '!') { //if we're in a comment, do something special
                        // We treat all comments as literals, even more than preformatted tags
                        // we just look for the appropriate close tag
                        content = [this.get_comment(tag_start)];
                        break;
                    }

                    if (indent_handlebars && tag_start_char === '{' && content.length > 2 && content[content.length - 2] === '}' && content[content.length - 1] === '}') {
                        break;
                    }
                } while (input_char !== '>');

                var tag_complete = content.join('');
                var tag_index;
                var tag_offset;

                if (tag_complete.indexOf(' ') !== -1) { //if there's whitespace, thats where the tag name ends
                    tag_index = tag_complete.indexOf(' ');
                } else if (tag_complete[0] === '{') {
                    tag_index = tag_complete.indexOf('}');
                } else { //otherwise go with the tag ending
                    tag_index = tag_complete.indexOf('>');
                }
                if (tag_complete[0] === '<' || !indent_handlebars) {
                    tag_offset = 1;
                } else {
                    tag_offset = tag_complete[2] === '#' ? 3 : 2;
                }
                var tag_check = tag_complete.substring(tag_offset, tag_index).toLowerCase();
                if (tag_complete.charAt(tag_complete.length - 2) === '/' ||
                    this.Utils.in_array(tag_check, this.Utils.single_token)) { //if this tag name is a single tag type (either in the list or has a closing /)
                    if (!peek) {
                        this.tag_type = 'SINGLE';
                    }
                } else if (indent_handlebars && tag_complete[0] === '{' && tag_check === 'else') {
                    if (!peek) {
                        this.indent_to_tag('if');
                        this.tag_type = 'HANDLEBARS_ELSE';
                        this.indent_content = true;
                        this.traverse_whitespace();
                    }
                } else if (this.is_unformatted(tag_check, unformatted)) { // do not reformat the "unformatted" tags
                    comment = this.get_unformatted('</' + tag_check + '>', tag_complete); //...delegate to get_unformatted function
                    content.push(comment);
                    tag_end = this.pos - 1;
                    this.tag_type = 'SINGLE';
                } else if (tag_check === 'script' &&
                    (tag_complete.search('type') === -1 ||
                    (tag_complete.search('type') > -1 &&
                    tag_complete.search(/\b(text|application)\/(x-)?(javascript|ecmascript|jscript|livescript)/) > -1))) {
                    if (!peek) {
                        this.record_tag(tag_check);
                        this.tag_type = 'SCRIPT';
                    }
                } else if (tag_check === 'style' &&
                    (tag_complete.search('type') === -1 ||
                    (tag_complete.search('type') > -1 && tag_complete.search('text/css') > -1))) {
                    if (!peek) {
                        this.record_tag(tag_check);
                        this.tag_type = 'STYLE';
                    }
                } else if (tag_check.charAt(0) === '!') { //peek for <! comment
                    // for comments content is already correct.
                    if (!peek) {
                        this.tag_type = 'SINGLE';
                        this.traverse_whitespace();
                    }
                } else if (!peek) {
                    if (tag_check.charAt(0) === '/') { //this tag is a double tag so check for tag-ending
                        this.retrieve_tag(tag_check.substring(1)); //remove it and all ancestors
                        this.tag_type = 'END';
                    } else { //otherwise it's a start-tag
                        this.record_tag(tag_check); //push it on the tag stack
                        if (tag_check.toLowerCase() !== 'html') {
                            this.indent_content = true;
                        }
                        this.tag_type = 'START';
                    }

                    // Allow preserving of newlines after a start or end tag
                    if (this.traverse_whitespace()) {
                        this.space_or_wrap(content);
                    }

                    if (this.Utils.in_array(tag_check, this.Utils.extra_liners)) { //check if this double needs an extra line
                        this.print_newline(false, this.output);
                        if (this.output.length && this.output[this.output.length - 2] !== '\n') {
                            this.print_newline(true, this.output);
                        }
                    }
                }

                if (peek) {
                    this.pos = orig_pos;
                    this.line_char_count = orig_line_char_count;
                }

                return content.join(''); //returns fully formatted tag
            };

            this.get_comment = function(start_pos) { //function to return comment content in its entirety
                // this is will have very poor perf, but will work for now.
                var comment = '',
                    delimiter = '>',
                    matched = false;

                this.pos = start_pos;
                input_char = this.input.charAt(this.pos);
                this.pos++;

                while (this.pos <= this.input.length) {
                    comment += input_char;

                    // only need to check for the delimiter if the last chars match
                    if (comment[comment.length - 1] === delimiter[delimiter.length - 1] &&
                        comment.indexOf(delimiter) !== -1) {
                        break;
                    }

                    // only need to search for custom delimiter for the first few characters
                    if (!matched && comment.length < 10) {
                        if (comment.indexOf('<![if') === 0) { //peek for <![if conditional comment
                            delimiter = '<![endif]>';
                            matched = true;
                        } else if (comment.indexOf('<![cdata[') === 0) { //if it's a <[cdata[ comment...
                            delimiter = ']]>';
                            matched = true;
                        } else if (comment.indexOf('<![') === 0) { // some other ![ comment? ...
                            delimiter = ']>';
                            matched = true;
                        } else if (comment.indexOf('<!--') === 0) { // <!-- comment ...
                            delimiter = '-->';
                            matched = true;
                        }
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;
                }

                return comment;
            };

            this.get_unformatted = function(delimiter, orig_tag) { //function to return unformatted content in its entirety

                if (orig_tag && orig_tag.toLowerCase().indexOf(delimiter) !== -1) {
                    return '';
                }
                var input_char = '';
                var content = '';
                var min_index = 0;
                var space = true;
                do {

                    if (this.pos >= this.input.length) {
                        return content;
                    }

                    input_char = this.input.charAt(this.pos);
                    this.pos++;

                    if (this.Utils.in_array(input_char, this.Utils.whitespace)) {
                        if (!space) {
                            this.line_char_count--;
                            continue;
                        }
                        if (input_char === '\n' || input_char === '\r') {
                            content += '\n';
                            /*  Don't change tab indention for unformatted blocks.  If using code for html editing, this will greatly affect <pre> tags if they are specified in the 'unformatted array'
                for (var i=0; i<this.indent_level; i++) {
                  content += this.indent_string;
                }
                space = false; //...and make sure other indentation is erased
                */
                            this.line_char_count = 0;
                            continue;
                        }
                    }
                    content += input_char;
                    this.line_char_count++;
                    space = true;

                    if (indent_handlebars && input_char === '{' && content.length && content[content.length - 2] === '{') {
                        // Handlebars expressions in strings should also be unformatted.
                        content += this.get_unformatted('}}');
                        // These expressions are opaque.  Ignore delimiters found in them.
                        min_index = content.length;
                    }
                } while (content.toLowerCase().indexOf(delimiter, min_index) === -1);
                return content;
            };

            this.get_token = function() { //initial handler for token-retrieval
                var token;

                if (this.last_token === 'TK_TAG_SCRIPT' || this.last_token === 'TK_TAG_STYLE') { //check if we need to format javascript
                    var type = this.last_token.substr(7);
                    token = this.get_contents_to(type);
                    if (typeof token !== 'string') {
                        return token;
                    }
                    return [token, 'TK_' + type];
                }
                if (this.current_mode === 'CONTENT') {
                    token = this.get_content();
                    if (typeof token !== 'string') {
                        return token;
                    } else {
                        return [token, 'TK_CONTENT'];
                    }
                }

                if (this.current_mode === 'TAG') {
                    token = this.get_tag();
                    if (typeof token !== 'string') {
                        return token;
                    } else {
                        var tag_name_type = 'TK_TAG_' + this.tag_type;
                        return [token, tag_name_type];
                    }
                }
            };

            this.get_full_indent = function(level) {
                level = this.indent_level + level || 0;
                if (level < 1) {
                    return '';
                }

                return Array(level + 1).join(this.indent_string);
            };

            this.is_unformatted = function(tag_check, unformatted) {
                //is this an HTML5 block-level link?
                if (!this.Utils.in_array(tag_check, unformatted)) {
                    return false;
                }

                if (tag_check.toLowerCase() !== 'a' || !this.Utils.in_array('a', unformatted)) {
                    return true;
                }

                //at this point we have an  tag; is its first child something we want to remain
                //unformatted?
                var next_tag = this.get_tag(true /* peek. */ );

                // test next_tag to see if it is just html tag (no external content)
                var tag = (next_tag || "").match(/^\s*<\s*\/?([a-z]*)\s*[^>]*>\s*$/);

                // if next_tag comes back but is not an isolated tag, then
                // let's treat the 'a' tag as having content
                // and respect the unformatted option
                if (!tag || this.Utils.in_array(tag, unformatted)) {
                    return true;
                } else {
                    return false;
                }
            };

            this.printer = function(js_source, indent_character, indent_size, wrap_line_length, brace_style) { //handles input/output and some other printing functions

                this.input = js_source || ''; //gets the input for the Parser
                this.output = [];
                this.indent_character = indent_character;
                this.indent_string = '';
                this.indent_size = indent_size;
                this.brace_style = brace_style;
                this.indent_level = 0;
                this.wrap_line_length = wrap_line_length;
                this.line_char_count = 0; //count to see if wrap_line_length was exceeded

                for (var i = 0; i < this.indent_size; i++) {
                    this.indent_string += this.indent_character;
                }

                this.print_newline = function(force, arr) {
                    this.line_char_count = 0;
                    if (!arr || !arr.length) {
                        return;
                    }
                    if (force || (arr[arr.length - 1] !== '\n')) { //we might want the extra line
                        if ((arr[arr.length - 1] !== '\n')) {
                            arr[arr.length - 1] = rtrim(arr[arr.length - 1]);
                        }
                        arr.push('\n');
                    }
                };

                this.print_indentation = function(arr) {
                    for (var i = 0; i < this.indent_level; i++) {
                        arr.push(this.indent_string);
                        this.line_char_count += this.indent_string.length;
                    }
                };

                this.print_token = function(text) {
                    // Avoid printing initial whitespace.
                    if (this.is_whitespace(text) && !this.output.length) {
                        return;
                    }
                    if (text || text !== '') {
                        if (this.output.length && this.output[this.output.length - 1] === '\n') {
                            this.print_indentation(this.output);
                            text = ltrim(text);
                        }
                    }
                    this.print_token_raw(text);
                };

                this.print_token_raw = function(text) {
                    // If we are going to print newlines, truncate trailing
                    // whitespace, as the newlines will represent the space.
                    if (this.newlines > 0) {
                        text = rtrim(text);
                    }

                    if (text && text !== '') {
                        if (text.length > 1 && text[text.length - 1] === '\n') {
                            // unformatted tags can grab newlines as their last character
                            this.output.push(text.slice(0, -1));
                            this.print_newline(false, this.output);
                        } else {
                            this.output.push(text);
                        }
                    }

                    for (var n = 0; n < this.newlines; n++) {
                        this.print_newline(n > 0, this.output);
                    }
                    this.newlines = 0;
                };

                this.indent = function() {
                    this.indent_level++;
                };

                this.unindent = function() {
                    if (this.indent_level > 0) {
                        this.indent_level--;
                    }
                };
            };
            return this;
        }

        /*_____________________--------------------_____________________*/

        multi_parser = new Parser(); //wrapping functions Parser
        multi_parser.printer(html_source, indent_character, indent_size, wrap_line_length, brace_style); //initialize starting values

        while (true) {
            var t = multi_parser.get_token();
            multi_parser.token_text = t[0];
            multi_parser.token_type = t[1];

            if (multi_parser.token_type === 'TK_EOF') {
                break;
            }

            switch (multi_parser.token_type) {
                case 'TK_TAG_START':
                    multi_parser.print_newline(false, multi_parser.output);
                    multi_parser.print_token(multi_parser.token_text);
                    if (multi_parser.indent_content) {
                        multi_parser.indent();
                        multi_parser.indent_content = false;
                    }
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_STYLE':
                case 'TK_TAG_SCRIPT':
                    multi_parser.print_newline(false, multi_parser.output);
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_END':
                    //Print new line only if the tag has no content and has child
                    if (multi_parser.last_token === 'TK_CONTENT' && multi_parser.last_text === '') {
                        var tag_name = multi_parser.token_text.match(/\w+/)[0];
                        var tag_extracted_from_last_output = null;
                        if (multi_parser.output.length) {
                            tag_extracted_from_last_output = multi_parser.output[multi_parser.output.length - 1].match(/(?:<|{{#)\s*(\w+)/);
                        }
                        if (tag_extracted_from_last_output === null ||
                            tag_extracted_from_last_output[1] !== tag_name) {
                            multi_parser.print_newline(false, multi_parser.output);
                        }
                    }
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_SINGLE':
                    // Don't add a newline before elements that should remain unformatted.
                    var tag_check = multi_parser.token_text.match(/^\s*<([a-z-]+)/i);
                    if (!tag_check || !multi_parser.Utils.in_array(tag_check[1], unformatted)) {
                        multi_parser.print_newline(false, multi_parser.output);
                    }
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_TAG_HANDLEBARS_ELSE':
                    multi_parser.print_token(multi_parser.token_text);
                    if (multi_parser.indent_content) {
                        multi_parser.indent();
                        multi_parser.indent_content = false;
                    }
                    multi_parser.current_mode = 'CONTENT';
                    break;
                case 'TK_CONTENT':
                    multi_parser.print_token(multi_parser.token_text);
                    multi_parser.current_mode = 'TAG';
                    break;
                case 'TK_STYLE':
                case 'TK_SCRIPT':
                    if (multi_parser.token_text !== '') {
                        multi_parser.print_newline(false, multi_parser.output);
                        var text = multi_parser.token_text,
                            _beautifier,
                            script_indent_level = 1;
                        if (multi_parser.token_type === 'TK_SCRIPT') {
                            _beautifier = typeof js_beautify === 'function' && js_beautify;
                        } else if (multi_parser.token_type === 'TK_STYLE') {
                            _beautifier = typeof css_beautify === 'function' && css_beautify;
                        }

                        if (options.indent_scripts === "keep") {
                            script_indent_level = 0;
                        } else if (options.indent_scripts === "separate") {
                            script_indent_level = -multi_parser.indent_level;
                        }

                        var indentation = multi_parser.get_full_indent(script_indent_level);
                        if (_beautifier) {
                            // call the Beautifier if avaliable
                            text = _beautifier(text.replace(/^\s*/, indentation), options);
                        } else {
                            // simply indent the string otherwise
                            var white = text.match(/^\s*/)[0];
                            var _level = white.match(/[^\n\r]*$/)[0].split(multi_parser.indent_string).length - 1;
                            var reindent = multi_parser.get_full_indent(script_indent_level - _level);
                            text = text.replace(/^\s*/, indentation)
                                .replace(/\r\n|\r|\n/g, '\n' + reindent)
                                .replace(/\s+$/, '');
                        }
                        if (text) {
                            multi_parser.print_token_raw(text);
                            multi_parser.print_newline(true, multi_parser.output);
                        }
                    }
                    multi_parser.current_mode = 'TAG';
                    break;
                default:
                    // We should not be getting here but we don't want to drop input on the floor
                    // Just output the text and move on
                    if (multi_parser.token_text !== '') {
                        multi_parser.print_token(multi_parser.token_text);
                    }
                    break;
            }
            multi_parser.last_token = multi_parser.token_type;
            multi_parser.last_text = multi_parser.token_text;
        }
        var sweet_code = multi_parser.output.join('').replace(/[\r\n\t ]+$/, '');
        if (end_with_newline) {
            sweet_code += '\n';
        }
        return sweet_code;
    }

    if (typeof define === "function" && define.amd) {
        // Add support for AMD ( https://github.com/amdjs/amdjs-api/wiki/AMD#defineamd-property- )
        define(["require", "./beautify", "./beautify-css"], function(requireamd) {
            var js_beautify =  requireamd("./beautify");
            var css_beautify =  requireamd("./beautify-css");

            return {
              html_beautify: function(html_source, options) {
                return style_html(html_source, options, js_beautify.js_beautify, css_beautify.css_beautify);
              }
            };
        });
    } else if (typeof exports !== "undefined") {
        // Add support for CommonJS. Just put this file somewhere on your require.paths
        // and you will be able to `var html_beautify = require("beautify").html_beautify`.
        var js_beautify = require('./beautify.js');
        var css_beautify = require('./beautify-css.js');

        exports.html_beautify = function(html_source, options) {
            return style_html(html_source, options, js_beautify.js_beautify, css_beautify.css_beautify);
        };
    } else if (typeof window !== "undefined") {
        // If we're running a web page and don't have either of the above, add our one global
        window.html_beautify = function(html_source, options) {
            return style_html(html_source, options, window.js_beautify, window.css_beautify);
        };
    } else if (typeof global !== "undefined") {
        // If we don't even have window, try global.
        global.html_beautify = function(html_source, options) {
            return style_html(html_source, options, global.js_beautify, global.css_beautify);
        };
    }

}());
});

},
'xideve/views/VisualEditorLayout':function(){
/** @module xideve/views/VisualEditorLayout **/
define([
    "dcl/dcl",
    'xaction/Action',
    'xide/types',
    "davinci/Workbench",
    "dojo/dom-style"
], function (dcl,Action,types, Workbench,domStyle) {
    /**
     *
     * @mixin module:xideve/views/VisualEditorLayout
     * @lends module:xideve/views/VisualEditor
     */
    return dcl(null, {
        declaredClass:"xideve/views/VisualEditorLayout",
        changeGlobalLayout:function(type){
            var pageEditor = this.getPageEditor();
            var mainView = this.getMainView();
            var left = mainView.layoutLeft;
            var right = mainView.layoutRight;
            var bottom = mainView.layoutBottom;

            var thiz = this;

            /**
             *      case types.VIEW_SPLIT_MODE.DESIGN:
             {
                 newMode = 'design';
                 break;
             }
             case types.VIEW_SPLIT_MODE.SOURCE:
             {
                 newMode = 'source';
                 break;
             }
             case types.VIEW_SPLIT_MODE.SPLIT_VERTICAL:
             {
                 newMode = 'splitVertical';
                 break;
             }
             case types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL:
             {
                 newMode = 'splitHorizontal';
                 break;
             }

             */

            switch(type){

                case types.VE_LAYOUT_RIGHT_CENTER_BOTTOM:{

                    //close left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'closed');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);


                    break;
                }
                case types.VE_LAYOUT_CENTER_BOTTOM:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);

                    //close left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'closed');
                    }

                    //close right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'closed');
                    }
                    break;
                }
                case types.VE_LAYOUT_CENTER_RIGHT:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.DESIGN);

                    //close left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'closed');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }

                    break;
                }
                case types.VE_LAYOUT_LEFT_CENTER_RIGHT:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.DESIGN);

                    //open left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'full');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }

                    break;
                }
                case types.VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);
                    domStyle.set(left.domNode,{
                        width:'200px'
                    });
                    //open left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'full');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }


                    break;
                }
            }


            this.publish(types.EVENTS.RESIZE);

            /*
             pageEditor._bc.resize();
             var tab = pageEditor.bottomTabContainer;
             tab.selectChild(tab.getChildren()[tab.getChildren().length-1]);
             tab.selectChild(tab.getChildren()[0]);
             */

            setTimeout(function(){
                thiz.publish(types.EVENTS.RESIZE);
            },1000)


        },
        /**
         *
         */
        getLayoutActions:function(){

            if(this._layoutActions){
                return this._layoutActions;
            }
            var thiz = this;
            var idx = 0;
            function _createLayoutAction(label,command,icon,handler) {
                var _default = Action.create(label, icon, 'View/' +command + '_' + idx, false, null, types.ITEM_TYPE.WIDGET, 'viewActions', null, false,function(){
                    thiz.changeGlobalLayout(command);
                });

                _default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                    label: '',
                    widgetArgs:{
                        style:'text-align:right;float:right;font-size:120%;'
                    },
                    permanent:function(){
                        return thiz.destroyed==false;
                    }
                });
                idx++;
                return _default;
            }


            var VE_LAYOUT_RIGHT_CENTER_BOTTOM = _createLayoutAction('Center Bottom Right',
                types.VE_LAYOUT_RIGHT_CENTER_BOTTOM,'layoutIcon-rightCenterBottom');
            var VE_LAYOUT_CENTER_BOTTOM = _createLayoutAction('Center Bottom',
                types.VE_LAYOUT_CENTER_BOTTOM,'layoutIcon-centerBottom');
            var VE_LAYOUT_CENTER_RIGHT = _createLayoutAction('Center Right',
                types.VE_LAYOUT_CENTER_RIGHT,'layoutIcon-centerRight');

            var VE_LAYOUT_LEFT_CENTER_RIGHT = _createLayoutAction('Left Center Right',
                types.VE_LAYOUT_LEFT_CENTER_RIGHT,'layoutIcon-leftCenterRight');

            var VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM = _createLayoutAction('Left Center Right Bottom',
                types.VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM,'layoutIcon-leftCenterRightBottom');

            var out = [
                VE_LAYOUT_RIGHT_CENTER_BOTTOM,// trouble!
                VE_LAYOUT_CENTER_BOTTOM,//ok
                VE_LAYOUT_CENTER_RIGHT,//ok

                VE_LAYOUT_LEFT_CENTER_RIGHT,
                VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM];


            this._layoutActions = out;

            return out;
        },
        doSplit: function (mode) {

             0 && console.log('-do split ' + mode);

            return;

            if (this.editor) {

                var newMode = 'design';
                switch (mode) {

                    case types.VIEW_SPLIT_MODE.DESIGN:
                    {
                        newMode = 'design';
                        break;
                    }
                    case types.VIEW_SPLIT_MODE.SOURCE:
                    {
                        newMode = 'source';
                        break;
                    }
                    case types.VIEW_SPLIT_MODE.SPLIT_VERTICAL:
                    {
                        newMode = 'splitVertical';
                        break;
                    }
                    case types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL:
                    {
                        newMode = 'splitHorizontal';
                        break;
                    }
                }


                //this.editor.switchDisplayMode(newMode);
                var thiz = this;
                var pe = thiz.getPageEditor();
                pe.switchDisplayMode(newMode);

                var tab = pe.bottomTabContainer;
                /*if(!tab._didSelectFirstPane){
                 pe._bc.resize();
                 return;
                 }else {*/
                tab._didSelectFirstPane=true;
                setTimeout(function () {

                    if (tab) {
                        tab.resize();
                        var fCP = tab.getChildren()[0];
                        if(fCP) {
                            tab.selectChild(fCP);
                            fCP.resize();
                        }

                        thiz.publish(types.EVENTS.RESIZE);
                        tab.resize();
                        pe._bc.resize();
                    }
                    thiz.publish(types.EVENTS.RESIZE);
                    pe._bc.resize();
                }, 1500);
                pe._bc.resize();
            }
        },
        onSplit: function () {

            if (this.editor) {
                Workbench._switchEditor(null);
                this.publish("/davinci/ui/editorSelected", [{
                    editor: null,
                    oldEditor: this.editor
                }]);
            }
        },
        getSplitTarget: function () {
            return this.containerNode;
        },
        getSplitContent: function () {
            return this.editor.domNode;
        }
    });
});
},
'xideve/views/VisualEditorTools':function(){
/** @module xideve/views/VisualEditorTools **/
define([
    "dcl/dcl",
    "davinci/ve/widget"
], function (dcl,widgetUtils) {
    $.fn.inlineStyle = function (prop) {
        var styles = this.attr("style"),
            value;
        styles && styles.split(";").forEach(function (e) {
            var style = e.split(":");
            if ($.trim(style[0]) === prop) {
                value = style[1];
            }
        });
        return value;
    };
    /**
     *
     * @mixin module:xideve/views/VisualEditorTools
     * @lends module:xideve/views/VisualEditor
     */
    return dcl(null,{
        declaredClass:"xideve/views/VisualEditorTools",
        /**
         * Returns all instances withing our parent tab container
         * @returns {Array}
         */
        getEditors: function () {
            var result = [];
            if (this.parentContainer) {

                var children = this.parentContainer.getChildren();
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    if (child.declaredClass === "davinci.ve.PageEditor") {
                        result.push(child);
                    }
                }
            }

            return result;
        },
        getEditorContext: function () {
            var context = null;
            if (this.editor && this.editor.currentEditor && this.editor.currentEditor.context) {
                context = this.editor.currentEditor.context;
            }
            return context;
        },

        getPageEditor:function(){
            return this.editor;
        },
        onWidgetAction:function(selection){

            /*
            var context = this.getEditorContext();
            var command = this.getContext().getCommandForStyleChange(value); //#23
            if (command) {
                this.getContext().getCommandStack().execute(command);
                if (command._newId) {
                    var widget = widgetUtils.byId(command._newId, context.getDocument());
                    this.context.select(widget);
                }
                this._srcChanged();
                dojo.publish("/davinci/ui/widgetValuesChanged", [value]);
            }
            */

        },
        _sameSize:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = $(selection[0].domNode);

            var cWidth = firstItem.css('width'),
                cHeight = firstItem.css('height');

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {


                var target = selection[i];
                var _target = $(target.domNode),
                    values = {
                        width:cWidth,
                        height:cHeight

                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23

                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _sameWidth:function(){

            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = $(selection[0].domNode);

            var cWidth = firstItem.css('width'),
                cHeight = firstItem.css('height');

            //pop first
            selection.shift();
            for (var i = 0; i < selection.length; i++) {


                var target = selection[i];
                var _target = $(target.domNode),
                    values = {
                        width:cWidth
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };

                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }

        },
        _sameHeight:function(){


            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = $(selection[0].domNode);

            var cWidth = firstItem.css('width'),
                cHeight = firstItem.css('height');

            //pop first
            selection.shift();


            var _n2 = this.getSelection();

            for (var i = 0; i < selection.length; i++) {


                var target = selection[i];
                var _target = $(target.domNode),
                    values = {
                        height:cHeight
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command,true);
                    var _n = this.getSelection();
                    if (command._newId) {
                        var widget = widgetUtils.byId(command._newId, context.getDocument());
                    }
                }
            }
        },
        _alignBottomHorizontal:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "bottom right",
                    "targetAt": "bottom left",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        top: cTop + (offset.moveBy.y * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }

        },
        _alignTopHorizontal:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "top right",
                    "targetAt": "top left",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        top: cTop + (offset.moveBy.y * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _alignRightVertical:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();
            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "bottom right",
                    "targetAt": "top right",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        left:cLeft +(offset.moveBy.x * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _alignCenterVertical:function(){
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();
            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    "itemAt": "bottom center",
                    "targetAt": "top center",
                    boundary:$(document.body)
                });

                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        left:cLeft +(offset.moveBy.x * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command);
                }
            }
        },
        _alignLeftVertical:function(){

            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];

            //pop first
            selection.shift();

            for (var i = 0; i < selection.length; i++) {
                var target = selection[i];
                var calculator = new jQuery.PositionCalculator({
                    item: $(firstItem.domNode),
                    target:$(target.domNode),
                    itemAt: "bottom left",
                    targetAt: "top left",
                    boundary:$(document.body)
                });
                var offset = calculator.calculate(),
                    _target = $(target.domNode),
                    cTop = _target.position().top,
                    cLeft = _target.position().left,
                    values = {
                        left:cLeft +(offset.moveBy.x * -1) + 'px'
                    },
                    value = {
                        appliesTo:'inline',
                        appliesToWhichState:null,
                        cascade:null,
                        values:[
                            values
                        ]
                    };
                var command = ctx.getCommandForStyleChange(value,[target]); //#23
                if (command) {
                    ctx.getCommandStack().execute(command,true);
                }
            }
        }
    });
});

},
'xideve/palette/Palette':function(){
define([
    'xdojo/declare',
    'xide/utils',
    'davinci/Workbench',
    'davinci/library',
    'davinci/Runtime',
    'xide/lodash',
    'xide/$',
    'dstore/Trackable',
    'xide/data/TreeMemory',
    'davinci/ve/metadata',
    'dojo/promise/all',
    'dojo/Deferred',
    './Grid',
    './Model'
], function (declare, utils, Workbench, Library, Runtime, _,
    $, Trackable, TreeMemory,
    Metadata, all, Deferred, GridCSourcelass, Model
) {

    console.clear();
    const test = false;

    const _context = () => {
        try {
            return Runtime.currentEditor.getContext();
        } catch (e) {

        }
    }

    var ctx = window.sctx,
        root;

    function createContainer(ctx, title) {
        var leftContainer = ctx.mainView.leftLayoutContainer;
        return leftContainer.createTab(title, 'fa-cube', true, null, {
            parentContainer: leftContainer,
            open: true,
            icon: 'fa-folder'
        });
    }

    ///////////////////////////////////////////////////////////////
    //
    //
    const Module = declare('m', null, []);
    class Palette {
        // Whether the given preset has created its PaletteFolder and PaletteItems
        constructor(context) {
            this.ctx = context;
            this._presetCreated = {};
            this._presetSections = {}; // Assoc array of all paletteItem objects, indexed by [preset][section]
            this._userWidgetSection = {
                "id": "$$UserWidgets$$",
                "name": "User Widgets",
                "includes": []
            }
        }
        _prepareSectionItem(item, section, paletteItemGroup) {
            var $wm = Metadata.getLibraryMetadataForType(item.type);
            item.$library = $wm;
            item.section = section;
            item._paletteItemGroup = paletteItemGroup;
        }
        _createSectionItems(section, preset, widgetList) {
            section.items = [];
            var collections = preset.collections;
            var includes = section.includes || [];
            for (var inc = 0; inc < includes.length; inc++) {
                var includeValue = includes[inc];
                // Each item in "includes" property can be an array of strings or string
                var includeArray = _.isArray(includeValue) ? includeValue : [includeValue];
                var sectionItems = [];
                for (var ii = 0; ii < includeArray.length; ii++) {
                    var includeItem = includeArray[ii];
                    var items = [];
                    if (includeItem.substr(0, 5) === 'type:') {
                        // explicit widget type
                        var item = Metadata.getWidgetDescriptorForType(includeItem.substr(5));
                        if (item) {
                            items.push(item);
                        }
                    } else {
                        items = Metadata.getWidgetsWithTag(includeItem);
                    }
                    for (var itemindex = 0; itemindex < items.length; itemindex++) {
                        var item = items[itemindex];
                        var newItem = dojo.clone(item);
                        this._prepareSectionItem(newItem, section, this._paletteItemGroupCount);
                        sectionItems.push(newItem);
                    }
                }
                // Sort sectionItems based on order in "collections" property
                var sortedItems = [];
                // Created a sorted list of items, using preset.collections to define the order
                // of widgets within this group.
                if (collections && collections.length) {
                    for (var co = 0; co < collections.length; co++) {
                        var collection = collections[co];
                        var si = 0;
                        while (si < sectionItems.length) {
                            var sectionItem = sectionItems[si];
                            if (sectionItem.collection == collection.id) {
                                sortedItems.push(sectionItem);
                                sectionItems.splice(si, 1);
                            } else {
                                si++;
                            }
                        }
                    }
                    // Add any remaining section items to end of sortedItems
                    for (var si = 0; si < sectionItems.length; si++) {
                        sortedItems.push(sectionItems[si]);
                    }
                } else {
                    sortedItems = sectionItems;
                }
                for (var si = 0; si < sortedItems.length; si++) {
                    var sortedItem = sortedItems[si];
                    var idx = widgetList.indexOf(sortedItem.type);
                    if (idx >= 0) {
                        // Remove the current widget type from widgetList array
                        // to indicate that the given widget has been added to palette
                        widgetList.splice(idx, 1);
                    }
                    section.items.push(sortedItem);
                }
                this._paletteItemGroupCount++;
            }
        }
        initMeta(ctx) {
            var metaRoot = require.toUrl('xideve/metadata/');
            return Metadata.init(ctx || window.sctx, metaRoot);
        }
        init() {
            var metaRoot = require.toUrl('xideve/metadata/');
            var c = Metadata.init(sctx, metaRoot);
            var allLibraries = Metadata.getLibrary();
            var userLibs = Library.getUserLibs(Workbench.getProject()) || {};
            var libraries = {};
            const subs = [];

            function findInAll(name, version) {
                for (var n in allLibraries) {
                    if (allLibraries.hasOwnProperty(n)) {
                        var lib = allLibraries[n];
                        if (lib.name === name && lib.version === version) {
                            var ro = {};
                            ro[name] = allLibraries[n];
                            return ro;
                        }
                    }
                }
                return null;
            }

            userLibs.forEach((ulib) => {
                const library = findInAll(ulib.id, ulib.version);
                dojo.mixin(libraries, library);
            });
            var customWidgetDescriptors = {};

            const customs = new Deferred();
            subs.push(customs);
            const pWidgets = Library.getCustomWidgets();
            if (pWidgets) {
                pWidgets.then((args) => {
                    var customWidgets = args;
                    var customWidgetDescriptors = Library.getCustomWidgetDescriptors();
                    customs.resolve();
                    //if (customWidgets) {
                    //    dojo.mixin(libraries, customWidgets);
                    //}
                    // 0 && console.log('libs', libraries);
                    //debugger;
                });
            }
            /*
            var customWidgetDescriptors = Library.getCustomWidgetDescriptors();
            if (customWidgets) {
                dojo.mixin(libraries, customWidgets);
            }
            */
            var widgetTypeList = [];
            for (var name in libraries) {
                if (libraries.hasOwnProperty(name)) {
                    var lib = libraries[name].$wm;
                    if (!lib) {
                        continue;
                    }
                    lib && lib.widgets && lib.widgets.forEach((item) => {
                        // skip untested widgets
                        if (item.category == "untested" || item.hidden) {
                            return;
                        }
                        widgetTypeList.push(item.type);
                    });
                }
            }
            // groups
            this._paletteItemGroupCount = 0;
            this._widgetPalette = Runtime.getSiteConfigData('widgetPalette');
            const work = new Deferred();
            subs.push(work);
            if (!this._widgetPalette) {
                console.error('widgetPalette.json not defined (in siteConfig folder)');
            } else {
                // There should be a preset for each of built-in composition types (desktop, mobile, sketchhifi, sketchlofi)
                // In future, we might allow users to create custom presets
                var presets = this._widgetPalette.presets || {};

                for (var p in presets) {
                    var widgetList = widgetTypeList.concat(); // clone the array
                    this._presetSections[p] = [];
                    var preset = presets[p];
                    var catchAllSection;
                    // For each preset, the 'sections' property can either be a string or an array of section objects.
                    // If a string, then that string is an reference to a sub-property of the top-level 'defs' object 
                    var sections = (typeof preset.sections == 'string') ?
                        (this._widgetPalette.defs ? this._widgetPalette.defs[preset.sections] : undefined) :
                        preset.sections;

                    if (sections && sections.length) {
                        // console.warning('No sections defined for preset ' + p + ' in widgetPalette.json (in siteConfig folder)');
                        var arr = [];
                        for (var cw in customWidgetDescriptors) {
                            var obj = customWidgetDescriptors[cw];
                            if (obj.descriptor) {
                                arr.push({
                                    name: cw,
                                    value: obj
                                });
                            }
                        }
                        arr.sort(function (a, b) {
                            var aa = a.name.split('/').pop().toLowerCase();
                            var bb = b.name.split('/').pop().toLowerCase();
                            return aa < bb ? -1 : (aa > bb ? 1 : 0);
                        });
                        var UserWidgetSectionAdded = false;
                        var userWidgetSection, customIncludes;
                        for (var j = 0; j < arr.length; j++) {
                            if (!UserWidgetSectionAdded) {
                                userWidgetSection = dojo.clone(this._userWidgetSection);
                                sections = sections.concat(userWidgetSection);
                                UserWidgetSectionAdded = true;
                                customIncludes = userWidgetSection.includes;
                            }
                            var custWidgets = arr[j].value.descriptor.widgets;
                            for (var cw = 0; cw < custWidgets.length; cw++) {
                                var custWidget = custWidgets[cw];
                                customIncludes.push('type:' + custWidget.type);
                            }
                        }
                        for (var s = 0; s < sections.length; s++) {
                            // For each sections, the value can either be a string or a section objects.
                            // If a string, then that string is an reference to a sub-property of the top-level 'defs' object 
                            var sectionObj = (typeof sections[s] == 'string') ?
                                (this._widgetPalette.defs ? this._widgetPalette.defs[sections[s]] : undefined) :
                                sections[s];
                            var section = dojo.clone(sectionObj);
                            // Add preset name to object so downstream logic can add an appropriate CSS class
                            // to the paletteFolder and paletteItem DOM nodes
                            section.preset = preset;
                            section.presetId = p;
                            if (section.subsections && section.subsections.length) {
                                var subsections = section.subsections;
                                for (var sub = 0; sub < subsections.length; sub++) {
                                    if (typeof subsections[sub] == 'string') {
                                        var subsectionObj = this._widgetPalette.defs ? this._widgetPalette.defs[subsections[sub]] : undefined;
                                        if (subsectionObj) {
                                            subsections[sub] = dojo.clone(subsectionObj);
                                        }
                                    }
                                    var subsection = subsections[sub];
                                    if (subsection.includes && subsection.includes.indexOf("$$AllOthers$$") >= 0) {
                                        catchAllSection = subsection;
                                    }
                                    subsection.preset = preset;
                                    subsection.presetId = p;
                                    // Stuffs in value for section.items
                                    this._createSectionItems(subsection, preset, widgetList);
                                }
                            } else {
                                // Stuffs in value for section.items
                                this._createSectionItems(section, preset, widgetList);
                                if (section.includes && section.includes.indexOf("$$AllOthers$$") >= 0) {
                                    catchAllSection = section;
                                }
                            }
                            this._presetSections[p].push(section);
                        }
                    }
                    for (var wi = 0; wi < widgetList.length; wi++) {
                        var widgetType = widgetList[wi];
                        if (catchAllSection) {
                            var item = Metadata.getWidgetDescriptorForType(widgetType);
                            var newItem = dojo.clone(item);
                            this._prepareSectionItem(newItem, catchAllSection, this._paletteItemGroupCount);
                            catchAllSection.items.push(newItem);
                            this._paletteItemGroupCount++;
                        } else {
                             0 && console.log('For preset ' + p + ' Not in widget palette: ' + widgetList[wi]);
                        }
                    }
                }
                work.resolve();
            }
            return all(subs);
        }
        createRootItem(section) {
            const store = this.store;
            section.group = section.subsections.length || section.items.length;            
            section.title = section.name;
            section.parentId = section.parentId || '.';
            //  0 && console.log('create root item : ' + section.name, section);
            section = store.putSync(section);
            if (section.subsections && section.subsections.length) {

                for (var i = 0; i < section.subsections.length; i++) {
                    var subsection = section.subsections[i];
                    subsection._id = subsection.id;
                    subsection.id = section.id + '_' + subsection.name;
                    subsection.parentId = section.id;
                    subsection.section = section;
                    subsection.subsections = subsection.subsections || [];
                    subsection.items = subsection.items || [];
                    subsection.items.forEach((item) => {
                        this.createItem(item, subsection);
                    });
                    this.createRootItem(subsection);
                }
            }
            return section;
        }
        createItem(item, parent, id) {
            item.id = id || (parent.id + '_' + item.type);
            item.parentId = parent.id;
            var name = item.name;
            if (item.$library) {
                name = item.$library._maqGetString(item.type) ||
                    item.$library._maqGetString(item.name) || item.name;
            }

            name = name.replace('<', '');
            name = name.replace('>', '');
            item.title = name;
            item.name = name;
            this.store.putSync(item);
        }
        createStylesSection() {
            const section = {
                "id": "Styles",
                "name": "States",
                "includes": [],
                "subsections": [],
                "items": [],
                "group": true
            }
            var styles = this.ctx.getLibraryManager().getSetting('styles');
            var store = this.ctx.getLibraryManager().getStore();
            // 0 && console.log('add states',store.data);

            store.data.forEach((style) => {
                section.items.push({
                    id: 'Styles_' + style.id,
                    name: style.id,
                    type: style.value.type,
                    group: false,
                    properties: utils.mixin({}, style.value.properties, {
                        name: style.id
                    })
                })
            });

            const root = this.createRootItem(section);
            root.items.forEach((style) => {
                this.createItem(style, root, style.id);
            });
            return root;
        }
        createExtras() {
            const root = this.createStylesSection();
            const store = this.ctx.getLibraryManager().getStore();
            store.on('update', (evt) => {
                const _style = evt.target;            
                const style = {
                    id: 'Styles_' + _style.id,
                    name: _style.id,
                    type: _style.value.type,
                    group: false,
                    properties: utils.mixin({}, _style.value.properties, {
                        name: _style.id
                    })
                }
                this.createItem(style, root, _style.id);                
            });            
        }
        createStore() {
            const comptype = 'delite';
            const storeClass = declare('paletteStore', [TreeMemory, Trackable], {});
            const data = [];
            const store = new storeClass({
                data: data,
                idProperty: 'id',
                parentProperty: 'parentId',
                id: utils.createUUID(),
                ctx: sctx,
                Model: Model,
                mayHaveChildren: function () {
                    return true;
                },
                /**
                 * Return the root item, is actually private
                 * @TODO: root item unclear
                 * @returns {{path: string, name: string, mount: *, directory: boolean, virtual: boolean, _S: (xfile|data|FileStore), getPath: Function}}
                 */
                getRootItem: function () {
                    return {
                        _EX: true,
                        id: '.',
                        name: '.',
                        group: true,
                        virtual: true,
                        _S: this,
                        getPath: function () {
                            return this.id;
                        }
                    };
                },
                getParent: function (mixed) {
                    if (!mixed) {
                        return null;
                    }
                    var item = mixed,
                        result = null;

                    if (_.isString(item)) {
                        item = this.getSync(mixed);
                    }

                    if (item && item.parentId) {
                        result = this.getSync(item.parentId);
                    }
                    return result || this.getRootItem();
                }
            });

            this.store = store;
            this._presetCreated[comptype] = true;
            var presetClassName = this._presetClassNamePrefix + comptype; // Only used for debugging purposes
            var editorPrefs = {
                widgetPaletteLayout: 'icons',
                snap: true
            };
            //FIXME: current preset might be different than comptype once various new UI options become available
            var orderedDescriptions = [];
            var sections = this._presetSections[comptype];
            if (sections) {
                for (var s = 0; s < sections.length; s++) {
                    var section = sections[s];
                    if (!section._created) {
                        var orderedDescriptors = [section];
                        this.createRootItem(section);
                        section._created = true;
                    }
                }
            }
            this.createExtras()
            return store;
        }
    }
    Module.create = function () {
        const p = new Palette();
        p.initMeta();
        setTimeout(() => {
            p.init();
            const store = p.createStore();
        }, 2000);
    }
    if (window.sctx && test) {
        const p = new Palette(window.sctx);
        p.initMeta(window.sctx);
        setTimeout(() => {
            p.init();
            const store = p.createStore();
            if (window['targetContainer']) {
                window['targetContainer'].destroy();
            }
            const target = createContainer(ctx, 'Palette2');
            window['targetContainer'] = target;
            const grid2 = utils.addWidget(GridCSourcelass, {
                ctx: ctx,
                attaddCustachDirect: true,
                collection: store,
                open: true,
                expandOnClick: true,
                sources: [],
                palette: p
            }, null, target, true);
            target.add(grid2);
            $(grid2.bodyNode).addClass('dojoyPalette');
            $(grid2.bodyNode).addClass('paletteLayoutIcons');
            // const windowManager = ctx.getWindowManager();
            // windowManager.registerView(grid2, true);
        }, 2000);
    }
    Module.Palette = Palette;
    Module.Grid = GridCSourcelass;
    return Module;
});
},
'xideve/palette/Grid':function(){
define([
    'xdojo/declare',
    'xide/types',
    'xide/utils',

    "davinci/Workbench",

    "davinci/Runtime",
    "xide/lodash",


    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    "xide/widgets/_Widget",

    'xblox/views/ThumbRenderer',
    "xblox/views/DnD",
    'dojo/dom-construct',
    'xgrid/Selection',
    'xgrid/Search',
    'xgrid/KeyboardNavigation',
    'xide/$',

    "dijit/focus",
    "davinci/ui/dnd/DragManager",
    "davinci/ve/utils/GeomUtils",
    "davinci/ui/dnd/DragSource",
    "davinci/ve/metadata",
    "davinci/ve/tools/CreateTool",
    'dojo/_base/lang',
    'xaction/DefaultActions'
], function (declare, types, utils, Workbench, Runtime, _,
    TreeRenderer, Grid, MultiRenderer, _Widget, ThumbRenderer, BlocksDnD, domConstruct,
    Selection, Search, KeyboardNavigation, $, FocusUtils, DragManager, GeomUtils, DragSource, Metadata, CreateTool, lang, DefaultActions) {

        const ACTION = types.ACTION;
        const DefaultPermissions = [
            //ACTION.EDIT,
            //ACTION.RENAME,
            ACTION.RELOAD,
            ACTION.DELETE,
            //ACTION.CLIPBOARD,
            // ACTION.SELECTION,
            // ACTION.TOOLBAR,
            // ACTION.HEADER,
            ACTION.SEARCH,
            //ACTION.CONTEXT_MENU,
            // 'File/New'
        ]

        const runAction = function (who, action) {
             0 && console.log('run action ', action);
            const selection = who.getSelection();
            const deleteState = (state) => {
                const libMgr = who.ctx.getLibraryManager();
                const store = libMgr.getStore();
                libMgr.setSetting(state.name, null);
                who.collection.removeSync(state.id);
                who.refresh();
            }
            switch (action.command) {
                case 'File/New Group':
                    {
                        selection.forEach(deleteState);
                        break;
                    }

                case 'File/Delete':
                    {
                        selection.forEach(deleteState);
                        break;
                    }

            }
            //return who.inherited(arguments);
        }

        window.pRunAction = runAction;

        const _context = () => {
            try {
                return Runtime.currentEditor.getContext();
            } catch (e) {

            }
        }
        const ThumbClass = declare('xideve.palette.ThumbRenderer2', [ThumbRenderer], {
            resizeThumb: true,
            __type: 'thumb',
            deactivateRenderer: function () {
                $(this.domNode.parentNode).removeClass('metro');
                $(this.domNode).css('padding', '');
                this.isThumbGrid = false;
            },
            activateRenderer: function () {
                $(this.contentNode).css('padding', '8px');
                this.isThumbGrid = true;
                this.refresh();
            },
            _getIconUri: function (uri, fallbackUri) {
                if (uri) {
                    // maybe already resolved
                    if (uri.indexOf("http") === 0) {
                        return uri;
                    }
                    return Workbench.location() + uri;
                }
                return require.toUrl("davinci/" + fallbackUri);
            },
            _setIconProperties: function (opt) {
                // for small icons, first look for small icons, else look for large icons, else use file_obj.gif
                opt.icon = opt.iconBase64 || (opt.icon && this._getIconUri(opt.icon, "ve/resources/images/file_obj.gif")) ||
                    opt.iconLargeBase64 ||
                    (opt.iconLarge && this._getIconUri(opt.iconLarge, "ve/resources/images/file_obj.gif")) ||
                    this._getIconUri(opt.icon, "ve/resources/images/file_obj.gif");
                // for large icons, first look for small icons, else look for large icons, else use file_obj.gif
                opt.iconLarge = opt.iconLargeBase64 ||
                    (opt.iconLarge && this._getIconUri(opt.iconLarge, "ve/resources/images/file_obj.gif")) ||
                    opt.iconBase64 || this._getIconUri(opt.icon, "ve/resources/images/file_obj.gif");
            },
            template2: '' +
                '<span class="paletteItemSelectionContainer"></span>' +
                '<span class="paletteItemNormalContainer">' +
                '<span class="paletteItemImageContainer" style="margin-top:8px">' +
                '<img class="paletteItemImage" border="0"/>' +
                '</span>' +
                '<span class="paletteItemLabelContainer">' +
                '<span class="paletteItemLabel"></span>' +
                '</span>' +
                '</span>' +
                '',
            /**
             * Override renderRow
             * @param obj
             * @returns {*}
             */
            renderRow: function (obj) {
                var div = domConstruct.create('div', {
                    className: "tile widget form-control dojoyPaletteCommon dojoyPaletteItem maqPaletteSection_desktop dojoyPaletteItemSunken widgetBase",
                    // className: "tile widget form-control",
                    // style: 'float:left;padding:3px; width:100px; margin:4px'
                }),
                    label = obj.name,
                    imageClass = 'paletteItemImage';

                div.innerHTML = this.template2;
                var iconStyle = 'float:left; margin-left:3px;margin-top:3px;margin-right:3px;text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: inherit;opacity: 0.4;width:35px';
                this._setIconProperties(obj);

                if (obj.icon.indexOf('base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR') !== -1) {
                    obj.icon = '';
                }
                var label = div.querySelector('.paletteItemLabel');

                label.appendChild(dojo.doc.createTextNode(obj.name));
                var img = div.querySelector('img');
                if (obj && obj.iconHTML) {
                    dojo.destroy(img);
                    var container = div.querySelector('.paletteItemImageContainer');
                    container.innerHTML = item.iconHTML;
                } else {
                    img.src = obj.icon;
                }
                return div;
            }
        });

        const GroupRenderer = declare('xideve.palette.GroupRenderer', TreeRenderer, {
            itemClass: ThumbClass,
            groupClass: TreeRenderer,
            renderRow: function (obj) {
                return (!obj.group ? this.itemClass : this.groupClass).prototype.renderRow.apply(this, arguments);
            }
        });
        var Implementation = {},
            renderers = [GroupRenderer, ThumbClass, TreeRenderer],
            multiRenderer = declare.classFactory('xideve.palette.PaletteGridRenderer', {}, renderers, MultiRenderer.Implementation);

        const SharedDndGridSource = BlocksDnD.BlockGridSource;

        const GridClass = Grid.createGridClass('xideve.palette.PaletteGrid', Implementation, {
            SELECTION: {
                CLASS: Selection
            },
            KEYBOARD_SELECTION: true,
            //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            COLUMN_HIDER: false,
            WIDGET: {
                CLASS: _Widget
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            SEARCH: {
                CLASS: Search
            }
        }, {
                RENDERER: multiRenderer
            }, {
                renderers: renderers,
                selectedRenderer: GroupRenderer,
                permissions: DefaultPermissions
            });

        const GridCSourcelass = declare('ActionGrid', GridClass, {
            formatColumn: function (field, value, obj) {
                var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
                if (renderer.formatColumn_) {
                    var result = renderer.formatColumn.apply(arguments);
                    if (result) {
                        return result;
                    }
                }
                if (obj.renderColumn_) {
                    var rendered = obj.renderColumn.apply(this, arguments);
                    if (rendered) {
                        return rendered;
                    }
                }
                switch (field) {
                    case "title":
                        {
                            value = obj.group ? ('<span style="float:left;margin-right: 10px">' + value + '</span><hr style="margin-top: 13px;margin-left: 4px"/>') : value;
                        }
                }
                return value;
            },
            _columns: {},
            postMixInProperties: function () {
                var state = this.state;
                if (state) {
                    if (state._columns) {
                        this._columns = state._columns;
                    }
                }
                this.columns = this.getColumns();
                return this.inherited(arguments);
            },
            getColumns: function (formatters) {
                var self = this;
                this.columns = [{
                    renderExpando: true,
                    label: 'Name',
                    field: 'title',
                    sortable: true,
                    formatter: function (value, obj) {
                        return self.formatColumn('title', value, obj);
                    },
                    hidden: false
                }];
                return this.columns;
            },
            dragMananager: null,
            veContext: null,
            updateDragManager: function (context) {
                if (context) {
                    this.dragMananager.document = context.getDocument();
                    const frameNode = context.frameNode;
                    if (frameNode) {
                        const coords = dojo.coords(frameNode);
                        const containerNode = context.getContainerNode();
                        this.dragMananager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                        this.dragMananager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
                    }
                }
            },
            onContextDestroyed: function (context) {
                if (context.__veDNDDevHandles) {
                    _.each(context.__veDNDDevHandles, utils.destroy);
                    context.__veDNDDevHandles;
                }
                delete this.editorContext;
            },
            onAppReady: function (evt) {
                if (this.editorContext && this.editorContext != evt.context) {
                    this.editorContext = evt.context;
                }
                if (!this.dragMananager) {
                    this.dragMananager = DragManager;
                }
                this.editorContext = evt.context;
                this.updateDragManager(evt.context);
            },
            addCustomWidget: function (lib) {
                //  0 && console.log('add custom widget', lib);
                /* make sure the pallette has loaded. if it hasnt, the init will take care of customs */
                // if (!this._loaded) return;
                if (!lib || !lib.$wm || !lib.$wm.widgets || !lib.$wm.widgets.length) {
                    return;
                }
                //const context = Runtime.currentEditor.getContext();
                const comptype = 'delite';
                const palette = this.palette;

                const $library = lib.$wm;
                const widgets = lib.$wm.widgets;
                const folderToShow = null;
                for (var w = 0; w < widgets.length; w++) {
                    var item = widgets[w];
                    for (var presetId in this.palette._presetSections) {
                        var customSection = null;
                        var sections = this.palette._presetSections[presetId];
                        if (!sections) {
                            console.error('Palette.js:addCustomWidget - no sections for comptype=' + presetId);
                        } else {
                            for (var s = 0; s < sections.length; s++) {
                                var section = sections[s];
                                if (section.id == '$$UserWidgets$$') {
                                    customSection = section;
                                    break;
                                }
                            }
                            if (!customSection) {
                                customSection = dojo.clone(palette._userWidgetSection);
                                customSection.preset = palette._presetSections[presetId];
                                customSection.presetId = presetId;
                                customSection.items = [];
                                sections.push(customSection);
                                this.customSection = customSection;
                                if (this.palette._presetCreated[presetId]) {
                                    var orderedDescriptors = [customSection];
                                    customSection._created = true;
                                    customSection.subsections = [];
                                    customSection.group = 4;

                                }
                            }
                            var includesValue = 'type:' + item.type;
                            if (customSection.includes.indexOf(includesValue) < 0) {
                                customSection.includes.push(includesValue);
                                item.$library = $library;
                                item.section = customSection;
                                item._paletteItemGroup = palette._paletteItemGroupCount++;
                                customSection.items.push(item);
                                var name = 'custom';
                                var folder = null;
                                if (!this.__c) {
                                    customSection._created = true;
                                    customSection.subsections = [];
                                    customSection.group = 4;
                                    palette.createRootItem(customSection);
                                    this.set('collection', this.collection.filter({
                                        parentId: '.'
                                    }));
                                    this.__c = true;
                                }
                                palette.createItem(item, customSection);
                            }
                        }
                    }
                }
                if (folderToShow) {
                    // open the currently active custom widget folder after creating a new custom widget
                    folderToShow.showHideFolderContents(true);
                }

                setTimeout(() => {
                    this.set('collection', this.collection.filter({
                        parentId: '.'
                    }))
                }, 1000);
            },
            runAction: function (action) {
                return pRunAction(this, action);
                return this.inherited(arguments);
            },
            _shouldDisableStates: function () {
                const selection = this.getSelection() || [];
                const first = selection[0];
                if (first) {
                    if (first.id === 'Styles') {
                        return true;
                    }
                    const parent = first.isChildOfId('Styles');
                    if (parent) {
                        return false;
                    }
                    if (first.id === 'Styles') {
                        return false;
                    }
                }
                return true;
            },
            _getActions: function () {
                const actions = [];
                const defaultMixin = {
                    addPermission: true
                };
/*
                actions.push(this.createAction({
                    label: 'New Group',
                    command: 'File/New Group',
                    icon: 'fa-file-o',
                    tab: 'Home',
                    group: 'New',
                    mixin: utils.mixin({
                        quick: true
                    }, defaultMixin),
                    shouldDisable: () => this._shouldDisableStates
                }));*/
                /*
                actions.push(this.createAction({
                    label: 'New State',
                    command: 'File/New State',
                    icon: 'fa-magic',
                    tab: 'Home',
                    group: 'New',
                    mixin: utils.mixin({
                        quick: true
                    }, defaultMixin),
                    shouldDisable: () => this._shouldDisableStates
                }));
                */
                return actions;
            },
            startup: function (item) {

                const libMgr = this.ctx.getLibraryManager();
                const store = libMgr.getStore();
                store.on('update', () => {
                    this.refresh();
                });
                this._on('addAction', (action) => {
                    if (action.command === 'File/Delete') {
                        action.shouldDisable = () => {
                            return this._shouldDisableStates();
                        };
                    }
                })
                const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
                this.addActions(_defaultActions);
                this.addActions(this._getActions());
                this.subscribe("/davinci/ui/addedCustomWidget", this.addCustomWidget);
                const _dragStart = function (from, node, context, e, proto, userData, properties) {
                    proto = utils.clone(proto);
                    utils.mixin(proto.properties, properties);
                    const data = e.dragSource.data;
                    userData = data.userData || userData;
                    userData.onCreate = function () { }
                    Metadata.getHelper(proto.type, 'tool').then(function (ToolCtor) {
                        // Copy the data in case something modifies it downstream -- what types can data.data be?
                        const tool = new (ToolCtor || CreateTool)(dojo.clone(proto), userData);
                        context.setActiveTool(tool);
                    }.bind(this));

                    // Sometimes blockChange doesn't get cleared, force a clear upon starting a widget drag operation
                    context.blockChange(false);
                    // Place an extra DIV onto end of dragCloneDiv to allow
                    // posting a list of possible parent widgets for the new widget
                    // and register the dragClongDiv with Context
                    if (e._dragClone) {
                        $(e._dragClone).addClass('paletteDragContainer');
                        //domClass.add(e._dragClone, 'paletteDragContainer');
                        dojo.create('div', {
                            className: 'maqCandidateParents'
                        }, e._dragClone);
                    }
                    //FIXME: Attach dragClone and event listeners to tool instead of context?
                    context.setActiveDragDiv(e._dragClone);
                };
                const _dragEnd = function () {

                    if (FocusUtils.curNode && FocusUtils.curNode.blur) {
                        FocusUtils.curNode.blur();
                    }
                };

                TreeRenderer.prototype.activateRenderer.apply(this);

                const makeDNDG = function (from, node, context, proto, userData, properties) {
                    const clone = node.domNode;
                    const ds = new DragSource(node.domNode, "component", node, clone);
                    ds.targetShouldShowCaret = true;
                    ds.returnCloneOnFailure = false;
                    const result = {
                        handles: [],
                        context: context,
                        node: clone,
                        ds: ds,
                        destroy: function () {
                            delete this.context;
                            _.each(this.handles, dojo.disconnect);
                            delete this.handles;
                            delete this.node.__vednd;
                            delete this.node;
                            delete this.ds;
                        }
                    };
                    const handles = result.handles;

                    handles.push(dojo.connect(ds, "onDragStart", dojo.hitch(from, function (e) {
                        _dragStart(from, node, context, e, proto, userData, properties);
                    })));

                    handles.push(dojo.connect(ds, "onDragEnd", dojo.hitch(from, function (e) {
                        _dragEnd();
                    })));
                    handles.push(dojo.connect(node.domNode, "onmouseover", function (e) {
                        node._mouseover = true;
                    }));

                    handles.push(dojo.connect(node.domNode, "onmouseout", function (e) {
                        node._mouseover = false;
                    }));

                    handles.push(dojo.connect(node.domNode, "onmousedown", function (e) {
                        const DragManager = from.dragMananager;
                        DragManager.document = context.getDocument();
                        const frameNode = context.frameNode;
                        if (frameNode) {
                            const coords = dojo.coords(frameNode);
                            const containerNode = context.getContainerNode();
                            DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                            DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
                        }
                    }));

                    return result;
                };

                this._showHeader(false);
                // this.showToolbar(false);
                var res = this.inherited(arguments);
                _.each(this.getRows(), function (row) {
                    this.expand(row, false);
                }, this);
                $(this.domNode).addClass('blockPalette');

                this.subscribe([
                    types.EVENTS.ON_APP_READY,
                    types.EVENTS.ON_CONTEXT_DESTROYED
                ]);
                const thiz = this;
                const ctx = thiz.ctx;

                this._on('selectionChanged', function (evt) {
                    const selection = evt.selection;
                    //  0 && console.log('selected : ', selection);
                    const item = selection && selection.length == 1 ? selection[0] : null;
                    if (!item) {
                        return;
                    }
                    const row = thiz.row(item);
                    const node = row.element;

                    if (row.element.__vednd) {
                        return;
                    }
                    node.__vednd = true;
                    const _props = {}
                    const _editorContext = thiz.editorContext;
                    const rowObject = {
                        domNode: node
                    };
                    if (_editorContext) {
                        if (!_editorContext.__veDNDDevHandles) {
                            _editorContext.__veDNDDevHandles = [];
                        }
                        const _protoDefault = {
                            properties: item.properties || {},
                            type: item.type,
                            userData: {}
                        };
                        if (!item.group) {
                            _editorContext.__veDNDDevHandles.push(makeDNDG(this, rowObject, _editorContext, _protoDefault, item, _props));
                        }
                    }
                });
                this.onAppReady({
                    context: _context()
                });
                return res;
            }
        });
        return GridCSourcelass;
    });
},
'xideve/palette/Model':function(){
/** @module xfile/model/File **/
define([
    "dcl/dcl",
    "xide/data/Model",
    "xide/utils",
    "xide/types",
    "xide/lodash"
], function (dcl, Model, utils, types, _) {
    return dcl(Model, {
        declaredClass: 'xideve.palette.model',
        getFolder: function () {
            var path = this.getPath();
            if (this.directory) {
                return path;
            }
            return utils.pathinfo(path, types.PATH_PARTS.ALL).dirname;
        },
        getChildren: function () {
            return this.children;
        },
        isChildOfId: function (id) {
            const parent = this.getParent();
            if (parent) {
                if (parent.id === id) {
                    return parent;
                }
                if (parent.isChildOfId) {
                    return parent.isChildOfId(id);
                }
            }
            return null;
        },
        getParent: function () {
            //current folder:
            var store = this.getStore() || this._S;
            return store.getParent(this);
        },
        getChild: function (path) {
            return _.find(this.getChildren(), {
                path: path
            });
        },
        getStore: function () {
            return this._store || this._S;
        }
    });
});
},
'xideve/Embedded':function(){
define([
    'dojo/_base/declare',
    'davinci/davinci_',
    'dojo/Deferred',
    'davinci/ve/metadata',
    'davinci/Runtime',
    'require',
    "dojo/_base/connect",
    'davinci/Workbench',
    "davinci/ve/metadata",
    "xideve/views/StyleView"
], function (declare, davinci, Deferred, metadata, Runtime, require, connect, Workbench, Metadata, StyleView) {
    var handleIoError = function (deferred, reason) {
        /*
         *  Called by the subscription to /dojo/io/error , "
         *  /dojo/io/error" is sent whenever an IO request has errored.
         *	It passes the error and the dojo.Deferred
         *	for the request with the topic.
         */

        if (reason.status == 401 || reason.status == 403) {
            //sessionTimedOut();
            // Only handle error if it is as of result of a failed XHR connection, not
            // (for example) if a callback throws an error. (For dojo.xhr, def.cancel()
            // is only called if connection fails or if it times out.)
        } else if (deferred.canceled === true) {
            // Filter on XHRs for maqetta server commands.  Possible values which we
            // test for:
            //     cmd/findResource
            //     ./cmd/createResource
            //     http://<host>/maqetta/cmd/getComments
            var reCmdXhr = new RegExp('(^|\\.\\/|' + document.baseURI + '\\/)cmd\\/');
            var url = deferred.ioArgs.url;
            if (reCmdXhr.test(url)) {
                // Make exception for "getBluePageInfo" because it regularly gets cancelled
                // by the type ahead searching done from the combo box on the 3rd panel of
                // the R&C wizard. The cancellation is not really an error.
                if (url.indexOf("getBluePageInfo") >= 0) {
                    return;
                }
            } else {
                // Must not be a Maqetta URL (like for JSONP on GridX), so skip
                return;
            }

            Runtime.handleError(reason.message);
             0 && console.warn('Failed to load url=' + url + ' message=' + reason.message +
                ' status=' + reason.status);
        }
    };
    var debug = false;
    /**
     *
     * @class xide/ve/Embedded
     */
    return declare("xideve/Embedded", null, {

        _loadActionClasses: function () {

            var editorID = 'davinci.ve.HTMLPageEditor'; //this.editorExtension.id;
            var editorActions = [];

            var extensions = Runtime.getExtensions('davinci.editorActions', function (ext) {
                if (editorID == ext.editorContribution.targetID) {
                    editorActions.push(ext.editorContribution);
                    return true;
                }
            });
            if (editorActions.length == 0) {
                var extensions = Runtime.getExtension('davinci.defaultEditorActions', function (ext) {
                    editorActions.push(ext.editorContribution);
                    return true;
                });
            }
            var libraryActions = Metadata.getLibraryActions('davinci.editorActions', editorID);
            // Clone editorActions, otherwise, library actions repeatedly get appended to original plugin object
            editorActions = dojo.clone(editorActions);
            if (editorActions.length > 0 && libraryActions.length) {
                // We want to augment the action list, so let's clone the
                // action set before pushing new items onto the end of the
                // array
                dojo.forEach(libraryActions, function (libraryAction) {
                    var Workbench = require("davinci/Workbench");
                    if (libraryAction.action) {
                        Workbench._loadActionClass(libraryAction);
                    }
                    if (libraryAction.menu) {
                        for (var i = 0; i < libraryAction.menu.length; i++) {
                            var subAction = libraryAction.menu[0];
                            if (subAction.action) {
                                Workbench._loadActionClass(subAction);
                            }
                        }
                    }
                    editorActions[0].actions.push(libraryAction);
                });
            }

            // 0 && console.log('editor actions : ',editorActions);

            return editorActions;
        },

        started: function () {

        },
        onReady: function (dfd) {

            this._loadActionClasses();

            var _c = document.getElementById('focusContainer');
            if (!_c) {
                _c = dojo.create('div', {
                    'class': 'focusContainer',
                    id: 'focusContainer'
                }, document.body);
                Workbench.focusContainer = _c;
            }

            Workbench._state = {
                "activeEditor": null,
                "editors": [],
                "nhfo": {
                    "project1": {
                        "device": "iphone",
                        "layout": "flow",
                        "themeSet": {
                            "name": "(none)",
                            "desktopTheme": "claro",
                            "mobileTheme": [{
                                    "theme": "android",
                                    "device": "Android"
                                },
                                {
                                    "theme": "blackberry",
                                    "device": "BlackBerry"
                                },
                                {
                                    "theme": "ipad",
                                    "device": "iPad"
                                },
                                {
                                    "theme": "iphone",
                                    "device": "iPhone"
                                },
                                {
                                    "theme": "iphone",
                                    "device": "other"
                                }
                            ]
                        }
                    }
                },
                "project": "project1",
                "id": "",
                "Fields": []
            };

            Workbench.run2();

            this.started();

            dfd.resolve(this);

        },

        /**
         *
         * @param ctx
         * @param mini
         * @param cmdOffset
         * @param userBaseUrl
         */
        start: function (ctx, mini, cmdOffset, userBaseUrl) {

            debug &&  0 && console.log('start ve with cmd offset', cmdOffset);

            var thiz = this,
                dfd = new Deferred();

            Runtime.cmdOffset = cmdOffset || '';
            Runtime.loadPlugins();
            Runtime.initialPerspective = "davinci.ve.pageDesign";

            if (mini === true) {
                Runtime.isLocalInstall = true;
                // Needed by review code
                Runtime.userName = 'none';
                Runtime.userEmail = 'none';
                Runtime.run();
                Workbench.ctx = ctx;
                thiz.onReady();
                return;
            }

            var fileManager = ctx.getFileManager();
            var serviceClass = 'XApp_XIDE_Workbench_Service';
            var ready = function (result) {
                Runtime.userBaseUrl = userBaseUrl;
                Runtime._initializationInfo = result;
                var userInfo = result.userInfo;
                Runtime.isLocalInstall = userInfo.userId == 'maqettaUser';

                // Needed by review code
                Runtime.userName = userInfo.userId;
                Runtime.userEmail = userInfo.email;

                var metaRoot = require.toUrl('xideve/metadata/');
                metadata.init(ctx, metaRoot);
                Runtime.run();
                Workbench.ctx = ctx;
                thiz.onReady(dfd);
                Runtime.subscribe("/davinci/states/state/changed",
                    function (e) {
                        var currentEditor = Runtime.currentEditor;
                        // ignore updates in theme editor and review editor
                        if ((currentEditor.declaredClass != "davinci.ve.themeEditor.ThemeEditor" &&
                                currentEditor.declaredClass != "davinci.review.editor.ReviewEditor") /*"davinci.ve.VisualEditor"*/ ) {
                            currentEditor.visualEditor.onContentChange.apply(currentEditor.visualEditor, arguments);
                        }
                    }
                );
                // bind overlay widgets to corresponding davinci states. singleton; no need to unsubscribe
                connect.subscribe("/davinci/states/state/changed", function (args) {

                    //FIXME: This is page editor-specific logic within Workbench.
                    var context = (Runtime.currentEditor && Runtime.currentEditor.declaredClass == "davinci.ve.PageEditor" &&
                        Runtime.currentEditor.visualEditor && Runtime.currentEditor.visualEditor.context);
                    if (!context) {
                        return;
                    }
                    var prefix = "_show:",
                        widget, dvWidget, helper;
                    var thisDijit = context ? context.getDijit() : null;
                    var widgetUtils = require("davinci/ve/widget");
                    if (args.newState && !args.newState.indexOf(prefix)) {
                        widget = thisDijit.byId(args.newState.substring(6));
                        dvWidget = widgetUtils.getWidget(widget.domNode);
                        helper = dvWidget.getHelper();
                        helper && helper.popup && helper.popup(dvWidget);
                    }
                    if (args.oldState && !args.oldState.indexOf(prefix)) {
                        widget = thisDijit.byId(args.oldState.substring(6));
                        dvWidget = widgetUtils.getWidget(widget.domNode);
                        helper = dvWidget.getHelper();
                        helper && helper.tearDown && helper.tearDown(dvWidget);
                    }
                });
                // bind overlay widgets to corresponding davinci states. singleton; no need to unsubscribe
                connect.subscribe("/davinci/ui/repositionFocusContainer", function (args) {
                    Workbench._repositionFocusContainer();
                });

            };

            var inner = fileManager.callMethodEx(serviceClass, 'getInfo', ['nada'], ready, true);
            Runtime.subscribe('/dojo/io/error', handleIoError); // /dojo/io/error" is sent whenever an IO request has errored.
            return dfd;
        }
    });
});
},
'xideve/views/StyleView':function(){
define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "davinci/lang/ve",
    "xide/layout/ContentPane",
    "xideve/widgets/WidgetLite",
    "xide/mixins/ReloadMixin",
    "davinci/ve/widgets/HTMLStringUtil"
], function (declare, lang, veNls, ContentPane,
    WidgetLite, ReloadMixin, HTMLStringUtil) {

    return declare("xideve/views/StyleView", [WidgetLite, ReloadMixin], {
        /* FIXME: These won't expand into pageTemplate. Not sure if that's a JS issue or dojo.declare issue.
         * Temporary copied into each property below but would be better if could get reusable values working somehow.
         _paddingMenu:['', '0px', '1em'],
         _radiusMenu:['', '0px', '6px'],
         */
        showWidgetProperties: true,
        _editor: null, // selected editor
        _widget: null, // selected widget
        _widgets: null,
        _subWidget: null, // selected sub widget
        _titleBarDiv: "<div class='palette_titleBarDiv'><span class='paletteCloseBox'></span><span class='titleBarDivTitle'></span></div>",
        constructor: function (params, srcNodeRef) {
            this.subscribe("/davinci/ui/editorSelected", dojo.hitch(this, this._editorSelected));
            this.subscribe("/davinci/ui/widgetSelected", dojo.hitch(this, this._widgetSelectionChanged));
            this.subscribe("/davinci/states/state/changed", dojo.hitch(this, this._stateChanged));
            this.subscribe("/maqetta/appstates/state/changed", dojo.hitch(this, this._stateChanged));
            this.subscribe("/davinci/ui/initialPerspectiveReady", dojo.hitch(this, this._initialPerspectiveReady));
            this.subscribe("/davinci/workbench/ready", dojo.hitch(this, this._workbenchReady));
        },
        getPageTemplate: function () {
            return [

                //Note: the keys here must match the propsect_* values in the supports() functions
                //in the various editors, such as PageEditor.js and ThemeEditor.js

                /*
                 {key: "common",
                 className:'maqPropertySection page_editor_only',
                 addCommonPropertiesAtTop:false,
                 pageTemplate:{html: "<div dojoType='davinci.ve.widgets.CommonProperties'></div>"}},
                 */
                // NOTE: the first section (widgetSpecific) is injected within buildRendering()
                {
                    key: "widgetSpecific",
                    className: 'maqPropertySection page_editor_only',
                    addCommonPropertiesAtTop: false,
                    html: "<div dojoType='davinci.ve.widgets.WidgetProperties'></div>"
                },

                // NOTE: other sections are added dynamically via first call to _beforeEditorSelected
                {
                    key: "events",
                    className: 'maqPropertySection page_editor_only',
                    addCommonPropertiesAtTop: false,
                    pageTemplate: {
                        html: "<div dojoType='davinci.ve.widgets.EventSelection'></div>"
                    }
                },
                {
                    key: "layout",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    /* startsNewGroup:true, // This flag causes a few extra pixels between this and previous button */
                    pageTemplate: [{
                            display: "width",
                            type: "multi",
                            target: ["width"],
                            values: ['', 'auto', '100%', '200px', '10em']
                        },
                        {
                            display: "height",
                            type: "multi",
                            target: ["height"],
                            values: ['', 'auto', '100%', '200px', '10em']
                        },
                        {
                            html: "&nbsp;"
                        },
                        {
                            key: "showMinMax",
                            display: "&nbsp;&nbsp;&nbsp;",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "min-height",
                                    type: "multi",
                                    target: ["min-height"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "max-height",
                                    type: "multi",
                                    target: ["max-height"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "min-width",
                                    type: "multi",
                                    target: ["min-width"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "max-width",
                                    type: "multi",
                                    target: ["max-width"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    html: "&nbsp;",
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        },
                        {
                            display: "position",
                            type: "combo",
                            target: ["position"],
                            values: ['', 'absolute', 'fixed', 'relative', 'static']
                        },
                        {
                            display: "left",
                            type: "multi",
                            target: ["left"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "top",
                            type: "multi",
                            target: ["top"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "right",
                            type: "multi",
                            target: ["right"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "bottom",
                            type: "multi",
                            target: ["bottom"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "display",
                            type: "combo",
                            target: ["display"],
                            values: ['', 'none', 'block', 'inline', 'inline-block']
                        },
                        {
                            display: "opacity",
                            type: "multi",
                            target: ["opacity"],
                            values: ['0', '0.5', '1.0']
                        },
                        {
                            display: "box-shadow",
                            type: "text",
                            target: ["box-shadow"],
                            value: ['', 'none', '1px 1px rgba(0,0,0,.5)']
                        },
                        {
                            display: "float",
                            type: "combo",
                            target: ["float"],
                            values: ['', 'none', 'left', 'right']
                        },
                        {
                            display: "clear",
                            type: "combo",
                            target: ["clear"],
                            values: ['', 'none', 'left', 'right', 'both']
                        },
                        {
                            display: "overflow",
                            type: "combo",
                            target: ["overflow"],
                            values: ['', 'visible', 'hidden', 'scroll', 'auto']
                        },
                        {
                            display: "z-index",
                            type: "multi",
                            target: ["z-index"],
                            values: ['', 'auto', '0', '1', '100', '-1', '-100']
                        },
                        {
                            display: "box-sizing",
                            type: "combo",
                            target: ['box-sizing', '-webkit-box-sizing', '-ms-box-sizing', '-moz-box-sizing'],
                            values: ['', 'content-box', 'border-box']
                        }
                    ]
                },
                {
                    key: "padding",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "<b>(padding)</b>",
                            type: "multi",
                            target: ["padding"],
                            values: ['', '0px', '1em']
                        },
                        {
                            key: "showtrbl",
                            display: "&nbsp;&nbsp;&nbsp;",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "padding-top",
                                    type: "multi",
                                    target: ["padding-top"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "padding-right",
                                    type: "multi",
                                    target: ["padding-right"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "padding-bottom",
                                    type: "multi",
                                    target: ["padding-bottom"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "padding-left",
                                    type: "multi",
                                    target: ["padding-left"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        }
                    ]
                },
                {
                    key: "margins",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "<b>(margin)</b>",
                            type: "multi",
                            target: ["margin"],
                            values: ['', '0px', '1em']
                        },
                        {
                            key: "showtrbl",
                            display: "&nbsp;&nbsp;&nbsp;",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "margin-top",
                                    type: "multi",
                                    target: ["margin-top"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "margin-right",
                                    type: "multi",
                                    target: ["margin-right"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "margin-bottom",
                                    type: "multi",
                                    target: ["margin-bottom"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "margin-left",
                                    type: "multi",
                                    target: ["margin-left"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        }
                    ]
                },
                {
                    key: "background",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "background-color",
                            type: "background",
                            target: ['background-color'],
                            colorswatch: true
                        },
                        {
                            display: "background-image",
                            type: "background",
                            target: ['background-image'],
                            values: ['', 'none']
                        },
                        {
                            display: "background-repeat",
                            type: "background",
                            values: ['', 'repeat', 'repeat-x', 'repeat-y', 'no-repeat'],
                            target: ['background-repeat']
                        },
                        {
                            display: "background-position",
                            type: "background",
                            target: ['background-position'],
                            values: ['', '0px 0px', '0% 0%', 'left top', 'center center', 'right bottom']
                        },
                        {
                            display: "background-size",
                            type: "background",
                            target: ['background-size'],
                            values: ['', 'auto', 'contain', 'cover', '100%']
                        },
                        {
                            display: "background-origin",
                            type: "background",
                            target: ['background-origin'],
                            values: ['', 'border-box', 'padding-box', 'content-box']
                        },
                        {
                            display: "background-clip",
                            type: "background",
                            target: ['background-clip'],
                            values: ['', 'border-box', 'padding-box', 'content-box']
                        }
                    ]
                },
                {
                    key: "border",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "<b>(border)</b>",
                            type: "multi",
                            target: ['border'],
                            values: ['', 'none', '1px solid black']
                        },
                        {
                            display: "show",
                            type: "combo",
                            values: ['none', 'props', 'sides', 'all'],
                            id: 'properties_show_select',
                            onchange: function (propIndex) {
                                if (typeof propIndex != "number") {
                                    return;
                                }
                                var showRange = function (sectionData, first, last, begin, end) {
                                    for (var k = first; k <= last; k++) {
                                        var thisProp = sectionData[k];
                                        var thisRow = dojo.byId(thisProp.rowId);
                                        if (k >= begin && k <= end) {
                                            dojo.removeClass(thisRow, "propertiesSectionHidden");
                                        } else {
                                            dojo.addClass(thisRow, "propertiesSectionHidden");
                                        }
                                    }
                                };
                                if (this.pageTemplate) {
                                    var propObj = this.pageTemplate[propIndex];
                                    var value;
                                    if (propObj && propObj.id) {
                                        value = dojo.byId(propObj.id).value;
                                    }
                                    // In some circumstances, value is undefined, which causes exception.
                                    // Make sure it is a string.
                                    if (dojo.isString(value)) {
                                        if (value === 'none') {
                                            showRange(this.pageTemplate, 2, 20, -1, -1);
                                        } else if (value === 'sides') {
                                            showRange(this.pageTemplate, 2, 20, 2, 5);
                                        } else if (value === 'props') {
                                            showRange(this.pageTemplate, 2, 20, 6, 8);
                                        } else if (value === 'all') {
                                            showRange(this.pageTemplate, 2, 20, 9, 20);
                                        }
                                    }
                                }
                            }
                        },
                        {
                            display: "border-top",
                            type: "multi",
                            target: ['border-top'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right",
                            type: "multi",
                            target: ['border-right'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom",
                            type: "multi",
                            target: ['border-bottom'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left",
                            type: "multi",
                            target: ['border-left'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },


                        {
                            display: "border-width",
                            type: "multi",
                            target: ['border-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-style",
                            type: "multi",
                            target: ['border-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-color",
                            type: "color",
                            target: ['border-color'],
                            rowClass: 'propertiesSectionHidden'
                        },

                        {
                            display: "border-top-width",
                            type: "multi",
                            target: ['border-top-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-top-style",
                            type: "multi",
                            target: ['border-top-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-top-color",
                            type: "color",
                            target: ['border-top-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right-width",
                            type: "multi",
                            target: ['border-right-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right-style",
                            type: "multi",
                            target: ['border-right-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right-color",
                            type: "color",
                            target: ['border-right-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom-width",
                            type: "multi",
                            target: ['border-bottom-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom-style",
                            type: "multi",
                            target: ['border-bottom-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom-color",
                            type: "color",
                            target: ['border-bottom-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left-width",
                            type: "multi",
                            target: ['border-left-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left-style",
                            type: "multi",
                            target: ['border-left-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left-color",
                            type: "color",
                            target: ['border-left-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-collapse",
                            type: "combo",
                            target: ['border-collapse'],
                            values: ['', 'separate', 'collapse']
                        },
                        {
                            display: "<b>(border-radius)</b>",
                            type: "multi",
                            target: ['border-radius', '-moz-border-radius'],
                            values: ['', '0px', '6px']
                        },
                        {
                            key: "showDetails",
                            display: "",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "border-top-left-radius",
                                    type: "multi",
                                    target: ["border-top-left-radius", '-moz-border-radius-topleft'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "border-top-right-radius",
                                    type: "multi",
                                    target: ['border-top-right-radius', '-moz-border-radius-topright'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "border-bottom-right-radius",
                                    type: "multi",
                                    target: ['border-bottom-right-radius', '-moz-border-radius-bottomright'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "border-bottom-left-radius",
                                    type: "multi",
                                    target: ['border-bottom-left-radius', '-moz-border-radius-bottomleft'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        }
                    ]
                },
                {
                    key: "fontsAndText",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "font",
                            type: "text",
                            target: ["font"]
                        },
                        {
                            display: "font-family",
                            type: "font",
                            target: ["font-family"]
                        },
                        {
                            display: "size",
                            type: "multi",
                            target: ["font-size"],
                            values: ['', '100%', '1em', '10px', '10pt']
                        },
                        {
                            display: "color",
                            type: "color",
                            target: ["color"]
                        },
                        {
                            display: "font-weight",
                            type: "combo",
                            target: ["font-weight"],
                            values: ['', 'normal', 'bold']
                        },
                        {
                            display: "font-style",
                            type: "combo",
                            target: ["font-style"],
                            values: ['', 'normal', 'italic']
                        },
                        {
                            display: "text-decoration",
                            type: "combo",
                            target: ["text-decoration"],
                            values: ['', 'none', 'underline', 'line-through']
                        },
                        {
                            display: "text-align",
                            type: "combo",
                            target: ["text-align"],
                            values: ['', 'left', 'center', 'right', 'justify']
                        },
                        {
                            display: "vertical-align",
                            type: "combo",
                            target: ["vertical-align"],
                            values: ['', 'baseline', 'top', 'middle', 'bottom']
                        },
                        {
                            display: "white-space",
                            type: "combo",
                            target: ['white-space'],
                            values: ['', 'normal', 'nowrap', 'pre', 'pre-line', 'pre-wrap']
                        },
                        {
                            display: "text-indent",
                            type: "multi",
                            target: ["text-indent"],
                            values: ['', '0', '1em', '10px']
                        },
                        {
                            display: "line-height",
                            type: "multi",
                            target: ["line-height"],
                            values: ['', 'normal', '1.2', '120%']
                        }
                    ]
                }
                /*,
                                 {key: "shapesSVG",
                                 className:'maqPropertySection',
                                 addCommonPropertiesAtTop:true,
                                 pageTemplate:[{display:"stroke", type:"color", target:["stroke"]},
                                 {display:"stroke-width", type:"multi", target:["stroke-width"], values:['','1', '2', '3', '4', '5', '10']},
                                 {display:"fill", type:"color", target:["fill"]}
                                 ]}
                                 */
                /*,
                 {title:"Animations",
                 pageTemplate:[{html:"in progress..."}]},
                 {title:"Transistions",
                 pageTemplate:[{html:"in progress..."}]},
                 {title:"Transforms",
                 pageTemplate:[{html:"in progress..."}]},*/

            ];
        },
        buildRendering: function () {
            this.domNode = dojo.doc.createElement("div");
            dojo.addClass(this.domNode, 'propertiesContent');
            dojo.addClass(this.domNode, 'widget');
            var template = "";
            template += this._titleBarDiv;
            template += "<div class='propertiesToolBar' dojoType='davinci.ve.widgets.WidgetToolBar'></div>";
            template += "<div dojoType='davinci.ve.widgets.WidgetProperties'></div>";
            template += "<div class='propScrollableArea'>";
            template += "<table class='propRootDetailsContainer'>";
            template += "<tr>";
            template += "<td class='propPaletteRoot'>";
            //run through pageTemplate to insert localized strings
            var langObj = veNls;

            this.pageTemplate = this.getPageTemplate();

            for (var i = 0; i < this.pageTemplate.length; i++) {
                this.pageTemplate[i].title = langObj[this.pageTemplate[i].key] ? langObj[this.pageTemplate[i].key] : "Key not found";
                if (this.pageTemplate[i].pageTemplate) {
                    for (var j = 0; j < this.pageTemplate[i].pageTemplate.length; j++) {
                        if (this.pageTemplate[i].pageTemplate[j].key) {
                            this.pageTemplate[i].pageTemplate[j].display += langObj[this.pageTemplate[i].pageTemplate[j].key] ? langObj[this.pageTemplate[i].pageTemplate[j].key] : "Key not found";
                        }
                    }
                }
            }
            this.domNode.innerHTML = template;

            this.inherited(arguments);
        },
        _widgetValuesChanged: function (event) {
            var currentPropSection = this._currentPropSection;
            if (currentPropSection) {
                var found = false;
                for (var propSectionIndex = 0; propSectionIndex < this.pageTemplate.length; propSectionIndex++) {
                    if (this.pageTemplate[propSectionIndex].key == currentPropSection) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    var visibleCascade = this._getVisibleCascade(propSectionIndex);
                    for (var i = 0; i < visibleCascade.length; i++)
                        visibleCascade[i]._widgetValuesChanged(event);
                }
            }
        },
        _getVisibleCascade: function (targetIndex) {
            if (targetIndex)
                return this.pageTemplate[targetIndex]['cascade'];
            var visibleCascade = [];
            var currentPropSection = this._currentPropSection;
            if (currentPropSection) {
                for (var i = 0; i < this.pageTemplate.length; i++) {
                    if (this.pageTemplate[i].key == currentPropSection) {
                        visibleCascade = visibleCascade.concat(this.pageTemplate[i]['cascade']);
                        break;
                    }
                }
            }
            return visibleCascade;
        },
        _updatePaletteValues: function (widgets) {
            //debugger;
            if (!this._editor) {
                console.error('have no editor', widgets);
                return;
            }
            //debugger;

            if (!lang.isArray(widgets)) {
                widgets = [widgets];
            }


            var widget = widgets[0];
            /* What about state changes and undo/redo? wdr
             * if(this._widget == widget && this._subwidget==widget.subwidget)
             return false;
             */
            this._widget = widget;
            this._subwidget = widget && widget.subwidget;

            this.setReadOnly(!(this._widget || this._subwidget));
            var visibleCascade = this._getVisibleCascade();
            for (var i = 0; i < visibleCascade.length; i++) {
                visibleCascade[i]._widgetSelectionChanged(widgets);
            }

        },
        _widgetSelectionChanged: function (changeEvent) {
            this._updatePaletteValues(changeEvent);
        },
        _stateChanged: function () {
            var widgets = this._widget ? [this._widget] : [];
            this._updatePaletteValues(widgets);
        },
        _widgetPropertiesChanged: function (widgets) {
            /* Check to see that this is for the same widget
             * Some widget like Tree update the DataStore but not the widget it's self from smar input
             * Cant check the id, it may change.
             */
            if (!this._widget) {
                console.error('have no widget');
            }
            if ((!this._widget) || (this._widget.type === widgets[0].type)) {
                this._updatePaletteValues(widgets);
            }
        },
        _titlePaneOpen: function (index) {

            var visibleCascade = this._getVisibleCascade(index);
            for (var i = 0; i < visibleCascade.length; i++) {
                visibleCascade[i]._editorSelected({
                    'editor': this._editor
                });
                //visibleCascade[i]._widgetSelectionChanged([this._widget]);

            }

        },
        startup: function () {

            this.domNode.style.height = "100%";
            /*this._editor = Runtime.currentEditor;*/

            this.inherited(arguments);

            //FIXME: Do we need this?
            // Install any onchange handlers for specific input elements
            for (var i = 0; i < this.pageTemplate.length; i++) {
                var section = this.pageTemplate[i];
                if (section.pageTemplate) {
                    for (var j = 0; j < section.pageTemplate.length; j++) {
                        var propData = section.pageTemplate[j];
                        if (propData.onchange && propData.id) {
                            // onchange function gets invoked with "this" pointing to pageTemplate[i]
                            // and receives parameter j
                            dojo.connect(dojo.byId(propData.id), "onchange", dojo.hitch(section, propData.onchange, j));
                        }
                    }
                }
            }
            //FIXME: Do we need this?
            for (var v = 0; v < this.pageTemplate.length; v++) {
                this.pageTemplate[v]['cascade'] = [];
            }
            this.setReadOnly(true);
            this.onEditorSelected();
            this.subscribe("/davinci/ui/widgetValuesChanged", dojo.hitch(this, this._widgetValuesChanged));
            this.subscribe("/davinci/ui/widgetPropertiesChanged", dojo.hitch(this, this._widgetPropertiesChanged));
            //Don't need to subscribe here. ViewLite already does it for us.
            this.subscribe("/davinci/ui/widgetSelected", dojo.hitch(this, this._widgetSelectionChanged));
        },
        setReadOnly: function (isReadOnly) {
            for (var v = 0; v < this.pageTemplate.length; v++) {
                var page = this.pageTemplate[v]['pageTemplate'];
                if (!page)
                    continue;
                for (var i = 0; i < page.length; i++) {
                    var widget = page[i]['widget'];
                    if (widget)
                        widget.set("readOnly", isReadOnly);
                    else {
                        var node = page[i].domNode;
                        if (node)
                            dojo.attr(node, "disabled", isReadOnly);
                    }
                }
            }
        },
        _modelEntryById: function (id) {
            for (var v = 0; v < this.pageTemplate.length; v++) {
                var page = this.pageTemplate[v]['pageTemplate'];
                if (!page)
                    continue;
                for (var i = 0; i < page.length; i++) {
                    var sectionId = page[i]['id'];
                    if (id == sectionId) {
                        return page[i];
                    }
                }
            }
        },
        _editorSelected: function (editorChange) {

            this._editor = editorChange.editor;
            this.onEditorSelected(this._editor);

            var parentTabContainer = this.getParent();
            // 0 && console.log('_editor selected',parentTabContainer);
            var selectedChild = parentTabContainer.selectedChildWidget;
            var updatedSelectedChild = false;
            var allSections = dojo.query('.maqPropertySection', parentTabContainer.domNode);
            if (this._editor) {
                if (this._editor.declaredClass != 'davinci.ve.PageEditor' &&
                    this._editor.declaredClass != 'davinci.ve.themeEditor.ThemeEditor') {
                    //Hide all sections
                    allSections.forEach(function (section) {
                        var contentPane = dijit.byNode(section);
                        if (contentPane.controlButton) {
                            contentPane.controlButton.domNode.style.display = 'none';
                        }
                        if (contentPane == selectedChild) {
                            updatedSelectedChild = true;
                        }
                    });
                } else {
                    //Show all sections
                    allSections.forEach(function (section) {
                        var contentPane = dijit.byNode(section);
                        if (contentPane.controlButton) {
                            contentPane.controlButton.domNode.style.display = '';
                        }
                    });
                    if (this._editor.declaredClass == 'davinci.ve.themeEditor.ThemeEditor') {
                        //Hide sections intended only for page editor
                        var pageEditorOnlySections = dojo.query('.page_editor_only', parentTabContainer.domNode);
                        pageEditorOnlySections.forEach(function (section) {
                            var contentPane = dijit.byNode(section);
                            if (contentPane.controlButton) {
                                contentPane.controlButton.domNode.style.display = 'none';
                            } else {

                            }
                            if (contentPane == selectedChild) {
                                updatedSelectedChild = true;
                            }
                        });
                    }
                }
            } else {
                //No editor, so bring back to default state by showing all sections
                allSections.forEach(function (section) {
                    var contentPane = dijit.byNode(section);
                    contentPane.controlButton.domNode.style.display = '';
                });
                updatedSelectedChild = true;
            }
            if (updatedSelectedChild) {
                this._selectFirstVisibleTab();
            }
        },
        onEditorSelected: function () {
            //we should clear selected widget from the old editor
            this._widget = null;
            this._subWidget = null;

            //this._updateToolBars();

            /* add the editors ID to the top of the properties pallete so you can target CSS rules based on editor */
            if (this._oldClassName) {
                if (this.domNode) {
                    dojo.removeClass(this.domNode.parentNode.parentNode, this._oldClassName); //remove the class from the  tab container
                } else {
                    console.error('have no dom node !');
                }
            }

            if (!this._editor) {
                return;
            }
            if (this._editor && this._editor.editorID) {
                this._oldClassName = this._editor.editorID.replace(/\./g, "_");
                if (this.domNode) {
                    dojo.addClass(this.domNode.parentNode.parentNode, this._oldClassName); //put the class on the  tab container
                } else {
                    console.error('have no dom node !');
                }
            }

            if (!this.domNode) {
                console.error('have no dom node !');
                return;
            }

            //FIXME: I'm pretty sure at least some of the code below is no longer necessary
            // Hide or show the various section buttons on the root pane
            var currentPropSection = this._currentPropSection;
            var sectionButtons = dojo.query(".propSectionButton", this.domNode);
            for (var i = 0; i < sectionButtons.length; i++) {
                var sectionButton = sectionButtons[i];
                if (this._editor && this._editor.supports && this._editor.supports('propsect_' + this.pageTemplate[i].key)) {
                    dojo.removeClass(sectionButton, 'dijitHidden');
                } else {
                    dojo.addClass(sectionButton, 'dijitHidden');
                    if (currentPropSection == this.pageTemplate[i].key) {
                        // If a hidden section is currently showing, then
                        // jump to Property palette's root view.
                        HTMLStringUtil.showRoot();
                    }
                }
            }
            //ENDOF FIXME COMMENT

            var visibleCascade = [];
            for (var i = 0; i < this.pageTemplate.length; i++) {
                var cascade = this.pageTemplate[i]['cascade'];
                if (cascade) {
                    visibleCascade = visibleCascade.concat(cascade);
                } else {
                     0 && console.log("no cascade found");
                }
            }
            for (var i = 0; i < visibleCascade.length; i++) {
                var cascade = visibleCascade[i];
                if (cascade._editorSelected) {
                    cascade._editorSelected({
                        'editor': this._editor
                    });
                }
            }


        },
        destroy: function () {
            this.inherited(arguments);
        },
        _destroyContent: function () {
            var containerNode = (this.containerNode || this.domNode);
            dojo.forEach(dojo.query("[widgetId]", containerNode).map(dijit.byNode), function (w) {
                w.destroy();
            });
            while (containerNode.firstChild) {
                dojo._destroyElement(containerNode.firstChild);
            }
            dojo.forEach(this._tooltips, function (t) {
                t.destroy();
            });
            this._tooltips = undefined;
        },
        sectionTitleFromKey: function (key) {
            for (var i = 0; i < this.pageTemplate.length; i++) {
                if (this.pageTemplate[i].key == key) {
                    return this.pageTemplate[i].title;
                }
            }
        },
        _initialPerspectiveReady: function () {

             0 && console.log('_initialPerspectiveReady');

            /*
             var parentTabContainer = this.getParent();
             var tabs = this._tabContainer.getChildren();
             for(var i=0; i<tabs.length; i++){
             var tab = tabs[i];
             this._tabContainer.removeChild(tab);
             parentTabContainer.addChild(tab);
             }
             parentTabContainer.removeChild(this);
             dojo.addClass(parentTabContainer.domNode, 'propRootDetailsContainer');
             dojo.addClass(parentTabContainer.domNode, 'propertiesContent');
             */
            if (!this._alreadySplitIntoMultipleTabs) {

                //var parentTabContainer = this.getParent();

                var parentTabContainer = this.tabContainer;

                dojo.addClass(parentTabContainer.domNode, 'propRootDetailsContainer');
                dojo.addClass(parentTabContainer.domNode, 'propertiesContent');

                for (var i = 0; i < this.pageTemplate.length; i++) {
                    var key = this.pageTemplate[i].key;
                    var title = this.pageTemplate[i].title;
                    var className = this.pageTemplate[i].className;
                    if (!className) {
                        className = '';
                    }
                    var topContent = this._titleBarDiv;
                    topContent += "<div class='propertiesToolBar' dojoType='davinci.ve.widgets.WidgetToolBar'></div>";
                    topContent += "<div class='cascadeBackButtonDiv'><button onclick='davinci.ve.widgets.HTMLStringUtil.showSection(\"" + key + "\",\"" + title + "\")'>" + title + " " + veNls.properties + "</button></div>";
                    var paneContent = HTMLStringUtil.generateTemplate(this.pageTemplate[i]);
                    var content = topContent + paneContent;
                    if (i == 0) {
                        cp = this;
                        cp.set('title', title);
                        dojo.addClass(cp.domNode, className);                        
                    } else {
                        var contentPane = parentTabContainer.createTab(title, null, null, null, {
                            content: content,
                            'class': className,
                            delegate: this
                        });
                        var cp = contentPane.add(ContentPane, {
                            title: title,
                            content: content,
                            'class': className,
                            delegate: this
                        }, null, true);
                    }
                    cp._maqPropGroup = this.pageTemplate[i].key;
                    //FIXME: temp hack
                    var closeBoxNodes = dojo.query('.paletteCloseBox', cp.domNode);
                    if (closeBoxNodes.length > 0) {
                        var closeBox = closeBoxNodes[0];
                        dojo.connect(closeBox, 'click', this, function (event) {
                            davinci.Workbench.collapsePaletteContainer(event.currentTarget);
                        });
                    }
                }

                for (var v = 0; v < this.pageTemplate.length; v++) {
                    this.pageTemplate[v]['cascade'] = [];
                    var page = this.pageTemplate[v]['pageTemplate'];
                    if (!page)
                        continue;
                    for (var i = 0; i < page.length; i++) {
                        var id = page[i]['id'];
                        var widget = dijit.byId(id);
                        if (widget) {
                            page[i]['widget'] = widget;
                        } else {
                            widget = dojo.byId(id);
                            if (widget)
                                page[i]['domNode'] = widget;
                        }
                    }
                    var sectionId = this.pageTemplate[v].id;
                    var nodeList = dojo.query("#" + sectionId + " .CascadeTop");
                    nodeList.forEach(function (target) {
                        return function (p) {
                            var cascade = dijit.byId(p.id);
                            target['cascade'].push(cascade);
                        };
                    }(this.pageTemplate[v]));
                }

                dojo.connect(parentTabContainer, 'selectChild', this, function (tab) {
                    // If the currently selected tab is invisible, then switch to the first
                    // visible tab, which will trigger yet another call to this same callback
                    if (tab.controlButton.domNode.style.display == 'none') {
                        if (!this._recursiveSelectChildInProcess) {
                            this._recursiveSelectChildInProcess = true;
                            this._selectFirstVisibleTab();
                            delete this._recursiveSelectChildInProcess;
                            return;
                        }
                    }
                    if (tab._maqPropGroup) {
                        this._currentPropSection = tab._maqPropGroup;
                        var context = (this._editor && this._editor.getContext) ? this._editor.getContext() : null;
                        var selection = (context && context.getSelection) ? context.getSelection() : this._widgets;
                        this._updatePaletteValues(selection);
                        HTMLStringUtil._initSection(this._currentPropSection);
                    }
                });
                this._alreadySplitIntoMultipleTabs = true;
            }

        },
        _workbenchReady: function () {
            this._updateToolBars();
        },

        _updateToolBars: function () {
            var propertyToolbarContainers = dojo.query('.propertiesToolBar');
            propertyToolbarContainers.forEach(function (container) {
                if (this._editor && this._editor.declaredClass == 'davinci.ve.PageEditor') {
                    dojo.removeClass(container, "dijitHidden");
                    container.style.display = '';
                } else {
                    dojo.addClass(container, 'dijitHidden');
                }
            }.bind(this));
        },

        _selectFirstVisibleTab: function () {
            var parentTabContainer = this.getParent();
            var children = parentTabContainer.getChildren();
            for (var i = 0; i < children.length; i++) {
                var cp = children[i];
                if (cp.controlButton.domNode.style.display != 'none') {
                    // This flag prevents Workbench.js logic from triggering expand/collapse
                    // logic based on selectChild() event
                    parentTabContainer._maqDontExpandCollapse = true;
                    parentTabContainer.selectChild(cp);
                    delete parentTabContainer._maqDontExpandCollapse;
                    break;
                }
            }
        }
    });
});
},
'xideve/widgets/WidgetLite':function(){
define([
	"dojo/_base/declare",
    "dijit/_WidgetBase",
	"xide/mixins/EventedMixin"
], function(declare, WidgetBase, EventedMixin) {

return declare("xideve/widgets/WidgetLite", [WidgetBase,EventedMixin], {
	
	/* super lite weight templated widget constructed programmatically */
	
	buildRendering: function(){
		this.inherited(arguments);

		
		if(/dojotype/i.test(this.domNode.innerHTML || "")){
			// Store widgets that we need to start at a later point in time
			this._startupWidgets = dojo.parser.parse(this.domNode, {
				noStart: !this._earlyTemplatedStartup,
				inherited: {dir: this.dir, lang: this.lang}
			});
		}
	},

	_destroyContent: function(){
		
		var containerNode = (this.containerNode || this.domNode);
		dojo.forEach(dojo.query("[widgetId]", containerNode).map(dijit.byNode), function(w){
			w.destroy();
		});
		while(containerNode.firstChild){
			dojo._destroyElement(containerNode.firstChild);
		}
		dojo.forEach(this._tooltips, function(t){
			t.destroy();
		});
		delete this._tooltips;
	},

	startup: function(){
		dojo.forEach(this._startupWidgets, function(w){
			if(w && !w._started && w.startup){
				w.startup();
			}
		});
		this.inherited(arguments);
	}
});
});

},
'xideve/manager/WidgetManager':function(){
/** @module xideve/manager/WidgetManager **/
define([
    'dcl/dcl',
    "dojo/_base/lang",
    'dojo/dom-construct',
    "xide/manager/ManagerBase",
    "xide/mixins/ReloadMixin",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xide/layout/ContentPane',
    'xide/widgets/_MenuMixin2',
    'xaction/Action',
    './WidgetManagerBlox',
    './WidgetManagerJS',
    'xide/mixins/VariableMixin',
    "davinci/ve/metadata",
    "xdojo/has!xtrack?xide/interface/Track",
    "xide/interface/Route",
    "xide/noob",
    'xide/data/Reference',
    'xide/data/ReferenceMapping',
    "xide/mixins/ActionProvider"
], function (dcl, lang, domConstruct, ManagerBase,
    ReloadMixin, types, utils, factory,
    ContentPane, _MenuMixin,
    Action,
    WidgetManagerBlox,
    WidgetManagerJS, VariableMixin,
    Metadata, Track, Route, noob, Reference,
    ReferenceMapping, ActionProvider) {

    const contextMenu = false;
    const extendProperties = false;
    const REFERENCE_CLASS = dcl([Reference, ReferenceMapping], {});
    let TRACKING_IMPL = null;
    if (Track) {
        TRACKING_IMPL = {
            track: function () {
                return true;
            },
            getTrackingCategory: function () {
                return utils.capitalize(this.beanNamespace);
            },
            getTrackingEvent: function () {
                return types.ACTION.OPEN;
            },
            getTrackingLabel: function (item) {
                return this.getMetaValue(item, types.DRIVER_PROPERTY.CF_DRIVER_NAME);
            },
            getActionTrackingUrl: function (command) {
                return lang.replace(
                    this.breanScheme + '?action={action}&' + this.beanUrlPattern, {
                        //id: item.id,
                        action: command
                    });
            },
            getTrackingUrl: function (item) {
                return lang.replace(
                    this.breanScheme + '{view}/' + this.beanUrlPattern, {
                        id: item.id,
                        view: 'settings'
                    });
            }
        };
    }

    /**
     * Extend standard action visibility for widget properties
     */
    utils.mixin(types.ACTION_VISIBILITY, {
        /**
         * Action visibility 'WIDGET_PROPERTY'
         * @memberOf module:xide/types/ACTION_VISIBILITY
         */
        WIDGET_PROPERTY: 'WIDGET_PROPERTY'
    });

    const _foo = null,
        _nativeEvents = {
            "onclick": _foo,
            "ondblclick": _foo,
            "onmousedown": _foo,
            "onmouseup": _foo,
            "onmouseover": _foo,
            "onmousemove": _foo,
            "onmouseout": _foo,
            "onkeypress": _foo,
            "onkeydown": _foo,
            "onkeyup": _foo,
            "onfocus": _foo,
            "onblur": _foo,
            "onchange": _foo
        };
    /**
     * Handler for davinci context and visual editor events. Currently just a stub. Concrete impl. happens in app subclasses.
     *
     * @class module:xide/manager/WidgetManager
     *
     * @augments  module:xide/manager/ManagerBase
     * @augments  module:xide/widgets/_MenuMixin
     * @augments  module:xide/mixins/ActionMixin
     */
    //ActionMixin
    return dcl([Route.dcl, ActionProvider.dcl, ManagerBase, ReloadMixin.dcl, WidgetManagerBlox, WidgetManagerJS, _MenuMixin, VariableMixin.dcl, Track ? dcl(Track.dcl, TRACKING_IMPL) : noob.dcl], {
        declaredClass: "xideve/manager/WidgetManager",
        /**
         * currentContext holds the current davinci.ve.Context instance
         */
        currentContext: null,

        _currentWidget: null,
        /**
         * This class has actions for this visibility:
         */
        visibility: types.ACTION_VISIBILITY.WIDGET_PROPERTY,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onReloaded: function () {},

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Event callbacks
        //
        /////////////////////////////////////////////////////////////////////////////////////

        /**
         * @TODO: Currently wrong place to dance to create block palettes
         *
         *
         * Event when an editor create a 'library container' for palette, outline etc...
         *
         * This will add shared blocks 'xblox' into an additional palette, so a user
         * just drop in his 'scripts' onto widgets
         *
         * The current procedure is:
         *
         * 1. let ContextManager load all the scene's blocks
         * 2. publish 'createBlockPalette', wait for consumers to fill it up with items
         * 3. when any consumer has blocks for this editor, then actually create the palette.
         *
         *
         *
         * @param evt {Object} event data
         * @param evt.item {File} the file object of the editor
         * @param evt.container {LayoutContainer} the container created (Accordion, Tab,..)
         * @param evt.editor {VisualEditor} the editor
         * @param evt.context {xide/manager/Context} the root context of the editor
         */
        onLibraryContainerCreated: function (evt) {},
        /**
         * Event when an editor context and its containing application has been fully
         * loaded.
         *
         * @param evt {Object} event data
         * @param evt.context {davinci/ve/Context} the davinci context
         * @param evt.appContext {xapp/manager/Context} the xapp context
         * @param evt.settings {Object} the application's settings
         * @param evt.ctx {xide/manager/Context} the root context (IDE context)
         * @param evt.global {Object} the loaded document's global
         * @param evt.blockScopes {xblox/model/Scope[]} the loaded xblox scopes
         *
         */
        onContextReady: function (evt) {

        },
        _completeEventInfo: function (params) {


            const thiz = this;
            //set to context to the selected widget
            const expressionContext = params.target || null;

            //get davinci context
            const widgetContext = params.widget ? params.target.getContext() : null;



            if (!widgetContext.editor) {
                params.editor = widgetContext._edit_context.editor;
            } else {
                params.editor = widgetContext.editor;
            }

            //get global
            const global = widgetContext ? widgetContext.getGlobal() : null;

            //get app context
            const appContext = widgetContext.appContext;

            //the actual application
            const app = appContext.application;

            let node = expressionContext.domNode || expressionContext;
            if (expressionContext.isDijitWidget) {
                node = expressionContext.dijitWidget.domNode;
            }



            params.appSettings = app.settings; //the applications settings
            params.appContext = appContext;
            params.app = app;
            params.global = global;
            params.node = node;
            params.widgetContext = expressionContext;
            params.dijitWidget = expressionContext.isDijitWidget ? expressionContext.dijitWidget : null;

            return params;


        },
        onWidgetPropertyChanged: function (evt) {

            const params = this._completeEventInfo(evt);

            // 0 && console.log('widget changed ' , params);

            const prop = params.widgetProperty,
                node = params.node,
                widget = params.dijitWidget,
                _isNative = prop in _nativeEvents,
                global = params.global;


            if (prop && node) {


                if (_isNative && widget && widget.on) {

                    node.removeAttribute(prop);

                    if (node[prop]) {
                        node.removeEventListener(prop, node[prop]);
                    }
                    node[prop] = null; //

                    global.dojo.setAttr(params.node, params.widgetProperty, '');

                    global.dojo.setAttr(params.node, params.widgetProperty, params.value);

                    /*
                    if(widget['__' + prop]){
                        widget['__' + prop].remove();
                    }

                    var _evt = '' + prop.replace('on','');
                    widget['__' + prop] = widget.on(_evt,function(){
                        // 0 && console.log('asdfasdf');
                        global.eval(params.value,widget);

                    });
                    */
                }


                //params.node.removeEventListener(prop);

                /*
                node[params.widgetProperty] = null;

                params.node[prop] = function(){
                     0 && console.log('noob');
                };
                params.global.dojo.setAttr(params.node,params.widgetProperty,params.value);
                */
            }

        },
        /**
         * Callback when a widget property has been rendered. This is common entry only. It
         * @param evt
         */
        onWidgetPropertyRendered: function (evt) {

            //  0 && console.log('widget prop rendered',evt.row.type);

            if (!extendProperties) {
                //return;
            }
            //avoid multiple handling
            if (evt.wmHandled) {
                return;
            }

            evt.wmHandled = true;

            const prop = evt['row'];

            //react per widget property type
            switch (prop.type) {
                /*
                case 'file':
                {
                    this.renderFileWidget(prop, evt['node'], evt['view']);
                    break;
                }

                case 'block':
                {
                    this.renderBlockWidget(prop, evt['node'], evt['view']);
                    break;
                }

                case 'number':
                {
                    this.onNumberPropertyRendered(prop, evt['node'], evt['view'],evt['row']);
                    break;
                }*/
                case 'widgetState':
                    {
                        this.onStatePropertyRendered(prop, evt['node'], evt['view'], evt['row']);
                        break;
                    }
                case 'iconClass':
                    {
                        this.onIconClassPropertyRendered(prop, evt['node'], evt['view'], evt['row']);
                        break;
                    }
                case 'state':
                    {
                        this.onEventPropertyRendered2(prop, evt['node'], evt['view'], evt['row']);
                        break;
                    }
                default:
                    {}
            }
        },
        onEventPropertyRendered2: function () {
            //  0 && console.log('onEventPropertyRendered', arguments);
        },

        onIconClassPropertyRendered: function (prop, node, view, row) {
            //  0 && console.log('on onIconClassPropertyRendered');
            const where = node;
            if (!node.iconPicker) {
                const iconPicker = $(node).iconpicker({
                    selectedCustomClass: 'label label-success',
                    showFooter: false
                });

                node.iconPicker = iconPicker;
                iconPicker.on('iconpickerSelected', function (e) {
                    const value = e.iconpickerValue;
                    const valuesObject = {};
                    valuesObject['iconClass'] = 'fa ' + value;
                    const command = new davinci.ve.commands.ModifyCommand(iconPicker._widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: iconPicker._context.editor_id,
                        command: command
                    }]);
                });
                /*
                iconPicker.on('change', function () {
                    var value = selectize.getValue();
                    var valuesObject = {};
                    valuesObject['state'] = value;
                    var command = new davinci.ve.commands.ModifyCommand(selectize._widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: selectize._context.editor_id,
                        command: command
                    }]);
                });
                */

            }
            if (view._widget) {
                const widget = utils.getNode(view._widget);
                node.iconPicker._widget = view._widget;
                node.iconPicker._context = view.context;
            }
        },
        onStatePropertyRendered: function (prop, node, view, row) {
            //  0 && console.log('onStatePropertyRendered');
            const where = node;
            if (!node._selectize) {
                const selectize = $(node).selectize({
                    value: "",
                    delimiter: ',',
                    persist: false,
                    maxItems: 1,
                    create: true,
                    sortField: 'text'
                }).data().selectize;

                node._selectize = selectize;
                selectize.on('change', () => {
                    let value = selectize.getValue();
                    if(value==='None'){
                        value = '';
                    }
                    const valuesObject = {};
                    valuesObject['state'] = value;
                    const command = new davinci.ve.commands.ModifyCommand(selectize._widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: selectize._context.editor_id,
                        command: command
                    }]);
                });

            }
            node._selectize.setValue(['None'], true);

            if (view._widget) {
                const widget = utils.getNode(view._widget);
                if (widget) {
                    if (widget.getStates) {
                        const states = widget.getStates();
                        _.each(states, (state) => {
                            node._selectize.addOption({
                                value: state.name,
                                text: state.name
                            });
                        });
                    }
                    node._selectize.addOption({
                        value: 'None',
                        text: 'None'
                    });
                    node._selectize.setValue(widget.state, true);
                    node._selectize._widget = view._widget;
                    node._selectize._context = view.context;
                }
            }
        },
        /**
         *
         * Default actions for widget/node property. This should
         * support :
         * - 'Bind to Javascript'  and a few variants:
         *  - 'Expression'
         *  - 'Bind to data or store' (bi-direction double xwire with possibly in/outbound filters)
         *  - 'XBlox'
         */
        defaultNumberActions: function (context) {

            const thiz = this;

            const _handlerJS = function (command, menu, owner, menuItem, action) {
                thiz.onJavascript(action.context);
            };

            const _js = Action.createDefault('Javascript', 'fa-code', 'Widget/Bind to Javascript', 'widgetPropertyAction', _handlerJS);
            _js.setVisibility(this.visibility, {});
            _js.context = context;



            const _handlerBlox = function (command, menu, owner, menuItem, action) {
                thiz.onXBlox(action.context);
            };

            const _blox = Action.createDefault('XBlox', 'fa-play-circle-o', 'Widget/Bind to XBlox', 'widgetPropertyAction', _handlerBlox);
            _blox.setVisibility(types.ACTION_VISIBILITY.WIDGET_PROPERTY, {});
            _blox.context = context;


            const _actions = [
                _js,
                _blox
            ];

            return _actions;
        },
        /**
         * 
         * @param menu
         * @param prop
         * @param view
         * @param row
         * @private
         */
        _showContextActions: function (menu, prop, view, row) {

            if (!contextMenu) {
                return;
            }

            if (!menu.actions) {

                const actions = this.defaultNumberActions({
                    prop: prop,
                    view: view,
                    widget: view._widget, //current selection
                    row: row
                });

                //tell every body, xblox and others might add some more actions
                this.publish(types.EVENTS.ON_SET_WIDGET_PROPERTY_ACTIONS, {
                    actions: actions,
                    property: prop,
                    view: view,
                    menu: menu
                });

                this._renderActions(actions, menu, null);

                menu.actions = actions;
            }
        },
        /**
         * Create context menu on the widget property wizard button. Render
         * the actions onOpen.

         * @param widget
         * @param prop
         * @param view
         * @private
         */
        _createContextMenu: function (widget, prop, view, row) {

            if (!contextMenu) {
                return;
            }

            //skip if we've got a menu already
            if (widget._ctxMenu) {
                return;
            }
            //custom open, forward
            const _openFn = function (menu) {
                this._showContextActions(menu, prop, view, row);
            }.bind(this);

            //done in _MenuMixin
            widget._ctxMenu = this.createContextMenu(widget, _openFn, {
                leftClickToOpen: true
            });

            widget.row = row;


            return widget._ctxMenu;

        },
        /**
         *
         * @param node
         * @param prop
         * @param view
         * @param _button
         */
        doWidgetPropWizard: function (node, prop, view, _button, row) {
            this._createContextMenu(_button, prop, view, row);
        },
        /**
         *
         * @param prop
         * @param node
         * @param view
         */
        renderBlockWidget: function (prop, node, view) {
            //runBlox('workspace://min.xblox','e813a85f-7511-723f-bc1f-c13698456a5b',this)
             0 && console.log('block rendered', arguments);
            //x-script : view._widget.domNode
        },
        /**
         * Callback when a 'number' property has been rendered. This adds a 'wizard' button
         * right side of the widget property value field.
         *
         * @param prop
         * @param node
         * @param view
         */
        onNumberPropertyRendered: function (prop, node, view, row) {

            if (!extendProperties) {
                return;
            }
            let where = node;

            if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                where = node.parentNode.nextSibling;
            }

            //var _button = factory.createButton(where, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);

            const _button = factory.createSimpleButton('', 'fa-magic', null, {
                style: "margin-left:11px !important; border-radius:2px;"
            }, where);

            this.doWidgetPropWizard(node, prop, view, _button, row);
        },
        /**
         * Callback when a ui-designer's context has been fully loaded. The context is tracked.
         * This is currently loading additional modules!
         * @param context
         */
        onContextLoaded: function (context) {

            this.currentContext = context;
        },
        /**
         * Callback when a 'number' property has been rendered. This adds a 'wizard' button
         * right side of the widget property value field.
         *
         * @param prop
         * @param node
         * @param view
         */
        onEventPropertyRendered: function (prop, node, view) {
            if (!extendProperties) {
                return;
            }
            let where = node;

            if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                where = node.parentNode.nextSibling;
            }

            //var _button = factory.createButton(where, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);
            const _button = factory.createSimpleButton('', 'fa-magic', null, {
                style: "margin-left:11px !important; border-radius:2px;"
            }, where);

            this.doWidgetPropWizard(node, prop, view, _button);
        },
        /**
         * Event when the event property section is being rendered,
         * fish out
         * @param evt
         */

        onEventSelectionRendered: function (evt) {

            if (!extendProperties) {
                return;
            }
             0 && console.log('onEventSelectionRendered : ', evt);
            const pageTemplate = evt.pageTemplate;

            for (let i = 0; i < pageTemplate.length; i++) {

                const page = pageTemplate[i];
                const propNode = utils.find('.propertyExtra', dojo.byId(page.rowId), true);

                //var _button = factory.createButton(propNode, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);
                const _button = factory.createSimpleButton('', 'fa-magic', null, {
                    style: "margin-left:11px !important; border-radius:2px;",
                    id: 'btn_sel' + utils.createUUID()
                }, propNode);


                this.doWidgetPropWizard(null, null, evt.eventSelection, _button, page);

            }

        },

        /**
         * Callback when user selected another widget.
         * @param evt {Array} the current ui-designer selection.
         */
        onSelectionChanged: function (evt) {

             0 && console.log('selection changed');
        },

        /**
         * Callback when user selected a widget
         * @param evt {Array}
         */
        onWidgetSelected: function (evt) {

            if (!extendProperties) {
                return;
            }
             0 && console.log('widget selected', evt);

            if (evt.isFake === true) {
                return;
            }

            this._currentWidget = evt.selection;

            const mainView = this.ctx.getApplication().mainView;

            const selection = evt.selection;



            //update description in main view
            // when a widget is selected, pick the description from meta data and place it the right - bottom panel
            const layoutRightMain = mainView.getLayoutRightMain ? mainView.getLayoutRightMain() : null;
            if (evt && lang.isArray(selection) && selection.length == 1) {

                const widgetProps = selection[0];

                let _description = Metadata.getOamDescriptivePropertyForType(widgetProps.type, 'description');
                if (!_description) {
                    //Metadata.getWidgetDescriptorForType(widgetProps.type) ||

                    _description = Metadata.getOamDescriptivePropertyForType(widgetProps.type, 'title');
                    if (_description && _description.value) {
                        _description = _description.value;
                    }
                } else {

                    _description = _description.value;

                }

                if (!_description && widgetProps.metadata && widgetProps.metadata.description && widgetProps.metadata.description.value) {
                    _description = widgetProps.metadata.description.value;
                }

                //render description on info panel
                if (_description) {
                    if (mainView) {
                        //this should return a tab-container
                        if (mainView.getRightBottomTarget) {
                            const parent = mainView.getRightBottomTarget(true, this._doOpenRight);
                            if (parent) {
                                if (this._doOpenRight) {
                                    this._doOpenRight = false;
                                }
                                const _pane = utils.addWidget(ContentPane, {
                                    delegate: this,
                                    style: "width:100%;height:100%;padding:0px",
                                    parentContainer: parent,
                                    title: 'Description',
                                    closable: true,
                                    splitter: true
                                }, this, parent, true);
                                _pane.containerNode.innerHTML = _description;
                            }
                        }
                    }
                }

                //now publish on event bus
                selection[0].beanType = 'widget';

                this.publish(types.EVENTS.ON_ITEM_SELECTED, {
                    item: selection[0],
                    beanType: 'widget',
                    view: evt.context.editor
                }, evt.context.editor); //re-root owner of the publisher to the current editor




            } else {
                mainView.getRightBottomTarget && mainView.getRightBottomTarget(true, false);
            }

            const editor = evt.context.getVisualEditor();
            editor._emit('selectionChanged', {
                selection: selection
            });

            layoutRightMain && layoutRightMain.resize();


        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX factory and utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Renders a file picker button at a given node
         *
         * @param prop {Array} : property data
         * @param node {HTMLElement}
         * @param view {davinci/ve/views/SwitchingStyleView}
         */
        renderFileWidget: function (prop, node, view) {
            let where = node;
            if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                where = node.parentNode.nextSibling; //should be the
            }

            const selectButton = factory.createButton(domConstruct.create('div'), "el-icon-folder-open", "elusiveButton", "", "", function () {
                this.onSelectFile(node, prop, view);
            }.bind(this), this);

            dojo.place(selectButton.domNode, where, 'after');

        },

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Standard XIDE context calls
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Init is being called upon {link:xide/manager/Context}::initManagers()
         *
         */
        init: function () {
            this.subscribe("/davinci/ui/onSelectionChanged", this.onSelectionChanged);
            this.subscribe("/davinci/ui/widgetSelected", this.onWidgetSelected);
            this.subscribe("/davinci/ui/context/loaded", this.onContextLoaded);
            this.subscribe([
                types.EVENTS.WIDGET_PROPERTY_RENDERED, //extend style view additional actions per widget/node properties
                types.EVENTS.ON_EVENT_SELECTION_RENDERED,
                types.EVENTS.ON_WIDGET_PROPERTY_CHANGED,
                types.EVENTS.ON_LIBRARY_CONTAINER_CREATED,
                types.EVENTS.ON_CONTEXT_READY,
                types.EVENTS.ON_RENDER_WELCOME_GRID_GROUP
            ]);
            this.registerRoutes();
        },

        //
        // ─── XTRACK ──────────────────────────────────────────────────────
        //

        /**
         * Impl. router interface
         * @param {module:xide/interface/RouteDefinition} route
         */
        handleRoute: function (route) {
            switch (route.parameters.view) {
                case 'settings':
                    {
                        return this.openItemSettings(this.getDriverById(route.parameters.id));
                    }
            }
        },
        registerRoutes: function () {

            this.getContext().registerRoute({
                id: 'Scene',
                path: 'scene://{view}/{id}',
                schemes: ['scenes'],
                delegate: this
            });

            this.getContext().registerRoute({
                id: 'New Scene',
                path: 'File/New Scene',
                schemes: [''],
                delegate: this
            });
        },
        /**
         * @param evt {object}
         * @param evt.item {object}
         * @param evt.items {array|null}
         * @param evt.store {module:xide/data/_Base}
         */
        onRenderWelcomeGridGroup: function (evt) {
            const ctx = this.getContext();
            const trackingManager = ctx.getTrackingManager();
            const router = ctx.getRouter();
            const store = evt.store;
            const self = this;
            const grid = evt.grid;
            switch (evt.item.group) {
                case 'New':
                    {
                        const actions = [];
                        actions.push(this.createAction({
                            label: 'New Scene',
                            command: 'File/New Scene',
                            icon: 'fa-magic',
                            mixin: {
                                addPermission: true
                            }
                        }));

                        _.map(actions, function (item) {
                            return store.putSync(utils.mixin(item, {
                                url: item.command,
                                parent: 'New',
                                group: null,
                                label: item.title,
                                owner: self,
                                runAction: function (action, item, from) {
                                    return DriverTreeView.prototype.runAction.apply(DriverTreeView.prototype, [{
                                        command: this.url
                                    }, item, from]);
                                }
                            }));
                        });
                        break;
                    }
                case 'Recent':
                    {
                        if (!trackingManager) {
                            break;
                        }
                        //head
                        store.putSync({
                            group: 'Scenes',
                            label: 'Scenes',
                            url: 'Scenes',
                            parent: 'Recent',
                            directory: true
                        });

                        //collect recent
                        let devices = trackingManager.filterMin({
                            category: 'Driver'
                        }, 'counter', 1); //the '2' means here that those items had to be opened at least 2 times

                        //sort along usage
                        devices = trackingManager.most(null, devices);
                        //make last as first
                        trackingManager.last(devices);

                        return;
/*
                        //map
                        devices = _.map(devices, function (item) {
                            item.parent = 'Recent';
                            item.icon = 'fa-exchange';
                            return item;
                        });

                        //@TODO: find a way to split impl. from interface
                        const deviceActions = _.map(DriverTreeView.getActions.apply(DriverTreeView.prototype, [$('<div/>')[0], DriverTreeView.DefaultPermissions,
                            grid
                        ]), function (action) {
                            action.custom = true;
                            return action;
                        });
                        let deviceStoreItem = null;
                        _.each(devices, function (device) {
                            const route = router.match({
                                path: device.url
                            });
                            if (!route) {
                                 0 && console.warn('cant get a router for ' + device.url);
                                return;
                            }
                            deviceStoreItem = self.routeToReference(route);
                            if (!deviceStoreItem) {
                                return;
                            }
                            store.removeSync(device.url, true);
                            let reference = new REFERENCE_CLASS({
                                url: device.url,
                                enabled: true,
                                label: device.label,
                                parent: 'Drivers',
                                _mayHaveChildren: false,
                                virtual: true,
                                icon: 'fa-exchange',
                                owner: self,
                                getActions: function () {
                                    return deviceActions;
                                },
                                runAction: function (action, item, from) {
                                    return DriverView2.prototype.runAction.apply(DriverView2.prototype, [action, item, from]);
                                }
                            });
                            reference = store.putSync(reference, false);
                            if (deviceStoreItem && deviceStoreItem.addReference) {
                                deviceStoreItem.addReference(reference, {
                                    properties: {
                                        "name": true,
                                        "value": true,
                                        "state": true
                                    },
                                    onDelete: true,
                                    onChange: true
                                }, true);
                            } else {
                                console.error('device store item has no addReference!', reference);
                            }
                        });
                        store.emit('added', {
                            target: deviceStoreItem
                        });
                        break;
                        */
                    }
            }
        },
        /**
         * @param {module:xide/interface/RouteDefinition} route
         */
        getRouteRenderParams: function (route) {
            return {
                icon: null
            };
        },
        routeToReference: function (route) {
            return this.getDriverById(route ? route.parameters.id : null);
        },
        /**
         *
         * @param item
         * @returns {*}
         */
        referenceToRoute: function (item) {
            return lang.replace(
                this.breanScheme + '{view}/' + this.beanUrlPattern, {
                    id: item.id,
                    view: 'settings'
                });
        }

    });
});
},
'xideve/manager/WidgetManagerBlox':function(){
/** @module xideve/manager/WidgetManager **/
define([
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xide/views/CIActionDialog'
], function (dcl,types, utils, CIActionDialog) {
    /**
     *
     *
     * @TODOs

     * 1. changes are not reflected in the event's combo box history
     *
     * 2. changes to nothing don't update the widget's dom node: done
     * 3. figure out what widgets do emit else, put that in the meta-database and feed the ve
     *
     * 4. embed the selected block in the ve designer's widget-property panel, just for better
     * tuning
     *
     * 5. support share titles for better readability
     * 6. when a block pool has been added to the scene, use a filter select for quicker navigation
     * 7. do the same directly on the combo-box
     *


     * 8. find a way to pass custom arguments to a blox: done
     * 9. build a better way to complete arguments for this process: done
     *
     * 10. dojo event handles are gone when parsed, how to get them back: done
     *
     * 11. support multiple actions per widget events
     *
     *
     *
     *
     * @mixin module:xide/manager/WidgetManagerBlox
     *
     * */

    return dcl(null, {
        declaredClass:"xideve/manager/WidgetManagerBlox",
        _removeEventHandlers: function (widget, type) {

        },
        _setEventHandler: function (widget, type, value) {

            if (widget._attachEvents) {

                //normalize to .on
                const _event = type.replace('on', '');

                //remove old handler, including those from the parser
                _.each(widget._attachEvents, function (cb) {
                    if (cb.type === _event) {
                        cb.remove();
                    }

                    widget._attachEvents.remove(cb);
                });

                //add a new one
                if (value) {
                    const _function = new Function("{" + value + "; }");
                    const _handle = widget.on(_event, function (e) {
                        _function.call(widget, e);
                    });
                    _handle.type = type;
                    widget._attachEvents.push(_handle);
                }
            }

        },
        onModifiedXBLOX: function (params) {

        },
        /**
         *
         * @param params
         */
        openBloxWizard: function (params) {


            const thiz = this;

            params = this._completeEventInfo(params);
            /***
             * There are 4 cases to taken into account when changing a event handler for a widget:
             *
             * 1. the widget has already an event handler, set by dojo.parser (from dijit/_AttachMixin)
             * 2. the widget has no event handler yet
             * 3. its not a widget, same applies here 1 & 2 except the connect setup differs!
             */

            //@TODO
            let bloxFile = 'min.xblox';
            let mount = 'workspace';

            if(params.appSettings && params.appSettings.xbloxScripts && params.appSettings.xbloxScripts.length>0){
                const scriptItem = params.appSettings.xbloxScripts[0];
                bloxFile = scriptItem.path.replace('./','');
                mount = scriptItem.mount;
            }

            let block = null;

            const updateElement = true;

            const complete = function (_block, _settings) {

                thiz.ctx.blockManager.ignoreItemSelection = false;
                const path = utils.buildPath(mount, bloxFile, false);
                if (block) {
                    let settingsOut = '';
                    if (_settings) {
                        settingsOut = ',' + _settings;
                    }
                    const value = "runBlox('" + path + "','" + block.id + "',this" + settingsOut + ")";
                    thiz._setEventHandler(params.dijitWidget || params.node, params.widgetProperty, value);
                    params.editor.setDirty(true);
                    params.widget.set('value', value);
                    thiz.onModifiedXBLOX(params, block);
                }
            };

            //get xblox scope via file
            this.ctx.getBlockManager().load(mount, bloxFile).then(function (scope) {

                thiz.ctx.blockManager.ignoreItemSelection = true;

                const _d = dojo.subscribe('onItemSelected', function (evt) {
                    block = evt.item;
                });

                const widgetValue = '' + params.value;
                const split = widgetValue.split(',');
                const blockId = utils.replaceAll("'", '', split[1]);
                const blockSettings = dojo.fromJson(split[3]);
                block = scope.getBlockById(blockId);

                const actionDialog = new CIActionDialog({
                    title: 'Select Block and override inputs',
                    style: 'width:800px;min-height:600px;',
                    resizeable: true,
                    delegate: {
                        onOk: function (dlg, data) {

                            const blockSettings = utils.getCIInputValueByName(data, 'Settings');
                            if (block) {
                                complete(block, blockSettings);
                                _d.remove();
                            }
                        }
                    },
                    cis: [
                        utils.createCI('Block', types.ECIType.BLOCK_REFERENCE, '', {
                            group: 'Block',
                            delegate: null,
                            showDialog: false,
                            scope: scope,
                            block: block
                        }),
                        utils.createCI('Settings', types.ECIType.BLOCK_SETTINGS, '', {
                            group: 'Settings',
                            scope: scope,
                            block: block,
                            settings: blockSettings
                        })
                    ]
                });
                actionDialog.show();

                /*

                 simple version but no settings involved;

                 var dialog = new CIActionDialog({
                 title: 'Select Block',
                 resizeable: true,
                 resizeToContent: false,
                 fitContent: true,
                 dialogClass: 'CIDialogRoot',
                 style: 'min-width:600px;min-height:500px',
                 delegate: {
                 onOk:function(){
                 if(block){
                 complete();
                 _d.remove();
                 }
                 }
                 }
                 });

                 dialog.show().then(function () {

                 var blockEditor = utils.addWidget(BlocksFileEditor, {
                 style: 'height:inherit;width:inherit;padding:0px;',
                 beanContextName: thiz.id
                 }, thiz, dialog, true);

                 blockEditor.initWithScope(scope);
                 });
                 */

            });

        },
        onXBlox: function (context) {

            let widget = context.row.widget;
            let val = '';//widget.get('value');
            if(!widget){
                widget = dijit.byId(context.row.id);
                if(widget){
                    val = widget.get('value');
                }
            }
            if(!widget){
                widget = dojo.byId(context.row.id);
                if(widget){
                    val = widget.value;
                }
            }

            // 0 && console.log('widget_value ' + val , context);
            //build complete info

            const props = {
                owner: this,
                delegate: this,
                widget: widget,
                value: val,
                target: context.widget,//actual scene widget
                widgetProperty: context.row.display,
                meta: {
                    context: context
                }
            };
            this.openBloxWizard(props);
        }
    });
});

},
'xideve/manager/WidgetManagerJS':function(){
/** @module xideve/manager/WidgetManager **/
define([
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xide/views/CIActionDialog'
], function (dcl,types, utils, CIActionDialog) {
    /**
     *
     * @mixin module:xide/manager/WidgetManagerBlox
     *
     * */
     return dcl(null,{
         declaredClass:"xideve/manager/WidgetManagerJS",
         onReloaded:function(){
             console.error('reloadex2:');
         },
         /**
          * time to update the HTML model
          * @param params
          */
         onModifiedJS:function(params){

         },
         _createView:function(value,params){

             const thiz = this;
             //set to context to the selected widget
             const expressionContext = params.target || null;

             //get davinci context
             const widgetContext = params.widget ? params.target.getContext() : null;

             //get global
             const global = widgetContext ? widgetContext.getGlobal() : null;

             /**
              * Create an expression delegate, this will run the code
              * directly on davinci's context. In case this part of native
              * event, the script gets the context of the widget, just like
              * in real.
              * @type {{runExpression: Function}}
              */
             const expressionDelegate = {

                 runExpression:function(value,run,error){
                     //var _eval = global['runx'] || global['eval'];
                     let _result = null;
                     if(global['runx']) {
                         try {
                             _result = global['runx'](expressionContext, value);
                         }catch(e){
                             if(error){
                                 error('' + e );
                                 return;
                             }
                         }
                     }else{
                         _result = global['eval'].call(expressionContext,value);
                     }
                     // 0 && console.log('run expression : ' + value + ' with result ' + _result);
                     if(run){
                         run('Expression ' + value + ' evaluates to; '   + _result);
                     }
                 }
             };
             /**
              * Procedure to open multiple values
              */
             const actionDialog = new CIActionDialog({
                 title: 'Javascript',
                 style: 'width:800px;min-height:600px;',
                 resizeable: true,
                 delegate: {
                     onOk: function (dlg, data) {
                         const _p = params;
                         const expression = utils.getCIInputValueByName(data, 'Expression');
                         widgetContext.editor.setDirty(true);
                         if(params.widget.set) {
                             params.widget.set('value', expression);
                         }else if(params.widget.value!=null){
                             params.widget.value = expression;
                             params.widget.owner._onChange({
                                 target:params.widget.pageIndex
                             })
                         }
                         thiz.onModifiedJS(expression,params);
                     }
                 },
                 cis: [

                     utils.createCI('Settings', types.ECIType.STRING, value, {
                         group: 'Settings'
                     }),

                     utils.createCI('Expression', types.ECIType.EXPRESSION_EDITOR, value, {
                         group: 'Script',
                         delegate:expressionDelegate
                     })

                 ]
             });


             actionDialog.show();

         },
         openJSWizard:function(params){
             this._createView(params.value,params);
         },
         onJavascript:function(context){
             let widget = context.row.widget;
             let val = '';//widget.get('value');
             if(!widget){
                 widget = dijit.byId(context.row.id);
                 if(widget){
                     val = widget.get('value');
                 }
             }
             if(!widget){
                 widget = dojo.byId(context.row.id);
                 if(widget){
                     val = widget.value;
                 }
             }

             //build complete info
             const props  = {
                 owner:this,
                 delegate:this,
                 widget:widget,//widget cascade
                 value:val,//the value
                 target:context.widget,//actual scene widget
                 meta:{
                     context:context
                 }
             };

             this.openJSWizard(props);
         }
     });
});

},
'xideve/manager/ContextManager':function(){
/** @module xideve/manager/WidgetManager **/
define([
    'dcl/dcl',
    'dojo/Deferred',
    'xide/manager/ManagerBase',
    'xide/mixins/VariableMixin',
    'xide/mixins/ReloadMixin',
    'xide/types',
    'xide/utils',
    'xide/mixins/EventedMixin',
    'davinci/ve/widget',
    "xide/manager/ServerActionBase",
    'xdojo/has',
    'xaction/ActionProvider',
    'xide/lodash',
    'xide/$'
], function (dcl, Deferred, ManagerBase, VariableMixin,
    ReloadMixin, types, utils, EventedMixin,
    Widget, ServerActionBase, has, ActionProvider, _, $) {


    const debugWiring = false;
    const debugBlox = false;
    const debugBoot = false;
    const debugDojo = false;
    const debugRun = false;
/*
    function popup(args) {
        if (window['lastArgs']) {
            return;
        }
        window['lastArgs'] = args;
        var partID = args.partID,
            domNode = args.domNode,
            context = args.context,
            widgetCallback = args.openCallback;
         0 && console.log('p2', args);
    }

    window['vePopup'] = popup;

    const toCommand = (action) => {
        switch (action) {
            case 'Undo':
                return 'Edit/Undo';
            case 'Redo':
                return 'Edit/Redo';
            case 'Cut':
                return 'Edit/Cut';
            case 'Copy':
                return 'Edit/Copy';
            case 'Paste':
                return 'Edit/Paste';
            case 'Delete':
                return 'Edit/Delete';
        }

    }
    const getIconClass = (iconClassIn) => {
        if (!iconClassIn) {
            return '';
        }
        switch (iconClassIn) {
            case "editActionIcon undoIcon":
                {
                    return 'text-info fa-undo';
                }
            case "editActionIcon redoIcon":
                {
                    return 'text-info fa-repeat';
                }

            case "editActionIcon editCutIcon":
                {
                    return 'text-warning fa-scissors';
                }
            case "editActionIcon editCopyIcon":
                {
                    return 'text-warning fa-copy';
                }
            case "editActionIcon editPasteIcon":
                {
                    return 'text-warning fa-paste';
                }
            case "editActionIcon editDeleteIcon":
                {
                    return 'text-danger fa-remove';
                }
        }
        return iconClassIn;
    }

    const toAction = (action, editorContext, who) => {
        var cmd = action.command || toCommand(action.label) || action.id;
        if (!cmd) {
            return null;
        }
        var _default = who.createAction({
            label: action.label,
            group: action.group || 'Ungrouped',
            icon: getIconClass(action.iconClass) || action.iconClass,
            command: cmd,
            tab: action.tab || 'Home',
            mixin: {
                action: action,
                addPermission: true,
                quick: true,
                actionId: action.id
            },
            shouldDisable: function () {
                if (editorContext && action.action && action.action.isEnabled) {
                    return !action.action.isEnabled(editorContext);
                }
                return false;
            }
        });

        switch (action.id) {
            case "documentSettings":
                {
                    return null;
                }
            case 'undo':
            case 'outline':
                {
                    _default.group = 'Widget';
                    return _default;
                }
            case 'redo':
                {
                    _default.group = 'Edit';
                    return _default;
                }
            case 'cut':
            case 'copy':
            case 'paste':
                {
                    _default.group = 'Edit';
                    return _default;
                }
            case 'delete':
                {
                    _default.group = 'Organize';
                    return _default;

                }
            case 'theme':
            case 'rotateDevice':
            case 'chooseDevice':
            case 'stickynote':
            case 'savecombo':
            case 'showWidgetsPalette':
            case 'layout':
            case 'sourcecombo':
            case 'design':
            case 'closeactiveeditor':
            case 'tableCommands':
                {
                    return null;
                }
        }
        debug &&  0 && console.log('unknown action : ' + action.id, action);
        return _default;
    }


    const toActions = (actions, editorContext, who) => {
        const result = [];
        const defaultMixin = {
            addPermission: true,
            quick: true
        };

        function completeAndAddAction(action) {
            var _action = toAction(action, editorContext, who);
            if (!_action || !_action.command) {
                return;
            }
            _action.tab = 'Home';
            _action.mixin = defaultMixin;
            _action.action = action;
            result.push(_action);
        }
        for (var i = 0; i < actions.length; i++) {
            var action = actions[i];
            Workbench._loadActionClass(action);
            var parms = {
                showLabel: false
            };
            ['label', 'showLabel', 'iconClass'].forEach(function (prop) {
                if (action.hasOwnProperty(prop)) {
                    parms[prop] = action[prop];
                }
            });
            if (action.className) {
                parms['class'] = action.className;
            }
            if (action.menu && (action.type == 'DropDownButton' || action.type == 'ComboButton')) {
                for (var ddIndex = 0; ddIndex < action.menu.length; ddIndex++) {
                    var menuItemObj = action.menu[ddIndex];
                    Workbench._loadActionClass(menuItemObj);
                    var menuItemParms = {};
                    var props = ['label', 'iconClass'];
                    props.forEach(function (prop) {
                        if (menuItemObj[prop]) {
                            menuItemParms[prop] = menuItemObj[prop];
                        }
                    });

                    completeAndAddAction(menuItemObj);
                }

            } else if (action.toggle || action.radioGroup) {

            }
            var disabled = false;
            completeAndAddAction(action);
        }

        return result;

    }

    if (sctx && window.lastArgs) {
        const workbench = window.workbench;
        var args = window.lastArgs;
        var partID = args.partID,
            domNode = args.domNode,
            context = args.context,
            widgetCallback = args.openCallback;
        var o = workbench.getActionSets(partID);
        var ctxManager = sctx.getContextManager();
        if (context.cMenu) {
            context.cMenu.destroy();
        }        
        context.cMenu = _createContextMenu.apply(context.getVisualEditor(), [domNode]);
        context.getVisualEditor().add(context.cMenu);
        
    }

    function _createContextMenu(node) {
        var _ctorArgs = this.contextMenuArgs || {};
        var mixin = {
            owner: this,
            delegate: this,
            actionFilter: {
                quick: true
            }
        };
        utils.mixin(_ctorArgs, mixin);
        var contextMenu = new ContextMenu(_ctorArgs, node);
        contextMenu.openTarget = node;
        contextMenu.init({
            preventDoubleContext: false
        });
        contextMenu._registerActionEmitter(this);
        $(node).one('contextmenu', function (e) {
            e.preventDefault();
            if (!this.store) {
                contextMenu.setActionStore(this.getActionStore(), this);
            }
        }.bind(this));

        this.contextMenu = contextMenu;
    }

    */
    /**
     *
     * @class module:xideve/manager/ContextManager
     * @extends  module:xide/manager/ManagerBase
     * @augments  module:xide/widgets/_MenuMixin
     * @augments  module:xide/mixins/ActionMixin
     */
    return dcl([ManagerBase, ServerActionBase, VariableMixin.dcl, ReloadMixin.dcl, ActionProvider.dcl], {
        declaredClass: "xideve/manager/ContextManager",
        serviceClass: 'XIDE_VE_Service',
        singleton: true,
        contexts: [],
        /**
         * Resource variable delimiters
         * @type {Object}
         */
        variableDelimiters: {
            begin: "%%",
            end: "%%"
        },
        onReloaded: function () {},
        /**
         *
         * @param item
         */
        getViewUrl: function (item, params, extraParams) {
            if (!item) {
                console.error('invalid item!');
            }
            let url = this.ctx.getResourceManager().getVariable('ROOT') +
                'xideve/preview/' + item.mount + '/' + item.path.replace('./', '') +
                (has('debug') ? '?debug=true' : '');

            url = url + (url.indexOf('?') === -1 ? '?' : '&');
            url = url + ('userDirectory=' + encodeURIComponent(this.ctx.getUserDirectory()));
            return url;


            /*
            var config =  this.ctx.getFileManager().config;
            var downloadUrl = decodeURIComponent(config.FILE_SERVICE);
            downloadUrl = downloadUrl.replace('view=rpc','view=smdCall');
            var path = utils.buildPath(item.mount,item.path,true);
            path = this.base64_encode(path);
            var serviceClass = this.serviceClass;
            if(downloadUrl.indexOf('?')!=-1){
                downloadUrl+='&';
            }else{
                downloadUrl+='?';
            }

            downloadUrl+='service=' + serviceClass + '.view&file=' + path + '&callback=asdf';
            downloadUrl+='&raw=html';
            downloadUrl+='&attachment=0';
            downloadUrl+='&send=1';
            var aParams = utils.getUrlArgs(location.href);
            utils.mixin(aParams,{
                "service": serviceClass + ".view",
                "path":path,
                "callback":"asdf",
                "raw":"html",
                "attachment":"0",
                "send":"1"
            });
            utils.mixin(aParams,extraParams);
            var pStr  =  JSON.stringify(aParams);
            var signature = SHA1._hmac(pStr, config.RPC_PARAMS.rpcSignatureToken, 1);
            downloadUrl+='&' + config.RPC_PARAMS.rpcUserField + '=' + config.RPC_PARAMS.rpcUserValue;
            downloadUrl+='&' + config.RPC_PARAMS.rpcSignatureField + '=' + signature;
            if(extraParams){
                for(var p in extraParams){
                    downloadUrl+='&' + p + '=' + extraParams[p];
                }
            }

            return downloadUrl;
            */
        },
        viewFile: function (item) {
            /**
             * 1. gather options and arguments
             *
             */
            item = {
                path: 'delite.html',
                mount: 'workspace'
            };

            /*var path = utils.buildPath(item.mount,item.path,true);
            path = this.base64_encode(path);*/

            const viewUrl = this.getViewUrl(item);

        },
        /**
         * Context is setting header data, complete asset urls
         * @param evt {Object}
         * @param evt.context {module:xideve/delite/Context} : the context object
         * @param evt.editor {module:xideve/views/VisualEditor} : the editor
         * @param evt.data {Object} : the header data
         */
        onBuildDojoConfig: function (evt) {
            if (evt && evt.data && evt.data) {

                const dojoConfig = evt.data;
                if (dojoConfig.baseUrl) {
                    dojoConfig.baseUrl = this.resolve(dojoConfig.baseUrl);
                }
            }
            debugDojo &&  0 && console.log('new dojo config', evt);
        },
        onSetDojoUrl: function (evt) {
            if (evt && evt.data && evt.data) {
                if (evt.data.dojoUrl) {
                    evt.data.dojoUrl = this.resolve('%%XIDEVE_DOJO_URL%%');
                    debugDojo &&  0 && console.log('did resolve dojo url: ' + evt.data.dojoUrl);
                }
            }
        },
        /**
         * Context is setting whole source data
         * @param evt
         */
        onSetSourceData: function (evt) {
            if (evt.context.widgetsReadyDfd) {
                delete evt.context.widgetsReadyDfd;
                evt.context.widgetsReadyDfd = null;
            }
            evt.context.widgetsReadyDfd = new Deferred();
            debugDojo &&  0 && console.log('xideve/manager/WidgetManager::setSourceData', evt);
        },
        /**
         * Callback when context is determining the application's Dojo packages. This is being
         * used to extend those package information with our custom packages.
         * @todo move to a more centralized place
         * @param data
         */
        onGetLoaderPackages: function (data) {
            const _base = dojo.baseUrl;
            const makePackage = function (where, what, offset, release) {
                offset = offset || '';
                where.packages.push({
                    name: what,
                    location: release !== true ? _base + '/' + offset + what : _base + '/' + what + '-release/' + what
                });
            };

            makePackage(data, 'xapp');
            makePackage(data, 'dijit');
            makePackage(data, 'dojox');
            makePackage(data, 'xblox');
            makePackage(data, 'dstore');
            makePackage(data, 'xide');
            makePackage(data, 'xfile');
            makePackage(data, 'xwire');
            makePackage(data, 'xlang');
            makePackage(data, 'xaction');
            makePackage(data, 'xgrid');
            makePackage(data, 'xnode');
            const _offset = 'ibm-js/';
            makePackage(data, 'delite', _offset);
            makePackage(data, 'deliteful', _offset);
            makePackage(data, 'decor', _offset);
            makePackage(data, 'dpointer', _offset);
            makePackage(data, 'dcl', _offset);
            makePackage(data, 'requirejs-dplugins', _offset);
            makePackage(data, 'requirejs-domready', _offset);
            makePackage(data, 'requirejs-text', _offset);
            makePackage(data, 'jquery', _offset);
        },
        /**
         * Callback when a ui-designer's context has been fully loaded. The context is tracked.
         * @param context
         */
        onContextLoaded: function (context) {

            this.currentContext = context;
            const global = context.global, thiz = this;

            global['_'] = window['_'];
            global['Velocity'] = window['Velocity'];

            global['delegate'] = this;

            if (global.bootx) {
                //this will load xapp dependencies and runs start
                global.bootx({
                    delegate: this
                }, Deferred).then(function (params) {
                    const appContext = params.context;
                    debugBoot &&  0 && console.log('xapp ready!');
                    debugBoot &&  0 && console.log('   context: ' + params.context.application.id, params.context);
                    debugBoot &&  0 && console.log('   settings: ', params.settings);
                    //@TODO
                    context.appContext = params.context;
                    thiz.onContextReady(context, params.context, params.settings);
                }, function (e) {
                    console.error('error global.bootx ' + e, e);
                });
            } else {
                 0 && console.warn('have no bootx');
            }
        },
        /**
         *
         * @param context {module:xideve/delite/Context}
         * @param appContext {module:xapp/manager/Application} This is the application object inside the document
         * @param appSettings
         */
        onContextReady: function (context, appContext, appSettings) {
            const global = context.global;
            const doc = context.getDocument(), thiz = this;

            //hard wire file manager to IDE file manager
            appContext.fileManager = this.ctx.fileManager;
            //hard wire resource manager to IDE resource manager
            appContext.resourceManager = this.ctx.resourceManager;
            appContext.driverManager = thiz.ctx.driverManager;
            appContext.deviceManager = thiz.ctx.deviceManager;
            appContext.nodeServiceManager = thiz.ctx.nodeServiceManager;
            appContext.blockManager = thiz.ctx.blockManager;
            appContext.delegate = {
                isDesignMode: function () {
                    return context.isDesignMode();
                },
                getBlockSettings: function () {
                    return context.getBlockSettings();
                },
                getEditorContext: function () {
                    return context;
                }
            };
            debugBoot &&  0 && console.log('context ready: ', appSettings);
            appSettings.delegate = thiz;
            appSettings.xbloxScripts = [];
            appContext.initVe();
            appContext.notifier.publish('onContextReady', appContext);
            appContext.notifier.publish('DevicesConnected');
            if (appSettings.xbloxScripts) {

                const ve = context.getVisualEditor();

                function loadXBLOXFiles() {

                    debugBoot &&  0 && console.log('  loadXBLOX files', appSettings);
                    thiz.ctx.getBlockManager().loadFiles(appSettings.xbloxScripts).then(function (scopes) {
                        appSettings.didXBLOX = true;
                        const _eventArgs = {
                            context: context,
                            appContext: appContext,
                            appSettings: appSettings,
                            ctx: thiz.ctx,
                            global: global,
                            document: doc,
                            blockScopes: scopes
                        };

                        ve.onSceneBlocksLoaded(_eventArgs);
                        const fileStore = ve.item._S;
                        const folder = ve.item.getParent();
                        appContext.settings = appSettings;
                        appContext.onReady(appSettings);
                        fileStore.resetQueryLog();
                        fileStore._loadPath(folder.path, true).then(function () {
                            if (folder.path === '.') {
                                fileStore.resetQueryLog();
                            }
                            thiz.publish(types.EVENTS.ON_CONTEXT_READY, _eventArgs);
                            thiz.onSceneBlocksLoaded(_eventArgs);
                        });


                        if (!appContext.__didv) {
                            appContext.__didv = true;
                        }
                    })
                }
                if (appSettings.xbloxScripts.length == 0) {
                    var item = context.getVisualEditor().item;
                    if (item) {
                        const mount = utils.replaceAll('/', '', item.mount);
                        const extension = utils.getFileExtension(item.path);
                        const path = item.path.replace('.' + extension, '.xblox');
                        const sceneBloxItem = {
                            mount: mount,
                            path: path
                        };

                        appSettings.xbloxScripts.push(sceneBloxItem);
                        const content = {
                            "blocks": [],
                            "variables": []
                        };
                        this.ctx.getFileManager().mkfile(mount, path, JSON.stringify(content, null, 2)).then(function () {
                            loadXBLOXFiles();
                        });
                    }
                }
            }
            appContext.application.run(appSettings);
            debugBoot &&  0 && console.log('run xapp application');
            const params = {
                context: context,
                application: appContext.application,
                delegate: this
            };

            this.publish(types.EVENTS.ON_APP_READY, params);

            for (let i = 0; i < this.contexts.length; i++) {
                var item = this.contexts[i];
                if (item.context == context) {
                    return;
                }
            }
            this.contexts.push({
                context: context,
                appContext: appContext,
                global: global,
                doc: doc
            });
        },
        onBuildHeader: function (evt) {
            if (evt && evt.data && evt.data.styleSheets) {
                const sheets = evt.data.styleSheets;
                for (let i = 0; i < sheets.length; i++) {
                    const sheet = sheets[i];
                    evt.data.styleSheets[i] = this.resolve(sheet);
                }
            }
        },
        onModuleUpdated: function (event) {
            _.each(this.contexts, function (context) {
                context.appContext.onModuleUpdated(event);
            }, this);
        },
        /**
         * @TODO move to xcf
         * @param event
         */
        onDriverVariableChanged: function (event) {
            _.each(this.contexts, function (context) {
                if (!context.context.isDesignMode()) {
                    context.appContext.publish('onDriverVariableChanged', event);
                }
            }, this);
        },
        onContextDestroyed: function (context) {
            const entry = _.find(this.contexts, {
                context: context
            });
            if (entry) {
                if (context.systemEvents) {
                    _.each(context.systemEvents, function (handle) {
                        if (handle && handle.remove) {
                            handle.remove();
                        } else {
                            console.error('have invalid handle!');
                        }
                    });
                }
                this.contexts.remove(entry);
            }
        },
        getVariable: function (deviceId, driverId, variableId) {
            const deviceManager = this.ctx.getDeviceManager();
            const device = deviceManager.getDeviceById(deviceId);
            let result = null;

            if (!device) {
                return null;
            }

            let driverScope = device.driver;
            //not initiated driver!
            if (driverScope && driverScope.blockScope) {
                driverScope = driverScope.blockScope;
            }

            if (!driverScope) {
                if (device) {
                    var driverId = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                    driverScope = this.ctx.getBlockManager().getScope(driverId);
                    result = driverScope.getVariableById(driverId + '/' + variableId);
                }
            }
            return result;
        },

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Widget's blocks
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getBlock: function (url) {
            return this.ctx.getDeviceManager().getBlock(url);
        },
        wireNode: function (widget, event, block, editorContext, appContext, params) {
            debugWiring &&  0 && console.log('wireNode: ' + event);

            const thiz = this;
            let rejectFunction = null;
            let onBeforeRun = null;

            if (params) {
                if (event === types.EVENTS.ON_DRIVER_VARIABLE_CHANGED) {
                    const varParams = params.params;
                    const variableId = varParams[2];
                    rejectFunction = function (evt) {
                        const variable = evt.item;
                        const _variableIn = thiz.getBlock(variableId);
                        if (_variableIn && variable && _variableIn.id === variable.id) {
                            return false;
                        }
                        if (variable.id === variableId) {
                            return false;
                        }
                        return true;
                    };
                    onBeforeRun = function (block, evt) {
                        const variable = evt.item;
                        block.override = {
                            variables: {
                                value: variable.value
                            }
                        };
                    }
                }
            }
            if (!widget) {
                console.error('have no widget for event ' + event);
                return;
            }
            if (!block) {
                console.error('have no block for event ' + event);
                return;
            }
            if (!event) {
                console.error('have no event');
                return;
            }

            if (!_.isString(event)) {
                console.error('event not string ', event);
                return;
            }

            if (!editorContext.systemEvents) {
                editorContext.systemEvents = [];
            }

            //special treatment
            if (widget.tagName === 'BODY' && widget._destroyHandles) {
                widget._destroyHandles();
                delete widget['__setup'];
            }

            if (!widget['__setup']) {
                widget['__setup'] = {};
            }



            if (widget['__setup'][block.id]) {
                console.error('did wire ' + block.name + ' to ' + widget.id + '_' + widget['wire id'], widget);
                return;
            }

            widget['__setup'][block.id] = true;
            if (!widget['wire id']) {
                widget['wire id'] = utils.createUUID();
            }

            debugWiring &&  0 && console.log('wire node : ' + event + ' ' + block.name + ' _ ' + widget['wire id']);
            const context = widget;
            if (block && context) {
                block.context = context;
                block._targetReference = context;
            }
            /**
             *
             * @param event
             * @param value: original event data
             * @param block
             * @param widget
             */
            const run = function (event, value, block, widget) {
                debugWiring &&  0 && console.log('run ! ' + event);
                //filter, in design mode, we ain't do anything
                if (event !== 'load' && editorContext && editorContext.isDesignMode) {
                    if (editorContext.isDesignMode()) {
                        return;
                    }
                }
                //filter, custom reject function
                if (rejectFunction) {
                    const abort = rejectFunction(value);
                    if (abort) {
                        return;
                    }
                }
                if (block._destroyed) {
                     0 && console.log('run failed block invalid');
                    return;
                }
                if (!block.enabled) {
                    return;
                }

                debugRun &&  0 && console.log('run ! ' + event + ' for block ' + block.name + ':' + block.id);

                //setup variables
                const context = widget;

                let result;

                if (block && context) {
                    block.context = context;
                    block._targetReference = context;

                    if (onBeforeRun) {
                        onBeforeRun(block, value);
                    }
                    result = block.solve(block.scope, {
                        highlight: true,
                        args: [value]
                    });
                    debugBlox &&  0 && console.log('run ' + block.name + ' for even ' + event, result);
                }
            };

            //patch the target
            if (!widget.subscribe) {
                utils.mixin(widget, EventedMixin.prototype);
            }

            let _target = widget.domNode || widget;
            const _event = event;
            let _isWidget = widget.declaredClass || widget.startup;
            const _hasWidgetCallback = widget.on != null && widget['on' + utils.capitalize(_event)] != null;
            let _handle = null;
            const _isDelite = _target.render != null && _target.on != null;


            if (_isWidget &&
                //dijit
                (widget.baseClass && widget.baseClass.indexOf('dijitContentPane') != -1)
                //delite
                ||
                widget.render != null || widget.on != null) {
                _isWidget = false; //use on
            }

            if (_target) {
                //plain node
                if (!_isDelite && (!_hasWidgetCallback || !_isWidget)) {

                    if (utils.isSystemEvent(event)) {
                        const _handler = function (evt) {
                            run(event, evt, block, widget);
                        }.bind(this);
                        _handle = widget.subscribe(event, _handler);
                        block._on('destroy', function () {
                            console.error('block destroyed');
                            _handle.remove();
                        })
                    } else {
                        _handle = widget.__on(_target, event, function (evt) {
                            run(event, evt, block, widget);
                        });
                    }
                } else {
                    _target = widget;
                    const useOn = true;
                    if (useOn) {
                        if (!_isDelite) {
                            const _e = 'on' + utils.capitalize(_event);

                            widget[_e] = function (val, nada) {
                                if (_target.ignore !== true) {
                                    run(event, val);
                                }
                            }
                        } else {

                            if (utils.isSystemEvent(event)) {
                                _handle = _target.subscribe(event, function (evt) {
                                    run(event, evt, block, widget);
                                }.bind(this), widget);

                            } else {
                                if (utils.isNativeEvent(event)) {
                                    event = event.replace('on', '');
                                }
                                _handle = _target.on(event, function (evt) {
                                    let value = evt.target.value;

                                    if ("checked" in evt.target) {
                                        value = evt.target.checked;
                                    }
                                    run(event, value, block, widget);
                                }.bind(this));
                            }
                        }
                    } else {
                        widget['on' + utils.capitalize(_event)] = function (val) {
                            if (_target.ignore !== true) {
                                run(event, val);
                            }
                        }
                    }
                }
                if (_handle) {
                    if (widget.addHandle) {
                        widget.addHandle(event, _handle);
                    }
                    if (!block._widgetHandles) {
                        block._widgetHandles = [];
                    }
                    block._widgetHandles.push(_handle);
                } else {
                    console.error('wire widget: have no handle', widget);
                }
            }
        },
        wireWidget: function (editorContext, ctx, scope, widget, node, event, group, appContext, params) {
            const blocks = scope.getBlocks({
                group: group
            });
            if (!blocks || !blocks.length) {
                debugBlox &&  0 && console.log('have no blocks for group : ' + group);
            }
            for (let j = 0; j < blocks.length; j++) {
                const block = blocks[j];
                debugBlox &&  0 && console.log('wire block : ' + block.name + ' for ' + event, block);
                this.wireNode(widget.domNode, event, block, editorContext, appContext, params);
            }
        },
        wireWidget2: function (editorContext, ctx, scope, appContext, widget) {

            const allGroups = scope.allGroups(), widgets = [];

            const getParams = function (group) {
                group = group || "";

                let event = null;
                let widgetId = null;
                const parts = group.split('__');
                let params = [];

                //no element:
                if (parts.length == 1) {
                    event = parts[0];
                    widgetId = 'body';
                    const _body = editorContext.rootWidget;
                    _body.domNode.runExpression = editorContext.global.runExpression;
                }

                if (parts.length == 2) {
                    let blockUrl;
                    //can be: event & block url: onDriverVariableChanged__variable://deviceScope=user_devices&device=bc09b5c4-cfe6-b621-c412-407dbb7bcef8&driver=9db866a4-bb3e-137b-ae23-793b729c44f8&driverScope=user_drivers&block=2219d68b-862f-92ab-de5d-b7a847930a7a
                    //can be: widget id & event: btnCurrentFileName__load
                    if (parts[1].indexOf('://') !== -1) {
                        event = parts[0];
                        widgetId = 'body';
                        blockUrl = parts[1];
                    } else {
                        event = parts[1];
                        widgetId = parts[0];

                    }
                    if (blockUrl) {
                        const url_parts = utils.parse_url(blockUrl);
                        const url_args = utils.urlArgs(url_parts.host);
                        params = [
                            url_args.device.value,
                            url_args.driver.value,
                            blockUrl
                        ]
                    }
                }

                if (parts.length == 3) {
                    event = parts[1];
                    widgetId = parts[0];
                    const _blockUrl = parts[2];
                    const _url_parts = utils.parse_url(_blockUrl);
                    const _url_args = utils.urlArgs(_url_parts.host);
                    params = [
                        _url_args.device.value,
                        _url_args.driver.value,
                        _blockUrl
                    ]
                }

                if (parts.length == 5) {
                    event = parts[1];
                    widgetId = parts[0];
                    params = [
                        parts[2],
                        parts[3],
                        parts[4]
                    ]
                }

                if (event && widgetId) {
                    let widget = Widget.byId(widgetId);
                    if (widgetId === 'body') {
                        widget = editorContext.rootWidget;
                    }
                    return {
                        event: event,
                        widgetId: widgetId,
                        widget: widget,
                        params: params
                    }
                }
                return null;
            };

            for (var i = 0; i < allGroups.length; i++) {
                const group = allGroups[i];
                const params = getParams(group);
                if (!params) {
                    console.error('invalid params for group ' + group);
                    continue;
                }
                if (params.widget == widget) {
                    this.wireWidget(editorContext, ctx, scope, params.widget, params.widget.domNode, params.event, group, appContext, params);
                    params.widget && widgets.indexOf(params.widget) == -1 && widgets.push(params.widget);
                }
            }

            for (var i = 0; i < widgets.length; i++) {
                var widget = widgets[i];
                if (widget.domNode) {
                    widget = widget.domNode;
                }
                if (widget.__didEmitLoad) {
                    return;
                }
                widget.__didEmitLoad = true;
                if (widget.emit) {
                    widget.emit('load', widget);
                }
            }
        },
        wireScope: function (editorContext, ctx, scope, appContext) {
            debugWiring &&  0 && console.log('wire scope ' + scope.id);
            const allGroups = scope.allGroups(), thiz = this, widgets = [];

            const getParams = function (group) {
                group = group || "";

                let event = null;
                let widgetId = null;
                const parts = group.split('__');
                let params = [];

                //no element:
                if (parts.length == 1) {
                    event = parts[0];
                    widgetId = 'body';
                    const _body = editorContext.rootWidget;
                    _body.domNode.runExpression = editorContext.global.runExpression;
                }

                if (parts.length == 2) {
                    let blockUrl;
                    //can be: event & block url: onDriverVariableChanged__variable://deviceScope=user_devices&device=bc09b5c4-cfe6-b621-c412-407dbb7bcef8&driver=9db866a4-bb3e-137b-ae23-793b729c44f8&driverScope=user_drivers&block=2219d68b-862f-92ab-de5d-b7a847930a7a
                    //can be: widget id & event: btnCurrentFileName__load
                    if (parts[1].indexOf('://') !== -1) {
                        event = parts[0];
                        widgetId = 'body';
                        blockUrl = parts[1];
                    } else {
                        event = parts[1];
                        widgetId = parts[0];

                    }
                    if (blockUrl) {
                        const url_parts = utils.parse_url(blockUrl);
                        const url_args = utils.urlArgs(url_parts.host);
                        params = [
                            url_args.device.value,
                            url_args.driver.value,
                            blockUrl
                        ]
                    }
                }

                //scripted__onDriverVariableChanged__variable://deviceScope=user_devices&device=bc09b5c4-cfe6-b621-c412-407dbb7bcef8&driver=9db866a4-bb3e-137b-ae23-793b729c44f8&driverScope=user_drivers&block=2219d68b-862f-92ab-de5d-b7a847930a7a
                if (parts.length == 3) {
                    event = parts[1];
                    widgetId = parts[0];
                    const _blockUrl = parts[2];
                    const _url_parts = utils.parse_url(_blockUrl);
                    const _url_args = utils.urlArgs(_url_parts.host);
                    params = [
                        _url_args.device.value,
                        _url_args.driver.value,
                        _blockUrl
                    ]
                }

                if (parts.length == 5) {
                    event = parts[1];
                    widgetId = parts[0];
                    params = [
                        parts[2],
                        parts[3],
                        parts[4]
                    ]

                }

                if (event && widgetId) {
                    let widget = Widget.byId(widgetId);

                    if (widgetId === 'body') {
                        widget = editorContext.rootWidget;
                    }
                    return {
                        event: event,
                        widgetId: widgetId,
                        widget: widget,
                        params: params
                    }
                }

                return null;
            };

            const wireBlock = function (block) {
                block._on(types.EVENTS.ON_ITEM_REMOVED, function (evt) {
                    try {
                        if (block._widgetHandles) {
                            const _handles = block._widgetHandles;
                            for (let i = 0; i < _handles.length; i++) {
                                if (_handles[i].remove) {
                                    _handles[i].remove();
                                }
                            }
                            delete block._widgetHandles;

                        }
                    } catch (e) {
                        console.error('troubble!' + e, e);
                    }
                }, this);
            };
            for (var i = 0; i < allGroups.length; i++) {
                const group = allGroups[i];
                const params = getParams(group);
                if (!params) {
                    console.error('invalid params for group ' + group);
                    continue;
                }

                if (params.widget) {
                    this.wireWidget(editorContext, ctx, scope, params.widget, params.widget.domNode, params.event, group, appContext, params);
                } else {
                     0 && console.warn('have no widget for group ' + group, params);
                }

                const blocks = scope.getBlocks({
                    group: group
                });

                if (!blocks || !blocks.length) {
                     0 && console.warn('have no blocks for group : ' + group);
                }
                for (let j = 0; j < blocks.length; j++) {
                    const block = blocks[j];
                    wireBlock(block);
                }

                params.widget && widgets.indexOf(params.widget) == -1 && widgets.push(params.widget);
            }

            for (var i = 0; i < widgets.length; i++) {
                let widget = widgets[i];

                if (widget.domNode) {
                    widget = widget.domNode;
                }
                if (widget.__didEmitLoad) {
                    return;
                }
                debugBoot &&  0 && console.log('emit load', widget);
                widget.__didEmitLoad = true;

                if (widget.nodeName === 'BODY') {
                    $(widget.nodeName).trigger('load');
                } else {
                    if (widget.emit) {
                        widget.emit('load', widget);
                    }
                }
            }
            scope._on(types.EVENTS.ON_ITEM_REMOVED, function (evt) {});
            scope._on(types.EVENTS.ON_ITEM_ADDED, function (evt) {
                const params = getParams(evt.item.group);
                if (params && params.widget) {

                    const item = evt.item;
                    debugBlox &&  0 && console.log('on item added', arguments);
                    thiz.wireNode(params.widget.domNode, params.event, evt.item, editorContext, params);
                    wireBlock(evt.item);
                }
            });
        },
        onSceneBlocksLoaded: function (evt) {
            const context = evt.context, appContext = evt.appContext, appSettings = evt.appSettings, ctx = evt.ctx, global = evt.global, document = evt.doc, blockScopes = evt.blockScopes, thiz = this;

            if (evt.context.widgetsReadyDfd) {
                evt.context.widgetsReadyDfd.then(function () {

                    if (appContext.didWireWidgets) {
                        return;
                    }
                    appContext.didWireWidgets = true;

                    for (let i = 0; i < blockScopes.length; i++) {
                        const scope = blockScopes[i];
                        scope.global = global;
                        scope.document = context.getDocument();
                        scope.editorContext = context;
                        scope.appContext = appContext;
                        thiz.wireScope(context, thiz.ctx, scope, appContext);
                    }
                });
            }
        },
        onItemRemoved: function (evt) {},
        /**
         * Init is being called upon {link:xide/manager/Context}::initManagers()
         *
         */
        init: function () {
            this.subscribe("/davinci/ui/context/loaded", this.onContextLoaded);
            this.subscribe([
                types.EVENTS.ON_GET_LOADER_PACKAGES, //add more custom packages
                types.EVENTS.ON_BUILD_HEADER, //hook into the documents header build
                types.EVENTS.ON_BUILD_DOJO_CONFIG, //hook into davinci/ve/Context dojoConfig completion to resolve variables
                types.EVENTS.ON_SET_SOURCE_DATA, //resolve the documents resource variables
                types.EVENTS.ON_SET_DOJO_URL, //update dojo url
                types.EVENTS.ON_MODULE_UPDATED, //forward module updates to application
                types.EVENTS.ON_CONTEXT_DESTROYED,
                types.EVENTS.ON_DRIVER_VARIABLE_CHANGED,
                types.EVENTS.ON_ITEM_REMOVED
            ]);
        }
    });
});
}}});
// wrapped by build app
define("xideve/xideve", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){

});
