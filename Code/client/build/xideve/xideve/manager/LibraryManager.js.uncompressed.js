/** module:xideve/manager/LibraryManager **/
define("xideve/manager/LibraryManager", [
    'dcl/dcl',
    "xide/manager/ServerActionBase",
    "xide/utils",
    "xide/manager/SettingsManager",
    "xide/data/Memory",
    "dojo/Deferred"
], function (dcl, ServerActionBase, utils, SettingsManager, Memory, Deferred) {
    const Module = dcl([SettingsManager], {
        declaredClass: "xideve.manager.LibraryManager",
        serviceClass: 'Library_Store',
        settingsStore: null,
        section: 'styles',
        init: function () {
            const dfd = new Deferred();
            if (this.settingsStore) {
                dfd.resolve(this.settingsStore);
                return dfd;
            }
            this.serviceObject.__init.then(() => {
                this.read(this.section, '.', null, this.onSettingsReceived.bind(this)).then((data) => {
                    dfd.resolve(this.settingsStore);
                });
            });
            return dfd;
        }
    });
    return Module;
});