/** @module xideve/manager/WidgetManager **/
define("xideve/manager/WidgetManagerJS", [
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xide/views/CIActionDialog'
], function (dcl,types, utils, CIActionDialog) {
    /**
     *
     * @mixin module:xide/manager/WidgetManagerBlox
     *
     * */
     return dcl(null,{
         declaredClass:"xideve/manager/WidgetManagerJS",
         onReloaded:function(){
             console.error('reloadex2:');
         },
         /**
          * time to update the HTML model
          * @param params
          */
         onModifiedJS:function(params){

         },
         _createView:function(value,params){

             const thiz = this;
             //set to context to the selected widget
             const expressionContext = params.target || null;

             //get davinci context
             const widgetContext = params.widget ? params.target.getContext() : null;

             //get global
             const global = widgetContext ? widgetContext.getGlobal() : null;

             /**
              * Create an expression delegate, this will run the code
              * directly on davinci's context. In case this part of native
              * event, the script gets the context of the widget, just like
              * in real.
              * @type {{runExpression: Function}}
              */
             const expressionDelegate = {

                 runExpression:function(value,run,error){
                     //var _eval = global['runx'] || global['eval'];
                     let _result = null;
                     if(global['runx']) {
                         try {
                             _result = global['runx'](expressionContext, value);
                         }catch(e){
                             if(error){
                                 error('' + e );
                                 return;
                             }
                         }
                     }else{
                         _result = global['eval'].call(expressionContext,value);
                     }
                     // 0 && console.log('run expression : ' + value + ' with result ' + _result);
                     if(run){
                         run('Expression ' + value + ' evaluates to; '   + _result);
                     }
                 }
             };
             /**
              * Procedure to open multiple values
              */
             const actionDialog = new CIActionDialog({
                 title: 'Javascript',
                 style: 'width:800px;min-height:600px;',
                 resizeable: true,
                 delegate: {
                     onOk: function (dlg, data) {
                         const _p = params;
                         const expression = utils.getCIInputValueByName(data, 'Expression');
                         widgetContext.editor.setDirty(true);
                         if(params.widget.set) {
                             params.widget.set('value', expression);
                         }else if(params.widget.value!=null){
                             params.widget.value = expression;
                             params.widget.owner._onChange({
                                 target:params.widget.pageIndex
                             })
                         }
                         thiz.onModifiedJS(expression,params);
                     }
                 },
                 cis: [

                     utils.createCI('Settings', types.ECIType.STRING, value, {
                         group: 'Settings'
                     }),

                     utils.createCI('Expression', types.ECIType.EXPRESSION_EDITOR, value, {
                         group: 'Script',
                         delegate:expressionDelegate
                     })

                 ]
             });


             actionDialog.show();

         },
         openJSWizard:function(params){
             this._createView(params.value,params);
         },
         onJavascript:function(context){
             let widget = context.row.widget;
             let val = '';//widget.get('value');
             if(!widget){
                 widget = dijit.byId(context.row.id);
                 if(widget){
                     val = widget.get('value');
                 }
             }
             if(!widget){
                 widget = dojo.byId(context.row.id);
                 if(widget){
                     val = widget.value;
                 }
             }

             //build complete info
             const props  = {
                 owner:this,
                 delegate:this,
                 widget:widget,//widget cascade
                 value:val,//the value
                 target:context.widget,//actual scene widget
                 meta:{
                     context:context
                 }
             };

             this.openJSWizard(props);
         }
     });
});
