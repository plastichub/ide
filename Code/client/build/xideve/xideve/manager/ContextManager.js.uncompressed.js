/** @module xideve/manager/WidgetManager **/
define("xideve/manager/ContextManager", [
    'dcl/dcl',
    'dojo/Deferred',
    'xide/manager/ManagerBase',
    'xide/mixins/VariableMixin',
    'xide/mixins/ReloadMixin',
    'xide/types',
    'xide/utils',
    'xide/mixins/EventedMixin',
    'davinci/ve/widget',
    "xide/manager/ServerActionBase",
    'xdojo/has',
    'xaction/ActionProvider',
    'xide/lodash',
    'xide/$'
], function (dcl, Deferred, ManagerBase, VariableMixin,
    ReloadMixin, types, utils, EventedMixin,
    Widget, ServerActionBase, has, ActionProvider, _, $) {


    const debugWiring = false;
    const debugBlox = false;
    const debugBoot = false;
    const debugDojo = false;
    const debugRun = false;
/*
    function popup(args) {
        if (window['lastArgs']) {
            return;
        }
        window['lastArgs'] = args;
        var partID = args.partID,
            domNode = args.domNode,
            context = args.context,
            widgetCallback = args.openCallback;
        console.log('p2', args);
    }

    window['vePopup'] = popup;

    const toCommand = (action) => {
        switch (action) {
            case 'Undo':
                return 'Edit/Undo';
            case 'Redo':
                return 'Edit/Redo';
            case 'Cut':
                return 'Edit/Cut';
            case 'Copy':
                return 'Edit/Copy';
            case 'Paste':
                return 'Edit/Paste';
            case 'Delete':
                return 'Edit/Delete';
        }

    }
    const getIconClass = (iconClassIn) => {
        if (!iconClassIn) {
            return '';
        }
        switch (iconClassIn) {
            case "editActionIcon undoIcon":
                {
                    return 'text-info fa-undo';
                }
            case "editActionIcon redoIcon":
                {
                    return 'text-info fa-repeat';
                }

            case "editActionIcon editCutIcon":
                {
                    return 'text-warning fa-scissors';
                }
            case "editActionIcon editCopyIcon":
                {
                    return 'text-warning fa-copy';
                }
            case "editActionIcon editPasteIcon":
                {
                    return 'text-warning fa-paste';
                }
            case "editActionIcon editDeleteIcon":
                {
                    return 'text-danger fa-remove';
                }
        }
        return iconClassIn;
    }

    const toAction = (action, editorContext, who) => {
        var cmd = action.command || toCommand(action.label) || action.id;
        if (!cmd) {
            return null;
        }
        var _default = who.createAction({
            label: action.label,
            group: action.group || 'Ungrouped',
            icon: getIconClass(action.iconClass) || action.iconClass,
            command: cmd,
            tab: action.tab || 'Home',
            mixin: {
                action: action,
                addPermission: true,
                quick: true,
                actionId: action.id
            },
            shouldDisable: function () {
                if (editorContext && action.action && action.action.isEnabled) {
                    return !action.action.isEnabled(editorContext);
                }
                return false;
            }
        });

        switch (action.id) {
            case "documentSettings":
                {
                    return null;
                }
            case 'undo':
            case 'outline':
                {
                    _default.group = 'Widget';
                    return _default;
                }
            case 'redo':
                {
                    _default.group = 'Edit';
                    return _default;
                }
            case 'cut':
            case 'copy':
            case 'paste':
                {
                    _default.group = 'Edit';
                    return _default;
                }
            case 'delete':
                {
                    _default.group = 'Organize';
                    return _default;

                }
            case 'theme':
            case 'rotateDevice':
            case 'chooseDevice':
            case 'stickynote':
            case 'savecombo':
            case 'showWidgetsPalette':
            case 'layout':
            case 'sourcecombo':
            case 'design':
            case 'closeactiveeditor':
            case 'tableCommands':
                {
                    return null;
                }
        }
        debug && console.log('unknown action : ' + action.id, action);
        return _default;
    }


    const toActions = (actions, editorContext, who) => {
        const result = [];
        const defaultMixin = {
            addPermission: true,
            quick: true
        };

        function completeAndAddAction(action) {
            var _action = toAction(action, editorContext, who);
            if (!_action || !_action.command) {
                return;
            }
            _action.tab = 'Home';
            _action.mixin = defaultMixin;
            _action.action = action;
            result.push(_action);
        }
        for (var i = 0; i < actions.length; i++) {
            var action = actions[i];
            Workbench._loadActionClass(action);
            var parms = {
                showLabel: false
            };
            ['label', 'showLabel', 'iconClass'].forEach(function (prop) {
                if (action.hasOwnProperty(prop)) {
                    parms[prop] = action[prop];
                }
            });
            if (action.className) {
                parms['class'] = action.className;
            }
            if (action.menu && (action.type == 'DropDownButton' || action.type == 'ComboButton')) {
                for (var ddIndex = 0; ddIndex < action.menu.length; ddIndex++) {
                    var menuItemObj = action.menu[ddIndex];
                    Workbench._loadActionClass(menuItemObj);
                    var menuItemParms = {};
                    var props = ['label', 'iconClass'];
                    props.forEach(function (prop) {
                        if (menuItemObj[prop]) {
                            menuItemParms[prop] = menuItemObj[prop];
                        }
                    });

                    completeAndAddAction(menuItemObj);
                }

            } else if (action.toggle || action.radioGroup) {

            }
            var disabled = false;
            completeAndAddAction(action);
        }

        return result;

    }

    if (sctx && window.lastArgs) {
        const workbench = window.workbench;
        var args = window.lastArgs;
        var partID = args.partID,
            domNode = args.domNode,
            context = args.context,
            widgetCallback = args.openCallback;
        var o = workbench.getActionSets(partID);
        var ctxManager = sctx.getContextManager();
        if (context.cMenu) {
            context.cMenu.destroy();
        }        
        context.cMenu = _createContextMenu.apply(context.getVisualEditor(), [domNode]);
        context.getVisualEditor().add(context.cMenu);
        
    }

    function _createContextMenu(node) {
        var _ctorArgs = this.contextMenuArgs || {};
        var mixin = {
            owner: this,
            delegate: this,
            actionFilter: {
                quick: true
            }
        };
        utils.mixin(_ctorArgs, mixin);
        var contextMenu = new ContextMenu(_ctorArgs, node);
        contextMenu.openTarget = node;
        contextMenu.init({
            preventDoubleContext: false
        });
        contextMenu._registerActionEmitter(this);
        $(node).one('contextmenu', function (e) {
            e.preventDefault();
            if (!this.store) {
                contextMenu.setActionStore(this.getActionStore(), this);
            }
        }.bind(this));

        this.contextMenu = contextMenu;
    }

    */
    /**
     *
     * @class module:xideve/manager/ContextManager
     * @extends  module:xide/manager/ManagerBase
     * @augments  module:xide/widgets/_MenuMixin
     * @augments  module:xide/mixins/ActionMixin
     */
    return dcl([ManagerBase, ServerActionBase, VariableMixin.dcl, ReloadMixin.dcl, ActionProvider.dcl], {
        declaredClass: "xideve/manager/ContextManager",
        serviceClass: 'XIDE_VE_Service',
        singleton: true,
        contexts: [],
        /**
         * Resource variable delimiters
         * @type {Object}
         */
        variableDelimiters: {
            begin: "%%",
            end: "%%"
        },
        onReloaded: function () {},
        /**
         *
         * @param item
         */
        getViewUrl: function (item, params, extraParams) {
            if (!item) {
                console.error('invalid item!');
            }
            let url = this.ctx.getResourceManager().getVariable('ROOT') +
                'xideve/preview/' + item.mount + '/' + item.path.replace('./', '') +
                (has('debug') ? '?debug=true' : '');

            url = url + (url.indexOf('?') === -1 ? '?' : '&');
            url = url + ('userDirectory=' + encodeURIComponent(this.ctx.getUserDirectory()));
            return url;


            /*
            var config =  this.ctx.getFileManager().config;
            var downloadUrl = decodeURIComponent(config.FILE_SERVICE);
            downloadUrl = downloadUrl.replace('view=rpc','view=smdCall');
            var path = utils.buildPath(item.mount,item.path,true);
            path = this.base64_encode(path);
            var serviceClass = this.serviceClass;
            if(downloadUrl.indexOf('?')!=-1){
                downloadUrl+='&';
            }else{
                downloadUrl+='?';
            }

            downloadUrl+='service=' + serviceClass + '.view&file=' + path + '&callback=asdf';
            downloadUrl+='&raw=html';
            downloadUrl+='&attachment=0';
            downloadUrl+='&send=1';
            var aParams = utils.getUrlArgs(location.href);
            utils.mixin(aParams,{
                "service": serviceClass + ".view",
                "path":path,
                "callback":"asdf",
                "raw":"html",
                "attachment":"0",
                "send":"1"
            });
            utils.mixin(aParams,extraParams);
            var pStr  =  JSON.stringify(aParams);
            var signature = SHA1._hmac(pStr, config.RPC_PARAMS.rpcSignatureToken, 1);
            downloadUrl+='&' + config.RPC_PARAMS.rpcUserField + '=' + config.RPC_PARAMS.rpcUserValue;
            downloadUrl+='&' + config.RPC_PARAMS.rpcSignatureField + '=' + signature;
            if(extraParams){
                for(var p in extraParams){
                    downloadUrl+='&' + p + '=' + extraParams[p];
                }
            }

            return downloadUrl;
            */
        },
        viewFile: function (item) {
            /**
             * 1. gather options and arguments
             *
             */
            item = {
                path: 'delite.html',
                mount: 'workspace'
            };

            /*var path = utils.buildPath(item.mount,item.path,true);
            path = this.base64_encode(path);*/

            const viewUrl = this.getViewUrl(item);

        },
        /**
         * Context is setting header data, complete asset urls
         * @param evt {Object}
         * @param evt.context {module:xideve/delite/Context} : the context object
         * @param evt.editor {module:xideve/views/VisualEditor} : the editor
         * @param evt.data {Object} : the header data
         */
        onBuildDojoConfig: function (evt) {
            if (evt && evt.data && evt.data) {

                const dojoConfig = evt.data;
                if (dojoConfig.baseUrl) {
                    dojoConfig.baseUrl = this.resolve(dojoConfig.baseUrl);
                }
            }
            debugDojo && console.log('new dojo config', evt);
        },
        onSetDojoUrl: function (evt) {
            if (evt && evt.data && evt.data) {
                if (evt.data.dojoUrl) {
                    evt.data.dojoUrl = this.resolve('%%XIDEVE_DOJO_URL%%');
                    debugDojo && console.log('did resolve dojo url: ' + evt.data.dojoUrl);
                }
            }
        },
        /**
         * Context is setting whole source data
         * @param evt
         */
        onSetSourceData: function (evt) {
            if (evt.context.widgetsReadyDfd) {
                delete evt.context.widgetsReadyDfd;
                evt.context.widgetsReadyDfd = null;
            }
            evt.context.widgetsReadyDfd = new Deferred();
            debugDojo && console.log('xideve/manager/WidgetManager::setSourceData', evt);
        },
        /**
         * Callback when context is determining the application's Dojo packages. This is being
         * used to extend those package information with our custom packages.
         * @todo move to a more centralized place
         * @param data
         */
        onGetLoaderPackages: function (data) {
            const _base = dojo.baseUrl;
            const makePackage = function (where, what, offset, release) {
                offset = offset || '';
                where.packages.push({
                    name: what,
                    location: release !== true ? _base + '/' + offset + what : _base + '/' + what + '-release/' + what
                });
            };

            makePackage(data, 'xapp');
            makePackage(data, 'dijit');
            makePackage(data, 'dojox');
            makePackage(data, 'xblox');
            makePackage(data, 'dstore');
            makePackage(data, 'xide');
            makePackage(data, 'xfile');
            makePackage(data, 'xwire');
            makePackage(data, 'xlang');
            makePackage(data, 'xaction');
            makePackage(data, 'xgrid');
            makePackage(data, 'xnode');
            const _offset = 'ibm-js/';
            makePackage(data, 'delite', _offset);
            makePackage(data, 'deliteful', _offset);
            makePackage(data, 'decor', _offset);
            makePackage(data, 'dpointer', _offset);
            makePackage(data, 'dcl', _offset);
            makePackage(data, 'requirejs-dplugins', _offset);
            makePackage(data, 'requirejs-domready', _offset);
            makePackage(data, 'requirejs-text', _offset);
            makePackage(data, 'jquery', _offset);
        },
        /**
         * Callback when a ui-designer's context has been fully loaded. The context is tracked.
         * @param context
         */
        onContextLoaded: function (context) {

            this.currentContext = context;
            const global = context.global, thiz = this;

            global['_'] = window['_'];
            global['Velocity'] = window['Velocity'];

            global['delegate'] = this;

            if (global.bootx) {
                //this will load xapp dependencies and runs start
                global.bootx({
                    delegate: this
                }, Deferred).then(function (params) {
                    const appContext = params.context;
                    debugBoot && console.log('xapp ready!');
                    debugBoot && console.log('   context: ' + params.context.application.id, params.context);
                    debugBoot && console.log('   settings: ', params.settings);
                    //@TODO
                    context.appContext = params.context;
                    thiz.onContextReady(context, params.context, params.settings);
                }, function (e) {
                    console.error('error global.bootx ' + e, e);
                });
            } else {
                console.warn('have no bootx');
            }
        },
        /**
         *
         * @param context {module:xideve/delite/Context}
         * @param appContext {module:xapp/manager/Application} This is the application object inside the document
         * @param appSettings
         */
        onContextReady: function (context, appContext, appSettings) {
            const global = context.global;
            const doc = context.getDocument(), thiz = this;

            //hard wire file manager to IDE file manager
            appContext.fileManager = this.ctx.fileManager;
            //hard wire resource manager to IDE resource manager
            appContext.resourceManager = this.ctx.resourceManager;
            appContext.driverManager = thiz.ctx.driverManager;
            appContext.deviceManager = thiz.ctx.deviceManager;
            appContext.nodeServiceManager = thiz.ctx.nodeServiceManager;
            appContext.blockManager = thiz.ctx.blockManager;
            appContext.delegate = {
                isDesignMode: function () {
                    return context.isDesignMode();
                },
                getBlockSettings: function () {
                    return context.getBlockSettings();
                },
                getEditorContext: function () {
                    return context;
                }
            };
            debugBoot && console.log('context ready: ', appSettings);
            appSettings.delegate = thiz;
            appSettings.xbloxScripts = [];
            appContext.initVe();
            appContext.notifier.publish('onContextReady', appContext);
            appContext.notifier.publish('DevicesConnected');
            if (appSettings.xbloxScripts) {

                const ve = context.getVisualEditor();

                function loadXBLOXFiles() {

                    debugBoot && console.log('  loadXBLOX files', appSettings);
                    thiz.ctx.getBlockManager().loadFiles(appSettings.xbloxScripts).then(function (scopes) {
                        appSettings.didXBLOX = true;
                        const _eventArgs = {
                            context: context,
                            appContext: appContext,
                            appSettings: appSettings,
                            ctx: thiz.ctx,
                            global: global,
                            document: doc,
                            blockScopes: scopes
                        };

                        ve.onSceneBlocksLoaded(_eventArgs);
                        const fileStore = ve.item._S;
                        const folder = ve.item.getParent();
                        appContext.settings = appSettings;
                        appContext.onReady(appSettings);
                        fileStore.resetQueryLog();
                        fileStore._loadPath(folder.path, true).then(function () {
                            if (folder.path === '.') {
                                fileStore.resetQueryLog();
                            }
                            thiz.publish(types.EVENTS.ON_CONTEXT_READY, _eventArgs);
                            thiz.onSceneBlocksLoaded(_eventArgs);
                        });


                        if (!appContext.__didv) {
                            appContext.__didv = true;
                        }
                    })
                }
                if (appSettings.xbloxScripts.length == 0) {
                    var item = context.getVisualEditor().item;
                    if (item) {
                        const mount = utils.replaceAll('/', '', item.mount);
                        const extension = utils.getFileExtension(item.path);
                        const path = item.path.replace('.' + extension, '.xblox');
                        const sceneBloxItem = {
                            mount: mount,
                            path: path
                        };

                        appSettings.xbloxScripts.push(sceneBloxItem);
                        const content = {
                            "blocks": [],
                            "variables": []
                        };
                        this.ctx.getFileManager().mkfile(mount, path, JSON.stringify(content, null, 2)).then(function () {
                            loadXBLOXFiles();
                        });
                    }
                }
            }
            appContext.application.run(appSettings);
            debugBoot && console.log('run xapp application');
            const params = {
                context: context,
                application: appContext.application,
                delegate: this
            };

            this.publish(types.EVENTS.ON_APP_READY, params);

            for (let i = 0; i < this.contexts.length; i++) {
                var item = this.contexts[i];
                if (item.context == context) {
                    return;
                }
            }
            this.contexts.push({
                context: context,
                appContext: appContext,
                global: global,
                doc: doc
            });
        },
        onBuildHeader: function (evt) {
            if (evt && evt.data && evt.data.styleSheets) {
                const sheets = evt.data.styleSheets;
                for (let i = 0; i < sheets.length; i++) {
                    const sheet = sheets[i];
                    evt.data.styleSheets[i] = this.resolve(sheet);
                }
            }
        },
        onModuleUpdated: function (event) {
            _.each(this.contexts, function (context) {
                context.appContext.onModuleUpdated(event);
            }, this);
        },
        /**
         * @TODO move to xcf
         * @param event
         */
        onDriverVariableChanged: function (event) {
            _.each(this.contexts, function (context) {
                if (!context.context.isDesignMode()) {
                    context.appContext.publish('onDriverVariableChanged', event);
                }
            }, this);
        },
        onContextDestroyed: function (context) {
            const entry = _.find(this.contexts, {
                context: context
            });
            if (entry) {
                if (context.systemEvents) {
                    _.each(context.systemEvents, function (handle) {
                        if (handle && handle.remove) {
                            handle.remove();
                        } else {
                            console.error('have invalid handle!');
                        }
                    });
                }
                this.contexts.remove(entry);
            }
        },
        getVariable: function (deviceId, driverId, variableId) {
            const deviceManager = this.ctx.getDeviceManager();
            const device = deviceManager.getDeviceById(deviceId);
            let result = null;

            if (!device) {
                return null;
            }

            let driverScope = device.driver;
            //not initiated driver!
            if (driverScope && driverScope.blockScope) {
                driverScope = driverScope.blockScope;
            }

            if (!driverScope) {
                if (device) {
                    var driverId = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                    driverScope = this.ctx.getBlockManager().getScope(driverId);
                    result = driverScope.getVariableById(driverId + '/' + variableId);
                }
            }
            return result;
        },

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Widget's blocks
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getBlock: function (url) {
            return this.ctx.getDeviceManager().getBlock(url);
        },
        wireNode: function (widget, event, block, editorContext, appContext, params) {
            debugWiring && console.log('wireNode: ' + event);

            const thiz = this;
            let rejectFunction = null;
            let onBeforeRun = null;

            if (params) {
                if (event === types.EVENTS.ON_DRIVER_VARIABLE_CHANGED) {
                    const varParams = params.params;
                    const variableId = varParams[2];
                    rejectFunction = function (evt) {
                        const variable = evt.item;
                        const _variableIn = thiz.getBlock(variableId);
                        if (_variableIn && variable && _variableIn.id === variable.id) {
                            return false;
                        }
                        if (variable.id === variableId) {
                            return false;
                        }
                        return true;
                    };
                    onBeforeRun = function (block, evt) {
                        const variable = evt.item;
                        block.override = {
                            variables: {
                                value: variable.value
                            }
                        };
                    }
                }
            }
            if (!widget) {
                console.error('have no widget for event ' + event);
                return;
            }
            if (!block) {
                console.error('have no block for event ' + event);
                return;
            }
            if (!event) {
                console.error('have no event');
                return;
            }

            if (!_.isString(event)) {
                console.error('event not string ', event);
                return;
            }

            if (!editorContext.systemEvents) {
                editorContext.systemEvents = [];
            }

            //special treatment
            if (widget.tagName === 'BODY' && widget._destroyHandles) {
                widget._destroyHandles();
                delete widget['__setup'];
            }

            if (!widget['__setup']) {
                widget['__setup'] = {};
            }



            if (widget['__setup'][block.id]) {
                console.error('did wire ' + block.name + ' to ' + widget.id + '_' + widget['wire id'], widget);
                return;
            }

            widget['__setup'][block.id] = true;
            if (!widget['wire id']) {
                widget['wire id'] = utils.createUUID();
            }

            debugWiring && console.log('wire node : ' + event + ' ' + block.name + ' _ ' + widget['wire id']);
            const context = widget;
            if (block && context) {
                block.context = context;
                block._targetReference = context;
            }
            /**
             *
             * @param event
             * @param value: original event data
             * @param block
             * @param widget
             */
            const run = function (event, value, block, widget) {
                debugWiring && console.log('run ! ' + event);
                //filter, in design mode, we ain't do anything
                if (event !== 'load' && editorContext && editorContext.isDesignMode) {
                    if (editorContext.isDesignMode()) {
                        return;
                    }
                }
                //filter, custom reject function
                if (rejectFunction) {
                    const abort = rejectFunction(value);
                    if (abort) {
                        return;
                    }
                }
                if (block._destroyed) {
                    console.log('run failed block invalid');
                    return;
                }
                if (!block.enabled) {
                    return;
                }

                debugRun && console.log('run ! ' + event + ' for block ' + block.name + ':' + block.id);

                //setup variables
                const context = widget;

                let result;

                if (block && context) {
                    block.context = context;
                    block._targetReference = context;

                    if (onBeforeRun) {
                        onBeforeRun(block, value);
                    }
                    result = block.solve(block.scope, {
                        highlight: true,
                        args: [value]
                    });
                    debugBlox && console.log('run ' + block.name + ' for even ' + event, result);
                }
            };

            //patch the target
            if (!widget.subscribe) {
                utils.mixin(widget, EventedMixin.prototype);
            }

            let _target = widget.domNode || widget;
            const _event = event;
            let _isWidget = widget.declaredClass || widget.startup;
            const _hasWidgetCallback = widget.on != null && widget['on' + utils.capitalize(_event)] != null;
            let _handle = null;
            const _isDelite = _target.render != null && _target.on != null;


            if (_isWidget &&
                //dijit
                (widget.baseClass && widget.baseClass.indexOf('dijitContentPane') != -1)
                //delite
                ||
                widget.render != null || widget.on != null) {
                _isWidget = false; //use on
            }

            if (_target) {
                //plain node
                if (!_isDelite && (!_hasWidgetCallback || !_isWidget)) {

                    if (utils.isSystemEvent(event)) {
                        const _handler = function (evt) {
                            run(event, evt, block, widget);
                        }.bind(this);
                        _handle = widget.subscribe(event, _handler);
                        block._on('destroy', function () {
                            console.error('block destroyed');
                            _handle.remove();
                        })
                    } else {
                        _handle = widget.__on(_target, event, function (evt) {
                            run(event, evt, block, widget);
                        });
                    }
                } else {
                    _target = widget;
                    const useOn = true;
                    if (useOn) {
                        if (!_isDelite) {
                            const _e = 'on' + utils.capitalize(_event);

                            widget[_e] = function (val, nada) {
                                if (_target.ignore !== true) {
                                    run(event, val);
                                }
                            }
                        } else {

                            if (utils.isSystemEvent(event)) {
                                _handle = _target.subscribe(event, function (evt) {
                                    run(event, evt, block, widget);
                                }.bind(this), widget);

                            } else {
                                if (utils.isNativeEvent(event)) {
                                    event = event.replace('on', '');
                                }
                                _handle = _target.on(event, function (evt) {
                                    let value = evt.target.value;

                                    if ("checked" in evt.target) {
                                        value = evt.target.checked;
                                    }
                                    run(event, value, block, widget);
                                }.bind(this));
                            }
                        }
                    } else {
                        widget['on' + utils.capitalize(_event)] = function (val) {
                            if (_target.ignore !== true) {
                                run(event, val);
                            }
                        }
                    }
                }
                if (_handle) {
                    if (widget.addHandle) {
                        widget.addHandle(event, _handle);
                    }
                    if (!block._widgetHandles) {
                        block._widgetHandles = [];
                    }
                    block._widgetHandles.push(_handle);
                } else {
                    console.error('wire widget: have no handle', widget);
                }
            }
        },
        wireWidget: function (editorContext, ctx, scope, widget, node, event, group, appContext, params) {
            const blocks = scope.getBlocks({
                group: group
            });
            if (!blocks || !blocks.length) {
                debugBlox && console.log('have no blocks for group : ' + group);
            }
            for (let j = 0; j < blocks.length; j++) {
                const block = blocks[j];
                debugBlox && console.log('wire block : ' + block.name + ' for ' + event, block);
                this.wireNode(widget.domNode, event, block, editorContext, appContext, params);
            }
        },
        wireWidget2: function (editorContext, ctx, scope, appContext, widget) {

            const allGroups = scope.allGroups(), widgets = [];

            const getParams = function (group) {
                group = group || "";

                let event = null;
                let widgetId = null;
                const parts = group.split('__');
                let params = [];

                //no element:
                if (parts.length == 1) {
                    event = parts[0];
                    widgetId = 'body';
                    const _body = editorContext.rootWidget;
                    _body.domNode.runExpression = editorContext.global.runExpression;
                }

                if (parts.length == 2) {
                    let blockUrl;
                    //can be: event & block url: onDriverVariableChanged__variable://deviceScope=user_devices&device=bc09b5c4-cfe6-b621-c412-407dbb7bcef8&driver=9db866a4-bb3e-137b-ae23-793b729c44f8&driverScope=user_drivers&block=2219d68b-862f-92ab-de5d-b7a847930a7a
                    //can be: widget id & event: btnCurrentFileName__load
                    if (parts[1].indexOf('://') !== -1) {
                        event = parts[0];
                        widgetId = 'body';
                        blockUrl = parts[1];
                    } else {
                        event = parts[1];
                        widgetId = parts[0];

                    }
                    if (blockUrl) {
                        const url_parts = utils.parse_url(blockUrl);
                        const url_args = utils.urlArgs(url_parts.host);
                        params = [
                            url_args.device.value,
                            url_args.driver.value,
                            blockUrl
                        ]
                    }
                }

                if (parts.length == 3) {
                    event = parts[1];
                    widgetId = parts[0];
                    const _blockUrl = parts[2];
                    const _url_parts = utils.parse_url(_blockUrl);
                    const _url_args = utils.urlArgs(_url_parts.host);
                    params = [
                        _url_args.device.value,
                        _url_args.driver.value,
                        _blockUrl
                    ]
                }

                if (parts.length == 5) {
                    event = parts[1];
                    widgetId = parts[0];
                    params = [
                        parts[2],
                        parts[3],
                        parts[4]
                    ]
                }

                if (event && widgetId) {
                    let widget = Widget.byId(widgetId);
                    if (widgetId === 'body') {
                        widget = editorContext.rootWidget;
                    }
                    return {
                        event: event,
                        widgetId: widgetId,
                        widget: widget,
                        params: params
                    }
                }
                return null;
            };

            for (var i = 0; i < allGroups.length; i++) {
                const group = allGroups[i];
                const params = getParams(group);
                if (!params) {
                    console.error('invalid params for group ' + group);
                    continue;
                }
                if (params.widget == widget) {
                    this.wireWidget(editorContext, ctx, scope, params.widget, params.widget.domNode, params.event, group, appContext, params);
                    params.widget && widgets.indexOf(params.widget) == -1 && widgets.push(params.widget);
                }
            }

            for (var i = 0; i < widgets.length; i++) {
                var widget = widgets[i];
                if (widget.domNode) {
                    widget = widget.domNode;
                }
                if (widget.__didEmitLoad) {
                    return;
                }
                widget.__didEmitLoad = true;
                if (widget.emit) {
                    widget.emit('load', widget);
                }
            }
        },
        wireScope: function (editorContext, ctx, scope, appContext) {
            debugWiring && console.log('wire scope ' + scope.id);
            const allGroups = scope.allGroups(), thiz = this, widgets = [];

            const getParams = function (group) {
                group = group || "";

                let event = null;
                let widgetId = null;
                const parts = group.split('__');
                let params = [];

                //no element:
                if (parts.length == 1) {
                    event = parts[0];
                    widgetId = 'body';
                    const _body = editorContext.rootWidget;
                    _body.domNode.runExpression = editorContext.global.runExpression;
                }

                if (parts.length == 2) {
                    let blockUrl;
                    //can be: event & block url: onDriverVariableChanged__variable://deviceScope=user_devices&device=bc09b5c4-cfe6-b621-c412-407dbb7bcef8&driver=9db866a4-bb3e-137b-ae23-793b729c44f8&driverScope=user_drivers&block=2219d68b-862f-92ab-de5d-b7a847930a7a
                    //can be: widget id & event: btnCurrentFileName__load
                    if (parts[1].indexOf('://') !== -1) {
                        event = parts[0];
                        widgetId = 'body';
                        blockUrl = parts[1];
                    } else {
                        event = parts[1];
                        widgetId = parts[0];

                    }
                    if (blockUrl) {
                        const url_parts = utils.parse_url(blockUrl);
                        const url_args = utils.urlArgs(url_parts.host);
                        params = [
                            url_args.device.value,
                            url_args.driver.value,
                            blockUrl
                        ]
                    }
                }

                //scripted__onDriverVariableChanged__variable://deviceScope=user_devices&device=bc09b5c4-cfe6-b621-c412-407dbb7bcef8&driver=9db866a4-bb3e-137b-ae23-793b729c44f8&driverScope=user_drivers&block=2219d68b-862f-92ab-de5d-b7a847930a7a
                if (parts.length == 3) {
                    event = parts[1];
                    widgetId = parts[0];
                    const _blockUrl = parts[2];
                    const _url_parts = utils.parse_url(_blockUrl);
                    const _url_args = utils.urlArgs(_url_parts.host);
                    params = [
                        _url_args.device.value,
                        _url_args.driver.value,
                        _blockUrl
                    ]
                }

                if (parts.length == 5) {
                    event = parts[1];
                    widgetId = parts[0];
                    params = [
                        parts[2],
                        parts[3],
                        parts[4]
                    ]

                }

                if (event && widgetId) {
                    let widget = Widget.byId(widgetId);

                    if (widgetId === 'body') {
                        widget = editorContext.rootWidget;
                    }
                    return {
                        event: event,
                        widgetId: widgetId,
                        widget: widget,
                        params: params
                    }
                }

                return null;
            };

            const wireBlock = function (block) {
                block._on(types.EVENTS.ON_ITEM_REMOVED, function (evt) {
                    try {
                        if (block._widgetHandles) {
                            const _handles = block._widgetHandles;
                            for (let i = 0; i < _handles.length; i++) {
                                if (_handles[i].remove) {
                                    _handles[i].remove();
                                }
                            }
                            delete block._widgetHandles;

                        }
                    } catch (e) {
                        console.error('troubble!' + e, e);
                    }
                }, this);
            };
            for (var i = 0; i < allGroups.length; i++) {
                const group = allGroups[i];
                const params = getParams(group);
                if (!params) {
                    console.error('invalid params for group ' + group);
                    continue;
                }

                if (params.widget) {
                    this.wireWidget(editorContext, ctx, scope, params.widget, params.widget.domNode, params.event, group, appContext, params);
                } else {
                    console.warn('have no widget for group ' + group, params);
                }

                const blocks = scope.getBlocks({
                    group: group
                });

                if (!blocks || !blocks.length) {
                    console.warn('have no blocks for group : ' + group);
                }
                for (let j = 0; j < blocks.length; j++) {
                    const block = blocks[j];
                    wireBlock(block);
                }

                params.widget && widgets.indexOf(params.widget) == -1 && widgets.push(params.widget);
            }

            for (var i = 0; i < widgets.length; i++) {
                let widget = widgets[i];

                if (widget.domNode) {
                    widget = widget.domNode;
                }
                if (widget.__didEmitLoad) {
                    return;
                }
                debugBoot && console.log('emit load', widget);
                widget.__didEmitLoad = true;

                if (widget.nodeName === 'BODY') {
                    $(widget.nodeName).trigger('load');
                } else {
                    if (widget.emit) {
                        widget.emit('load', widget);
                    }
                }
            }
            scope._on(types.EVENTS.ON_ITEM_REMOVED, function (evt) {});
            scope._on(types.EVENTS.ON_ITEM_ADDED, function (evt) {
                const params = getParams(evt.item.group);
                if (params && params.widget) {

                    const item = evt.item;
                    debugBlox && console.log('on item added', arguments);
                    thiz.wireNode(params.widget.domNode, params.event, evt.item, editorContext, params);
                    wireBlock(evt.item);
                }
            });
        },
        onSceneBlocksLoaded: function (evt) {
            const context = evt.context, appContext = evt.appContext, appSettings = evt.appSettings, ctx = evt.ctx, global = evt.global, document = evt.doc, blockScopes = evt.blockScopes, thiz = this;

            if (evt.context.widgetsReadyDfd) {
                evt.context.widgetsReadyDfd.then(function () {

                    if (appContext.didWireWidgets) {
                        return;
                    }
                    appContext.didWireWidgets = true;

                    for (let i = 0; i < blockScopes.length; i++) {
                        const scope = blockScopes[i];
                        scope.global = global;
                        scope.document = context.getDocument();
                        scope.editorContext = context;
                        scope.appContext = appContext;
                        thiz.wireScope(context, thiz.ctx, scope, appContext);
                    }
                });
            }
        },
        onItemRemoved: function (evt) {},
        /**
         * Init is being called upon {link:xide/manager/Context}::initManagers()
         *
         */
        init: function () {
            this.subscribe("/davinci/ui/context/loaded", this.onContextLoaded);
            this.subscribe([
                types.EVENTS.ON_GET_LOADER_PACKAGES, //add more custom packages
                types.EVENTS.ON_BUILD_HEADER, //hook into the documents header build
                types.EVENTS.ON_BUILD_DOJO_CONFIG, //hook into davinci/ve/Context dojoConfig completion to resolve variables
                types.EVENTS.ON_SET_SOURCE_DATA, //resolve the documents resource variables
                types.EVENTS.ON_SET_DOJO_URL, //update dojo url
                types.EVENTS.ON_MODULE_UPDATED, //forward module updates to application
                types.EVENTS.ON_CONTEXT_DESTROYED,
                types.EVENTS.ON_DRIVER_VARIABLE_CHANGED,
                types.EVENTS.ON_ITEM_REMOVED
            ]);
        }
    });
});