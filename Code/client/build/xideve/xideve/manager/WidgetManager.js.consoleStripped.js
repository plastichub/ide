/** @module xideve/manager/WidgetManager **/
define("xideve/manager/WidgetManager", [
    'dcl/dcl',
    "dojo/_base/lang",
    'dojo/dom-construct',
    "xide/manager/ManagerBase",
    "xide/mixins/ReloadMixin",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xide/layout/ContentPane',
    'xide/widgets/_MenuMixin2',
    'xaction/Action',
    './WidgetManagerBlox',
    './WidgetManagerJS',
    'xide/mixins/VariableMixin',
    "davinci/ve/metadata",
    "xdojo/has!xtrack?xide/interface/Track",
    "xide/interface/Route",
    "xide/noob",
    'xide/data/Reference',
    'xide/data/ReferenceMapping',
    "xide/mixins/ActionProvider"
], function (dcl, lang, domConstruct, ManagerBase,
    ReloadMixin, types, utils, factory,
    ContentPane, _MenuMixin,
    Action,
    WidgetManagerBlox,
    WidgetManagerJS, VariableMixin,
    Metadata, Track, Route, noob, Reference,
    ReferenceMapping, ActionProvider) {

    const contextMenu = false;
    const extendProperties = false;
    const REFERENCE_CLASS = dcl([Reference, ReferenceMapping], {});
    let TRACKING_IMPL = null;
    if (Track) {
        TRACKING_IMPL = {
            track: function () {
                return true;
            },
            getTrackingCategory: function () {
                return utils.capitalize(this.beanNamespace);
            },
            getTrackingEvent: function () {
                return types.ACTION.OPEN;
            },
            getTrackingLabel: function (item) {
                return this.getMetaValue(item, types.DRIVER_PROPERTY.CF_DRIVER_NAME);
            },
            getActionTrackingUrl: function (command) {
                return lang.replace(
                    this.breanScheme + '?action={action}&' + this.beanUrlPattern, {
                        //id: item.id,
                        action: command
                    });
            },
            getTrackingUrl: function (item) {
                return lang.replace(
                    this.breanScheme + '{view}/' + this.beanUrlPattern, {
                        id: item.id,
                        view: 'settings'
                    });
            }
        };
    }

    /**
     * Extend standard action visibility for widget properties
     */
    utils.mixin(types.ACTION_VISIBILITY, {
        /**
         * Action visibility 'WIDGET_PROPERTY'
         * @memberOf module:xide/types/ACTION_VISIBILITY
         */
        WIDGET_PROPERTY: 'WIDGET_PROPERTY'
    });

    const _foo = null,
        _nativeEvents = {
            "onclick": _foo,
            "ondblclick": _foo,
            "onmousedown": _foo,
            "onmouseup": _foo,
            "onmouseover": _foo,
            "onmousemove": _foo,
            "onmouseout": _foo,
            "onkeypress": _foo,
            "onkeydown": _foo,
            "onkeyup": _foo,
            "onfocus": _foo,
            "onblur": _foo,
            "onchange": _foo
        };
    /**
     * Handler for davinci context and visual editor events. Currently just a stub. Concrete impl. happens in app subclasses.
     *
     * @class module:xide/manager/WidgetManager
     *
     * @augments  module:xide/manager/ManagerBase
     * @augments  module:xide/widgets/_MenuMixin
     * @augments  module:xide/mixins/ActionMixin
     */
    //ActionMixin
    return dcl([Route.dcl, ActionProvider.dcl, ManagerBase, ReloadMixin.dcl, WidgetManagerBlox, WidgetManagerJS, _MenuMixin, VariableMixin.dcl, Track ? dcl(Track.dcl, TRACKING_IMPL) : noob.dcl], {
        declaredClass: "xideve/manager/WidgetManager",
        /**
         * currentContext holds the current davinci.ve.Context instance
         */
        currentContext: null,

        _currentWidget: null,
        /**
         * This class has actions for this visibility:
         */
        visibility: types.ACTION_VISIBILITY.WIDGET_PROPERTY,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onReloaded: function () {},

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Event callbacks
        //
        /////////////////////////////////////////////////////////////////////////////////////

        /**
         * @TODO: Currently wrong place to dance to create block palettes
         *
         *
         * Event when an editor create a 'library container' for palette, outline etc...
         *
         * This will add shared blocks 'xblox' into an additional palette, so a user
         * just drop in his 'scripts' onto widgets
         *
         * The current procedure is:
         *
         * 1. let ContextManager load all the scene's blocks
         * 2. publish 'createBlockPalette', wait for consumers to fill it up with items
         * 3. when any consumer has blocks for this editor, then actually create the palette.
         *
         *
         *
         * @param evt {Object} event data
         * @param evt.item {File} the file object of the editor
         * @param evt.container {LayoutContainer} the container created (Accordion, Tab,..)
         * @param evt.editor {VisualEditor} the editor
         * @param evt.context {xide/manager/Context} the root context of the editor
         */
        onLibraryContainerCreated: function (evt) {},
        /**
         * Event when an editor context and its containing application has been fully
         * loaded.
         *
         * @param evt {Object} event data
         * @param evt.context {davinci/ve/Context} the davinci context
         * @param evt.appContext {xapp/manager/Context} the xapp context
         * @param evt.settings {Object} the application's settings
         * @param evt.ctx {xide/manager/Context} the root context (IDE context)
         * @param evt.global {Object} the loaded document's global
         * @param evt.blockScopes {xblox/model/Scope[]} the loaded xblox scopes
         *
         */
        onContextReady: function (evt) {

        },
        _completeEventInfo: function (params) {


            const thiz = this;
            //set to context to the selected widget
            const expressionContext = params.target || null;

            //get davinci context
            const widgetContext = params.widget ? params.target.getContext() : null;



            if (!widgetContext.editor) {
                params.editor = widgetContext._edit_context.editor;
            } else {
                params.editor = widgetContext.editor;
            }

            //get global
            const global = widgetContext ? widgetContext.getGlobal() : null;

            //get app context
            const appContext = widgetContext.appContext;

            //the actual application
            const app = appContext.application;

            let node = expressionContext.domNode || expressionContext;
            if (expressionContext.isDijitWidget) {
                node = expressionContext.dijitWidget.domNode;
            }



            params.appSettings = app.settings; //the applications settings
            params.appContext = appContext;
            params.app = app;
            params.global = global;
            params.node = node;
            params.widgetContext = expressionContext;
            params.dijitWidget = expressionContext.isDijitWidget ? expressionContext.dijitWidget : null;

            return params;


        },
        onWidgetPropertyChanged: function (evt) {

            const params = this._completeEventInfo(evt);

            // 0 && console.log('widget changed ' , params);

            const prop = params.widgetProperty,
                node = params.node,
                widget = params.dijitWidget,
                _isNative = prop in _nativeEvents,
                global = params.global;


            if (prop && node) {


                if (_isNative && widget && widget.on) {

                    node.removeAttribute(prop);

                    if (node[prop]) {
                        node.removeEventListener(prop, node[prop]);
                    }
                    node[prop] = null; //

                    global.dojo.setAttr(params.node, params.widgetProperty, '');

                    global.dojo.setAttr(params.node, params.widgetProperty, params.value);

                    /*
                    if(widget['__' + prop]){
                        widget['__' + prop].remove();
                    }

                    var _evt = '' + prop.replace('on','');
                    widget['__' + prop] = widget.on(_evt,function(){
                        // 0 && console.log('asdfasdf');
                        global.eval(params.value,widget);

                    });
                    */
                }


                //params.node.removeEventListener(prop);

                /*
                node[params.widgetProperty] = null;

                params.node[prop] = function(){
                     0 && console.log('noob');
                };
                params.global.dojo.setAttr(params.node,params.widgetProperty,params.value);
                */
            }

        },
        /**
         * Callback when a widget property has been rendered. This is common entry only. It
         * @param evt
         */
        onWidgetPropertyRendered: function (evt) {

            //  0 && console.log('widget prop rendered',evt.row.type);

            if (!extendProperties) {
                //return;
            }
            //avoid multiple handling
            if (evt.wmHandled) {
                return;
            }

            evt.wmHandled = true;

            const prop = evt['row'];

            //react per widget property type
            switch (prop.type) {
                /*
                case 'file':
                {
                    this.renderFileWidget(prop, evt['node'], evt['view']);
                    break;
                }

                case 'block':
                {
                    this.renderBlockWidget(prop, evt['node'], evt['view']);
                    break;
                }

                case 'number':
                {
                    this.onNumberPropertyRendered(prop, evt['node'], evt['view'],evt['row']);
                    break;
                }*/
                case 'widgetState':
                    {
                        this.onStatePropertyRendered(prop, evt['node'], evt['view'], evt['row']);
                        break;
                    }
                case 'iconClass':
                    {
                        this.onIconClassPropertyRendered(prop, evt['node'], evt['view'], evt['row']);
                        break;
                    }
                case 'state':
                    {
                        this.onEventPropertyRendered2(prop, evt['node'], evt['view'], evt['row']);
                        break;
                    }
                default:
                    {}
            }
        },
        onEventPropertyRendered2: function () {
            //  0 && console.log('onEventPropertyRendered', arguments);
        },

        onIconClassPropertyRendered: function (prop, node, view, row) {
            //  0 && console.log('on onIconClassPropertyRendered');
            const where = node;
            if (!node.iconPicker) {
                const iconPicker = $(node).iconpicker({
                    selectedCustomClass: 'label label-success',
                    showFooter: false
                });

                node.iconPicker = iconPicker;
                iconPicker.on('iconpickerSelected', function (e) {
                    const value = e.iconpickerValue;
                    const valuesObject = {};
                    valuesObject['iconClass'] = 'fa ' + value;
                    const command = new davinci.ve.commands.ModifyCommand(iconPicker._widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: iconPicker._context.editor_id,
                        command: command
                    }]);
                });
                /*
                iconPicker.on('change', function () {
                    var value = selectize.getValue();
                    var valuesObject = {};
                    valuesObject['state'] = value;
                    var command = new davinci.ve.commands.ModifyCommand(selectize._widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: selectize._context.editor_id,
                        command: command
                    }]);
                });
                */

            }
            if (view._widget) {
                const widget = utils.getNode(view._widget);
                node.iconPicker._widget = view._widget;
                node.iconPicker._context = view.context;
            }
        },
        onStatePropertyRendered: function (prop, node, view, row) {
            //  0 && console.log('onStatePropertyRendered');
            const where = node;
            if (!node._selectize) {
                const selectize = $(node).selectize({
                    value: "",
                    delimiter: ',',
                    persist: false,
                    maxItems: 1,
                    create: true,
                    sortField: 'text'
                }).data().selectize;

                node._selectize = selectize;
                selectize.on('change', () => {
                    let value = selectize.getValue();
                    if(value==='None'){
                        value = '';
                    }
                    const valuesObject = {};
                    valuesObject['state'] = value;
                    const command = new davinci.ve.commands.ModifyCommand(selectize._widget, valuesObject, null);
                    dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                        source: selectize._context.editor_id,
                        command: command
                    }]);
                });

            }
            node._selectize.setValue(['None'], true);

            if (view._widget) {
                const widget = utils.getNode(view._widget);
                if (widget) {
                    if (widget.getStates) {
                        const states = widget.getStates();
                        _.each(states, (state) => {
                            node._selectize.addOption({
                                value: state.name,
                                text: state.name
                            });
                        });
                    }
                    node._selectize.addOption({
                        value: 'None',
                        text: 'None'
                    });
                    node._selectize.setValue(widget.state, true);
                    node._selectize._widget = view._widget;
                    node._selectize._context = view.context;
                }
            }
        },
        /**
         *
         * Default actions for widget/node property. This should
         * support :
         * - 'Bind to Javascript'  and a few variants:
         *  - 'Expression'
         *  - 'Bind to data or store' (bi-direction double xwire with possibly in/outbound filters)
         *  - 'XBlox'
         */
        defaultNumberActions: function (context) {

            const thiz = this;

            const _handlerJS = function (command, menu, owner, menuItem, action) {
                thiz.onJavascript(action.context);
            };

            const _js = Action.createDefault('Javascript', 'fa-code', 'Widget/Bind to Javascript', 'widgetPropertyAction', _handlerJS);
            _js.setVisibility(this.visibility, {});
            _js.context = context;



            const _handlerBlox = function (command, menu, owner, menuItem, action) {
                thiz.onXBlox(action.context);
            };

            const _blox = Action.createDefault('XBlox', 'fa-play-circle-o', 'Widget/Bind to XBlox', 'widgetPropertyAction', _handlerBlox);
            _blox.setVisibility(types.ACTION_VISIBILITY.WIDGET_PROPERTY, {});
            _blox.context = context;


            const _actions = [
                _js,
                _blox
            ];

            return _actions;
        },
        /**
         * 
         * @param menu
         * @param prop
         * @param view
         * @param row
         * @private
         */
        _showContextActions: function (menu, prop, view, row) {

            if (!contextMenu) {
                return;
            }

            if (!menu.actions) {

                const actions = this.defaultNumberActions({
                    prop: prop,
                    view: view,
                    widget: view._widget, //current selection
                    row: row
                });

                //tell every body, xblox and others might add some more actions
                this.publish(types.EVENTS.ON_SET_WIDGET_PROPERTY_ACTIONS, {
                    actions: actions,
                    property: prop,
                    view: view,
                    menu: menu
                });

                this._renderActions(actions, menu, null);

                menu.actions = actions;
            }
        },
        /**
         * Create context menu on the widget property wizard button. Render
         * the actions onOpen.

         * @param widget
         * @param prop
         * @param view
         * @private
         */
        _createContextMenu: function (widget, prop, view, row) {

            if (!contextMenu) {
                return;
            }

            //skip if we've got a menu already
            if (widget._ctxMenu) {
                return;
            }
            //custom open, forward
            const _openFn = function (menu) {
                this._showContextActions(menu, prop, view, row);
            }.bind(this);

            //done in _MenuMixin
            widget._ctxMenu = this.createContextMenu(widget, _openFn, {
                leftClickToOpen: true
            });

            widget.row = row;


            return widget._ctxMenu;

        },
        /**
         *
         * @param node
         * @param prop
         * @param view
         * @param _button
         */
        doWidgetPropWizard: function (node, prop, view, _button, row) {
            this._createContextMenu(_button, prop, view, row);
        },
        /**
         *
         * @param prop
         * @param node
         * @param view
         */
        renderBlockWidget: function (prop, node, view) {
            //runBlox('workspace://min.xblox','e813a85f-7511-723f-bc1f-c13698456a5b',this)
             0 && console.log('block rendered', arguments);
            //x-script : view._widget.domNode
        },
        /**
         * Callback when a 'number' property has been rendered. This adds a 'wizard' button
         * right side of the widget property value field.
         *
         * @param prop
         * @param node
         * @param view
         */
        onNumberPropertyRendered: function (prop, node, view, row) {

            if (!extendProperties) {
                return;
            }
            let where = node;

            if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                where = node.parentNode.nextSibling;
            }

            //var _button = factory.createButton(where, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);

            const _button = factory.createSimpleButton('', 'fa-magic', null, {
                style: "margin-left:11px !important; border-radius:2px;"
            }, where);

            this.doWidgetPropWizard(node, prop, view, _button, row);
        },
        /**
         * Callback when a ui-designer's context has been fully loaded. The context is tracked.
         * This is currently loading additional modules!
         * @param context
         */
        onContextLoaded: function (context) {

            this.currentContext = context;
        },
        /**
         * Callback when a 'number' property has been rendered. This adds a 'wizard' button
         * right side of the widget property value field.
         *
         * @param prop
         * @param node
         * @param view
         */
        onEventPropertyRendered: function (prop, node, view) {
            if (!extendProperties) {
                return;
            }
            let where = node;

            if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                where = node.parentNode.nextSibling;
            }

            //var _button = factory.createButton(where, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);
            const _button = factory.createSimpleButton('', 'fa-magic', null, {
                style: "margin-left:11px !important; border-radius:2px;"
            }, where);

            this.doWidgetPropWizard(node, prop, view, _button);
        },
        /**
         * Event when the event property section is being rendered,
         * fish out
         * @param evt
         */

        onEventSelectionRendered: function (evt) {

            if (!extendProperties) {
                return;
            }
             0 && console.log('onEventSelectionRendered : ', evt);
            const pageTemplate = evt.pageTemplate;

            for (let i = 0; i < pageTemplate.length; i++) {

                const page = pageTemplate[i];
                const propNode = utils.find('.propertyExtra', dojo.byId(page.rowId), true);

                //var _button = factory.createButton(propNode, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);
                const _button = factory.createSimpleButton('', 'fa-magic', null, {
                    style: "margin-left:11px !important; border-radius:2px;",
                    id: 'btn_sel' + utils.createUUID()
                }, propNode);


                this.doWidgetPropWizard(null, null, evt.eventSelection, _button, page);

            }

        },

        /**
         * Callback when user selected another widget.
         * @param evt {Array} the current ui-designer selection.
         */
        onSelectionChanged: function (evt) {

             0 && console.log('selection changed');
        },

        /**
         * Callback when user selected a widget
         * @param evt {Array}
         */
        onWidgetSelected: function (evt) {

            if (!extendProperties) {
                return;
            }
             0 && console.log('widget selected', evt);

            if (evt.isFake === true) {
                return;
            }

            this._currentWidget = evt.selection;

            const mainView = this.ctx.getApplication().mainView;

            const selection = evt.selection;



            //update description in main view
            // when a widget is selected, pick the description from meta data and place it the right - bottom panel
            const layoutRightMain = mainView.getLayoutRightMain ? mainView.getLayoutRightMain() : null;
            if (evt && lang.isArray(selection) && selection.length == 1) {

                const widgetProps = selection[0];

                let _description = Metadata.getOamDescriptivePropertyForType(widgetProps.type, 'description');
                if (!_description) {
                    //Metadata.getWidgetDescriptorForType(widgetProps.type) ||

                    _description = Metadata.getOamDescriptivePropertyForType(widgetProps.type, 'title');
                    if (_description && _description.value) {
                        _description = _description.value;
                    }
                } else {

                    _description = _description.value;

                }

                if (!_description && widgetProps.metadata && widgetProps.metadata.description && widgetProps.metadata.description.value) {
                    _description = widgetProps.metadata.description.value;
                }

                //render description on info panel
                if (_description) {
                    if (mainView) {
                        //this should return a tab-container
                        if (mainView.getRightBottomTarget) {
                            const parent = mainView.getRightBottomTarget(true, this._doOpenRight);
                            if (parent) {
                                if (this._doOpenRight) {
                                    this._doOpenRight = false;
                                }
                                const _pane = utils.addWidget(ContentPane, {
                                    delegate: this,
                                    style: "width:100%;height:100%;padding:0px",
                                    parentContainer: parent,
                                    title: 'Description',
                                    closable: true,
                                    splitter: true
                                }, this, parent, true);
                                _pane.containerNode.innerHTML = _description;
                            }
                        }
                    }
                }

                //now publish on event bus
                selection[0].beanType = 'widget';

                this.publish(types.EVENTS.ON_ITEM_SELECTED, {
                    item: selection[0],
                    beanType: 'widget',
                    view: evt.context.editor
                }, evt.context.editor); //re-root owner of the publisher to the current editor




            } else {
                mainView.getRightBottomTarget && mainView.getRightBottomTarget(true, false);
            }

            const editor = evt.context.getVisualEditor();
            editor._emit('selectionChanged', {
                selection: selection
            });

            layoutRightMain && layoutRightMain.resize();


        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX factory and utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Renders a file picker button at a given node
         *
         * @param prop {Array} : property data
         * @param node {HTMLElement}
         * @param view {davinci/ve/views/SwitchingStyleView}
         */
        renderFileWidget: function (prop, node, view) {
            let where = node;
            if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                where = node.parentNode.nextSibling; //should be the
            }

            const selectButton = factory.createButton(domConstruct.create('div'), "el-icon-folder-open", "elusiveButton", "", "", function () {
                this.onSelectFile(node, prop, view);
            }.bind(this), this);

            dojo.place(selectButton.domNode, where, 'after');

        },

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Standard XIDE context calls
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Init is being called upon {link:xide/manager/Context}::initManagers()
         *
         */
        init: function () {
            this.subscribe("/davinci/ui/onSelectionChanged", this.onSelectionChanged);
            this.subscribe("/davinci/ui/widgetSelected", this.onWidgetSelected);
            this.subscribe("/davinci/ui/context/loaded", this.onContextLoaded);
            this.subscribe([
                types.EVENTS.WIDGET_PROPERTY_RENDERED, //extend style view additional actions per widget/node properties
                types.EVENTS.ON_EVENT_SELECTION_RENDERED,
                types.EVENTS.ON_WIDGET_PROPERTY_CHANGED,
                types.EVENTS.ON_LIBRARY_CONTAINER_CREATED,
                types.EVENTS.ON_CONTEXT_READY,
                types.EVENTS.ON_RENDER_WELCOME_GRID_GROUP
            ]);
            this.registerRoutes();
        },

        //
        // ─── XTRACK ──────────────────────────────────────────────────────
        //

        /**
         * Impl. router interface
         * @param {module:xide/interface/RouteDefinition} route
         */
        handleRoute: function (route) {
            switch (route.parameters.view) {
                case 'settings':
                    {
                        return this.openItemSettings(this.getDriverById(route.parameters.id));
                    }
            }
        },
        registerRoutes: function () {

            this.getContext().registerRoute({
                id: 'Scene',
                path: 'scene://{view}/{id}',
                schemes: ['scenes'],
                delegate: this
            });

            this.getContext().registerRoute({
                id: 'New Scene',
                path: 'File/New Scene',
                schemes: [''],
                delegate: this
            });
        },
        /**
         * @param evt {object}
         * @param evt.item {object}
         * @param evt.items {array|null}
         * @param evt.store {module:xide/data/_Base}
         */
        onRenderWelcomeGridGroup: function (evt) {
            const ctx = this.getContext();
            const trackingManager = ctx.getTrackingManager();
            const router = ctx.getRouter();
            const store = evt.store;
            const self = this;
            const grid = evt.grid;
            switch (evt.item.group) {
                case 'New':
                    {
                        const actions = [];
                        actions.push(this.createAction({
                            label: 'New Scene',
                            command: 'File/New Scene',
                            icon: 'fa-magic',
                            mixin: {
                                addPermission: true
                            }
                        }));

                        _.map(actions, function (item) {
                            return store.putSync(utils.mixin(item, {
                                url: item.command,
                                parent: 'New',
                                group: null,
                                label: item.title,
                                owner: self,
                                runAction: function (action, item, from) {
                                    return DriverTreeView.prototype.runAction.apply(DriverTreeView.prototype, [{
                                        command: this.url
                                    }, item, from]);
                                }
                            }));
                        });
                        break;
                    }
                case 'Recent':
                    {
                        if (!trackingManager) {
                            break;
                        }
                        //head
                        store.putSync({
                            group: 'Scenes',
                            label: 'Scenes',
                            url: 'Scenes',
                            parent: 'Recent',
                            directory: true
                        });

                        //collect recent
                        let devices = trackingManager.filterMin({
                            category: 'Driver'
                        }, 'counter', 1); //the '2' means here that those items had to be opened at least 2 times

                        //sort along usage
                        devices = trackingManager.most(null, devices);
                        //make last as first
                        trackingManager.last(devices);

                        return;
/*
                        //map
                        devices = _.map(devices, function (item) {
                            item.parent = 'Recent';
                            item.icon = 'fa-exchange';
                            return item;
                        });

                        //@TODO: find a way to split impl. from interface
                        const deviceActions = _.map(DriverTreeView.getActions.apply(DriverTreeView.prototype, [$('<div/>')[0], DriverTreeView.DefaultPermissions,
                            grid
                        ]), function (action) {
                            action.custom = true;
                            return action;
                        });
                        let deviceStoreItem = null;
                        _.each(devices, function (device) {
                            const route = router.match({
                                path: device.url
                            });
                            if (!route) {
                                 0 && console.warn('cant get a router for ' + device.url);
                                return;
                            }
                            deviceStoreItem = self.routeToReference(route);
                            if (!deviceStoreItem) {
                                return;
                            }
                            store.removeSync(device.url, true);
                            let reference = new REFERENCE_CLASS({
                                url: device.url,
                                enabled: true,
                                label: device.label,
                                parent: 'Drivers',
                                _mayHaveChildren: false,
                                virtual: true,
                                icon: 'fa-exchange',
                                owner: self,
                                getActions: function () {
                                    return deviceActions;
                                },
                                runAction: function (action, item, from) {
                                    return DriverView2.prototype.runAction.apply(DriverView2.prototype, [action, item, from]);
                                }
                            });
                            reference = store.putSync(reference, false);
                            if (deviceStoreItem && deviceStoreItem.addReference) {
                                deviceStoreItem.addReference(reference, {
                                    properties: {
                                        "name": true,
                                        "value": true,
                                        "state": true
                                    },
                                    onDelete: true,
                                    onChange: true
                                }, true);
                            } else {
                                console.error('device store item has no addReference!', reference);
                            }
                        });
                        store.emit('added', {
                            target: deviceStoreItem
                        });
                        break;
                        */
                    }
            }
        },
        /**
         * @param {module:xide/interface/RouteDefinition} route
         */
        getRouteRenderParams: function (route) {
            return {
                icon: null
            };
        },
        routeToReference: function (route) {
            return this.getDriverById(route ? route.parameters.id : null);
        },
        /**
         *
         * @param item
         * @returns {*}
         */
        referenceToRoute: function (item) {
            return lang.replace(
                this.breanScheme + '{view}/' + this.beanUrlPattern, {
                    id: item.id,
                    view: 'settings'
                });
        }

    });
});