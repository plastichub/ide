/** @module xide/types
 *  @description All the package's constants and enums in C style structures.
*/
 define("xideve/types", [
     'xide/types',
     'xide/utils'
], function (types,utils) {
     utils.mixin(types,{
         VE_LAYOUT_RIGHT_CENTER_BOTTOM:'VE_LAYOUT_RIGHT_CENTER_BOTTOM',
         VE_LAYOUT_CENTER_BOTTOM:'VE_LAYOUT_CENTER_BOTTOM',
         VE_LAYOUT_CENTER_RIGHT:'VE_LAYOUT_CENTER_RIGHT',
         VE_LAYOUT_LEFT_CENTER_RIGHT:'VE_LAYOUT_LEFT_CENTER_RIGHT',
         VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM:'VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM'
     });

     /***
      * Extend core event keys for widget related events
      *
      */
     utils.mixin(types.EVENTS,{
         WIDGET_PROPERTIES_RENDERED: 'OnWidgetPropertiesRendered',
         WIDGET_PROPERTY_RENDERED: 'onWidgetPropertyRendered',
         ON_SET_WIDGET_PROPERTY_ACTIONS: 'onSetWidgetPropertyActions',
         ON_GET_LOADER_PACKAGES: 'onGetLoaderPackages',
         ON_VISUAL_EDITOR_SAVE: 'onVisualEditorSave',
         ON_BUILD_HEADER: 'onBuildHeader',
         ON_BUILD_DOJO_CONFIG: 'onBuildDojoConfig',
         ON_SET_DOJO_URL: 'onSetDojoUrl',
         ON_SET_SOURCE_DATA: 'onSetSourceData',
         ON_EVENT_SELECTION_RENDERED: 'onEventSelectionRendered',
         ON_WIDGET_PROPERTY_CHANGED: 'onWidgetPropertyChanged',
         ON_LIBRARY_CONTAINER_CREATED:'onLibraryContainerCreated',
         ON_CONTEXT_READY:'onContextReady',
         ON_CONTEXT_DESTROYED:'onContextDestroyed',
         ON_BUILD_WIDGET_BLOCK_LIBRARY:'onBuildWidgetBlockLibrary',
         ON_SHOW_BLOCK_SMART_INPUT:'onShowBlockSmartInput',
         ON_WIDGET_CHANGED:'onWidgetChanged'
     });
     return types;
});