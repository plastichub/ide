define("xideve/palette/Grid", [
    'xdojo/declare',
    'xide/types',
    'xide/utils',

    "davinci/Workbench",

    "davinci/Runtime",
    "xide/lodash",


    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    "xide/widgets/_Widget",

    'xblox/views/ThumbRenderer',
    "xblox/views/DnD",
    'dojo/dom-construct',
    'xgrid/Selection',
    'xgrid/Search',
    'xgrid/KeyboardNavigation',
    'xide/$',

    "dijit/focus",
    "davinci/ui/dnd/DragManager",
    "davinci/ve/utils/GeomUtils",
    "davinci/ui/dnd/DragSource",
    "davinci/ve/metadata",
    "davinci/ve/tools/CreateTool",
    'dojo/_base/lang',
    'xaction/DefaultActions'
], function (declare, types, utils, Workbench, Runtime, _,
    TreeRenderer, Grid, MultiRenderer, _Widget, ThumbRenderer, BlocksDnD, domConstruct,
    Selection, Search, KeyboardNavigation, $, FocusUtils, DragManager, GeomUtils, DragSource, Metadata, CreateTool, lang, DefaultActions) {

        const ACTION = types.ACTION;
        const DefaultPermissions = [
            //ACTION.EDIT,
            //ACTION.RENAME,
            ACTION.RELOAD,
            ACTION.DELETE,
            //ACTION.CLIPBOARD,
            // ACTION.SELECTION,
            // ACTION.TOOLBAR,
            // ACTION.HEADER,
            ACTION.SEARCH,
            //ACTION.CONTEXT_MENU,
            // 'File/New'
        ]

        const runAction = function (who, action) {
            console.log('run action ', action);
            const selection = who.getSelection();
            const deleteState = (state) => {
                const libMgr = who.ctx.getLibraryManager();
                const store = libMgr.getStore();
                libMgr.setSetting(state.name, null);
                who.collection.removeSync(state.id);
                who.refresh();
            }
            switch (action.command) {
                case 'File/New Group':
                    {
                        selection.forEach(deleteState);
                        break;
                    }

                case 'File/Delete':
                    {
                        selection.forEach(deleteState);
                        break;
                    }

            }
            //return who.inherited(arguments);
        }

        window.pRunAction = runAction;

        const _context = () => {
            try {
                return Runtime.currentEditor.getContext();
            } catch (e) {

            }
        }
        const ThumbClass = declare('xideve.palette.ThumbRenderer2', [ThumbRenderer], {
            resizeThumb: true,
            __type: 'thumb',
            deactivateRenderer: function () {
                $(this.domNode.parentNode).removeClass('metro');
                $(this.domNode).css('padding', '');
                this.isThumbGrid = false;
            },
            activateRenderer: function () {
                $(this.contentNode).css('padding', '8px');
                this.isThumbGrid = true;
                this.refresh();
            },
            _getIconUri: function (uri, fallbackUri) {
                if (uri) {
                    // maybe already resolved
                    if (uri.indexOf("http") === 0) {
                        return uri;
                    }
                    return Workbench.location() + uri;
                }
                return require.toUrl("davinci/" + fallbackUri);
            },
            _setIconProperties: function (opt) {
                // for small icons, first look for small icons, else look for large icons, else use file_obj.gif
                opt.icon = opt.iconBase64 || (opt.icon && this._getIconUri(opt.icon, "ve/resources/images/file_obj.gif")) ||
                    opt.iconLargeBase64 ||
                    (opt.iconLarge && this._getIconUri(opt.iconLarge, "ve/resources/images/file_obj.gif")) ||
                    this._getIconUri(opt.icon, "ve/resources/images/file_obj.gif");
                // for large icons, first look for small icons, else look for large icons, else use file_obj.gif
                opt.iconLarge = opt.iconLargeBase64 ||
                    (opt.iconLarge && this._getIconUri(opt.iconLarge, "ve/resources/images/file_obj.gif")) ||
                    opt.iconBase64 || this._getIconUri(opt.icon, "ve/resources/images/file_obj.gif");
            },
            template2: '' +
                '<span class="paletteItemSelectionContainer"></span>' +
                '<span class="paletteItemNormalContainer">' +
                '<span class="paletteItemImageContainer" style="margin-top:8px">' +
                '<img class="paletteItemImage" border="0"/>' +
                '</span>' +
                '<span class="paletteItemLabelContainer">' +
                '<span class="paletteItemLabel"></span>' +
                '</span>' +
                '</span>' +
                '',
            /**
             * Override renderRow
             * @param obj
             * @returns {*}
             */
            renderRow: function (obj) {
                var div = domConstruct.create('div', {
                    className: "tile widget form-control dojoyPaletteCommon dojoyPaletteItem maqPaletteSection_desktop dojoyPaletteItemSunken widgetBase",
                    // className: "tile widget form-control",
                    // style: 'float:left;padding:3px; width:100px; margin:4px'
                }),
                    label = obj.name,
                    imageClass = 'paletteItemImage';

                div.innerHTML = this.template2;
                var iconStyle = 'float:left; margin-left:3px;margin-top:3px;margin-right:3px;text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: inherit;opacity: 0.4;width:35px';
                this._setIconProperties(obj);

                if (obj.icon.indexOf('base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR') !== -1) {
                    obj.icon = '';
                }
                var label = div.querySelector('.paletteItemLabel');

                label.appendChild(dojo.doc.createTextNode(obj.name));
                var img = div.querySelector('img');
                if (obj && obj.iconHTML) {
                    dojo.destroy(img);
                    var container = div.querySelector('.paletteItemImageContainer');
                    container.innerHTML = item.iconHTML;
                } else {
                    img.src = obj.icon;
                }
                return div;
            }
        });

        const GroupRenderer = declare('xideve.palette.GroupRenderer', TreeRenderer, {
            itemClass: ThumbClass,
            groupClass: TreeRenderer,
            renderRow: function (obj) {
                return (!obj.group ? this.itemClass : this.groupClass).prototype.renderRow.apply(this, arguments);
            }
        });
        var Implementation = {},
            renderers = [GroupRenderer, ThumbClass, TreeRenderer],
            multiRenderer = declare.classFactory('xideve.palette.PaletteGridRenderer', {}, renderers, MultiRenderer.Implementation);

        const SharedDndGridSource = BlocksDnD.BlockGridSource;

        const GridClass = Grid.createGridClass('xideve.palette.PaletteGrid', Implementation, {
            SELECTION: {
                CLASS: Selection
            },
            KEYBOARD_SELECTION: true,
            //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            COLUMN_HIDER: false,
            WIDGET: {
                CLASS: _Widget
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            SEARCH: {
                CLASS: Search
            }
        }, {
                RENDERER: multiRenderer
            }, {
                renderers: renderers,
                selectedRenderer: GroupRenderer,
                permissions: DefaultPermissions
            });

        const GridCSourcelass = declare('ActionGrid', GridClass, {
            formatColumn: function (field, value, obj) {
                var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
                if (renderer.formatColumn_) {
                    var result = renderer.formatColumn.apply(arguments);
                    if (result) {
                        return result;
                    }
                }
                if (obj.renderColumn_) {
                    var rendered = obj.renderColumn.apply(this, arguments);
                    if (rendered) {
                        return rendered;
                    }
                }
                switch (field) {
                    case "title":
                        {
                            value = obj.group ? ('<span style="float:left;margin-right: 10px">' + value + '</span><hr style="margin-top: 13px;margin-left: 4px"/>') : value;
                        }
                }
                return value;
            },
            _columns: {},
            postMixInProperties: function () {
                var state = this.state;
                if (state) {
                    if (state._columns) {
                        this._columns = state._columns;
                    }
                }
                this.columns = this.getColumns();
                return this.inherited(arguments);
            },
            getColumns: function (formatters) {
                var self = this;
                this.columns = [{
                    renderExpando: true,
                    label: 'Name',
                    field: 'title',
                    sortable: true,
                    formatter: function (value, obj) {
                        return self.formatColumn('title', value, obj);
                    },
                    hidden: false
                }];
                return this.columns;
            },
            dragMananager: null,
            veContext: null,
            updateDragManager: function (context) {
                if (context) {
                    this.dragMananager.document = context.getDocument();
                    const frameNode = context.frameNode;
                    if (frameNode) {
                        const coords = dojo.coords(frameNode);
                        const containerNode = context.getContainerNode();
                        this.dragMananager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                        this.dragMananager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
                    }
                }
            },
            onContextDestroyed: function (context) {
                if (context.__veDNDDevHandles) {
                    _.each(context.__veDNDDevHandles, utils.destroy);
                    context.__veDNDDevHandles;
                }
                delete this.editorContext;
            },
            onAppReady: function (evt) {
                if (this.editorContext && this.editorContext != evt.context) {
                    this.editorContext = evt.context;
                }
                if (!this.dragMananager) {
                    this.dragMananager = DragManager;
                }
                this.editorContext = evt.context;
                this.updateDragManager(evt.context);
            },
            addCustomWidget: function (lib) {
                // console.log('add custom widget', lib);
                /* make sure the pallette has loaded. if it hasnt, the init will take care of customs */
                // if (!this._loaded) return;
                if (!lib || !lib.$wm || !lib.$wm.widgets || !lib.$wm.widgets.length) {
                    return;
                }
                //const context = Runtime.currentEditor.getContext();
                const comptype = 'delite';
                const palette = this.palette;

                const $library = lib.$wm;
                const widgets = lib.$wm.widgets;
                const folderToShow = null;
                for (var w = 0; w < widgets.length; w++) {
                    var item = widgets[w];
                    for (var presetId in this.palette._presetSections) {
                        var customSection = null;
                        var sections = this.palette._presetSections[presetId];
                        if (!sections) {
                            console.error('Palette.js:addCustomWidget - no sections for comptype=' + presetId);
                        } else {
                            for (var s = 0; s < sections.length; s++) {
                                var section = sections[s];
                                if (section.id == '$$UserWidgets$$') {
                                    customSection = section;
                                    break;
                                }
                            }
                            if (!customSection) {
                                customSection = dojo.clone(palette._userWidgetSection);
                                customSection.preset = palette._presetSections[presetId];
                                customSection.presetId = presetId;
                                customSection.items = [];
                                sections.push(customSection);
                                this.customSection = customSection;
                                if (this.palette._presetCreated[presetId]) {
                                    var orderedDescriptors = [customSection];
                                    customSection._created = true;
                                    customSection.subsections = [];
                                    customSection.group = 4;

                                }
                            }
                            var includesValue = 'type:' + item.type;
                            if (customSection.includes.indexOf(includesValue) < 0) {
                                customSection.includes.push(includesValue);
                                item.$library = $library;
                                item.section = customSection;
                                item._paletteItemGroup = palette._paletteItemGroupCount++;
                                customSection.items.push(item);
                                var name = 'custom';
                                var folder = null;
                                if (!this.__c) {
                                    customSection._created = true;
                                    customSection.subsections = [];
                                    customSection.group = 4;
                                    palette.createRootItem(customSection);
                                    this.set('collection', this.collection.filter({
                                        parentId: '.'
                                    }));
                                    this.__c = true;
                                }
                                palette.createItem(item, customSection);
                            }
                        }
                    }
                }
                if (folderToShow) {
                    // open the currently active custom widget folder after creating a new custom widget
                    folderToShow.showHideFolderContents(true);
                }

                setTimeout(() => {
                    this.set('collection', this.collection.filter({
                        parentId: '.'
                    }))
                }, 1000);
            },
            runAction: function (action) {
                return pRunAction(this, action);
                return this.inherited(arguments);
            },
            _shouldDisableStates: function () {
                const selection = this.getSelection() || [];
                const first = selection[0];
                if (first) {
                    if (first.id === 'Styles') {
                        return true;
                    }
                    const parent = first.isChildOfId('Styles');
                    if (parent) {
                        return false;
                    }
                    if (first.id === 'Styles') {
                        return false;
                    }
                }
                return true;
            },
            _getActions: function () {
                const actions = [];
                const defaultMixin = {
                    addPermission: true
                };
/*
                actions.push(this.createAction({
                    label: 'New Group',
                    command: 'File/New Group',
                    icon: 'fa-file-o',
                    tab: 'Home',
                    group: 'New',
                    mixin: utils.mixin({
                        quick: true
                    }, defaultMixin),
                    shouldDisable: () => this._shouldDisableStates
                }));*/
                /*
                actions.push(this.createAction({
                    label: 'New State',
                    command: 'File/New State',
                    icon: 'fa-magic',
                    tab: 'Home',
                    group: 'New',
                    mixin: utils.mixin({
                        quick: true
                    }, defaultMixin),
                    shouldDisable: () => this._shouldDisableStates
                }));
                */
                return actions;
            },
            startup: function (item) {

                const libMgr = this.ctx.getLibraryManager();
                const store = libMgr.getStore();
                store.on('update', () => {
                    this.refresh();
                });
                this._on('addAction', (action) => {
                    if (action.command === 'File/Delete') {
                        action.shouldDisable = () => {
                            return this._shouldDisableStates();
                        };
                    }
                })
                const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
                this.addActions(_defaultActions);
                this.addActions(this._getActions());
                this.subscribe("/davinci/ui/addedCustomWidget", this.addCustomWidget);
                const _dragStart = function (from, node, context, e, proto, userData, properties) {
                    proto = utils.clone(proto);
                    utils.mixin(proto.properties, properties);
                    const data = e.dragSource.data;
                    userData = data.userData || userData;
                    userData.onCreate = function () { }
                    Metadata.getHelper(proto.type, 'tool').then(function (ToolCtor) {
                        // Copy the data in case something modifies it downstream -- what types can data.data be?
                        const tool = new (ToolCtor || CreateTool)(dojo.clone(proto), userData);
                        context.setActiveTool(tool);
                    }.bind(this));

                    // Sometimes blockChange doesn't get cleared, force a clear upon starting a widget drag operation
                    context.blockChange(false);
                    // Place an extra DIV onto end of dragCloneDiv to allow
                    // posting a list of possible parent widgets for the new widget
                    // and register the dragClongDiv with Context
                    if (e._dragClone) {
                        $(e._dragClone).addClass('paletteDragContainer');
                        //domClass.add(e._dragClone, 'paletteDragContainer');
                        dojo.create('div', {
                            className: 'maqCandidateParents'
                        }, e._dragClone);
                    }
                    //FIXME: Attach dragClone and event listeners to tool instead of context?
                    context.setActiveDragDiv(e._dragClone);
                };
                const _dragEnd = function () {

                    if (FocusUtils.curNode && FocusUtils.curNode.blur) {
                        FocusUtils.curNode.blur();
                    }
                };

                TreeRenderer.prototype.activateRenderer.apply(this);

                const makeDNDG = function (from, node, context, proto, userData, properties) {
                    const clone = node.domNode;
                    const ds = new DragSource(node.domNode, "component", node, clone);
                    ds.targetShouldShowCaret = true;
                    ds.returnCloneOnFailure = false;
                    const result = {
                        handles: [],
                        context: context,
                        node: clone,
                        ds: ds,
                        destroy: function () {
                            delete this.context;
                            _.each(this.handles, dojo.disconnect);
                            delete this.handles;
                            delete this.node.__vednd;
                            delete this.node;
                            delete this.ds;
                        }
                    };
                    const handles = result.handles;

                    handles.push(dojo.connect(ds, "onDragStart", dojo.hitch(from, function (e) {
                        _dragStart(from, node, context, e, proto, userData, properties);
                    })));

                    handles.push(dojo.connect(ds, "onDragEnd", dojo.hitch(from, function (e) {
                        _dragEnd();
                    })));
                    handles.push(dojo.connect(node.domNode, "onmouseover", function (e) {
                        node._mouseover = true;
                    }));

                    handles.push(dojo.connect(node.domNode, "onmouseout", function (e) {
                        node._mouseover = false;
                    }));

                    handles.push(dojo.connect(node.domNode, "onmousedown", function (e) {
                        const DragManager = from.dragMananager;
                        DragManager.document = context.getDocument();
                        const frameNode = context.frameNode;
                        if (frameNode) {
                            const coords = dojo.coords(frameNode);
                            const containerNode = context.getContainerNode();
                            DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                            DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
                        }
                    }));

                    return result;
                };

                this._showHeader(false);
                // this.showToolbar(false);
                var res = this.inherited(arguments);
                _.each(this.getRows(), function (row) {
                    this.expand(row, false);
                }, this);
                $(this.domNode).addClass('blockPalette');

                this.subscribe([
                    types.EVENTS.ON_APP_READY,
                    types.EVENTS.ON_CONTEXT_DESTROYED
                ]);
                const thiz = this;
                const ctx = thiz.ctx;

                this._on('selectionChanged', function (evt) {
                    const selection = evt.selection;
                    // console.log('selected : ', selection);
                    const item = selection && selection.length == 1 ? selection[0] : null;
                    if (!item) {
                        return;
                    }
                    const row = thiz.row(item);
                    const node = row.element;

                    if (row.element.__vednd) {
                        return;
                    }
                    node.__vednd = true;
                    const _props = {}
                    const _editorContext = thiz.editorContext;
                    const rowObject = {
                        domNode: node
                    };
                    if (_editorContext) {
                        if (!_editorContext.__veDNDDevHandles) {
                            _editorContext.__veDNDDevHandles = [];
                        }
                        const _protoDefault = {
                            properties: item.properties || {},
                            type: item.type,
                            userData: {}
                        };
                        if (!item.group) {
                            _editorContext.__veDNDDevHandles.push(makeDNDG(this, rowObject, _editorContext, _protoDefault, item, _props));
                        }
                    }
                });
                this.onAppReady({
                    context: _context()
                });
                return res;
            }
        });
        return GridCSourcelass;
    });