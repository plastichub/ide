define("xideve/Embedded", [
    'dojo/_base/declare',
    'davinci/davinci_',
    'dojo/Deferred',
    'davinci/ve/metadata',
    'davinci/Runtime',
    'require',
    "dojo/_base/connect",
    'davinci/Workbench',
    "davinci/ve/metadata",
    "xideve/views/StyleView"
], function (declare, davinci, Deferred, metadata, Runtime, require, connect, Workbench, Metadata, StyleView) {
    var handleIoError = function (deferred, reason) {
        /*
         *  Called by the subscription to /dojo/io/error , "
         *  /dojo/io/error" is sent whenever an IO request has errored.
         *	It passes the error and the dojo.Deferred
         *	for the request with the topic.
         */

        if (reason.status == 401 || reason.status == 403) {
            //sessionTimedOut();
            // Only handle error if it is as of result of a failed XHR connection, not
            // (for example) if a callback throws an error. (For dojo.xhr, def.cancel()
            // is only called if connection fails or if it times out.)
        } else if (deferred.canceled === true) {
            // Filter on XHRs for maqetta server commands.  Possible values which we
            // test for:
            //     cmd/findResource
            //     ./cmd/createResource
            //     http://<host>/maqetta/cmd/getComments
            var reCmdXhr = new RegExp('(^|\\.\\/|' + document.baseURI + '\\/)cmd\\/');
            var url = deferred.ioArgs.url;
            if (reCmdXhr.test(url)) {
                // Make exception for "getBluePageInfo" because it regularly gets cancelled
                // by the type ahead searching done from the combo box on the 3rd panel of
                // the R&C wizard. The cancellation is not really an error.
                if (url.indexOf("getBluePageInfo") >= 0) {
                    return;
                }
            } else {
                // Must not be a Maqetta URL (like for JSONP on GridX), so skip
                return;
            }

            Runtime.handleError(reason.message);
             0 && console.warn('Failed to load url=' + url + ' message=' + reason.message +
                ' status=' + reason.status);
        }
    };
    var debug = false;
    /**
     *
     * @class xide/ve/Embedded
     */
    return declare("xideve/Embedded", null, {

        _loadActionClasses: function () {

            var editorID = 'davinci.ve.HTMLPageEditor'; //this.editorExtension.id;
            var editorActions = [];

            var extensions = Runtime.getExtensions('davinci.editorActions', function (ext) {
                if (editorID == ext.editorContribution.targetID) {
                    editorActions.push(ext.editorContribution);
                    return true;
                }
            });
            if (editorActions.length == 0) {
                var extensions = Runtime.getExtension('davinci.defaultEditorActions', function (ext) {
                    editorActions.push(ext.editorContribution);
                    return true;
                });
            }
            var libraryActions = Metadata.getLibraryActions('davinci.editorActions', editorID);
            // Clone editorActions, otherwise, library actions repeatedly get appended to original plugin object
            editorActions = dojo.clone(editorActions);
            if (editorActions.length > 0 && libraryActions.length) {
                // We want to augment the action list, so let's clone the
                // action set before pushing new items onto the end of the
                // array
                dojo.forEach(libraryActions, function (libraryAction) {
                    var Workbench = require("davinci/Workbench");
                    if (libraryAction.action) {
                        Workbench._loadActionClass(libraryAction);
                    }
                    if (libraryAction.menu) {
                        for (var i = 0; i < libraryAction.menu.length; i++) {
                            var subAction = libraryAction.menu[0];
                            if (subAction.action) {
                                Workbench._loadActionClass(subAction);
                            }
                        }
                    }
                    editorActions[0].actions.push(libraryAction);
                });
            }

            // 0 && console.log('editor actions : ',editorActions);

            return editorActions;
        },

        started: function () {

        },
        onReady: function (dfd) {

            this._loadActionClasses();

            var _c = document.getElementById('focusContainer');
            if (!_c) {
                _c = dojo.create('div', {
                    'class': 'focusContainer',
                    id: 'focusContainer'
                }, document.body);
                Workbench.focusContainer = _c;
            }

            Workbench._state = {
                "activeEditor": null,
                "editors": [],
                "nhfo": {
                    "project1": {
                        "device": "iphone",
                        "layout": "flow",
                        "themeSet": {
                            "name": "(none)",
                            "desktopTheme": "claro",
                            "mobileTheme": [{
                                    "theme": "android",
                                    "device": "Android"
                                },
                                {
                                    "theme": "blackberry",
                                    "device": "BlackBerry"
                                },
                                {
                                    "theme": "ipad",
                                    "device": "iPad"
                                },
                                {
                                    "theme": "iphone",
                                    "device": "iPhone"
                                },
                                {
                                    "theme": "iphone",
                                    "device": "other"
                                }
                            ]
                        }
                    }
                },
                "project": "project1",
                "id": "",
                "Fields": []
            };

            Workbench.run2();

            this.started();

            dfd.resolve(this);

        },

        /**
         *
         * @param ctx
         * @param mini
         * @param cmdOffset
         * @param userBaseUrl
         */
        start: function (ctx, mini, cmdOffset, userBaseUrl) {

            debug &&  0 && console.log('start ve with cmd offset', cmdOffset);

            var thiz = this,
                dfd = new Deferred();

            Runtime.cmdOffset = cmdOffset || '';
            Runtime.loadPlugins();
            Runtime.initialPerspective = "davinci.ve.pageDesign";

            if (mini === true) {
                Runtime.isLocalInstall = true;
                // Needed by review code
                Runtime.userName = 'none';
                Runtime.userEmail = 'none';
                Runtime.run();
                Workbench.ctx = ctx;
                thiz.onReady();
                return;
            }

            var fileManager = ctx.getFileManager();
            var serviceClass = 'XApp_XIDE_Workbench_Service';
            var ready = function (result) {
                Runtime.userBaseUrl = userBaseUrl;
                Runtime._initializationInfo = result;
                var userInfo = result.userInfo;
                Runtime.isLocalInstall = userInfo.userId == 'maqettaUser';

                // Needed by review code
                Runtime.userName = userInfo.userId;
                Runtime.userEmail = userInfo.email;

                var metaRoot = require.toUrl('xideve/metadata/');
                metadata.init(ctx, metaRoot);
                Runtime.run();
                Workbench.ctx = ctx;
                thiz.onReady(dfd);
                Runtime.subscribe("/davinci/states/state/changed",
                    function (e) {
                        var currentEditor = Runtime.currentEditor;
                        // ignore updates in theme editor and review editor
                        if ((currentEditor.declaredClass != "davinci.ve.themeEditor.ThemeEditor" &&
                                currentEditor.declaredClass != "davinci.review.editor.ReviewEditor") /*"davinci.ve.VisualEditor"*/ ) {
                            currentEditor.visualEditor.onContentChange.apply(currentEditor.visualEditor, arguments);
                        }
                    }
                );
                // bind overlay widgets to corresponding davinci states. singleton; no need to unsubscribe
                connect.subscribe("/davinci/states/state/changed", function (args) {

                    //FIXME: This is page editor-specific logic within Workbench.
                    var context = (Runtime.currentEditor && Runtime.currentEditor.declaredClass == "davinci.ve.PageEditor" &&
                        Runtime.currentEditor.visualEditor && Runtime.currentEditor.visualEditor.context);
                    if (!context) {
                        return;
                    }
                    var prefix = "_show:",
                        widget, dvWidget, helper;
                    var thisDijit = context ? context.getDijit() : null;
                    var widgetUtils = require("davinci/ve/widget");
                    if (args.newState && !args.newState.indexOf(prefix)) {
                        widget = thisDijit.byId(args.newState.substring(6));
                        dvWidget = widgetUtils.getWidget(widget.domNode);
                        helper = dvWidget.getHelper();
                        helper && helper.popup && helper.popup(dvWidget);
                    }
                    if (args.oldState && !args.oldState.indexOf(prefix)) {
                        widget = thisDijit.byId(args.oldState.substring(6));
                        dvWidget = widgetUtils.getWidget(widget.domNode);
                        helper = dvWidget.getHelper();
                        helper && helper.tearDown && helper.tearDown(dvWidget);
                    }
                });
                // bind overlay widgets to corresponding davinci states. singleton; no need to unsubscribe
                connect.subscribe("/davinci/ui/repositionFocusContainer", function (args) {
                    Workbench._repositionFocusContainer();
                });

            };

            var inner = fileManager.callMethodEx(serviceClass, 'getInfo', ['nada'], ready, true);
            Runtime.subscribe('/dojo/io/error', handleIoError); // /dojo/io/error" is sent whenever an IO request has errored.
            return dfd;
        }
    });
});