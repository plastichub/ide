require({cache:{
'xnode/main':function(){
define([
    "dojo/_base/kernel",
    "xnode/manager/NodeServiceManager",
    "xnode/manager/NodeServiceManagerUI",
    "xnode/views/NodeServiceView",
    "xnode/component"
], function(dojo){
    return dojo.xnode;
});

},
'xnode/manager/NodeServiceManager':function(){
/** @module xnode/manager/NodeServiceManager **/
define([
    "dcl/dcl",
    "xide/manager/ServerActionBase",
    "xide/manager/BeanManager",
    'xide/types',
    'xide/factory',
    'xide/data/Memory',
    'xide/client/WebSocket',
    'xdojo/has',
    'xide/factory/Clients',
    'dojo/Deferred',
    'xdojo/has!xnode-ui?./NodeServiceManagerUI'
], function (dcl, ServerActionBase, BeanManager, types,factory,Memory,WebSocket,has,Clients,Deferred,NodeServiceManagerUI) {
    var bases = [ServerActionBase, BeanManager];

    if(NodeServiceManagerUI){
        bases.push(NodeServiceManagerUI);
    }
    /**
     * Manager dealing with Node-Services though PHP shell (XPHP). This is is a typical
     * 'bean-manager' implementation.
     *
     * @class module: xnode/manager/NodeServiceManager
     */

    var NodeServiceManager = dcl(bases, {
        declaredClass:"xnode.manager.NodeServiceManager",
        serviceClass: 'XIDE_NodeJS_Service',
        cookiePrefix: 'nodeJSServices',
        singleton: true,
        serviceView: null,
        clients: null,
        beanNamespace: 'serviceConsoleView',
        consoles: {},
        /**
         * Create a socket client to the service (service shell) if applicable.
         * @param item
         * @returns {*}
         */
        createClient: function (item) {
            if (!item.info) {
                console.error('NodeJs service has no host info');
                return;
            }
            !this.clients && (this.clients = {});
            var hash = this.getViewId(item);
            if (this.clients[hash]) {
                return this.clients[hash];
            }
            var client = new WebSocket({});
            this.clients[hash] = client;
            client.init({
                options: {
                    host: item.info.host,
                    port: item.info.port,
                    debug: {
                        "all": false,
                        "protocol_connection": true,
                        "protocol_messages": true,
                        "socket_server": true,
                        "socket_client": true,
                        "socket_messages": true,
                        "main": true
                    }
                }
            });
            client.connect();
            return client;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Public API
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getStore: function () {
            return this.store;
        },
        /***
         * Common function that this instance is in a valid state
         * @returns {boolean}
         */
        isValid: function () {
            return this.store != null;
        },
        /***
         * Init our store
         * @param data
         * @returns {xide.data.Memory}
         */
        initStore: function (data) {
            var sdata = {
                identifier: "name",
                label: "Name",
                items: data
            };

            this.store = new Memory({
                data: sdata,
                idProperty:'name'
            });
            return this.store;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Main entries, called by the context
        //
        /////////////////////////////////////////////////////////////////////////////////////
        init: function () {
            var dfd = new Deferred();
            var self = this;
            if(this.serviceObject && this.serviceObject.__init) {
                this.serviceObject.__init.then(function () {
                    self.ls().then(function () {
                        dfd.resolve();
                    });
                });
            }else if(this.services){//exported apps have the services already
                return this.ls();
            }
            return dfd;

        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Server methods
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getDefaults: function (readyCB) {
            return this.callMethodEx(null, 'getDefaults', null, readyCB, true);
        },
        checkServer: function (settings, readyCB) {
            return this.callMethodEx(null, 'checkServer', [settings], readyCB, true);
        },
        runServer: function (settings, readyCB) {
            return this.callMethodEx(null, 'runDebugServer', [settings], readyCB, true);
        },
        runDebug: function (settings, readyCB) {
            return this.callMethodEx(null, 'run', [settings], readyCB, true);
        },
        stopServer: function (services, readyCB) {
            return this.callMethodEx(null, 'stop', [services], readyCB, true);
        },
        startServer: function (services, readyCB) {
            return this.callMethodEx(null, 'start', [services], readyCB, true);
        },
        /***
         * ls is enumerating all drivers in a given scope
         * @param readyCB   {function}
         * @param errorCB   {function}
         * @param emit   {Boolean}
         * @returns {*}
         */
        ls: function (readyCB, errorCB, emit) {
            var thiz = this;
            var dfd = null;
            function ready(data) {
                thiz.rawData = data;
                thiz.initStore(data);
                if (emit !== false) {
                    thiz.publish(types.EVENTS.ON_NODE_SERVICE_STORE_READY, {store: thiz.store});
                }
                if (readyCB) {
                    readyCB(data);
                }
            }
            if(this.services){
                dfd = new Deferred();
                ready(this.services);
                dfd.resolve();
                return dfd;
            }
            dfd = this.runDeferred(null, 'ls');
            try {
                dfd.then(function (data) {
                    ready(data);
                });
            }catch(e){
                logError(e,"error loading store");
            }
            return dfd;
        }
    });
    return NodeServiceManager;
});

},
'xnode/manager/NodeServiceManagerUI':function(){
/** @module xnode/manager/NodeServiceManager **/
define([
    "dcl/dcl",
    'xide/encoding/MD5',
    'xide/types',
    'xide/utils',
    "dojo/cookie",
    "dojo/json",
    "xide/lodash",
    'xdojo/has!xnode-ui?xide/views/ConsoleView',
    'xdojo/has!xnode-ui?xnode/views/NodeServiceView'
], function (dcl,MD5, types, utils,cookie, json,_,ConsoleView,NodeServiceView) {
    /**
     * Manager dealing with Node-Services though PHP shell (XPHP). This is is a typical
     * 'bean-manager' implementation.
     *
     * @class module: xnode/manager/NodeServiceManager
     */
    return dcl(null, {
        declaredClass:"xnode.manager.NodeServiceManagerUI",
        /**
         * Bean protocol implementation, only getViewId gets managed through here, the rest
         * Override getViewId in bean protocol
         * @param item
         * @returns {string}
         */
        getViewId: function (item) {
            var data = {
                info: item.info
            };
            return this.beanNamespace + MD5(JSON.stringify(data), 1);
        },
        onConsoleEnter: function (evt, what) {
            if (what.length == 0) {
                return;
            }
            var view = evt.view;
            var client = view.client;
            if (_.isString(what)) {
                client.emit(null, what);
            }
        },
        createConsole: function (what, parentContainer, client) {
            var viewId = this.getViewId(what);
            var view = utils.addWidget(ConsoleView, {
                delegate: this,
                title: what.name,
                closable: true,
                style: 'padding:0px;margin:0px;height:inherit',
                className: 'runView',
                client: client,
                item: what
            }, this, parentContainer, true);
            this.consoles[viewId] = view;
            client.delegate = view;
            return view;
        },
        openConsole: function (item) {
            var view = this.getView(item);
            view = this.createConsole(item, this.getViewTarget(), this.createClient(item));
        },
        onReload: function () {
            this.ls(function () {
                this.serviceView.refresh(this.store);
            }.bind(this));
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Storage & Persistence
        //
        /////////////////////////////////////////////////////////////////////////////////////
        loadPreferences: function () {
            var _cookie = this.cookiePrefix + '_debug_settings';
            var settings = cookie(_cookie);

            settings = settings ? json.parse(settings) : {};
            return settings;
        },
        savePreferences: function (settings) {
            var _cookie = this.cookiePrefix + '_debug_settings';
            cookie(_cookie, json.stringify(settings));
        },
        getViewTarget: function () {
            var mainView = this.ctx.getApplication().mainView;
            return mainView.getNewAlternateTarget();
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX factory and utils
        // @TODO : move it to somewhere else
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /***
         *
         * @param store
         * @param where
         */
        createServiceView: function (store, where) {
            var parent = where || this.getViewTarget();
            this.serviceView = utils.addWidget(NodeServiceView, {
                delegate: this,
                store: store,
                title: 'Services',
                closable: true,
                style:'padding:0px'
            }, this, parent, true);
        },
        openServiceView: function () {
            if (!this.isValid()) {
                var thiz = this;
                var _cb = function () {
                    thiz.createServiceView(thiz.store);
                };
                this.ls(_cb);
            } else {
                this.createServiceView(this.store);
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX Callbacks
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onStart: function (command, items, owner) {
            var itemsOut = [];
            for (var i = 0; i < items.length; i++) {
                itemsOut.push(items[i].name);
            }
            this.startServer(itemsOut, function () {
                this.onReload();
            }.bind(this));
        },
        onStop: function (command, items, owner) {
            var itemsOut = [];
            for (var i = 0; i < items.length; i++) {
                itemsOut.push(items[i].name);
            }
            this.stopServer(itemsOut, function () {
                this.onReload();
            }.bind(this));
        },
        openServices:function(){
            this.openServiceView();
        },
        getActions:function(){
            return [];
        },
        init:function(){
            var ctx = this.ctx;
            ctx.addActions(this.getActions());
        }
    });
});

},
'xnode/views/NodeServiceView':function(){
define([
    'dcl/dcl',
    "dojo/_base/declare",
    "dgrid/OnDemandGrid",
    "dgrid/Selection",
    'xide/types',
    'xide/utils',
    'xaction/Action',
    'xide/layout/Container'
], function (dcl, declare, OnDemandGrid, Selection, types, utils, Action, Container) {
    return dcl(Container, {
        declaredClass: 'xide.views.NodeServiceView',
        delegate: null,
        store: null,
        cssClass: "layoutContainer normalizedGridView",
        /**
         *
         * @param store
         */
        createWidgets: function (store) {
            var grid = new (declare([OnDemandGrid, Selection, "Keyboard"]))({
                collection: store,
                columns: {
                    Name: {
                        field: "name", // get whole item for use by formatter
                        label: "Name",
                        sortable: true
                    },
                    Status: {
                        field: "status", // get whole item for use by formatter
                        label: "Status",
                        sortable: true,
                        formatter: function (status) {
                            var tpl = '<div style=\"color:${color}\">' + utils.capitalize(status) + '</div>';
                            return utils.substituteString(tpl, {
                                color: status == 'offline' ? 'red' : 'green'
                            });
                        }
                    },
                    Clients: {
                        field: "clients", // get whole item for use by formatter
                        label: "Clients",
                        sortable: true
                    }
                }
            }, this.containerNode);

            //grid.sort("name");
            grid.refresh();
            this.grid = grid;
            this.onGridCreated(grid);
        },
        startup: function () {
            this.inherited(arguments);
            if (this.store) {
                this.createWidgets(this.store);
            }
        },
        //////////////////////////////////////////////////////////////
        //
        //  Bean protocol implementation
        //
        //////////////////////////////////////////////////////////////
        hasItemActions: function () {
            return this.getCurrentSelection(true) != null;
        },
        getItem: function () {
            return this.getCurrentSelection(true);
        },
        getItemActions: function () {
            var currentItem = this.getItem()[0];

            var actions = [],
                thiz = this.delegate;//forward to owner


            /***
             *  Service commands : stop & start
             */
            var isOnline = currentItem.status === types.SERVICE_STATUS.ONLINE;
            var dstCommand = currentItem.status == types.SERVICE_STATUS.OFFLINE ? 'start' : 'stop';
            var canServiceAction = currentItem.can[dstCommand] !== false;
            var serviceActionFunction = canServiceAction ? dstCommand == 'start' ? function (command, item, owner) {
                thiz.onStart(command, [currentItem], owner)
            } : function (command, item, owner) {
                thiz.onStop(command, [currentItem], owner);
            } : null;

            var title = currentItem.status == types.SERVICE_STATUS.OFFLINE ? 'Start' : 'Stop';
            var command = currentItem.status == types.SERVICE_STATUS.OFFLINE ? 'Start' : 'Stop';
            var icon = currentItem.status == types.SERVICE_STATUS.OFFLINE ? 'el-icon-play' : 'el-icon-stop';


            var _controlAction = Action.createDefault(title, icon, 'Edit/' + title, 'xnode', null, {
                handler: serviceActionFunction
            }).setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''}).setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {}).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, {});

            actions.push(_controlAction);

            var _reloadAction = Action.createDefault('Reload', 'fa-refresh', 'Edit/Reload', 'xnode', null, {
                handler: function () {
                    thiz.onReload();
                }
            }).setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''}).setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {}).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, {});

            actions.push(_reloadAction);

            var _consoleAction = Action.createDefault('Console', 'el-icon-indent-left', 'View/Console', 'xnode', null, {
                handler: function () {
                    thiz.openConsole(currentItem);
                },
                widgetArgs: {
                    disabled: !isOnline
                }
            }).setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''}).setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {}).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, {});

            actions.push(_consoleAction);

            if (actions.length == 0) {
                return null;
            }
            return actions;
        }

    });
});

},
'xnode/component':function(){
define([
    "dcl/dcl",
    "xide/model/Component"
], function (dcl,Component) {
    /**
     * @class xnode.component
     * @inheritDoc
     */
    return dcl(Component, {
        /**
         * @inheritDoc
         */
        beanType:'NODE_SERVICE',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        hasEditors:function(){
            return ['xnode'];
        },
        getDependencies:function(){
            return [
                'xide/xide',
                'xnode/types',
                'xnode/manager/NodeServiceManager',
                'xnode/views/NodeServiceView'
            ];
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return 'xnode';
        },
        /**
         * @inheritDoc
         */
        getBeanType:function(){
            return this.beanType;
        }
    });
});


},
'xnode/types':function(){
define([

],function(){
    return null;
});
}}});
