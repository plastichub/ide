require([
    "xwire/_Base",
    "xwire/Source",
    "xwire/Target",
    "xwire/Binding",
    "xwire/WidgetSource",
    "xwire/WidgetTarget",
    "xwire/DeviceTarget",
    "xwire/EventSource"
], function () {
}, undefined, true);    // Force synchronous loading so we don't have to wait.