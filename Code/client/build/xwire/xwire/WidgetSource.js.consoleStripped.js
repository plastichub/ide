define("xwire/WidgetSource", [
    'dcl/dcl',
    'dojo/_base/kernel'
],function(dcl,Source,dojo){
    /**
     * Event based binding source
     */
    return dcl(Source,{
        declaredClass:"xwire.WidgetSource",
        /**
         * Trigger specifies the event name
         * {String|Array}
         */
        trigger:null,
        /**
         * Dijit widget instance
         */
        object:null,
        /**
         * Widget changed its value,
         * @param value
         */
        onTriggered:function(value){
            var thiz=this;
            //skip
            if(this.object && this.object.ignore===true){
                setTimeout(function(){
                    thiz.object.ignore=false;
                },500);
                return;
            }
            /**
             * forward to owner
             */
            if(this.binding){
                this.binding.trigger({
                    value:value,
                    source:this
                });
            }
        },
        /***
         * Start will subscribe to event specified in trigger
         */
        start:function(){
            var thiz=this;
            this.handle=dojo.connect(this.object,this.trigger, function (value) {
                thiz.onTriggered(value);
            });
        },
        /**
         * Cleanup
         */
        destroy:function(){
            this.handle.remove();
        }
    });
});