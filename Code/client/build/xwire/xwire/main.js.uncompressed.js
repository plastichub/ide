define("xwire/main", [
    "xwire/Binding",
    "xwire/Source",
    "xwire/Target",
    "xwire/WidgetSource",
    "xwire/WidgetTarget",
    "xwire/EventSource",
    "xwire/DeviceTarget"
], function(){});