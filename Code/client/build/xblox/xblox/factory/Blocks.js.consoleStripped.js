define("xblox/factory/Blocks", [
    'xide/factory',
    'xide/utils',
    'xide/types',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin',
    "xblox/model/logic/CaseBlock",
    "xblox/model/Block",
    "xblox/model/functions/CallBlock",
    "xblox/model/functions/StopBlock",
    "xblox/model/functions/PauseBlock",
    "xblox/model/functions/SetProperties",
    "xblox/model/code/CallMethod",
    "xblox/model/code/RunScript",
    "xblox/model/loops/ForBlock",
    "xblox/model/loops/WhileBlock",
    "xblox/model/variables/VariableAssignmentBlock",
    "xblox/model/logic/IfBlock",
    "xblox/model/logic/ElseIfBlock",
    "xblox/model/logic/SwitchBlock",
    "xblox/model/variables/VariableSwitch",
    "xblox/model/logging/Log",
    "xblox/model/server/RunServerMethod",
    "xblox/model/server/Shell",
    "xblox/model/code/RunBlock",
    "xblox/model/events/OnEvent",
    "xblox/model/events/OnKey",
    "xblox/model/mqtt/Subscribe",
    "xblox/model/mqtt/Publish",
    "xblox/model/File/ReadJSON",
    "xcf/factory/Blocks"
], function (factory,
             utils,
             types,
             ReloadMixin, EventedMixin,
             CaseBlock,
             Block,
             CallBlock,
             StopBlock,
             PauseBlock,
             SetProperties,
             CallMethod,
             RunScript,
             ForBlock,
             WhileBlock,
             VariableAssignmentBlock,
             IfBlock,
             ElseIfBlock,
             SwitchBlock,
             VariableSwitch,
             Log,
             RunServerMethod,
             Shell,
             RunBlock,
             OnEvent,
             OnKey,
             Subscribe,
             Publish,
             ReadJSON) {

    var cachedAll = null;
    /***
     *
     * @param mixed String|Prototype
     * @param ctorArgs
     * @param baseClasses
     * @param publish
     */
    factory.createBlock = function (mixed, ctorArgs, baseClasses, publish) {
        //complete missing arguments:
        Block.prototype.prepareArgs(ctorArgs);
        var block = factory.createInstance(mixed, ctorArgs, baseClasses);
        block.ctrArgs = null;
        var newBlock;
        try {
            if (block && block.init) {
                block.init();
            }
            //add to scope
            if (block.scope) {
                newBlock = block.scope.registerBlock(block, publish);
            }
            if (block.initReload) {
                block.initReload();
            }
        } catch (e) {
            logError(e, 'create block');
        }
        return newBlock || block;
    };
    factory.clearVariables = function () {
    };
    factory.getAllBlocks = function (scope, owner, target, group, allowCache) {
        if (allowCache !== false && cachedAll != null) {
            return cachedAll;
        } else if (allowCache == false) {
            cachedAll = null;
        }
        var items = factory._getFlowBlocks(scope, owner, target, group);
        items = items.concat(factory._getLoopBlocks(scope, owner, target, group));
        items = items.concat(factory._getCommandBlocks(scope, owner, target, group));
        items = items.concat(factory._getCodeBlocks(scope, owner, target, group));
        items = items.concat(factory._getEventBlocks(scope, owner, target, group));
        items = items.concat(factory._getLoggingBlocks(scope, owner, target, group));
        items = items.concat(factory._getServerBlocks(scope, owner, target, group));
        items = items.concat(factory._getMQTTBlocks(scope, owner, target, group));
        items = items.concat(factory._getFileBlocks(scope, owner, target, group));
        cachedAll = items;
        return items;
    };
    factory._getMQTTBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'MQTT',
            iconClass: 'fa-cloud',
            items: [
                {
                    name: 'Subscribe',
                    owner: owner,
                    iconClass: 'fa-bell',
                    proto: Subscribe,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Publish',
                    owner: owner,
                    iconClass: 'fa-send',
                    proto: Publish,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'MQTT'
        });
        return items;

    };

    factory._getFileBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'File',
            iconClass: 'fa-file',
            items: [
                {
                    name: '%%Read JSON',
                    owner: owner,
                    iconClass: 'fa-file',
                    proto: ReadJSON,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'File'
        });
        return items;

    };

    factory._getServerBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Server',
            iconClass: 'el-icon-repeat',
            items: [
                {
                    name: 'Run Server Method',
                    owner: owner,
                    iconClass: 'fa-plug',
                    proto: RunServerMethod,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Shell',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: Shell,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Server'
        });
        return items;
    };
    factory._getVariableBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Flow',
            iconClass: 'el-icon-random',
            items: [
                {
                    name: 'If...Else',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: IfBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        condition: "[value]=='PW'"
                    }
                }/*,
                 {
                 name:'Switch',
                 owner:owner,
                 iconClass:'el-icon-fork',
                 proto:SwitchBlock,
                 target:target,
                 ctrArgs:{
                 scope:scope,
                 group:group
                 }
                 }
                 */
            ]
        });

        return items;
    };
    factory._getEventBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Events',
            iconClass: 'fa-bell',
            items: [
                {
                    name: 'On Event',
                    owner: owner,
                    iconClass: 'fa-bell',
                    proto: OnEvent,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'On Key',
                    owner: owner,
                    iconClass: 'fa-keyboard-o',
                    proto: OnKey,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Events'
        });

        return items;
    };
    factory._getLoggingBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Logging',
            iconClass: 'fa-bug',
            items: [
                {
                    name: 'Log',
                    owner: owner,
                    iconClass: 'fa-bug',
                    proto: Log,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Logging'
        });

        return items;
    };
    factory._getCodeBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Code',
            iconClass: 'fa-code',
            items: [
                {
                    name: 'Call Method',
                    owner: owner,
                    iconClass: 'el-icon-video',
                    proto: CallMethod,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Run Script',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: RunScript,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Run Block',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: RunBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Set Properties',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: SetProperties,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Code'
        });
        return items;
    };
    factory._getFlowBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Flow',
            iconClass: 'el-icon-random',
            items: [
                {
                    name: 'If...Else',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: IfBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        condition: "[value]=='PW'"
                    }
                },
                {
                    name: 'Switch',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: SwitchBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Variable Switch',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: VariableSwitch,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Flow'
        });
        return items;
    };
    factory._getLoopBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Loops',
            iconClass: 'el-icon-repeat',
            items: [
                {
                    name: 'While',
                    owner: owner,
                    iconClass: 'el-icon-repeat',
                    proto: WhileBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        condition: "[Volume]<=100"
                    }
                },
                {
                    name: 'For',
                    owner: owner,
                    iconClass: 'el-icon-repeat',
                    proto: ForBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        initial: '1',
                        comparator: "<=",
                        "final": '5',
                        modifier: '+1',
                        counterName: 'value'
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Loops'
        });
        return items;
    };
    factory._getMathBlocks = function (scope, owner, dstItem, group) {
        var items = [];
        items.push({
            name: 'Math',
            owner: this,
            iconClass: 'el-icon-qrcode',
            dstItem: dstItem,
            items: [
                {
                    name: 'If...Else',
                    owner: dstItem,
                    iconClass: 'el-icon-compass',
                    proto: IfBlock,
                    item: dstItem,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        return items;
    };
    factory._getTimeBlocks = function (scope, owner, dstItem, group) {
        var items = [];
        items.push({
            name: 'Time',
            owner: this,
            iconClass: 'el-icon-qrcode',
            dstItem: dstItem,
            items: [
                {
                    name: 'If...Else',
                    owner: dstItem,
                    iconClass: 'el-icon-time',
                    proto: IfBlock,
                    item: dstItem,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }

            ]
        });
        return items;
    };
    factory._getTransformBlocks = function (scope, owner, dstItem, group) {
        var items = [];
        items.push({
            name: 'Time',
            owner: this,
            iconClass: 'el-icon-magic',
            dstItem: dstItem,
            items: [
                {
                    name: 'If...Else',
                    owner: dstItem,
                    iconClass: 'el-icon-time',
                    proto: IfBlock,
                    item: dstItem,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        return items;
    };
    return factory;
});