require({cache:{
'xblox/main':function(){
define([
	"dojo/_base/kernel"
], function(){
    return dojo.xblox;
});

},
'xblox/component':function(){
define([
    "dcl/dcl",
    "xdojo/has",
    "xide/model/Component"
], function (dcl,has,Component) {

    /**
     * @class xblox.component
     * @inheritDoc
     */
    return dcl(Component, {
        /**
         * @inheritDoc
         */
        beanType:'BLOCK',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        _load:function(){
        },
        hasEditors:function(){
            return ['xblox'];
        },
        getDependencies:function(){
            if( 1 ) {
                return [
                    'xide/xide',
                    'xblox/types/Types',
                    'xblox/manager/BlockManager',
                    'xblox/manager/BlockManagerUI',
                    'xblox/embedded_ui',
                    'xblox/views/BlockGridPalette',
                    'xide/widgets/ExpressionJavaScript',
                    'xide/widgets/Expression',
                    'xide/widgets/RichTextWidget',
                    'xide/widgets/ExpressionEditor',
                    'xide/widgets/WidgetReference'
                    //'xide/widgets/DomStyleProperties',
                    //'xblox/views/BlocksFileEditor'
                    //'xide/widgets/BlockPickerWidget',
                    //'xide/widgets/BlockSettingsWidget'
                ];
            }else{
                return [
                    'xide/xide',
                    'xblox/types/Types',
                    'xblox/manager/BlockManager',
                    'xblox/embedded'
                ];
            }
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return 'xblox';
        },
        /**
         * @inheritDoc
         */
        getBeanType:function(){
            return this.beanType;
        }
    });
});


},
'xblox/types/Types':function(){
/** @module xblox/types **/
define([
    'xide/types/Types',
    'xide/utils'
], function (types, utils) {

    /**
     * The block's capabilities. This will be evaluated in the interface but also
     * by the run-time (speed ups).
     *
     * @enum {integer} module:xide/types/BLOCK_CAPABILITIES
     * @memberOf module:xide/types
     */
    var BLOCK_CAPABILITIES = {
        /**
         * No other block includes this one.
         * @constant
         * @type int
         */
        TOPMOST: 0x00004000,
        /**
         * The block's execution context can be changed to another object.
         * @constant
         * @type int
         */
        TARGET: 0x00040000,
        /**
         * The block may create additional input terminals ('reset', 'pause', ...).
         * @constant
         * @type int
         */
        VARIABLE_INPUTS: 0x00000080,
        /**
         * The block may create additional output terminals ('onFinish', 'onError').
         * @constant
         * @type int
         */
        VARIABLE_OUTPUTS: 0x00000100,
        /**
         * The block may create additional ouput parameters ('result', 'error',...).
         * @constant
         * @type int
         */
        VARIABLE_OUTPUT_PARAMETERS: 0x00000200,
        /**
         * The block may create additional input parameters.
         * @constant
         * @type int
         */
        VARIABLE_INPUT_PARAMETERS: 0x00000400,
        /**
         * The block can contain child blocks.
         * @constant
         * @type int
         */
        CHILDREN: 0x00000020,
        /**
         * Block provides standard signals ('paused', 'error').
         * @constant
         * @type int
         */
        SIGNALS: 0x00000080
    }
    /**
     * Flags to describe a block's execution behavior.
     *
     * @enum {integer} module:xide/types/RUN_FLAGS
     * @memberOf module:xide/types
     */
    var RUN_FLAGS = {
        /**
         * The block can execute child blocks.
         * @constant
         * @type int
         */
        CHILDREN: 0x00000020,
        /**
         * Block is waiting for a message => EXECUTION_STATE::RUNNING
         * @constant
         * @type int
         */
        WAIT: 0x000008000
    };

    /**
     * Flags to describe a block's execution state.
     *
     * @enum {integer} module:xide/types/EXECUTION_STATE
     * @memberOf module:xide/types
     */
    var EXECUTION_STATE = {
        /**
         * The block is doing nothing and also has done nothing. The is the default state
         * @constant
         * @type int
         */
        NONE:0x00000000,
        /**
         * The block is running.
         * @constant
         * @type int
         */
        RUNNING: 0x00000001,
        /**
         * The block is an error state.
         * @constant
         * @type int
         */
        ERROR: 0x00000002,
        /**
         * The block is in an paused state.
         * @constant
         * @type int
         */
        PAUSED: 0x00000004,
        /**
         * The block is an finished state, ready to be cleared to "NONE" at the next frame.
         * @constant
         * @type int
         */
        FINISH: 0x00000008,
        /**
         * The block is an stopped state, ready to be cleared to "NONE" at the next frame.
         * @constant
         * @type int
         */
        STOPPED: 0x00000010,
        /**
         * The block has been launched once...
         * @constant
         * @type int
         */
        ONCE: 0x80000000,
        /**
         * Block will be reseted next frame
         * @constant
         * @type int
         */
        RESET_NEXT_FRAME: 0x00800000,
        /**
         * Block is locked and so no further inputs can be activated.
         * @constant
         * @type int
         */
        LOCKED: 0x20000000	// Block is locked for utilisation in xblox
    }

    types.BLOCK_MODE = {
        NORMAL: 0,
        UPDATE_WIDGET_PROPERTY: 1
    };

    /**
     * Flags to describe a block's belonging to a standard signal.
     * @enum {integer} module:xblox/types/BLOCK_OUTLET
     * @memberOf module:xblox/types
     */
    types.BLOCK_OUTLET = {
        NONE: 0x00000000,
        PROGRESS: 0x00000001,
        ERROR: 0x00000002,
        PAUSED: 0x00000004,
        FINISH: 0x00000008,
        STOPPED: 0x00000010
    };

    utils.mixin(types.EVENTS, {
        ON_RUN_BLOCK: 'onRunBlock',
        ON_RUN_BLOCK_FAILED: 'onRunBlockFailed',
        ON_RUN_BLOCK_SUCCESS: 'onRunBlockSuccess',
        ON_BLOCK_SELECTED: 'onItemSelected',
        ON_BLOCK_UNSELECTED: 'onBlockUnSelected',
        ON_BLOCK_EXPRESSION_FAILED: 'onExpressionFailed',
        ON_BUILD_BLOCK_INFO_LIST: 'onBuildBlockInfoList',
        ON_BUILD_BLOCK_INFO_LIST_END: 'onBuildBlockInfoListEnd',
        ON_BLOCK_PROPERTY_CHANGED: 'onBlockPropertyChanged',
        ON_SCOPE_CREATED: 'onScopeCreated',
        ON_VARIABLE_CHANGED: 'onVariableChanged',
        ON_CREATE_VARIABLE_CI: 'onCreateVariableCI'
    });


    types.BlockType = {
        AssignmentExpression: 'AssignmentExpression',
        ArrayExpression: 'ArrayExpression',
        BlockStatement: 'BlockStatement',
        BinaryExpression: 'BinaryExpression',
        BreakStatement: 'BreakStatement',
        CallExpression: 'CallExpression',
        CatchClause: 'CatchClause',
        ConditionalExpression: 'ConditionalExpression',
        ContinueStatement: 'ContinueStatement',
        DoWhileStatement: 'DoWhileStatement',
        DebuggerStatement: 'DebuggerStatement',
        EmptyStatement: 'EmptyStatement',
        ExpressionStatement: 'ExpressionStatement',
        ForStatement: 'ForStatement',
        ForInStatement: 'ForInStatement',
        FunctionDeclaration: 'FunctionDeclaration',
        FunctionExpression: 'FunctionExpression',
        Identifier: 'Identifier',
        IfStatement: 'IfStatement',
        Literal: 'Literal',
        LabeledStatement: 'LabeledStatement',
        LogicalExpression: 'LogicalExpression',
        MemberExpression: 'MemberExpression',
        NewExpression: 'NewExpression',
        ObjectExpression: 'ObjectExpression',
        Program: 'Program',
        Property: 'Property',
        ReturnStatement: 'ReturnStatement',
        SequenceExpression: 'SequenceExpression',
        SwitchStatement: 'SwitchStatement',
        SwitchCase: 'SwitchCase',
        ThisExpression: 'ThisExpression',
        ThrowStatement: 'ThrowStatement',
        TryStatement: 'TryStatement',
        UnaryExpression: 'UnaryExpression',
        UpdateExpression: 'UpdateExpression',
        VariableDeclaration: 'VariableDeclaration',
        VariableDeclarator: 'VariableDeclarator',
        WhileStatement: 'WhileStatement',
        WithStatement: 'WithStatement'
    };
    types.BLOCK_CAPABILITIES = BLOCK_CAPABILITIES;
    types.EXECUTION_STATE = EXECUTION_STATE;
    types.RUN_FLAGS = RUN_FLAGS;


    return types;
});
},
'xblox/embedded':function(){
define([
    'dojo/_base/declare',
    'xide/types',
    'xblox/types/Types',
    'xide/factory',
    'xide/utils',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin',
    "xblox/model/logic/CaseBlock",
    "xblox/model/Block",
    "xblox/model/functions/CallBlock",
    "xblox/model/code/CallMethod",
    "xblox/model/code/RunScript",
    "xblox/model/code/RunBlock",
    "xblox/model/loops/ForBlock",
    "xblox/model/loops/WhileBlock",
    "xblox/model/variables/VariableAssignmentBlock",
    "xblox/model/logic/IfBlock",
    "xblox/model/logic/ElseIfBlock",
    "xblox/model/logic/SwitchBlock",
    "xblox/model/variables/VariableSwitch",
    "xblox/model/events/OnEvent",
    "xblox/model/events/OnKey",
    "xblox/model/logging/Log",
    "xblox/model/html/SetStyle",
    "xblox/model/html/SetCSS",
    "xblox/model/html/SetStyle",
    "xblox/manager/BlockManager",
    "xblox/factory/Blocks",
    "xdojo/has!xblox-ui?xblox/model/Block_UI"
], function () {
    if(!Array.prototype.remove){
        Array.prototype.remove= function(){
            var what, a= arguments, L= a.length, ax;
            while(L && this.length){
                what= a[--L];
                if(this.indexOf==null){
                    break;
                }
                while((ax= this.indexOf(what))!= -1){
                    this.splice(ax, 1);
                }
            }
            return this;
        };
    }
    if(!Array.prototype.swap){
        Array.prototype.swap = function (x,y) {
            var b = this[x];
            this[x] = this[y];
            this[y] = b;
            return this;
        };
    }

    if ( typeof String.prototype.startsWith != 'function' ) {
        String.prototype.startsWith = function( str ) {
            return this.substring( 0, str.length ) === str;
        };
    }

    if ( typeof String.prototype.endsWith != 'function' ) {
        String.prototype.endsWith = function( str ) {
            return this.substring( this.length - str.length, this.length ) === str;
        };
    }

    if(!Function.prototype.bind) {
        // Cheap polyfill to approximate bind(), make Safari happy
        Function.prototype.bind = Function.prototype.bind || function (that) {
            return dojo.hitch(that, this);
        };
    }
});
},
'xblox/model/logic/CaseBlock':function(){
define([
    'dcl/dcl',
    'xide/utils',
    'xblox/model/Block',
    'dojo/Deferred',
    "xblox/model/logic/BreakBlock"
], function (dcl, utils, Block, Deferred, BreakBlock) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The Case Block model. Each case block contains a comparation and a commands block.
    //      If the comparation result is true, the block is executed
    //
    //      This block should have an "SwitchBlock" parent

    // module:
    //		xblox.model.logic.CaseBlock
    return dcl(Block, {
        declaredClass: "xblox.model.logic.CaseBlock",
        //comparator: xblox.model.Comparator
        // Comparison to be applied -> compare <switch variable> width <expression>
        comparator: null,
        //expression: xblox.model.Expression
        // expression to be compared
        expression: null,
        //items: Array (xblox.model.Block)
        //  block to be executed if the comparison result is true
        items: null,
        name: 'Case',
        icon: '',
        hasInlineEdits: true,
        toText: function () {
            var _comparator = '' + this.comparator;
            if (_comparator == '==') {
                //_comparator =''
            }
            return '<span style="text-indent: 1em;">&nbsp;&nbsp;&nbsp;' + this.getBlockIcon('I') + this.name + ' ' + this.makeEditable('comparator', 'right', 'text', 'Enter a comparison', 'inline') + (this.expression != null ? ' ' + this.makeEditable('expression', 'right', 'text', 'Enter a value to compare') : '') + '</span>';
        },
        canAdd: function () {
            return [];
        },
        /**
         *
         * @param scope
         * @param settings
         * @param switchblock
         * @returns {Array}
         * @private
         */
        _solve: function (scope, settings, switchblock) {
            settings = settings || {
                    highlight: false
                };
            var ret = [];
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];
                if (block.declaredClass.indexOf('BreakBlock') !== -1) {
                    switchblock.stop();
                }
                this.addToEnd(ret, block.solve(scope, settings));
            }

            return ret;
        },
        /***
         * Solves the case block
         * @param scope
         * @param settings
         * @param switchBlock   => parent SwitchCommand block
         */
        solve: function (scope, switchBlock, settings) {
            try {
                var _var = scope.getVariableById(switchBlock.variable);
                if (!_var && settings.args && settings.args[0]) {
                    _var = {value: settings.args[0]};
                }
                // Get the variable to evaluate
                var switchVarValue = '';
                if (_var) {
                    switchVarValue = this._getArg(_var.value, true);
                } else {
                    this.onFailed(this, settings);
                    // Comparation is false
                    return false;
                }
                //var compResult = scope.parseExpression("'" + switchVarValue+ "'" + this.comparator + this.expression);
                var compResult = scope.parseExpression("" + switchVarValue + "" + this.comparator + this._getArg(this.expression, true));
                if (compResult !== true) {
                    this.onFailed(this, settings);
                    // Comparation is false
                    return false;
                } else {
                    this.onSuccess(this, settings);
                    // Comparation is true. Return block.solve();
                    this._solve(scope, settings, switchBlock);
                    return true;
                }
            } catch (e) {
                logError(e);
            }
        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            return this.items;
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('Expression', 13, this.expression, {
                group: 'General',
                title: 'Expression',
                dst: 'expression'
            }));

            function makeOption(value, label) {
                return {
                    label: label || value,
                    value: value
                }
            }

            fields.push(utils.createCI('Comparator', 3, this.comparator, {
                group: 'General',
                title: 'Comparator',
                dst: 'comparator',
                widget: {
                    options: [
                        /*makeOption('==',"Equals"),
                         makeOption('<=',"Smaller or equal"),
                         makeOption('=>',"Greater or equal"),
                         makeOption('!=',"Not equal"),
                         makeOption('<',"Smaller than"),
                         makeOption('>',"Greater than")*/
                        makeOption('=='),
                        makeOption('<='),
                        makeOption('=>'),
                        makeOption('!='),
                        makeOption('<'),
                        makeOption('>')
                    ],
                    editable: true
                }
            }));
            return fields;
        },
        runAction: function (action) {
            if (action.command === 'New/Break') {
                var dfd = new Deferred();
                var newBlock = this.add(BreakBlock, {
                    group: null
                });
                var defaultDfdArgs = {
                    select: [newBlock],
                    focus: true,
                    append: false
                };
                dfd.resolve(defaultDfdArgs);
                newBlock.refresh();
                return dfd;
            }
        },
        getActions: function () {
            return [this.createAction({
                label: 'Break',
                command: 'New/Break',
                tab: 'Home',
                icon: 'fa-stop',
                group: 'File',
                mixin: {
                    addPermission: true,
                    custom: true,
                    quick: false
                }
            })
            ]
        }
    });
});
},
'xblox/model/Block':function(){
/** @module xblox/model/Block **/
define([
    'dcl/dcl',
    "dojo/Deferred",
    "dojo/_base/lang",
    "dojo/has",
    "xblox/model/ModelBase",
    "xide/factory",
    "xide/utils",
    "xide/types",
    "xide/utils/ObjectUtils",
    "xide/lodash",
    "xdojo/has!xblox-ui?xblox/model/Block_UI"
], function (dcl, Deferred, lang, has, ModelBase, factory, utils, types, ObjectUtils, _, Block_UI) {

    var bases = [ModelBase];

    function index(what, items) {
        for (var j = 0; j < items.length; j++) {
            if (what.id === items[j].id) {
                return j;
            }
        }
    }

    function compare(left, right) {
        return index(left) - index(right);
    }

    if (! 1 ) {
        bases.push(dcl(null, {
            getStatusIcon: function () {},
            getStatusClass: function () {},
            setStatusClass: function () {},
            onActivity: function () {},
            onRun: function () {},
            onFailed: function () {},
            onSuccess: function () {},
            getIconClass: function () {}
        }));
    }
    if (Block_UI) {
        bases.push(Block_UI);
    }

    /***
     * First things first, extend the core types for block flags:
     */
    utils.mixin(types, {
        /**
         * Flags to describe flags of the inner state of a block which might change upon the optimization. It also
         * contains some other settings which might be static, default or changed by the UI(debugger, etc...)
         *
         * @enum {integer} module:xide/types/BLOCK_FLAGS
         * @memberOf module:xide/types
         */
        BLOCK_FLAGS: {
            NONE: 0x00000000, // Reserved for future use
            ACTIVE: 0x00000001, // This behavior is active
            SCRIPT: 0x00000002, // This behavior is a script
            RESERVED1: 0x00000004, // Reserved for internal use
            USEFUNCTION: 0x00000008, // Block uses a function and not a graph
            RESERVED2: 0x00000010, // Reserved for internal use
            SINGLE: 0x00000020, // Only this block will excecuted, child blocks not.
            WAITSFORMESSAGE: 0x00000040, // Block is waiting for a message to activate one of its outputs
            VARIABLEINPUTS: 0x00000080, // Block may have its inputs changed by editing them
            VARIABLEOUTPUTS: 0x00000100, // Block may have its outputs changed by editing them
            VARIABLEPARAMETERINPUTS: 0x00000200, // Block may have its number of input parameters changed by editing them
            VARIABLEPARAMETEROUTPUTS: 0x00000400, // Block may have its number of output parameters changed by editing them
            TOPMOST: 0x00004000, // No other Block includes this one
            BUILDINGBLOCK: 0x00008000, // This Block is a building block (eg: not a transformer of parameter operation)
            MESSAGESENDER: 0x00010000, // Block may send messages during its execution
            MESSAGERECEIVER: 0x00020000, // Block may check messages during its execution
            TARGETABLE: 0x00040000, // Block may be owned by a different object that the one to which its execution will apply
            CUSTOMEDITDIALOG: 0x00080000, // This Block have a custom Dialog Box for parameters edition .
            RESERVED0: 0x00100000, // Reserved for internal use.
            EXECUTEDLASTFRAME: 0x00200000, // This behavior has been executed during last process. (Available only in profile mode )
            DEACTIVATENEXTFRAME: 0x00400000, // Block will be deactivated next frame
            RESETNEXTFRAME: 0x00800000, // Block will be reseted next frame

            INTERNALLYCREATEDINPUTS: 0x01000000, // Block execution may create/delete inputs
            INTERNALLYCREATEDOUTPUTS: 0x02000000, // Block execution may create/delete outputs
            INTERNALLYCREATEDINPUTPARAMS: 0x04000000, // Block execution may create/delete input parameters or change their type
            INTERNALLYCREATEDOUTPUTPARAMS: 0x08000000, // Block execution may create/delete output parameters or change their type
            INTERNALLYCREATEDLOCALPARAMS: 0x40000000, // Block execution may create/delete local parameters or change their type

            ACTIVATENEXTFRAME: 0x10000000, // Block will be activated next frame
            LOCKED: 0x20000000, // Block is locked for utilisation in xblox
            LAUNCHEDONCE: 0x80000000 // Block has not yet been launched...
        },

        /**
         *  Mask for the messages the callback function of a block should be aware of. This goes directly in
         *  the EventedMixin as part of the 'emits' chain (@TODO)
         *
         * @enum module:xide/types/BLOCK_CALLBACKMASK
         * @memberOf module:xide/types
         */
        BLOCK_CALLBACKMASK: {
            PRESAVE: 0x00000001, // Emits PRESAVE messages
            DELETE: 0x00000002, // Emits DELETE messages
            ATTACH: 0x00000004, // Emits ATTACH messages
            DETACH: 0x00000008, // Emits DETACH messages
            PAUSE: 0x00000010, // Emits PAUSE messages
            RESUME: 0x00000020, // Emits RESUME messages
            CREATE: 0x00000040, // Emits CREATE messages
            RESET: 0x00001000, // Emits RESET messages
            POSTSAVE: 0x00000100, // Emits POSTSAVE messages
            LOAD: 0x00000200, // Emits LOAD messages
            EDITED: 0x00000400, // Emits EDITED messages
            SETTINGSEDITED: 0x00000800, // Emits SETTINGSEDITED messages
            READSTATE: 0x00001000, // Emits READSTATE messages
            NEWSCENE: 0x00002000, // Emits NEWSCENE messages
            ACTIVATESCRIPT: 0x00004000, // Emits ACTIVATESCRIPT messages
            DEACTIVATESCRIPT: 0x00008000, // Emits DEACTIVATESCRIPT messages
            RESETINBREAKPOINT: 0x00010000, // Emits RESETINBREAKPOINT messages
            RENAME: 0x00020000, // Emits RENAME messages
            BASE: 0x0000000E, // Base flags :attach /detach /delete
            SAVELOAD: 0x00000301, // Base flags for load and save
            PPR: 0x00000130, // Base flags for play/pause/reset
            EDITIONS: 0x00000C00, // Base flags for editions of settings or parameters
            ALL: 0xFFFFFFFF // All flags
        }

    });
    /**
     * Base block class.
     *
     * @class module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     * @lends module:xblox/model/Block_UI
     */
    var Block = dcl(bases, {
        declaredClass: "xblox.model.Block",
        scopeId: null,
        isCommand: false,
        outlet: 0,
        _destroyed: false,
        /**
         * Switch to include the block for execution.
         * @todo, move to block flags
         * @type {boolean}
         * @default true
         */
        enabled: true,
        /**
         * Switch to include the block for serialization.
         * @todo, move to block flags
         * @type {boolean}
         * @default true
         */
        serializeMe: true,
        /**
         * Name is used for the interface, mostly as prefix within
         * this.toText() which includes also the 'icon' (as icon font).
         * This should be unique and expressive.
         *
         * This field can be changed by the user. Examples
         * 'Run Script' will result in result 'Run Script + this.script'
         *
         * @todo: move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {string|null}
         * @default null
         * @required false
         */
        name: null,
        /**
         * @todo: same as name, move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {string}
         * @default 'No Description'
         * @required true
         */
        shareTitle: '',
        /**
         * The blocks internal user description
         * Description is used for the interface. This should be short and expressive and supports plain and html text.
         *
         * @todo: same as name, move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {boolean}
         * @default 'No Description'
         * @required true
         */
        sharable: false,
        /**
         * Container holding a 'child' blocks. Subclassing block might hold that somewhere else.
         * @type {Block[]}
         * @default null
         * @required false
         */
        items: null,
        /**
         * Parent up-link
         * @type {string|Block}
         * @default null
         * @required false
         */
        parent: null,
        /**
         * @var temporary variable to hold remainin blocks to run
         */
        _return: null,
        /**
         * @var temporary variable to store the last result
         */
        _lastResult: null,
        _deferredObject: null,
        _currentIndex: 0,
        _lastRunSettings: null,
        _onLoaded: false,
        beanType: 'BLOCK',
        override: {},
        /**
         * ignore these due to serialization
         */
        ignoreSerialize: [
            '_didSubscribe',
            '_currentIndex',
            '_deferredObject',
            '_destroyed',
            '_return',
            'parent',
            '__started',
            'ignoreSerialize',
            '_lastRunSettings',
            '_onLoaded',
            'beanType',
            'sharable',
            'override',
            'virtual',
            '_scenario',
            '_didRegisterSubscribers',
            'additionalProperties',
            'renderBlockIcon',
            'serializeMe',
            '_statusIcon',
            '_statusClass',
            'hasInlineEdits',
            '_loop',
            'help',
            'owner',
            '_lastCommand',
            'allowActionOverride',
            'canDelete',
            'isCommand',
            'lastCommand',
            'autoCreateElse',
            '_postCreated',
            '_counter'
        ],
        _parseString: function (string, settings, block, flags) {
            try {
                settings = settings || this._lastSettings || {};
                flags = flags || settings.flags || types.CIFLAG.EXPRESSION;
                var scope = this.scope;
                var value = string;
                var parse = !(flags & types.CIFLAG.DONT_PARSE);
                var isExpression = (flags & types.CIFLAG.EXPRESSION);
                if (flags & types.CIFLAG.TO_HEX) {
                    value = utils.to_hex(value);
                }
                if (parse !== false) {
                    value = utils.convertAllEscapes(value, "none");
                }
                var override = settings.override || this.override || {};
                var _overrides = (override && override.variables) ? override.variables : null;
                var res = "";
                if (isExpression && parse !== false) {
                    res = scope.parseExpression(value, null, _overrides, null, null, null, override.args, flags);
                } else {
                    res = '' + value;
                }
            } catch (e) {
                console.error(e);
            }
            return res;
        },
        postCreate: function () {},
        /**
         *
         * @param clz
         * @returns {Array}
         */
        childrenByClass: function (clz) {
            var all = this.getChildren();
            var out = [];
            for (var i = 0; i < all.length; i++) {
                var obj = all[i];
                if (obj.isInstanceOf(clz)) {
                    out.push(obj);
                }
            }
            return out;
        },
        /**
         *
         * @param clz
         * @returns {Array}
         */
        childrenByNotClass: function (clz) {
            var all = this.getChildren();
            var out = [];
            for (var i = 0; i < all.length; i++) {
                var obj = all[i];
                if (!obj.isInstanceOf(clz)) {
                    out.push(obj);
                }
            }
            return out;
        },
        /**
         *
         * @returns {null}
         */
        getInstance: function () {
            var instance = this.scope.instance;
            if (instance) {
                return instance;
            }
            return null;
        },
        pause: function () {},
        mergeNewModule: function (source) {
            for (var i in source) {
                var o = source[i];
                if (o && _.isFunction(o)) {
                    this[i] = o; //swap
                }
            }
        },
        __onReloaded: function (newModule) {
            this.mergeNewModule(newModule.prototype);
            var _class = this.declaredClass;
            var _module = lang.getObject(utils.replaceAll('/', '.', _class)) || lang.getObject(_class);
            if (_module) {
                if (_module.prototype && _module.prototype.solve) {
                    this.mergeNewModule(_module.prototype);
                }
            }
        },
        reparent: function () {
            var item = this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            if (parent) {} else {
                var _next = item.next(null, 1) || item.next(null, -1);
                if (_next) {
                    item.group = null;
                    _next._add(item);
                }
            }
        },
        unparent: function (blockgroup, move) {
            var item = this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            if (parent && parent.removeBlock) {
                parent.removeBlock(item, false);
            }

            item.group = blockgroup;
            item.parentId = null;
            item.parent = null;
            if (move !== false) {
                item._place(null, -1, null);
                item._place(null, -1, null);
            }
        },
        move: function (dir) {
            var item = this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            var items = null;
            var store = item._store;
            if (parent) {
                items = parent[parent._getContainer(item)];
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }
                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var upperItem = items[cIndex + (dir)];
                if (!upperItem) {
                    return false;
                }
                items[cIndex + (dir)] = item;
                items[cIndex] = upperItem;
                return true;
            } else {
                item._place(null, dir);
                return true;
            }
        },
        _place: function (ref, direction, items) {
            var store = this._store,
                dst = this;
            ref = ref || dst.next(null, direction);
            if (!ref) {
                console.error('have no next', this);
                return;
            }
            ref = _.isString(ref) ? store.getSync(ref) : ref;
            dst = _.isString(dst) ? store.getSync(dst) : dst;
            items = items || store.storage.fullData;
            direction = direction == -1 ? 0 : 1;
            items.remove(dst);
            if (direction == -1) {
                direction = 0;
            }
            items.splice(Math.max(index(ref, items) + direction, 0), 0, dst);
            store._reindex();
        },
        index: function () {
            var item = this,
                parent = item.getParent(),
                items = null,
                group = item.group,
                store = this._store;

            if (parent) {
                items = parent[parent._getContainer(item)] || [];
                items = items.filter(function (item) {
                    return item.group === group;
                });
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }
                return this.indexOf(items, item);
            } else {
                items = store.storage.fullData;
                items = items.filter(function (item) {
                    return item.group === group;
                });
                return this.indexOf(items, item);
            }
        },
        numberOfParents: function () {
            var result = 0;
            var parent = this.getParent();
            if (parent) {
                result++;
                result += parent.numberOfParents();
            }
            return result;
        },
        getTopRoot: function () {
            var last = this.getParent();
            if (last) {
                var next = last.getParent();
                if (next) {
                    last = next;
                }
            }
            return last;
        },
        next: function (items, dir) {
            items = items || this._store.storage.fullData;

            function _next(item, items, dir, step, _dstIndex) {
                var start = item.indexOf(items, item);
                var upperItem = items[start + (dir * step)];
                if (upperItem) {
                    if (!upperItem.parentId && upperItem.group && upperItem.group === item.group) {
                        _dstIndex = start + (dir * step);
                        return upperItem;
                    } else {
                        step++;
                        return _next(item, items, dir, step, _dstIndex);
                    }
                }
                return null;
            }
            return _next(this, items, dir, 1, 0);
        },
        /**
         *
         * @param createRoot
         * @returns {module:xblox/model/Block|null}
         */
        getParent: function (createRoot) {
            if (this.parentId) {
                return this.scope.getBlockById(this.parentId);
            }
        },
        getScope: function () {
            var scope = this.scope;
            if (this.scopeId && this.scopeId.length > 0) {
                var owner = scope.owner;
                if (owner && owner.hasScope) {
                    if (owner.hasScope(this.scopeId)) {
                        scope = owner.getScope(this.scopeId);
                    } else {
                        console.error('have scope id but cant resolve it', this);
                    }
                }
            }
            return scope;
        },
        /**
         *
         * @returns {null}
         */
        canAdd: function () {
            return null;
        },
        getTarget: function () {
            var _res = this._targetReference;
            if (_res) {
                return _res;
            }
            var _parent = this.getParent();
            if (_parent && _parent.getTarget) {
                _res = _parent.getTarget();
            }
            return _res;
        },
        // adds array2 at the end of array1 => useful for returned "solve" commands
        addToEnd: function (array1, array2) {
            if (array2 && array1.length != null && array2.length != null) {
                array1.push.apply(array1, array2);
            }
            return array1;
        },
        /**
         *
         * @param what
         * @param del delete block
         */
        removeBlock: function (what, del) {
            if (what) {
                if (del !== false && what.empty) {
                    what.empty();
                }
                if (del !== false) {
                    delete what.items;
                }
                what.parent = null;
                what.parentId = null;
                if (this.items) {
                    this.items.remove(what);
                }
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Accessors
        //
        /////////////////////////////////////////////////////////////////////////////////////
        _getContainer: function (item) {
            return 'items';
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        empty: function (what) {
            try {
                this._empty(what)
            } catch (e) {

                debugger;
            }
        },
        /*
         * Empty : removes all child blocks, recursively
         * @param proto : prototype|instance
         * @param ctrArgs
         * @returns {*}
         */
        _empty: function (what) {
            var data = what || this.items;
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var subBlock = data[i];

                    if (subBlock && subBlock.empty) {
                        subBlock.empty();
                    }
                    if (subBlock && this.scope && this.scope.blockStore) {
                        this.scope.blockStore.remove(subBlock.id);
                    }
                }
            }
        },
        /**
         * This was needed. FF bug.
         * @param data
         * @param obj
         * @returns {boolean}
         */
        containsItem: function (data, obj) {
            var i = data.length;
            while (i--) {
                if (data[i].id === obj.id) {
                    return true;
                }
            }
            return false;
        },
        /**
         * This was needed. FF bug
         * @param data
         * @param obj
         * @returns {*}
         */
        indexOf: function (data, obj) {
            var i = data.length;
            while (i--) {
                if (data[i].id === obj.id) {
                    return i;
                }
            }
            return -1;
        },
        _getBlock: function (dir) {
            var item = this;
            if (!item || !item.parentId) {
                return false;
            }
            //get parent
            var parent = this.scope.getBlockById(item.parentId);
            if (!parent) {
                return null;
            }
            var items = parent[parent._getContainer(item)];
            if (!items || items.length < 2 || !this.containsItem(items, item)) {
                return null;
            }
            var cIndex = this.indexOf(items, item);
            if (cIndex + (dir) < 0) {
                return false;
            }
            var upperItem = items[cIndex + (dir)];
            if (upperItem) {
                return upperItem;
            }
            return null;
        },
        getPreviousBlock: function () {
            return this._getBlock(-1);
        },
        getNextBlock: function () {
            return this._getBlock(1);
        },
        _getPreviousResult: function () {
            var parent = this.getPreviousBlock() || this.getParent();
            if (parent && parent._lastResult != null) {
                if (this.isArray(parent._lastResult)) {
                    return parent._lastResult;
                } else {
                    return [parent._lastResult];
                }
            }
            return null;
        },
        getPreviousResult: function () {
            var parent = null;
            var prev = this.getPreviousBlock();
            if (!prev || !prev._lastResult || !prev.enabled) {
                parent = this.getParent();
            } else {
                parent = prev;
            }

            if (parent && !parent._lastResult) {
                var _newParent = parent.getParent();
                if (_newParent) {
                    parent = _newParent;
                }
            }

            if (parent && parent._lastResult != null) {
                if (this.isArray(parent._lastResult)) {
                    return parent._lastResult;
                } else {
                    return parent._lastResult;
                }
            }
            return null;
        },
        _getArg: function (val, escape) {
            var _float = parseFloat(val);
            if (!isNaN(_float)) {
                return _float;
            } else {
                if (val === 'true' || val === 'false') {
                    return val === 'true';
                } else if (val && escape && _.isString(val)) {
                    return '\'' + val + '\'';
                }
                return val;
            }
        },
        /**
         *
         * @returns {Array}
         */
        getArgs: function (settings) {
            var result = [];
            settings = settings || {};
            var _inArgs = settings.args || this._get('args');
            if (settings.override && settings.override.args) {
                _inArgs = settings.override.args;
            }
            if (_inArgs) { //direct json
                result = utils.getJson(_inArgs, null, false);
            }
            //try comma separated list
            if (result && result.length == 0 && _inArgs && _inArgs.length && _.isString(_inArgs)) {

                if (_inArgs.indexOf(',') !== -1) {
                    var splitted = _inArgs.split(',');
                    for (var i = 0; i < splitted.length; i++) {
                        //try auto convert to number
                        var _float = parseFloat(splitted[i]);
                        if (!isNaN(_float)) {
                            result.push(_float);
                        } else {
                            if (splitted[i] === 'true' || splitted[i] === 'false') {
                                result.push(utils.toBoolean(splitted[i]));
                            } else {
                                result.push(splitted[i]); //whatever
                            }
                        }
                    }
                    return result;
                } else {
                    result = [this._getArg(_inArgs)]; //single argument
                }
            }

            !_.isArray(result) && (result = []);
            //add previous result
            var previousResult = this.getPreviousResult();
            if (previousResult != null) {
                if (_.isArray(previousResult) && previousResult.length == 1) {
                    result.push(previousResult[0]);
                } else {
                    result.push(previousResult);
                }
            }

            return result || [_inArgs];
        },
        /*
         * Remove : as expected, removes a block
         * @param proto : prototype|instance
         * @param ctrArgs
         * @returns {*}
         */
        remove: function (what) {
            this._destroyed = true;
            if (this.parentId != null && this.parent == null) {
                this.parent = this.scope.getBlockById(this.parentId);
            }
            if (this.parent && this.parent.removeBlock) {
                this.parent.removeBlock(this);
                return;
            }
            what = what || this;
            if (what) {
                if (what.empty) {
                    what.empty();
                }
                delete what.items;
                what.parent = null;
                if (this.items) {
                    this.items.remove(what);
                }
            }

        },
        prepareArgs: function (ctorArgs) {
            if (!ctorArgs) {
                ctorArgs = {};
            }
            //prepare items
            if (!ctorArgs['id']) {
                ctorArgs['id'] = this.createUUID();
            }
            if (!ctorArgs['items']) {
                ctorArgs['items'] = [];
            }
        },
        /**
         * Private add-block function
         * @param proto
         * @param ctrArgs
         * @param where
         * @param publish
         * @returns {*}
         * @private
         */
        _add: function (proto, ctrArgs, where, publish) {
            var block = null;
            try {
                //create or set
                if (ctrArgs) {
                    //use case : normal object construction
                    this.prepareArgs(ctrArgs);
                    block = factory.createBlock(proto, ctrArgs, null, publish);
                } else {
                    //use case : object has been created so we only do the leg work
                    if (ctrArgs == null) {
                        block = proto;
                    }
                    //@TODO : allow use case to use ctrArgs as mixin for overriding
                }
                ///////////////////////
                //  post work

                //inherit scope
                block.scope = this.scope;
                //add to scope
                if (this.scope) {
                    block = this.scope.registerBlock(block, publish);
                }
                if (has('debug')) {
                    if (block.id === this.id) {
                        console.error('adding new block to our self');
                        debugger;
                    }
                }
                block.parent = this;
                block.parentId = this.id;
                block.scope = this.scope;

                var container = where || this._getContainer();
                if (container) {
                    if (!this[container]) {
                        this[container] = [];
                    }
                    var index = this.indexOf(this[container], block);
                    if (index !== -1) {
                        console.error(' have already ' + block.id + ' in ' + container);
                    } else {
                        if (this.id == block.id) {
                            console.error('tried to add our self to ' + container);
                            return;
                        }
                        this[container].push(block);
                    }
                }
                block.group = null;
                return block;
            } catch (e) {
                logError(e, '_add');
            }
            return null;

        },
        getStore: function () {
            return this.getScope().getStore();
        },
        /**
         * Public add block function
         * @param proto {}
         * @param ctrArgs
         * @param where
         * @returns {*}
         */
        add: function (proto, ctrArgs, where) {
            var block = this._add(proto, ctrArgs, where);
            return block.getStore().getSync(block.id);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Run 
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getContext: function () {
            if (this.scope.instance && this.scope.instance) {
                return this.scope.instance;
            }
            return null;
        },
        resolved: function () {
            if (this._deferredObject) {
                this._deferredObject.resolve();
                delete this._deferredObject;
            }
        },
        /***
         * Solves all the commands into items[]
         *
         * @param manager   =>  BlockManager
         * @return  list of commands to send
         */
        _solve: function (scope, settings) {
            settings = settings || {
                highlight: false
            };
            var ret = [];
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];
                this.addToEnd(ret, block.solve(scope, settings));
            }
            return ret;
        },
        /***
         * Solves all the commands into items[]
         *
         * @param manager   =>  BlockManager
         * @return  list of commands to send
         */
        solve: function (scope, settings) {
            settings = settings || {
                highlight: false
            };
            var ret = [];
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];
                this.addToEnd(ret, block.solve(scope, settings));
            }
            return ret;
        },
        /***
         * Solves all the commands into items[]
         *
         * @param manager   =>  BlockManager
         * @return  list of commands to send
         */
        solveMany: function (scope, settings) {
            if (!this._lastRunSettings && settings) {
                this._lastRunSettings = settings;
            }
            settings = this._lastRunSettings || settings;
            this._currentIndex = 0;
            this._return = [];
            var ret = [],
                items = this[this._getContainer()];

            if (items.length) {
                var res = this.runFrom(items, 0, settings);
                this.onSuccess(this, settings);
                return res;
            } else {
                this.onSuccess(this, settings);
            }
            return ret;
        },
        runFrom: function (blocks, index, settings) {
            var thiz = this;
            blocks = blocks || this.items;
            if (!this._return) {
                this._return = [];
            }
            var onFinishBlock = function (block, results) {
                block._lastResult = block._lastResult || results;
                thiz._currentIndex++;
                thiz.runFrom(blocks, thiz._currentIndex, settings);
            };
            var wireBlock = function (block) {
                block._deferredObject.then(function (results) {
                    onFinishBlock(block, results);
                });
            };

            if (blocks.length) {
                for (var n = index; n < blocks.length; n++) {
                    var block = blocks[n];
                    if (block.deferred === true) {
                        block._deferredObject = new Deferred();
                        this._currentIndex = n;
                        wireBlock(block);
                        this.addToEnd(this._return, block.solve(this.scope, settings));
                        break;
                    } else {
                        this.addToEnd(this._return, block.solve(this.scope, settings));
                    }
                }
            } else {
                this.onSuccess(this, settings);
            }
            return this._return;
        },
        serializeField: function (name) {
            return this.ignoreSerialize.indexOf(name) == -1; //is not in our array
        },
        onLoad: function () {},
        activate: function () {},
        deactivate: function () {},
        _get: function (what) {
            if (this.override) {
                return (what in this.override ? this.override[what] : this[what]);
            }
        },
        onDidRun: function () {
            if (this.override) {
                this.override.args && delete this.override.args;
                delete this.override;
            }
        },
        destroy: function () {
            this.stop(true);
            this.reset();
            this._destroyed = true;
            delete this.virtual;
        },
        reset: function () {
            this._lastSettings = {};
            clearTimeout(this._loop);
            this._loop = null;
            delete this.override;
            this.override = null;
            delete this._lastResult;
            this.override = {};
        },
        stop: function () {
            this.reset();
            this.getItems && _.invoke(this.getItems(), 'stop');
        }

    });
    Block.FLAGS = types.BLOCK_FLAGS;
    Block.EMITS = types.BLOCK_CALLBACKMASK;
    //that's really weird: using dynamic base classes nor Block.extend doesnt work.
    //however, move dojo complete out of blox
    if (!Block.prototype.onSuccess) {
        Block.prototype.onSuccess = function () {};
        Block.prototype.onRun = function () {};
        Block.prototype.onFailed = function () {};
    }
    dcl.chainAfter(Block, 'stop');
    dcl.chainAfter(Block, 'destroy');
    dcl.chainAfter(Block, 'onDidRun');
    return Block;
});
},
'xblox/model/ModelBase':function(){
/** @module xblox/model/ModelBase
 *  @description The base for block related classes, this must be kept small and light as possible
 */
define([
    'dcl/dcl',
    "xide/utils",
    "xide/types",
    "xide/mixins/EventedMixin",
    "xide/lodash"
], function(dcl,utils,types,EventedMixin,_){
    /**
     * The model mixin for a block
     * @class module:xblox.model.ModelBase
     */
    var Module = dcl(EventedMixin.dcl,{
        declaredClass:'xblox.model.ModelBase',
        id:null,
        description:'',
        parent:null,
        parentId:null,
        group:null,
        order:0,
        _store:null,
        ////////////////////////////////////////////////////////////
        //
        //  Functions to expose out & in - lets
        //
        ////////////////////////////////////////////////////////////
        /**
         *
         * Implmented by the subclass. Each block must provide an output signature.
         * The format is currently the same as Dojo SMD
         *
         * @returns {Array}
         */
        outputs:function(){
           return [];
        },
        /**
         * Implemented by the subclass. Each block must provide an input signature.
         * The format is currently the same as Dojo SMD
         * @returns {Array}
         */
        takes:function(){
            return [];
        },
        /**
         * Implemented by the subclass. Each block must provide an needed input signature.
         * The format is currently the same as Dojo SMD. This is a filtered version of
         * 'takes'
         *
         * @returns {Array}
         */
        needs:function(){
            return [];
        },
        ////////////////////////////////////////////////////////////
        //
        //  Functions to expose outlets
        //
        ////////////////////////////////////////////////////////////
        /***
         * Standard constructor for all sub classing blocks
         * @param {array} args
         */
        constructor: function(args){
            //simple mixin of constructor arguments
            for (var prop in args) {
                if (args.hasOwnProperty(prop)) {
                    this[prop] = args[prop];
                }
            }
            if(!this.id){
                this.id = this.createUUID();
            }
            //short cuts
            this.utils=utils;
            this.types=types;
        },
        ////////////////////////////////////////////////////////////
        //
        //  Standard tools
        //
        ////////////////////////////////////////////////////////////
        keys: function (a) {
            var b = [];
            for (var c in a) {
                b.push(c);
            }
            return b;
        },
        values: function (b) {
            var a = [];
            for (var c in b) {
                a.push(b[c]);
            }
            return a;
        },
        toArray: function () {
            return this.map();
        },
        size: function () {
            return this.toArray().length;
        },
        createUUID:utils.createUUID,
        canEdit:function(){
            return true;
        },
        getFields:function(){
            return null;
        },
        isString: function (a) {
            return typeof a == "string"
        },
        isNumber: function (a) {
            return typeof a == "number"
        },
        isBoolean: function (a) {
            return typeof a == "boolean"
        },
        isObject:_.isObject,
        isArray:_.isArray,
        getValue:function(val){
            var _float = parseFloat(val);
            if(!isNaN(_float)){
               return _float;
            }
            if(val==='true' || val===true){
                return true;
            }
            if(val==='false' || val===false){
                return false;
            }
            return val;
        },
        isScript:function(val){
            return this.isString(val) &&(
                    val.indexOf('return')!=-1||
                    val.indexOf(';')!=-1||
                    val.indexOf('(')!=-1||
                    val.indexOf('+')!=-1||
                    val.indexOf('-')!=-1||
                    val.indexOf('<')!=-1||
                    val.indexOf('*')!=-1||
                    val.indexOf('/')!=-1||
                    val.indexOf('%')!=-1||
                    val.indexOf('=')!=-1||
                    val.indexOf('==')!=-1||
                    val.indexOf('>')!=-1||
                    val.indexOf('[')!=-1||
                    val.indexOf('{')!=-1||
                    val.indexOf('}')!=-1
                );
        },
        replaceAll:function(find, replace, str) {
            if(this.isString(str)){
                return str.split(find).join(replace);
            }
            return str;
        },
        isInValidState:function(){
            return true;
        },
        destroy:function(){}
    });
    dcl.chainAfter(Module,'destroy');
    return Module;
});
},
'xblox/model/logic/BreakBlock':function(){
define([
    'dcl/dcl',
    'xblox/model/Block'
], function (dcl, Block) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */

    // summary:
    //		The Case Block model. Each case block contains a comparation and a commands block.
    //      If the comparation result is true, the block is executed
    //
    //      This block should have an "SwitchBlock" parent

    // module:
    //		xblox.model.logic.CaseBlock
    return dcl(Block, {
        declaredClass: "xblox.model.logic.BreakBlock",
        name: 'Break',
        icon: 'fa-stop',
        hasInlineEdits: false,
        canAdd: false,
        toText: function () {
            return '&nbsp;<span class="fa-stop text-warning"></span>&nbsp;&nbsp;<span>' + this.name + '</span>';
        },
        /***
         * Solves the case block
         * @param scope
         * @param settings
         */
        solve: function (scope, settings) {
            this.onSuccess(this, settings);
        }
    });
});
},
'xblox/model/functions/CallBlock':function(){
define([
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'dojo/Deferred',
    "xblox/model/Block",
    "xcf/model/Command"
], function(dcl,utils,types,Deferred,Block,Command){

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.functions.CallBlock
    /**
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl(Command,{
        declaredClass:"xblox.model.functions.CallBlock",
        //command: (String)
        //  block action name
        command:'Select command please',
        icon:'',
        args:null,
        _timeout:100,

        isCommand:true,


        _commandHandles:null,
        /**
         * onCommandFinish will be excecuted which a driver did run a command
         * @param msg {object}
         * @param msg.id {string} the command job id
         * @param msg.src {string} the source id, which is this block id
         * @param msg.cmd {string} the command string being sent
         */
        onCommandProgress:function(msg){

            var scope = this.getScope();
            var context = scope.getContext();//driver instance
            var result = {};
            var params = msg.params;

            if(params && params.id){
                this._emit('cmd:'+msg.cmd + '_' + params.id,{
                    msg:msg
                });
                msg.lastResponse && this.storeResult(msg.lastResponse);
                this._emit('progress',{
                    msg:msg,
                    id:params.id
                });
            }

            var command = this._lastCommand;

            this._lastResult = null;


            this._lastResult = msg ? msg.result : null;

            var items = this.getItems(types.BLOCK_OUTLET.PROGRESS);
            if(!this._lastSettings){
                this._lastSettings = {}
            }
            this._lastSettings.override = {};
            if(items.length) {
                this.runFrom(items,0,this._lastSettings);
            }
        },
        stop:function(){
            this._lastCommand && this._lastCommand.stop();
        },
        pause:function(){
            this._lastCommand && this._lastCommand.pause();
        },
        destroy:function(){
            _.invoke(this._commandHandles,'remove');
            delete this._commandHandles;
            delete this._lastCommand;
        },
        /***
         * Returns the block run result
         * @param scope
         */
        solve:function(scope,settings) {
            if(!this._commandHandles){
                this._commandHandles=[];
            }else{
                _.invoke(this._commandHandles,'remove');
                this._commandHandles = [];
            }

            var timeout = this._timeout || 50;
            if(_.isString(timeout)){
                timeout = parseInt(timeout);
            }

            var dfd = new Deferred();

            var handles = this._commandHandles;

            settings = settings || {}

            setTimeout(function(){
                if (this.command){

                    var _args = null;
                    if(this.args){

                        settings.override = settings.override || {};
                        var args = scope.expressionModel.replaceVariables(scope,this.args,false,false,null,null,{
                            begin:"%%",
                            end:"%%"
                        });
                        try {
                            _args = utils.fromJson(args);
                        }catch(e){
                            _args = args;
                        }
                        settings.override['args']= _.isArray(_args) ? _args : [args];
                        settings.override['mixin']=_args;
                    }
                    this._lastCommand = scope.resolveBlock(this.command);
                    
                    if(this._lastCommand){
                        handles.push(this._lastCommand._on('paused',this.onCommandPaused,this));
                        handles.push(this._lastCommand._on('finished',this.onCommandFinish,this));
                        handles.push(this._lastCommand._on('stopped',this.onCommandStopped,this));
                        handles.push(this._lastCommand._on('error',this.onCommandError,this));
                        handles.push(this._lastCommand._on('progress',this.onCommandProgress,this));                    
                    }
                    
                    var res = scope.solveBlock(this.command,settings);
                    if(res){
                        this.onSuccess(this,settings);
                    }else{
                        this.onFailed(this,settings);
                    }
                    dfd.resolve(res);
                    return res;
                }
            }.bind(this),timeout);
            return dfd;
        },
        hasInlineEdits:true,
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable:function(field,pos,type,title,mode,options,value){
            var optionsString = "";
            return "<a " + optionsString + "  tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable editable-click'  href='#'>" + this[field] +"</a>";
        },
        getFieldOptions:function(field){
            if(field ==="command"){
                return this.scope.getCommandsAsOptions("text");
            }
        },
        toText:function(){
            var text = 'Unknown';
            var block = this.scope.getBlock(this.command);
            if(block){
                text = block.name;
            }
            if(this.command.indexOf('://')!==-1) {
                text = '<span class="text-info">' +this.scope.toFriendlyName(this,this.command) + '</span>';
            }
            var _out = this.getBlockIcon('D') + 'Call Command : ' + text;
            return _out;
        },
        //  standard call for editing
        getFields:function(){

            var fields = this.getDefaultFields();
            var thiz=this;

            var title = 'Command';

            if(this.command.indexOf('://')){
                title = this.scope.toFriendlyName(this,this.command);
            }

            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.command,{
                    group:'General',
                    title:'Command',
                    dst:'command',
                    options:this.scope.getCommandsAsOptions(),
                    block:this,
                    pickerType:'command',
                    value:this.command
            }));

            fields.push(utils.createCI('arguments',27,this.args,{
                group:'Arguments',
                title:'Arguments',
                dst:'args'
            }));

            fields.push(utils.createCI('timeout',13,this._timeout,{
                group:'General',
                title:'Delay',
                dst:'_timeout'
            }));

            return fields;
        }
    });
});
},
'xblox/model/code/CallMethod':function(){
define([
    'dcl/dcl',
    "xblox/model/Block",
    'xide/utils'
], function(dcl,Block,utils){

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    return dcl(Block,{
        declaredClass:"xblox.model.code.CallMethod",
        //method: (String)
        //  block action name
        name:'Call Method',
        //method: (String)
        //  block action name
        method:'',
        args:'',

        sharable:true,
        /***
         * Returns the block run result
         * @param scope
         */
        solve:function(scope,settings) {
            var context = this.getContext();
            if (context && context[this.method]!=null)
            {


                var res = [];
                var _fn = context[this.method];
                try{
                    var _args = this.getArgs(settings);
                     0 && console.log('args',_args);
                    var _res = _fn.apply(context,_args||[]);
                    res = _res;
                    this.onSuccess(this,settings);
                    return res;
                }catch(e){
                    console.error('call method ' + this.method + ' failed: '+e);
                    logError(e);
                    this.onFailed(this,settings);
                }
            }else{
                this.onFailed(this,settings);
                return [];
            }
            return [];
        },
        toText:function(){

            var result = this.getBlockIcon() + ' ' + this.name + ' ';
            if(this.method){
                result+= this.makeEditable('method','bottom','text','Enter a driver method','inline');
            }
            return result;
        },

        //  standard call for editing
        getFields:function(){

            var fields = this.getDefaultFields();

            var context = this.getContext();
/*
             0 && console.log('call method ', this.getScope().getContext());
             0 && console.log('call method ', context);*/


            fields.push(utils.createCI('value',13,this.method,{
                    group:'General',
                    title:'Method',
                    dst:'method'
                }));

            fields.push(utils.createCI('value',27,this.args,{
                    group:'Arguments',
                    dst:'args',
                    widget:{
                        title:''
                    }
                }));

            return fields;
        },
        getBlockIcon:function(){
            return '<span class="fa-caret-square-o-right"></span>';
        }
    });
});
},
'xblox/model/code/RunScript':function(){
/** @module xblox/model/code/RunScript **/
define([
    'dcl/dcl',
    'xdojo/has',
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xblox/model/Contains',
    'dojo/promise/all',
    'xide/types',
    'module'
    //'xdojo/has!host-node?dojo/node!tracer',
    //'xdojo/has!host-node?nxapp/utils/_console'
    //"xdojo/has!xblox-ui?dojo/text!./RunScript.html"
    //"xdojo/has!xblox-ui?dojo/text!xblox/docs/code/RunScript.md"
], function(dcl,has,Deferred,Block,utils,Contains,all,types,module,tracer,_console,Description,Help){

    
    var isServer =  0 ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if(isServer && tracer && console && console.error){
        console = _console;
    }
    /**
     *
     * @class module:xblox/model/code/RunScript
     * @extends module:xblox/model/Block
     */
    return dcl([Block,Contains],{
        declaredClass:"xblox.model.code.RunScript",
        name:'Run Script',
        method:'',
        args:'',
        deferred:false,
        sharable:true,
        context:null,
        icon:'fa-code',
        observed:[
            'method'
        ],
        getContext:function(){
            return this.context || (this.scope.getContext ?  this.scope.getContext() : this);
            return this.context || this;
        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        solve2:function(scope,settings,run,error) {
            this._currentIndex = 0;
            this._return=[];
            var _script = '' + this._get('method');
            var thiz=this,
                ctx = this.getContext();
            if(_script && _script.length) {

                var runScript = function() {
                    var _function = new Function("{" + _script + "}");
                    var _args = thiz.getArgs() || [];
                    try {
                        var _parsed = _function.apply(ctx, _args || {});
                        thiz._lastResult = _parsed;
                        if (run) {
                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                        }
                        if (_parsed !== 'false' && _parsed !== false) {
                            thiz.onSuccess(thiz, settings,{
                                result:_parsed
                            });
                        } else {
                            thiz.onFailed(thiz, settings);
                            return [];
                        }
                    } catch (e) {
                        if (error) {
                            error('invalid expression : \n' + _script + ': ' + e);
                        }
                        thiz.onFailed(thiz, settings);
                        return [];
                    }
                };

                if(scope.global){
                    (function() {
                        window = scope.global;
                        var _args = thiz.getArgs() || [];
                        try {
                            var _parsed = null;
                            if(!ctx.runExpression) {
                                var _function = new Function("{" + _script + "}").bind(this);
                                _parsed = _function.apply(ctx, _args || {});
                            }else{
                                _parsed = ctx.runExpression(_script,null,_args);
                            }

                            thiz._lastResult = _parsed;

                            if (run) {
                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                            }
                            if (_parsed !== 'false' && _parsed !== false) {
                                thiz.onSuccess(thiz, settings);
                            } else {
                                thiz.onFailed(thiz, settings);
                                return [];
                            }
                        } catch (e) {
                            thiz._lastResult = null;
                            if (error) {
                                error('invalid expression : \n' + _script + ': ' + e);
                            }
                            thiz.onFailed(thiz, settings);
                            return [];
                        }

                    }).call(scope.global);

                }else{
                    return runScript();
                }
            }else{
                console.error('have no script');
            }
            var ret=[], items = this[this._getContainer()];
            if(items.length) {
                this.runFrom(items,0,settings);
            }else{
                this.onSuccess(this, settings);
            }
            this.onDidRun();
            return ret;
        },
        /**
         *
         * @param scope
         * @param settings
         * @param run
         * @param error
         */
        solve:function(scope,settings,isInterface,send,run,error){

            this._currentIndex = 0;
            this._return=[];


            settings = settings || {};
            var _script = send || (this._get('method') ? this._get('method') : this.method);

            if(!scope.expressionModel){
                //console.error('mar',scope);
                throw new Error('na');
                return;
            }

            var thiz=this,
                ctx = this.getContext(),
                items = this[this._getContainer()],

                //outer
                dfd = new Deferred,
                listener = settings.listener,
                isDfd = thiz.deferred,
                expressionModel = scope.getExpressionModel();



            this.onRunThis(settings);

            function globalEval(text) {
                var ret;
                // Properly escape \, " and ' in the input, normalize \r\n to an escaped \n
                text = text.replace(/["'\\]/g, "\\$&").replace(/\r\n/g, "\\n");

                // You have to use eval() because not every expression can be used with an assignment operator
                var where = typeof window!=='undefined' ? window : global;

                where.execScript("globalEval.____lastInputResult____ = eval('" + text + "');} }");

                // Store the result and delete the property
                ret = globalEval.____lastInputResult____;
                delete globalEval.____lastInputResult____;

                return ret;
            }
            if(!expressionModel){
                console.error('scope has no expression model');
                return false;
            }
            var expression = expressionModel.replaceVariables(scope,_script,null,null);
            var _function = expressionModel.expressionCache[expression];
            if(!_function){
                _function = expressionModel.expressionCache[expression] = new Function("{" + expression + "}");
            }
            var _args = thiz.getArgs(settings) || [];
            try {
                if(isDfd){
                    ctx.resolve=function(result){
                        if(thiz._deferredObject) {
                            thiz._deferredObject.resolve();
                        }
                        thiz.onDidRunThis(dfd,result,items,settings);
                    }
                }
                var _parsed = _function.apply(ctx, _args || {});
                thiz._lastResult = _parsed;
                if (run) {
                    run('Expression ' + _script + ' evaluates to ' + _parsed);
                }
                if(!isDfd) {
                    thiz.onDidRunThis(dfd,_parsed,items,settings);
                }
                if (_parsed !== 'false' && _parsed !== false) {
                    thiz.onSuccess(thiz, settings);
                } else {
                    thiz.onFailed(thiz, settings);
                }
            } catch (e) {
                e=e ||{};
                thiz.onDidRunItemError(dfd,e,settings);
                thiz.onFailed(thiz,settings);
                if (error) {
                    error('invalid expression : \n' + _script + ': ' + e);
                }
            }
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){

            var result = '<span style="">' + this.getBlockIcon() + ' ' + this.name + ' :: '+'</span>';
            if(this.method){
                result+= this.method.substr(0,50);
            }
            return result;
        },
        canAdd:function(){
            return true;
        },
        getFields:function(){
            if(this.description === 'No Description'){
                this.description = Description;
            }
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz=this;
            fields.push(
                utils.createCI('name',13,this.name,{
                    group:'General',
                    title:'Name',
                    dst:'name'
                })
            );
            fields.push(
                utils.createCI('deferred',0,this.deferred,{
                    group:'General',
                    title:'Deferred',
                    dst:'deferred'
                })
            );
            fields.push(utils.createCI('arguments',27,this.args,{
                    group:'Arguments',
                    title:'Arguments',
                    dst:'args'
                }));

            fields.push(
                utils.createCI('value',types.ECIType.EXPRESSION_EDITOR,this.method,{
                    group:'Script',
                    title:'Script',
                    dst:'method',
                    select:true,
                    widget:{
                        allowACECache:true,
                        showBrowser:false,
                        showSaveButton:true,
                        editorOptions:{
                            showGutter:true,
                            autoFocus:false
                        },
                        item:this
                    },
                    delegate:{
                        runExpression:function(val,run,error){
                            var old = thiz.method;
                            thiz.method=val;
                            var _res = thiz.solve(thiz.scope,null,run,error);
                        }
                    }
                }));
            return fields;
        }
    });
});
},
'xblox/model/Contains':function(){
define([
    'dcl/dcl',
    "dojo/promise/all",
    "xide/types"
], function (dcl, all, types) {
    /**
     * Contains provides implements functions to deal with sub blocks.
     */
    return dcl(null, {
        declaredClass: 'xblox.model.Contains',
        runByType: function (outletType, settings) {
            var items = this.getItemsByType(outletType);
            if (items.length) {
                this.runFrom(items, 0, settings);
            }
        },
        getItemsByType: function (outletType) {
            var items = this.items;
            if (!outletType) {
                return items;
            }
            var result = [];
            _.each(items, function (item) {
                if (item.outlet & outletType) {
                    result.push(item);
                }
            });
            return result;
        },
        getContainer: function () {
            return this[this._getContainer()];
        },
        /**
         * Store is asking this!
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren: function (parent) {
            var items = this[this._getContainer()];
            return items != null && items.length > 0;
        },
        /**
         * Store function
         * @param parent
         * @returns {Array}
         */
        getChildren: function (parent) {
            return this[this._getContainer()];
        },
        //  standard call from interface
        canAdd: function () {
            return [];
        },
        /***
         * Generic: run sub blocks
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        _solve: function (scope, settings, run, error) {
            if (!this._lastRunSettings && settings) {
                this._lastRunSettings = settings;
            }
            settings = this._lastRunSettings || settings;
            this._currentIndex = 0;
            this._return = [];
            var ret = [], items = this[this._getContainer()];
            if (items.length) {
                var res = this.runFrom(items, 0, settings);
                this.onSuccess(this, settings);
                return res;
            } else {
                this.onSuccess(this, settings);
            }
            return ret;
        },
        onDidRunItem: function (dfd, result) {
            this._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, this);
            dfd.resolve(result);
        },
        onDidRunItemError: function (dfd, result) {
            dfd.reject(result);
        },
        onRunThis: function () {
            this._emit(types.EVENTS.ON_RUN_BLOCK, this);
        },
        onDidRunThis: function (dfd, result, items, settings) {
            var thiz = this;
            //more blocks?
            if (items && items.length) {
                var subDfds = thiz.runFrom(items, 0, settings);
                all(subDfds).then(function () {
                    thiz.onDidRunItem(dfd, result, settings);
                }, function (err) {
                    thiz.onDidRunItem(dfd, err, settings);
                });

            } else {
                thiz.onDidRunItem(dfd, result, settings);
            }
        },
        ___solve: function (scope, settings, run, error) {
        }
    });
});
},
'xblox/model/code/RunBlock':function(){
define([
    'dcl/dcl',
    "xblox/model/Block",
    'xide/types',
    'xide/utils'
], function(dcl,Block,types,utils){

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod


    return dcl(Block,{
        declaredClass:"xblox.model.code.RunBlock",
        //method: (String)
        //  block action name
        name:'Run Block',

        file:'',
        //method: (String)
        //  block action name
        method:'',

        args:'',

        sharable:true,

        block:'',

        description:"Runs another Block",
        /***
         * Returns the block run result
         * @param scope
         */
        solve:function(scope,settings) {

            var context = this.getContext();
            if (context && context[this.method]!=null)
            {
                var res = [];
                var _fn = context[this.method];
                try{
                    var _args = this._getArgs();
                    var _res = _fn.apply(context,_args||[]);
                    res = _res;
                    this.onSuccess(this,settings);
                    return res;
                }catch(e){
                    console.error('call method failed');
                    this.onFailed(this,settings);
                }
            }else{
                this.onFailed(this,settings);
                return [];
            }
            return [];
        },
        toText:function(){

            var result = this.getBlockIcon() + ' ' + this.name + ' ';
            if(this.method){
                result+= this.method.substr(0,20);
            }
            return result;
        },

        //  standard call for editing
        getFields:function(){


            var fields = this.getDefaultFields();


            fields.push(utils.createCI('Block', types.ECIType.BLOCK_REFERENCE, this.block, {
                toolTip:'Enter  block, you can use also the block\'s share title',
                group: 'General',
                dst: 'block',
                value: this.block,
                title:'Block',
                scope:this.scope
            }));

            fields.push(utils.createCI('File', types.ECIType.FILE, this.file, {
                toolTip:'Leave empty to auto-select this file',
                group: 'General',
                dst: 'file',
                value: this.file,
                intermediateChanges: false,
                acceptFolders: false,
                acceptFiles: true,
                encodeFilePath: false,
                buildFullPath: true,
                filePickerOptions: {
                    dialogTitle: 'Select Block File',
                    filePickerMixin: {
                        beanContextName: this.id,
                        persistent: false,
                        globalPanelMixin: {
                            allowLayoutCookies: false
                        }
                    },
                    configMixin: {
                        beanContextName: this.id,
                        LAYOUT_PRESET: types.LAYOUT_PRESET.SINGLE,
                        PANEL_OPTIONS:{
                            ALLOW_MAIN_MENU:false,
                            ALLOW_NEW_TABS:true,
                            ALLOW_MULTI_TAB:false,
                            ALLOW_INFO_VIEW:true,
                            ALLOW_LOG_VIEW:false,
                            ALLOW_CONTEXT_MENU:true,
                            ALLOW_LAYOUT_SELECTOR:true,
                            ALLOW_SOURCE_SELECTOR:true,
                            ALLOW_COLUMN_RESIZE:true,
                            ALLOW_COLUMN_REORDER:true,
                            ALLOW_COLUMN_HIDE:true,
                            ALLOW_ACTION_TOOLBAR:true,
                            ALLOW_BREADCRUMBS:false
                        }
                    },
                    defaultStoreOptions: {
                        "fields": 1663,
                        "includeList": "xblox",
                        "excludeList": "*"
                    },
                    startPath: this.file
                }
            }));

            return fields;

            /*
            fields.push(utils.createCI('value',27,this.args,{
                    group:'General',
                    title:'Arguments',
                    dst:'args'
                }));

            return fields;
            */
        },
        getBlockIcon:function(){
            return '<span class="el-icon-share-alt"></span>';
        }


    });
});
},
'xblox/model/loops/ForBlock':function(){
define([
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/variables/Variable",
    'xblox/model/Contains',
    "dojo/promise/all",
    "dojo/Deferred"
], function (dcl, Block, Variable, Contains, all, Deferred) {

    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The for block model. It repeats a block a number of times, while the condition is true.
    //

    // module:
    //		xblox.model.loops.ForBlock
    return dcl([Block, Contains], {
        declaredClass: "xblox.model.loops.ForBlock",
        // initial: xcf.model.Expression
        // the initial value
        initial: null,
        // final: xcf.model.Expression
        // the final value to be compared with the counter. Once the final value equals to the counter, the loop stops
        "final": null,
        //comparator: xblox.model.Comparator
        // Comparison to be applied -> compare <counter variable> width <final>
        comparator: null,
        // modifier: xcf.model.Expression
        // expression to be applied to the counter on every step. Expression: "<counter><modifier>"
        modifier: null,

        //items: Array (xblox.model.Block)
        //  block to be executed while the condition compare <counter variable> width <final> is false
        items: null,

        //counterName: String
        // the counter variable name
        counterName: null,
        // (private) counter: xblox.model.Variable
        // counter to be compared and updated on every step
        _counter: null,
        name: 'For',
        sharable: true,
        icon: '',
        ignoreErrors: false,
        deferred: true,
        _forState: false,
        _currentForIndex: 0,
        runFrom: function (_blocks, index, settings) {
            var thiz = this,
                blocks = _blocks || this.items,
                allDfds = [];

            var onFinishBlock = function (block, results) {
                block._lastResult = block._lastResult || results;
                thiz._currentIndex++;
                thiz.runFrom(blocks, thiz._currentIndex, settings);
            };

            var wireBlock = function (block) {
                block._deferredObject.then(function (results) {
                    onFinishBlock(block, results);
                });
            };

            if (blocks.length) {
                for (var n = index; n < blocks.length; n++) {
                    var block = blocks[n];
                    if (block.enabled === false) {
                        continue;
                    }
                    if (block.deferred === true) {
                        block._deferredObject = new Deferred();
                        this._currentIndex = n;
                        wireBlock(block);
                        allDfds.push(block.solve(this.scope, settings));
                        break;
                    } else {
                        allDfds.push(block.solve(this.scope, settings));
                    }
                }

            } else {
                this.onSuccess(this, settings);
            }
            return allDfds;
        },
        runFromDirect: function (_blocks, index, settings) {
            var thiz = this,
                blocks = _blocks || this.items,
                allDfds = [];

            var onFinishBlock = function (block, results) {
                block._lastResult = block._lastResult || results;
                thiz._currentIndex++;
                thiz.runFrom(blocks, thiz._currentIndex, settings);
            };

            var wireBlock = function (block) {
                block._deferredObject.then(function (results) {
                    onFinishBlock(block, results);
                });
            };
            if (blocks.length) {
                for (var n = index; n < blocks.length; n++) {
                    var block = blocks[n];
                    if (block.enabled === false) {
                        continue;
                    }
                    if (block.deferred === true) {
                        block._deferredObject = new Deferred();
                        this._currentIndex = n;
                        wireBlock(block);
                        allDfds.push(block.solve(this.scope, settings));
                        break;
                    } else {
                        allDfds.push(block.solve(this.scope, settings));
                    }
                }
            } else {
                this.onSuccess(this, settings);
            }

            return allDfds;
        },
        solveSubs: function (dfd, result, items, settings) {
            var thiz = this;
            settings.override = settings.override || {};
            settings.override['args'] = [this._currentForIndex];
            //more blocks?
            if (items.length) {
                var subDfds = thiz.runFrom(items, 0, settings);
                all(subDfds).then(function (what) {
                }, function (err) {
                    thiz.onDidRunItem(dfd, err, settings);
                });
                return subDfds;

            } else {
                thiz.onDidRunItem(dfd, result, settings);
            }
        },
        solveSubsDirect: function (dfd, result, items, settings) {
            var thiz = this;
            settings.override = settings.override || {};
            settings.override['args'] = [this._currentForIndex];
            //more blocks?
            if (items.length) {
                return thiz.runFromDirect(items, 0, settings);
            }
        },
        _solve: function (scope, settings) {
            var dfd = new Deferred(),
                self = this;
            var result = this.solveSubs(dfd, null, this.items, settings);
            if (result) {
                all(result).then(function (res) {
                    var falsy = res.indexOf(false);
                    if (self.ignoreErrors !== true && falsy !== -1) {
                        dfd.resolve(false);
                    } else {
                        dfd.resolve(true);
                    }
                });
            } else {
                dfd.resolve(true);
            }

            return dfd;
        },
        step: function (scope, settings) {
            var state = this._checkCondition(scope, settings);
            var dfd = new Deferred();
            if (state) {
                //run blocks
                var subs = this._solve(scope, settings);
                subs.then(function (result) {
                    if (result == true) {
                        dfd.resolve(true);
                    } else {
                        dfd.resolve(false);
                    }
                });
            }
            return dfd;
        },
        loop: function (scope, settings) {
            var stepResult = this.step(scope, settings),
                self = this;
            stepResult.then(function (proceed) {
                self._updateCounter(scope);
                self._currentForIndex = self._counter.value;
                if (proceed == true) {
                    self.loop(scope, settings);
                } else {
                    self.onFailed(self, settings);
                }
            });
        },
        _solveDirect: function (scope, settings) {
            return this.solveSubsDirect(null, null, this.items, settings);
        },
        stepDirect: function (scope, settings) {
            return this._solveDirect(scope, settings);
        },
        loopDirect: function (scope, settings) {
            this.stepDirect(scope, settings)
            for (var index = parseInt(this.initial, 10); index < parseInt(this['final'], 10); index++) {
                this.stepDirect(scope, settings);
            }
        },

        // solves the for block (runs the loop)
        solve: function (scope, settings) {
            // 1. Create and initialize counter variable
            this._counter = new Variable({
                title: this.counterName,
                value: this.initial,
                scope: scope,
                register: false
            });
            //prepare
            this._forState = true;
            this._currentForIndex = this.initial;
            this.deferred ? this.loop(scope, settings) : this.loopDirect(scope, settings);
            return [];
        },
        // checks the loop condition
        _checkCondition: function (scope, settings) {
            var expression = '' + this._counter.value + this.comparator + this['final'];
            var result = scope.parseExpression(expression);
            if (result != false) {
                this.onSuccess(this, settings);
            }
            this._forState = result;
            return result;
        },
        // updates the counter
        _updateCounter: function (scope) {
            var value = this._counter.value;
            var expression = '' + value + this.modifier;
            value = scope.parseExpression(expression);
            // Detect infinite loops
            if (value == this._counter.value) {
                return false;
            } else {
                this._counter.value = value;
                return true;
            }
        },
        /**
         * Store function override
         * @returns {boolean}
         */
        mayHaveChildren: function () {
            return this.items != null && this.items.length > 0;
        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            var result = [];
            if (this.items) {
                result = result.concat(this.items);
            }
            return result;
        },
        /**
         * should return a number of valid classes
         * @returns {Array}
         */
        canAdd: function () {
            return [];
        },
        /**
         * UI, Block row editor, returns the entire text for this block
         * @returns {string}
         */
        toText: function () {
            return this.getBlockIcon('F') + this.name + ' ' + this.initial + ' ' + this.comparator + ' ' + this['final'] + ' with ' + this.modifier;
        },
        /**
         * UI
         * @returns {*[]}
         */
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields = fields.concat([
                this.utils.createCI('initial', 13, this.initial, {
                    group: 'General',
                    title: 'Initial',
                    dst: 'initial'
                }),
                this.utils.createCI('Final', 13, this['final'], {
                    group: 'General',
                    title: 'Final',
                    dst: 'final'
                }),
                this.utils.createCI('comparator', 13, this.comparator, {
                    group: 'General',
                    title: 'Comparision',
                    dst: 'comparator'
                }),
                this.utils.createCI('modifier', 13, this.modifier, {
                    group: 'General',
                    title: 'Modifier',
                    dst: 'modifier'
                }),
                this.utils.createCI('Abort on Error', 0, this.ignoreErrors, {
                    group: 'General',
                    title: 'Ignore Errors',
                    dst: 'ignoreErrors'
                }),
                this.utils.createCI('Deferred', 0, this.deferred, {
                    group: 'General',
                    title: 'Use Deferred',
                    dst: 'deferred'
                })
            ]);
            return fields;
        }
    });
});
},
'xblox/model/variables/Variable':function(){
/** @module xblox/model/variables/Variable */
define([
    'dcl/dcl',
    'xide/types',
    "xblox/model/Block"
], function(dcl,types,Block){
    /**
     *  The command model. A 'command' consists out of a few parameters and a series of
     *  expressions. Those expressions need to be evaluated before send them to the device
     *
     * @class module:xblox.model.variables.Variable
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     */
    return dcl(Block,{
        declaredClass:"xblox.model.variables.Variable",
        //name: String
        //  the variable's name, it should be unique within a scope
        name:null,

        //value: Current variable value
        value:null,

        register:true,

        readOnly:false,

        initial:null,
        
        isVariable:true,
        flags: 0x000001000,
        getValue:function(){
            return this.value;
        },
        canDisable:function(){
            return false;
        },
        canMove:function(){
            return false;
        },
        getIconClass:function(){
            return 'el-icon-quotes-alt';
        },
        getBlockIcon:function(){
            return '<span class="'+this.icon+'"></span> ';
        },
        toText:function(){
            return "<span class='text-primary'>" + this.getBlockIcon() +  this.makeEditable('name','right','text','Enter a unique name','inline') +"</span>";
        },
        solve:function(){

            var _result = this.scope.parseExpression(this.getValue(),true);
            // 0 && console.log('resolved variable ' + this.title + ' to ' + _result);
            return [];

        },
        getFields:function(){
            var fields = this.getDefaultFields();
            var thiz=this,
                defaultArgs = {
                    allowACECache:true,
                    showBrowser:false,
                    showSaveButton:true,
                    editorOptions:{
                        showGutter:false,
                        autoFocus:false,
                        hasConsole:false
                    },
                    aceOptions:{
                        hasEmmet:false,
                        hasLinking:false,
                        hasMultiDocs:false
                    },
                    item:this
                };

            fields.push(this.utils.createCI('title',types.ECIType.STRING,this.name,{
                group:'General',
                title:'Name',
                dst:'name'
            }));

            fields.push(this.utils.createCI('value',types.ECIType.EXPRESSION,this.value,{
                group:'General',
                title:'Value',
                dst:'value',
                delegate:{
                    runExpression:function(val,run,error){
                        return thiz.scope.expressionModel.parse(thiz.scope,val,false,run,error);
                    }
                }
            }));

            


            //this.types.ECIType.EXPRESSION_EDITOR
            /*
            fields.push(this.utils.createCI('initial',this.types.ECIType.EXPRESSION,this.initial,{
                group:'General',
                title:'Initial',
                dst:'initial',
                widget:defaultArgs,
                delegate:{
                    runExpression:function(val,run,error){
                        if(thiz.group=='processVariables'){
                            var _val = thiz.scope.getVariable("value");
                            var extra = "";
                            if(_val) {
                                _val = _val.value;
                                if(!thiz.isNumber(_val)){
                                    _val = ''+_val;
                                    _val = "'" + _val + "'";
                                }
                                extra = "var value = " + _val +";\n";
                            }
                        }
                        return thiz.scope.expressionModel.parse(thiz.scope,extra + val,false,run,error);
                    }
                }
            }));
            */
            return fields;
        }
    });
});
},
'xblox/model/loops/WhileBlock':function(){
define([
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/variables/Variable"
], function(dcl,Block,Variable){

    // summary:
    //		The while block model. It repeats a block a number of times, while the condition is true.
    //

    // module:
    //		xblox.model.loops.WhileBlock
    return dcl(Block,{

        declaredClass:"xblox.model.loops.WhileBlock",
        // condition: (String) expression to be evaluated every step
        condition: null,
        /**
         * Blocks to be executed while the condition is true
         * @type {xblox.model.Block[]}
         * @inheritDoc
         */
        items: null,

        loopLimit: 1500,

        name:'While',

        wait:0,

        _currentIndex:0,

        sharable:true,

        icon:"",

        _timer:null,

        //  standard call from interface
        canAdd:function(){
            return [];
        },
        _solve:function(scope,settings){
            var ret=[];
            for(var n = 0; n < this.items.length ; n++)
            {
                this.items[n].solve(scope,settings);
            }
            return ret;
        },

        doStep:function(settings){

            if(this._currentIndex < this.loopLimit){

                var ret = [];

                var _cond = this._checkCondition(this.scope);
                if(_cond) {
                    this.onSuccess(this,settings);
                    this.addToEnd( ret , this._solve(this.scope,settings));
                    this._currentIndex++;
                }else{
                    if(this._timer){
                        clearInterval(this._timer);
                    }


                    this.onFailed(this,settings);
                }
            }else{
                console.error('--while block : reached loop limit');
                this.reset();
            }
        },
        reset:function(){
            if(this._timer){
                clearTimeout(this._timer);
                this._timer = null;
            }
            this._currentIndex=0;


        },
        // solves the while block (runs the loop)
        solve:function(scope,settings) {

            // 0 && console.log('solve while ');
            this.loopLimit = 1500;
            settings = settings || { };

            var iterations = 0;

            var ret = [],
                thiz = this;

            var delay = this._getArg(this.wait);

            this.reset();

            // has delay
            if(delay>0){

                this._timer = setInterval(function(){
                    thiz.doStep(settings);
                },delay);

                return [];
            }

            // Evaluate condition
            while ((this._checkCondition(scope)) && (iterations < this.loopLimit)) {
                this._solve(scope,settings);
                iterations++;
            }
            //cleanup

            this.reset();

            return ret;

        },

        /**
         * Block row editor, returns the entire text for this block
         * @returns {string}
         */
        toText:function(){
            return this.getBlockIcon('G') + this.name + ' ' + this.condition;
        },

        // checks the loop condition
        _checkCondition:function(scope) {
            return scope.parseExpression(this.condition);
        },
        /**
         * Store function override
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren:function(parent){
            return this.items!=null && this.items.length>0;
        },

        /**
         * Store function override
         * @param parent
         * @returns {Array}
         */
        getChildren:function(parent){
            var result=[];

            if(this.items){
                result=result.concat(this.items);
            }
            return result;
        },
        getFields:function(){


            var thiz=this;

            var fields = this.inherited(arguments) || this.getDefaultFields();

            fields.push(

                this.utils.createCI('condition',25,this.condition,{
                    group:'General',
                    title:'Expression',
                    dst:'condition',
                    delegate:{
                        runExpression:function(val,run,error){
                            return thiz.scope.expressionModel.parse(thiz.scope,val,false,run,error);
                        }
                    }
                })
            );

            fields.push(this.utils.createCI('wait',13,this.wait,{
                    group:'General',
                    title:'Wait',
                    dst:'wait'
            }));
            return fields;
        }


    });
});
},
'xblox/model/variables/VariableAssignmentBlock':function(){
/** @module xblox/model/variables/VariableAssignmentBlock **/
define([
    'dcl/dcl',
    "xblox/model/Block",
    "xide/utils",
    "xide/types",
    'dstore/legacy/DstoreAdapter',
    "xide/factory",
    'xdojo/has'
], function(dcl,Block,utils,types,DstoreAdapter,factory,has){

    var isServer =  0 ;
    var BLOCK_INSERT_ROOT_COMMAND = 'Step/Insert';
    


    /**
     *
     * @class module:xblox/model/variables/VariableAssignmentBlock
     * @extends xblox/model/Block
     */
    var Module = dcl(Block,{
        declaredClass: "xblox.model.variables.VariableAssignmentBlock",

        //variable: (String)
        //  variable title
        variable:null,

        //value: (String)
        // Expression to be asigned
        value:null,
        name:'Set Variable',
        icon:'',
        hasInlineEdits:true,
        flags:0x00000004,
        toText:function(){
            var _variable = this.scope.getVariableById(this.variable);
            var _text = _variable ? _variable.name : '';
            if(this.variable && this.variable.indexOf('://')!==-1) {
                _text = '<span class="text-info">' +this.scope.toFriendlyName(this, this.variable)+'</span>';
            }
            return this.getBlockIcon('C') + this.name + ' ' + _text  + "<span class='text-muted small'> to <kbd class='text-warning'>" + this.makeEditable("value",'bottom','text','Enter the string to send','inline') + "</kbd></span>";
        },
        _getPreviousResult: function () {
            var parent = null;
            var prev = this.getPreviousBlock();
            if(!prev || !prev._lastResult || !prev.enabled){
                parent = this.getParent();
            }else{
                parent = prev;
            }

            if (parent && parent._lastResult != null) {
                if (this.isArray(parent._lastResult)) {
                    return parent._lastResult;
                } else {
                    return parent._lastResult;
                }
            }
            return null;
        },
        /***
         * Makes the assignation
         * @param scope
         */

        solve:function(scope,settings) {
            var value = this.value;
            var changed = false;
            if(!value){
                var _value = this.getArgs();
                if(_value.length>0){
                    value = _value[0];
                }
            }
            if (this.variable && value!==null){

                this.onRun(this,settings);
                //var _variable = scope.getVariable(this.variable).value = scope.parseExpression(this.value);
                var _variable = this.variable.indexOf('://')!==-1 ? this.scope.resolveBlock(this.variable) : scope.getVariableById(this.variable);
                // 0 && console.log('assign variable',settings);
                var _value = this._getArg(value);

                var _args = this.getArgs(settings) || [];
                // 0 && console.log('run with args ' , _args);

                if(!_variable){
                    //console.error('     no such variable : ' + this.variable);
                    return [];
                }
                var _parsed = null;
                if(this.isScript(_value)){
                    var override = this.override || {};
                    _parsed = scope.parseExpression(value,null,null,null,null,null,_args || override.args);
                    //_parsed = scope.parseExpression(_value);
                    _parsed = this.replaceAll("'",'',_parsed);
                    //_variable.value = scope.parseExpression(_value);
                    //_variable.value = this.replaceAll("'",'',_variable.value);

                    if(_variable.value!==_parsed){
                        changed = true;
                    }

                }else{

                    if(_args && _args.length==1 && value==null){
                        _value = _args[0];
                    }

                    if(_variable.value!==_value){
                        changed = true;
                    }

                    _variable.value = _value;
                    _parsed = _value;
                }


                _variable.set('value',_parsed);

                var publish = false;


                var context = this.getContext();
                if(context) {
                    var device = context.device;
                    if(device && device.info && isServer && device.info.serverSide) {
                        if (this.flags & types.VARIABLE_FLAGS.PUBLISH_IF_SERVER) {
                            publish = true;
                        }else{
                            publish=false;
                        }
                    }
                }

                if(this.flags & types.VARIABLE_FLAGS.PUBLISH && changed){
                    publish = true;
                }

                changed && factory.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED,{
                    item:_variable,
                    scope:this.scope,
                    save:false,
                    block:this,
                    name:_variable.name,
                    value:_value,
                    publish:publish,
                    result:_parsed
                });
                this.onSuccess(this,settings);
                return [];
            }
        },
        canAdd:function(){
            return null;
        },
        getFields:function(){

            var fields = this.inherited(arguments) || this.getDefaultFields(false);
            var thiz=this;

            /*
            fields.push(this.utils.createCI('Variable',3,this.variable,{
                    group:'General',
                    dst:'variable',
                    widget:{
                        store:new DstoreAdapter(this.scope.blockStore),
                        query:{
                            group:'basicVariables'
                        }
                    }
            }));
            */




            fields.push(this.utils.createCI('value',29,this.value,{
                    group:'General',
                    title:'Value',
                    dst:'value',
                    widget:{
                        allowACECache:true,
                        showBrowser:false,
                        showSaveButton:true,
                        editorOptions:{
                            showGutter:false,
                            autoSelect: false,
                            autoFocus: false,
                            hasConsole:false,
                            hasItemActions:function(){
                                return false
                            }
                        },
                        item:this
                    },
                    delegate:{
                        runExpression:function(val,run,error){
                            return thiz.scope.expressionModel.parse(thiz.scope,val,false,run,error);
                        }
                    }
            }));




            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.variable,{
                group:'Variable',
                title:'Variable',
                dst:'variable',
                //options:this.scope.getVariablesAsOptions(),
                block:this,
                pickerType:'variable',
                value:this.variable,
                widget:{
                    store:this.scope.blockStore,
                    labelField:'name',
                    valueField:'id',
                    value:this.variable,
                    query:[
                        {
                            group:'basicVariables'
                        },
                        {
                            group:'processVariables'
                        }
                    ]

                }
            }));

            fields.push(this.utils.createCI('flags',5,this.flags,{
                group:'Variable',
                title:'Flags',
                dst:'flags',
                data:[
                    {
                        value: 0x00000002,
                        label: 'Publish to network',
                        title:"Publish to network in order to make a network variable"
                    },
                    {
                        value: 0x00000004,//2048
                        label: 'Publish if server',
                        title: 'Publish only on network if this is running server side'
                    }
                ],
                widget:{
                    hex:true
                }

            }));

            return fields;
        }
    });

    return Module;

});
},
'xblox/model/logic/IfBlock':function(){
/** @module xblox/model/logic/IfBlock **/
define([
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/Statement",
    "xblox/model/logic/ElseIfBlock",
    "dojo/Deferred",
    "xide/utils"
], function (dcl, Block, Statement, ElseIfBlock, Deferred, utils) {

    /**
     * Base block class.
     *
     * @class module:xblox/model/logic/IfBlock
     * @augments module:xblox/model/ModelBase
     * @extends module:xblox/model/Block
     */
    return dcl(Block, {
        declaredClass: "xblox.model.logic.IfBlock",
        // condition: (String) expression to be evaluated
        condition: 'Invalid Expression',

        // consequent: (Block) block to be run if the condition is true
        consequent: null,

        // elseIfBlocks: (optional) Array[ElseIfBlock] -> blocks to be run if the condition is false. If any of these blocks condition is
        //          true, the elseIf/else sequence stops
        elseIfBlocks: null,

        // alternate: (optional) (Block) -> block to be run if the condition is false and none of the "elseIf" blocks is true
        alternate: null,

        //  standard call from interface
        canAdd: function () {
            return [];
        },

        //  autoCreateElse : does auto creates the else part
        autoCreateElse: true,

        //  name : this name is displayed in the block row editor
        name: 'if',

        icon: '',
        //  add
        //
        // @param proto {mixed : Prototype|Object} : the new block's call prototype or simply a ready to use block
        // @param ctrArgs {Array} : constructor arguments for the new block
        // @param where {String} : consequent or alternate or elseif
        // @returns {Block}
        //
        add: function (proto, ctrArgs, where) {
            if (where == null) {
                where = 'consequent';
            }
            return this._add(proto, ctrArgs, where, false);
        },
        //  overrides default store integration
        __addToStore: function (store) {
            //add our self to the store
            store.put(this);
        },
        /**
         * Store function override
         * @returns {boolean}
         */
        mayHaveChildren: function () {
            return (this.items !== null && this.items.length) ||
                (this.elseIfBlocks !== null && this.elseIfBlocks.length) ||
                (this.consequent != null && this.consequent.length) ||
                (this.alternate != null && this.alternate.length);

        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            var result = [];
            if (this.consequent) {
                result = result.concat(this.consequent);
            }
            if (this.elseIfBlocks) {
                result = result.concat(this.elseIfBlocks);
            }
            if (this.alternate) {
                result = result.concat(this.alternate);
            }
            return result;
        },
        /**
         * Block row editor, returns the entire text for this block
         * @returns {string}
         */
        toText: function () {
            return "<span class='text-primary'>" + this.getBlockIcon('E') + this.name + " </span>" + "<span class='text-warning small'>" + this.condition + "<span>";
        },
        _checkCondition: function (scope) {
            return scope.parseExpression(this.condition, null, null);
        },
        solve: function (scope, settings) {
            // 1. Check the condition
            var solvedCondition = this._checkCondition(scope);
            var elseIfBlocks = this.getElseIfBlocks();
            var others = this.childrenByNotClass(ElseIfBlock);
            var result = null;

            others = others.filter(function (block) {
                return !block.isInstanceOf(Statement);
            });

            // 2. TRUE? => run consequent
            if (solvedCondition == true || solvedCondition > 0) {
                this.onSuccess(this, settings);
                if (others && others.length) {
                    for (var i = 0; i < others.length; i++) {
                        result = others[i].solve(scope, settings);
                    }
                }
                return result;
            } else {
                // 3. FALSE?
                var anyElseIf = false;
                this.onFailed(this, settings);
                if (elseIfBlocks) {
                    // 4. ---- check all elseIf blocks. If any of the elseIf conditions is true, run the elseIf consequent and
                    //           stop the process
                    for (var n = 0; ( n < elseIfBlocks.length ) && (!anyElseIf); n++) {
                        var _elseIfBlock = elseIfBlocks[n];
                        if (_elseIfBlock._checkCondition(scope)) {
                            _elseIfBlock.onSuccess(_elseIfBlock, settings);
                            anyElseIf = true;
                            return _elseIfBlock.solve(scope, settings);
                        } else {
                            _elseIfBlock.onFailed(_elseIfBlock, settings);
                        }
                    }
                }

                var alternate = this.childrenByClass(Statement);
                // 5. ---- If none of the ElseIf blocks has been run, run the alternate
                if (alternate.length > 0 && (!anyElseIf)) {
                    result = null;
                    for (var i = 0; i < alternate.length; i++) {
                        result = alternate[i].solve(scope, settings);
                    }
                    return result;
                }
            }
            return [];
        },
        /**
         * Default override empty. We have 3 arrays to clean : items, alternate and consequent
         */
        empty: function () {
            this._empty(this.alternate);
            this._empty(this.consequent);
            this._empty(this.elseIfBlocks);
        },
        /**
         * Deletes us or children block in alternate or consequent
         * @param what
         */
        removeBlock: function (what) {
            if (what) {
                if (what && what.empty) {
                    what.empty();
                }
                delete what.items;
                what.parent = null;
                this.alternate.remove(what);
                this.consequent.remove(what);
                this.elseIfBlocks.remove(what);
            }
        },
        // evaluate the if condition
        _getContainer: function (item) {
            if (this.consequent.indexOf(item) != -1) {
                return 'consequent';
            } else if (this.alternate.indexOf(item) != -1) {
                return 'alternate';
            } else if (this.elseIfBlocks.indexOf(item) != -1) {
                return 'elseIfBlocks';
            }
            return '_';
        },
        /**
         * Default override, prepare all variables
         */
        init: function () {
            this.alternate = this.alternate || [];
            this.consequent = this.consequent || [];
            this.elseIfBlocks = this.elseIfBlocks || [];

            for (var i = 0; i < this.alternate.length; i++) {
                this.alternate[i].parentId = this.id;
                this.alternate[i].parent = this;
            }
            for (var i = 0; i < this.elseIfBlocks.length; i++) {
                this.elseIfBlocks[i].parentId = this.id;
                this.elseIfBlocks[i].parent = this;
            }
            for (var i = 0; i < this.consequent.length; i++) {
                this.consequent[i].parentId = this.id;
                this.consequent[i].parent = this;
            }
            //var store = this.scope.blockStore;
        },
        getFields: function () {
            var thiz = this;
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(
                this.utils.createCI('condition', this.types.ECIType.EXPRESSION_EDITOR, this.condition, {
                    group: 'General',
                    title: 'Expression',
                    dst: 'condition',
                    delegate: {
                        runExpression: function (val, run, error) {
                            return thiz.scope.expressionModel.parse(thiz.scope, val, false, run, error);
                        }
                    }
                })
            );
            return fields;
        },
        postCreate: function () {
            if (this._postCreated) {
                return;
            }
            this._postCreated = true;
        },
        toCode: function (lang, params) {
        },
        getElseIfBlocks: function () {
            return this.childrenByClass(ElseIfBlock);
        },
        runAction: function (action) {
            var store = this.scope.blockStore;
            var command = action.command;
            if (command === 'New/Else' || command === 'New/Else If') {
                var newBlockClass = command === 'New/Else If' ? ElseIfBlock : Statement;
                var args = utils.mixin({
                        name: 'else',
                        items: [],
                        dstField: 'alternate',
                        parentId: this.id,
                        parent: this,
                        scope: this.scope,
                        canAdd: function () {
                            return [];
                        },
                        canEdit: function () {
                            return false;
                        }
                    }, newBlockClass == ElseIfBlock ? {name: 'else if', dstField: 'elseIfBlocks'} : {
                        name: 'else', dstField: 'alternate'
                    }
                );

                var newBlock = this.add(newBlockClass, args, newBlockClass == Statement ? 'alternate' : 'elseIfBlocks');
                var defaultDfdArgs = {
                    select: [newBlock],
                    focus: true,
                    append: false,
                    expand: true,
                    delay: 10
                };
                var dfd = new Deferred();
                store._emit('added', {
                    target: newBlock
                });
                dfd.resolve(defaultDfdArgs);
                newBlock.refresh();
                return dfd;
            }
        },
        getActions: function () {
            var result = [];
            if (this.alternate.length == 0) {
                result.push(this.createAction({
                    label: 'Else',
                    command: 'New/Else',
                    icon: this.getBlockIcon('I'),
                    tab: 'Home',
                    group: 'File',
                    mixin: {
                        addPermission: true,
                        custom: true
                    }
                }));
            }
            result.push(this.createAction({
                label: 'Else If',
                command: 'New/Else If',
                icon: this.getBlockIcon('I'),
                tab: 'Home',
                group: 'File',
                mixin: {
                    addPermission: true,
                    custom: true
                }
            }));
            return result;
        }
    });
});
},
'xblox/model/Statement':function(){
define([
    "dcl/dcl",
    "xblox/model/Block"
], function(dcl,Block){

    // summary:
    //		The statement block is only a wrapper for items like in 'else'

    // module:
    //		xblox.model.Statement
    return dcl(Block,{
        declaredClass:"xblox.model.Statement",
        /**
         * Return block name
         * @returns {name|*}
         */
        toText:function(){
            return this.name;
        },
        /**
         *
         * @returns {items|*}
         */
        getChildren:function(){
            return this.items;
        }
    });
});
},
'xblox/model/logic/ElseIfBlock':function(){
define([
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/Contains"
], function (dcl, Block, Contains) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The ElseIf Block model. Each ElseIf block contains a condition and a consequent to be run if the condition
    //          is true
    //
    //      This block should have an "IfBlock" parent

    // module:
    //		xblox.model.logic.ElseIfBlock
    return dcl([Block, Contains], {
        declaredClass: "xblox.model.logic.ElseIfBlock",
        //  condition: (String) expression to be evaluated
        condition: "",
        //  consequent: (Block) block to be run if the condition is true
        consequent: null,
        name: 'else if',
        icon: '',
        solve: function (scope, settings) {
            if (this._checkCondition(scope)) {
                return this._solve(scope, settings)
            }
            return false;
        },
        toText: function () {
            return "<span class='text-primary'>" + this.getBlockIcon('E') + this.name + " </span>" + "<span class='text-warning small'>" + (this.condition || "") + "<span>";
        },
        // checks the ElseIf condition
        _checkCondition: function (scope) {
            if (this.condition !== null) {
                return scope.parseExpression(this.condition);
            }
            return false;
        },
        getFields: function () {
            var thiz = this;
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(
                this.utils.createCI('condition', this.types.ECIType.EXPRESSION_EDITOR, this.condition, {
                    group: 'General',
                    title: 'Expression',
                    dst: 'condition',
                    delegate: {
                        runExpression: function (val, run, error) {
                            return thiz.scope.expressionModel.parse(thiz.scope, val, false, run, error);
                        }
                    }
                })
            );
            return fields;
        }
    });
});
},
'xblox/model/logic/SwitchBlock':function(){
/** @module xblox/model/logic/SwitchBlock **/
define([
    'dcl/dcl',
    "xblox/model/Block",
    "xblox/model/logic/CaseBlock",
    "xblox/model/logic/DefaultBlock",
    "dojo/Deferred",
    "xide/lodash"
], function (dcl, Block, CaseBlock, DefaultBlock, Deferred, _) {
    /**
     *
     * @class module:xblox/model/logic/SwitchBlock
     * @extends module:xblox/model/Block
     */
    return dcl(Block, {
        declaredClass: "xblox.model.logic.SwitchBlock",
        items: null,
        name: 'Switch',
        icon: null,
        toText: function () {
            return this.getBlockIcon('H') + this.name + ' ';
        },
        /**
         *
         * @param what {module:xblox/model/Block}
         * @returns {*}
         */
        canAdd: function (what) {
            if(what && what.isInstanceOf){
                return what.isInstanceOf(CaseBlock) || what.isInstanceOf(DefaultBlock);
            }
            return [];
        },
        getFields: function () {
            return this.getDefaultFields(false, false);
        },
        /***
         * Solve the switchblock
         * @param scope
         * @param settings
         * @returns {string} execution result
         */
        solve: function (scope, settings) {
            this._stopped = false;
            var anyCase = false;    // check if any case is reached
            var ret = [];
            this.onSuccess(this, settings);
            // iterate all case blocks
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];

                if (block.declaredClass === 'xblox.model.logic.CaseBlock'/* instanceof CaseBlock*/) {
                    var caseret;
                    // solve each case block. If the comparison result is false, the block returns "false"
                    caseret = block.solve(scope, this, settings);
                    if (caseret != false) {
                        // If the case block return is not false, don't run "else" block
                        anyCase = true;
                        this.addToEnd(ret, caseret);
                        break;
                    }
                }
                if (this._stopped) {
                    break;
                }
            }
            // iterate all "else" blocks if none of the cases occurs
            if (!anyCase) {
                for (var n = 0; n < this.items.length; n++) {
                    var block = this.items[n];
                    if (!(block.declaredClass == 'xblox.model.logic.CaseBlock')) {
                        this.addToEnd(ret, block.solve(scope, settings));
                    }
                }
            }
            return ret;
        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            return this.items;
        },
        stop: function () {
            this._stopped = true;
        },
        runAction: function (action) {
            var command = action.command;
            if (command === 'New/Case' || action.command === 'New/Default') {
                var store = this.scope.blockStore;
                var dfd = new Deferred();
                var newBlock = null;

                switch (command) {
                    case 'New/Case': {
                        newBlock = this.add(CaseBlock, {
                            comparator: "==",
                            expression: "on",
                            group: null
                        });
                        break;
                    }
                    case 'New/Default': {
                        newBlock = this.add(DefaultBlock, {
                            group: null
                        });
                        break;
                    }
                }
                dfd.resolve({
                    select: [newBlock],
                    focus: true,
                    append: false
                });
                newBlock.refresh();
                store._emit('added', {
                    target: newBlock
                });
            }
        },
        getActions: function (permissions, owner) {
            var result = [this.createAction({
                label: 'New Case',
                command: 'New/Case',
                icon: this.getBlockIcon('I'),
                tab: 'Home',
                group: 'File',
                mixin: {
                    addPermission: true,
                    custom: true,
                    quick: false
                }
            })];
            if (!_.find(this.items, {declaredClass: 'xblox.model.logic.DefaultBlock'})) {
                result.push(this.createAction({
                    label: 'Default',
                    command: 'New/Default',
                    icon: 'fa-eject',
                    tab: 'Home',
                    group: 'File',
                    mixin: {
                        addPermission: true,
                        custom: true,
                        quick: false
                    }
                }));
            }
            return result;
        }
    });
});
},
'xblox/model/logic/DefaultBlock':function(){
define([
    'dcl/dcl',
    'xblox/model/Block'
], function (dcl, Block) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The Case Block model. Each case block contains a comparation and a commands block.
    //      If the comparation result is true, the block is executed
    //
    //      This block should have an "SwitchBlock" parent

    // module:
    //		xblox.model.logic.CaseBlock
    return dcl(Block, {
        declaredClass: "xblox.model.logic.DefaultBlock",
        name: 'Default',
        icon: '',
        hasInlineEdits: false,
        toText: function () {
            return '&nbsp;<span class="fa-eject text-info"></span>&nbsp;&nbsp;<span>' + this.name + '</span>';
        },
        solve: function (scope, settings) {
            this.onSuccess(this, settings);
            return this._solve(scope, settings);
        }
    });
});
},
'xblox/model/variables/VariableSwitch':function(){
/** @module xblox/model/variables/VariableSwitch **/
define([
    'dcl/dcl',
    "xblox/model/logic/SwitchBlock",
    'xide/types',
    "xblox/model/logic/CaseBlock",
    "xblox/model/logic/DefaultBlock",
    "dojo/Deferred"
], function(dcl,SwitchBlock,types,CaseBlock,DefaultBlock,Deferred){
    /**
     *
     * The switch command model. These kind of commands takes a existing variable and applies some comparison.
     * Depending on the comparison results, the code into each case block is executed or not.
     * @class module:xblox/model/variables/VariableSwitch
     * @extends module:xblox/model/Block
     */
    return dcl(SwitchBlock,{
        declaredClass:"xblox.model.variables.VariableSwitch",
        name:'Switch on Variable',
        icon:'',
        variable:"PowerState",
        toText:function(){
            var _variable = this.scope.getVariableById(this.variable);
            var _text = _variable ? _variable.name : '';
            return this.getBlockIcon('H')  + this.name + ' ' + _text;
        },
        //  standard call for editing
        getFields:function(){
            //options:this.scope.getVariablesAsOptions(),
            var fields = this.getDefaultFields(false,false);
            fields = fields.concat([
                this.utils.createCI('Variable',3,this.variable,
                    {
                        group:'General',
                        widget:{
                            store:this.scope.blockStore,
                            labelField:'name',
                            valueField:'id',
                            query:[
                                {
                                    group:'basicVariables'
                                },
                                {
                                    group:'processVariables'
                                }
                            ]

                        },
                        dst:'variable'
                    }
                )
            ]);
            return fields;
        }
    });
});
},
'xblox/model/events/OnEvent':function(){
define([
    'dcl/dcl',
    "dojo/_base/lang",
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xblox/model/Referenced',
    'xide/registry',
    'dojo/on',
    'xwire/_Base'
], function(dcl,lang,Deferred,Block,utils,types,EventedMixin,Referenced,registry,on,_Base){




    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    return dcl([Block,EventedMixin.dcl,Referenced.dcl,_Base],{
        declaredClass:"xblox.model.events.OnEvent",
        //method: (String)
        //  block action name
        name:'On Event',
        event:'',
        reference:'',
        references:null,
        sharable:true,
        _didSubscribe:false,
        filterPath:"item.name",
        filterValue:"",
        valuePath:"item.value",
        _nativeEvents:[
            "onclick",
            "ondblclick",
            "onmousedown",
            "onmouseup",
            "onmouseover",
            "onmousemove",
            "onmouseout",
            "onkeypress",
            "onkeydown",
            "onkeyup",
            "onfocus",
            "onblur",
            "onchange"
        ],

        stop:function(){

            this._destroy();

        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        solve:function(scope,settings,isInterface,error) {

            if(isInterface){
                this._destroy();
            }

            settings = this._lastSettings = settings || this._lastSettings;

            if(!this._didSubscribe){
                this._registerEvent(this.event);
                this.onSuccess(this, settings);
                return false;
            }

            this.onSuccess(this, settings);

            this._currentIndex=0;
            this._return=[];

            var ret=[], items = this[this._getContainer()];
            if(items.length) {
                // 0 && console.log('solve ',settings);
                var res = this.runFrom(items,0,settings);
                this.onSuccess(this, settings);
                return res;
            }else{
                this.onSuccess(this, settings);
            }
            return ret;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){
            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if(this.event){
                result+= this.event;
            }
            return result;
        },

        //  standard call from interface
        canAdd:function(){
            return [];
        },

        //  standard call for editing
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz=this;

            var _ref = this.deserialize(this.reference);
            var isNative = utils.contains(this._nativeEvents,this.event)>-1;
            var options = null;
            if(!isNative){
                options = this.scope.getEventsAsOptions(this.event);
            }else{

                options = [
                    {label:"onclick", value:"onclick"},
                    {label:"ondblclick",value:"ondblclick"},
                    {label:"onmousedown",value:"onmousedown"},
                    {label:"onmouseup",value:"onmouseup"},
                    {label:"onmouseover",value:"onmouseover"},
                    {label:"onmousemove",value:"onmousemove"},
                    {label:"onmouseout",value:"onmouseout"},
                    {label:"onkeypress",value:"onkeypress"},
                    {label:"onkeydown",value:"onkeydown"},
                    {label:"onkeyup",  value:"onkeyup"},
                    {label:"onfocus",  value:"onfocus"},
                    {label:"onblur",  value:"onblur"},
                    {label:"onchange",  value:"onchange"}
                ];

                //select the event we are listening to
                for (var i = 0; i < options.length; i++) {
                    var obj = options[i];
                    if(obj.value===this.event){
                        obj.selected=true;
                        break;
                    }
                }
            }


            fields.push(utils.createCI('Event',types.ECIType.ENUMERATION,this.event,{
                group:'General',
                options:options,
                dst:'event',
                widget:{
                    search:true
                }
            }));

            fields.push(utils.createCI('Filter Path',13,this.filterPath,{
                group:'General',
                dst:'filterPath'
            }));

            fields.push(utils.createCI('Filter Value',13,this.filterValue,{
                group:'General',
                dst:'filterValue'
            }));

            fields.push(utils.createCI('Value Path',13,this.valuePath,{
                group:'General',
                dst:'valuePath'
            }));


            fields.push(utils.createCI('Object/Widget',types.ECIType.WIDGET_REFERENCE,this.reference,{
                group:'Widget',
                dst:'reference',
                value:this.reference
            }));
            return fields;
        },

        getBlockIcon:function(){
            return '<span class="fa-bell"></span>';
        },
        onEvent:function(evt){

            this._lastResult=evt;

            /*
            if(this.scope && evt.scope && evt.scope!==this.scope){
                return;
            }*/

            if(this.filterPath && this.filterValue){
                var value = this.getValue(evt,this.filterPath);
                if(value && this.filterValue !==value){
                    return;
                }
            }

            var eventValue = null;
            if(this.valuePath){

                if(!this._lastSettings){
                    this._lastSettings = {};
                }
                eventValue = this.getValue(evt,this.valuePath);
                if(eventValue!==null){
                    !this._lastSettings.override && (this._lastSettings.override = {});
                    this._lastSettings.override.args = [eventValue];
                }
            }

            // 0 && console.log('on event ',this._lastSettings);
            this.solve(this.scope,this._lastSettings);
        },
        _subscribe:function(evt,handler,obj){

            if(!evt){
                return;
            }
            var isNative = utils.contains(this._nativeEvents,evt);
            if(isNative==-1){

                if(this.__events && this.__events[evt]) {
                    var _handles = this.__events[evt];

                    _.each(_handles, function (e) {
                        this.unsubscribe(e.type, e.handler);
                        e.remove();
                    }, this);

                    _.each(_handles, function (e) {
                        this.__events[evt].remove(e);
                    }, this);
                }

                this.subscribe(evt, this.onEvent);
            }else{

                if(obj) {
                    var _event = evt.replace('on', ''),
                        thiz = this;

                    var handle = on(obj, _event, function (e) {
                        thiz.onEvent(e)
                    });
                     0 && console.log('wire native event : ' + _event);
                    this._events.push(handle);
                }

            }

        },
        _registerEvent:function(evt){

            try {
                if (!evt || !evt.length) {
                    return;
                }
                 0 && console.log('register event : ' + evt + ' for ' + this.reference);
                var objects = this.resolveReference(this.deserialize(this.reference));
                var thiz = this;
                if (objects && objects.length) {
                    for (var i = 0; i < objects.length; i++) {
                        var obj = objects[i];

                        //try widget
                        if (obj && obj.id) {
                            var _widget = registry.byId(obj.id);
                            if (_widget && _widget.on) {
                                var _event = this.event.replace('on', '');
                                 0 && console.log('found widget : ' + obj.id + ' will register event ' + _event);
                                var _handle = _widget.on(_event, lang.hitch(this, function (e) {
                                     0 && console.log('event triggered : ' + thiz.event);
                                    thiz.onEvent(e);
                                }));
                                this._events.push(_handle);
                            } else {

                                this._subscribe(evt, this.onEvent, obj);
                            }
                        } else {

                            this._subscribe(evt, this.onEvent, obj);
                        }
                    }
                     0 && console.log('objects found : ', objects);
                } else {
                    this._subscribe(evt, this.onEvent);
                }
            }catch(e){
                logError(e,'registering event failed');
            }
            this._didSubscribe=evt;
        },
        onLoad:function(){
            this._onLoaded=true;
            if(this.event && this.event.length && this.enabled){
                this._registerEvent(this.event);
            }
        },
        updateEventSelector:function(objects,cis){

            var options = [];

            if(!objects || !objects.length){
                options= this.scope.getEventsAsOptions(this.event);
            }else{

                options = [{label:"onclick", value:"onclick"},
                    {label:"ondblclick",value:"ondblclick"},
                    {label:"onmousedown",value:"onmousedown"},
                    {label:"onmouseup",value:"onmouseup"},
                    {label:"onmouseover",value:"onmouseover"},
                    {label:"onmousemove",value:"onmousemove"},
                    {label:"onmouseout",value:"onmouseout"},
                    {label:"onkeypress",value:"onkeypress"},
                    {label:"onkeydown",value:"onkeydown"},
                    {label:"onkeyup",  value:"onkeyup"},
                    {label:"onfocus",  value:"onfocus"},
                    {label:"onblur",  value:"onblur"},
                    {label:"onchange",  value:"onchange"}];

                //select the event we are listening to
                for (var i = 0; i < options.length; i++) {
                    var obj = options[i];
                    if(obj.value===this.event){
                        obj.selected=true;
                        break;
                    }
                }
            }

            for (var i = 0; i < cis.length; i++) {
                var ci = cis[i];
                if(ci['widget'] && ci['widget'].title==='Event'){
                    // 0 && console.log('event!');
                    var widget = ci['_widget'];
                    widget.nativeWidget.set('options',options);
                    widget.nativeWidget.reset();
                    widget.nativeWidget.set('value',this.event);
                    this.publish(types.EVENTS.RESIZE,{});
                    break;
                }
            }
        },
        onReferenceChanged:function(newValue,cis){

            this._destroy();//unregister previous event(s)

            this.reference = newValue;
            var objects = this.resolveReference(this.deserialize(newValue));
            this.updateEventSelector(objects,cis);
            this._registerEvent(this.event);

        },
        onChangeField:function(field,newValue,cis){

            if(field=='event'){
                this._destroy();    //unregister previous event
                if(this._onLoaded){ // we've have been activated at load time, so re-register our event
                    this.event = newValue;
                    this._registerEvent(newValue);
                }
            }
            if(field=='reference'){
                this.onReferenceChanged(newValue,cis);
            }

            this.inherited(arguments);
        },
        activate:function(){
            this._destroy();//you never know
            this._registerEvent(this.event);
        },
        deactivate:function(){
            this._destroy();
        },
        _destroy:function(){

            if(!this._events){this._events=[];}
            _.each(this._events, dojo.unsubscribe);
            this.unsubscribe(this.event,this.onEvent);
            this._lastResult=null;
            this._didSubscribe = false;
        }
    });
});
},
'xblox/model/Referenced':function(){
define([
    'dcl/dcl',
    "dojo/_base/declare",
    "xide/mixins/ReferenceMixin",
    "xide/utils"
], function (dcl,declare, ReferenceMixin,utils) {
    var Implementation = {
        /**
         * JSON String in that format : reference(string) | mode (string)
         */
        reference: null,
        /**
         * 'reference' is a JSON structure
         * @param value
         * @returns {*}
         */
        deserialize: function (value) {
            if (!value || value.length == 0) {
                return {};
            }
            try {
                return utils.fromJson(value);
            } catch (e) {
                return {};
            }
        }
    };
    /**
     * Holds information to locate an object by string or direct reference.
     * This must be used as mixin rather as base class!
     */
    var Module = declare('xblox.model.Referenced', [ReferenceMixin],Implementation);
    Module.dcl = dcl(ReferenceMixin.dcl,Implementation);
    return Module;
});
},
'xblox/model/events/OnKey':function(){
define([
    'dcl/dcl',
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xblox/model/Referenced',
    'xblox/model/Contains',
    'xblox/model/events/OnEvent',
    'xide/registry',
    'dojo/on'
], function(dcl,lang,array,Deferred,Block,utils,types,EventedMixin,Referenced,Contains,OnEvent,registry,on){

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    return dcl([Block,EventedMixin.dcl,Referenced.dcl,Contains],{
        declaredClass:"xblox.model.events.OnKey",
        //method: (String)
        //  block action name
        name:'On Key',

        event:'',

        reference:'',

        references:null,

        description:'Triggers when a keyboard sequence ' + this.event +' has been entered',

        listeners:null,

        sharable:true,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){

            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if(this.event){
                result+= this.event;
            }
            return result;
        },

        //  standard call from interface
        canAdd:function(){
            return [];
        },

        //  standard call for editing
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();

            fields.push(utils.createCI('Keyboard Sequence',types.ECIType.STRING,this.event,{
                group:'General',
                dst:'event',
                value:this.event,
                intermediateChanges:false
            }));

            fields.push(utils.createCI('Object/Widget',types.ECIType.WIDGET_REFERENCE,this.reference,{
                group:'General',
                dst:'reference',
                value:this.reference
            }));
            return fields;
        },
        getBlockIcon:function(){
            return '<span class="fa-keyboard-o"></span>';
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Store
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onEvent:function(evt){
            this._lastResult=evt;
            this.solve(this.scope,this._lastRunSettings);

        },
        _addListerner:function(keys,handler,obj){
            if(this.listeners==null){
                this.listeners=[];
            }

            var my_defaults = {
                is_unordered    : true,
                prevent_repeat  : false,
                prevent_default : false,
                on_keyup:function(e){
                     0 && console.log('up');
                },
                on_keydown:function(e){
                     0 && console.log('down');
                },
                on_release:function(e){
                     0 && console.log('release');
                }
            };
            var listener =null;
            listener = new window.keypress.Listener(obj, my_defaults);
            listener.simple_combo(keys, function(e) {
                if(handler){
                    handler(arguments);
                }
            });

            this.listeners.push(listener);
        },
        _subscribe:function(keys,handler,obj){

            if(!keys){
                return;
            }

            if(obj && obj.domNode){
                obj = obj.domNode;
            }

            this._addListerner(keys,handler,obj);

        },
        _registerEvent:function(evt){

            if(!evt || !evt.length){
                return;
            }
            var objects = this.resolveReference(this.deserialize(this.reference));
            var thiz=this;
            if (objects && objects.length) {
                for (var i = 0; i < objects.length; i++) {
                    var obj = objects[i];
                    //try widget
                    if (obj && obj.id) {
                        var _widget = registry.byId(obj.id);
                        _widget=null;
                        if (_widget && _widget.on) {
                            var _event = this.event.replace('on','');
                            var _handle = _widget.on(_event,lang.hitch(this,function(e){
                                thiz.onEvent(e);
                            }));
                            this._events.push( _handle);
                        }else{

                            this._subscribe(evt, function(){thiz.onEvent(arguments)},obj);
                        }
                    }else{

                        this._subscribe(evt, function(){thiz.onEvent(arguments)},obj);
                    }
                }
            }else{
                this._subscribe(evt, function(){thiz.onEvent(arguments)});
            }
        },
        onLoad:function(){

            this._onLoaded=true;

            if(this.event && this.event.length && this.enabled){

                this._registerEvent(this.event);
            }
        },
        destroy:function(){
            this.inherited(arguments);
        },
        updateEventSelector:function(objects,cis){

            var options = [];

            if(!objects || !objects.length){
                options= this.scope.getEventsAsOptions(this.event);
            }else{

                options = [{label:"onclick", value:"onclick"},
                    {label:"ondblclick",value:"ondblclick"},
                    {label:"onmousedown",value:"onmousedown"},
                    {label:"onmouseup",value:"onmouseup"},
                    {label:"onmouseover",value:"onmouseover"},
                    {label:"onmousemove",value:"onmousemove"},
                    {label:"onmouseout",value:"onmouseout"},
                    {label:"onkeypress",value:"onkeypress"},
                    {label:"onkeydown",value:"onkeydown"},
                    {label:"onkeyup",  value:"onkeyup"},
                    {label:"onfocus",  value:"onfocus"},
                    {label:"onblur",  value:"onblur"},
                    {label:"onchange",  value:"onchange"}];

                //select the event we are listening to
                for (var i = 0; i < options.length; i++) {
                    var obj = options[i];
                    if(obj.value===this.event){
                        obj.selected=true;
                        break;
                    }
                }
            }

            for (var i = 0; i < cis.length; i++) {
                var ci = cis[i];
                if(ci['widget'] && ci['widget'].title==='Event'){
                    // 0 && console.log('event!');
                    var widget = ci['_widget'];
                    widget.nativeWidget.set('options',options);
                    widget.nativeWidget.reset();
                    widget.nativeWidget.set('value',this.event);
                    this.publish(types.EVENTS.RESIZE,{});
                    break;
                }
            }
        },
        onReferenceChanged:function(newValue,cis){

            this._destroy();//unregister previous event(s)

            this.reference = newValue;
            var objects = this.resolveReference(this.deserialize(newValue));
            this._registerEvent(this.event);

        },
        onChangeField:function(field,newValue,cis){

            if(field=='event'){
                this._destroy();    //unregister previous event
                if(this._onLoaded){ // we've have been activated at load time, so re-register our event
                    this.event = newValue;
                    this._registerEvent(newValue);
                }
            }
            if(field=='reference'){
                this.onReferenceChanged(newValue,cis);
            }

            this.inherited(arguments);
        },
        activate:function(){
            this._destroy();//you never know
            this._registerEvent(this.event);
        },
        deactivate:function(){
            this._destroy();
        },
        _destroy:function(){

            if(this.listeners){

                for (var i = 0; i < this.listeners.length; i++) {
                    var obj = this.listeners[i];
                    obj.stop_listening();
                    var combos = obj.get_registered_combos();
                    if(combos){
                        obj.unregister_many(combos);
                    }
                    obj.reset();

                     0 && console.log('did destroy listener');

                }
            }
            this.listeners=[];
        },
        onFieldsRendered:function(block,cis){}


    });
});
},
'xblox/model/logging/Log':function(){
define([
    'dcl/dcl',
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin'
], function (dcl, Deferred, Block, utils, types, EventedMixin) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl([Block, EventedMixin.dcl], {
        declaredClass: "xblox.model.logging.Log",
        name: 'Log Message',
        level: 'info',
        message: 'return "Message: " + arguments[0];',
        _type: 'XBlox',
        host: 'this host',
        sharable: true,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function () {
            var _cls = 'text-primary';
            switch (this.level) {
                case 'info': {
                    _cls = 'text-info';
                    break;
                }
                case 'warn': {
                    _cls = 'text-warning';
                    break;
                }
                case 'error': {
                    _cls = 'text-danger';
                    break;
                }
            }
            var result = this.getBlockIcon() + " " + this.name + " : " + "<span class='" + _cls + " small'> " + ' :: ';
            if (this.message) {
                result += this.message;
            }
            return result + "</span>";
        },
        /***
         * Returns the block run result
         * @param expression
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {string}
         */
        _solveExpression: function (expression, scope, settings, run, error) {
            var _script = '' + expression;
            if (_script && _script.length) {
                _script = utils.convertAllEscapes(_script, "none");
                var _args = this.getArgs();
                try {
                    var _parsed = scope.parseExpression(_script, null, null, null, null, this, _args);
                    if (run) {
                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                    }
                    return _parsed;
                } catch (e) {
                    if (error) {
                        error('invalid expression : \n' + _script + ': ' + e);
                    }
                    this.onFailed(this, settings);
                    return _script;
                }
            }
            return _script;
        },
        /**
         *
         * @param scope
         * @param settings
         * @param run
         * @param error
         */
        solve: function (scope, settings, run, error) {
            var dfd = new Deferred();
            var device = scope.device;
            var _message = this._solveExpression(this.message, scope, settings, run, error);
            var message = {
                message: _message,
                level: this.level,
                type: this._type,
                details: this.getArgs(),
                time: new Date().getTime(),
                data: {
                    device: device ? device.info : null
                },
                write: true
            };
            this.onSuccess(this, settings);
            dfd.resolve(_message);
            try {
                this.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, message);
            } catch (e) {
                this.onFailed(this, settings);
            }

            return dfd;

        },
        //  standard call from interface
        canAdd: function () {
            return null;
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz = this;
            var options = [
                {
                    value: 'info',
                    label: 'Info'
                },
                {
                    value: 'warn',
                    label: 'Warning'
                },
                {
                    value: 'error',
                    label: 'Error'
                },
                {
                    value: 'debug',
                    label: 'Debug'
                },
                {
                    value: 'help',
                    label: 'Help'
                },
                {
                    value: 'verbose',
                    label: 'verbose'
                },
                {
                    value: 'silly',
                    label: 'Silly'
                }
            ];

            fields.push(utils.createCI('Level', 3, this.level, {
                group: 'General',
                options: options,
                dst: 'level'
            }));

            fields.push(
                utils.createCI('message', 25, this.message, {
                    group: 'General',
                    title: 'Message',
                    dst: 'message',
                    delegate: {
                        runExpression: function (val, run, error) {
                            thiz._solveExpression(val, thiz.scope, null, run, error);
                        }
                    }
                }));

            fields.push(
                utils.createCI('message', 13, this._type, {
                    group: 'General',
                    title: 'Type',
                    dst: '_type'
                }));

            return fields;
        },
        getBlockIcon: function () {
            return '<span class="fa-bug"></span>';
        }
    });
});
},
'xblox/model/html/SetStyle':function(){
/** @module xblox/model/html/SetStyle **/
define([
    "dcl/dcl",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xblox/model/Referenced',
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/_base/Color",
    "xide/registry"
     // not loaded yet
], function (dcl, Block, utils, types, EventedMixin, Referenced, domAttr, domStyle, Color, registry) {

    var debug = false;
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    /**
     *
     * @class module:xblox/model/html/SetStyle
     * @extends module:xblox/model/Block
     */
    var Impl = {
        declaredClass: "xblox.model.html.SetStyle",
        name: 'Set Style',
        reference: '',
        references: null,
        description: 'Sets HTML Node Style Attribute',
        value: '',
        mode: 1,
        sharable: true,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         *
         * @param params (object in that format : reference(string) | mode (string))
         */
        /**
         * Run this block
         * @param scope
         * @param settings
         */
        solve: function (scope, settings) {
            debug &&  0 && console.log('-set style solve');
            var value = this.value;
            settings = settings || {};
            var override = settings.override || this.override || {};

            if (override.variables) {
                value = utils.replace(value, null, override.variables, {
                    begin: '{',
                    end: '}'
                });
            }

            if (override.args && override.args[0] !== null) {
                value = utils.replace(value, null, {value: override.args[0]}, {
                    begin: '{',
                    end: '}'
                });
            }
            this.updateObjects(null, value, this.mode, settings);
            this.onSuccess(this, settings);
            this.onDidRun();
        },
        /**
         * Get human readable string for the UI
         * @returns {string}
         */
        toText: function () {
            var _ref = this.deserialize(this.reference);
            var result = this.getBlockIcon() + ' ' + this.name + ' :: on ' + _ref.reference + ' to' || ' ' + ' to ';
            if (this.value) {
                result += ' ' + this.value;
            }
            return result;
        },
        /**
         * Standard call when editing this block
         * @returns {*}
         */
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('Value', types.ECIType.DOM_PROPERTIES, this.value, {
                group: 'General',
                dst: 'value',
                value: this.value,
                intermediateChanges: false
            }));
            fields.push(utils.createCI('Mode', types.ECIType.ENUMERATION, this.mode, {
                group: 'General',
                options: [
                    utils.createOption('Set', 1),
                    utils.createOption('Add', 2),
                    utils.createOption('Remove', 3),
                    utils.createOption('Increase', 4),
                    utils.createOption('Decrease', 5)
                ],
                dst: 'mode'
            }));
            var referenceArgs = {
                group: 'General',
                dst: 'reference',
                value: this.reference
            };

            if (this.scope) {
                if (this.scope.global) {
                    referenceArgs.window = this.scope.global;
                    referenceArgs.allowHTMLNodes = true;
                    referenceArgs.allowWidgets = false;

                }
                if (this.scope.document) {
                    referenceArgs.document = this.scope.document;
                }
            }
            fields.push(utils.createCI('Target', types.ECIType.WIDGET_REFERENCE, this.reference, referenceArgs));

            return fields;
        },
        getBlockIcon: function () {
            return '<span class="fa-paint-brush"></span>';
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Lifecycle
        //
        /////////////////////////////////////////////////////////////////////////////////////
        updateEventSelector: function (objects, cis) {
            var options = [];
            if (!objects || !objects.length) {
                options = this.scope.getEventsAsOptions(this.event);
            } else {
                options = [{label: "onclick", value: "onclick"},
                    {label: "ondblclick", value: "ondblclick"},
                    {label: "onmousedown", value: "onmousedown"},
                    {label: "onmouseup", value: "onmouseup"},
                    {label: "onmouseover", value: "onmouseover"},
                    {label: "onmousemove", value: "onmousemove"},
                    {label: "onmouseout", value: "onmouseout"},
                    {label: "onkeypress", value: "onkeypress"},
                    {label: "onkeydown", value: "onkeydown"},
                    {label: "onkeyup", value: "onkeyup"},
                    {label: "onfocus", value: "onfocus"},
                    {label: "onblur", value: "onblur"},
                    {label: "onchange", value: "onchange"}];

                //select the event we are listening to
                for (var i = 0; i < options.length; i++) {
                    var obj = options[i];
                    if (obj.value === this.event) {
                        obj.selected = true;
                        break;
                    }
                }
            }

            for (var i = 0; i < cis.length; i++) {
                var ci = cis[i];
                if (ci['widget'] && ci['widget'].title === 'Event') {
                    var widget = ci['_widget'];
                    widget.nativeWidget.set('options', options);
                    widget.nativeWidget.reset();
                    widget.nativeWidget.set('value', this.event);
                    this.publish(types.EVENTS.RESIZE, {});
                    break;
                }
            }
        },
        onReferenceChanged: function (newValue, cis, settings) {
            this.reference = newValue;
            this.references = this.resolveReference(this.deserialize(newValue), settings);
            this.updateObjects(this.references, this.value, null, settings);
        },
        getPropValue: function (stylesObject, prop) {
            for (var _prop in stylesObject) {
                if (_prop === prop) {
                    return stylesObject[_prop];
                }
            }
            return null;
        },
        _getStyle: function (name, obj, jObj) {
            switch (name) {
                case "height": {
                    return jObj.outerHeight();
                }
                case "width": {
                    return jObj.outerWidth();
                }
                case "color": {
                    return jObj.css("color");
                }
                case "border-color": {
                    return jObj.css("border-color") || "rgba(0,0,0,0)";
                }
            }

            return null;
        },
        updateObject: function (obj, style, mode, settings) {
            if (!obj) {
                return false;
            }
            mode = mode || 1;

            var _obj = obj.id ? registry.byId(obj.id) : null;
            if (_obj) {
                obj = _obj;
            }

            if (obj.domNode != null) {
                obj = obj.domNode;
            }
            var currentStyle = domAttr.get(obj, 'style');
            if (currentStyle === ";") {
                currentStyle = "";
            }
            if (currentStyle === "") {
                if (obj['lastStyle'] != null) {
                    currentStyle = obj['lastStyle'];
                } else {
                    currentStyle = style;
                }
            }

            if (currentStyle === ";") {
                currentStyle = style;
            }
            switch (mode) {
                //set
                case 1: {

                    var currentStyleMap = this._toObject(currentStyle);
                    var props = style.split(';');
                    var css = {};
                    for (var i = 0; i < props.length; i++) {
                        var _style = props[i].split(':');
                        if (_style.length == 2) {
                            currentStyleMap[_style[0]] = _style[1];
                        }
                    }
                    var styles = [];
                    for (var p in currentStyleMap) {
                        styles.push(p + ':' + currentStyleMap[p]);
                    }
                    $(obj).attr('style', styles.join(';'));
                    break;
                }
                //add
                case 2: {

                    var _newStyle = currentStyle + ';' + style,
                        _newStyleT = _.uniq(_newStyle.split(';')).join(';');

                    domAttr.set(obj, 'style', _newStyleT);
                    break;
                }
                //remove
                case 3: {
                    domAttr.set(obj, 'style', utils.replaceAll(style, '', currentStyle));
                    break;
                }
                //increase
                case 4:
                //decrease
                case 5: {
                    var numbersOnlyRegExp = new RegExp(/(\D*)(-?)(\d+)(\D*)/);
                    /**
                     * compute current style values of the object
                     * @type {{}}
                     */
                    var stylesRequested = this._toObject(style);
                    var stylesComputed = {};
                    var jInstance = $(obj);
                    ///determine from node it self
                    if (stylesRequested) {
                        for (var prop in stylesRequested) {
                            var currentStyle = this._getStyle(prop, obj, jInstance);
                            stylesComputed[prop] = currentStyle;
                        }
                    }

                    var _newStyleObject = {};
                    /**
                     * compute the new style
                     * @type {number}
                     */
                    for (var prop in stylesRequested) {
                        var _prop = '' + prop.trim();
                        var multiplicator = 1;
                        if (stylesComputed[_prop] != null) {

                            var _valueRequested = stylesRequested[prop];
                            var _valueComputed = stylesComputed[prop];

                            var _isHex = _valueRequested.indexOf('#') != -1;
                            var _isRGB = _valueRequested.indexOf('rgb') != -1;
                            var _isRGBA = _valueRequested.indexOf('rgba') != -1;

                            if (_isHex || _isRGB || _isRGBA) {

                                var dColorMultiplicator = dojo.colorFromString(_valueRequested);
                                var dColorNow = dojo.colorFromString(_valueRequested);
                                var dColorComputed = dojo.colorFromString(_valueComputed);
                                var dColorNew = new Color();

                                _.each(["r", "g", "b", "a"], function (x) {
                                    dColorNew[x] = Math.min(dColorComputed[x] + dColorMultiplicator[x], x == "a" ? 1 : 255);
                                });

                                 0 && console.log('color computed ' + dColorComputed.toRgba() + ' color requested: ' + dColorNow.toRgba() + ' | multiplicator color = ' + dColorMultiplicator.toRgba() + ' is then = ' + dColorNew.toRgba());

                                var _valueOut = '';
                                if (_isHex) {
                                    _valueOut = dColorNew.toHex();
                                } else if (_isRGB) {
                                    _valueOut = dColorNew.toCss(false);
                                } else if (_isRGBA) {
                                    _valueOut = dColorNew.toCss(true);
                                }
                                _newStyleObject[prop] = _valueOut;
                                domStyle.set(obj, prop, _valueOut);

                            } else {
                                //extract actual number :
                                var numberOnly = numbersOnlyRegExp.exec(stylesComputed[_prop]);
                                if (numberOnly && numberOnly.length >= 3) {
                                    var _int = parseInt(numberOnly[3]);
                                    if (_int && _int > 0) {
                                        multiplicator = _int;
                                    }
                                }
                            }
                        }
                    }
                    var delta = mode == 4 ? 1 : -1;
                    //now get an object array of the styles we'd like to alter
                    var styles = this._toObject(currentStyle);
                    var inStyles = this._toObject(style);
                    if (!styles) {
                        return false;
                    }
                    var _skipped = [];
                    for (var prop in styles) {
                        var _prop = '' + prop.trim();
                    }

                    var newStyleString = this._toStyleString(_newStyleObject);
                    break;
                }
            }
        },
        onDomStyleChanged: function (objects, newStyle, mode, settings) {
            objects = objects || this.resolveReference(this.deserialize(this.reference), settings);
            if (!objects) {
                debug &&  0 && console.warn('have no objects');
                return;
            }
            debug &&  0 && console.log('change dom style to ' + newStyle + ' on ' + objects.length + ' objects');
            for (var i = 0; i < objects.length; i++) {
                var obj = objects[i];
                if (obj && obj.id && obj.id.indexOf('davinci') != -1) {
                    continue;
                }
                this.updateObject(obj, newStyle, mode, settings);
            }
        },
        /**
         *
         * @param objects
         * @param domStyleString
         * @param mode
         * @param settings
         */
        updateObjects: function (objects, domStyleString, mode, settings) {
            objects = objects || this.resolveReference(this.deserialize(this.reference), settings);
            this.onDomStyleChanged(objects, domStyleString, mode, settings);
        },
        onChangeField: function (field, newValue, cis) {
            this._destroy();
            if (field == 'mode' && newValue !== this.mode) {
                this.mode = newValue;
            }
            if (field == 'value' && newValue !== this.value) {
                this.onDomStyleChanged(null, newValue, this.mode);
                this.value = newValue;
            }
            if (field == 'reference') {
                this.onReferenceChanged(newValue, cis);
            }
            this.inherited(arguments);
        },
        activate: function () {
            this._destroy();//you never know
        },
        deactivate: function () {
            this._destroy();
        },
        _destroy: function () {

        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        _changeValue: function (value, delta) {
            if (!value) {
                return "";
            }
            var split = value.split(" ");
            var result = "";
            for (var i = 0; i < split.length; i++) {
                if (i > 0)
                    result += " ";
                var bits = split[i].match(/([-\d\.]+)([a-zA-Z%]*)/);
                if (!bits) {
                    result += split[i];
                } else {
                    if (bits.length == 1) {
                        result += bits[0];
                    } else {
                        for (var z = 1; z < bits.length; z++) {
                            if (!isNaN(bits[z]) && bits[z] != "") {
                                result += parseFloat(bits[z]) + delta;
                            } else {
                                result += bits[z];
                            }
                        }
                    }
                }
            }
            return result;
        },
        /**
         * Convert Style String to an object array, eg: { color:value,.... }
         * @param styleString
         * @returns {{}}
         * @private
         */
        _toObject: function (styleString) {
            if (!styleString) {
                return {};
            }
            var _result = {};
            var _values = styleString.split(';');
            for (var i = 0; i < _values.length; i++) {
                var obj = _values[i];
                if (!obj || obj.length == 0 || !obj.split) {
                    continue;
                }
                var keyVal = obj.split(':');
                if (!keyVal || !keyVal.length) {
                    continue;
                }
                var key = obj.substring(0, obj.indexOf(':'));
                var value = obj.substring(obj.indexOf(':') + 1, obj.length);

                _result[key] = value;
            }
            return _result;
        },
        _toStyleString: function (values) {
            var _values = [];
            for (var prop in values) {
                _values.push(prop + ':' + values[prop]);
            }
            return _values.join(';') + ';';
        }

    };

    //package via declare
    var _class = dcl([Block, Referenced.dcl], Impl);
    //static access to Impl.
    _class.Impl = Impl;
    return _class;

});
},
'xblox/model/html/SetCSS':function(){
define([
    "dojo/_base/declare",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xblox/model/Targeted'
], function(declare,Block,utils,types,EventedMixin,Targeted){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return declare("xblox.model.html.SetCSS",[Block,EventedMixin,Targeted],{
        //method: (String)
        //  block name
        name:'Set CSS',
        file:'',
        reference:'',
        references:null,
        description:'Sets HTML Node CSS',
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        solve:function(scope,settings) {
            this.onSuccess(this,settings);
        },
        toText:function(){
            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if(this.event){
                result+= this.event;
            }
            return result;
        },

        //  standard call for editing
        getFields:function(){
            try {
                var fields = this.inherited(arguments) || this.getDefaultFields();
                fields.push(utils.createCI('File', types.ECIType.FILE, this.file, {
                    group: 'General',
                    dst: 'file',
                    value: this.file,
                    intermediateChanges: false,
                    acceptFolders: false,
                    acceptFiles: true,
                    encodeFilePath: false,
                    buildFullPath: true,
                    filePickerOptions: {
                        dialogTitle: 'Select CSS File',
                        filePickerMixin: {
                            beanContextName: 'CSSFilePicker',
                            persistent: false,
                            globalPanelMixin: {
                                allowLayoutCookies: false
                            }
                        },
                        configMixin: {
                            beanContextName: 'CSSFilePicker',
                            LAYOUT_PRESET: types.LAYOUT_PRESET.SINGLE,
                            PANEL_OPTIONS:{
                                ALLOW_MAIN_MENU:false
                            }
                        },
                        defaultStoreOptions: {
                            "fields": 1663,
                            "includeList": "css",
                            "excludeList": "*"
                        },
                        startPath: this.file
                    }
                }));
                fields.push(utils.createCI('Target', types.ECIType.WIDGET_REFERENCE, this.reference, {
                    group: 'General',
                    dst: 'reference',
                    value: this.reference
                }));

            }catch(e){

            }
            return fields;
        },
        getBlockIcon:function(){
            return '<span class="fa-paint-brush"></span>';
        },
        onReferenceChanged:function(newValue){
            this._destroy();//unregister previous event(s)
            this.reference = newValue;
        },
        onChangeField:function(field,newValue,cis){
            if(field=='reference'){
                this.onReferenceChanged(newValue,cis);
            }
            this.inherited(arguments);
        },
        activate:function(){
            this._destroy();
        },
        deactivate:function(){
            this._destroy();
        },
        _destroy:function(){
        }
    });
});
},
'xblox/model/Targeted':function(){
define([
    "dojo/_base/declare",
    "./Referenced"
], function(declare,Referenced){

    /**
     * Targeted provides functions to get an object through various ways
     */
    return declare('xblox.model.Targeted',[Referenced],{

    });
});
},
'xblox/manager/BlockManager':function(){
define([
    'dcl/dcl',
    'dojo/has',
    'dojo/Deferred',
    'dojo/promise/all',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xblox/model/ModelBase',
    'xblox/model/Scope',
    'xblox/model/BlockModel',
    'xide/mixins/ReloadMixin',
    'xide/manager/ManagerBase',
    'xblox/data/Store',
    "xdojo/has!xblox-ui?xblox/manager/BlockManagerUI",
    "xide/lodash"
],function (dcl,has,Deferred,all,types,utils,factory,ModelBase,Scope,BlockModel,ReloadMixin,ManagerBase,Store,BlockManagerUI,_){

    var bases =  1  &&  1  ? [BlockManagerUI,ManagerBase,ReloadMixin.dcl] : [ManagerBase,ReloadMixin.dcl];
    var debug = false;

    return dcl(bases,{
        declaredClass:"xblox/manager/BlockManager",
        serviceObject:null,
        loaded:{},
        test:function(){

        },
        /***
         *  scope: storage for all registered variables / commands
         */
        scope:null,
        scopes:null,
        //track original create block function
        _createBlock:null,
        _registerActions:function(){},
        toScope:function(data){
            try {
                data = utils.getJson(data);
            } catch (e) {
                console.error('BlockManager::toScope: failed,err='+e);
                return null;
            }

            if(!data){
                console.error('correct data');
                data = {
                    "blocks": [
                    ],
                    "variables": []
                };
            }
            var scopeId = utils.createUUID();
            var blockInData = data;
            //check structure
            if (_.isArray(data)) {// a flat list of blocks

            } else if (_.isObject(data)) {
                scopeId = data.scopeId || scopeId;
                blockInData = data.blocks || [];
            }
            var scopeUserData = {
                owner:this
            };
            var blockScope = this.getScope(scopeId, scopeUserData, true);
            var allBlocks = blockScope.blocksFromJson(blockInData);
            for (var i = 0; i < allBlocks.length; i++) {
                var obj = allBlocks[i];
                obj._lastRunSettings = {
                    force: false,
                    highlight: true
                };
            }
            blockScope.serviceObject = this.serviceObject;
            return blockScope;
        },
        /**
         *
         * @param files{Object[]} array of items to load in this format
         * @example:
         * @returns {Deferred.promise}
         */
        loadFiles:function(files){

            var thiz=this,
                _createDfd = function(mount,path,force,publish)
                {
                    return thiz.load(mount,path,force);
                },
                _promises = [],
                dfd = new Deferred();

            //build promise chain for 'all'
            for (var i = 0; i < files.length; i++) {
                var item = files[i];
                _promises.push(_createDfd(item.mount, item.path, item.force, item.publish));
            }

            //run and resolve head
            all(_promises).then(function(results){
                debug &&  0 && console.log('got all block files ',results);
                dfd.resolve(results);
            });

            return dfd.promise;
        },
        load:function(mount,path,forceReload){
            var dfd = new Deferred(),
                thiz = this,
                _mount = utils.replaceAll('/','',mount),
                _path = utils.replaceAll('./','',path);

            var full = _mount + _path;
            full = full.trim();

            if(this.loaded[full] && forceReload===true){
                this.removeScope(this.loaded[full].id);
                this.loaded[full]=null;
            }

            if(forceReload !==true && this.loaded[full]){
                dfd.resolve(this.loaded[full]);
                return dfd.promise;
            }
            var _ready = function(data){
                var scope = thiz.toScope(data);
                if(scope){
                    thiz.loaded[full] = scope;

                    scope.mount = mount;//track file info
                    scope.path = path;
                }
                dfd.resolve(scope);
            };
            this.ctx.getFileManager().getContent(_mount,_path,_ready,false);
            return dfd.promise;
        },
        onBlocksReady:function(scope){
            var blocks = scope.allBlocks();
            for (var i = 0; i < blocks.length; i++) {
                var obj = blocks[i];
                this.setScriptFunctions(obj,scope,this);
            }
            /**
             * pick 'On Load' blocks
             */

            var loadBlocks = scope.getBlocks({
                group:'On Load'
            });
            if(loadBlocks && loadBlocks.length>0){
                for (var i = 0; i < loadBlocks.length; i++) {
                    var loadBlock  = loadBlocks[i];
                    if(loadBlock.onLoad){
                        loadBlock.onLoad();
                    }
                }
            }
        },
        getBlock:function(){

        },
        setScriptFunctions:function(obj,scope,owner){

            var thiz=owner;
            //scope.context = obj;//set the context of the blox scope
            if(!obj.blockScope) {
                obj.blockScope = scope;
            }
            debug &&  0 && console.log('set script functions ' + scope.id,obj);
            scope.serviceObject = this.serviceObject;
            ///////////////////////////////////////////////////////////////////////////////
            //
            //  Variables
            //
            ///////////////////////////////////////////////////////////////////////////////
            /**
             * Add 'setVariable'
             * @param title
             * @param value
             */
            if(!obj.setVariable) {
                obj.setVariable = function (title, value, save, publish, source) {
                    var _scope = this.blockScope;
                    var _variable = _scope.getVariable(title);
                    if (_variable) {
                        _variable.value = value;
                        debug &&  0 && console.log('setting variable '+title + ' to ' + value);
                    } else {
                        debug &&  0 && console.log('no such variable : ' + title);
                        return;
                    }
                    if (publish !== false) {

                        thiz.publish(types.EVENTS.ON_VARIABLE_CHANGED, {
                            item: _variable,
                            scope: scope,
                            driver: obj,
                            owner: thiz,
                            save: save === true,
                            source: source || types.MESSAGE_SOURCE.BLOX  //for prioritizing
                        });
                    }
                };
            }
            /**
             * Add getVariable
             * @param title
             */
            if(!obj.getVariable) {
                obj.getVariable = function (title) {
                    var _scope = this.blockScope;
                    var _variable = _scope.getVariable(title);
                    if (_variable) {
                        return _variable.value;
                    }
                    return '';
                };
            }

        },
        hasScope:function(id) {
            if (!this.scopes) {
                this.scopes = {};
            }
            if (this.scopes[id]) {
                return this.scopes[id];
            }
            return null;
        },
        getScope:function(id,userData,publish){
            if(!this.scopes){
              this.scopes={};
            }
            if(!this.scopes[id]){
                this.scopes[id]=this.createScope({
                    id:id,
                    ctx:this.ctx
                });
                this.scopes[id].userData=userData;
                if(publish!==false){
                    try{
                        factory.publish(types.EVENTS.ON_SCOPE_CREATED,this.scopes[id]);
                    }catch(e){
                        console.error('bad, scope creation failed ' +e ,e);
                    }
                }
            }
            return this.scopes[id];
        },
        /**
         *
         * @param id
         * @returns {null}
         */
        removeScope:function(id){
            if(!this.scopes){
                this.scopes={};
            }
            for (var scopeId in this.loaded){
                if(this.loaded[scopeId].id==id){
                    delete this.loaded[scopeId];
                }
            }
            if (this.scopes[id]) {
                this.scopes[id]._destroy();
                delete this.scopes[id];
            }
            return null;
        },
        /**
         *
         * @param mixed
         * @param data
         * @returns {*}
         */
        createScope:function(mixed,data,errorCB){
            data = data || [];
            var blockStore = new Store({
                data: [],
                Model:BlockModel,
                id:utils.createUUID(),
                __events:{

                },
                observedProperties:[
                    "name",
                    "enabled",
                    "value"
                ],
                getLabel:function(item){
                    return item.name;
                },
                labelAttr:'name'
            });
            blockStore.reset();
            blockStore.setData([]);
            var args = {
                owner:this,
                blockStore:blockStore,
                serviceObject:this.serviceObject,
                config:this.config
            };
            utils.mixin(args,mixed);
            try {
                var scope = new Scope(args);
                data && scope.initWithData(data,errorCB);
                scope.init();
            }catch(e){
                logError(e,'error creating scope, data:',mixed);
            }

            return scope;
        },
        onReloaded:function(){
            debug &&  0 && console.log('on reloaded');
        },
        init:function() {
            this.scope = {
                variables:[],
                blocks: []
            };
            ModelBase.prototype.types=types;
            ModelBase.prototype.factory=factory;
            if(this.onReady){
                this.onReady();
            }
        }
    });
});
},
'xblox/model/Scope':function(){
/** @module xblox/model/Scope **/
define([
    'dcl/dcl',
    './ModelBase',
    './Expression',
    'xide/factory',
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'dojo/_base/lang',
    'dojo/has',
    'xide/encoding/MD5',
    'xcf/model/Variable',
    'xdojo/has!host-node?nxapp/utils/_console'
], function (dcl, ModelBase, Expression, factory, utils, types, EventedMixin, lang, has, MD5, Variable, _console) {
    var console = typeof window !== 'undefined' ? window.console : typeof global !== 'undefined' ? global.console : _console
    if (_console) {
        console = _console
    }
    /*
     var console = typeof window !== 'undefined' ? window.console : console;
     if(tracer && tracer.error && console && console.error){
     console = _console;
     }
     */

    function mergeNewModule(block, source) {
        for (var i in source) {
            var o = source[i]
            if (o && _.isFunction(o) /* && lang.isFunction(target[i]) */ ) {
                block[i] = o // swap
            }
        }
    }

    var debug = false
    var isIDE = has('xcf-ui')
    /**
     * The scope acts as a real scope as usual. All registered variables and blocks are excecuted in this scope only.
     * @class module:xblox/model/Scope
     */
    var Module = dcl([ModelBase, EventedMixin.dcl], {
        declaredClass: 'xblox.model.Scope',
        variableStore: null,
        serviceObject: null,
        context: null,
        blockStore: null,
        /**
         *  @type {module:xblox/model/Expression}
         */
        expressionModel: null,
        start: function () {
            if (this.__didStartBlocks === true) {
                console.error('already started blocks')
                return
            }
            this.__didStartBlocks = true
            var responseVariable = this.getVariable('value')
            if (!responseVariable) {
                responseVariable = new Variable({
                    id: utils.createUUID(),
                    name: 'value',
                    value: '',
                    scope: this,
                    type: 13,
                    group: 'processVariables',
                    gui: false,
                    cmd: false
                })

                this.blockStore.putSync(responseVariable)
            }
            var autoBlocks = []
            var initBlocks = this.getBlocks({
                group: types.COMMAND_TYPES.INIT_COMMAND
            })

            var self = this;
            try {
                _.each(initBlocks, function (block) {
                    if (block.enabled !== false && block.__started !== true) {
                        block.solve(self);
                        block.__started = true
                    }
                }, this);
            } catch (e) {
                console.error('starting init blocks failed', e)
                logError(e, this)
            }
            autoBlocks = autoBlocks.concat(this.getBlocks({
                group: types.COMMAND_TYPES.BASIC_COMMAND
            }))

            // console.error('auto blocks : '+autoBlocks.length + ' ' + this.id);
            for (var i = 0; i < autoBlocks.length; i++) {
                var block = autoBlocks[i]
                if (block.enabled && block.start && block.startup && block.__started !== true) {
                    block.start()
                    block.__started = true
                }
            }
        },
        /**
         *
         * @returns {module:xblox/model/Expression}
         */
        getExpressionModel: function () {
            if (!this.expressionModel) {
                this.expressionModel = new Expression()
            }
            return this.expressionModel
        },
        /**
         *
         * @param block
         * @param url
         * @returns {*}
         */
        toFriendlyName: function (block, url) {
            if (!url || !block) {
                return null
            }
            var blockScope = this,
                ctx = this.ctx,
                driver = this.driver,
                deviceManager = ctx.getDeviceManager(),
                driverManager = ctx.getDriverManager()

            if (url.indexOf('://') == -1) {
                var _block = blockScope.getBlockById(url)
                if (_block) {
                    return _block.name
                }
                return url
            }
            var parts = utils.parse_url(url) // strip scheme

            parts = utils.urlArgs(parts.host) // go on with query string
            var _device = deviceManager.getItemById(parts.device.value)
            if (_device) {
                var info = deviceManager.toDeviceControlInfo(_device)
                driver = driverManager.getDriverById(info.driverId)
                var driverInstance = _device.driverInstance
                if (driverInstance || driver) {
                    blockScope = driver.blockScope ? driver.blockScope : driverInstance ? driverInstance.blockScope : blockScope
                    block = blockScope.getStore().getSync(parts.block.value)
                    if (block) {
                        return info.title + '/' + block.name
                    } else if (driverInstance && driverInstance.blockScope) {
                        block = driverInstance.blockScope.getBlock(parts.block.value)
                        if (block) {
                            return info.title + '/' + block.name
                        }
                    }
                }
            }
            return url
        },
        getContext: function () {
            return this.instance
        },
        toString: function () {
            var all = {
                blocks: null,
                variables: null
            }
            var blocks = this.blocksToJson()
            try {
                utils.fromJson(JSON.stringify(blocks))
            } catch (e) {
                debug && console.error('scope::toString : invalid data in scope')
                return
            }
            all.blocks = blocks
            return JSON.stringify(all, null, 2)
        },
        /**
         * @param data
         * @param errorCB {function}
         */
        initWithData: function (data, errorCB) {
            data && this.blocksFromJson(data, null, errorCB)
            this.clearCache()
        },
        // ///////////////////////////////////////////////////////
        //
        //  Service uplink related
        //
        // ///////////////////////////////////////////////////////
        /** @member {Object} */
        getService: function () {
            return this.serviceObject
        },
        // ///////////////////////////////////////////////////////
        //
        //  Store related
        //
        // ///////////////////////////////////////////////////////
        getStore: function () {
            return this.blockStore
        },
        reset: function () {
            this.getExpressionModel().reset()
        },
        /**
         *
         */
        empty: function () {
            this.clearCache()
            var store = this.blockStore
            var allBlocks = this.getBlocks()
            store.silent(true)
            _.each(allBlocks, function (block) {
                if (block) {
                    store.removeSync(block.id)
                } else {
                    debug && console.error('have no block')
                }
            })
            store.setData([])
            store.silent(false)
        },
        fromScope: function (source) {
            var store = this.blockStore
            store.silent(true)
            this.empty()
            var _t = source.blocksToJson()
            this.blocksFromJson(_t)
            store.silent(false)
        },
        /**
         *
         */
        clearCache: function () {
            this.getExpressionModel().reset()
        },
        /**
         * @returns {dojo/store/Memory}
         */
        getVariableStore: function () {
            return this.blockStore
        },
        getBlockStore: function () {
            return this.blockStore
        },
        getVariables: function (query) {
            if (!this.blockStore) {
                return []
            }
            var all = this.blockStore.data
            var out = []
            if (query && query.group === 'processVariables') {
                for (var i = 0; i < all.length; i++) {
                    if (all[i].group === 'processVariables') {
                        out.push(all[i])
                    }
                }
                return out
            }
            // query = query || {id:/\S+/};//all variables
            if (!query) {
                for (var i = 0; i < all.length; i++) {
                    var block = all[i],
                        cls = block.declaredClass
                    if (cls == 'xblox.model.variables.Variable' || cls == 'xcf.model.Variable') {
                        out.push(block)
                    }
                }
                return out
            }
            return this.blockStore.query(query)
        },
        loopBlock: function (block, settings) {
            if (block._destroyed == true) {
                console.error('block destroyed')
            }
            var interval = block.getInterval ? block.getInterval() : 0
            if (block && interval > 0 && block.enabled && block._destroyed !== true) {
                var thiz = this
                if (block._loop) {
                    clearInterval(block._loop)
                }
                block._loop = setInterval(function () {
                    if (!block.enabled || block._destroyed) {
                        clearInterval(block._loop)
                        block._loop = null
                        return
                    }
                    block.solve(thiz, settings || block._lastSettings)
                }, interval)
            }
        },
        getEventsAsOptions: function (selected) {
            var result = []
            for (var e in types.EVENTS) {
                var label = types.EVENTS[e]

                var item = {
                    label: label,
                    value: types.EVENTS[e]
                }
                result.push(item)
            }
            result = result.concat([{
                    label: 'onclick',
                    value: 'onclick'
                },
                {
                    label: 'ondblclick',
                    value: 'ondblclick'
                },
                {
                    label: 'onmousedown',
                    value: 'onmousedown'
                },
                {
                    label: 'onmouseup',
                    value: 'onmouseup'
                },
                {
                    label: 'onmouseover',
                    value: 'onmouseover'
                },
                {
                    label: 'onmousemove',
                    value: 'onmousemove'
                },
                {
                    label: 'onmouseout',
                    value: 'onmouseout'
                },
                {
                    label: 'onkeypress',
                    value: 'onkeypress'
                },
                {
                    label: 'onkeydown',
                    value: 'onkeydown'
                },
                {
                    label: 'onkeyup',
                    value: 'onkeyup'
                },
                {
                    label: 'onfocus',
                    value: 'onfocus'
                },
                {
                    label: 'onblur',
                    value: 'onblur'
                },
                {
                    label: 'onchange',
                    value: 'onchange'
                }
            ])

            // select the event we are listening to
            for (var i = 0; i < result.length; i++) {
                var obj = result[i]
                if (obj.value === selected) {
                    obj.selected = true
                    break;
                }
            }
            return result
        },
        /**
         *
         * @returns {{}}
         */
        getVariablesAsObject: function () {
            var variables = this.getVariables()
            var result = {}
            for (var i = 0; i < variables.length; i++) {
                result[variables[i].title] = variables[i].value
            }
            return result
        },
        getVariablesAsOptions: function () {
            var variables = this.getVariables()
            var result = []
            if (variables) {
                for (var i = 0; i < variables.length; i++) {
                    result.push({
                        label: variables[i].label,
                        value: variables[i].variable
                    })
                }
            }
            return result
        },
        getCommandsAsOptions: function (labelField) {
            var items = this.getBlocks({
                declaredClass: 'xcf.model.Command'
            })
            var result = []
            if (items) {
                for (var i = 0; i < items.length; i++) {
                    var item = {}
                    item[labelField || 'label'] = items[i].name
                    item['value'] = items[i].name
                    result.push(item)
                }
            }
            return result
        },
        _cached: null,
        getBlocks: function (query, allowCache) {
            if (!isIDE && allowCache !== false) {
                if (!this._cached) {
                    this._cached = {}
                }
                if (query) {
                    var hash = MD5(JSON.stringify(query), 1)
                    var cached = this._cached[hash]
                    if (cached) {
                        return cached
                    }
                }
            }
            // no store,
            if (!this.blockStore) {
                return []
            }
            query = query || {
                id: /\S+/
            } // all blocks
            var result = _.isEmpty(query) ? this.blockStore.data : this.blockStore.query(query, null, true);
            if (!isIDE && allowCache !== false) {
                var hash = MD5(JSON.stringify(query), 1)
                this._cached[hash] = result
            }
            return result
        },
        /***
         * Register a variable into the scope
         *
         * The variable title is unique within the scope
         *
         * @param variable  =>  xblox.model.Variable
         */
        registerVariable: function (variable) {
            this.variables[variable.title] = variable
            if (this.blockStore) {
                this.blockStore.putSync(variable)
            }
        },
        /***
         * Returns a variable from the scope
         *
         * @param title => variable title
         * @return variable
         */
        getVariable: function (title) {
            var _variables = this.getVariables()
            for (var i = 0; i < _variables.length; i++) {
                var obj = _variables[i]
                if (obj.name === title) {
                    return obj
                }
            }
            return null
        },
        /***
         * Returns a variable from the scope
         *
         * @param title => variable title
         * @return variable
         */
        getVariableById: function (id) {
            if (!id) {
                return null
            }
            var parts = id.split('/')
            var scope = this
            if (parts.length == 2) {
                var owner = scope.owner
                if (owner && owner.hasScope) {
                    if (owner.hasScope(parts[0])) {
                        scope = owner.getScope(parts[0])
                    } else {
                        console.error('have scope id but cant resolve it', this)
                    }
                }
                id = parts[1]
            }
            var _var = scope.blockStore.getSync(id)
            if (_var) {
                return _var
            }
            return null
        },
        /***
         * Register a block into the scope
         *
         * The block name is unique within the scope
         *
         * @param block   =>    xblox.model.Block
         */
        registerBlock: function (block, publish) {
            var store = this.blockStore
            if (store) {
                var added = store.getSync(block.id)
                if (added) {
                    debug &&  0 && console.warn('block already in store! ' + block.id, block)
                    return added
                }
                var result = null
                // custom add block to store function
                if (block.addToStore) {
                    result = block.addToStore(store)
                } else {
                    result = store.putSync(block, publish)
                }
                return result
            }
        },
        /***
         * Return all blocks
         *
         * @returns {xblox.model.Block[]}
         */
        allBlocks: function (query, allowCache) {
            return this.getBlocks({}, allowCache)
        },
        /**
         * Returns whether there is any block belongs to a given group
         * @param group {String}
         * @returns {boolean}
         */
        hasGroup: function (group) {
            var all = this.allGroups({}, false)
            for (var i = 0; i < all.length; i++) {
                var obj = all[i]
                if (obj === group) {
                    return true
                }
            }
            return false
        },
        /**
         * Return all block groups
         * @returns {String[]}
         */
        allGroups: function () {
            var result = []
            var all = this.allBlocks({}, false)
            var _has = function (what) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i] === what) {
                        return true
                    }
                }
                return false
            }
            for (var i = 0; i < all.length; i++) {
                var obj = all[i]
                if (obj.parentId) {
                    continue;
                }
                if (obj.group) {
                    if (!_has(obj.group)) {
                        result.push(obj.group)
                    }
                } else {
                    if (!_has('No Group')) {
                        result.push('No Group')
                    }
                }
            }
            return result
        },
        /**
         * Serializes all variables
         * @returns {Array}
         */
        variablesToJson: function () {
            var result = []
            var data = this.variableStore ? this.getVariables() : this.variables
            for (var e in data) {
                var variable = data[e]
                if (variable.serializeMe === false) {
                    continue;
                }
                if (variable.keys == null) {
                    continue;
                }
                var varOut = {}
                for (var prop in variable) {
                    // copy all serializables over
                    if (
                        this.isString(variable[prop]) ||
                        this.isNumber(variable[prop]) ||
                        this.isBoolean(variable[prop])
                    ) {
                        varOut[prop] = variable[prop]
                    }
                }

                result.push(varOut)
            }
            return result
        },
        isScript: function (val) {
            return this.isString(val) && (
                val.indexOf('return') != -1 ||
                val.indexOf(';') != -1 ||
                val.indexOf('[') != -1 ||
                val.indexOf('{') != -1 ||
                val.indexOf('}') != -1
            )
        },
        /**
         * Serializes all variables
         * @returns {Array}
         */
        variablesToJavascriptEx: function (skipVariable, expression) {
            var result = []
            var data = this.variableStore ? this.getVariables() : this.variables
            for (var i = 0; i < data.length; i++) {
                var _var = data[i]
                if (_var == skipVariable) {
                    continue;
                }
                var _varVal = '' + _var.value

                // optimization
                if (skipVariable && skipVariable.value && skipVariable.value.indexOf(_var.title) == -1) {
                    continue;
                }
                if (expression && expression.indexOf(_var.title) == -1) {
                    continue;
                }

                if (_varVal.length == 0) {
                    continue;
                }
                if (!this.isScript(_varVal) && _varVal.indexOf("'") == -1) {
                    _varVal = "'" + _varVal + "'"
                } else if (this.isScript(_varVal)) {
                    _varVal = this.expressionModel.parseVariable(this, _var)
                }
                if (_varVal === "''") {
                    _varVal = "'0'"
                }
                result.push(_varVal)
            }
            return result
        },
        variablesToJavascript: function (skipVariable, expression) {
            var result = ''
            var data = this.variableStore ? this.getVariables() : this.variables || []
            for (var i = 0; i < data.length; i++) {
                var _var = data[i]
                if (_var == skipVariable) {
                    continue;
                }
                var _varVal = '' + _var.value

                // optimization
                if (skipVariable && skipVariable.value && skipVariable.value.indexOf(_var.title) == -1) {
                    continue;
                }
                if (expression && expression.indexOf(_var.title) == -1) {
                    continue;
                }

                if (_varVal.length == 0) {
                    continue;
                }
                if (!this.isScript(_varVal) && _varVal.indexOf("'") == -1) {
                    _varVal = "'" + _varVal + "'"
                } else if (this.isScript(_varVal)) {
                    // _varVal = "''";
                    _varVal = this.expressionModel.parseVariable(this, _var)
                }

                if (_varVal === "''") {
                    _varVal = "'0'"
                }
                result += 'var ' + _var.title + ' = ' + _varVal + ';'
                result += '\n'
            }

            return result
        },
        /**
         * Convert from JSON data. Creates all Variables in this scope
         * @param data
         * @returns {Array}
         */
        variablesFromJson: function (data) {
            var result = []
            for (var i = 0; i < data.length; i++) {
                var variable = data[i]
                variable['scope'] = this
                if (!variable.declaredClass) {
                     0 && console.log('   variable has no class ')
                    continue;
                }
                var _class = utils.replaceAll('.', '/', variable.declaredClass)
                var variableClassProto = require(_class)
                if (!variableClassProto) {
                    continue;
                }
                result.push(new variableClassProto(variable)) // looks like a leak but the instance is tracked and destroyed in this scope
            }
            return result
        },
        regenerateIDs: function (blocks) {
            var thiz = this
            var updateChildren = function (block) {
                var newId = utils.createUUID()
                var children = thiz.getBlocks({
                    parentId: block.id
                })
                if (children && children.length > 0) {
                    for (var i = 0; i < children.length; i++) {
                        var child = children[i]
                        child.parentId = newId
                        updateChildren(child)
                    }
                }
                block.id = newId
            }
            for (var i = 0; i < blocks.length; i++) {
                var block = blocks[i]
                updateChildren(block)
            }
        },
        /**
         * Clone blocks
         * @param blocks
         * @returns {module:xblox/model/Block[]}
         */
        cloneBlocks2: function (blocks, forceGroup) {
            var blocksJSON = this.blocksToJson(blocks);
            var tmpScope = this.owner.getScope(utils.createUUID(), null, false);
            var newBlocks = tmpScope.blocksFromJson(blocksJSON, false);
            var store = this.blockStore;
            newBlocks = tmpScope.allBlocks();
            tmpScope.regenerateIDs(newBlocks);
            blocksJSON = tmpScope.blocksToJson(newBlocks);
            if (forceGroup) {
                for (var i = 0; i < blocksJSON.length; i++) {
                    var block = blocksJSON[i];
                    if (block.parentId == null) { // groups are only needed for top level blocks
                        block.group = forceGroup;
                    }
                }
            }
            var result = [];
            newBlocks = this.blocksFromJson(blocksJSON); // add it to our scope
            _.each(newBlocks, function (block) {
                result.push(store.getSync(block.id));
            })
            return result
        },
        /**
         * Clone blocks
         * @param blocks
         */
        cloneBlocks: function (blocks) {
            var blocksJSON = this.blocksToJson(blocks)
            var tmpScope = this.owner.getScope(utils.createUUID(), null, false)
            var newBlocks = tmpScope.blocksFromJson(blocksJSON, false)
            newBlocks = tmpScope.allBlocks()
            for (var i = 0; i < newBlocks.length; i++) {
                var block = newBlocks[i]
                block.id = utils.createUUID()
                block.parentId = null
            }

            this.blocksToJson(newBlocks)
            this.blocksFromJson(newBlocks) // add it us
            return newBlocks
        },
        /**
         *
         * @param block
         * @returns {Object}
         */
        blockToJson: function (block) {
            var blockOut = {
                // this property is used to recreate the child blocks in the JSON -> blocks process
                _containsChildrenIds: []
            }
            for (var prop in block) {
                if (prop == 'ctrArgs') {
                    continue;
                }

                if (typeof block[prop] !== 'function' && !block.serializeField(prop)) {
                    continue;
                }

                // copy all strings over
                if (this.isString(block[prop]) ||
                    this.isNumber(block[prop]) ||
                    this.isBoolean(block[prop])) {
                    blockOut[prop] = block[prop]
                }
                // flatten children to ids. Skip "parent" field
                if (prop != 'parent') {
                    if (this.isBlock(block[prop])) {
                        // if the field is a single block container, store the child block's id
                        blockOut[prop] = block[prop].id

                        // register this field name as children ID container
                        blockOut._containsChildrenIds.push(prop)
                    } else if (this.areBlocks(block[prop])) {
                        // if the field is a multiple blocks container, store all the children blocks' id
                        blockOut[prop] = []

                        for (var i = 0; i < block[prop].length; i++) {
                            blockOut[prop].push(block[prop][i].id)
                        }

                        // register this field name as children IDs container
                        blockOut._containsChildrenIds.push(prop)
                    }
                }
            }

            return blockOut
        },
        /**
         * Serializes all blocks to JSON data.
         * It needs a custom conversation because we're having cyclic
         * object dependencies.
         * @returns {Array}
         */
        blocksToJson: function (data) {
            try {
                var result = []
                data = (data && data.length) ? data : (this.blockStore ? this.blockStore.data : this.blocks)
                for (var b in data) {
                    var block = data[b]
                    if (block.keys == null) {
                        continue;
                    }
                    if (block.serializeMe === false) {
                        continue;
                    }
                    var blockOut = {
                        // this property is used to recreate the child blocks in the JSON -> blocks process
                        _containsChildrenIds: []
                    }

                    for (var prop in block) {
                        if (prop == 'ctrArgs') {
                            continue;
                        }

                        if (typeof block[prop] !== 'function' && !block.serializeField(prop)) {
                            continue;
                        }

                        // copy all strings over
                        if (this.isString(block[prop]) ||
                            this.isNumber(block[prop]) ||
                            this.isBoolean(block[prop])) {
                            blockOut[prop] = block[prop]
                        }

                        if (_.isObject(block[prop]) && block.serializeObject) {
                            if (block.serializeObject(prop) === true) {
                                blockOut[prop] = JSON.stringify(block[prop], null, 2)
                            }
                        }

                        // flatten children to ids. Skip "parent" field

                        if (prop != 'parent') {
                            if (this.isBlock(block[prop])) {
                                // if the field is a single block container, store the child block's id
                                blockOut[prop] = block[prop].id

                                // register this field name as children ID container
                                blockOut._containsChildrenIds.push(prop)
                            } else if (this.areBlocks(block[prop])) {
                                // if the field is a multiple blocks container, store all the children blocks' id
                                blockOut[prop] = []

                                for (var i = 0; i < block[prop].length; i++) {
                                    blockOut[prop].push(block[prop][i].id)
                                }

                                // register this field name as children IDs container
                                blockOut._containsChildrenIds.push(prop)
                            }
                        }
                    }
                    result.push(blockOut)
                }
            } catch (e) {
                console.error('from json failed : ' + e)
            }
            return result
        },
        _createBlockStore: function () {},
        blockFromJson: function (block) {
            block['scope'] = this
            if (block._containsChildrenIds == null) {
                block._containsChildrenIds = []
            }

            // Store all children references into "children"
            var children = {}
            for (var cf = 0; cf < block._containsChildrenIds.length; cf++) {
                var propName = block._containsChildrenIds[cf]
                children[propName] = block[propName]
                block[propName] = null
            }
            delete block._containsChildrenIds

            // Create the block
            if (!block.declaredClass) {
                 0 && console.log('   not a class ')
                return null
            }
            var blockClassProto = null
            var _class = null
            try {
                _class = utils.replaceAll('.', '/', block.declaredClass)
                blockClassProto = require(_class)
            } catch (e) {
                try {
                    _class = utils.replaceAll('/', '.', block.declaredClass)
                    blockClassProto = require(_class)
                } catch (e) {
                    debug && console.error('couldnt resolve class ' + _class)
                }
                debug && console.error('couldnt resolve class ' + _class)
            }
            if (!blockClassProto) {
                blockClassProto = dcl.getObject(block.declaredClass)
            }
            if (!blockClassProto) {
                debug &&  0 && console.log('couldn`t resolve ' + _class)
                return null
            }

            var blockOut = null
            try {
                blockOut = factory.createBlock(blockClassProto, block)
            } catch (e) {
                debug && console.error('error in block creation ', e)
                logError(e)
                return null
            }

            // assign the children references into block._children
            blockOut._children = children

            return blockOut
        },
        /**
         * Convert from JSON data. Creates all blocks in this scope
         * @param data
         * @returns {Array}
         */
        blocksFromJson: function (data, check, errorCB) {
            var resultSelected = []
            var childMap = {}
            for (var i = 0; i < data.length; i++) {
                var block = data[i]
                block['scope'] = this

                if (block._containsChildrenIds == null) {
                    block._containsChildrenIds = []
                }

                // Store all children references into "children"
                var children = {}
                for (var cf = 0; cf < block._containsChildrenIds.length; cf++) {
                    var propName = block._containsChildrenIds[cf]
                    children[propName] = block[propName]
                    block[propName] = null
                }
                delete block._containsChildrenIds

                // Create the block
                if (!block.declaredClass) {
                     0 && console.log('   not a class ')
                    continue;
                }
                var blockClassProto = null
                var _class = null
                try {
                    _class = utils.replaceAll('.', '/', block.declaredClass)
                    blockClassProto = require(_class)
                } catch (e) {
                    console.error('couldnt resolve class ' + _class)
                }
                if (!blockClassProto) {
                    blockClassProto = dcl.getObject(block.declaredClass)
                }
                if (!blockClassProto) {
                     0 && console.log('couldnt resolve ' + _class)
                    continue;
                }

                var blockOut = null
                try {
                    blockOut = factory.createBlock(blockClassProto, block, null, false)
                } catch (e) {
                    console.error('error in block creation ', e + ' ' + block.declaredClass)
                    logError(e)
                    continue;
                }

                // assign the children references into block._children
                blockOut._children = children
                childMap[blockOut.id] = children
                resultSelected.push(blockOut)
            }

            // 2nd pass, update child blocks
            var allBlocks = this.allBlocks(null, false)
            for (var i = 0; i < allBlocks.length; i++) {
                var block = allBlocks[i]
                block._children = childMap[block.id]
                if (block._children) {
                    // get all the block container fields
                    for (var propName in block._children) {
                        if (typeof block._children[propName] == 'string') {
                            // single block
                            var child = this.getBlockById(block._children[propName])
                            if (!child) {
                                this.blockStore.removeSync(block._children[propName])
                                if (errorCB) {
                                    errorCB('   couldnt resolve child: ' + block._children[propName] + '@' + block.name + ':' + block.declaredClass)
                                }
                                 0 && console.log('   couldnt resolve child: ' + block._children[propName] + '@' + block.name + ':' + block.declaredClass)
                                continue;
                            }
                            block[propName] = child
                            child.parent = block
                            if (child.postCreate) {
                                child.postCreate()
                            }
                        } else if (typeof block._children[propName] == 'object') {
                            // multiple blocks
                            block[propName] = []
                            for (var j = 0; j < block._children[propName].length; j++) {
                                var child = this.getBlockById(block._children[propName][j])
                                if (!child) {
                                    if (errorCB) {
                                        errorCB('   couldnt resolve child: ' + block._children[propName] + '@' + block.name + ':' + block.declaredClass)
                                    }
                                     0 && console.log('   couldnt resolve child: ' + block._children[propName][j] + '@' + block.name + ':' + block.declaredClass)
                                    continue;
                                }
                                block[propName].push(child)
                                var _parent = this.getBlockById(child.parentId)
                                if (_parent) {
                                    child.parent = _parent
                                } else {
                                    console.error('child has no parent')
                                }
                            }
                        }
                    }
                    delete block._children
                }

                if (check !== false && block.parentId != null) {
                    var parent = this.getBlockById(block.parentId)
                    if (parent == null) {
                        debug && console.error('have orphan block!', block)
                        block.parentId = null
                    }
                }
                block.postCreate()
            }
            var result = this.allBlocks()
            return resultSelected
        },
        /**
         *
         * @param url {String}
         * @returns {module:xblox/model/Block[]}
         */
        resolveDevice: function (url) {
            var blockScope = this,
                ctx = this.ctx,
                driver = this.driver,
                device = this.device,
                deviceManager = ctx.getDeviceManager(),
                driverManager = ctx.getDriverManager()

            if (url.indexOf('://') == -1) {
                var _block = this.getBlockById(url)
                if (_block) {
                    return _block
                }
                return url
            }
            var parts = utils.parse_url(url) // strip scheme

            parts = utils.urlArgs(parts.host) // go on with query string
            var _device = deviceManager.getItemById(parts.device.value)
            // support device by name
            if (!_device) {
                var _instance = deviceManager.getInstanceByName(parts.device.value)
                if (_instance) {
                    _device = _instance.device
                }
            }
            return device || _device;
        },
        /**
         *
         * @param url {String}
         * @returns {module:xblox/model/Block[]}
         */
        resolveBlock: function (url) {
            var blockScope = this,
                ctx = this.ctx,
                driver = this.driver,
                device = this.device,
                deviceManager = ctx.getDeviceManager(),
                driverManager = ctx.getDriverManager()

            if (url.indexOf('://') == -1) {
                var _block = this.getBlockById(url)
                if (_block) {
                    return _block
                }
                return url
            }
            var parts = utils.parse_url(url) // strip scheme

            parts = utils.urlArgs(parts.host) // go on with query string
            var _device = deviceManager.getItemById(parts.device.value)
            // support device by name
            if (!_device) {
                var _instance = deviceManager.getInstanceByName(parts.device.value)
                if (_instance) {
                    _device = _instance.device
                }
            }
            if (_device) {
                var info = deviceManager.toDeviceControlInfo(_device);
                if (!info) {
                     0 && console.warn('cant get device info for ' + _device.title, device);
                    return;
                }

                driver = driverManager.getDriverById(info.driverId)
                var driverInstance = _device.driverInstance
                if (driverInstance || driver) {
                    blockScope = driverInstance ? driverInstance.blockScope : driver.blockScope
                    var block = blockScope ? blockScope.getStore().getSync(parts.block.value) : null
                    if (block) {
                        return block
                    }
                }
            }
        },
        getBlock: function (id) {
            return this.getBlockById(id)
        },
        /***
         * Returns a block from the scope
         * @param name {String}
         * @return block {module:xblox/model/Block[]}
         */
        getBlockByName: function (name) {
            if (name.indexOf('://') !== -1) {
                var block = this.resolveBlock(name)
                if (block) {
                    return block
                }
            }
            var allBlocks = this.getBlocks()
            for (var i = 0; i < allBlocks.length; i++) {
                var block = allBlocks[i]
                if (block.name === name) {
                    return block
                }
            }
            var blocks = this.blockStore.query({
                name: name
            })
            return blocks && blocks.length > 0 ? blocks[0] : null
        },
        /***
         * Returns a block from the scope
         *
         * @param name  =>  block name
         * @return block
         */
        getBlockById: function (id) {
            return this.blockStore.getSync(id);
            /* || this.variableStore.getSync(id) */
        },
        /**
         * Returns an array of blocks
         * @param blocks {module:xblox/model/Block[]
         * @returns {module:xblox/model/Block[]}
         */
        _flatten: function (blocks) {
            var result = []
            for (var b in blocks) {
                var block = blocks[b]
                if (block.keys == null) {
                    continue;
                }
                result.push(block)
                for (var prop in block) {
                    if (prop == 'ctrArgs') {
                        continue;
                    }
                    // flatten children to ids. Skip "parent" field
                    if (prop !== 'parent') {
                        if (this.isBlock(block[prop])) {
                            // if the field is a single block container, store the child block's id
                            result.push(block[prop])
                        } else if (this.areBlocks(block[prop])) {
                            for (var i = 0; i < block[prop].length; i++) {
                                result.push(block[prop][i])
                            }
                        }
                    }
                }
            }
            return result
        },
        /**
         *
         * @param blocks {module:xblox/model/Block[]}
         * @returns {module:xblox/model/Block[]}
         */
        flatten: function (blocks) {
            var result = []
            for (var b in blocks) {
                var block = blocks[b]

                if (block.keys == null) {
                    continue;
                }
                var found = _.find(result, {
                    id: block.id
                })

                if (found) {
                    // console.error('already in array  : ' +found.name);
                } else {
                    result.push(block)
                }

                for (var prop in block) {
                    if (prop == 'ctrArgs') {
                        continue;
                    }
                    // flatten children to ids. Skip "parent" field
                    if (prop !== 'parent') {
                        var value = block[prop]
                        if (this.isBlock(value)) {
                            // if the field is a single block container, store the child block's id
                            found = _.find(result, {
                                id: value.id
                            })
                            if (found) {

                            } else {
                                result.push(value)
                            }
                        } else if (this.areBlocks(value)) {
                            for (var i = 0; i < value.length; i++) {
                                var sBlock = value[i]
                                found = _.find(result, {
                                    id: sBlock.id
                                })
                                if (found) {} else {
                                    result.push(sBlock)
                                }
                                result = result.concat(this.flatten([sBlock]))
                            }
                        }
                    }
                }
            }
            result = _.uniq(result, false, function (item) {
                return item.id
            })
            return result
        },
        _getSolve: function (block) {
            return block.prototype ? block.prototype.solve : block.__proto__.solve
        },
        solveBlock: function (mixed, settings, force, isInterface) {
            settings = settings || {
                highlight: false
            }
            var block = null
            if (this.isString(mixed)) {
                block = this.getBlockByName(mixed)
                if (!block) {
                    block = this.getBlockById(mixed)
                }
            } else if (this.isObject(mixed)) {
                block = mixed
            }
            var result = null
            if (block) {
                if (settings.force !== true && block.enabled == false) {
                    return null
                }
                if (settings.force === true) {
                    settings.force = false
                }
                var _class = block.declaredClass
                var _module = lang.getObject(utils.replaceAll('/', '.', _class)) || lang.getObject(_class)
                if (_module) {
                    if (_module.prototype && _module.prototype.solve) {
                        result = _module.prototype.solve.apply(block, [this, settings])
                    }
                } else {
                    result = block.solve(block.getScope(), settings, force, isInterface)
                    delete block.override
                    block.override = {}
                }
            } else {
                debug && console.error('solving block failed, have no block! ', mixed)
            }
            return result
        },
        /***
         * Solves all the commands into [items]
         *
         * @param manager   =>  BlockManager
         * @return  list of commands to send
         */
        solve: function (scope, settings) {
            var ret = ''
            for (var n = 0; n < this.items.length; n++) {
                ret += this.items[n].solve(scope, settings)
            }
            return ret
        },
        /***
         * Parses an expression
         *
         * @param expression
         * @returns {String} parsed expression
         */
        /**
         *
         * @param expression
         * @param addVariables
         * @param variableOverrides
         * @param runCallback
         * @param errorCallback
         * @param context
         * @param args
         * @returns {*}
         */
        parseExpression: function (expression, addVariables, variableOverrides, runCallback, errorCallback, context, args, flags) {
            return this.getExpressionModel().parse(this, expression, addVariables, runCallback, errorCallback, context, variableOverrides, args, flags)
        },
        isString: function (a) {
            return typeof a == 'string'
        },
        isNumber: function (a) {
            return typeof a == 'number'
        },
        isBoolean: function (a) {
            return typeof a == 'boolean'
        },
        isObject: function (a) {
            return typeof a === 'object'
        },
        isBlock: function (a) {
            var ret = false

            if ((typeof a == 'object') && (a != null) && (a.length == undefined)) {
                if (a.serializeMe) {
                    ret = true
                }
            }
            return ret
        },
        areBlocks: function (a) {
            var ret = false

            if ((typeof a == 'object') && (a != null) && (a.length > 0)) {
                if (this.isBlock(a[0])) {
                    ret = true
                }
            }
            return ret
        },
        /**
         *
         * @private
         */
        _onVariableChanged: function (evt) {
            if (evt.item && this.getExpressionModel().variableFuncCache[evt.item.title]) {
                delete this.expressionModel.variableFuncCache[evt.item.title]
            }
        },

        init: function () {
            this.getExpressionModel() // create
            this.subscribe(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, this._onVariableChanged)
            var thiz = this

            this.subscribe(types.EVENTS.ON_MODULE_RELOADED, function (evt) {
                var mid = evt.module,
                    newModule = evt.newModule,
                    blocks = thiz.getBlocks(),
                    instances = blocks.filter(function (block) {
                        if (block.declaredClass == mid || block.declaredClass == utils.replaceAll('/', '.', mid)) {
                            return block
                        }
                        return null
                    })

                instances && _.each(instances, function (block) {
                    mergeNewModule(block, newModule.prototype)
                })
            })
        },
        /**
         *
         */
        _destroy: function () {
            var allblocks = this.allBlocks()
            for (var i = 0; i < allblocks.length; i++) {
                var obj = allblocks[i]
                if (!obj) {
                    continue;
                }
                try {
                    if (obj && obj.stop) {
                        obj.stop(true)
                    }

                    if (obj && obj.reset) {
                        obj.reset()
                    }
                    if (obj && obj._destroy) {
                        obj._destroy()
                    }
                    if (obj && obj.destroy) {
                        obj.destroy()
                    }

                    if (obj._emit) {
                        obj._emit(types.EVENTS.ON_ITEM_REMOVED, {
                            item: obj
                        })
                    }
                } catch (e) {
                    debug && console.error('Scope::_destroy: error destroying block ' + e.message, obj ? (obj.id + ' ' + obj.name) : 'empty')
                    debug &&  0 && console.trace()
                }
            }
        },
        destroy: function () {
            this._destroy()
            this.reset()
            this._destroyed = true
            delete this.expressionModel
        },
        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add
         * @returns {boolean}
         */
        moveTo: function (source, target, before, add) {
             0 && console.log('move to : ', arguments);
            /**
             * treat first the special cases of adding an item
             */
            if (add) {
                // remove it from the source parent and re-parent the source
                if (target.canAdd && target.canAdd()) {
                    var sourceParent = this.getBlockById(source.parentId)
                    if (sourceParent) {
                        sourceParent.removeBlock(source, false)
                    }
                    return target.add(source, null, null);
                } else {
                    console.error('cant reparent')
                    return false
                }
            }

            // for root level move
            if (!target.parentId && add === false) {
                // if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if (sourceParent && sourceParent.removeBlock) {
                    sourceParent.removeBlock(source, false);
                    source.parentId = null;
                    source.group = target.group
                }

                var itemsToBeMoved = [];
                var groupItems = this.getBlocks({
                    group: target.group
                });

                var rootLevelIndex = [];
                var store = this.getBlockStore();

                var sourceIndex = store.storage.index[source.id];
                var targetIndex = store.storage.index[target.id];
                for (var i = 0; i < groupItems.length; i++) {
                    var item = groupItems[i];
                    // keep all root-level items

                    if (groupItems[i].parentId == null && // must be root
                        groupItems[i] != source // cant be source
                    ) {
                        var itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if (add) {
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id])
                        }
                    }
                }

                // remove them the store
                for (var j = 0; j < itemsToBeMoved.length; j++) {
                    store.remove(itemsToBeMoved[j].id)
                }

                // remove source
                this.getBlockStore().remove(source.id);

                // if before, put source first
                if (before) {
                    this.getBlockStore().putSync(source)
                }

                // now place all back
                for (var j = 0; j < itemsToBeMoved.length; j++) {
                    store.put(itemsToBeMoved[j])
                }

                // if after, place source back
                if (!before) {
                    this.getBlockStore().putSync(source)
                }
                return true;
                // we move from root to lower item
            } else if (!source.parentId && target.parentId && add == false) {
                source.group = target.group;

                // we move from root to into root item
            } else if (!source.parentId && !target.parentId && add) {
                if (target.canAdd && target.canAdd()) {
                    source.group = null;
                    target.add(source, null, null)
                }
                return true;

                // we move within the same parent
            } else if (source.parentId && target.parentId && add == false && source.parentId === target.parentId) {
                var parent = this.getBlockById(source.parentId);
                if (!parent) {
                    return false
                }
                var items = parent[parent._getContainer(source)];
                var cIndexSource = source.indexOf(items, source);
                var cIndexTarget = source.indexOf(items, target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - (cIndexTarget + (before == true ? -1 : 1)))
                for (var i = 0; i < distance - 1; i++) {
                    source.move(direction);
                }
                return true;
                // we move within the different parents
            } else if (source.parentId && target.parentId && add == false && source.parentId !== target.parentId) {
                var sourceParent = this.getBlockById(source.parentId);
                if (!sourceParent) {
                    return false
                }

                var targetParent = this.getBlockById(target.parentId);
                if (!targetParent) {
                    return false
                }

                // remove it from the source parent and re-parent the source
                if (sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()) {
                    sourceParent.removeBlock(source, false);
                    targetParent.add(source, null, null)
                } else {
                    return false
                }

                // now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if (items == null) {
                    console.error('weird : target parent has no item container')
                }
                var cIndexSource = targetParent.indexOf(items, source);
                var cIndexTarget = targetParent.indexOf(items, target);
                if (!cIndexSource || !cIndexTarget) {
                    console.error(' weird : invalid drop processing state, have no valid item indicies')
                    return
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - (cIndexTarget + (before == true ? -1 : 1)))
                for (var i = 0; i < distance - 1; i++) {
                    targetParent.move(source, direction)
                }
                return true
            }

            return false
        }

    })
    dcl.chainAfter(Module, 'destroy')
    return Module
})
},
'xblox/model/Expression':function(){
/** @module xblox/model/Expression */
define([
    "xdojo/declare",
    "xdojo/has",
    "xide/utils",
    "xide/types",
    "xblox/model/ModelBase"
], function (declare, has, utils, types, ModelBase, tracer, _console) {
    'use strict';
    var isServer =  0 ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if (isServer && tracer && console && console.error) {
        console = _console;
    }
    var _debug = false;
    /**
     * The expression
     * @class module:xblox.model.Expression
     * @extends module:xblox/model/ModelBase
     */
    return declare("xblox.model.Expression", [ModelBase], {
        id: null,
        context: null,
        // Constants
        variableDelimiters: {
            begin: "[",
            end: "]"
        },
        blockCallDelimiters: {
            begin: "{",
            end: "}"
        },
        expressionCache: null,
        variableFuncCache: null,
        constructor: function () {
            this.reset();
        },
        reset: function () {
            this.expressionCache = {};
            this.variableFuncCache = {};
        },
        /**
         * Replace variable calls width variable values
         * @param scope
         * @param expression
         * @param _evaluate
         * @param _escape
         * @param variableOverrides
         * @returns {*}
         */
        replaceVariables: function (scope, expression, _evaluate, _escape, variableOverrides, useVariableGetter, variableDelimiters, flags) {
            var FLAG = types.CIFLAG;
            variableDelimiters = variableDelimiters || this.variableDelimiters;
            flags = flags || FLAG.NONE;
            if (flags & FLAG.DONT_ESCAPE) {
                _escape = false;
            }
            if (flags & FLAG.DONT_PARSE) {
                _evaluate = false;
            }
            var occurrence = this.findOccurrences(expression, variableDelimiters);
            if (occurrence) {
                for (var n = 0; n < occurrence.length; n++) {
                    // Replace each variable call width the variable value
                    var oc = occurrence[n];
                    oc = oc.replace(variableDelimiters.begin, '');
                    oc = oc.replace(variableDelimiters.end, '');
                    var _var = this._getVar(scope, oc);
                    if (_var && _var.flags & FLAG.DONT_PARSE) {
                        _evaluate = false;
                    }
                    var value = null;
                    if (_var) {
                        if (useVariableGetter) {
                            expression = expression.replace(occurrence[n], 'this.getVariable(\'' + _var.name + '\')');
                            continue;
                        }
                        value = this.getValue(_var.value);
                        if (variableOverrides && _var.name in variableOverrides) {
                            value = variableOverrides[_var.name];
                        }
                        if (this.isScript(value) && _evaluate !== false) {
                            try {
                                //put other variables on the stack: should be avoided
                                var _otherVariables = scope.variablesToJavascript(_var, true);
                                if (_otherVariables) {
                                    value = _otherVariables + value;
                                }
                                var _parsed = (new Function("{\n" + value + "\n}")).call(scope.context || {});
                                //wasnt a script
                                if (_parsed === 'undefined' || typeof _parsed === 'undefined') {
                                    value = '' + _var.value;
                                } else {
                                    value = _parsed;
                                    !(flags & FLAG.DONT_ESCAPE) && (value = "'" + value + "'");
                                }
                            } catch (e) {
                                 0 && console.log(' parsed variable expression failed \n' + value, e);
                            }
                        } else {
                            if (!this.isNumber(value)) {
                                if (_escape !== false) {
                                    value = "'" + value + "'";
                                }
                            }
                        }
                    } else {
                        _debug &&  0 && console.log('   expression failed, no such variable :' + occurrence[n] + ' ! setting to default ' + '');
                        value = occurrence[n];
                    }
                    expression = expression.replace(occurrence[n], value);
                }
            }
            return expression;
        },
        /**
         *
         * @param scope
         * @param expression
         * @param addVariables
         * @param runCallback
         * @param errorCallback
         * @param context
         * @param variableOverrides
         * @param args {[*]}
         * @param flags {CIFLAGS}
         * @returns {*}
         */
        parse: function (scope, expression, addVariables, runCallback, errorCallback, context, variableOverrides, args, flags) {
            expression = this.replaceAll("''", "'", expression);
            var expressionContext = context || scope.context || scope.getContext() || {};
            var useVariableGetter = expressionContext['getVariable'] != null;
            expression = this.replaceVariables(scope, expression, null, null, variableOverrides, useVariableGetter, null, flags);
            var isExpression = this.isScript(expression);
            if (!isExpression && (this.isString(expression) || this.isNumber(expression))) {
                if (runCallback) {
                    runCallback('Expression ' + expression + ' evaluates to ' + expression);
                }
                return expression;
            }
            if (expression.indexOf('return') == -1 && isExpression) {
                expression = 'return ' + expression;
            }
            addVariables = false;
            if (addVariables === true) {
                var _otherVariables = scope.variablesToJavascript(null, expression);
                if (_otherVariables) {
                    expression = _otherVariables + expression;
                    expression = this.replaceAll("''", "'", expression);//weird!
                }
            }
            var parsed = this;
            try {
                expression = this.replaceAll("''", "'", expression);
                var _function = this.expressionCache[expression];
                if (!_function) {
                    _debug &&  0 && console.log('create function ' + expression);
                    _function = new Function("{" + expression + "; }");
                    this.expressionCache[expression] = _function;
                } else {

                }
                parsed = _function.apply(expressionContext, args);
            } catch (e) {
                console.error('invalid expression : \n' + expression, e);
                if (errorCallback) {
                    errorCallback('invalid expression : \n' + expression + ': ' + e, e);
                }
                parsed = '' + expression;
                return parsed;
            }
            if (parsed === true) {
                _debug &&  0 && console.log('expression return true! : ' + expression);
            }

            if (runCallback) {
                runCallback('Expression ' + expression + ' evaluates to ' + parsed);
            }
            return parsed;
        },
        parseVariable: function (scope, _var, _prefix, escape, allowCache, context, args) {
            var value = '' + _var.value;
            _prefix = _prefix || '';
            if (allowCache !== false) {
                var _function = this.variableFuncCache[scope.id + '|' + _var.title];
                if (!_function) {
                    _function = new Function("{" + _prefix + value + "}");
                    this.variableFuncCache[scope.id + '|' + _var.title] = _function;
                }
            } else {
                _function = new Function("{" + _prefix + value + "}");
            }
            var _parsed = _function.apply(context || scope.context || {}, args || []);
            if (_parsed === 'undefined' || typeof _parsed === 'undefined') {
                value = '' + _var.value;
            } else {
                if (!this.isNumber(_parsed) && escape !== false) {
                    value = '' + _parsed;
                    value = "'" + value + "'";
                } else {
                    value = _parsed;
                }
            }
            return value;
        },
        // Replace block call with block result
        replaceBlockCalls: function (scope, expression) {
            var occurrences = this.findOccurrences(expression, this.blockCallDelimiters);
            if (occurrences) {
                for (var n = 0; n < occurrences.length; n++) {
                    // Replace each block call with block result
                    var blockName = this._removeDelimiters(occurrences[n], this.blockCallDelimiters);
                    var blockResult = scope.solveBlock(blockName).join("\n");
                    expression = expression.replace(occurrences[n], blockResult);
                }
            }
            return expression;
        },
        // gets a variable from the scope using text [variableName]
        _getVar: function (scope, string) {
            return scope.getVariable(this._getVarName(string));
        },
        _getVarName: function (string) {
            return this._removeDelimiters(string, this.variableDelimiters);
        },
        _removeDelimiters: function (text, delimiters) {
            return text.replace(delimiters.begin, '').replace(delimiters.end, '');
        },
        // escape regular expressions special chars
        _escapeRegExp: function (string) {
            var special = ["[", "]", "(", ")", "{", "}", "*", "+", "."];
            for (var n = 0; n < special.length; n++) {
                string = string.replace(special[n], "\\" + special[n]);
            }
            return string;
        },
        /**
         * Finds a term in an expression by start and end delimiters
         * @param expression
         * @param delimiters
         * @private
         */
        findOccurrences: function (expression, delimiters) {
            var d = {
                begin: this._escapeRegExp(delimiters.begin),
                end: this._escapeRegExp(delimiters.end)
            };
            return expression.match(new RegExp(d.begin + "(" + "[^" + d.end + "]*" + ")" + d.end, 'g'));
        }
    });
});
},
'xblox/model/BlockModel':function(){
define([
    'dcl/dcl',
    'xdojo/declare',
    'xide/data/Model',
    'xide/data/Source'
], function(dcl,declare,Model,Source){
    /**
     * Contains provides implements functions to deal with sub blocks.
     */
    return declare('xblox.model.BlockModel',[Model,Source],{
        declaredClass:'xblox.model.BlockModel',
        icon:'fa-play',
        /**
         * Store function override
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren: function (parent) {
            return this.items != null && this.items.length > 0;
        },
        /**
         * Store function override
         * @param parent
         * @returns {Array}
         */
        getChildren: function (parent) {
            return this.items;
        },
        getBlockIcon:function(){
            return '<span class="' +this.icon + '"></span>';
        }
    });
});
},
'xblox/data/Store':function(){
/** @module xblox/data/Store **/
define([
    "dojo/_base/declare",
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'dojo/Deferred'
], function (declare, TreeMemory, ObservableStore, Trackable, Deferred) {
    return declare("xblox.data.Store", [TreeMemory, Trackable, ObservableStore], {
        idProperty: 'id',
        parentField: 'parentId',
        parentProperty: 'parentId',
        filter: function (data) {
            var _res = this.inherited(arguments);
            delete this._state.filter;
            this._state.filter = data;
            return _res;
        },
        getRootItem:function(){
            return {
                canAdd:function(){
                    return true
                },
                id:this.id +'_root',
                group:null,
                name:'root',
                isRoot:true,
                parentId:null
            }
        },
        getChildren: function (object) {
            return this.root.filter({parentId: this.getIdentity(object)});
        },
        _fetchRange: function (kwArgs) {
            var deferred = new Deferred();
            var _res = this.fetchRangeSync(kwArgs);
            var _items;
            if (this._state.filter) {
                //the parent query
                if (this._state.filter['parentId']) {
                    var _item = this.getSync(this._state.filter.parentId);
                    if (_item) {
                        this.reset();
                        _items = _item.items;
                        if (_item.getChildren) {
                            _items = _item.getChildren();
                        }
                        deferred.resolve(_items);
                        _res = _items;
                    }
                }

                //the group query
                if (this._state && this._state.filter && this._state.filter['group']) {
                    _items = this.getSync(this._state.filter.parent);
                    if (_item) {
                        this.reset();
                        _res = _item.items;
                    }
                }
            }
            deferred.resolve(_res);
            return deferred;
        },
        mayHaveChildren: function (parent) {
            if (parent.mayHaveChildren) {
                return parent.mayHaveChildren(parent);
            }
            return parent.items != null && parent.items.length > 0;
        }
    });
});
},
'xblox/factory/Blocks':function(){
define([
    'xide/factory',
    'xide/utils',
    'xide/types',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin',
    "xblox/model/logic/CaseBlock",
    "xblox/model/Block",
    "xblox/model/functions/CallBlock",
    "xblox/model/functions/StopBlock",
    "xblox/model/functions/PauseBlock",
    "xblox/model/functions/SetProperties",
    "xblox/model/code/CallMethod",
    "xblox/model/code/RunScript",
    "xblox/model/loops/ForBlock",
    "xblox/model/loops/WhileBlock",
    "xblox/model/variables/VariableAssignmentBlock",
    "xblox/model/logic/IfBlock",
    "xblox/model/logic/ElseIfBlock",
    "xblox/model/logic/SwitchBlock",
    "xblox/model/variables/VariableSwitch",
    "xblox/model/logging/Log",
    "xblox/model/server/RunServerMethod",
    "xblox/model/server/Shell",
    "xblox/model/code/RunBlock",
    "xblox/model/events/OnEvent",
    "xblox/model/events/OnKey",
    "xblox/model/mqtt/Subscribe",
    "xblox/model/mqtt/Publish",
    "xblox/model/File/ReadJSON",
    "xcf/factory/Blocks"
], function (factory,
             utils,
             types,
             ReloadMixin, EventedMixin,
             CaseBlock,
             Block,
             CallBlock,
             StopBlock,
             PauseBlock,
             SetProperties,
             CallMethod,
             RunScript,
             ForBlock,
             WhileBlock,
             VariableAssignmentBlock,
             IfBlock,
             ElseIfBlock,
             SwitchBlock,
             VariableSwitch,
             Log,
             RunServerMethod,
             Shell,
             RunBlock,
             OnEvent,
             OnKey,
             Subscribe,
             Publish,
             ReadJSON) {

    var cachedAll = null;
    /***
     *
     * @param mixed String|Prototype
     * @param ctorArgs
     * @param baseClasses
     * @param publish
     */
    factory.createBlock = function (mixed, ctorArgs, baseClasses, publish) {
        //complete missing arguments:
        Block.prototype.prepareArgs(ctorArgs);
        var block = factory.createInstance(mixed, ctorArgs, baseClasses);
        block.ctrArgs = null;
        var newBlock;
        try {
            if (block && block.init) {
                block.init();
            }
            //add to scope
            if (block.scope) {
                newBlock = block.scope.registerBlock(block, publish);
            }
            if (block.initReload) {
                block.initReload();
            }
        } catch (e) {
            logError(e, 'create block');
        }
        return newBlock || block;
    };
    factory.clearVariables = function () {
    };
    factory.getAllBlocks = function (scope, owner, target, group, allowCache) {
        if (allowCache !== false && cachedAll != null) {
            return cachedAll;
        } else if (allowCache == false) {
            cachedAll = null;
        }
        var items = factory._getFlowBlocks(scope, owner, target, group);
        items = items.concat(factory._getLoopBlocks(scope, owner, target, group));
        items = items.concat(factory._getCommandBlocks(scope, owner, target, group));
        items = items.concat(factory._getCodeBlocks(scope, owner, target, group));
        items = items.concat(factory._getEventBlocks(scope, owner, target, group));
        items = items.concat(factory._getLoggingBlocks(scope, owner, target, group));
        items = items.concat(factory._getServerBlocks(scope, owner, target, group));
        items = items.concat(factory._getMQTTBlocks(scope, owner, target, group));
        items = items.concat(factory._getFileBlocks(scope, owner, target, group));
        cachedAll = items;
        return items;
    };
    factory._getMQTTBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'MQTT',
            iconClass: 'fa-cloud',
            items: [
                {
                    name: 'Subscribe',
                    owner: owner,
                    iconClass: 'fa-bell',
                    proto: Subscribe,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Publish',
                    owner: owner,
                    iconClass: 'fa-send',
                    proto: Publish,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'MQTT'
        });
        return items;

    };

    factory._getFileBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'File',
            iconClass: 'fa-file',
            items: [
                {
                    name: '%%Read JSON',
                    owner: owner,
                    iconClass: 'fa-file',
                    proto: ReadJSON,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'File'
        });
        return items;

    };

    factory._getServerBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Server',
            iconClass: 'el-icon-repeat',
            items: [
                {
                    name: 'Run Server Method',
                    owner: owner,
                    iconClass: 'fa-plug',
                    proto: RunServerMethod,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Shell',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: Shell,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Server'
        });
        return items;
    };
    factory._getVariableBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Flow',
            iconClass: 'el-icon-random',
            items: [
                {
                    name: 'If...Else',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: IfBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        condition: "[value]=='PW'"
                    }
                }/*,
                 {
                 name:'Switch',
                 owner:owner,
                 iconClass:'el-icon-fork',
                 proto:SwitchBlock,
                 target:target,
                 ctrArgs:{
                 scope:scope,
                 group:group
                 }
                 }
                 */
            ]
        });

        return items;
    };
    factory._getEventBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Events',
            iconClass: 'fa-bell',
            items: [
                {
                    name: 'On Event',
                    owner: owner,
                    iconClass: 'fa-bell',
                    proto: OnEvent,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'On Key',
                    owner: owner,
                    iconClass: 'fa-keyboard-o',
                    proto: OnKey,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Events'
        });

        return items;
    };
    factory._getLoggingBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Logging',
            iconClass: 'fa-bug',
            items: [
                {
                    name: 'Log',
                    owner: owner,
                    iconClass: 'fa-bug',
                    proto: Log,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Logging'
        });

        return items;
    };
    factory._getCodeBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Code',
            iconClass: 'fa-code',
            items: [
                {
                    name: 'Call Method',
                    owner: owner,
                    iconClass: 'el-icon-video',
                    proto: CallMethod,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Run Script',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: RunScript,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Run Block',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: RunBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Set Properties',
                    owner: owner,
                    iconClass: 'fa-code',
                    proto: SetProperties,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Code'
        });
        return items;
    };
    factory._getFlowBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Flow',
            iconClass: 'el-icon-random',
            items: [
                {
                    name: 'If...Else',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: IfBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        condition: "[value]=='PW'"
                    }
                },
                {
                    name: 'Switch',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: SwitchBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                },
                {
                    name: 'Variable Switch',
                    owner: owner,
                    iconClass: 'el-icon-fork',
                    proto: VariableSwitch,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Flow'
        });
        return items;
    };
    factory._getLoopBlocks = function (scope, owner, target, group) {
        var items = [];
        items.push({
            name: 'Loops',
            iconClass: 'el-icon-repeat',
            items: [
                {
                    name: 'While',
                    owner: owner,
                    iconClass: 'el-icon-repeat',
                    proto: WhileBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        condition: "[Volume]<=100"
                    }
                },
                {
                    name: 'For',
                    owner: owner,
                    iconClass: 'el-icon-repeat',
                    proto: ForBlock,
                    target: target,
                    ctrArgs: {
                        scope: scope,
                        group: group,
                        initial: '1',
                        comparator: "<=",
                        "final": '5',
                        modifier: '+1',
                        counterName: 'value'
                    }
                }
            ]
        });

        //tell everyone
        factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST, {
            items: items,
            group: 'Loops'
        });
        return items;
    };
    factory._getMathBlocks = function (scope, owner, dstItem, group) {
        var items = [];
        items.push({
            name: 'Math',
            owner: this,
            iconClass: 'el-icon-qrcode',
            dstItem: dstItem,
            items: [
                {
                    name: 'If...Else',
                    owner: dstItem,
                    iconClass: 'el-icon-compass',
                    proto: IfBlock,
                    item: dstItem,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        return items;
    };
    factory._getTimeBlocks = function (scope, owner, dstItem, group) {
        var items = [];
        items.push({
            name: 'Time',
            owner: this,
            iconClass: 'el-icon-qrcode',
            dstItem: dstItem,
            items: [
                {
                    name: 'If...Else',
                    owner: dstItem,
                    iconClass: 'el-icon-time',
                    proto: IfBlock,
                    item: dstItem,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }

            ]
        });
        return items;
    };
    factory._getTransformBlocks = function (scope, owner, dstItem, group) {
        var items = [];
        items.push({
            name: 'Time',
            owner: this,
            iconClass: 'el-icon-magic',
            dstItem: dstItem,
            items: [
                {
                    name: 'If...Else',
                    owner: dstItem,
                    iconClass: 'el-icon-time',
                    proto: IfBlock,
                    item: dstItem,
                    ctrArgs: {
                        scope: scope,
                        group: group
                    }
                }
            ]
        });
        return items;
    };
    return factory;
});
},
'xblox/model/functions/StopBlock':function(){
/** @module xblox/model/functions/StopBlock **/
define([
    'dcl/dcl',
    'xide/utils',
    "xblox/model/Block"
], function(dcl,utils,Block){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl(Block,{
        declaredClass:"xblox.model.functions.StopBlock",
        command:'Select command please',
        icon:'',
        args:null,
        _timeout:100,
        hasInlineEdits:true,
        solve:function(scope,settings) {
            if (this.command){
                var block = scope.resolveBlock(this.command);
                if(block && block.stop){
                    var res = block.stop();
                    this.onSuccess(this,settings);
                }else{
                    this.onFailed(this,settings);
                }
                return res;
            }
        },
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable:function(field,pos,type,title,mode){
            return "<a tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable editable-click'  href='#'>" + this[field] +"</a>";
        },
        getFieldOptions:function(field){
            if(field ==="command"){
                return this.scope.getCommandsAsOptions("text");
            }
        },
        toText:function(){
            var text = 'Unknown';
            var block = this.scope.getBlock(this.command);
            if(block){
                text = block.name;
            }
            if(this.command.indexOf('://')!==-1) {
                text = '<span class="text-info">' +this.scope.toFriendlyName(this,this.command) + '</span>';
            }
            return this.getBlockIcon('D') + 'Stop Command : ' + text;
        },
        onChangeField:function(what,value){
        },
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.command,{
                    group:'General',
                    title:'Command',
                    dst:'command',
                    options:this.scope.getCommandsAsOptions(),
                    block:this,
                    pickerType:'command',
                    value:this.command
            }));
            return fields;
        }
    });
});
},
'xblox/model/functions/PauseBlock':function(){
/** @module xblox/model/functions/PauseBlock **/
define([
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'dojo/Deferred',
    "xblox/model/Block"
], function(dcl,utils,types,Deferred,Block){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl(Block,{
        declaredClass:"xblox.model.functions.PauseBlock",
        command:'Select command please',
        icon:'',
        args:null,
        _timeout:100,
        hasInlineEdits:true,
        /***
         * Returns the block run result
         * @param scope
         */
        solve:function(scope,settings) {
            if (this.command){
                var _args = null;
                var block = scope.resolveBlock(this.command);
                if(block && block.pause){
                    var res = block.pause();
                    this.onSuccess(this,settings);
                }else{
                    this.onFailed(this,settings);
                }
                return res;
            }
        },
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable:function(field,pos,type,title,mode,options,value){
            var optionsString = "";
            return "<a " + optionsString + "  tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable editable-click'  href='#'>" + this[field] +"</a>";
        },
        getFieldOptions:function(field){
            if(field ==="command"){
                return this.scope.getCommandsAsOptions("text");
            }
        },
        toText:function(){
            var text = 'Unknown';
            var block = this.scope.getBlock(this.command);
            if(block){
                text = block.name;
            }
            if(this.command.indexOf('://')!==-1) {
                text = '<span class="text-info">' +this.scope.toFriendlyName(this,this.command) + '</span>';
            }
            var _out = this.getBlockIcon('D') + 'Pause Command : ' + text;
            return _out;
        },
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz=this;
            var title = 'Command';
            if(this.command.indexOf('://')){
                title = this.scope.toFriendlyName(this,this.command);
            }
            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.command,{
                    group:'General',
                    title:'Command',
                    dst:'command',
                    options:this.scope.getCommandsAsOptions(),
                    block:this,
                    pickerType:'command',
                    value:this.command
            }));
            return fields;
        }
    });
});
},
'xblox/model/functions/SetProperties':function(){
define([
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'dojo/Deferred',
    "xblox/model/Block",
    "xide/lodash"
], function(dcl,utils,types,Deferred,Block,_){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl(Block,{
        declaredClass:"xblox.model.functions.SetProperties",
        command:'Select block',
        icon:'',
        args:null,
        _timeout:100,
        hasInlineEdits:false,
        solve:function(scope,settings) {
            var dfd = new Deferred();
            if (this.command){
                var block = scope.resolveBlock(this.command);
                if(block && this.props){
                    for(var prop in this.props){
                        block.set(prop,this.props[prop]);
                        block[prop] = this.props[prop];
                        block.onChangeField && block.onChangeField(prop,this.props[prop]);
                    }
                    this.onSuccess(this,settings);
                }else{
                    this.onFailed(this,settings);
                }
                dfd.resolve([]);
                return dfd;
            }
            return dfd;
        },
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable:function(field,pos,type,title,mode){
            var optionsString = "";
            return "<a " + optionsString + "  tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable editable-click'  href='#'>" + this[field] +"</a>";
        },
        getFieldOptions:function(field){
            if(field ==="command"){
                return this.scope.getCommandsAsOptions("text");
            }
        },
        toText:function(){
            var text = 'Unknown';
            var block = this.scope.getBlock(this.command);
            if(block){
                text = block.name;
            }
            if(this.command.indexOf('://')!==-1) {
                text = '<span class="text-info">' +this.scope.toFriendlyName(this,this.command) + '</span>';
            }
            return this.getBlockIcon('D') + 'Set Properties : ' + text;
        },
        serializeObject:function(field){
            return field === 'props';
        },
        onChangeField:function(field){
            if(field==='command'){
                delete this.props;
                this.props = {};
            }
        },
        init:function(){
            if(this.props && _.isString(this.props)){
                this.props = utils.fromJson(this.props);
            }

        },
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.command,{
                    group:'General',
                    title:'Command',
                    dst:'command',
                    options:this.scope.getCommandsAsOptions(),
                    block:this,
                    pickerType:'command',
                    value:this.command
            }));
            var block = this.scope.resolveBlock(this.command);
            if(block && block.getFields){
                if(!this.props){
                    this.props = {};
                }
                var _fields = block.getFields();
                var descr = _.find(_fields,{
                    dst:"description"
                });
                _fields.remove(descr);
                _.each(_fields,function(_field){
                    _field.group = "Properties";
                    _field.value = utils.getAt(this.props,_field.dst,_field.value);
                    _field.dst = "props." + _field.dst;

                },this);
                fields = fields.concat(_fields);
            }
            return fields;
        }
    });
});
},
'xblox/model/server/RunServerMethod':function(){
define([
    "dcl/dcl",
    "xblox/model/server/ServerBlock",
    'xide/utils'
], function (dcl, ServerBlock, utils) {
    /**
     * Runs a JSON-RPC-2.0 method on the server. This assumes that this block's scope has
     * a 'service object'
     */
    return dcl(ServerBlock, {
        declaredClass:"xblox.model.server.RunServerMethod",
        description: 'Runs a JSON-RPC-2.0 method on the server',

        /**
         * The name of the block, used in the UI
         * @member {string}
         */
        name: 'Run Server Method',

        /**
         * The full string of the service class method, ie: MyPHPServerClass::method
         * @member {string}
         */
        method: 'XShell::run',
        /**
         * Arguments for the server call
         * @member {string}
         */
        args: '',
        /**
         * Override in super class, this block runs async by default
         * @member {boolean}
         */
        deferred: true,
        /**
         * The default for the server RPC class
         * @member {string}
         */
        defaultServiceClass: 'XShell',
        /**
         * The default for the server RPC class method
         * @member {string}
         */
        defaultServiceMethod: 'run',

        sharable:true,
        /**
         * Callback when user edited the 'method' field. This will pre-populate the arguments field when empty
         * with the known SMD parameters : if possible.
         * @param newMethod
         * @param cis
         */
        onMethodChanged: function (newMethod, cis) {
            
            this.method = newMethod;

            //we auto populate the arguments field
            if (!utils.isValidString(this.args)) {

                var newServerParams = this.getServerParams();
                if (newServerParams) {
                    this._updateArgs(newServerParams, cis);
                }

            }
        },
        _getArgs: function () {


            /*
            var test = [
                {
                    "name": "shellType",
                    "default": "sh", "optional": false, "value": "notset"
                },
                {
                    "name": "cmd",
                    "optional": false,
                    "value": "[CurrentDirectory]"
                },
                {
                    "name": "cwd",
                    "default": null,
                    "optional": true,
                    "value": "[CurrentDirectory]"
                }
            ];*/


            var _args = utils.getJson(this.args || '[]');
            var result = [];
            if (_args) {
                var isSMD = false;
                //now check this is still in 'SMD' format
                if (_args && _args[0] && _args[0]['optional'] != null) {
                    isSMD = true;
                }
                //if SMD true, evaluate the value field
                if (isSMD) {
                    for (var i = 0; i < _args.length; i++) {
                        var _arg = _args[i];
                        var _variableValue = _arg.value;
                        var isBase64 = _arg.name.indexOf('Base64') != -1;
                        if(isBase64){
                            _variableValue = this.getService().base64_encode(_variableValue);
                        }

                        if (_arg.value !== 'notset') {
                            if (_arg.value.indexOf('[') != -1 && _arg.value.indexOf(']') != -1) {
                                _variableValue = this.scope.expressionModel.replaceVariables(this.scope, _arg.value, false, false);
                                if (_arg.name.indexOf('Base64') != -1) {
                                    _variableValue = this.getService().base64_encode(_variableValue);
                                }
                                result.push(_variableValue);
                            } else {
                                result.push(_variableValue || _arg['default']);
                            }

                        } else {
                            result.push(_arg['default'] || _variableValue);
                        }
                    }
                } else {

                }
            } else {
                return [this.args];
            }

            return result;

        },
        /**
         * Update this.args (string) with a SMD parameter set
         * @param params
         * @param cis
         * @private
         */
        _updateArgs: function (params, cis) {

            var argumentWidget = this.utils.getCIWidgetByName(cis, 'args');
            if (argumentWidget) {
                var _string = JSON.stringify(params);
                argumentWidget.editBox.set('value', _string);
                this.args = _string;

            }
        },
        /**
         * Find SMD for the current method
         * @returns {*}
         */
        getServerParams: function () {
            var service = this.getService();
            var params = service.getParameterMap(this.getServiceClass(), this.getServiceMethod());
            if (params) {
                for (var i = 0; i < params.length; i++) {
                    var param = params[i];
                    param.value = 'notset';
                }
            }
            return params;
        },
        onReloaded: function (evt) {

             0 && console.log('sdfsd');
            this._solve()
        },
        _solve:function(scope,settings,run,error){

             0 && console.log('solve223');

        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        solve: function (scope, settings, run, error) {



            this._return = [];
            this._lastResult=null;
            var thiz = this;
            var ret = [];

            //this._solve();



            // 0 && console.dir(this.scope);


            if(!this.isInValidState()){
                this.onFailed(this, settings);
                return ret;
            }

            var _args = this._getArgs();//returns SMD ready array of values
            var _cbSuccess = function (response) {
                thiz._lastResult = thiz.utils.getJson(response) || [response];
                thiz.resolved(thiz._lastResult);
                thiz.onSuccess(thiz, settings);
            };
            var _cbError = function (response) {
                //console.error('   server method ' + thiz.method + ' with params ' + JSON.stringify(_args)  + 'failed ' + response);
                thiz._lastResult = thiz.utils.getJson(response) || [response];
                thiz.resolved(thiz._lastResult);
                thiz.onFailed(thiz, settings);
            };

            this.onRun(this, settings);

            var service = this.getService();
            var serviceObject = this.scope.serviceObject;
            //service.callMethodEx(this.getServiceClass(), this.getServiceMethod(), _args, _cbSuccess,_cbError);

            console.error('run deferred');

            var dfd = serviceObject.runDeferred(this.getServiceClass(),this.getServiceMethod(),_args);
            if(dfd){
                dfd.then(function(data){
                    console.error('returned ',data);
                });
            }

            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function () {

            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if (this.method) {
                result += this.method.substr(0, 50);
            }
            return result;
        },

        //  standard call from interface
        canAdd: function () {
            return [];
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz = this;

            fields.push(utils.createCI('value', 25, this.method, {
                group: 'General',
                title: 'Method',
                dst: 'method',
                description: 'This should be in the format : MyServerClass::myMethod',
                delegate: {
                    runExpression: function (val, run, error) {
                        var old = thiz.method;
                        thiz.method = val;
                        var _res = thiz.solve(thiz.scope, null, run, error);
                    }
                }
            }));
            fields = fields.concat(this.getServerDefaultFields());
            return fields;
        },
        getBlockIcon: function () {
            return '<span class="fa-plug"></span>';
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Store
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Store function override
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren: function (parent) {
            return this.items != null && this.items.length > 0;
        },

        /**
         * Store function override
         * @param parent
         * @returns {Array}
         */
        getChildren: function (parent) {
            return this.items;
        },

        onChangeField: function (field, newValue, cis) {
            if (field === 'method') {
                this.onMethodChanged(newValue, cis);
            }
        },

        /**
         * Check our scope has a service object
         * @returns {boolean}
         */
        isInValidState: function () {

            return this.getService() != null;
        },
        getService: function () {

            var service = this.scope.getService();

            if(!service){
                console.error('have no service object');
            }
            return service;
        },
        getServiceClass: function () {
            return this.method.split('::')[0] || this.defaultServiceClass;
        },
        getServiceMethod: function () {
            return this.method.split('::')[1] || this.defaultServiceMethod;
        },
        hasMethod: function (method) {
            return this.isInValidState() &&
            this.getService()[this.getServiceClass()] != null &&
            this.getService()[this.getServiceClass()][this.getServiceMethod()] != null
        },
        hasServerClass: function (_class) {
            return this.isInValidState() &&
            this.getService()[this.getServiceClass()] != null;
        },
        getServerFunction: function () {
            if (this.isInValidState() && this.getServiceClass() && this.getServiceMethod()) {
                return this.getService()[this.getServiceClass()][this.getServiceMethod()];
            }
            return null;
        }


    });
});
},
'xblox/model/server/ServerBlock':function(){
define([
    'dcl/dcl',
    "xblox/model/Block",
    "xide/utils"
], function (dcl,Block,utils) {
    /**
     * Runs a JSON-RPC-2.0 method on the server. This assumes that this block's scope has
     * a 'service object'
     */
    return dcl(Block, {
        declaredClass:"xblox.model.server.ServerBlock",
        /**
         * The name of the block, used in the UI
         * @member {string}
         */
        name: 'Run Server Block',
        /**
         * The full string of the service class method, ie: MyPHPServerClass::method
         * @member {string}
         */
        method: 'XShell::run',
        /**
         * Arguments for the server call
         * @member {string}
         */
        args: '',
        /**
         * Override in super class, this block runs async by default
         * @member {boolean}
         */
        deferred: true,
        /**
         * The default for the server RPC class
         * @member {string}
         */
        defaultServiceClass: 'XShell',
        /**
         * The default for the server RPC class method
         * @member {string}
         */
        defaultServiceMethod: 'run',
        /**
         * Debugging
         * @member {function}
         */
        sharable:true,

        onReloaded: function () {
        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        solve: function (scope, settings, run, error) {

            this._currentIndex = 0;
            this._return = [];

            var _script = '' + this.method;
            var thiz = this;

            if (_script && _script.length) {

                var _function = new Function("{" + _script + "}");
                var _args = this.getArgs();
                try {
                    var _parsed = _function.apply(this, _args || {});
                    this._lastResult = _parsed;

                    if (run) {
                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                    }
                    if (_parsed !== 'false' && _parsed !== false) {
                        this.onSuccess(this, settings);
                    } else {
                        this.onFailed(this, settings);
                        return [];
                    }
                } catch (e) {
                    if (error) {
                        error('invalid expression : \n' + _script + ': ' + e);
                    }
                    this.onFailed(this, settings);
                    return [];
                }
            } else {
                console.error('have no script');
            }
            var ret = [], items = this[this._getContainer()];
            if (items.length) {
                this.runFrom(items, 0, settings);
            } else {
                this.onSuccess(this, settings);
            }

            return ret;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function () {

            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if (this.method) {
                result += this.method.substr(0, 50);
            }
            return result;
        },

        //  standard call from interface
        canAdd: function () {
            return [];
        },
        getServerDefaultFields:function(target){

            var fields = target || [];

            fields.push(utils.createCI('args', 27, this.args, {
                group: 'General',
                title: 'Arguments',
                dst: 'args'
            }));

            return fields;
        },
        getBlockIcon: function () {
            return '<span class="fa-plug"></span>';
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Store
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Store function override
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren: function (parent) {
            return this.items != null && this.items.length > 0;
        },

        /**
         * Store function override
         * @param parent
         * @returns {Array}
         */
        getChildren: function (parent) {
            return this.items;
        },

        /**
         * Check our scope has a service object
         * @returns {boolean}
         */
        isInValidState: function () {
            return this.getService() != null;
        },
        getService: function () {
            return this.scope.getService();
        },
        getServiceClass: function () {
            return this.method.split('::')[0] || this.defaultServiceClass;
        },
        getServiceMethod: function () {
            return this.method.split('::')[1] || this.defaultServiceMethod;
        },
        hasMethod: function (method) {
            return this.isInValidState() &&
            this.getService()[this.getServiceClass()] != null &&
            this.getService()[this.getServiceClass()][this.getServiceMethod()] != null
        },
        hasServerClass: function (_class) {
            return this.isInValidState() &&
            this.getService()[this.getServiceClass()] != null;
        },
        getServerFunction: function () {
            if (this.isInValidState() && this.getServiceClass() && this.getServiceMethod()) {
                return this.getService()[this.getServiceClass()][this.getServiceMethod()];
            }
            return null;
        }
    });
});
},
'xblox/model/server/Shell':function(){
define([
    "dcl/dcl",
    "xblox/model/server/ServerBlock",
    'xide/utils',
    'xcf/model/Command'
], function (dcl, ServerBlock, utils, Command) {
    /**
     * Runs a JSON-RPC-2.0 method on the server. This assumes that this block's scope has
     * a 'service object'
     */
    return dcl([Command, ServerBlock], {
        declaredClass: "xblox.model.server.Shell",
        description: 'Runs a JSON-RPC-2.0 method on the server',
        /**
         * The name of the block, used in the UI
         * @member {string}
         */
        name: 'Run Shell',

        /**
         * The full string of the service class method, ie: MyPHPServerClass::method
         * @member {string}
         */
        method: '',
        /**
         * Arguments for the server call
         * @member {string}
         */
        args: '',
        /**
         * Override in super class, this block runs async by default
         * @member {boolean}
         */
        deferred: true,
        /**
         * The default for the server RPC class
         * @member {string}
         */
        defaultServiceClass: 'XShell',
        /**
         * The default for the server RPC class method
         * @member {string}
         */
        defaultServiceMethod: 'run',

        sharable: true,
        /**
         * Callback when user edited the 'method' field. This will pre-populate the arguments field when empty
         * with the known SMD parameters : if possible.
         * @param newMethod
         * @param cis
         */
        onMethodChanged: function (newMethod, cis) {
            this.method = newMethod;
            //we auto populate the arguments field
            if (!utils.isValidString(this.args)) {

                var newServerParams = this.getServerParams();
                if (newServerParams) {
                    this._updateArgs(newServerParams, cis);
                }

            }
        },
        _getArgs: function () {


            /*
             var test = [
             {
             "name": "shellType",
             "default": "sh", "optional": false, "value": "notset"
             },
             {
             "name": "cmd",
             "optional": false,
             "value": "[CurrentDirectory]"
             },
             {
             "name": "cwd",
             "default": null,
             "optional": true,
             "value": "[CurrentDirectory]"
             }
             ];*/


            var _args = utils.getJson(this.args || '[]');
            var result = [];
            if (_args) {
                var isSMD = false;
                //now check this is still in 'SMD' format
                if (_args && _args[0] && _args[0]['optional'] != null) {
                    isSMD = true;
                }
                //if SMD true, evaluate the value field
                if (isSMD) {
                    for (var i = 0; i < _args.length; i++) {
                        var _arg = _args[i];
                        var _variableValue = _arg.value;
                        var isBase64 = _arg.name.indexOf('Base64') != -1;
                        if (isBase64) {
                            _variableValue = this.getService().base64_encode(_variableValue);
                        }

                        if (_arg.value !== 'notset') {
                            if (_arg.value.indexOf('[') != -1 && _arg.value.indexOf(']') != -1) {
                                _variableValue = this.scope.expressionModel.replaceVariables(this.scope, _arg.value, false, false);
                                if (_arg.name.indexOf('Base64') != -1) {
                                    _variableValue = this.getService().base64_encode(_variableValue);
                                }
                                result.push(_variableValue);
                            } else {
                                result.push(_variableValue || _arg['default']);
                            }

                        } else {
                            result.push(_arg['default'] || _variableValue);
                        }
                    }
                } else {

                }
            } else {
                return [this.args];
            }

            return result;

        },
        /**
         * Update this.args (string) with a SMD parameter set
         * @param params
         * @param cis
         * @private
         */
        _updateArgs: function (params, cis) {

            var argumentWidget = this.utils.getCIWidgetByName(cis, 'args');
            if (argumentWidget) {
                var _string = JSON.stringify(params);
                argumentWidget.editBox.set('value', _string);
                this.args = _string;

            }
        },
        /**
         * Find SMD for the current method
         * @returns {*}
         */
        getServerParams: function () {
            var service = this.getService();
            var params = service.getParameterMap(this.getServiceClass(), this.getServiceMethod());
            if (params) {
                for (var i = 0; i < params.length; i++) {
                    var param = params[i];
                    param.value = 'notset';
                }
            }
            return params;
        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        solve: function (scope, settings, isInterface, send, run, error) {
            this._return = [];
            this._lastResult = null;
            var thiz = this;
            var ret = [];
            settings = this._lastSettings = settings || this._lastSettings;
            var instance = this.getInstance();
            var code = scope.expressionModel.replaceVariables(scope, this.method, false, false);
            instance.runShell(code, utils.mixin({}, {}), this.id, this.id, this);
            return;

        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function () {

            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if (this.method) {
                result += this.method.substr(0, 50);
            }
            return result;
        },

        //  standard call from interface
        canAdd: function () {
            return [];
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz = this;
            fields.push(utils.createCI('value', 25, this.method, {
                group: 'General',
                title: 'Cmd',
                dst: 'method',
                delegate: {
                    runExpression: function (val, run, error) {
                        var old = thiz.method;
                        thiz.method = val;
                        var _res = thiz.solve(thiz.scope, null, run, error);
                    }
                }
            }));
            return fields;
        },
        getBlockIcon: function () {
            return '<span class="fa-plug"></span>';
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Store
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Store function override
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren: function (parent) {
            return this.items != null && this.items.length > 0;
        },

        /**
         * Store function override
         * @param parent
         * @returns {Array}
         */
        getChildren: function (parent) {
            return this.items;
        },

        onChangeField: function (field, newValue, cis) {
            if (field === 'method') {
                this.onMethodChanged(newValue, cis);
            }
        },

        /**
         * Check our scope has a service object
         * @returns {boolean}
         */
        isInValidState: function () {

            return this.getService() != null;
        },
        getService: function () {
            var service = this.scope.getService();
            if (!service) {
                console.error('have no service object');
            }
            return service;
        },
        getServiceClass: function () {
            return this.method.split('::')[0] || this.defaultServiceClass;
        },
        getServiceMethod: function () {
            return this.method.split('::')[1] || this.defaultServiceMethod;
        },
        hasMethod: function (method) {
            return this.isInValidState() &&
                this.getService()[this.getServiceClass()] != null &&
                this.getService()[this.getServiceClass()][this.getServiceMethod()] != null
        },
        hasServerClass: function (_class) {
            return this.isInValidState() &&
                this.getService()[this.getServiceClass()] != null;
        },
        getServerFunction: function () {
            if (this.isInValidState() && this.getServiceClass() && this.getServiceMethod()) {
                return this.getService()[this.getServiceClass()][this.getServiceMethod()];
            }
            return null;
        }
    });
});
},
'xblox/model/mqtt/Subscribe':function(){
/** @module xblox/model/mqtt/Subscribe **/
define([
    'dcl/dcl',
    'xdojo/has',
    "xblox/model/Block",
    'xide/utils',
    'xblox/model/Contains',
    'xide/types'
    //'dojo/has!host-node?dojo/node!tracer',
    //'dojo/has!host-node?nxapp/utils/_console'
], function(dcl,has,Block,utils,Contains,types,tracer,_console){

    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    var isServer =  0 ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if(isServer && tracer && console && console.error){
        console = _console;
    }
    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    /**
     * Base block class.
     *
     * @class module:xblox/model/mqtt/Subscribe
     * @extends module:xblox/model/Block
     */
    return dcl([Block,Contains],{
        declaredClass:"xblox.model.mqtt.Subscribe",
        //method: (String)
        //  block action name
        name:'Subscribe',
        //method: (String)
        //  block action name
        topic:'Topic',
        args:'',
        deferred:false,
        sharable:true,
        context:null,
        icon:'fa-bell',
        /**
         * @type {string|null}
         */
        path:'',
        qos:0,
        stop:function(){
            var instance = this.getInstance();
            if(instance){
                instance.callMethod('unSubscribeTopic',utils.mixin({
                    topic:this.topic
                },utils.getJson(this.args || {})),this.id,this.id);
            }
        },
        onData:function(message){
            if(message && message.topic && message.topic==this.topic){
                delete message['src'];
                delete message['id'];
                var items = this[this._getContainer()];
                var settings = this._lastSettings;
                var ret=[];
                if(items.length>0){
                    var value = message;
                    var path = this.path && this.path.length ? this.path : (message.payload!==null) ? 'payload' : null;
                    if(path && _.isObject(message)){
                        value = utils.getAt(message,path,message);
                    }
                    for(var n = 0; n < this.items.length ; n++)
                    {
                        var block = this.items[n];
                        if(block.enabled) {
                            block.override ={
                                args: [value]
                            };
                            ret.push(block.solve(this.scope,settings));
                        }
                    }
                }
                this.onSuccess(this, this._lastSettings);
                return ret;
            }
        },
        observed:[
            'topic'
        ],
        getContext:function(){
            return this.context || (this.scope.getContext ?  this.scope.getContext() : this);
        },
        solve:function(scope,settings){
            this._currentIndex = 0;
            this._return=[];
            this._lastSettings = settings;
            var instance = this.getInstance();

            if(instance){
                instance.callMethod('subscribeTopic',utils.mixin({
                    topic:this.topic,
                    qos:this.qos
                },utils.getJson(this.args ||"{}")),this.id,this.id);
            }
            settings = settings || {};
            this.onRunThis(settings);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){
            var result = '<span style="">' +
                this.getBlockIcon() + '' + this.makeEditable('topic','bottom','text','Enter a topic','inline') + ' :: '+'</span>';

            if(this.topic){
                result+= this.topic.substr(0,30);
            }
            return result;
        },
        //  standard call from interface
        canAdd:function(){
            return [];
        },
        //  standard call for editing
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(
                utils.createCI('name',13,this.name,{
                    group:'General',
                    title:'Name',
                    dst:'name'
                })
            );

            fields.push(utils.createCI('arguments',27,this.args,{
                    group:'Arguments',
                    title:'Arguments',
                    dst:'args'
                }));

            fields.push(utils.createCI('topic',types.ECIType.STRING,this.topic,{
                    group:'General',
                    title:'Topic',
                    dst:'topic',
                    select:true
                }));

            fields.push(utils.createCI('name',types.ECIType.ENUMERATION,this.qos,{
                    group:'General',
                    title:'QOS',
                    dst:'qos',
                    widget: {
                        options: [
                            {
                                label:"0 - at most once",
                                value:0
                            },
                            {
                                label:"1 - at least once",
                                value:1
                            },
                            {
                                label:"2 - exactly once",
                                value:2
                            }
                        ]
                    }
                })
            );
            fields.push(utils.createCI('path',types.ECIType.STRING,this.path,{
                group:'General',
                title:'Value Path',
                dst:'path'
            }));
            return fields;
        }
    });
});
},
'xblox/model/mqtt/Publish':function(){
/** @module xblox/model/mqtt/Publish **/
define([
    'dcl/dcl',
    'xdojo/has',
    'xide/utils',
    'xide/types',
    'xcf/model/Command'
    //'xdojo/has!host-node?dojo/node!tracer',
    //'xdojo/has!host-node?nxapp/utils/_console'
], function (dcl, has, utils, types, Command, tracer, _console) {

    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */

    var isServer =  0 ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if (isServer && tracer && console && console.error) {
        console = _console;
    }

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    /**
     * Base block class.
     *
     * @class module:xblox/model/mqtt/Publish
     * @extends module:xblox/model/Block
     */
    return dcl(Command, {
        declaredClass: "xblox.model.mqtt.Publish",
        //method: (String)
        //  block action name
        name: 'Publish',
        //method: (String)
        //  block action name
        topic: '',
        args: '',
        deferred: false,
        sharable: true,
        context: null,
        icon: 'fa-send',
        isCommand: true,
        qos: 0,
        retain: false,
        /**
         * @type {string|null}
         */
        path: null,
        flags: 0,
        onData: function (message) {
            if (message && message.topic && message.topic == this.topic) {
                var thiz = this,
                    ctx = this.getContext(),
                    items = this[this._getContainer()];

                var settings = this._lastSettings;
                var ret = [];
                if (items.length > 0) {
                    var value = message;
                    this.path = 'value';
                    if (this.path && _.isObject(message)) {
                        value = utils.getAt(message, this.path, message);
                    }

                    for (var n = 0; n < this.items.length; n++) {
                        var block = this.items[n];
                        if (block.enabled) {
                            block.override = {
                                args: [value]
                            };
                            ret.push(block.solve(this.scope, settings));
                        }
                    }
                }
                this.onSuccess(this, this._lastSettings);
                return ret;
            }
        },
        observed: [
            'topic'
        ],
        getContext: function () {
            return this.context || (this.scope.getContext ? this.scope.getContext() : this);
        },
        /**
         *
         * @param scope
         * @param settings
         * @param isInterface
         * @returns {boolean}
         */
        solve: function (scope, settings, isInterface) {
            this._currentIndex = 0;
            this._return = [];
            settings = this._lastSettings = settings || this._lastSettings;

            var instance = this.getInstance();
            if (isInterface === true && this._loop) {
                this.reset();
            }

            var args = this.args;
            var inArgs = this.getArgs(settings);
            if (inArgs[0]) {
                args = inArgs[0];
            }

            if (instance) {
                var flags = settings.flags || this.flags;
                var parse = !(flags & types.CIFLAG.DONT_PARSE);
                var isExpression = (flags & types.CIFLAG.EXPRESSION);
                var value = utils.getJson(args, true, false);
                if (value === null || value === 0 || value === true || value === false || !_.isObject(value)) {
                    value = {
                        payload: this.args
                    };
                }
                var topic = scope.expressionModel.replaceVariables(scope, this.topic, false, false);
                
                if (value.payload && _.isString(value.payload) && parse) {
                    var override = settings.override || this.override || {};
                    var _overrides = (override && override.variables) ? override.variables : null;
                    if (parse !== false) {
                        value.payload = utils.convertAllEscapes(value.payload, "none");
                    }
                    if (isExpression && parse !== false) {
                        value.payload = scope.parseExpression(value.payload, null, _overrides, null, null, null, override.args);
                    }
                }
                instance.callMethod('publishTopic', utils.mixin({
                    topic: topic,
                    qos: this.qos,
                    retain: this.retain
                }, value), this.id, this.id);
            }

            settings = settings || {};
            this.onDidRun();
            this.onSuccess(this, settings);
            return true;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function (icon) {
            var out = '<span style="">' + this.getBlockIcon() + ' ' + this.makeEditable('name', 'top', 'text', 'Enter a name', 'inline') + ' :: ' + '</span>';
            if (this.topic) {
                out += this.makeEditable('topic', 'bottom', 'text', 'Enter a topic', 'inline');
                this.startup && (out += this.getIcon('fa-bell inline-icon text-warning', 'text-align:right;float:right;', ''));
                this.interval > 0 && (out += this.getIcon('fa-clock-o inline-icon text-warning', 'text-align:right;float:right', ''));
            }
            return out;
        },
        //  standard call from interface
        canAdd: function () {
            return [];
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('qos', types.ECIType.ENUMERATION, this.qos, {
                group: 'General',
                title: 'QOS',
                dst: 'qos',
                widget: {
                    options: [
                        {
                            label: "0 - at most once",
                            value: 0
                        },
                        {
                            label: "1 - at least once",
                            value: 1
                        },
                        {
                            label: "2 - exactly once",
                            value: 2
                        }
                    ]
                }
            })
            );
            fields.push(utils.createCI('arguments', 27, this.args, {
                group: 'Arguments',
                title: 'Arguments',
                dst: 'args'
            }));
            fields.push(this.utils.createCI('flags', 5, this.flags, {
                group: 'Arguments',
                title: 'Flags',
                dst: 'flags',
                data: [
                    {
                        value: 0x000001000,
                        label: 'Dont parse',
                        title: "Do not parse the string and use it as is"
                    },
                    {
                        value: 0x00000800,//2048
                        label: 'Expression',
                        title: 'Parse it as Javascript'
                    }
                ],
                widget: {
                    hex: true
                }
            }));
            fields.push(utils.createCI('topic', types.ECIType.STRING, this.topic, {
                group: 'General',
                title: 'Topic',
                dst: 'topic',
                select: true
            }));
            fields.push(utils.createCI('retain', types.ECIType.BOOL, this.retain, {
                group: 'General',
                title: 'Retain',
                dst: 'retain'
            }));
            fields.remove(_.find(fields, {
                name: "send"
            }));

            fields.remove(_.find(fields, {
                name: "waitForResponse"
            }));
            return fields;
        }
    });
});
},
'xblox/model/File/ReadJSON':function(){
/** @module xblox/model/File/ReadJSON **/
define([
    'dcl/dcl',
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xblox/model/Contains',
    'xide/types',
    "xdojo/has!xblox-ui?xfile/data/DriverStore",
    'xdojo/has!xblox-ui?xfile/views/FileGridLight'
], function (dcl, Deferred, Block, utils, Contains, types, DriverStore, FileGridLight) {

    /**
     *
     * @param ext
     * @param config
     * @param options
     * @param fileServer
     * @returns {*}
     */
    function createStore(ext,options,fileServer) {
        return new DriverStore({
            data: [],
            config: {},
            mount: 'none',
            options: options,
            driver: fileServer,
            micromatch: "(*.json)|!(*.*)", // Only folders and json files
            //micromatch: "(*.mp3)|(*.wav)|(*.webm)|!(*.*)", // Only folders and json files
            glob: ext
        });
    }
    /**
     *
     * @class module:xblox/model/code/RunScript
     * @extends module:xblox/model/Block
     * @augments module:xblox/model/Block_UI
     */
    return dcl([Block, Contains], {
        declaredClass: "xblox.model.File.ReadJSON",
        name: 'Read JSON',
        deferred: false,
        sharable: false,
        context: null,
        icon: 'fa-file',
        observed: [
            'path'
        ],
        getContext: function () {
            return this.context || (this.scope.getContext ? this.scope.getContext() : this);
        },
        getFileContent: function (path) {
            var scope = this.getScope();
            var ctx = scope.ctx;
            var deviceManager = ctx.getDeviceManager();
            var fileServer = deviceManager.getInstanceByName('File-Server');
            return fileServer.callCommand('GetProg', {
                override: {
                    args: [path]
                }
            });
        },
        processJSON: function (data, settings) {
            var path = this.jsonPath;
            if (path) {
                this._lastResult = utils.getAt(data, path);
            } else {
                this._lastResult = data;
            }
            this.onSuccess(this, settings);
            this.runByType(types.BLOCK_OUTLET.FINISH, settings);
        },
        /**
         *
         * @param scope
         * @param settings
         * @param isInterface
         * @param run
         * @param error
         * @returns {Deferred}
         */
        solve: function (scope, settings, isInterface, run, error) {
            this._currentIndex = 0;
            this._return = [];
            settings = this._lastSettings = settings || this._lastSettings || {};
            var _script = ('' + this._get('path')),
                thiz = this,
                dfd = new Deferred(),
                self = this;

            this.onRunThis(settings);
            var expression = scope.expressionModel.replaceVariables(scope, _script, null, null);
            var getDfd = this.getFileContent(expression);
            getDfd.then(function (data) {
                var content = data.content;
                if (content) {
                    content = utils.getJson(content, true);
                    if (content) {
                        self.processJSON(content, settings);
                    }
                }
            }.bind(this));
            try {
                if (run) {
                    run('Expression ' + _script + ' evaluates to ' + expression);
                }
            } catch (e) {
                thiz.onDidRunItemError(dfd, e, settings);
                thiz.onFailed(thiz, settings);
                if (error) {
                    error('invalid expression : \n' + _script + ': ' + e);
                }
            }
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI impl.
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function () {
            var result = '<span style="">' + this.getBlockIcon() + ' ' + this.name + ' :: ' + '</span>';
            if (this.path) {
                result += this.path.substr(0, 50);
            }
            return result;
        },
        //  standard call from interface
        canAdd: function () {
            return [];
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var scope = this.getScope();
            var ctx = scope.ctx;
            var deviceManager = ctx.getDeviceManager();
            var fileServer = deviceManager.getInstanceByName('File-Server');//system's default
            var permissions = utils.clone(types.DEFAULT_FILE_GRID_PERMISSIONS);
            if (fileServer && DriverStore) {
                var FilePickerOptions = {
                    ctx: ctx,
                    owner: this,
                    selection: '/',
                    resizeToParent: true,
                    Module: FileGridLight,
                    permissions: permissions
                },
                options = {
                    fields: types.FIELDS.SHOW_ISDIR | types.FIELDS.SHOW_OWNER | types.FIELDS.SHOW_SIZE |
                    types.FIELDS.SHOW_FOLDER_SIZE |
                    types.FIELDS.SHOW_MIME |
                    types.FIELDS.SHOW_PERMISSIONS |
                    types.FIELDS.SHOW_TIME |
                    types.FIELDS.SHOW_MEDIA_INFO
                };
                FilePickerOptions.leftStore = createStore("/*",options,fileServer);
                FilePickerOptions.rightStore = createStore("/*",options,fileServer);
                fields.push(utils.createCI('path', 4, this.path, {
                    group: 'General',
                    title: 'Path',
                    dst: 'path',
                    filePickerOptions: FilePickerOptions,
                    widget: {
                        item: this
                    }
                }));
            }
            fields.push(utils.createCI('jsonPath', 13, this.jsonPath, {
                group: 'General',
                title: 'Select',
                dst: 'jsonPath'
            }));
            return fields;
        }
    });
});
},
'xblox/embedded_ui':function(){
define([
    'xblox/embedded',
    'xblox/factory/Blocks',
    'xide/widgets/ScriptWidget'
], function () {
});
},
'xblox/views/BlockGrid':function(){
/** @module xblox/views/BlockGrid **/
define([
    'dojo/_base/declare',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xblox/views/Grid',
    'xide/layout/_Accordion',
    'xide/views/CIViewMixin',
    'xaction/DefaultActions',
    'dojo/dom-class',
    'dojo/Deferred',
    'xide/views/History',
    'xide/widgets/IconPicker' // not loaded yet
], function (declare, types, utils, factory, Grid, _Accordion, CIViewMixin, DefaultActions, domClass, Deferred, History, IconPicker) {

    var ACTION = types.ACTION;
    var debugMark = false;

    function clearMarkTimer(element) {
        if (element.hightlightTimer) {
            clearTimeout(element.hightlightTimer);
            element.hightlightTimer = null;
        }
    }

    /**
     * Standard block grid class.
     * @class module:xblox/views/BlockGrid
     * @augments module:xide/mixins/EventedMixin
     */
    var IMPLEMENTATION = {
        highlightDelay: 500,
        currentCIView: null,
        targetTop: null,
        lastSelectedTopTabTitle: 'General',
        showHeader: false,
        toolbarInitiallyHidden: true,
        contextMenuArgs: {
            actionFilter: {
                quick: true
            }
        },
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Step': 115,
            'New': 4,
            'Window': 1
        },
        permissions: [
            //ACTION.TOOLBAR
            //ACTION.EDIT,
            //ACTION.RENAME,
            ACTION.RELOAD,
            ACTION.DELETE,
            ACTION.CLIPBOARD,
            ACTION.LAYOUT,
            ACTION.COLUMNS,
            ACTION.SELECTION,
            ACTION.DOWNLOAD,
            //ACTION.PREVIEW,
            ACTION.SAVE,
            ACTION.SEARCH,
            'Step/Run',
            'Step/Move Up',
            'Step/Move Down',
            ACTION.TOOLBAR,
            'Step/Properties',
            'Step/Edit',
            'Step/Back',
            'Step/Pause',
            ACTION.HEADER,
            ACTION.CONTEXT_MENU
        ],
        tabOrder: {
            'Home': 100,
            'View': 50,
            'Settings': 20
        },
        tabSettings: {
            'Step': {
                width: '190px'
            },
            'Show': {
                width: '130px'
            },
            'Settings': {
                width: '100%'
            }
        },
        groupOrder: {
            'Clipboard': 110,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'Select': 0
        },
        options: utils.clone(types.DEFAULT_GRID_OPTIONS),
        columns: [
            {
                renderExpando: true,
                label: "Name",
                field: "name",
                sortable: false,
                formatter: function (value, block) {
                    var blockText = '';
                    if (block.toText) {

                        blockText = block.toText();

                        return '<span class="" style="width: 95%;">' + blockText + '</span>';
                    }
                    return value;
                }
            }
        ],
        getTypeMap: function () {
            return {};
        },
        getBlock: function (id) {
            return this.blockScope.getBlockById(id);
        },
        formatOrder: function (value, block) {

            if (block) {
                var parent = block.getParent();
                var _index = block.index();
                if (parent) {
                    var parents = block.numberOfParents();
                    var _out = '&nbsp;';
                    for (var i = 0; i < parents; i++) {
                        _out += '&nbsp;'
                    }
                    _out += '';

                    for (var i = 0; i < parents; i++) {
                        _out += '-'
                    }
                    _out += '&nbsp;';
                    _index = _out + _index;
                } else {
                    return '&nbsp;' + _index;
                }
                return _index;
            }

            return '';
        },
        focus: function (element) {

            if (!element) {
                return;
            }
            return this.inherited(arguments);
        },
        postMixInProperties: function () {

            var _has = false;

            var self = this;
            _.each(this.columns, function (col) {
                if (col.field == 'order') {
                    _has = true;
                }
            });

            if (!_has && this.blockGroup !== 'basicVariables') {
                this.columns.unshift({
                    label: "Line",
                    field: "order",
                    sortable: false,
                    hidden: false,
                    formatter: function (val, b) {
                        return self.formatOrder(val, b, self);
                    }
                });
            }
            this.inherited(arguments);

        }, /*
         resize:function(){
          0 && console.log('-r');
         return this.inherited(arguments);
         },
         */
        /**
         * Override render row to add additional CSS class
         * for indicating a block's enabled state
         * @param object
         * @returns {*}
         */
        renderRow: function (object) {

            if (!this.collection) {
                 0 && console.warn('BlockGrid::renderRow : have no collection! Abort');
                return;
            }

            var res = this.inherited(arguments);

            if (object.enabled == false) {
                domClass.add(res, 'disabledBlock');
            }
            var _row = $(res),
                scope = object.getScope();
            var scopeId = scope ? scope.id : null;

            //restore status css class
            if (object.getStatusClass && scopeId) {
                var clsInfo = object.getStatusClass(scopeId);
                var element = $(res);
                //the block is marked with a class, restore it
                if (clsInfo && !element[0].hightlightTimer) {
                    this.mark($(res), clsInfo.className, object, clsInfo.delay);
                }
            }

            //add inline editors
            if (object.hasInlineEdits) {
                if (_row.__inline) {
                    return;
                }
                _row.__inline = true;
                var _links = _row.find('.editable');
                _.each(_links, function (link) {
                    link.object = object;

                    var $link = $(link),
                        _field = $link.attr('data-prop'),
                        _type = $link.attr('display-type') || 'text',
                        options = object.getFieldOptions ? object.getFieldOptions(_field) : null,
                        _editOptions = {
                            type: _type,
                            placement: $link.attr('pos') || 'right',
                            mode: $link.attr('display-mode') || 'popup',
                            title: $link.prop('data-title'),
                            inputclass: 'input-transparent',
                            success: function (data, value) {
                                var prop = $(this).attr('data-prop');
                                this.object.set(prop, value);
                            }
                        };
                    if (options) {
                        _editOptions.source = options;
                    }
                    $link.editable(_editOptions);
                    $link.attr("tabIndex", "-1");
                });
            }
            return res;
        },
        getCurrentParent: function () {
            var history = this.getHistory();
            var now = history.getNow();
            if (now) {
                return this.getBlock(now);
            } else {
                return null;
            }
        },
        /**
         *
         * @param item
         * @param action
         */
        addItem: function (item, action) {

            if (!item) {
                return false;
            }

            action = action || {};

            var target = this.getSelection()[0],
                proto = item.proto || action.proto,
                ctorArgs = item.ctrArgs || action.ctrArgs,
                where = null,
                newBlock = null;


            if (!target) {
                target = this.getCurrentParent();
            }

            //cache problem:
            if (!target) {
                var _item = this.getSelection()[0];
                if (_item) {
                    target = _item;
                }
            }


            if (ctorArgs) {
                if (ctorArgs.id) {
                    ctorArgs.id = utils.createUUID();
                }
                if (ctorArgs.parent) {
                    ctorArgs.parent = null;
                }
                if (ctorArgs.parentId) {
                    ctorArgs.parentId = null;
                }
                if (ctorArgs.items) {
                    ctorArgs.items = [];
                }
            }

            if (target && proto && ctorArgs) {

                if (target.owner && target.dstField) {
                    where = target.dstField;
                    target = target.owner;
                }

                ctorArgs['parentId'] = target.id;
                ctorArgs['group'] = null;
                ctorArgs['parent'] = target;//should be obselete

                newBlock = target.add(proto, ctorArgs, where);
                newBlock.parent = target;

            } else if (!target && proto && ctorArgs) {

                if (ctorArgs.group === "No Group" && this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.blockGroup) {
                    ctorArgs.group = this.blockGroup;
                }

                if (ctorArgs.group && this.newRootItemGroup && ctorArgs.group !== this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }

                newBlock = factory.createBlock(proto, ctorArgs);//root block
            }

            if (newBlock && newBlock.postCreate) {
                newBlock.postCreate();
            }

            if (!newBlock) {
                console.error('didnt create block');
                return;
            }


            var store = this.getStore(target) || this.collection,
                storeItem = store.getSync(newBlock[store.idProperty]);

            if (!storeItem) {
                console.error('new block not in store');
                store.putSync(newBlock);
            }

            try {
                newBlock.scope._emit(types.EVENTS.ON_ITEM_ADDED, {
                    item: newBlock,
                    owner: newBlock.scope
                });
            } catch (e) {
                logError(e);
            }
            this._itemChanged('added', newBlock, store);

            return storeItem;

        },
        _itemChanged: function (type, item, store) {

            store = store || this.getStore(item);

            var thiz = this;

            function _refreshParent(item, silent) {

                var parent = item.getParent();
                if (parent) {
                    var args = {
                        target: parent
                    };
                    if (silent) {
                        this._muteSelectionEvents = true;
                    }
                    store.emit('update', args);
                    if (silent) {
                        this._muteSelectionEvents = false;
                    }
                } else {
                    thiz.refresh();
                }
            }

            function refresh() {
                thiz.refreshRoot();
                //thiz.refreshCurrent();
                thiz.refresh();
                thiz.refreshCurrent();
            }

            function select(item, reason) {
                thiz.select(item, null, true, {
                    focus: true,
                    delay: 1,
                    append: false,
                    expand: true
                }, reason);
            }


            switch (type) {

                case 'added': {
                    refresh();
                    select(item);
                    break;
                }
                case 'moved': {
                    break;
                }
                case 'deleted': {
                    var parent = item.getParent();
                    var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                    if (parent) {
                        var _container = parent.getContainer();
                        if (_container) {
                            _.each(_container, function (child) {
                                if (child.id == item.id) {
                                    _container.remove(child);
                                }
                            });
                        }
                    }
                    this.refresh();
                    if (_prev) {
                        select(_prev);
                    }
                    break;
                }
            }
        },
        printRootOrder: function (items) {

            var items = items || this.collection.storage.fullData;
            var str = '';
            _.each(items, function (item, i) {
                str += 'id: ' + item.id + ' | ' + i + ' \n';
            });
             0 && console.log('order: \n' + str, JSON.stringify(this.collection.storage.index));

        },

        /**
         *
         * @param selection
         */
        refreshAll: function (selection) {

            if (!selection) {
                this._preserveSelection();
            }

            this.refreshRoot();
            this.refresh();

            if (!selection) {
                this._restoreSelection();
            }
            var thiz = this;

            if (selection) {
                setTimeout(function () {
                    thiz.select(selection, null, true, {
                        focus: true
                    });
                }, 30);
            }
        },
        getStore: function (item) {
            if (item) {
                if (_.isArray()) {
                    item = item[0];
                }
                if (item._store) {
                    return item._store;
                }
            }
            return null;
        },
        /**
         * Special refresh for changes at root level
         */
        refreshRoot: function () {
            return this.set('collection', this.collection.filter(this.getRootFilter()));
        },
        refresCurrent: function () {
            return this.set('collection', this.collection.filter(this.getRootFilter()));
        },
        /**
         * Standard filter for root level
         * @returns {{group: (null|string|*|string|className)}}
         */
        getRootFilter: function () {
            return {
                group: this.blockGroup
            };
        },
        _onCIChanged: function (ci, block, oldValue, newValue, field) {
        },
        clearPropertyPanel: function () {

            var props = this.getPropertyStruct();

            if (props.currentCIView) {
                props.currentCIView.empty();
                props.currentCIView = null;
            }

            if (props.targetTop) {
                utils.destroy(props.targetTop, true);
                props.targetTop = null;
            }

            props._lastItem = null;

            this.setPropertyStruct(props);
        },
        getState: function (state) {
            state = this.inherited(arguments) || {};
            var right = this.getRightPanel();
            state.properties = right.isExpanded();
            return state;
        },
        onCIChanged: function (ci, block, oldValue, newValue, field, cis) {
            if (oldValue === newValue) {
                return;
            }
            var _col = this.collection;
            block = this.collection.getSync(block.id);
            block.set(field, newValue);
            this.refreshActions();
            block[field] = newValue;
            _col.emit('update', {
                target: block,
                type: 'update'
            });
            block._store.emit('update', {
                target: block
            });
            this._itemChanged('changed', block, _col);
            block.onChangeField(field, newValue, cis, this);
            var cell = this.cell(block.id, '1');
            if (cell && cell.column) {
                this.refreshCell(cell).then(function (e) {
                });
            }
            if (field === 'enabled') {
                this.refresh();
            }
            this._itemChanged('changed', block, _col);
        },
        onShowProperties: function (item) {
            var _console = this._console;
            if (_console && item) {
                var value = item.send || item.method;
                var editor = _console.getConsoleEditor();
                editor.set('value', value);
                this._lastConsoleItem = item;
            }
        },
        showProperties: function (item, force) {
            var dfd = new Deferred(),
                block = item || this.getSelection()[0],
                thiz = this,
                rightSplitPosition = thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);

            if (!block || rightSplitPosition == 1 || block.canEdit()===false) {
                return;
            }
            var right = this.getRightPanel('Properties', null, 'DefaultTab', {});
            right.closeable(false);
            var props = this.getPropertyStruct();
            if (block == props._lastItem && force !== true) {
                return;
            }

            this.clearPropertyPanel();
            props = this.getPropertyStruct();
            props._lastItem = item;
            var _title = block.name || block.title;
            function propertyTabShown(tab, show) {
                if (!tab) {
                    return;
                }
                var title = tab.title;

                if (show) {
                    props.lastSelectedTopTabTitle = title;

                } else if (!show && props.lastSelectedTopTabTitle === title) {
                    props.lastSelectedTopTabTitle = null;
                }
            }

            var tabContainer = props.targetTop;
            if (!tabContainer) {
                tabContainer = utils.addWidget(_Accordion, {}, null, right.containerNode, true);

                $(tabContainer.domNode).addClass('CIView Accordion');
                props.targetTop = tabContainer;
                thiz.addHandle('show', tabContainer._on('show', function (evt) {
                    propertyTabShown(evt.view, true);
                }));

                thiz.addHandle('hide', tabContainer._on('hide', function (evt) {
                    propertyTabShown(evt.view, false);
                }));
            }

            _.each(tabContainer.getChildren(), function (tab) {
                tabContainer.removeChild(tab);
            });

            if (props.currentCIView) {
                props.currentCIView.empty();
            }

            if (!block.getFields) {
                 0 && console.log('have no fields', block);
                return;
            }

            var cis = block.getFields();
            for (var i = 0; i < cis.length; i++) {
                cis[i].vertical = true;
            }

            var ciView = new CIViewMixin({
                typeMap: thiz.getTypeMap(),
                tabContainerClass: _Accordion,
                tabContainer: props.targetTop,
                delegate: this,
                viewStyle: 'padding:0px;',
                autoSelectLast: true,
                item: block,
                source: this.callee,

                options: {
                    groupOrder: {
                        'General': 1,
                        'Advanced': 2,
                        'Script': 3,
                        'Arguments': 4,
                        'Description': 5,
                        'Share': 6

                    }
                },
                cis: cis
            });
            ciView.initWithCIS(cis);
            props.currentCIView = ciView;
            if (block.onFieldsRendered) {
                block.onFieldsRendered(block, cis);
            }
            thiz.addHandle('valueChanged', ciView._on('valueChanged', function (evt) {
                setTimeout(function () {
                    thiz.onCIChanged && thiz.onCIChanged(evt.ci, block, evt.oldValue, evt.newValue, evt.ci.dst);
                }, 10);
            }));
            var tabToOpen = tabContainer.getTab(props.lastSelectedTopTabTitle);
            if (tabToOpen) {
                tabContainer.selectChild(props.lastSelectedTopTabTitle, true);
            } else {
                props.lastSelectedTopTabTitle = null;
            }
            this.setPropertyStruct(props);
            this.onShowProperties(block);
            return dfd;

        },
        onSelectionChanged: function (evt, force) {
            var selection = evt.selection;
            var item = selection[0];
            var thiz = this,
                ctx = this.ctx,
                actionStore = thiz.getActionStore(),
                contextMenu = this.getContextMenu(),
                toolbar = this.getToolbar(),
                mainToolbar = ctx.getWindowManager().getToolbar(),
                itemActions = item && item.getActions ? item.getActions() : null,
                _debugTree = false,
                enableAction = this.getAction('Step/Enable');

            if (item) {
                enableAction.value = item.enabled;
            }
            var globals = actionStore.query({
                global: true
            });

            if (globals && !itemActions) {
                itemActions = [];
            }

            _.each(globals, function (globalAction) {
                var globalItemActions = globalAction.getActions ? globalAction.getActions(thiz.permissions, thiz) : null;
                if (globalItemActions && globalItemActions.length) {
                    itemActions = itemActions.concat(globalItemActions);
                }
            });
            contextMenu && contextMenu.removeCustomActions();
            toolbar && toolbar.removeCustomActions();
            mainToolbar && mainToolbar.removeCustomActions();
            if (!itemActions || _.isEmpty(itemActions)) {
                return;
            }
            var newStoredActions = this.addActions(itemActions);
            actionStore._emit('onActionsAdded', newStoredActions);
        },
        startup: function () {
            if (this._started) {
                return;
            }
            this.inherited(arguments);
            this._history = new History();
            if (!this.propertyStruct) {
                this.propertyStruct = {
                    currentCIView: null,
                    targetTop: null,
                    _lastItem: null
                };
            }
            this._dontSubscribeToHighligt = true;

            $(this.domNode).addClass('blockGrid');
            var clipboardData, pastedData;
            /*
             $(this.domNode).on('paste',function(e){
             e.stopPropagation();
             e.preventDefault();
             console.error('paste',e);
             // Get pasted data via clipboard API
             clipboardData = e.clipboardData || window.clipboardData;
             pastedData = clipboardData.getData('Text');

             // Do whatever with pasteddata
             alert(pastedData);
             });
             */
            var thiz = this,
                permissions = this.permissions || [];

            if (permissions) {
                var _defaultActions = DefaultActions.getDefaultActions(permissions, this);
                _defaultActions = _defaultActions.concat(this.getBlockActions(permissions));
                this.addActions(_defaultActions);
            }

            if (DefaultActions.hasAction(permissions, 'Step/Properties')) {
                this._on('selectionChanged', function (evt) {
                    var selection = evt.selection,
                        item = selection[0];

                    clearTimeout(thiz._propsTimer);
                    thiz._propsTimer = null;
                    if (item) {
                        thiz._propsTimer = setTimeout(function () {
                            var props = thiz.getPropertyStruct();
                            thiz.showProperties(item);
                        }, 500);
                    } else if (!item && evt.why == 'clear') {
                        //clear property panel when empty
                        thiz.clearPropertyPanel();
                    }
                    evt.view = thiz;
                    item && thiz.publish('ON_SELECTED_BLOCK',evt);
                });
            }

            this.addHandle('update', this.collection.on('update', function (evt) {
                var item = evt.target,
                    type = evt.type;

                if (type === 'update' && evt.property === 'value') {
                    var node = thiz.toNode(evt);
                    if (node) {
                        thiz.mark(node, 'successBlock', item);
                    }
                }
            }));

            //console.error('- subscribe ' + this.collection.id + ' group ' + this.blockGroup);
            this.addHandle('added', this.collection._on('added', function (e) {
                if (thiz._refreshTimer) {
                    return;
                }

                thiz._refreshTimer = setTimeout(function () {
                    thiz.refresh();
                    delete thiz._refreshTimer;
                    thiz._refreshTimer = null;
                    e.target && thiz.select(e.target, null, true, {
                        focus: true,
                        append: false,
                        expand: true,
                        delay: 10
                    });
                }, 10);
            }));

            this.addHandle('update', this.collection._on('update', function (evt) {
                var item = evt.target,
                    node = thiz.toNode(evt),
                    type = evt.type;
                if (type === 'update' && evt.property === 'value') {
                    thiz.mark(node, 'successBlock', item);
                }
            }));


            this.subscribe(types.EVENTS.ON_RUN_BLOCK, function (evt) {
                thiz.mark(thiz.toNode(evt), 'activeBlock', evt.target, evt.timeout);
            });
            this.subscribe(types.EVENTS.ON_RUN_BLOCK_FAILED, function (evt) {
                thiz.mark(thiz.toNode(evt), 'failedBlock', evt.target, evt.timeout);
            });
            this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS, function (evt) {
                thiz.mark(thiz.toNode(evt), 'successBlock', evt.target, evt.timeout);
            });

            this._on('selectionChanged', this.onSelectionChanged, this);
            this.publish('ON_BLOCK_GRID',{
                view:this
            });
            
            this.setRenderer(this.selectedRenderer, false);
            
        },
        /**
         * Override removeRow to prevent marking leaks
         * @param rowElement {HTMLElement}
         * @param preserveDom {boolean}
         * @returns {*|{dir, lang}}
         */
        removeRow: function (rowElement, preserveDom) {
            rowElement = rowElement.element || rowElement;
            if (rowElement && rowElement.hightlightTimer && preserveDom === false) {
                clearMarkTimer(rowElement);
            }
            return this.inherited(arguments);
        },
        mark: function (element, cssClass, block, timeout) {
            if (!element) {
                return;
            }
            if (!this.isRendered(block)) {
                return;
            }
            block = block || {};
            if (this._highlight === false) {
                return;
            }
            if (block.__ignoreChangeMark === true) {
                return;
            }
            var thiz = this;
            var debugMark = false;
            timeout = timeout == false ? false : (thiz.highlightDelay || 500);
            //already
            if (element[0]._currentHighlightClass === cssClass) {
                return;
            }

            delete element[0]._currentHighlightClass;
            element[0]._currentHighlightClass = null;
            element[0]._currentHighlightClass = cssClass;
            var scopeId = block.getScope().id;
            //debugMark &&  0 && console.log('mark ' + cssClass + ' delay ' + timeout + ' scope id');;
            element.removeClass('failedBlock successBlock activeBlock');
            element.addClass(cssClass);
            block.setStatusClass(scopeId, {
                className: cssClass,
                delay: timeout
            });
            clearMarkTimer(element[0]);
            if (timeout !== false && timeout > 0) {
                element[0].hightlightTimer = setTimeout(function () {
                    element.removeClass(cssClass);
                    element.removeClass('failedBlock successBlock activeBlock');
                    block.setStatusClass(scopeId, null);
                    delete element[0]._currentHighlightClass;
                    element[0]._currentHighlightClass = null;
                }, timeout);
            }
        },
        toNode: function (evt) {
            var item = evt.target || evt;
            if (item) {
                var row = this.row(item);
                if (row) {
                    var element = row.element;
                    if (element) {
                        return $(element);
                    }
                }
            }
            return null;
        }
    };

    var Module = declare("xblox.views.BlockGrid", Grid,IMPLEMENTATION);

    Module.IMPLEMENTATION = IMPLEMENTATION;

    return Module;

});

},
'xblox/views/Grid':function(){
/** @module xblox/views/Grid **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xblox/views/ThumbRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xide/views/_LayoutMixin',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'xgrid/KeyboardNavigation',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/utils',
    'xblox/BlockActions',
    'xgrid/Search',
    "xide/widgets/_Widget",
    "xblox/views/DnD"

], function (declare, types,
             ListRenderer, TreeRenderer, ThumbRenderer,
             Grid, MultiRenderer, _LayoutMixin,
             Defaults, Layout, Focus, KeyboardNavigation,
             OnDemandGrid, EventedMixin, utils, BlockActions, Search, _Widget,DnD) {

    var renderers = [TreeRenderer, ThumbRenderer],
        multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);


    /**
     * Block grid base class.
     * @class module:xblox/views/Grid
     */
    var GridClass = Grid.createGridClass('xblox.views.Grid',
        {
            options: utils.clone(types.DEFAULT_GRID_OPTIONS)
        },
        //features
        {

            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            COLUMN_RESIZER: types.GRID_FEATURES.COLUMN_RESIZER,
            SPLIT: {
                CLASS: _LayoutMixin
            },
            BLOCK_ACTIONS: {
                CLASS: BlockActions
            },
            DND:{
                CLASS:DnD
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },

            SEARCH: {
                CLASS: Search
            },
            WIDGET: {
                CLASS: _Widget
            }

        },
        {
            //base flip
            RENDERER: multiRenderer

        },
        {
            //args
            renderers: renderers,
            selectedRenderer: TreeRenderer
        },
        {
            GRID: OnDemandGrid,
            //EDITOR: Editor,
            LAYOUT: Layout,
            DEFAULTS: Defaults,
            RENDERER: ListRenderer,
            EVENTED: EventedMixin,
            FOCUS: Focus
        }
    );

    //static exports
    GridClass.DEFAULT_RENDERERS = renderers;
    GridClass.DEFAULT_MULTI_RENDERER = multiRenderer;
    return GridClass;
});
},
'xblox/views/ThumbRenderer':function(){
/** @module xblox/views/ThumbRenderer **/
define([
    "xdojo/declare",
    'xide/utils',
    'dojo/dom-construct',
    'xgrid/ThumbRenderer',
    "xide/$"
], function (declare, utils, domConstruct, ThumbRenderer, $) {
    /**
     * The list renderer does nothing since the xgrid/Base is already inherited from
     * dgrid/OnDemandList and its rendering as list already.
     *
     * @class module:xfile/ThumbRenderer
     * @extends module:xgrid/Renderer
     */
    return declare('xblox.views.ThumbRenderer', [ThumbRenderer], {
        thumbSize: "400",
        resizeThumb: true,
        deactivateRenderer: function () {
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding', '');
            this.isThumbGrid = false;

        },
        activateRenderer: function () {
            $(this.domNode.parentNode).addClass('metro');
            $(this.contentNode).css('padding', '8px');
            this.isThumbGrid = true;
            this.refresh();
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            if (obj.renderRow) {
                var _res = obj.renderRow.apply(this, [obj]);
                if (_res) {
                    return _res;
                }
            }
            var thiz = this,
                div = domConstruct.create('div', {
                    className: "tile widget"
                }),
                icon = obj.icon,
                no_access = obj.read === false && obj.write === false,
                isBack = obj.name == '..',
                directory = true,
                useCSS = false,
                label = '',
                imageClass = obj.icon ? (obj.icon + ' fa-5x' ) : 'fa fa-folder fa-5x',
                isImage = false;


            this._doubleWidthThumbs = true;
            var iconStyle = 'text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;opacity: 0.7';
            var contentClass = 'icon';
            if (directory) {

                if (isBack) {
                    imageClass = 'fa fa-level-up fa-5x itemFolder';
                    useCSS = true;
                } else if (!no_access) {
                    imageClass = 'fa fa-folder fa-5x itemFolder';
                    useCSS = true;
                } else {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                }

            } else {
                imageClass = 'itemIcon';
                if (no_access) {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                } else {


                    imageClass = 'fa fa-5x ' + 'fa-play';
                    useCSS = true;
                }

            }

            label = obj.name;

            imageClass = obj.icon ? (obj.icon + ' fa-5x' ) : 'fa fa-folder fa-5x';

            var folderContent = '<span style="' + iconStyle + '" class="fa fa-6x ' + imageClass + '"></span>';

            var _image = false;
            if (_image) {

                var url = this.getImageUrl(obj);
                if (url) {
                    obj.icon = url;
                } else {
                    obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                }

                imageClass = '';
                contentClass = 'image';
                folderContent = '<div style="" class="tile-content image">' +
                    '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                    '</div>';

                useCSS = true;
                isImage = true;

            }
            var label2 = label + '\n' + obj.modified;
            var html = '<div title="' + label2 + '" class="tile-content ' + contentClass + '">' +
                folderContent +
                '</div>' +

                '<div class="brand opacity">' +
                '<span class="thumbText text opacity ellipsis" style="">' +
                label +
                '</span>' +
                '</div>';
            if (isImage || this._doubleWidthThumbs) {
                $(div).addClass('double');
            }

            if (useCSS) {
                div.innerHTML = html;
                return div;
            }
            if (directory) {
                div.innerHTML = html;
            } else {
                div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
            }
            return div;
        }
    });
});
},
'xblox/BlockActions':function(){
/** @module xblox/BlockActions **/
define([
    'xdojo/declare',
    'xdojo/has',
    'xide/utils',
    'xide/types',
    'xaction/DefaultActions',
    'xide/factory',
    'dojo/promise/all',
    "dojo/Deferred",
    'xide/views/History',
    'xfile/views/FilePreview',
    'xblox/views/BlockEditView',
    'xide/views/_CIPanelDialog',
    'xide/mixins/Electron',
    'xblox/model/variables/VariableAssignmentBlock',
    'xcf/model/Variable',
    'xblox/views/DeleteDialog',
    'xide/widgets/ArgumentsWidget'

], function (declare, has, utils, types, DefaultActions, factory, all, Deferred, History, FilePreview, BlockEditView,
    _CIPanelDialog, Electron, VariableAssignmentBlock, Variable, DeleteDialog) {

    var _debugHightlighting = false;
    var _debugRun = false;
    var BLOCK_INSERT_ROOT_COMMAND = 'Step/Insert';
    var GLOBAL_CLIPBOARD = {};
    /**
     * Action implementation for block grids.
     * @class module:xblox/BlockActions
     * @extends module:xblox/views/BlockGrid
     * @augments module:xide/mixins/EventedMixin
     */
    var Module = declare('xblox.BlockActions', [Electron], {
        clipboardCopy: function () {
            this.currentCutSelection = null;
            this.currentCopySelection = this.getSelection();
            GLOBAL_CLIPBOARD = null;
            GLOBAL_CLIPBOARD = this.currentCopySelection;
            var clipboard = this.getClipboard();
            if (clipboard) {
                var blocks = this.blockScope.blocksToJson(this.currentCopySelection);
                var blocksStr = JSON.stringify(blocks, null, 2);
                clipboard.writeText(blocksStr);
            }
        },
        download: function () {
            var blocks = this.blockScope.blocksToJson();
            var blocksStr = JSON.stringify(blocks, null, 2);
            utils.download("blocks.json", blocksStr);
        },
        save: function () {
            var driver = this.userData.driver,
                ctx = this.ctx,
                fileManager = ctx.getFileManager(),
                scope = this.blockScope,
                path = driver.path.replace('.meta.json', '.xblox'),
                mount = driver.scope;

            if (scope) {
                var _onSaved = function () {};
                fileManager.setContent(mount, path, scope.toString(), _onSaved);
            }
        },
        /***********************************************************************/
        /*
         * Shared Property View
         */
        propertyStruct: {
            currentCIView: null,
            targetTop: null,
            _lastItem: null
        },
        getPropertyStruct: function () {
            return this.propertyStruct;
        },
        setPropertyStruct: function (struct) {
            this.propertyStruct = struct;
        },
        /**
         *
         * @param source
         * @param target
         * @param before
         * @param grid
         * @param targetState
         * @param insert
         * @returns {*}
         */
        onDrop: function (source, target, before, grid, targetState, insert) {
            var ctrArgs = source.ctrArgs || {};
            var proto = source.proto;
            var newBlock = null;
            var isNewItem = false;

            //prepare args
            if (source.ctrArgs) { //comes from factory
                ctrArgs.scope = ctrArgs.scope || target.scope;
                ctrArgs.group = ctrArgs.group || target.group;
                ctrArgs.parentId = ctrArgs.parentId || target.id;
                isNewItem = true;
            }

            if (isNewItem) {
                //new item at root level
                if (!target.parentId && !insert) {
                    ctrArgs.parentId = null;
                    factory.createBlock(proto, ctrArgs); //root block
                } else if (insert && target.canAdd && target.canAdd() != null) { //new item at item level
                    target.add(proto, ctrArgs, null);
                }
            } else {
                //real item move, before or after
                if (targetState === 'Moved') {
                    if (source.scope && target.scope && source.scope == target.scope) {
                        source.group = null;
                        target.scope.moveTo(source, target, before, insert);
                        return source;
                    }
                }
            }
        },
        _paste: function (items, owner, cut) {
            if (!items) {
                return;
            }
            var target = this.getSelection()[0],
                _flatten,
                source,
                thiz = this,
                scope = thiz.blockScope;

            if (!owner) {
                return; //@TODO : support
            }

            //special case when paste on nothing
            if (!target) {
                //save parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj.parentId) {
                        obj._parentOri = '' + obj.parentId;
                        obj.parentId = null;
                    }
                }
                //flatten them
                _flatten = scope.flatten(items);
                var _flat2 = _.uniq(_flatten, false, function (item) {
                    return item.id;
                });
                //clone them
                var _clones = scope.cloneBlocks2(_flat2, this.newRootItemGroup);

                scope.flatten(_clones);
                var firstItem = null;
                for (var i = 0; i < _clones.length; i++) {
                    var clone = _clones[i];
                    if (!firstItem) {
                        firstItem = clone;
                    }
                }
                //restore parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj._parentOri) { //restore
                        obj.parentId = obj._parentOri;
                        delete obj['_parentOri'];
                    }
                }
                return _clones;
            }

            var grid = owner;

            var srcScope = items[0].scope;
            var dstScope = target.scope;
            if (srcScope != dstScope) {
                return;
            }
            var insert = target.canAdd() || false;
            var parent = srcScope.getBlockById(target.parentId);
            if (!parent) {
                parent = target;
            }
            var targetState = 'Moved';
            var before = false;
            _flatten = scope.flatten(items);
            _flatten = _.uniq(_flatten, false, function (item) {
                return item.id;
            });

            items = srcScope.cloneBlocks2(_flatten);
            var newItems = [];
            for (var i = 0; i < items.length; i++) {
                source = items[i];
                //Case source=target
                if (source.parentId) {
                    newItems.push(source);
                } else {
                    source.parentId = parent.id;
                    parent.add(source);
                    var nItem = this.onDrop(source, target, before, grid, targetState, insert);
                    newItems.push(nItem);
                }
            }
            return newItems;
        },
        paste: function (items, owner, cut) {
            if (!items) {
                return;
            }
            var target = this.getSelection()[0],
                _flatten,
                source,
                thiz = this,
                scope = thiz.blockScope;

            if (!owner) {
                return; //@TODO : support
            }

            //special case when paste on nothing
            if (!target) {
                //save parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj.parentId) {
                        obj._parentOri = '' + obj.parentId;
                        obj.parentId = null;
                    }
                }
                //flatten them
                _flatten = scope.flatten(items);

                var _flat2 = _.uniq(_flatten, false, function (item) {
                    return item.id;
                });
                //clone them
                var _clones = scope.cloneBlocks2(_flat2, this.newRootItemGroup);
                scope.flatten(_clones);
                var firstItem = null;
                for (var i = 0; i < _clones.length; i++) {
                    var clone = _clones[i];
                    if (!firstItem) {
                        firstItem = clone;
                    }
                }
                //restore parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj._parentOri) { //restore
                        obj.parentId = obj._parentOri;
                        delete obj['_parentOri'];
                    }
                }
                return _clones;
            }

            var grid = owner;
            var srcScope = items[0].scope;
            var dstScope = target.scope;
            if (srcScope != dstScope) {
                return;
            }
            var insert = target.canAdd() || false;
            var parent = srcScope.getBlockById(target.parentId);
            if (!parent) {
                parent = target;
            }
            var targetState = 'Moved';
            var before = false;

            _flatten = scope.flatten(items);

            items = srcScope.cloneBlocks2(_flatten);
            var newItems = [];
            var parentTop = target.getTopRoot();

            for (var i = 0; i < items.length; i++) {
                source = items[i];
                var sourceParent = source.getTopRoot();
                //Case source=target
                if (source.parentId) {

                    if (!parentTop) {
                        var _parent = dstScope.getBlockById(source.parentId);
                        //the parent is part of the copy
                        if (_parent && _.find(items, {
                                id: _parent.id
                            })) {

                        } else {
                            source.parentId = null;
                        }
                    }
                }
                if (source.parentId && parentTop) {
                    var sourceTop = source.getTopRoot();
                    //same roots
                    if (sourceTop.id && parentTop.id && (sourceTop.id !== target.id)) {
                        source.parentId = null;
                    }
                }

                if (source.parentId) {
                    newItems.push(source);
                } else {
                    source.parentId = parent.id;
                    parent.add(source);
                    var nItem = this.onDrop(source, target, before, grid, targetState, insert);
                    newItems.push(nItem);
                }
            }
            return newItems;
        },
        /**
         *
         * @param items
         * @param owner
         * @param cut
         * @returns {*}
         */
        defaultActionResult: function (items) {
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 0,
                select: items,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        setParentBlock: function (block) {
            block = _.isString(block) ? this.collection.getSync(block) : block;
            this.set('collection', this.collection.filter({
                parentId: block.id
            }));

            var history = this.getHistory();
            history.push(block.id);
        },
        refreshCurrent: function () {
            var history = this.getHistory();
            var now = history.getNow();
            if (now) {
                this.setParentBlock(now);
            }
        },
        /**
         * Action runner
         * @param action {module xaction/ActionModel| String }
         * @returns {boolean}
         */
        runAction: function (action) {
            action = this.getAction(action);
            if (!action) {
                return null;
            }
            var renderer = this.getSelectedRenderer();
            var _rendererResult = renderer && renderer.runAction ? renderer.runAction.apply(this, [action]) : null;
            if (_rendererResult) {
                return _rendererResult;
            }
            try {

                if (action.command.indexOf(BLOCK_INSERT_ROOT_COMMAND) !== -1) {
                    return this.addItem(action.item, action);
                }

                var selection = this.getSelection(),
                    item = selection[0],
                    thiz = this,
                    ACTION = types.ACTION;

                if (action.command == ACTION.DOWNLOAD) {
                    return this.download();
                }

                function addItem(_class, group, parent) {
                    var dfd = new Deferred();
                    var cmd = null;
                    if (_class == Variable) {
                        cmd = factory.createBlock(_class, {
                            name: "No Title",
                            scope: thiz.blockScope,
                            group: group
                        });

                    } else {
                        cmd = thiz.addItem({
                            proto: _class,
                            ctrArgs: {
                                scope: thiz.blockScope,
                                group: group
                            }
                        });
                    }

                    var defaultDfdArgs = {
                        select: [cmd],
                        focus: true,
                        append: false
                    };

                    var ref = thiz.refresh();
                    ref && ref.then && ref.then(function () {
                        dfd.resolve(defaultDfdArgs);
                    });

                    if (thiz.grids) {
                        _.each(thiz.grids, function (grid) {
                            if (grid && grid.refreshRoot) {
                                grid.refreshRoot();
                                grid.refresh();
                                grid.refreshCurrent();
                            }
                        });
                    } else {
                        if (thiz.refreshRoot) {
                            thiz.refreshRoot();
                            thiz.refresh();
                            thiz.refreshCurrent();
                        }
                    }
                    return dfd;
                }

                switch (action.command) {

                    case 'File/Delete':
                        {
                            return this.deleteBlock(selection);
                        }
                    case 'File/Reload':
                        {
                            return this.refresh();
                        }
                    case 'Step/Run':
                        {
                            return this.execute(selection);
                        }
                    case 'Step/Stop':
                        {
                            return this.stop(selection);
                        }
                    case 'Step/Pause':
                        {
                            return this.pause(selection);
                        }
                    case 'Step/Back':
                        {
                            return this.goBack();
                        }
                    case 'Step/Move Left':
                        {
                            return this.reParentBlock(-1);
                        }
                    case 'Step/Move Right':
                        {
                            return this.reParentBlock(1);
                        }
                    case 'Step/Move Up':
                        {
                            this.move(-1);
                            return this._itemChanged('moved', item);
                        }
                    case 'Step/Move Down':
                        {
                            this.move(1);
                            return this._itemChanged('moved', item);
                        }
                    case 'Step/Edit':
                        {
                            return this.editBlock();
                        }
                    case 'Step/Open':
                        {
                            return this.openBlock();
                        }
                    case 'New/Variable':
                        {
                            return addItem(Variable, 'Variables');
                        }
                    case 'File/Select/None':
                        {
                            return this.deselectAll();
                        }
                    case 'Step/Enable':
                        {
                            _.each(selection, function (item) {
                                item && (item.enabled = !item.enabled);
                                item.enabled ? item.activate() : item.deactivate();
                                item.set('enabled', item.enabled);
                            });
                            this.refreshRoot();
                            this.refreshCurrent();
                            return this.defaultActionResult(selection);
                        }
                    case 'View/Show/Properties':
                        {
                            var dfd = new Deferred();
                            var right = this.getRightPanel();

                            function collapse() {

                                var splitter = right.getSplitter();
                                if (splitter) {
                                    if (splitter.isCollapsed()) {
                                        splitter.expand();
                                        return true;
                                    } else {
                                        right.collapse();
                                        return false;
                                    }
                                }
                            }

                            var wasClosed = collapse();
                            if (wasClosed) {
                                this.showProperties(item);
                            }
                            var defaultSelectArgs = {
                                focus: true,
                                append: false,
                                delay: 500,
                                select: selection
                            };
                            dfd.resolve(defaultSelectArgs);
                            this.onAfterAction('View/Show/Properties');
                            return dfd;
                        }
                    case 'Step/Help':
                        {
                            return this.showHelp(item);
                        }

                }

            } catch (e) {
                logError(e);
            }
            return this.inherited(arguments);
        },
        /**
         * Step/Move Left & Step/Move Right action
         * @param dir
         * @returns {boolean}
         */
        reParentBlock: function (dir) {
            var item = this.getSelection()[0];
            if (!item) {
                 0 && console.log('cant move, no selection or parentId', item);
                return false;
            }
            var thiz = this;
            if (dir == -1) {
                item.unparent(thiz.blockGroup);
            } else {
                item.reparent();
            }


            thiz.deselectAll();
            thiz.refreshRoot();
            this.refreshCurrent();
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 100,
                select: item,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        /**
         * Step/Move Down & Step/Move Up action
         * @param dir
         */
        move: function (dir) {
            var items = this.getSelection();
            if (!items || !items.length /*|| !item.parentId*/ ) {
                 0 && console.log('cant move, no selection or parentId', items);
                return;
            }
            var thiz = this;
            _.each(items, function (item) {
                item.move(dir);
            }.bind(this));

            thiz.refresh();
            this.select(items, null, true, {
                focus: true,
                delay: 10
            }).then(function () {
                thiz.refreshActions();
            });
        },
        /**
         * Clipboard/Paste action
         */
        clipboardPaste: function () {
            var selection = this.currentCopySelection || this.currentCutSelection || GLOBAL_CLIPBOARD;
            var clipboard = this.getClipboard();

            if (clipboard && (!selection || !selection[0])) {
                var eClipboard = clipboard.readText();
                if (eClipboard) {
                    try {
                        var blocks = JSON.parse(eClipboard);
                        if (blocks && blocks.length) {
                            var tmpScope = this.blockScope.owner.getScope(utils.createUUID(), null, false);
                            var newBlocks = tmpScope.blocksFromJson(blocks); //add it to our scope
                            if (newBlocks && newBlocks.length) {
                                selection = newBlocks;
                            }
                        }

                    } catch (e) {
                        logError(e, 'clipboard extraction from electron failed');
                    }
                }
            }
            if (!selection || !selection[0]) {
                return;
            }

            var newItems = this.paste(selection, this, this.currentCutSelection != null);
            var dfd = new Deferred();
            if (newItems) {
                this.refreshRoot();
                this.refreshCurrent();
                var defaultSelectArgs = {
                    focus: true,
                    append: false,
                    delay: 2,
                    select: newItems,
                    expand: true
                };
                dfd.resolve(defaultSelectArgs);
            }
            return dfd;
        },
        /**
         * File/Delete Action
         * @param items
         */
        deleteBlock: function (items) {
            var self = this;
            var dfd = new Deferred();

            var item = items[0];
            var parent = item.getParent();
            var _prev = item.next(null, -1) || item.next(null, 1) || parent;

            function main() {
                function _delete(item) {
                    if (!item) {
                        return false;
                    }

                    //try individual item remove function
                    if (item.remove) {
                        item.remove();
                    }

                    if (item.destroy) {
                        item.destroy();
                    }

                    //this should be redundant as item.remove should do the same too
                    var store = this.getStore(item);
                    if (store) {
                        var _itm = store.getSync(item[store.idProperty]);
                        store.removeSync(item[store.idProperty]);
                    }

                    item._destroyed = true;
                    item.scope._emit(types.EVENTS.ON_ITEM_REMOVED, {
                        item: item
                    });

                    return true;
                }
                _.each(items, _delete, self);
                this.refreshRoot();
                this.refreshCurrent();
            }
            var title = 'Delete ' + items.length + ' ' + 'items ?';
            var dlg = new DeleteDialog({
                ctx: self.ctx,
                notificationMessage: null,
                title: title,
                type: types.DIALOG_TYPE.DANGER,
                onCancel: function () {
                    dfd.resolve({
                        select: items,
                        focus: true,
                        append: false
                    });
                },
                onOk: function () {
                    main.bind(self)();
                    dfd.resolve({
                        focus: true,
                        append: false,
                        select: _prev
                    });
                }
            });
            dlg.show();
            return dfd;
        },
        /**
         * File/Edit action
         * @param item
         * @param changedCB
         * @param select
         */
        editBlock2: function (item, changedCB, select) {
            if (!item) {
                return;
            }
            if (!item.canEdit()) {
                return;
            }
            var title = 'Edit ',
                thiz = this;

            title += item.name;
            var cis = item.getFields();
            var cisView = null;
            var self = this;

            function ok(changedCIS) {
                _.each(changedCIS, function (evt) {
                    thiz.onCIChanged && thiz.onCIChanged(evt.ci, item, evt.oldValue, evt.newValue, evt.ci.dst, cis, evt.props);
                });
                self.showProperties(item, true);
            }

            var panel = new _CIPanelDialog({
                title: title,
                containerClass: 'CIDialog',
                onOk: function (changedCIS) {
                    ok(changedCIS);
                    this.headDfd.resolve(changedCIS);
                },
                onCancel: function (changedCIS) {
                    this.headDfd.resolve(changedCIS);
                },
                cis: cis,
                CIViewClass: BlockEditView,
                CIViewOptions: {
                    delegate: this,
                    resizeToParent: true,
                    ciSort: true,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Send': 2,
                            'Advanced': 4,
                            'Description': 5
                        }
                    },
                    cis: cis
                }
            });
            var dfd = panel.show();
            return dfd;
        },
        _getText: function (url) {
            if (has('debug')) {
                url += '?time=' + new Date().getTime();
            }
            try {
                var result;
                dojo.xhrGet({
                    url: url,
                    sync: true,
                    handleAs: 'text',
                    cache: false,
                    load: function (text) {
                        result = text;
                    }
                });
                return (result || 'No help avaiable');
            } catch (e) {
                return false;
            }
        },
        _helpCache: {},
        getHelp: function (item) {

            var mid = utils.replaceAll('.', '/', item.declaredClass);
            //special treat for xcf
            mid = mid.replace('xcf', 'xblox');
            var rootNS = 'xblox/model/';
            mid = 'xblox/docs/' + mid.replace(rootNS, '');

            var cached = this._helpCache[mid];
            if (cached === false) {
                return false;
            } else if (_.isString(cached)) {
                return cached;
            }

            var helpText = this._getText(require.toUrl(mid) + '.md');
            if (!helpText) {
                return false;
            }
            //https://github.com/showdownjs/showdown
            if (typeof showdown !== 'undefined') {
                var converter = new showdown.Converter();
                helpText = converter.makeHtml(helpText);
                 0 && console.log('load from ' + require.toUrl(mid) + '.md');
            } else {
                 0 && console.warn('have no `showdown`, proceed without');
            }
            this._helpCache[mid] = helpText;
            return helpText;
        },
        showHelp: function (item, parent) {
            var row = this.row(item),
                el = row.element,
                self = this;

            function render(where, item) {
                var help = self.getHelp(item);
                if (help) {
                    where.html(help);
                }
                where.addClass('xbloxHelp');
            }

            if (this._preview) {
                this._preview.open();
                render(this._preview.preview, item);
                return;
            }
            var _prev = new FilePreview({
                node: $(el),
                parent: $(el),
                ctx: self.ctx,
                container: parent ? parent.containerNode : null,
                title: 'Help'
            });
            parent ? _prev.buildRenderingEmbedded() : _prev.buildRendering();
            _prev.init();
            _prev.exec();
            this._preview = _prev;
            this._preview.handler = this;
            _prev.onOpened = function () {
                render(_prev.preview, item);
            };
            this.add(_prev);
            self._on('selectionChanged', function (e) {
                if (self._preview.getstate() == 0) {
                    return;
                }
                var _item = self.getSelectedItem();
                if (_item) {
                    _prev.item = _item;
                    render(self._preview.preview, _item);
                }
            });
        },
        getHistory: function () {
            if (!this._history) {
                this._history = new History();
            }

            return this._history;
        },
        goBack: function () {
            var store = this.collection,
                history = this.getHistory(),
                now = null,
                prev = history.getNow(),
                self = this,
                head = new Deferred(),
                select = true;
            history.pop();
            now = history.getNow();

            function resolve(item) {
                head.resolve({
                    select: select !== false ? (item ? item : self.getRows()[0]) : null,
                    focus: true,
                    append: false,
                    delay: 250
                });
            }
            if (now && store.getSync(now)) {
                this.set('collection', this.collection.filter({
                    parentId: now
                }));
                resolve();
            } else {
                store.reset();
                this.refreshRoot();
                resolve(store.getSync(prev));
            }
            return head;

        },
        openBlock: function (item, changedCB, select) {
            var selection = this.getSelection();
            item = selection[0] || item;
            if (!item) {
                return;
            }
            var head = new Deferred();
            var self = this;
            var isBack = false;
            this.setParentBlock(item);
            select = true;
            var rows = self.getRows()[0];
            if (!rows) {
                setTimeout(function () {
                    self.contentNode.focus();
                }, 10);
            }
            head.resolve({
                select: select !== false ? (isBack ? item : rows) : null,
                focus: true,
                append: false,
                delay: 1
            });

            return head;
        },
        editBlock: function (item, changedCB, select) {
            var selection = this.getSelection();
            item = selection[0] || item;
            if (!item) {
                return;
            }
            var head = new Deferred();
            var children = item.getChildren(),
                self = this;

            var dlgDfd = this.editBlock2(item, null);
            if (!dlgDfd) {
                head.resolve();
                return;
            }
            dlgDfd.then(function () {
                head.resolve({
                    select: [item],
                    focus: true,
                    append: false,
                    delay: 1
                });
            });

            return head;
        },
        execute: function (_blocks) {
            var thiz = this,
                dfds = [], //all Deferreds of selected blocks to run
                handles = [],
                EVENTS = types.EVENTS,
                blocks = _.isArray(_blocks) ? _blocks : [_blocks];


            function run(block) {

                var _runHandle = block._on(EVENTS.ON_RUN_BLOCK, function (evt) {
                        _debugHightlighting && console.error('active');
                        thiz.mark(thiz.toNode(evt), 'activeBlock', block);
                    }),

                    //event handle "Success"
                    _successHandle = block._on(EVENTS.ON_RUN_BLOCK_SUCCESS, function (evt) {
                        _debugHightlighting &&  0 && console.log('marke success', evt);
                        thiz.mark(thiz.toNode(evt), 'successBlock', block);

                    }),

                    //event handle "Error"
                    _errHandle = block._on(EVENTS.ON_RUN_BLOCK_FAILED, function (evt) {
                        _debugHightlighting && console.error('failed', evt);
                        thiz.mark(thiz.toNode(evt), 'failedBlock', block);
                    });

                _debugRun && console.error('run block ');


                if (!block || !block.scope) {
                    console.error('have no scope');
                    return;
                }

                try {
                    var blockDfd = block.scope.solveBlock(block, {
                        highlight: true,
                        force: true,
                        listener: thiz
                    });

                    dfds.push(blockDfd);

                } catch (e) {
                    logError(e, 'excecuting block -  ' + block.name + ' failed! : ');
                }
                handles = handles.concat([_runHandle, _errHandle, _successHandle]);
                return true;
            }

            function _patch(block) {

                block.runFrom = function (_blocks, index, settings) {

                    var thiz = this,
                        blocks = _blocks || this.items,
                        allDfds = [];

                    var onFinishBlock = function (block, results) {
                        block._lastResult = block._lastResult || results;
                        thiz._currentIndex++;
                        thiz.runFrom(blocks, thiz._currentIndex, settings);
                    };

                    var wireBlock = function (block) {
                        block._deferredObject.then(function (results) {
                             0 && console.log('----def block finish');
                            onFinishBlock(block, results);
                        });
                    };

                    if (blocks.length) {

                        for (var n = index; n < blocks.length; n++) {
                            var block = blocks[n];
                             0 && console.log('run child \n' + block.method);

                            _patch(block);

                            if (block.deferred === true) {
                                block._deferredObject = new Deferred();
                                this._currentIndex = n;
                                wireBlock(block);
                                //this.addToEnd(this._return, block.solve(this.scope, settings));
                                var blockDfd = block.solve(this.scope, settings);
                                allDfds.push(blockDfd);
                                break;
                            } else {
                                //this.addToEnd(this._return, block.solve(this.scope, settings));

                                var blockDfd = block.solve(this.scope, settings);
                                allDfds.push(blockDfd);
                            }

                        }

                    } else {
                        this.onSuccess(this, settings);
                    }

                    return allDfds;
                };

                block.solve = function (scope, settings, run, error) {

                    this._currentIndex = 0;
                    this._return = [];

                    var _script = '' + this._get('method');

                    var thiz = this,
                        ctx = this.getContext(),
                        items = this[this._getContainer()],

                        //outer,head dfd
                        dfd = new Deferred(),
                        listener = settings.listener,
                        isDfd = thiz.deferred;

                    //moved to Contains#onRunThis
                    if (listener) {
                        listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                    }

                    //function when a block did run successfully,
                    // moved to Contains#onDidRunItem
                    function _finish(dfd, result, event) {

                        if (listener) {
                            listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                        }
                        dfd.resolve(result);


                    }

                    //function when a block did run successfully
                    function _error(result) {
                        dfd.reject(result);
                        if (listener) {
                            listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                        }
                    }


                    //moved to Contains#onDidRunThis
                    function _headDone(result) {


                        //more blocks?
                        if (items.length) {
                            var subDfds = thiz.runFrom(items, 0, settings);

                            all(subDfds).then(function (what) {
                                 0 && console.log('all solved!', what);
                                _finish(dfd, result);
                            }, function (err) {
                                console.error('error in chain', err);
                                _finish(dfd, err);
                            });

                        } else {
                            _finish(dfd, result);
                        }
                    }


                    if (_script && _script.length) {

                        var runScript = function () {

                            var _function = new Function("{" + _script + "}");
                            var _args = thiz.getArgs() || [];
                            try {

                                if (isDfd) {
                                    ctx.resolve = function (result) {
                                        if (thiz._deferredObject) {
                                            thiz._deferredObject.resolve();
                                        }
                                        _headDone(result);
                                    };
                                }
                                var _parsed = _function.apply(ctx, _args || {});
                                thiz._lastResult = _parsed;
                                if (run) {
                                    run('Expression ' + _script + ' evaluates to ' + _parsed);
                                }
                                if (!isDfd) {
                                    thiz.onDidRunThis(dfd, _parsed, items, settings);
                                }
                            } catch (e) {
                                e = e || {};
                                _error(e);
                                if (error) {
                                    error('invalid expression : \n' + _script + ': ' + e);
                                }
                            }
                        };

                        if (scope.global) {
                            (function () {
                                window = scope.global;
                                var _args = thiz.getArgs() || [];
                                try {
                                    var _parsed = null;
                                    if (!ctx.runExpression) {
                                        var _function = new Function("{" + _script + "}").bind(this);
                                        _parsed = _function.apply(ctx, _args || {});
                                    } else {
                                        _parsed = ctx.runExpression(_script, null, _args);
                                    }

                                    thiz._lastResult = _parsed;

                                    if (run) {
                                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                                    }
                                    if (_parsed !== 'false' && _parsed !== false) {
                                        thiz.onSuccess(thiz, settings);
                                    } else {
                                        thiz.onFailed(thiz, settings);
                                        return [];
                                    }
                                } catch (e) {
                                    thiz._lastResult = null;
                                    if (error) {
                                        error('invalid expression : \n' + _script + ': ' + e);
                                    }
                                    thiz.onFailed(thiz, settings);
                                    return [];
                                }
                            }).call(scope.global);
                        } else {
                            runScript();
                        }
                    } else {
                        console.error('have no script');
                    }
                    return dfd;
                };
            }

            _.each(blocks, run);
            all(dfds).then(function () {
                _debugRun &&  0 && console.log('did run all selected blocks!', thiz);
                _.invoke(handles, 'remove');
            });
        },
        stop: function (_blocks) {
            var blocks = _.isArray(_blocks) ? _blocks : [_blocks];

            function stop(block) {
                if (!block || !block.scope) {
                    console.error('have no scope');
                    return;
                }
                try {
                    block.stop(false);
                } catch (e) {
                    logError(e, 'stop block -  ' + block.name + ' failed! : ');
                }
                return true;
            }

            _.each(blocks, stop);
        },
        pause: function (_blocks) {
            var blocks = _.isArray(_blocks) ? _blocks : [_blocks];

            function stop(block) {
                if (!block || !block.scope) {
                    console.error('have no scope');
                    return;
                }
                try {
                    block.pause(true);
                } catch (e) {
                    logError(e, 'pause block -  ' + block.name + ' failed! : ');
                }
                return true;
            }

            _.each(blocks, stop);
        },
        /**
         *
         * @param item
         * @returns {Array}
         */
        getAddActions: function (item) {
            var thiz = this;
            var items = this.inherited(arguments) || [];
            item = item || {};
            items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
            //tell everyone
            thiz.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this.owner,
                item: item,
                group: this.blockGroup,
                scope: this.blockScope
            });

            thiz._emit(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this.owner
            });
            return items;
        },
        getBlockActions: function (permissions) {
            var result = this.inherited(arguments) || [],
                ACTION = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                thiz = this,
                actionStore = thiz.getActionStore();

            var defaultMixin = {
                addPermission: true
            };

            result.push(thiz.createAction({
                label: 'Save',
                command: 'File/Save',
                icon: 'fa-save',
                tab: 'Home',
                group: 'File',
                keycombo: ['ctrl s'],
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Save As',
                command: 'File/Save As',
                icon: 'fa-save',
                tab: 'Home',
                group: 'File',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Open',
                command: 'Step/Open',
                icon: 'fa-folder-open',
                keycombo: ['ctrl enter'],
                tab: 'Home',
                group: 'Navigation',
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Go up',
                command: 'Step/Back',
                icon: ACTION_ICON.GO_UP,
                tab: 'Home',
                group: 'Navigation',
                keycombo: ['backspace'],
                mixin: {
                    quick: true
                }
            }));

            function _selection() {
                var selection = thiz.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                var item = selection[0];
                if (!item) {
                    return null;
                }
                return selection;
            }

            function canParent() {
                var selection = thiz.getSelection();
                if (!selection) {
                    return true;
                }
                var item = selection[0];
                if (!item) {
                    return true;
                }
                if (this.command === 'Step/Move Left') {
                    return !item.getParent();
                } else {
                    return item.getParent();
                }
            }

            function isItem() {
                var selection = _selection();
                if (!selection) {
                    return true;
                }
                return false;
            }

            function canMove() {
                var selection = _selection();
                if (!selection) {
                    return true;
                }
                var item = selection[0];
                if (this.command === 'Step/Move Up') {
                    return !item.canMove(null, -1);
                } else {
                    return !item.canMove(null, 1);
                }
            }

            result.push(thiz.createAction({
                label: 'Run',
                command: 'Step/Run',
                icon: 'text-success fa-play',
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Stop',
                command: 'Step/Stop',
                icon: 'text-warning fa-stop',
                tab: 'Home',
                group: 'Step',
                keycombo: ['s'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Pause',
                command: 'Step/Pause',
                icon: 'text-info fa-pause',
                tab: 'Home',
                group: 'Step',
                keycombo: ['p'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            //////////////////////////////////////////////////////////////////
            //
            //  Step - Move
            //

            result.push(thiz.createAction({
                label: 'MoveUp',
                command: 'Step/Move Up',
                icon: 'text-info fa-arrow-up',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt up'],
                shouldDisable: canMove,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'MoveDown',
                command: 'Step/Move Down',
                icon: 'text-info fa-arrow-down',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt down'],
                shouldDisable: canMove,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'MoveLeft',
                command: 'Step/Move Left',
                icon: 'text-info fa-arrow-left',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt left'],
                shouldDisable: canMove,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'MoveRight',
                command: 'Step/Move Right',
                icon: 'text-info fa-arrow-right',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt right'],
                shouldDisable: canParent,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Help',
                command: 'Step/Help',
                icon: 'fa-question',
                tab: 'Home',
                group: 'Step',
                keycombo: ['h'],
                shouldDisable: isItem,
                mixin: defaultMixin
            }));


            result.push(thiz.createAction({
                label: 'On',
                command: 'Step/Enable',
                icon: 'fa-toggle-off',
                tab: 'Home',
                group: 'Step',
                keycombo: ['alt o'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    actionType: 'multiToggle',
                    quick: true
                }, defaultMixin),
                onCreate: function (action) {
                    //action.set('value',true);
                    action.value = true;
                    action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                        group: 'Common'
                    });
                },
                onChange: function (property, value) {
                    var item = thiz.getSelectedItem();
                    if (item) {
                        item.set('enabled', value);
                    }
                    this.value = value;
                    thiz.refreshActions();
                    thiz.refresh(true);
                    thiz.select(item);
                }
            }));

            result.push(this.createAction({
                label: 'Variable',
                command: 'New/Variable',
                icon: 'fa-code',
                tab: 'Home',
                group: 'Insert',
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(this.createAction({
                label: 'Edit',
                command: 'Step/Edit',
                icon: ACTION_ICON.EDIT,
                keycombo: ['f4', 'enter', 'dblclick'],
                tab: 'Home',
                group: 'Step',
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Properties',
                command: 'View/Show/Properties',
                icon: 'fa-gears',
                tab: 'Home',
                group: 'Show',
                keycombo: ['alt p'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    actionType: 'multiToggle',
                    value: true
                }, defaultMixin),
                onCreate: function (action) {
                    var right = thiz.getRightPanel();
                    right.getSplitter();
                    //action.set('value',true);
                    action.value = true;
                },
                onChange: function (property, value) {
                    var right = thiz.getRightPanel();

                    function collapse() {
                        var splitter = right.getSplitter();
                        if (splitter) {
                            if (splitter.isCollapsed()) {
                                splitter.expand();
                                return true;
                            } else {
                                right.collapse();
                                return false;
                            }
                        }
                    }

                    var wasClosed = collapse();
                    var result = true;
                    if (wasClosed) {
                        thiz.showProperties(null);
                    }
                    thiz.onAfterAction('View/Show/Properties');
                    return result;
                }
            }));


            var newBlockActions = this.getAddActions();
            var levelName = '';

            var rootAction = BLOCK_INSERT_ROOT_COMMAND;
            result.push(thiz.createAction({
                label: 'Add Block',
                command: '' + rootAction,
                icon: 'fa-plus',
                tab: 'Home',
                group: 'Insert',
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            function addItems(commandPrefix, items) {

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];

                    levelName = item.name;

                    var path = commandPrefix + '/' + levelName;
                    var isContainer = !_.isEmpty(item.items);

                    result.push(thiz.createAction({
                        label: levelName,
                        command: path,
                        icon: item.iconClass,
                        tab: 'Home',
                        group: 'Insert',
                        mixin: utils.mixin({
                            item: item,
                            quick: true
                        }, defaultMixin)
                    }));
                    if (isContainer) {
                        addItems(path, item.items);
                    }
                }
            }

            addItems(rootAction, newBlockActions);
            ////////////////////////////////////////////////////////
            //
            //  Variables
            //
            var setVariableRoot = rootAction + '/Set Variable';

            function _getSetVariableActions(permissions, owner) {
                var variables = thiz.blockScope.getVariables();
                var result = [];
                for (var i = 0; i < variables.length; i++) {
                    var variable = variables[i];
                    result.push(thiz.createAction({
                        label: variable.name,
                        command: '' + setVariableRoot + '/' + variable.name,
                        icon: 'fa-paper-plane',
                        tab: 'Home',
                        group: 'Step',
                        mixin: utils.mixin({
                            item: variable,
                            proto: VariableAssignmentBlock,
                            custom: true,
                            ctrArgs: {
                                variable: variable.id,
                                variableId: variable.id,
                                scope: thiz.blockScope,
                                value: '0'
                            }
                        }, defaultMixin)
                    }));
                }
                return result;
            }

            result.push(thiz.createAction({
                label: 'Set Variable',
                command: '' + setVariableRoot,
                icon: 'fa-paper-plane',
                tab: 'Home',
                group: 'Step',
                mixin: utils.mixin({
                    global: true,
                    quick: true,
                    getActions: _getSetVariableActions
                }, defaultMixin)
            }));
            result.push(thiz.createAction({
                label: 'None',
                command: '' + setVariableRoot + '/None',
                icon: 'fa-paper-plane',
                tab: 'Home',
                group: 'Step',
                mixin: utils.mixin({
                    proto: VariableAssignmentBlock,
                    item: VariableAssignmentBlock,
                    quick: true,
                    ctrArgs: {
                        scope: this.blockScope,
                        value: '0'
                    }
                }, defaultMixin)
            }));

            this._emit('onAddActions', {
                actions: result,
                permissions: permissions,
                store: actionStore
            });
            return result;
        }

    });
    Module.BLOCK_INSERT_ROOT_COMMAND = BLOCK_INSERT_ROOT_COMMAND;
    return Module;
});
},
'xblox/views/BlockEditView':function(){
define([
    'dcl/dcl',
    'xide/views/CIView'
],function (dcl,CIView){
    return dcl([CIView],{
        declaredClass:"xblox.views.BlockEditView",
        onSave:function(ci,value){
            if(this.delegate && this.delegate.onSave){
                this.delegate.onSave(ci,value);
            }
        }
    });
});
},
'xblox/views/DeleteDialog':function(){
/** @module xfile/views/FileOperationDialog **/
define([
    "dcl/dcl",
    'xide/types',
    "xide/views/_Dialog"
], function (dcl, types, _Dialog) {
    var Module = dcl(_Dialog, {
        title: '',
        type: types.DIALOG_TYPE.INFO,
        size: types.DIALOG_SIZE.SIZE_SMALL,
        bodyCSS: {},
        failedText: ' Failed!',
        successText: ': Success!',
        showSpinner: true,
        spinner: '  <span class="fa-spinner fa-spin"/>',
        notificationMessage: null,
        doOk: function (dfd) {
            this.onBeforeOk && this.onBeforeOk();
            var msg = this.showMessage(),
                thiz = this;
            dfd.then(function (result) {
                thiz._onSuccess(result);
            }, function (err) {
                thiz._onError();
            });
        },
        _onSuccess: function (title, suffix, message) {
            title = title || this.title;
            message = message || this.notificationMessage;
            message && message.update({
                message: title + this.successText + (suffix ? '<br/>' + suffix : ''),
                type: 'info',
                actions: false,
                duration: 1500
            });
            this.onSuccess && this.onSuccess();
        },
        _onError: function (title, suffix, message) {
            title = title || this.title;
            message = message || this.notificationMessage;
            message && message.update({
                message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                type: 'error',
                actions: false,
                duration: 15000
            });
            this.onError && this.onError(suffix);
        },
        onOk: function () {
            var msg = this.showMessage(),
                thiz = this;
            this.doOk(this.getOkDfd());
        },
        showMessage: function (title) {
            if (this.notificationMessage) {
                return this.notificationMessage;
            }
            title = title || this.title;
            var msg = this.ctx.getNotificationManager().postMessage({
                message: title + (this.showSpinner ? this.spinner : ''),
                type: 'info',
                showCloseButton: true,
                duration: 4500
            });
            this.notificationMessage = msg;
            return msg;
        }
    });
    return Module;
});
},
'xblox/views/DnD':function(){
define([
    'dojo/_base/declare',
    'dojo/when',
    'xgrid/DnD'
], function (declare, when, DnD) {

    var Source = DnD.GridSource;
    /**
     * @class module:xblox/views/BlockGridDnDSource
     * @extends module:xgrid/DnD/GridDnDSource
     */
    var BlockGridDnDSource = declare(DnD.GridSource, {
        declaredClass: 'xblox/views/BlockGridDnDSource',
        _lastTargetId: null,
        _lastTargetCrossCounter: 0,
        /**
         * Assigns a class to the current target anchor based on "before" status
         * @param before {boolean} insert before, if true, after otherwise
         * @param center {boolean}
         * @param canDrop {boolean}
         * @private
         */
        _markTargetAnchor: function (before, center, canDrop) {
            if (this.current == this.targetAnchor && this.before === before && this.center === center) {
                return;
            }
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, this.before ? "Before" : "After");
                this._removeItemClass(this.targetAnchor, "Disallow");
                this._removeItemClass(this.targetAnchor, "Center");
            }
            //track crossing count for trees, open it if crossed
            /*
             if(!this.current){
             this._lastTargetId=null;
             }else if(this.current.id!==this._lastTargetId){
             this._lastTargetCrossCounter=0;
             }else if(this.current.id==this._lastTargetId && center){
             this._lastTargetCrossCounter++;
             }
             */

            this.targetAnchor = this.current;
            this.targetBox = null;
            this.before = before;
            this.center = center;
            this._before = before;
            this._center = center;
            if (this.targetAnchor) {
                if (center) {
                    this._removeItemClass(this.targetAnchor, "Before");
                    this._removeItemClass(this.targetAnchor, "After");
                    this._addItemClass(this.targetAnchor, 'Center');
                }!center && this._addItemClass(this.targetAnchor, this.before ? "Before" : "After");
                center && canDrop === false && this._addItemClass(this.targetAnchor, "Disallow");

                /*
                 if(this._lastTargetCrossCounter>1 && this.grid.expand){
                  0 && console.log('open');
                 }
                 this._lastTargetId = this.targetAnchor.id;
                 */

            }
        },
        /**
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:xide/data/Model}
         */
        onDropInternal: function (nodes, copy, targetItem) {
            var grid = this.grid,
                store = grid.collection,
                targetSource = this,
                anchor = targetSource._targetAnchor,
                targetRow,
                nodeRow;

            if (anchor) { // (falsy if drop occurred in empty space after rows)
                targetRow = this._center ? anchor : this.before ? anchor.previousSibling : anchor.nextSibling;
            }
            // Don't bother continuing if the drop is really not moving anything.
            // (Don't need to worry about edge first/last cases since dropping
            // directly on self doesn't fire onDrop, but we do have to worry about
            // dropping last node into empty space beyond rendered rows.)
            nodeRow = grid.row(nodes[0]);
            if (!copy && (targetRow === nodes[0] || (!targetItem && nodeRow && grid.down(nodeRow).element === nodes[0]))) {
                return;
            }
            var rows = grid.__dndNodesToModel(nodes);
            var _target = grid._sourceToModel(targetSource, grid);
            var DND_PARAMS = {
                before: this._before,
                center: this._center,
                targetGrid: grid
            };

            if (this._center) {
                grid.__onDrop(rows, _target, grid, DND_PARAMS, copy);
            }
            nodes.forEach(function (node) {
                when(targetSource.getObject(node), function (object) {
                    var id = store.getIdentity(object);
                    // For copy DnD operations, copy object, if supported by store;
                    // otherwise settle for put anyway.
                    // (put will relocate an existing item with the same id, i.e. move).
                    grid._trackError(function () {
                        return store[copy && store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {

                            // Self-drops won't cause the dgrid-select handler to re-fire,
                            // so update the cached node manually
                            if (targetSource._selectedNodes[id]) {
                                targetSource._selectedNodes[id] = grid.row(id).element;
                            }!this.center && grid.__onDrop(rows, _target, grid, DND_PARAMS, copy);
                        });
                    });
                });
            });
        },
        /**
         * @param sourceSource {module:dojo/dnd/Source}
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:dojo/dnd/Target}
         */
        onDropExternal: function (sourceSource, nodes, copy, targetItem) {
            // Note: this default implementation expects that two grids do not
            // share the same store.  There may be more ideal implementations in the
            // case of two grids using the same store (perhaps differentiated by
            // query), dragging to each other.
            var grid = this.grid,
                store = this.grid.collection,
                sourceGrid = sourceSource.grid,
                targetSource = this;

            function done(grid, object) {
                grid.refresh();
                return grid.select(object, null, true, {
                    focus: true,
                    append: false,
                    expand: true,
                    delay: 2
                }, 'mouse');
            }

            nodes.forEach(function (node, i) {
                when(sourceSource.getObject(node), function (object) {
                    var DND_PARAMS = {
                        before: targetSource._before,
                        center: targetSource._center,
                        targetGrid: grid
                    };
                    if (object.toDropObject) {
                        object = object.toDropObject(object, DND_PARAMS.center ? sourceGrid._sourceToModel(targetSource) : targetItem, DND_PARAMS, copy);
                        if (DND_PARAMS.center) {
                            return done(grid, object);
                        } else {
                            grid.refreshRoot();
                            setTimeout(()=>{
                                done(grid, object);
                            },100);                            
                        }
                    }
                    // Copy object, if supported by store; otherwise settle for put
                    // (put will relocate an existing item with the same id).
                    // Note that we use store.copy if available even for non-copy dnd:
                    // since this coming from another dnd source, always behave as if
                    // it is a new store item if possible, rather than replacing existing.
                    grid._trackError(function () {
                        return store[store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {
                            if (!copy) {
                                if (sourceGrid) {
                                    // Remove original in the case of inter-grid move.
                                    // (Also ensure dnd source is cleaned up properly)
                                    var id = sourceGrid.collection.getIdentity(object);
                                    !i && sourceSource.selectNone(); // Deselect all, one time
                                    sourceSource.delItem(node.id);
                                    done(grid, object);
                                    return sourceGrid.collection.remove(id);
                                } else {
                                    sourceSource.deleteSelectedNodes();
                                }
                            }
                        });
                    });
                });
            });
        }
    });

    /**
     * @class module:xblox/views/SharedDndGridSource
     * @extends module:xblox/views/BlockGridDnDSource
     * @extends module:xblox/views/DojoDndMixin
     */
    var SharedDndGridSource = declare([BlockGridDnDSource], {
        autoSync: true,
        skipForm: true,
        selfAccept: false,
        onDndDrop: function (source, nodes, copy, target) {
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, "Hover");
            }
            if (this == target) {
                // this one is for us => move nodes!
                this.onDrop(source, nodes, copy, target);
                this.grid.refresh();
            }
            this.onDndCancel();
        }
    });


    var dndParams = {
        allowNested: true, // also pick up indirect children w/ dojoDndItem class
        checkAcceptance: function (source, nodes) {
            return true; //source !== this; // Don't self-accept.
        },
        isSource: true
    };
    /**
     * @class module:xblox/views/DnD
     * @extends module:xgrid/DnD/
     */
    var Module = declare("xblox.widgets.DojoDndMixin", DnD, {
        dndParams: dndParams,
        dndConstructor: SharedDndGridSource, // use extension defined above
        dropEvent: "/dnd/drop",
        dragEvent: "/dnd/start",
        overEvent: "/dnd/source/over",
        /**
         * Final
         * @param sources {module:xblox/model/Block[]}
         * @param target {module:xblox/model/Block}
         * @param sourceGrid {module:xgrid/Base| module:dgrid/List}
         * @param params {object}
         * @param params.SAME_STORE {object}
         * @param copy {boolean}
         * @private
         */
        __onDrop: function (sources, target, sourceGrid, params, copy) {
            var into = params.center === true;
            _.each(sources, function (source) {
                if (source.toDropObject) {
                    source = source.toDropObject(source, target, params, copy);
                }
                //happens when dragged on nothing
                if (source == target) {
                    return;
                }
                if (into) {
                    if (target.canAdd(source) !== false) {
                        target.scope.moveTo(source, target, params.before, into);
                    }
                } else {
                    var sourceParent = source.getParent();
                    if (sourceParent) {
                        var targetParent = target.getParent();
                        if (!targetParent) {
                            //move to root - end
                            source.group = this.blockGroup;
                            sourceParent.removeBlock(source, false);
                            source._store.putSync(source);
                        } else {

                            //we dropped at the end within the same tree
                            if (targetParent == source.getParent()) {
                                target.scope.moveTo(source, target, params.before, into);
                            }
                        }
                    }
                }
                source.refresh();
            }, this);


            this.set('collection', this.collection.filter({
                group: this.blockGroup
            }));

            this.select(sources[0], null, true, {
                focus: true,
                append: false,
                expand: true,
                delay: 2
            }, 'mouse');
        }
        /*,
                startup: function () {
                    if (this._started) {
                        return;
                    }

                    this.dropEvent && this.subscribe(this.dropEvent, function (source, nodes, copy, target) {
                        return;
                        if (source.grid == this) {
                            var rows = this.__dndNodesToModel(nodes);
                            target = target || {};
                            var _target = this._sourceToModel(target, target.grid);

                            if (!_target || _.isEmpty(rows)) {
                                return;
                            }
                            var DND_PARAMS = {
                                SAME_STORE: rows[0]._store == _target._store,
                                before: target._before,
                                center: target._center
                            };
                            if (rows && _target) {
                                this.__onDrop(rows, _target, source.grid, DND_PARAMS, copy);
                            }
                        }
                    });
                    return this.inherited(arguments);
                }*/
    });

    Module.BlockGridSource = BlockGridDnDSource;

    return Module;
});
},
'xblox/manager/BlockManagerUI':function(){
/** module xblox/manager/BlockManagerUI **/
define([
    'dcl/dcl',
    "xide/manager/BeanManager"
], function (dcl, BeanManager) {
    /**
     * @mixin module:xblox/manager/BlockManagerUI
     * @extends {module:xide/manager/BeanManager}
     */
    return dcl(BeanManager, {
        declaredClass: "xblox/manager/BlockManagerUI",
        init: function () {}
    });
});
},
'xblox/views/BlocksFileEditor':function(){
define([
    'dcl/dcl',
    'dojo/_base/declare',
    'xide/factory',
    'xide/utils',
    'xblox/model/variables/Variable',
    'dojo/Deferred',
    'xide/types',
    'xide/views/_LayoutMixin',
    'xblox/views/BlockGrid',
    'xaction/ActionProvider',
    'xide/layout/Container',
    'xdocker/Docker2',
    "xide/views/_CIPanelDialog",
    "xide/lodash"
], function (dcl, declare, factory, utils, Variable, Deferred, types, _LayoutMixin, BlockGrid, ActionProvider, Container, Docker, _CIPanelDialog, _) {
    var LayoutClass = dcl(_LayoutMixin.dcl, {
        rootPanel: null,
        _lastTab: null,
        getDocker: function () {
            if (!this._docker) {
                this._docker = Docker.createDefault(this.containerNode);
                this.add(this._docker);
            }
            return this._docker;
        },
        getRootContainer: function (args) {
            if (this.rootPanel) {
                return this.rootPanel;
            }
            this.reparent = true;
            var docker = this.getDocker();
            var DOCKER = types.DOCKER;
            var defaultTabArgs = {
                icon: false,
                closeable: true,
                moveable: true,
                tabOrientation: DOCKER.TAB.TOP,
                location: DOCKER.DOCK.STACKED

            };
            this.rootPanel = docker.addTab(null, utils.mixin(defaultTabArgs, args));
            return this.rootPanel;
        },
        createLayout: function () {
            if (!this.rootPanel) {
                this.rootPanel = this.getRootContainer({
                    title: 'test'
                });
            }
            return this.rootPanel;
        },
        startup: function () {

        },
        getPropertyStruct: function () {
            return this.propertyStruct;
        },
        setPropertyStruct: function (struct) {
            this.propertyStruct = struct;
        },
        getGroupContainer: function () {
            return this.rootPanel;
        },
        createGroupView: function (groupContainer, group, scope, closable, iconClass, selected, args) {
            var DOCKER = types.DOCKER;
            var defaultTabArgs = {
                icon: iconClass,
                closeable: true,
                moveable: true,
                title: group,
                target: this._lastTab || this.rootPanel,
                tabOrientation: DOCKER.TAB.TOP,
                location: DOCKER.DOCK.STACKED
            };
            var tab = this.getDocker().addTab(null, utils.mixin(defaultTabArgs, args));
            this._lastTab = tab;
            this.tabs.push(tab);
            return tab;
        }
    });

    var GridClass = declare('BlockGrid', BlockGrid, {
        /**
         * Step/Move Down & Step/Move Up action
         * @param dir
         */
        move: function (dir) {
            var items = this.getSelection();
            if (!items || !items.length) {
                 0 && console.log('cant move, no selection or parentId', items);
                return;
            }
            var thiz = this;
            _.each(items, function (item) {
                item.move(item, dir);
            });
            thiz.refreshRoot();
            thiz.refreshCurrent();
            this.select(items, null, true, {
                focus: true,
                delay: 10
            }).then(function () {
                thiz.refreshActions();
            });
        },
        reParentBlock: function (dir) {
            var item = this.getSelection()[0];
            if (!item) {
                 0 && console.log('cant move, no selection or parentId', item);
                return false;
            }
            var thiz = this;
            if (dir == -1) {
                item.unparent(thiz.blockGroup);
            } else {
                item.reparent();
            }
            thiz.deselectAll();
            thiz.refreshRoot();
            this.refreshCurrent();
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 100,
                select: item,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        defaultActionResult: function (items) {
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 0,
                select: items,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        }
    });

    var Module = dcl([Container, LayoutClass, ActionProvider.dcl], {
        declaredClass: "xblox.views.BlocksFileEditor",
        registerView: false,
        _item: null,
        cssClass: 'bloxEditor',
        blockManager: null,
        blockManagerClass: 'xblox.manager.BlockManager',
        model: null,
        store: null,
        tree: null,
        currentItem: null,
        didLoad: false,
        selectable: false,
        beanType: 'BLOCK',
        newGroupPrefix: '',
        _debug: false,
        blockScope: null,
        groupContainer: null,
        canAddGroups: true,
        gridClass: GridClass,
        activeGrid: null,
        activeTab: null,
        registerGrids: true,
        visibileTab: null,
        setVisibleTab: function (tab) {
            this.visibileTab = tab;
        },
        getVisibleTab: function () {
            return this.visibileTab;
        },
        constructor: function (options, container) {
            utils.mixin(this, options);
        },
        onGridAction: function (evt) {
            var action = evt.action ? evt.action : evt,
                command = action.command,
                ACTION = types.ACTION;

            switch (command) {
                case ACTION.SAVE: {
                    return this.save();
                }
            }
        },
        clearGroupViews: function (all) {
            _.each(this.tabs, function (tab) {
                tab.destroy();
            });
            delete this.tabs;
            this.destroyWidgets();
        },
        getContainerLabel: function (group) {
            var title = '' + group;
            if (utils.isNativeEvent(group)) {
                title = title.replace('on', '');
            }

            if (group.indexOf(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED) !== -1) {
                var deviceManager = this.ctx.getDeviceManager();
                var driverManager = this.ctx.getDriverManager();
                var parts = group.split('__');
                var blockUrl = parts[1];
                var device = deviceManager.getDeviceByUrl(blockUrl);
                var deviceTitle = device ? deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE) : 'Invalid Device';
                var variable = driverManager.getBlock(group);
                title = '(' + deviceTitle + '/' + (variable ? variable.name : 'Unknown Variable') + ')';
                return title;
            }
            if(title==='basicVariables'){
                title = 'Action Variables';
            }
            return title;
        },
        addNewBlockGroup: function (group) {
            if (!group) {
                return;
            }
            var blockScope = this.blockScope;
            var title = this.getContainerLabel(group);
            var contentPane = this.createGroupView(null, title, blockScope, true, 'fa-bell');
            var newGroup = this.newGroupPrefix + group;
            var gridViewConstructurArgs = {
                newRootItemGroup: newGroup
            };
            var view = this.createGroupedBlockView(contentPane, newGroup, blockScope, gridViewConstructurArgs);
            contentPane.grid = view;
            this.activeGrid = view;
            contentPane.select();
        },
        createGroupedBlockView: function (container, group, scope, extra, gridBaseClass) {
            var thiz = this;
            gridBaseClass = gridBaseClass || this.gridClass;
            var store = scope.blockStore;
            if (this._lastTab && !this.__right) {
                this.__right = this.getRightPanel(null, null, 'DefaultFixed', {
                    target: this._lastTab
                });
            }
            var gridArgs = {
                __right: this.__right,
                ctx: this.ctx,
                blockScope: scope,
                blockGroup: group,
                attachDirect: true,
                resizeToParent: true,
                collection: store.filter({
                    group: group
                }),
                _parent: container,
                getPropertyStruct: this.getPropertyStruct,
                setPropertyStruct: this.setPropertyStruct
            };

            extra && utils.mixin(gridArgs, extra);
            var view = utils.addWidget(gridBaseClass, gridArgs, null, container, false);

            if (!view.__editorActions) {
                view.__editorActions = true;
                this.addGridActions(view, view);
            }
            container.grid = view;
            view._on('selectionChanged', function (evt) {
                thiz._emit('selectionChanged', evt);
            });
            view._on('onAfterAction', function (e) {
                thiz.onGridAction(e);
            });
            container.on(types.DOCKER.EVENT.VISIBILITY_CHANGED, function (visible) {
                if (visible) {
                    thiz.setVisibleTab(container);
                    view.onShow();
                    var right = thiz.getRightPanel();
                    var splitter = right.getSplitter();
                    if (!splitter.isCollapsed()) {
                        view.showProperties(view.getSelection()[0]);
                    }
                    if (view.__last) {
                        view._restoreSelection();
                        view.focus(view.row(view.__last.focused));
                    }
                } else {
                    view.__last = view._preserveSelection();
                }
                if (visible && !this.grid._started) {
                }
            });
            this.registerGrids && this.ctx.getWindowManager().registerView(view, false);
            if (!container._widgets) {
                container._widgets = [];
            }
            container.add(view);
            return view;
        },
        getDeviceVariablesAsEventOptions: function (startIntend) {
            var options = [];
            var _item = function (label, value, intend, selected, displayValue) {
                var string = "<span style=''>" + label + "</span>";
                var pre = "";
                if (intend > 0) {
                    for (var i = 0; i < intend; i++) {
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label: _final,
                    label2: displayValue,
                    value: value
                };
            };
            var deviceManager = this.ctx.getDeviceManager();
            var items = deviceManager.getDevices(false, true);

            for (var i = 0; i < items.length; i++) {
                var device = items[i];
                var driver = device.driver;
                if (!driver) {
                    continue;
                }

                var title = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                var id = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_ID);
                options.push(_item(title, driver.id + '/' + driver.id, startIntend, false));
                var blockScope = driver.blockScope;
                var variables = blockScope.getVariables();
                for (var j = 0; j < variables.length; j++) {
                    var variable = variables[j];
                    var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED + '__' + id + '__' + driver.id + '__' + variable.id;
                    var selected = false;
                    options.push(_item(variable.name, value, startIntend + 1, selected, title + '/' + variable.name));
                }
            }

            return options;
        },
        openNewGroupDialog: function () {
            var options = [];
            var _item = function (label, value, intend, isHTML) {
                var string = isHTML !== true ? "<span style=''>" + label + "</span>" : label;
                var pre = "";
                if (intend > 0) {
                    for (var i = 0; i < intend; i++) {
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label: _final,
                    value: value,
                    label2: label
                };
            };

            options.push(_item('HTML', '', 0));
            options = options.concat([
                _item('onclick', 'click', 1),
                _item('ondblclick', 'dblclick', 1),
                _item('onmousedown', 'mousedown', 1),
                _item('onmouseup', 'mouseup', 1),
                _item('onmouseover', 'mouseover', 1),
                _item('onmousemove', 'mousemove', 1),
                _item('onmouseout', 'mouseout', 1),
                _item('onkeypress', 'keypress', 1),
                _item('onkeydown', 'keydown', 1),
                _item('onkeyup', 'keyup', 1),
                _item('onfocus', 'focus', 1),
                _item('onblur', 'blur', 1),
                _item('onchange', 'change', 1),
                _item('input', 'input', 1),
                _item('load', 'load', 1)
            ]);

            var inVariableStr = "";

            var thiz = this;
            var actionDialog = new _CIPanelDialog({
                title: 'Create a new block group',
                style: 'width:500px;min-height:200px;',
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                resizeable: true,
                onOk: function (changedCIS) {
                    //command picker
                    if (changedCIS && changedCIS[0] && changedCIS[0].dst === 'variable') {
                        var url = changedCIS[0].newValue;
                        var parts = utils.parse_url(url);//strip scheme
                        parts = utils.urlArgs(parts.host);//go on with query string
                        //var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED+ '__' + parts.device.value +'__'+parts.driver.value + '__'+ parts.block.value;
                        var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED + '__' + url;
                        thiz.addNewBlockGroup(value);
                        return;
                    }
                    if (changedCIS && changedCIS[0]) {
                        thiz.addNewBlockGroup(changedCIS[0].newValue);
                    }
                },
                cis: [
                    utils.createCI('Event', types.ECIType.ENUMERATION, '',
                        {
                            group: 'Event',
                            delegate: null,
                            options: options,
                            value: 'HTML',
                            title: 'Select an event &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                            widget: {
                                search: true,
                                "class": "xide.form.FilterSelect"
                            }
                        }
                    ),
                    utils.createCI('value', 'xcf.widgets.CommandPicker', inVariableStr, {
                        group: 'Event',
                        title: ' ',
                        dst: 'variable',
                        pickerType: 'variable',
                        value: ' Select Driver Variable ',
                        block: {
                            scope: {
                                ctx: thiz.ctx
                            }
                        },
                        widget: {
                            //store:this.scope.blockStore,
                            labelField: 'name',
                            valueField: 'id',
                            value: 'Select Driver Variable',
                            query: [
                                {
                                    group: 'basicVariables'
                                },
                                {
                                    group: 'processVariables'
                                }
                            ]

                        }
                    })
                ]
            });

            this.add(actionDialog);
            return actionDialog.show();
        },
        onGroupsCreated: function () {
            //this.ctx.getWindowManager().registerView(this,true);
        },
        getActionStore: function () {
            if (this.activeGrid) {
                return this.activeGrid.getActionStore();
            }
            return null;
        },
        getTabContainer: function () {
            return this.groupContainer;
        },
        onReloaded: function () {
            this.destroyWidgets();
            this.openItem(this._item);
        },
        /**
         * default entry when opening a file item through xfile
         * @param item
         */
        openItem: function (item) {

            var dfd = new Deferred();
            this._item = item;
            var thiz = this;

            var blockManager = this.getBlockManager();

            blockManager.load(item.mount, item.path).then(function (scope) {
                thiz.initWithScope(scope);
                dfd.resolve(thiz);
            });

            return dfd;

        },
        /**
         * Init with serialized string, forward to
         * @param content
         */
        initWithContent: function (content) {
            var data = null;
            try {
                data = utils.getJson(content);
            } catch (e) {
                console.error('invalid block data');
            }

            if (data) {
                this.initWithData(data);
            }
        },
        /**
         * Entry point when a blox scope is fully parsed
         * @param blockScope
         */
        initWithScope: function (blockScope) {
            this.blockScope = blockScope;
            var allBlockGroups = blockScope.allGroups(),
                thiz = this;
            if (allBlockGroups.indexOf('Variables') == -1) {
                allBlockGroups.push('Variables');
            }
            if (allBlockGroups.indexOf('Events') == -1) {
                allBlockGroups.push('Events');
            }
            if (allBlockGroups.indexOf('On Load') == -1) {
                allBlockGroups.push('On Load');
            }
            thiz.renderGroups(allBlockGroups, blockScope);

        },
        getScopeUserData: function () {
            return {
                owner: this
            };
        },
        initWithData: function (data) {
            if (this._debug) {
                 0 && console.log('init with data', data);
            }

            this.onLoaded();

            var scopeId = utils.createUUID(),
                blockInData = data,
                variableInData = data;

            //check structure
            if (_.isArray(data)) {// a flat list of blocks

            } else if (_.isObject(data)) {
                scopeId = data.scopeId || scopeId;
                blockInData = data.blocks || [];
            }
            var blockManager = this.getBlockManager();
            this.blockManager = blockManager;
            var scopeUserData = this.getScopeUserData();
            var blockScope = blockManager.getScope(scopeId, scopeUserData, true);
            var allBlocks = blockScope.blocksFromJson(blockInData);
            for (var i = 0; i < allBlocks.length; i++) {
                var obj = allBlocks[i];

                obj._lastRunSettings = {
                    force: false,
                    highlight: true
                };
            }
            blockManager.onBlocksReady(blockScope);
            if (allBlocks) {
                return this.initWithScope(blockScope);
            }
        },
        destroyWidgets: function () {
            if (this.blockManager && this.blockScope) {
                this.blockManager.removeScope(this.blockScope.id);
            }
            this.blockManager = null;
        },
        destroy: function () {
            this.destroyWidgets();
            delete this.tabs;
            delete this.propertyStruct;
            delete this.visualEditor;
            delete this.widget;
            delete this.params;
            delete this.pageEditor;
            delete this.editorContext;
            delete this.activeGrid;

            this.item = null;
            this._parent = null;
            this.ctx = null;
            this.docker = null;
            this.__right = null;
            this.blockScope = null;
            this.delegate = null;


        },
        getBlockManager: function () {
            if (!this.blockManager) {
                if (this.ctx.blockManager) {
                    return this.ctx.blockManager;
                }
                this.blockManager = factory.createInstance(this.blockManagerClass, {
                    ctx: this.ctx
                });
                if (this._debug) {
                     0 && console.log('_createBlockManager ', this.blockManager);
                }
            }
            return this.blockManager;
        },
        getActiveGrid: function () {
            return this.activeGrid;
        },
        runAction: function (action) {
            if (action.command == 'File/New Group') {
                this.openNewGroupDialog();
                return null;
            }
            return this.getActiveGrid().runAction(arguments);
        },
        addGridActions: function (grid, who) {
            var result = [];
            var thiz = this;
            var defaultMixin = {addPermission: true};
            result.push(thiz.createAction({
                label: 'New Group',
                command: 'File/New Group',
                icon: 'fa-magic',
                tab: 'Home',
                group: 'File',
                keycombo: ['f7'],
                mixin: utils.mixin({quick: true}, defaultMixin)
            }));
            result.push(thiz.createAction({
                label: 'Delete Group',
                command: 'File/Delete Group',
                icon: 'fa-remove',
                tab: 'Home',
                group: 'File',
                keycombo: ['ctrl f7'],
                mixin: utils.mixin({quick: true}, defaultMixin)
            }));
            (who || this).addActions(result);
        },
        onShowGrid: function (grid) {
        },
        renderGroup: function (group, blockScope, gridBaseClass) {
            blockScope = blockScope || this.blockScope;
            var thiz = this;

            var title = group.replace(this.newGroupPrefix, '');
            if (utils.isNativeEvent(group)) {
                title = title.replace('on', '');
            }
            title = this.getContainerLabel(group.replace(this.newGroupPrefix, ''));
            var isVariableView = group === 'Variables';
            var contentPane = this.createGroupView(null, title, blockScope, !isVariableView, isVariableView ? ' fa-info-circle' : 'fa-bell', !isVariableView);
            var gridViewConstructurArgs = {};
            if (group === 'Variables') {
                gridViewConstructurArgs.newRootItemFunction = function () {
                    new Variable({
                        title: 'No-Title-Yet',
                        type: 13,
                        value: 'No Value',
                        enumType: 'VariableType',
                        save: false,
                        initialize: '',
                        group: 'Variables',
                        id: utils.createUUID(),
                        scope: blockScope
                    });

                };

                gridViewConstructurArgs.onGridDataChanged = function (evt) {

                    var item = evt.item;
                    if (item) {
                        item[evt.field] = evt.newValue;
                    }
                    thiz.save();
                };
                gridViewConstructurArgs.showAllBlocks = false;
                gridViewConstructurArgs.newRootItemLabel = 'New Variable';
                gridViewConstructurArgs.newRootItemIcon = 'fa-code';
                gridViewConstructurArgs.gridParams = {
                    cssClass: 'bloxGridView',
                    getColumns: function () {
                        return [
                            {
                                label: "Name",
                                field: "title",
                                sortable: true

                            },
                            {
                                label: "Value",
                                field: "value",
                                sortable: false
                            }
                        ];
                    }
                };
            }


            gridViewConstructurArgs.newRootItemGroup = group;

            var view = this.createGroupedBlockView(contentPane, group, blockScope, gridViewConstructurArgs, gridBaseClass);
            contentPane.blockView = view;
            !this.activeGrid && (this.activeGrid = view);
            return view;
        },
        renderGroups: function (_array, blockScope) {
            this._isCreating = true;
            this.activeGrid = null;
            this.tabs = [];
            this._lastTab = null;
            this.setPropertyStruct({
                currentCIView: null,
                targetTop: null,
                _lastItem: null,
                id: utils.createUUID()
            });
            for (var i = 0; i < _array.length; i++) {
                var group = _array[i];
                if (this.newGroupPrefix == '' && group.indexOf('__') !== -1) {
                    continue;
                }
                try {
                    var groupBlocks = blockScope.getBlocks({
                        group: group
                    });

                    if (group !== 'Variables' && (!groupBlocks || !groupBlocks.length)) {//skip empty
                        continue;
                    }
                    this.renderGroup(group, blockScope);
                } catch (e) {
                    logError(e);
                }
            }

            if (this.tabs && this.tabs[0]) {
                this.tabs[0].silent = true;
                this.tabs[0].select();
                this.tabs[0].silent = false;
            }
        },
        onSave: function (groupedBlockView) {
            this.save();
        },
        save: function () {
            if (this.blockScope) {
                var fileManager = this.ctx.getFileManager(),
                    item = this.item;
                fileManager.setContent(item.mount, item.path, this.blockScope.toString(), function () {
                     0 && console.log('saved blocks! to ' + item.path);
                });
            } else {
                 0 && console.warn('BlocksFileEditor::save : have no block scope');
            }
        }
    });
    return Module;
});
},
'xblox/model/Block_UI':function(){
/** @module xblox/model/Block_UI **/
define([
    'dcl/dcl',
    "xide/utils",
    "xide/types",
    "xide/data/Source",
    "xaction/ActionProvider"
], function (dcl,utils, types,Source,ActionProvider) {
    /**
     * All ui - related addons/interfaces for blocks
     * Please read {@link module:xide/types}
     * @class module:xblox/model/Block_UI
     * @lends module:xblox/model/Block
     */
    return dcl([Source.dcl,ActionProvider.dcl],{
        declaredClass:"xblox.model.Block_UI",
        _statusIcon:null,
        /**
         * hash-map per scope - id
         */
        _statusClass:null,
        /**
         * The blocks internal user description
         * Description is used for the interface. This should be short and expressive and supports plain and html text.
         *
         * @todo: same as name, move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {string}
         * @default 'No Description'
         * @required true
         */
        description: 'No Description',
        /**
         * UI flag, prevents that a block can be deleted.
         * @todo, move to block flags
         * @type {boolean}
         * @default true
         */
        canDelete: true,
        renderBlockIcon: true,
        /**
         * Return current status icon
         * @returns {string|null}
         */
        getStatusIcon:function(){
          return this._statusIcon;
        },
        /**
         * Returns the stored highlight class per scope-id
         * @param scopeId {string}
         * @returns {object|null}
         */
        getStatusClass:function(scopeId){
            if(this._statusClass){
                return this._statusClass[scopeId];
            }
            return null;
        },
        /**
         * Store the highlight class per scope id
         * @param scopeId
         * @param statusClass
         */
        setStatusClass:function(scopeId,statusClass){
            if(!this._statusClass){
                this._statusClass = {};
            }
            delete this._statusClass[scopeId];
            statusClass && (this._statusClass[scopeId]=statusClass);
        },
        _getText: function (url) {
            var result;
            dojo.xhrGet({
                url: url,
                sync: true,
                handleAs: 'text',
                load: function (text) {
                    result = text;
                }
            });
            return '' + result + '';
        },
        /**
         * implements
         */
        getHelp:function(){
        },
        /**
         * provides
         * @param which
         * @param style
         * @param after
         * @returns {string}
         */
        getIcon:function(which,style,after){
            return '<span style="' + (style||"")+'" class="'+which+'"></span> ' + (after || "");
        },
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable2:function(field,pos,type,title,mode){
            return "<a tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable'  href='#'>" + this[field] +"</a>";
        },
        makeEditable:function(field,pos,type,title,mode){
            var editableClass = this.canEdit() ? 'editable' : 'disabled';
            return "<a data-value='" + this[field] +  "' tabIndex='-1' pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='" +editableClass + "' href='#'>" + this[field] +"</a>";
        },
        /**
         * Called by UI, determines whether a block can be moved up or down
         * @param item
         * @param dir
         * @returns {boolean}
         */
        canMove: function (item, dir) {
            item = item || this;
            if (!item) {
                return false;
            }
            var parent = item.getParent(),
                items = null;
            if (parent) {
                items = parent[parent._getContainer(item)];
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }
                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var upperItem = items[cIndex + (dir)];
                if (!upperItem) {
                    return false;
                }
            }else{
                var store = this._store;
                items = store.storage.fullData;
                var _next = this.next(items,dir);
                return _next!=null;
            }
            return true;
        },
        /**
         * Moves an item up/down in the container list
         * @param item
         * @param dir
         * @returns {boolean}
         */
        move: function (item, dir) {
            item = item || this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            var items = null;
            var store = item._store;
            if(parent) {
                items = parent[parent._getContainer(item)];
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }

                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var upperItem = items[cIndex + (dir)];
                if (!upperItem) {
                    return false;
                }
                items[cIndex + (dir)] = item;
                items[cIndex] = upperItem;
                return true;

            }else{
                if(store && item.group){
                    items = store.storage.fullData;
                }
                var _dstIndex = 0;
                var step = 1;
                function _next(item,items,dir){
                    var cIndex = item.indexOf(items, item);
                    var upperItem = items[cIndex + (dir * step)];
                    if(upperItem){
                        if(!upperItem.parentId && upperItem.group && upperItem.group===item.group){

                            _dstIndex = cIndex + (dir * step);
                            return upperItem;
                        }else{
                            step++;
                            return _next(item,items,dir);
                        }
                    }
                    return null;
                }
                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var next = _next(item,items,dir);
                if (!next) {
                    return false;
                }
                items[_dstIndex]=item;
                items[cIndex] = next;
                store._reindex();
                return true;
            }
        },
        /**
         * provides
         * @param block
         * @param settings
         * @param event
         * @param args
         */
        onActivity: function (block, settings, event,args) {
            args = args || {};
            args.target = block;
            this._emit(event, args, block);
            this.publish(event,args,block);
        },
        /**
         * provides
         * @param block
         * @param settings
         */
        onRun: function (block, settings,args) {
            var highlight = settings && settings.highlight;
            if (block && highlight) {
                this.onActivity(block, settings, types.EVENTS.ON_RUN_BLOCK,args);
            }
            this._statusIcon = 'text-info fa-spinner fa-spin';
        },
        /**
         * provides
         * @param block
         * @param settings
         * @param args
         */
        onFailed: function (block, settings,args) {
            var highlight = settings && settings.highlight;
            if (block && highlight) {
                this.onActivity(block, settings, types.EVENTS.ON_RUN_BLOCK_FAILED,args);
            }
            this._statusIcon = 'text-danger fa-exclamation';
        },
        /**
         * provides
         * @param block
         * @param settings
         * @param args
         */
        onSuccess: function (block, settings,args) {
            var highlight = settings && settings.highlight;
            if (block && highlight) {
                this.onActivity(block, settings, types.EVENTS.ON_RUN_BLOCK_SUCCESS,args);
            }
            this._statusIcon = 'text-success fa-check';
        },
        /**
         * implements
         * @returns {boolean}
         */
        canDisable: function () {
            return true;
        },
        /**
         * provides defaults
         * @param icon {boolean=true}
         * @param share {boolean=true}
         * @param outlets {boolean=true}
         * @returns {Array}
         */
        getDefaultFields: function (icon,share,outlets) {
            var fields = [];
            if (this.canDisable && this.canDisable() !== false) {
                fields.push(
                    utils.createCI('enabled', 0, this.enabled, {
                        group: 'General',
                        title: 'Enabled',
                        dst: 'enabled',
                        actionTarget:'value',
                        order:210
                    })
                );
            }
            fields.push(utils.createCI('description', 26, this.description, {
                group: 'Description',
                title: 'Description',
                dst: 'description',
                useACE: false
            }));

            icon!==false && fields.push(utils.createCI('icon', 17, this.icon, {
                group: 'General',
                title: 'Icon',
                dst: 'icon',
                useACE: false,
                order:206
            }));

            outlets!==false && fields.push(utils.createCI('outlet', 5, this.outlet, {
                group: 'Special',
                title: 'Type',
                dst: 'outlet',
                order:205,
                data:[
                    {
                        value: 0x00000001,
                        label: 'Progress',
                        title: 'Executed when progress'
                    },
                    {
                        value: 0x00000002,
                        label: 'Error',
                        title: "Executed when errors"
                    },
                    {
                        value: 0x00000004,
                        label: 'Paused',
                        title: "Executed when paused"
                    },
                    {
                        value: 0x00000008,
                        label: 'Finish',
                        title: "Executed when finish"
                    },
                    {
                        value: 0x00000010,
                        label: 'Stopped',
                        title: "Executed when stopped"
                    }
                ],
                widget:{
                    hex:true
                }
            }));
            if (this.sharable) {
                fields.push(
                    utils.createCI('enabled', 13, this.shareTitle, {
                        group: 'Share',
                        title: 'Title',
                        dst: 'shareTitle',
                        toolTip: 'Enter an unique name to share this block!'
                    })
                );
            }
            return fields;
        },
        /**
         * implements
         * @returns {*|Array}
         */
        getFields: function () {
            return this.getDefaultFields();
        },
        /**
         * util
         * @param str
         * @returns {*}
         */
        toFriendlyName: function (str) {
            var special = ["[", "]", "(", ")", "{", "}"];
            for (var n = 0; n < special.length; n++) {
                str = str.replace(special[n], '');
            }
            str = utils.replaceAll('==', ' equals ', str);
            str = utils.replaceAll('<', ' is less than ', str);
            str = utils.replaceAll('=<', ' is less than ', str);
            str = utils.replaceAll('>', ' is greater than ', str);
            str = utils.replaceAll('>=', ' is greater than ', str);
            str = utils.replaceAll("'", '', str);
            return str;
        },
        /**
         * implements
         * @returns {string}
         */
        getIconClass: function () {
            return this.icon;
        },
        /**
         * implements
         * @param symbol
         * @returns {string}
         */
        getBlockIcon: function (symbol) {
            symbol = symbol || '';
            return this.renderBlockIcon == true ? '<span class="xBloxIcon ' + this.icon + '">' + symbol + '</span>' : '';
        },
        /**
         * provides
         * @param block
         * @param cis
         */
        onFieldsRendered: function (block, cis) {
        },
        /**
         * inherited
         * @param field
         * @param newValue
         */
        onChangeField: function (field, newValue) {
            if (field == 'enabled') {
                if (newValue == true) {
                    this.activate();
                } else {
                    this.deactivate();
                }
            }
        },
        /**
         * implements
         */
        destroy:function(){
            this.setStatusClass(this.getScope().id,null);
        }
    });
});
},
'xblox/RunScript':function(){
define([
    "dojo/_base/lang",
    "dojo/on",
    "dcl/dcl",//make sure
    "delite/register",
    "delite/CustomElement",
    //explicit because a bootstrap might not be loaded at some point
    "xide/factory/Events",
    //explicit because a bootstrap might not be loaded at some point
    'xide/utils/StringUtils',
    'xide/types/Types',
    'xblox/model/Referenced',
    'xide/mixins/EventedMixin',
    'xide/mixins/ReloadMixin',
    /** 2way binding dependencies **/
    'xwire/Binding',
    'xwire/EventSource',
    'xwire/WidgetTarget'

], function (lang, on, dcl, register, CustomElement, Events, utils, Types, Referenced, EventedMixin, ReloadMixin, Binding, EventSource, WidgetTarget, registry) {

    var debugWidgets = false;
    var debugApp = false;
    var debugAttach = false;
    var debugCreated = false;
    var debugBinding = false;
    var debugRun = false;
    /**
     * Proxy widget to run a selected blox script on the parent widget/node.
     *
     * @class xblox/RunScript
     */
    var Impl = {
        declaredClass: 'xblox/RunScript',
        targetevent: '',
        sourceevent: "",
        sourceeventvaluepath: "",
        sourceeventnamepath: "",
        targetproperty: "",
        targetvariable: "",
        targetfilter: "",
        script: "",
        bidirectional: false,
        blockGroup: '',
        block: '',
        _targetBlock: null,
        _targetReference: null,
        _appContext: null,
        _complete: false,
        enabled: true,
        stop: false,
        _events: [],
        context: null,
        accept: '',
        transform: '',
        mode: 0,
        _2wayHandle: null,//the handle
        binding: null,//the binding
        /**
         * soft destroy
         */
        reset: function () {
            this._destroyHandles();
            if (this._2wayHandle) {
                this._2wayHandle.remove();
            }
            if (this.binding) {
                this.binding.destroy();
            }
            delete this.binding;
            this._appContext = null;
            this._targetReference = null;
        },
        /**
         *
         * @param newSettings
         */
        onSettingsChanged: function () {
            this.reset();
            if (!this.enabled) {
                return;
            }
            this.onAppReady(null);
        },
        getChildren: function () {
            return [];
        },
        /**
         * @inheritDoc
         */
        destroy: function () {
            this.onDestroy && this.onDestroy();
            this.reset();
            delete this.binding;
            delete this.context;
        },
        /**
         * The final execution when 'target event' has been triggered. This
         * will run the select block.
         * @param event
         * @param val
         */
        run: function (event, val) {
            if (!this.enabled) {
                return;
            }
            var settings = {};
            //filter, in design mode, we ain't do anything
            if (this.context && this.context.delegate) {
                if (this.context.delegate.isDesignMode && this.context.delegate.isDesignMode()) {
                    return;
                }
                if (this.context.delegate.getBlockSettings) {
                    settings = this.context.delegate.getBlockSettings();
                }
            }
            //setup variables
            var block = this._targetBlock,
                context = this._targetReference,
                result;

            if (block && context) {
                block.context = context;
                block._targetReference = context;
                if (this.targetvariable && this.targetvariable.length && val != null) {
                    block.override = {
                        variables: {}
                    };
                    block.override.variables[this.targetvariable] = val;
                }
                result = block.solve(block.scope, settings);
                debugRun &&  0 && console.log('run ' + block.name + ' for even ' + event, result + ' for ' + this.id, this._targetReference);
            }
        },
        /**
         * Callback when the minimum parameters are given: targetReference & targetBlock
         */
        onReady: function () {
            if (!this._targetReference) {
                this._setupTargetReference();
            }

            //resolve 2way binding
            if (this._targetReference && this['bidirectional'] === true && this.sourceevent && this.sourceevent.length && !this.binding) {
                this._setup2WayBinding();
            }

            if (this._complete) {
                return;
            }
            if (!this._targetReference) {
                console.error('have no target reference');
            }
            if (!this._targetBlock) {
                console.error('have no target block');
            }

            if (this._targetReference && this._targetBlock) {
                //we trigger on events
                if (this.targetevent) {
                    this._complete = true;
                    //patch the target
                    utils.mixin(this._targetReference, EventedMixin.prototype);
                    var _target = this._targetReference.domNode || this._targetReference,
                        _event = this.targetevent,
                        _isWidget = this._targetReference.declaredClass || this._targetReference.startup,
                        _hasWidgetCallback = this._targetReference.on != null && this._targetReference['on' + utils.capitalize(_event)] != null,
                        _handle = null,
                        _isDelite = _target.render != null && _target.on != null,
                        thiz = this;

                    if (_isWidget && (this._targetReference.baseClass && this._targetReference.baseClass.indexOf('dijitContentPane') != -1) || this._targetReference.render != null || this._targetReference.on != null) {
                        _isWidget = false;//use on
                    }

                    if (_target) {
                        debugBinding &&  0 && console.log('wire success ' + this.id + ' for ' + this.targetevent);
                        if (!_isDelite && (!_hasWidgetCallback || !_isWidget)) {
                            _handle = on(_target, this.targetevent, function (evt) {
                                this.run(this.targetevent);
                            }.bind(this));
                        } else {
                            _target = this._targetReference;
                            var useOn = true;
                            if (useOn) {
                                if (!_isDelite) {
                                    var _e = 'on' + utils.capitalize(_event);
                                    this._targetReference[_e] = function (val, nada) {
                                        if (_target.ignore !== true) {
                                            thiz.run(thiz.targetevent, val);
                                        }
                                    };
                                } else {
                                    _handle = _target.on(this.targetevent, function (evt) {
                                        if (this.stop) {
                                            evt.preventDefault();
                                            evt.stopImmediatePropagation();
                                        }
                                        this.run(this.targetevent, evt.currentTarget.value);
                                    }.bind(this));
                                }
                            } else {
                                this._targetReference['on' + utils.capitalize(_event)] = function (val) {
                                    if (_target.ignore !== true) {
                                        thiz.run(thiz.targetevent, val);
                                    }
                                };
                            }
                        }
                        _handle && this._events.push(_handle);
                    } else {
                        console.error('have no target to wire');
                    }
                }
            } else {
                console.error('invalid params, abort', this);
            }
            if (this.binding) {
                this.binding.start();
            }
        },
        resolveBlock: function (block) {
            var ctx = this._appContext;
            var deviceManager = ctx.getDeviceManager();
            if (block.indexOf('://') !== -1) {
                if (!deviceManager) {
                    return;
                }
                var _block = deviceManager.getBlock(this.block);
                if (_block) {
                    return _block;
                }
            }
        },
        /**
         *
         * @param ctx
         * @private
         */
        _setBlock: function (ctx) {
            ctx = ctx || window['appContext'];
            if (!ctx || !ctx.getBlockManager) {
                debugApp &&  0 && console.warn('have no context or block manager');
                return;
            }
            this._appContext = ctx;
            var blockManager = ctx.getBlockManager(),
                deviceManager = ctx.getDeviceManager(),
                thiz = this;

            if (!blockManager) {
                return;
            }
            var _block = this.block ? this.block : this.getAttribute('block');
            if (_block && _block.length > 0) {
                var parts = utils.parse_url(_block);
                if (_block.indexOf('://') !== -1) {
                    if (!deviceManager) {
                        debugApp &&  0 && console.warn('xScript::_setBlock : have no device manager');
                        return;
                    }
                    var _block2 = deviceManager.getBlock(_block);
                    if (_block2) {
                        thiz._targetBlock = _block2;
                        thiz.onReady();
                    } else {
                        debugBinding &&  0 && console.warn('cant get block : ' + _block);
                    }
                } else {
                    blockManager.load(parts.scheme, parts.host).then(function (scope) {
                        var block = scope.getBlockById(thiz.blockid);
                        if (block) {
                            thiz._targetBlock = block;
                            thiz.onReady();
                        }
                    });
                }
            } else if (this.scopeid) {
                var scope = blockManager.hasScope(thiz.scopeid);
                if (scope) {
                    var block = scope.getBlockById(thiz.blockid);
                    if (block) {
                        thiz._targetBlock = block;
                        thiz.onReady();
                    } else {
                        block = scope.getVariableById(thiz.blockid);
                        if (block) {
                            thiz._targetBlock = block;
                            thiz.onReady();
                        }
                    }
                } else {
                    console.error('have no scope!');
                }
            }
        },
        initWithReference: function (ref) {
            if (ref.nodeType !== 1) {
                return;
            }
            this._targetReference = ref;
            this._setBlock(null);
        },
        resolveFilter: function (expression, value, widget) {
            if (this._targetBlock) {
                var expressionModel = this._targetBlock.scope.expressionModel;
                value = expressionModel.parseVariable(this._targetBlock.scope, {
                    value: expression
                }, '', false, false, widget, [value]);
            }
            return value;
        },
        /**
         * setup outbound wire, assumes all parameters are checked
         * @private
         */
        _setup1WayBinding: function () {
            debugBinding &&  0 && console.log('setup 1 way binding');
            //destroy old handle
            if (this._2wayHandle) {
                this._2wayHandle.remove();
            }

            if (!this._targetBlock) {
                console.error('invalid params for one way binding');
                return;
            }
            var sourceVariableTitle = this._targetBlock.name;
            //wire to system event
            var bindingSource = new EventSource({
                //listen to variable changes
                trigger: this.sourceevent,
                //the path to value, ie: 'item.value'
                path: this.sourceeventvaluepath,
                //add an event filter
                filters: [{
                    // variable title must match,ie: 'item.title'
                    path: this.sourceeventnamepath,
                    // the name of the variable, ie: 'Volume'
                    value: sourceVariableTitle
                }]
            });


            //now map the event source to a widget
            var bindingTarget = new WidgetTarget({
                //the path to value
                path: this.targetproperty,
                object: this._targetReference,
                targetFilter: this.targetfilter,
                delegate: this
            });
            var accept = this._findbyTagAndName('D-SCRIPT', 'accept');
            var transform = this._findbyTagAndName('D-SCRIPT', 'transform');
            //construct the binding
            var binding = new Binding({
                source: bindingSource,
                target: bindingTarget,
                accept: this._findbyTagAndName('D-SCRIPT', 'accept'),
                transform: this._findbyTagAndName('D-SCRIPT', 'transform')
            });
            this.binding = binding;
            binding.start();
        },
        _findbyTagAndName: function (tag, name) {
            var scripts = $(this).find(tag);
            for (var i = 0; i < scripts.length; i++) {
                var script = scripts[i];
                if ($(script).attr('name') === name) {
                    return script;
                }
            }
            return null;
        },
        /**
         * setup inbound wire, assumes all parameters are checked
         * @private
         */
        _setup2WayBinding: function () {
            if (this.binding) {
                return;
            }
            debugBinding &&  0 && console.log('setup 2 way binding');
            //destroy old handle
            if (this._2wayHandle) {
                this._2wayHandle.remove();
            }
            //wire to system event
            var bindingSource = new EventSource({
                //listen to variable changes
                trigger: this.sourceevent,
                //the path to value, ie: 'item.value'
                path: this.sourceeventvaluepath,
                //add an event filter
                filters: [{
                    // variable title must match,ie: 'item.title'
                    path: this.sourceeventnamepath,
                    // the name of the variable, ie: 'Volume'
                    value: this.targetvariable
                }]
            });

            //now map the event source to a widget
            var bindingTarget = new WidgetTarget({
                //the path to value
                path: 'value',
                object: this._targetReference
            });
            this.binding = new Binding({
                source: bindingSource,
                target: bindingTarget
            });
            this.binding.start();
        },
        /**
         * Returns the widget whose DOM tree contains the specified DOMNode, or null if
         * the node is not contained within the DOM tree of any widget
         * @param {Element} node
         */
        getEnclosingWidget: function (node) {
            if (node) {
                do {
                    if (node.nodeType === 1 && node.render) {
                        return node;
                    }
                } while ((node = node.parentNode));
            }
            return null;
        },
        /**
         * Function to setup the target reference
         * on the surrounding widget!
         *
         */
        _setupTargetReference: function () {
            var i = 0,
                element = this,
                widget = null;

            while (i < 2 && !widget) {
                if (element) {
                    element = element.parentNode;
                    widget = this.getEnclosingWidget(element, "widgetId");
                    if (!widget) {
                        widget = this.getEnclosingWidget(element, "widgetid");
                    }
                }
                i++;
            }
            if (widget) {
                debugWidgets &&  0 && console.info('have widget reference' + '  : ', widget);
                this.initWithReference(widget);
            } else {
                if (this.domNode && this.domNode.parentNode) {
                    this.initWithReference(this.domNode.parentNode);
                    debugWidgets && console.error('cant find widget reference, using parent node', this._targetReference);
                } else {
                    if (this.parentNode) {
                        this.initWithReference(this.parentNode);
                    }
                    debugWidgets && console.error('cant find widget reference', this);
                }

            }
        },
        /**
         * Required in case of dojoConfig.parseOnLoad
         * @param evt
         */
        onAppReady: function (evt) {
            debugApp &&  0 && console.log('-ready');
            //resolve target reference
            if (!this._targetReference) {
                this._setupTargetReference();
            }
            //resolve target block
            if (!this._targetBlock) {
                this._setBlock(evt ? evt.context : null);
            }

            this.mode = this['bidirectional'] === true ? 0 : 1;
            //normal mode, allows 2-way binding
            if (this.mode === 0) {
                //resolve 2way binding
                if (this._targetBlock && this._targetReference && this['bidirectional'] === true && this.sourceevent && this.sourceevent.length) {
                    this._setup2WayBinding();
                }

                //if both are valid, run the the init procedure
                if (this._targetReference && this._targetBlock) {
                    this.onReady();
                }

            } else if (this.mode === 1 && this._targetBlock) {
                if (this._targetReference && this.sourceevent && this.sourceevent.length && this.targetproperty && this.targetproperty.length) {
                    this._setup1WayBinding();
                    if (this.binding) {
                        this.binding.start();
                    }
                }
            }
            //track context {xapp/manager/Context}
            if (evt && evt.context) {
                this.context = evt.context;
            }
        },
        detachedCallback: function () {
            debugAttach &&  0 && console.info('detachedCallback', this);
            if (this._appContext) {
                this.destroy();
            }
        },
        /**
         * Delite created callback
         */
        createdCallback: function () {
            debugCreated &&  0 && console.info('createdCallback', this);
        },
        /**
         * Delite attached callback
         */
        attachedCallback: function () {
            debugAttach &&  0 && console.info('attachedCallback', this);
            if (this._started) {
                return;
            }
            this.initReload();
            this.subscribe(Types.EVENTS.ON_APP_READY);
            this._started = true;

        },
        detachCallback: function () {
        },
        render: function () {

        },
        postRender: function () {

        },
        startup: function () {
            debugAttach &&  0 && console.log('startup');
            this.inherited(arguments);
            this.onAppReady();
            this.initReload();
            this.subscribe(Types.EVENTS.ON_APP_READY);

        }
    };

    //package and declare via dcl
    var _class = dcl([EventedMixin.dcl, ReloadMixin.dcl, Referenced.dcl], Impl);
    //static access to Impl.
    _class.Impl = Impl;
    return register("d-xscript", [HTMLElement, CustomElement, _class]);
});
},
'xblox/CSSState':function(){
define([
    "dcl/dcl",
    "delite/register",
    "delite/CustomElement",
    'xblox/_State',
    'xide/utils',
    'xdojo/has'
], function (dcl, register, CustomElement, _State, utils, has) {
    var extraRules = [],
        extraSheet,
        removeMethod,
        rulesProperty,
        invalidCssChars = /([^A-Za-z0-9_\u00A0-\uFFFF-])/g;

    has.add('dom-contains', function (global, doc, element) {
        return !!element.contains; // not supported by FF < 9
    });

    function removeRule(index) {
        // Function called by the remove method on objects returned by addCssRule.
        var realIndex = extraRules[index],
            i, l;
        if (realIndex === undefined) {
            return; // already removed
        }

        // remove rule indicated in internal array at index
        extraSheet[removeMethod](realIndex);

        // Clear internal array item representing rule that was just deleted.
        // NOTE: we do NOT splice, since the point of this array is specifically
        // to negotiate the splicing that occurs in the stylesheet itself!
        extraRules[index] = undefined;

        // Then update array items as necessary to downshift remaining rule indices.
        // Can start at index + 1, since array is sparse but strictly increasing.
        for (i = index + 1, l = extraRules.length; i < l; i++) {
            if (extraRules[i] > realIndex) {
                extraRules[i]--;
            }
        }
    }
    var Impl = {
        _lastState: null,
        declaredClass: 'xblox/CSSState',
        cssClass: "",
        addCssRule: function (selector, css) {
            // summary:
            //		Dynamically adds a style rule to the document.  Returns an object
            //		with a remove method which can be called to later remove the rule.

            if (!extraSheet) {
                // First time, create an extra stylesheet for adding rules
                extraSheet = document.createElement('style');
                document.getElementsByTagName('head')[0].appendChild(extraSheet);
                // Keep reference to actual StyleSheet object (`styleSheet` for IE < 9)
                extraSheet = extraSheet.sheet || extraSheet.styleSheet;
                // Store name of method used to remove rules (`removeRule` for IE < 9)
                removeMethod = extraSheet.deleteRule ? 'deleteRule' : 'removeRule';
                // Store name of property used to access rules (`rules` for IE < 9)
                rulesProperty = extraSheet.cssRules ? 'cssRules' : 'rules';
            }

            var index = extraRules.length;
            extraRules[index] = (extraSheet.cssRules || extraSheet.rules).length;
            extraSheet.addRule ?
                extraSheet.addRule(selector, css) :
                extraSheet.insertRule(selector + '{' + css + '}', extraRules[index]);
            return {
                get: function (prop) {
                    return extraSheet[rulesProperty][extraRules[index]].style[prop];
                },
                set: function (prop, value) {
                    if (typeof extraRules[index] !== 'undefined') {
                        extraSheet[rulesProperty][extraRules[index]].style[prop] = value;
                    }
                },
                remove: function () {
                    removeRule(index);
                },
                sheet: extraSheet
            };
        },
        escapeCssIdentifier: function (id, replace) {
            return typeof id === 'string' ? id.replace(invalidCssChars, replace || '\\$1') : id;
        },
        detachedCallback: function () {
            this._styles && _.each(this._styles, function (style) {
                style.remove();
            });
            delete this._styles;
        },
        applyTo: function (widget, name) {
            if (this._lastState) {
                this._lastState.remove();
            }
            delete this._lastStateName;
            this._lastStateName = name;
            if (!this._attached) {
                return;
            }
            var cssClass = this.cssClass;
            var isCSSClass = cssClass.length > 0;
            var id = widget.id || utils.createUUID();
            var _uniqueId = widget.tagName.replace(/\./g, "_") + '_' + id;
            var css = '' + this.innerHTML;
            css = css.replace('.style', '');
            css = css.replace(/<(?:.|\n)*?>/gm, '');
            css = css.replace('{', '');
            css = css.replace('}', '');
            css = css.replace(/(\r\n|\n|\r|\t)/gm, "");

            _uniqueId += '_state_' + name;

            $(widget).removeClass($(widget).data('_lastCSSState'));
            $(widget).removeClass($(widget).data('_lastCSSClass'));
            $(widget).removeClass(cssClass);

            if (!cssClass) {
                $(widget).addClass(_uniqueId);
                $(widget).data('_lastCSSState', _uniqueId);
                var selectorPrefix = '.' + this.escapeCssIdentifier(_uniqueId);
                if (!this._styles) {
                    this._styles = [];
                }
                var style = this.addCssRule(selectorPrefix, css);
                this._styles.push(style);
            } else {
                $(widget).addClass(cssClass);
                $(widget).data('_lastCSSClass', cssClass);
            }
        }

    };

    var _class = dcl([_State], Impl);
    //static access to Impl.
    _class.Impl = Impl;
    return register("d-xstate-css", [HTMLElement, CustomElement, _class]);
});
},
'xblox/_State':function(){
define([
    "dojo/_base/lang",
    "dojo/on",
    "dcl/dcl",//make sure
    "delite/register",
    "delite/CustomElement",
    //explicit because a bootstrap might not be loaded at some point
    "xide/factory/Events",
    //explicit because a bootstrap might not be loaded at some point
    'xide/utils/StringUtils',
    'xide/types/Types',
    'xblox/model/Referenced',
    'xide/mixins/EventedMixin',
    'xide/mixins/ReloadMixin',
    'xwire/Binding',
    'xwire/EventSource',
    'xwire/WidgetTarget'
], function (lang, on, dcl,register, CustomElement, Events, utils, Types, Referenced, EventedMixin, ReloadMixin, Binding, EventSource, WidgetTarget) {
    var debugWidgets = false;
    var debugApp = false;
    var debugAttach = false;
    var debugCreated = false;
    var debugBinding = false;
    var debugRun = false;
    /**
     * Proxy widget to run a selected blox script on the parent widget/node.
     *
     * @class xblox/RunScript
     */
    var Impl = {
        declaredClass: 'xblox/_State',
        script:"",
        bidirectional: false,
        _targetBlock: null,
        _targetReference: null,
        _complete: false,
        enabled: true,
        stop: false,
        _events: [],
        context: null,
        name:"Default",
        isState:true,
        _isState:function(){
            return true;
        },
        /**
         * soft destroy
         */
        reset:function(){
            
        },
        getChildren: function () {
            return [];
        },
        /**
         * @inheritDoc
         */
        destroy: function () {
            this.onDestroy && this.onDestroy();
            this.reset();
        },
        /**
         * The final execution when 'target event' has been triggered. This
         * will run the select block.
         * @param event
         * @param val
         */
        run: function (event, val) {
            if (!this.enabled) {
                return;
            }            
        },
        /**
         * Callback when the minimum parameters are given: targetReference & targetBlock
         */
        onReady: function () {            
        },
        getEnclosingWidget: function (node) {
            if(node) {
                do {
                    if (node.nodeType === 1 && node.render) {
                        return node;
                    }
                } while ((node = node.parentNode));
            }
            return null;
        },
        initWithReference: function (ref) {
            //target node or widget
            if(ref.nodeType!==1){
                return;
            }
            this._targetReference = ref;
        },
        /**
         * Function to setup the target reference
         * on the surrounding widget!
         *
         */
        _setupTargetReference: function () {
            var i = 0,
                element = this,
                widget = null;

            while (i < 2 && !widget) {
                if (element) {
                    element = element.parentNode;
                    widget = this.getEnclosingWidget(element, "widgetId");
                    if (!widget) {
                        widget = this.getEnclosingWidget(element, "widgetid");
                    }
                }
                i++;
            }
            if (widget) {
                debugWidgets &&  0 && console.info('have widget reference' + '  : ', [widget,this]);
                this.initWithReference(widget);
                if(widget._attached && widget.stateReady){
                    widget.stateReady(this);
                }

            } else {
                if (this.domNode && this.domNode.parentNode) {
                    this.initWithReference(this.domNode.parentNode);
                    debugWidgets && console.error('cant find widget reference, using parent node', this._targetReference);
                } else {
                    if(this.parentNode){
                        this.initWithReference(this.parentNode);
                    }
                    debugWidgets && console.error('cant find widget reference', this);
                }
            }
        },
        onAppReady: function (evt) {
            debugApp &&  0 && console.log('-ready');
            //resolve target reference
            //if (!this._targetReference) {
                this._setupTargetReference();
            //}

            //track context {xapp/manager/Context}
            if (evt && evt.context) {
                this.context = evt.context;
            }
        },
        detachedCallback:function(){
            debugAttach &&  0 && console.info('detachedCallback', this);
            if(this._appContext){
                this.destroy();
            }

        },
        applyTo:function(widget){
            
        },
        /**
         * Delite created callback
         */
        createdCallback: function () {
            debugCreated &&  0 && console.info('createdCallback', this);
            if (!this._targetReference) {
                this._setupTargetReference();
                if(this._targetReference && this._targetReference.stateReady){
                    this._targetReference.stateReady(this);
                }
            }
        },
        attachedCallback: function () {
            debugAttach &&  0 && console.info('attachedCallback', this);
            if (this._started) {
                return;
            }
            this.onAppReady();//emulates
            this.subscribe(Types.EVENTS.ON_APP_READY);
            this._started = true;
        }

    };
    //package and declare via dcl
    var _class = dcl([EventedMixin.dcl,Referenced.dcl], Impl);
    return _class; 
});
},
'xblox/model/html/SetState':function(){
define([
    "dcl/dcl",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xblox/model/Referenced',
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/_base/Color",
    "xide/registry"
], function (dcl, Block, utils, types, EventedMixin, Referenced, domAttr, domStyle, Color, registry) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    var Impl = {
        declaredClass: "xblox.model.html.SetState",
        name: 'Set State',
        reference: '',
        references: null,
        description: 'Switches to a state',
        value: '',
        mode: 1,
        sharable: false,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Run this block
         * @param scope
         * @param settings
         */
        solve: function (scope, settings) {
            var value = this.value;
            settings = settings || {};
            settings.flags = types.CIFLAG.DONT_PARSE;
            var objects = this.resolveReference(this.deserialize(this.reference), settings);
            if (this.override && this.override.variables) {
                value = utils.replace(value, null, this.override.variables, {
                    begin: '{',
                    end: '}'
                });
            }
            if (objects && objects.length) {
                _.each(objects, function (object) {
                    var widget = object
                    var _widget = registry.byId(widget.id) || widget;
                    if (widget != _widget) {

                    }
                    if (_widget && _widget.setState) {
                        _widget.setState(value);
                    }
                });
            }
            this.onSuccess(this, settings);
            this.onDidRun(); //clear overrides
        },
        /**
         * Get human readable string for the UI
         * @returns {string}
         */
        toText: function () {
            var _ref = this.deserialize(this.reference);
            var result = this.getBlockIcon() + ' ' + this.name + ' :: on ' + (_ref.reference || 'this' ) + ' to' || ' ' + ' to ';
            if (this.value) {
                result += ' ' + this.value;
            }
            return result;
        },
        /**
         * Standard call when editing this block
         * @returns {*}
         */
        getFields: function () {
            var fields = this.getDefaultFields(false);
            var referenceArgs = {
                group: 'General',
                dst: 'reference',
                value: this.reference
            };
            fields.push(utils.createCI('State', types.ECIType.STRING, this.value, {
                group: 'General',
                dst: 'value',
                value: this.value,
                intermediateChanges: false
            }));
            fields.push(utils.createCI('Target', types.ECIType.WIDGET_REFERENCE, this.reference, referenceArgs));
            return fields;
        },
        getBlockIcon: function () {
            return '<span class="fa-paint-brush"></span>';
        },
        getPropValue: function (stylesObject, prop) {
            for (var _prop in stylesObject) {
                if (_prop === prop) {
                    return stylesObject[_prop];
                }
            }
            return null;
        },
        updateObject: function (obj, style, mode) {
            if (!obj) {
                return false;
            }
            mode = mode || 1;
            if (obj.domNode != null) {
                obj = obj.domNode;
            }
            var currentStyle = domAttr.get(obj, 'style');
            if (currentStyle === ";") {
                currentStyle = "";
            }
            if (currentStyle === "") {
                if (obj['lastStyle'] != null) {
                    currentStyle = obj['lastStyle'];
                } else {
                    currentStyle = style;
                }
            }

            if (currentStyle === ";") {
                currentStyle = style;
            }
            switch (mode) {
                //set
                case 1:
                    {
                        var currentStyleMap = this._toObject(currentStyle);
                        var props = style.split(';');
                        for (var i = 0; i < props.length; i++) {
                            var _style = props[i].split(':');
                            if (_style.length == 2) {
                                currentStyleMap[_style[0]] = _style[1];
                            }
                        }
                        var styles = [];
                        for (var p in currentStyleMap) {
                            styles.push(p + ':' + currentStyleMap[p]);
                        }
                        $(obj).attr('style', styles.join(';'));
                        break;
                    }
                    //add
                case 2:
                    {
                        var _newStyle = currentStyle + ';' + style,
                            _newStyleT = _.uniq(_newStyle.split(';')).join(';');
                        domAttr.set(obj, 'style', _newStyleT);
                        break;
                    }
                    //remove
                case 3:
                    {
                        domAttr.set(obj, 'style', utils.replaceAll(style, '', currentStyle));
                        break;
                    }
                    //increase
                case 4:
                    //decrease
                case 5:
                    {
                        var numbersOnlyRegExp = new RegExp(/(\D*)(-?)(\d+)(\D*)/);
                        /**
                         * compute current style values of the object
                         * @type {{}}
                         */
                        var stylesRequested = this._toObject(style);
                        var stylesComputed = {};
                        var jInstance = $(obj);
                        ///determine from node it self
                        if (stylesRequested) {
                            for (var prop in stylesRequested) {
                                stylesComputed[prop] = this._getStyle(prop, obj, jInstance);
                            }
                        }

                        var _newStyleObject = {};
                        /**
                         * compute the new style
                         * @type {number}
                         */
                        for (var prop in stylesRequested) {
                            var _prop = '' + prop.trim();
                            var multiplicator = 1;
                            if (stylesComputed[_prop] != null) {

                                var _valueRequested = stylesRequested[prop];
                                var _valueComputed = stylesComputed[prop];

                                var _isHex = _valueRequested.indexOf('#') != -1;
                                var _isRGB = _valueRequested.indexOf('rgb') != -1;
                                var _isRGBA = _valueRequested.indexOf('rgba') != -1;

                                if (_isHex || _isRGB || _isRGBA) {

                                    var dColorMultiplicator = dojo.colorFromString(_valueRequested);
                                    var dColorNow = dojo.colorFromString(_valueRequested);
                                    var dColorComputed = dojo.colorFromString(_valueComputed);
                                    var dColorNew = new Color();

                                    _.each(["r", "g", "b", "a"], function (x) {
                                        dColorNew[x] = Math.min(dColorComputed[x] + dColorMultiplicator[x], x == "a" ? 1 : 255);
                                    });

                                    var _valueOut = '';
                                    if (_isHex) {
                                        _valueOut = dColorNew.toHex();
                                    } else if (_isRGB) {
                                        _valueOut = dColorNew.toCss(false);
                                    } else if (_isRGBA) {
                                        _valueOut = dColorNew.toCss(true);
                                    }
                                    _newStyleObject[prop] = _valueOut;
                                    domStyle.set(obj, prop, _valueOut);


                                } else {
                                    //extract actual number :
                                    var numberOnly = numbersOnlyRegExp.exec(stylesComputed[_prop]);
                                    if (numberOnly && numberOnly.length >= 3) {
                                        var _int = parseInt(numberOnly[3]);
                                        if (_int && _int > 0) {
                                            multiplicator = _int;
                                        }
                                    }
                                }
                            }
                        }
                        //now get an object array of the styles we'd like to alter
                        var styles = this._toObject(currentStyle);
                        if (!styles) {
                            return false;
                        }
                        break;
                    }
            }
        },
        activate: function () {
            this._destroy(); //you never know
        },
        deactivate: function () {
            this._destroy();
        }
    };

    //package via declare
    var _class = dcl([Block, EventedMixin.dcl, Referenced.dcl], Impl);
    //static access to Impl.
    _class.Impl = Impl;
    return _class;

});
},
'xblox/views/BlockGridPalette':function(){
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xblox/views/BlockGrid',
    'xblox/BlockActions',
    'xide/views/_LayoutMixin',
    "xide/widgets/_Widget",
    'module',
    'xide/factory',
    'xblox/views/ThumbRenderer',
    "xblox/views/DnD",
    'dojo/dom-construct',
    'xgrid/Selection',
    'xgrid/KeyboardNavigation',
    'xaction/ActionStore',
    'xlang/i18',
    'xide/$',
    'xide/_base/_Widget',
    'xdojo/has',
    'dojo/has!debug?xide/tests/TestUtils'
], function (dcl, declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, Editor, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, BlockGrid,
             BlockActions, _LayoutMixin, _Widget, module, factory, ThumbRenderer, BlocksDnD, domConstruct,
             Selection, KeyboardNavigation, ActionStore, i18, $, _BWidget, has, TestUtils) {

    var ThumbClass = declare('xblox.ThumbRenderer2', [ThumbRenderer], {
        thumbSize: "400",
        resizeThumb: true,
        __type: 'thumb',
        deactivateRenderer: function () {
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding', '');
            this.isThumbGrid = false;

        },
        activateRenderer: function () {
            $(this.contentNode).css('padding', '8px');
            this.isThumbGrid = true;
            this.refresh();
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            var div = domConstruct.create('div', {
                    className: "tile widget form-control",
                    style: 'margin-left:8px;margin-top:8px;margin-right:8px;width:150px;height:initial;max-width:200px;float:left;padding:8px'
                }),
                label = obj.title,
                imageClass = obj.icon || 'fa fa-folder fa-2x';

            var iconStyle = 'float:left; margin-left:3px;margin-top:3px;margin-right:3px;text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: inherit;opacity: 0.4';
            var folderContent = '<span style="' + iconStyle + '" class="' + imageClass + '"></span>';
            div.innerHTML =
                '<div class="opacity">' +
                folderContent +
                '<span class="thumbText text opacity ellipsis" style="text-align:center">' + label + '</span>' +
                '</div>';
            return div;
        }
    });

    var GroupRenderer = declare('xblox.GroupRenderer', TreeRenderer, {
        itemClass: ThumbClass,
        groupClass: TreeRenderer,
        renderRow: function (obj) {
            return (!obj.group ? this.itemClass : this.groupClass).prototype.renderRow.apply(this, arguments);
        }
    });
    var Implementation = {},
        renderers = [GroupRenderer, ThumbClass, TreeRenderer],
        multiRenderer = declare.classFactory('xgrid/MultiRendererGroups', {}, renderers, MultiRenderer.Implementation);

    //console.clear();

    var SharedDndGridSource = BlocksDnD.BlockGridSource;
    var GridClass = Grid.createGridClass('xblox.views.BlockGridPalette', Implementation, {
            SELECTION: {
                CLASS: Selection
            },
            KEYBOARD_SELECTION: true,
            CONTEXT_MENU: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            WIDGET: {
                CLASS: _Widget
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            Dnd: {
                CLASS: BlocksDnD
            }
        },
        {
            RENDERER: multiRenderer
        },
        {
            renderers: renderers,
            selectedRenderer: GroupRenderer
        }
    );


    var HelpWidget = dcl(_BWidget, {
        templateString: '<div class="xbloxHelp"></div>',
        startup: function () {
        }
    });
    var GridCSourcelass = declare('ActionGrid', GridClass, {
        dndParams: {
            allowNested: false, // also pick up indirect children w/ dojoDndItem class
            checkAcceptance: function (source, nodes) {
                return false;//source !== this; // Don't self-accept.
            },
            isSource: true
        },
        formatColumn: function (field, value, obj) {
            var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
            if (renderer.formatColumn_) {
                var result = renderer.formatColumn.apply(arguments);
                if (result) {
                    return result;
                }
            }
            if (obj.renderColumn_) {
                var rendered = obj.renderColumn.apply(this, arguments);
                if (rendered) {
                    return rendered;
                }
            }
            switch (field) {
                case "title": {
                    value = obj.group ? ('<span style="float:left;margin-right: 10px">' + value + '</span><hr style="margin-top: 13px;margin-left: 4px"/>') : value;
                }
            }
            return value;
        },
        _columns: {},
        postMixInProperties: function () {
            var state = this.state;
            if (state) {
                if (state._columns) {
                    this._columns = state._columns;
                }
            }
            this.columns = this.getColumns();
            var newBlockActions = this.getAddActions();
            var levelName = '';
            var result = [];
            var BLOCK_INSERT_ROOT_COMMAND = '';
            var defaultMixin = {addPermission: true};
            var rootAction = BLOCK_INSERT_ROOT_COMMAND;
            var thiz = this;

            function addItems(commandPrefix, items) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    levelName = item.name;
                    var path = commandPrefix ? commandPrefix + '/' + levelName : levelName;
                    var isContainer = !_.isEmpty(item.items);
                    result.push(thiz.createAction({
                        label: levelName,
                        command: path,
                        icon: item.iconClass,
                        tab: 'Home',
                        mixin: utils.mixin({
                            item: item,
                            parent: isContainer ? "root" : commandPrefix,
                            quick: true,
                            title: i18.localize(levelName),
                            group: isContainer,
                            icon: item.iconClass,
                            toDropObject: function (item, targetItem, params) {
                                var blockArgs = item.item;
                                var ctrArgs = blockArgs.ctrArgs;
                                ctrArgs.id = null;
                                ctrArgs.items = null;
                                ctrArgs.scope = params.targetGrid.blockScope;
                                var proto = blockArgs.proto;
                                var parent = params.center === true && targetItem ? targetItem : null;
                                if (parent) {
                                    var block = factory.createBlock(proto, ctrArgs);//root block
                                    return parent.add(block, null, null);

                                } else {
                                    ctrArgs.group = params.targetGrid.blockGroup;
                                }
                                return factory.createBlock(proto, ctrArgs);//root block
                            }
                        }, defaultMixin)

                    }));

                    if (isContainer) {
                        addItems(path, item.items);
                    }
                }
            }

            addItems(rootAction, newBlockActions);
            var actionStore = new ActionStore({
                parentProperty: 'parent',
                parentField: 'parent',
                idProperty: 'command',
                mayHaveChildren: function (parent) {
                    if (parent._mayHaveChildren === false) {
                        return false;
                    }
                    return true;
                }
            });
            actionStore.setData(result);
            var all = actionStore.query(null);
            _.each(all, function (item) {
                item._store = actionStore;
            });
            var $node = $(this.domNode);
            $node.addClass('xFileGrid metro');
            this.collection = actionStore.filter({
                parent: "root"
            });
            return this.inherited(arguments);
        },
        getColumns: function (formatters) {
            var self = this;
            this.columns = [
                {
                    renderExpando: true,
                    label: 'Name',
                    field: 'title',
                    sortable: true,
                    formatter: function (value, obj) {
                        return self.formatColumn('title', value, obj);
                    },
                    hidden: false
                }
            ];
            return this.columns;
        },
        getAddActions: function (item) {
            var thiz = this;
            item = item || {};
            var items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
            //tell everyone
            thiz.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this,
                item: item,
                group: this.blockGroup,
                scope: this.blockScope
            });

            thiz._emit(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this
            });
            return items;


        },
        startup: function () {
            this._showHeader(false);
            
            this._on('selectionChanged', function (evt) {
                var selection = evt.selection,
                    item = selection[0];
                var view = this.lastSource;
                if (item && view && item.item.proto && item.item.proto.prototype) {
                    view.clearPropertyPanel();
                    var help = view.getHelp(item.item.proto.prototype);
                    if (help) {
                        var props = view.getPropertyStruct();
                        var target = view.__right;
                        var widget = utils.addWidget(HelpWidget, {}, null, target.containerNode, true);
                        $(widget.domNode).append(help);
                        props.targetTop = widget;
                    }
                }
            });
            var res = this.inherited(arguments);
            _.each(this.getRows(), function (row) {
                this.expand(row);
            }, this);
            $(this.domNode).addClass('blockPalette');
            return res;
        }
    });

    var ctx = window.sctx,
        root;

    function createContainer(ctx, title) {
        var leftContainer = ctx.mainView.leftLayoutContainer;
        return leftContainer.createTab(title, 'fa-cube', true, null, {
            parentContainer: leftContainer,
            open: true,
            icon: 'fa-folder'
        });
    }

    function handler(e, ctx) {
        function check() {
            if (ctx.blockPalette && ctx.blockPalette.sources.length === 0) {
                utils.destroy(ctx.blockPalette);
                utils.destroy(ctx.blockPalette._parent);
                ctx.blockPalette = null;
            }
        }

        function wireView(view) {
            if (ctx.blockPalette.sources.indexOf(view) === -1) {
                ctx.blockPalette.sources.push(view);
                view._on('destroy', function () {
                    ctx.blockPalette && ctx.blockPalette.sources.remove(view);
                    check();
                })
            }
        }

        if (ctx.blockPalette) {
            ctx.blockPalette.lastSource = e.view;
            wireView(e.view);
            return;
        }

        var target = createContainer(ctx, 'Block-Palette');
        var grid2 = utils.addWidget(GridCSourcelass, {
            ctx: ctx,
            attachDirect: true,
            open: true,
            sources: []
        }, null, target, true);
        target.add(grid2);
        ctx.blockPalette = grid2;
        grid2.lastSource = e.view;
        wireView(e.view);
    }

    function init(ctx) {
        function inner(e) {
            handler(e, ctx);
        }
        ctx.subscribe('ON_SELECTED_BLOCK', inner);
        ctx.subscribe('ON_BLOCK_GRID', inner);

    }
    var test = false;
    if (test && has('debug')) {
        function createScope() {
            var data = {
                "blocks": [
                    {
                        "_containsChildrenIds": [
                            "items"
                        ],
                        "group": "click",
                        "id": "root",
                        "items": [
                            "sub0",
                            "sub1"
                        ],
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 1",
                        "method": " 0 && console.log('asd',this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "icon": 'fa-bell text-fatal',
                        "_scenario": "update"
                    },

                    {
                        "group": "click4",
                        "id": "root4",
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 4",
                        "method": " 0 && console.log(this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0
                    },
                    {
                        "group": "click",
                        "id": "root2",
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 2",
                        "method": " 0 && console.log(this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0

                    },

                    {
                        "group": "click",
                        "id": "root3",
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 3",
                        "method": " 0 && console.log(this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        'icon': 'fa-play'

                    },
                    {
                        "_containsChildrenIds": [],
                        "parentId": "root",
                        "id": "sub0",
                        "name": "On Event",
                        "event": "",
                        "reference": "",
                        "declaredClass": "xblox.model.events.OnEvent",
                        "_didRegisterSubscribers": false,
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "_scenario": "update"
                    },
                    {
                        "_containsChildrenIds": [],
                        "parentId": "root",
                        "id": "sub1",
                        "name": "On Event2",
                        "event": "",
                        "reference": "",
                        "declaredClass": "xblox.model.events.OnEvent",
                        "_didRegisterSubscribers": false,
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "_scenario": "update"
                    },
                    {
                        "_containsChildrenIds": [],
                        "group": "click",
                        "id": "6a12d54a-f5af-aaf8-0abb-bff866211fc4",
                        "declaredClass": "xblox.model.logic.SwitchBlock",
                        "name": "Switch",
                        "outlet": 0,
                        "enabled": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "order": 0,
                        "type": "added"
                    },
                    {
                        "_containsChildrenIds": [],
                        "group": "click",
                        "id": "8e1c9d2a-d2fe-356f-d484-82e27c793dc9",
                        "declaredClass": "xblox.model.variables.VariableSwitch",
                        "name": "Switch on Variable",
                        "icon": "",
                        "variable": "PowerState",
                        "outlet": 0,
                        "enabled": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "order": 0,
                        "type": "added"
                    }
                ],
                "variables": []
            };
            return blockManager.toScope(data);
        }
        if (ctx) {
            var blockManager = ctx.getBlockManager();
            if(!blockManager){
                return;
            }

            function createGridClass() {
                var renderers = [TreeRenderer, ThumbRenderer];

                var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
                return Grid.createGridClass('driverTreeView', {
                        options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                    },
                    //features
                    {

                        SELECTION: true,
                        KEYBOARD_SELECTION: true,
                        PAGINATION: false,
                        ACTIONS: types.GRID_FEATURES.ACTIONS,
                        CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                        TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                        CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                        SPLIT: {
                            CLASS: _LayoutMixin
                        },
                        Dnd: {
                            CLASS: BlocksDnD
                        },
                        Base: {
                            CLASS: declare('Base', null, BlockGrid.IMPLEMENTATION)
                        },
                        BLOCK_ACTIONS: {
                            CLASS: BlockActions
                        },
                        WIDGET: {
                            CLASS: _Widget
                        }
                    },
                    {
                        //base flip
                        RENDERER: multiRenderer

                    },
                    {
                        //args
                        renderers: renderers,
                        selectedRenderer: TreeRenderer
                    },
                    {
                        GRID: OnDemandGrid,
                        EDITOR: Editor,
                        LAYOUT: Layout,
                        DEFAULTS: Defaults,
                        RENDERER: ListRenderer,
                        EVENTED: EventedMixin,
                        FOCUS: Focus
                    }
                );
            }

            var blockScope = createScope('docs');
            var mainView = sctx.mainView;
            if (mainView) {
                if (window['dHandle']) {
                    window['dHandle'].remove();
                }
                if (window['dHandle2']) {
                    window['dHandle2'].remove();
                }
                window['dHandle'] = ctx.subscribe('ON_SELECTED_BLOCK', handler);
                window['dHandle2'] = ctx.subscribe('ON_BLOCK_GRID', handler);

                var parent = TestUtils.createTab('BlockGrid-Test-Source', null, module.id);
                var thiz = this,
                    grid, grid2;
                var store = blockScope.blockStore;
                var _gridClass = createGridClass();
                var dndParams = {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                };
                var gridArgs = {
                    ctx: ctx,
                    /**
                     * @type {module:xblox/views/SharedDndGridSource}
                     */
                    dndConstructor: SharedDndGridSource, // use extension defined above
                    blockScope: blockScope,
                    blockGroup: 'click',
                    dndParams: dndParams,
                    name: "Grid - 1",
                    collection: store.filter({
                        group: "click"
                    })
                };
                var gridArgs2 = {
                    ctx: ctx,
                    attachDirect: true,
                    open: true

                };
                grid = utils.addWidget(_gridClass, gridArgs, null, parent, true);

            }
        }
    }
    GridCSourcelass.init = init;
    return GridCSourcelass;
});
}}});
