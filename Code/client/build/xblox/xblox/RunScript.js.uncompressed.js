define("xblox/RunScript", [
    "dojo/_base/lang",
    "dojo/on",
    "dcl/dcl",//make sure
    "delite/register",
    "delite/CustomElement",
    //explicit because a bootstrap might not be loaded at some point
    "xide/factory/Events",
    //explicit because a bootstrap might not be loaded at some point
    'xide/utils/StringUtils',
    'xide/types/Types',
    'xblox/model/Referenced',
    'xide/mixins/EventedMixin',
    'xide/mixins/ReloadMixin',
    /** 2way binding dependencies **/
    'xwire/Binding',
    'xwire/EventSource',
    'xwire/WidgetTarget'

], function (lang, on, dcl, register, CustomElement, Events, utils, Types, Referenced, EventedMixin, ReloadMixin, Binding, EventSource, WidgetTarget, registry) {

    var debugWidgets = false;
    var debugApp = false;
    var debugAttach = false;
    var debugCreated = false;
    var debugBinding = false;
    var debugRun = false;
    /**
     * Proxy widget to run a selected blox script on the parent widget/node.
     *
     * @class xblox/RunScript
     */
    var Impl = {
        declaredClass: 'xblox/RunScript',
        targetevent: '',
        sourceevent: "",
        sourceeventvaluepath: "",
        sourceeventnamepath: "",
        targetproperty: "",
        targetvariable: "",
        targetfilter: "",
        script: "",
        bidirectional: false,
        blockGroup: '',
        block: '',
        _targetBlock: null,
        _targetReference: null,
        _appContext: null,
        _complete: false,
        enabled: true,
        stop: false,
        _events: [],
        context: null,
        accept: '',
        transform: '',
        mode: 0,
        _2wayHandle: null,//the handle
        binding: null,//the binding
        /**
         * soft destroy
         */
        reset: function () {
            this._destroyHandles();
            if (this._2wayHandle) {
                this._2wayHandle.remove();
            }
            if (this.binding) {
                this.binding.destroy();
            }
            delete this.binding;
            this._appContext = null;
            this._targetReference = null;
        },
        /**
         *
         * @param newSettings
         */
        onSettingsChanged: function () {
            this.reset();
            if (!this.enabled) {
                return;
            }
            this.onAppReady(null);
        },
        getChildren: function () {
            return [];
        },
        /**
         * @inheritDoc
         */
        destroy: function () {
            this.onDestroy && this.onDestroy();
            this.reset();
            delete this.binding;
            delete this.context;
        },
        /**
         * The final execution when 'target event' has been triggered. This
         * will run the select block.
         * @param event
         * @param val
         */
        run: function (event, val) {
            if (!this.enabled) {
                return;
            }
            var settings = {};
            //filter, in design mode, we ain't do anything
            if (this.context && this.context.delegate) {
                if (this.context.delegate.isDesignMode && this.context.delegate.isDesignMode()) {
                    return;
                }
                if (this.context.delegate.getBlockSettings) {
                    settings = this.context.delegate.getBlockSettings();
                }
            }
            //setup variables
            var block = this._targetBlock,
                context = this._targetReference,
                result;

            if (block && context) {
                block.context = context;
                block._targetReference = context;
                if (this.targetvariable && this.targetvariable.length && val != null) {
                    block.override = {
                        variables: {}
                    };
                    block.override.variables[this.targetvariable] = val;
                }
                result = block.solve(block.scope, settings);
                debugRun && console.log('run ' + block.name + ' for even ' + event, result + ' for ' + this.id, this._targetReference);
            }
        },
        /**
         * Callback when the minimum parameters are given: targetReference & targetBlock
         */
        onReady: function () {
            if (!this._targetReference) {
                this._setupTargetReference();
            }

            //resolve 2way binding
            if (this._targetReference && this['bidirectional'] === true && this.sourceevent && this.sourceevent.length && !this.binding) {
                this._setup2WayBinding();
            }

            if (this._complete) {
                return;
            }
            if (!this._targetReference) {
                console.error('have no target reference');
            }
            if (!this._targetBlock) {
                console.error('have no target block');
            }

            if (this._targetReference && this._targetBlock) {
                //we trigger on events
                if (this.targetevent) {
                    this._complete = true;
                    //patch the target
                    utils.mixin(this._targetReference, EventedMixin.prototype);
                    var _target = this._targetReference.domNode || this._targetReference,
                        _event = this.targetevent,
                        _isWidget = this._targetReference.declaredClass || this._targetReference.startup,
                        _hasWidgetCallback = this._targetReference.on != null && this._targetReference['on' + utils.capitalize(_event)] != null,
                        _handle = null,
                        _isDelite = _target.render != null && _target.on != null,
                        thiz = this;

                    if (_isWidget && (this._targetReference.baseClass && this._targetReference.baseClass.indexOf('dijitContentPane') != -1) || this._targetReference.render != null || this._targetReference.on != null) {
                        _isWidget = false;//use on
                    }

                    if (_target) {
                        debugBinding && console.log('wire success ' + this.id + ' for ' + this.targetevent);
                        if (!_isDelite && (!_hasWidgetCallback || !_isWidget)) {
                            _handle = on(_target, this.targetevent, function (evt) {
                                this.run(this.targetevent);
                            }.bind(this));
                        } else {
                            _target = this._targetReference;
                            var useOn = true;
                            if (useOn) {
                                if (!_isDelite) {
                                    var _e = 'on' + utils.capitalize(_event);
                                    this._targetReference[_e] = function (val, nada) {
                                        if (_target.ignore !== true) {
                                            thiz.run(thiz.targetevent, val);
                                        }
                                    };
                                } else {
                                    _handle = _target.on(this.targetevent, function (evt) {
                                        if (this.stop) {
                                            evt.preventDefault();
                                            evt.stopImmediatePropagation();
                                        }
                                        this.run(this.targetevent, evt.currentTarget.value);
                                    }.bind(this));
                                }
                            } else {
                                this._targetReference['on' + utils.capitalize(_event)] = function (val) {
                                    if (_target.ignore !== true) {
                                        thiz.run(thiz.targetevent, val);
                                    }
                                };
                            }
                        }
                        _handle && this._events.push(_handle);
                    } else {
                        console.error('have no target to wire');
                    }
                }
            } else {
                console.error('invalid params, abort', this);
            }
            if (this.binding) {
                this.binding.start();
            }
        },
        resolveBlock: function (block) {
            var ctx = this._appContext;
            var deviceManager = ctx.getDeviceManager();
            if (block.indexOf('://') !== -1) {
                if (!deviceManager) {
                    return;
                }
                var _block = deviceManager.getBlock(this.block);
                if (_block) {
                    return _block;
                }
            }
        },
        /**
         *
         * @param ctx
         * @private
         */
        _setBlock: function (ctx) {
            ctx = ctx || window['appContext'];
            if (!ctx || !ctx.getBlockManager) {
                debugApp && console.warn('have no context or block manager');
                return;
            }
            this._appContext = ctx;
            var blockManager = ctx.getBlockManager(),
                deviceManager = ctx.getDeviceManager(),
                thiz = this;

            if (!blockManager) {
                return;
            }
            var _block = this.block ? this.block : this.getAttribute('block');
            if (_block && _block.length > 0) {
                var parts = utils.parse_url(_block);
                if (_block.indexOf('://') !== -1) {
                    if (!deviceManager) {
                        debugApp && console.warn('xScript::_setBlock : have no device manager');
                        return;
                    }
                    var _block2 = deviceManager.getBlock(_block);
                    if (_block2) {
                        thiz._targetBlock = _block2;
                        thiz.onReady();
                    } else {
                        debugBinding && console.warn('cant get block : ' + _block);
                    }
                } else {
                    blockManager.load(parts.scheme, parts.host).then(function (scope) {
                        var block = scope.getBlockById(thiz.blockid);
                        if (block) {
                            thiz._targetBlock = block;
                            thiz.onReady();
                        }
                    });
                }
            } else if (this.scopeid) {
                var scope = blockManager.hasScope(thiz.scopeid);
                if (scope) {
                    var block = scope.getBlockById(thiz.blockid);
                    if (block) {
                        thiz._targetBlock = block;
                        thiz.onReady();
                    } else {
                        block = scope.getVariableById(thiz.blockid);
                        if (block) {
                            thiz._targetBlock = block;
                            thiz.onReady();
                        }
                    }
                } else {
                    console.error('have no scope!');
                }
            }
        },
        initWithReference: function (ref) {
            if (ref.nodeType !== 1) {
                return;
            }
            this._targetReference = ref;
            this._setBlock(null);
        },
        resolveFilter: function (expression, value, widget) {
            if (this._targetBlock) {
                var expressionModel = this._targetBlock.scope.expressionModel;
                value = expressionModel.parseVariable(this._targetBlock.scope, {
                    value: expression
                }, '', false, false, widget, [value]);
            }
            return value;
        },
        /**
         * setup outbound wire, assumes all parameters are checked
         * @private
         */
        _setup1WayBinding: function () {
            debugBinding && console.log('setup 1 way binding');
            //destroy old handle
            if (this._2wayHandle) {
                this._2wayHandle.remove();
            }

            if (!this._targetBlock) {
                console.error('invalid params for one way binding');
                return;
            }
            var sourceVariableTitle = this._targetBlock.name;
            //wire to system event
            var bindingSource = new EventSource({
                //listen to variable changes
                trigger: this.sourceevent,
                //the path to value, ie: 'item.value'
                path: this.sourceeventvaluepath,
                //add an event filter
                filters: [{
                    // variable title must match,ie: 'item.title'
                    path: this.sourceeventnamepath,
                    // the name of the variable, ie: 'Volume'
                    value: sourceVariableTitle
                }]
            });


            //now map the event source to a widget
            var bindingTarget = new WidgetTarget({
                //the path to value
                path: this.targetproperty,
                object: this._targetReference,
                targetFilter: this.targetfilter,
                delegate: this
            });
            var accept = this._findbyTagAndName('D-SCRIPT', 'accept');
            var transform = this._findbyTagAndName('D-SCRIPT', 'transform');
            //construct the binding
            var binding = new Binding({
                source: bindingSource,
                target: bindingTarget,
                accept: this._findbyTagAndName('D-SCRIPT', 'accept'),
                transform: this._findbyTagAndName('D-SCRIPT', 'transform')
            });
            this.binding = binding;
            binding.start();
        },
        _findbyTagAndName: function (tag, name) {
            var scripts = $(this).find(tag);
            for (var i = 0; i < scripts.length; i++) {
                var script = scripts[i];
                if ($(script).attr('name') === name) {
                    return script;
                }
            }
            return null;
        },
        /**
         * setup inbound wire, assumes all parameters are checked
         * @private
         */
        _setup2WayBinding: function () {
            if (this.binding) {
                return;
            }
            debugBinding && console.log('setup 2 way binding');
            //destroy old handle
            if (this._2wayHandle) {
                this._2wayHandle.remove();
            }
            //wire to system event
            var bindingSource = new EventSource({
                //listen to variable changes
                trigger: this.sourceevent,
                //the path to value, ie: 'item.value'
                path: this.sourceeventvaluepath,
                //add an event filter
                filters: [{
                    // variable title must match,ie: 'item.title'
                    path: this.sourceeventnamepath,
                    // the name of the variable, ie: 'Volume'
                    value: this.targetvariable
                }]
            });

            //now map the event source to a widget
            var bindingTarget = new WidgetTarget({
                //the path to value
                path: 'value',
                object: this._targetReference
            });
            this.binding = new Binding({
                source: bindingSource,
                target: bindingTarget
            });
            this.binding.start();
        },
        /**
         * Returns the widget whose DOM tree contains the specified DOMNode, or null if
         * the node is not contained within the DOM tree of any widget
         * @param {Element} node
         */
        getEnclosingWidget: function (node) {
            if (node) {
                do {
                    if (node.nodeType === 1 && node.render) {
                        return node;
                    }
                } while ((node = node.parentNode));
            }
            return null;
        },
        /**
         * Function to setup the target reference
         * on the surrounding widget!
         *
         */
        _setupTargetReference: function () {
            var i = 0,
                element = this,
                widget = null;

            while (i < 2 && !widget) {
                if (element) {
                    element = element.parentNode;
                    widget = this.getEnclosingWidget(element, "widgetId");
                    if (!widget) {
                        widget = this.getEnclosingWidget(element, "widgetid");
                    }
                }
                i++;
            }
            if (widget) {
                debugWidgets && console.info('have widget reference' + '  : ', widget);
                this.initWithReference(widget);
            } else {
                if (this.domNode && this.domNode.parentNode) {
                    this.initWithReference(this.domNode.parentNode);
                    debugWidgets && console.error('cant find widget reference, using parent node', this._targetReference);
                } else {
                    if (this.parentNode) {
                        this.initWithReference(this.parentNode);
                    }
                    debugWidgets && console.error('cant find widget reference', this);
                }

            }
        },
        /**
         * Required in case of dojoConfig.parseOnLoad
         * @param evt
         */
        onAppReady: function (evt) {
            debugApp && console.log('-ready');
            //resolve target reference
            if (!this._targetReference) {
                this._setupTargetReference();
            }
            //resolve target block
            if (!this._targetBlock) {
                this._setBlock(evt ? evt.context : null);
            }

            this.mode = this['bidirectional'] === true ? 0 : 1;
            //normal mode, allows 2-way binding
            if (this.mode === 0) {
                //resolve 2way binding
                if (this._targetBlock && this._targetReference && this['bidirectional'] === true && this.sourceevent && this.sourceevent.length) {
                    this._setup2WayBinding();
                }

                //if both are valid, run the the init procedure
                if (this._targetReference && this._targetBlock) {
                    this.onReady();
                }

            } else if (this.mode === 1 && this._targetBlock) {
                if (this._targetReference && this.sourceevent && this.sourceevent.length && this.targetproperty && this.targetproperty.length) {
                    this._setup1WayBinding();
                    if (this.binding) {
                        this.binding.start();
                    }
                }
            }
            //track context {xapp/manager/Context}
            if (evt && evt.context) {
                this.context = evt.context;
            }
        },
        detachedCallback: function () {
            debugAttach && console.info('detachedCallback', this);
            if (this._appContext) {
                this.destroy();
            }
        },
        /**
         * Delite created callback
         */
        createdCallback: function () {
            debugCreated && console.info('createdCallback', this);
        },
        /**
         * Delite attached callback
         */
        attachedCallback: function () {
            debugAttach && console.info('attachedCallback', this);
            if (this._started) {
                return;
            }
            this.initReload();
            this.subscribe(Types.EVENTS.ON_APP_READY);
            this._started = true;

        },
        detachCallback: function () {
        },
        render: function () {

        },
        postRender: function () {

        },
        startup: function () {
            debugAttach && console.log('startup');
            this.inherited(arguments);
            this.onAppReady();
            this.initReload();
            this.subscribe(Types.EVENTS.ON_APP_READY);

        }
    };

    //package and declare via dcl
    var _class = dcl([EventedMixin.dcl, ReloadMixin.dcl, Referenced.dcl], Impl);
    //static access to Impl.
    _class.Impl = Impl;
    return register("d-xscript", [HTMLElement, CustomElement, _class]);
});