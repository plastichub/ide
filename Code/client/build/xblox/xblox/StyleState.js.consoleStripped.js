define("xblox/StyleState", [
    'dcl/dcl',
    'delite/register',
    'delite/CustomElement',
    'xide/factory/Events',
    'xide/utils/StringUtils',
    'xide/types/Types',
    'xblox/_State'
], function (dcl,register, CustomElement, Events, utils, Types,_State) {
    var Impl = {
        declaredClass: 'xblox/StyleState',
        _targetReference: null,        
        name:"Default",
        _widget:null,
        /**
         * Convert Style String to an object array, eg: { color:value,.... }
         * @param styleString
         * @returns {{}}
         * @private
         */
        _toObject:function(styleString){
            if(!styleString){
                return {};
            }
            var _result = {};
            var _values = styleString.split(';');
            for (var i = 0; i < _values.length; i++) {
                var obj = _values[i];
                if(!obj || obj.length==0 || !obj.split){
                    continue;
                }
                var keyVal = obj.split(':');
                if(!keyVal || !keyVal.length){
                    continue;
                }
                var key = obj.substring(0,obj.indexOf(':')).trim();
                var value = obj.substring(obj.indexOf(':')+1,obj.length).trim();
                _result[key]=value;
            }
            return _result;
        },
        _toStyleString:function(values){
            var _values = [];
            for(var prop in values){
                _values.push( prop + ':' + values[prop]);
            }
            return _values.join(';') + ';';
        },
        onChanged:function () {
            this.applyTo(this._widget);
        },
        attachedCallback: function () {
            //if($(this).attr('style').indexOf('display')==-1){
               // this.style.display = 'none';
            //}
            // 
			/*
			 0 && console.log('attached ' + has('ide'));
			if(!has('ide')){
				var style = $(this).attr('style');
				var background = utils.getBackgroundUrl(style);
				 0 && console.log('style : '+background,this);
			}
			*/
        },
        _lastStyle:'',
        applyTo:function(widget){
            $(widget).removeClass($(widget).data('_lastCSSState'));
            $(widget).removeClass($(widget).data('_lastCSSClass'));
            if(widget && widget._attached){
                this._widget = widget;
                var _cssWidget = this._toObject($(widget).attr('style'));
                var _cssThis = this._toObject($(this).attr('style'));
                this._lastStyle = _cssThis;
                widget._lastStyle = _cssThis;
                var styleOut = utils.mixin(_cssWidget,_cssThis);
                $(widget).attr('style',this._toStyleString(styleOut));
            }
        }
    };
    var _class = dcl(_State, Impl);
    return register("d-xstate-style", [HTMLElement, CustomElement, _class]);
});