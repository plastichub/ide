/** module xblox/manager/BlockManagerUI **/
define("xblox/manager/BlockManagerUI", [
    'dcl/dcl',
    "xide/manager/BeanManager"
], function (dcl, BeanManager) {
    /**
     * @mixin module:xblox/manager/BlockManagerUI
     * @extends {module:xide/manager/BeanManager}
     */
    return dcl(BeanManager, {
        declaredClass: "xblox/manager/BlockManagerUI",
        init: function () {}
    });
});