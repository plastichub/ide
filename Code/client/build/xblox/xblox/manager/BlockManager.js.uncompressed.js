define("xblox/manager/BlockManager", [
    'dcl/dcl',
    'dojo/has',
    'dojo/Deferred',
    'dojo/promise/all',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xblox/model/ModelBase',
    'xblox/model/Scope',
    'xblox/model/BlockModel',
    'xide/mixins/ReloadMixin',
    'xide/manager/ManagerBase',
    'xblox/data/Store',
    "xdojo/has!xblox-ui?xblox/manager/BlockManagerUI",
    "xide/lodash"
],function (dcl,has,Deferred,all,types,utils,factory,ModelBase,Scope,BlockModel,ReloadMixin,ManagerBase,Store,BlockManagerUI,_){

    var bases =  1  &&  1  ? [BlockManagerUI,ManagerBase,ReloadMixin.dcl] : [ManagerBase,ReloadMixin.dcl];
    var debug = false;

    return dcl(bases,{
        declaredClass:"xblox/manager/BlockManager",
        serviceObject:null,
        loaded:{},
        test:function(){

        },
        /***
         *  scope: storage for all registered variables / commands
         */
        scope:null,
        scopes:null,
        //track original create block function
        _createBlock:null,
        _registerActions:function(){},
        toScope:function(data){
            try {
                data = utils.getJson(data);
            } catch (e) {
                console.error('BlockManager::toScope: failed,err='+e);
                return null;
            }

            if(!data){
                console.error('correct data');
                data = {
                    "blocks": [
                    ],
                    "variables": []
                };
            }
            var scopeId = utils.createUUID();
            var blockInData = data;
            //check structure
            if (_.isArray(data)) {// a flat list of blocks

            } else if (_.isObject(data)) {
                scopeId = data.scopeId || scopeId;
                blockInData = data.blocks || [];
            }
            var scopeUserData = {
                owner:this
            };
            var blockScope = this.getScope(scopeId, scopeUserData, true);
            var allBlocks = blockScope.blocksFromJson(blockInData);
            for (var i = 0; i < allBlocks.length; i++) {
                var obj = allBlocks[i];
                obj._lastRunSettings = {
                    force: false,
                    highlight: true
                };
            }
            blockScope.serviceObject = this.serviceObject;
            return blockScope;
        },
        /**
         *
         * @param files{Object[]} array of items to load in this format
         * @example:
         * @returns {Deferred.promise}
         */
        loadFiles:function(files){

            var thiz=this,
                _createDfd = function(mount,path,force,publish)
                {
                    return thiz.load(mount,path,force);
                },
                _promises = [],
                dfd = new Deferred();

            //build promise chain for 'all'
            for (var i = 0; i < files.length; i++) {
                var item = files[i];
                _promises.push(_createDfd(item.mount, item.path, item.force, item.publish));
            }

            //run and resolve head
            all(_promises).then(function(results){
                debug && console.log('got all block files ',results);
                dfd.resolve(results);
            });

            return dfd.promise;
        },
        load:function(mount,path,forceReload){
            var dfd = new Deferred(),
                thiz = this,
                _mount = utils.replaceAll('/','',mount),
                _path = utils.replaceAll('./','',path);

            var full = _mount + _path;
            full = full.trim();

            if(this.loaded[full] && forceReload===true){
                this.removeScope(this.loaded[full].id);
                this.loaded[full]=null;
            }

            if(forceReload !==true && this.loaded[full]){
                dfd.resolve(this.loaded[full]);
                return dfd.promise;
            }
            var _ready = function(data){
                var scope = thiz.toScope(data);
                if(scope){
                    thiz.loaded[full] = scope;

                    scope.mount = mount;//track file info
                    scope.path = path;
                }
                dfd.resolve(scope);
            };
            this.ctx.getFileManager().getContent(_mount,_path,_ready,false);
            return dfd.promise;
        },
        onBlocksReady:function(scope){
            var blocks = scope.allBlocks();
            for (var i = 0; i < blocks.length; i++) {
                var obj = blocks[i];
                this.setScriptFunctions(obj,scope,this);
            }
            /**
             * pick 'On Load' blocks
             */

            var loadBlocks = scope.getBlocks({
                group:'On Load'
            });
            if(loadBlocks && loadBlocks.length>0){
                for (var i = 0; i < loadBlocks.length; i++) {
                    var loadBlock  = loadBlocks[i];
                    if(loadBlock.onLoad){
                        loadBlock.onLoad();
                    }
                }
            }
        },
        getBlock:function(){

        },
        setScriptFunctions:function(obj,scope,owner){

            var thiz=owner;
            //scope.context = obj;//set the context of the blox scope
            if(!obj.blockScope) {
                obj.blockScope = scope;
            }
            debug && console.log('set script functions ' + scope.id,obj);
            scope.serviceObject = this.serviceObject;
            ///////////////////////////////////////////////////////////////////////////////
            //
            //  Variables
            //
            ///////////////////////////////////////////////////////////////////////////////
            /**
             * Add 'setVariable'
             * @param title
             * @param value
             */
            if(!obj.setVariable) {
                obj.setVariable = function (title, value, save, publish, source) {
                    var _scope = this.blockScope;
                    var _variable = _scope.getVariable(title);
                    if (_variable) {
                        _variable.value = value;
                        debug && console.log('setting variable '+title + ' to ' + value);
                    } else {
                        debug && console.log('no such variable : ' + title);
                        return;
                    }
                    if (publish !== false) {

                        thiz.publish(types.EVENTS.ON_VARIABLE_CHANGED, {
                            item: _variable,
                            scope: scope,
                            driver: obj,
                            owner: thiz,
                            save: save === true,
                            source: source || types.MESSAGE_SOURCE.BLOX  //for prioritizing
                        });
                    }
                };
            }
            /**
             * Add getVariable
             * @param title
             */
            if(!obj.getVariable) {
                obj.getVariable = function (title) {
                    var _scope = this.blockScope;
                    var _variable = _scope.getVariable(title);
                    if (_variable) {
                        return _variable.value;
                    }
                    return '';
                };
            }

        },
        hasScope:function(id) {
            if (!this.scopes) {
                this.scopes = {};
            }
            if (this.scopes[id]) {
                return this.scopes[id];
            }
            return null;
        },
        getScope:function(id,userData,publish){
            if(!this.scopes){
              this.scopes={};
            }
            if(!this.scopes[id]){
                this.scopes[id]=this.createScope({
                    id:id,
                    ctx:this.ctx
                });
                this.scopes[id].userData=userData;
                if(publish!==false){
                    try{
                        factory.publish(types.EVENTS.ON_SCOPE_CREATED,this.scopes[id]);
                    }catch(e){
                        console.error('bad, scope creation failed ' +e ,e);
                    }
                }
            }
            return this.scopes[id];
        },
        /**
         *
         * @param id
         * @returns {null}
         */
        removeScope:function(id){
            if(!this.scopes){
                this.scopes={};
            }
            for (var scopeId in this.loaded){
                if(this.loaded[scopeId].id==id){
                    delete this.loaded[scopeId];
                }
            }
            if (this.scopes[id]) {
                this.scopes[id]._destroy();
                delete this.scopes[id];
            }
            return null;
        },
        /**
         *
         * @param mixed
         * @param data
         * @returns {*}
         */
        createScope:function(mixed,data,errorCB){
            data = data || [];
            var blockStore = new Store({
                data: [],
                Model:BlockModel,
                id:utils.createUUID(),
                __events:{

                },
                observedProperties:[
                    "name",
                    "enabled",
                    "value"
                ],
                getLabel:function(item){
                    return item.name;
                },
                labelAttr:'name'
            });
            blockStore.reset();
            blockStore.setData([]);
            var args = {
                owner:this,
                blockStore:blockStore,
                serviceObject:this.serviceObject,
                config:this.config
            };
            utils.mixin(args,mixed);
            try {
                var scope = new Scope(args);
                data && scope.initWithData(data,errorCB);
                scope.init();
            }catch(e){
                logError(e,'error creating scope, data:',mixed);
            }

            return scope;
        },
        onReloaded:function(){
            debug && console.log('on reloaded');
        },
        init:function() {
            this.scope = {
                variables:[],
                blocks: []
            };
            ModelBase.prototype.types=types;
            ModelBase.prototype.factory=factory;
            if(this.onReady){
                this.onReady();
            }
        }
    });
});