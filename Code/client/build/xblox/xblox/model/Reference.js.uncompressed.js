define("xblox/model/Reference", [
    "dojo/_base/declare",
    "xide/utils"
], function(declare,utils){
    /**
     * Holds information to locate an object by string or direct reference!
     */
    return declare('xblox.model.Reference',null,{
        reference:null
    });
});