define("xblox/model/logging/Log", [
    'dcl/dcl',
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin'
], function (dcl, Deferred, Block, utils, types, EventedMixin) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl([Block, EventedMixin.dcl], {
        declaredClass: "xblox.model.logging.Log",
        name: 'Log Message',
        level: 'info',
        message: 'return "Message: " + arguments[0];',
        _type: 'XBlox',
        host: 'this host',
        sharable: true,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function () {
            var _cls = 'text-primary';
            switch (this.level) {
                case 'info': {
                    _cls = 'text-info';
                    break;
                }
                case 'warn': {
                    _cls = 'text-warning';
                    break;
                }
                case 'error': {
                    _cls = 'text-danger';
                    break;
                }
            }
            var result = this.getBlockIcon() + " " + this.name + " : " + "<span class='" + _cls + " small'> " + ' :: ';
            if (this.message) {
                result += this.message;
            }
            return result + "</span>";
        },
        /***
         * Returns the block run result
         * @param expression
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {string}
         */
        _solveExpression: function (expression, scope, settings, run, error) {
            var _script = '' + expression;
            if (_script && _script.length) {
                _script = utils.convertAllEscapes(_script, "none");
                var _args = this.getArgs();
                try {
                    var _parsed = scope.parseExpression(_script, null, null, null, null, this, _args);
                    if (run) {
                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                    }
                    return _parsed;
                } catch (e) {
                    if (error) {
                        error('invalid expression : \n' + _script + ': ' + e);
                    }
                    this.onFailed(this, settings);
                    return _script;
                }
            }
            return _script;
        },
        /**
         *
         * @param scope
         * @param settings
         * @param run
         * @param error
         */
        solve: function (scope, settings, run, error) {
            var dfd = new Deferred();
            var device = scope.device;
            var _message = this._solveExpression(this.message, scope, settings, run, error);
            var message = {
                message: _message,
                level: this.level,
                type: this._type,
                details: this.getArgs(),
                time: new Date().getTime(),
                data: {
                    device: device ? device.info : null
                },
                write: true
            };
            this.onSuccess(this, settings);
            dfd.resolve(_message);
            try {
                this.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, message);
            } catch (e) {
                this.onFailed(this, settings);
            }

            return dfd;

        },
        //  standard call from interface
        canAdd: function () {
            return null;
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz = this;
            var options = [
                {
                    value: 'info',
                    label: 'Info'
                },
                {
                    value: 'warn',
                    label: 'Warning'
                },
                {
                    value: 'error',
                    label: 'Error'
                },
                {
                    value: 'debug',
                    label: 'Debug'
                },
                {
                    value: 'help',
                    label: 'Help'
                },
                {
                    value: 'verbose',
                    label: 'verbose'
                },
                {
                    value: 'silly',
                    label: 'Silly'
                }
            ];

            fields.push(utils.createCI('Level', 3, this.level, {
                group: 'General',
                options: options,
                dst: 'level'
            }));

            fields.push(
                utils.createCI('message', 25, this.message, {
                    group: 'General',
                    title: 'Message',
                    dst: 'message',
                    delegate: {
                        runExpression: function (val, run, error) {
                            thiz._solveExpression(val, thiz.scope, null, run, error);
                        }
                    }
                }));

            fields.push(
                utils.createCI('message', 13, this._type, {
                    group: 'General',
                    title: 'Type',
                    dst: '_type'
                }));

            return fields;
        },
        getBlockIcon: function () {
            return '<span class="fa-bug"></span>';
        }
    });
});