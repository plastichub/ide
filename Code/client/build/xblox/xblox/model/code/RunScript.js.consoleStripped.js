/** @module xblox/model/code/RunScript **/
define("xblox/model/code/RunScript", [
    'dcl/dcl',
    'xdojo/has',
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xblox/model/Contains',
    'dojo/promise/all',
    'xide/types',
    'module'
    //'xdojo/has!host-node?dojo/node!tracer',
    //'xdojo/has!host-node?nxapp/utils/_console'
    //"xdojo/has!xblox-ui?dojo/text!./RunScript.html"
    //"xdojo/has!xblox-ui?dojo/text!xblox/docs/code/RunScript.md"
], function(dcl,has,Deferred,Block,utils,Contains,all,types,module,tracer,_console,Description,Help){

    
    var isServer =  0 ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if(isServer && tracer && console && console.error){
        console = _console;
    }
    /**
     *
     * @class module:xblox/model/code/RunScript
     * @extends module:xblox/model/Block
     */
    return dcl([Block,Contains],{
        declaredClass:"xblox.model.code.RunScript",
        name:'Run Script',
        method:'',
        args:'',
        deferred:false,
        sharable:true,
        context:null,
        icon:'fa-code',
        observed:[
            'method'
        ],
        getContext:function(){
            return this.context || (this.scope.getContext ?  this.scope.getContext() : this);
            return this.context || this;
        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        solve2:function(scope,settings,run,error) {
            this._currentIndex = 0;
            this._return=[];
            var _script = '' + this._get('method');
            var thiz=this,
                ctx = this.getContext();
            if(_script && _script.length) {

                var runScript = function() {
                    var _function = new Function("{" + _script + "}");
                    var _args = thiz.getArgs() || [];
                    try {
                        var _parsed = _function.apply(ctx, _args || {});
                        thiz._lastResult = _parsed;
                        if (run) {
                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                        }
                        if (_parsed !== 'false' && _parsed !== false) {
                            thiz.onSuccess(thiz, settings,{
                                result:_parsed
                            });
                        } else {
                            thiz.onFailed(thiz, settings);
                            return [];
                        }
                    } catch (e) {
                        if (error) {
                            error('invalid expression : \n' + _script + ': ' + e);
                        }
                        thiz.onFailed(thiz, settings);
                        return [];
                    }
                };

                if(scope.global){
                    (function() {
                        window = scope.global;
                        var _args = thiz.getArgs() || [];
                        try {
                            var _parsed = null;
                            if(!ctx.runExpression) {
                                var _function = new Function("{" + _script + "}").bind(this);
                                _parsed = _function.apply(ctx, _args || {});
                            }else{
                                _parsed = ctx.runExpression(_script,null,_args);
                            }

                            thiz._lastResult = _parsed;

                            if (run) {
                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                            }
                            if (_parsed !== 'false' && _parsed !== false) {
                                thiz.onSuccess(thiz, settings);
                            } else {
                                thiz.onFailed(thiz, settings);
                                return [];
                            }
                        } catch (e) {
                            thiz._lastResult = null;
                            if (error) {
                                error('invalid expression : \n' + _script + ': ' + e);
                            }
                            thiz.onFailed(thiz, settings);
                            return [];
                        }

                    }).call(scope.global);

                }else{
                    return runScript();
                }
            }else{
                console.error('have no script');
            }
            var ret=[], items = this[this._getContainer()];
            if(items.length) {
                this.runFrom(items,0,settings);
            }else{
                this.onSuccess(this, settings);
            }
            this.onDidRun();
            return ret;
        },
        /**
         *
         * @param scope
         * @param settings
         * @param run
         * @param error
         */
        solve:function(scope,settings,isInterface,send,run,error){

            this._currentIndex = 0;
            this._return=[];


            settings = settings || {};
            var _script = send || (this._get('method') ? this._get('method') : this.method);

            if(!scope.expressionModel){
                //console.error('mar',scope);
                throw new Error('na');
                return;
            }

            var thiz=this,
                ctx = this.getContext(),
                items = this[this._getContainer()],

                //outer
                dfd = new Deferred,
                listener = settings.listener,
                isDfd = thiz.deferred,
                expressionModel = scope.getExpressionModel();



            this.onRunThis(settings);

            function globalEval(text) {
                var ret;
                // Properly escape \, " and ' in the input, normalize \r\n to an escaped \n
                text = text.replace(/["'\\]/g, "\\$&").replace(/\r\n/g, "\\n");

                // You have to use eval() because not every expression can be used with an assignment operator
                var where = typeof window!=='undefined' ? window : global;

                where.execScript("globalEval.____lastInputResult____ = eval('" + text + "');} }");

                // Store the result and delete the property
                ret = globalEval.____lastInputResult____;
                delete globalEval.____lastInputResult____;

                return ret;
            }
            if(!expressionModel){
                console.error('scope has no expression model');
                return false;
            }
            var expression = expressionModel.replaceVariables(scope,_script,null,null);
            var _function = expressionModel.expressionCache[expression];
            if(!_function){
                _function = expressionModel.expressionCache[expression] = new Function("{" + expression + "}");
            }
            var _args = thiz.getArgs(settings) || [];
            try {
                if(isDfd){
                    ctx.resolve=function(result){
                        if(thiz._deferredObject) {
                            thiz._deferredObject.resolve();
                        }
                        thiz.onDidRunThis(dfd,result,items,settings);
                    }
                }
                var _parsed = _function.apply(ctx, _args || {});
                thiz._lastResult = _parsed;
                if (run) {
                    run('Expression ' + _script + ' evaluates to ' + _parsed);
                }
                if(!isDfd) {
                    thiz.onDidRunThis(dfd,_parsed,items,settings);
                }
                if (_parsed !== 'false' && _parsed !== false) {
                    thiz.onSuccess(thiz, settings);
                } else {
                    thiz.onFailed(thiz, settings);
                }
            } catch (e) {
                e=e ||{};
                thiz.onDidRunItemError(dfd,e,settings);
                thiz.onFailed(thiz,settings);
                if (error) {
                    error('invalid expression : \n' + _script + ': ' + e);
                }
            }
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){

            var result = '<span style="">' + this.getBlockIcon() + ' ' + this.name + ' :: '+'</span>';
            if(this.method){
                result+= this.method.substr(0,50);
            }
            return result;
        },
        canAdd:function(){
            return true;
        },
        getFields:function(){
            if(this.description === 'No Description'){
                this.description = Description;
            }
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz=this;
            fields.push(
                utils.createCI('name',13,this.name,{
                    group:'General',
                    title:'Name',
                    dst:'name'
                })
            );
            fields.push(
                utils.createCI('deferred',0,this.deferred,{
                    group:'General',
                    title:'Deferred',
                    dst:'deferred'
                })
            );
            fields.push(utils.createCI('arguments',27,this.args,{
                    group:'Arguments',
                    title:'Arguments',
                    dst:'args'
                }));

            fields.push(
                utils.createCI('value',types.ECIType.EXPRESSION_EDITOR,this.method,{
                    group:'Script',
                    title:'Script',
                    dst:'method',
                    select:true,
                    widget:{
                        allowACECache:true,
                        showBrowser:false,
                        showSaveButton:true,
                        editorOptions:{
                            showGutter:true,
                            autoFocus:false
                        },
                        item:this
                    },
                    delegate:{
                        runExpression:function(val,run,error){
                            var old = thiz.method;
                            thiz.method=val;
                            var _res = thiz.solve(thiz.scope,null,run,error);
                        }
                    }
                }));
            return fields;
        }
    });
});