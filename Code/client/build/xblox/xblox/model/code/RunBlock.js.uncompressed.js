define("xblox/model/code/RunBlock", [
    'dcl/dcl',
    "xblox/model/Block",
    'xide/types',
    'xide/utils'
], function(dcl,Block,types,utils){

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod


    return dcl(Block,{
        declaredClass:"xblox.model.code.RunBlock",
        //method: (String)
        //  block action name
        name:'Run Block',

        file:'',
        //method: (String)
        //  block action name
        method:'',

        args:'',

        sharable:true,

        block:'',

        description:"Runs another Block",
        /***
         * Returns the block run result
         * @param scope
         */
        solve:function(scope,settings) {

            var context = this.getContext();
            if (context && context[this.method]!=null)
            {
                var res = [];
                var _fn = context[this.method];
                try{
                    var _args = this._getArgs();
                    var _res = _fn.apply(context,_args||[]);
                    res = _res;
                    this.onSuccess(this,settings);
                    return res;
                }catch(e){
                    console.error('call method failed');
                    this.onFailed(this,settings);
                }
            }else{
                this.onFailed(this,settings);
                return [];
            }
            return [];
        },
        toText:function(){

            var result = this.getBlockIcon() + ' ' + this.name + ' ';
            if(this.method){
                result+= this.method.substr(0,20);
            }
            return result;
        },

        //  standard call for editing
        getFields:function(){


            var fields = this.getDefaultFields();


            fields.push(utils.createCI('Block', types.ECIType.BLOCK_REFERENCE, this.block, {
                toolTip:'Enter  block, you can use also the block\'s share title',
                group: 'General',
                dst: 'block',
                value: this.block,
                title:'Block',
                scope:this.scope
            }));

            fields.push(utils.createCI('File', types.ECIType.FILE, this.file, {
                toolTip:'Leave empty to auto-select this file',
                group: 'General',
                dst: 'file',
                value: this.file,
                intermediateChanges: false,
                acceptFolders: false,
                acceptFiles: true,
                encodeFilePath: false,
                buildFullPath: true,
                filePickerOptions: {
                    dialogTitle: 'Select Block File',
                    filePickerMixin: {
                        beanContextName: this.id,
                        persistent: false,
                        globalPanelMixin: {
                            allowLayoutCookies: false
                        }
                    },
                    configMixin: {
                        beanContextName: this.id,
                        LAYOUT_PRESET: types.LAYOUT_PRESET.SINGLE,
                        PANEL_OPTIONS:{
                            ALLOW_MAIN_MENU:false,
                            ALLOW_NEW_TABS:true,
                            ALLOW_MULTI_TAB:false,
                            ALLOW_INFO_VIEW:true,
                            ALLOW_LOG_VIEW:false,
                            ALLOW_CONTEXT_MENU:true,
                            ALLOW_LAYOUT_SELECTOR:true,
                            ALLOW_SOURCE_SELECTOR:true,
                            ALLOW_COLUMN_RESIZE:true,
                            ALLOW_COLUMN_REORDER:true,
                            ALLOW_COLUMN_HIDE:true,
                            ALLOW_ACTION_TOOLBAR:true,
                            ALLOW_BREADCRUMBS:false
                        }
                    },
                    defaultStoreOptions: {
                        "fields": 1663,
                        "includeList": "xblox",
                        "excludeList": "*"
                    },
                    startPath: this.file
                }
            }));

            return fields;

            /*
            fields.push(utils.createCI('value',27,this.args,{
                    group:'General',
                    title:'Arguments',
                    dst:'args'
                }));

            return fields;
            */
        },
        getBlockIcon:function(){
            return '<span class="el-icon-share-alt"></span>';
        }


    });
});