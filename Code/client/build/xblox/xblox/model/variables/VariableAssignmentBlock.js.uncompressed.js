/** @module xblox/model/variables/VariableAssignmentBlock **/
define("xblox/model/variables/VariableAssignmentBlock", [
    'dcl/dcl',
    "xblox/model/Block",
    "xide/utils",
    "xide/types",
    'dstore/legacy/DstoreAdapter',
    "xide/factory",
    'xdojo/has'
], function(dcl,Block,utils,types,DstoreAdapter,factory,has){

    var isServer =  0 ;
    var BLOCK_INSERT_ROOT_COMMAND = 'Step/Insert';
    


    /**
     *
     * @class module:xblox/model/variables/VariableAssignmentBlock
     * @extends xblox/model/Block
     */
    var Module = dcl(Block,{
        declaredClass: "xblox.model.variables.VariableAssignmentBlock",

        //variable: (String)
        //  variable title
        variable:null,

        //value: (String)
        // Expression to be asigned
        value:null,
        name:'Set Variable',
        icon:'',
        hasInlineEdits:true,
        flags:0x00000004,
        toText:function(){
            var _variable = this.scope.getVariableById(this.variable);
            var _text = _variable ? _variable.name : '';
            if(this.variable && this.variable.indexOf('://')!==-1) {
                _text = '<span class="text-info">' +this.scope.toFriendlyName(this, this.variable)+'</span>';
            }
            return this.getBlockIcon('C') + this.name + ' ' + _text  + "<span class='text-muted small'> to <kbd class='text-warning'>" + this.makeEditable("value",'bottom','text','Enter the string to send','inline') + "</kbd></span>";
        },
        _getPreviousResult: function () {
            var parent = null;
            var prev = this.getPreviousBlock();
            if(!prev || !prev._lastResult || !prev.enabled){
                parent = this.getParent();
            }else{
                parent = prev;
            }

            if (parent && parent._lastResult != null) {
                if (this.isArray(parent._lastResult)) {
                    return parent._lastResult;
                } else {
                    return parent._lastResult;
                }
            }
            return null;
        },
        /***
         * Makes the assignation
         * @param scope
         */

        solve:function(scope,settings) {
            var value = this.value;
            var changed = false;
            if(!value){
                var _value = this.getArgs();
                if(_value.length>0){
                    value = _value[0];
                }
            }
            if (this.variable && value!==null){

                this.onRun(this,settings);
                //var _variable = scope.getVariable(this.variable).value = scope.parseExpression(this.value);
                var _variable = this.variable.indexOf('://')!==-1 ? this.scope.resolveBlock(this.variable) : scope.getVariableById(this.variable);
                //console.log('assign variable',settings);
                var _value = this._getArg(value);

                var _args = this.getArgs(settings) || [];
                //console.log('run with args ' , _args);

                if(!_variable){
                    //console.error('     no such variable : ' + this.variable);
                    return [];
                }
                var _parsed = null;
                if(this.isScript(_value)){
                    var override = this.override || {};
                    _parsed = scope.parseExpression(value,null,null,null,null,null,_args || override.args);
                    //_parsed = scope.parseExpression(_value);
                    _parsed = this.replaceAll("'",'',_parsed);
                    //_variable.value = scope.parseExpression(_value);
                    //_variable.value = this.replaceAll("'",'',_variable.value);

                    if(_variable.value!==_parsed){
                        changed = true;
                    }

                }else{

                    if(_args && _args.length==1 && value==null){
                        _value = _args[0];
                    }

                    if(_variable.value!==_value){
                        changed = true;
                    }

                    _variable.value = _value;
                    _parsed = _value;
                }


                _variable.set('value',_parsed);

                var publish = false;


                var context = this.getContext();
                if(context) {
                    var device = context.device;
                    if(device && device.info && isServer && device.info.serverSide) {
                        if (this.flags & types.VARIABLE_FLAGS.PUBLISH_IF_SERVER) {
                            publish = true;
                        }else{
                            publish=false;
                        }
                    }
                }

                if(this.flags & types.VARIABLE_FLAGS.PUBLISH && changed){
                    publish = true;
                }

                changed && factory.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED,{
                    item:_variable,
                    scope:this.scope,
                    save:false,
                    block:this,
                    name:_variable.name,
                    value:_value,
                    publish:publish,
                    result:_parsed
                });
                this.onSuccess(this,settings);
                return [];
            }
        },
        canAdd:function(){
            return null;
        },
        getFields:function(){

            var fields = this.inherited(arguments) || this.getDefaultFields(false);
            var thiz=this;

            /*
            fields.push(this.utils.createCI('Variable',3,this.variable,{
                    group:'General',
                    dst:'variable',
                    widget:{
                        store:new DstoreAdapter(this.scope.blockStore),
                        query:{
                            group:'basicVariables'
                        }
                    }
            }));
            */




            fields.push(this.utils.createCI('value',29,this.value,{
                    group:'General',
                    title:'Value',
                    dst:'value',
                    widget:{
                        allowACECache:true,
                        showBrowser:false,
                        showSaveButton:true,
                        editorOptions:{
                            showGutter:false,
                            autoSelect: false,
                            autoFocus: false,
                            hasConsole:false,
                            hasItemActions:function(){
                                return false
                            }
                        },
                        item:this
                    },
                    delegate:{
                        runExpression:function(val,run,error){
                            return thiz.scope.expressionModel.parse(thiz.scope,val,false,run,error);
                        }
                    }
            }));




            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.variable,{
                group:'Variable',
                title:'Variable',
                dst:'variable',
                //options:this.scope.getVariablesAsOptions(),
                block:this,
                pickerType:'variable',
                value:this.variable,
                widget:{
                    store:this.scope.blockStore,
                    labelField:'name',
                    valueField:'id',
                    value:this.variable,
                    query:[
                        {
                            group:'basicVariables'
                        },
                        {
                            group:'processVariables'
                        }
                    ]

                }
            }));

            fields.push(this.utils.createCI('flags',5,this.flags,{
                group:'Variable',
                title:'Flags',
                dst:'flags',
                data:[
                    {
                        value: 0x00000002,
                        label: 'Publish to network',
                        title:"Publish to network in order to make a network variable"
                    },
                    {
                        value: 0x00000004,//2048
                        label: 'Publish if server',
                        title: 'Publish only on network if this is running server side'
                    }
                ],
                widget:{
                    hex:true
                }

            }));

            return fields;
        }
    });

    return Module;

});