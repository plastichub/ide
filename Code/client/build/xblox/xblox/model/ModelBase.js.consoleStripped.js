/** @module xblox/model/ModelBase
 *  @description The base for block related classes, this must be kept small and light as possible
 */
define("xblox/model/ModelBase", [
    'dcl/dcl',
    "xide/utils",
    "xide/types",
    "xide/mixins/EventedMixin",
    "xide/lodash"
], function(dcl,utils,types,EventedMixin,_){
    /**
     * The model mixin for a block
     * @class module:xblox.model.ModelBase
     */
    var Module = dcl(EventedMixin.dcl,{
        declaredClass:'xblox.model.ModelBase',
        id:null,
        description:'',
        parent:null,
        parentId:null,
        group:null,
        order:0,
        _store:null,
        ////////////////////////////////////////////////////////////
        //
        //  Functions to expose out & in - lets
        //
        ////////////////////////////////////////////////////////////
        /**
         *
         * Implmented by the subclass. Each block must provide an output signature.
         * The format is currently the same as Dojo SMD
         *
         * @returns {Array}
         */
        outputs:function(){
           return [];
        },
        /**
         * Implemented by the subclass. Each block must provide an input signature.
         * The format is currently the same as Dojo SMD
         * @returns {Array}
         */
        takes:function(){
            return [];
        },
        /**
         * Implemented by the subclass. Each block must provide an needed input signature.
         * The format is currently the same as Dojo SMD. This is a filtered version of
         * 'takes'
         *
         * @returns {Array}
         */
        needs:function(){
            return [];
        },
        ////////////////////////////////////////////////////////////
        //
        //  Functions to expose outlets
        //
        ////////////////////////////////////////////////////////////
        /***
         * Standard constructor for all sub classing blocks
         * @param {array} args
         */
        constructor: function(args){
            //simple mixin of constructor arguments
            for (var prop in args) {
                if (args.hasOwnProperty(prop)) {
                    this[prop] = args[prop];
                }
            }
            if(!this.id){
                this.id = this.createUUID();
            }
            //short cuts
            this.utils=utils;
            this.types=types;
        },
        ////////////////////////////////////////////////////////////
        //
        //  Standard tools
        //
        ////////////////////////////////////////////////////////////
        keys: function (a) {
            var b = [];
            for (var c in a) {
                b.push(c);
            }
            return b;
        },
        values: function (b) {
            var a = [];
            for (var c in b) {
                a.push(b[c]);
            }
            return a;
        },
        toArray: function () {
            return this.map();
        },
        size: function () {
            return this.toArray().length;
        },
        createUUID:utils.createUUID,
        canEdit:function(){
            return true;
        },
        getFields:function(){
            return null;
        },
        isString: function (a) {
            return typeof a == "string"
        },
        isNumber: function (a) {
            return typeof a == "number"
        },
        isBoolean: function (a) {
            return typeof a == "boolean"
        },
        isObject:_.isObject,
        isArray:_.isArray,
        getValue:function(val){
            var _float = parseFloat(val);
            if(!isNaN(_float)){
               return _float;
            }
            if(val==='true' || val===true){
                return true;
            }
            if(val==='false' || val===false){
                return false;
            }
            return val;
        },
        isScript:function(val){
            return this.isString(val) &&(
                    val.indexOf('return')!=-1||
                    val.indexOf(';')!=-1||
                    val.indexOf('(')!=-1||
                    val.indexOf('+')!=-1||
                    val.indexOf('-')!=-1||
                    val.indexOf('<')!=-1||
                    val.indexOf('*')!=-1||
                    val.indexOf('/')!=-1||
                    val.indexOf('%')!=-1||
                    val.indexOf('=')!=-1||
                    val.indexOf('==')!=-1||
                    val.indexOf('>')!=-1||
                    val.indexOf('[')!=-1||
                    val.indexOf('{')!=-1||
                    val.indexOf('}')!=-1
                );
        },
        replaceAll:function(find, replace, str) {
            if(this.isString(str)){
                return str.split(find).join(replace);
            }
            return str;
        },
        isInValidState:function(){
            return true;
        },
        destroy:function(){}
    });
    dcl.chainAfter(Module,'destroy');
    return Module;
});