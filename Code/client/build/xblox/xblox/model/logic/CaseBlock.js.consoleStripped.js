define("xblox/model/logic/CaseBlock", [
    'dcl/dcl',
    'xide/utils',
    'xblox/model/Block',
    'dojo/Deferred',
    "xblox/model/logic/BreakBlock"
], function (dcl, utils, Block, Deferred, BreakBlock) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The Case Block model. Each case block contains a comparation and a commands block.
    //      If the comparation result is true, the block is executed
    //
    //      This block should have an "SwitchBlock" parent

    // module:
    //		xblox.model.logic.CaseBlock
    return dcl(Block, {
        declaredClass: "xblox.model.logic.CaseBlock",
        //comparator: xblox.model.Comparator
        // Comparison to be applied -> compare <switch variable> width <expression>
        comparator: null,
        //expression: xblox.model.Expression
        // expression to be compared
        expression: null,
        //items: Array (xblox.model.Block)
        //  block to be executed if the comparison result is true
        items: null,
        name: 'Case',
        icon: '',
        hasInlineEdits: true,
        toText: function () {
            var _comparator = '' + this.comparator;
            if (_comparator == '==') {
                //_comparator =''
            }
            return '<span style="text-indent: 1em;">&nbsp;&nbsp;&nbsp;' + this.getBlockIcon('I') + this.name + ' ' + this.makeEditable('comparator', 'right', 'text', 'Enter a comparison', 'inline') + (this.expression != null ? ' ' + this.makeEditable('expression', 'right', 'text', 'Enter a value to compare') : '') + '</span>';
        },
        canAdd: function () {
            return [];
        },
        /**
         *
         * @param scope
         * @param settings
         * @param switchblock
         * @returns {Array}
         * @private
         */
        _solve: function (scope, settings, switchblock) {
            settings = settings || {
                    highlight: false
                };
            var ret = [];
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];
                if (block.declaredClass.indexOf('BreakBlock') !== -1) {
                    switchblock.stop();
                }
                this.addToEnd(ret, block.solve(scope, settings));
            }

            return ret;
        },
        /***
         * Solves the case block
         * @param scope
         * @param settings
         * @param switchBlock   => parent SwitchCommand block
         */
        solve: function (scope, switchBlock, settings) {
            try {
                var _var = scope.getVariableById(switchBlock.variable);
                if (!_var && settings.args && settings.args[0]) {
                    _var = {value: settings.args[0]};
                }
                // Get the variable to evaluate
                var switchVarValue = '';
                if (_var) {
                    switchVarValue = this._getArg(_var.value, true);
                } else {
                    this.onFailed(this, settings);
                    // Comparation is false
                    return false;
                }
                //var compResult = scope.parseExpression("'" + switchVarValue+ "'" + this.comparator + this.expression);
                var compResult = scope.parseExpression("" + switchVarValue + "" + this.comparator + this._getArg(this.expression, true));
                if (compResult !== true) {
                    this.onFailed(this, settings);
                    // Comparation is false
                    return false;
                } else {
                    this.onSuccess(this, settings);
                    // Comparation is true. Return block.solve();
                    this._solve(scope, settings, switchBlock);
                    return true;
                }
            } catch (e) {
                logError(e);
            }
        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            return this.items;
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('Expression', 13, this.expression, {
                group: 'General',
                title: 'Expression',
                dst: 'expression'
            }));

            function makeOption(value, label) {
                return {
                    label: label || value,
                    value: value
                }
            }

            fields.push(utils.createCI('Comparator', 3, this.comparator, {
                group: 'General',
                title: 'Comparator',
                dst: 'comparator',
                widget: {
                    options: [
                        /*makeOption('==',"Equals"),
                         makeOption('<=',"Smaller or equal"),
                         makeOption('=>',"Greater or equal"),
                         makeOption('!=',"Not equal"),
                         makeOption('<',"Smaller than"),
                         makeOption('>',"Greater than")*/
                        makeOption('=='),
                        makeOption('<='),
                        makeOption('=>'),
                        makeOption('!='),
                        makeOption('<'),
                        makeOption('>')
                    ],
                    editable: true
                }
            }));
            return fields;
        },
        runAction: function (action) {
            if (action.command === 'New/Break') {
                var dfd = new Deferred();
                var newBlock = this.add(BreakBlock, {
                    group: null
                });
                var defaultDfdArgs = {
                    select: [newBlock],
                    focus: true,
                    append: false
                };
                dfd.resolve(defaultDfdArgs);
                newBlock.refresh();
                return dfd;
            }
        },
        getActions: function () {
            return [this.createAction({
                label: 'Break',
                command: 'New/Break',
                tab: 'Home',
                icon: 'fa-stop',
                group: 'File',
                mixin: {
                    addPermission: true,
                    custom: true,
                    quick: false
                }
            })
            ]
        }
    });
});