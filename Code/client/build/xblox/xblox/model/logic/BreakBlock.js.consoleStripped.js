define("xblox/model/logic/BreakBlock", [
    'dcl/dcl',
    'xblox/model/Block'
], function (dcl, Block) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */

    // summary:
    //		The Case Block model. Each case block contains a comparation and a commands block.
    //      If the comparation result is true, the block is executed
    //
    //      This block should have an "SwitchBlock" parent

    // module:
    //		xblox.model.logic.CaseBlock
    return dcl(Block, {
        declaredClass: "xblox.model.logic.BreakBlock",
        name: 'Break',
        icon: 'fa-stop',
        hasInlineEdits: false,
        canAdd: false,
        toText: function () {
            return '&nbsp;<span class="fa-stop text-warning"></span>&nbsp;&nbsp;<span>' + this.name + '</span>';
        },
        /***
         * Solves the case block
         * @param scope
         * @param settings
         */
        solve: function (scope, settings) {
            this.onSuccess(this, settings);
        }
    });
});