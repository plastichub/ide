/** @module xblox/model/functions/StopBlock **/
define("xblox/model/functions/StopBlock", [
    'dcl/dcl',
    'xide/utils',
    "xblox/model/Block"
], function(dcl,utils,Block){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    return dcl(Block,{
        declaredClass:"xblox.model.functions.StopBlock",
        command:'Select command please',
        icon:'',
        args:null,
        _timeout:100,
        hasInlineEdits:true,
        solve:function(scope,settings) {
            if (this.command){
                var block = scope.resolveBlock(this.command);
                if(block && block.stop){
                    var res = block.stop();
                    this.onSuccess(this,settings);
                }else{
                    this.onFailed(this,settings);
                }
                return res;
            }
        },
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable:function(field,pos,type,title,mode){
            return "<a tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable editable-click'  href='#'>" + this[field] +"</a>";
        },
        getFieldOptions:function(field){
            if(field ==="command"){
                return this.scope.getCommandsAsOptions("text");
            }
        },
        toText:function(){
            var text = 'Unknown';
            var block = this.scope.getBlock(this.command);
            if(block){
                text = block.name;
            }
            if(this.command.indexOf('://')!==-1) {
                text = '<span class="text-info">' +this.scope.toFriendlyName(this,this.command) + '</span>';
            }
            return this.getBlockIcon('D') + 'Stop Command : ' + text;
        },
        onChangeField:function(what,value){
        },
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('value','xcf.widgets.CommandPicker',this.command,{
                    group:'General',
                    title:'Command',
                    dst:'command',
                    options:this.scope.getCommandsAsOptions(),
                    block:this,
                    pickerType:'command',
                    value:this.command
            }));
            return fields;
        }
    });
});