/** @module xgrid/Base **/
define("xblox/utils/TestUtils", [
    "xdojo/declare",
    "xide/utils",
    "xide/types",
    "xblox/views/BlockGrid",
    "xaction/DefaultActions"

], function (declare,utils,types,BlockGrid) {

    var Module = declare('xblox.utils.TestUtils',null,{});

    function createScope(blockManager,extraBlocks,clear,mixin) {
        extraBlocks = extraBlocks || [];
        var defaultBlocks = clear === true ? [] : [
            {
                "_containsChildrenIds": [],
                "group": "click",
                "id": "5fc8ea23-bac4-3a4e-0f37-b9ed6f4b340f",
                "declaredClass": "xblox.model.variables.VariableSwitch",
                "name": "Switch on Variable",
                "icon": "",
                "variable": "PowerState",
                "isCommand": false,
                "enabled": true,
                "shareTitle": "",
                "allowActionOverride": true,
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            },
            {
                "_containsChildrenIds": [],
                "name": "PowerState",
                "send": "nada",
                "group": "Variables",
                "id": "31c98cdd-02a8-3af1-3a49-11955c0fad48",
                "declaredClass": "xcf.model.Variable",
                "gui": "off",
                "cmd": "off",
                "save": false,
                "target": "None",
                "type": "added",
                "value": "off",
                "register": true,
                "readOnly": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "isCommand": false,
                "allowActionOverride": true,
                "icon": "fa-play"
            },
            {
                "_containsChildrenIds": [
                    "items"
                ],
                "group": "click",
                "id": "root",
                "items": [
                    "sub0",
                    "sub1"
                ],
                "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                "name": "Root - 1",
                "method": "console.log('asd',this);",
                "args": "",
                "deferred": false,
                "declaredClass": "xblox.model.code.RunScript",
                "enabled": true,
                "serializeMe": true,
                "shareTitle": "",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0,
                "additionalProperties": true,
                "_scenario": "update"
            },
            {
                "group": "click",
                "id": "root2",
                "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                "name": "Root - 2",
                "method": "console.log(this);",
                "args": "",
                "deferred": false,
                "declaredClass": "xblox.model.code.RunScript",
                "enabled": true,
                "serializeMe": true,
                "shareTitle": "",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0

            },
            {
                "group": "click",
                "id": "root3",
                "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                "name": "Root - 3",
                "method": "console.log(this);",
                "args": "",
                "deferred": false,
                "declaredClass": "xblox.model.code.RunScript",
                "enabled": true,
                "serializeMe": true,
                "shareTitle": "",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0

            },
            {
                "group": "click",
                "id": "root4",
                "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                "name": "Root - 4",
                "method": "console.log(this);",
                "args": "",
                "deferred": false,
                "declaredClass": "xblox.model.code.RunScript",
                "enabled": true,
                "serializeMe": true,
                "shareTitle": "",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0
            },
            {
                "group": "click",
                "id": "root5",
                "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                "name": "Root - 5",
                "method": "console.log(this);",
                "args": "",
                "deferred": false,
                "declaredClass": "xblox.model.code.RunScript",
                "enabled": true,
                "serializeMe": true,
                "shareTitle": "",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0
            },
            {
                "_containsChildrenIds": [],
                "parentId": "root",
                "id": "sub0",
                "name": "On Event",
                "event": "",
                "reference": "",
                "declaredClass": "xblox.model.events.OnEvent",
                "_didRegisterSubscribers": false,
                "enabled": true,
                "serializeMe": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0,
                "additionalProperties": true,
                "_scenario": "update"
            },
            {
                "_containsChildrenIds": [],
                "parentId": "root",
                "id": "sub1",
                "name": "On Event2",
                "event": "",
                "reference": "",
                "declaredClass": "xblox.model.events.OnEvent",
                "_didRegisterSubscribers": false,
                "enabled": true,
                "serializeMe": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0,
                "additionalProperties": true,
                "_scenario": "update"
            }
        ];

        var data = {
            "blocks": extraBlocks.concat(defaultBlocks)
        };



        var scope = blockManager.toScope(data);
        utils.mixin(scope,mixin);
        return scope;
    }

    function createBlockGrid(ctx,parent,mixin,startup){

        var ACTION = types.ACTION;
        var blockManager = ctx.getBlockManager();
        var blockScope = createScope(blockManager, [
            {
                "_containsChildrenIds": [],
                "id": "83de87c0-f8c7-74da-161d-8e9cf51d67b1",
                "name": "value",
                "value": "MVMAX 98",
                "type": "added",
                "group": "processVariables",
                "gui": false,
                "cmd": false,
                "declaredClass": "xcf.model.Variable",
                "save": false,
                "target": "None",
                "register": true,
                "readOnly": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "isCommand": false,
                "allowActionOverride": true,
                "icon": "fa-play"
            },
            {
                "_containsChildrenIds": [],
                "name": "PowerOn",
                "send": "PWON",
                "group": "basic",
                "id": "53a10527-709b-4c7d-7a90-37f58f17c8db",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "auto": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "icon": "text-success fa-circle",
                "interval": "0",
                "waitForResponse": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "PowerOff",
                "send": "PWSTANDBY",
                "group": "basic",
                "id": "84961334-9cd2-d384-25dc-a6b943e8cb8e",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "auto": "-1",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "icon": "text-danger fa-power-off",
                "interval": "0",
                "waitForResponse": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "VolumeUp",
                "send": "return 'MV' + (Math.abs(this.getVariable('Volume')) +2);",
                "group": "basic",
                "id": "6d0c5e0e-5c04-bb98-44a0-705c8269de07",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "auto": "-1",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "icon": "fa-arrow-up",
                "interval": "0",
                "waitForResponse": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "Volume",
                "type": "added",
                "value": 59,
                "enumType": "VariableType",
                "save": false,
                "initialize": "",
                "group": "basicVariables",
                "id": "3403a69e-252a-30dc-b130-40a028d1cde4",
                "register": true,
                "readOnly": false,
                "declaredClass": "xcf.model.Variable",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "icon": "fa-automobile",
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "VolumeDown",
                "send": "return 'MV' + (this.getVariable('Volume') - 2);",
                "group": "basic",
                "id": "69f6d4fb-4300-0498-9bbf-27554f5f1fa4",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "auto": "-1",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "icon": "fa-arrow-down",
                "interval": "0",
                "waitForResponse": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "Ping",
                "group": "basic",
                "id": "a5423bf7-7b99-023d-c637-363fbf9a7f18",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "send": "pw?",
                "interval": "1000",
                "waitForResponse": false,
                "icon": "fa-bell",
                "enabled": false,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "auto": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [
                    "items"
                ],
                "name": "Fade-Volume-Down",
                "group": "conditional",
                "id": "1f969cc6-89c4-f559-e824-daf7dfff35cf",
                "items": [
                    "c6bc0ef4-5b85-b543-4df7-3c00dd73a9eb"
                ],
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "send": "",
                "interval": 0,
                "icon": "fa-exclamation",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "waitForResponse": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [
                    "items"
                ],
                "condition": "[Volume]>20",
                "parentId": "1f969cc6-89c4-f559-e824-daf7dfff35cf",
                "id": "c6bc0ef4-5b85-b543-4df7-3c00dd73a9eb",
                "declaredClass": "xblox.model.loops.WhileBlock",
                "loopLimit": 1500,
                "name": "While",
                "wait": "10",
                "icon": "",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description<br><br>sdfsdf",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "isCommand": false,
                "allowActionOverride": true,
                "items": [
                    "0caac742-956e-de6b-6b53-36fb8174e5e0"
                ],
                "_timer": 3564
            },
            {
                "_containsChildrenIds": [
                    "items"
                ],
                "name": "Fade-Volume-Up",
                "group": "conditional",
                "id": "48a83acf-f2ef-ccdf-44a7-621fb635e3c4",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "send": "",
                "interval": "0",
                "icon": "fa-exclamation",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "items": [
                    "0a561903-9422-f97d-8a76-0757177a7471"
                ],
                "waitForResponse": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [
                    "items"
                ],
                "condition": "[Volume]<80",
                "parentId": "48a83acf-f2ef-ccdf-44a7-621fb635e3c4",
                "id": "0a561903-9422-f97d-8a76-0757177a7471",
                "declaredClass": "xblox.model.loops.WhileBlock",
                "loopLimit": 1500,
                "name": "While",
                "wait": "10",
                "icon": "",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "isCommand": false,
                "allowActionOverride": true,
                "items": [
                    "660a553b-ff5d-ca79-c114-955800c8de76"
                ],
                "_timer": 2740
            },
            {
                "_containsChildrenIds": [],
                "name": "PowerState",
                "send": "nada",
                "group": "basicVariables",
                "id": "31c98cdd-02a8-3af1-3a49-11955c0fad48",
                "declaredClass": "xcf.model.Variable",
                "gui": "off",
                "cmd": "off",
                "save": false,
                "target": "None",
                "type": "added",
                "value": "off",
                "register": true,
                "readOnly": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "isCommand": false,
                "allowActionOverride": true,
                "icon": "fa-play"
            },
            {
                "_containsChildrenIds": [],
                "name": "Ping - Volume",
                "group": "basic",
                "id": "dec01610-0355-f038-71c8-a46b2cde5fd2",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "send": "MV?",
                "interval": "1000",
                "waitForResponse": false,
                "icon": "fa-exclamation",
                "enabled": false,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "auto": false,
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "VolumeNormal",
                "send": "MV?",
                "group": "basic",
                "id": "963944c2-c0b0-fe8a-504e-1b8bedd2a3cf",
                "declaredClass": "xcf.model.Command",
                "startup": true,
                "auto": true,
                "enabled": false,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "interval": "2000",
                "waitForResponse": false,
                "isCommand": false,
                "allowActionOverride": true,
                "icon": "fa-play"
            },
            {
                "_containsChildrenIds": [
                    "items"
                ],
                "group": "conditionalProcess",
                "id": "43062d7e-6cfd-040c-34bf-7a01c428c057",
                "items": [
                    "1c8b8909-5fb8-2df5-2937-03799f54c3df",
                    "ac702535-d89b-3072-63c1-6774fe7b1a87"
                ],
                "declaredClass": "xblox.model.variables.VariableSwitch",
                "name": "Switch on Variable",
                "icon": "",
                "variable": "31c98cdd-02a8-3af1-3a49-11955c0fad48",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "comparator": "==",
                "expression": "on",
                "id": "1c8b8909-5fb8-2df5-2937-03799f54c3df",
                "declaredClass": "xblox.model.logic.CaseBlock",
                "name": "Case",
                "icon": "",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "parentId": "43062d7e-6cfd-040c-34bf-7a01c428c057",
                "order": 0,
                "type": "added",
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "comparator": "==",
                "expression": "off",
                "id": "ac702535-d89b-3072-63c1-6774fe7b1a87",
                "declaredClass": "xblox.model.logic.CaseBlock",
                "name": "Case",
                "icon": "",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "parentId": "43062d7e-6cfd-040c-34bf-7a01c428c057",
                "order": 0,
                "type": "added",
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "group": "conditionalProcess",
                "condition": "[Volume]>60",
                "id": "4d6f219e-fb31-6184-f238-447a8b0ff616",
                "declaredClass": "xblox.model.logic.IfBlock",
                "autoCreateElse": true,
                "name": "if",
                "icon": "",
                "enabled": false,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "isCommand": false,
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "Volume-Loud",
                "group": "basicVariables",
                "id": "b16bfc09-e449-d514-dc98-10b8afbb14f8",
                "declaredClass": "xcf.model.Variable",
                "gui": "off",
                "cmd": "off",
                "save": false,
                "target": "None",
                "value": "65",
                "register": true,
                "readOnly": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "isCommand": false,
                "allowActionOverride": true,
                "icon": "fa-play"
            },
            {
                "_containsChildrenIds": [],
                "name": "No Title",
                "group": "basic",
                "id": "12cdb13e-9c72-2079-8791-87402763c720",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "auto": false,
                "send": "return 'MV' + ([Volume] + 20);",
                "interval": "4000",
                "waitForResponse": false,
                "icon": "fa-exclamation",
                "isCommand": false,
                "enabled": false,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "allowActionOverride": true
            },
            {
                "_containsChildrenIds": [],
                "name": "Test",
                "group": "basicVariables",
                "id": "487b9e80-ca4c-c096-0950-91aad9dd4612",
                "declaredClass": "xcf.model.Variable",
                "gui": "off",
                "cmd": "off",
                "save": false,
                "target": "None",
                "value": "return [Volume] + 2;",
                "register": true,
                "readOnly": false,
                "isCommand": false,
                "enabled": true,
                "shareTitle": "",
                "allowActionOverride": true,
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "icon": "fa-play"
            },
            {
                "_containsChildrenIds": [],
                "condition": "",
                "parentId": "c6bc0ef4-5b85-b543-4df7-3c00dd73a9eb",
                "id": "0caac742-956e-de6b-6b53-36fb8174e5e0",
                "declaredClass": "xblox.model.functions.CallBlock",
                "command": "69f6d4fb-4300-0498-9bbf-27554f5f1fa4",
                "icon": "",
                "_timeout": 100,
                "isCommand": false,
                "enabled": true,
                "shareTitle": "",
                "allowActionOverride": true,
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            },
            {
                "_containsChildrenIds": [],
                "id": "Shell-Block",
                "description": "Runs a JSON-RPC-2.0 method on the server",
                "name": "Run Server Method",
                "method": "XShell::run",
                "args": "ls",
                "deferred": true,
                "defaultServiceClass": "XShell",
                "defaultServiceMethod": "run",
                "declaredClass": "xblox.model.server.RunServerMethod",
                "enabled": true,
                "shareTitle": "",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "group": "click"
            }
        ]);

        var grid,
            store = blockScope.blockStore,
            gridArgs = utils.mixin({
                ctx: ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect: true,
                collection: store.filter({
                    group: "click"
                }),
                permissions: [
                    ACTION.CLIPBOARD,
                    'Step/Properties',
                    ACTION.TOOLBAR,
                    //ACTION.EDIT,
                    ACTION.RENAME,
                    ACTION.RELOAD,
                    ACTION.DELETE,
                    ACTION.CLIPBOARD,
                    ACTION.LAYOUT,
                    ACTION.COLUMNS,
                    ACTION.SELECTION,
                    //ACTION.PREVIEW,
                    ACTION.SAVE,
                    ACTION.SEARCH,
                    'Step/Run',
                    'Step/Move Up',
                    'Step/Move Down',
                    'Step/Edit'
                    //ACTION.TOOLBAR
                ],
                __getBlockActions: function (permissions) {

                    var result = [],
                        ACTION = types.ACTION,
                        ACTION_ICON = types.ACTION_ICON,
                        VISIBILITY = types.ACTION_VISIBILITY,
                        thiz = this,
                        container = thiz.domNode,
                        actionStore = thiz.getActionStore();

                    function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

                        var action = null;
                        mixin = mixin || {};
                        utils.mixin(mixin, {owner: thiz});

                        if (mixin.addPermission || DefaultActions.hasAction(permissions, command)) {

                            if (!handler) {
                                handler = function (action) {
                                    console.log('log run action', arguments);
                                    var who = this;
                                    if (who.runAction) {
                                        who.runAction.apply(who, [action]);
                                    }
                                }
                            }

                            if (!mixin.tooltip && keycombo) {

                                if (_.isString(keycombo)) {
                                    keycombo = [keycombo];
                                }


                                mixin.tooltip = keycombo.join('<br/>').toUpperCase();

                            }


                            action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);
                            result.push(action);
                            return action;
                        }
                    }




                    var rootAction = 'Block/Insert';
                    var defaultMixin = { addPermission: true };

                    result.push(thiz.createAction({
                        label: 'New Block',
                        command: rootAction,
                        icon: 'fa-plus',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['alt up'],
                        mixin: defaultMixin
                    }));
                    result.push(thiz.createAction({
                        label: 'Save',
                        command: 'File/Save',
                        icon: 'fa-save',
                        tab: 'Home',
                        group: 'File',
                        keycombo: ['ctrl '],
                        mixin: defaultMixin
                    }));
                    result.push(thiz.createAction({
                        label: 'Save As',
                        command: 'File/Save As',
                        icon: 'fa-save',
                        tab: 'Home',
                        group: 'File',
                        mixin: defaultMixin
                    }));

                    var newBlockActions = this.getAddActions();
                    var levelName = '';

                    function addItems(commandPrefix, items) {

                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];

                            levelName = item.name;

                            var path = commandPrefix + '/' + levelName;
                            var isContainer = !_.isEmpty(item.items);

                            result.push(thiz.createAction({
                                label: levelName,
                                command: path,
                                icon: item.iconClass,
                                tab: 'Home',
                                group: 'Step',
                                mixin:defaultMixin
                            }));

                            /*
                             addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {
                             item: item,
                             addPermission: true
                             }, null, null);
                             */


                            if (isContainer) {
                                addItems(path, item.items);
                            }
                        }

                    }

                    addItems(rootAction, newBlockActions);



                    function _selection() {
                        var selection = thiz.getSelection();
                        if (!selection || !selection.length) {
                            return null;
                        }
                        var item = selection[0];
                        if (!item) {
                            console.error('have no item');
                            return null;
                        }
                        return selection;
                    }

                    function _canMove() {

                        var selection = _selection();
                        if (!selection) {
                            return true;
                        }
                        return selection[0].canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

                    }


                    function canParent() {

                        var selection = thiz.getSelection();
                        if (!selection) {
                            return true;
                        }
                        var item = selection[0];
                        if (!item) {
                            //console.error('canParent:have no item!!!',this);
                            return true;
                        }
                        if (this.command === 'Step/Move Left') {
                            return !item.getParent();
                        } else {
                            return item.getParent();
                        }
                    }

                    function isItem() {
                        var selection = _selection();
                        if (!selection) {
                            return true;
                        }
                        return false;
                    }

                    function canMove() {
                        var selection = _selection();

                        if (!selection) {
                            return true;
                        }

                        var item = selection[0];

                        if (this.command === 'Step/Move Up') {
                            return !item.canMove(null, -1);
                        } else {
                            return !item.canMove(null, 1);
                        }

                        return false;
                    }

                    result.push(thiz.createAction({
                        label: 'Run',
                        command: 'Step/Run',
                        icon: 'fa-play',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['r'],
                        shouldDisable: isItem
                    }));

                    //////////////////////////////////////////////////////////////////
                    //
                    //  Step - Move
                    //

                    result.push(thiz.createAction({
                        label: 'MoveUp',
                        command: 'Step/Move Up',
                        icon: 'fa-arrow-up',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['alt up'],
                        shouldDisable: canMove
                    }));

                    result.push(thiz.createAction({
                        label: 'MoveDown',
                        command: 'Step/Move Down',
                        icon: 'fa-arrow-down',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['alt down'],
                        shouldDisable: canMove,
                        mixin: defaultMixin
                    }));

                    result.push(thiz.createAction({
                        label: 'MoveLeft',
                        command: 'Step/Move Left',
                        icon: 'fa-arrow-left',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['alt left'],
                        shouldDisable: canMove,
                        mixin: defaultMixin
                    }));

                    result.push(thiz.createAction({
                        label: 'MoveRight',
                        command: 'Step/Move Right',
                        icon: 'fa-arrow-right',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['alt right'],
                        shouldDisable: canParent,
                        mixin: defaultMixin
                    }));


                    //Step enable/disable
                    /*
                     addAction('On', 'Step/Enable', 'fa-toggle-off', ['alt d'], 'Home', 'Step', 'item|view', null, null,
                     {
                     addPermission:true,
                     onCreate: function (action) {
                     action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                     widgetClass: declare.classFactory('_Checked', [ToggleButton,_ActionValueWidgetMixin], null, {} ,null),
                     widgetArgs: {
                     icon1: 'fa-toggle-on',
                     icon2: 'fa-toggle-off',
                     delegate: thiz,
                     checked:true,
                     iconClass:'fa-toggle-on'
                     }
                     });
                     }
                     },null, function(){
                     return thiz.getSelection().length==0;
                     });
                     */

                    result.push(thiz.createAction({
                        label: 'On',
                        command: 'Step/Enable',
                        icon: 'fa-toggle-off',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['alt d'],
                        shouldDisable: isItem,
                        mixin: defaultMixin
                    }));


                    result.push(this.createAction({
                        label: 'Edit',
                        command: 'Step/Edit',
                        icon: ACTION_ICON.EDIT,
                        keycombo: ['f4', 'enter', 'dblclick'],
                        tab: 'Home',
                        group: 'Step',
                        shouldDisable: isItem
                    }));

                    /*
                     addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                     {
                     addPermission: true,
                     onCreate: function (action) {
                     action.handle=false;
                     action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                     widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                     widgetArgs: {
                     icon1: 'fa-toggle-on',
                     icon2: 'fa-toggle-off',
                     delegate: thiz,
                     checked: false,
                     iconClass: 'fa-toggle-off'
                     }
                     });
                     }
                     }, null, function () {
                     return thiz.getSelection().length == 0;
                     });
                     */
                    result.push(thiz.createAction({
                        label: 'Properties',
                        command: 'Step/Properties',
                        icon: 'fa-arrow-up',
                        tab: 'Home',
                        group: 'Step',
                        keycombo: ['alt enter'],
                        shouldDisable: isItem,
                        mixin: defaultMixin
                    }));

                    this._emit('onAddActions', {
                        actions: result,
                        addAction: addAction,
                        permissions: permissions,
                        store: actionStore
                    });

                    //console.dir(_.pluck(result,'command'));


                    return result;
                }
            },mixin);

        grid = utils.addWidget(BlockGrid, gridArgs, null, parent, startup!==false ? true : false);

        return grid;



    }

    Module.createScope = createScope;
    Module.createBlockGrid = createBlockGrid;

    return Module;

});