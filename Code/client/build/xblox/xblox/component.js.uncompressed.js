define("xblox/component", [
    "dcl/dcl",
    "xdojo/has",
    "xide/model/Component"
], function (dcl,has,Component) {

    /**
     * @class xblox.component
     * @inheritDoc
     */
    return dcl(Component, {
        /**
         * @inheritDoc
         */
        beanType:'BLOCK',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        _load:function(){
        },
        hasEditors:function(){
            return ['xblox'];
        },
        getDependencies:function(){
            if( 1 ) {
                return [
                    'xide/xide',
                    'xblox/types/Types',
                    'xblox/manager/BlockManager',
                    'xblox/manager/BlockManagerUI',
                    'xblox/embedded_ui',
                    'xblox/views/BlockGridPalette',
                    'xide/widgets/ExpressionJavaScript',
                    'xide/widgets/Expression',
                    'xide/widgets/RichTextWidget',
                    'xide/widgets/ExpressionEditor',
                    'xide/widgets/WidgetReference'
                    //'xide/widgets/DomStyleProperties',
                    //'xblox/views/BlocksFileEditor'
                    //'xide/widgets/BlockPickerWidget',
                    //'xide/widgets/BlockSettingsWidget'
                ];
            }else{
                return [
                    'xide/xide',
                    'xblox/types/Types',
                    'xblox/manager/BlockManager',
                    'xblox/embedded'
                ];
            }
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return 'xblox';
        },
        /**
         * @inheritDoc
         */
        getBeanType:function(){
            return this.beanType;
        }
    });
});

