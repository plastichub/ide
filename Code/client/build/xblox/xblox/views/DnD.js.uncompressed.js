define("xblox/views/DnD", [
    'dojo/_base/declare',
    'dojo/when',
    'xgrid/DnD'
], function (declare, when, DnD) {

    var Source = DnD.GridSource;
    /**
     * @class module:xblox/views/BlockGridDnDSource
     * @extends module:xgrid/DnD/GridDnDSource
     */
    var BlockGridDnDSource = declare(DnD.GridSource, {
        declaredClass: 'xblox/views/BlockGridDnDSource',
        _lastTargetId: null,
        _lastTargetCrossCounter: 0,
        /**
         * Assigns a class to the current target anchor based on "before" status
         * @param before {boolean} insert before, if true, after otherwise
         * @param center {boolean}
         * @param canDrop {boolean}
         * @private
         */
        _markTargetAnchor: function (before, center, canDrop) {
            if (this.current == this.targetAnchor && this.before === before && this.center === center) {
                return;
            }
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, this.before ? "Before" : "After");
                this._removeItemClass(this.targetAnchor, "Disallow");
                this._removeItemClass(this.targetAnchor, "Center");
            }
            //track crossing count for trees, open it if crossed
            /*
             if(!this.current){
             this._lastTargetId=null;
             }else if(this.current.id!==this._lastTargetId){
             this._lastTargetCrossCounter=0;
             }else if(this.current.id==this._lastTargetId && center){
             this._lastTargetCrossCounter++;
             }
             */

            this.targetAnchor = this.current;
            this.targetBox = null;
            this.before = before;
            this.center = center;
            this._before = before;
            this._center = center;
            if (this.targetAnchor) {
                if (center) {
                    this._removeItemClass(this.targetAnchor, "Before");
                    this._removeItemClass(this.targetAnchor, "After");
                    this._addItemClass(this.targetAnchor, 'Center');
                }!center && this._addItemClass(this.targetAnchor, this.before ? "Before" : "After");
                center && canDrop === false && this._addItemClass(this.targetAnchor, "Disallow");

                /*
                 if(this._lastTargetCrossCounter>1 && this.grid.expand){
                 console.log('open');
                 }
                 this._lastTargetId = this.targetAnchor.id;
                 */

            }
        },
        /**
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:xide/data/Model}
         */
        onDropInternal: function (nodes, copy, targetItem) {
            var grid = this.grid,
                store = grid.collection,
                targetSource = this,
                anchor = targetSource._targetAnchor,
                targetRow,
                nodeRow;

            if (anchor) { // (falsy if drop occurred in empty space after rows)
                targetRow = this._center ? anchor : this.before ? anchor.previousSibling : anchor.nextSibling;
            }
            // Don't bother continuing if the drop is really not moving anything.
            // (Don't need to worry about edge first/last cases since dropping
            // directly on self doesn't fire onDrop, but we do have to worry about
            // dropping last node into empty space beyond rendered rows.)
            nodeRow = grid.row(nodes[0]);
            if (!copy && (targetRow === nodes[0] || (!targetItem && nodeRow && grid.down(nodeRow).element === nodes[0]))) {
                return;
            }
            var rows = grid.__dndNodesToModel(nodes);
            var _target = grid._sourceToModel(targetSource, grid);
            var DND_PARAMS = {
                before: this._before,
                center: this._center,
                targetGrid: grid
            };

            if (this._center) {
                grid.__onDrop(rows, _target, grid, DND_PARAMS, copy);
            }
            nodes.forEach(function (node) {
                when(targetSource.getObject(node), function (object) {
                    var id = store.getIdentity(object);
                    // For copy DnD operations, copy object, if supported by store;
                    // otherwise settle for put anyway.
                    // (put will relocate an existing item with the same id, i.e. move).
                    grid._trackError(function () {
                        return store[copy && store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {

                            // Self-drops won't cause the dgrid-select handler to re-fire,
                            // so update the cached node manually
                            if (targetSource._selectedNodes[id]) {
                                targetSource._selectedNodes[id] = grid.row(id).element;
                            }!this.center && grid.__onDrop(rows, _target, grid, DND_PARAMS, copy);
                        });
                    });
                });
            });
        },
        /**
         * @param sourceSource {module:dojo/dnd/Source}
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:dojo/dnd/Target}
         */
        onDropExternal: function (sourceSource, nodes, copy, targetItem) {
            // Note: this default implementation expects that two grids do not
            // share the same store.  There may be more ideal implementations in the
            // case of two grids using the same store (perhaps differentiated by
            // query), dragging to each other.
            var grid = this.grid,
                store = this.grid.collection,
                sourceGrid = sourceSource.grid,
                targetSource = this;

            function done(grid, object) {
                grid.refresh();
                return grid.select(object, null, true, {
                    focus: true,
                    append: false,
                    expand: true,
                    delay: 2
                }, 'mouse');
            }

            nodes.forEach(function (node, i) {
                when(sourceSource.getObject(node), function (object) {
                    var DND_PARAMS = {
                        before: targetSource._before,
                        center: targetSource._center,
                        targetGrid: grid
                    };
                    if (object.toDropObject) {
                        object = object.toDropObject(object, DND_PARAMS.center ? sourceGrid._sourceToModel(targetSource) : targetItem, DND_PARAMS, copy);
                        if (DND_PARAMS.center) {
                            return done(grid, object);
                        } else {
                            grid.refreshRoot();
                            setTimeout(()=>{
                                done(grid, object);
                            },100);                            
                        }
                    }
                    // Copy object, if supported by store; otherwise settle for put
                    // (put will relocate an existing item with the same id).
                    // Note that we use store.copy if available even for non-copy dnd:
                    // since this coming from another dnd source, always behave as if
                    // it is a new store item if possible, rather than replacing existing.
                    grid._trackError(function () {
                        return store[store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {
                            if (!copy) {
                                if (sourceGrid) {
                                    // Remove original in the case of inter-grid move.
                                    // (Also ensure dnd source is cleaned up properly)
                                    var id = sourceGrid.collection.getIdentity(object);
                                    !i && sourceSource.selectNone(); // Deselect all, one time
                                    sourceSource.delItem(node.id);
                                    done(grid, object);
                                    return sourceGrid.collection.remove(id);
                                } else {
                                    sourceSource.deleteSelectedNodes();
                                }
                            }
                        });
                    });
                });
            });
        }
    });

    /**
     * @class module:xblox/views/SharedDndGridSource
     * @extends module:xblox/views/BlockGridDnDSource
     * @extends module:xblox/views/DojoDndMixin
     */
    var SharedDndGridSource = declare([BlockGridDnDSource], {
        autoSync: true,
        skipForm: true,
        selfAccept: false,
        onDndDrop: function (source, nodes, copy, target) {
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, "Hover");
            }
            if (this == target) {
                // this one is for us => move nodes!
                this.onDrop(source, nodes, copy, target);
                this.grid.refresh();
            }
            this.onDndCancel();
        }
    });


    var dndParams = {
        allowNested: true, // also pick up indirect children w/ dojoDndItem class
        checkAcceptance: function (source, nodes) {
            return true; //source !== this; // Don't self-accept.
        },
        isSource: true
    };
    /**
     * @class module:xblox/views/DnD
     * @extends module:xgrid/DnD/
     */
    var Module = declare("xblox.widgets.DojoDndMixin", DnD, {
        dndParams: dndParams,
        dndConstructor: SharedDndGridSource, // use extension defined above
        dropEvent: "/dnd/drop",
        dragEvent: "/dnd/start",
        overEvent: "/dnd/source/over",
        /**
         * Final
         * @param sources {module:xblox/model/Block[]}
         * @param target {module:xblox/model/Block}
         * @param sourceGrid {module:xgrid/Base| module:dgrid/List}
         * @param params {object}
         * @param params.SAME_STORE {object}
         * @param copy {boolean}
         * @private
         */
        __onDrop: function (sources, target, sourceGrid, params, copy) {
            var into = params.center === true;
            _.each(sources, function (source) {
                if (source.toDropObject) {
                    source = source.toDropObject(source, target, params, copy);
                }
                //happens when dragged on nothing
                if (source == target) {
                    return;
                }
                if (into) {
                    if (target.canAdd(source) !== false) {
                        target.scope.moveTo(source, target, params.before, into);
                    }
                } else {
                    var sourceParent = source.getParent();
                    if (sourceParent) {
                        var targetParent = target.getParent();
                        if (!targetParent) {
                            //move to root - end
                            source.group = this.blockGroup;
                            sourceParent.removeBlock(source, false);
                            source._store.putSync(source);
                        } else {

                            //we dropped at the end within the same tree
                            if (targetParent == source.getParent()) {
                                target.scope.moveTo(source, target, params.before, into);
                            }
                        }
                    }
                }
                source.refresh();
            }, this);


            this.set('collection', this.collection.filter({
                group: this.blockGroup
            }));

            this.select(sources[0], null, true, {
                focus: true,
                append: false,
                expand: true,
                delay: 2
            }, 'mouse');
        }
        /*,
                startup: function () {
                    if (this._started) {
                        return;
                    }

                    this.dropEvent && this.subscribe(this.dropEvent, function (source, nodes, copy, target) {
                        return;
                        if (source.grid == this) {
                            var rows = this.__dndNodesToModel(nodes);
                            target = target || {};
                            var _target = this._sourceToModel(target, target.grid);

                            if (!_target || _.isEmpty(rows)) {
                                return;
                            }
                            var DND_PARAMS = {
                                SAME_STORE: rows[0]._store == _target._store,
                                before: target._before,
                                center: target._center
                            };
                            if (rows && _target) {
                                this.__onDrop(rows, _target, source.grid, DND_PARAMS, copy);
                            }
                        }
                    });
                    return this.inherited(arguments);
                }*/
    });

    Module.BlockGridSource = BlockGridDnDSource;

    return Module;
});