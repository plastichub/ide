define("xblox/views/BlockGridPalette", [
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xblox/views/BlockGrid',
    'xblox/BlockActions',
    'xide/views/_LayoutMixin',
    "xide/widgets/_Widget",
    'module',
    'xide/factory',
    'xblox/views/ThumbRenderer',
    "xblox/views/DnD",
    'dojo/dom-construct',
    'xgrid/Selection',
    'xgrid/KeyboardNavigation',
    'xaction/ActionStore',
    'xlang/i18',
    'xide/$',
    'xide/_base/_Widget',
    'xdojo/has',
    'dojo/has!debug?xide/tests/TestUtils'
], function (dcl, declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, Editor, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, BlockGrid,
             BlockActions, _LayoutMixin, _Widget, module, factory, ThumbRenderer, BlocksDnD, domConstruct,
             Selection, KeyboardNavigation, ActionStore, i18, $, _BWidget, has, TestUtils) {

    var ThumbClass = declare('xblox.ThumbRenderer2', [ThumbRenderer], {
        thumbSize: "400",
        resizeThumb: true,
        __type: 'thumb',
        deactivateRenderer: function () {
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding', '');
            this.isThumbGrid = false;

        },
        activateRenderer: function () {
            $(this.contentNode).css('padding', '8px');
            this.isThumbGrid = true;
            this.refresh();
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            var div = domConstruct.create('div', {
                    className: "tile widget form-control",
                    style: 'margin-left:8px;margin-top:8px;margin-right:8px;width:150px;height:initial;max-width:200px;float:left;padding:8px'
                }),
                label = obj.title,
                imageClass = obj.icon || 'fa fa-folder fa-2x';

            var iconStyle = 'float:left; margin-left:3px;margin-top:3px;margin-right:3px;text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: inherit;opacity: 0.4';
            var folderContent = '<span style="' + iconStyle + '" class="' + imageClass + '"></span>';
            div.innerHTML =
                '<div class="opacity">' +
                folderContent +
                '<span class="thumbText text opacity ellipsis" style="text-align:center">' + label + '</span>' +
                '</div>';
            return div;
        }
    });

    var GroupRenderer = declare('xblox.GroupRenderer', TreeRenderer, {
        itemClass: ThumbClass,
        groupClass: TreeRenderer,
        renderRow: function (obj) {
            return (!obj.group ? this.itemClass : this.groupClass).prototype.renderRow.apply(this, arguments);
        }
    });
    var Implementation = {},
        renderers = [GroupRenderer, ThumbClass, TreeRenderer],
        multiRenderer = declare.classFactory('xgrid/MultiRendererGroups', {}, renderers, MultiRenderer.Implementation);

    //console.clear();

    var SharedDndGridSource = BlocksDnD.BlockGridSource;
    var GridClass = Grid.createGridClass('xblox.views.BlockGridPalette', Implementation, {
            SELECTION: {
                CLASS: Selection
            },
            KEYBOARD_SELECTION: true,
            CONTEXT_MENU: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            WIDGET: {
                CLASS: _Widget
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            Dnd: {
                CLASS: BlocksDnD
            }
        },
        {
            RENDERER: multiRenderer
        },
        {
            renderers: renderers,
            selectedRenderer: GroupRenderer
        }
    );


    var HelpWidget = dcl(_BWidget, {
        templateString: '<div class="xbloxHelp"></div>',
        startup: function () {
        }
    });
    var GridCSourcelass = declare('ActionGrid', GridClass, {
        dndParams: {
            allowNested: false, // also pick up indirect children w/ dojoDndItem class
            checkAcceptance: function (source, nodes) {
                return false;//source !== this; // Don't self-accept.
            },
            isSource: true
        },
        formatColumn: function (field, value, obj) {
            var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
            if (renderer.formatColumn_) {
                var result = renderer.formatColumn.apply(arguments);
                if (result) {
                    return result;
                }
            }
            if (obj.renderColumn_) {
                var rendered = obj.renderColumn.apply(this, arguments);
                if (rendered) {
                    return rendered;
                }
            }
            switch (field) {
                case "title": {
                    value = obj.group ? ('<span style="float:left;margin-right: 10px">' + value + '</span><hr style="margin-top: 13px;margin-left: 4px"/>') : value;
                }
            }
            return value;
        },
        _columns: {},
        postMixInProperties: function () {
            var state = this.state;
            if (state) {
                if (state._columns) {
                    this._columns = state._columns;
                }
            }
            this.columns = this.getColumns();
            var newBlockActions = this.getAddActions();
            var levelName = '';
            var result = [];
            var BLOCK_INSERT_ROOT_COMMAND = '';
            var defaultMixin = {addPermission: true};
            var rootAction = BLOCK_INSERT_ROOT_COMMAND;
            var thiz = this;

            function addItems(commandPrefix, items) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    levelName = item.name;
                    var path = commandPrefix ? commandPrefix + '/' + levelName : levelName;
                    var isContainer = !_.isEmpty(item.items);
                    result.push(thiz.createAction({
                        label: levelName,
                        command: path,
                        icon: item.iconClass,
                        tab: 'Home',
                        mixin: utils.mixin({
                            item: item,
                            parent: isContainer ? "root" : commandPrefix,
                            quick: true,
                            title: i18.localize(levelName),
                            group: isContainer,
                            icon: item.iconClass,
                            toDropObject: function (item, targetItem, params) {
                                var blockArgs = item.item;
                                var ctrArgs = blockArgs.ctrArgs;
                                ctrArgs.id = null;
                                ctrArgs.items = null;
                                ctrArgs.scope = params.targetGrid.blockScope;
                                var proto = blockArgs.proto;
                                var parent = params.center === true && targetItem ? targetItem : null;
                                if (parent) {
                                    var block = factory.createBlock(proto, ctrArgs);//root block
                                    return parent.add(block, null, null);

                                } else {
                                    ctrArgs.group = params.targetGrid.blockGroup;
                                }
                                return factory.createBlock(proto, ctrArgs);//root block
                            }
                        }, defaultMixin)

                    }));

                    if (isContainer) {
                        addItems(path, item.items);
                    }
                }
            }

            addItems(rootAction, newBlockActions);
            var actionStore = new ActionStore({
                parentProperty: 'parent',
                parentField: 'parent',
                idProperty: 'command',
                mayHaveChildren: function (parent) {
                    if (parent._mayHaveChildren === false) {
                        return false;
                    }
                    return true;
                }
            });
            actionStore.setData(result);
            var all = actionStore.query(null);
            _.each(all, function (item) {
                item._store = actionStore;
            });
            var $node = $(this.domNode);
            $node.addClass('xFileGrid metro');
            this.collection = actionStore.filter({
                parent: "root"
            });
            return this.inherited(arguments);
        },
        getColumns: function (formatters) {
            var self = this;
            this.columns = [
                {
                    renderExpando: true,
                    label: 'Name',
                    field: 'title',
                    sortable: true,
                    formatter: function (value, obj) {
                        return self.formatColumn('title', value, obj);
                    },
                    hidden: false
                }
            ];
            return this.columns;
        },
        getAddActions: function (item) {
            var thiz = this;
            item = item || {};
            var items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
            //tell everyone
            thiz.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this,
                item: item,
                group: this.blockGroup,
                scope: this.blockScope
            });

            thiz._emit(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this
            });
            return items;


        },
        startup: function () {
            this._showHeader(false);
            
            this._on('selectionChanged', function (evt) {
                var selection = evt.selection,
                    item = selection[0];
                var view = this.lastSource;
                if (item && view && item.item.proto && item.item.proto.prototype) {
                    view.clearPropertyPanel();
                    var help = view.getHelp(item.item.proto.prototype);
                    if (help) {
                        var props = view.getPropertyStruct();
                        var target = view.__right;
                        var widget = utils.addWidget(HelpWidget, {}, null, target.containerNode, true);
                        $(widget.domNode).append(help);
                        props.targetTop = widget;
                    }
                }
            });
            var res = this.inherited(arguments);
            _.each(this.getRows(), function (row) {
                this.expand(row);
            }, this);
            $(this.domNode).addClass('blockPalette');
            return res;
        }
    });

    var ctx = window.sctx,
        root;

    function createContainer(ctx, title) {
        var leftContainer = ctx.mainView.leftLayoutContainer;
        return leftContainer.createTab(title, 'fa-cube', true, null, {
            parentContainer: leftContainer,
            open: true,
            icon: 'fa-folder'
        });
    }

    function handler(e, ctx) {
        function check() {
            if (ctx.blockPalette && ctx.blockPalette.sources.length === 0) {
                utils.destroy(ctx.blockPalette);
                utils.destroy(ctx.blockPalette._parent);
                ctx.blockPalette = null;
            }
        }

        function wireView(view) {
            if (ctx.blockPalette.sources.indexOf(view) === -1) {
                ctx.blockPalette.sources.push(view);
                view._on('destroy', function () {
                    ctx.blockPalette && ctx.blockPalette.sources.remove(view);
                    check();
                })
            }
        }

        if (ctx.blockPalette) {
            ctx.blockPalette.lastSource = e.view;
            wireView(e.view);
            return;
        }

        var target = createContainer(ctx, 'Block-Palette');
        var grid2 = utils.addWidget(GridCSourcelass, {
            ctx: ctx,
            attachDirect: true,
            open: true,
            sources: []
        }, null, target, true);
        target.add(grid2);
        ctx.blockPalette = grid2;
        grid2.lastSource = e.view;
        wireView(e.view);
    }

    function init(ctx) {
        function inner(e) {
            handler(e, ctx);
        }
        ctx.subscribe('ON_SELECTED_BLOCK', inner);
        ctx.subscribe('ON_BLOCK_GRID', inner);

    }
    var test = false;
    if (test && has('debug')) {
        function createScope() {
            var data = {
                "blocks": [
                    {
                        "_containsChildrenIds": [
                            "items"
                        ],
                        "group": "click",
                        "id": "root",
                        "items": [
                            "sub0",
                            "sub1"
                        ],
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 1",
                        "method": "console.log('asd',this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "icon": 'fa-bell text-fatal',
                        "_scenario": "update"
                    },

                    {
                        "group": "click4",
                        "id": "root4",
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 4",
                        "method": "console.log(this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0
                    },
                    {
                        "group": "click",
                        "id": "root2",
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 2",
                        "method": "console.log(this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0

                    },

                    {
                        "group": "click",
                        "id": "root3",
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Root - 3",
                        "method": "console.log(this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        'icon': 'fa-play'

                    },
                    {
                        "_containsChildrenIds": [],
                        "parentId": "root",
                        "id": "sub0",
                        "name": "On Event",
                        "event": "",
                        "reference": "",
                        "declaredClass": "xblox.model.events.OnEvent",
                        "_didRegisterSubscribers": false,
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "_scenario": "update"
                    },
                    {
                        "_containsChildrenIds": [],
                        "parentId": "root",
                        "id": "sub1",
                        "name": "On Event2",
                        "event": "",
                        "reference": "",
                        "declaredClass": "xblox.model.events.OnEvent",
                        "_didRegisterSubscribers": false,
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "_scenario": "update"
                    },
                    {
                        "_containsChildrenIds": [],
                        "group": "click",
                        "id": "6a12d54a-f5af-aaf8-0abb-bff866211fc4",
                        "declaredClass": "xblox.model.logic.SwitchBlock",
                        "name": "Switch",
                        "outlet": 0,
                        "enabled": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "order": 0,
                        "type": "added"
                    },
                    {
                        "_containsChildrenIds": [],
                        "group": "click",
                        "id": "8e1c9d2a-d2fe-356f-d484-82e27c793dc9",
                        "declaredClass": "xblox.model.variables.VariableSwitch",
                        "name": "Switch on Variable",
                        "icon": "",
                        "variable": "PowerState",
                        "outlet": 0,
                        "enabled": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "order": 0,
                        "type": "added"
                    }
                ],
                "variables": []
            };
            return blockManager.toScope(data);
        }
        if (ctx) {
            var blockManager = ctx.getBlockManager();
            if(!blockManager){
                return;
            }

            function createGridClass() {
                var renderers = [TreeRenderer, ThumbRenderer];

                var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
                return Grid.createGridClass('driverTreeView', {
                        options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                    },
                    //features
                    {

                        SELECTION: true,
                        KEYBOARD_SELECTION: true,
                        PAGINATION: false,
                        ACTIONS: types.GRID_FEATURES.ACTIONS,
                        CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                        TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                        CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                        SPLIT: {
                            CLASS: _LayoutMixin
                        },
                        Dnd: {
                            CLASS: BlocksDnD
                        },
                        Base: {
                            CLASS: declare('Base', null, BlockGrid.IMPLEMENTATION)
                        },
                        BLOCK_ACTIONS: {
                            CLASS: BlockActions
                        },
                        WIDGET: {
                            CLASS: _Widget
                        }
                    },
                    {
                        //base flip
                        RENDERER: multiRenderer

                    },
                    {
                        //args
                        renderers: renderers,
                        selectedRenderer: TreeRenderer
                    },
                    {
                        GRID: OnDemandGrid,
                        EDITOR: Editor,
                        LAYOUT: Layout,
                        DEFAULTS: Defaults,
                        RENDERER: ListRenderer,
                        EVENTED: EventedMixin,
                        FOCUS: Focus
                    }
                );
            }

            var blockScope = createScope('docs');
            var mainView = sctx.mainView;
            if (mainView) {
                if (window['dHandle']) {
                    window['dHandle'].remove();
                }
                if (window['dHandle2']) {
                    window['dHandle2'].remove();
                }
                window['dHandle'] = ctx.subscribe('ON_SELECTED_BLOCK', handler);
                window['dHandle2'] = ctx.subscribe('ON_BLOCK_GRID', handler);

                var parent = TestUtils.createTab('BlockGrid-Test-Source', null, module.id);
                var thiz = this,
                    grid, grid2;
                var store = blockScope.blockStore;
                var _gridClass = createGridClass();
                var dndParams = {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                };
                var gridArgs = {
                    ctx: ctx,
                    /**
                     * @type {module:xblox/views/SharedDndGridSource}
                     */
                    dndConstructor: SharedDndGridSource, // use extension defined above
                    blockScope: blockScope,
                    blockGroup: 'click',
                    dndParams: dndParams,
                    name: "Grid - 1",
                    collection: store.filter({
                        group: "click"
                    })
                };
                var gridArgs2 = {
                    ctx: ctx,
                    attachDirect: true,
                    open: true

                };
                grid = utils.addWidget(_gridClass, gridArgs, null, parent, true);

            }
        }
    }
    GridCSourcelass.init = init;
    return GridCSourcelass;
});