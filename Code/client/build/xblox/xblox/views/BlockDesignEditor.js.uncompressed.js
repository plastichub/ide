/** @module xgrid/Base **/
define("xblox/views/BlockDesignEditor", [
    "xdojo/declare",
    'dojo/dom-class',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/ThumbRenderer',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    "xblox/widgets/BlockGridRowEditor",
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',

    'xide/factory',


    'dijit/Menu',

    'xide/data/Reference',

    'dijit/form/DropDownButton',
    'dijit/MenuItem',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    "dojo/has!host-browser?xblox/views/BlockEditDialog",
    'xblox/views/BlockGrid',
    'xgrid/DnD',
    'xblox/views/BlocksGridDndSource',
    'xblox/widgets/DojoDndMixin',
    'xide/registry',
    'dojo/topic',
    //'xide/tests/TestUtils',
    //'./TestUtils',
    'module'
], function (declare, domClass,types,
             utils, ListRenderer, TreeRenderer, ThumbRenderer,
             _ActionMixin,
             Grid, MultiRenderer, RadioButton, Ribbon, Registry, DefaultActions, Action, BlockGridRowEditor,
             Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Menu,Reference,DropDownButton,
             MenuItem,CIViewMixin,TabContainer,

             BlockEditDialog,
             BlockGrid,
             Dnd,BlocksGridDndSource,DojoDndMixin,
             registry,topic,TestUtils,BlockUtils,module
    ) {
    /**
     *
     */

	


		/***
     * Block - Designer - Remarks
     * 
     * 
     * VIEW : 
     * 
     * 			1. Source - VIEW
     * 			2. Block  - Grid  ( Properties)
     * 			3. Console
     * 			4. 
     * 
     * 
     */


    /***
     * playground
     */
    var ctx = window.sctx;

    function doGridTest(grid,scope){



        grid.refresh().then(function(){
            grid.select([0]).then(function(e){
                grid.runAction('Step/Run');
            });
        });

    }




    if (ctx) {

        var blockManager = ctx.getBlockManager();
        var blockScope = BlockUtils.createScope(blockManager,[
            {
                "_containsChildrenIds": [],
                "id": "Shell-Block",
                "description": "Runs a JSON-RPC-2.0 method on the server",
                "name": "Run Server Method",
                "method": "XShell::run",
                "args": "ls",
                "deferred": true,
                "defaultServiceClass": "XShell",
                "defaultServiceMethod": "run",
                "declaredClass": "xblox.model.server.RunServerMethod",
                "enabled": true,
                "shareTitle": "",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "group": "click"
            }
        ]);


        var mainView = ctx.mainView;

        if (mainView) {

            var parent = TestUtils.createTab(null,null,module.id);

            var grid,
                store = blockScope.blockStore,
                gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                collection: store.filter({
                    group: "click"
                })
            };

            grid = utils.addWidget(BlockGrid,gridArgs,null,parent,true);

            ctx.getWindowManager().registerView(grid,true);


            var blocks = blockScope.allBlocks();




            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {
                doGridTest(grid,blockScope);
                return;

            }

            function test2() {
                return;
            }

            setTimeout(function () {
                test();
            }, 1000);


            setTimeout(function () {
                test2();
            }, 2000);

        }
    }

    return Grid;

});