define("xdocker/_Base", [
    "dojo/_base/declare",
    'xide/mixins/EventedMixin',
    'xide/registry'
], function (declare, EventedMixin,registry) {
    /**
     * @class xdocker/_Base
     * Base class for all docker classes
     */
    return declare('xide.docker._Base', EventedMixin, {

        /**
         * Returns all dojo/dijit widgets in this node
         * @param startNode
         * @returns {_WidgetBase[]}
         * @private
         */
        _findWidgets:function(startNode){

            var result = [],
                node = startNode || this.containerNode;

            _.each(node.children, function (child) {
                if (child.id) {
                    var _widget = registry.byId(child.id);
                    if (_widget){
                        result.push(_widget);
                    }
                }
            });

            return result;
        },

        /**
         * Search upwards for a parent by class string or module
         * @todo get rid of declared class in xDocker
         * @param className {string|Object}
         * @returns {*}
         * @private
         */
        _parentByClass: function(className) {
            var parent = this._parent;
            if(_.isString(className)) {
                while (parent && !(parent.declaredClass === ('xide.docker.' + className))) {
                    parent = parent._parent;
                }
            }

            return parent;
        }
    });
});