/** @module xdocker/Panel2 */
define("xdocker/Panel2", [
    "dcl/dcl",
    "dcl/inherited",
    'xide/types',
    'xide/utils',
    'wcDocker/panel',
    'xide/mixins/EventedMixin',
    'xide/registry',
    'wcDocker/types',
    'xide/widgets/_Widget'
], function (dcl,inherited,types,utils,panel,EventedMixin,registry,wcDocker,_Widget) {

    /**
     * @class module:xdocker/Panel2
     * @extends module:wcDocker/panel
     */
    return dcl([panel,_Widget.dcl,EventedMixin.dcl], {
        selected:false,
        _isVisible:function(){
            var node = $(this.containerNode);
            if(!node.length){
                return false;
            }
            if(node[0].offsetHeight === 0){
                return false;
            }else if(node.css("display") == "none"){
                return false;
            }else if((node.css("visibility") == "hidden")){
                return false;
            }
            return true;
        },
        next:function(direction){
            var frame = this.getFrame();
            if(!frame){
                return;
            }
            var pos = parseInt(frame._curTab,10);
            return frame.panelAt(pos + direction);
        },
        set:function(key,value){
            if(key==='changed'){
                if(this.$title){
                    value ? this.$title.addClass('changed') : this.$title.removeClass('changed');
                }
                return true;
            }
            if(key==='loading'){
                value ? this.startLoading() : this.finishLoading();
            }
            if(key==='height'){
                var parent = $(this.containerNode);
                parent.css('height', value+ 'px');
                this._maxSize.y = value;
                this._minSize.y = value;
                this._actualSize.y = value;
                return true;
            }
            return this.inherited(arguments);
        },
        isExpanded:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return true;
            }
            return splitter.isExpanded();
        },
        isCollapsed:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            return splitter.isCollapsed();
        },
        expand:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            splitter.expand();
            return _.invoke(this._findWidgets,'onShow');
        },
        collapse:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            splitter.collapse(splitter._pane[0].panelIndex(this) == -1 ? 1 : 0);
            return _.invoke(this._findWidgets,'onHide');
        },
        // stub
        onSaveLayout:function(e){
            this._emit('onSaveLayout',e);
        },
        onRestoreLayout:function(e){
            this._emit('onRestoreLayout',e);
        },
        // Initialize
        __init: function () {
            this.on(wcDocker.EVENT.SAVE_LAYOUT,function(data){
                data.title = this.title();
            });
            this.on(wcDocker.EVENT.RESTORE_LAYOUT,function(data){
                data.title && this.title(data.title);
            });
            var layoutClass = (this._options && this._options.layout) || 'wcLayoutTable';
            this._layout = new (this.docker().__getClass(layoutClass))(this.$container, this);
            this.$title = $('<li class="wcPanelTab">');
            this.$titleText = $('<a>' + this._title + '</a>');
            this.$title.append(this.$titleText);

            if (this._options.hasOwnProperty('title')) {
                this.title(this._options.title);
            }

            if (this._options.icon) {
                this.icon(this._options.icon);
                this._icon = this._options.icon;
            }
            if (this._options.faicon) {
                this.faicon(this._options.faicon);
            }

            if(!this.id){
                this.id = registry.getUniqueId(this.declaredClass.replace(/\./g, "_"));
            }

            utils.mixin(this,this._options.mixin);
            registry.add(this);
        },
        /**
         * Returns all dojo/dijit widgets in this node
         * @param startNode
         * @returns {_WidgetBase[]}
         * @private
         */
        _findWidgets:function(startNode){
            var result = [],
                node = startNode || this.containerNode || ( this.$container ? this.$container[0] : null);

            if(node) {
                _.each(node.children, function (child) {
                    if (child.id) {
                        var _widget = registry.byId(child.id);
                        if (_widget) {
                            result.push(_widget);
                        }
                    }
                });
            }
            this._widgets && (result = result.concat(this._widgets));
            return result;
        },

        __onShow:function(){
            _.each(this._findWidgets(), function (widget) {});
        },

        /**
         * Std API
         */
        destroy: function (removeFromDocker) {
            this.inherited(arguments);
            if(removeFromDocker!==false) {
                var _docker = this.docker();
                _docker && _docker.removePanel(this);
            }
            registry.remove(this.id);
        },
        __destroy: function () {
            this._emit('destroy');
            _.each(this._findWidgets(),function(w){
                if(w && w.destroy && !w._destroyed){
                    w.destroy();
                }
            });
            this.inherited(arguments);
            this._destroyHandles();
            this.off();
        }
    });
});