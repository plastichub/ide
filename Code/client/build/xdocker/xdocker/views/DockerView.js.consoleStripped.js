/** @module xgrid/Base **/
define("xdocker/views/DockerView", [
    'xdojo/declare',
    'xide/types',
    'xdocker/Docker2',
    'xdocker/Panel2',
    'dojo/has!debug?xide/tests/TestUtils',
    'dojo/has!debug?xfile/tests/TestUtils',
    'dcl/dcl',
    'require',
    'xide/registry',
    'xide/utils',
    'xide/views/_LayoutMixin',
    'xide/_base/_Widget',
    'xaction/ActionProvider',
    'xfile/Breadcrumb',
    'xide/widgets/MainMenu',
    'module'

], function (declare,types,Docker,Panel,TestUtils,FTestUtils,dcl,require,registry,utils,_LayoutMixin,_Widget,ActionProvider,Breadcrumb,MainMenu,module) {

    var mainViewPermissions = {
        "MAIN_MENU":"MAIN_MENU",
        "RIBBON":"RIBBON",
        "BREADCRUMB":"BREADCRUMB"
    };
    utils.mixin(types.ACTION,mainViewPermissions);

    var ACTION = types.ACTION;
    /***
     *
     * playground
     */
    var leftGrid = window[module.id + 'leftGrid'];
    var ctx = window.sctx,
        parent;

    var LayoutClass = dcl(null,{});
    var BorderLayoutClass = dcl(LayoutClass,{

        getLayoutMain:function(args,create){
        },
        getLastTab:function(){
            var panels = this.getDocker().allPanels(),
                main = this.getLayoutMain();
            return _.find(panels,function(panel){
                var canAdd = panel.option('canAdd');
                return panel!=main && canAdd == true;
            });
        },
        getNewDefaultTab:function(args){

            var docker = this.getDocker(),
                main = this.getLayoutMain(),
                last = this.getLastTab();

            var targetTab  = last || main;

            utils.mixin(args,{
                target: targetTab,
                location:types.DOCKER.DOCK.STACKED,
                tabOrientation : types.DOCKER.TAB.TOP
            });
            if(targetTab && targetTab.option('autoHideTabBar')==true){
                targetTab.getFrame().showTitlebar(true);
                var title = targetTab.option('titleStacked');
                targetTab.title(title);
            }
            var tab = docker.addTab(null, args);
            setTimeout(function(){
                docker.resize();
            },100);
            return tab;
        },

        createLayout:function(docker,permissions){
            var top;
            if(this.hasAction(ACTION.BREADCRUMB) || this.hasAction(ACTION.MAIN_MENU) || this.hasAction(ACTION.RIBBON)) {

                top = this.layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                    h: 50,
                    title: false,
                    canAdd:false,
                    mixin: {
                        resizeToChildren: true,
                        _scrollable: {
                            x: false,
                            y: false
                        }
                    }
                });

                if(this.hasAction(ACTION.MAIN_MENU)){
                    var _args = {
                        style: 'height:auto',
                        resizeToParent: false
                    };

                    this.mainMenu = utils.addWidget(MainMenu, _args, this, top, true);
                    $(this.mainMenu.domNode).css('height:auto');
                }


                if(this.hasAction(ACTION.BREADCRUMB)) {

                    var breadcrumb = utils.addWidget(Breadcrumb, {
                        resizeToParent: false
                    }, null, top, true);

                    breadcrumb.resizeToParent = false;
                    $(breadcrumb.domNode).css('height:auto');

                    this.breadcrumb = breadcrumb;
                }
            }

            this.layoutLeft = docker.addPanel('DefaultTab', types.DOCKER.DOCK.BOTTOM, top, {
                title:'&nbsp;&nbsp;',
                autoHideTabBar:true,
                titleStacked:"Navigation"
            });


            this.layoutLeft.closeable(false);


            this.layoutLeft.resizeToChildren=false;
            this.layoutLeft.getFrame().showTitlebar(false);
            //this.layoutLeft.initSize('100%','100%');
            this.layoutLeft.uuid = 'left';

            // 0 && console.dir(top._parent.$container.css('padding'));
            // 0 && console.log( _c.height() + ' ' + _c.outerHeight(),_c);
            docker.resize();

        }
    });





    var DockerView = dcl([_Widget,_LayoutMixin.dcl,ActionProvider.dcl,BorderLayoutClass],{
        templateString:'<div style="height: 100%;width: 100%;"></div>',
        permissions:[

            ACTION.BREADCRUMB,
            //ACTION.RIBBON,
            ACTION.MAIN_MENU
        ],
        getLayoutMain:function(){
            return this.layoutLeft;
        },
        onLastPanelClosed:function(){

            var main = this.getLayoutMain();

            console.error('last closed');
            if(main.option('autoHideTabBar')){
                main.title(main.option('title'));
                main.getFrame().showTitlebar(false);
            }
            this.getDocker().resize();
        },
        onPanelClosed:function(panel){
            var self = this,
                main = self.getLayoutMain(),
                docker = self.getDocker();

            setTimeout(function(){
                var panels = docker.allPanels().filter(function(panel){
                    return panel.option('canAdd') !== false;
                });

                if(panels.length==1 && panels[0] == main){
                    self.onLastPanelClosed();
                }
            },1);

            this.getDocker().resize();

        },
        startup:function(){

            var docker = Docker.createDefault(this.domNode);
            this._docker = docker;
            var self = this;
            docker._on(types.DOCKER.EVENT.CLOSED,function(panel){
                self.onPanelClosed(panel);
            });
            this.createLayout(docker,this.permissions);
        }

    });


    function completeView(view){

        var _args = {
            style:'height:auto',
            resizeToParent:false
        };
        var top = view.layoutTop,
            docker = view.getDocker(),
            breadcrumb = view.breadcrumb,
            mainMenu = view.mainMenu,
            main = view.getLayoutMain();

        if(top) {
/*
            mainMenu = utils.addWidget(MainMenu, _args, this, top, true);
            $(mainMenu.domNode).css('height:auto');


            breadcrumb = utils.addWidget(Breadcrumb, {
                resizeToParent: false
            }, null, top, true);

            breadcrumb.resizeToParent = false;

            $(breadcrumb.domNode).css('height:auto');
*/


        }

        var grid = FTestUtils.createFileGrid('root',{
                resizeToParent:true,
                newTarget:function(args){
                    return view.getNewDefaultTab(args);
                }
            },
            //overrides
            {

            },'TestGrid',module.id,true,view.getLayoutMain());


        if(breadcrumb) {
            breadcrumb.setSource(grid);
            var srcStore = grid.collection;


            function _onChangeFolder(store, item, grid) {
                if (breadcrumb.grid != grid) {
                    breadcrumb.setSource(grid);
                }
                breadcrumb.clear();
                breadcrumb.setPath('.', srcStore.getRootItem(), item.getPath(), store);
            }

            grid._on('openedFolder', function (evt) {
                console.error('opened folder!!');
                _onChangeFolder(srcStore, evt.item, grid);
            });
        }


        if(mainMenu) {

            mainMenu._registerActionEmitter(grid);
            mainMenu.setActionEmitter(grid);

            var totalHeight = utils.getHeight([mainMenu, breadcrumb]);
            top._minSize.y = totalHeight;
            top.set('height', totalHeight);
            top.resize();
        }
        setTimeout(function(){
            //grid.resize();
            //docker.resize();
            main.resize();

        },1);
        // 0 && console.dir(top._findWidgets());
    }

    function testDockerView(parent){

        var view = utils.addWidget(DockerView,{
            resizeToParent:true
        },null,parent,true);

        completeView(view);

        setTimeout(function(){

            var docker = view.getDocker(),
                main = view.getLayoutMain();


            var targetTab  = main;
            

            view.getBottomPanel('props',0.1);

            //debugger;

            //targetTab.title(false);

            //targetTab.resize();
            //targetTab.getFrame().showTitlebar(true);

            //targetTab.resize();

/*
            if(targetTab && targetTab.option('autoHideTabBar')==true){

                targetTab.getFrame().showTitlebar(true);
                var title = targetTab.option('titleStacked');
                targetTab.title(title);
                targetTab.resize();
            }
            */

            setTimeout(function(){
                //docker.resize();
            },100);



        },1000);

    }


    function testMain(tab){


        var mainView = ctx.mainView;
        var mainDocker = mainView.getDocker();
        var parent = TestUtils.createTab(null,null,module.id);
        parent.uuid = 'main tab';

        setTimeout(function(){
            testDockerView(parent);
        },1000);

    }

    if (ctx) {
        var doTest = true;
        if (doTest) {
            var mainView = ctx.mainView;
            testMain();

        }
    }
    return declare('m',null,[]);
});