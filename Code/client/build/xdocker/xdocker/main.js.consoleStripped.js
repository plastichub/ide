define("xdocker/main", [
	"dojo/_base/kernel",
    "xdocker/Docker2",
    'xdocker/Panel2',
    'xdocker/Frame2'
], function(){
    return dojo.xdocker;
});
