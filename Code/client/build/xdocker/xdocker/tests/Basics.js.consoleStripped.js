/** @module xgrid/Base **/
define("xdocker/tests/Basics", [
    'xdojo/declare',
    'xide/types',
    'xdocker/Docker2',
    'xdocker/Panel2',
    'xide/tests/TestUtils',
    'xfile/tests/TestUtils',
    'dcl/dcl',
    'require',
    'xide/registry',
    'xide/utils',
    'module'

], function (declare,types,Docker,Panel,TestUtils,FTestUtils,dcl,require,registry,utils,module) {

    console.clear();

    /***
     *
     * playground
     */
    var leftGrid = window[module.id + 'leftGrid'];
    var ctx = window.sctx,
        parent;






    ///////////////////////////////////////////////////
    //
    //
    //
    //  Switched
    //
    //

    var CREATE_LEFT_FILE_GRID = false;


    function createDockerClass(){

        var _class = dcl(null,{

        });

        return _class;
    }

    function createSubDocker(tab){

        var myDocker = Docker.createDefault(tab.containerNode, {
            extension:createDockerClass()
        });
        myDocker.uuid = 'sub docker';
        return myDocker;
    }
    

    function addFileGridToLeft(){

        utils.destroy(leftGrid);


        var mainView = ctx.mainView,
            application = ctx.application,
            toolbar = mainView.getToolbar(),
            docker = mainView.getDocker(),
            left = mainView.layoutLeft,
            container = application.leftLayoutContainer;


        var grid = FTestUtils.createFileGrid(null,{},null,'Files',module.id,true,container);

        window[module.id + 'leftGrid'] = grid;

        return grid;

    }

    function _saveProp(who,what,args){
        who['__' + what] = _.isFunction(who[what]) ? who[what]() : who[what];
        args && who[what].apply(who,args);
    }

    function _restoreProp(who,what,call){
        //this prop also exists
        if(who['_' + what]){
            who['_' + what] = who['__' + what];
        }
        var _args = who['__' + what];
        if(call!==false) {
            return _.isFunction(who[what]) ? who[what].apply(who, [_args]) : who[what];
        }else {
            return _args;
        }
    }

    function restoreFrameProps(frame){

        _.each([frame.$tabBar,frame.$buttonBar],function(el){
            el.css('display','inherit');
        });
        _.each(frame._panelList,function(panel){

            //restore title & min size
            var ms = _restoreProp(panel,'minSize',false);
            panel.minSize(ms.x,ms.y);
            _restoreProp(panel,'title');
        });

        _restoreProp(frame,'showTitlebar');

    }

        //if (this._orientation === wcDocker.ORIENTATION.HORIZONTAL) {
        //splitter.orientation(types.DOCKER.ORIENTATION.VERTICAL);

        /*
        splitter.__getState = function(){

            var pane0 = this._pane[0],
                pane1 = this._pane[1];

            return {
                pos:{
                    now:this.pos(),
                    min:this.__minPos(),
                    max:this.__maxPos()
                },

                min:{
                    pane0:pane0.minSize(),
                    pane1:pane0.minSize()
                },
                max:{
                    pane0:pane0.maxSize(),
                    pane1:pane1.maxSize()
                }
            }

        };*/


    function setFramePropsMin(frame,hide){

        _saveProp(frame,'minSize',[0,0]);
        _saveProp(frame,'pos');
        _saveProp(frame,'showTitlebar',[]);//call with undefined

        if(hide!==false) {
            _.each([frame.$tabBar, frame.$buttonBar], function (el) {
                el.css('display', 'none');
            });
        }

        _.each(frame._panelList,function(panel){
            //save title & min size
            _saveProp(panel,'minSize',[0,0]);
            _saveProp(panel,'title');
        });


        hide && frame.showTitlebar(false);

    }

    function testMinimize(left,right){

        left.expand = function(){

            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            return splitter.expand();
        }

        left.collapse = function(){

            var splitter = this.getSplitter(),
                frame = this.getFrame();

            if(!splitter){
                return false;
            }

            /*
            var pane0 = splitter._pane[0],
                pane1 = splitter._pane[1];

            var panelIndex = pane0.panelIndex(this);
            var panelIndex1 = pane1.panelIndex(this);
            var side = panelIndex == -1 ? 1:0;
             0 && console.log('panel index  ' + panelIndex + ' ' + panelIndex1  + ' side ' + side);
            */

            return splitter.collapse(splitter._pane[0].panelIndex(this) == -1 ? 1 : 0);
        }

        var splitter = left.getSplitter();


        /**
        splitter.expand=function(){

            var pane0 = this._pane[0],
                pane1 = this._pane[1];

            restoreFrameProps(pane0);
            _restoreProp(this,'pos');
            restoreFrameProps(pane1);

        };

        splitter.collapse=function(side){

            var pane0 = this._pane[0],
                pane1 = this._pane[1];

            _saveProp(this,'pos');
            setFramePropsMin(pane0,side==1);
            setFramePropsMin(pane1,side==0);
            this.pos(1);
            var thiz = this;
            setTimeout(function(){
                thiz.expand();

            },2000);
        }
        */

        //splitter.collapse(0);


        //left.expand();

        left.collapse();


        //left.expand();


        setTimeout(function(){
            left.expand();
        },2000);
    }

    function testMinimize2(left,right,gridLeft,gridRight){


        var grid = ctx.application.fileGrid;
        var _parent = grid.parentByClass('wcPanel');
         0 && console.log('parent ', _parent);

        //left.collapse();
        setTimeout(function(){
            //left.expand();
        },2000);
    }

    function testMain(tab){


        CREATE_LEFT_FILE_GRID && addFileGridToLeft();



        var mainView = ctx.mainView;
        var mainDocker = mainView.getDocker();
        var parent = TestUtils.createTab('test',null,module.id);
        parent.uuid = 'main tab';

/*
        parent.onResize=function(){

             0 && console.log('---main tab resize');

            //transfer size and resize on widgets
            _.each(this.containerNode.children, function (child) {

                // 0 && console.log('       ',child);
                var _widget = registry.byId(child.id);

                 0 && console.log('docker ' + child.id,_widget);


                child = $(child);

                if(child.hasClass('wcDocker')){


                };


            });
        };
        */
        var docker = createSubDocker(parent);

        var fileTab = docker.addTab(null,{
            title:'Left Panel'
        });

        var fileTabRight = docker.addTab(null,{
            title:'Right Panel',
            target:fileTab,
            /*location:types.DOCKER.DOCK.STACKED*/
            location:types.DOCKER.DOCK.RIGHT
        });



        fileTab.uuid = 'docker holder';


        var grid = FTestUtils.createFileGrid(null,{},null,'Files Left',module.id,true,fileTab);
//collection:grid.collection
        var grid1 = FTestUtils.createFileGrid(null,{

        },null,'Files Right',module.id,true,fileTabRight);


        setTimeout(function(){
            testMinimize2(fileTab,fileTabRight,grid,grid1);
        },1000);

    }

    if (ctx) {

        var doTest = true;
        if (doTest) {

            var mainView = ctx.mainView;
            setTimeout(function () {
                mainView.resize();
            }, 1000);

            function test() {
                testMain(parent);
            }
            setTimeout(function () {
                test();
            }, 2000);



        }
    }
    return declare('m',null,[]);
});