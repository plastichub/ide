/** @module xgrid/Base **/
define("xdocker/tests/Absolute", [
    'xdojo/declare',
    'xide/types',
    'xide/utils',
    'xdocker/Docker2',
    'xdocker/Panel2',
    'dcl/dcl',
    'require',
    'xide/tests/TestUtils',
    'wcDocker/absolute'

], function (declare, types, utils, Docker, Panel, dcl, require, TestUtils, wcAbsolute) {
    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent;

    function createDockerClass() {}


    function createSubDocker(tab) {

        var mainView = ctx.mainView;

        var docker = mainView.getDocker();

        require({
            cacheBust: null
        });

        // 0 && console.log('theme url  ' + require.toUrl('xdocker') + '/Themes/' );

        var myDocker = new Docker.createDefault(tab.containerNode, {
            uuid: 'sub'
        });
        console.clear();
        myDocker.registerPanelType('Info Panel', {
            isPrivate: true,
            onCreate: function (myPanel) {
                let id = 'uuid.v4()';
                let panelId = 'p12';
                myPanel.id = panelId;
                var $container = $(`<div style="position:absolute;top:0px;left:0px;right:0px;bottom:0px;"></div>`);
                var $content = $(`<div style="height: 100%; width: 100%" id="${id}">bla</div>`);
                var absolutePanel = new wcAbsolute($container, myPanel);
                absolutePanel.setContent($content);
                myPanel.layout().addItem($container);
                myPanel.closeable(true);
                /*
                setTimeout(() => {
                    render(
                        <WebView src="http://www.xero.com" style={{height: '100%', width: '100%'}}/>
                        , $content[0])
                }, 0);*/
            }
        });

        
        var infoPanel = myDocker.addPanel('Info Panel', types.DOCKER.DOCK.TOP, null);



        return myDocker;
    }


    function addPanes(docker) {

        docker.uuid = 'sub';

        /*
        var tab = docker.addTab(null, {
            title: 'Sub Tab',
            icon: 'fa-folder'
        });
        */

        var defaultTabArgs = {
            icon: 'fa-folder',
            closeable: false,
            movable: false,
            moveable: true,
            tabOrientation: types.DOCKER.TAB.TOP,
            location: types.DOCKER.DOCK.STACKED
        };

        // 'Basic' commands tab
        var basicCommandsTab = docker.addTab('DefaultFixed', utils.mixin(defaultTabArgs, {
            title: 'Basic Commands',
            h: '90%'
        }));

        return;

        // 'Conditional' commands tab
        var condCommandsTab = docker.addTab('DefaultFixed', utils.mixin(defaultTabArgs, {
            title: 'Conditional Commands',
            target: basicCommandsTab,
            select: false,
            h: '90%',
            tabOrientation: types.DOCKER.TAB.TOP
        }));



        // 'Variables' tab
        var variablesTab = docker.addTab(null,
            utils.mixin(defaultTabArgs, {
                title: 'Variables',
                target: condCommandsTab,
                select: false,
                h: 100,
                tabOrientation: types.DOCKER.TAB.BOTTOM,
                location: types.DOCKER.TAB.BOTTOM
            }));


        // 'Response' tab
        var responsesTab = docker.addTab(null,
            utils.mixin(defaultTabArgs, {
                title: 'Responses',
                target: variablesTab,
                select: false,
                h: 100,
                icon: null,
                tabOrientation: types.DOCKER.TAB.BOTTOM,
                location: types.DOCKER.DOCK.STACKED
            }));

        var logsTab = docker.addTab(null,
            utils.mixin(defaultTabArgs, {
                title: 'Log',
                target: responsesTab,
                select: false,
                icon: 'fa-calendar',
                tabOrientation: types.DOCKER.TAB.BOTTOM,
                location: types.DOCKER.DOCK.STACKED
            }));

        var right = docker.addPanel('Collapsible', types.DOCKER.DOCK.RIGHT, condCommandsTab, {
            w: '30%',
            title: ' props '
        });


    }

    function testMain(tab) {

        var subDocker = createSubDocker(tab);
        subDocker.resize();

        //addPanes(subDocker);

    }

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var mainView = ctx.mainView;

            var docker = mainView.getDocker();



            docker.uuid = 'main';
            if (window._lastGrid) {
                docker.removePanel(window._lastGrid);
            }


            parent = docker.addTab(null, {
                title: 'TestDocker',
                icon: 'fa-folder'
            });
            window._lastGrid = parent;
            testMain(parent);
        }
    }
    return declare('m', null, []);
});