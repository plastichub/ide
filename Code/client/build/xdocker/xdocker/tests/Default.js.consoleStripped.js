/** @module xgrid/Base **/
define("xdocker/tests/Default", [
    'xdojo/declare',
    'xide/types',
    'xide/utils',
    'xdocker/Docker2',
    'xdocker/Panel2',
    'dcl/dcl',
    'require',
    'xide/tests/TestUtils'

], function (declare,types,utils,Docker,Panel,dcl,require,TestUtils) {
    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent;

    function createDockerClass(){}


    function createSubDocker(tab){

        var mainView = ctx.mainView;

        var docker = mainView.getDocker();

        require({cacheBust: null});

        // 0 && console.log('theme url  ' + require.toUrl('xdocker') + '/Themes/' );

        var myDocker = new Docker.createDefault(tab.containerNode, {
            uuid:'sub'
        });




        return myDocker;
    }


    function addPanes(docker){

        docker.uuid='sub';

        /*
        var tab = docker.addTab(null, {
            title: 'Sub Tab',
            icon: 'fa-folder'
        });
        */

        var defaultTabArgs = {
            icon:'fa-folder',
            closeable:false,
            movable:false,
            moveable:true,
            tabOrientation:types.DOCKER.TAB.TOP,
            location:types.DOCKER.DOCK.STACKED
        };

        // 'Basic' commands tab
        var basicCommandsTab = docker.addTab('DefaultFixed',utils.mixin(defaultTabArgs,
            {
                title: 'Basic Commands',
                h:'90%'
            }));

        // 'Conditional' commands tab
        var condCommandsTab = docker.addTab('DefaultFixed',utils.mixin(defaultTabArgs,{
                title: 'Conditional Commands',
                target:basicCommandsTab,
                select:false,
                h:'90%',
                tabOrientation:types.DOCKER.TAB.TOP
        }));



        // 'Variables' tab
        var variablesTab = docker.addTab(null,
            utils.mixin(defaultTabArgs,{
                title: 'Variables',
                target:condCommandsTab,
                select:false,
                h:100,
                tabOrientation:types.DOCKER.TAB.BOTTOM,
                location:types.DOCKER.TAB.BOTTOM
            }));


        // 'Response' tab
        var responsesTab = docker.addTab(null,
            utils.mixin(defaultTabArgs,{
                title: 'Responses',
                target:variablesTab,
                select:false,
                h:100,
                icon:null,
                tabOrientation: types.DOCKER.TAB.BOTTOM,
                location: types.DOCKER.DOCK.STACKED
            }));

        var logsTab = docker.addTab(null,
            utils.mixin(defaultTabArgs, {
                title: 'Log',
                target: responsesTab,
                select: false,
                icon:'fa-calendar',
                tabOrientation: types.DOCKER.TAB.BOTTOM,
                location: types.DOCKER.DOCK.STACKED
            }));

        var right = docker.addPanel('Collapsible', types.DOCKER.DOCK.RIGHT, condCommandsTab, {
            w: '30%',
            title:' props '
        });


    }
    function testMain(tab){

        var subDocker = createSubDocker(tab);

        addPanes(subDocker);

    }

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var mainView = ctx.mainView;

            var docker = mainView.getDocker();



            docker.uuid='main';
            if(window._lastGrid){
                docker.removePanel(window._lastGrid);
            }


            parent = docker.addTab(null, {
                title: 'TestDocker',
                icon: 'fa-folder'
            });

            window._lastGrid = parent;


            setTimeout(function () {
                mainView.resize();
            }, 1000);


            function test() {
                testMain(parent);
            }

            setTimeout(function () {
                test();
            }, 2000);



        }
    }
    return declare('m',null,[]);
});