require({cache:{
'xdocker/main':function(){
define([
	"dojo/_base/kernel",
    "xdocker/Docker2",
    'xdocker/Panel2',
    'xdocker/Frame2'
], function(){
    return dojo.xdocker;
});

},
'xdocker/Docker2':function(){
/** @module xDocker/Docker2 */
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'wcDocker/docker',
    'xdocker/Panel2',
    'xdocker/Frame2',
    'xdocker/Splitter2',
    'xide/mixins/EventedMixin',
    'xide/registry',
    'wcDocker/types',
    'wcDocker/base',
    'xaction/ActionProvider',
    'xide/widgets/_Widget',
    'xide/widgets/ContextMenu',
    'require'
], function (dcl, types, utils, wcDocker, Panel2, Frame2, Splitter2, EventedMixin, registry, dTypes, base, ActionProvider, _CWidget, ContextMenu, require) {
    /**
     * @class module:xDocker/Docker2
     * @extends module:wcDocker/docker
     */
    var Module = dcl([wcDocker, base, ActionProvider.dcl, _CWidget.dcl, EventedMixin.dcl], {
        isResizing: false,
        _last: null,
        _lastResize: null,
        _needResize: false,
        _lastResizeTimer: null,
        _declaredClass: 'xdocker.Docker2',
        __eventHandles: [],
        contextMenu: null,
        contextMenuEventTarget: null,
        contextMenuPanelId: null,
        setupActions: function (_mixin) {
            this.__on(this._root.$container, 'contextmenu', '.wcPanelTab', function (e) {
                self.contextMenuEventTarget = e.target;
                self.contextMenuPanelId = e.target.parentNode ? e.target.parentNode.id : null;
            });

            var self = this,
                //default action mixin
                mixin = {
                    addPermission: true
                },
                defaultProps = {
                    group: 'Misc',
                    tab: 'View',
                    mixin: _mixin || mixin
                },
                _actions = [],
                action = function (label, command, icon) {
                    return self.createAction(_.extend({
                        label: label,
                        command: command,
                        icon: icon
                    }, defaultProps));
                },
                actions = this.addActions([
                    action('Close', 'View/Close', 'fa-close'),
                    action('Close others', 'View/Close Others', 'fa-close'),
                    action('Close all in group', 'View/Close all group', 'fa-close'),
                    action('Split Horizontal', 'View/Split Horizontal', 'fa-columns'),
                    action('Split Vertical', 'View/Split Vertical', 'fa-columns')
                ]),
                node = this._root.$container[0],
                contextMenu = new ContextMenu({
                    owner: this,
                    delegate: this,
                    limitTo: 'wcPanelTab',
                    openTarget: node
                }, node);

            contextMenu.init({
                preventDoubleContext: false
            });
            contextMenu.setActionEmitter(this, types.EVENTS.ON_VIEW_SHOW, this);
            this.contextMenu = contextMenu;
            this.add(contextMenu, null, false);

            return actions;
        },
        runAction: function (action, type, event) {

            var DOCKER_TYPES = types.DOCKER,
                _panel = this.contextMenuEventTarget ? this.__panel(this.contextMenuEventTarget) : null,
                self = this,
                frame = _panel ? _panel.getFrame() : null,
                panels = frame ? frame.panels() : null;

            if (action.command === 'View/Close') {
                if (_panel) {
                    this.removePanel(_panel);
                }
            }

            if (action.command === 'View/Close all group' && _panel) {
                if (frame) {
                    var toRemove = [];
                    _.each(panels, function (panel) {
                        if (panel && panel._moveable && panel._closeable) {
                            toRemove.push(panel);
                        }
                    });
                    _.each(toRemove, function (panel) {
                        self.removePanel(panel);
                    })
                }

            }
            if (action.command === 'View/Close Others' && _panel) {
                frame && _.each(panels, function (panel) {
                    if (panel && panel != _panel && panel._moveable && panel._closeable) {
                        self.removePanel(panel);
                    }
                });

                _panel.select();
            }

            function split(direction) {
                var otherPanel = _panel.next(-1) || _panel.next(1);
                if (otherPanel && _panel != otherPanel) {

                    self.movePanel(otherPanel, direction == DOCKER_TYPES.ORIENTATION.HORIZONTAL ?
                        DOCKER_TYPES.DOCK.BOTTOM : DOCKER_TYPES.DOCK.RIGHT, _panel, direction);

                    var splitter = otherPanel.getSplitter();
                    splitter && splitter.pos(0.5);
                }
            }

            if (action.command === 'View/Split Horizontal' && _panel) {
                split(DOCKER_TYPES.ORIENTATION.HORIZONTAL);
            }
            if (action.command === 'View/Split Vertical' && _panel) {
                split(DOCKER_TYPES.ORIENTATION.VERTICAL);
            }
        },
        __panel: function (el) {

            var panels = this.getPanels();
            var frame = null,
                self = this;

            _.each(this._frameList, function (_frame) {
                if ($.contains(_frame.$container[0], el)) {
                    frame = _frame;
                }
            });

            var id = self.contextMenuPanelId;
            this.contextMenuEventTarget = null;
            this.contextMenuPanelId = null;
            if (frame && id !== null) {
                var _panel = frame.panel(id);
                if (_panel) {
                    return _panel;
                }
            }
        },
        /**********************************************************************/
        allPanels: function () {
            var result = [];
            _.each(this._frameList, function (frame) {
                _.each(frame._panelList, function (panel) {
                    result.push(panel);
                });
            });
            return result;
        },
        destroy: function () {
            registry.remove(this.id);
            this.__destroy();
        },
        /**
         * Collect wcDocker's body event handles
         * @param element {HTMLElement}
         * @param type {string} event type
         * @param selector {string|null|function}
         * @param handler {function|null}
         * @private
         */
        __on: function (element, type, selector, handler) {

            if (typeof selector == 'function' && !handler) {
                //no selector given, swap arg[3] with arg[2]
                handler = selector;
                selector = null;
            }

            element.on(type, selector, handler);

            this.__eventHandles.push({
                element: element,
                type: type,
                selector: selector,
                handler: handler
            });
        },
        __destroy: function () {
            var self = this;
            //docker handles
            _.each(self.__eventHandles, function (handle) {
                handle && handle.element && handle.element.off(handle.type, handle.selector, handle.handler);
                self.__eventHandles.remove(handle);
            });

            //xide handles
            _.each(self._events, function (handles, type) {
                _.each(handles, function (handler) {
                    self.off(type, handler);
                });
            });

            self.clear();
            delete self._events;
            delete self.__eventHandles;
            self._updateId && clearTimeout(self._updateId);
        },
        resize: function (force, noob, why) {
            if (why === 'deselected') {
                return;
            }
            var self = this;

            function _resize() {
                if (self.$container) {
                    var h = self.$container.height();
                    var w = self.$container.width();
                    if (h < 110 || w < 110) {
                        return;
                    }
                }
                self._dirty = true;
                self._root && self._root.__update(true);
                _.invoke(self._floatingList, '__update');
            }
            return this.debounce('resize', _resize.bind(this), 100, null);
        },
        /**
         * Helper
         * @returns {xdocker/Panel[]}
         */
        getPanels: function () {
            var result = [];
            _.each(this._frameList, function (frame) {
                _.each(frame._panelList, function (panel) {
                    result.push(panel);
                });
            });
            return result;
        },
        /**
         * Returns a default panel
         * @returns {xdocker/Panel}
         */
        getDefaultPanel: function () {
            return _.find(this.getPanels(), {
                isDefault: true
            });
        },
        /**
         * Std API
         * @todo convert to addChild
         * @param mixed {string|object}
         * @param options {object}
         * @param options.title {string|null}
         * @param options.tabOrientation {string|null}
         * @param options.location {string|null}
         * @param options.select {boolean}
         * @param options.target {object}
         * @returns {*}
         */
        addTab: function (mixed, options) {

            var panel = null,
                thiz = this;

            options = options || {};
            mixed = mixed || 'DefaultTab';

            /**
             * Complete options to default
             * @param options
             */
            function completeOptions(options) {

                if (!('tabOrientation' in options)) {
                    options.tabOrientation = types.DOCKER.TAB.TOP
                }

                if (!('location' in options)) {
                    options.location = types.DOCKER.DOCK.STACKED;
                }

                if (!('target' in options)) {
                    options.target = thiz.getDefaultPanel();
                }

                if (!('select' in options)) {
                    options.select = true;
                }

                if (!('title' in options)) {
                    options.title = ' ';
                }
            }
            completeOptions(options);

            if (_.isString(mixed)) {
                panel = this.addPanel(mixed, options.location, options.target, options);
            } else if (_.isObject(mixed)) {
                panel = mixed;
            }

            function applyOptions(who, what) {
                for (var option in what) {
                    if (_.isFunction(who[option])) {
                        who[option](what[option]);
                    }
                }
            }
            applyOptions(panel, options);
            if (options.select === true) {
                panel.select();
            }
            return panel;
        }
    });
    /**
     * Registration of a panel type
     * @todo typos & API
     * @static
     * @param label
     * @param iconClass
     * @param closable
     * @param collapsible
     * @param movable
     * @param onCreate
     * @returns {{faicon: *, closeable: *, title: *, onCreate: Function}}
     */
    Module.defaultPaneType = function (label, iconClass, closable, collapsible, moveable, onCreate, isPrivate) {

        return {
            isPrivate: isPrivate !== null ? isPrivate : false,
            faicon: iconClass,
            closeable: closable,
            title: label,
            moveable: moveable,
            onCreate: function (panel, options) {

                var docker = panel.docker();

                utils.mixin(panel._options, options);

                panel.on(wcDocker.EVENT.ATTACHED, function () {
                    docker._emit(wcDocker.EVENT.ATTACHED, panel);
                });

                panel.on(wcDocker.EVENT.DETACHED, function () {
                    docker._emit(wcDocker.EVENT.DETACHED, panel);
                });

                panel.on(wcDocker.EVENT.CLOSED, function () {
                    docker._emit(wcDocker.EVENT.CLOSED, panel);
                });

                if (closable !== null) {
                    panel.closeable(closable);
                }

                if (collapsible !== null && panel.collapsible) {
                    panel.collapsible(collapsible);
                }

                if (moveable !== null) {
                    panel.moveable(moveable);
                }

                if (options) {
                    panel.title(options.title);
                    if (options.closeable) {
                        panel.closeable(options.closeable);
                    }
                }

                var parent = $('<div style="height: 100%;width: 100%;overflow: hidden;" class="panelParent"/>');
                panel.layout().addItem(parent).stretch('100%', '100%');
                panel.containerNode = parent[0];
                utils.mixin(panel, options.mixin);

                function resize(panel, parent, why) {
                    var extraH = 0;

                    var resizeToChildren = panel.resizeToChildren;
                    panel.onResize && panel.onResize();
                    if (resizeToChildren) {
                        var totalHeight = utils.getHeight(panel._findWidgets()),
                            panelParent = panel._parent.$container;

                        extraH = panelParent.outerHeight() - panelParent.height();
                        if (extraH) {
                            totalHeight += extraH;
                        }
                        parent.css('width', panel._actualSize.x + 'px');
                        panel.set('height', totalHeight);

                    } else {
                        parent.css('height', panel._actualSize.y + 'px');
                        parent.css('width', panel._actualSize.x + 'px');
                        //transfer size and resize on widgets
                        _.each(panel.containerNode.children, function (child) {
                            if (child.id) {

                                var _widget = registry.byId(child.id);
                                var doResize = true;
                                if (_widget && _widget.resizeToParent === false) {
                                    doResize = false;
                                }

                                if (doResize) {
                                    //update
                                    $(child).css('height', panel._actualSize.y - extraH + 'px');
                                    $(child).css('width', panel._actualSize.x + 'px');
                                }
                                if (_widget) {
                                    _widget.resize && _widget.resize(null, null, why);
                                } else {
                                    console.warn('cant get widget : ' + child.id);
                                }
                            }
                        });
                    }
                }


                function select(selected, reason) {
                    //console.log(reason + ' : select ' +panel.title() +' selected:'+ selected + ' visible:' + panel.isVisible() + ' p#selected:' + panel.selected + ' __isVisible'+panel.isVisible());

                    if (panel.selected && selected) {
                        return true;
                    }
                    panel.selected = selected;
                    selected && (docker._lastSelected = panel);
                    resize(panel, parent, selected ? 'selected' : 'deselected');

                    var apiMethod = selected ? 'onShow' : 'onHide';
                    panel[apiMethod] && panel[apiMethod]();




                    //tell widgets
                    _.each(panel._findWidgets(), function (widget) {
                        if (!widget) {
                            return;
                        }
                        if (selected) {
                            if (!widget._started) {
                                widget.startup && widget.startup();
                            }
                        }
                        //call std API
                        if (widget[apiMethod]) {
                            widget[apiMethod]();
                        }
                        //forward
                        if (widget._emit) {
                            widget._emit(selected ? types.EVENTS.ON_VIEW_SHOW : types.EVENTS.ON_VIEW_HIDE, widget);
                        }

                        //forward
                        if (widget.setFocused) {
                            widget.setFocused(selected);
                        }
                    });
                }

                panel.on(types.DOCKER.EVENT.VISIBILITY_CHANGED, function (visible) {
                    panel.silent !== true && select(visible, 'vis changed');
                });
                panel._on(types.DOCKER.EVENT.SELECT, function (what) {
                    panel.silent !== true && select(true, 'select');
                    docker._emit(types.DOCKER.EVENT.SELECT, panel);
                });

                panel._on(types.DOCKER.EVENT.MOVE_STARTED, function () {
                    docker.trigger(types.DOCKER.EVENT.MOVE_STARTED, panel);
                });

                panel._on(types.DOCKER.EVENT.MOVE_ENDED, function () {
                    docker.trigger(types.DOCKER.EVENT.MOVE_ENDED, panel);
                });

                panel.on(types.DOCKER.EVENT.SAVE_LAYOUT, function (customData) {


                    panel.onSaveLayout({
                        data: customData,
                        panel: panel
                    });

                    if (!customData.widgets) {
                        customData.widgets = [];
                    }

                    _.each(panel._findWidgets(), function (w) {
                        if (!w) {
                            console.warn('SAVE_LAYOUT : invalid widget');
                            return;
                        }
                        w._emit && w._emit(types.EVENTS.SAVE_LAYOUT, {
                            panel: panel,
                            data: customData
                        });
                        w.onSaveLayout && w.onSaveLayout({
                            data: customData,
                            owner: panel
                        })
                    });
                });
                panel.on(types.DOCKER.EVENT.RESTORE_LAYOUT, function (customData) {

                    var eventData = {
                        data: customData,
                        panel: panel
                    };
                    panel.onRestoreLayout(eventData);
                    //console.log('restore layout');
                    //docker.trigger('restorePanel',eventData);
                });

                panel.on(types.DOCKER.EVENT.RESIZE_STARTED, function () {
                    panel.onResizeBegin(arguments);
                    docker.trigger(types.DOCKER.EVENT.BEGIN_RESIZE, panel);
                });
                panel.on(types.DOCKER.EVENT.RESIZE_ENDED, function () {
                    panel.onResizeEnd(arguments);
                    resize(panel, parent);
                    docker.trigger(types.DOCKER.EVENT.END_RESIZE, panel);
                });
                panel.on(types.DOCKER.EVENT.RESIZED, function () {
                    resize(panel, parent);
                });
                utils.mixin(panel, options.mixin);

                if (onCreate) {
                    onCreate(panel);
                }
            }
        }
    };
    /**
     * Register default panel types on docker
     * @static
     * @param docker
     */
    Module.registerDefaultTypes = function (docker) {
        docker.registerPanelType('DefaultFixed', Module.defaultPaneType('', '', false, false, true, null, true));
        docker.registerPanelType('DefaultTab', Module.defaultPaneType('', '', true, false, true, null, true));
        docker.registerPanelType('Collapsible', Module.defaultPaneType('', '', false, true, true, null, true));
    };

    /**
     * Creates a default docker
     * @static
     * @param container
     * @param options
     */
    Module.createDefault = function (container, options) {
        require({
            cacheBust: null
        });
        options = options || {};

        //get or create new docker variant
        var _class = Module;
        options.extension && (_class = dcl([Module, options.extension], {}));
        var docker = new _class($(container), utils.mixin({
            allowCollapse: true,
            responseRate: 100,
            allowContextMenu: false,
            themePath: require.toUrl('xdocker') + '/Themes/',
            theme: 'transparent2',
            wcPanelClass: Panel2,
            wcFrameClass: Frame2,
            wcSplitterClass: Splitter2
        }, options || {}));

        Module.registerDefaultTypes(docker);

        function hideFrames(hide) {
            var frame = $('#xIFrames');
            frame.css('display', hide ? 'none' : 'block');
            frame.children().css('display', hide ? 'none' : 'block');
        }

        docker.on(wcDocker.EVENT.BEGIN_DOCK, _.partial(hideFrames, true));
        docker.on(wcDocker.EVENT.END_DOCK, _.partial(hideFrames, false));
        docker.on(wcDocker.EVENT.RESIZE_STARTED, _.partial(hideFrames, true));
        docker.on(wcDocker.EVENT.RESIZED, _.partial(hideFrames, false));
        if (!docker.id) {
            docker.id = registry.getUniqueId(docker._declaredClass.replace(/\./g, "_"));
            docker.$container.attr('id', docker.id);
        }
        registry.add(docker);
        return docker;
    };

    Module.DOCK = wcDocker.DOCK;
    Module.EVENT = wcDocker.EVENT;
    Module.LAYOUT = wcDocker.LAYOUT;
    Module.ORIENTATION = wcDocker.ORIENTATION;

    return Module;
});
},
'xdocker/Panel2':function(){
/** @module xdocker/Panel2 */
define([
    "dcl/dcl",
    "dcl/inherited",
    'xide/types',
    'xide/utils',
    'wcDocker/panel',
    'xide/mixins/EventedMixin',
    'xide/registry',
    'wcDocker/types',
    'xide/widgets/_Widget'
], function (dcl,inherited,types,utils,panel,EventedMixin,registry,wcDocker,_Widget) {

    /**
     * @class module:xdocker/Panel2
     * @extends module:wcDocker/panel
     */
    return dcl([panel,_Widget.dcl,EventedMixin.dcl], {
        selected:false,
        _isVisible:function(){
            var node = $(this.containerNode);
            if(!node.length){
                return false;
            }
            if(node[0].offsetHeight === 0){
                return false;
            }else if(node.css("display") == "none"){
                return false;
            }else if((node.css("visibility") == "hidden")){
                return false;
            }
            return true;
        },
        next:function(direction){
            var frame = this.getFrame();
            if(!frame){
                return;
            }
            var pos = parseInt(frame._curTab,10);
            return frame.panelAt(pos + direction);
        },
        set:function(key,value){
            if(key==='changed'){
                if(this.$title){
                    value ? this.$title.addClass('changed') : this.$title.removeClass('changed');
                }
                return true;
            }
            if(key==='loading'){
                value ? this.startLoading() : this.finishLoading();
            }
            if(key==='height'){
                var parent = $(this.containerNode);
                parent.css('height', value+ 'px');
                this._maxSize.y = value;
                this._minSize.y = value;
                this._actualSize.y = value;
                return true;
            }
            return this.inherited(arguments);
        },
        isExpanded:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return true;
            }
            return splitter.isExpanded();
        },
        isCollapsed:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            return splitter.isCollapsed();
        },
        expand:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            splitter.expand();
            return _.invoke(this._findWidgets,'onShow');
        },
        collapse:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            splitter.collapse(splitter._pane[0].panelIndex(this) == -1 ? 1 : 0);
            return _.invoke(this._findWidgets,'onHide');
        },
        // stub
        onSaveLayout:function(e){
            this._emit('onSaveLayout',e);
        },
        onRestoreLayout:function(e){
            this._emit('onRestoreLayout',e);
        },
        // Initialize
        __init: function () {
            this.on(wcDocker.EVENT.SAVE_LAYOUT,function(data){
                data.title = this.title();
            });
            this.on(wcDocker.EVENT.RESTORE_LAYOUT,function(data){
                data.title && this.title(data.title);
            });
            var layoutClass = (this._options && this._options.layout) || 'wcLayoutTable';
            this._layout = new (this.docker().__getClass(layoutClass))(this.$container, this);
            this.$title = $('<li class="wcPanelTab">');
            this.$titleText = $('<a>' + this._title + '</a>');
            this.$title.append(this.$titleText);

            if (this._options.hasOwnProperty('title')) {
                this.title(this._options.title);
            }

            if (this._options.icon) {
                this.icon(this._options.icon);
                this._icon = this._options.icon;
            }
            if (this._options.faicon) {
                this.faicon(this._options.faicon);
            }

            if(!this.id){
                this.id = registry.getUniqueId(this.declaredClass.replace(/\./g, "_"));
            }

            utils.mixin(this,this._options.mixin);
            registry.add(this);
        },
        /**
         * Returns all dojo/dijit widgets in this node
         * @param startNode
         * @returns {_WidgetBase[]}
         * @private
         */
        _findWidgets:function(startNode){
            var result = [],
                node = startNode || this.containerNode || ( this.$container ? this.$container[0] : null);

            if(node) {
                _.each(node.children, function (child) {
                    if (child.id) {
                        var _widget = registry.byId(child.id);
                        if (_widget) {
                            result.push(_widget);
                        }
                    }
                });
            }
            this._widgets && (result = result.concat(this._widgets));
            return result;
        },

        __onShow:function(){
            _.each(this._findWidgets(), function (widget) {});
        },

        /**
         * Std API
         */
        destroy: function (removeFromDocker) {
            this.inherited(arguments);
            if(removeFromDocker!==false) {
                var _docker = this.docker();
                _docker && _docker.removePanel(this);
            }
            registry.remove(this.id);
        },
        __destroy: function () {
            this._emit('destroy');
            _.each(this._findWidgets(),function(w){
                if(w && w.destroy && !w._destroyed){
                    w.destroy();
                }
            });
            this.inherited(arguments);
            this._destroyHandles();
            this.off();
        }
    });
});
},
'xdocker/Frame2':function(){
define([
    "dcl/dcl",
    "wcDocker/frame"
], function (dcl,frame) {

    var Module = dcl([frame],{
        _showTitlebar:true,
        panel: function (tabIndex, autoFocus) {
            if (typeof tabIndex !== 'undefined') {
                if (this.isCollapser() && tabIndex === this._curTab) {
                    this.collapse();
                    tabIndex = -1;
                }
                if (tabIndex < this._panelList.length) {
                    this.$tabBar.find('> .wcTabScroller > .wcPanelTab[id="' + this._curTab + '"]').removeClass('wcPanelTabActive active');
                    this.$center.children('.wcPanelTabContent[id="' + this._curTab + '"]').addClass('wcPanelTabContentHidden');
                    if (this._curTab !== tabIndex) {
                        this.collapse();
                    }
                    this._curTab = tabIndex;
                    if (tabIndex > -1) {
                        this.$tabBar.find('> .wcTabScroller > .wcPanelTab[id="' + tabIndex + '"]').addClass('wcPanelTabActive active');
                        this.$center.children('.wcPanelTabContent[id="' + tabIndex + '"]').removeClass('wcPanelTabContentHidden');
                        this.expand();
                    }
                    this.__updateTabs(autoFocus);
                }
            }

            if (this._curTab > -1 && this._curTab < this._panelList.length) {
                return this._panelList[this._curTab];
            } else if (this.isCollapser() && this._panelList.length) {
                return this._panelList[0];
            }
            return false;
        },
        __init: function () {
            this.$frame = $('<div class="wcFrame wcWide wcTall widget">');
            this.$title = $('<div class="wcFrameTitle">');
            this.$titleBar = $('<div class="wcFrameTitleBar wcFrameTopper">');
            this.$tabBar = $('<header class="wcFrameTitleBar">');
            this.$tabScroll = $('<ul class="wcTabScroller nav nav-tabs">');
            this.$center = $('<div class="wcFrameCenter wcPanelBackground">');
            this.$tabLeft = $('<div class="wcFrameButton" title="Scroll tabs to the left."><span class="fa fa-arrow-left"></span>&lt;</div>');
            this.$tabRight = $('<div class="wcFrameButton" title="Scroll tabs to the right."><span class="fa fa-arrow-right"></span>&gt;</div>');
            this.$close = $('<div class="wcFrameButton" title="Close the currently active panel tab"><div class="fa fa-close"></div>X</div>');
            this.$collapse = $('<div class="wcFrameButton" title="Collapse the active panel"><div class="fa fa-download"></div>C</div>');
            this.$buttonBar = $('<div class="wcFrameButtonBar">');
            this.$tabButtonBar = $('<div class="wcFrameButtonBar">');

            this.$tabBar.append(this.$tabScroll);
            this.$tabBar.append(this.$tabButtonBar);
            this.$frame.append(this.$buttonBar);
            this.$buttonBar.append(this.$close);
            this.$buttonBar.append(this.$collapse);
            this.$frame.append(this.$center);

            if (this._isFloating) {
                this.$top = $('<div class="wcFrameEdgeH wcFrameEdge"></div>').css('top', '-6px').css('left', '0px').css('right', '0px');
                this.$bottom = $('<div class="wcFrameEdgeH wcFrameEdge"></div>').css('bottom', '-6px').css('left', '0px').css('right', '0px');
                this.$left = $('<div class="wcFrameEdgeV wcFrameEdge"></div>').css('left', '-6px').css('top', '0px').css('bottom', '0px');
                this.$right = $('<div class="wcFrameEdgeV wcFrameEdge"></div>').css('right', '-6px').css('top', '0px').css('bottom', '0px');
                this.$corner1 = $('<div class="wcFrameCornerNW wcFrameEdge"></div>').css('top', '-6px').css('left', '-6px');
                this.$corner2 = $('<div class="wcFrameCornerNE wcFrameEdge"></div>').css('top', '-6px').css('right', '-6px');
                this.$corner3 = $('<div class="wcFrameCornerNW wcFrameEdge"></div>').css('bottom', '-6px').css('right', '-6px');
                this.$corner4 = $('<div class="wcFrameCornerNE wcFrameEdge"></div>').css('bottom', '-6px').css('left', '-6px');

                this.$frame.append(this.$top);
                this.$frame.append(this.$bottom);
                this.$frame.append(this.$left);
                this.$frame.append(this.$right);
                this.$frame.append(this.$corner1);
                this.$frame.append(this.$corner2);
                this.$frame.append(this.$corner3);
                this.$frame.append(this.$corner4);
            }
            this.__container(this.$container);
            if (this._isFloating) {
                this.$frame.addClass('wcFloating');
            }
            this.$center.scroll(this.__scrolled.bind(this));
        },
        /**
         * Get/Set for 'show title bar'
         * @param show {boolean}
         * @returns {boolean}
         */
        showTitlebar:function(show){
            if(show!=null) {
                var prop = show ? 'inherit' : 'none',
                    what = 'display',
                    who = this;

                who.$titleBar.css(what, prop);
                who.$tabBar.css(what, prop);
                who.$center.css('top', show ? 30 : 0);
                this._showTitlebar = show;
            }
            return this._showTitlebar
        },
        currentPanel:function(){
            return this._panelList[this._curTab];
        },
        __widgets:function(){
            return this._panelList;
        },
        panels:function(){
            return this._panelList;
        },
        panelAt:function(index){
            return this._panelList[index];
        },
        panelIndex:function(panel) {
            return _.findIndex(this._panelList,panel);
        }
    });    
    return Module;
});
},
'xdocker/Splitter2':function(){
define([
    "dcl/dcl",
    'wcDocker/splitter',
    'xide/mixins/EventedMixin'
], function (dcl,wcSplitter,EventedMixin) {



    /**
     * Function to set a wcFrame's tab bar elements
     * @param frame
     * @param hide
     */
    function hideFrameTabBar(frame,hide){
        _.invoke(frame.$tabBar, frame.$buttonBar,'css',['display',hide ===true ? 'none' : 'inherit']);
    }
    /**
     * Function to preserve a wcFrame's most important properties before collapse:
     *  - minSize
     *  - pos
     *  - all child panel's minSize
     *  - all child panel's titles
     *
     * @param frame
     * @param hide
     */
    function setFramePropsMin(frame,hide){

        frame._saveProp('minSize',[0,0]);
        frame._saveProp('pos');
        frame._saveProp('showTitlebar',[]);//call with undefined

        hide!==false && hideFrameTabBar(frame,hide);

        frame._panelList && _.each(frame._panelList,function(panel){
            //save title & min size
            panel._saveProp('minSize',[0,0]);
            panel._saveProp('title');
        });
        hide && frame.showTitlebar && frame.showTitlebar(false);
    }
    /**
     * Function to restore wcFrame properties: minSize, title, showTitlebar
     * @param frame
     */
    function restoreFrameProps(frame){
        hideFrameTabBar(frame,false);
        _.each(frame._panelList,function(panel){
            var ms = panel._restoreProp('minSize',false);
            ms && panel.minSize(ms.x, ms.y);
            panel._restoreProp('title');
        });
        frame._restoreProp('showTitlebar');
    }

    /**
     * Extend splitter for evented and collapse functions
     */
    return dcl([wcSplitter,EventedMixin.dcl], {
        _lastCollapsed:null,
        _isCollapsed:false,
        // Updates the size of the splitter.
        isExpanded:function(){
            return this._lastCollapsed ==null;
        },
        isCollapsed:function(){
            return this._lastCollapsed !=null;
        },
        collapse:function(side,pos){
            this._isCollapsed = true;
            this._saveProp('pos');
            setFramePropsMin(this._pane[0],side==0);
            setFramePropsMin(this._pane[1],side==1);
            this._lastCollapsed = side;
            this.pos( pos!=null ? pos : 1);
        },
        expand:function(){
            this._isCollapsed = false;
            restoreFrameProps(this._pane[0]);
            this._restoreProp('pos');
            restoreFrameProps(this._pane[1]);
            this._lastCollapsed = null;
        }
    });
});
},
'xdocker/types':function(){
/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types'
], function (declare, types) {

    /*!
     * Web Cabin Docker - Docking Layout Interface.
     *
     * Dependencies:
     *  JQuery 1.11.1
     *  JQuery-contextMenu 1.6.6
     *  font-awesome 4.2.0
     *
     * Author: Jeff Houde (lochemage@webcabin.org)
     * Web: https://docker.webcabin.org/
     *
     * Licensed under
     *   MIT License http://www.opensource.org/licenses/mit-license
     *   GPL v3 http://opensource.org/licenses/GPL-3.0
     *
     */


    types.DOCKER = {
        DOCK:null
    };

    types.DOCKER.DOCK = {
        /** A floating panel that blocks input until closed */
        MODAL: 'modal',
        /** A floating panel */
        FLOAT: 'float',
        /** Docks to the top of a target or window */
        TOP: 'top',
        /** Docks to the left of a target or window */
        LEFT: 'left',
        /** Docks to the right of a target or window */
        RIGHT: 'right',
        /** Docks to the bottom of a target or window */
        BOTTOM: 'bottom',
        /** Docks as another tabbed item along with the target */
        STACKED: 'stacked'
    };

    /**
     * Enumerated Internal events
     * @version 3.0.0
     * @enum {String}
     */

    types.DOCKER.EVENT = {

        SELECT: 'ON_VIEW_SELECT',

        /** When the panel is initialized */

        INIT: 'panelInit',
        /** When all panels have finished loading */
        LOADED: 'dockerLoaded',
        /** When the panel is updated */
        UPDATED: 'panelUpdated',
        /** When the panel has changed its visibility */
        VISIBILITY_CHANGED: 'panelVisibilityChanged',
        /** When the user begins moving any panel from its current docked position */
        BEGIN_DOCK: 'panelBeginDock',
        /** When the user finishes moving or docking a panel */
        END_DOCK: 'panelEndDock',
        /** When the user brings this panel into focus */
        GAIN_FOCUS: 'panelGainFocus',
        /** When the user leaves focus on this panel */
        LOST_FOCUS: 'panelLostFocus',
        /** When the panel is being closed */
        CLOSED: 'panelClosed',
        /** When a custom button is clicked, See [wcPanel.addButton]{@link wcPanel#addButton} */
        BUTTON: 'panelButton',
        /** When the panel has moved from floating to a docked position */
        ATTACHED: 'panelAttached',
        /** When the panel has moved from a docked position to floating */
        DETACHED: 'panelDetached',
        /** When the user has started moving the panel (top-left coordinates changed) */
        MOVE_STARTED: 'panelMoveStarted',
        /** When the user has finished moving the panel */
        MOVE_ENDED: 'panelMoveEnded',
        /** When the top-left coordinates of the panel has changed */
        MOVED: 'panelMoved',
        /** When the user has started resizing the panel (width or height changed) */
        RESIZE_STARTED: 'panelResizeStarted',
        /** When the user has finished resizing the panel */
        RESIZE_ENDED: 'panelResizeEnded',
        /** When the panels width or height has changed */
        RESIZED: 'panelResized',
        /** When the contents of the panel has been scrolled */
        SCROLLED: 'panelScrolled',
        /** When the layout is being saved, See [wcDocker.save]{@link wcDocker#save} */
        SAVE_LAYOUT: 'layoutSave',
        /** When the layout is being restored, See [wcDocker.restore]{@link wcDocker#restore} */
        RESTORE_LAYOUT: 'layoutRestore',
        /** When the current tab on a custom tab widget associated with this panel has changed, See {@link wcTabFrame} */
        CUSTOM_TAB_CHANGED: 'customTabChanged',
        /** When a tab has been closed on a custom tab widget associated with this panel, See {@link wcTabFrame} */
        CUSTOM_TAB_CLOSED: 'customTabClosed',
        /** When a splitter position has been changed */
        SPLITTER_POS_CHANGED: 'splitterPosChanged',
        
        BEGIN_FLOAT_RESIZE: 'beginFloatResize',
        END_FLOAT_RESIZE: 'endFloatResize',
        BEGIN_RESIZE:"beginResize",
        END_RESIZE:"endResize"

    };

    /**
     * The name of the placeholder panel.
     * @constant {String}
     */
    types.DOCKER.PANEL_PLACEHOLDER = '__wcDockerPlaceholderPanel';


    /**
     * Used when [adding]{@link wcDocker#addPanel} or [moving]{@link wcDocker#movePanel} a panel to designate the target location as collapsed.<br>
     * Must be used with [docking]{@link wcDocker.DOCK} positions LEFT, RIGHT, or BOTTOM only.
     * @constant {String}
     */

    types.DOCKER.COLLAPSED = '__wcDockerCollapsedPanel';

    /**
     * Used for the splitter bar orientation.
     * @version 3.0.0
     * @enum {Boolean}
     */
    types.DOCKER.ORIENTATION = {
        /** Top and Bottom panes */
        VERTICAL: false,
        /** Left and Right panes */
        HORIZONTAL: true
    };


    types.DOCKER.TAB = {
        /** The default, puts tabs at the top of the frame */
        TOP: 'top',
        /** Puts tabs on the left side of the frame */
        LEFT: 'left',
        /** Puts tabs on the right side of the frame */
        RIGHT: 'right',
        /** Puts tabs on the bottom of the frame */
        BOTTOM: 'bottom'
    };

    types.DOCKER.TAB.ORIENTATION = {
        /** Top and Bottom panes */
        VERTICAL: false,
        /** Left and Right panes */
        HORIZONTAL: true
    };


    types.PANEL_TYPES = {
        DefaultFixed : 'DefaultFixed',
        DefaultTab : 'DefaultTab',
        Collapsible : 'Collapsible'
    }

    return declare('xide.docker.types',null,{});


});
}}});
// wrapped by build app
define("xdocker/xdocker", ["dojo","dijit","dojox"], function(dojo,dijit,dojox){
//stub module
});
