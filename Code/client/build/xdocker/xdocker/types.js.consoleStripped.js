/** @module xgrid/Base **/
define("xdocker/types", [
    "xdojo/declare",
    'xide/types'
], function (declare, types) {

    /*!
     * Web Cabin Docker - Docking Layout Interface.
     *
     * Dependencies:
     *  JQuery 1.11.1
     *  JQuery-contextMenu 1.6.6
     *  font-awesome 4.2.0
     *
     * Author: Jeff Houde (lochemage@webcabin.org)
     * Web: https://docker.webcabin.org/
     *
     * Licensed under
     *   MIT License http://www.opensource.org/licenses/mit-license
     *   GPL v3 http://opensource.org/licenses/GPL-3.0
     *
     */


    types.DOCKER = {
        DOCK:null
    };

    types.DOCKER.DOCK = {
        /** A floating panel that blocks input until closed */
        MODAL: 'modal',
        /** A floating panel */
        FLOAT: 'float',
        /** Docks to the top of a target or window */
        TOP: 'top',
        /** Docks to the left of a target or window */
        LEFT: 'left',
        /** Docks to the right of a target or window */
        RIGHT: 'right',
        /** Docks to the bottom of a target or window */
        BOTTOM: 'bottom',
        /** Docks as another tabbed item along with the target */
        STACKED: 'stacked'
    };

    /**
     * Enumerated Internal events
     * @version 3.0.0
     * @enum {String}
     */

    types.DOCKER.EVENT = {

        SELECT: 'ON_VIEW_SELECT',

        /** When the panel is initialized */

        INIT: 'panelInit',
        /** When all panels have finished loading */
        LOADED: 'dockerLoaded',
        /** When the panel is updated */
        UPDATED: 'panelUpdated',
        /** When the panel has changed its visibility */
        VISIBILITY_CHANGED: 'panelVisibilityChanged',
        /** When the user begins moving any panel from its current docked position */
        BEGIN_DOCK: 'panelBeginDock',
        /** When the user finishes moving or docking a panel */
        END_DOCK: 'panelEndDock',
        /** When the user brings this panel into focus */
        GAIN_FOCUS: 'panelGainFocus',
        /** When the user leaves focus on this panel */
        LOST_FOCUS: 'panelLostFocus',
        /** When the panel is being closed */
        CLOSED: 'panelClosed',
        /** When a custom button is clicked, See [wcPanel.addButton]{@link wcPanel#addButton} */
        BUTTON: 'panelButton',
        /** When the panel has moved from floating to a docked position */
        ATTACHED: 'panelAttached',
        /** When the panel has moved from a docked position to floating */
        DETACHED: 'panelDetached',
        /** When the user has started moving the panel (top-left coordinates changed) */
        MOVE_STARTED: 'panelMoveStarted',
        /** When the user has finished moving the panel */
        MOVE_ENDED: 'panelMoveEnded',
        /** When the top-left coordinates of the panel has changed */
        MOVED: 'panelMoved',
        /** When the user has started resizing the panel (width or height changed) */
        RESIZE_STARTED: 'panelResizeStarted',
        /** When the user has finished resizing the panel */
        RESIZE_ENDED: 'panelResizeEnded',
        /** When the panels width or height has changed */
        RESIZED: 'panelResized',
        /** When the contents of the panel has been scrolled */
        SCROLLED: 'panelScrolled',
        /** When the layout is being saved, See [wcDocker.save]{@link wcDocker#save} */
        SAVE_LAYOUT: 'layoutSave',
        /** When the layout is being restored, See [wcDocker.restore]{@link wcDocker#restore} */
        RESTORE_LAYOUT: 'layoutRestore',
        /** When the current tab on a custom tab widget associated with this panel has changed, See {@link wcTabFrame} */
        CUSTOM_TAB_CHANGED: 'customTabChanged',
        /** When a tab has been closed on a custom tab widget associated with this panel, See {@link wcTabFrame} */
        CUSTOM_TAB_CLOSED: 'customTabClosed',
        /** When a splitter position has been changed */
        SPLITTER_POS_CHANGED: 'splitterPosChanged',
        
        BEGIN_FLOAT_RESIZE: 'beginFloatResize',
        END_FLOAT_RESIZE: 'endFloatResize',
        BEGIN_RESIZE:"beginResize",
        END_RESIZE:"endResize"

    };

    /**
     * The name of the placeholder panel.
     * @constant {String}
     */
    types.DOCKER.PANEL_PLACEHOLDER = '__wcDockerPlaceholderPanel';


    /**
     * Used when [adding]{@link wcDocker#addPanel} or [moving]{@link wcDocker#movePanel} a panel to designate the target location as collapsed.<br>
     * Must be used with [docking]{@link wcDocker.DOCK} positions LEFT, RIGHT, or BOTTOM only.
     * @constant {String}
     */

    types.DOCKER.COLLAPSED = '__wcDockerCollapsedPanel';

    /**
     * Used for the splitter bar orientation.
     * @version 3.0.0
     * @enum {Boolean}
     */
    types.DOCKER.ORIENTATION = {
        /** Top and Bottom panes */
        VERTICAL: false,
        /** Left and Right panes */
        HORIZONTAL: true
    };


    types.DOCKER.TAB = {
        /** The default, puts tabs at the top of the frame */
        TOP: 'top',
        /** Puts tabs on the left side of the frame */
        LEFT: 'left',
        /** Puts tabs on the right side of the frame */
        RIGHT: 'right',
        /** Puts tabs on the bottom of the frame */
        BOTTOM: 'bottom'
    };

    types.DOCKER.TAB.ORIENTATION = {
        /** Top and Bottom panes */
        VERTICAL: false,
        /** Left and Right panes */
        HORIZONTAL: true
    };


    types.PANEL_TYPES = {
        DefaultFixed : 'DefaultFixed',
        DefaultTab : 'DefaultTab',
        Collapsible : 'Collapsible'
    }

    return declare('xide.docker.types',null,{});


});