define("davinci/ve/actions/_LayerAction", [
    	"dojo/_base/declare",
    	"davinci/actions/Action",
    	"davinci/ve/metadata"
], function(declare, Action, Metadata){


return declare("davinci.ve.actions._LayerAction", [Action], {
	

	isEnabled: function(context){
		if(!context){
			return false;
		}
		const selection = context.getSelection();
		if(selection.length != 1){
			return false;
		}
		const widget = selection[0];
		return !!Metadata.queryDescriptor(widget.type, "isLayered");
	}
});
});