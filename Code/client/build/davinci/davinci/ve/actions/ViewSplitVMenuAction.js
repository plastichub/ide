//>>built
define("davinci/ve/actions/ViewSplitVMenuAction",["dojo/_base/declare","davinci/ve/actions/ContextAction"],function(b,c){return b("davinci.ve.actions.ViewSplitVMenuAction",[c],{run:function(a){(a=this.fixupContext(a))&&a.editor&&a.editor.switchDisplayModeSplitVertical&&a.editor.switchDisplayModeSplitVertical()}})});
//# sourceMappingURL=ViewSplitVMenuAction.js.map