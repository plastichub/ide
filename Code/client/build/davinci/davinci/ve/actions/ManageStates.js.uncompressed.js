define("davinci/ve/actions/ManageStates", [
	"dojo/_base/declare",
	"davinci/Runtime",
	"davinci/Workbench",
	"davinci/ve/actions/ContextAction",
	"davinci/ve/States",
	"davinci/lang/ve"
], function(
	declare,
	Runtime,
	Workbench,
	ContextAction,
	States,
	veNls){

return declare("davinci.ve.actions.ManageStates", [ContextAction], {

	run: function(){
		let context;
		if(Runtime.currentEditor && Runtime.currentEditor.currentEditor && Runtime.currentEditor.currentEditor.context){
			context = Runtime.currentEditor.currentEditor.context;
		}else{
			return;
		}
		const statesFocus = States.getFocus(context.rootNode);
		if(!statesFocus || !statesFocus.stateContainerNode){
			return;
		}
		const w = new davinci.ve.actions._ManageStatesWidget({node: statesFocus.stateContainerNode });
		w._calledBy = 'ManageStates';
		w.okButton.set("label", veNls.updateLabel);
		w.updateDialog();
		const dialog = Workbench.showModal(w, veNls.manageStates, {width: 400}, null, true);
	},

	isEnabled: function(context){
		context = this.fixupContext(context);
		const e = Workbench.getOpenEditor();
		if (e && context) {
			if(e.declaredClass == 'davinci.ve.PageEditor'){
				return (context.getSelection().length > 0);
			}else{
				return false;
			}
		}else{
			return false;
		}
	},

	shouldShow: function(context){
		context = this.fixupContext(context);
		const editor = context ? context.editor : null;
		return (editor && editor.declaredClass == 'davinci.ve.PageEditor');
	}
});
});