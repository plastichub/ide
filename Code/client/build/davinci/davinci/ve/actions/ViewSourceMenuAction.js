//>>built
define("davinci/ve/actions/ViewSourceMenuAction",["dojo/_base/declare","davinci/ve/actions/ContextAction"],function(b,c){return b("davinci.ve.actions.ViewSourceMenuAction",[c],{run:function(a){(a=this.fixupContext(a))&&a.editor&&a.editor.switchDisplayModeSource&&a.editor.switchDisplayModeSource()}})});
//# sourceMappingURL=ViewSourceMenuAction.js.map