define("davinci/ve/actions/ShowInOutline", [
	"dojo/_base/declare",
	"davinci/Runtime",
	"davinci/Workbench",
	"davinci/ve/actions/ContextAction"
], function(
	declare,
	Runtime,
	Workbench,
	ContextAction){
return declare("davinci.ve.actions.ManageStates", [ContextAction], {

	run: function(){
		let context;
		//console.log('run action ');

		const editor = Runtime.currentEditor;
		if(editor){
			const visual = editor.delegate;
			visual.showInOutline();
		}
	},

	isEnabled: function(context){
		context = this.fixupContext(context);
		const e = Workbench.getOpenEditor();
		if (e && context) {
			if(e.declaredClass == 'davinci.ve.PageEditor'){
				return (context.getSelection().length > 0);
			}else{
				return false;
			}
		}else{
			return false;
		}
	},

	shouldShow: function(context){
		context = this.fixupContext(context);
		const editor = context ? context.editor : null;
		return (editor && editor.declaredClass == 'davinci.ve.PageEditor');
	}
});
});