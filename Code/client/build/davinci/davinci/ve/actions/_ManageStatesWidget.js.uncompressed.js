require({cache:{
'url:davinci/ve/actions/templates/ManageStates.html':"<div>\n\t<div class=\"dijitDialogPaneContentArea\">\n\t\t<div class=\"addStateDialogOptions\">\n\t\t\t<div class='manageStatesStatesListDiv'>\n\t\t\t\t<!-- Filled in by JavaScript -->\n\t\t\t</div>\n\t\t\t<div class='manageStatesCheckAcceleratorsDiv'>\n\t\t\t\t${veNls.manageStatesCheckLabel}\n\t\t\t\t<a href='' class='manageStatesCheckCurrentStateOnly'>${veNls.manageStatesCheckCurrentStateOnly}</a>\n\t\t\t\t/\n\t\t\t\t<a href='' class='manageStatesCheckAll'>${veNls.manageStatesCheckAll}</a>\n\t\t\t\t/\n\t\t\t\t<a href='' class='manageStatesUncheckAll'>${veNls.manageStatesUncheckAll}</a>\n\t\t\t\t/\n\t\t\t\t<a href='' class='manageStatesCheckBackgroundOnly'>${veNls.manageStatesCheckBackgroundOnly}</a>\n\t\t\t</div>\n\t\t\t\n\t\t</div>\t\n\t</div>\n\t<div class=\"dijitDialogPaneActionBar\">\n\t\t<button dojoType='dijit.form.Button' dojoAttachPoint=\"okButton\" dojoAttachEvent='onClick:onOk' label='${veNls.createLabel}' class=\"maqPrimaryButton\" type=\"submit\"></button>\n\t\t<button dojoType='dijit.form.Button' dojoAttachEvent='onClick:onCancel' label='${commonNls.buttonCancel}' class=\"maqSecondaryButton\"></button>\n\t</div>\n</div>\n"}});
define("davinci/ve/actions/_ManageStatesWidget", [
	"dojo/_base/declare",
	"dojo/dom-construct",
	"dojo/on",
	"dojo/dom-class",
	"dojo/_base/event",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"davinci/Runtime",
	"davinci/ve/States",
	"davinci/commands/CompoundCommand",
	"davinci/ve/commands/StyleCommand",
	"davinci/lang/ve",
	"dijit/lang/_common",
	"dojo/text!./templates/ManageStates.html"
], function(
	declare,
	domConstruct,
	On,
	domClass,
	Event,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	Runtime,
	States,
	CompoundCommand,
	StyleCommand,
	veNls,
	commonNls,
	templateString){

const NONE_VISIBLE = 0;
const ALL_VISIBLE = 1;
const SOME_VISIBLE = 2;

return declare("davinci.ve.actions._ManageStatesWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
	templateString: templateString,
	widgetsInTemplate: true,
	anyCheckBoxChanges: false,
	_states:[],				// array of all states in doc
	_stateContainers:[],	// array of all corresponding stateContainer nodes
	_checkBoxes:[],	// Eyeball icon SPAN for each of the states
	_overrideDisplayValue:[],	// Initialized to null for each state. If user sets explicit value, becomes a string (e.g., 'none')
	_notes:[],	// TriStateCheckBoxes for each of the states
	_handlers:[],

	veNls: veNls,
	commonNls: commonNls,
	
	constructor: function(){
		this.handlers = [];
	},
	
	postCreate: function(){
		const context = this._getContext();
		if(!context){
			return;
		}
		const rootNode = context.rootNode;
		const manageStatesStatesListDiv = this.domNode.querySelector('.manageStatesStatesListDiv');
		if(manageStatesStatesListDiv){
            //Get list of all states (and corresponding stateContainers) in the doc
            const obj = this._getAllStatesInDoc();
            this._states = obj.states;
            this._stateContainers = obj.stateContainers;

            const manageStatesCheckAcceleratorsTable = this.domNode.querySelector('.manageStatesCheckAcceleratorsTable');
            if(manageStatesCheckAcceleratorsTable){
				manageStatesCheckAcceleratorsTable.style.width = '100%';
			}
            const manageStatesCheckCurrentStateOnlyCell = this.domNode.querySelector('.manageStatesCheckCurrentStateOnlyCell');
            if(manageStatesCheckCurrentStateOnlyCell){
				manageStatesCheckCurrentStateOnlyCell.style.textAlign = 'left';
			}
            const manageStatesCheckAllCell = this.domNode.querySelector('.manageStatesCheckAllCell');
            if(manageStatesCheckAllCell){
				manageStatesCheckAllCell.style.textAlign = 'center';
			}
            const manageStatesUncheckAllCell = this.domNode.querySelector('.manageStatesUncheckAllCell');
            if(manageStatesUncheckAllCell){
				manageStatesUncheckAllCell.style.textAlign = 'right';
			}

            //Create table with TriStateCheckBox in col 1 and state name in col 2
            let table;

            let tr;
            let td;
            let div;
            table = domConstruct.create('table', 
					{'class':'manageStatesStatesListTable',
					style:'width:100%',
					border:0, cellspacing:0, cellpadding:3}, 
					manageStatesStatesListDiv);
            for(let i=0; i<this._states.length; i++){
				tr = domConstruct.create('tr', {}, table);
				td = domConstruct.create('td', {'class':'manageStatesCheckboxCell'}, tr);
				this._checkBoxes[i] = domConstruct.create('div', {id:'manageStatesCheckBox_'+i, 'class':'manageStatesCheckbox'}, td);
				this._handlers.push(
					On(this._checkBoxes[i], 'click', function(i, event){
						const checkbox = this._checkBoxes[i];
						this.anyCheckBoxChanges = true;
						domClass.remove(checkbox, 'manageStatesCheckboxNoneVisible');
						domClass.remove(checkbox, 'manageStatesCheckboxAllVisible');
						domClass.remove(checkbox, 'manageStatesCheckboxAllVisibleBackgroundAll');
						domClass.remove(checkbox, 'manageStatesCheckboxAllVisibleBackgroundSome');
						domClass.remove(checkbox, 'manageStatesCheckboxSomeVisible');
						if(checkbox._checkValue == ALL_VISIBLE){
							checkbox._checkValue = NONE_VISIBLE;
							domClass.add(checkbox, 'manageStatesCheckboxNoneVisible');
							this._overrideDisplayValue[i] = 'none';
						}else{
							checkbox._checkValue = ALL_VISIBLE;
							domClass.add(checkbox, 'manageStatesCheckboxAllVisible');
							this._overrideDisplayValue[i] = '';
						}
						this.updateDialog();
					}.bind(this, i))
				);
				this._overrideDisplayValue[i] = null;
				const state = this._states[i];
				const stateDisplayName = state == 'Normal' ? 'Background' : state;
				domConstruct.create('td', {'class':'manageStatesStateNameCell', innerHTML:stateDisplayName}, tr);
				td = domConstruct.create('td', {'class':'manageStatesNotesCell'}, tr);
				this._notes[i] = domConstruct.create('span', {'class':'manageStatesNotesSpan'}, td);
			}
        }
		const manageStatesCheckCurrentStateOnly = this.domNode.querySelector('.manageStatesCheckCurrentStateOnly');
		if(manageStatesCheckCurrentStateOnly){
			this._handlers.push(
				On(manageStatesCheckCurrentStateOnly, 'click', function(event){
					Event.stop(event);
					this._acceleratorClicked('current');
				}.bind(this))
			);
		}
		const manageStatesCheckAll = this.domNode.querySelector('.manageStatesCheckAll');
		if(manageStatesCheckAll){
			this._handlers.push(
				On(manageStatesCheckAll, 'click', function(event){
					Event.stop(event);
					this._acceleratorClicked('all');
				}.bind(this))
			);
		}
		const manageStatesUncheckAll = this.domNode.querySelector('.manageStatesUncheckAll');
		if(manageStatesUncheckAll){
			this._handlers.push(
				On(manageStatesUncheckAll, 'click', function(event){
					Event.stop(event);
					this._acceleratorClicked('none');
				}.bind(this))
			);
		}
		const manageStatesCheckBackgroundOnly = this.domNode.querySelector('.manageStatesCheckBackgroundOnly');
		if(manageStatesCheckBackgroundOnly){
			this._handlers.push(
				On(manageStatesCheckBackgroundOnly, 'click', function(event){
					Event.stop(event);
					this._acceleratorClicked('background');
				}.bind(this))
			);
		}
		this.anyCheckBoxChanges = false;
		// By default, browsers put focus on first hyperlink ("Check: Current state") which looks ugly
		// so move focus to the "Do it" button
		// Doesn't work without a setTimeout
		setTimeout(function(){
			this.okButton.focus();
		}.bind(this), 500);
	},
	
	/**
	 * User clicked on one of the accelerators
	 * @param value {'current'|'all'|'none'}
	 */
	_acceleratorClicked: function(type){
		const context = this._getContext();
		if(!context){
			return;
		}
		const statesFocus = States.getFocus(context.rootNode);
		if(!statesFocus || !statesFocus.stateContainerNode){
			return;
		}
		const currentState = States.getState(statesFocus.stateContainerNode);
		for(let i=0; i<this._states.length; i++){
			let state = this._states[i];
			if(state == 'undefined' || state == States.NORMAL){
				state = undefined;
			}
			const checkbox = this._checkBoxes[i];
			domClass.remove(checkbox, 'manageStatesCheckboxNoneVisible');
			domClass.remove(checkbox, 'manageStatesCheckboxAllVisible');
			domClass.remove(checkbox, 'manageStatesCheckboxAllVisibleBackgroundAll');
			domClass.remove(checkbox, 'manageStatesCheckboxAllVisibleBackgroundSome');
			domClass.remove(checkbox, 'manageStatesCheckboxSomeVisible');
			if(type == 'current'){
				if(state == currentState && statesFocus.stateContainerNode == this._stateContainers[i]){
					checkbox._checkValue = ALL_VISIBLE;
					domClass.add(checkbox, 'manageStatesCheckboxAllVisible');
					this._overrideDisplayValue[i] = '';
				}else{
					checkbox._checkValue = NONE_VISIBLE;
					domClass.add(checkbox, 'manageStatesCheckboxNoneVisible');
					this._overrideDisplayValue[i] = 'none';
				}
			}else if(type == 'all'){
				checkbox._checkValue = ALL_VISIBLE;
				domClass.add(checkbox, 'manageStatesCheckboxAllVisible');
				this._overrideDisplayValue[i] = '';
			}else if(type == 'none'){
				checkbox._checkValue = NONE_VISIBLE;
				domClass.add(checkbox, 'manageStatesCheckboxNoneVisible');
				this._overrideDisplayValue[i] = 'none';
			}else if(type == 'background'){
				checkbox._checkValue = ALL_VISIBLE;
				domClass.add(checkbox, 'manageStatesCheckboxAllVisible');
				if(i == 0){
					this._overrideDisplayValue[i] = '';
				}else{
					this._overrideDisplayValue[i] = '$MAQ_DELETE_PROPERTY$';
				}
			}
		}
		this.anyCheckBoxChanges = true;
		this.updateDialog();
		
	},

	_isValid: function() {
		return true;
	},

	_onKeyPress: function(e) {
		if (e.keyCode!=dojo.keys.ENTER) {
			if (this._isValid()) {
				this.okButton.set("disabled", false);
			} else {
				this.okButton.set("disabled", true);
			}
		}
	},
	
	/**
	 * Returns list of all states in document.
	 * @returns {states:[{string}], stateContainers:[{element}]}, 
	 *		where states is a list of all state names
	 *		and stateContainers is the corresponding stateContainer node
	 */
	_getAllStatesInDoc: function(){
		let context;
		if(Runtime.currentEditor && Runtime.currentEditor.currentEditor && Runtime.currentEditor.currentEditor.context){
			context = Runtime.currentEditor.currentEditor.context;
		}else{
			console.error('_ManageStatesWidget.js (from '+this._calledBy+' - cannot determine context.');
			return;
		}
		const states = [];
		const stateContainersList = [];
		const stateContainers = States.getAllStateContainers(context.rootNode);
		if(stateContainers){
			for(let i=0; i<stateContainers.length; i++){
				const statesList = States.getStates(stateContainers[i]);
				for(let j=0; j<statesList.length; j++){
					states.push(statesList[j]);
					stateContainersList.push(stateContainers[i]);
				}
			}
		}
		return {states:states, stateContainers:stateContainersList};
	},
	
	_getContext: function(){
		let context;
		if(Runtime.currentEditor && Runtime.currentEditor.currentEditor && Runtime.currentEditor.currentEditor.context){
			context = Runtime.currentEditor.currentEditor.context;
		}else{
			console.error('_ManageStatesWidget.js (from '+this._calledBy+' - cannot determine context.')
		}
		return context;
	},

	/**
	 * Returns list of all widgets to be effected by this dialog
	 * @returns [{widgets}]
	 */
	_getAllEffectedWidgets: function(){
		const context = this._getContext();
		if(!context){
			return [];
		}
		const statesFocus = States.getFocus(context.rootNode);
		if(!statesFocus || !statesFocus.stateContainerNode){
			return;
		}
		return context.getSelection().slice(0);	// clone operation
	},

	updateDialog: function(){
		const context = this._getContext();
		if(!context){
			return;
		}
		const statesFocus = States.getFocus(context.rootNode);
		if(!statesFocus || !statesFocus.stateContainerNode){
			return;
		}
		const widgets = this._getAllEffectedWidgets();
		const overrideBackground = this._overrideDisplayValue[0];
		for(let i=0; i<this._states.length; i++){
			let state = this._states[i];
			if(state == States.NORMAL || state == 'undefined'){
				state = undefined;
			}
			let countVisible = 0;
			let countVisibleThisState = 0;
			for(let j=0; j<widgets.length; j++){
				const widget = widgets[j];
				const overrides = {'undefined': overrideBackground};
				const effectiveState = (state === undefined) ? 'undefined' : state;
				overrides[effectiveState] = this._overrideDisplayValue[i];
				const obj = States.getEffectiveDisplayValue(context, widget, state, overrides);
				if(obj.effectiveDisplayValue.indexOf('none') != 0){
					countVisible++;
					if(!state && obj.effectiveState == 'undefined'){
						countVisibleThisState++;
					}else if(state && obj.effectiveState == state){
						countVisibleThisState++;
					}
				}
			}
			const checkbox = this._checkBoxes[i];
			const notes = this._notes[i];
			domClass.remove(checkbox, 'manageStatesCheckboxNoneVisible');
			domClass.remove(checkbox, 'manageStatesCheckboxAllVisible');
			domClass.remove(checkbox, 'manageStatesCheckboxAllVisibleBackgroundAll');
			domClass.remove(checkbox, 'manageStatesCheckboxAllVisibleBackgroundSome');
			domClass.remove(checkbox, 'manageStatesCheckboxSomeVisible');
			notes.innerHTML = '';
			if(countVisible == 0){
				checkbox._checkValue = NONE_VISIBLE;
				domClass.add(checkbox, 'manageStatesCheckboxNoneVisible');
			}else if(countVisible == widgets.length){
				checkbox._checkValue = ALL_VISIBLE;
				if(countVisibleThisState == widgets.length){
					domClass.add(checkbox, 'manageStatesCheckboxAllVisible');
				}else if(countVisibleThisState > 0){
					domClass.add(checkbox, 'manageStatesCheckboxAllVisibleBackgroundSome');
					notes.innerHTML = veNls.manageStatesSomeVisibleFromBackground;
				}else{
					domClass.add(checkbox, 'manageStatesCheckboxAllVisibleBackgroundAll');
					notes.innerHTML = veNls.manageStatesAllVisibleFromBackground;
				}
			}else{
				checkbox._checkValue = SOME_VISIBLE;
				domClass.add(checkbox, 'manageStatesCheckboxSomeVisible');
				notes.innerHTML = veNls.manageStatesSomeVisibleSomeHidden;
			}
		}
	},

	onOk: function() {
		if(!this.anyCheckBoxChanges){
			return;
		}
		const context = this._getContext();
		if(!context){
			return;
		}
		let command;
		const widgets = this._getAllEffectedWidgets();
		for(let i=0; i<this._states.length; i++){
			let state = this._states[i];
			if(state == States.NORMAL || state == 'undefined'){
				state = undefined;
			}
			const value = this._checkBoxes[i]._checkValue;
			if(value === NONE_VISIBLE || value === ALL_VISIBLE){
				for(let j=0; j<widgets.length; j++){
					const widget = widgets[j];
					if(!command){
						command = new CompoundCommand();
					}
					const displayValue = this._overrideDisplayValue[i] ? this._overrideDisplayValue[i] :
						(value == ALL_VISIBLE ? '' : 'none');
					command.add(new StyleCommand(widget, [{display: displayValue}], state));
				}
			}
		}
		if(command){
			context.getCommandStack().execute(command);
		}
	},

	onCancel: function() {
		this.onClose();
	},

	destroy: function(){
		this.inherited(arguments);
		for(let i=0; i<this._handlers.length; i++){
			this._handlers[i].remove();
		}
		this._handlers = [];
	}
});

});