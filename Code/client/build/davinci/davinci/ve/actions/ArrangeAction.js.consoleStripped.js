define("davinci/ve/actions/ArrangeAction", [
		"dojo/_base/declare",
		"./_ReorderAction"
], function(declare, _ReorderAction){


return declare("davinci.ve.actions.ArrangeAction", [_ReorderAction], {
	
	run: function(context){
		// This is a dropdown button. Actions are only available on dropdown menu
	},

	/**
	 * Enable this command if this command would actually make a change to the document.
	 * Otherwise, disable.
	 */
	isEnabled: function(context){
		return true;
	},

	shouldShow: function(context){
		context = this.fixupContext(context);
		const editor = context ? context.editor : null;
		return (editor && editor.declaredClass == 'davinci.ve.PageEditor');
	}
});
});