require({cache:{
'url:davinci/ui/templates/ChooseDevice.html':"<div>\n  <select dojoType=\"dijit.form.Select\" dojoAttachPoint=\"select\" style=\"width: 100%\">\n  </select>\n</div>\n"}});
define("davinci/ve/actions/ChooseDeviceAction", [
    	"dojo/_base/declare",
    	"davinci/actions/Action",
      "dijit/_WidgetBase",
      "dijit/_TemplatedMixin",
      "dijit/_WidgetsInTemplateMixin",
	"../../Workbench",
    	"dojo/store/Memory",
    	"dojo/text!../../ui/templates/ChooseDevice.html",
    	"davinci/lang/ve",
    	"davinci/lang/actions",
	"dijit/form/Select"
], function(declare, Action, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Workbench, Memory, templateString, veNls, actionNLS){


declare("davinci.ve.actions.ChooseDeviceActionContent", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
  templateString: templateString,
	widgetsInTemplate: true,

	select: null,

	device: null,
	deviceList: null,

	postCreate: function() {
    const store = new Memory({data:this.deviceList, idProperty: "name"});
    this.select.labelAttr = "name";
    this.select.setStore(store);
		this.select.set("value", this.device);
	},

	getValue: function() {
		return this.select.get("value")
	}
});	

return declare("davinci.ve.actions.ChooseDeviceAction", [Action], {

	
	run: function(selection){
		if (!this.isEnabled(null)){ return; }
		const e = davinci.Workbench.getOpenEditor();
		if (e && e.isDirty){
			//Give editor a chance to give us a more specific message
			let message = e.getOnUnloadWarningMessage();
			if (!message) {
				//No editor-specific message, so use our canned one
				message = dojo.string.substitute(veNls.filesHasUnsavedChanges, [e.fileName]);
			}
			Workbench.showDialog({
				title: veNls.chooseDeviceSilhouette, 
				content: message, 
				style: {width: 300}, 
				okCallback: dojo.hitch(this,this._okToSwitch), 
				okLabel: 'Save',
				hideLabel: null
			});
		} else {
			this._okToSwitch();
		}                                                     
	},
	
	_okToSwitch: function(){
		const e = davinci.Workbench.getOpenEditor();
		if (e.isDirty) {
			e.save();
		}
		this.showDevices(); 
	},

	isEnabled: function(selection){
		const e = davinci.Workbench.getOpenEditor();
		return e.declaredClass == 'davinci.ve.PageEditor'; // this is a hack to only support undo for theme editor for 0.5
	},

	showDevices: function(){

		const e = davinci.Workbench.getOpenEditor();
		const c = e.getContext();
		const device = c.visualEditor.deviceName;
		const deviceList = [{name: "none"}, {name: "iphone"}, {name: "ipad"}, {name: "android_340x480"}, {name: "android_480x800"}, {name: "androidtablet"}, {name: "blackberry"}];

		const ui = new davinci.ve.actions.ChooseDeviceActionContent({device: device, deviceList: deviceList});

		function _callback() {
			const e = davinci.Workbench.getOpenEditor();
			const context = e.getContext();
			context.visualEditor.setDevice(ui.getValue());
			e._visualChanged();
		}

		Workbench.showDialog({
			title: veNls.chooseDeviceSilhouette, 
			content: ui, 
			style: {width: 300}, 
			okCallback: dojo.hitch(this, _callback), 
			okLabel: actionNLS.select
		});
	}
});

});
