//>>built
define("davinci/ve/actions/ShowInOutline",["dojo/_base/declare","davinci/Runtime","davinci/Workbench","davinci/ve/actions/ContextAction"],function(c,d,e,f){return c("davinci.ve.actions.ManageStates",[f],{run:function(){var a=d.currentEditor;a&&a.delegate.showInOutline()},isEnabled:function(a){a=this.fixupContext(a);var b=e.getOpenEditor();return b&&a?"davinci.ve.PageEditor"==b.declaredClass?0<a.getSelection().length:!1:!1},shouldShow:function(a){return(a=(a=this.fixupContext(a))?a.editor:null)&&"davinci.ve.PageEditor"==
a.declaredClass}})});
//# sourceMappingURL=ShowInOutline.js.map