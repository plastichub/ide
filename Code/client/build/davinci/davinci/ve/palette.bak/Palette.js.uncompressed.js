define("davinci/ve/palette.bak/Palette", [
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/_base/connect",
	"dojo/on",
	"dojo/query",
	"dojo/dom-class",
	"dojo/Deferred",
	"dijit/focus",
	"davinci/Runtime",
	"davinci/Workbench",
	"dijit/_KeyNavContainer",
	"davinci/ui/dnd/DragSource",
	"davinci/ve/metadata",
	"davinci/library",
	"./PaletteFolder", // There must be a circular dependency - not available when this routine is loaded
	"./PaletteItem",
	"davinci/_common",
	"davinci/ve/tools/CreateTool",
	"davinci/workbench/Preferences",
	"dijit/_WidgetBase"
], function (
	declare, Lang, connect, On, Query, domClass, Deferred, FocusUtils, Runtime, Workbench, _KeyNavContainer,
	DragSource, Metadata, Library, PaletteFolder,
	PaletteItem, commonNls, CreateTool, Preferences, WidgetBase) {
	const debug = false;
	// Disable dijit's automatic key handlers until there is time to do it right
	return declare("davinci.ve.palette.Palette", [WidgetBase, _KeyNavContainer], {
		descriptors: "", // "fooDescriptor,barDescriptor"
		//	_resource: null,
		//	_context: null,
		_displayShowValue: 'block', // either block or inline-block, depending on editorPrefs.widgetPaletteLayout
		_presetClassNamePrefix: 'maqPaletteSection_', // Only used for debugging purposes
		_presetSections: {}, // Assoc array of all paletteItem objects, indexed by [preset][section]
		_presetCreated: {}, // Whether the given preset has created its PaletteFolder and PaletteItems
		raisedItems: [], // PaletteItems that have "raised" styling
		sunkenItems: [], // PaletteItems that have "sunken" styling
		moreItems: [], // PaletteItems that have "more" tooltip dialog showing
		helpItems: [], // PaletteItems that have "help" tooltip dialog showing
		_userWidgetSection: {
			"id": "$$UserWidgets$$",
			"name": "User Widgets",
			"includes": []
		},
		postMixInProperties: function () {
			this._resource = commonNls;
		},
		postCreate: function () {
			dojo.addClass(this.domNode, "dojoyPalette");
			this.refresh();
			// Disable dijit's automatic key handlers until there is time to do it right
			// As currently implemented, causes hidden palette folders from other presets to appear
			//this.connectKeyNavHandlers([dojo.keys.UP_ARROW], [dojo.keys.DOWN_ARROW]);

			/* Removing "refresh" logic because currently Palette.js only works
			 * at program initialization. Bad things happen if we try to recreate
			 * the palette in middle of a session. Of course, would be good to make
			 * the palette such that it could be reconstructed in middle of session.
			connect.subscribe("/davinci/ui/libraryChanged", this, "refresh");
			*/
			connect.subscribe("/davinci/ui/addedCustomWidget", this, "addCustomWidget");
			connect.subscribe("/davinci/preferencesChanged", this, "preferencesChanged");
		},

		addCustomWidget: function (lib) {
			/* make sure the pallette has loaded. if it hasnt, the init will take care of customs */
			if (!this._loaded) return;
			if (!lib || !lib.$wm || !lib.$wm.widgets || !lib.$wm.widgets.length) {
				return;
			}
			const context = Runtime.currentEditor.getContext();
			let comptype = context.getCompType();
			const editorPrefs = Preferences.getPreferences('davinci.ve.editorPrefs', Workbench.getProject());
			if (editorPrefs.showAllWidgets) {
				comptype = (comptype == 'mobile') ? "$ALLMOBILE" : "$ALLDESKTOP";
			}

			const $library = lib.$wm;
			const widgets = lib.$wm.widgets;
			let folderToShow = null;
			for (let w = 0; w < widgets.length; w++) {
				const item = widgets[w];
				for (const presetId in this._presetSections) {
					let customSection = null;
					const sections = this._presetSections[presetId];
					if (!sections) {
						console.error('Palette.js:addCustomWidget - no sections for comptype=' + presetId);
					} else {
						for (let s = 0; s < sections.length; s++) {
							const section = sections[s];
							if (section.id == '$$UserWidgets$$') {
								customSection = section;
								break;
							}
						}
						if (!customSection) {
							customSection = dojo.clone(this._userWidgetSection);
							customSection.preset = this._presetSections[presetId];
							customSection.presetId = presetId;
							customSection.items = [];
							sections.push(customSection);
							if (this._presetCreated[presetId]) {
								const orderedDescriptors = [customSection];
								this._generateCssRules(orderedDescriptors);
								this._createPalette(customSection);
								customSection._created = true;
							}
						}
						const includesValue = 'type:' + item.type;
						if (customSection.includes.indexOf(includesValue) < 0) {
							customSection.includes.push(includesValue);
							item.$library = $library;
							item.section = customSection;
							item._paletteItemGroup = this._paletteItemGroupCount++;
							customSection.items.push(item);
							const name = 'custom';
							let folder = null;
							const children = this.getChildren();
							for (let ch = 0; ch < children.length; ch++) {
								const child = children[ch];
								if (child.declaredClass == 'davinci.ve.palette.PaletteFolder') {
									if (child.presetId == presetId && child.section.id == '$$UserWidgets$$') {
										folder = child;
										break;
									}
								}
							}
							if (folder) {
								folder.domNode.style.display = 'none';
								const opt = {
									displayName: item.$library._maqGetString(item.type) ||
										item.$library._maqGetString(item.name) ||
										item.name,
									description: item.$library._maqGetString(item.type + "_description") ||
										item.$library._maqGetString(item.name + "_description") ||
										item.description ||
										item.type,
									name: item.name,
									paletteId: this.id,
									type: item.type,
									data: item.data || {
										name: item.name,
										type: item.type,
										properties: item.properties,
										children: item.children
									},
									tool: item.tool,
									category: name,
									section: customSection,
									PaletteFolderSection: folder,
									PaletteFolderSubsection: null,
									_paletteItemGroup: item._paletteItemGroup,
									_paletteGroupSelected: true
								};
								this._setIconProperties(item, opt);
								const newPaletteItem = this._createItem(opt, folder);
								newPaletteItem.domNode.style.display = 'none';
								if (comptype == presetId) {
									folderToShow = folder;
								}
							}
						}
					}
				}
			}
			if (folderToShow) {
				// open the currently active custom widget folder after creating a new custom widget
				folderToShow.showHideFolderContents(true);
			}
		},

		setContext: function (context) {
			this._context = context;
			try {
				this._loadPalette();
			} catch (e) {
				console.error('_loadPalette:crash ' + e);
			}
			// this._filter();
			this.updatePaletteVisibility();
		},

		refresh: function () {
			delete this._loaded;
			this._presetSections = {};
			this._createFolderTemplate();
			if (this._context) {
				try {
					this._loadPalette();
				} catch (e) {
					console.error('_loadPalette:crash');
				}
			}
		},

		_loadPalette: function () {
			if (this._loaded) {
				return;
			}

			const allLibraries = Metadata.getLibrary();
			const userLibs = Library.getUserLibs(Workbench.getProject()) || {};
			const libraries = {};

			function findInAll(name, version) {
				for (const n in allLibraries) {
					if (allLibraries.hasOwnProperty(n)) {
						const lib = allLibraries[n];
						if (lib.name === name && lib.version === version) {
							const ro = {};
							ro[name] = allLibraries[n];
							return ro;
						}
					}
				}
				return null;
			}
			userLibs.forEach((ulib) => {
				const library = findInAll(ulib.id, ulib.version);
				if (library) {
					dojo.mixin(libraries, library);
				}
			});

			// For developer notes on how custom widgets work in Maqetta, see:
			// https://github.com/maqetta/maqetta/wiki/Custom-widgets	

			const customWidgets = null;
			const customWidgetDescriptors = {};
			/*
			var customWidgets = Library.getCustomWidgets(Workbench.getProject());
			var customWidgetDescriptors = Library.getCustomWidgetDescriptors();
			if (customWidgets) {
				dojo.mixin(libraries, customWidgets);
			}
			*/
			debug && console.log('load palette', libraries);

			// Create a master list of widget types.
			// Used later to make sure that there aren't widgets in widgets.json files
			// that do not appear anywhere in widget palette
			const widgetTypeList = [];
			for (const name in libraries) {
				if (libraries.hasOwnProperty(name)) {
					const lib = libraries[name].$wm;
					if (!lib) {
						continue;
					}
					lib && lib.widgets && lib.widgets.forEach((item) => {
						// skip untested widgets
						if (item.category == "untested" || item.hidden) {
							return;
						}
						widgetTypeList.push(item.type);
					});
				}
			}
			this._paletteItemGroupCount = 0;
			this._widgetPalette = Runtime.getSiteConfigData('widgetPalette');
			if (!this._widgetPalette) {
				console.error('widgetPalette.json not defined (in siteConfig folder)');
			} else {
				// There should be a preset for each of built-in composition types (desktop, mobile, sketchhifi, sketchlofi)
				// In future, we might allow users to create custom presets
				const presets = this._widgetPalette.presets;
				if (!presets || typeof presets != 'object') {
					// console.warning('No presets defined in widgetPalette.json (in siteConfig folder)');
				} else {
					for (const p in presets) {
						const widgetList = widgetTypeList.concat(); // clone the array
						this._presetSections[p] = [];
						const preset = presets[p];
						let catchAllSection;
						// For each preset, the 'sections' property can either be a string or an array of section objects.
						// If a string, then that string is an reference to a sub-property of the top-level 'defs' object 
						let sections = (typeof preset.sections == 'string') ?
							(this._widgetPalette.defs ? this._widgetPalette.defs[preset.sections] : undefined) :
							preset.sections;
						if (!sections || !sections.length) {
							// console.warning('No sections defined for preset ' + p + ' in widgetPalette.json (in siteConfig folder)');
						} else {
                            const arr = [];
                            for (var cw in customWidgetDescriptors) {
								const obj = customWidgetDescriptors[cw];
								if (obj.descriptor) {
									arr.push({
										name: cw,
										value: obj
									});
								}
							}
                            arr.sort(function (a, b) {
								const aa = a.name.split('/').pop().toLowerCase();
								const bb = b.name.split('/').pop().toLowerCase();
								return aa < bb ? -1 : (aa > bb ? 1 : 0);
							});
                            let UserWidgetSectionAdded = false;
                            let userWidgetSection;
                            let customIncludes;
                            for (let j = 0; j < arr.length; j++) {
								if (!UserWidgetSectionAdded) {
									userWidgetSection = dojo.clone(this._userWidgetSection);
									sections = sections.concat(userWidgetSection);
									UserWidgetSectionAdded = true;
									customIncludes = userWidgetSection.includes;
								}
								const custWidgets = arr[j].value.descriptor.widgets;
								for (var cw = 0; cw < custWidgets.length; cw++) {
									const custWidget = custWidgets[cw];
									customIncludes.push('type:' + custWidget.type);
								}
							}
                            for (let s = 0; s < sections.length; s++) {
								// For each sections, the value can either be a string or a section objects.
								// If a string, then that string is an reference to a sub-property of the top-level 'defs' object 
								const sectionObj = (typeof sections[s] == 'string') ?
									(this._widgetPalette.defs ? this._widgetPalette.defs[sections[s]] : undefined) :
									sections[s];
								const section = dojo.clone(sectionObj);
								// Add preset name to object so downstream logic can add an appropriate CSS class
								// to the paletteFolder and paletteItem DOM nodes
								section.preset = preset;
								section.presetId = p;
								if (section.subsections && section.subsections.length) {
									const subsections = section.subsections;
									for (let sub = 0; sub < subsections.length; sub++) {
										if (typeof subsections[sub] == 'string') {
											const subsectionObj = this._widgetPalette.defs ? this._widgetPalette.defs[subsections[sub]] : undefined;
											if (subsectionObj) {
												subsections[sub] = dojo.clone(subsectionObj);
											}
										}
										const subsection = subsections[sub];
										if (subsection.includes && subsection.includes.indexOf("$$AllOthers$$") >= 0) {
											catchAllSection = subsection;
										}
										subsection.preset = preset;
										subsection.presetId = p;
										// Stuffs in value for section.items
										this._createSectionItems(subsection, preset, widgetList);
									}
								} else {
									// Stuffs in value for section.items
									this._createSectionItems(section, preset, widgetList);
									if (section.includes && section.includes.indexOf("$$AllOthers$$") >= 0) {
										catchAllSection = section;
									}
								}
								this._presetSections[p].push(section);
							}
                        }
						//console.error('widgetList');
						//console.dir(widgetList);
						for (let wi = 0; wi < widgetList.length; wi++) {
							const widgetType = widgetList[wi];
							if (catchAllSection) {
								const item = Metadata.getWidgetDescriptorForType(widgetType);
								const newItem = dojo.clone(item);
								this._prepareSectionItem(newItem, catchAllSection, this._paletteItemGroupCount);
								catchAllSection.items.push(newItem);
								this._paletteItemGroupCount++;
							} else {
								console.log('For preset ' + p + ' Not in widget palette: ' + widgetList[wi]);
							}
						}
					}
				}
			}
			this._loaded = true; // call this only once

		},

		// generate CSS Rules for icons based on this._descriptor
		// TODO: Currently this is used by Outline only, Palette should use
		_generateCssRules: function (descriptor) {
			const sheet = dojo.doc.styleSheets[0]; // This is dangerous...assumes content.css is first position
			if (!sheet) {
				return;
			}

			dojo.forEach(descriptor, function (component) {
				if (component.items) {
					dojo.forEach(component.items, function (item) {
						const iconSrc = item.iconBase64 || this._getIconUri(item.icon, "ve/resources/images/file_obj.gif");
						const selector = "img.davinci_" + item.type.replace(/[\.\/]/g, "_");
						const rule = "{background-image: url(" + iconSrc + ")}";
						if (dojo.isIE) {
							sheet.addRule(selector, rule);
						} else {
							sheet.insertRule(selector + rule, sheet.cssRules.length);
						}
					}, this);
				}
			}, this);
		},

		/**
		 * Creates the PaletteItems for a given section or subsection (the "component")
		 * @param {object} component  either a section object or subsection object from widgetPalette.json
		 * @param {object} params
		 *    params.presetClassName {string} classname to put onto PaletteItems (for debugging purposes)
		 */
		_createPaletteItemsForComponent: function (component, params) {
			debug && console.log('_createPaletteItemsForComponent ' + component.name + ' preset id ' + component.presetId, component);
			//console.log('_createPaletteItemsForComponent',params);
			if (component && component.id) {
				debug && console.log('_createPaletteItemsForComponent ' + component.id);
			}
			if (component.items) {
				let lastPaletteItemGroup = null;
				dojo.forEach(component.items, function (item) {
					// XXX For now, we want to keep some items hidden. If item.hidden is set, then don't
					//  add this item to palette (see bug 5626).
					if (item.hidden) {
						debug && console.log('_createPaletteItemsForComponent ' + component.id + ' is hidden');
						return;
					}

					const opt = {
						displayName: item.$library._maqGetString(item.type) ||
							item.$library._maqGetString(item.name) ||
							item.name,
						description: item.$library._maqGetString(item.type + "_description") ||
							item.$library._maqGetString(item.name + "_description") ||
							item.description ||
							item.type,
						name: item.name,
						paletteId: this.id,
						type: item.type,
						data: item.data || {
							name: item.name,
							type: item.type,
							properties: item.properties,
							children: item.children
						},
						category: component.name,
						section: component,
						preset: component.preset,
						presetId: component.presetId,
						presetClassName: params.presetClassName, // Only used for debugging purposes
						PaletteFolderSection: params.PaletteFolderSection,
						PaletteFolderSubsection: params.PaletteFolderSubsection,
						_paletteItemGroup: item._paletteItemGroup,
						_paletteGroupSelected: (item._paletteItemGroup != lastPaletteItemGroup), // Initially, first widget in group is selected
						_collectionName: (item.$library && item.$library.collections && item.$library.collections[item.collection] && item.$library.collections[item.collection].name),
						ignoreMeta: item.ignoreMeta,
						userData: item.userData,
						forceShow: item.forceShow,
						iconHTML: item.iconHTML
					};
					this._setIconProperties(item, opt);
					this._createItem(opt);
					lastPaletteItemGroup = item._paletteItemGroup;
				}, this);
			}
		},
		_createPalette: function (component) {
			const iconUri = "ve/resources/images/fldr_obj.gif";
			debug && console.log('_createPalette ' + component.id + '/' + component.name, component);
			// Only used for debugging purposes
			const presetClassName = component.presetId ? this._presetClassNamePrefix + component.presetId : null;
			const opt = {
				paletteId: this.id,
				icon: this._getIconUri(component.icon, iconUri),
				iconBase64: component.iconBase64,
				displayName: /* XXX component.provider.getDescriptorString(component.name) ||*/ component.name,
				section: component,
				subsections: component.subsections,
				subsection_container: null,
				preset: component.preset,
				presetId: component.presetId,
				presetClassName: presetClassName // Only used for debugging purposes
			};
			// this._createFolder() has a miniscule chance of not happening synchronously
			const deferred = this._createFolder(opt);
			deferred.then(function (PaletteFolderSection) {
				if (component.subsections && component.subsections.length) {
					for (let i = 0; i < component.subsections.length; i++) {
						const subsection = component.subsections[i];
						const opt2 = {
							paletteId: this.id,
							icon: this._getIconUri(subsection.icon, iconUri),
							iconBase64: subsection.iconBase64,
							displayName: subsection.name,
							section: component,
							subsection: subsection,
							subsection_container: PaletteFolderSection,
							preset: component.preset,
							presetId: component.presetId,
							presetClassName: presetClassName // Only used for debugging purposes
						};
						// this._createFolder() has a miniscule chance of not happening synchronously
						const deferred = this._createFolder(opt2);
						deferred.then(function (PaletteFolderSubsection) {
							PaletteFolderSection._children.push(PaletteFolderSubsection);
							this._createPaletteItemsForComponent(component.subsections[i], {
								presetClassName: presetClassName,
								PaletteFolderSection: PaletteFolderSection,
								PaletteFolderSubsection: PaletteFolderSubsection
							});
						}.bind(this));
					}
				} else {
					this._createPaletteItemsForComponent(component, {
						presetClassName: presetClassName,
						PaletteFolderSection: PaletteFolderSection,
						PaletteFolderSubsection: null
					});
				}
			}.bind(this));
		},

		_getIconUri: function (uri, fallbackUri) {
			if (uri) {
				/* maybe already resolved */
				if (uri.indexOf("http") == 0) {
					return uri;
				}

				return Workbench.location() + uri;
			}
			return require.toUrl("davinci/" + fallbackUri);
		},

		_createFolder: function (opt) {
			const deferred = new Deferred();
			debug && console.log('create palette folder', opt);
			// Must have a circular require() reference going on because
			// doesn't work to put require for PaletteFolder at top of file
			require(["davinci/ve/palette/PaletteFolder"], function (PaletteFolder) {
				var PaletteFolder = new PaletteFolder(opt);
				this.addChild(PaletteFolder);
				deferred.resolve(PaletteFolder);
			}.bind(this));
			return deferred;
		},

		_createFolderTemplate: function () {
			// <DIV class="dojoyPaletteFolder">
			//     <A href="javascript:void(0)"><IMG src="a.gif">label</A>
			// </DIV>
			this.folderTemplate = dojo.create('div', {
				className: 'dojoyPaletteCommon dojoyPaletteFolder dojoyPaletteFolderLow ui-widget-header',
				innerHTML: '<a href="javascript:void(0)"><img border="0"/></a>'
			});
		},

		_updateShowAllWidgetsPreference: function (newValue) {
			const id = 'davinci.ve.editorPrefs';
			const base = Workbench.getProject();
			const editorPrefs = Preferences.getPreferences(id, base);
			editorPrefs.showAllWidgets = newValue;
			Preferences.savePreferences(id, base, editorPrefs);
		},


		_clearFilter: function () {
			if (this.filterField) {
				this.filterField.set("value", "");
			}
			this._filter();
		},

		_filter: function () {
            const context = this.context;
            //xmaqhack
            let comptype = context.getCompType();
            const editorPrefs = Preferences.getPreferences('davinci.ve.editorPrefs', Workbench.getProject());
            editorPrefs.showAllWidgets = true;
            if (editorPrefs.showAllWidgets) {
				comptype = (comptype == 'mobile') ? "$ALLMOBILE" : "$ALLDESKTOP";
			}

            const // this.filterField.get("value"),
            value = '';

            const re = new RegExp(value, 'i');
            let action;
            const filterWidgetList = {};

            // reset to default state -- only show category headings
            const displayShowValue = this._displayShowValue;

            function resetWidgets(child) {
				if (child.declaredClass != 'dijit.form.TextBox') {
					const style = (child.declaredClass === 'davinci.ve.palette.PaletteFolder' &&
							child.presetId == comptype &&
							child._children.length > 0 &&
							(child._type == 'simple' || child._type == 'subsection_container')) ?
						'block' : 'none';
					dojo.style(child.domNode, 'display', style);
				}
			}

            // show widgets which match filter text
            function filterWidgets(child) {
				if (child.declaredClass == 'dijit.form.TextBox') {
					// do nothing
				} else if (child.declaredClass === 'davinci.ve.palette.PaletteFolder') {
					dojo.style(child.domNode, 'display', 'none');
				} else if (child.name && re.test(child.name)) {
					if (child.presetId == comptype && child._paletteGroupSelected && !filterWidgetList[child.type]) {
						dojo.style(child.domNode, 'display', displayShowValue);
						filterWidgetList[child.type] = true;
					} else {
						dojo.style(child.domNode, 'display', 'none');
					}
				} else {
					dojo.style(child.domNode, 'display', 'none');
				}
			}

            if (value === '') {
				action = resetWidgets;
				dojo.removeClass(this.domNode, 'maqWidgetsFiltered');
				if (this.toolbarDiv) {
					dojo.removeClass(this.toolbarDiv, 'maqWidgetsToolbarFiltered');
				}
			} else {
				action = filterWidgets;
				dojo.addClass(this.domNode, 'maqWidgetsFiltered');
				if (this.toolbarDiv) {
					dojo.addClass(this.toolbarDiv, 'maqWidgetsToolbarFiltered');
				}
			}
            this.getChildren().forEach(action);
        },

		_hasItem: function (type) {
			const children = this.getChildren();
			for (let i = 0; i < children.length; i++) {

				if (children[i].type == type) {
					return true; // already exists.
				}
			}
			return false;
		},

		_createItem: function (opt, folder) {
			debug && console.log('create item ' + opt.name, folder);
			if (opt.section.id != '$$UserWidgets$$') {
				// See if this proposed PaletteItem's collection property is included in the current preset
				const collection = Metadata.queryDescriptor(opt.type, 'collection');
				let presetCollections;
				if (opt.preset) {
					presetCollections = opt.preset.collections;
					if (!presetCollections || !presetCollections.length) {
						console.error('_create item : presetCollections.l=0');
						return;
					}
				}
				if (opt.preset && opt.preset.exclude) {
					const exclude = (typeof opt.preset.exclude == 'string') ?
						(this._widgetPalette.defs ? this._widgetPalette.defs[opt.preset.exclude] : undefined) :
						opt.preset.exclude;
					if (exclude && exclude.indexOf(opt.type) >= 0) {
						console.error('_create item : exclude = 1');
						return;
					}
				}
				let active = false;

				if (presetCollections && presetCollections.length) {
					for (let i = 0; i < presetCollections.length; i++) {
						if (presetCollections[i].id == collection && presetCollections[i].show) {
							active = true;
							break;
						}
					}

					//last chance
					if (opt.ignoreMeta === true) {
						active = true;
					}

					if (!active) {
						if (collection === 'dojoxmobile') {
							// debugger;
						}
						debug && console.log('createItem : not active ' + collection);
						return;
					}
				}
			} else {

			}
			//FIXME: temporary
			opt.icon = opt.iconLarge;
			const node = new PaletteItem(opt);
			if (!folder) {
				this.addChild(node);
			} else {
				folder.addChild(node);
			}

			opt.paletteItem = node;

			if (opt.PaletteFolderSubsection) {
				opt.PaletteFolderSubsection._children.push(node);
				node.domNode.style.display = (opt.PaletteFolderSubsection._isOpen && node._paletteGroupSelected) ? this._displayShowValue : 'none';
			} else if (opt.PaletteFolderSection) {
				opt.PaletteFolderSection._children.push(node);
				node.domNode.style.display = (opt.PaletteFolderSection._isOpen && node._paletteGroupSelected) ? this._displayShowValue : 'none';
			}
			const nodeToClone = Query('.paletteItemImageContainer', node.domNode)[0];
			const ds = new DragSource(node.domNode, "component", node, nodeToClone);
			ds.targetShouldShowCaret = true;
			ds.returnCloneOnFailure = false;
			this.connect(ds, "onDragStart", dojo.hitch(this, function (e) {
				this.onDragStart(e);
			})); // move start
			this.connect(ds, "onDragEnd", dojo.hitch(this, function (e) {
				this.onDragEnd(e);
			})); // move end
			if (opt.forceShow) {
				node.domNode.style.display = 'inline-block';
			}
			return node;
		},

		/**
		 * Prepare a section item
		 */
		_prepareSectionItem: function (item, section, paletteItemGroup) {
			const $wm = Metadata.getLibraryMetadataForType(item.type);
			item.$library = $wm;
			item.section = section;
			item._paletteItemGroup = paletteItemGroup;
		},

		/**
		 * Stuffs values into section.items (or subsection.items).
		 * section.items holds the list of PaletteItems that belong to this section (or subsection)
		 * of the widget palette. This routine does all of the detailed processing of the "includes"
		 * property in widgetPalette.json, including the grouping of widgets that represent 
		 * alternative versions of the same widget, sorting the alternatives by precedence order
		 * for the various widget libraries for the given "preset" (mobile, desktop, sketchhifi, sketchlofi)
		 * For each widget processed, if widget is in widgetList array, remove from the array
		 * @param {object} section - section or subsection object from widgetPalette.json file
		 * @param {object} preset - current preset
		 * @param {array[string]} widgetList - List of widget types, gets updated by this routine
		 */
		_createSectionItems: function (section, preset, widgetList) {
			section.items = [];
			collections = preset.collections;
			const includes = section.includes || [];
			for (let inc = 0; inc < includes.length; inc++) {
				const includeValue = includes[inc];
				// Each item in "includes" property can be an array of strings or string
				const includeArray = Lang.isArray(includeValue) ? includeValue : [includeValue];
				const sectionItems = [];
				for (let ii = 0; ii < includeArray.length; ii++) {
					const includeItem = includeArray[ii];
					let items = [];
					if (includeItem.substr(0, 5) === 'type:') {
						// explicit widget type
						var item = Metadata.getWidgetDescriptorForType(includeItem.substr(5));
						if (item) {
							items.push(item);
						}
					} else {
						items = Metadata.getWidgetsWithTag(includeItem);
					}
					for (let itemindex = 0; itemindex < items.length; itemindex++) {
						var item = items[itemindex];
						const newItem = dojo.clone(item);
						this._prepareSectionItem(newItem, section, this._paletteItemGroupCount);
						sectionItems.push(newItem);
					}
				}
				// Sort sectionItems based on order in "collections" property
				let sortedItems = [];
				// Created a sorted list of items, using preset.collections to define the order
				// of widgets within this group.
				if (collections && collections.length) {
					for (let co = 0; co < collections.length; co++) {
						const collection = collections[co];
						var si = 0;
						while (si < sectionItems.length) {
							const sectionItem = sectionItems[si];
							if (sectionItem.collection == collection.id) {
								sortedItems.push(sectionItem);
								sectionItems.splice(si, 1);
							} else {
								si++;
							}
						}
					}
					// Add any remaining section items to end of sortedItems
					for (var si = 0; si < sectionItems.length; si++) {
						sortedItems.push(sectionItems[si]);
					}
				} else {
					sortedItems = sectionItems;
				}
				for (var si = 0; si < sortedItems.length; si++) {
					const sortedItem = sortedItems[si];
					const idx = widgetList.indexOf(sortedItem.type);
					if (idx >= 0) {
						// Remove the current widget type from widgetList array
						// to indicate that the given widget has been added to palette
						widgetList.splice(idx, 1);
					}
					section.items.push(sortedItem);
				}
				this._paletteItemGroupCount++;
			}
		},

		/**
		 * Control visibility of the various paletteFolder and paletteItem controls
		 * based on the preset that applies to the currently open editor.
		 */
		updatePaletteVisibility: function () {
			// Determine which preset applies to the current editor
			/*
			if(!Runtime.currentEditor || Runtime.currentEditor.declaredClass != "davinci.ve.PageEditor" ||
					!Runtime.currentEditor.getContext){
				return;
			}
			*/
			this._clearFilter();
			const context = Runtime.currentEditor.getContext();
			const comptype = context.getCompType();
			/*var editorPrefs = Preferences.getPreferences('davinci.ve.editorPrefs', Workbench.getProject());
			if(editorPrefs.showAllWidgets){
				comptype = (comptype == 'mobile') ? "$ALLMOBILE" : "$ALLDESKTOP";
			}*/

			this._presetCreated[comptype] = true;
			const presetClassName = this._presetClassNamePrefix + comptype; // Only used for debugging purposes

			const editorPrefs = {
				widgetPaletteLayout: 'icons',
				snap: true
			};
			if (!editorPrefs.widgetPaletteLayout || editorPrefs.widgetPaletteLayout == 'icons') {
				dojo.addClass(this.domNode, "paletteLayoutIcons");
				this._displayShowValue = 'inline-block';
			} else {
				dojo.removeClass(this.domNode, "paletteLayoutIcons");
				this._displayShowValue = 'block';
			}

			//FIXME: current preset might be different than comptype once various new UI options become available
			const orderedDescriptions = [];
			const sections = this._presetSections[comptype];
			if (sections) {
				for (let s = 0; s < sections.length; s++) {
					const section = sections[s];
					if (!section._created) {
						const orderedDescriptors = [section];
						this._generateCssRules(orderedDescriptors);
						this._createPalette(section);
						section._created = true;
					}
				}
			}
			// Set display property to show only those PaletteFolder's and PaletteItem's
			// that correspond to the current preset
			// Set display:none for any PaletteFolders without any visible children
			const children = this.getChildren();
			for (let i = 0, len = children.length; i < len; i++) {
				const child = children[i];
				if (child && child.domNode && child.presetId) {
					if (child.declaredClass == "davinci.ve.palette.PaletteFolder") {
						if (child.presetId == comptype) {
							// Initially, hide any PaletteFolder's that are contain a subsection.
							if (child._children.length == 0 || child._type == 'subsection') {
								child.domNode.style.display = 'none';
							} else {
								child.domNode.style.display = 'block';
							}
						} else {
							child.domNode.style.display = 'none';
						}
						if (child._type == 'subsubsection_container') {
							child._openSubsection = null; // Only PaletteFolder's that have subsections use the "_openSubsection" property
						}
						child._isOpen = false;
					} else {
						if (child.forceShow) {
							child.domNode.style.display = 'inline-block';
						} else {
							child.domNode.style.display = 'none';
						}
					}
				}

			}
		},

		preferencesChanged: function () {
			//FIXME: the following logic has never been tested because currently
			//the preference for changing between icon and list view in widget palette
			//is unavailable
			const editorPrefs = Preferences.getPreferences('davinci.ve.editorPrefs', Workbench.getProject());
			const prop = (!editorPrefs.widgetPaletteLayout || editorPrefs.widgetPaletteLayout == 'icons') ?
				'iconLarge' : 'icon';
			const children = this.getChildren();
			for (let ch = 0; ch < children.length; ch++) {
				const child = children[ch];
				if (child.declaredClass == 'davinci.ve.palette.PaletteItem') {
					child.updateImgSrc(child[prop]);
				}
			}

			this.updatePaletteVisibility();
		},

		/**
		 * Call flat() for all PaletteItems that have raised or sunken styling
		 */
		flattenAll: function () {
			for (var i = 0; i < this.raisedItems.length; i++) {
				this.raisedItems[i].flat(this.raisedItems[i].domNode);
			}
			this.raisedItems = [];
			for (var i = 0; i < this.sunkenItems.length; i++) {
				const paletteItem = this.sunkenItems[i];
				if (paletteItem._tooltipDialog) {
					//FIXME: Need to generalize for help feature, too
					paletteItem.paletteItemMoreCloseCleanup();
					paletteItem.paletteItemHelpCloseCleanup();
				}
				paletteItem.flat(paletteItem.domNode);
				paletteItem._selectionShowing = false;
			}
			this.sunkenItems = [];
		},

		/**
		 * Remove any selection content for any of the PaletteItems
		 */
		removeSelectionAll: function () {
			try {
				this.flattenAll();
			} catch (e) {
				console.error('flatten failed');
			}

			const paletteItemSelectionArray = Query('.paletteItemSelectionContent', this.domNode);
			for (let i = 0; i < paletteItemSelectionArray.length; i++) {
				const node = paletteItemSelectionArray[i];
				if (node && node.parentNode) {
					node.parentNode.innerHTML = '';
				}
			}
		},

		/**
		 * Find all siblings paletteItem's that share the given paletteItemGroup.
		 * Assumes all widgets with same paletteItemGroup are adjacent siblings
		 * @param {PaletteItem} one of the PaletteItem's within the paletteItemGroup
		 * @returns {array}  an array of paletteItem's
		 */
		getPaletteItemsSameGroup: function (paletteItem) {
			const paletteItems = [];
			const paletteItemGroup = paletteItem._paletteItemGroup;
			let thisPaletteItem = paletteItem;
			let firstPaletteItem;
			// Go backwards while still matching
			while (thisPaletteItem && thisPaletteItem._paletteItemGroup) {
				if (thisPaletteItem._paletteItemGroup == paletteItemGroup) {
					firstPaletteItem = thisPaletteItem;
					thisPaletteItem = thisPaletteItem && thisPaletteItem.domNode && thisPaletteItem.domNode.previousSibling &&
						thisPaletteItem.domNode.previousSibling._paletteItem;
				} else {
					break;
				}
			}
			// Go forwards while still matching
			thisPaletteItem = firstPaletteItem;
			while (thisPaletteItem && thisPaletteItem._paletteItemGroup == paletteItemGroup) {
				paletteItems.push(thisPaletteItem);
				thisPaletteItem = thisPaletteItem && thisPaletteItem.domNode && thisPaletteItem.domNode.nextSibling &&
					thisPaletteItem.domNode.nextSibling._paletteItem;
			}
			return paletteItems;
		},

		_setIconProperties: function (item, opt) {
			// for small icons, first look for small icons, else look for large icons, else use file_obj.gif
			opt.icon = item.iconBase64 || (item.icon && this._getIconUri(item.icon, "ve/resources/images/file_obj.gif")) ||
				item.iconLargeBase64 ||
				(item.iconLarge && this._getIconUri(item.iconLarge, "ve/resources/images/file_obj.gif")) ||
				this._getIconUri(item.icon, "ve/resources/images/file_obj.gif");
			// for large icons, first look for small icons, else look for large icons, else use file_obj.gif
			opt.iconLarge = item.iconLargeBase64 ||
				(item.iconLarge && this._getIconUri(item.iconLarge, "ve/resources/images/file_obj.gif")) ||
				item.iconBase64 || this._getIconUri(item.icon, "ve/resources/images/file_obj.gif");
		},

		onDragStart: function (e) {
			this.removeSelectionAll();
			this.selectedItem = null;
			const data = e.dragSource.data;
			Metadata.getHelper(data.type, 'tool').then(function (ToolCtor) {
				// Copy the data in case something modifies it downstream -- what types can data.data be?
				const tool = new(ToolCtor || CreateTool)(dojo.clone(data.data), data.userData);
				this._context.setActiveTool(tool);
			}.bind(this));

			// Sometimes blockChange doesn't get cleared, force a clear upon starting a widget drag operation
			this._context.blockChange(false);

			// Place an extra DIV onto end of dragCloneDiv to allow 
			// posting a list of possible parent widgets for the new widget
			// and register the dragClongDiv with Context
			if (e._dragClone) {
				domClass.add(e._dragClone, 'paletteDragContainer');
				dojo.create('div', {
					className: 'maqCandidateParents'
				}, e._dragClone);
			}
			//FIXME: Attach dragClone and event listeners to tool instead of context?
			this._context.setActiveDragDiv(e._dragClone);
			this._dragKeyDownListener = dojo.connect(document, 'onkeydown', dojo.hitch(this, function (event) {
				const tool = this._context.getActiveTool();
				if (tool && tool.onKeyDown) {
					tool.onKeyDown(event);
				}
			}));
			this._dragKeyUpListener = dojo.connect(document, 'onkeyup', dojo.hitch(this, function (event) {
				const tool = this._context.getActiveTool();
				if (tool && tool.onKeyUp) {
					tool.onKeyUp(event);
				}
			}));
		},

		onDragEnd: function (e) {
			this._context.setActiveTool(null);
			this._context.setActiveDragDiv(null);
			dojo.disconnect(this._dragKeyDownListener);
			dojo.disconnect(this._dragKeyUpListener);
			if (FocusUtils.curNode && FocusUtils.curNode.blur) {
				FocusUtils.curNode.blur();
			}
			this.removeSelectionAll();
			this.selectedItem = null;
			this.flattenAll();

		},

		onDragMove: function (e) {
			// someone may want to connect to this...
		},

		nop: function () {
			return false;
		},

		__dummy__: null
	});
});