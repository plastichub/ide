define("davinci/ve/_ContextWidgets", [
    "dojo/_base/declare",
    "dojo/query",
    "dojo/Deferred",
    "dojo/promise/all",
    "dojo/_base/connect",
    "../Theme",
    "./widget",
    "./Focus",
    "./metadata",
    "./States",
    "./HTMLWidget",
    "./utils/GeomUtils",
    "dojox/html/_base"	// for dojox.html.evalInGlobal
], function (declare,
             query,
             Deferred,
             all,
             connect,
             Theme,
             Widget,
             Focus,
             metadata,
             States,
             HTMLWidget,
             GeomUtils) {

    return declare(null, {
        /**
         * Process dojoType, oawidget and dvwidget attributes on text content for containerNode
         */
        _processWidgets: function (containerNode, attachWidgets, states, scripts) {
            var prereqs = [];
            this._loadFileDojoTypesCache = {};
            dojo.forEach(query("*", containerNode), function (n) {
                var type = n.getAttribute("data-dojo-type") || n.getAttribute("dojoType") || /*n.getAttribute("oawidget") ||*/ n.getAttribute("dvwidget");
                //FIXME: This logic assume that if it doesn't have a dojo type attribute, then it's an HTML widget
                //Need to generalize to have a check for all possible widget type designators
                //(dojo and otherwise)
                if (!type) {
                    type = 'html.' + n.tagName.toLowerCase();
                }
                //doUpdateModelDojoRequires=true forces the SCRIPT tag with dojo.require() elements
                //to always check that scriptAdditions includes the dojo.require() for this widget.
                //Cleans up after a bug we had (7714) where model wasn't getting updated, so
                //we had old files that were missing some of their dojo.require() statements.
                prereqs.push(this.loadRequires((type || "").replace(/\./g, "/"), false/*doUpdateModel*/, true/*doUpdateModelDojoRequires*/));
                prereqs.push(this._preProcess(n));

                this._preserveStates(n, states);
                this._preserveDojoTypes(n);
            }, this);
            var promise = new Deferred();
            all(prereqs).then(function () {
                //FIXME: dojo/ready call may no longer be necessary, now that parser requires its own modules
                this.getGlobal()["require"]("dojo/ready")(function () {
                    //console.error('_processWidgets: ready!');
                    try {
                        // M8: Temporary hack for #3584.  Should be moved to a helper method if we continue to need this.
                        // Because of a race condition, override _getValuesAttr with a fixed value rather than querying
                        // individual slots.
                        try {

                            /*var sws = this.getGlobal()["require"]("dojox/mobile/SpinWheelSlot");
                             if (sws && sws.prototype && !sws.prototype._hacked) {
                             sws.prototype._hacked = true;
                              0 && console.warn("Patch SpinWheelSlot");
                             var keySuper = sws.prototype._getKeyAttr;
                             sws.prototype._getKeyAttr = function() {
                             if(!this._started) {
                             if(this.items) {
                             for(var i = 0; i < this.items.length; i++) {
                             if(this.items[i][1] == this.value) {
                             return this.items[i][0];
                             }
                             }
                             }
                             return null;
                             }
                             return keySuper.apply(this);
                             };
                             var valueSuper = sws.prototype._getValueAttr;
                             sws.prototype._getValueAttr = function() {
                             if(!this._started){
                             return this.value;
                             }
                             return valueSuper.apply(this) || this.value;
                             };
                             }*/
                        } catch (e) {
                            // ignore
                        }


                        var _re = this.getGlobal()["require"]("dijit/registry");

                        /*
                          0 && console.log('process widgets ', _re._hash);

                         // remove all registered widgets
                         _re.forEach(function(w) { //FIXME: use require?
                          0 && console.log('destroy widget : ' + w.id);
                         w.destroy();
                         });

                         // remove all registered widgets
                         _re.forEach(function(w) { //FIXME: use require?
                          0 && console.log('destroy widget 2- : ' + w.id);
                         w.destroy();
                         _re.remove(w);
                         });
                         _re._destroyAll();*/

                         0 && console.log('process widgets 2 ', _re._hash);

                        /*
                         if(_re){
                         _re._destroyAll();
                         _re._hash={};
                         }
                         */

                        /**
                         * 1. set root node,
                         * 2. set source data
                         * 3. process widgets;
                         */

                        this.getGlobal()["require"]("dojo/parser").parse(containerNode).then(function () {
                            // In some cases, parser wipes out the data-dojo-type. But we need
                            // the widget type in order to do our widget initialization logic.
                            this._restoreDojoTypes();

                            promise.resolve();

                            if (attachWidgets) {
                                this._attachAll();
                            }

                            if (scripts) {
                                try {
                                    dojox.html.evalInGlobal(scripts, containerNode);
                                } catch (e) {
                                    console.error('Error eval script in Context._setSourceData, ' + e);
                                }
                            }
                        }.bind(this), function (e) {
                            promise.reject(e);
                        });
                    } catch (e) {
                        // When loading large files on FF 3.6 if the editor is not the active editor (this can happen at start up
                        // the dojo parser will throw an exception trying to compute style on hidden containers
                        // so to fix this we catch the exception here and add a subscription to be notified when this editor is seleected by the user
                        // then we will reprocess the content when we have focus -- wdr

                        console.error('error in _processWidgets', e);
                        promise.reject(e);
                    }
                }.bind(this));
            }.bind(this));

            /*
             promise.otherwise(function(e){
             // remove all registered widgets, some may be partly constructed.
             this.getGlobal().dijit.registry.forEach(function(w){
             w.destroy();
             });

             this._editorSelectConnection = connect.subscribe("/davinci/ui/editorSelected",
             this, '_editorSelectionChange');
             });
             */

            return promise;
        },
        _preProcess: function (node) {
            //need a helper to pre process widget
            // also, prime the helper cache
            var type = node.getAttribute("data-dojo-type") || node.getAttribute("dojoType");
            //FIXME: This logic assume that if it doesn't have a dojo type attribute, then it's an HTML widget
            //Need to generalize to have a check for all possible widget type designators
            //(dojo and otherwise)
            if (!type) {
                type = 'html.' + node.tagName.toLowerCase();
            }
            return Widget.requireWidgetHelper((type || "").replace(/\./g, "/")).then(function (helper) {
                if (helper && helper.preProcess) {
                    helper.preProcess(node, this);
                }
            }.bind(this));
        },
        attach: function (widget) {

            if (!widget || widget._edit_focus) {
                return;
            }

            if (!widget._srcElement) {
                widget._srcElement = this._srcDocument.findElement(widget.id);
            }
            // The following two assignments needed for OpenAjax widget support
            if (!widget.type) {
                if (widget.isHtmlWidget) {
                    widget.type = "html." + widget.getTagName();
                } else if (widget.isGenericWidget) {
                    widget.type = widget.domNode.getAttribute('dvwidget');
                } else if (widget.isObjectWidget) {
                    widget.type = widget.getObjectType();
                } else {
                    widget.type = widget.declaredClass.replace(/\./g, "/"); //FIXME: not a safe association
                }
            }

            widget.metadata = widget.metadata || metadata.query(widget.type);
            widget._edit_context = this;

            widget.attach();

            //TODO: dijit-specific convention of "private" widgets
            if (widget.type.charAt(widget.type.lastIndexOf(".") + 1) == "_") {
                widget.internal = true;
                // internal Dijit widget, such as _StackButton, _Splitter, _MasterTooltip
                return;
            }

            var addOnce = function (array, item) {
                if (array.indexOf(item) === -1) {
                    array.push(item);
                }
            };
            var id = widget.getId();
            if (id) {
                addOnce(this._widgetIds, id);
            }
            var objectId = widget.getObjectId(widget);
            if (objectId) {
                addOnce(this._objectIds, objectId);
            }

            // Recurse down widget hierarchy
            dojo.forEach(widget.getChildren(true), this.attach, this);
        },
        _attachAll: function () {
             0 && console.log('context::_attachAll!');
            var rootWidget = this.rootWidget = new HTMLWidget({}, this.rootNode);
            rootWidget._edit_context = this;
            rootWidget.isRoot = true;
            var _doc = this._srcDocument;
            var _docEl = _doc.getDocumentElement();
            var body = _docEl.getChildElement("body");


            rootWidget._srcElement = this._srcDocument.getDocumentElement().getChildElement("body");
            var style = null;
            if (rootWidget._srcElement) {
                rootWidget._srcElement.setAttribute("id", "myapp");                
            }
            this._attachChildren(this.rootNode);
        },
        _attachChildren: function (containerNode) {
            query("> *", containerNode).map(Widget.getWidget).forEach(this.attach, this);
        },

        detach: function (widget) {
            // FIXME: detaching context prevent destroyWidget from working
            //widget._edit_context = undefined;
            var _global = this.getGlobal();

            var arrayRemove = function (array, item) {
                var i = array.indexOf(item);
                if (i != -1) {
                    array.splice(i, 1);
                }
            };

            var id = widget.getId(),
                thiz = this;

            if (id) {
                arrayRemove(this._widgetIds, id);
            }
            var objectId = widget.getObjectId();
            if (objectId) {
                arrayRemove(this._objectIds, objectId);
            }
            if (this._selection) {
                for (var i = 0; i < this._selection.length; i++) {
                    if (this._selection[i] == widget) {
                        this.focus(null, i);
                        this._selection.splice(i, 1);
                    }
                }
            }


            var library = metadata.getLibraryForType(widget.type);
            if (library) {
                var libId = library.name,
                    data = [widget.type, this];

                // Always invoke the 'onRemove' callback.
                metadata.invokeCallback(library, 'onRemove', data);
                // If this is the last widget removed from page from a given library,
                // then invoke the 'onLastRemove' callback.
                this._widgets[libId] -= 1;
                if (this._widgets[libId] === 0) {
                    metadata.invokeCallback(library, 'onLastRemove', data);
                }
            }

            //  0 && console.log('destroy children ',widget);
            var _c = widget.getChildren();
            //  0 && console.log('       children ',_c);


            dojo.forEach(widget.getChildren(), function (w) {
                thiz.detach(w);
            }, this);

            delete this._containerControls;
        },
        /**
         * Called by any command that can causes widgets to be added/deleted/moved/changed
         *
         * @param {number} type  0 - modified, 1 - added, 2 - removed
         */
        widgetChanged: function (type, widget) {
            if (type == 1) {
                this.widgetHash[widget.id] = widget;
            } else if (type == 2) {
                delete this.widgetHash[widget.id];
            }
        },
        resizeAllWidgets: function () {
            this.getTopWidgets().forEach(function (widget) {
                if (widget.resize) {
                    widget.resize();
                }
            });
        },
        /**
         * Returns an array of all widgets in the current document.
         * In the returned result, parents are listed before their children.
         */
        getAllWidgets: function () {
            var result = [];
            var find = function (widget) {
                result.push(widget);
                widget.getChildren().forEach(function (child) {
                    find(child);
                });
            };
            if (this.rootWidget) {
                find(this.rootWidget);
            }
            return result;
        },
        /**
         * Called by any commands that can causes widgets to be added or deleted.
         */
        widgetAddedOrDeleted: function (resetEverything) {
            var helper = Theme.getHelper(this.getTheme());
            if (helper && helper.widgetAddedOrDeleted) {
                helper.widgetAddedOrDeleted(this, resetEverything);
            } else if (helper && helper.then) { // it might not be loaded yet so check for a deferred
                helper.then(function (result) {
                    if (result.helper) {
                        this.theme.helper = result.helper;
                        if (result.helper.widgetAddedOrDeleted) {
                            result.helper.widgetAddedOrDeleted(this, resetEverything);
                        }
                    }
                }.bind(this));
            }
        },
        /**
         * Clear any cached widget bounds
         */
        clearCachedWidgetBounds: function () {
            this.getAllWidgets().forEach(function (widget) {
                var domNode = widget.domNode;
                if (domNode) {
                    GeomUtils.clearGeomCache(domNode);
                }
            });
        },
        updateFocus: function (widget, index, inline) {
            if (this.editor.getDisplayMode && this.editor.getDisplayMode() == 'source') {
                return;
            }
            Widget.requireWidgetHelper(widget.type).then(function (helper) {
                if (!this.editor.isActiveEditor()) {
                    return;
                }
                var box, op, parent;

                if (!metadata.queryDescriptor(widget.type, "isInvisible")) {
                    //Get the margin box (deferring to helper when available)
                    var helper = widget.getHelper();
                    if (helper && helper.getMarginBoxPageCoords) {
                        box = helper.getMarginBoxPageCoords(widget);
                    } else {
                        var node = widget.getStyleNode();
                        if (helper && helper.getSelectNode) {
                            node = helper.getSelectNode(this) || node;
                        }
                        box = GeomUtils.getMarginBoxPageCoords(node);
                    }

                    parent = widget.getParent();
                    op = {move: !(parent && parent.isLayout && parent.isLayout())};

                    //FIXME: need to consult metadata to see if layoutcontainer children are resizable, and if so on which axis
                    var resizable = (parent && parent.isLayout && parent.isLayout() ) ?
                        "none" : metadata.queryDescriptor(widget.type, "resizable");
                    switch (resizable) {
                        case "width":
                            op.resizeWidth = true;
                            break;
                        case "height":
                            op.resizeHeight = true;
                            break;
                        case "both":
                            op.resizeWidth = true;
                            op.resizeHeight = true;
                    }
                }
                this.focus({
                    box: box,
                    op: op,
                    hasLayout: (widget.isLayout && widget.isLayout()),
                    isChild: parent && parent.isLayout && parent.isLayout()
                }, index, inline);

                // Currently only used by theme editor
                this._focuses[0].showContext(this, widget);

            }.bind(this));
        },
        updateFocusAll: function () {
            if (this.editor && this.editor.getDisplayMode && this.editor.getDisplayMode() == 'source') {
                return;
            }
            var selection = this._selection;
            if (selection) {
                for (var i = 0; i < selection.length; i++) {
                    this.updateFocus(selection[i], i);
                }
            }
            //Update all of the highlights that show which widgets appear in a custom state
            // but which are actually visible on the base state and "shining through" to custom state
            States.updateHighlightsBaseStateWidgets(this);
        },
        hideFocusAll: function (startIndex) {
            if (!startIndex) {
                startIndex = 0;
            }
            var containerNode = this.getFocusContainer();
            if (this._focuses) {
                for (var i = startIndex; i < this._focuses.length; i++) {
                    var focus = this._focuses[i];
                    if (focus.domNode.parentNode == containerNode) {
                        focus.hide();
                        containerNode.removeChild(focus.domNode);
                    }
                }
            }
        },
        // If widget is in selection, returns the focus object for that widget
        getFocus: function (widget) {
            var i = this.getSelection().indexOf(widget);
            return i == -1 ? null : this._focuses[i];
        },
        /**
         * Returns true if the given node is part of the focus (ie selection) chrome
         */
        isFocusNode: function (node) {
            if (this._selection && this._selection.length && this._focuses && this._focuses.length >= this._selection.length) {
                for (var i = 0; i < this._selection.length; i++) {
                    if (this._focuses[i].isFocusNode(node)) {
                        return true;
                    }
                }
            }
            return false;
        },
        focus: function (state, index, inline) {
            this._focuses = this._focuses || [];
            var clear = false;
            if (index === undefined) {
                clear = true;
                index = 0;
            }
            var focus;
            if (index < this._focuses.length) {
                focus = this._focuses[index];
            } else {
                dojo.withDoc(this.getDocument(), dojo.hitch(this, function () {
                    focus = new Focus();
                    focus._edit_focus = true;
                    focus._context = this;
                }));
                this._focuses.push(focus);
            }

            //FIXME: DELETE THIS var containerNode = this.getContainerNode();
            var containerNode = this.getFocusContainer();

            if (state) {
                if (state.box && state.op) {
                    if (!focus._connected) {
                        this._connects.push(connect.connect(focus, "onExtentChange", this, "onExtentChange"));
                        focus._connected = true;
                    }
                    var w = this.getSelection();
                    focus.resize(state.box, w[0]);
                    var windex = index < w.length ? index : 0;	// Just being careful in case index is messed up
                    focus.resize(state.box, w[windex]);
                    focus.allow(state.op);
                    if (focus.domNode.parentNode != containerNode) {
                        containerNode.appendChild(focus.domNode);
                    }
                    focus.show(w[windex], {inline: inline});
                } else { // hide
                    focus.hide();
                }
                index++; // for clear
            } else if (!clear) { // remove
                if (focus.domNode.parentNode == containerNode) {
                    focus.hide();
                    containerNode.removeChild(focus.domNode);
                }
                this._focuses.splice(index, 1);
                this._focuses.push(focus); // recycle
            }
            if (clear) {
                this.hideFocusAll(index);
            }

        },
        /**
         * Returns the container node for all of the focus chrome DIVs
         */
        getFocusContainer: function () {

            var _c = document.getElementById('focusContainer');
            if (!_c) {
                _c = dojo.create('div', {'class': 'focusContainer', id: 'focusContainer'}, document.body);
                davinci.Workbench.focusContainer = _c;
            }
            return _c;
        },

        getTopWidgets: function () {
            var topWidgets = [];
            for (var node = this.rootNode.firstChild; node; node = node.nextSibling) {
                if (node.nodeType == 1 && node._dvWidget) {
                    topWidgets.push(node._dvWidget);
                }
            }
            return topWidgets;
        }
    });
});
