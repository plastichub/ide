require({cache:{
'url:davinci/ve/prefs/HtmlEditPreferences.html':"<div>\n\t<table style='margin: 4px;' cellspacing='4'>\n\t\t<tbody>\n\t\t\t<!--\n\t\t\t<tr>\n\t\t\t\t<td>${_loc.flowLayout}:</td>\n\t\t\t\t<td>\n\t\t\t\t\t<div dojoAttachPoint='flowBoxNode'></div>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t-->\n\t\t\t<tr>\n\t\t\t\t<td>${_loc.snapToNearestWidget}:</td>\n\t\t\t\t<td>\n\t\t\t\t\t<div dojoAttachPoint='snapNode'></div>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td>${_loc.showPossibleParents}:</td>\n\t\t\t\t<td>\n\t\t\t\t\t<div dojoAttachPoint='showPossibleParentsNode'></div>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td>${_loc.warnOnCSSOverride}:</td>\n\t\t\t\t<td>\n\t\t\t\t\t<div dojoAttachPoint='cssOverrideWarn'></div>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td>${_loc.absoluteWidgetsZindex}:</td>\n\t\t\t\t<td>\n\t\t\t\t\t<div dojoAttachPoint='absoluteWidgetsZindex'></div>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<!-- FIXME: Disabled for now. Ultimately, UI for this option should go to widget palette\n\t\t\t<tr>\n\t\t\t\t<td>${_loc.widgetPaletteLayout}:</td>\n\t\t\t\t<td>\n\t\t\t\t\t<div dojoAttachPoint='widgetPaletteLayout'></div>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t-->\n\t\t\t<tr>\n\t\t\t\t<td>${_loc.zazl}:</td>\n\t\t\t\t<td>\n\t\t\t\t\t<div dojoAttachPoint='zazl'></div>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody>\n\t</table>\n</div>"}});
define("davinci/ve/prefs/HTMLEditPreferences", [
    "dojo/_base/declare",
	"dijit/layout/ContentPane",
	"dijit/_TemplatedMixin",
	"dijit/form/CheckBox",
	"dijit/form/TextBox",
	"davinci/lang/ve/common",
	"dojo/text!./HtmlEditPreferences.html"
], function(
	declare,
	ContentPane,
	TemplatedMixin,
	CheckBox,
	TextBox,
	commonNls,
	templateString
) {

return declare([ContentPane, TemplatedMixin], {

	templateString: templateString,

	postMixInProperties: function(){
		this._loc = commonNls;
	},	

	postCreate: function(){
		//this._flowBox = new CheckBox({}, this.flowBoxNode);
		this._snap = new CheckBox({}, this.snapNode);
		this._showPossibleParents = new CheckBox({}, this.showPossibleParentsNode);
		this._cssOverrideWarn = new CheckBox({}, this.cssOverrideWarn);
		this._absoluteWidgetsZindex = new TextBox({}, this.absoluteWidgetsZindex);
/*FIXME: Disabled for now. Ultimately, UI for this option should go to widget palette
		this._widgetPaletteLayout = new Select({
			options:[
				{ label:commonNls.widgetPaletteShow_Icons,  value:'icons' },
				{ label:commonNls.widgetPaletteShow_List,  value:'list' }
			]
		}, this.widgetPaletteLayout);
*/
		this._zazl = new CheckBox({}, this.zazl);
		if(!this.containerNode){
			this.containerNode = this.domNode;
		}
	},

	getDefaults: function () {
	},
	
	setDefaults: function () {
	},
	
	doApply: function () {
	},
	
	getPreferences: function(){
		return {
			//flowLayout: this._flowBox.checked,
			snap: this._snap.checked,
			showPossibleParents: this._showPossibleParents.checked,
			cssOverrideWarn: this._cssOverrideWarn.checked,
			absoluteWidgetsZindex: this._absoluteWidgetsZindex.value,
/*FIXME: Disabled for now. Ultimately, UI for this option should go to widget palette
			widgetPaletteLayout: this._widgetPaletteLayout.value,
*/
			zazl: this._zazl.checked
		};
	},

	setPreferences: function(preferences){
		preferences = preferences || {};
		//this._check(this._flowBox, !!preferences.flowLayout);
		this._check(this._snap, !!preferences.snap);
		this._check(this._showPossibleParents, !!preferences.showPossibleParents);
		this._check(this._cssOverrideWarn, !!preferences.cssOverrideWarn);
		this._absoluteWidgetsZindex.set("value", preferences.absoluteWidgetsZindex);
/*FIXME: Disabled for now. Ultimately, UI for this option should go to widget palette
		this._widgetPaletteLayout.set("value", preferences.widgetPaletteLayout);
*/
		this._check(this._zazl, !!preferences.zazl);
	},

	_check: function(widget, checked){
		widget.set("checked", checked);
	},
	
	save: function(prefs){
		davinci.ve._preferences = prefs; //FIXME: missing dependency
	}
});
});
