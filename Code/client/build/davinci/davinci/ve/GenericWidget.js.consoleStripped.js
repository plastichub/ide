define("davinci/ve/GenericWidget", [
        "dojo/_base/declare",
        "./_Widget"
], function(declare, _Widget) {

return declare("davinci.ve.GenericWidget", _Widget, {
	isGenericWidget: true,
	constructor: function (params,node,type,metadata,srcElement) {
		dojo.attr(node, "dvwidget", type);
		if(srcElement) {
			srcElement.addAttribute("dvwidget", type);
		}
	},
	buildRendering: function() {
		this.containerNode = this.domNode; // for getDescendants()
		if(this._params) {
			for(const name in this._params) {
				this.domNode.setAttribute(name, this._params[name]);
			}
			this._params = undefined;
		}
	},
	_getChildrenData: function(options) {
		const childrenData = [];
		const childNodes = this.domNode.childNodes;
		for(let i = 0; i < childNodes.length; i++) {
			const n = childNodes[i];
			let d;
			switch(n.nodeType) {
			case 1: // Element
				const w = require("davinci/ve/widget").byNode(n);
				if(w) {
					d = w.getData( options);
				}
				break;
			case 3: // Text
				d = n.nodeValue.trim();
				if(d && options.serialize) {
					d = davinci.html.escapeXml(d);
				}
				break;
			case 8: // Comment
				d = "<!--" + n.nodeValue + "-->";
				break;
			}
			if(d) {
				childrenData.push(d);
			}
		}
		if(childrenData.length === 0) {
			return undefined;
		}
		return childrenData;
	},

	setProperties: function(properties) {
		const node = this.domNode;
		for(const name in properties) {
			if (name === 'style') { // needed for position absolute
				dojo.style(node, properties[name]);
			} else {
				if(!properties[name]) {
					node.removeAttribute(name);
				} else {
					node[name]= properties[name];
				}
			}
		}
		this.inherited(arguments);
	},

	_attr: function(name,value) {
		if (arguments.length>1) {
			this.domNode.setAttribute(name, value);
		} else {
			return this.domNode.getAttribute(name);
		}
	},

	getTagName: function() {
		return this.domNode.nodeName.toLowerCase();
	}
});

});
