//>>built
define("davinci/ve/RebaseDownload",["dojo/_base/declare","./RebuildPage","../library"],function(b,e,f){return b(e,{constructor:function(a){this.libs=a},getLibraryBase:function(a,d){for(var b in this.libs){var c=this.libs[b];if(c.id==a&&c.version==d)return a=new Deferred,a.resolve(c.root),a}return f.getLibRoot(a,d)||""}})});
//# sourceMappingURL=RebaseDownload.js.map