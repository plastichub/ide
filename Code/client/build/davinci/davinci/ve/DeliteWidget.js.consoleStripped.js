define("davinci/ve/DeliteWidget", [
    "dojo/_base/declare",
    "./_Widget",
    "./widget",
    "require",
    "xide/registry",
    "xide/utils"
], function (declare, _Widget, Widget, require, registry, utils) {

    const debugProps = false;
    const debugGetData = false;
    const debugGetChildren = false;

    function fixType(widget) {
        if (widget.declaredClass && widget.declaredClass !== 'davinci.ve.DeliteWidget') {
            widget.type = widget.declaredClass;
        } else if (widget.domNode && widget.domNode.declaredClass && widget.domNode.declaredClass.indexOf('uniq') == -1) {
            widget.type = widget.domNode.declaredClass;
        }


        if (!widget.type && widget.domNode.baseClass) {
            const type = 'delite' + '/' + utils.capitalize(widget.domNode.baseClass.replace('d-', ''));
            switch (type) {

                case 'delite/Radio-button':
                    {
                        widget.type = 'delite/RadioButton';
                        break;
                    }
                case 'delite/Checkbox':
                    {
                        widget.type = 'delite/Checkbox';
                        break;
                    }
                case 'delite/Accordion-header':
                    {
                        widget.type = 'delite/AccordionHeader';
                        break;
                    }
                case 'delite/Combobox':
                    {
                        widget.type = 'delite/Combobox';
                        break;
                    }
                case 'delite/Toggle-button':
                    {
                        widget.type = 'delite/ToggleButton';
                        break;
                    }
                case 'delite/Tab-bar':
                    {
                        widget.type = 'delite/TabBar';
                        break;
                    }
                case 'delite/View-stack':
                    {
                        widget.type = 'delite/ViewStack';
                        break;
                    }
                case 'delite/MediaPlayer':
                    {
                        widget.type = 'delite/MediaPlayer';
                        break;
                    }
                case 'delite/Panel':
                    {
                        widget.type = 'delite/Panel';
                        break;
                    }
                case 'delite/Select':
                    {
                        widget.type = 'delite/Select';
                        break;
                    }
                case 'delite/Button':
                    {
                        widget.type = 'delite/Button';
                        break;
                    }
                case 'delite/Accordion':
                    {
                        widget.type = 'delite/Accordion';
                        break;
                    }
                case 'delite/Slider':
                    {
                        widget.type = 'delite/Slider';
                        break;
                    }
            }

        }
        if (!widget.type) {
            if (widget.domNode && widget.domNode.getAttribute('is')) {
                const _is = widget.domNode.getAttribute('is');
                widget.type = _is;
            }
        }
        if (!widget.type) {
            console.error('have no widget type ', widget);
        } else {
            registry.add(widget.domNode)
        }


    };
    const Module = declare("davinci.ve.DeliteWidget", _Widget, {
        isGenericWidget: true,
        helper: Widget,
        getData: function (options) {
            options = options || {
                identify: true,
                preserveStates: true
            };

            let data;
            const helper = this.getHelper();
            if (helper && helper.getData) {
                data = helper.getData.apply(helper, [this, options]);
            } else {
                data = this._getData(options);
            }


            //  0 && console.log('get data : ' + options);

            let updatedId = false;
            if (this.id === 'no_id') {
                let _uniqueId = dijit.getUniqueId(this.type.toLowerCase().replace('/', '-').replace(/\./g, "_"));
                _uniqueId = _uniqueId.replace('delite/', 'd-').toLowerCase();
                this.id = _uniqueId;
                data.properties['id'] = _uniqueId;
                data.properties.id = _uniqueId;
                this.domNode.id = _uniqueId;
                updatedId = true;
            }

            if (options.identify && !updatedId) {
                const existing = registry.byId(this.id);
                if (existing) {
                    /*
                    _uniqueId = dijit.getUniqueId(this.type.replace(/\./g,"_"));
                    _uniqueId = _uniqueId.replace('delite/','d-').toLowerCase();
                    this.id = _uniqueId;
                    data.properties['id'] = _uniqueId;
                    data.properties.id = _uniqueId;
                    this.domNode.id = _uniqueId;*/
                    //console.error('already exists, set a new id : '+_uniqueId);
                }
            }

            registry.add(this.domNode);

            data.maqAppStates = dojo.clone(this.domNode._maqAppStates);
            data.maqDeltas = dojo.clone(this.domNode._maqDeltas);
            if (!data.properties)
                data.properties = {};

            if (this.properties) {
                for (var name in this.properties) {
                    if (!(name in data.properties)) {
                        data.properties[name] = this.properties[name];
                    }
                }
            }

            // Find "on*" event attributes and a[href] attributes that are in the model and
            // place on the data object. Note that Maqetta strips
            // on* event attributes and href attributes from the DOM that appears on visual canvas.
            // Upon creating new widgets, the calling logic needs to
            // put these attributes in model but not in visual canvas.
            const srcElement = this._srcElement;
            //FIXME: Assumes "attributes" is a public API. See #nnn
            const attributes = srcElement.attributes;
            for (let i = 0; i < attributes.length; i++) {
                const attribute = attributes[i];
                var name = attribute.name;
                if (attribute.name.substr(0, 2).toLowerCase() == "on") {
                    data.properties[attribute.name] = attribute.value;
                } else if (srcElement.tag.toLowerCase() == 'a' && attribute.name.toLowerCase() == 'href') {
                    data.properties[attribute.name] = attribute.value;
                } else if (name === 'stop' || name === "bidirectional" || name === 'block' || name === 'targetevent') {
                    data.properties[attribute.name] = attribute.value;
                }
            }

            // 0 && console.log('get data : '+data.properties.id + ' : ' + options.identify + ' this.id = ' + this.id,data);
            return data;
        },
        _fixType: function (widget) {
            return fixType(widget);
        },
        _getChildren: function (attach) {
            const children = [];
            const thiz = this;

            let searchNode = this.container || this.domNode;
            //this.containerNode is davinci.DeliteWidget, and if the widget is a 'Container', use it
            if (searchNode.containerNode && this.domNode.render == null) {
                searchNode = searchNode.containerNode;
            }
            if (this.domNode && this.domNode.render != null) {
                searchNode = this.domNode;
            }
            const veWidget = require("davinci/ve/widget");

            function search(searchNode) {
                dojo.forEach(searchNode.children, function (node) {
                    const _widget = veWidget.getWidget(node, false);

                    // 0 && console.log('delite widget: get children '+attach,[_widget,node]);
                    if (_widget && !_widget.type) {
                        thiz._fixType(_widget);
                    }

                    const ignore = false;
                    if (thiz.metadata && thiz.metadata.ignore && _widget.type) {
                        if (thiz.metadata.ignore.indexOf(_widget.type) !== -1) {
                             0 && console.log('ignore : ', attach);
                            return;
                        }
                    }
                    if (_widget && _widget.type && (_widget.type.indexOf('html.') == -1 || (thiz.metadata && thiz.metadata.htmlContent == true))) {
                        children.push(_widget);
                    }
                });
            }
            if (searchNode) {
                search(searchNode);
                if (children.length == 0 && this.domNode.containerNode) {
                    search(this.domNode.containerNode);
                }
            }

            if (thiz.metadata && thiz.metadata.noChildren) {
                return [];
            }

            // 0 && console.log('delite widget: get children for '+attach,searchNode);
            debugGetChildren &&  0 && console.log('delite widget: get children: ' + attach, [searchNode, children]);

            return children;
        },
        getChildren: function (attach) {
            const helper = this.getHelper();
            if (helper && helper.getChildren) {
                return helper.getChildren(this, attach);
            }

            const children = this._getChildren(attach);
            // 0 && console.log('_children '+this.id,children);
            return children;
        },
        constructor: function (params, node, type, metadata, srcElement) {
            this.acceptsHTMLChildren = true;
        },
        buildRendering: function () {
            //		if(this.srcNodeRef) {
            //			this.domNode = this.srcNodeRef;
            //		}else{
            //			this.domNode = dojo.doc.createElement("div");
            //		}
            this.containerNode = this.domNode; // for getDescendants()
            if (this._params) {
                for (const name in this._params) {
                    this.domNode.setAttribute(name, this._params[name]);
                }
                this._params = undefined;
            }
        },
        _getChildrenData: function (options) {


            function getTextContent(node) {
                let d = node.nodeValue.trim();
                if (d /*&& options.serialize*/ ) { // #2349
                    d = davinci.html.escapeXml(d);
                }
                return d;
            }
            const domNode = this.domNode;

            if (!domNode.hasChildNodes()) {
                return null;
            }

            if (domNode && domNode.getChildrenData) {
                return domNode.getChildrenData();
            }

            // Check if text node is the only child. If so, return text content as
            // the child data. We do this to match up with the code in
            // davinci.ve.widget.createWidget(), which can take child data either
            // as an array or as a string (representing the innerHTML of a node).
            if (domNode.childNodes.length === 1 && domNode.firstChild.nodeType === 3) {
                //return getTextContent(domNode.firstChild);
                 0 && console.log('"child content : ' + getTextContent(domNode.firstChild));
            }

            const childrenData = [];
            let childNodes = this.domNode.childNodes;
            const _widget = require("davinci/ve/widget");
            if (this.metadata && this.metadata.noChildren) {
                childNodes = [];
            }
            for (let i = 0; i < childNodes.length; i++) {
                const n = childNodes[i];
                let d;
                let isText = false;
                //var _w = _widget.byNode(n);
                switch (n.nodeType) {
                    case 1: // Element

                        const w = _widget.byNode(n);
                        if (w && w.type && w.type.indexOf('html.') !== -1 && this.metadata.htmlContent !== true) {
                            break;
                        }
                        if (this.metadata && this.metadata.ignore && w.type) {
                            if (this.metadata.ignore.indexOf(w.type) !== -1) {
                                break;
                            }
                        }
                        if (w) {
                            d = w.getData(options);
                        }
                        break;
                    case 3: // Text
                        d = n.nodeValue.trim();
                        if (d && (options.serialize || this.metadata.htmlContent === true)) {
                            d = davinci.html.escapeXml(d);
                        }
                        isText = true;
                        break;
                    case 8: // Comment
                        d = "<!--" + n.nodeValue + "-->";
                        break;
                }

                if (d && (d.type && d.type.indexOf('html.') == -1) || (d && this.metadata.htmlContent === true)) {
                    childrenData.push(d);
                }
            }

            // 0 && console.log('_getChildrenData', childrenData);
            if (childrenData.length === 0) {
                return undefined;
            }
            debugGetData &&  0 && console.log('_getchildren data', childrenData);
            return childrenData;
        },
        _getPropertyValue: function (name) {
            debugProps &&  0 && console.log('get properties', arguments);
            if (this.domNode._get) {
                let _p = this.domNode._get(name);
                if (_p != null) {
                    return _p;
                } else {
                    _p = this.domNode.getAttribute(name);
                }
                return _p;
            } else {
                //not parsed yet?
                if (this.containerNode && name in this.containerNode) {
                    return this.containerNode[name];
                }
            }

        },
        setProperties: function (properties) {
            const node = this.domNode;
            debugProps &&  0 && console.log('set properties', arguments);
            for (const name in properties) {
                if (name === 'style') { // needed for position absolute
                    dojo.style(node, properties[name]);
                } else {
                    if (!properties[name]) {
                        node.removeAttribute(name);
                    } else {
                        node[name] = properties[name];
                        if (node._set) {
                            node._set(name, properties[name]);
                        }
                        dojo.attr(node, name, properties[name]);
                    }
                }

            }
            this.inherited(arguments);
        },
        _attr: function (name, value) {
            // 0 && console.log('_attr', arguments);
            //return this.dijitWidget.get.apply(this.dijitWidget, arguments);
            /*
             if (arguments.length>1) {
             this.domNode.setAttribute(name, value);
             } else {
             return this.domNode.getAttribute(name);
             }*/
        },
        getTagName: function () {
            return this.domNode.nodeName.toLowerCase();
        },
        getParent: function () {

            if (this.type == 'xblox/RunScript' /*|| this.type=='xblox/CSSState' || this.type=='xblox/StyleState'*/ ) {
                const instance = this.domNode;
                if (instance && instance._targetReference && instance._targetReference._dvWidget) {
                    return instance._targetReference._dvWidget;
                }
            }
            return this.inherited(arguments);
            /*return require("davinci/ve/widget").getEnclosingWidget(this.domNode.parentNode) || this.domNode.parentNode;*/
        }
    });

    Module.fixType = fixType;
    return Module;
});