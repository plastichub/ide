/** @module xgrid/Base **/
define("davinci/ve/widgets/DropDown", [
    "dcl/dcl",
    "xdojo/declare",
    'xide/factory',
    "dojo/_base/lang",
    "dojo/_base/kernel",
    'xide/widgets/TemplatedWidgetBase',
    "xide/form/Select",
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable'
], function (
    dcl, declare, factory, lang, dojo,
    TemplatedWidgetBase, Select,
    TreeMemory, ObservableStore, Trackable) {

    var MultiInputDropDown = dcl(TemplatedWidgetBase, {
        numberDelta: 1,
        templateString: '<div ></div>',
        insertPosition: 1,
        declaredClass: "davinci.ve.widgets.MultiInputDropDown",
        data: null,
        divider: '---',
        postCreate: function () {},
        get: function (what) {
            if (what === 'value' && this._dropDown) {
                return this._getValueAttr();
            }
            return "";
        },
        startup: function () {
            var topSpan = dojo.doc.createElement("div");
            this._run = {};
            var self = this;
            if (!this.data) {
                this.data = [{
                        value: "auto"
                    },
                    {
                        value: "0px"
                    },
                    {
                        value: MultiInputDropDown.divider
                    },
                    {
                        value: "Remove Value",
                        run: function () {
                            this.set('value', '', false);
                            self._dropDown.set('value', '')
                        }
                    },
                    {
                        value: MultiInputDropDown.divider
                    },
                    {
                        value: "Help",
                        run: function () {
                            alert("help!")
                        }
                    }
                ];
            } else {
                this.data.push({
                    value: MultiInputDropDown.divider
                });
                this.data.push({
                    label: "Remove Value",
                    value: "Remove Value",
                    run: function () {
                        this.set('value', '', false);
                        self._dropDown.set('value', '', true);
                    }
                });
            }
            var displayValues = [];
            for (var i = 0; i < this.data.length; i++) {
                displayValues.push(this.data[i].value);
                if (this.data[i].run) {
                    this._run[this.data[i].value] = this.data[i].run;
                }
            }
            var options = [];
            _.each(this.data, function (data) {
                if (data.value || data.label === 'Remove Value') {
                    options.push({
                        label: data.label || data.value,
                        value: data.value,
                        run: data.run
                    })
                }
            })



            var SELECT_CLASS = Select;
            this._dropDown = new SELECT_CLASS({
                EDITABLE_CLASS: SELECT_CLASS.TYPEAHEAD,
                storeClass: declare('driverStore', [TreeMemory, Trackable, ObservableStore], {
                    setValues: function (values) {
                        var items = [];
                        var counter = 0;
                        if (values) {
                            this._values = values;
                        }
                        dojo.forEach(this.data, dojo.hitch(this, function (item) {
                            items.push({
                                label: item.label,
                                value: item.value,
                                id: counter++
                            });
                        }));

                        this._jsonData = {
                            identifier: "id",
                            items: items
                        };
                        this.setData(items);
                        this.root._emit('update', {
                            target: null
                        });
                    },
                    modifyItem: function (oldValue, newValue) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].value === oldValue) {
                                this.data[i].value = newValue;
                            }
                        }
                        self._dropDown.set('value', newValue);
                        this.setValues();
                    },
                    /* insert an item at the given index */
                    insert: function (atIndex, value) {
                        this.data.splice(atIndex, 0, {
                            value: value,
                            label: value
                        });
                        this.setValues();
                    },
                    contains: function (item) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].value == item) {
                                return true;
                            }
                        }
                        return false;
                    },
                    /* finds a value in the store that has the same units as specified value */
                    findSimilar: function (value) {
                        var numbersOnlyRegExp = new RegExp(/(\D*)(-?)(\d+)(\D*)/);
                        var numberOnly = numbersOnlyRegExp.exec(value);
                        if (!numberOnly) {
                            return;
                        }
                        var unitRegExp = new RegExp((numberOnly.length > 0 ? numberOnly[1] : "") + "(-?)(\\d+)" + (numberOnly.length > 3 ? numberOnly[4] : ""));
                        for (var i = 0; i < this.data.length; i++) {
                            if (unitRegExp.test(this.data[i].value)) {
                                return this.data[i].value;
                            }
                        }
                    },
                    getItemNumber: function (index) {
                        return this.data[index];
                    },
                    clearValues: function () {
                        this._loadFinished = false;
                    }
                }),
                required: false,
                title: "",
                editable: true,
                options: options,
                userData: {}
            });
            this._dropDown.startup();

            this._store = this._dropDown.store;
            var plus = factory.createButton(this._dropDown.previewNode, 'fa-plus', 'btn-default btn-xs2', null, '');
            this._plus = plus;
            dojo.style(plus, 'float', 'right');
            var minus = factory.createButton(this._dropDown.button0, 'fa-minus', 'btn-default btn-xs2', null, '');
            this._minus = minus;

            var div = dojo.create("div", {
                'class': "propInputWithIncrDecrButtons"
            });
            div.appendChild(this._dropDown.domNode);
            topSpan.appendChild(div);

            div = dojo.doc.createElement("div");
            dojo.style(div, "clear", "both");
            topSpan.appendChild(div);

            this._currentValue = this._store.data[0];
            dojo.connect(this._dropDown, "onKeyUp", this, "_updateSpinner");
            var thiz = this;

            if (this._dropDown._on) {
                this._dropDown._on('change', function (value) {
                    thiz._onChange(value);
                })
            } else {
                dojo.connect(this._dropDown, "onChange", this, "_onChange");
            }

            dojo.connect(this._plus, "onclick", this, "_plusButton", false);
            dojo.connect(this._minus, "onclick", this, "_minusButton", false);

            this._updateSpinner();
            this.domNode.appendChild(topSpan);
            this.add(this._dropDown);
            this.add(this._store);
        },
        _setReadOnlyAttr: function (isReadOnly) {
            this._isReadOnly = isReadOnly;
            if (this._dropDown) {
                this._dropDown.set("disabled", isReadOnly);
                dojo.attr(this._plus, "disabled", isReadOnly);
                dojo.attr(this._minus, "disabled", isReadOnly);
            }
        },
        onChange: function (event) {},
        _getValueAttr: function () {
            return this._dropDown.get("value");
        },
        _setValueAttr: function (value, priority) {
            this._dropDown.set("value", value, true);
            this._currentValue = this._dropDown.get("value");
            priority !== false && this._onChange(this._currentValue);
            if (!priority) {
                this.onChange();
            }
        },
        _changeValue: function (value, delta) {
            var split = value.split(" ");
            var result = "";
            for (var i = 0; i < split.length; i++) {
                if (i > 0) {
                    result += " ";
                }
                var bits = split[i].match(/([-\d\.]+)([a-zA-Z%]*)/);
                if (!bits) {
                    result += split[i];
                } else {
                    if (bits.length == 1) {
                        result += bits[0];
                    } else {
                        for (var z = 1; z < bits.length; z++) {
                            if (!isNaN(bits[z]) && bits[z] != "") {
                                result += parseFloat(bits[z]) + delta;
                            } else {
                                result += bits[z];
                            }
                        }
                    }
                }
            }
            return result;
        },
        _plusButton: function () {
            var oldValue = this._dropDown.get("value");
            var newString = this._changeValue(oldValue, this.numberDelta);
            this._store.modifyItem(oldValue, newString);
            this._dropDown.set("value", newString);
            this._onChange(newString);
        },
        _minusButton: function () {
            var oldValue = this._dropDown.get("value");
            var newString = this._changeValue(oldValue, -1 * this.numberDelta);
            this._store.modifyItem(oldValue, newString);
            this._dropDown.set("value", newString);
            this._onChange(newString);
        },
        _updateSpinner: function () {
            var value = this._dropDown.get("value");
            var numbersOnlyRegExp = /(-?)(\d+){1}/;
            var numberOnly = numbersOnlyRegExp.exec(value);
            if (numberOnly && numberOnly.length) {
                this._minus.disabled = this._plus.disabled = false;
                //dojo.removeClass(this._minus, "dijitHidden");
                //dojo.removeClass(this._plus, "dijitHidden");
            } else {
                //dojo.addClass(this._minus, "dijitHidden");
                //dojo.addClass(this._plus, "dijitHidden");
                this._minus.disabled = this._plus.disabled = true;
            }
            return true;
        },
        _onChange: function (event) {
            var similar;
            if (event in this._run) {
                this._dropDown.get("value", this._store.getItemNumber(0));
                dojo.hitch(this, this._run[event])();
            } else if (event == MultiInputDropDown.divider) {
                this._dropDown.get("value", this._store.getItemNumber(0));
            } else if (similar = this._store.findSimilar(event)) {
                this._store.modifyItem(similar, event);
            } else if (!this._store.contains(event)) {
                this._store.insert(this.insertPosition, event);
            }
            if (this._currentValue != this._dropDown.get("value")) {
                this._currentValue = this._dropDown.get("value");
                this.onChange(event);
            }
            this._updateSpinner();
        }

    });
    
    lang.setObject("davinci.ve.widgets.MultiInputDropDown", MultiInputDropDown);
    return MultiInputDropDown;
});