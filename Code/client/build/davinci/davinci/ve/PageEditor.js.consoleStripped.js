/** @module davinci/ve/PageEditor **/
define("davinci/ve/PageEditor", [
    "require",
    "dojo/_base/declare",
    "../ui/ModelEditor",
    "../Runtime",
    "../commands/CommandStack",
    "../html/ui/HTMLEditor",
    "./VisualEditor",
    "./VisualEditorOutline",
    "./widget",
    "./States",
    "../XPathUtils",
    "../html/HtmlFileXPathAdapter",
    "./utils/GeomUtils",
    'xide/mixins/EventedMixin',
    'xide/mixins/ReloadMixin',
    'xide/utils',
    'xide/types',
    'xideve/views/BlocksFileEditor',
    'xide/widgets/_Widget',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'wcDocker/iframe',
    'dijit/form/Select'
], function (require, declare, ModelEditor,
    Runtime, CommandStack, HTMLEditor,
    VisualEditor, VisualEditorOutline,
    widgetUtils, States, XPathUtils, HtmlFileXPathAdapter,
    GeomUtils, EventedMixin,
    ReloadMixin,
    utils, types, BlocksFileEditor, _Widget, _LayoutMixin, Docker) {


    const debugBlocks = false;
    /**
     * @class module:davinci/ve/PageEditor
     */
    const Module = declare("davinci.ve.PageEditor", [ModelEditor, _Widget, EventedMixin, ReloadMixin, _LayoutMixin], {
        _latestSourceMode: "source",
        _latestLayoutMode: "flow",
        item: null,
        delegate: null,
        template: null,
        bottomTabContainer: null,
        textEditorPane: null,
        didBlockViews: false,
        _propCP: null,
        createTab: function (type, args) {
            const DOCKER = types.DOCKER;
            const defaultTabArgs = {
                icon: false,
                closeable: false,
                moveable: true,
                tabOrientation: DOCKER.TAB.TOP,
                location: DOCKER.DOCK.STACKED

            };
            return this._docker.addTab(type || 'DefaultTab', utils.mixin(defaultTabArgs, args));
        },
        createLayout: function () {},
        constructor: function (element, fileName, fileItem, editorCreationFn, template, additionalEditors) {
            const DOCKER = types.DOCKER;
            const bottomTarget = null;
            this.item = fileItem;
            this.template = template;
            const thiz = this;
            this._commandStack = new CommandStack(this);
            this.savePoint = 0;
            this._docker = Docker.createDefault(element, {});

            this.domNode = this._docker.$container[0];
            this._designCP = this.createTab(null, {
                title: 'Design',
                icon: 'fa-eye'
            });

            const unlock = (delay) => {
                setTimeout(() => {
                    const context = thiz.getContext();
                    context && context.frameNode && $(context.frameNode).css('pointer-events', 'initial');
                }, delay || 4000);
            }
            this._docker.on(types.DOCKER.EVENT.BEGIN_RESIZE, function (e) {
                const context = thiz.getContext();
                context && context.frameNode && $(context.frameNode).css('pointer-events', 'none');
                unlock();
            });

            this._docker.on(types.DOCKER.EVENT.MOVE_STARTED, function (e) {
                const context = thiz.getContext();
                context && context.frameNode && $(context.frameNode).css('pointer-events', 'none');
                unlock();
            });

            this._docker.on(types.DOCKER.EVENT.MOVE_ENDED, () => unlock(1));
            this._docker.on(types.DOCKER.EVENT.END_RESIZE, () => unlock(1));
            this._docker.on(types.DOCKER.EVENT.BEGIN_FLOAT_RESIZE, function (e) {
                const context = thiz.getContext();
                $(context.frameNode).css('pointer-events', 'none');
                unlock();
            });

            this._docker.on(types.DOCKER.EVENT.END_FLOAT_RESIZE, () => unlock(1));
            this._designCP.$container[0].domNode = this._designCP.$container[0];

            this._designCP.domNode = this._designCP.$container[0];
            this._designCP.minSize(400, 400);

            this.visualEditor = new VisualEditor(this._designCP.$container[0], this, this.template);
            this.currentEditor = this.visualEditor;
            this.currentEditor._commandStack = this._commandStack;
            this.initReload();
            try {
                const editors = additionalEditors;
                this._propCP = this.createTab(null, {
                    title: 'Props',
                    icon: 'fa-cogs',
                    tabOrientation: DOCKER.TAB.TOP,
                    location: DOCKER.TAB.RIGHT,
                    target: this._designCP
                });
                this._propCP.maxSize(400);
                this._propCP.minSize(350);
                this._propCP.closeable(false);

                let textEditorParent = null;
                let editorPane = null;

                if (editors.length) {
                    editorPane = this.createTab(null, {
                        title: 'HTML',
                        icon: 'fa-code',
                        tabOrientation: DOCKER.TAB.TOP,
                        location: DOCKER.TAB.BOTTOM
                    });
                    textEditorParent = editorPane.domNode;
                    this._srcCP = editorPane;

                    const propPane = this.createTab(null, {
                        title: 'States',
                        icon: 'fa-eye',
                        tabOrientation: DOCKER.TAB.RIGHT,
                        target: editorPane,
                        location: DOCKER.TAB.RIGHT
                    });
                    propPane.maxSize(400);
                    propPane.minSize(350);
                    // propPane.closeable(true);
                    propPane.initSize(400, null);
                    this.statesTab = propPane;

                    // propPane.maxSize(null, 200);


                } else {
                    console.error('have no editors, ');
                }

                const useAce = true;
                let htmlEditor = null;
                try {
                    if (!useAce) {
                        htmlEditor = this.htmlEditor = new HTMLEditor(textEditorParent, fileName, true);
                    } else {
                        htmlEditor = this.htmlEditor = editorCreationFn(editorPane);
                    }
                } catch (e) {
                    debugger;
                }


                this.model = htmlEditor.model;

                htmlEditor._on(types.EVENTS.ON_FILE_CONTENT_CHANGED, function (evt) {
                    thiz.delegate.onEditorContentChanged(evt);
                });


                this._displayMode = "design";
                this._connect(this.visualEditor, "onContentChange", "_visualChanged");
                this.subscribe("/davinci/ui/styleValuesChange", this._stylePropertiesChange);
                this.subscribe("/davinci/ui/widgetSelected", this._widgetSelectionChange);
                this.subscribe("/davinci/ui/selectionChanged", this._modelSelectionChange);
                //this._connect(this.visualEditor.context, "onSelectionChange","_widgetSelectionChange");
                this.subscribe("/davinci/ui/editorSelected", this._editorSelected.bind(this));
                this.subscribe("/davinci/ui/context/loaded", this._contextLoaded.bind(this));
                this.subscribe("/davinci/ui/deviceChanged", this._deviceChanged.bind(this));
                this._designCP.getSplitter().pos(0.5);
                setTimeout(() => {
                    this._srcCP.getSplitter().pos(0.5);
                    this._propCP.maxSize(500);
                }, 1500);
                this.resize();

            } catch (e) {
                this.visualEditor._connectCallback(e);
                console.error('page editor crash : ' + e);
            }
            this.add(this._docker);
        },
        getPropertyPane: function () {
            return this._propCP;
        },
        getDesignPane: function () {
            return this._designCP;
        },
        getSourcePane: function () {
            return this._srcCP;
        },
        /**
         * Return Davinci's Visual Editor
         * @returns {module:davinci/ve/VisualEditor}
         */
        getVisualEditor: function () {
            return this.visualEditor;
        },
        getBlocksPane: function () {
            return this._blocksTab;
        },
        /**
         *
         * @param evt
         */
        onSceneBlocksLoaded: function (evt, ve) {
            if (ve.showBlocks == false) {
                return;
            }
            if (this.didBlockViews) {
                return;
            }
            this.didBlockViews = true;

            const scopes = evt.blockScopes;
            const ctx = evt.ctx;
            const tabContainer = this.bottomTabContainer;
            const thiz = this;
            const DOCKER = types.DOCKER;
            this._blocksTab = this.createTab(null, {
                title: 'Blocks',
                icon: 'fa-eye',
                target: this._srcCP,
                tabOrientation: DOCKER.TAB.TOP,
                location: DOCKER.DOCK.STACKED
            });

            const _createBlockEditor = function (scope, where) {
                const blockEditor = utils.addWidget(BlocksFileEditor, {
                    style: 'height:inherit;width:inherit;padding:0px;',
                    ctx: ctx,
                    pageEditor: thiz,
                    visualEditor: ve,
                    editorContext: thiz.getContext(),
                    item: {
                        path: scope.path,
                        mount: scope.mount
                    }
                }, thiz, where, true);

                blockEditor.initWithScope(scope);
                where.add(blockEditor);
                return blockEditor;
            };

            const scope = scopes[0];
            this._blockEditor = _createBlockEditor(scope, this._blocksTab);
            const selectedItem = ve.getItem();
            if (selectedItem) {
                this._blockEditor._widgetSelectionChanged({
                    selection: [selectedItem]
                });
            }
            return;
        },
        getBlockEditor: function () {
            return this._blockEditor;
        },
        setRootElement: function (rootElement) {
            this._rootElement = rootElement;
        },
        supports: function (something) {
            // Note: the propsect_* values need to match the keys in SwitchingStyleView.js
            const regex = /^palette|properties|style|states|inline-style|MultiPropTarget|propsect_common|propsect_widgetSpecific|propsect_events|propsect_layout|propsect_paddingMargins|propsect_background|propsect_border|propsect_fontsAndText|propsect_shapesSVG$/;
            return something.match(regex);
        },
        focus: function () {
            //		if(this.currentEditor==this.visualEditor)
            //			this.visualEditor.onContentChange();
        },
        _editorSelected: function (event) {
            const context = this.getContext();
            if (!context) {
                return;
            }
            if (this == event.oldEditor) {
                context.hideFocusAll();
            }
            if (event.editor && event.editor.editorContainer &&
                (event.editor.declaredClass == 'davinci.ve.PageEditor' ||
                    event.editor.declaredClass == 'davinci.ve.themeEditor.ThemeEditor')) {
                if (this == event.editor) {
                    const flowLayout = context.getFlowLayout();
                    const layout = flowLayout ? 'flow' : 'absolute';
                    this._updateLayoutDropDownButton(layout);
                    context.clearCachedWidgetBounds();
                }
            }
        },
        _contextLoaded: function () {

        },
        _deviceChanged: function () {
            if (Runtime.currentEditor == this && this.editorContainer) {
                const context = this.getContext();
                if (context && context.updateFocusAll) {
                    // setTimeout is fine to use for updateFocusAll
                    // Need to insert a delay because new geometry
                    // isn't ready right away.
                    // FIXME: Should figure out how to use deferreds or whatever
                    // to know for sure that everything is all set and we
                    // can successfully redraw focus chrome
                    setTimeout(function () {
                        context.updateFocusAll();
                    }, 1000);
                }
            }
        },
        _updateLayoutDropDownButton: function (newLayout) {
            const layoutDropDownButtonNode = dojo.query('.maqLayoutDropDownButton');
            if (layoutDropDownButtonNode && layoutDropDownButtonNode[0]) {
                const layoutDropDownButton = dijit.byNode(layoutDropDownButtonNode[0]);
                if (layoutDropDownButton) {
                    //layoutDropDownButton.set('label', veNls['LayoutDropDownButton-'+newLayout]);
                }
            }

        },
        _selectLayout: function (layout) {
            this._latestLayoutMode = layout;
            require(["davinci/actions/SelectLayoutAction"], function (SelectLayoutAction) {
                var SelectLayoutAction = new SelectLayoutAction();
                SelectLayoutAction._changeLayoutCommand(layout);
            });
            this._updateLayoutDropDownButton(layout);
        },
        selectLayoutFlow: function () {
            this._selectLayout('flow');
        },
        selectLayoutAbsolute: function () {
            this._selectLayout('absolute');
        },
        getDisplayMode: function () {
            return this._displayMode;
        },
        getSourceDisplayMode: function () {
            return this._latestSourceMode;
        },
        _switchDisplayModeSource: function (newMode) {
            this._latestSourceMode = newMode;
            this.switchDisplayMode(newMode);
        },
        switchDisplayModeSource: function () {
            this._switchDisplayModeSource("source");
        },
        switchDisplayModeSplitVertical: function () {
            this._switchDisplayModeSource("splitVertical");
        },
        switchDisplayModeSplitHorizontal: function () {
            this._switchDisplayModeSource("splitHorizontal");
        },
        switchDisplayModeSourceLatest: function () {
            this.switchDisplayMode(this._latestSourceMode);
        },
        switchDisplayModeDesign: function () {
            this.switchDisplayMode("design");
        },
        switchDisplayMode: function (newMode) {
            return;
        },
        hidePropertyPane: function (hide) {
            const pane = this.getPropertyPane();
            //pane && hide==true && pane._close ? pane._close() : pane._open();
        },
        _modelSelectionChange: function (selection) {
            /*
             * we do not want to drive selection on the view editor unless:
             *     - we are in an editor mode which has a view editor (not source mode)
             *     - we are the current editor
             */
            if (this._displayMode == "source" || require("davinci/Runtime").currentEditor !== this) { //FIXME: require("davinci/Runtime")!=Runtime.  Why??
                return;
            }

            this._selectionCssRules = null;
            if (selection.length) {
                const htmlElement = selection[0].model;
                if (htmlElement && htmlElement.elementType == "HTMLElement") {
                    const id = htmlElement.getAttribute("id");
                    if (id && this._displayMode != "source") {
                        const widget = widgetUtils.byId(id, this.visualEditor.context.getDocument());
                        const box = GeomUtils.getMarginBoxPageCoords(widget.domNode);
                        this.getContext().getGlobal().scroll(box.l, box.t);
                        this.visualEditor.context.select(widget);
                    }
                }
            }
        },
        _widgetSelectionChange: function (selection) {
            if (selection.isFake === true) {
                return;
            }
            if (!this.visualEditor.context ||
                (selection && selection.length && selection[0]._edit_context != this.visualEditor.context)) {
                return;
            }
            var selection = this.visualEditor.context.getSelection();
            if (selection && selection.length) {
                if (this._displayMode != "design") {
                    this.htmlEditor.selectModel([{
                        model: selection[0]._srcElement
                    }]);
                }
            }
        },
        _stylePropertiesChange: function (value) {
            if (value && value.cascade && value.cascade._widget && value.cascade._widget.isFake == true) {
                return;
            }
            this.visualEditor._stylePropertiesChange(value);
            const selection = this.visualEditor.context.getSelection();
            if (selection && selection[0]) {
                const node = utils.getNode(selection[0]);
                if (node && node.onChanged) {
                    node.onChanged();
                }
            }
            const widget = value.cascade._topWidgetDom;
            const style = $(widget).attr("style");
            this.getContext().updateBackground(widget);
        },
        _setDirty: function () {
            this.setDirty(true);
        },
        setDirty: function (isDirty) {
            this.isDirty = isDirty;
            if (isDirty) {
                this.lastModifiedTime = Date.now();
            }
            if (this.editorContainer) {
                this.editorContainer.setDirty(isDirty);
            }
        },
        _visualChanged: function (skipDirty) {
            if (!this.delegate.shouldUpdateTextEditor()) {
                return;
            }
            if (!skipDirty) {
                this._setDirty();
            }
            this.htmlEditor.set('value', this.model.getText(), true);
            this.getContext().visualEditorChanged();
        },
        _srcChanged: function () {
            // Because this can be called from SourceChangeCommand, make sure dojo.doc is bound to the Workbench
            dojo.withDoc(window.document, function () {
                const wasTyping = this.htmlEditor.isTyping;
                if (wasTyping) {
                    this.visualEditor.skipSave = true;
                }
                const context = this.visualEditor.context;
                const statesScenes = context && this._getStatesScenes(context);
                this.visualEditor.setContent(this.fileName, this.htmlEditor.model);
                this.editorContainer.updateToolbars();
                dojo.publish('/davinci/ui/context/pagerebuilt', [context]);
                if (statesScenes) {
                    this._setStatesScenes(context, statesScenes);
                }
                delete this.visualEditor.skipSave;
                this._setDirty();
            }, this);
        },
        /**
         * Returns an object holding the set of currently selected application states and (mobile) scenes
         * @return {object}  { statesInfo:statesInfo, scenesInfo:scenesInfo }
         */
        _getStatesScenes: function (context) {
            return [];
            let statesFocus = States.getFocus(context.rootNode);
            if (!statesFocus) {
                statesFocus = {
                    stateContainerNode: context.rootNode
                };
            }
            if (typeof statesFocus.state != 'string') {
                statesFocus.state = States.NORMAL;
            }
            const statesInfo = States.getAllStateContainers(context.rootNode).map(function (stateContainer) {
                const currentState = States.getState(stateContainer);
                const currentStateString = typeof currentState == 'string' ? currentState : States.NORMAL;
                const xpath = XPathUtils.getXPath(
                    stateContainer._dvWidget._srcElement,
                    HtmlFileXPathAdapter);
                const focus = statesFocus.stateContainerNode == stateContainer &&
                    statesFocus.state == currentStateString;
                return {
                    currentStateXPath: xpath,
                    state: currentState,
                    focus: focus
                };
            });
            scenesInfo = {};
            const sceneManagers = context.sceneManagers;
            for (const smIndex in sceneManagers) {
                const sm = sceneManagers[smIndex];
                scenesInfo[smIndex] = {
                    sm: sm
                };
                scenesInfo[smIndex].sceneContainers = sm.getAllSceneContainers().map(function (sceneContainer) {
                    const currentScene = sm.getCurrentScene(sceneContainer);
                    const sceneContainerXPath = XPathUtils.getXPath(
                        sceneContainer._dvWidget._srcElement,
                        HtmlFileXPathAdapter);
                    const currentSceneXPath = XPathUtils.getXPath(
                        currentScene._dvWidget._srcElement,
                        HtmlFileXPathAdapter);
                    return {
                        sceneContainerXPath: sceneContainerXPath,
                        currentSceneXPath: currentSceneXPath
                    };
                });
            }
            return {
                statesInfo: statesInfo,
                scenesInfo: scenesInfo
            };
        },
        /**
         * Sets the current scene(s) and/or current application state
         * @param {object}  object of form { statesInfo:statesInfo, scenesInfo:scenesInfo }
         */
        _setStatesScenes: function (context, statesScenes) {
            //#xmaqhack
            return;
            const statesInfo = statesScenes.statesInfo;
            if (statesInfo) {
                for (var i = 0; i < statesInfo.length; i++) {
                    const info = statesInfo[i];
                    var xpath = info.currentStateXPath;
                    var element = context.model.evaluate(xpath);
                    if (!element) {
                        continue;
                    }
                    var widget = widgetUtils.byId(element.getAttribute('id'), context.getDocument());
                    States.setState(info.state, widget.domNode, {
                        focus: info.focus
                    });
                }
            }

            const scenesInfo = statesScenes.scenesInfo;
            for (const smIndex in scenesInfo) {
                const sm = scenesInfo[smIndex].sm;
                const allSceneContainers = scenesInfo[smIndex].sceneContainers;
                for (i = 0; i < allSceneContainers.length; i++) {
                    const sceneContainer = allSceneContainers[i];
                    var xpath = sceneContainer.sceneContainerXPath;
                    var element = context.model.evaluate(xpath);
                    if (!element) {
                        continue;
                    }
                    var widget = widgetUtils.byId(element.getAttribute('id'), context.getDocument());
                    const sceneContainerNode = widget.domNode;

                    xpath = sceneContainer.currentSceneXPath;
                    element = context.model.evaluate(xpath);
                    if (!element) {
                        continue;
                    }
                    sm.selectScene({
                        sceneContainerNode: sceneContainerNode,
                        sceneId: element.getAttribute('id')
                    });
                }
            }
        },
        getContext: function () {
            return this.visualEditor.context;
        },
        getOutline: function () {
            if (!this.outline) {
                this.outline = new VisualEditorOutline(this);
            }
            return this.outline;
        },
        getPropertiesView: function () {
            return this.currentEditor.getPropertiesView();
        },
        setContent: function (filename, content, newHtmlParams) {
            this.fileName = filename;
            this.htmlEditor.setContent(filename, content);
            this.visualEditor.setContent(filename, this.htmlEditor.model, newHtmlParams);
            this._connect(this.htmlEditor.model, "onChange", "_themeChange");
            // update the source with changes which may have been made during initialization without setting dirty bit
            //this.htmlEditor.setValue(this.model.getText(), true);
        },
        _themeChange: function (e) {
            if (e && e.elementType === 'CSSRule') {
                this.setDirty(true); // a rule change so the CSS files are dirty. we need to save on exit
                this.visualEditor.context.hotModifyCssRule(e);
            }
        },
        getDefaultContent: function () {
            this._isNewFile = true;
            return this.visualEditor.getDefaultContent();
        },
        selectModel: function (selection, editor) {
            if (this.publishingSelect || (editor && this != editor)) {
                return;
            }
            const selectionItem = selection && selection[0];
            if (!selectionItem) {
                return;
            }
            if (selectionItem.elementType) {
                this.htmlEditor.selectModel(selection);
            } else if (selectionItem.model && selectionItem.model.isWidget) {
                this.visualEditor.context.select(selectionItem.model, selectionItem.add);
            }
        },
        save: function (isAutoSave) {


            if (this.delegate && this.delegate.save) {

                const model = this.visualEditor.context.getModel();
                const modelText = model.getText();
                this.delegate.save(this, this.visualEditor, model, modelText);
                return;
            }
            this.savePoint = this._commandStack.getUndoCount();
            const promises = this.visualEditor.save(isAutoSave);
            if (promises && promises.then) {
                promises.then(
                    function (results) {
                        this.isDirty = isAutoSave;
                        if (this.editorContainer && this.editorContainer.domNode) {
                            this.editorContainer.setDirty(isAutoSave);
                        }
                    }.bind(this),
                    function (error) {
                        /*var message = veNls.vteErrorSavingResourceMessage + error;
                         dojo.publish("/davinci/resource/saveError", [{message:message, type:"error"}]);
                         console.error(message);
                         */
                    }
                );
            }


        },
        removeWorkingCopy: function () { //wdr
        },
        previewInBrowser: function () {
            this.visualEditor.previewInBrowser();
        },
        destroy: function () {
            this.inherited(arguments);
            this.visualEditor.destroy();
            this.htmlEditor.destroy();
            utils.destroy(this.bottomTabContainer);
        },
        getText: function () {
            return this.htmlEditor.getText();
        },
        onResize: function () {
            const context = this.getContext();
            const selections = context.getSelection();
            for (let i = 0; i < selections.length; i++) {
                const add = (i != 0);
                context.select(selections[i], add);
            }
        },
        // dummy handler
        handleKeyEvent: function (e) {},
        /**
         * Return clipping bounds for focusContainer node, whose main purpose is to
         * clip the selection chrome so it doesn't impinge on other parts of the UI
         */
        getFocusContainerBounds: function () {
            if (this._displayMode == 'source') {
                return {
                    l: 0,
                    t: 0,
                    w: 0,
                    h: 0
                };
            } else {
                const clipTo = this._designCP.domNode;
                const box = GeomUtils.getBorderBoxPageCoords(clipTo);

                /*FIXME: See #2951. This isn't working in all cases yet, so commenting out.
                 When a silhouette is active, need to check for an active scroll bar on this._designCP.domNode
                 but when no silhouette, need to check the HTML node on the user's document within iframe.
                 Code below only deals with this._designCP.domNode.
                 // Back off selection chrome in case this._designCP has scrollbar(s)
                 if(clipTo.scrollWidth > clipTo.clientWidth && (clipTo.clientWidth - scrollbarWidth) < box.w){
                 box.w = clipTo.clientWidth - scrollbarWidth;
                 }
                 if(clipTo.scrollHeight > clipTo.clientHeight && (clipTo.clientHeight - scrollbarWidth) < box.h){
                 box.h = clipTo.clientHeight - scrollbarWidth;
                 }
                 */
                // Make the clip area 8px bigger in all directions to make room
                // for selection chrome, which is placed just outside bounds of widget
                box.l -= 8;
                box.t -= 8;
                const device = (this.visualEditor && this.visualEditor.getDevice) ? this.visualEditor.getDevice() : 'none';
                if (device == 'none') {
                    box.w += (this._displayMode == 'splitVertical' ? 8 : 16);
                    box.h += (this._displayMode == 'splitHorizontal' ? 8 : 16);
                } else {
                    box.w += 8;
                    box.h += 8;
                }
                return box;
            }
        },
        getCommandStack: function () {
            const context = this.getContext();
            return context.getCommandStack();
        }
    });

    Module.__test = "asd2f";

    return Module;
});