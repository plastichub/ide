//FIXME: A bunch of hard-coded strings in here that need to be globalized
define("davinci/ui/Resource", [
    'system/resource',
    '../model/Path',
    '../Runtime',
    '../Workbench',
    '../workbench/Preferences'
    //'davinci/lang/ui/ui',
    //'davinci/Theme',
    //'dijit/form/Button'
], function (Resource, Path, Runtime, Workbench, Preferences) {

        var createNewDialog = function () {};
    var checkFileName = function (fullPath) {
        var resource = Resource.findResource(fullPath);
        if (resource) {
            alert("File already exists!");
        }

        return !resource;
    };

    var getSelectedResource = function () {
        return (uiResource.getSelectedResources() || [])[0];
    };

    var uiResource = {
        newHTMLDialogSpecificClass: "davinci/ui/widgets/NewHTMLFileOptions",

        newHTMLMobile: function () {
            /*
            this.newHTML({
                comptype: 'mobile',
                title: uiNLS.createMobileApplication,
                dialogSpecificClassOptions: {showDevices: true, showThemeSetsButton: true}
            });
            */
        },
        newHTMLDesktop: function () {
            /*
            this.newHTML({
                comptype: 'desktop',
                title: uiNLS.createDesktopApplication,
                dialogSpecificClassOptions: {showDevices: false, showThemeSetsButton: true},
                device: 'desktop'
            });
            */
        },
        newHTMLSketchHiFi: function () {
           
        },
        newHTMLSketchLoFi: function () {
           
        },

        newHTML: function (params) {

        },

        newCSS: function () {
          
        },

        /* method to select a given resource in the explorer tree */

        selectResource: function (resource) {
            var resourceTree = dijit.byId("resourceTree");
            var path = [];
            for (var i = resource; i.parent; i = i.parent) {
                path.unshift(i);
            }

            resourceTree.set('path', path);
        },

        newFolder: function (parentFolder, callback) {
            
        },

        /* close an editor editting given resource */
        closeEditor: function (resource, flush) {
            
        },
        save: function () {
             0 && console.log('resource save');
            var editor = Workbench.getOpenEditor();
            if (editor) {
                // check if read only
                system.resource.findResourceAsync(editor.fileName).then(
                    dojo.hitch(this, function (resource) {
                        if (resource.readOnly()) {
                            this.saveAs(resource.getExtension(), 'save');
                        } else {
                            editor.save();
                        }
                    })
                );
            }
        },

        saveAs: function (extension, optionalMessage) {
            /*
            var oldEditor = Workbench.getOpenEditor();
            var oldFileName = oldEditor.fileName;

            var newFileName = new Path(oldFileName).lastSegment();
            var oldResource = Resource.findResource(oldFileName);

            var newDialog = createNewDialog(uiNLS.fileName, uiNLS.save, extension, null, null, newFileName, oldResource, optionalMessage);
            var executor = function () {
                var resourcePath = newDialog.get('value');
                var oldResource = Resource.findResource(oldFileName);
                var oldContent;
                var themeSet;
                var theme;

                if (oldEditor.editorID == "davinci.html.CSSEditor") {
                    // this does some css formatting
                    oldContent = oldEditor.getText();
                } else {
                    oldContent = (oldEditor.model && oldEditor.model.getText) ? oldEditor.model.getText() : oldEditor.getText();
                }
                if (oldEditor.editorID == "davinci.ve.HTMLPageEditor") {
                    themeSet = Theme.getThemeSet(oldEditor.visualEditor.context);
                    theme = oldEditor.visualEditor.context.theme;
                }


                var existing = Resource.findResource(resourcePath);

                oldEditor.editorContainer.forceClose(oldEditor);
                if (existing) {
                    existing.removeWorkingCopy();
                    existing.deleteResource();
                }
                // Do various cleanups around currently open file
                //oldResource.removeWorkingCopy(); // 2453 Factory will clean this up..
                oldEditor.isDirty = false;
                // Create a new editor for the new filename
                var file = Resource.createResource(resourcePath);
                new RebuildPage().rebuildSource(oldContent, file, theme, themeSet).then(function (newText) {
                    file.setContents(newText).then(function () {
                        Workbench.openEditor({fileName: file});
                    });
                });
            };
            Workbench.showModal(newDialog, uiNLS.saveFileAs, '', executor);
            */
        },

        newJS: function () {
           
        },

        openFile: function () {
            
        },

        addFiles: function () {
            
        },

        addFilesZip: function () {
            
        },

        getNewFileName: function (fileOrFolder, fileDialogParentFolder, extension) {

            var existing, proposedName;
            var count = 0;
            if (!extension) {
                extension = "";
            }
            do {
                count++;
                if (fileOrFolder === 'folder') {
                    proposedName = 'folder' + count;
                } else {
                    proposedName = 'file' + count + extension;
                }
                var fullname = fileDialogParentFolder.getPath() + '/' + proposedName;
                existing = Resource.findResource(fullname);
            } while (existing);
            return proposedName;
        },

        canModify: function (item) {
            return !item.readOnly();
        },

        newProject: function () {
            /*
            var projectDialog = new NewProject({});
            Workbench.showModal(projectDialog, uiNLS.newProject, {"min-width": '330px'}, null, true);*/
        },

        renameAction: function () {

        },

        getResourceIcon: function (item, opened) {
            
        },

        getResourceClass: function (item) {
            if (item.readOnly()) {
                return "readOnlyResource";
            }
        },

        deleteAction: function () {
            
        },

        getSelectedResources: function () {
            var selection = Runtime.getSelection();
            if (selection[0] && selection[0].resource) {
                return dojo.map(selection, function (item) {
                    return item.resource;
                });
            }
        },

        alphabeticalSortFilter: {
            filterList: function (list) {
                return list.sort(function (file1, file2) {
                    return file1.name > file2.name ? 1 : file1.name < file2.name ? -1 : 0;
                });
            }

        },
        foldersFilter: {
            filterItem: function (item) {
                if (item.elementType == 'File') {
                    return true;
                }
            }
        },

        openPath: function (path, text) {
            
        },

        openResource: function (resource, newHtmlParams) {

        }

    };

    return dojo.setObject("davinci.ui.Resource", uiResource);
});
