require({cache:{
'url:davinci/ui/widgets/templates/projectToolbar.html':"<div class='projectToolbar'>\t \n\t<table cellSpacing=\"0\" cellPadding=\"0\" border=\"0\" style=\"width:100%\">\n\t<tr>\n\t<td width=\"100%\">\n\t<div class=\"projectSelection\" dojoType=\"davinci.ui.widgets.ProjectSelection\" dojoAttachPoint=\"_projectSelection\"></div>\n\t</td>\n\t</tr>\n\t</table>\n</div>"}});
define("davinci/ui/widgets/ProjectToolbar", ["dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "system/resource",
        "davinci/Workbench",
        "../Rename",
        "dojo/dom-attr",
        "dojo/text!./templates/projectToolbar.html",
        "davinci/lang/ui/ui",
        "dijit/form/Button",
        "dijit/form/TextBox",
        "dijit/form/RadioButton",
        "dijit/layout/ContentPane",
        "./ProjectSelection"
],function(declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, systemResource, Workbench, Rename, domAttr, templateString, uiNLS){
	
	return declare("davinci.ui.widgets.ProjectToolbar",   [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

		templateString: templateString,

		postCreate: function(){
			this.connect(this._projectSelection, "onChange", this._projectSelectionChanged);
			this._currentProject = this._projectSelection.get("value");
		},
		
		onChange: function(){
		},

		_projectSelectionChanged: function(){
			var newProject = this._projectSelection.get("value");
			if(newProject==this._currentProject) {
				return;
			}
			Workbench.loadProject(newProject);
		}

	});
});