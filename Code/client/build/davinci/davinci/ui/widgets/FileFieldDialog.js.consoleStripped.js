require({cache:{
'url:davinci/ui/widgets/templates/FileFieldDialog.html':"<div>\n\t<div style='float:right;'>\n\t\t<button type='button' style='font-size:.75em;float:right;' dojoType=\"dijit.form.Button\" dojoAttachPoint=\"button\" dojoAttachEvent=\"onClick:_showFileSelectionDialog\">...</button>\n\t</div>\n\t<div style='margin-right:35px;'>\n\t\t<input style='width:100%;' type='text' dojoType=\"dijit.form.TextBox\" dojoAttachPoint=\"textField\" dojoAttachEvent=\"onChange:_onChange\"></input>\n\t</div>\n</div>\n\n"}});
define("davinci/ui/widgets/FileFieldDialog", ["dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "davinci/Workbench",
    "davinci/ui/widgets/OpenFile",
    "dijit/form/Button",
    "davinci/model/Path",
    "davinci/lang/ui",
    "dijit/lang/_common",
    "dojo/text!./templates/FileFieldDialog.html",
    "dijit/form/TextBox",
    "xide/views/_PanelDialog",
    "xide/utils",
    "xide/types",
    "xfile/views/FileGrid",
    "xfile/FileActions"

], function (declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Workbench, OpenFile, Button, Path, uiNLS,
             commonNLS, templateString,TextBox,_PanelDialog,utils,types,FileGrid,FileActions) {

    var idPrefix = "davinci_ui_widgets_filefielddialog_generated";
    var __id = 0;
    var getId = function () {
        return idPrefix + (__id++);
    };

    return declare("davinci.ui.widgets.FileFieldDialog", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: templateString,
        widgetsInTemplate: true,
        _fileSelectionDialog: null,
        _showFileSelectionDialog: function () {
            var thiz = this;
            function done(item){
                var path = utils.buildPath(item.mount,item.path,false);
                var url = thiz.ctx.getFileManager().getImageUrl(item,true,{
                    oriPath : path
                });
                 0 && console.log(' file picker done ' ,path);
                thiz.textField.set("value", path);
                thiz._onChange();
            }
            this.ctx = Workbench.ctx;
            var picker = FileActions.createFilePicker(this,"",done,uiNLS.selectFile,null,{
                mount:'workspace_user',
                selection:'.'
            },null,FileGrid,{
                micromatch : "(*.png)|(*.jpg)|!(*.*)" // Only folders and markdown files
            });

        },
        _setBaseLocationAttr: function (baseLocation) {
            // this is the directory to make everything relative to. also the first directory to show
            this._baseLocation = baseLocation;
        },
        _setValueAttr: function (value) {
            if (this.value != value) {
                this.value = value;
                this.textField.set("value", value);
            }
        },
        _setDisabledAttr: function (value) {
            this.textField.set("disabled", value);
            this.button.set("disabled", value);
            this.inherited(arguments);
        },
        _setIntermediateChangesAttr: function (value) {
            this.textField.set("intermediateChanges", value);
            this.inherited(arguments);
        },
        _onChange: function () {
            var value = this.textField.get("value");

            if (this.value != value) {
                this.value = value;
                this.textField.set('value', this.value);
                this.onChange(value);
            }
        },
        onChange: function (event) {        },
        _getValueAttr: function () {
            return this.textField.get("value");
        }
    });
});