require({cache:{
'url:davinci/ui/templates/downloadSelected.html':"<div class='downloadDialog'>\t \n\t<div class=\"dijitDialogPaneContentArea\">\n\t\t<div style=\"margin-bottom: 20px\">\n\t\t  ${fileName}: <input data-dojo-type='dijit/form/ValidationTextBox' type=\"text\" maxLength='${_fileNameMaxLength}' data-dojo-attach-point=\"__fileName\" value='${_projectName}.zip' data-dojo-props=\"regExp:'${_fileNameValidationRegExp}', required:true, invalidMessage:'${invalidDownloadFileName}'\"></input>\n\t\t</div>\n\t\t\n\t\t<div data-dojo-attach-point=\"_selectionDiv\"></div>\n\n  </div>\n\t<div class=\"dijitDialogPaneActionBar\">\n\t\t<button data-dojo-type='dijit/form/Button' data-dojo-attach-point=\"_okButton\" data-dojo-attach-event='onClick:okButton' label='${downloadButtonLabel}' class=\"maqPrimaryButton\" type=\"submit\"></button>\n\t\t<button data-dojo-type='dijit/form/Button' data-dojo-attach-event='onClick:cancelButton' label='${buttonCancel}' class=\"maqSecondaryButton\"></button>\n\t</div>\n</div>"}});
define("davinci/ui/DownloadSelected", ["dojo/_base/declare",
        "./Download",
        "./Resource",
        "davinci/lang/ui/ui",
        "dojo/text!./templates/downloadSelected.html"
],function(declare, Download, ResourceUI, uiNLS, templateString){

	return declare([Download], {
		templateString: templateString,

		_buildUITable: function(){
			this._files = ResourceUI.getSelectedResources();
			var uiArray = ["<div class='downloadSelectedHeader'>" + uiNLS.selectedFiles + "</div>",
			               "<div class='downloadSelectedList'>"];
			if(!this._files){
				uiArray.push("<b>" + uiNLS.noFilesSelected + "</b>");
				this._files = [];
				dojo.attr(this._okButton, "disabled", "true");
			}

			uiArray = uiArray.concat(this._files.map(function(file) {
				return file.getPath() + "<br>";
			}));
			uiArray.push("</div><br><br>");
			dojo.place(uiArray.join(""), this._selectionDiv);
		},

		_getResources: function(){
			return this._files.map(function(item){ return item.getPath();});
		},

		_getLibs: function(){
			return [];
		}
	});
});