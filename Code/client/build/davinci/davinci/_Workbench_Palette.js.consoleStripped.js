define("davinci/_Workbench_Palette", [
    "dcl/dcl",
    "davinci/Runtime"
], function (dcl, Runtime){
    var paletteTabWidth = 71;	// Width of tabs for left- and right-side palettes
    var paletteTabDelta = 20;	// #pixels - if this many or fewer pixels of tab are showing, treat as collapsed
    var paletteCache = {};
    var debugActions = false;
    var debugMenu = false;
    var debugPalette = false;
    var WorkbenchImplementation = {
        declaredClass:"davinci.Workbench_Menu",
        _rearrangePalettes: function (newEditor) {
            debugPalette  && console.error('_rearrangePalettes');
            var palettePerspectiveId,
                newEditorRightPaletteExpanded,
                newEditorLeftPaletteExpanded;

            //Determine what perspective to get palette info out of based on whether we have an editor or not
            if (newEditor) {
                // First, we will get the metadata for the extension and get its list of
                // palettes to bring to the top
                var editorExtensions = Runtime.getExtensions("davinci.editor", function (extension) {
                    return newEditor ? (extension.id === newEditor.editorID) : false;
                });
                if (editorExtensions && editorExtensions.length > 0) {
                    var editorExtension = editorExtensions[0];
                    palettePerspectiveId = editorExtension.palettePerspective;
                }

                //Remember if palettes had been expanded because as we add/remove/select tabs these values will be
                //altered and we'll want to restore them
                newEditorRightPaletteExpanded = newEditor._rightPaletteExpanded;
                newEditorLeftPaletteExpanded = newEditor._leftPaletteExpanded;
            } else {
                //No editor, so use the initital perspective
                palettePerspectiveId = Runtime.initialPerspective || "davinci.ui.main";
            }

            if (palettePerspectiveId) {
                var palettePerspective = Runtime.getExtension("davinci.perspective", palettePerspectiveId);
                if (!palettePerspective) {
                    //Runtime.handleError(dojo.string.substitute(webContent.perspectiveNotFound, [editorExtension.palettePerspective]));
                    console.error('---error');
                }
                var paletteDefs = palettePerspective.views;

                // Loop through palette ids and select appropriate palettes
                dojo.forEach(paletteDefs, function (paletteDef) {
                    // Look up the tab for the palette and get its
                    // parent to find the right TabContainer
                    var paletteId = paletteDef.viewID;
                    var position = paletteDef.position;
                    if (position.indexOf("bottom") < 0) {
                        position += "-top";
                    }
                    var tab = dijit.byId(paletteId);
                    if (tab) {
                        var tabContainer = tab.getParent();
                        var desiredTabContainer = mainBody.tabs.perspective[position];

                        //Move tab
                        if (tabContainer != desiredTabContainer) {
                            if (tabContainer) {
                                //Need to remove from the old tabbed container
                                tabContainer.removeChild(tab);
                            }
                            if (!paletteDef.hidden) {
                                desiredTabContainer.addChild(tab);
                                tabContainer = desiredTabContainer;
                            }
                        }

                        // Select/hide tab
                        if (tabContainer) {
                            if (paletteDef.hidden) {
                                tabContainer.removeChild(tab);
                            } else {
                                if (paletteDef.selected) {
                                    // This flag prevents Workbench.js logic from triggering expand/collapse
                                    // logic based on selectChild() event
                                    tabContainer._maqDontExpandCollapse = true;
                                    tabContainer.selectChild(tab);
                                    delete tabContainer._maqDontExpandCollapse;
                                }
                            }
                        }
                    }
                });
            }

            //Restore left/right palette expanded states that were saved earlier
            if (newEditor) {
                if (newEditor.hasOwnProperty("_rightPaletteExpanded")) {
                    newEditor._rightPaletteExpanded = newEditorRightPaletteExpanded;
                }
                if (newEditor.hasOwnProperty("_leftPaletteExpanded")) {
                    newEditor._leftPaletteExpanded = newEditorLeftPaletteExpanded;
                }
            }
        },
        _nearlyCollapsed: function (paletteContainerNode) {
            // Check actual width of palette area. If actual width is smaller than the
            // size of the tabs plus a small delta, then treat as if the palettes are collapsed
            debugPalette  && console.error('_nearlyCollapsed');
            var width = dojo.style(paletteContainerNode, 'width');
            if (typeof width == 'string') {
                width = parseInt(width);
            }
            return width < (paletteTabWidth + paletteTabDelta);
        },
        _expandCollapsePaletteContainer: function (tab) {
            debugPalette  && console.error('_expandCollapsePaletteContainer');
            if (!tab || !tab.domNode) {
                return;
            }
            var paletteContainerNode = davinci.Workbench.findPaletteContainerNode(tab.domNode);
            if (!paletteContainerNode.id) {
                return;
            }
            var expanded = paletteContainerNode._maqExpanded;
            var expandToSize;
            if (this._nearlyCollapsed(paletteContainerNode)) {
                expanded = false;
                expandToSize = (paletteCache[paletteContainerNode.id].expandToSize >= (paletteTabWidth + paletteTabDelta)) ?
                    paletteCache[paletteContainerNode.id].expandToSize : paletteCache[paletteContainerNode.id].initialExpandToSize;
            }
            if (expanded) {
                this.collapsePaletteContainer(paletteContainerNode);
            } else {
                this.expandPaletteContainer(paletteContainerNode, {expandToSize: expandToSize});
            }
        },
        _expandCollapsePaletteContainers: function (newEditor, params) {
            debugPalette  && console.error('_expandCollapsePaletteContainers');
            var leftBC = dijit.byId('left_mainBody');
            var rightBC = dijit.byId('right_mainBody');
            if (!newEditor) {
                if (leftBC) {
                    this.collapsePaletteContainer(leftBC.domNode, params);
                }
                if (rightBC) {
                    this.collapsePaletteContainer(rightBC.domNode, params);
                }
            } else {
                // First, we will get the metadata for the extension and get its list of
                // palettes to bring to the top
                var editorExtensions = Runtime.getExtensions("davinci.editor", function (extension) {
                    return extension.id === newEditor.editorID;
                });
                if (editorExtensions && editorExtensions.length > 0) {
                    var expandPalettes = editorExtensions[0].expandPalettes;
                    var expand;
                    if (leftBC) {
                        if (newEditor && newEditor.hasOwnProperty("_leftPaletteExpanded")) {
                            expand = newEditor._leftPaletteExpanded;
                        } else {
                            expand = (expandPalettes && expandPalettes.indexOf('left') >= 0);
                        }
                        if (expand) {
                            this.expandPaletteContainer(leftBC.domNode, params);
                        } else {
                            this.collapsePaletteContainer(leftBC.domNode, params);
                        }
                    }
                    if (rightBC) {
                        if (newEditor && newEditor.hasOwnProperty("_rightPaletteExpanded")) {
                            expand = newEditor._rightPaletteExpanded;
                        } else {
                            expand = (expandPalettes && expandPalettes.indexOf('right') >= 0);
                        }
                        if (expand) {
                            this.expandPaletteContainer(rightBC.domNode, params);
                        } else {
                            this.collapsePaletteContainer(rightBC.domNode, params);
                        }
                    }
                }

            }
        },

        /**
         * Look for the "palette container node" from node or one of its descendants,
         * where the palette container node id identified by its
         * having class 'davinciPaletteContainer'
         * @param {Element} node  reference node
         * @returns {Element|undefined}  the palette container node, if found
         */
        findPaletteContainerNode: function (node) {
            debugPalette  && console.error('findPaletteContainerNode');
            var paletteContainerNode;
            var n = node;
            while (n && n.tagName != 'BODY') {
                if (dojo.hasClass(n, 'davinciPaletteContainer')) {
                    paletteContainerNode = n;
                    break;
                }
                n = n.parentNode;
            }
            return paletteContainerNode;
        },

        /**
         * In response to clicking on palette's collapse button,
         * collapse all palettes within the given palette container node to just show tabs.
         * @param {Element} node  A descendant node of the palette container node.
         *        In practice, the node for the collapse icon (that the user has clicked).
         * @params {object} params
         *      params.dontPreserveWidth says to not cache current palette width
         */
        collapsePaletteContainer: function (node, params) {
            debugPalette  && console.error('collapsePaletteContainer');
            var paletteContainerNode = davinci.Workbench.findPaletteContainerNode(node);
            if (paletteContainerNode && paletteContainerNode.id) {
                var id = paletteContainerNode.id;
                var paletteContainerNodeWidth = dojo.style(paletteContainerNode, 'width');
                var paletteContainerWidget = dijit.byNode(paletteContainerNode);
                var tablistNodes = dojo.query('[role=tablist]', paletteContainerNode);
                if (paletteContainerWidget && tablistNodes.length > 0) {
                    var tablistNode = tablistNodes[0];
                    var tablistNodeSize = dojo.marginBox(tablistNode);
                    var parentWidget = paletteContainerWidget.getParent();
                    if (parentWidget && parentWidget.resize && tablistNodeSize && tablistNodeSize.w) {
                        if (!this._nearlyCollapsed(paletteContainerNode) && (!params || !params.dontPreserveWidth)) {
                            paletteCache[id].expandToSize = paletteContainerNodeWidth; // Note: just a number, no 'px' at end
                        }
                        paletteContainerNode.style.width = tablistNodeSize.w + 'px';
                        parentWidget.resize();
                        paletteContainerWidget._isCollapsed = true;
                    }
                }
                dojo.removeClass(paletteContainerNode, 'maqPaletteExpanded');
                paletteContainerNode._maqExpanded = false;
                davinci.Workbench._repositionFocusContainer();
                var currentEditor = Runtime.currentEditor;
                if (currentEditor) {
                    if (paletteContainerNode.id == 'left_mainBody') {
                        currentEditor._leftPaletteExpanded = false;
                    } else if (paletteContainerNode.id == 'right_mainBody') {
                        currentEditor._rightPaletteExpanded = false;
                    }
                }
            }
        },
        /**
         * In response to user clicking on one of the palette tabs,
         * see if the parent palette container node is collapsed.
         * If so, expand it.
         * @param {Element} node  A descendant node of the palette container node.
         *        In practice, the node for the collapse icon (that the user has clicked).
         * @param {object} params  A descendant node of the palette container node.
         *        params.expandToSize {number}  Desired width upon expansion
         */
        expandPaletteContainer: function (node, params) {
            debugPalette  && console.error('expandPaletteContainer');
            var expandToSize = params && params.expandToSize;
            var paletteContainerNode = davinci.Workbench.findPaletteContainerNode(node);
            if (paletteContainerNode && paletteContainerNode.id) {
                var id = paletteContainerNode.id;
                var paletteContainerWidget = dijit.byNode(paletteContainerNode);
                if (expandToSize) {
                    paletteCache[id].expandToSize = expandToSize;
                }
                if (paletteContainerWidget && paletteCache[id].expandToSize) {
                    var parentWidget = paletteContainerWidget.getParent();
                    if (parentWidget && parentWidget.resize) {
                        paletteContainerNode.style.width = paletteCache[id].expandToSize + 'px';
                        parentWidget.resize();
                        delete paletteContainerWidget._isCollapsed;
                    }
                }
                dojo.addClass(paletteContainerNode, 'maqPaletteExpanded');
                paletteContainerNode._maqExpanded = true;
                davinci.Workbench._repositionFocusContainer();
                var currentEditor = Runtime.currentEditor;
                if (currentEditor) {
                    if (paletteContainerNode.id == 'left_mainBody') {
                        currentEditor._leftPaletteExpanded = true;
                    } else if (paletteContainerNode.id == 'right_mainBody') {
                        currentEditor._rightPaletteExpanded = true;
                    }
                }
            }
        }

    };
    return dcl(null,WorkbenchImplementation);
});
