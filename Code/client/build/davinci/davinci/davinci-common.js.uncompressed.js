define("davinci/davinci-common", [
	"./version",
	"./repositoryinfo",
	"dijit/dijit",
	"dojo/parser",
	"dijit/form/Button",
	"dijit/form/DropDownButton",
	"dijit/form/Form",
	"dijit/form/TextBox",
	"dijit/form/ValidationTextBox"],
function(){}
);