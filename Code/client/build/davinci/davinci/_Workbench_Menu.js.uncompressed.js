define("davinci/_Workbench_Menu", [
    "dcl/dcl",
    "dojo/_base/declare",
    "./Runtime",
    "dijit/Menu",
    "dijit/MenuBar",
    "dijit/MenuItem",
    "dijit/MenuSeparator",
    "dijit/PopupMenuBarItem",
    "dijit/form/DropDownButton",
    "davinci/_WokbenchEditors",
    "davinci/_WokbenchMisc"
], function (dcl, declare,Runtime,
             Menu,
             MenuBar,
             MenuItem,
             MenuSeparator,
             PopupMenuBarItem,
             DropDownButton){


    var paletteTabWidth = 71;	// Width of tabs for left- and right-side palettes
    var paletteTabDelta = 20;	// #pixels - if this many or fewer pixels of tab are showing, treat as collapsed
    var paletteCache = {};
    var debugActions = false;
    var debugMenu = false;
    
    function getWorkbench(){
        return davinci.Workbench;
    }

    var PopupMenu = declare(Menu, {
        menuOpened: function (event) {
        },
        _openMyself: function (event) {
            this.menuOpened(event);
            var open;
            try {
                // Create a DIV that will overlay entire app and capture events that might go to interior iframes
                var menuOverlayDiv = document.getElementById('menuOverlayDiv');
                if (!menuOverlayDiv) {
                    menuOverlayDiv = dojo.create('div', {
                        id: 'menuOverlayDiv',
                        style: 'left:0px; top:0px; width:100%; height:100%; position:absolute; z-index:10;'
                    }, document.body);
                }
                if (this.adjustPosition) {
                    var offsetPosition = this.adjustPosition(event);
                    open = dijit.popup.open;
                    dijit.popup.open = function (args) {
                        args.x += offsetPosition.x;
                        args.y += offsetPosition.y;
                        open.call(dijit.popup, args);
                    };
                }
                this.onClose = function () {
                    var menuOverlayDiv = document.getElementById('menuOverlayDiv');
                    if (menuOverlayDiv) {
                        menuOverlayDiv.parentNode.removeChild(menuOverlayDiv);
                    }
                }.bind(this);
                this.inherited(arguments);
            } finally {
                if (open) {
                    dijit.popup.open = open;
                }
            }
        }
    });

    
    var WorkbenchImplementation = {
        declaredClass:"davinci.Workbench_Menu",
        updateMenubar: function (node, actionSets) {
            console.error('updateMenubar');
            /*
            return ;
            var menuTree = Workbench._createMenuTree(actionSets);

            var menuTop = dijit.byId(node.id);
            if (!menuTop) {
                menuTop = new MenuBar({'class': 'dijitInline'}, node);
            }
            Workbench._addItemsToMenubar(menuTree, menuTop);
            */
        },
        _updateMainMenubar: function (menuDiv, menuTree) {
            console.error('_updateMainMenubar');

            return ;
            /*
            for (var i = 0; i < menuTree.length; i++) {
                var menuTreeItem = menuTree[i];
                for (var j = 0; j < menuTreeItem.menus.length; j++) {
                    var menu = menuTreeItem.menus[j];
                    var menuWidget = Workbench._createMenu(menu);
                    menu.id = menu.id.replace(".", "-"); // kludge to work around the fact that '.' is being used for ids, and that's not compatible with CSS
                    // Set up top banner region. (Top banner is an extensibility point)
                    if (window.maqetta && maqetta.TopBanner && maqetta.TopBanner.attachMenu) {
                        maqetta.TopBanner.attachMenu(menu, menuWidget, menuDiv);
                    } else {
                        var widget = dijit.byId(menu.id + "-dropdown");
                        if (!widget) {
                            var params = {label: menu.label, dropDown: menuWidget, id: menu.id + "-dropdown"};
                            if (menu.hasOwnProperty('showLabel')) {
                                params.showLabel = menu.showLabel;
                            }
                            if (menu.hasOwnProperty('iconClass')) {
                                params.iconClass = menu.iconClass;
                            }
                            if (menu.hasOwnProperty('className')) {
                                params['class'] = menu.className;
                            }
                            widget = new DropDownButton(params);
                            menuDiv.appendChild(widget.domNode);
                        }
                    }
                }
            }
            */
        },
        _addItemsToMenubar: function (menuTree, menuTop) {
            console.error('_addItemsToMenubar');
            return ;
            /*
            dojo.forEach(menuTree, function (m) {
                var menus = m.menus,
                    menuLen = menus.length;
                if (menuLen) {
                    dojo.forEach (menus, function (menu) {
                        menu.id = menu.id.replace(/\./g, "-"); // kludge to work around the fact that '.' is being used for ids, and that's not compatible with CSS
                        var menuWidget = Workbench._createMenu(menu),
                            widget = dijit.byId(menu.id + "-dropdown");
                        if (!widget) {
                            widget = new PopupMenuBarItem({
                                label: menu.label,
                                popup: menuWidget,
                                id: menu.id + "-dropdown"
                            });
                        }
                        menuTop.addChild(widget);
                    }, this);
                }
            }, this);
            */
        },
        _createMenu: function (menu, context) {
            debugMenu && console.log('create popup menu');
            //return ;
            var menuWidget, menus, connectFunction;
            if (menu.menus) {  // creating dropdown
                menuWidget = new Menu({parentMenu: menu});
                menus = menu.menus;
                connectFunction = "onOpen";
            } else {	// creating popup
                menuWidget = new PopupMenu({});
                menus = menu;
                connectFunction = "menuOpened";
            }

            menuWidget.domNode.style.display = "none";
            menuWidget.actionContext = context;
            this._rebuildMenu(menuWidget, menus);
            dojo.connect(menuWidget, connectFunction, this, function (evt) {
                if (menuWidget._widgetCallback) { // create popup
                    menuWidget._widgetCallback(evt);
                }
                this._rebuildMenu(menuWidget, menus).focus(); // call focus again, now that we messed with the widget contents
            });

            return menuWidget;

        },
        _rebuildMenu: function (menuWidget, menus) {
            debugMenu && console.log('_rebuildMenu');
            
            dojo.forEach(menuWidget.getChildren(), function (child) {
                menuWidget.removeChild(child);
                child.destroy();
            });
            menuWidget.focusedChild = null; // TODO: dijit.Menu bug?  Removing a focused child should probably reset focusedChild for us

            var addSeparator, menuAdded;
            menus.forEach(function (menu, i) {
                if (menu.menus.length) {
                    if (menu.isSeparator && i > 0) {
                        addSeparator = true;
                    }
                    menu.menus.forEach(function (item) {
                        if (addSeparator && menuAdded) {
                            menuWidget.addChild(new MenuSeparator({}));
                            addSeparator = false;
                        }
                        menuAdded = true;
                        var label = item.label;
                        if (item.action && item.action.getName) {
                            label = item.action.getName();
                        }


                        if (item.separator) {
                            var subMenu = getWorkbench()._createMenu(item);
                            var popupParent = new MenuItem({
                                label: label,
                                popup: subMenu,
                                id: subMenu.id + "item"
                            });
                            popupParent.actionContext = menuWidget.actionContext;
                            menuWidget.addChild(popupParent);
                        } else {
                            var enabled = true;
                            if (item.isEnabled) {
                                var selection = Runtime.getSelection();
                                var resource = selection[0] && selection[0].resource;
                                enabled = resource ? item.isEnabled(resource) : false;
                            }


                            if (item.action) {

                                if (item.action.shouldShow && !item.action.shouldShow(menuWidget.actionContext, {menu: menuWidget})) {
                                    return;
                                }
                                //FIXME: study this code for bugs.
                                //menuWidget.actionContext: is that always the current context?
                                //There were other bugs where framework objects pointed to wrong context/doc
                                enabled = item.action.isEnabled && item.action.isEnabled(menuWidget.actionContext);
                            }
                            //enabled=true;
                            var menuArgs = {
                                label: label,
                                id: item.id,
                                disabled: !enabled,
                                onClick: dojo.hitch(this, "_runAction", item, menuWidget.actionContext)
                            };
                            if (item.iconClass) {
                                menuArgs.iconClass = item.iconClass;
                            }

                            menuWidget.addChild(new MenuItem(menuArgs));
                        }
                    }, this);
                }
            }, this);

            return menuWidget;
        },
        _createMenuTree: function (actionSets, pathsOptional) {
            //return ;
            debugMenu && console.log('_createMenuTree',actionSets);
            if (!actionSets) {  // only get action sets not associated with part
                actionSets = Runtime.getExtensions("davinci.actionSets", function (actionSet) {
                    var associations = Runtime.getExtensions("davinci.actionSetPartAssociations", function (actionSetPartAssociation) {
                        return actionSetPartAssociation.targetID == actionSet.id;
                    });
                    return associations.length == 0;
                });
            }
            var menuTree = [];

            function findID(m, id) { //ALP: dijit.byId?
                for (var j = 0, jLen = m.length; j < jLen; j++) {
                    for (var k = 0, kLen = m[j].menus.length; k < kLen; k++) {
                        if (id == m[j].menus[k].id) {
                            return m[j].menus[k].menus;
                        }
                    }
                }
            }

            function addItem(item, path, pathsOptional) {
                path = path || "additions";
                path = path.split('/');
                var m = menuTree;

                getWorkbench()._loadActionClass(item);

                var sep = path[path.length - 1];
                if (path.length > 1) {
                    for (var i = 0, len = path.length - 1; i < len; i++) {
                        var k = findID(m, path[i]);
                        if (k) {
                            m = k;
                        }
                    }
                }
                for (var i = 0, len = m.length; i < len; i++) {
                    if (m[i].id == sep) {
                        var menus = m[i].menus;
                        menus.push(item);
                        if (item.separator) { // if menu
                            var wasAdditions = false;
                            menus = item.menus = [];
                            for (var j = 0; j < item.separator.length; j += 2) {
                                var id = item.separator[j];

                                wasAdditions = id == "additions";
                                menus.push({
                                    id: id,
                                    isSeparator: item.separator[j + 1],
                                    menus: []
                                });
                            }
                            if (!wasAdditions) {
                                menus.push({
                                    id: "additions",
                                    isSeparator: false,
                                    menus: []
                                });
                            }
                        }
                        return;
                    }
                }
                if (pathsOptional) {
                    menuTree.push({
                        id: sep,
                        isSeparator: false,
                        menus: [item]
                    });
                }
            }


            for (var actionSetN = 0, len = actionSets.length; actionSetN < len; actionSetN++) {
                var actionSet = actionSets[actionSetN];
                if (actionSet.visible) {
                    if (actionSet.menu) {
                        for (var menuN = 0, menuLen = actionSet.menu.length; menuN < menuLen; menuN++) {
                            var menu = actionSet.menu[menuN];
                            if (menu.__mainMenu) {
                                for (var j = 0; j < menu.separator.length; j += 2) {
                                    menuTree.push({
                                        id: menu.separator[j],
                                        isSeparator: menu.separator[j + 1],
                                        menus: []
                                    });
                                }
                            } else {
                                addItem(menu, menu.path, pathsOptional);
                                if (menu.populate instanceof Function) {
                                    var menuItems = menu.populate();
                                    for (var item in menuItems) {
                                        addItem(menuItems[item], menuItems[item].menubarPath);
                                    }
                                }

                            }
                        }
                    }
                }
            }

            for (var actionSetN = 0, len = actionSets.length; actionSetN < len; actionSetN++) {
                var actionSet = actionSets[actionSetN];
                if (actionSet.visible) {
                    for (var actionN = 0, actionLen = actionSet.actions.length; actionN < actionLen; actionN++) {
                        var action = actionSet.actions[actionN];
                        if (action.menubarPath) {
                            addItem(action, action.menubarPath, pathsOptional);
                        }
                    }
                }
            }

            return menuTree;

        }
    };


    return dcl(null,WorkbenchImplementation);
});
