define("davinci/Workbench", [
    "dcl/dcl",
    "dojo/_base/lang",
    "require",
    "./Runtime",
    "./ui/Dialog",
    "./ve/metadata",
    "./ve/utils/GeomUtils",
    "davinci/_WokbenchEditors",
    "davinci/_WokbenchMisc",
    "davinci/_Workbench_Menu",
    "davinci/_Workbench_Palette",
    'xide/widgets/ContextMenu',
    'xide/utils',
    'xide/types'
], function (dcl, lang, require, Runtime,
    Dialog,
    metadata,
    GeomUtils,
    _WokbenchEditors,
    _WokbenchMisc,
    _Workbench_Menu, _Workbench_Palette,
    ContextMenu, utils, types) {

    var debugActions = false;
    var debugMenu = false;

    // Cheap polyfill to approximate bind(), make Safari happy
    Function.prototype.bind = Function.prototype.bind || function (that) {
        return dojo.hitch(that, this);
    };

    var WorkbenchImplementation = {
        declaredClass: 'davinci.Workbench',
        _XX_last_member: true, // dummy with no trailing ','
        activePerspective: "",
        actionScope: [],
        _DEFAULT_PROJECT: "project1",
        _baseTitle: "Control-Freak ",
        hideEditorTabs: true,
        _editorTabClosing: {},
        _shadowTabClosing: {},
        service: null,
        serviceClass: 'XApp_XIDE_Workbench_Service',
        currentEditor: null,
        run2: function () {
            Runtime.subscribe("/davinci/ui/widgetPropertiesChanges",
                function () {
                    var ve = Runtime.currentEditor.visualEditor;
                    ve._objectPropertiesChange.apply(ve, arguments);
                }
            );
        },
        onResize: function (e) {
            var target = e.explicitOriginalTarget ? e.explicitOriginalTarget : e.srcElement;
            if (e.type == 'resize' || ((target.id && (target.id.indexOf('dijit_layout__Splitter_') > -1) ||
                    (target.nextSibling && target.nextSibling.id && target.nextSibling.id.indexOf('dijit_layout__Splitter_') > -1)))) {
                var ed = davinci && Runtime.currentEditor;
                if (ed && ed.onResize) {
                    ed.onResize();
                }
            }
            if (Workbench._originalOnResize) {
                Workbench._originalOnResize();
            }
            Workbench._repositionFocusContainer();
        },
        /* returns either the active editor, or the editor with given resource open */
        showModal: function (content, title, style, callback, submitOnEnter, onShow) {
            return Dialog.showModal(content, title, style, callback, submitOnEnter, onShow);
        },

        // simple dialog with an automatic OK button that closes it.
        showMessage: function (title, message, style, callback, submitOnEnter) {
            return Dialog.showMessage(title, message, style, callback, submitOnEnter);
        },

        // OK/Cancel dialog with a settable okLabel
        showDialog: function (params) {
            return Dialog.showDialog(params);
        },
        _loadActionClass: function (item) {
            debugActions &&  0 && console.log('_loadActionClass', item);
            if (item && typeof item.action == "string") {
                require([item.action], function (ActionClass) {

                    if (item.action === 'maq-metadata-html/html/table/SelectTableAction') {
                        /*debugger;*/
                    }

                    /* 0 && console.log('loaded action class : ' + item.action);*/
                    try {
                        if (ActionClass) {
                            item.action = new ActionClass();
                            item.action.item = item;
                        } else {
                            debugger;
                        }
                    } catch (e) {
                        console.error('action class creation failed : ' + item.action);
                    }
                });
            }
        },
        _toggleButton: function (button, context, group, arg) {
            if (!button.checked) {
                return;
            }
            group.forEach(function (item) {
                if (item != button) {
                    item.set('checked', false);
                }
            });
            Workbench._runAction(button.item, context, button.item.id);
        },
        //FIXME: "context" is really an editor, isn't it? Like davinci.ve.PageEditor?
        _runAction: function (item, context, arg) {

            // 0 && console.log('-run action',item);
            //FIXME: Not sure this code is correct, but sometimes this routine is passed
            //a context object that is not associated with the current document
            if (context && Runtime.currentEditor) {
                context = Runtime.currentEditor;
            }
            if (item.run) {
                item.run();
            } else if (item.action) {
                if (dojo.isString(item.action)) {
                    this._loadActionClass(item);
                }
                item.action.run(context);
            } else if (item.method && context && context[item.method] instanceof Function) {
                context[item.method](arg);
            } else if (item.commandID) {
                Runtime.executeCommand(item.commandID);
            }
        },
        createPopup: function (args) {
            var partID = args.partID,
                domNode = args.domNode,
                context = args.context,
                widgetCallback = args.openCallback;

            function _createContextMenu(node) {
                var _ctorArgs = this.contextMenuArgs || {};
                var mixin = {
                    owner: this,
                    delegate: this,
                    actionFilter: {
                        quick: true
                    }
                };
                utils.mixin(_ctorArgs, mixin);
                var contextMenu = new ContextMenu(_ctorArgs, node);
                contextMenu.openTarget = node;
                contextMenu.init({
                    preventDoubleContext: false
                });
                contextMenu._registerActionEmitter(this);
                $(node).one('contextmenu', function (e) {
                    e.preventDefault();
                    if (!this.store) {
                        contextMenu.setActionStore(this.getActionStore(), this);
                    }
                }.bind(this));

                this.contextMenu = contextMenu;
            }

            if (partID === 'davinci.ve.visualEditor') {
                if (!context.cMenu) {
                    context.cMenu = _createContextMenu.apply(context.getVisualEditor(), [domNode]);
                    context.getVisualEditor().add(context.cMenu);
                }
                return;
            }


            var o = this.getActionSets(partID);
            var clonedActionSets = o.clonedActionSets;
            var actionSets = o.actionSets;

            try {
                if (clonedActionSets.length > 0) {
                    var menuTree = Workbench._createMenuTree(clonedActionSets, true);

                    Workbench._initActionsKeys(actionSets, args);

                    debugMenu &&  0 && console.log('  action sets', [actionSets, menuTree]);
                    //return;
                    var popup = Workbench._createMenu(menuTree, context);
                    if (popup && domNode) {
                        popup.bindDomNode(domNode);
                    }
                    popup._widgetCallback = widgetCallback;
                    popup._partID = partID;
                    return popup;
                }
            } catch (e) {
                logError(e, 'creating popup');
            }
        },
        getActionSets: function (partID) {
            debugActions &&  0 && console.log('action sets', partID);
            var actionSetIDs = [];
            Runtime.getExtension("davinci.actionSetPartAssociations", function (extension) {
                return extension.parts.some(function (part) {
                    if (part == partID) {
                        actionSetIDs.push(extension.targetID);
                        return true;
                    }
                });
            });

            var actionSets;
            var clonedActionSets = [];
            if (actionSetIDs.length) {
                actionSets = Runtime.getExtensions("davinci.actionSets", function (extension) {
                    return actionSetIDs.some(function (setID) {
                        return setID == extension.id;
                    });
                });
                if (actionSets.length) {
                    // Determine if any widget libraries have indicated they want to augment the actions in
                    // the action set
                    actionSets.forEach(function (actionSet) {
                        var libraryActions = metadata.getLibraryActions(actionSet.id);
                        if (libraryActions.length) {
                            // We want to augment the action list, so let's copy the
                            // action set before pushing new items onto the end of the
                            // array.
                            actionSet = lang.mixin({}, actionSet); // shallow obj copy
                            actionSet.actions = actionSet.actions.concat(libraryActions); // copy array, add libraryActions
                        }
                        clonedActionSets.push(actionSet);
                    });
                }
            }
            return {
                actionSets: actionSets,
                clonedActionSets: clonedActionSets
            };
        },
        getFocusContainer: function () {
            var _c = document.getElementById('focusContainer');
            if (!_c) {
                _c = dojo.create('div', {
                    'class': 'focusContainer',
                    id: 'focusContainer'
                }, document.body);
                davinci.Workbench.focusContainer = _c;
            }
            return _c;
        },
        /**
         * Reposition the focusContainer node to align exactly with the position of editors_container node
         */
        _repositionFocusContainer: function () {
            var editors_container = dojo.byId('editorsStackContainer');

            var focusContainer = this.getFocusContainer();

            if (editors_container && focusContainer) {
                var currentEditor = Runtime.currentEditor;
                var box;
                if (currentEditor && currentEditor.getFocusContainerBounds) {
                    box = currentEditor.getFocusContainerBounds();
                } else {
                    box = GeomUtils.getBorderBoxPageCoords(editors_container);
                }
                if (box) {
                    focusContainer.style.left = box.l + 'px';
                    focusContainer.style.top = box.t + 'px';
                    focusContainer.style.width = box.w + 'px';
                    focusContainer.style.height = box.h + 'px';
                    if (currentEditor && currentEditor.getContext) {
                        var context = currentEditor.getContext();
                        if (context && context.updateFocusAll) {
                            context.updateFocusAll();
                        }
                    }
                }
            }
        },
        _removeFocusContainerChildren: function () {
            if (davinci.Workbench.focusContainer) {
                davinci.Workbench.focusContainer.innerHTML = '';
            }

        }
    };




    // put workbench state upload on a timer to reduce number of requests to the server
    window.setInterval(function () {
        if (Workbench.saveState) {
            Workbench.updateWorkbenchState();
        }
    }, 5000);

    var WorkbenchClass = dcl([_WokbenchEditors, _WokbenchMisc, _Workbench_Menu, _Workbench_Palette], WorkbenchImplementation);

    var Workbench = new WorkbenchClass();

    dojo.setObject("davinci.Workbench", Workbench);
    //dojo.setObject("davinci.Workbench",WorkbenchInstance);
    return Workbench;
});