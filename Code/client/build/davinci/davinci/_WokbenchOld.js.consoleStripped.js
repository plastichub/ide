define("davinci/_WokbenchOld", [
    "dcl/dcl",
    "dojo/_base/lang",
    "require",
    "./Runtime",
    "./model/Path",
    "./workbench/ViewPart",
    "./workbench/EditorContainer",
    "./ui/Dialog",
    "dijit/Toolbar",
    "dijit/ToolbarSeparator",
    "dijit/Menu",
    "dijit/MenuBar",
    "dijit/MenuItem",
    "dijit/MenuSeparator",
    "dijit/PopupMenuBarItem",
    "dijit/form/Button",
    "dijit/form/DropDownButton",
    "dijit/form/ComboButton",
    "dijit/form/ToggleButton",
    "dijit/layout/BorderContainer",
    "dijit/layout/StackController",
    "dijit/layout/StackContainer",
    "dijit/layout/ContentPane",
    "dijit/layout/TabController",
    "dijit/layout/TabContainer",
    "system/resource",
    "davinci/lang/webContent",
    "./ve/metadata",
    "dojo/Deferred",
    "dojo/promise/all",
    "dojo/_base/declare",
    "dojo/_base/connect",
    "dojo/_base/xhr",
    "./review/model/resource/root",
    "davinci/lang/ve/common",
    "./ve/utils/GeomUtils",
    "davinci/lang/ui/common",
    "davinci/review/model/resource/root",
    "xide/manager/ServerActionBase"
], function(
    dcl,
    lang,
    require,
    Runtime,
    Path,
    ViewPart,
    EditorContainer,
    Dialog,
    Toolbar,
    ToolbarSeparator,
    Menu,
    MenuBar,
    MenuItem,
    MenuSeparator,
    PopupMenuBarItem,
    Button,
    DropDownButton,
    ComboButton,
    ToggleButton,
    BorderContainer,
    StackController,
    StackContainer,
    ContentPane,
    TabController,
    TabContainer,
    sysResource,
    webContent,
    metadata,
    Deferred,
    all,
    declare,
    connect,
    xhr,
    reviewResource,
    veNLS,
    GeomUtils,
    uiCommonNls,
    revResource,
    ServerActionBase
) {


    var Module = dcl(null,{
        showPerspective: function (perspectiveID) {
             0 && console.log('showPerspective');
            Workbench.activePerspective = perspectiveID;
            var menuTree = Workbench._createMenuTree();	// no params means include "everything else"
            Workbench._updateMainMenubar(dojo.byId('davinci_main_menu'), menuTree);

            var o = this.getActionSets('davinci.ui.editorMenuBar');
            var clonedActionSets = o.clonedActionSets;
            if (clonedActionSets.length) {
                menuTree = Workbench._createMenuTree(clonedActionSets);
                Workbench._updateMainMenubar(dojo.byId('maq_banner_editor_commands'), menuTree);
            }

            var mainBody = dojo.byId('mainBody');
            if (!mainBody.tabs) {
                mainBody.tabs = [];
            }

            /* Large border container for the entire page */
            var mainBodyContainer = dijit.byId('mainBody');

            if (!mainBodyContainer) {
                mainBodyContainer = new BorderContainer({
                    gutters: false,
                    region: "center",
                    design: 'sidebar'
                }, mainBody);
            }

            var perspective = Runtime.getExtension("davinci.perspective", perspectiveID);

            if (!perspective) {
                Runtime.handleError(dojo.string.substitute(webContent.perspectiveNotFound, [perspectiveID]));
            }

            perspective = dojo.clone(perspective);	// clone so views aren't added to original definition

            var extensions = Runtime.getExtensions("davinci.perspectiveExtension",
                function (extension) {
                    return extension.targetID === perspectiveID;
                });
            dojo.forEach(extensions, function (extension) {
                // TODO: should check if view is already in perspective. filter + concat instead of foreach + push?
                dojo.forEach(extension.views, function (view) {
                    perspective.views.push(view);
                });
            });

            if (!mainBody.editorsStackContainer) {
                Workbench.editorsStackContainer = mainBody.editorsStackContainer =
                    new StackContainer({
                        region: 'center',
                        id: "editorsStackContainer",
                        controllerWidget: "dijit.layout.StackController"
                    });
            }
            // FIXME: THIS BYPASSES THE PLUGIN SYSTEM.
            // Hardcoding this for now. Need to figure out how to turn change
            // welcome page logic into something that is defined by ve_plugin.js.
            mainBodyContainer.addChild(mainBody.editorsStackContainer);
            if (!mainBody.editorsWelcomePage) {
                Workbench.editorsWelcomePage = mainBody.editorsWelcomePage =
                    new ContentPane({
                        id: "editorsWelcomePage",
                        href: "app/davinci/ve/resources/welcome_to_maqetta.html"
                    });
            }
            mainBody.editorsStackContainer.addChild(mainBody.editorsWelcomePage);
            if (!mainBody.tabs.editors) {
                Workbench.editorTabs = mainBody.tabs.editors =
                    new (Workbench.hideEditorTabs ? StackContainer : TabContainer)({
                        id: "editors_container",
                        controllerWidget: (Workbench.hideEditorTabs ? "dijit.layout.StackController" : "dijit.layout.TabController")
                    });
                Workbench.editorTabs.setTitle = function (editorContainer, title) {
                    editorContainer.attr('title', title);
                    // After letting Dijit put the title onto the ContentPane,
                    // force title to null string on the domNode so that the
                    // browser doesn't show an annoying tooltip while hovering
                    // over an editor.
                    editorContainer.domNode.title = '';
                    if (!Workbench.hideEditorTabs) {
                        this.tablist.pane2button[editorContainer.id].attr('label', title);
                    } else {
                        var editorId = editorContainer.id;
                        var shadowId = editorIdToShadowId(editorId);
                        var shadowTabContainer = dijit.byId("davinci_file_tabs");
                        var titleWithDirty = title + (editorContainer.isDirty ? '<span class="dirtyFileAsterisk"></span>' : '');
                        shadowTabContainer.tablist.pane2button[shadowId].attr('label', titleWithDirty);
                    }
                };

                dojo.connect(mainBody.tabs.editors, "removeChild", this, Workbench._editorTabClosed);
            }
            mainBody.editorsStackContainer.addChild(mainBody.tabs.editors);
            mainBody.editorsStackContainer.selectChild(mainBody.editorsWelcomePage);
            dojo.connect(dijit.byId("editors_container"), "selectChild", function (child) {
                if (!Workbench._processingSelectChild) {
                    Workbench._processingSelectChild = true;
                    var editorId = child.id;
                    var shadowId = editorIdToShadowId(editorId);
                    var shadowTab = dijit.byId(shadowId);
                    var shadowTabContainer = dijit.byId("davinci_file_tabs");
                    if (shadowTab && shadowTabContainer) {
                        shadowTabContainer.selectChild(shadowTab);
                    }
                    if (child.editor) {
                        Workbench._switchEditor(child.editor);
                    }
                    Workbench._processingSelectChild = false;
                }
            });
            mainBodyContainer.startup();

            // Put the toolbar and the main window in a border container
            var appBorderContainer = dijit.byId('davinci_app');
            if (!appBorderContainer) {
                appBorderContainer = new BorderContainer({
                    design: "headline",
                    gutters: false,
                    liveSplitters: false
                }, "davinci_app");

                var topBarPane = new ContentPane({
                    region: "top",
                    layoutPriority: 1
                }, "davinci_top_bar");

                var mainStackContainer = Workbench.mainStackContainer = mainBody.editorsStackContainer =
                    new StackContainer({
                        region: 'center',
                        id: "mainStackContainer",
                        controllerWidget: "dijit.layout.StackController"
                    });

                var mainBorderContainer = Workbench.mainBorderContainer = new BorderContainer({
                    design: "headline",
                    gutters: false,
                    id: 'mainBorderContainer',
                    liveSplitters: false
                });

                var shadowTabContainer = Workbench.shadowTabs = new TabContainer({
                    id: 'davinci_file_tabs',
                    closable: true,
                    region: "top",
                    layoutPriority: 1,
                    style: 'display:none'
                });

                Workbench.shadowTabs.setTitle = function (tab, title) {
                    tab.attr('title', title);
                    this.tablist.pane2button[tab.id].attr('label', title);
                };
                dojo.connect(shadowTabContainer, "selectChild", function (child) {
                    var shadowId = child.id;
                    var editorId = shadowIdToEditorId(shadowId);
                    var editorContainer = dijit.byId(editorId);
                    var editorsContainer = dijit.byId("editors_container");
                    if (editorsContainer && editorContainer && editorContainer.editor) {
                        // This is trigger (indirectly) the selectChild callback function on
                        // the editors_container widget, which will trigger Workbench._switchEditor
                        editorsContainer.selectChild(editorContainer);
                    }
                });
                dojo.connect(shadowTabContainer, "removeChild", this, Workbench._shadowTabClosed);
                var toolbarPane = new ContentPane({
                    id: 'davinci_toolbar_pane',
                    region: "top",
                    layoutPriority: 1,
                    content: '<div id="davinci_toolbar_container"></div>',
                    style: 'display:none'
                });

                appBorderContainer.addChild(topBarPane);
                appBorderContainer.addChild(mainStackContainer);
                mainStackContainer.addChild(mainBorderContainer);
                mainStackContainer.selectChild(mainBorderContainer);

                mainBorderContainer.addChild(shadowTabContainer);
                mainBorderContainer.addChild(toolbarPane);
                mainBorderContainer.addChild(mainBodyContainer);
                appBorderContainer.layout();
                appBorderContainer.startup();
                Workbench._originalOnResize = window.onresize;
                window.onresize = Workbench.onResize; //alert("All done");}
                dojo.connect(mainBodyContainer, 'onMouseUp', this, 'onResize');

                var shadowTabMenu = dijit.byId('davinci_file_tabs_tablist_Menu');
                if (shadowTabMenu) {
                    shadowTabMenu.addChild(new dijit.MenuItem({
                        label: veNLS.closeAllEditors,
                        onClick: this.closeAllEditors
                    }));
                }
            }
            /* close all of the old views */
            for (var position in mainBody.tabs.perspective) {
                var view = mainBody.tabs.perspective[position];
                if (!view) {
                    continue;
                }
                dojo.forEach(view.getChildren(), function (child) {
                    view.removeChild(child);
                    if (position != 'left' && position != 'right') {
                        child.destroyRecursive(false);
                    }
                });
                view.destroyRecursive(false);
                delete mainBody.tabs.perspective[position];
            }

            this._showViewPromises = dojo.map(perspective.views, function (view) {
                return Workbench.showView(view.viewID, view.selected, view.hidden);
            }, this);

            //FIXME: This is also ugly - creating a special DIV for visual editor's selection chrome
            //Note sure how best to factor this out, though.
            davinci.Workbench.focusContainer = dojo.create('div', {
                'class': 'focusContainer',
                id: 'focusContainer'
            }, document.body);

            // kludge to workaround problem where tabs are sometimes cutoff/shifted to the left in Chrome for Mac
            // would be nice if we had a workbench onload event that we could attach this to instead of relying on a timeout
            setTimeout(function () {
                appBorderContainer.resize();
                dojo.publish("/davinci/workbench/ready", []);
            }.bind(this), 3000);
        },
        _createEditor: function(editorExtension, fileName, keywordArgs, newHtmlParams) {

            var d = new Deferred();
            var nodeName = fileName.split('/').pop();
            var extension = keywordArgs && keywordArgs.fileName && keywordArgs.fileName.extension ?
            "." + keywordArgs.fileName.extension : "";
            nodeName += (extension == ".rev" ? extension : "");

            dojo.query('.loading').orphan();

            var editorsStackContainer = dijit.byId('editorsStackContainer'),
                editors_container = dijit.byId('editors_container');
            if (editorsStackContainer && editors_container) {
                editorsStackContainer.selectChild(editors_container);
                Workbench.mainStackContainer.selectChild(Workbench.mainBorderContainer);
            }

            var editorContainer = dijit.byId(filename2id(fileName)),
                editorsContainer = dijit.byId("editors_container"),
                shadowTabContainer = dijit.byId("davinci_file_tabs"),
                editorCreated = false,
                shadowTab = null;
            if (!editorContainer) {
                editorCreated = true;

                var editorId = filename2id(fileName);
                var shadowId = editorIdToShadowId(editorId);
                editorContainer = new EditorContainer({
                    title: nodeName,
                    id: editorId,
                    'class': "EditorContainer",
                    isDirty: keywordArgs.isDirty
                });
                shadowTab = new ContentPane({
                    title:nodeName,
                    closable: true,
                    id:shadowId
                });
                shadowTab.onClose = function(tc, tab){

                    var shadowId = tab.id;
                    var editorId = shadowIdToEditorId(shadowId);
                    var editorContainer = dijit.byId(editorId);
                    var editorsContainer = dijit.byId("editors_container");
                    function okToClose(){
                        editorContainer._skipDirtyCheck = true;
                        editorContainer.onClose.apply(editorContainer, [editorsContainer, editorContainer]);
                        tc.removeChild(tab);
                        tab.destroyRecursive();
                    }
                    function saveAndClose(){
                        editorContainer.editor.save();
                        okToClose();
                    }
                    if(editorsContainer && editorContainer){
                        if (editorContainer.editor.isDirty){
                            //Give editor a chance to give us a more specific message
                            var message = editorContainer.editor.getOnUnloadWarningMessage();
                            if (!message) {
                                //No editor-specific message, so use our canned one
                                message = dojo.string.substitute(webContent.fileHasUnsavedChanges, [editorContainer._getTitle()]);
                            }
                            Workbench.showDialog({
                                title: editorContainer._getTitle(),
                                content: message,
                                style: {width: 300},
                                okLabel: uiCommonNls.save,
                                okCallback: dojo.hitch(this,saveAndClose),
                                hideLabel: null,
                                submitOnEnter:true,
                                extendLabels: [uiCommonNls.discard],
                                extendCallbacks: [dojo.hitch(this,okToClose)]
                            });
                        } else {
                            okToClose();
                        }
                    }
                };
            }

            if (!editorExtension) {
                editorExtension = {
                    editorClass: 'davinci/ui/TextEditor',
                    id: 'davinci.ui.TextEditor'
                };
            }

            if (editorCreated) {
                editorsContainer.addChild(editorContainer);
                shadowTabContainer.addChild(shadowTab);
            }

            // add loading spinner
            if(!Workbench.hideEditorTabs){
                var loadIcon = dojo.query('.dijitTabButtonIcon',editorContainer.controlButton.domNode);
                dojo.addClass(loadIcon[0],'tabButtonLoadingIcon');
                dojo.removeClass(loadIcon[0],'dijitNoIcon');
            }

            if (!keywordArgs.noSelect) {
                editorsContainer.selectChild(editorContainer);
            }
            //FIXME: this is very kludgy. At initialization time, we want EditorContainer.js
            //to filter past all editors except the current filename to prevent the cs=null issue #3279.
            //But when not at initialization time, we need to make sure the
            //current activeEditor is set to the "fileName".
            if(!keywordArgs.initializationTime){
                Workbench._state.activeEditor = fileName;
            }
            editorContainer.setEditor(editorExtension, fileName, true, keywordArgs.fileName, editorContainer.domNode, newHtmlParams,keywordArgs.item).then(function(editor) {
                if (keywordArgs.startLine) {
                    editorContainer.editor.select(keywordArgs);
                }

                if (!keywordArgs.noSelect) {
                    if (Workbench._state.editors.indexOf(fileName) === -1) {
                        Workbench._state.editors.push(fileName);
                    }
                    Workbench._switchEditor(editorContainer.editor, keywordArgs.startup);
                }

                if(!Workbench.hideEditorTabs){
                    dojo.removeClass(loadIcon[0],'tabButtonLoadingIcon');
                    dojo.addClass(loadIcon[0],'dijitNoIcon');
                }

                setTimeout(function() {
                    editorContainer.resize(); //kludge, forces editor to correct size, delayed to force contents to redraw
                }, 100);
                d.resolve(editorContainer.editor);
            }, function(error) {
                if(!Workbench.hideEditorTabs){
                    dojo.removeClass(loadIcon[0],'tabButtonLoadingIcon');
                    dojo.addClass(loadIcon[0],'tabButtonErrorIcon');
                }

                d.reject(error);
            });

             0 && console.log('create editor',arguments);
            return d;
        },
        /**
         * Creates a toolbar widget out of the definitions in the plugin file(s)
         * @param {string} toolbarProp  The property name from plugin file that corresponds to this particular toolbar
         * @param {Element} targetDiv  Container DIV into which this toolbar should be instantiated
         * @param actionSets  Action sets from plugin file(s)
         * @param context  Document context FIXME: 95% sure that parameter is obsolete
         * @returns {Toolbar}  toolbar widget
         */
        _createToolBar: function (toolbarProp, targetDiv, actionSets, context){
            debugger;
            var _toolbarcache = [];
            if (!actionSets) {
                actionSets = Runtime.getExtensions('davinci.actionSets');
            }
            for (var i = 0, len = actionSets.length; i < len; i++) {
                var actions = actionSets[i].actions;
                for (var k = 0, len2 = actions.length; k < len2; k++) {
                    var action = actions[k],
                        toolBarPath = action[toolbarProp];
                    if (toolBarPath) {
                        if (!_toolbarcache[toolBarPath]) {
                            _toolbarcache[toolBarPath] = [];
                        }
                        _toolbarcache[toolBarPath].push(action);
                    }
                }
            }

            var toolbar1 = new Toolbar({'class':"davinciToolbar"}, targetDiv);
            var radioGroups = {};
            var firstgroup = true;
            for (var value in _toolbarcache) {
                if(!dojo.isArray(_toolbarcache[value])){
                    continue;
                }
                if (!firstgroup) {
                    var separator = new ToolbarSeparator();
                    toolbar1.addChild(separator);
                } else {
                    firstgroup = false;
                }
                var actions = _toolbarcache[value];
                for (var p = 0; p<actions.length; p++) {
                    var action = actions[p];
                    if(action==null){
                        debugger;
                    }
                    // dont add dupes

                    Workbench._loadActionClass(action);
                    var parms = {showLabel:false/*, id:(id + "_toolbar")*/};
                    ['label','showLabel','iconClass'].forEach(function(prop){
                        if(action.hasOwnProperty(prop)){
                            parms[prop] = action[prop];
                        }
                    });
                    if (action.className) {
                        parms['class'] = action.className;
                    }
                    var dojoAction;
                    var dojoActionDeferred = new Deferred();
                    if(action.menu && (action.type == 'DropDownButton' || action.type == 'ComboButton')){
                        var menu = new Menu({
                            style: "display: none;"
                        });
                        for(var ddIndex=0; ddIndex<action.menu.length; ddIndex++){
                            var menuItemObj = action.menu[ddIndex];
                            Workbench._loadActionClass(menuItemObj);
                            var menuItemParms = {
                                onClick: dojo.hitch(this, "_runAction", menuItemObj, context)
                            };
                            var props = ['label','iconClass'];
                            props.forEach(function(prop){
                                if(menuItemObj[prop]){
                                    menuItemParms[prop] = menuItemObj[prop];
                                }
                            });
                            var menuItem = new MenuItem(menuItemParms);
                            menuItem._maqAction = menuItemObj;
                            menu.addChild(menuItem);
                        }
                        parms.dropDown = menu;
                        if(action.type == 'DropDownButton'){
                            dojoAction = new DropDownButton(parms);
                        }else{
                            dojoAction = new ComboButton(parms);
                        }
                        dojoAction.onClick = dojo.hitch(this, "_runAction", action, context);
                        dojoAction._maqAction = action;
                        dojoActionDeferred.resolve();
                    }else if (action.toggle || action.radioGroup) {
                        dojoAction = new ToggleButton(parms);
                        dojoAction.item = action;
                        dojoAction.set('checked', action.initialValue);
                        if (action.radioGroup) {
                            var group = radioGroups[action.radioGroup];
                            if (!group) {
                                group = radioGroups[action.radioGroup]=[];
                            }
                            group.push(dojoAction);
                            dojoAction.onChange = dojo.hitch(this, "_toggleButton", dojoAction, context, group);
                        } else {
                            dojoAction.onChange = dojo.hitch(this,"_runAction", action, context);
                        }
                        dojoAction._maqAction = action;
                        dojoActionDeferred.resolve();
                    }else if(action.type){
                        require([action.type], function(ReviewToolBarText) {
                            dojoAction = new ReviewToolBarText();
                            dojoAction._maqActiond = action;
                            dojoActionDeferred.resolve();
                        });
                    }else{
                        dojoAction = new Button(parms);
                        dojoAction.onClick = dojo.hitch(this, "_runAction", action, context);
                        dojoAction._maqAction = action;
                        dojoActionDeferred.resolve();
                    }
                    if (action.icon) {
                        var imageNode = document.createElement('img');
                        imageNode.src = action.icon;
                        imageNode.height = imageNode.width = 18;
                        dojoAction.domNode.appendChild(imageNode);
                    }
                    dojoActionDeferred.then(function(){
                        toolbar1.addChild(dojoAction);
                        //FIXME: looks like the parameter to isEnabled is "context",
                        //but maybe that should be the current editor instead. Whatever,
                        //targetObjectId just has to be wrong.
                        if (action.isEnabled && !action.isEnabled(/*FIXME: targetObjectId*/)) {
                            dojoAction.isEnabled = action.isEnabled;
                            dojoAction.set('disabled', true);
                        } else {
                            dojoAction.set('disabled', false);
                        }
                    });
                }
            }
            return toolbar1;
        },
        showView: function(viewId, shouldFocus, hidden){


            var d = new Deferred();

            try {
                var mainBodyContainer = dijit.byId('mainBody'),
                    view = Runtime.getExtension("davinci.view", viewId),
                    mainBody = dojo.byId('mainBody'),
                    perspectiveId = Workbench.activePerspective,
                    perspective = Runtime.getExtension("davinci.perspective", perspectiveId),
                    position = 'left',
                    cp1;

                dojo.some(perspective.views, function(view){
                    if(view.viewID ==  viewId){
                        position = view.position;
                        return true;
                    }
                });

                mainBody.tabs = mainBody.tabs || {};
                mainBody.tabs.perspective = mainBody.tabs.perspective || {};

                // NOTE: Left-side and right-side palettes start up with 71px width
                // which happens to be the exact pixel size of the palette tabs.
                // This 71px setting prevents the user from seeing an initial flash
                // of temporarily opened left-side and right-side palettes.
                if (position == 'right' && !mainBody.tabs.perspective.right) {
                    mainBodyContainer.addChild(mainBody.tabs.perspective.right =
                        new BorderContainer({'class':'davinciPaletteContainer',
                            style: 'width: '+paletteTabWidth+'px;', id:"right_mainBody",
                            minSize:paletteTabWidth,	// prevent user from dragging splitter too far towards edge
                            region:'right', gutters: false, splitter:true}));
                    mainBody.tabs.perspective.right.startup();
                    // expandToSize is what expandPaletteContainer() uses as the
                    // width of the palette when it is in expanded state.
                    paletteCache.right_mainBody = {
                        expandToSize:340,
                        initialExpandToSize:340
                    };
                }

                if (position == 'left' && !mainBody.tabs.perspective.left) {
                    mainBodyContainer.addChild(mainBody.tabs.perspective.left =
                        new BorderContainer({'class':'davinciPaletteContainer',
                            style: 'width: '+paletteTabWidth+'px;', id:"left_mainBody",
                            minSize:paletteTabWidth,	// prevent user from dragging splitter too far towards edge
                            region:'left', gutters: false, splitter:true}));
                    mainBody.tabs.perspective.left.startup();
                    // expandToSize is what expandPaletteContainer() uses as the
                    // width of the palette when it is in expanded state.
                    paletteCache["left_mainBody"] = {
                        expandToSize:318,
                        initialExpandToSize:318
                    };
                }

                if (position === 'left' || position === 'right') {
                    position += "-top";
                }
                var positionSplit = position;

                if (!mainBody.tabs.perspective[position]) {
                    positionSplit = position.split('-');

                    var region = positionSplit[0],
                        parent = mainBodyContainer,
                        clazz = 'davinciPalette ',
                        style = '';
                    if (positionSplit[1] && (region == 'left' || region == 'right')) {
                        parent = mainBody.tabs.perspective[region];
                        region = positionSplit[1];
                        if (positionSplit[1] == "top") {
                            region = "center";
                            clazz += "davinciTopPalette";
                        } else {
                            style = 'height:30%;';
                            clazz += "davinciBottomPalette";
                        }
                    } else if(region == 'bottom') {
                        style = 'height:80px;';
                        clazz += "davinciBottomPalette";
                    }
                    cp1 = mainBody.tabs.perspective[position] = new TabContainer({
                        region: region,
                        id:'palette-tabcontainer-'+position,
                        tabPosition:positionSplit[0]+'-h',
                        tabStrip:false,
                        'class': clazz,
                        style: style,
                        splitter: region != "center",
                        controllerWidget: "dijit.layout.TabController"
                    });
                    parent.addChild(cp1);
                    dojo.connect(cp1, 'selectChild', this, function(tab){
                        if(tab && tab.domNode){
                            var tc = tab.getParent();
                            // Don't mess with which tab is selected or do any collapse/expand
                            // if selectChild is called in response to adding the first child
                            // of a TabContainer, which causes an implicit selectFirst(),
                            // or other programmatic selectChild() event (in particular,
                            // SwitchingStyleView.js puts _maqDontExpandCollapse on tabcontainer)
                            if(!this._showViewAddChildInProcess && !tc._maqDontExpandCollapse){
                                if(tc._maqLastSelectedChild == tab){
                                    this._expandCollapsePaletteContainer(tab);
                                }else{
                                    this.expandPaletteContainer(tab.domNode);
                                }
                            }
                            tc._maqLastSelectedChild = tab;
                        }
                    }.bind(this));
                } else {
                    cp1 = mainBody.tabs.perspective[position];
                }

                if (dojo.some(cp1.getChildren(), function(child){ return child.id == view.id; })) {
                    return;
                }
                this.instantiateView(view).then(function(tab) {
                    this._showViewAddChildInProcess = true;
                    if (!hidden) {
                        cp1.addChild(tab);
                    }
                    this._showViewAddChildInProcess = false;
                    // Put a tooltip on the tab button. Note that native TabContainer
                    // doesn't offer a tooltip capability for its tabs
                    var controlButton = tab.controlButton;
                    if(controlButton && controlButton.domNode){
                        controlButton.domNode.title = view.title + ' ' +  veNLS.palette;
                    }
                    if(shouldFocus) {
                        cp1.selectChild(tab);
                    }

                    d.resolve(tab);
                }.bind(this));
            } catch (ex) {
                console.error("Error loading view: "+view.id);
                console.error(ex);
            }

            return d;
        },
        instantiateView: function(view) {
            var d = new Deferred(),
                tab = dijit.byId(view.id);
            if (tab) {
                d.resolve(tab);
            } else {
                require([view.viewClass], function(viewCtor){
                    var params = { title: view.title,
                        id: view.id, closable: false, view: view };
                    if(view.iconClass){
                        params.iconClass = view.iconClass;
                    }
                    if(!Workbench.palettes){
                        Workbench.palettes = {};
                    }
                    // Stash the instantiated object corresponding to each palette class in
                    // associative array davinci.palettes, indexed by view.viewClass.
                    // Then pass the instantiated object as the argument to d.resolve().
                    d.resolve((Workbench.palettes[view.viewClass] = new (viewCtor || ViewPart)(params),
                        Workbench.palettes[view.viewClass]));
                });
            }
            return d;
        },

        hideView: function(viewId){
            for (var position in mainBody.tabs.perspective) {
                if(position=='left' || position == 'right'){
                    position+='-top';
                }
                if(!mainBody.tabs.perspective[position]){
                    continue;
                }
                var children = mainBody.tabs.perspective[position].getChildren();
                var found = false;
                for (var i = 0; i < children.length && !found; i++) {
                    if (children[i].id == viewId) {
                        mainBody.tabs.perspective[position].removeChild(children[i]);
                        children[i].destroyRecursive(false);
                    }
                }
            }
        },

        toggleView: function(viewId) {
            var found = dojo.byId(viewId);
            if(found) {
                Workbench.hideView(viewId);
            } else{
                Workbench.showView(viewId, true);
            }
        },
        /**
         * When using the "shadow" approach where there is a shadow
         * TabContainer that shows tabs for the open files, and a StackContainer
         * to hold the actual editors, then this callback is invoked when a user clicks
         * on the tab of the shadow TabContainer. This routine then calls
         * removeChild() on the StackContainer to remove to corresponding editor.
         * @param page  The child widget that is being closed.
         */
        _shadowTabClosed: function(page) {
            if(!davinci.Workbench._shadowTabClosing[page.id]){
                davinci.Workbench._shadowTabClosing[page.id] = true;
                var shadowId = page.id;
                var editorId = shadowIdToEditorId(shadowId);
                if(!davinci.Workbench._editorTabClosing[editorId]){
                    var editorContainer = dijit.byId(editorId);
                    var editorsContainer = dijit.byId("editors_container");
                    if(editorsContainer && editorContainer){
                        editorsContainer.removeChild(editorContainer);
                        editorContainer.destroyRecursive();
                    }
                }
                delete davinci.Workbench._shadowTabClosing[page.id];
            }
        }

    });

    return Module;

});