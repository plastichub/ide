//>>built
define("davinci/review/drawing/shapes/shapes",["./Arrow","./Rectangle","./Ellipse","./Text"],function(b,c,d,e){var a={};a.Arrow=b;a.Rectangle=c;a.Ellipse=d;a.Text=e;return dojo.setObject("davinci.review.drawing.shapes",a)});
//# sourceMappingURL=shapes.js.map