/**  
 * @class davinci.html.CSSFragment
   * @constructor 
   * @extends davinci.html.CSSFile
 */
define("davinci/html/CSSFragment", [
	"dojo/_base/declare",
	"davinci/html/CSSFile"
], function(declare, CSSFile) {

return CSSFile;
});
