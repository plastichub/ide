//>>built
define("davinci/html/ui/CSSOutlineModel",["dojo/_base/declare","davinci/ui/widgets/DavinciModelTreeModel"],function(c,d){return c("davinci.html.ui.CSSOutlineModel",d,{_childList:function(a){var b=[];switch(a.elementType){case "CSSFile":b=a.children;break;case "CSSRule":b=a.properties}return b}})});
//# sourceMappingURL=CSSOutlineModel.js.map