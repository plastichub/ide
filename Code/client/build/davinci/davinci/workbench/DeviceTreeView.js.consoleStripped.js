define("davinci/workbench/DeviceTreeView", [
	"dojo/_base/declare",
	"./ViewPart",
	"../Workbench",
	"../ui/widgets/OutlineTree",
	"dijit/layout/ContentPane",
	"davinci/lang/workbench",
	'xide/utils',
	'xcf/views/DeviceTreeView'
], function (declare, ViewPart, Workbench, OutlineTree, ContentPane, workbenchStrings, utils, DeviceTreeView) {

	return declare(ViewPart, {

		constructor: function (params, srcNodeRef) {
			this.subscribe("/davinci/ui/editorSelected", this.editorChanged);
			//this.subscribe("/davinci/ui/selectionChanged", this.selectionChanged);
			//this.subscribe("/davinci/ui/modelChanged", this.modelChanged);
			this.subscribe("/davinci/ui/context/pagerebuilt", this._pageRebuilt);
		},
		editorChanged: function (changeEvent) {

			 0 && console.log('asdfasdf');
			var _w = Workbench;

			var editor = changeEvent.editor;

			if (this.currentEditor) {
				if (this.currentEditor == editor) {
					return;
				}
			}
			this.currentEditor = editor;

			if (!editor) {
				return;
			}
			this.toolbarDiv.innerHTML = "";
			if (this.treeView) {
				return;
			}

			var deviceMgr = Workbench.ctx.getDeviceManager();
			if (deviceMgr.store) {
				deviceMgr.treeView = utils.addWidget(DeviceTreeView, {
					title: 'Devices',
					store: deviceMgr.getStore(),
					delegate: deviceMgr
				}, deviceMgr, this.containerNode, true);
				this.treeView = deviceMgr.treeView;
			}
		},

		createTree: function () {

			// BEGIN TEMPORARY HACK for bug 5277: Surround tree with content pane and subcontainer div overflow: auto set to workaround spurious dnd events.
			// Workaround should be removed after the following dojo bug is fixed: http://bugs.dojotoolkit.org/ticket/10585
			this.container = new ContentPane({
				style: "padding:0"
			});
			this.subcontainer = dojo.doc.createElement('div');

			this.container.domNode.appendChild(this.subcontainer);
			this.setContent(this.container);

		},

		_getViewContext: function () {
			return this.outlineProvider;
		},

		selectionChanged: function (selection) {
			console.error('sel changed');
		},

		_pageRebuilt: function () {

		},

		modelChanged: function (modelChanges) {

		}
	});
});