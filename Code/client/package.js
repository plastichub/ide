/* jshint node:true */
module.exports.getModules = function (grunt, addRepository) {

    var gitOptions = {
        verbose: true
    };

    var GITHUB_SOURCE_URI = 'https://github.com/gbaumgart';
    var GITLAB_SOURCE_URI = 'https://gitlab.com/xamiro';

    var profile = grunt.option('profile') || 'all';
    var privates = true;profile==='control-freak';

    addRepository('src/lib/xfile', GITHUB_SOURCE_URI + '/xfile.git');
    addRepository('src/lib/xide', GITHUB_SOURCE_URI + '/xide.git');
    addRepository('src/lib/xconsole', GITHUB_SOURCE_URI + '/xconsole.git');
    addRepository('src/lib/xaction', GITHUB_SOURCE_URI + '/xaction.git');
    addRepository('src/lib/davinci', GITHUB_SOURCE_URI + '/xdavinci.git');

    addRepository('src/lib/xexpression', GITHUB_SOURCE_URI + '/xexpression.git');
    addRepository('src/lib/xnode', GITHUB_SOURCE_URI + '/xnode.git');
    addRepository('src/lib/xapp', GITHUB_SOURCE_URI + '/xapp.git');
    addRepository('src/lib/xlog', GITHUB_SOURCE_URI + '/xlog.git');
    addRepository('src/lib/xdocker', GITHUB_SOURCE_URI + '/xdocker.git');
    addRepository('src/lib/dgrid', GITHUB_SOURCE_URI + '/dgrid.git');
    addRepository('src/lib/xdojo', GITHUB_SOURCE_URI + '/xdojo.git');
    addRepository('src/lib/dojo', GITHUB_SOURCE_URI + '/x-dojo.git');
    addRepository('src/lib/xgrid', GITHUB_SOURCE_URI + '/xgrid.git');
    addRepository('src/lib/dcl', GITHUB_SOURCE_URI + '/dcl.git');
    addRepository('src/lib/xideve', GITHUB_SOURCE_URI + '/xideve.git');

    addRepository('src/lib/wcDocker', GITHUB_SOURCE_URI + '/wcDocker.git');

    privates && addRepository('src/lib/xcf', GITLAB_SOURCE_URI + '/xcf.git');
    privates && addRepository('src/lib/xblox', GITLAB_SOURCE_URI + '/xblox-js.git');

    addRepository('src/lib/external/bootstrap-select', 'https://github.com/silviomoreto/bootstrap-select.git');
    addRepository('src/lib/external/bootstrap3-dialog', GITHUB_SOURCE_URI + '/bootstrap3-dialog.git');
    addRepository('src/lib/xlang', GITHUB_SOURCE_URI + '/xlang.git');
    addRepository('src/lib/external/messenger_latest', 'https://github.com/HubSpot/messenger.git');
    addRepository('src/lib/external/twitter-bootstrap-wizard', GITHUB_SOURCE_URI + '/twitter-bootstrap-wizard.git');
    addRepository('src/lib/external/selectize', GITHUB_SOURCE_URI + '/selectize.js.git');
    addRepository('src/lib/external/micromatch', GITHUB_SOURCE_URI + '/micromatch-browser.git');
    addRepository('src/lib/external/velocity', 'https://github.com/julianshapiro/velocity.git');
    addRepository('src/lib/external/font-awesome-picker', GITHUB_SOURCE_URI + '/fontawesome-iconpicker.git');
    //addRepository('src/lib/external/wysihtml', 'https://github.com/Voog/wysihtml.git');

    addRepository('src/theme', GITHUB_SOURCE_URI + '/admin-theme.git', null, gitOptions, {
        post: {
            command: "npm install"
        }
    });

    privates && addRepository('src/lib/xibm', GITLAB_SOURCE_URI + '/xibm.git', null, {
        recursive: true
    }, {
        post: {
            command: "git submodule foreach \'git checkout master\'"
        }
    });

    addRepository('src/lib/xace', GITHUB_SOURCE_URI + '/xace.git', null, {
        recursive: true
    }, {
        post: {
            command: "git submodule foreach \'git checkout master\'"
        }
    });
};