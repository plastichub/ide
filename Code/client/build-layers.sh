#!/usr/bin/env bash
grunt buildLayer --package=davinci
grunt buildLayer --package=xideve
grunt buildLayer --package=xblox
grunt buildLayer --package=xgrid
grunt buildLayer --package=xnode
grunt buildLayer --package=xace
grunt buildLayer --package=xfile
grunt buildLayer --package=dgrid
grunt buildLayer --package=xaction
grunt buildLayer --package=xdocker
grunt buildLayer --package=wcDocker
grunt buildLayer --package=xlog
grunt buildLayer --package=xwire
grunt buildLayer --package=dojox

