Core Javascript Frameworks for all projects
===========================================

This repository is not indented to be used directly but rather as part of a consuming application via 'git submodule'. 
Such an example application can be found [here](https://github.com/mc007/xbox-app).
   
   Important: this is work in progress, don't use it until I say so ;-)
   

# Installation 

## Checkout 

1. git clone --recursive https://github.com/mc007/xjs or : 
2. git submodule update --init --recursive

## Update 

1. git submodule foreach git pull origin master

# Contribute

git commit -m "updated my submodule"
git push


item.state = types.DEVICE_STATE.CONNECTING;
item.info = cInfo;
item._userStopped=null;
## Dependencies

This project requires the following other projects to run:
 * dojo (is a sub module,forked, version = 1.10.3)
 * dgrid (is a sub module,forked, version = 0.3)
 * dojox (is a sub module,forked, version = 1.10.3)
 * dijit (is a sub module,forked, version = 1.10.3)
 * util (build-chain, is a sub module,forked, version = 1.10.3)
 * dcl  (is a sub module, https://github.com/uhop/dcl.git)
 * requirejs (is a sub module, https://github.com/jrburke/requirejs.git)
 * requirejs-domReady
 * requirejs-dplugins
 * requirejs-test
 * [xdojo](https://github.com/mc007/xdojo) for abstracting dcl/delite in delite and legacy dojo projects or vice versa

## Testing
 
 Is done via "intern". The selenium server must be started before : 
``` bash 
java -jar /usr/local/lib/node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-2.43.1.jar -port 4444 -Dwebdriver.chrome.driver=chromedriver
```

 
## SDK 


[Guidelines] (GUIDELINES.md)

## Package Overview

**davinci** :  Old code of IBM's "Maqetta" project : to be removed soon! 

**xfile** : The file - manager widget, needs "xide".

**xide** : The IDEs foundation framework. This package serves the the infrastructure for all xjs based applications.

**xblox** : Library to render and execute  a visual programming language. This needs "xide", "xlog", "davinci", "system", "orion", "preview" and "dcl". This library can be used server and client side. Each "block" has an implementation to be rendered by "xide" (block fields,...). Please ignore this component for now!

**xstyle** Library to deal with CSS and Dom-Style manipulation.

**xlog** Library about logging, works server and client side. The library depends on "xide" and "dgrid" and contains custom views and widgets. 

**xwire** Library to bind data, events and widget properties all together. 

**orion** Old Editor library, parses CSS/Javascript and others. To be removed! 

**system** Old library, to be removed.

**preview** Old library, to be removed. 
 

## Build

``` bash
    cd src
    sh buildstandalone.sh    
```

This will create single layer file which contains all Dojo based in src/xfile/dojo/xbox.js!

## Build layers

For development speed, we did pre-compile some Dojo layers. By default 'dojox' and 'dijit' is pre-compiled. You can switch to normal in src/lib/xbox/run-release-debug.js !

``` bash
    cd src
    sh buildLayer.sh dojox
    sh buildLayer.sh dijit
```

To have 'pre-built' layers, you must have 3 files in your module folder: 

1. run.js
2. layer.profile.js
3. main.js

See in dojox, dijit or xfile for examples. 

## Feature Flags

The following 'has' flags are currently available:

### xblox

Enables the core for xblox and allows to run xblox scripts as such.   

### xblox-ui 

Enables the user-interface (views/widgets/etc...) for the xblox component. xblox is meant to run
on the server as well. When enabling, it adds at least 700KB in dependencies, mostly for the 
expression editor which comes which large data for the Javascript API.
 
### xlog 

When enabled, it add adds all from xlog/ to your application. It enables a LogManager and a Log-View.
Its not needed in most cases. 
 
### xideve

Enables the visual editor components, its huge and has currently no support for builds yet.

### xreload

Enables module hot-reloading for developing XIDE 

### xidebeans

Enables a XIDE specific layer in core classes and utils.  

### delite 

Tells the application that delite is or might be present. This affects nothing at the moment. 

### xexpression

Enables the more advanced expression engine.

### filtrex

Enables the filtrex(bison) parser for xexpression 
 
 
These are meant to be added to your dojoConfig/require-js has registry
 
'xblox-ui':true,'xlog':true,'xblox':true,'xideve':true,'xreload':true,'xidebeans':true,'delite':true,'xexpression':true, 'filtrex':true



