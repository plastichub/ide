/*global module */
module.exports = function (grunt) {
    var path = require('path');
    var ENV = grunt.option('env') || 'development'; // pass --env=production to compile minified css
    var fs = require('fs');
    var libRoot = path.resolve('./src/lib');
    var buildLibDest = path.resolve('./src/build');
    var releaseDirLibs = path.resolve('./build');
    var tmpdir = "./tmp/";
    var outprop = "amdoutput";
    var outdir = "./builddelite/";
    var deploydir = './deploy/';

    var appDir = './src/lib/xapp-build/sdk';
    var srcDir = './src/lib/xibm/ibm/';

    var common = {
        options: { banner: '<%= ' + outprop + '.header%>' },
        src: '<%= ' + outprop + '.modules.abs %>',
        dest: outdir + '<%= ' + outprop + '.layerPath %>'
    };

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        cssmin: {
            xtest: {
                options: {
                },
                files: {
                    'src/lib/xtest/xtest.min.css':
                        [


                        './src/css/elusive-icons/elusive-webfont.css',


                        './src/css/commons/Widgets.css',


                        './src/css/commons/Loading.css',

                        './src/css/commons/grid.css',

                        './src/css/commons/XFILE.css',

                        './src/css/commons/bootstrap.css',

                        './src/css/xfile/fileGridView.css',
                        './src/css/xfile/Layout.css',
                        './src/css/xfile/quicklook.css',


                        './src/css/xcf/Layout.css',
                        './src/css/xcf/GridView.css',

                        './src/css/xcf/ConsoleView.css',
                        './src/css/commons/XCF.css',

                        './src/css/commons/bootstrap.css',
                        './src/css/commons/Progress.css',
                        './src/css/json_editor/jsoneditor.css',

                        './src/css/xcf/Icons.css',
                        './src/css/fonts/stylesheet.css',
                        './src/lib/external/jQuery/plugins/tooltipster/css/tooltipster.css',
                        './src/lib/external/jQuery/plugins/tooltipster/css/themes/tooltipster-light.css',
                            './src/theme/html-white/dist/css/application.css',
                        './src/css/commons/stylesheets/bootstrap-override.css',
                        './src/lib/external/messenger_latest/build/css/messenger-theme-flat.css',
                        './src/lib/external/bootstrap-select/dist/css/bootstrap-select.css',
                        './src/lib/external/bootstrap3-dialog/dist/css/bootstrap-dialog.css',
                        './src/lib/external/font-awesome-picker/dist/css/fontawesome-iconpicker.min.css',
                        './src/lib/external/bootstrap3-editable/css/bootstrap-editable.css'
                    ]
                }
            },
            xcf: {
                options: {
                },
                files: {
                    'src/xcf/xcf.min.css':[
                            './src/css/elusive-icons/elusive-webfont.css',
                            './src/css/font-awesome/css/font-awesome.css',
                            './src/css/commons/Widgets.css',
                            './src/css/commons/Loading.css',
                            './src/css/commons/XFILE.css',
                            './src/css/xcf/main.css',
                            './src/fonts/stylesheet.css',
                            './src/css/commons/bootstrap.css',
                            './src/css/xfile/fileGridView.css',
                            './src/css/xfile/Layout.css',
                            './src/css/xfile/quicklook.css',
                            './src/css/xcf/Layout.css',
                            './src/css/xcf/GridView.css',
                            './src/css/xcf/ConsoleView.css',
                            './src/css/commons/bootstrap.css',
                            './src/css/json_editor/jsoneditor.css',
                            './src/css/xcf/Icons.css',
                            './src/css/xide/xideve.css',
                            './src/css/fonts/stylesheet.css',
                            './src/css/commons/stylesheets/bootstrap-override.css',
                            './src/lib/external/messenger_latest/build/css/messenger-theme-flat.css',
                            './src/lib/external/bootstrap-select/dist/css/bootstrap-select.css',
                            './src/lib/external/bootstrap3-dialog/dist/css/bootstrap-dialog.css',
                            './src/lib/external/font-awesome-picker/dist/css/fontawesome-iconpicker.min.css',
                            './src/lib/external/bootstrap3-editable/css/bootstrap-editable.css',
                            './src/lib/dijit/themes/claro/claro.css'
                        ]
                }
            },
            xbox: {
                options: {
                },
                files: {
                    'src/xbox/xbox.min.css':[
                        './src/css/elusive-icons/elusive-webfont.css',
                        './src/css/font-awesome/css/font-awesome.css',
                        './src/css/commons/Widgets.css',
                        './src/css/commons/Loading.css',
                        './src/css/commons/XFILE.css',
                        './src/css/xfile/fileGridView.css',
                        './src/css/xfile/Layout.css',
                        './src/css/xfile/quicklook.css',
                        './src/css/commons/bootstrap.css',
                        './src/css/commons/stylesheets/bootstrap-override.css',
                        './src/lib/external/messenger_latest/build/css/messenger-theme-flat.css',
                        './src/lib/external/bootstrap-select/dist/css/bootstrap-select.css',
                        './src/lib/external/bootstrap3-dialog/dist/css/bootstrap-dialog.css'
                    ]
                }
            },
            xfile:{
                options: {
                },
                files: {
                    'src/build/xfile/xfile.min.css':[
                        './src/css/xfile/main_lib.css'
                    ]
                }
            }
        },
        shell: {
            themeTransparent: {
                command: [
                    'cd src/theme',
                    'grunt --gruntfile GruntfileAcc.js --target=html-transparent-small dist-compass',
                    'cp html-transparent-small/dist/css/application.css ../css/theme-transparent.css'
                ].join('&&'),

                options: {
                    stderr: false

                }
            },
            themeWhite: {
                command: [
                    'cd src/theme',
                    'ls',
                    'grunt --gruntfile GruntfileAcc.js --target=html-white dist-compass',
                    'cp html-white/dist/css/application.css ../css/theme-white.css'
                ].join('&&'),

                options: {
                    stderr: false

                }
            }
        },
        dojo: {
            xgrid: {
                options: {
                    profile: './xgrid/layer.profile.js', // Profile for build
                    profiles: [], // Optional: Array of Profiles for build
                    releaseDir: releaseDirLibs+'/xgrid',
                    require: 'xgrid/main.js', // Optional: Module to require for the build (Default: nothing)
                    dojoConfig: './dojoBuildLibConfig.js',
                    ignoreErrors:true
                     // Optional: Array of locations of package.json (Default: nothing)
                }
            },
            options: {
                // You can also specify options to be used in all your tasks
                dojo: 'dojo/dojo.js', // Path to dojo.js file in dojo source
                load: 'build', // Optional: Utility to bootstrap (Default: 'build')
                profile: 'app.profile.js', // Profile for build
                profiles: [], // Optional: Array of Profiles for build
                appConfigFile: '', // Optional: Config file for dojox/app
                package: '', // Optional: Location to search package.json (Default: nothing)
                packages: [], // Optional: Array of locations of package.json (Default: nothing)
                require: '', // Optional: Module to require for the build (Default: nothing)
                requires: [], // Optional: Array of modules to require for the build (Default: nothing)
                action: 'release', // Optional: Build action, release, help. clean has been deprecated.
                cwd: libRoot, // Directory to execute build within
                dojoConfig: './dojoBuildLibConfig.js',  // Optional: Location of dojoConfig (Default: null),
                // Optional: Base Path to pass at the command line
                // Takes precedence over other basePaths
                // Default: null
                basePath: '.'
            }
        },
        requirejs: {
            /**
             * Grunt task to compile wcDocker as all-in-one and non-minified library
             * into Build/wcDocker.js
             */
            test: {
                options: {
                    baseUrl: './src/lib/',
                    out: './src/lib/xwire/build/xwirer.js',
                    optimize: 'none',
                    //name: 'libs/almond',
                    //require-js bootstrap
                    include: [
                        'xwire/r.layer.js'
                    ],
                    stubModules: [],
                    wrap: false,
                    paths: {
                        dojo: "empty:",
                        xdojo: "empty:",
                        xide: "empty:"
                    }

                }
            }

        },
        copy: {
            dist_xbox:{
                expand: true,
                src: '**/*',
                dest: './dist/src/xbox',
                cwd: './src/xbox'
            },
            dist_ext:{
                expand: true,
                src: '**/*',
                dest: './dist/src/ext',
                cwd: './src/ext'
            },
            dist_build:{
                expand: true,
                src: '**/*',
                dest: './dist/src/build',
                cwd: './src/build'
            },
            dist_xcf:{
                expand: true,
                src: '**/*',
                dest: './dist/src/xcf',
                cwd: './src/xcf'
            },
            "dist_themes":{
                src:[
                    'html-transparent/dist/css/*.css',
                    'html-white/dist/css/*.css',
                    'html-gray/dist/css/*.css',
                    'html-blue/dist/css/*.css'
                ],
                dest:'./dist/src/theme',
                expand: true,
                flatten: false,
                cwd:'./src/theme'
                
            },
            lib: {
                expand: true,
                src: srcDir + '/**',
                dest: appDir + '/sdk'
            },
            plugins: {
                expand: true,
                cwd: tmpdir,
                src: "<%= " + outprop + ".plugins.rel %>",
                dest: outdir,
                dot: true
            },
            deploy: {
                expand: true,
                cwd: outdir + '<%= amdloader.baseUrl %>',
                src: '**/*',
                dest: deploydir,
                dot: true
            },
            xapp:{
                expand: true,
                cwd: outdir + '<%= amdloader.baseUrl %>',
                src: '**/*',
                dest: './src/lib/xapp/build',
                dot: false
            }
        },
        clean: {
            erase: [outdir],//.concat(libDirsBuild),
            finish: [tmpdir]
        },
        uglify: {
            options: {
                banner: "/* My Custom banner */" + "<%= " + outprop + ".header%>",
                sourceMap: true
            },
            dist: {
                src: "<%= " + outprop + ".modules.abs %>",
                dest: outdir + "<%= " + outprop + ".layerPath %>"
            }
        },
        amdreportjson: {
            dir: outdir
        },
        amdloader: {
            // Everything should be relative to baseUrl
            inlineText: true,
            baseUrl: "./src/lib/",
            // Here goes the config for the amd plugins build process (has, i18n, ecma402...).
            config: {
                'delite/theme': {
                    theme: 'bootstrap'
                },
                'requirejs-dplugins/i18n': { locale: 'en-en' },
                "requirejs-dplugins/has": {
                    'xcf-ui': false,
                    'drivers':false,
                    'xexpression':false,
                    'xblox-ui':false,
                    'xideve':false,
                    'xcf':true,
                    'php':false,
                    'xace':false,
                    'filtrex':false,
                    'plugins':false,
                    'xreload':false,
                    'xnode-ui':false,
                    'host-node':false,
                    'electron':true
                },
                map: {
                    '*': {
                        'deliteful/Button': 'xdeliteful/Button',
                        'deliteful/Slider': 'xdeliteful/Slider',
                        'delite/Container': 'xdelite/Container'
                    }
                }
            },
            paths: {
                'requirejs-dplugins':'xibm/ibm/requirejs-dplugins',
                'xaction':'xaction/src',
                'xdojo':'xdojo/delite',
                'delite':'xibm/ibm/delite',
                'decor':'xibm/ibm/decor',
                'dpointer':'xibm/ibm/dpointer',
                'deliteful':'xibm/ibm/deliteful',
                'xdeliteful':'xibm/xibm/deliteful',
                'xdelite':'xibm/xibm/delite',
                'requirejs-domready':'xibm/ibm/requirejs-domready',
                'requirejs-text':'xibm/ibm/requirejs-text',
                'jquery':'xibm/ibm/jquery/',
                'lie':'xibm/ibm/lie',
                'ecma402':'xibm/ibm/ecma402'
            },
            packages: [
                {
                    name: 'xapp',
                    location: 'xapp'
                },
                {
                    name: 'xide',
                    location: 'xide'
                },
                {
                    name: 'requirejs-dplugins',
                    location: 'xibm/ibm/requirejs-dplugins'
                }
            ]
        },
        // The common build config
        amdbuild: {
            map: {
                jquery: {
                    "jquery/src/selector": "jquery/src/selector-native"     // don't pull in sizzle
                }
            },
            paths: {
                'angular': 'angular/angular',
                'angular-loader': 'angular-loader/angular-loader',
                'jquery':'xibm/ibm/jquery/dist/jquery',
                'requirejs-dplugins':'xibm/ibm/requirejs-dplugins'
            },
            packages: [
                {
                    name: 'xapp',
                    location: 'xapp'
                },
                {
                    name: 'xide',
                    location: 'xide'
                },
                {
                    name: 'requirejs-dplugins',
                    location: 'xibm/ibm/requirejs-dplugins'
                }
            ],
            // dir is the destination of processed files.
            dir: tmpdir,
            //buildPlugin: true,

            // List of plugins that the build should not try to resolve at build time.
            runtimePlugins: [],
            layers: [
                {
                    name: 'main_build',
                    include: [
                        "xapp/mainr"
                    ]
                }                
            ]
        },
        concat: {
            options: { separator: ';\n' },
            dist: common
        },
        config: {
            target: grunt.option('target') || 'html-transparent', // pass --target=html-transparent. possible targets: html-transparent, html-transparent-dark, html-white
            srcFolder: './src/css/commons/sass',
            distFolder: './src/css/commons/stylesheets'
        },
        sass: {
            options: {
                lineNumbers:true,
                importer:  function(url, prev, done) {
                    var urlPrefix ="";
                    if ((/^CSS:/.test(url))) { // if indexOf == true then url.indexOf == 0 == false
                        //fix vendor bug
                        url = url.replace('../../','');
                        return {
                            contents: fs.readFileSync(urlPrefix+url.replace('CSS:.', '') + '.css').toString()
                        };
                    } else {
                        return {
                            file: url
                        };
                    }
                },
                sourcemap: 'inline'
            },
            dist: {
                files: [
                    {
                    expand: true,
                    cwd: './src/css/commons/sass/',
                    src: ['*.scss'],
                    dest: './src/css/commons/stylesheets/',
                    ext: '.css'
                }]
            },
            _dist: {
                options: {
                    outputStyle: 'expanded',
                    precision: 10
                },
                files: {
                    "<%= config.distFolder %>/css/application.css":"<%= config.srcFolder %>/sass/application.scss"
                }
            },
            min: {
                options: {
                    outputStyle: 'compressed'
                },
                files: {
                    "<%= config.distFolder %>/css/application.min.css":"<%= config.srcFolder %>/sass/application.scss"
                }
            }
        },
        watch: {
            syncSass: {
                files: ['<%= config.srcFolder %>/sass/**.scss', '<%= config.srcFolder %>/sass/**.sass'],
                tasks: ['copy:syncTransparentDarkStyles', 'copy:syncWhiteStyles']
            },
            sass: {
                files: [
                    './src/css/commons/sass/**.scss'
                ],
                tasks: ['sass:dist']
            }

        },
        release: {
            options: {
                bump: true, //default: true
                changelog: false, //default: false
                changelogText: '<%= version %>\n', //default: '### <%= version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n'
                file: './dist/package.json', //default: package.json
                add: false, //default: true
                commit: false, //default: true
                tag: false, //default: true
                push: false, //default: true
                pushTags: false, //default: true
                npm: true, //default: true
                npmtag: false, //default: no tag
                indentation: '\t', //default: '  ' (two spaces)
                folder: './dist', //default project root
                tagName: 'some-tag-<%= version %>', //default: '<%= version %>'
                commitMessage: 'check out my release <%= version %>', //default: 'release <%= version %>'
                tagMessage: 'tagging version <%= version %>', //default: 'Version <%= version %>',
                beforeBump: [], // optional grunt tasks to run before file versions are bumped
                afterBump: [], // optional grunt tasks to run after file versions are bumped
                beforeRelease: [], // optional grunt tasks to run after release version is bumped up but before release is packaged
                afterRelease: [], // optional grunt tasks to run after release is packaged
                updateVars: [], // optional grunt config objects to update (this will update/set the version property on the object specified)
                _github: {
                    apiRoot: 'https://git.example.com/v3', // Default: https://github.com
                    repo: 'geddski/grunt-release', //put your user/repo here
                    accessTokenVar: 'GITHUB_ACCESS_TOKE', //ENVIRONMENT VARIABLE that contains GitHub Access Token

                    // Or you can use username and password env variables, we discourage you to do so
                    usernameVar: 'GITHUB_USERNAME', //ENVIRONMENT VARIABLE that contains GitHub username
                    passwordVar: 'GITHUB_PASSWORD' //ENVIRONMENT VARIABLE that contains GitHub password
                }
            }
        }

    });
    
    grunt.loadTasks("./tasks/");
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    
    // Load plugins
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-extend-config");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-dojo');
    grunt.registerTask('themes', ['shell:themes']);
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks('grunt-git');
    grunt.loadNpmTasks('grunt-release');
    grunt.registerTask('buildXFILE',null, function () {
        var packageName= 'xfile';
        var optimize = grunt.option('uglify') ===true ? 'uglify2' : 'none';

        var all = false;
        var allPackages = [
            'xide',
            'xlang',
            'dojo',
            'xwire',
            'xblox',
            'xideve',
            'xwire',
            'dcl',
            'xaction',
            'xblox',
            'dstore',
            'xcf',
            'delite',
            'decor',
            'wcDocker',
            'xdocker',
            'dgrid',
            'xgrid',
            'requirejs-domready'
        ];

        var paths = {
            'requirejs-dplugins/':'xibm/ibm/requirejs-dplugins/'
        };

        var main = 'mainr.js';
        var _main = grunt.option('main');
        if(_main){
            main = _main + '.js';
        }
        if(all!==true) {
            for (var i = 0; i < allPackages.length; i++) {
                var path = allPackages[i];
                if (path !== packageName) {
                    paths[path] = "empty:";
                }
            }
        }
        grunt.extendConfig({
            requirejs: {
                'buildLib':{
                    options: {
                        baseUrl: './src/lib/',
                        out: './src/lib/' + packageName +'/build/' + packageName  + 'r.js',
                        optimize: optimize,
                        include: [
                            packageName + '/'  + main
                        ],
                        exclude: [

                        ],
                        stubModules: [],
                        wrap: false,
                        paths: paths,
                        packages: [
                            {
                                name: 'requirejs-dplugins',
                                location: 'xibm/ibm/requirejs-dplugins'
                            },
                            {
                                name: 'delite',
                                location: 'xibm/ibm/delite'
                            },
                            {
                                name: 'decor',
                                location: 'xibm/ibm/decor'
                            },
                            {
                                name: 'requirejs-domready',
                                location: 'xibm/ibm/requirejs-domready'
                            },
                            {
                                name: 'wcDocker',
                                location: 'wcDocker/src'
                            },
                            {
                                name: 'xgrid',
                                location: 'xgrid'
                            },
                            {
                                name: 'dgrid',
                                location: 'dgrid'
                            },
                            {
                                name: 'xide',
                                location: 'xide'
                            },
                            {
                                name: 'xace',
                                location: 'xace'
                            },
                            {
                                name: 'xaction',
                                location: 'xaction/src'
                            },
                            {
                                name: 'xdocker',
                                location: 'xdocker'
                            },
                            {
                                name: 'xfile',
                                location: 'xfile'
                            },
                            {
                                name: 'dstore',
                                location: 'dstore'
                            }
                        ]
                    }
                }
            }
        });
        grunt.task.run('requirejs:buildLib');
        grunt.task.run('cssmin:xfile');
    });
    
    grunt.registerTask('buildLayerR',null, function () {

        var packageName= grunt.option('package');
        var optimize = grunt.option('uglify') ===true ? 'uglify2' : 'none';
        var all = grunt.option('all') === true ? true : false;
        var allPackages = [
            'xide',
            'xlang',
            'dojo',
            'xwire',
            'xblox',
            'xideve',
            'xwire',
            'dcl',
            'xaction',
            'xblox',
            'dstore',
            'xcf',
            'delite',
            'decor',
            'wcDocker',
            'xdocker',
            'dgrid',
            'xgrid',
            //'xdojo',
            'requirejs-domready'
        ];
        var paths = {
            'requirejs-dplugins/':'xibm/ibm/requirejs-dplugins/'
        };

        var main = 'main.js';
        var _main = grunt.option('main');
        if(_main){
            main = _main + '.js';
        }
        if(all!==true) {
            for (var i = 0; i < allPackages.length; i++) {
                console.error('remove package');
                var path = allPackages[i];
                if (path !== packageName) {
                    paths[path] = "empty:";
                }
            }
        }
        grunt.extendConfig({
            requirejs: {
                'buildLib':{
                    options: {
                        baseUrl: './src/lib/',
                        out: './src/lib/' + packageName +'/build/' + packageName  + 'r.js',
                        optimize: optimize,
                        include: [
                            packageName + '/'  + main
                        ],
                        exclude: [

                        ],
                        stubModules: [],
                        wrap: false,
                        paths: paths,
                        packages: [
                            {
                                name: 'requirejs-dplugins',
                                location: 'xibm/ibm/requirejs-dplugins'
                            },
                            {
                                name: 'delite',
                                location: 'xibm/ibm/delite'
                            },
                            {
                                name: 'decor',
                                location: 'xibm/ibm/decor'
                            },
                            {
                                name: 'requirejs-domready',
                                location: 'xibm/ibm/requirejs-domready'
                            },
                            {
                                name: 'wcDocker',
                                location: 'wcDocker/src'
                            }
                        ]
                    }
                }
            }
        });
        grunt.task.run('requirejs:buildLib');
    });

    /**
     * delite package
     */
    grunt.registerTask('buildDelite',null, function () {
        libRoot +='/xibm/ibm/';
        var packageName= grunt.option('package');
        var optimize = grunt.option('uglify') ===true ? 'uglify2' : 'none';
        var packageSourcePrefix = "/";
        try {
            packageSourcePrefix = fs.lstatSync(libRoot + '/' + packageName + '/src') ? '/src/' : '/';
        }catch(e){}

        var allPackages = [
            'xide',
            'xlang',
            'dojo',
            'xwire',
            'xblox',
            'xideve',
            'xwire',
            'dcl',
            'xaction',
            'xblox',
            'xcf',
            'delite',
            'decor',
            'requirejs-domready',
            'dpointer'
        ];

        var paths = {

        };

        var main = 'main.js';
        var _main = grunt.option('main');
        if(_main){
            main = _main + '.js';
        }

        for (var i = 0; i < allPackages.length; i++) {
            var path = allPackages[i];
            if(path!==packageName){
                paths[path] = "empty:";
            }
        }
        grunt.extendConfig({
            requirejs: {
                'buildLib':{
                    options: {
                        optimize: optimize,
                        baseUrl: './src/lib/xibm/ibm/',
                        out: './src/lib/xibm/xibm/' + packageName +'/build/' + packageName  + 'r.js',
                        include: [
                            '../xibm/' +packageName + '/'  + main
                        ],
                        exclude: [
                            "dojo/dojo", "dojo/has", "dcl/dcl"
                        ],
                        stubModules: [],
                        wrap: false,
                        paths: paths,
                        packages: [

                        ]
                    }
                }
            }
        });
        grunt.task.run('requirejs:buildLib');
    });

    grunt.registerTask('buildLayer',null, function () {
        var packageName= grunt.option('package');
        var packageBuildDirectory = './build' +'/'+packageName+'/'+packageName;
        var packageSourcePrefix = "/";
        try {
            packageSourcePrefix = fs.lstatSync(libRoot + '/' + packageName + '/src') ? '/src/' : '/';
        }catch(e){}

        var packages = [];
        var p = {};
        p.name = packageName;
        p.location = packageName + packageSourcePrefix;
        packages.push(p);

        grunt.extendConfig({
            dojo: {
                'buildLib': {
                    options: {
                        profile: './' + packageName + packageSourcePrefix + '/layer.profile.js', // Profile for build
                        profiles: [], // Optional: Array of Profiles for build
                        releaseDir: releaseDirLibs+'/' + packageName,
                        require: packageName + packageSourcePrefix + '/main.js', // Optional: Module to require for the build (Default: nothing)
                        dojoConfig: './dojoBuildLibConfig.js',
                        ignoreErrors:true,
                        package: packageName + '',
                        action: 'release'
                    }
                }
            }
        });
        grunt.extendConfig({
            copy: {
                'build': {
                    src:packageName +'.js',
                    expand: true,
                    flatten: false,
                    dest: buildLibDest + '/' + packageName +  '/',
                    cwd:packageBuildDirectory
                }
            }
        });
        grunt.task.run('dojo:buildLib');
        grunt.task.run('copy:build');
    });

    grunt.registerTask('buildDeliteApp',null, function () {
        var packageName= grunt.option('package');
        var optimize = grunt.option('uglify') ===true ? 'uglify2' : 'none';
        var allPackages = [
            'xide',
            'xlang',
            'dojo',
            'xwire',
            'xblox',
            'xideve',
            'xwire',
            'dcl',
            'xaction',
            'xblox',
            'dstore',
            'xcf',
            'delite',
            'decor',
            'requirejs-domready',
            'xapp',
            'xnode',
            'xfile',
            'xdeliteful'
        ];

        var paths = {
            'requirejs-dplugins/':'xibm/ibm/requirejs-dplugins/',
            'requirejs-text':'xibm/ibm/requirejs-text',
            'requirejs-domready':'xibm/ibm/requirejs-domready',
            'xaction':'xaction/src',
            'xdojo':'xdojo/delite',
            'dpointer':'xibm/ibm/dpointer',
            'decor':'xibm/ibm/decor',
            'delite':'xibm/ibm/delite',
            'deliteful':'xibm/ibm/deliteful',
            'xdeliteful':'xibm/xibm/deliteful',
            "nxapp":"../xassdsd"
        };

        var main = 'main.js';
        var _main = grunt.option('main');
        if(_main){
            main = _main + '.js';
        }
        grunt.extendConfig({
            requirejs: {
                'buildLib':{
                    options: {
                        baseUrl: './src/lib/',
                        out: './src/lib/' + packageName +'/build/' + packageName  + 'r.js',
                        optimize: optimize,
                        include: [
                            packageName + '/'  + main
                        ],
                        exclude: [
                            'delite/theme'
                        ],
                        stubModules: [],
                        wrap: false,
                        paths: paths,
                        mainConfigFile:'src/lib/xibm/build_config.js',
                        packages: [
                            {
                                name: 'requirejs-dplugins',
                                location: 'xibm/ibm/requirejs-dplugins'
                            },
                            {
                                name: 'delite',
                                location: 'xibm/ibm/delite'
                            },
                            {
                                name: 'dcl2',
                                location: 'xibm/ibm/dcl2'
                            },
                            {
                                name: 'decor',
                                location: 'xibm/ibm/decor'
                            },
                            {
                                name: 'requirejs-domready',
                                location: 'xibm/ibm/requirejs-domready'
                            }
                        ]
                    }
                }
            }
        });
        grunt.task.run('requirejs:buildLib');
    });

    grunt.registerTask('themes', [
        'shell:claro',
        'shell:themeWhite',
        'shell:themeTransparent'
    ]);

    // The main build task.
    grunt.registerTask("amdbuild2", function (amdloader) {
        console.error('-');
        function useAmdDepsScan(name) {
            var layerToGetDeps = [
                "delite/layer",
                "decor/layer",
                "deliteful/layer",
                "ecma402/layer",
                "dtreemap/layer"
            ];
            return layerToGetDeps.indexOf(name) >= 0;
        }

        // Create tasks list
        var tasksList = [];
        var name = this.name;
        var layers = grunt.config(name).layers;

        layers.forEach(function (layer) {
            if (useAmdDepsScan(layer.name)) {
                tasksList.push("amddepsscan:" + layer.name + ":" + name + ":" + amdloader);
            } else {
                tasksList.push("amddirscan:" + layer.name + ":" + name + ":" + amdloader);
            }
            tasksList.push("amdserialize:" + layer.name + ":" + name + ":" + amdloader + ":" + outprop);
            tasksList.push("uglify");
            tasksList.push("correctSourceMap:" + layer.name + ":" + name + ":" + outdir);
            // Remove references to useless html template before copying plugins files.
            tasksList.push("filterPluginFiles:\\.(html|json)\\.js$:" + outprop);
            tasksList.push("copy:plugins");
        });
        /*
        console.error('name : ' + name);
        var name = this.name,
            layers = grunt.config(name).layers;

        grunt.task.run("clean:erase");

        // Run all the tasks for all the layers with the right arguments.
        layers.forEach(function (layer) {
            grunt.task.run("amddepsscan:" + layer.name + ":" + name + ":" + amdloader);
            grunt.task.run("amdshim:" + layer.name + ":" + name + ":" + amdloader);
            grunt.task.run("amdserialize:" + layer.name + ":" + name + ":" + amdloader + ":" + outprop);
            // Generate a minified layer only if the name ends with ".min".
            if (layer.name.search(/\.min$/) !== -1) {
                //grunt.task.run("uglify");
            } else {
                //grunt.task.run("concat");
            }
            grunt.task.run("copy:plugins");
        });

        */
        grunt.task.run("amdreportjson:" + name);
    });


    grunt.registerTask('amdbuild', function (amdloader) {
        var name = this.name, layers = grunt.config(name).layers;
        layers.forEach(function (layer) {
            grunt.task.run("amdshim:" + layer.name + ":" + name + ":" + amdloader);
            //console.log('amddepsscan:' + layer.name + ':' + name + ':' + amdloader);
            grunt.task.run('amddepsscan:' + layer.name + ':' + name + ':' + amdloader);
            grunt.task.run('amdserialize:' + layer.name + ':' + name + ':' + outprop);
            grunt.task.run('concat');
            //grunt.task.run('closure-compiler');
            //grunt.task.run("uglify");
            grunt.task.run('copy:plugins');
            grunt.task.run("amdreportjson:" + name);
        });
    });
    grunt.loadNpmTasks("grunt-amd-build");
    grunt.registerTask('update-dist', [
            'copy:dist_xbox',
            'copy:dist_xcf',
            'copy:dist_ext',
            'copy:dist_build',
            'copy:dist_themes',
            'release'
        ]);
    grunt.registerTask('build-xapp', [
        'amdbuild:amdloader',
        'copy:xapp'
        //'amdreportjson:amdbuild'
        //'clean:finish'
    ]);
};