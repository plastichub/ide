#!/bin/bash

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"

# Destination directory for built code
DISTDIR="$BASEDIR/xcfBuild"

DISTDIR_FINAL="$BASEDIR/xcf"

rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="xcf/run-release"

# Main application package loader configuration
LOADERCONF="$SRCDIR/$LOADERMID.js"

# Main application package build configuration
PROFILE="$SRCDIR/xcf/app.profile.js"

CONFIG="$SRCDIR/xcf/dojoConfig.js"



# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

echo -n "Cleaning old files..."
rm -rf "$DISTDIR"
echo " Done"

########################################################################################
#
#   Create Symbolic Files to main CSS directories
#

cd "$TOOLSDIR"

if which java >/dev/null; then
    java -Xms1024m -Xmx2048m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build  --dojoConfig "$CONFIG" --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
else
    echo "Need node.js or Java to build!"
    exit 1
fi
#
#
#
########################################################################################

cd "$BASEDIR"
echo "Build complete"


########################################################################################
#
#   Copy dojo i8ln files
#
cp -rf "$DISTDIR/dojo/nls/" $DISTDIR/nls

######################
#
#   Clean up temp files
#
cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xcf/dojo/xcf_d.js
#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xcf/dojo/xcf.js

if [ ! -f $DISTDIR/dojo/dojo.js ]; then
    echo "Cant find dojo.js ----- incomplete build !!!!"
    exit;
fi

cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xcf/dojo/xcf.js
#cp -rf $DISTDIR/dojo/dojo.js.map $BASEDIR/xcf/dojo/xcf.js.map

#cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xfile/dojo/xbox_min.js
#find $DISTDIR -name *.uncompressed.js -exec rm '{}' ';'
#find $DISTDIR -name *.consoleStripped.js -exec rm '{}' ';'
#find $DISTDIR -name *.map -exec rm '{}' ';'


#cp -rf $DISTDIR/xbox/resources/app.css $BASEDIR/xfile/xbox/resources/app.css
ls -l --block-size=K xcf/dojo/
#
# POST STEPS FINISH
#
########################################################################################
