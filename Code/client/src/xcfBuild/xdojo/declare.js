//>>built
define("xdojo/declare",["dcl/dcl","module","xide/utils","dojo/_base/declare"],function(k,l,g,c){(function(c){c.classFactory=function(d,a,e,f,b){b=null!=a?a:g.cloneKeys(b||{});e=e||[];d=d||"xgrid.Base";f=f||{};a&&g.mixin(b,a);a=[];for(var h in b)b[h]&&a.push(b[h]);a=a.concat(e);return c(d,a,f)}})(c);return c});
//# sourceMappingURL=declare.js.map