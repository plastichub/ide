require({cache:{
'url:dstore/tests/data/contributors.csv':"id,last,first\n1,Aaron,Brandon\n2,Acord,Gary\n3,Addesa,Michael\n4,Agliullin,Mars\n5,Aguilar,Vincente Jimenez\n6,Albornez Mulligan,Jordi\n7,Algesten,Martin\n8,Allen,Robert\n9,Alpert,John Izaaic\n10,Ambrosi,Lucas\n11,Anderson,Christian O.\n12,Anderson,Matt\n13,Andrews,Jesse\n14,Anuruddha,Dilan\n15,Armstrong,Kevin\n16,Arnold,Brian\n17,Arps,Jens\n18,Arthur,Breton\n19,Ascher,David\n20,Ash,Christopher Baird\n21,Atre,Mac\n22,Aylott,Thomas\n23,Bakaus,Paul\n24,Baker,Christopher\n25,Baker,Jonathan\n26,Baker,Shawn\n27,Balogh,Jeff\n28,Baltopoulos,Ioannis\n29,Bannister,Michael\n30,Barber,Chris\n31,Barnt,Dave\n32,Barrett,Christopher\n33,Barzilai,Joseph\n34,Basler,Mark\n35,Bates,Simon\n36,Baz,David\n37,Beard,Jacob Evan\n38,Belongi,Mignon\n39,Benton,Morgan\n40,Berkhout,Just\n41,Bernstein,Daniel\n42,Bersac,Etienne\n43,Bertet,Pierre\n44,Best,Jason\n45,Bevin,Geert\n46,Bhatia,Naresh\n47,Bing,Dong\n48,Bird,Stefan\n49,Bittinger,Nathan\n50,Bodzak,Lee\n51,Bolter,David\n52,Bond-Caran,Jonathan\n53,Bond,Edward\n54,Bordet,Simone\n55,Bottard,Eric\n56,Bouchon,Paul\n57,Boyer,Marc\n58,Braid,Cameron\n59,Braun,Terry\n60,Brettle,Dean William\n61,Brewster,John\n62,Brinnand,John\n63,Brown,Dallas\n64,Brown,Eric W.\n65,Buch,Daniel James\n66,Buffone,Robert\n67,Buffoni,Fabio\n68,Burke,James Richard\n69,Burke,Thomas\n70,Cahill,Earl\n71,Cakalic,James\n72,Cappiello,John\n73,Carpenter,Lee\n74,Carreira,Jason\n75,Carrera,Daniel\n76,Carstensen,Brian\n77,Cartier,Dennis\n78,Caudill,Mark\n79,Cavalier,Brian\n80,Cavatorta,Andy\n81,Chauduary,Dipen\n82,Chen,Andrew\n83,Childers,James\n84,Chimene,Jeffrey\n85,Chiumenti,Andrea\n86,Cho,Youngho\n87,Chong,Petra\n88,Christiansen,Jason\n89,Chung,Stephen\n90,Cisternino,Antonio\n91,Clark,Patrick\n92,Clayton,David\n93,Clemmons,Eric\n94,Cochran,Samuel\n95,Comer,Kevin\n96,Conaway,Kevin\n97,Conlon,Martin John\n98,Conroy,Matt\n99,Cooper,James Cameron\n100,Cope,Wesley D.\n101,Coquet,Miguel\n102,Costan,Victor Marius\n103,Crowder,Thomas\n104,Cuervo,Jorge Martin\n105,D'Souza,Michelle\n106,Dalouche,Sami\n107,Damour,Remy\n108,Dangoor,Kevin\n109,Davis,Conor\n110,Davis,David Wayne\n111,Davis,Lee\n112,Davor,HRG\n113,DeJarnett,Philip\n114,Della-Franca,Sean\n115,Demay,Vincent\n116,Dierendonck,Gergoire\n117,Dimmick,Damon\n118,Distefano,Salvatoie\n119,Dominguez,Mariano\n120,Dondich,Taylor\n121,Dorn,Christoph\n122,Doughtie,Gavin\n123,Drew,David\n124,Dubois,Julien\n125,Dubon,Stephane\n126,Dudzik,Michael\n127,Duivenbode,Lance\n128,Dunklau,Ronan\n129,Dvoryansky,Leonid\n130,Dwelle,Tim\n131,Eagan,Sean\n132,Easterbrook,Zachary\n133,Edmondson,Paul\n134,Eernisse,Matthew\n135,Elliot,Thomas\n136,Ersoy,Ole\n137,Evans,Aaron M.\n138,Fabritz,Jason\n139,Fabulich,Daniel Grijalva\n140,Fair,Seth\n141,Falduto,Ariel O.\n142,Fan,Bertrand\n143,Feldman,David A.\n144,Fenwick,Nicholas\n145,Ferrari,Alessandro\n146,Ferrydiansyah,Reza\n147,Field,Terry\n148,Firsov,Olekasandr\n149,Fitzgerald,Nick\n150,Fitzgerald,Patrick\n151,Follia,Pierpaolo\n152,Forbes,Bryan\n153,Forrester,Ron\n154,Fortson,Frank\n155,Foster,Sam\n156,Foulds,Ian\n157,Fox,Pat\n158,France Telecom,\n159,Franks,Carl\n160,Franqueiro,Kenneth G.\n161,Franz,Martin\n162,Frohman,Lance\n163,Fryer,Anthony\n164,Fudge,David\n165,Galipo,Don\n166,Ganegedara,Hiran Shyanaka\n167,Garcia,John\n168,Gardner,Brett\n169,Garfield,Justin\n170,Garner,Sean\n171,Garrioch,Robb N.\n172,Ge,David\n173,Gerber,Achim\n174,Giammarchi,Andrea\n175,Gielczynski,Miriam\n176,Gilbert,Alain\n177,Gill,Rawld\n178,Girard,Wade\n179,Glebovskiy,Alexander\n180,Goessner,Stefan\n181,Goldstein,Peter\n182,Golebski,Marcin\n183,Goncharov,Mykyta Sergiyovych\n184,Gordon,Max\n185,Gorman,Mitch\n186,Gornick,Joseph Richard\n187,Grainger,Brendan\n188,Grandrath,Martin\n189,Green,Amit\n190,Greenberg,Jeffrey\n191,Grencik,Jozef\n192,Grimm,Steven\n193,Groth,Ryan\n194,Guillen,Revin\n195,Gupta,Aman\n196,Gwyn,Philip\n197,Hakansson,Finn\n198,Hale,Mark\n199,Hamilton,Gabe\n200,Hampton,Shawn\n201,Hanbanchong,Aphichit\n202,Hann,John M.\n203,Harris,Jeffrey\n204,Harrison,Matthew\n205,Harter,Laurie\n206,Hartmann,Michel\n207,Hashim,Ahmed\n208,Hayden,Jennifer\n209,Hayes,Kyle\n210,Hays,Jason Scott\n211,Heeringson,Jaanus\n212,Heil,Jerome\n213,Heimbuch,Ryan C.\n214,Henderson,Cal\n215,Hennebrueder,Sebastian\n216,Henricson,Mats\n217,Herrmann,Doug\n218,Hershberger,Matthew\n219,Hiester,Christopher\n220,Higgins,Peter\n221,Hitt,Jason\n222,Hixon,Alexander\n223,Hjelte,Henrik\n224,Hockey,Benjamin James\n225,Hofbauer,Christian\n226,Hoffman,Uwe\n227,Holm,Torkel\n228,Horoszowski,Matthew\n229,Horowitz,Richard\n230,Hu,Jian\n231,Huang,Ming Zhe\n232,Hudson,David\n233,Humphreys,Martin\n234,Hussenet,Claude\n235,Illit,Marcel\n236,Ionushonis,Victoria\n237,Ippolito,Robert\n238,Irish,Paul\n239,Irrschik,Manuels\n240,Irwin,Matthew\n241,Isik,Hakan\n242,Issakov,Antony\n243,James,Stephen\n244,Jekel,Peter\n245,Jenkins,Adam\n246,Jenkins,Scott\n247,Johansson,Fredrik\n248,Johansson,Niklas\n249,Johansyah,Robertus Harmawon\n250,Johns,Morris Peter\n251,Johnson,Aaron\n252,Johnson,Samuel B\n253,Joldersma,Benjamin\n254,Jones,Randall\n255,Jones,Russell\n256,Jonsson,Olle\n257,Joshi,Neil\n258,Julien,Mathevet\n259,Jurkiewicz,Jared\n260,Kaihol,Antti\n261,Kang,Huynh\n262,Kantor,Ilia\n263,Karr,David\n264,Katz,Omer\n265,Keese,Bill\n266,Kelly,Dirk\n267,Kime,Matthew\n268,Kimmel,Maximilian\n269,Kingma,D.\n270,Kings-Lynne,Christopher\n271,Kisel,Siarhey\n272,Klein,Stéphane\n273,Klubnik,Justin\n274,Knapp,Matthew\n275,Koberg,Robert S.\n276,Kokot,Peter\n277,Kolba,Nicholas\n278,Komarneni,Vamsikrishna\n279,Koonce,Grayson\n280,Krantz,Viktor\n281,Kress Jorg,\n282,Kriesing,Wolfram\n283,Kuhnert,\" George \"\"Jesse\"\"\"\n284,Kuka,Radovan\n285,Kulesa,Chad\n286,Kumar,Naresh\n287,Kuzmik,Roman\n288,Lain,Chih Chao\n289,Lam,Daniel\n290,Landolt,Dean\n291,Laparo,Craig\n292,Lapointe,Louis\n293,Lazutkin,Eugene\n294,Lear,Chris\n295,Lee,Laurence A.\n296,Leite,Kristian\n297,Lendvai,Attila\n298,Leonard,Jean-Rubin\n299,Leonardi,David\n300,Levinson,Todd\n301,Leydier,Thierry\n302,Li,Bin\n303,Lightbody,Patrick\n304,Linnenfelser,Marcel\n305,Lipps,Jonathan\n306,Liu,Heng\n307,Lodewick,Thomas\n308,Logemann,Marc\n309,Lokanuvat,Sakchai\n310,Lopes,Rui Godinho\n311,Lopez,Gerald\n312,Lorentsen,Bo\n313,Lowery,Ben\n314,Lucas,Brian\n315,Lulek,Marcin\n316,Lv,Yong\n317,Lynch,James William\n318,Lyon,Matt\n319,Lytle,Seth\n320,MacDonald,Jay John\n321,Machi,Dustin\n322,Maclennan,Caleb\n323,Madineni,Pradeep\n324,Maquire,Jordan\n325,Malage,Osandi Chirantra Midreviy\n326,Malpass,Ian Andrew\n327,Manninen,Juho\n328,Manteau,Pierre-Emmanuel\n329,Marginian,David\n330,Marginian,David Brian\n331,Mark,David\n332,Marko,Martin\n333,Marsh,John T. Jr.\n334,Martin,Benoit\n335,Martin,Nicholas\n336,Martinez,Jose Antonio\n337,Mason,Seth\n338,Mathias,Aaron\n339,Matzner Bernd,\n340,Mauger,Ryan\n341,Mautone,Steven\n342,Mayfield,Justin\n343,McCallister,Brian\n344,McCullough,Ryan\n345,McGee,Daniel\n346,McMaster,Doug\n347,McNab,David\n348,Medeiros,Miller H. Borges\n349,Melo,Vinicius\n350,Meschian,Rouben\n351,Metyas,Remoun\n352,Michelangeli,Enzo\n353,Michopoulos,Haris\n354,Migazzi,Pascal\n355,Mikula,Tomas\n356,Miles,Scott Joseph\n357,Mills,Drew\n358,Minarik,Andrej\n359,Moeller,Jonathan\n360,Mohan,Nirdesh\n361,Monroe,Daniel\n362,Montes,Luis\n363,Morawski,Jason\n364,Moreira,Jose\n365,Motovilov,Max\n366,Mott,Carla V.\n367,Muhlestein,Dennis\n368,Mullen,Patrick\n369,Municio,Angel\n370,Murphey,Rebecca\n371,Murray,Gregory Lee\n372,Nachbaur,Michael Alexander\n373,Nairn,Rowan\n374,Nakamura,Hioaki\n375,Nasonov,Igor\n376,Neden,Sean\n377,Nelson,Stephen\n378,Nepomnyashy,Marat\n379,Neuberg,Bradley Keith\n380,Newbill,Christopher\n381,Newlau,Andrei\n382,Newman,Joshua\n383,Nguyen,TA\n384,Nguyen,Thanh\n385,Noheda,Jose\n386,Nucera,Roberto\n387,O Shea,Sean\n388,Obermann,Gerhard\n389,Ogilvie,Cyan Jon\n390,Oliver,Vicky\n391,Ondrek,Samuel\n392,Onken,Nikolai\n393,Oriol,Guillaume\n394,Overton,James Alexander\n395,Oyapero,Owalabi\n396,Papayan,Vladislav\n397,Papineau,Jeff\n398,Parker,David\n399,Pasquier,Eric\n400,Pate,Benjamin\n401,Patil,Ashish\n402,Peart,Steve\n403,Peierls,Tim\n404,Penner,Robert\n405,Penniman,Cary\n406,Perdue,Crispin\n407,Pereira,Rom\n408,Perminov,Ilya\n409,Petrov,Stamen\n410,Phetra,Polawat\n411,Pillai,Anand I.\n412,Pliam,John\n413,Plumlee,Scott\n414,Popelo,Andrey\n415,Popescu,Alexandru\n416,Porcari,Giovanni\n417,Prakaptsou,Artsem\n418,Prevoteau,Eric\n419,Prokopiev,Eugene\n420,Pupius,Dan\n421,Pu,Li\n422,Rahalski,Vitali\n423,Rakovsky,Adrian\n424,Reed,Joshua Allen\n425,Rees,David\n426,Reicke,Craig\n427,Reimann,Marcus\n428,Remeika,Bob\n429,Repta,Martin\n430,Rhode,Devin\n431,Rhodes,Aaron\n432,Rice,Torrey\n433,Riley,William\n434,Rinehart,Randy\n435,Rizzo,Nicola\n436,Roberts,Baron\n437,Roberts,Neil\n438,Romero,Carlos\n439,Rought,Edward T.\n440,Rouse,Joseph\n441,Roy,Dibyendu\n442,Ruffles,Tim\n443,Ruggia,Pablo\n444,Ruggier,Mario\n445,Ruoss,Stefan\n446,Ruspini,Daniel\n447,Russell,Matthew A.\n448,Russell,\" Robert \"\"Alex\"\"\"\n449,Safiev,Anuarbek\n450,Sagolaev,Ivan\n451,Saint-Just Philippe,\n452,Sakar,Ahmet Taha\n453,Salipo,Dan\n454,Salminen,Jukka\n455,Salt,Kevin\n456,Sanders,Robert\n457,Santalucia,Benjamin\n458,Santovito,Filippo\n459,Saremi,Jeff\n460,Savage,Phillip\n461,Sayfullin,Robert\n462,Schall,Michael T.\n463,Schiemann,Dylan\n464,Schindler,William F.\n465,Schmidt,Andreas\n466,Scholz,Kyle\n467,Schontzler,David\n468,Schreiber,Maik\n469,Schuerig,Michael\n470,Schuster,Stefan\n471,Seeger,Chad\n472,Segal,Erel\n473,Sekharan,Satishkumar\n474,Semmens,Lance\n475,Sexton,Alexander\n476,Shah,Anjur\n477,Shah,Maulin\n478,Shamgin,Vladimir\n479,Shaver,Robert\n480,Shaw,Thomas R.\n481,Shi,Hong\n482,Shih,Kenneth\n483,Shimizu,Fumiyuki\n484,Shinnick,Thomas Loren\n485,Shneyderman,Alex\n486,Siemoneit,Oliver\n487,Simpson,Matt\n488,Simser,Daniel M.\n489,Sitter,Sean\n490,Skinner,Brian Douglas\n491,Smeets,Bram\n492,Smelkovs,Konrade\n493,Smith,Bradford Carl\n494,Smith,Donald Larry Jr.\n495,Smith,Kevin A\n496,Smith,Mark\n497,Smith,Michael J.\n498,Smith,Micheil\n499,Smith,Stephen\n500,Snopek,David\n501,Snover,Colin\n502,Sobol,Steve\n503,Solomenchuk,Vladimir\n504,Sorensen,Asael\n505,Sorensen,Matt\n506,Sotherland,Jamie\n507,Souzis,Adam\n508,Sowden,Paul\n509,Speich,Simon\n510,Squisky,\n511,Stallworthy,Phillip\n512,Stancapiano,Luca\n513,Stanfill,Erin\n514,Staravoitau,Aliaksei\n515,Staskawicz,Liam\n516,Staves,Aaron\n517,Stearns,Geoff\n518,Steenveld,Andre\n519,Stefaniuk,Daniel\n520,Steffensen,Gregory\n521,Stepanoski,Mauro Alberto\n522,Stern,David\n523,Stojic,Ivan\n524,Stott,Nathan Rains\n525,Strimpel,Jason\n526,Sulliman,Hani\n527,Sumilang,Richard\n528,Svensson,Peter\n529,Sykes,Jon\n530,Syndodinos,Dionysios\n531,Szklanny,Les\n532,Tan,Yi\n533,Tanfous,Hassen Ben\n534,Tangey,Greg\n535,Tarassenko,Gleb\n536,Taylor,Michael A.\n537,Taylor,Jamie\n538,Teer,Ellis\n539,Tempelton,Sean\n540,Teulings,Tijs\n541,Tiedt,Karl\n542,Tilley,Travis\n543,Tipling,Bjorn\n544,Todd,Aaron\n545,Toone,Nathan\n546,Trank,Aaron\n547,Trenka,Ron\n548,Trenka,Thomas\n549,Trutwin,Josh\n550,Tynan,Dylan\n551,Tyson,Matt\n552,Ukrop,Jakub\n553,Ultis,Jonathan\n554,Upton,Thomas\n555,Uren,Richard\n556,Vachou,Travis\n557,Valdelievre,Florent\n558,Valencia,Miguel Angel Perez\n559,Van De Sande,Brett\n560,Van Woerkom,Marc\n561,Vandenberg,John Mark\n562,VanderPlye,Nicholas\n563,Vantoll III,Theodore\n564,Veness,Chris\n565,Venkatachalam,Vidyasagar\n566,Vettervanta,\n567,Vichas,Deno\n568,Vincze,Gabor\n569,Visic,Mario\n570,Von Klipstein,Tobias\n571,Wagener,Peter\n572,Waite,Robert\n573,Walker,Andrew\n574,Walker,Joe\n575,Wallez,Sylvain\n576,Wang,Pei\n577,Wei,Coach\n578,Weinberger,Ferdinand\n579,Weisberg,Adrian\n580,Weiss,Robert\n581,Welte,Robert John\n582,Wenk,Norman\n583,Werner,Punz\n584,Whiteman,Todd\n585,Wiersma,Erik\n586,Wilcox,Mike\n587,Wildman,Allison\n588,Wilkins,Greg\n589,Wilkins,Gregory John\n590,Williams,David\n591,Williams,Jason\n592,Williams,Matthew Owens\n593,Williamson,Tim\n594,Wilson,Andrew\n595,Wilson,Mike\n596,Wood,Peter William Alfred\n597,Wooten,Isaac T.\n598,Wu,Thomas\n599,Wubben,Mark\n600,Wyss,Hannes F.\n601,Xi,Kun\n602,Xu,Xi\n603,Yarimagan,Ilgin\n604,Yeow,Cheah\n605,Young,Matthew\n606,Young,Rex\n607,Yu,Leon\n608,Zamir,Brett\n609,Zammetti,Frank\n610,Zastrow,Bettina\n611,Zboron,Lukas\n612,Zhang,Carrie\n613,Zhang,Stephen\n614,Zhang,Xiaoming\n615,Zhen,Wang\n616,Zhu,Wei\n617,Zipkin,Joel\n618,Zou,Ran\n619,Zyp,Kris",
'url:dstore/tests/data/noquote.csv':"id,last, first,middle,born,died\n3,Hawking,Stephen,William,1942-01-08,\n1,Einstein,Albert,,1879-03-14,1955-04-18\n2, Tesla, Nikola , ,1856-07-10, 1943-01-07\n",
'url:dstore/tests/data/quote.csv':"\n\"id\", \"name\", \"quote\" \n\"1\",\"Garfield\",\"Every time I think I've hit bottom, somebody throws me a shovel\"\n\"2\",\"Edgar Allan Poe\",\"Then this ebony bird beguiling my sad fancy into smiling,\nBy the grave and stern decorum of the countenance it wore,\n\"\"Though thy crest be shorn and shaven, thou,\"\" I said, \"\"art sure no craven,\nGhastly grim and ancient Raven wandering from the Nightly shore —\nTell me what thy lordly name is on the Night's Plutonian shore!\"\"\n    Quoth the Raven \"\"Nevermore.\"\"\"\n    \n\"3\", \"Steve\" , \"\"\"\"\"\"\n\"4\", \"Space Sphere\", \" S, P, ...ace! \"\n"}});
define("dstore/tests/Csv", [
	'intern!object',
	'intern/chai!assert',
	'dojo/_base/declare',
	'dstore/Csv',
	'dstore/Memory',
	'dojo/text!./data/noquote.csv',
	'dojo/text!./data/quote.csv',
	'dojo/text!./data/contributors.csv'
], function (registerSuite, assert, declare, Csv, Memory, noquote, quote, contributors) {
	var CsvMemory = declare([Memory, Csv]);
	var xhrBase = require.toUrl('dstore/tests/data');
	var csvs = {}; // holds retrieved raw CSV data
	var stores = {}; // holds stores created after CSV data is retrieved
	csvs.noQuote = noquote;
	stores.noQuoteNoHeader = new CsvMemory({
		data: noquote,
		newline: '\n',
		fieldNames: ['id', 'last', 'first', 'born', 'died']
	});
	stores.noQuoteWithHeader = new CsvMemory({
		data: noquote,
		newline: '\n'
		// No fieldNames; first row will be treated as header row.
	});
	stores.noQuoteTrim = new CsvMemory({
		data: noquote,
		newline: '\n',
		trim: true
	});
	csvs.quote = quote;
	stores.quoteNoHeader = new CsvMemory({
		data: quote,
		newline: '\n',
		fieldNames: ['id', 'name', 'quote']
	});
	stores.quoteWithHeader = new CsvMemory({
		data: quote,
		newline: '\n'
		// No fieldNames; first row will be treated as header row.
	});
	csvs.contributors = contributors;
	stores.contributors = new CsvMemory({
		data: contributors,
		newline: '\n'
	});
	registerSuite({
		name: 'dstore CSV',

		'no quote': function () {
			var noHeader = stores.noQuoteNoHeader,
				withHeader = stores.noQuoteWithHeader,
				trim = stores.noQuoteTrim,
				item, trimmedItem;

			// Test header vs. no header...
			assert.strictEqual(4, noHeader.data.length,
				'Store with fieldNames should have 4 items.');
			assert.strictEqual(3, withHeader.data.length,
				'Store using header row should have 3 items.');
			assert.strictEqual(5, noHeader.fieldNames.length,
				'Store with fieldNames should have 5 fields.');
			assert.strictEqual(6, withHeader.fieldNames.length,
				'Store using header row should have 6 fields.');
			assert.strictEqual('id', noHeader.getSync('id').id,
				'First line should be considered an item when fieldNames are set');
			assert.strictEqual('Albert', withHeader.getSync('1').first,
				'Field names picked up from header row should be trimmed.');
			assert.strictEqual(noHeader.getSync('1').last, withHeader.getSync('1').last,
				'Item with id of 1 should have the same data in both stores.');

			// Test trim vs. no trim...
			item = withHeader.getSync('2');
			trimmedItem = trim.getSync('2');
			assert.strictEqual(' Nikola ', item.first,
				'Leading/trailing spaces should be preserved if trim is false.');
			assert.strictEqual('Nikola', trimmedItem.first,
				'Leading/trailing spaces should be trimmed if trim is true.');
			assert.strictEqual(' ', item.middle,
				'Strings containing only whitespace should remain intact if trim is false.');
			assert.strictEqual('', trimmedItem.middle,
				'Strings containing only whitespace should be empty if trim is true.');

			// Test data integrity...
			item = withHeader.getSync('1');
			assert.isTrue(item.middle === '', 'Test blank value.');
			assert.strictEqual('1879-03-14', item.born, 'Test value after blank value.');
			assert.isTrue(withHeader.getSync('3').died === '', 'Test blank value at end of line.');
		},

		'quote': function () {
			var noHeader = stores.quoteNoHeader,
				withHeader = stores.quoteWithHeader;

			// Test header vs. no header...
			assert.strictEqual(5, noHeader.data.length,
				'Store with fieldNames should have 5 items.');
			assert.strictEqual(4, withHeader.data.length,
				'Store using header row should have 4 items.');
			assert.strictEqual('id', noHeader.getSync('id').id,
				'First line should be considered an item when fieldNames are set');
			assert.strictEqual(noHeader.getSync('1').name, withHeader.getSync('1').name,
				'Item with id of 1 should have the same data in both stores.');

			// Test data integrity...
			assert.strictEqual('""', withHeader.getSync('3').quote,
				'Value consisting of two double-quotes should pick up properly.');
			assert.strictEqual(' S, P, ...ace! ', withHeader.getSync('4').quote,
				'Leading/trailing spaces within quotes should be preserved.');
			assert.isTrue(/^Then[\s\S]*"Nevermore\."$/.test(withHeader.getSync('2').quote),
				'Multiline value should remain intact.');
			assert.isTrue(/smiling,\n/.test(withHeader.getSync('2').quote),
				'Multiline value should use same newline format as input.');
		},

		'import export': function () {
			assert.strictEqual(csvs.contributors, stores.contributors.toCsv(),
				'toCsv() should generate data matching original if it is well-formed');
		}
	});
});