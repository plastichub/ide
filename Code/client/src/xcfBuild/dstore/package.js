//>>built
var miniExcludes={"dstore/README.md":1,"dstore/package":1},isTestRe=/\/test\//,packages={};try{require(["util/build/buildControl"],function(a){packages=a.packages})}catch(a){console.error("Unable to retrieve packages for determining optional package support in dstore")}
var profile={resourceTags:{test:function(a,b){return isTestRe.test(a)},miniExclude:function(a,b){return/\/(?:tests|demos|docs)\//.test(a)||b in miniExcludes},amd:function(a,b){return/\.js$/.test(a)},copyOnly:function(a,b){return!packages.rql&&/RqlQuery\.js/.test(a)}}};
//# sourceMappingURL=package.js.map