/** @module xide/container/_ContainerBase **/
define("xide/container/_PaneBase", [
    "xdojo/has",
    "dcl/dcl",
    'xide/utils',
    "xide/_base/_Widget"
], function (has,dcl,utils,_Widget) {

    const Module = dcl(_Widget,{
        templateString:'<div/>',
        isContainer:true,
        declaredClass:'xide/container/_PaneBase',
        panelNode:null,
        selected:false,
        $toggleNode:null,
        $toggleButton:null,
        lazy:true,
        add:dcl.superCall(function(sup) {
            return function (mixed,options,parent,startup) {
                if(this.lazy && (mixed.allowLazy!==false && (options ? options.allowLazy!==false : true))){
                    startup = false;
                }
                return sup.apply(this, [mixed,options,parent,startup]);
            }
        }),
        addChild:function(what,mixed,startup){
            this.add(what,mixed);
            what.domNode && utils.addChild(this.containerNode,what.domNode);
            if(startup!==false && !what._started && what.startup){
                what.startup();
            }
        },
        unselect:function(){
            this.$toggleButton && this.$toggleButton.removeClass('active');
            this.$selectorNode && this.$selectorNode.removeClass('active');
            this.$containerNode && this.$containerNode.removeClass('active');
            this.selected = false;
        },
        select:function(){
            this.$toggleButton && this.$toggleButton.addClass('active');
            this.$selectorNode && this.$selectorNode.addClass('active');
            this.$containerNode && this.$containerNode.addClass('active');
            this._onShown();
            this.onSelect && this.onSelect();
        },
        destroy:function(){
            utils.destroy(this.$toggleButton[0]);
            utils.destroy(this.$containerNode[0]);
        },
        _checkWidgets:function(){},
        _onShown:function(){
            this.selected = true;
            this._startWidgets();
            this.resize();
            this.onShow();
            this.onSelect && this.onSelect();
            const thiz = this;
            setTimeout(function(){
                thiz.owner && thiz.owner.onShowTab(thiz);
            },1);
            this._emit('show',this);
        },
        _onShow:function(){
            this.selected = true;
            this.resize();
        },
        shouldResizeWidgets:function(){
            return this.selected;
        },
        _onHide:function(){
            this.selected = false;
            this.open = false;
            this.onHide();
        },
        _onHided:function(){
            this.open = false;
            this._emit('hide',{
                view:this
            });
        },
        hide:function(){
            const container = $(this.containerRoot);
            const toggleNode= $(this.toggleNode);

            toggleNode.addClass('collapsed');
            toggleNode.attr('aria-expanded',false);
            container.removeClass('collapse in');
            container.addClass('collapse');
            container.attr('aria-expanded',false);
            this.open = false;
        },
        show:function(){
            const container = $(this.containerRoot);
            const toggleNode = $(this.toggleNode);

            toggleNode.removeClass('collapsed');
            toggleNode.attr('aria-expanded',true);
            container.removeClass('collapse');
            container.addClass('collapse in');
            container.attr('aria-expanded',true);
            this.open = true;
        },
        getChildren:function(){
            //xmaqadd
            !this._widgets && (this._widgets = []);
            return this._widgets;
        },
        postMixInProperties:function(){
            const active = this.selected ? 'active' : '';
            this.templateString = '<div attachTo="containerNode" style="height:100%;width:100%;position:relative;" class="tab-pane ' + active + '"></div>';
        },
        __init:function(){
            const panel = this.$toggleNode;
            this.__addHandler(panel,'hidden.bs.tab','_onHided');
            this.__addHandler(panel,'hide.bs.tab','_onHide');
            this.__addHandler(panel,'shown.bs.tab','_onShown');
            this.__addHandler(panel,'show.bs.tab','_onShow');
        }
    });

    dcl.chainAfter(Module, "postMixInProperties");
    dcl.chainAfter(Module, "resize");
    dcl.chainAfter(Module, "show");

    return Module;

});