define("xide/PreComp", [
    'require'
], function (require) {
    return require([
        'dgrid/dgrid',
        'xfile/xfile',
        'xide/xide',
        'dojo/has!xblox?xblox/xblox'
    ], function () {});
});