define("xide/widgets/NativeWidget", [
    'dcl/dcl',
    "dojo/_base/declare",
    "xide/widgets/WidgetBase",
    "xide/factory"
], function (dcl, declare, WidgetBase, factory) {
    return dcl(WidgetBase, {
        declaredClass: 'xide.widgets.NativeWidget',
        value: "unset",
        _lastValue: null,
        _nativeHandles: false,
        widgetChanged: function (value) {
            this.changed = true;
            if (this.userData) {
                this.userData.changed = true;
            }
            this.setValue(value);
        },
        setupNativeWidgetHandler: function () {

            const thiz = this;

            if (this.nativeWidget && !this._nativeHandles) {
                this.nativeWidget._added = true;
                if (!this.nativeWidget._started) {
                    //console.error('native widget not started : ' + this.nativeWidget.declaredClass);
                    this.valueNode.innerHTML = "";
                    this.valueNode.appendChild(this.nativeWidget.domNode);
                    this.nativeWidget.startup();                    
                }

                this._nativeHandles = true;
                if (this.nativeWidget._on) {
                    this.nativeWidget._on("change", function (value) {
                        if (thiz._lastValue !== null && thiz._lastValue === value) {
                            return;
                        }
                        thiz._lastValue = value;
                        thiz.widgetChanged(this);

                    });
                } else {
                    this.addHandle("change", this.nativeWidget.on("change", function (value) {
                        if (thiz._lastValue !== null && thiz._lastValue === value) {
                            return;
                        }
                        thiz._lastValue = value;
                        thiz.widgetChanged(this);

                    }));
                }
                this.addHandle("blur", this.nativeWidget.on("blur", function () {
                    thiz.setActive(false);
                }));
                this.addHandle("focus", this.nativeWidget.on("focus", function () {
                    thiz.setActive(true);
                }));
            }
        },
        startup: function () {
            const data = this.userData;
            const type = this.type;
            const label = data.title || data.name;
            const value = this.value;
            let $root = $(this.valueNode);
            const thiz = this;
            if (type) {
                this.valueNode.innerHTML = "";
                this.titleNode.innerHTML = "";
                if (type === 'CheckBox') {
                    $root = $(this.titleNode);
                    const id = this.id + '_Checkbox';
                    let element = '';
                    element += '<div class="checkbox checkbox-success ">';
                    element += '<input id="' + id + '" type="checkbox" ' + (value == true ? 'checked' : '') + '>';
                    element += '<label for="' + id + '">';
                    element += this.localize(label) + '</label>';
                    element += '</div>';
                    const $widget = $(element);
                    $root.append($widget);
                    const $nativeWidget = $widget.find('INPUT');
                    this.__on($nativeWidget, 'change', null, function () {
                        const _value = $nativeWidget[0].checked;
                        if (thiz._lastValue !== null && thiz._lastValue === _value) {
                            return;
                        }
                        thiz._lastValue = _value;
                        thiz.widgetChanged(_value);
                    });
                }
            }
            this.onReady();
        }
    });
});