//>>built
define("xide/widgets/TemplatedWidgetBase",["dcl/dcl","xide/utils","xide/_base/_Widget"],function(b,c,a){a=b([a],{declaredClass:"xide.widgets.TemplatedWidgetBase",data:null,delegate:null,didLoad:!1,templateString:null,getParent:function(){return this._parent},debounce:function(a,b,d,e,f){return c.debounce(this,a,b,d,e,f)},translate:function(a){return this.localize(a)},_setupTranslations:function(){this._messages=[]},updateTitleNode:function(a){}});b.chainAfter(a,"startup");b.chainAfter(a,"destroy");
return a});
//# sourceMappingURL=TemplatedWidgetBase.js.map