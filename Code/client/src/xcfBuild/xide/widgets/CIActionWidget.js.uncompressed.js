define("xide/widgets/CIActionWidget", [
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",
    "dojo/dom-construct",
    "dojo/_base/lang",
    "dojo/string",
    "xide/_base/_Widget",
    "xide/views/_Dialog",

    "xfile/views/FileOperationDialog",
    "dojo/Deferred",
    'xide/views/CIView',
    'xide/views/_CIDialog',
    'xfile/views/FilePicker',
    'xfile/Breadcrumb',
    'xide/form/Select',
    'xide/factory'
], function (declare, dcl, types,
    utils, Grid, TemplatedWidgetBase, EventedMixin,
    TestUtils, FTestUtils, _Widget, registry, module,
    cache, domConstruct, lang, string, _XWidget, _Dialog,
    FileOperationDialog, Deferred, CIView, _CIDialog, FilePicker, Breadcrumb,
    Select, factory
) {

    const widgetClass = dcl(TemplatedWidgetBase, {
        templateString: '<div class="CIActionWidget"></div>',
        widgetClass: ''
    });
    const actionWidget = dcl(widgetClass, {
        cis: [

        ],
        startup: function () {
            const dfd = factory.renderCIS(this.cis, this.domNode, this);
            const self = this;
            dfd.then((widgets) => {
                widgets.forEach((w) => {
                    w._on('change', (what) => {
                        action.set('value', what);
                        console.log('changed', action.get('value'));
                    })

                })
            });
        },
        render: function (data, $menu) {
            console.log('render');
        }
    });


    var ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON;

    var grid = FTestUtils.createFileGrid('root', {},
        //overrides
        {
            getFileActions: function (permissions) {
                permissions = permissions || this.permissions;
                var result = [],
                    ACTION = types.ACTION,
                    ACTION_ICON = types.ACTION_ICON,
                    VISIBILITY = types.ACTION_VISIBILITY,
                    thiz = this,
                    ctx = thiz.ctx,
                    container = thiz.domNode,
                    resourceManager = ctx.getResourceManager(),
                    vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') : {}, //possibly resourceManager not there in some builds
                    actionStore = thiz.getActionStore();

                ///////////////////////////////////////////////////
                //
                //  misc
                //
                ///////////////////////////////////////////////////
                result.push(thiz.createAction({
                    label: 'CIAction',
                    command: 'File/CI',
                    icon: ACTION_ICON.GO_UP,
                    tab: 'Home',
                    group: 'Navigation',
                    mixin: {
                        quick: true,
                        addPermission: true
                    },
                    onCreate: function (action) {

                        action.setVisibility(types.ACTION_VISIBILITY_ALL, {
                            widgetClass: actionWidget,
                            closeOnClick: false,
                            widgetArgs: {}
                        });
                    }
                }));
                return result;
            },
            runAction: function (action) {
                console.log('run action');
                //return runAction.apply(this,[action]);
                var res = this.inherited(arguments);

                var _resInner = runAction.apply(this, [action]);

                return _resInner || res;
            }

        }, 'TestGrid', module.id, true, parent);
})

// doTests(parent, grid);
return declare('a', null, {});