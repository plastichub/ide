define("xide/widgets/_CacheMixin", [
    'dcl/dcl',
    'dojo/_base/declare',
    "dojo/dom-style"
], function (dcl, declare, domStyle) {

    const _getStruct = function (widget, where) {
        if (!widget) {
            return null;
        }
        const node = widget.domNode || widget;
        const widgetCache = dojo.byId(where);

        if (!node || !widgetCache) {
            return null;
        }
        return {
            node: node,
            target: widgetCache
        }
    };

    return dcl(null, {
        declaredClass: "xide/widgets/_CacheMixin",
        cacheRoot: 'widgetCache',
        park: function (widget) {

            const _struct = _getStruct(widget, this.cacheRoot);
            if (!_struct) {
                return false;
            }
            domStyle.set(_struct.node, 'display', 'node');
            dojo.place(_struct.node, _struct.target);
            return true;
        },
        unpark: function (widget, where) {
            const _struct = _getStruct(widget, this.cacheRoot);
            if (!_struct) {
                return false;
            }
            domStyle.set(_struct.node, 'display', '');
            dojo.place(_struct.node, where);
            return true;
        }
    });
});