//>>built
define("xide/widgets/_CacheMixin",["dcl/dcl","dojo/_base/declare","dojo/dom-style"],function(e,f,c){var d=function(a,b){if(!a)return null;a=a.domNode||a;b=dojo.byId(b);return a&&b?{node:a,target:b}:null};return e(null,{declaredClass:"xide/widgets/_CacheMixin",cacheRoot:"widgetCache",park:function(a){a=d(a,this.cacheRoot);if(!a)return!1;c.set(a.node,"display","node");dojo.place(a.node,a.target);return!0},unpark:function(a,b){a=d(a,this.cacheRoot);if(!a)return!1;c.set(a.node,"display","");dojo.place(a.node,
b);return!0}})});
//# sourceMappingURL=_CacheMixin.js.map