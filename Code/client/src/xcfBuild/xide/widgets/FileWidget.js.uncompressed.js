define("xide/widgets/FileWidget", [
    'dcl/dcl',
    "xide/widgets/WidgetBase",
    "xide/widgets/EditBox",
    'xide/factory',
    'xide/utils',
    'xfile/FileActions',
    "xfile/views/FileGridLight",
    "dojo/Deferred",
    "xdojo/has",
    'xide/mixins/Electron'
], function (dcl, WidgetBase, EditBox, factory, utils, FileActions,
             FileGridLight, Deferred,has,_Electron) {

    const isElectron =  true  && has('electronx');

    return dcl([WidgetBase], {
        declaredClass: "xide.widgets.FileWidget",
        oriPath: null,
        minHeight: "54px;",
        value: "",
        options: null,
        dialogTitle: 'Select Script',
        editBox: null,
        templateString: "<div class='widgetContainer widgetBorder widgetTable widget' style=''>" +
        "<table border='0' cellpadding='5px' width='100%' >" +
        "<tbody>" +
        "<tr attachTo='extensionRoot'>" +
        "<td width='15%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
        "<td width='80px' class='widgetValue2' valign='middle' attachTo='previewNode'></td>" +
        "<td class='extension' width='25px' attachTo='button0'></td>" +
        "<td class='extension' width='25px' attachTo='button1'></td>" +
        "<td class='extension' width='25px' attachTo='button2'></td>" +
        "</tr>" +
        "</tbody>" +
        "</table>" +
        "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
        "<div attachTo='last'></div>" +
        "</div>",
        filePathValidator: function (value, constraints) {
            return true;
        },
        onFileSelected: function (dlg, item) {
            if (item) {
                const acceptFiles = utils.toBoolean(this.userData['acceptFiles']);
                const acceptFolders = utils.toBoolean(this.userData['acceptFolders']);
                const encodeFilePath = utils.toBoolean(this.userData['encodeFilePath']);
                const buildFullPath = utils.toBoolean(this.userData['buildFullPath']);
                const isDirectory = item.directory === true;

                if (isDirectory && !acceptFolders) {
                    return;
                } else if (!isDirectory && !acceptFiles) {
                    return;
                }
                this.userData.changed = true;
                this.userData.active = true;
                const _newPath = buildFullPath ? utils.buildPath(item.mount, item.path, encodeFilePath) : item.path;
                this.editBox.set('value', _newPath);
                this.value = _newPath;
                this.setValue(_newPath);
            }
        },
        getOptions: function () {
            const ctx = this.ctx || window['xFileContext'];
            let _defaultOptions = {
                title: this.dialogTitle,
                owner: ctx,
                dst: {
                    name: 'Select',
                    path: '.'
                },
                src: '.',
                ctx: ctx
            };
            const options = this.options || ctx.defaultOptions;
            if (options) {
                _defaultOptions = utils.mixin(_defaultOptions, options);
                console.dir(options);
            } else {
                console.error('have no options');
            }

            //there are file picker options in the CI?
            const userOptions = this.userData ? this.userData.filePickerOptions : {};
            if (userOptions) {
                _defaultOptions = utils.mixin(_defaultOptions, userOptions);
            }

            //we actually need a new store with different options
            if (_defaultOptions.store && _defaultOptions.defaultStoreName) {

                if (!userOptions || !userOptions.defaultStoreOptions) {
                    _defaultOptions.defaultStoreOptions = {
                        "fields": 1663,
                        "includeList": "*",
                        "excludeList": "*"
                    };
                }
                //get a new store
                _defaultOptions.store = ctx.getStore(_defaultOptions.defaultStoreName, _defaultOptions.defaultStoreOptions);

            } else if (userOptions && userOptions.defaultStoreOptions) {
                _defaultOptions.store = ctx.getStore(_defaultOptions.defaultStoreName, userOptions.defaultStoreOptions);
            }

            return _defaultOptions;
        },
        onSelect: function () {
            const self = this;
            utils.destroy(this.picker);
            if (this._onSelect) {
                const _dlgDfd = this._onSelect(this);
                if (_dlgDfd.then) {
                    _dlgDfd.then(function (val) {
                        self.editBox.set('value', val);
                        self.setValue(val);
                    });
                }
                return _dlgDfd;
            }
            const defaultOptions = this.getOptions();
            const userData = this.userData;
            const pickerOptions = userData.filePickerOptions || {};

            try {
                function done(item, selection, picker) {
                    const firstItem = item;
                    const val = firstItem.realPath || firstItem.path;
                    self.editBox.set('value', val);
                    self.setValue(val);
                }
                const dfd = new Deferred();
                if(isElectron && pickerOptions.allowElectron===true){
                    const Electron = new _Electron();
                    const fs = Electron.srequire('fs');
                    const remote = Electron.remote();
                    const dialog = remote.dialog;
                    let directory = dialog.showOpenDialog({
                        properties: ['openDirectory'],
                        defaultPath:this.userData.value || '',
                        title: 'Select ' + this.userData.title
                    });
                    if (!directory) {
                        console.log('Open Folder: have no directory, abort');
                        return;
                    }
                    directory = directory[0];
                    self.editBox.set('value', directory);
                    self.setValue(directory);
                }else {
                    FileActions.createFilePicker(this, "", done, 'Select File', null, defaultOptions, dfd, FileGridLight);
                }
            } catch (e) {
                console.error('file widget crash : ' + e);
                logError(e);
            }
        },
        set: function (what, value) {
            if (this.editBox && what === 'value') {
                this.editBox.set(what, value);
            }
            return this.inherited(arguments);
        },
        postMixInProperties: function () {
            if (this.userData && this.userData.widget && this.userData.widget.title) {
                this.title = this.userData.widget.title;
            }
            if ((this.userData && this.userData.vertical === true) || this.vertical === true) {
                this.templateString = "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
                    "<table border='0' cellpadding='5px' width='100%' >" +
                    "<tbody>" +
                    "<tr attachTo='extensionRoot'>" +
                    "<td width='100%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
                    "</tr>" +
                    "<tr attachTo='extensionRoot'>" +
                    "<td width='100px' class='widgetValue2' valign='middle' attachTo='previewNode'></td>" +
                    "<td class='extension' width='25px' attachTo='button0'></td>" +
                    "<td class='extension' width='25px' attachTo='button1'></td>" +
                    "<td class='extension' width='25px' attachTo='button2'></td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>" +
                    "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
                    "<div attachTo='last'></div>" +
                    "</div>"
            }

            return this.inherited(arguments);
        },
        fillTemplate: function () {
            const value = utils.toString(this.userData['value']);
            const self = this;

            const editBox = utils.addWidget(EditBox, {
                userData: this.userData,
                title: ''
            }, null, this.previewNode, true);

            this.editBox = editBox;
            editBox._on('change', function (val) {
                self.setValue(val);
            })
            const btn = factory.createSimpleButton('', 'fa-folder', 'btn-default', {
                style: ''
            });
            $(btn).click(function () {
                self.onSelect();
            })
            $(this.button0).append(btn);

            this.add(editBox);
        },
        startup: function () {
            this.inherited(arguments);
            this.fillTemplate();
            this.onReady();
        }
    });
});