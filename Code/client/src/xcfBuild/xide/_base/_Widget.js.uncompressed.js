/**
 * @module xide/_base/_Widget
 */
define("xide/_base/_Widget", [
    'dcl/dcl',
    "dcl/inherited",
    'xide/widgets/_Widget',
    "xide/utils",
    "dojo/string",
    "dojo/_base/lang",
    "xide/registry",
    "dojo/cache",
    "dojo/dom-construct",
    'xide/lodash',
    'xide/$'
], function (dcl, inherited, _Widget, utils, string, lang, registry, cache, domConstruct, _, $) {

    function destroy(w, preserveDom) {
        if (w.destroyRecursive) {
            w.destroyRecursive(preserveDom);
        } else if (w.destroy) {
            w.destroy(preserveDom);
        }
    }

    /////////////////////////////////////////////////////////////////
    //
    //  Attach Mixin Class
    //
    const attachAttribute = 'attachTo';
    /**
     *		Mixin for widgets to attach to dom nodes and setup events via
     *		convenient data-dojo-attach-point and data-dojo-attach-event DOM attributes.
     *
     *		Superclass of _TemplatedMixin, and can also be used standalone when templates are pre-rendered on the
     *		server.
     *
     *		Does not [yet] handle widgets like ContentPane with this.containerNode set.   It should skip
     *		scanning for data-dojo-attach-point and data-dojo-attach-event inside this.containerNode, but it
     *		doesn't.


     * _attachPoints: [private] String[]
     *		List of widget attribute names associated with data-dojo-attach-point=... in the
     *		template, ex: ["containerNode", "labelNode"]
     _attachPoints: [],

     * _attachEvents: [private] Handle[]
     *		List of connections associated with data-dojo-attach-event=... in the
     *		template
     _attachEvents: [],

     * attachScope: [public] Object
     *		Object to which attach points and events will be scoped.  Defaults
     *		to 'this'.
     attachScope: undefined,
     */
    const _AttachMixinClass = dcl(null, {
        __attachAsjQueryObject: true,
        __attachViaAddChild: true,
        __stopAtContainerNode: false,
        _started: false,
        //attachDirect:true,
        declaredClass: "xide/_base/_AttachMixin",
        cssClass: '',
        /**
         * Attach to DOM nodes marked with special attributes.
         */
        buildRendering: function () {
            this._attachPoints = [];
            // recurse through the node, looking for, and attaching to, our
            // attachment points and events, which should be defined on the template node.
            this._attachTemplateNodes(this.domNode);
            this._beforeFillContent(); // hook for _WidgetsInTemplateMixin
            this.cssClass && this.domNode && $(this.domNode).addClass(this.cssClass);
        },
        _beforeFillContent: function () {},
        /**
         * Iterate through the dom nodes and attach functions and nodes accordingly.
         * @description Map widget properties and functions to the handlers specified in
         *		the dom node and it's descendants. This function iterates over all
         *		nodes and looks for these properties:
         *	    - attachTo
         * @param rootNode {HTMLElement}
         **/
        _attachTemplateNodes: function (rootNode) {
            // DFS to process all nodes except those inside of this.containerNode
            let node = rootNode;
            while (true) {
                if (
                    node.nodeType == 1 &&
                    (this._processTemplateNode(node, function (n, p) {
                        return n.getAttribute(p);
                    }, this._attach)) &&
                    node.firstChild
                ) {
                    node = node.firstChild;
                } else {
                    if (node == rootNode) {
                        return;
                    }
                    while (!node.nextSibling) {
                        node = node.parentNode;
                        if (node == rootNode) {
                            return;
                        }
                    }
                    node = node.nextSibling;
                }
            }
        },

        _processTemplateNode: function (baseNode, getAttrFunc, attachFunc) {
            // summary:
            //		Process data-dojo-attach-point and data-dojo-attach-event for given node or widget.
            //		Returns true if caller should process baseNode's children too.
            let ret = true;

            // Process data-dojo-attach-point
            const _attachScope = this.attachScope || this;

            const attachPoint = getAttrFunc(baseNode, attachAttribute);

            if (attachPoint) {
                let point;
                const points = attachPoint.split(/\s*,\s*/);
                while ((point = points.shift())) {
                    if (_.isArray(_attachScope[point])) {
                        _attachScope[point].push(baseNode);
                    } else {
                        _attachScope[point] = baseNode;
                        this.__attachAsjQueryObject && (_attachScope['$' + point] = $(baseNode));
                    }
                    ret = this.__stopAtContainerNode ? (point != "containerNode") : ret;
                    this._attachPoints.push(point);
                }
            }
            return ret;
        },
        /**
         * Detach and clean up the attachments made in _attachtempalteNodes.
         * @private
         */
        _detachTemplateNodes: function () {
            // Delete all attach points to prevent IE6 memory leaks.
            const _attachScope = this.attachScope || this;
            this._attachPoints.forEach((point) => {
                delete _attachScope[point];
            });
            this._attachPoints = [];
        },
        destroyRendering: function () {
            this._detachTemplateNodes();
            this.inherited && this.inherited(arguments);
        },
        startup: dcl.superCall(function (sup) {
            return function () {
                let res = null;
                if (sup) {
                    res = sup.call(this);
                }
                this._started = true;
                return res;
            };
        })
    });
    /////////////////////////////////////////////////////////////////
    //
    //  Templated Mixin Class
    //
    //
    const _TemplatedMixin = dcl(_AttachMixinClass, {
        declaredClass: "xide/_base/_TemplatedMixin",
        // summary:
        //		Mixin for widgets that are instantiated from a template
        // templateString: [protected] String
        //		A string that represents the widget template.
        //		Use in conjunction with dojo.cache() to load from a file.
        templateString: null,
        // templatePath: [protected deprecated] String
        //		Path to template (HTML file) for this widget relative to dojo.baseUrl.
        //		Deprecated: use templateString with require([... "dojo/text!..."], ...) instead
        templatePath: null,
        // skipNodeCache: [protected] Boolean
        //		If using a cached widget template nodes poses issues for a
        //		particular widget class, it can set this property to ensure
        //		that its template is always re-built from a string
        _skipNodeCache: false,
        /*=====
         // _rendered: Boolean
         //		Not normally use, but this flag can be set by the app if the server has already rendered the template,
         //		i.e. already inlining the template for the widget into the main page.   Reduces _TemplatedMixin to
         //		just function like _AttachMixin.
         _rendered: false,
         =====*/
        _stringRepl: function (tmpl) {
            // summary:
            //		Does substitution of ${foo} type properties in template string
            // tags:
            //		private
            const className = this.declaredClass;

            const _this = this;
            // Cache contains a string because we need to do property replacement
            // do the property replacement
            return string.substitute(tmpl, this, function (value, key) {
                if (key.charAt(0) == '!') {
                    value = lang.getObject(key.substr(1), false, _this);
                }
                if (typeof value == "undefined") {
                    const error = new Error(className + " template:" + key);
                    logError(error);
                } // a debugging aide
                if (value == null) {
                    return "";
                }

                // Substitution keys beginning with ! will skip the transform step,
                // in case a user wishes to insert unescaped markup, e.g. ${!foo}
                return key.charAt(0) == "!" ? value : this._escapeValue("" + value);
            }, this);
        },
        _escapeValue: function ( /*String*/ val) {
            // summary:
            //		Escape a value to be inserted into the template, either into an attribute value
            //		(ex: foo="${bar}") or as inner text of an element (ex: <span>${foo}</span>)

            // Safer substitution, see heading "Attribute values" in
            // http://www.w3.org/TR/REC-html40/appendix/notes.html#h-B.3.2
            // and also https://www.owasp.org/index.php/XSS_%28Cross_Site_Scripting%29_Prevention_Cheat_Sheet#RULE_.231_-_HTML_Escape_Before_Inserting_Untrusted_Data_into_HTML_Element_Content
            return val.replace(/["'<>&]/g, function (val) {
                return {
                    "&": "&amp;",
                    "<": "&lt;",
                    ">": "&gt;",
                    "\"": "&quot;",
                    "'": "&#x27;"
                }[val];
            });
        },
        /*
         * @description Construct the UI for this widget from a template, setting this.domNode.
         */
        buildRendering: dcl.superCall(function (sup) {
            return function () {
                if (!this._rendered) {
                    if (!this.templateString) {
                        this.templateString = cache(this.templatePath, {
                            sanitize: true
                        });
                    }
                    // Lookup cached version of template, and download to cache if it
                    // isn't there already.  Returns either a DomNode or a string, depending on
                    // whether or not the template contains ${foo} replacement parameters.
                    const cached = _TemplatedMixin.getCachedTemplate(this.templateString, this._skipNodeCache, this.ownerDocument);
                    let node;
                    if (_.isString(cached)) {
                        node = $(this._stringRepl(cached))[0];
                        if (node.nodeType != 1) {
                            // Flag common problems such as templates with multiple top level nodes (nodeType == 11)
                            throw new Error("Invalid template: " + cached);
                        }
                    } else {
                        // if it's a node, all we have to do is clone it
                        node = cached.cloneNode(true);
                    }
                    this.domNode = node;
                }

                // Call down to _WidgetBase.buildRendering() to get base classes assigned
                sup.call(this, arguments);
                if (!this._rendered) {
                    this._fillContent(this.srcNodeRef);
                }
                this._rendered = true;
                if (this.domNode && this.declaredClass) {
                    $(this.domNode).addClass(utils.replaceAll('/', '.', this.declaredClass));
                }
            };
        }),
        /**
         *
         * @param source {HTMLElement}
         * @private
         */
        _fillContent: function (source) {
            // summary:
            //		Relocate source contents to templated container node.
            //		this.containerNode must be able to receive children, or exceptions will be thrown.
            // tags:
            //		protected
            const dest = this.containerNode;
            if (source && dest) {
                while (source.hasChildNodes()) {
                    dest.appendChild(source.firstChild);
                }
            }
        }

    });

    // key is templateString; object is either string or DOM tree
    _TemplatedMixin._templateCache = {};
    _TemplatedMixin.getCachedTemplate = function (templateString, alwaysUseString, doc) {
        // summary:
        //		Static method to get a template based on the templatePath or
        //		templateString key
        // templateString: String
        //		The template
        // alwaysUseString: Boolean
        //		Don't cache the DOM tree for this template, even if it doesn't have any variables
        // doc: Document?
        //		The target document.   Defaults to document global if unspecified.
        // returns: Mixed
        //		Either string (if there are ${} variables that need to be replaced) or just
        //		a DOM tree (if the node can be cloned directly)

        // is it already cached?
        const tmplts = _TemplatedMixin._templateCache;
        const key = templateString;
        const cached = tmplts[key];
        if (cached) {
            try {
                // if the cached value is an innerHTML string (no ownerDocument) or a DOM tree created within the
                // current document, then use the current cached value
                if (!cached.ownerDocument || cached.ownerDocument == (doc || document)) {
                    // string or node of the same document
                    return cached;
                }
            } catch (e) { /* squelch */ } // IE can throw an exception if cached.ownerDocument was reloaded
            domConstruct.destroy(cached);
        }

        templateString = _.trim(templateString);

        if (alwaysUseString || templateString.match(/\$\{([^\}]+)\}/g)) {
            // there are variables in the template so all we can do is cache the string
            return (tmplts[key] = templateString); //String
        } else {
            // there are no variables in the template so we can cache the DOM tree
            const node = domConstruct.toDom(templateString, doc);
            if (node.nodeType != 1) {
                throw new Error("Invalid template: " + templateString);
            }
            return (tmplts[key] = node); //Node
        }
    };

    /////////////////////////////////////////////////////////////////
    //
    //  Actual Widget base class, adding an API
    //
    function createClass_WidgetBase() {
        const tagAttrs = {};

        function getAttrs(obj) {
            const ret = {};
            for (const attr in obj) {
                ret[attr.toLowerCase()] = true;
            }
            return ret;
        }

        function isEqual(a, b) {
            //	summary:
            //		Function that determines whether two values are identical,
            //		taking into account that NaN is not normally equal to itself
            //		in JS.
            return a === b || ( /* a is NaN */ a !== a && /* b is NaN */ b !== b);
        }
        /**
         * @class module:xide/_base/_Widget
         */
        const Module = dcl(null, {
            _attrPairNames: {}, // shared between all widgets
            attributeMap: {},
            declaredClass: 'xide/_base/_Widget',
            on: function (type, handler) {
                return this._on(type, handler);
            },
            emit: function (type, args) {
                return this._emit(type, args);
            },
            _set: function ( /*String*/ name, /*anything*/ value) {
                // summary:
                //		Helper function to set new value for specified property, and call handlers
                //		registered with watch() if the value has changed.
                const oldValue = this[name];
                this[name] = value;
                if (this._created && !isEqual(oldValue, value)) {
                    this._watchCallbacks && this._watchCallbacks(name, oldValue, value);
                    this.emit("attrmodified-" + name, {
                        detail: {
                            prevValue: oldValue,
                            newValue: value
                        }
                    });
                }
            },
            /**
             * Helper function to get value for specified property stored by this._set(),
             * i.e. for properties with custom setters.  Used mainly by custom getters.
             *  For example, CheckBox._getValueAttr() calls this._get("value").
             * @param name {string}
             * @returns {*}
             * @private
             */
            _get: function (name) {
                // future: return name in this.props ? this.props[name] : this[name];
                return this[name];
            },
            /**
             *  Helper function for get() and set().
             *  Caches attribute name values so we don't do the string ops every time.
             * @param name
             * @returns {*}
             * @private
             */
            _getAttrNames: function (name) {
                const apn = this._attrPairNames;
                if (apn[name]) {
                    return apn[name];
                }
                const uc = name.replace(/^[a-z]|-[a-zA-Z]/g, function (c) {
                    return c.charAt(c.length - 1).toUpperCase();
                });
                return (apn[name] = {
                    n: name + "Node",
                    s: "_set" + uc + "Attr", // converts dashes to camel case, ex: accept-charset --> _setAcceptCharsetAttr
                    g: "_get" + uc + "Attr",
                    l: uc.toLowerCase() // lowercase name w/out dashes, ex: acceptcharset
                });
            },
            /**
             * Set a property on a widget
             * @description Sets named properties on a widget which may potentially be handled by a setter in the widget.
             *
             *		For example, if the widget has properties `foo` and `bar`
             *		and a method named `_setFooAttr()`, calling
             *		`myWidget.set("foo", "Howdy!")` would be equivalent to calling
             *		`widget._setFooAttr("Howdy!")` and `myWidget.set("bar", 3)`
             *		would be equivalent to the statement `widget.bar = 3;`
             *
             *      This is equivalent to calling `set(foo, "Howdy")` and `set(bar, 3)`
             *
             *		set() may also be called with a hash of name/value pairs, ex:
             *
             *	@example
             *	myWidget.set({
                    foo: "Howdy",
                    bar: 3
                    });
             *
             * @param name {string} The property to set.
             * @param value {object|null}
             * @returns {*}
             */
            set: function (name, value) {
                if (typeof name === "object") {
                    for (const x in name) {
                        this.set(x, name[x]);
                    }
                    return this;
                }
                const names = this._getAttrNames(name);
                const setter = this[names.s];

                if (_.isFunction(setter)) {
                    // use the explicit setter
                    setter.apply(this, Array.prototype.slice.call(arguments, 1));
                } else {
                    // Mapping from widget attribute to DOMNode/subwidget attribute/value/etc.
                    // Map according to:
                    //		1. attributeMap setting, if one exists (TODO: attributeMap deprecated, remove in 2.0)
                    //		2. _setFooAttr: {...} type attribute in the widget (if one exists)
                    //		3. apply to focusNode or domNode if standard attribute name, excluding funcs like onClick.
                    // Checks if an attribute is a "standard attribute" by whether the DOMNode JS object has a similar
                    // attribute name (ex: accept-charset attribute matches jsObject.acceptCharset).
                    // Note also that Tree.focusNode() is a function not a DOMNode, so test for that.
                    const defaultNode = this.focusNode && !lang.isFunction(this.focusNode) ? "focusNode" : "domNode";

                    const tag = this[defaultNode] && this[defaultNode].tagName;
                    const attrsForTag = tag && (tagAttrs[tag] || (tagAttrs[tag] = getAttrs(this[defaultNode])));

                    const map = name in this.attributeMap ? this.attributeMap[name] :
                        names.s in this ? this[names.s] :
                        ((attrsForTag && names.l in attrsForTag && typeof value != "function") ||
                            /^aria-|^data-|^role$/.test(name)) ? defaultNode : null;

                    if (map != null) {
                        this._attrToDom(name, value, map);
                    }
                    this._set(name, value);
                }
                return this;
            },
            postCreate: function () {},
            postMixInProperties: dcl.superCall(function (sup) {
                return function (props) {
                    sup && sup.call(this, props);
                };
            }),
            create: function (params, srcNodeRef) {
                // summary:
                //		Kick off the life-cycle of a widget
                // description:
                //		Create calls a number of widget methods (postMixInProperties, buildRendering, postCreate,
                //		etc.), some of which of you'll want to override. See http://dojotoolkit.org/reference-guide/dijit/_WidgetBase.html
                //		for a discussion of the widget creation lifecycle.
                //
                //		Of course, adventurous developers could override create entirely, but this should
                //		only be done as a last resort.
                // params: Object|null
                //		Hash of initialization parameters for widget, including scalar values (like title, duration etc.)
                //		and functions, typically callbacks like onClick.
                //		The hash can contain any of the widget's properties, excluding read-only properties.
                // srcNodeRef: DOMNode|String?
                //		If a srcNodeRef (DOM node) is specified:
                //
                //		- use srcNodeRef.innerHTML as my contents
                //		- if this is a behavioral widget then apply behavior to that srcNodeRef
                //		- otherwise, replace srcNodeRef with my generated DOM tree
                // tags:
                //		private

                // First time widget is instantiated, scan prototype to figure out info about custom setters etc.
                //this._introspect();

                // store pointer to original DOM tree
                this.srcNodeRef = $(srcNodeRef)[0];

                // No longer used, remove for 2.0.
                this._connects = [];
                this._supportingWidgets = [];

                // this is here for back-compat, remove in 2.0 (but check NodeList-instantiate.html test)
                if (this.srcNodeRef && this.srcNodeRef.id) {
                    this.id = this.srcNodeRef.id;
                }

                // mix in our passed parameters
                if (params) {
                    this.params = params;
                    utils.mixin(this, params);
                }

                if (this.postMixInProperties) {
                    this.postMixInProperties();
                }

                // Generate an id for the widget if one wasn't specified, or it was specified as id: undefined.
                // Do this before buildRendering() because it might expect the id to be there.
                if (!this.id) {
                    this.id = registry.getUniqueId(this.declaredClass.replace(/\//g, "_"));
                    if (this.params) {
                        // if params contains {id: undefined}, prevent _applyAttributes() from processing it
                        delete this.params.id;
                    }
                }


                // The document and <body> node this widget is associated with
                this.ownerDocument = this.ownerDocument || (this.srcNodeRef ? this.srcNodeRef.ownerDocument : document);
                this.ownerDocumentBody = $('body')[0];
                registry.add(this);

                this.buildRendering();
                if (this.domNode) {
                    // Copy attributes listed in attributeMap into the [newly created] DOM for the widget.
                    // Also calls custom setters for all attributes with custom setters.
                    //this._applyAttributes();

                    // If srcNodeRef was specified, then swap out original srcNode for this widget's DOM tree.
                    // For 2.0, move this after postCreate().  postCreate() shouldn't depend on the
                    // widget being attached to the DOM since it isn't when a widget is created programmatically like
                    // new MyWidget({}).	See #11635.
                    const source = this.srcNodeRef;
                    if (source && source.parentNode && this.domNode !== source) {
                        source.parentNode.replaceChild(this.domNode, source);
                    }
                    // Note: for 2.0 may want to rename widgetId to dojo._scopeName + "_widgetId",
                    // assuming that dojo._scopeName even exists in 2.0
                    this.domNode.setAttribute("id", this.id);
                    if (this.style) {
                        $(this.domNode).css(this.style);
                    }
                }
                this.postCreate();
                this._created = true;
            },
            constructor: function (params, container) {
                this.postscript && this.postscript(params, container);
            },
            postscript: function (params, srcNodeRef) {
                return this.create(params, srcNodeRef);
            },
            /**
             *		Returns all direct children of this widget, i.e. all widgets underneath this.containerNode whose parent
             *		is this widget.   Note that it does not return all descendants, but rather just direct children.
             *		Analogous to [Node.childNodes](https:*developer.mozilla.org/en-US/docs/DOM/Node.childNodes),
             *		except containing widgets rather than DOMNodes.
             *
             *		The result intentionally excludes internally created widgets (a.k.a. supporting widgets)
             *		outside of this.containerNode.
             *
             *		Note that the array returned is a simple array.  Application code should not assume
             *		existence of methods like forEach().
             *
             * @returns {*}
             */
            getChildren: function () {
                return this.containerNode ? registry.findWidgets(this.containerNode) : []; // dijit/_WidgetBase[]
            },
            /**
             *
             * @returns {*}
             */
            getParent: function () {
                // summary:
                //		Returns the parent widget of this widget.
                return registry.getEnclosingWidget(this.domNode.parentNode);
            },
            //////////// DESTROY FUNCTIONS ////////////////////////////////
            /**
             * Destroy this widget and its descendants
             * @description If true, this method will leave the original DOM structure
             *		alone of descendant Widgets. Note: This will NOT work with
             *		dijit._TemplatedMixin widgets.
             * @param preserveDom {boolean}
             */
            destroyRecursive: function (preserveDom) {
                this._beingDestroyed = true;
                this.destroyDescendants(preserveDom);
                this.destroy(preserveDom);
            },
            /**
             *
             * @param preserveDom {boolean}. If true, this method will leave the original DOM structure alone.
             * Note: This will not yet work with _TemplatedMixin widgets
             */
            destroy: function (preserveDom) {
                // summary:
                //		Destroy this widget, but not its descendants.  Descendants means widgets inside of
                //		this.containerNode.   Will also destroy any resources (including widgets) registered via this.own().
                //
                //		This method will also destroy internal widgets such as those created from a template,
                //		assuming those widgets exist inside of this.domNode but outside of this.containerNode.
                //
                //		For 2.0 it's planned that this method will also destroy descendant widgets, so apps should not
                //		depend on the current ability to destroy a widget without destroying its descendants.   Generally
                //		they should use destroyRecursive() for widgets with children.
                this._beingDestroyed = true;
                // Destroy supporting widgets, but not child widgets under this.containerNode (for 2.0, destroy child widgets
                // here too).   if() statement is to guard against exception if destroy() called multiple times (see #15815).
                if (this.domNode) {
                    registry.findWidgets(this.domNode, this.containerNode).forEach((w) => destroy);
                }
                this.destroyRendering(preserveDom);
                registry.remove(this.id);
                this._destroyed = true;
                this._emit('destroy');
            },
            /**
             * Destroys the DOM nodes associated with this widget.
             * @param {boolean} preserveDom. If true, this method will leave the original DOM structure alone during tear-down. Note: this will not work with _Templated widgets yet.
             */
            destroyRendering: function (preserveDom) {
                if (this.domNode) {
                    if (preserveDom) {

                    } else {
                        domConstruct.destroy(this.domNode);
                    }
                    delete this.domNode;
                }
                if (this.srcNodeRef) {
                    if (!preserveDom) {
                        domConstruct.destroy(this.srcNodeRef);
                    }
                    delete this.srcNodeRef;
                }
            },
            destroyDescendants: function ( /*Boolean?*/ preserveDom) {
                // summary:
                //		Recursively destroy the children of this widget and their
                //		descendants.
                // preserveDom:
                //		If true, the preserveDom attribute is passed to all descendant
                //		widget's .destroy() method. Not for use with _Templated
                //		widgets.

                // get all direct descendants and destroy them recursively
                this.getChildren().forEach((widget) => {
                    if (widget.destroyRecursive) {
                        widget.destroyRecursive(preserveDom);
                    }
                });
            }
        });
        return Module;

    }

    const StoreMixin = dcl(null, {
        wireStore: function (store, updateFn, events) {
            store = store || this.store;
            const handles = [];
            events = events || ['update', 'added', 'remove', 'delete'];

            events.forEach(event => {
                const _handle = store.on(event, updateFn.bind(this));
                handles.push(_handle);
                this.addHandle && this.addHandle(event, _handle);
            });

            return handles;
        }
    });
    const _WidgetClass = createClass_WidgetBase();
    /////////////////////////////////////////////////////////////////
    //
    //  Module exports
    //
    //
    const Module = dcl([_WidgetClass, _TemplatedMixin, _Widget.dcl], {});

    Module.AttachClass = _AttachMixinClass;
    Module.TemplateClass = _TemplatedMixin;
    Module.WidgetClass = _WidgetClass;
    Module.StoreMixin = StoreMixin;

    dcl.chainAfter(Module, "resize");
    dcl.chainAfter(Module, "destroy");
    dcl.chainAfter(Module, "startup");
    return Module;
});