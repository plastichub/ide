## License

This package is licensed under: BSD 3-Clause, MIT, AFL and GPL-2. You can choose.

## Purpose
 
 - Multi purpose library to develop IDE like Dojo & Delite apps 
 - Widgets, Tabs, Editors per File-Extension
 
