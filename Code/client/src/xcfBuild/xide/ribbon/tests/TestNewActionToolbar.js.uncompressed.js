/** @module xgrid/Base **/
define("xide/ribbon/tests/TestNewActionToolbar", [
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "xide/tests/TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry',
    'xide/_base/_Widget',
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/DefaultActions',
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    "xblox/tests/TestUtils",
    "xide/_Popup",
    "dojo/dom", // dom.isDescendant
    "dojo/dom-construct", // domConstruct.create domConstruct.destroy
    "dojo/dom-geometry", // domGeometry.isBodyLtr
    "dojo/dom-style", // domStyle.set
    "dojo/_base/lang", // lang.hitch
    "dojo/keys",
    "dojo/on",
    "dijit/place",
    "xide/$",
    "xide/popup",
    "xide/widgets/_MenuMixin4"

], function (declare,dcl,types,utils,TestUtils,_Widget,module,registry,_XWidget,
             ActionMixin,ActionContext,DefaultActions,Path,Action,FTestUtils,i18,_TestBlockUtils,_Popup,
             dom, domConstruct, domGeometry, domStyle,lang,keys,on,place,$,popup,_MenuMixin4){
    function highZ(parent, limit){
        limit = limit || Infinity;
        parent = parent || document.body;
        let who;
        let temp;
        let max= 1;
        let opacity;
        let i= 0;
        const children = parent.childNodes;
        const length = children.length;
        const highest = null;
        while(i<length){
            who = children[i++];
            // element nodes only
            if (who.nodeType != 1) {
                continue;
            }
            opacity = deepCss(who,"opacity");
            if (deepCss(who,"position") !== "static") {
                temp = deepCss(who,"z-index");
                if (temp == "auto") { // positioned and z-index is auto, a new stacking context for opacity < 0. Further When zindex is auto ,it shall be treated as zindex = 0 within stacking context.
                    (opacity < 1)? temp=0:temp = highZ(who);
                } else {
                    temp = parseInt(temp, 10) || 0;
                }
            } else { // non-positioned element, a new stacking context for opacity < 1 and zindex shall be treated as if 0
                (opacity < 1)? temp=0:temp = highZ(who);
            }
            if (temp > max && temp <= limit){
                max = temp;
                last = who;
            }
        }

        //console.error('max ',who);
        return max;
    }

    function deepCss(who, css) {
        let sty;
        let val;
        const dv= document.defaultView || window;
        if (who.nodeType == 1) {
            sty = css.replace(/\-([a-z])/g, function(a, b){
                return b.toUpperCase();
            });
            val = who.style[sty];
            if (!val) {
                if(who.currentStyle) val= who.currentStyle[sty];
                else if (dv.getComputedStyle) {
                    val= dv.getComputedStyle(who,"").getPropertyValue(css);
                }
            }
        }
        return val || "";
    }

    const Gridnav = function (listelement) {
        const that = this;
        this.list = (typeof listelement) === 'string' ?
            document.querySelector(listelement) :
            listelement;
        if (!this.list) {
            throw Error('List item could not be found');
        }
        this.setcodes = function (amount) {
            that.codes = {
                39:1, 68:1,
                65:-1, 37:-1,
                87:-that.amount, 38:-that.amount,
                83: that.amount, 40:that.amount
            };
        }
        if (!this.list.getAttribute('data-element')) {
            this.element = this.list.firstElementChild.firstElementChild.tagName;
        } else {
            this.element = this.list.getAttribute('data-element').toUpperCase();
        }
        if (!this.list.getAttribute('data-amount')) {
            this.amount = 6502;
            this.setcodes(this.amount);
        } else {
            this.amount = +this.list.getAttribute('data-amount');
            this.setcodes(this.amount);
        }
        this.setcodes(this.amount);
        this.all = this.list.querySelectorAll(this.element);
        this.keynav = function(ev) {
            const t = ev.target;
            let c;
            let posx;
            let posy;
            //if (t.tagName === that.element) {
            console.error('k');
            for (var i = 0; i < that.all.length; i++) {
                if (that.all[i] === t) {
                    c = i;
                    posx = that.all[c].offsetLeft;
                    posy = that.all[c].offsetTop;
                    break;
                }
            }
            if (that.codes[ev.keyCode]) {
                const kc = that.codes[ev.keyCode];
                if (kc > -6502 && kc < 6502) {
                    if (that.all[c + kc]) {
                        that.all[c + kc].focus();
                    }
                } else {
                    const add = kc < 0 ? -1 : 1;
                    while (that.all[i]) {
                        if (that.all[i].offsetLeft === posx &&
                            that.all[i].offsetTop !== posy) {
                            that.all[i].focus();
                            break;
                        }
                        i += add;
                    }
                }
            }
            //}
        }
        this.list.addEventListener('keyup', this.keynav);
    };

    Gridnav.lists = [];
    const createCallback = function (func, menu, item) {
        return function (event) {
            console.error('click');
            func(event, menu, item);
        };
    };
    /*
    [].forEach.call(document.querySelectorAll('.gridnav'), function (item, key) {
        Gridnav.lists[key] = new Gridnav(item);
    });
    */

    const PopupManager = dcl(null, {
        // _stack: dijit/_WidgetBase[]
        //		Stack of currently popped up widgets.
        //		(someone opened _stack[0], and then it opened _stack[1], etc.)
        _stack: [],
        // _beginZIndex: Number
        //		Z-index of the first popup.   (If first popup opens other
        //		popups they get a higher z-index.)
        _beginZIndex: 1000,
        _idGen: 1,
        _repositionAll: function(){
            // summary:
            //		If screen has been scrolled, reposition all the popups in the stack.
            //		Then set timer to check again later.

            if(this._firstAroundNode){
                // guard for when clearTimeout() on IE doesn't work
                const oldPos = this._firstAroundPosition;

                const newPos = domGeometry.position(this._firstAroundNode, true);
                const dx = newPos.x - oldPos.x;
                const dy = newPos.y - oldPos.y;

                if(dx || dy){
                    this._firstAroundPosition = newPos;
                    for(let i = 0; i < this._stack.length; i++){
                        const style = this._stack[i].wrapper.style;
                        style.top = (parseFloat(style.top) + dy) + "px";
                        if(style.right == "auto"){
                            style.left = (parseFloat(style.left) + dx) + "px";
                        }else{
                            style.right = (parseFloat(style.right) - dx) + "px";
                        }
                    }
                }

                this._aroundMoveListener = setTimeout(lang.hitch(this, "_repositionAll"), dx || dy ? 10 : 50);
            }
        },
        /**
         * Initialization for widgets that will be used as popups.
         * Puts widget inside a wrapper DIV (if not already in one),and returns pointer to that wrapper DIV.
         * @param node
         * @returns {HTMLElement}
         * @private
         */
        _createWrapper: function(node,args){
            let wrapper = $(node).data('_popupWrapper');
            const owner = $(node).data('owner');
            if(!wrapper){
                const $wrapper = $("<div class='xPopup' style='display:none' role='region'></div>" );
                $('body').append(wrapper);
                $wrapper.append($(node));
                wrapper = $wrapper[0];
                const s = node.style;
                s.display = "";
                s.visibility = "";
                s.position = "";
                s.top = "0px";
                if(owner){
                    if(owner._on){
                        owner._on('destroy',function(e){
                            $wrapper.remove();
                        });
                    }
                }
                $(node).data('_popupWrapper',wrapper);
            }
            return wrapper;
        },
        /**
         * Moves the popup widget off-screen.
         * Do not use this method to hide popups when not in use, because
         * that will create an accessibility issue: the offscreen popup is
         * still in the tabbing order.
         * @param node {HTMLElement}
         * @returns {*}
         */
        moveOffScreen: function(node,args){
            // Create wrapper if not already there
            const wrapper = this._createWrapper(node,args);

            // Besides setting visibility:hidden, move it out of the viewport, see #5776, #10111, #13604
            const ltr = true;

            const style = {
                visibility: "hidden",
                top: "-9999px",
                display: ""
            };

            style[ltr ? "left" : "right"] = "-9999px";
            style[ltr ? "right" : "left"] = "auto";
            $(wrapper).css(style);
            return wrapper;
        },
        /**
         * Hide this popup widget (until it is ready to be shown).
         * Initialization for widgets that will be used as popups.
         * Also puts widget inside a wrapper DIV (if not already in one)
         * If popup widget needs to layout it should
         * do so when it is made visible, and popup._onShow() is called.
         * @param widget {HTMLElement}
         */
        hide: function(widget,args){
            // Create wrapper if not already there
            const wrapper = this._createWrapper(widget,args);
            $(wrapper).css({
                display: "none",
                height: "auto",			// Open() may have limited the height to fit in the viewport,
                overflowY: "visible",	// and set overflowY to "auto".
                border: ""			// Open() may have moved border from popup to wrapper.
            });
            // Open() may have moved border from popup to wrapper.  Move it back.
            const node = widget;
            if("_originalStyle" in node){
                node.style.cssText = node._originalStyle;
            }
        },
        getTopPopup: function(){
            // summary:
            //		Compute the closest ancestor popup that's *not* a child of another popup.
            //		Ex: For a TooltipDialog with a button that spawns a tree of menus, find the popup of the button.
            const stack = this._stack;
            for(var pi = stack.length - 1; pi > 0 && stack[pi].parent === stack[pi - 1].widget; pi--){
                /* do nothing, just trying to get right value for pi */
            }
            return stack[pi];
        },
        /**
         * Popup the widget at the specified position
         * example:
         *   opening at the mouse position
         *      popup.open({popup: menuWidget, x: evt.pageX, y: evt.pageY});
         *
         * example:
         *  opening the widget as a dropdown
         *      popup.open({parent: this, popup: menuWidget, around: this.domNode, onClose: function(){...}});
         *
         *  Note that whatever widget called dijit/popup.open() should also listen to its own _onBlur callback
         *  (fired from _base/focus.js) to know that focus has moved somewhere else and thus the popup should be closed.
         * @param args
         * @returns {*}
         */
        open: function(args){
            // summary:
            //		Popup the widget at the specified position
            //

            const last = null;



            const isLTR = true;
            const self = this;
            const stack = this._stack;
            const widget = args.popup;
            const node = args.popup;
            const orient = args.orient || ["below", "below-alt", "above", "above-alt"];
            const ltr = args.parent ? args.parent.isLeftToRight() : isLTR;
            const around = args.around;
            const owner = $(node).data('owner');
            const extraClass = args.extraClass || "";
            const id = (args.around && args.around.id) ? (args.around.id + "_dropdown") : ("popup_" + this._idGen++);

            // If we are opening a new popup that isn't a child of a currently opened popup, then
            // close currently opened popup(s).   This should happen automatically when the old popups
            // gets the _onBlur() event, except that the _onBlur() event isn't reliable on IE, see [22198].
            while(stack.length && (!args.parent || $.contains(args.parent.domNode,stack[stack.length - 1].widget.domNode))){
                this.close(stack[stack.length - 1].widget);
            }

            // Get pointer to popup wrapper, and create wrapper if it doesn't exist.  Remove display:none (but keep
            // off screen) so we can do sizing calculations.
            const wrapper = this.moveOffScreen(widget,args);
            //console.error('popup open s',$(owner.delegate.domNode).zIndex());
            //console.log('owner',owner);

            const $wrapper = $(wrapper);

            // Limit height to space available in viewport either above or below aroundNode (whichever side has more
            // room), adding scrollbar if necessary. Can't add scrollbar to widget because it may be a <table> (ex:
            // dijit/Menu), so add to wrapper, and then move popup's border to wrapper so scroll bar inside border.
            let maxHeight;

            const popupSize = domGeometry.position(node);
            if("maxHeight" in args && args.maxHeight != -1){
                maxHeight = args.maxHeight || Infinity;	// map 0 --> infinity for back-compat of _HasDropDown.maxHeight
            }else{
                const viewport = {
                    t:0,
                    l:0,
                    h:$(window).height(),
                    w:$(window).width()
                };
                const aroundPos = around ? domGeometry.position(around, false) : {y: args.y - (args.padding||0), h: (args.padding||0) * 2};
                maxHeight = Math.floor(Math.max(aroundPos.y, viewport.h - (aroundPos.y + aroundPos.h)));
            }
            //maxHeight = 300;
            if(popupSize.h > maxHeight){
                // Get style of popup's border.  Unfortunately domStyle.get(node, "border") doesn't work on FF or IE,
                // and domStyle.get(node, "borderColor") etc. doesn't work on FF, so need to use fully qualified names.
                const cs = domStyle.getComputedStyle(node);

                const borderStyle = cs.borderLeftWidth + " " + cs.borderLeftStyle + " " + cs.borderLeftColor;

                $wrapper.css({
                    'overflow-y': "scroll",
                    height: maxHeight + "px",
                    border: borderStyle	// so scrollbar is inside border
                });
                node._originalStyle = node.style.cssText;
                node.style.border = "none";
            }

            $wrapper.attr({
                id: id,
                "class": "xPopup " + (widget.baseClass || widget["class"] || "").split(" ")[0] + "Popup",
                dijitPopupParent: args.parent ? args.parent.id : ""
            });
            $wrapper.css('z-index',this._beginZIndex + stack.length);
            if(stack.length === 0 && around){
                // First element on stack. Save position of aroundNode and setup listener for changes to that position.
                this._firstAroundNode = around;
                //this._firstAroundPosition = domGeometry.position(around, true);
                const offset = $(around).offset();
                this._firstAroundPosition = {
                    w:$(around).width(),
                    h:$(around).height(),
                    x:offset.left,
                    y:offset.top
                };
                //this._aroundMoveListener = setTimeout(lang.hitch(this, "_repositionAll"), 50);
                this._aroundMoveListener = setTimeout(function(){
                    self._repositionAll();
                }, 50);
            }

            // position the wrapper node and make it visible
            const layoutFunc = null; //widget.orient ? lang.hitch(widget, "orient") : null;
            const best = around ?
                place.around(wrapper, around, orient, ltr, layoutFunc) :
                place.at(wrapper, args, orient == 'R' ? ['TR', 'BR', 'TL', 'BL'] : ['TL', 'BL', 'TR', 'BR'], args.padding,
                    layoutFunc);

            wrapper.style.visibility = "visible";
            node.style.visibility = "visible";	// counteract effects from _HasDropDown
            const handlers = [];
            if(!$(wrapper).data('keyboardSetup')){
                $(wrapper).data('keyboardSetup',true);
                $(wrapper).on('keydown', function (evt) {
                    if (evt.keyCode == 27 && args.onCancel) {//esape
                        evt.stopPropagation();
                        evt.preventDefault();
                        args.onCancel();
                    } else if (evt.keyCode == 9) {//tab
                        evt.stopPropagation();
                        evt.preventDefault();
                        const topPopup = self.getTopPopup();
                        if (topPopup && topPopup.onCancel) {
                            topPopup.onCancel();
                        }
                    }
                });
            }
            // watch for cancel/execute events on the popup and notify the caller
            // (for a menu, "execute" means clicking an item)
            if(widget.onCancel && args.onCancel){
                handlers.push(widget.on("cancel", args.onCancel));
            }
            /*
             handlers.push(widget.on(widget.onExecute ? "execute" : "change", lang.hitch(this, function(){
             var topPopup = this.getTopPopup();
             if(topPopup && topPopup.onExecute){
             topPopup.onExecute();
             }
             })));
             */
            stack.push({
                widget: widget,
                wrapper: wrapper,
                parent: args.parent,
                onExecute: args.onExecute,
                onCancel: args.onCancel,
                onClose: args.onClose,
                handlers: handlers
            });
            if(widget.onOpen){
                // TODO: in 2.0 standardize onShow() (used by StackContainer) and onOpen() (used here)
                widget.onOpen(best);
            }
            $(wrapper).addClass(extraClass);
            $(node).css('display','block');
            return best;
        },

        close: function(/*Widget?*/ popup){
            // summary:
            //		Close specified popup and any popups that it parented.
            //		If no popup is specified, closes all popups.


            const stack = this._stack;
            // Basically work backwards from the top of the stack closing popups
            // until we hit the specified popup, but IIRC there was some issue where closing
            // a popup would cause others to close too.  Thus if we are trying to close B in [A,B,C]
            // closing C might close B indirectly and then the while() condition will run where stack==[A]...
            // so the while condition is constructed defensively.
            while((popup && _.some(stack, function(elem){
                return elem.widget == popup;
            })) ||

            (!popup && stack.length)){
                const top = stack.pop();
                const widget = top.widget;
                const onClose = top.onClose;

                if(widget.onClose){
                    widget.onClose();
                }

                let h;

                while(h = top.handlers.pop()){
                    h.remove();
                }

                // Hide the widget and it's wrapper unless it has already been destroyed in above onClose() etc.
                if(widget && widget){
                    this.hide(widget);
                }
                if(onClose){
                    onClose();
                }
            }

            $(popup).css('display','none');

            if(stack.length === 0 && this._aroundMoveListener){
                clearTimeout(this._aroundMoveListener);
                this._firstAroundNode = this._firstAroundPosition = this._aroundMoveListener = null;
            }
        }
    });

    popup  = new PopupManager();
    console.clear();

    const ACTION = types.ACTION;
    const _debug = false;
    const _debugWidgets = true;
    const OPEN_DELAY = 200;
    const _ctx = {};
    //action renderer class
    const ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        templateString:'<div attachTo="navigation" class="actionToolbar">'+
        '<nav attachTo="navigationRoot" class="" role="navigation">'+
        '<ul attachTo="navBar" class="nav navbar-nav"/>'+
        '</nav>'+
        '</div>'
    });

    const MenuMixinClass =  dcl(null, {
        actionStores: null,
        correctSubMenu: false,
        _didInit: null,
        actionFilter: null,
        hideSubsFirst: false,
        collapseSmallGroups: 0,
        containerClass: '',
        lastTree:null,
        onActionAdded: function (actions) {
            this.setActionStore(this.getActionStore(), actions.owner || this, false, true, actions);
        },
        onActionRemoved: function (evt) {
            this.clearAction(evt.target);
        },
        clearAction: function (action) {
            const self = this;
            if (action) {
                const actionVisibility = action.getVisibility !== null ? action.getVisibility(self.visibility) : {};
                if (actionVisibility) {
                    const widget = actionVisibility.widget;
                    widget && action.removeReference && action.removeReference(widget);
                    if (widget && widget.destroy) {
                        widget.destroy();
                    }
                    delete actionVisibility.widget;
                    actionVisibility.widget = null;
                }
            }
        },
        removeCustomActions: function () {
            const oldStore = this.store;
            const oldActions = oldStore._find({
                custom: true
            });

            const menuData = this.menuData;
            _.each(oldActions, function (action) {
                oldStore.removeSync(action.command);
                const oldMenuItem = _.find(menuData, {
                    command: action.command
                });
                oldMenuItem && menuData.remove(oldMenuItem);
            });
        },
        /**
         * Return a field from the object's given visibility store
         * @param action
         * @param field
         * @param _default
         * @returns {*}
         */
        getVisibilityField: function (action, field, _default) {
            const actionVisibility = action.getVisibility !== null ? action.getVisibility(this.visibility) : {};
            return actionVisibility[field] !== null ? actionVisibility[field] : action[field] || _default;
        },
        /**
         * Sets a field in the object's given visibility store
         * @param action
         * @param field
         * @param value
         * @returns {*}
         */
        setVisibilityField: function (action, field, value) {
            const _default = {};
            if (action.getVisibility) {
                var actionVisibility = action.getVisibility(this.visibility) || _default;
                actionVisibility[field] = value;
            }
            return actionVisibility;
        },
        shouldShowAction: function (action) {
            if (this.getVisibilityField(action, 'show') === false) {
                return false;
            } else if (action.getVisibility && action.getVisibility(this.visibility) == null) {
                return false;
            }
            return true;
        },
        addActionStore: function (store) {
            if (!this.actionStores) {
                this.actionStores = [];
            }
            if (!this.actionStores.includes(store)) {
                this.actionStores.push(store);
            }
        },
        /**

         tree structure :

         {
            root: {
                Block:{
                    grouped:{
                        Step:[action,action]
                    }
                }
            },
            rootActions: string['File','Edit',...],

            allActionPaths: string[command],

            allActions:[action]
         }

         * @param store
         * @param owner
         * @returns {{root: {}, rootActions: Array, allActionPaths: *, allActions: *}}
         */
        constructor: function (options, node) {
            this.target = node;
            utils.mixin(this, options);
        },
        onClose: function (e) {
            this._rootMenu && this._rootMenu.parent().removeClass('open');
        },
        onOpen: function () {
            this._rootMenu && this._rootMenu.parent().addClass('open');
        },
        isLeftToRight: function () {
            return false;
        },
        init: function (opts) {
            if (this._didInit) {
                return;
            }
            this._didInit = true;
            let options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            const self = this;
            const root = $(document);
            this.__on(root, 'click', null, function (e) {
                if (!self.isOpen) {
                    return;
                }
                self.isOpen = false;
                self.onClose(e);
                $('.dropdown-context').css({
                    display: ''
                }).find('.drop-left').removeClass('drop-left');
            });
            if (options.preventDoubleContext) {
                this.__on(root, 'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            this.__on(root, 'mouseenter', '.dropdown-submenu', function (e) {
                try {
                    const _root = $(e.currentTarget);
                    let $sub = _root.find('.dropdown-context-sub:first');
                    let didPopup = false;
                    if ($sub.length === 0) {
                        $sub = _root.data('sub');
                        if ($sub) {
                            didPopup = true;
                        } else {
                            return;
                        }
                    }
                    const data = $sub.data('data');
                    const level = data ? data[0].level : 0;
                    const isFirst = level === 1;
                    if (self.menu) {
                        if (!$.contains(self.menu[0], _root[0])) {
                            return;
                        }
                    }

                    const _disabled = _root.hasClass('disabled');
                    if (_disabled) {
                        $sub.css('display', 'none');
                        return;
                    } else {
                        $sub.css('display', 'block');
                    }

                    if (isFirst) {
                        $sub.css('display', 'initial');
                        $sub.css('position', 'initial');
                        function close() {
                            const _wrapper = $sub.data('_popupWrapper');
                            popup.close({
                                domNode: $sub[0],
                                _popupWrapper: _wrapper
                            });
                        }

                        if (!didPopup) {
                            _root.data('sub', $sub);
                            $sub.data('owner', self);
                            $sub.on('mouseleave', function () {
                                close();
                            });
                            _root.on('mouseleave', function () {
                            });
                        }

                        popup.open({
                            //parent: self,
                            popup: $sub[0],
                            around: _root[0],
                            orient: ['below', 'above'],
                            maxHeight: -1,
                            owner: self,
                            onExecute: function () {
                                self.closeDropDown(true);
                            },
                            onCancel: function () {
                                close();
                            },
                            onClose: function () {
                                //console.log('close');
                                //domAttr.set(self._popupStateNode, "popupActive", false);
                                //domClass.remove(self._popupStateNode, "dijitHasDropDownOpen");
                                //self._set("_opened", false);	// use set() because _CssStateMixin is watching
                            }
                        });
                        return;
                    } else {
                        if (!$sub.data('didSetup')) {
                            $sub.data('didSetup', true);
                            _root.on('mouseleave', function () {
                                $sub.css('display', '');
                            });
                        }
                    }

                    //reset top
                    $sub.css({
                        top: 0
                    });

                    const autoH = $sub.height() + 0;
                    const totalH = $('html').height();
                    const pos = $sub.offset();
                    const overlapYDown = totalH - (pos.top + autoH);
                    if ((pos.top + autoH) > totalH) {
                        $sub.css({
                            top: overlapYDown - 30
                        }).fadeIn(options.fadeSpeed);
                    }

                    ////////////////////////////////////////////////////////////
                    const subWidth = $sub.width();

                    const subLeft = $sub.offset().left;
                    const collision = (subWidth + subLeft) > window.innerWidth;

                    if (collision) {
                        $sub.addClass('drop-left');
                    }
                } catch (e) {
                    logError(e);
                }
            });
        },
        getDefaultOptions: function () {
            return {
                fadeSpeed: 0,
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: true
            };
        },
        buildMenuItems: function ($menu, data, id, subMenu, addDynamicTag) {
            //this._debugMenu && console.log('build - menu items ', arguments);
            let linkTarget = '';

            const self = this;
            const visibility = this.visibility;

            data.forEach((item, i) => {
                let $sub;
                const widget = item.widget;

                if (typeof item.divider !== 'undefined' && !item.widget) {
                    let divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    divider += '"></li>';
                    item.widget = divider;
                    $menu.append(divider);
                    divider.data('item',item);

                } else if (typeof item.header !== 'undefined' && !item.widget) {
                    let header = item.vertical ? '<li class="divider-vertical' : '<li class="nav-header testClass';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + item.header + '</li>';
                    header = $(header);
                    item.widget = header;
                    $menu.append(header);
                    header.data('item',item);

                } else if (typeof item.menu_item_src !== 'undefined') {

                } else {

                    if (!widget && typeof item.target !== 'undefined') {
                        linkTarget = ' target="' + item.target + '"';
                    }
                    if (typeof item.subMenu !== 'undefined' && !widget) {
                        let sub_menu = '<li tabindex="-1" class="dropdown-submenu ' + this.containerClass;
                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a>';

                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }
                        sub_menu += item.text + '';
                        sub_menu += '</a></li>';
                        $sub = $(sub_menu);

                    } else {
                        if (!widget) {
                            if (item.render) {
                                $sub = item.render(item, $menu);
                            } else {
                                let element = '<li tabindex="-1" class="" ';
                                element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                                element += '><a >';
                                if (typeof data[i].icon !== 'undefined') {
                                    element += '<span class="' + item.icon + '"></span> ';
                                }
                                element += item.text + '</a></li>';
                                $sub = $(element);
                                if (item.postRender) {
                                    item.postRender($sub);
                                }
                            }
                        }
                    }

                    if (typeof item.action !== 'undefined' && !item.widget) {
                        if (item.addClickHandler && item.addClickHandler() === false) {
                        } else {
                            const $action = item.action;
                            if ($sub && $sub.find) {
                                const trigger = $sub.find('a');
                                trigger.addClass('context-event');
                                const handler = createCallback($action, item, $sub);
                                trigger.data('handler',handler).on('click',handler);
                            }
                        }
                    }

                    if ($sub && !widget) {

                        item.widget = $sub;
                        $sub.menu = $menu;
                        $sub.data('item', item);

                        item.$menu = $menu;
                        item.$sub = $sub;

                        item._render = function () {
                            if (item.index === 0) {
                                this.$menu.prepend(this.$sub);
                            } else {
                                this.$menu.append(this.$sub);
                            }
                        };
                        if (!item.lazy) {
                            item._render();
                        }
                    }

                    if ($sub) {
                        $sub.attr('level', item.level);
                    }

                    if (typeof item.subMenu != 'undefined' && !item.subMenuData) {
                        const subMenuData = self.buildMenu(item.subMenu, id, true);
                        $menu.subMenuData = subMenuData;
                        item.subMenuData = subMenuData;
                        $menu.find('li:last').append(subMenuData);
                        subMenuData.attr('level', item.subMenu.level);
                        if (self.hideSubsFirst) {
                            subMenuData.css('display', 'none');
                        }
                        $menu.data('item', item);
                    } else {
                        if (item.subMenu && item.subMenuData) {
                            this.buildMenuItems(item.subMenuData, item.subMenu, id, true);
                        }
                    }
                }

                if (!$menu._didOnClick) {
                    $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                        e.stopPropagation();
                    });
                    $menu._didOnClick = true;
                }
            });

            return $menu;
        },
        buildMenu: function (data, id, subMenu) {
            const subClass = (subMenu) ? (' dropdown-context-sub ' + this.containerClass ) : ' scrollable-menu ';
            const $menu = $('<ul tabindex="-1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>');
            if (!subMenu) {
                this._rootMenu = $menu;
            }
            const result = this.buildMenuItems($menu, data, id, subMenu);
            $menu.data('data', data);
            return result;
        },
        createNewAction: function (command) {
            const segments = command.split('/');
            const lastSegment = segments[segments.length - 1];
            const action = new Action({
                command: command,
                label: lastSegment,
                group: lastSegment,
                dynamic: true
            });
            return action;
        },
        findAction: function (command) {
            const stores = this.actionStores;
            let action = null;
            _.each(stores, function (store) {
                const _action = store ? store.getSync(command) : null;
                if (_action) {
                    action = _action;
                }
            });

            return action;
        },
        getAction: function (command, store) {
            store = store || this.store;
            let action = null;
            if (store) {
                action = this.findAction(command);
                if (!action) {
                    action = this.createNewAction(command);
                }
            }
            return action;
        },
        getActions: function (query) {
            let result = [];
            const stores = this.actionStores;
            const visibility = this.visibility;

            query = query || this.actionFilter;
            _.each(stores, function (store) {
                store && (result = result.concat(store._find(query)));
                //store && (result2= result2.concat(store._find(query)));
            });
            result = result.filter(function (action) {
                const actionVisibility = action.getVisibility != null ? action.getVisibility(visibility) : {};
                return !(action.show === false || actionVisibility === false || actionVisibility.show === false);
            });
            /*
             console.log('action: ',[result,result2]);
             */
            return result;
        },
        toActions: function (commands, store) {
            const result = [];
            const self = this;
            _.each(commands, function (path) {
                const _action = self.getAction(path, store);
                _action && result.push(_action);
            });
            return result;
        },
        onRunAction: function (action, owner, e) {
            const command = action.command;
            action = this.findAction(command);
            return DefaultActions.defaultHandler.apply(action.owner || owner, [action, e]);
        },
        getActionProperty: function (action, visibility, prop) {
            let value = prop in action ? action[prop] : null;
            if (visibility && prop in visibility) {
                value = visibility[prop];
            }
            return value;
        },
        toMenuItem: function (action, owner, label, icon, visibility, showKeyCombo, lazy) {
            const self = this;
            const labelLocalized = self.localize(label);
            const actionType = visibility.actionType || action.actionType;

            const item = {
                text: labelLocalized,
                icon: icon,
                data: action,
                owner: owner,
                command: action.command,
                lazy: lazy,
                addClickHandler: function () {
                    return actionType !== types.ACTION_TYPE.MULTI_TOGGLE;

                },
                render: function (data, $menu) {
                    if (self.renderItem) {
                        return self.renderItem(this, data, $menu, this.data, owner, label, icon, visibility, showKeyCombo, lazy);
                    }
                    const action = this.data;
                    const parentAction = action.getParent ? action.getParent() : null;
                    const closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
                    let keyComboString = ' \n';
                    let element = null;
                    if (action.keyboardMappings && showKeyCombo !== false) {
                        const mappings = action.keyboardMappings;
                        const keyCombos = mappings[0].keys;
                        if (keyCombos && keyCombos.length) {
                            keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                        }
                    }

                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        element = '<li tabindex="-1" class="" >';
                        const id = action._store.id + '_' + action.command + '_' + self.id;
                        const checked = action.get('value');
                        //checkbox-circle
                        element += '<div class="action-checkbox checkbox checkbox-success ">';
                        element += '<input id="' + id + '" type="checkbox" ' + (checked === true ? 'checked' : '') + '>';
                        element += '<label for="' + id + '">';
                        element += self.localize(data.text);
                        element += '</label>';
                        element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                        element += '</li>';

                        $menu.addClass('noclose');
                        const result = $(element);
                        const checkBox = result.find('INPUT');
                        checkBox.on('change', function (e) {
                            action._originReference = data;
                            action._originEvent = e;
                            action.set('value', checkBox[0].checked);
                            action._originReference = null;
                        });
                        self.setVisibilityField(action, 'widget', data);
                        return result;
                    }
                    closeOnClick === false && $menu.addClass('noclose');
                    if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                        const value = action.value || action.get('value');
                        const parentValue = parentAction.get('value');
                        if (value == parentValue) {
                            icon = 'fa fa-check';
                        }
                    }

                    const title = data.text || labelLocalized || self.localize(action.title);


                    //default:
                    element = '<li tabindex="-1"><a title="' + title + ' ' + keyComboString + '">';
                    const _icon = data.icon || icon;

                    //icon
                    if (typeof _icon !== 'undefined') {
                        //already html string
                        if (/<[a-z][\s\S]*>/i.test(_icon)) {
                            element += _icon;
                        } else {
                            element += '<span class="icon ' + _icon + '"/> ';
                        }
                    }
                    element += data.text;
                    element += '<span style="max-width:100px" class="text-muted pull-right ellipsis keyboardShortCut">' + (showKeyCombo ? keyComboString : "") + '</span></a></li>';
                    self.setVisibilityField(action, 'widget', data);
                    return $(element);
                },
                get: function (key) {
                },
                set: function (key, value) {
                    //_debugWidgets && _.isString(value) && console.log('set ' + key + ' ' + value);
                    const widget = this.widget;

                    function updateCheckbox(widget, checked) {
                        const what = widget.find("input[type=checkbox]");
                        if (what) {
                            if (checked) {
                                what.prop("checked", true);
                            } else {
                                what.removeAttr('checked');
                            }
                        }
                    }

                    if (widget) {
                        if (key === 'disabled') {
                            if (widget.toggleClass) {
                                widget.toggleClass('disabled', value);
                            }
                        }
                        if (key === 'icon') {
                            const _iconNode = widget.find('.icon');
                            if (_iconNode) {
                                _iconNode.attr('class', 'icon');
                                this._lastIcon = this.icon;
                                this.icon = value;
                                _iconNode.addClass(value);
                            }
                        }
                        if (key === 'value') {
                            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE ||
                                actionType === types.ACTION_TYPE.SINGLE_TOGGLE) {
                                updateCheckbox(widget, value);
                            }
                        }
                    }
                },
                action: function (e, data, menu) {
                    _debug && console.log('menu action', data);
                    return self.onRunAction(data.data, owner, e);
                },
                destroy: function () {
                    if (this.widget) {
                        this.widget.remove();
                    }
                }
            };
            return item;
        },
        attach: function (selector, data) {
            this.target = selector;
            this.menu = this.addContext(selector, data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
            return this.menu;
        },
        addReference: function (action, item) {
            if (action.addReference) {
                action.addReference(item, {
                    properties: {
                        "value": true,
                        "disabled": true,
                        "enabled": true
                    }
                }, true);
            }
        },
        onDidRenderActions: function (store, owner) {
            if (owner && owner.refreshActions) {
                owner.refreshActions();
            }
        },
        getActionData: function (action) {
            const actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label: actionVisibility.label != null ? actionVisibility.label : action.label,
                icon: actionVisibility.icon != null ? actionVisibility.icon : action.icon,
                command: actionVisibility.command != null ? actionVisibility.command : action.command,
                visibility: actionVisibility,
                group: actionVisibility.group != null ? actionVisibility.group : action.group,
                tab: actionVisibility.tab != null ? actionVisibility.tab : action.tab,
                expand: actionVisibility.expand != null ? actionVisibility.expand : false,
                widget: actionVisibility.widget
            };
        },
        _clearAction: function (action) {

        },
        _findParentData: function (oldMenuData, parentCommand) {
            const parent = _.find(oldMenuData, {
                command: parentCommand
            });
            if (parent) {
                return parent;
            }
            for (let i = 0; i < oldMenuData.length; i++) {
                const data = oldMenuData[i];
                if (data.subMenu) {
                    const found = this._findParentData(data.subMenu, parentCommand);
                    if (found) {
                        return found;
                    }
                }
            }
            return null;
        },
        _clear: function () {
            let actions = this.getActions();
            const store = this.store;
            if (store) {
                this.actionStores.remove(store);
            }
            const self = this;
            actions = actions.concat(this._tmpActions);
            _.each(actions, function (action) {
                if (action) {
                    const actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        const widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                        delete actionVisibility.widget;
                        actionVisibility.widget = null;
                    }
                }
            });
            this.$navBar && this.$navBar.empty();
        },
        buildActionTree: function (store, owner) {
            const self = this;
            const allActions = self.getActions();
            const visibility = self.visibility;

            self.wireStore(store, function (evt) {
                if (evt.type === 'update') {
                    const action = evt.target;
                    if (action.refreshReferences) {
                        action.refreshReferences(evt.property, evt.value);
                    }
                }
            });

            //return all actions with non-empty tab field
            const tabbedActions = allActions.filter(function (action) {
                    const _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                    if (action) {
                        return _vis.tab || action.tab;
                    }
                });

            const //group all tabbed actions : { Home[actions], View[actions] }
            groupedTabs = _.groupBy(tabbedActions, function (action) {
                const _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                if (action) {
                    return _vis.tab || action.tab;
                }
            });

            let //now flatten them
            _actionsFlattened = [];


            _.each(groupedTabs, function (items) {
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            let rootActions = [];
            _.each(tabbedActions, function (action) {
                const rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            //owner sort of top level
            store.menuOrder && (rootActions = owner.sortGroups(rootActions, store.menuOrder));

            const tree = {};
            //stats to count groups per tab
            let biggestTab = rootActions[0];
            let nbGroupsBiggest = 0;

            _.each(rootActions, function (level) {
                // collect all actions at level (File/View/...)
                let menuActions = owner.getItemsAtBranch(allActions, level);
                // convert action command strings to Action references
                let grouped = self.toActions(menuActions, store);

                // expand filter -------------------
                let addedExpanded = [];
                const toRemove = [];
                _.each(grouped, function (action) {
                    const actionData = self.getActionData(action);
                    if (actionData.expand) {
                        const children = action.getChildren();
                        children && children.length && (addedExpanded = addedExpanded.concat(children));
                        toRemove.push(action);
                    }
                });
                grouped = grouped.concat(addedExpanded);
                grouped = grouped.filter(function (action) {
                    return !toRemove.includes(action);
                });
                // expand filter ---------------    end

                // group all actions by group
                const groupedActions = _.groupBy(grouped, function (action) {
                    const _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                    if (action) {
                        return _vis.group || action.group;
                    }
                });

                let _actions = [];
                _.each(groupedActions, function (items, level) {
                    if (level !== 'undefined') {
                        _actions = _actions.concat(items);
                    }
                });

                //flatten out again
                menuActions = _.pluck(_actions, 'command');
                menuActions.grouped = groupedActions;
                tree[level] = menuActions;

                //update stats
                if (self.collapseSmallGroups) {
                    const nbGroups = _.keys(menuActions.grouped).length;
                    if (nbGroups > nbGroupsBiggest) {
                        nbGroupsBiggest = nbGroups;
                        biggestTab = level;
                    }
                }
            });

            //now move over any tab with less than 2 groups to the next bigger tab
            this.collapseSmallGroups && _.each(tree, function (actions, level) {
                if (_.keys(actions.grouped).length < self.collapseSmallGroups) {
                    //append tab groups of the biggest tab
                    tree[biggestTab] && _.each(actions.grouped, function (group, name) {
                        tree[biggestTab].grouped[name] = group;
                    });
                    //copy manually commands to that tab
                    tree[biggestTab] && _.each(actions, function (action) {
                        tree[biggestTab].push(action);
                    });
                    tree[biggestTab] && delete tree[level];
                }
            });
            const result = {
                root: tree,
                rootActions: rootActions,
                allActionPaths: _.pluck(allActions, 'command'),
                allActions: allActions
            };

            this.lastTree = result;
            return result;
        }
    });
    const ActionRendererClass = dcl(null,{
        renderTopLevel:function(name,where){
            where = where || $(this.getRootContainer());
            const item =$('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) +'<b class="caret"></b></a>'+
                '</li>');

            where.append(item);
            return item;
        },
        getRootContainer:function(){
            return this.navBar;
        }
    });


    /**
     * @extends module:xide/widgets/MainMenuActionRenderer
     */
    const _MainMenu = dcl([ContainerClass,MenuMixinClass,ActionRendererClass,_XWidget.StoreMixin],{
        declaredClass:'xide.widgets.Actiontoolbar',
        target:null,
        attachToGlobal:true,
        _isFollowing:false,
        _followTimer:null,
        _zIndex:1,
        hideSubsFirst:true,
        visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
        _renderItem:function(item,data,$menu,action, owner, label, icon, visibility, showKeyCombo, lazy){
            const self = this;
            const labelLocalized = self.localize(label);
            const actionType = visibility.actionType || action.actionType;
            const parentAction = action.getParent ? action.getParent() : null;
            const closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
            let keyComboString = ' \n';
            let element = null;
            if (action.keyboardMappings && showKeyCombo !== false) {
                const mappings = action.keyboardMappings;
                const keyCombos = mappings[0].keys;
                if (keyCombos && keyCombos.length) {
                    keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                }
            }

            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                element = '<li class="" >';
                const id = action._store.id + '_' + action.command + '_' + self.id;
                const checked = action.get('value');
                //checkbox-circle
                element += '<div class="action-checkbox checkbox checkbox-success ">';
                element += '<input id="' + id + '" type="checkbox" ' + (checked === true ? 'checked' : '') + '>';
                element += '<label for="' + id + '">';
                element += self.localize(data.text);
                element += '</label>';
                element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                element += '</li>';

                $menu.addClass('noclose');
                const result = $(element);
                const checkBox = result.find('INPUT');
                checkBox.on('change', function (e) {
                    action._originReference = data;
                    action._originEvent = e;
                    action.set('value', checkBox[0].checked);
                    action._originReference = null;
                });
                self.setVisibilityField(action, 'widget', data);
                return result;
            }
            closeOnClick === false && $menu.addClass('noclose');
            if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                const value = action.value || action.get('value');
                const parentValue = parentAction.get('value');
                if (value == parentValue) {
                    icon = 'fa fa-check';
                }
            }
            const title = data.text || labelLocalized || self.localize(action.title);
            //default:
            //element = '<li><a title="' + title + ' ' + keyComboString + '">';
            element = '<li class="" title="' + title + ' ' + keyComboString + '">';
            const _icon = data.icon || icon;
            //icon
            if (typeof _icon !== 'undefined') {
                //already html string
                if (/<[a-z][\s\S]*>/i.test(_icon)) {
                    element += _icon;
                } else {
                    element += '<span style="" class="icon ' + _icon + '"/> ';
                }
            }
            element +='<a class="">';
            element += data.text;
            //element += '<span style="max-width:100px" class="text-muted pull-right ellipsis keyboardShortCut">' + (showKeyCombo ? keyComboString : "") + '</span></a></li>';
            element += '</a></li>';
            self.setVisibilityField(action, 'widget', data);
            return $(element);
        },
        init2: function(opts){
            let options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            const self = this;
            const root = $(document);
            this.__on(root,'click',null,function(e){
                if(!self.isOpen){
                    return;
                }
                self.isOpen=false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display:''
                }).find('.drop-left').removeClass('drop-left');
            });

            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }

            function onOpenHandler($sub,$root){
                $sub.focus();
                $($sub.find("LI")[0]).addClass('focus');
                $($sub.find("LI")[0]).attr('tabindex',"-1");
                $($sub.find("LI")[0]).focus();

            }

            function mouseEnterHandler(e){
                const _root = $(e.currentTarget);
                let $sub = _root.find('.dropdown-context-sub:first');
                let didPopup = false;
                if ($sub.length === 0) {
                    $sub = _root.data('sub');
                    if($sub){
                        didPopup = true;
                    }else {
                        return;
                    }
                }

                const data = $sub.data('data');
                const level = data ? data[0].level : 0;
                const isFirst = level ===1;
                //if (self.menu) {
                //if (!$.contains(self.menu[0], _root[0])) {
                //console.error('not me');
                //return;
                //}
                //}
                $sub.css('display', 'none');
                $sub.data('left',false);
                _root.data('left',false);
                if(isFirst) {
                    $sub.css('display', 'initial');
                    $sub.css('position', 'initial');

                    const doHide = false;

                    function close() {
                        if(doHide==false){
                            return;
                        }
                        const _wrapper = $sub.data('_popupWrapper');
                        popup.close($sub[0]);
                        /*
                        popup.close({
                            domNode: $sub[0],
                            _popupWrapper: _wrapper
                        });
                        */
                    }

                    if(_root.data('didSetup')!==true){
                        _root.data('didSetup',true);
                        _root.data('sub',$sub);
                        _root.on('mouseleave', function (e) {
                            const _thisSub = _root.data('sub');
                            const next = e.toElement;
                            if(next==_thisSub[0] || $.contains(_thisSub[0],next)){
                                return;
                            }
                            close();
                        });
                    }


                    if (!didPopup) {
                        _root.data('sub', $sub);
                        $sub.data('owner', self);
                        if(!$sub.data('didSetup')){
                            function focusNextUp(origin){

                            }
                            $sub.data('root',_root);
                            $sub.on('mouseleave', function () {
                                $sub.data('left',true);
                                close();

                            });
                            $sub.data('didSetup',true);
                            $sub.on('mouseenter', function(e){
                                $sub.data('entered',true);
                            });
                            $sub.attr('data-element',"UL");
                            $sub.find("LI").on('mouseleave',function(e){
                               $(e.currentTarget).removeClass('focus');
                            });
                            $sub.find("LI").on('mouseenter',function(e){
                                $(e.currentTarget).addClass('focus');
                            });
                            $sub.on('keydown',function(evt){
                                //console.log('keydown',evt);
                            });
                            //Gridnav($sub[0]);
                        }
                    }
                    function open() {
                        popup.open({
                            popup: $sub[0],
                            around: _root[0],
                            orient: ['below', 'above'],
                            maxHeight: -1,
                            owner: self,
                            extraClass: 'ActionToolbar',
                            onExecute: function () {
                                self.closeDropDown(true);
                            },
                            onCancel: function () {
                                close();
                            },
                            onClose: function () {
                                _root.data('left',true);
                                close();
                            }
                        });

                        $sub.focus();
                    }
                    open();
                    clearTimeout($sub.data('openTimer'));
                    $sub.data('openTimer',setTimeout(function(){
                        if($sub.data('left')!==true) {
                            onOpenHandler($sub,_root);
                            //$sub.css('display', 'block');
                            //open();
                            //console.log('left sub main');
                        }else{
                            $sub.css('display', 'none');
                        }
                    },OPEN_DELAY));

                    return;
                }else{
                    $sub.css('display','block');
                    if(!$sub.data('didSetup')){
                        $sub.data('didSetup',true);
                        $sub.on('mouseleave',function(){
                            $sub.css('display','');
                            $sub.data('left',true);

                        });
                    }
                    if(!_root.data('didSetup')){
                        _root.data('didSetup',true);
                        _root.on('mouseleave',function(){
                            _root.data('left',true);
                            $sub.css('display','');

                        });
                    }
                }
                $sub.css({
                    top: 0
                });

                const autoH = $sub.height() + 0;
                const totalH = $('html').height();
                const pos = $sub.offset();
                const overlapYDown = totalH - (pos.top + autoH);
                if ((pos.top + autoH) > totalH) {
                    $sub.css({
                        top: overlapYDown - 30
                    }).fadeIn(options.fadeSpeed);
                }

                ////////////////////////////////////////////////////////////
                const subWidth = $sub.width();

                const subLeft = $sub.offset().left;
                const collision = (subWidth + subLeft) > window.innerWidth;

                if (collision) {
                    $sub.addClass('drop-left');
                }
            }

            this.__on(root,'mouseenter', '.dropdown-submenu', mouseEnterHandler.bind(this));
        },
        resize:function(){
            this._height = this.$navBar.height();
            this.$navigation.css('height',this._height);
        },
        destroy:function(){
            utils.destroy(this.$navBar[0]);
            utils.destroy(this.$navigation[0]);
        },
        buildMenu:function (data, id, subMenu,update) {
            const subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ';
            const menuString = '<ul tabindex="-1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>';
            const $menu = update ? (this._rootMenu || this.$navBar || $(menuString)) : $(menuString);

            if(!subMenu){
                this._rootMenu = $menu;
            }
            $menu.data('data',data);
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        setActionStore: function (store, owner,subscribe,update,itemActions) {
            if(!update && store && this.store && store!=this.store){
                this.removeCustomActions();
            }

            if(!update){
                this._clear();
                this.addActionStore(store);
            }

            if(!store){
                return;
            }

            this.store = store;

            const self = this;
            const visibility = self.visibility;
            const rootContainer = $(self.getRootContainer());

            const tree = update ? self.lastTree : self.buildActionTree(store,owner);

            const allActions = tree.allActions;
            const rootActions = tree.rootActions;
            const allActionPaths = tree.allActionPaths;
            const oldMenuData = self.menuData;

            if(subscribe!==false) {
                if(!this['_handleAdded_' + store.id]) {
                    this.addHandle('added', store._on('onActionsAdded', function (actions) {
                        self.onActionAdded(actions);
                    }));

                    this.addHandle('delete', store.on('delete', function (evt) {
                        self.onActionRemoved(evt);
                    }));
                    this['_handleAdded_' + store.id]=true;
                }
            }

            // final menu data
            const data = [];
            if(!update) {
                _.each(tree.root, function (menuActions, level) {
                    const lastGroup = '';
                    _.each(menuActions, function (command) {
                        let action = self.getAction(command, store);
                        let isDynamicAction = false;
                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }
                        if (action) {
                            const renderData = self.getActionData(action);
                            const icon = renderData.icon;
                            const label = renderData.label;
                            const visibility = renderData.visibility;
                            const group = renderData.group;

                            const lastHeader = {
                                header: ''
                            };


                            if (visibility === false) {
                                return;
                            }

                            /*
                             if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 1) {
                             //if (lastGroup !== group) {
                             var name = groupedActions[group].length >= 2 ? i18.localize(group) : "";
                             lastHeader = {divider: name,vertial:true};
                             data.push(lastHeader);
                             lastGroup = group;
                             //}
                             }
                             */

                            const item = self.toMenuItem(action, owner, '', icon, visibility || {}, false);
                            data.push(item);
                            item.level = 0;
                            visibility.widget = item;

                            self.addReference(action, item);

                            const childPaths = new Path(command).getChildren(allActionPaths, false);
                            const isContainer = childPaths.length > 0;

                            function parseChildren(command, parent) {
                                const childPaths = new Path(command).getChildren(allActionPaths, false);
                                const isContainer = childPaths.length > 0;
                                const childActions = isContainer ? self.toActions(childPaths, store) : null;
                                if (childActions) {
                                    const subs = [];
                                    _.each(childActions, function (child) {
                                        const _renderData = self.getActionData(child);
                                        const _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility,false);
                                        const parentLevel = parent.level || 0;
                                        _item.level = parentLevel + 1;
                                        self.addReference(child, _item);
                                        subs.push(_item);
                                        const _childPaths = new Path(child.command).getChildren(allActionPaths, false);
                                        const _isContainer = _childPaths.length > 0;
                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }
                            parseChildren(command, item);
                            self.buildMenuItems(rootContainer, [item], "-" + new Date().getTime());
                        }
                    });
                });
                self.onDidRenderActions(store, owner);
                this.menuData = data;
            }else{
                if(itemActions || !_.isEmpty(itemActions)) {

                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            const action = self.getAction(newAction.command);
                            if (action){
                                const renderData = self.getActionData(action);
                                const icon = renderData.icon;
                                const label = renderData.label;
                                const aVisibility = renderData.visibility;
                                const group = renderData.group;
                                const item = self.toMenuItem(action, owner, label, icon, aVisibility || {},null,false);

                                if(aVisibility.widget){
                                    return;
                                }

                                aVisibility.widget = item;

                                self.addReference(newAction, item);

                                if(!action.getParentCommand){
                                    return;
                                }
                                const parentCommand = action.getParentCommand();
                                const parent = self._findParentData(oldMenuData,parentCommand);
                                if(parent && parent.subMenu){
                                    parent.lazy = true;
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });
                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }
            this.resize();
        },
        startup:function(){
            this.correctSubMenu = true;
            this.init2({
                preventDoubleContext: false
            });
            this.menu = this.$navigation;
        }
    });

    const MainMenu = dcl([ContainerClass,_MenuMixin4,ActionRendererClass,_XWidget.StoreMixin],{
        declaredClass:'xide.widgets.Actiontoolbar',
        target:null,
        attachToGlobal:true,
        _isFollowing:false,
        _followTimer:null,
        _zIndex:1,
        hideSubsFirst:true,
        visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
        _renderItem:function(item,data,$menu,action, owner, label, icon, visibility, showKeyCombo, lazy){
            const self = this;
            const labelLocalized = self.localize(label);
            const actionType = visibility.actionType || action.actionType;
            const parentAction = action.getParent ? action.getParent() : null;
            const closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
            let keyComboString = ' \n';
            let element = null;
            if (action.keyboardMappings && showKeyCombo !== false) {
                const mappings = action.keyboardMappings;
                const keyCombos = mappings[0].keys;
                if (keyCombos && keyCombos.length) {
                    keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                }
            }

            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                element = '<li class="" >';
                const id = action._store.id + '_' + action.command + '_' + self.id;
                const checked = action.get('value');
                //checkbox-circle
                element += '<div class="action-checkbox checkbox checkbox-success ">';
                element += '<input id="' + id + '" type="checkbox" ' + (checked === true ? 'checked' : '') + '>';
                element += '<label for="' + id + '">';
                element += self.localize(data.text);
                element += '</label>';
                element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                element += '</div>';

                $menu.addClass('noclose');
                const result = $(element);
                const checkBox = result.find('INPUT');
                checkBox.on('change', function (e) {
                    action._originReference = data;
                    action._originEvent = e;
                    action.set('value', checkBox[0].checked);
                    action._originReference = null;
                });
                self.setVisibilityField(action, 'widget', data);
                return result;
            }
            closeOnClick === false && $menu.addClass('noclose');
            if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                const value = action.value || action.get('value');
                const parentValue = parentAction.get('value');
                if (value == parentValue) {
                    icon = 'fa fa-check';
                }
            }
            const title = data.text || labelLocalized || self.localize(action.title);
            //default:
            //element = '<li><a title="' + title + ' ' + keyComboString + '">';
            element = '<li class="" title="' + title + ' ' + keyComboString + '">';
            const _icon = data.icon || icon;
            //icon
            if (typeof _icon !== 'undefined') {
                //already html string
                if (/<[a-z][\s\S]*>/i.test(_icon)) {
                    element += _icon;
                } else {
                    element += '<span style="" class="icon ' + _icon + '"/> ';
                }
            }
            element +='<a class="">';
            element += data.text;
            //element += '<span style="max-width:100px" class="text-muted pull-right ellipsis keyboardShortCut">' + (showKeyCombo ? keyComboString : "") + '</span></a></li>';
            element += '</a></li>';
            self.setVisibilityField(action, 'widget', data);
            return $(element);
        },
        init2: function(opts){
            let options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            const self = this;
            const root = $(document);
            this.__on(root,'click',null,function(e){
                if(!self.isOpen){
                    return;
                }
                self.isOpen=false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display:''
                }).find('.drop-left').removeClass('drop-left');
            });
            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }

            function correctPosition(_root,$sub){
                const level = 0;
                let _levelAttr = $sub.attr('level');
                if(!_levelAttr){
                    _levelAttr = parseInt($sub.parent().attr('level'),10);
                }

                if(_levelAttr===0){
                    $sub.css({
                        left:0
                    });
                }
                const parent = _root.parent();
                const parentTopAbs = parent.offset().top;
                const rootOffset = _root.offset();
                const rootPos = _root.position();
                const rootHeight = _root.height();
                const parentDelta = parentTopAbs - rootOffset.top;
                let newTop = (rootPos.top + rootHeight) + parentDelta;
                const offset = -20;
                if(_levelAttr!==0) {
                    newTop += offset;
                }
                $sub.css({
                    top:newTop
                });
                const subHeight = $sub.height() + 0;
                const totalH = $('html').height();
                const pos = $sub.offset();
                const overlapYDown = totalH - (pos.top + subHeight);
                if((pos.top + subHeight) > totalH){
                    $sub.css({
                        top: overlapYDown - 30
                    });
                }

                ////////////////////////////////////////////////////////////
                const subWidth = $sub.width();

                const subLeft = $sub.offset().left;
                const collision = (subWidth+subLeft) > window.innerWidth;

                if(collision){
                    $sub.addClass('drop-left');
                }
            }

            function mouseEnterHandler(e){
                const _root = $(e.currentTarget);
                let $sub = _root.find('.dropdown-context-sub:first');
                let didPopup = false;
                if ($sub.length === 0) {
                    $sub = _root.data('sub');
                    if($sub){
                        didPopup = true;
                    }else {
                        return;
                    }
                }

                const data = $sub.data('data');
                const level = data ? data[0].level : 0;
                const isFirst = level ===1;

                $sub.css('display', 'none');
                $sub.data('left',false);
                _root.data('left',false);
                if(isFirst) {
                    $sub.css('display', 'initial');
                    $sub.css('position', 'initial');
                    function close() {
                        $sub.data('open',false);
                        popup.close($sub[0]);
                    }
                    if(_root.data('didSetup')!==true){
                        _root.data('didSetup',true);
                        _root.data('sub',$sub);
                        _root.on('mouseleave', function (e) {
                            const _thisSub = _root.data('sub');
                            const next = e.toElement;
                            if(next==_thisSub[0] || $.contains(_thisSub[0],next)){
                                return;
                            }
                            close();
                        });
                    }
                    if (!didPopup) {
                        _root.data('sub', $sub);
                        $sub.data('owner', self);
                        if(!$sub.data('didSetup')){
                            $sub.data('root',_root);
                            $sub.on('mouseleave', function () {
                                $sub.data('left',true);
                                close();

                            });
                            $sub.data('didSetup',true);
                            $sub.on('mouseenter', function(e){
                                $sub.data('entered',true);
                            });
                        }
                    }
                    function open() {

                        if($sub.data('open')){
                            return;
                        }
                        $sub.data('open',true);
                        const wrapper = popup.open({
                            popup: $sub[0],
                            around: _root[0],
                            orient: ['below', 'above'],
                            maxHeight: -1,
                            owner: self,
                            extraClass: 'ActionToolbar',
                            onExecute: function () {
                                self.closeDropDown(true);
                            },
                            onCancel: function () {
                                close();
                            },
                            onClose: function () {
                                _root.data('left',true);
                                close();
                            }
                        });
                        correctPosition(_root,$sub,$(wrapper));
                    }
                    open();
                    //clearTimeout($sub.data('openTimer'));

                    return;
                    $sub.data('openTimer',setTimeout(function(){
                        if($sub.data('left')!==true) {

                        }else{
                            console.log('left');
                            $sub.css('display', 'none');
                        }
                    },OPEN_DELAY));

                    return;
                }else{
                    $sub.css('display','block');
                    if(!$sub.data('didSetup')){
                        $sub.data('didSetup',true);
                        $sub.on('mouseleave',function(){
                            $sub.css('display','');
                            $sub.data('left',true);

                        });
                    }
                    if(!_root.data('didSetup')){
                        _root.data('didSetup',true);
                        _root.on('mouseleave',function(){
                            _root.data('left',true);
                            $sub.css('display','');

                        });
                    }
                }

                /*
                 $sub.css({
                 top: 0
                 });

                 */



                //console.log('correct position');

                return;

                const autoH = $sub.height() + 0;
                const totalH = $('html').height();
                const pos = $sub.offset();
                const overlapYDown = totalH - (pos.top + autoH);
                if ((pos.top + autoH) > totalH) {
                    $sub.css({
                        top: overlapYDown - 30
                    }).fadeIn(options.fadeSpeed);
                }

                ////////////////////////////////////////////////////////////
                const subWidth = $sub.width();

                const subLeft = $sub.offset().left;
                const collision = (subWidth + subLeft) > window.innerWidth;

                if (collision) {
                    $sub.addClass('drop-left');
                }
            }
            this.__on(root,'mouseenter', '.dropdown-submenu', mouseEnterHandler.bind(this));
        },
        resize:function() {
            this._height = this.$navBar.height();
            if (this._height > 0){
                this.$navBar.css('width',this.$navigation.width());
                this.$navigationRoot.css('width',this.$navigation.width());
                this.$navigation.css('height', this._height);
                this.$navigationRoot.css('height', this._height);
            }
        },
        destroy:function(){
            utils.destroy(this.$navBar[0]);
            utils.destroy(this.$navigation[0]);
        },
        buildMenu:function (data, id, subMenu,update) {
            const subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ';
            const menuString = '<ul aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>';
            const $menu = update ? (this._rootMenu || this.$navBar || $(menuString)) : $(menuString);

            if(!subMenu){
                this._rootMenu = $menu;
            }
            $menu.data('data',data);
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        setActionStore: function (store, owner,subscribe,update,itemActions) {
            if(!update && store && this.store && store!=this.store){
                this.removeCustomActions();
            }

            if(!update){
                this._clear();
                this.addActionStore(store);
            }

            if(!store){
                return;
            }

            this.store = store;

            const self = this;
            const visibility = self.visibility;
            const rootContainer = $(self.getRootContainer());

            const tree = update ? self.lastTree : self.buildActionTree(store,owner);

            const allActions = tree.allActions;
            const rootActions = tree.rootActions;
            const allActionPaths = tree.allActionPaths;
            const oldMenuData = self.menuData;

            if(subscribe!==false) {
                if(!this['_handleAdded_' + store.id]) {
                    this.addHandle('added', store._on('onActionsAdded', function (actions) {
                        self.onActionAdded(actions);
                    }));

                    this.addHandle('delete', store.on('delete', function (evt) {
                        self.onActionRemoved(evt);
                    }));
                    this['_handleAdded_' + store.id]=true;
                }
            }

            // final menu data
            const data = [];
            if(!update) {
                _.each(tree.root, function (menuActions, level) {
                    const lastGroup = '';
                    _.each(menuActions, function (command) {
                        let action = self.getAction(command, store);
                        let isDynamicAction = false;
                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }
                        if (action) {
                            const renderData = self.getActionData(action);
                            const icon = renderData.icon;
                            const label = renderData.label;
                            const visibility = renderData.visibility;
                            const group = renderData.group;

                            const lastHeader = {
                                header: ''
                            };


                            if (visibility === false) {
                                return;
                            }

                            /*
                             if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 1) {
                             //if (lastGroup !== group) {
                             var name = groupedActions[group].length >= 2 ? i18.localize(group) : "";
                             lastHeader = {divider: name,vertial:true};
                             data.push(lastHeader);
                             lastGroup = group;
                             //}
                             }
                             */

                            const item = self.toMenuItem(action, owner, '', icon, visibility || {}, false);
                            data.push(item);
                            item.level = 0;
                            visibility.widget = item;

                            self.addReference(action, item);

                            const childPaths = new Path(command).getChildren(allActionPaths, false);
                            const isContainer = childPaths.length > 0;

                            function parseChildren(command, parent) {
                                const childPaths = new Path(command).getChildren(allActionPaths, false);
                                const isContainer = childPaths.length > 0;
                                const childActions = isContainer ? self.toActions(childPaths, store) : null;
                                if (childActions) {
                                    const subs = [];
                                    _.each(childActions, function (child) {
                                        const _renderData = self.getActionData(child);
                                        const _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility,false);
                                        const parentLevel = parent.level || 0;
                                        _item.level = parentLevel + 1;
                                        self.addReference(child, _item);
                                        subs.push(_item);
                                        const _childPaths = new Path(child.command).getChildren(allActionPaths, false);
                                        const _isContainer = _childPaths.length > 0;
                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }
                            parseChildren(command, item);
                            self.buildMenuItems(rootContainer, [item], "-" + new Date().getTime());
                        }
                    });
                });
                self.onDidRenderActions(store, owner);
                this.menuData = data;
            }else{
                if(itemActions || !_.isEmpty(itemActions)) {

                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            const action = self.getAction(newAction.command);
                            if (action){
                                const renderData = self.getActionData(action);
                                const icon = renderData.icon;
                                const label = renderData.label;
                                const aVisibility = renderData.visibility;
                                const group = renderData.group;
                                const item = self.toMenuItem(action, owner, label, icon, aVisibility || {},null,false);

                                if(aVisibility.widget){
                                    return;
                                }

                                aVisibility.widget = item;

                                self.addReference(newAction, item);

                                if(!action.getParentCommand){
                                    return;
                                }
                                const parentCommand = action.getParentCommand();
                                const parent = self._findParentData(oldMenuData,parentCommand);
                                if(parent && parent.subMenu){
                                    parent.lazy = true;
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });
                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }
            this.resize();
        },
        startup:function(){
            this.correctSubMenu = true;
            this.init2({
                preventDoubleContext: false
            });
            this.menu = this.$navigation;
        }
    });

    dcl.chainAfter(MainMenu,'destroy');

    const actions = [];
    const thiz = this;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let CIS;


    function doTests(tab){
        const grid = FTestUtils.createFileGrid('root',{},{

        },'TestGrid',module.id,false,tab);

        const menuNode = grid.header;
        /*
        //var context = new MainMenu({}, container.domNode);
        var mainMenu = utils.addWidget(MainMenu,{

        },null,menuNode,true);
        mainMenu.init({preventDoubleContext: false});
        */
        grid.resize();
        grid.showToolbar(true,MainMenu,menuNode,true);
        //////////////////////////////////////////////////
        //mainMenu._registerActionEmitter(grid);
        //mainMenu.setActionEmitter(grid);
        return;
    }
    function doBlockTests(tab){


        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            __permissions:[]
        });


        const menuNode = grid.header;
        //var context = new MainMenu({}, container.domNode);
        /*
        var mainMenu = utils.addWidget(MainMenu,{
        },null,menuNode,true);
        mainMenu.init({preventDoubleContext: false});
        */
        grid.resize();
        //////////////////////////////////////////////////
        /*
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);
        */
        const windowManager = ctx.getWindowManager();
        grid.showToolbar(true,MainMenu,menuNode,true);


        //windowManager.registerView(grid,true);


        setTimeout(function(){
            grid.select([0],null,true,{
                focus:true,
                delay:10
            });
        },1000);

        //grid.add(mainMenu);
        return;
    }
    function doGlobalTests(tab){


        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            //permissions:[]
        });


        const menuNode = grid.header;
        const mainView = ctx.mainView;
        const where = mainView.layoutTop;
        const windowManager = ctx.getWindowManager();
        const application = ctx.getApplication();


        utils.destroy(mainView.toolbar);


        const id = '__' + module.id;
        if(window[id]){
            utils.destroy(window[id]);
        }

        const mainMenu = utils.addWidget(MainMenu,{
            id:id,
            attachToGlobal:false,
            actionFilter:{
                quick:true
            }
        },null,where,true);

        window[id] = mainMenu;


        windowManager.addActionReceiver(mainMenu);
        $(mainMenu.domNode).css({
            //'float':'right',
            'height':'40px',
            'width':'50%'
        });



        mainMenu.init({preventDoubleContext: false});

        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);

        grid.showToolbar(true);

        mainMenu._registerActionEmitter(application.fileGrid);

        setTimeout(function(){
            grid.select([0],null,true,{
                focus:true,
                delay:10
            });
        },1000);





        //grid.add(mainMenu,null,false);



        return;
    }

    var ctx = window.sctx;
    let root;

    if (ctx) {
        const parent = TestUtils.createTab(null,null,module.id);

        //doTests(parent);
        //doGlobalTests(parent);
        doBlockTests(parent);

        window.ACTIONTOOLBARCLASS = MainMenu;


        return declare('a',null,{});
    }
    return _Widget;
});

