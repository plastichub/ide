define("xide/src/src/types", ["require", "exports", "dojo-compose/compose"], function (require, exports, compose_1) {
    "use strict";
    exports.EVENTS = compose_1.default({}).static({});
    let types;
    (function (types_1) {
        let EVENTS;
        (function (EVENTS) {
            EVENTS[EVENTS["ONE"] = 'sd2'] = "ONE";
        })(EVENTS = types_1.EVENTS || (types_1.EVENTS = {}));
        types_1.types = compose_1.default({}).static({
            EVENTS: EVENTS
        });
        class Rectangle {
        }
        types_1.Rectangle = Rectangle;
    })(types = exports.types || (exports.types = {}));
});
