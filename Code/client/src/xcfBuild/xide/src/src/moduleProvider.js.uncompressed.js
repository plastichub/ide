define("xide/src/src/moduleProvider", ["require", "exports"], function (require, exports) {
    "use strict";
    let Shapes;
    (function (Shapes) {
        class Rectangle {
            constructor(height, width) {
                this.height = height;
                this.width = width;
            }
            save(callback) {
                callback(42);
            }
        }
        Shapes.Rectangle = Rectangle;
        // This works!
        const rect1 = new Rectangle(10, 4);
    })(Shapes = exports.Shapes || (exports.Shapes = {}));
    let Point;
    const a = new Point(2);
});
