define("xide/src/src/testEvented", ["require", "exports", "xide/mixins/EventedMixin", "dojo-compose/compose", "dojo-core/aspect"], function (require, exports, EventedMixin, compose_1, aspect_1) {
    "use strict";
    let button;
    aspect_1.on(button, "df", function (e) {
    });
    class Evented {
        constructor() {
            this.__events = {};
        }
        /**
         * @param  {string|Array<string>} type
         * @returns Array
         */
        subscribe(type) {
            return "2";
        }
    }
    const EventedComposed = compose_1.default.create(EventedMixin);
    class another {
    }
    EventedComposed.extend(another);
});
