define("xide/src/src/manager/ContextBase", ["require", "exports", "../Evented"], function (require, exports, Evented_1) {
    "use strict";
    const ContextBase = Evented_1.default.extend("ContextBase", {
        bar: ""
    });
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = ContextBase;
});
