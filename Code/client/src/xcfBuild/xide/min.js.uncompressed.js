define("xide/min", [
    'xide/utils',
    'xide/utils/StringUtils',
    'xide/utils/HTMLUtils',
    'xide/utils/WidgetUtils',
    'xide/types',
    'xide/types/Types',
    'xide/factory',
    'xide/factory/Objects',
    'xide/factory/Events',
    'xide/lodash'
], function () {
    if(!Array.prototype.remove) {
        Array.prototype.remove = function () {
            let what;
            const a = arguments;
            let L = a.length;
            let ax;
            while (L && this.length) {
                what = a[--L];
                if (this.indexOf == null) {
                    break;
                }
                while ((ax = this.indexOf(what)) != -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };
    }
});
