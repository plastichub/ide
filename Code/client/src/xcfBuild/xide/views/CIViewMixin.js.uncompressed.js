define("xide/views/CIViewMixin", [
    "dcl/dcl",
    "xdojo/declare",
    "dojo/Stateful",
    'xide/utils',
    'xide/factory',
    'xide/mixins/EventedMixin',
    'xide/layout/_TabContainer',
    'xide/widgets/_Widget',
    'dojo/Deferred',
    "dojo/promise/all",
    'dojo/when'
], function (dcl,declare, Stateful, utils, factory, EventedMixin,_TabContainer,_Widget,Deferred,all,when) {
    const _debug = false;
    let Module = null;
    const Implementation = {
        declaredClass:'xide.views.CIViewMixin',
        widgets: null,
        delegate: null,
        helpNodes: null,
        store: null,
        groups: null,
        groupContainer: null,
        cssClass: 'CIView',
        ciSort:true,
        options: {
        },
        tabContainer: null,
        viewStyle: '',
        tabs: null,
        tabContainerClass:_TabContainer,
        _didRenderCIS:false,
        typeMap:null,
        containerArgs:null,
        getWidgetByType: function (type) {
            for (let i = 0; i < this.widgets.length; i++) {
                const widget = this.widgets[i];
                if (widget.userData.type === type) {
                    return widget;
                }
            }
            return null;
        },
        updateWidget:function(id,prop,value){
            const widget = this.getWidgetById(id);
            if(prop && widget && widget.set){
                widget.set(prop,value);
            }
        },
        getWidgetById: function (id) {
            let result = null;
            _.each(this.widgets,function(widget){
                if (widget.userData && widget.userData.id === id){
                    result=widget;
                }
            });
            return result;
        },
        constructor:function(args){
            utils.mixin(this,args);
        },
        onValueChanged:function(evt){
            this._emit('valueChanged',evt);
        },
        createGroupContainer: function () {
            if (this.tabContainer) {
                return this.tabContainer;
            }
            const tabContainer = utils.addWidget(this.tabContainerClass || _TabContainer,utils.mixin({
                direction:'left',
                style: "min-width:450px;",
                _parent:this,
                resizeToParent:true
            },this.containerArgs),null,this,true);
            this.tabContainer = tabContainer;
            this.add(tabContainer);
            return tabContainer;
        },
        getGroupContainer: function () {
            if (this.groupContainer) {
                return this.groupContainer;
            }
            this.groupContainer = this.createGroupContainer();
            return this.groupContainer;
        },
        createGroupView: function (groupContainer, group,icon,selected) {
            return groupContainer.createTab(group,icon,selected,this.tabContainerClass.tabClass ||  _TabContainer.tabClass);
        },
        attachWidgets: function (data, dstNode,view) {
            const thiz = this;
            dstNode = dstNode || this.domNode;
            const isSingle = !dstNode;
            if(!dstNode && this.tabContainer){
                dstNode = this.tabContainer.containerNode;
            }
            if (!dstNode) {
                console.error('have no parent dstNode!');
                return;
            }
            data = data.reverse();
            for (let i = 0; i < data.length; i++) {
                const widget = data[i];
                widget.delegate = this.owner || this;
                dstNode.appendChild(widget.domNode);
                const ci = widget.userData;
                _debug && console.log('attach widget ',widget);
                if(view && view.lazy===true) {
                    widget._startOnShow = true;
                }else{
                    try {
                        !widget._started && widget.startup();
                    }catch(e){
                        logError(e,'Error starting widget');
                    }
                }
                widget._on('valueChanged',function(evt){
                    evt.view = view;
                    evt.widget = widget;
                    thiz.onValueChanged(evt);
                });
                !widget.__emitted && this._emit('widget',{
                    widget:widget,
                    ci:ci
                });
                widget.__emitted = true;
                this.widgets.indexOf(widget)==-1 && this.widgets.push(widget);
                widget.userData.view=view;
                widget.onAttached && widget.onAttached(view);
                if(view && view.add && view.add(widget)){

                }else{
                    _debug && console.error('view has no add',view);
                    this.add(widget);
                }
            }
        },
        empty: function (destroyHandles) {
            _.each(this.helpNodes,utils.destroy);
            _.each(this.widgets,utils.destroy);
            destroyHandles !==false && this._destroyHandles();
            this.tabs = [];
            this.widgets = [];
        },
        toArray: function (obj) {
            const result = [];
            for (const c in obj) {
                result.push({
                    name: c,
                    value: obj[c]
                });
            }
            return result;
        },
        onRendered:function(){
            const container = this.getGroupContainer();
            if(this.options.select) {
                container.selectChild(this.options.select);
            }else{
                container.selectChild(0);
            }
            container.resize();
        },
        getTypeMap:function(){
        },
        renderGroup:function(container,title,data){
            const view = this.createGroupView(container, title);
            this.tabs.push(view);
            if(this.ciSort) {
                data = data.sort(function (left, right) {
                    const a = left.order || 0;
                    const b = right.order || 0;
                    return a > b ? -1 : 1;
                });
            }
            const groupDfd = factory.createWidgetsFromArray(data, this, null, false,this.getTypeMap(),!this.ciSort);
            const thiz  = this;

            when(groupDfd,function(widgets){
                if (widgets) {
                    _debug && console.log('render group : ' + title,[data,widgets]);
                    thiz.attachWidgets(widgets,view.containerNode,view);
                }
            });

            return groupDfd;
        },
        renderGroups: function (groups) {
            const groupContainer = this.getGroupContainer();
            const _array = groups;
            const thiz = this;
            const dfd = new Deferred();
            const promises = [];

            this.widgets=[];
            this.helpNodes = [];
            for (let i = 0; i < _array.length; i++) {
                try {
                    const groupDfd = this.renderGroup(groupContainer,_array[i].name,_array[i].value);
                    promises.push(groupDfd);
                } catch (e) {
                    logError(e);
                }
            }
            all(promises).then(function(){
                groupContainer.resize();
                thiz.onRendered();
                dfd.resolve();
            });
            return dfd;
        },
        getCIS: function () {
            return this.data;
        },
        initWithCIS: function (data) {
            if(this._didRenderCIS){
                return null;
            }
            this._didRenderCIS = true;
            this.empty(false);
            data = data || this.cis;
            data = utils.flattenCIS(data);
            this.data = data;
            let head = null;
            const thiz = this;

            let groups = _.groupBy(data,function(obj){
                return obj.group;
            });

            const groupOrder = this.options.groupOrder || {};

            const groupsToRender = [];
            groups = this.toArray(groups);

            //filter groups for visible CIs
            _.each(groups,function(group){
                _.find(group.value,{visible:true}) && groupsToRender.push(group);
            });
            groups = groupsToRender;
            const grouped = _.sortByOrder(groups, function(obj){
                return groupOrder[obj.name] || 100;
            });

            if (grouped != null && grouped.length > 1) {
                head = this.renderGroups(grouped);
            } else {
                head = factory.createWidgetsFromArray(data, thiz, null, false,this.getTypeMap(),!this.ciSort);
                when(head,function(widgets){
                    thiz.widgets = widgets;
                    _debug && console.log('attach widgets',widgets);
                    if (widgets) {
                        thiz.attachWidgets(widgets);
                    }
                });
            }
            head.then(function(){
                _.invoke(thiz.widgets,'onDidRenderWidgets',thiz,thiz.widgets);
            });
            if(!this.groupContainer){
                $(this.domNode).addClass('CIViewSingle');
            }
            return head;
        },
        destroy:function(){
            Module.release(this.cis);
            delete this.helpNodes;
            this.delegate = null;
            this.cis=null;
            this.data=null;
            this.groupContainer=null;
            this.tabContainer=null;
            delete this.tabs;
            this.owner = null;
            this.storeItem = null;
            this.storeDelegate = null;

        }
    };
    Module = declare("xide.views.CIViewMixin", [_Widget,EventedMixin],Implementation);
    Module.dcl = dcl([_Widget.dcl,EventedMixin.dcl],Implementation);
    Module.release = function(cis){
        cis = _.map(cis, function (ci) {
            ci.view = null;
            ci._widget = null;
            ci.options = null;
            delete ci.view;
            delete ci._widget;
            delete ci.options;
            return ci;
        });
        return cis;
    }
    return Module;
});