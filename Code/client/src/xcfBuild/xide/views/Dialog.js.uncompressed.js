define("xide/views/Dialog", [
    "dojo/_base/declare",
    "xide/mixins/EventedMixin",
    "xide/factory",
    "dojo/dom-class", // domClass.add domClass.contains
    "dojo/dom-geometry", // domGeometry.position
    "dojo/dom-style", // domStyle.set
    "dojo/window"
], function (declare, EventedMixin, factory,domClass, domGeometry, domStyle,winUtils) {

    //was based from dijit/Dialog
    return declare("xide.views.Dialog", [EventedMixin],{
            cssClass: "Dialog",
            maxRatio: 0.9,
            _messages: null,
            minimizable: false,
            _minimizeButton: null,
            resizeable: true,
            _resizeHandle:null,
            getCSSNode: function () {
                return this.containerNode;
            },
            _buildRendering: function () {
                this.inherited(arguments);
            },
            minimize: function () {},

            _position: function () {
                // summary:
                //		Position the dialog in the viewport.  If no relative offset
                //		in the viewport has been determined (by dragging, for instance),
                //		center the dialog.  Otherwise, use the Dialog's stored relative offset,
                //		adjusted by the viewport's scroll.
                if (!domClass.contains(this.ownerDocumentBody, "dojoMove")) {
                    // don't do anything if called during auto-scroll
                    const node = this.domNode;

                    const viewport = winUtils.getBox(this.ownerDocument);
                    const p = this._relativePosition;
                    const bb = domGeometry.position(node);
                    const l = Math.floor(viewport.l + (p ? Math.min(p.x, viewport.w - bb.w) : (viewport.w - bb.w) / 2));
                    const t = Math.floor(viewport.t + (p ? Math.min(p.y, viewport.h - bb.h) : (viewport.h - bb.h) / 2));

                    domStyle.set(node, {
                        left: l + "px",
                        top: t + "px"
                    });
                }
            },
            _fitContent:function(){
                const thiz = this;
                const contentBox = domGeometry.getContentBox(thiz.containerNode);
                const titleBox = domGeometry.getContentBox(thiz.titleBar);
                const box = domGeometry.getContentBox(thiz.domNode);
                const newHeight = box.h - titleBox.h;
                domStyle.set(thiz.containerNode, {
                    height: newHeight + 'px'
                });
            },
            show: function () {
                const def = this.inherited(arguments);
                const thiz=this;
                this.set('title',this.translate(this.title));
                return def.then(function(){
                    if(thiz.fitContent){
                        thiz._fitContent();
                    }
                    setTimeout(function(){
                        thiz.resize();
                    },10);
                });
            }
        });
});