/** @module xide/views/_CIPanelDialog **/
define("xide/views/_CIPanelDialog", [
    "dcl/dcl",
    'xide/utils',
    'xide/views/_Panel',
    'xide/views/CIView'
], function (dcl, utils, _Panel, CIView) {
    const Module = dcl(_Panel, {
        containerClass: 'CIDialog',
        getContentSize: function () {
            return {
                width: '600px',
                height: '500px'
            }
        },
        getDefaultOptions: function (mixin) {
            const self = this;
            
            const options = {
                "contentSize": this.getContentSize(),
                footerToolbar: [
                    {
                        item: "<button style='margin-left:5px;' type='button'><span class='...'></span></button>",
                        event: "click",
                        btnclass: "btn btn-danger btn-sm",
                        btntext: " Cancel",
                        callback: function (event) {
                            CIView.release(self.cis);
                            event.data.close();
                            self.onCancel();
                        }
                    },
                    {
                        item: "<button style='margin-left:5px;' type='button'><span class='...'></span></button>",
                        event: "click",
                        btnclass: "btn btn-primary btn-sm",
                        btntext: " Ok",
                        callback: function (event) {
                            CIView.release(self.cis);
                            //dont close if owner returns false
                            if (self.onOk(self.changedCIS, event.data) !== false) {
                                event.data.close();
                            }
                        }
                    }
                ]
            };
            utils.mixin(options, mixin);
            return options;
        },
        CIViewClass: CIView,
        CIViewOptions: {},

        onShow: function (panel, contentNode, instance) {
            this.changedCIS = [];
            const self = this;
            this.cisView = utils.addWidget(this.CIViewClass, utils.mixin({
                delegate: this,
                resizeToParent: true,
                ciSort: false,
                options: {
                    groupOrder: {
                        'General': 1,
                        'Send': 2,
                        'Advanced': 4,
                        'Description': 5
                    }
                },
                cis: this.cis
            }, this.CIViewOptions), this, contentNode, false);
            self.add(this.cisView);
            this.cisView.startup();
            this.cisView.startDfd.then(function () {
                self.resize();
            });
            //collect changed CIs
            this.cisView._on('valueChanged', function (evt) {
                const ci = _.find(self.changedCIS, { ci: evt.ci });
                if (ci) {
                    self.changedCIS.remove(ci);
                }
                self.changedCIS.push({
                    ci: evt.ci,
                    oldValue: evt.oldValue,
                    newValue: evt.newValue,
                    dst: evt.ci.dst,
                    props: evt.props
                });
            });
            return [this.cisView];
        }
    });
    dcl.chainAfter(Module, 'onShow');
    return Module;
});