define("xide/client/ClientBase", [
    'dcl/dcl',
    'xide/mixins/EventedMixin',
    'xide/model/Base',
    'dojo/_base/unload',
    'xide/utils'
], function (dcl,EventedMixin,Base, unload,utils) {
    const Module = dcl([Base.dcl,EventedMixin.dcl], {
        declaredClass:"xide.client.ClientBase",
        options: null,
        _socket: null,
        onConnectionReady: function () {
        },
        onClosed: function () {
        },
        destroy: function () {
            if (this._socket && this._socket.close) {
                this._socket.close();
                this.onClosed();
            }
        },
        _defaultOptions: function () {
            return {};
        },
        init: function (args) {
            this.options = utils.mixin(this._defaultOptions(), args.options);
            //disconnect on onload
            unload.addOnUnload(function () {
                this.pageUnloaded=true;
                this.destroy();
            }.bind(this));
        }
    });

    dcl.chainAfter(Module,"destroy");
    return Module;
});