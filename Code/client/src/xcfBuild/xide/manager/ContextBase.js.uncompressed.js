/** module:xide/manager/ContextBase **/
define("xide/manager/ContextBase", [
    'dcl/dcl',
    'xide/factory',
    'xide/types',
    'xide/utils',
    'xide/mixins/EventedMixin',
    'dojo/_base/kernel',
    'dojo/_base/lang'
], function (dcl, factory, types, utils, EventedMixin, dojo, lang) {
    const _debug = false;
    /**
     * @class module:xide/manager/ContextBase
     * @extends module:xide/mixins/EventedMixin
     */
    const Module = dcl(EventedMixin.dcl, {
        declaredClass: "xide.manager.ContextBase",
        language: "en",
        managers: [],
        mixins: null,
        _getModuleDojo: function (mid) {
            return lang.getObject(utils.replaceAll('/', '.', mid)) || lang.getObject(mid);
        },
        _getModuleDcl: function (mid) {
            return (dcl.getObject ? dcl.getObject(mid) || dcl.getObject(utils.replaceAll('/', '.', mid)) : null);
        },
        /**
         *
         * @param module {string}
         * @returns {*|Object|null}
         */
        getModule: function (module) {
            return this._getModuleDojo(module) || this._getModuleDcl(module);
        },
        /**
         * @param mid {string}
         * @param object {module}
         * @returns {*|Object|null}
         */
        setModule: function (mid, module) {
            const moduleDojo = this._getModuleDojo(mid);
            if (moduleDojo) {
                return lang.setObject(utils.replaceAll('/', '.', mid), module);
            }
            const moduleDCL = this._getModuleDcl(mid);
            if (moduleDCL) {
                return dcl.setObject(utils.replaceAll('/', '.', mid), module);
            }
        },
        /***
         * createManager creates and instances and tracks it in a local array.
         * @param clz : class name or prototype
         * @param config {object|null}: explicit config, otherwise its using local config
         * @param ctrArgs {object|null}: extra constructor arguments
         * @returns {module:xide/manager/ManagerBase} : instance of the manager
         */
        createManager: function (clz, config, ctrArgs) {
            try {
                if (!this.managers) {
                    this.managers = [];
                }
                //1. prepare constructor arguments
                const ctrArgsFinal = {
                    ctx: this,
                    config: config || this.config
                };
                utils.mixin(ctrArgsFinal, ctrArgs);
                if (_.isString(clz) && this.namespace) {
                    let _clz = null;
                    if (!clz.includes('.')) {
                        _clz = this.namespace + clz;
                    } else {
                        _clz = '' + clz;
                    }
                    //test this really exists, if not fallback to default namespace
                    if (!dojo.getObject(_clz) || !dcl.getObject(_clz)) {
                        _debug && console.log('creating manager instance : ' + _clz + ' doesnt exists!' + ' Using default! ');
                        clz = this.defaultNamespace + clz;
                    } else {
                        clz = _clz;
                    }
                    _debug && console.log('creating manager instance : ' + clz);
                } else if (_.isObject(clz)) {
                    _debug && console.log('creating manager instance : ' + (clz.declaredClass || clz.prototype.declaredClass));
                }

                //2. create instance
                const mgr = factory.createInstance(clz, ctrArgsFinal);
                if (!mgr) {
                    _debug && console.error('creating manager instance failed : ' + clz);
                    return;
                }

                //3. track instance
                this.managers.push(mgr);

                //4. tell everybody
                factory.publish(types.EVENTS.ON_CREATED_MANAGER, {
                    instance: mgr,
                    className: clz,
                    ctx: this,
                    config: config || this.config
                });

                return mgr;
            } catch (e) {
                console.error('error creating manager ' + e, arguments);
            }
        },
        constructManagers: function () {
        },
        initManagers: function () {
        },
        /***
         * Monkey patch prototypes
         * @param mixins
         */
        doMixins: function (mixins) {
            this.mixins = mixins || this.mixins;

            mixins.forEach(mixin => {
                let obj = this.getModule(mixin.declaredClass);
                if (mixin.declaredClass === this.declaredClass) {
                    obj = this;
                }
                if (obj) {
                    utils.mixin(obj.prototype, mixin.mixin);
                } else {
                    _debug && console.error('couldnt apply mixin to : ' + mixin.declaredClass);
                }
            });
        }
    });
    dcl.chainAfter(Module, 'constructManagers');
    dcl.chainAfter(Module, 'initManagers');
    return Module;
});