define("xide/manager/Application_XIDEVE", [
    'dcl/dcl',
    'xdojo/has',
    'xideve/manager/WidgetManager',
    'dojo/Deferred'
], function (dcl, has, WidgetManager, Deferred) {
    /**
     * @class xide.manager.Application_XIDEVE
     * @augments module:xide/manager/ManagerBase
     */
    return dcl(null, {
        declaredClass: "xide.manager.Application_XIDEVE",
        initXIDEVE: function () {
            const dfd = new Deferred();
            if ( true ) {
                if (!this.ctx.getWidgetManager()) {
                    this.ctx.widgetManager = this.ctx.createManager(WidgetManager, null);
                    this.ctx.widgetManager.init();
                }
            }
            dfd.resolve();
            return dfd;
        }
    });
});