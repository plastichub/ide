define("xide/manager/Application_XNODE", [
    'dcl/dcl',
    'dojo/Deferred'
], function (dcl,Deferred) {
    /**
     * @class xide.manager.Application_XNODE
     * @extends module:xide/manager/ManagerBase
     */
    return dcl(null, {
        declaredClass:"xide.manager.Application_XNODE",
        onXIDELoaded: function (min, WebSocket) {

            const thiz = this;
            const _ctorArgs = {
                delegate: {
                    onServerResponse: function (e) {
                        thiz.ctx.onXIDEMessage(utils.fromJson(e.data));
                    }
                }
            };

            try {
                const client = new WebSocket(_ctorArgs);

                lang.mixin(client, _ctorArgs);

                client.init({
                    options: {
                        host: 'http://127.0.0.1',
                        port: 9994,
                        channel: '',
                        debug: {
                            "all": false,
                            "protocol_connection": true,
                            "protocol_messages": true,
                            "socket_server": true,
                            "socket_client": true,
                            "socket_messages": true,
                            "main": true
                        }
                    }
                });
                client.connect();

            } catch (e) {
                console.error('create client with store : failed : ' + e);
            }

        },
        loadXIDE: function () {
            const thiz = this;
            require(['xide/min', 'xide/client/WebSocket'], function (min, WebSocket) {
                if (thiz._didXIDE) {
                    return;
                }
                thiz._didXIDE = true;
                thiz.onXIDELoaded(min, WebSocket);
            });
        },
        initXNODE: function () {
            const _require = require;
            const dfd = new Deferred();
            const nodeServiceManagerProto = _require('xnode/manager/NodeServiceManager');
            const thiz = this;
            const nodeManager = this.ctx.createManager(nodeServiceManagerProto, null);

            this.ctx.nodeServiceManager = nodeManager;
            nodeManager.init();
            dfd.resolve();
            return dfd;
        }
    });
});