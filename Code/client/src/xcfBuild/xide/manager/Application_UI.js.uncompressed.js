/** module:xide/manager/Application_UI **/
define("xide/manager/Application_UI", [
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'dojo/has',
    'xide/registry',
    'xide/editor/Default',
    "xide/mixins/PersistenceMixin",
    "dojo/has!host-browser?dojo/dom-geometry",
    'xdojo/has!xcf-ui?xide/editor/Registry',
    'dojo/has!host-browser?xfile/views/FileConsole',
    'dojo/has!host-browser?dojo/window',
    'dojo/has!host-browser?xide/editor/Default',
    'dojo/has!xideve?xide/manager/Application_XIDEVE',
    'dojo/has!xnode?xide/manager/Application_XNODE',
    'dojo/has!xfile?xide/manager/Application_XFILE',
    'dojo/has!xblox?xide/manager/Application_XBLOX',
    'dojo/has!xace?xace/views/Editor'

], (
    dcl,
    utils,
    types,
    has,
    registry,
    ACEEditor,
    PersistenceMixin,
    domGeometry,
    Registry,
    FileConsole,
    win,
    Default,
    Application_XIDEVE,
    Application_XNODE,
    Application_XFILE,
    Application_XBLOX,
    Editor
) => {

    const bases = [];
    Application_XIDEVE && bases.push(Application_XIDEVE);
    Application_XNODE && bases.push(Application_XNODE);
    Application_XFILE && bases.push(Application_XFILE);
    Application_XBLOX && bases.push(Application_XBLOX);

    const Persistence = dcl([PersistenceMixin.dcl], {

        declaredClass: 'xcf.manager.ApplicationPersistence',
        defaultPrefenceTheme: 'idle_fingers',
        defaultPrefenceFontSize: 14,
        saveValueInPreferences: true,
        cookiePrefix: '_xcf_application',
        getDefaultPreferences: function () {
            return utils.mixin({
                    theme: this.defaultPrefenceTheme,
                    fontSize: this.defaultPrefenceFontSize
                },
                this.saveValueInPreferences ? {

                } : null);

        },
        onAfterAction: function (action) {
            this.savePreferences({});
            return this.inherited(arguments);
        },
        /**
         * Override id for pref store:
         * know factors:
         *
         * - IDE theme
         * - per bean description and context
         * - by container class string
         * - app / plugins | product / package or whatever this got into
         * -
         **/
        toPreferenceId: function (prefix) {
            prefix = '';
            return (prefix || this.cookiePrefix || '') + '_xcf_application';
        },
        getDefaultOptions: function () {
            //take our defaults, then mix with prefs from store,
            const _super = this.inherited(arguments);
            const _prefs = this.loadPreferences(null);
            (_prefs && utils.mixin(_super, _prefs) || this.savePreferences(this.getDefaultPreferences()));
            return _super;
        }
    });
    /**
     * @class module:xide/manager/Application_UI
     * @augments module:xide/manager/Application
     */
    return dcl(bases, {
        declaredClass: "xide.manager.Application_UI",
        mainView: null,
        leftLayoutContainer: null,
        rightLayoutContainer: null,
        showGUI: true,
        showFiles: true,
        lastPane: null,
        _lastWizard: null,
        _statusMessageTimer: null,
        _maximized: false,
        openFile: function (args) {
            const self = this;
            const _editor = args.editor;
            const editor = Registry.getEditor(_editor) || Registry.getDefaultEditor({
                path: args.file
            }) || Registry.getEditor('Default Editor');

            const _require = window['eRequire'];
            //var app = _require('remote').require('app');
            const app = _require('electron').remote.app;
            //var BrowserWindow = _require('remote').require('browser-window');
            const BrowserWindow = _require('electron').remote.BrowserWindow;
            const wind = BrowserWindow.getFocusedWindow() || BrowserWindow.mainWindow;
            wind && wind.setTitle(args.file);

            if (editor) {
                const editorInstance = editor.onEdit({
                    path: args.file,
                    mount: '__direct__',
                    getPath: function () {
                        return this.path;
                    }
                });
                if (editorInstance._on) {
                    this.ctx.getWindowManager().registerView(editorInstance, true);
                } else if (editorInstance.then) {
                    editorInstance.then(instance => {
                        self.ctx.getWindowManager().registerView(instance, true);
                    });
                }
            } else {
                console.error('--have no editor ', args.editor);
            }
        },
        /***
         * Register custom types
         */
        registerCustomTypes: function () {},
        /**
         * Register new editors for xfile
         */
        registerEditorExtensions: function () {

            const ctx = this.ctx;

            Default.Implementation.ctx = ctx;
            Default.ctx = ctx;

            //sample default text editor open function, not needed
            const editInACE = (item, owner) => {
                let where = null;
                if (owner) {
                    //where = owner ? owner.newTarget : null;
                    if (_.isFunction(owner.newTarget)) {
                        where = owner.newTarget({
                            title: item.name,
                            icon: 'fa-code'
                        });
                    }
                }
                return Default.Implementation.open(item, where, null, null, owner);
            };

            ctx.registerEditorExtension('Default Editor', '*', 'fa-code', this, false, editInACE, ACEEditor, {
                updateOnSelection: false,
                leftLayoutContainer: this.leftLayoutContainer,
                ctx: ctx,
                defaultEditor: true
            });
            if (typeof JSONEditor !== 'undefined') {
                ctx.registerEditorExtension('JSON Editor', 'json', 'fa-code', this, true, null, JSONEditor, {
                    updateOnSelection: false,
                    leftLayoutContainer: this.leftLayoutContainer,
                    ctx: ctx,
                    registerView: true
                });
            }


            types.registerCustomMimeIconExtension('cfhtml', 'fa-laptop');

            this.registerSplitEditors && this.registerSplitEditors();
        },
        collapseNavigation: function (on) {
            const panel = this.ctx.mainView.layoutLeft;
            if (!panel) {
                return;
            }
            const splitter = panel.getSplitter();
            if (splitter) {
                if(on){
                    if (!splitter.isCollapsed()) {
                        //splitter.expand();
                        splitter.collapse(0, 0);
                    }                    
                }else{
                    if (splitter.isCollapsed()) {
                        splitter.expand();
                        //splitter.collapse(0, 0);
                    }    
                }                
            }
        },
        getActions: function () {

            let result = [];
            const thiz = this;

            const settingsMgr = this.ctx.getSettingsManager();

            function collapseNavigation(force) {
                const panel = thiz.ctx.mainView.layoutLeft;
                if (!panel) {
                    return;
                }
                const splitter = panel.getSplitter();
                if (splitter) {
                    if (splitter.isCollapsed()) {
                        splitter.expand() && console.log('expand');
                        settingsMgr.setSetting('navigation', {
                            on: true
                        });
                    } else {
                        splitter.collapse(0, 0) && console.log('collapse');
                        settingsMgr.setSetting('navigation', {
                            on: false
                        });
                    }
                }
            }
            if (has('electronx')) {
                const _require = window['eRequire'];
                const electron = _require('electron');
                const frame = electron.webFrame;

                function zoom(zoomStep) {
                    const now = frame.getZoomFactor();
                    frame.setZoomFactor(now + zoomStep);
                }


                result.push(this.ctx.createAction({
                    label: 'Zoom In',
                    command: 'Window/Zoom In',
                    icon: 'fa-search-plus',
                    keycombo: ['ctrl +'],
                    tab: 'Home',
                    group: 'Window',
                    owner: thiz,
                    handler: function () {
                        zoom(0.1);
                    },
                    mixin: {
                        addPermission: true
                    },
                    onCreate: function (action) {
                        action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, false);
                    }
                }));

                result.push(this.ctx.createAction({
                    label: 'Zoom Out',
                    command: 'Window/Zoom Out',
                    icon: 'fa-search-minus',
                    keycombo: ['ctrl -'],
                    tab: 'Home',
                    group: 'Window',
                    owner: thiz,
                    handler: function () {
                        zoom(-0.1);
                    },
                    mixin: {
                        addPermission: true
                    },
                    onCreate: function (action) {
                        action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, false);
                    }
                }));

                if (window['eRequire']) {
                    result.push(this.ctx.createAction({
                        label: 'Open In Browser',
                        command: 'Window/Open In Browser',
                        icon: 'fa-share',
                        tab: 'Home',
                        group: 'Window',
                        owner: thiz,
                        handler: function () {
                            const _require = window['eRequire'];
                            const shell = _require("electron").shell;
                            shell.openExternal(location.href);
                        },
                        mixin: {
                            addPermission: true
                        },
                        onCreate: function (action) {
                            action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, false);
                        }
                    }));
                }
            }

            result.push(this.ctx.createAction({
                label: 'Show Navigation',
                command: 'Window/Navigation',
                icon: 'fa-cube',
                keycombo: ['ctrl f11'],
                tab: 'Home',
                group: 'Window',
                owner: thiz,
                handler: collapseNavigation,
                mixin: {
                    addPermission: true
                },
                onCreate: function (action) {
                    action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, {
                        label: 'Show Nav',
                        group: 'Organize'
                    });
                }
            }));


            result.push(this.ctx.createAction({
                label: 'Maximize',
                command: 'View/Maximize',
                icon: types.ACTION_ICON.MAXIMIZE,
                keycombo: ['alt f11'],
                tab: 'Home',
                group: 'View',
                owner: thiz,
                mixin: {
                    addPermission: true
                },
                handler: function () {
                    const toolbar = thiz.ctx.mainView.getToolbar();
                    if (thiz._maximized) {
                        collapseNavigation();
                        if (toolbar) {
                            toolbar.expand();
                        }
                        thiz._maximized = false;
                    } else {
                        collapseNavigation(true);
                        thiz._maximized = true;
                    }
                    setTimeout(() => {
                        thiz.ctx.mainView.resize();
                        thiz.publish(types.EVENTS.RESIZE);
                    }, 100);
                }
            }));

            if (this.createAppActions) {
                result = result.concat(this.createAppActions());
            }

            return result;
        },
        getConsoleActions: function (where) {
            const ctx = this.ctx;
            const self = this;
            const windowManager = ctx.getWindowManager();

            //	adds 3 shells
            const result = [];

            const shells = [
                ["Javacript", "fa-terminal", 'javascript', null, null, 'return 2'],
                ["Bash-Shell", "fa-terminal", 'sh', null, null, 'ls -l --color=always'],
                ["PHP-Shell", "fa-code", 'php', null, null, '<?php \n\n>']
            ];

            function createShell(action) {
                const mainView = ctx.mainView;
                const container = where || mainView.layoutCenter;
                const parent = windowManager.createTab(action.label, action.icon, container);
                const shell = utils.addWidget(FileConsole.createShellViewClass(), {
                    type: action.shellType,
                    title: action.label,
                    icon: action.icon,
                    ctx: ctx,
                    owner: self,
                    value: action.value
                }, null, parent, true);

                const handlerClass = FileConsole.createShellViewDelegate();
                shell.delegate = new handlerClass();
                return shell;
            }

            function mkAction(title, icon, type, owner, handler, value) {
                const _action = {
                    label: title,
                    command: "Window/" + title,
                    icon: icon || 'fa-code',
                    tab: "Home",
                    group: 'View',
                    handler: handler || createShell,
                    mixin: {
                        addPermission: true,
                        shellType: type,
                        value: value
                    },
                    owner: self
                };
                result.push(ctx.createAction(_action));
            }
            for (let i = 0; i < shells.length; i++) {
                mkAction.apply(self, shells[i]);
            }
            return result;
        },
        getTheme: function () {
            return this.ctx.getSettingsManager().getSetting('theme');
        },
        getThemeActions: function () {
            const result = [];
            const thiz = this;
            const ctx = thiz.ctx;

            // Function to reload page with specified theme, rtl, and a11y settings
            function setUrl(theme, rtl, a11y) {

                const aParams = utils.getUrlArgs(location.href);
                let _newUrl = "?theme=" + theme + (rtl ? "&dir=rtl" : "") + (a11y ? "&a11y=true" : "");
                if (aParams) {
                    _.each(aParams, (value, key) => {
                        if (key !== 'theme') {
                            _newUrl += '&' + key + '=' + value;
                        }
                    });
                }
                location.search = _newUrl;
            }

            //in between theme action handler
            function changeTheme(action) {
                const theme = action.theme;
                ctx.getSettingsManager().write2(null, '.', {
                    id: 'theme'
                }, {
                    value: theme
                }, true, null).then(() => {
                    location.reload();
                });
            }

            //default action props
            const defaults = {
                tab: "Window",
                group: 'View',
                handler:  true  ? changeTheme : action => {
                    setUrl(action.value);
                },
                owner: this
            };

            const mixin = {
                addPermission: true,
                actionType: types.ACTION_TYPE.SINGLE_TOGGLE
            };

            const root = 'Window/IDE-Theme/';
            const icon = 'fa-paint-brush';

            //factory
            function _createAction(item) {
                const parts = item.split('|');
                const label = parts[0];
                const theme = parts[1];

                result.push(ctx.createActionShort(label, root + label, icon, defaults, utils.mixin({
                    theme: theme,
                    value: theme
                }, mixin)));
            }

            //loop with factory
            _.each([
                'Dark|transparent',
                'White|white',
                'Blue Transparent|blue',
                'Gray|gray'
            ], _createAction);

            return result;
        },
        /**
         * Does all
         * @returns {*}
         */
        init: function () {
            this.ctx.addActions(this.getActions());
            if ( false ) {
                this.ctx.addActions(this.getConsoleActions(this.ctx.permissions));
            }
            this.ctx.addActions(this.getThemeActions());
            return this.inherited(arguments);
        },
        /***********************************************************************/
        /*
         * Window utils
         */
        resizeToNode: function (parent) {
            if (parent != null) {
                const parentO = dojo.byId(parent);
                if (parentO) {
                    const size = domGeometry.getMarginBox(parentO);
                    let dstHeight = size.h;
                    dstHeight -= 20;
                    const mainContainer = registry.byId("main");
                    if (mainContainer) {
                        mainContainer.containerNode.style.height = dstHeight + "px";
                        mainContainer.resize();
                        setTimeout(() => {
                            mainContainer.resize();
                        }, 300);
                    }
                }
            }
        },
        /**
         *
         */
        repaint: function () {}
    });
});