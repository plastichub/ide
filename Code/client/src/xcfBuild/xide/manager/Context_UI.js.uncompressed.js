/** module:xide/manager/Context_UI **/
define("xide/manager/Context_UI", [
    'dcl/dcl',
    'dojo/Deferred',
    'dojo/has',
    'xide/types',
    'xide/utils',
    'xide/mixins/EventedMixin',
    'require',
    'xide/manager/PluginManager',
    'xide/manager/WindowManager',
    'xide/manager/NotificationManager',
    'xide/manager/SettingsManager',
    'xide/editor/Registry',
    'xaction/ActionProvider',
    'xide/lodash',
    'xide/manager/Router',
    'dojo/promise/all'
], function (
    dcl,
    Deferred,
    has,
    types,
    utils,
    EventedMixin,
    _require,
    PluginManager,
    WindowManager,
    NotificationManager,
    SettingsManager,
    Registry,
    ActionProvider,
    _,
    Router,
    all
) {
    ! true  &&  true || has.add('xlog', () => true, true);
    var test = false;
    var debugEditors = false;
    if (sctx && test) {
        var m = sctx.getFileManager();
        var store = m.getStore('workspace_user', false);
        var file = './A-MediaPlayer.dhtml';
        var parts = file.split('/');
        store.getItem(file, true).then((root) => {
            console.log('loaded root : ', root);
        });
    }

    const isServer =  false ;
    const isBrowser =  true ;
    /**
     * @class module:xide/manager/Context_UI
     * @extends module:xide/manager/ContextBase
     * @extends module:xaction/ActionProvider
     * @extends module:xide/mixins/EventedMixin
     * @extends module:xide/editor/Registry
     *
     */
    const Module = dcl([EventedMixin.dcl, ActionProvider.dcl, Registry], {
        declaredClass: "xide.manager.Context_UI",
        widgetManager: null,
        settingsManager: null,
        trackingManager: null,
        mainView: null,
        registerRoute: function (config) {
            if (!this.routes) {
                this.routes = [];
            }
            this.routes.push(config);
        },
        ready: function () {
            this.getRouter();
        },
        routes: null,
        /**
         * @type {module:xide/manager/Router}
         */
        router: null,
        /**
         *
         * @returns {module:xide/manager/Router}
         */
        getRouter: function () {
            if (!this.router || !this.router.match) {
                this.router = new Router({
                    routes: this.routes
                });
            }
            return this.router;
        },
        /**
         *
         * @returns {null|module:xide/manager/WindowManager}
         */
        getWindowManager: function () {
            return this.windowManager;
        },
        /**
         *
         * @returns {null|module:xideve/manager/WindowManager}
         */
        getWidgetManager: function () {
            return this.widgetManager;
        },
        getNotificationManager: function () {
            return this.notificationManager;
        },
        getSettingsManager: function () {
            return this.settingsManager;
        },
        getTrackingManager: function () {
            return this.trackingManager;
        },
        getMainView: function () {
            return this.mainView;
        },
        /**
         * Run global actions here
         * @param action {string|module:xaction/ActionModel}
         * @returns {module:dojo/Deferred}
         */
        runAction: function (action) {
            action = this.getAction(action);
            let actionDfd = this.inherited(arguments);
            const who = this;
            if (actionDfd == null && action.handler) {
                actionDfd = action.handler.apply(who, [action]);
            }
            return actionDfd;
        },
        getOpenFilesP: function () {
            const head = new Deferred();
            const settingsManager = this.getSettingsManager();
            const settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };

            const props = settingsStore.getSync('openFiles') || {
                value: {
                    files: []
                }
            };
            const fileManager = this.getFileManager();
            const out = [];
            const defs = [];
            props.value.files.forEach((item) => {
                const store = fileManager.getStore(item.mount, false);
                defs.push(store.getItem(item.path, true).then((item) => {
                    out.push(item);
                }));
            });

            all(defs).then(() => {
                head.resolve(out);
            });

            return head;
        },


        trackEditor: function (item, editor) {
            const settingsManager = this.getSettingsManager();

            const settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };

            const props = settingsStore.getSync('openFiles') || {
                value: {
                    files: []
                }
            };

            const value = props.value;
            if (_.find(value.files, {
                    path: item.path,
                    mount: item.mount
                })) {
                return;
            }
            value.files.push({
                path: item.path,
                mount: item.mount
            })

            settingsManager.write2(null, '.', {
                id: 'openFiles'
            }, {
                value: value
            }, true, null);
        },
        untrackEditor: function (item, editor) {
            const settingsManager = this.getSettingsManager();
            const settingsStore = settingsManager.getStore() || {
                getSync: function () {}
            };

            const props = settingsStore.getSync('openFiles') || {
                value: {
                    files: []
                }
            };
            const value = props.value;
            const data = _.find(value.files, {
                path: item.path,
                mount: item.mount
            });
            value.files.remove(data);
            settingsManager.write2(null, '.', {
                id: 'openFiles'
            }, {
                value: value
            }, true, null);
        },
        openItem: function (item, isBack, select) {
            if (!item) {
                return;
            }

            const editors = Registry.getEditors(item);
            const defaultEditor = _.find(editors, editor => editor.isDefault === true);
            if (defaultEditor) {
                return defaultEditor.onEdit(item, this);
            }
            return this.getWindowManager().openItem(item, null, {
                register: true
            });
        },
        _restoreNavigation: function () {
            const on = this.getSettingsManager().getSetting('navigation') || {
                value: true
            }
            this.getApplication().collapseNavigation(on.on !==undefined ? !on.on : false);
        },
        onComponentsReady: function () {
            // todo : store is leaked!
            this.getSettingsManager().initStore().then(() => {
                this._restoreNavigation();
                this.getOpenFilesP().then((items) => {
                    items.forEach((item) => {
                        this.openItem(item);
                    });
                });
            });
        },
        /***********************************************************************/
        /*
         * Editors
         */
        /**
         * Create editor
         * @param ctrArgs {object} ctor args
         * @param item {module:xfile/model/File}
         * @param editorOverrides
         * @param where {HTMLElement|module:xide/widgets/_Widget}
         * @param owner {*|null}
         * @returns {module:dojo/Deferred}
         */
        createEditor: function (ctrArgs, item, editorOverrides, where, owner) {
            const dfd = new Deferred();
            const registerInWindowManager = owner && owner.registerEditors === true ? true : true;

            if (_.isArray(item)) {
                item = item[0];
            }

            const thiz = this;
            const title = item && item.name ? item.name : (utils.pathinfo(item.path, 'PATHINFO_FILENAME') + '.' + utils.pathinfo(item.path, 'PATHINFO_EXTENSION'));
            const ctx = this;
            const mainView = ctx.mainView;
            const docker = mainView.getDocker();

            const ctrArgsFinal = {
                ctx: ctx,
                config: this.config,
                item: item,
                title: title,
                closeable: true,
                closable: true
            };

            utils.mixin(ctrArgsFinal, ctrArgs);
            if (_.isString(ctrArgs.editorClass)) {
                //test this really exists
                if (!this.getModule(ctrArgs.editorClass)) {
                    return null;
                }
            }

            if (ctrArgs.runFunctionOnly === true) {
                ctrArgs.editorClass.apply(this, [item]);
                dfd.resolve(null);
                return dfd;
            }

            where = where || mainView.layoutCenter;

            const root = docker.addTab(null, {
                title: title,
                target: where ? where._parent : null,
                icon: 'fa-code'
            });

            ctrArgsFinal._parent = root;

            //- tell everybody
            thiz.publish(types.EVENTS.ON_CREATE_EDITOR_BEGIN, {
                args: ctrArgsFinal,
                item: item
            }, this);


            root.set('loading', true);
            const editor = utils.addWidget(ctrArgs.editorClass, ctrArgsFinal, thiz, root, true, null, null, null, editorOverrides);

            debugEditors && console.log('create editor!', editor);

            this.trackEditor(item, editor);

            editor._on('destroy', () => {
                thiz.untrackEditor(item);
            });


            //- tell everybody
            thiz.publish(types.EVENTS.ON_CREATE_EDITOR_END, {
                args: ctrArgsFinal,
                item: item,
                editor: editor
            }, this);

            //- use openItem if possible
            if (editor.openItem) {
                const _dfd = editor.openItem(item);
                dfd.resolve(editor);
                if (registerInWindowManager) {
                    this.getWindowManager().registerView(editor, false);
                }
            }
            //-resize if possible
            root.resize && root.resize();

            if (dfd.then) {
                dfd.then(() => {
                    root.set('loading', false);
                })
            }
            return dfd;
        },
        /**
         *
         * @param name
         * @param extensions
         * @param iconClass
         * @param owner {module:xide/model/Component|*|null}
         * @param isDefault
         * @param onEdit
         * @param editorClass
         * @param editorArgs
         */
        registerEditorExtension: function (name, extensions, iconClass, owner, isDefault, onEdit, editorClass, editorArgs, actions) {
            iconClass = iconClass || 'el-icon-brush';
            let _editorArgs = {
                name: name,
                extensions: extensions,
                onEdit: onEdit,
                iconClass: iconClass,
                owner: this,
                isDefault: isDefault != null ? isDefault : false,
                editorClass: editorClass
            };
            _editorArgs = utils.mixin(_editorArgs, editorArgs);
            if (!onEdit) {
                _editorArgs.onEdit = (item, _owner, overrides) => {
                    //some components may have an additional bootstrap in top of 'run':
                    if (owner && owner.onCreateEditor) {
                        const dfd = new Deferred();
                        owner.onCreateEditor().then(() => {
                            const editor = this.createEditor(_editorArgs, item, overrides, null, _owner);
                            dfd.resolve(editor);
                        });
                        return dfd;
                    }

                    return this.createEditor(_editorArgs, item, overrides, null, _owner);
                }
            }

            if (_.isString(extensions) && !extensions.includes(',')) {
                types.registerCustomMimeIconExtension(extensions, iconClass);
            }
            Registry.onRegisterEditor(_editorArgs);
            this.publish(types.EVENTS.REGISTER_EDITOR, _editorArgs);
        },
        /***********************************************************************/
        /*
         * Global event handlers
         */
        /***********************************************************************/
        /*
         * STD - API
         */
        initManagers: function () {

            if (this.settingsManager) {
                this.settingsManager.init();
            }

            if (this.storeDelegate) {
                this.storeDelegate.appService = this.settingsManager;
            }

            if (this.settingsManager) {
                this.settingsManager.serviceObject = this.serviceObject;
                this.settingsManager.ctx = this;
                this.settingsManager.config = this.config;
                this.settingsManager.init();

            }
            if (this.storeDelegate) {
                this.storeDelegate.serviceObject = this.serviceObject;
            }

            if (this.notificationManager) {
                this.notificationManager.init();
                this.notificationManager.postMessage({
                    message: 'Loading application, please wait!',
                    duration: 1000,
                    showCloseButton: true
                });
            }
            this.application && this.application.init();
            this.windowManager && this.windowManager.init();
            const self = this;
            this.registerEditorExtension('Browser', 'php|html|mp4|doc|xls|pdf|zip|tar|iso|avi|gz|mp3|mkv|ogg|png|jpg|cfhtml', 'fa-play-circle-o', this, false, null, item => {
                window.open(self.getFileManager().getImageUrl(item));
            }, {
                ctx: this.ctx,
                registerView: false,
                runFunctionOnly: true,
                isDefault: false
            });

        },
        constructManagers: function () {
            this.pluginManager = this.createManager(PluginManager);
            this.windowManager = this.createManager(WindowManager);
            this.notificationManager = this.createManager(NotificationManager);
            this.settingsManager = this.createManager(SettingsManager);
        }
    });

    dcl.chainAfter(Module, 'constructManagers');
    dcl.chainAfter(Module, 'initManagers');
    dcl.chainAfter(Module, 'ready');

    return Module;
});