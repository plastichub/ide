/** @module xide/manager/BeanManager */
define("xide/manager/BeanManager", [
    'dcl/dcl',
    "dojo/_base/lang",
    "xdojo/has",
    "xide/utils",
    'xide/encoding/MD5',
    'xide/registry',
    'xide/data/TreeMemory'
], function (dcl, lang, has, utils, MD5, registry, TreeMemory) {

    /**
     * @class module:xide/manager/BeanManager
     */
    const Base = dcl(null, {
        declaredClass: 'xide.manager.BeanManager',
        beanNamespace: 'beanNS',
        beanName: 'beanName',
        beanPriority: -1,
        /**
         *
         * @param title
         * @param scope
         * @param parentId
         * @param path
         * @param isDir
         * @param beanType
         * @returns {{name: *, isDir: *, parentId: *, path: *, beanType: *, scope: *}}
         */
        createItemStruct: function (title, scope, parentId, path, isDir, beanType) {
            return {
                name: title,
                isDir: isDir,
                parentId: parentId,
                path: path,
                beanType: beanType,
                scope: scope
            };
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Standard impl of the bean interface
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getMetaValue: function (item, title) {
            const path = this.itemMetaPath || 'user';
            return utils.getCIInputValueByName(utils.getAt(item, path), title);
        },
        /***
         * @returns {dojo.data.ItemFileWriteStore}
         */
        getStore: function () {
            return this.store;
        },
        /***
         * Common function that this instance is in a valid state
         * @returns {boolean}
         */
        isValid: function () {
            return this.store != null;
        },
        /***
         * @TODO:remove
         * Inits the store with the driver data
         * @param data
         * @returns {module:xide/data/TreeMemory}
         */
        initStore: function (data) {
            this.store = new TreeMemory({
                data: data.items,
                idProperty: 'path'
            });
            this.onStoreReady(this.store);
            return this.store;
        },
        /**
         * Stub
         */
        onStoreReady: function () {
        },
        _onReloaded: function (newModule) {
            this.mergeNewModule(newModule.prototype);
            const _class = this.declaredClass;
            const _module = lang.getObject(utils.replaceAll('/', '.', _class)) || lang.getObject(_class);
            if (_module) {
                if (_module.prototype && _module.prototype.solve) {
                    this.mergeNewModule(_module.prototype);
                }
            }
        }
    });

    if ( true ) {
        /**
         * @class module:xide/manager/BeanManager
         */
        return dcl(Base, {
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  UX related utils
            //
            /////////////////////////////////////////////////////////////////////////////////////
            /***
             * Determine a parent container for new views. Currently
             * the 'Application' holds a 'mainView' instance which
             * prepares us a tab container.
             *
             * @returns {*}
             */
            getViewTarget: function () {
                const mainView = this.ctx.getApplication().mainView;
                return mainView.getNewAlternateTarget();
            },
            getRightTopTarget: function (clear, open, style, className) {
                const mainView = this.ctx.getApplication().mainView;
                return mainView.getRightTopTarget(clear, open, style, className);
            },
            getLayoutRightMain: function (clear, open) {
                const mainView = this.ctx.getApplication().mainView;
                return mainView.getLayoutRightMain(clear, open);
            },
            getRightBottomTarget: function (clear, open) {
                const mainView = this.ctx.getApplication().mainView;
                return mainView.getRightBottomTarget(clear, open);
            },
            /***
             * getViewId generates a unique id upon a driver's scope and the drivers path.
             * @param item
             * @param suffix {string}
             * @returns {string}
             */
            getViewId: function (item, suffix) {
                return this.beanNamespace + MD5(utils.toString(item.scope) + utils.toString(item.path), 1) + (suffix != null ? suffix : '');
            },
            /**
             *
             * @param item
             */
            getView: function (item, suffix) {
                const id = this.getViewId(item, suffix);
                return registry.byId(id);
            },
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Data related
            //
            /////////////////////////////////////////////////////////////////////////////////////

            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Server methods
            //
            /////////////////////////////////////////////////////////////////////////////////////
            createGroup: function (scope, name, readyCB) {
                return this.callMethodEx(null, 'createGroup', [scope, name], readyCB, true);
            },
            removeGroup: function (scope, path, name, readyCB) {
                return this.callMethodEx(null, 'removeGroup', [scope, path, name], readyCB, true);
            },
            removeItem: function (scope, path, name, readyCB) {
                return this.callMethodEx(null, 'removeItem', [scope, path, name], readyCB, true);
            },
            /**
             * Shared for consumer
             * @param ci
             * @param newValue
             * @param oldValue
             * @param storeRef
             */
            updateCI: function (ci, newValue, oldValue, storeRef) {
                if (ci && storeRef) {
                    this.updateItemMetaData(
                        utils.toString(storeRef.scope), //the scope of the driver
                        utils.toString(storeRef.path),  //the relative path of the driver
                        this.itemMetaStorePath || '/inputs',  //the path of the CIS in the meta db
                        {
                            id: utils.toString(ci.id)
                        },
                        {
                            value: newValue
                        }
                    );
                }
            },
            /**
             * updateItemMetaData updates a CI in the drivers meta data store
             * @param scope {string}
             * @param driverMetaPath {string}
             * @param dataPath {string} : /inputs
             * @param query
             * @param value
             * @param readyCB
             * @param errorCB
             * @returns {*}
             */
            updateItemMetaData: function (scope, driverMetaPath, dataPath, query, value, readyCB, errorCB) {
                return this.callMethodEx(null, 'updateItemMetaData', [scope, driverMetaPath, dataPath, query, value], readyCB, false);
            },
            mergeNewModule: function (source) {
                for (const i in source) {
                    const o = source[i];
                    if (o && _.isFunction(o)) {
                        this[i] = o;
                    }
                }
            }
        });
    }else{
        return Base;
    }
});