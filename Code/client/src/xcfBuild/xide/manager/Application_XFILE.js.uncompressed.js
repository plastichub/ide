define("xide/manager/Application_XFILE", [
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'xdojo/has',
    'dojo/Deferred'
], function (dcl,utils,types,has,Deferred) {
    return dcl(null,{
        declaredClass:"xide.manager.Application_XFILE",
        onXFileReady: function (config,gridClass) {
            if (!has('showFiles')) {
                return;
            }
            const _require = require;
            const thiz = this;
            const mainView = thiz.mainView;
            const toolbar = mainView.getToolbar();
            const container = this.leftLayoutContainer;
            const breadcrumb = mainView && mainView.getBreadcrumb ? mainView.getBreadcrumb() : null;
            const state = this.getNavigationState && this.getNavigationState() || {files:false};

            if(breadcrumb) {
                this.subscribe(types.EVENTS.ON_OPEN_VIEW, function (e) {
                    const view = e.view;
                    if(view instanceof gridClass) {
                        view.addHandle('click',view.on('click',function(){
                            breadcrumb.setSource(view);
                        }));
                        breadcrumb.setSource(view);
                        const srcStore = view.collection;
                        function _onChangeFolder(store,item,grid){
                            if(breadcrumb.grid!=grid){
                                breadcrumb.setSource(grid);
                            }
                            breadcrumb.clear();
                            breadcrumb.setPath('.',srcStore.getRootItem(),item.getPath(),store);
                        }

                        view._on('openFolder', function (evt) {
                            _onChangeFolder(srcStore,evt.item,view);
                        });
                    }
                });
            }

            if(!container){
                return;
            }

            let newTarget= mainView.layoutCenter;
            const ctx = this.ctx;

            if(mainView.getNewDefaultTab){
                newTarget = function(args){
                    return mainView.getNewDefaultTab(args);
                }
            }
            _require([
                'xfile/factory/Store',
                'xfile/types',
                'xfile/views/Grid',
                'xfile/views/FileGrid',
                'xfile/views/FilePreview'
            ],function(factory,types,Grid,FileGrid,FilePreview){
                ctx.registerEditorExtension('Preview', 'mp4|ma4|mov|html|pdf|avi|mp3|mkv|ogg|png|jpg', 'fa-play', this, true, null, FilePreview.EditorClass, {
                    updateOnSelection: false,
                    leftLayoutContainer: newTarget,
                    ctx: ctx
                });

                const store = factory.createFileStore('workspace_user',null,config,null,thiz.ctx);
                const tab = container.createTab('Files','fa-folder',state.files ? true : ! true );
                const grid = utils.addWidget(FileGrid,{
                    newTabArgs:{
                        showHeader:true
                    },
                    style:'height:100%',

                    newTarget: newTarget,
                    collection: store.getDefaultCollection(),
                    showHeader: false,
                    registerEditors:true,
                    open: state.files ? true : ! true ,
                    resizeToParent:true,
                    _columns: {
                        "Name": true,
                        "Path": false,
                        "Size": false,
                        "Modified": false
                    },
                    title:'Files',
                    icon:'fa-folder'
                },null,tab,false);
                tab.add(grid);
                thiz.ctx.getWindowManager().registerView(grid,true);
                if(breadcrumb){
                    breadcrumb.setSource(grid);
                    const srcStore = grid.collection;

                    function _onChangeFolder(store, item, grid) {
                        if (breadcrumb.grid != grid) {
                            breadcrumb.setSource(grid);
                        }
                        breadcrumb.clear();
                        breadcrumb.setPath('.', srcStore.getRootItem(), item.getPath(), store);
                    }

                    grid._on('openedFolder', function (evt) {
                        _onChangeFolder(srcStore, evt.item, grid);
                    });
                }
                thiz.fileGrid = grid;

                grid._on('startup',function(){
                    grid.showStatusbar(false);
                });



                if(! true ){
                    grid.startup();
                    grid.resize();
                }

                thiz.addNavigationGrid && thiz.addNavigationGrid(grid);
                if(thiz._saveNavigationState) {
                    state.files && setTimeout(function(){
                        grid.startup();
                    },500);
                }

            });
        },
        initXFile: function () {
            let config = {};
            const dfd = new Deferred();
            types.config = config = {};
            const xFileConfiguration = config || typeof(xFileConfig) !== 'undefined' ? xFileConfig : null;
            if (xFileConfiguration) {
                if (typeof xFileConfigMixin != 'undefined') {
                    utils.mixin(xFileConfiguration, xFileConfigMixin);
                }
                utils.mixin(types.config, xFileConfiguration);
            }

            const _re = require;
            const thiz = this;
            const ctx = this.ctx;

            const fMgr = _re([
                'xfile/manager/FileManager',
                'xfile/manager/MountManager',
                'xfile/views/FileGrid'
            ],function(FileManager,MountManager,FileGrid){

                FileGrid.prototype.ctx = ctx;
                ctx.doMixins(ctx.mixins);
                utils.mixin(thiz.config,types.config);

                ctx.fileManager = ctx.createManager(FileManager,thiz.config);
                ctx.fileManager.init();

                ctx.mountManager = ctx.createManager(MountManager,thiz.config);
                ctx.mountManager.init();
                ctx.mountManager.ls().then(function(){
                    thiz.onXFileReady(thiz.config,FileGrid);
                });
                dfd.resolve();
            });

            return dfd;
        }
    });
});