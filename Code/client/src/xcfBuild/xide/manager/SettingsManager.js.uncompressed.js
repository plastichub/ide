/** module:xide/manager/SettingsManager **/
define("xide/manager/SettingsManager", [
    'dcl/dcl',
    "xide/manager/ServerActionBase",
    "xide/utils",
    "xide/manager/ManagerBase",
    "xide/data/Memory",
    "dojo/Deferred"
], function (dcl, ServerActionBase, utils, ManagerBase, Memory, Deferred) {
    const Module = dcl([ManagerBase, ServerActionBase], {
        declaredClass: "xide.manager.SettingsManager",
        serviceClass: 'XApp_Store',
        settingsStore: null,
        settingsDataAll: null,
        section: 'settings',
        store: null,
        has: function (section, path, query, data, readyCB) {},
        getSetting: function (id) {
            const _val = this.getStore().query({
                id: id
            });
            return _val && _val[0] ? _val[0].value : null;
        },
        setSetting: function (id, value) {
            let props = this.getStore().getSync(id);
            if(value==null){
                this.getStore().removeSync(id);
                const prom = this.remove(null, '.', {
                    id: id
                });
                return;
            }
            if(!props){
                props = this.getStore().putSync({
                    id:id,
                    value: value
                })
            }else{
                props.value = value;
            }
            this.getStore().emit('update',{
                target:props
            })
            const prom = this.write2(null, '.', {
                id: id
            }, {
                value: props.value
            }, true, null);
            
            return prom;
        },
        getStore: function () {
            return this.settingsStore;
        },
        _createStore: function (data) {
            return new Memory({
                data: data,
                idProperty: 'id'
            });
        },
        onSettingsReceived: function (data) {
            this.settingsDataAll = data;
            if (!data) {
                this._createStore([]);
                return;
            }
            this.settingsStore = this._createStore(data['' + this.section]);
        },
        replace: function (path, query, operation, newValue, readyCB) {
            const thiz = this;
            const defered = this.serviceObject[this.serviceClass].set(
                this.store,
                path || '.', query, operation, newValue
            );
            defered.addCallback(function (res) {
                if (readyCB) {
                    readyCB(res);
                }
            });
        },
        read: function (section, path, query, readyCB) {
            return this.runDeferred(this.serviceClass, 'get', [section, path, query]).then(function (data) {
                readyCB && readyCB(data);
            }.bind(this));
        },
        update: function (section, path, query, data, decode) {
            return this.runDeferred(null, 'update', [section || this.section, path, query, data, decode]);
        },
        remove: function (section, path, query) {
            return this.runDeferred(null, 'remove', [section || this.section, path, query]);
        },
        write: function (section, path, query, data, decode, readyCB) {
            try {
                const itemLocal = utils.queryStoreEx(this.settingsStore, {
                    id: data.id
                }, true, true);
                if (itemLocal) {
                    return this.update(section, path, {
                        id: data.id
                    }, data.data, decode, readyCB);
                } else {
                    return this.callMethodEx(this.serviceClass, 'set', [section || this.section, path, query, data, decode], readyCB, false);
                }
            } catch (e) {
                logError(e, 'update');
            }
        },
        write2: function (section, path, query, data, decode, readyCB) {
            try {
                //var itemLocal = utils.queryStoreEx(this.settingsStore, {id: data.id}, true, true);
                const itemLocal = utils.queryStoreEx(this.settingsStore, query, true, true);
                if (itemLocal) {
                    utils.mixin(itemLocal, data);
                    return this.update(section, path, query, data, decode, readyCB);
                } else {
                    return this.callMethodEx(this.serviceClass, 'set', [section || this.section, path, query, data, decode], readyCB, false);
                }
            } catch (e) {
                logError(e, 'update');
            }
        },
        initStore: function () {
            const dfd = new Deferred();
            if (this.settingsStore) {
                dfd.resolve(this.settingsStore);
                return dfd;
            }
            this.serviceObject.__init.then(() => {
                this.read(this.section, '.', null, this.onSettingsReceived.bind(this)).then(() => {
                    dfd.resolve(this.settingsStore);
                });
            });
            return dfd;
        },
        init: function () {
            const dfd = new Deferred();
            dfd.resolve();
            return dfd;
        }
    });
    return Module;
});