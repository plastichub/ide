/** @module xide/manager/ResourceManager **/
define("xide/manager/ResourceManager", [
    'dcl/dcl',
    "xide/manager/ServerActionBase",
    "xide/utils",
    'xide/mixins/VariableMixin'
], function (dcl,ServerActionBase, utils, VariableMixin) {
    /**
     *
     * Resource manager which provides:
     *
     * - Resolving variables in strings
     * - Loading & unloading of resources: CSS,JS, Blox, API-Docs and plugins
     *
     * @class xide.manager.ResourceManager
     */
    return dcl([ServerActionBase,VariableMixin.dcl], {
        declaredClass:"xide.manager.ResourceManager",
        serviceClass: "XApp_Resource_Service",
        resourceData: null,
        resourceVariables: null,
        getResourceVariables:function(){
            return this.resourceVariables;
        },
        setVariable: function (variableName,value) {
            return this.resourceVariables[variableName]=value;
        },
        getVariable: function (variableName) {
            return this.resourceVariables[variableName];
        },
        init: function () {
            if (!this.resourceVariables) {
                this.resourceVariables = {};
            }
        },
        replaceVariables: function (string, variables) {
            
            return utils.multipleReplace('' + string, variables || this.resourceVariables);
        }
    });
});