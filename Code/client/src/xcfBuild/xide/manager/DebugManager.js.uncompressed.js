define("xide/manager/DebugManager", [
    'dcl/dcl',
    'dojo/dom-class',
    "xide/manager/ServerActionBase",
    'xide/encoding/MD5',
    'xide/types',
    'xide/utils',
    'xide/factory',
    "dojo/cookie",
    "dojo/json",
    'xide/views/DebugSettingsDialog'
], function (dcl, domClass, ServerActionBase, MD5, types, utils, factory, cookie, json, DebugSettingsDialog) {
    return dcl(ServerActionBase, {
        declaredClass: "xide.manager.DebugManager",
        serviceClass: 'XCF_NodeJS_Debug_Service',
        cookiePrefix: 'dbgSettings',
        singleton: false,
        serverDefaults: null,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Public API
        //
        /////////////////////////////////////////////////////////////////////////////////////

        debugDriver: function (relativePath, userData, srcView) {

            /*
             //1.pick up meta data
             var meta = utils.toObject(driver['user']);

             //2. pick meta data path
             var path = utils.toString(driver['path']);

             //3. pick scope
             var scope= utils.toString(driver['scope']);

             //4. pick name
             var name= utils.toString(driver['name']);

             //5. pick 'inputs' from meta
             var cis = meta['inputs'];


             //6. pick script ci
             var scriptCI = utils.getInputCIByName(meta,types.DRIVER_PROPERTY.CF_DRIVER_CLASS);

             //7. pick script ci's value
             var scriptCIValue  = utils.toString(scriptCI['value']);

             //8. alternate way to pick a CI value
             var scriptCIValue2  = utils.getCIInputValueByName(meta,types.DRIVER_PROPERTY.CF_DRIVER_CLASS);

             var driverMgr = this.ctx.getDriverManager();

             //console.dir(driverMgr);

             //console.error('debug driver : ' + name  + ' with script : ' + scriptCIValue + ' in scope:' + scope);
             console.error('debug driver : ' + name  + ' with script : ' + scriptCIValue2 + ' in scope:' + scope);
             */

            const thiz = this;

            const delegate = {
                onOk: function (dlg) {

                    const view = dlg.view;
                    const settings = view.getValue();
                    const rcp_call_options = thiz.mapSettings(settings);

                    thiz.savePreferences(settings);

                    factory.showStandBy(true);

                    //var view = thiz.ctx.getApplication().mainView.layoutMain.containerNode;

                    thiz.checkServer(
                        rcp_call_options,
                        function (data) {

                            if (thiz.serverIsRunning(data)) {
                                // Server is already up!

                                thiz.runDebug(rcp_call_options, function (data) {
                                    // Debug is running!
                                    console.log(thiz.getDebuggerUrl(data));

                                    factory.publish(types.EVENTS.ON_DEBUGGER_READY, {
                                        url: thiz.getDebuggerUrl(data),
                                        context: userData,
                                        options: rcp_call_options
                                    }, thiz);
                                });
                            } else {
                                // Server is not present, run it
                                thiz.runServer(rcp_call_options, function (data) {

                                    // Wait for the server
                                    setTimeout(function () {
                                        thiz.runDebug(rcp_call_options, function (data) {
                                            // Debug is running!

                                            console.log(thiz.getDebuggerUrl(data));

                                            factory.publish(types.EVENTS.ON_DEBUGGER_READY, {
                                                url: thiz.getDebuggerUrl(data),
                                                context: userData,
                                                options: rcp_call_options
                                            }, thiz);

                                        });
                                    }, 2000);

                                });
                            }
                        }
                    );

                }
            };

            try {

                var dlg = new DebugSettingsDialog({
                    title: 'Debug Settings',
                    delegate: delegate,
                    settings: null

                });
                domClass.add(dlg.domNode, 'debugSettingsDialog');
                dlg.show();
                const settings = thiz.loadPreferences();
                //settings.user = userData;
                settings.script = relativePath;
                dlg.initWithSettings(settings);
                dlg.resize();
            } catch (e) {

            }

            setTimeout(function () {
                dlg.resize();
            }, 1000);


        },
        loadPreferences: function () {
            const _cookie = this.cookiePrefix + '_debug_settings';
            let settings = cookie(_cookie);

            settings = settings ? json.parse(settings) : {
                port: this.serverDefaults.XAPP_NODEJS_DEBUG_TCP_PORT,
                host: this.serverDefaults.XAPP_NODEJS_DEBUG_TCP_HOST,
                debug_port: this.serverDefaults.XAPP_NODEJS_DEBUG_PORT
            };
            return settings;
        },

        savePreferences: function (settings) {
            const _cookie = this.cookiePrefix + '_debug_settings';
            cookie(_cookie, json.stringify(settings));
        },

        getViewTarget: function () {
            const mainView = this.ctx.getApplication().mainView;
            return mainView.getNewAlternateTarget();
        },
        /***
         * getViewId generates a unique id upon a driver's scope and the drivers path.
         * @param item
         * @returns {string}
         */
        getViewId: function (item) {
            return 'driver' + MD5(utils.toString(item.scope) + utils.toString(item.path), 1);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX related functions, @TODO : move it to somewhere else
        //
        /////////////////////////////////////////////////////////////////////////////////////
        mapSettings: function (settings) {
            const options = {

                XAPP_NODEJS_DEBUG_TCP_PORT: parseInt(settings.port),
                XAPP_NODEJS_DEBUG_TCP_HOST: settings.host,
                XAPP_NODEJS_DEBUG_PORT: parseInt(settings.debug_port),
                XAPP_NODEJS_DRIVER: settings.script

            };
            return json.stringify(options);
        },
        serverIsRunning: function (response) {
            return response.serverIsReady;
        },
        getDebuggerUrl: function (response) {
            return response.serverUrl;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Data related
        //
        /////////////////////////////////////////////////////////////////////////////////////
        init: function () {

            this.inherited(arguments);
            const thiz = this;
            this.getDefaults(function (data) {
                thiz.serverDefaults = data;
            })
        },

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Server methods
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getDefaults: function (readyCB) {
            return this.callMethodEx(null, 'getDefaults', null, readyCB, true);
        },
        checkServer: function (settings, readyCB) {
            return this.callMethodEx(null, 'checkServer', [settings], readyCB, true);
        },

        runServer: function (settings, readyCB) {
            return this.callMethodEx(null, 'runDebugServer', [settings], readyCB, true);
        },

        runDebug: function (settings, readyCB) {
            return this.callMethodEx(null, 'run', [settings], readyCB, true);
        },
        /***
         * ls is enumerating all drivers in a given scope
         * @param scope{string}
         * @param readyCB{function}
         * @param errorCB{function}
         * @returns {*}
         */
        ls: function (scope, readyCB, errorCB) {

            const thiz = this;

            const _cb = function (data) {

                //keep a copy
                thiz.rawData = data;
                thiz.initStore(data);

                if (readyCB) {
                    readyCB(data);
                }

            };
            return this.callMethodEx(null, 'ls', [scope], _cb, true);
        }
    });
});
