define("xide/editor/Default", [
    'xdojo/declare',
    'xide/utils',
    'xdojo/has!xace?xace/views/Editor'
],function (declare,utils,Editor){

    const debug = false;
    /**
    * A default editor, using ACE!
     */
    const Implementation = {
        ctx:null,
        /**
         *
         * @param item
         * @param where
         * @param mixin
         * @returns {Editor}
         */
        open:function(item,where,mixin,select,owner){
            if(!Editor){
                debug && console.error('have no xace! abort opening default editor!');
            }

            if(!item){
                debug && console.error('invalid item!');
                return null;
            }

            if(!item.getPath().match(/^(.*\.(?!(zip|tar|gz|bzip2)$))?[^.]*$/i)) {
                return null;
            }

            const mime = item.mime;
            const mediaTypes = ["image/png","image/jpg"];

            if(mime){
                if(mediaTypes.includes(mime)||
                    mime.includes('video')||
                    mime.includes('audio') ||
                    mime.includes('image'))
                {
                    return null;
                }
            }

            const thiz=this;
            const ctx = thiz.ctx;
            const mainView = ctx ? ctx.mainView : null;
            const title = utils.toString(item.name) ||  (utils.pathinfo(item.path,'PATHINFO_FILENAME') + '.'+ utils.pathinfo(item.path,'PATHINFO_EXTENSION'));
            const docker = mainView ? mainView.getDocker() : null;
            const registerInWindowManager = owner && owner.registerEditors===true ? true : false;
            const container  = where || mainView.layoutCenter;
            let root = null;

            mixin = mixin || {}

            if(!where) {
                root = mixin.attachTo || docker.addTab(null, {
                    title: title,
                    target: container ? container._parent : null,
                    icon: 'fa-code'
                });
            }else{
                root = container;
            }

            const args = {

                item:item,
                style:'padding:0px;',
                iconClass:'fa-code',
                options:utils.mixin(mixin,{
                    filePath:item.path,
                    fileName:item.name
                }),
                ctx:ctx,
                /***
                 * Provide a text editor store delegate
                 */
                storeDelegate:{
                    getContent:function(onSuccess,_item){
                        const file = _item || item;
                        return thiz.ctx.getFileManager().getContent(file.mount,file.path,onSuccess);
                    },
                    saveContent:function(value,onSuccess,onError){
                        return thiz.ctx.getFileManager().setContent(item.mount,item.path,value,onSuccess);
                    }
                },
                title:title
            };

            utils.mixin(args,mixin);

            const editor = utils.addWidget(Editor,args,this,root,true,null,null,select);

            root.resize && root.resize() && editor.resize();

            select!==false && root.selectChild && root.selectChild(editor);

            docker.resize();

            registerInWindowManager && ctx.getWindowManager().registerView(editor,false);

            return editor;
        }
    };

    //package via declare
    const _class = declare('xide.editor.Default',null,Implementation);
    _class.Implementation = Implementation;
    return _class;

});