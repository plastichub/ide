//>>built
define("xide/debug",["xdojo/declare","dojo/has","xdojo/has!debug?xide/serverDebug"],function(a,b,c){a=a("xide.debug",null,{});window.onerror=function(d,a,b,c,e){console.error(d,e)};window.logError=function(a,b){console.error((b||"")+" :: "+a.message+" stack:\n",a)};return a});
//# sourceMappingURL=debug.js.map