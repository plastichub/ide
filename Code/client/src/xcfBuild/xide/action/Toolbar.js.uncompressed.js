/** @module xide/action/Toolbar **/
define("xide/action/Toolbar", [
    'dcl/dcl',
    "xdojo/declare",
    'xide/utils',
    'xide/types',
    'xide/widgets/ActionToolbar',
    'xide/widgets/_Widget'
], function (dcl,declare,utils,types,ActionToolbar,_Widget) {
    /**
     * An action oriented Toolbar
     * @class module:xide/action/Toolbar
     */
    const Implementation = {
        _toolbar:null,
        toolbarInitiallyHidden:false,
        runAction:function(action){
            if(action.command==types.ACTION.TOOLBAR){
                this.showToolbar(this._toolbar==null,null,null,this);
            }
            return this.inherited(arguments);
        },
        getToolbar:function(){
            return this._toolbar;
        },
        showToolbar:function(show,toolbarClass,where,emitter){

            !show && (show = this._toolbar==null);

            if(!this._toolbar){

                const toolbar = this.add(toolbarClass || ActionToolbar ,{
                        style:'height:auto;width:100%'
                    },where || this.header,true);

                toolbar.addActionEmitter(emitter || this);
                toolbar.setActionEmitter(emitter || this);

                this._toolbar = toolbar;
            }

            if(!show && this._toolbar){
                utils.destroy(this._toolbar,true,this);
            }
            this.resize();
        },
        startup:function(){
            const TOOLBAR = types.ACTION.TOOLBAR;
            const hasToolbar = _.contains(this.permissions,TOOLBAR);

            hasToolbar && this.showToolbar(hasToolbar,null,null,this);

            const self = this;
            const node = self.domNode.parentNode;

            this._on('onAddActions', function (evt) {
                //be careful, it could be here already
                !evt.store.getSync(TOOLBAR) && evt.actions.push(self.createAction('Toolbar', TOOLBAR, types.ACTION_ICON.TOOLBAR, ['ctrl b'], 'View', 'Show', 'item|view', null, null, null, null, null, evt.permissions, node, self));
            });
        }
    };

    //package via declare
    const _class = declare('xide/action/Toolbar',_Widget,Implementation);
    _class.Implementation = Implementation;

    _class.dcl = dcl([_Widget.dcl],Implementation);

    return _class;
});