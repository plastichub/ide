/** @module xide/action/Action **/
define("xide/action/Action", [
    'xdojo/declare',
    'dojo/_base/lang',
    'xide/model/Base',
    'xide/types',
    'xide/utils',
    'xide/mixins/EventedMixin'
], function (declare, lang, Base, types,utils,EventedMixin) {
    /***
     * Extend the core types for action visibility(main menu,...) options/enums:
     * 1. 'Main menu',
     * 2. 'Context menu'
     * 3. 'Action toolbar'
     * 4. 'Property view'
     */
    utils.mixin(types, {
        /**
         * ActionVisibility
         * @enum module:xide/types/ACTION_VISIBILITY
         * @memberOf module:xide/types
         */
        ACTION_VISIBILITY: {
            /**
             * Enable visibility in main menu, which does
             * render actions in a menu bar whereby 'sub' levels
             * are rendered as sub level menus.
             *
             * @default null, means not visible. Actually in xjs its 1/0/{}
             * @type {int|Object|xide/action/Action}
             * @constant
             */
            MAIN_MENU: 'MAIN_MENU',

            /**
             * Enable visivibilty in context menu.
             * 
             * Different to the main menu, all actions
             * are 'flatted'. The action's group field
             * will auto-create separators among these 
             * groups. 
             * 
             * @default null, means not visible. Actually in xjs its 1/0/{}
             * @type {int|Object|xide/action/Action}
             * @constant
             */
            CONTEXT_MENU: 'CONTEXT_MENU',

            QUICK_LAUNCH: 'QUICK_LAUNCH',

            /**
             * Enable visivibilty in primary action toolbar.
             * 
             * Same as in the "Context Menu", actions will
             * rendered out flat, just the label is being removed.
             * 
             * @default null, means not visible. Actually in xjs its 1/0/{}
             * @type {int|Object|xide/action/Action}
             * @constant
             */
            ACTION_TOOLBAR: 'ACTION_TOOLBAR',

            /**
             * Enable visibility in an item's property view (if such exists).
             *
             * Same as in the "Context Menu", actions will
             * rendered out flat, just the label is being removed.
             *
             * @default null, means not visible. Actually in xjs its 1/0/{}
             * @type {int|Object|xide/action/Action}
             * @constant
             */
            PROPERTY_VIEW: 'PROPERTY_VIEW',

            /**
             * Enable visibility the ribbon toolbar (if such exists).
             *
             * Same as in the "Context Menu", actions will
             * rendered out flat, just the label is being removed.
             *
             * @default null, means not visible. Actually in xjs its 1/0/{}
             * @type {int|Object|xide/action/Action}
             * @constant
             */
            RIBBON: 'RIBBON',

            /**
             * A mixin to be used whilst creating the widget
             * @type {object}
             */
            widgetArgs:null,

            /**
             * Util for the constructor yo create a visibilty. A visibility is a key/value store where the 
             * key is ACTION_VISIBILITY 
             * and its value is stored in this.ACTION_VISIBILITY_val ! Thus you'r accessing this store in a doc-friendly 
             * and enum like function 
             * @example 
             *  this.getVisibility(types.ACTION_VISIBILITY.MAIN_MENU)'     * 
             *
             * the returning value is of type {object}, or {integer(1|0)}
             * @type {function}
             * @returns {module:xide/types/ACTION_VISIBILITY}
             */
            factory: function () {
                const _in = arguments[1] || lang.clone(types.ACTION_VISIBILITY);
                const _args = arguments;

                // A modus when we have the arguments like (MAIN_MENU,something). set value in this.ENUM_val
                if(_.isString(_args[0][0])){
                    if(_args[0][2]===true) {
                        lang.mixin(_in[_args[0][0] + '_val'],_args[0][2]);
                    }else{
                        _in[_args[0][0] + '_val'] = _args[0][1];
                        return _in;
                    }
                    return _args[1];
                }
                return _in;
            }
        }
    });
    types.ACTION_VISIBILITY_ALL = 'ACTION_VISIBILITY_ALL';
    /**
     * Basic model to represent an 'action'. Its just a structure
     * object with some factory methods and built-in store to have 
     * versions of it self per 'ACTION_VISIBILITY' which may alter
     * rendering for such visibility.
     * 
     * Please read {@link module:xide/types}
     * 
     * @class module:xide/action/Action
     * @augments xide/model/Base
     */
    const Module = declare("xide/action/Action", [Base,EventedMixin], {

        disabled:false,
        /**
         * Turn on/off this action
         * @type {boolean}
         * @default true
         */
        enabled:true,
        /**
         * The object or bean we're up to. This is mostly the user's selection.
         * @type {Object|Object[]|Array}
         */
        object: null,
        /**
         * Show/hide this action in ui
         * @member show {boolean}
         */
        show:true,
        /**
         * A group for this action. This is being used in interface only.
         * @type {string|Object=}
         */
        group: '',
        /**
         * A comma seperated list of bean types. This specifies on which bean types
         * this action can be applied
         * @type {string|Object=}
         */
        types: '',
        /**
         * A identifier of a command within a "bean action context". This should be human readable.
         * Remember, this is being used for populating menu items in toolbars.
         * @example "Edit/Copy", "Views/Log" and so forth
         * @type {string|integer}
         */
        command: null,
        /**
         * Icon class. You can use font-awesome, dijit icon classes or Elusive icons
         * @type {string}
         * @default fa-play
         */
        icon: 'fa-play',
        /**
         * An event key when the action is performed. This will be published automatically when this action
         * is performed.
         * @type {string|null}
         * @default null
         */
        event:null,
        /**
         * The function to be invoked
         * @type {function|null}
         */
        handler:null,
        /**
         * The tab (visual)
         * @type {string|null}
         * @default null
         */
        tab:null,

        /**
         * A store to override per visibility an action attributes like label, icon, renderer, handler
         * or whatever this action needs. This acts as store per VISIBILITY "Zone" as descried in the enumerations. Its
         * one simple object or single integer store.
         *
         * This storage must be fast as its used in mouse-over, don't use any dojo/dstore or whatever fancy stuff; the
         * operations in the consumer side are already heavy enought (loadash 'group' and 'sort' come up to 5000 calls for
         * just 10 actions)
         *
         * @see {module:xide/types/ACTION_VISIBILITY}
         * @type {xide/types/ACTION_VISIBILITY}
         * @augments {xide/types/ACTION_VISIBILITY}
         * @default null
         * @property
         * @member
         * 
         * @example
         * {
         *      MAIN_MENU:"MAIN_MENU",
         *      MAIN_MENU_val:0
         *      //or MAIN_MENU_val:1
         *      ACTION_TOOLBAR:"ACTION_TOOLBAR",
         *      ACTION_TOOLBAR_val:{
         *          icon:"fa or el or dijit", //supports font-awesome, elusive or dojo/dijit
         *          label:"" // in some cases like an action bar you may override this per visibility to hide a label 
         *      }
         * }
         *
         */
        visibility_: null,
        /**
         * An action might contain a value. For instance the action might toggle
         * a checkbox...
         *
         * @type {object|*|null}
         */
        value:null,
        /**
         * Sets visibility options per visibility type.
         *
         * @param {mixed} arguments will blend a number of integers into a copy of
         * xide/types/ACTION_VISIBILITY. Be aware of the exact order!
         * @example
         *
         *
            //Example 1. : set the visibility per type
            setVisibility(1,1,0);// will result in:
            {
                    MAIN_MENU:1,
                    CONTEXT_MENU:1,
                    ACTION_TOOLBAR:0
            }

            //Example 2. : set the visibility per type. @TODO:specify merge filter bits
            setVisibility(types.ACTION_VISIBILITY.MAIN_MENU,{
                label:null  //don't show a label
            });

         */
        setVisibility: function () {
            if(arguments.length==2 && _.isString(arguments[0]) && arguments[0]==types.ACTION_VISIBILITY_ALL){
                const _obj = arguments[1];
                const _vis = types.ACTION_VISIBILITY;
                const thiz = this;

                //track vis key in all
                [_vis.MAIN_MENU,_vis.ACTION_TOOLBAR,_vis.CONTEXT_MENU,_vis.RIBBON].forEach(function(vis){
                    thiz.setVisibility(vis,utils.cloneKeys(_obj,false));
                });
                return this;
            }

            const _args = _.isArray(arguments[0]) ? arguments[0] : arguments;
            this.visibility_ = types.ACTION_VISIBILITY.factory(_args,this.visibility_);

            return this;
        },
        /**
         * Visibilty getter
         * @param key
         * @returns {module:xide/types/ACTION_VISIBILITY}
         */
        getVisibility:function(key){
            if(!this.visibility_){
                this.setVisibility(types.ACTION_VISIBILITY_ALL,{});
            }
            if(this.visibility_){
                if(this.visibility_[key + '_val']==null){
                    this.visibility_[key + '_val']={
                        vis:key
                    };
                }
                return this.visibility_[key + '_val'];
            }
            return {};
        },
        /**
         *
         * @param _visibility
         * @param who
         * @param newItem
         * @returns {boolean}
         */
        shouldDestroyWidget:function(_visibility,who,newItem){
            const visibility = this.getVisibility!= null ? this.getVisibility(_visibility) : null;
            let destroy = true;
            if(visibility && visibility.permanent){
                destroy = !(_.isFunction(visibility.permanent) ? visibility.permanent(this,who,newItem) : visibility.permanent);
            }
            return destroy;
        }

    });
    /**
     * Static factory
     * @param label {string}
     * @param icon
     * @param command
     * @param permanent
     * @param operation
     * @param btypes
     * @param group
     * @param visibility
     * @param register
     * @param handler
     * @param mixin
     * @static
     * @memberOf xide/action/Action
     *
     * @example for queuing a clip board action:
     *
     *  var _copyAction  = Action.create('Copy', 'fa-copy', 'Edit/Copy', true, types.OPERATION_INT.CLIPBOARD_COPY, types.ITEM_TYPE.FILE, 'clipboard', null, true, _clipboardManager);
     *  _copy.accelKey = 'CTRL+C';
     *
     * @returns {module:xide/action/Action}
     */
    Module.create=function(label,icon,command,permanent,operation,btypes,group,visibility,register,handler,mixin){
        const _action = new Module({
            permanent:permanent,
            command:command,
            icon:icon,
            label:label,
            owner:this,
            types:btypes,
            operation:operation,
            group:group,
            handler:handler,
            title:label
        });

        const VISIBILITY = types.ACTION_VISIBILITY;

        const VISIBILITIES = [
            VISIBILITY.ACTION_TOOLBAR,
            VISIBILITY.RIBBON,
            VISIBILITY.MAIN_MENU,
            VISIBILITY.CONTEXT_MENU
        ];

        utils.mixin(_action,mixin);
        return _action;
    };
    /**
     * Simple wrapper for action.create
     * @param label {string}
     * @param icon
     * @param command
     * @param group
     * @param handler
     * @param mixin
     * @returns {module:xide/action/Action}
     */
    Module.createDefault = function(label,icon,command,group,handler,mixin){
        return Module.create(label,icon,command,false,null,null,group||'nogroup',null,false,handler,mixin);
    };
    
    return Module;
});
