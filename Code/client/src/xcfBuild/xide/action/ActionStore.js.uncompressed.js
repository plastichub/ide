/** @module xide/action/ActionStore **/
define("xide/action/ActionStore", [
    "xdojo/declare",
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'dojo/Deferred',
    'xide/action/ActionModel'

], function (declare, TreeMemory,ObservableStore,Trackable, Deferred,ActionModel) {
    /**
     * @class module:xide/action/ActionStore
     */
    const Module = declare("xide.action.ActionStore", [TreeMemory,Trackable,ObservableStore], {
        idProperty:'command',
        Model:ActionModel,
        renderers:null,
        observedProperties:[
            "value",
            "icon",
            "disabled",
            "enabled"
        ],
        getAll:function(){
            return this.data;
        },
        addRenderer:function(renderer){
            !this.renderers && (this.renderers = []);
            !_.contains(this.renderers,renderer) && this.renderers.push(renderer);
        }
    });

    Module.createDefault = function(args){
    	const result = new Module(args);
    	return result;
    }

    return Module;
    
});
