define("xide/Managers", [
    'dojo/_base/declare',
    'xide/manager/Context',
    'xide/manager/Application',
    'xide/manager/ManagerBase',
    'xide/manager/SettingsManager',
    'xide/manager/PluginManager'
],function(declare){
    return declare("xide.Managers", null,{});
});