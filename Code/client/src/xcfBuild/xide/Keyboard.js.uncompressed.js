/** @module xide/Keyboard **/
define("xide/Keyboard", [
    'xdojo/declare',
    'dcl/dcl',
    'dojo/_base/lang',
    'xide/types',
    'xide/utils/ObjectUtils'    //possibly not loaded yet
], function (declare, dcl, lang, types, utils) {

    /**
     * First things first, mixin KEYBOARD_FLAGS into core types.
     */
    utils.mixin(types, {
        /**
         * KEYBOARD_EVENT describes all possible events a subscriber can listen to.
         *
         * @enum module:xide/types/KEYBOARD_EVENT
         * @memberOf module:xide/types
         */
        KEYBOARD_EVENT: {
            /**
             * Add a custom callback for a key-up event.
             *
             * @default null, not required.
             * @type {function}
             * @constant
             */
            UP: 'on_keyup',
            /**
             * Add a custom callback for a key-dow event.
             *
             * @default null, not required.
             * @type {function}
             * @constant
             */
            DOWN: 'on_keydown',
            /**
             * Add a custom callback for a release event. This is similar to keyup, but will fire once
             * when ALL of the keys of a combo have been released. If you're unsure, you probably want to
             * ignore this and use UP.
             *
             * @default null, not required.
             * @type {function}
             * @constant
             */
            RELEASE: 'on_release'
        }
    });

    /**
     * Define a public struct for a 'keyboard - mapping
     */
    utils.mixin(types, {

        /**
         * KEYBOARD_MAPPING
         *
         * Keys accepted in human readable format as 'shift s', see the full map:
         *
         _modifier_event_mapping =
         "cmd"   : "metaKey"
         "ctrl"  : "ctrlKey"
         "shift" : "shiftKey"
         "alt"   : "altKey"

         _keycode_alternate_names =
         "escape"        : "esc"
         "control"       : "ctrl"
         "command"       : "cmd"
         "break"         : "pause"
         "windows"       : "cmd"
         "option"        : "alt"
         "caps_lock"     : "caps"
         "apostrophe"    : "\'"
         "semicolon"     : ";"
         "tilde"         : "~"
         "accent"        : "`"
         "scroll_lock"   : "scroll"
         "num_lock"      : "num"

         _keycode_shifted_keys =
         "/"     : "?"
         "."     : ">"
         ","     : "<"
         "\'"    : "\""
         ";"     : ":"
         "["     : "{"
     "]"     : "}"
         "\\"    : "|"
         "`"     : "~"
         "="     : "+"
         "-"     : "_"
         "1"     : "!"
         "2"     : "@"
         "3"     : "#"
         "4"     : "$"
         "5"     : "%"
         "6"     : "^"
         "7"     : "&"
         "8"     : "*"
         "9"     : "("
         "0"     : ")"

         _keycode_dictionary =
         0   : "\\"          # Firefox reports this keyCode when shift is held
         8   : "backspace"
         9   : "tab"
         12  : "num"
         13  : "enter"
         16  : "shift"
         17  : "ctrl"
         18  : "alt"
         19  : "pause"
         20  : "caps"
         27  : "esc"
         32  : "space"
         33  : "pageup"
         34  : "pagedown"
         35  : "end"
         36  : "home"
         37  : "left"
         38  : "up"
         39  : "right"
         40  : "down"
         44  : "print"
         45  : "insert"
         46  : "delete"
         48  : "0"
         49  : "1"
         50  : "2"
         51  : "3"
         52  : "4"
         53  : "5"
         54  : "6"
         55  : "7"
         56  : "8"
         57  : "9"
         65  : "a"
         66  : "b"
         67  : "c"
         68  : "d"
         69  : "e"
         70  : "f"
         71  : "g"
         72  : "h"
         73  : "i"
         74  : "j"
         75  : "k"
         76  : "l"
         77  : "m"
         78  : "n"
         79  : "o"
         80  : "p"
         81  : "q"
         82  : "r"
         83  : "s"
         84  : "t"
         85  : "u"
         86  : "v"
         87  : "w"
         88  : "x"
         89  : "y"
         90  : "z"
         91  : "cmd"
         92  : "cmd"
         93  : "cmd"
         96  : "num_0"
         97  : "num_1"
         98  : "num_2"
         99  : "num_3"
         100 : "num_4"
         101 : "num_5"
         102 : "num_6"
         103 : "num_7"
         104 : "num_8"
         105 : "num_9"
         106 : "num_multiply"
         107 : "num_add"
         108 : "num_enter"
         109 : "num_subtract"
         110 : "num_decimal"
         111 : "num_divide"
         112 : "f1"
         113 : "f2"
         114 : "f3"
         115 : "f4"
         116 : "f5"
         117 : "f6"
         118 : "f7"
         119 : "f8"
         120 : "f9"
         121 : "f10"
         122 : "f11"
         123 : "f12"
         124 : "print"
         144 : "num"
         145 : "scroll"
         186 : ";"
         187 : "="
         188 : ","
         189 : "-"
         190 : "."
         191 : "/"
         192 : "`"
         219 : "["
         220 : "\\"
         221 : "]"
         222 : "\'"
         223 : "`"
         224 : "cmd"
         225 : "alt"
         # Opera weirdness
         57392   : "ctrl"
         63289   : "num"
         # Firefox weirdness
         59 : ";"
         61 : "-"
         173 : "="

         *
         * @class module:xide/types/KEYBOARD_MAPPING
         * @memberOf module:xide/types
         */

        /**
         * KEYBOARD_MAPPING is defines keyboard mapping struct:
         *
         * @memberOf module:xide/types
         * @class module:xide/types/KEYBOARD_EVENT
         *
         */
        KEYBOARD_MAPPING: {
            /**
             * @param keys {string|string[]} the key sequence (see below for the right codes ).
             * This option can be either an array of strings, or a single space separated string of key names that describe
             * the keys that make up the combo.
             */
            keys: null,
            /**
             * @param handler {function|xide/types/KEYBOARD_EVENT} the callback for the key sequence. This can be one
             * function an structure per keyboard event. Usually its enough to leave this empty. You can also pass this
             * in the params
             */
            handler: null,
            /**
             * @param scope {Object|null} the scope in which the handler(s) are excecuted.
             */
            scope: null,
            /**
             * @param target {HTMLElement|null} the element on the listerner is bound to. Null means global!
             */
            target: null,
            /**
             * @param type {string|null} the keypress combo type, can be:
             * simple_combo(keys, on_keydown_callback); // Registers a very basic combo;
             * counting_combo(keys, on_count_callback); // Registers a counting combo
             * sequence_combo(keys, callback); // Registers a sequence combo
             * register_combo(combo_dictionary); // Registers a combo from a dictionary
             */
            type: null,
            /**
             * @param mixin to override the 'keypress' libraries internal setup for a listener
             * @default {

                prevent_repeat: false,
                prevent_default: false,
                is_unordered: false,
                is_counting: false,
                is_exclusive: false,
                is_solitary: false,
                is_sequence: false
            */
            params: null,
            /**
             *
             * @param mouse {Object|null|true|false} filter to setup an addtional trigger constraint for keyboard
             * sequence. Example: mouse.before='mousedown' and keys ='ctrl' will fire the handler when the mouse is hold whilst
             * ctrl key is hold. default:null.
             *
             */
            mouse: {
                brefore: null,//true
                after: null//false
            },
            eventArgs: null
        }

    });

    /**
     * Global array of keypress listeners
     * @type {Object[]}
     * @private
     */
    const listeners = [];
    const byNode = {};

    /**
     * Util to extend a keyboard mapping with control functions per listener. Keyboard mappings can
     * have multiple key sequences and this will take care about stop(), listen() and destroy().
     * @param mapping
     * @param listeners
     */
    const addListenerControls = function (mapping, listeners) {
        mapping.stop = function () {
            return;
            //if(listeners && listeners.length) {
            //    _.invoke(listeners, 'stop_listening');
            //}
        };
        mapping.listen = function () {
            _.invoke(listeners, 'listen');
        };
        mapping.destroy = function () {
            mapping.stop();
            _.each(listeners, function (listener) {
                listener.destroy();
                listeners.remove(listener);
                delete byNode[listener.targetId];
            });
        };
        return mapping;
    };

    /**
     * Safe link to keypress prototype
     * @type {Listener|keypress.Listener}
     * @private
     */
    const keypressProto = window ? window['keypress'] ? window.keypress.Listener : null : null;
    if (!keypressProto) {
        console.error('you need keypress.min.js installed to use xide/Keyboard');
    }

    const Implementation = {
        /**
         * @member listener {Object[]} all keypress listener instances
         */
        _keyboardListeners: null,
        /**
         * The default setup for a listener, this is 'keypress' specific.
         *
         * @returns {{prevent_repeat: boolean, prevent_default: boolean, is_unordered: boolean, is_counting: boolean, is_exclusive: boolean, is_solitary: boolean, is_sequence: boolean}}
         */
        keyPressDefault: function () {
            return {
                prevent_repeat: false,
                prevent_default: true,
                is_unordered: false,
                is_counting: false,
                is_exclusive: false,
                is_solitary: false,
                is_sequence: true
            };
        },
        /**
         * Private listener creation method, accepts multiple key sequences for the same handler.
         *
         * @param keys {string|string[]} the key sequence (see below for the right codes ).
         * This option can be either an array of strings, or a single space separated string of key names that describe
         * the keys that make up the combo.
         *
         * @param params {Object|null} an additional parameter structure to override the default 'keypress' setup.
         * See this.keyPressDefault
         *
         * @param scope {Object|null} the scope in which the handler(s) are excecuted, defaults to 'this' as we are
         * a mixin.
         *
         *
         * @param type {string|null} the keypress combo type, can be:
         * simple_combo(keys, on_keydown_callback); // Registers a very basic combo;
         * counting_combo(keys, on_count_callback); // Registers a counting combo
         * sequence_combo(keys, callback); // Registers a sequence combo
         * register_combo(combo_dictionary); // Registers a combo from a dictionary
         *
         * @param handler {function|xide/types/KEYBOARD_EVENT} the callback for the key sequence. This can be one
         * function an structure per keyboard event. Usually its enough to leave this empty. You can also pass this
         * in the params

         * @param target {HTMLElement|null} the element on the listener is bound to. Null means global!
         *
         * @param eventArgs {array|null} Event arguments passed to the handler. Defaults to keyboard event.
         *
         * @public
         */
        addKeyboardListerner: function (keys, params, type, scope, handler, target, eventArgs) {
            // prepare keypress args
            const _defaults = lang.clone(this.keyPressDefault());
            //mixin override
            utils.mixin(_defaults, params);

            // defaults
            _defaults['this'] = _defaults['this'] || scope || this;

            // use simple_combo as default
            type = type || 'simple_combo';

            //normalize to array
            keys = !_.isArray(keys) ? [keys] : keys;

            const _listeners = [];
            const ignore = ['ctrl s', 'ctrl l', 'ctrl r', 'ctrl w', 'ctrl f4', 'shift f4', 'alt tab', 'ctrl tab'];

            _.each(keys, function (key_seq) {
                const targetId = target && target.id ? target.id : 'global';
                const wasCached = target ? !!byNode[targetId] : false;
                let registered = false;

                let listener = byNode[targetId];
                if(listener && listener["_seq"+key_seq]){
                    registered = true;
                }

                if(!registered) {
                    if (!listener) {
                        listener = new keypressProto(target, _defaults);
                        listener.targetId = targetId;
                    }

                    listener["_seq" + key_seq] = true;
                    listener[type](key_seq, function (e) {
                        if (e._did) {
                            return;
                        }
                        e._did = true;
                        const className = e.target.className.toLowerCase();
                        //skip input fields
                        if (e.target.tagName!=='BUTTON' && !className.includes('input') || className.includes('ace_text-input')) {
                            if (handler && handler.apply) {
                                handler.apply(_defaults['this'], eventArgs || [e]);
                                if (ignore.includes(key_seq)) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                }
                            }
                        }
                    });
                    if (byNode[targetId]) {

                    } else {
                        byNode[targetId] = listener;
                    }

                    if (!wasCached) {
                        !this._keyboardListeners && (this._keyboardListeners = []);
                        //store in local
                        this._keyboardListeners.push(listener);
                        //store in global
                        _listeners.push(listener);
                    }
                }
            }, this);

            return _listeners;
        },
        /**
         * Public interface to register a keyboard mapping
         * @param mapping {xide/types/KEYBOARD_MAPPING}
         * @returns {xide/types/KEYBOARD_MAPPING}
         */
        registerKeyboardMapping: function (mapping) {
            const listeners = this.addKeyboardListerner(mapping.keys, mapping.params, null, mapping.scope, mapping.handler, mapping.target, mapping.eventArgs);
            mapping.listeners = listeners;
            return addListenerControls(mapping, listeners);
        },
        destroy:function(){
            this.inherited && this.inherited(arguments);
            const targetId = this.id;
            const listener = byNode[targetId];
            if(listener){
                listener.destroy();
                delete byNode[targetId];
            }
        }
    };

    /**
     * Generic keyboard handler, using the external 'keypress' Javascript library
     * which handles keyboard events a bit more elegant and robust, it does allow
     * registration of keyboard - sequences as 'shift s':
     * @example
     *
     * listener.simple_combo("shift s", function() {
     *  console.log("You pressed shift and s");
     * });
     *
     * @link http://dmauro.github.io/Keypress/
     * @link https://github.com/dmauro/Keypress/blob/master/keypress.coffee#L728-864
     */
    const _Keyboard = declare("xide/Keyboard", null, Implementation);
    /**
     * Static mapping factory
     * @param keys
     * @param params
     * @param type
     * @param scope
     * @param handler
     * @param target
     * @param eventArgs
     * @memberOf module:xide/Keyboard
     * @returns {xide/types/KEYBOARD_MAPPING}
     */
    _Keyboard.createMapping = function (keys, params, type, scope, handler, target, eventArgs) {
        const mapping = utils.clone(types.KEYBOARD_MAPPING);//@TODO: bad copy, uses a ctr followed by a lang.mixin
        function getHandler(__handler) {
            return _.isString(__handler) ? lang.hitch(scope, __handler) : __handler;
        }

        mapping.keys = keys;
        mapping.params = params || {};
        mapping.type = type;
        mapping.scope = scope;
        mapping.handler = getHandler(handler);
        mapping.target = target;
        mapping.eventArgs = eventArgs;
        mapping.setHandler = function (event, handler) {
            mapping.params[event] = getHandler(handler);
            return mapping;
        };
        return mapping;

    };

    _Keyboard.defaultMapping = function (keys, handler, params, node, who, args) {
        return _Keyboard.createMapping(keys, params, null, who, handler, node, args);
    };

    _Keyboard.dcl = dcl(null, Implementation);
    _Keyboard.byNode = byNode;
    _Keyboard.listeners = listeners;
    return _Keyboard;
});