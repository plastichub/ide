define("xide/main_test", [
    'dojo/has',
    'require',
    'xide/Features'
], function (has, require, Features) {
    //--collect electron
    has.add("electronx", function () {
        return window['getElectronWindow'] != null;
    }, true, false);

    //--root selector node
    const ROOT_DIV = '#root';

    //-- back compat config, inserted in header as global
    const _xcfConfig = xcfConfig;

    let main = null;

    //http://localhost/projects/x4mm/Code/xapp/xcf/?debug=true&run=run-release-debug&protocols=false&drivers=false&plugins=false&xblox=false&files=false&dijit=debug&xdocker=debug&xfile=debug&davinci=debug&dgrid=debug&xgrid=debug&xace=debug&xaction=debug&xfile=false&xideve=debug&davinci=debug&dijit=debug&app=xide&xideve=false&devices=false
    //console.error('is windows2 ' + has('windows'));

    //--make flags as has & collect for ux-actions
    const APP_FEATURES = [
        'drivers',  //needed to do anything with drivers or devices
        'devices',  //hide device view
        'protocols',//enabled protocol actions and ux
        'xblox',    //toggle explicit xblox
        'plugins',  //mime editors
        'xideve',   //visual editor
        'drivers-ui',//driver interface
        'force-electron',//emulated box release
        'debug', // simulate release (for checking left to kill d messages)
        'runDrivers',
        'electron'
    ];
    _.each(APP_FEATURES, function (feature) {
        has.add(feature, function () {
            return !location.href.includes(feature + '=false');
        },true,true);
    });
    /**
     * @TODO forward
     * @param e
     * @param text
     */
    function mkError(e, text) {
        typeof logError !== 'undefined' && logError(e, text);
    }

    /**
     * get custom app class via url arg, needed for prototyping the whole IDE as own dog-food
     * @param utils
     * @returns {*}
     */
    function getCustomApp(utils) {
        const urlParts = utils.urlArgs(location.href);
        let app = urlParts.start ? urlParts.start.value : null;
        if (app) {
            app = utils.replaceAll('.', '/', app);
            app && has.add('x-' + app, function () {
                return true;
            });
            try {
                const _appClass = utils.getObject(require.toUrl(app));
                _appClass.then && _appClass.then(function (_appClass) {
                    main(_appClass);
                }, function (e) {
                    mkError(e, 'custom app failed ' + e.message);
                    ('err', e);
                });
                return _appClass;
            } catch (e) {
                mkError(e, 'custom app failed ' + e.message);
            }
        }
        return app;
    }
    function load() {
        return require([
            'xdojo/declare',
            //'dijit/dijitb',
            'xcf/types',
            'wcDocker/wcDocker',
            'xaction/xaction',
            'xblox/xblox',
            'xace/xace',
            'xwire/xwire',
            'xfile/xfile',
            'dgrid/dgrid',
            'xdocker/xdocker',
            'xgrid/xgrid',
            'xlog/xlog',
            'xnode/xnode',
            'xlang/i18',
            'xide/utils/StringUtils',
            'xide/types/Types',
            'xide/Lang'
        ], function (declare, cvTypes, _wcDocker, xaction, _xblox, _xace,
                     _xwire, _xfile, dgrid, _xdocker, _xgrid,
                     _xlog, _xnode, i18, StringUtils, Types, Lang) {

            function loadMain() {
                require([
                    'xide/debug',
                    'xide/types/Types',
                    'xcf/types/Types',
                    'xide/Widgets',
                    'xide/Managers',
                    'xide/Views',
                    'xcf/XCFCommons',
                    'xide/GUIALL',
                    'xide/manager/TestContext',
                    'dojo/_base/lang',
                    'xide/utils',
                    'dojo/domReady!'
                ], function (debug, Types, XCTypes, Widgets, Managers, Views, XCFCommons,GUIALL, XCFContext, lang, utils) {
                    try {
                        main = function (appClass) {
                            const params = utils.urlArgs(location.href);
                            const appArgs = {};
                            if (has('electronx')) {
                                $('body').addClass("electron");//add theme
                                //transfer CLI args to object
                                const _require = window['eRequire'];
                                const remote = _require('electron').remote;
                                const Menu = remote.Menu;
                                const MenuItem = remote.MenuItem;

                                const cut = new MenuItem({
                                    label: "Cut",
                                    click: function () {
                                        document.execCommand("cut");
                                    }
                                });

                                const copy = new MenuItem({
                                    label: "Copy",
                                    click: function () {
                                        document.execCommand("copy");
                                    }
                                });

                                const paste = new MenuItem({
                                    label: "Paste",
                                    click: function () {
                                        document.execCommand("paste");
                                    }
                                });

                                const textMenu = new Menu();
                                textMenu.append(cut);
                                textMenu.append(copy);
                                textMenu.append(paste);

                                document.addEventListener('contextmenu', function (e) {

                                    switch (e.target.nodeName) {
                                        case 'TEXTAREA':
                                        case 'INPUT':
                                            e.preventDefault();
                                            textMenu.popup(remote.getCurrentWindow());
                                            break;
                                    }

                                }, false);

                                const BrowserWindow = remote.BrowserWindow;
                                //var BrowserWindow = _require('remote').require('browser-window');
                                const wind = BrowserWindow.getFocusedWindow() || BrowserWindow.mainWindow;
                                const args = BrowserWindow.args;
                                if (wind) {
                                    const webContents = wind.webContents;
                                    webContents.on('will-navigate', function (e) {
                                        try {
                                            e.preventDefault();
                                        } catch (e) {
                                            console.error('');
                                        }
                                    })
                                }

                                console.info('start with args', args);

                                //windows build, has no --file=arg
                                if ((args.length == 2 || args.length == 3) && args[0].includes('.exe') && !params.userDirectory) {
                                    args[1] = '--file=' + args[1];
                                }

                                _.each(args, function (arg) {
                                    if (arg.includes('--')) {
                                        const parts = arg.split('=');
                                        if (parts.length == 2) {
                                            appArgs[parts[0].replace('--', '')] = parts[1];
                                        }
                                    }
                                });


                                const argv = remote.require('yargs-parser')(args);
                                const array = utils.toArray(argv).filter(function (what) {
                                    return typeof what !== 'function'
                                });

                                const flags = [];
                                _.each(array, function (item) {
                                    if (item.value === 'true' || item.value === 'false') {
                                        flags.push({name: item.name, value: item.value === 'true'})
                                    }
                                });

                                _.each(flags, function (flag) {
                                    has.cache[flag.name] = flag.value;
                                });

                                if (params.userDirectory) {
                                    appArgs['userDirectory'] = params.userDirectory.value;
                                }

                            } else {
                                if (params.userDirectory) {
                                    appArgs['userDirectory'] = params.userDirectory.value;
                                }

                            }

                            _xcfConfig['APP_FEATURES'] = APP_FEATURES;

                            //context
                            const xasContext = new XCFContext(_xcfConfig, appArgs);

                            //-- collect for prototyping, super context only
                            !window.sctx && (window.sctx = xasContext);

                            xasContext.prepare();
                            xasContext.getKeyTarget = function () {
                                return $(ROOT_DIV)[0];
                            };

                            //-- construct managers
                            xasContext.constructManagers(appClass);
                            try {
                                xasContext.initManagers();
                            } catch (e) {
                                mkError(e, 'init managers');
                            }
                            return xasContext.getApplication().start(true, null, appArgs);
                        };


                        $('body').addClass("claro");//back compat for old packages

                        // --custom app
                        const app = getCustomApp(utils);//urlParts.start ? urlParts.start.value : null;
                        if (!app) {
                            //--default xcf.manager.Application
                            return main(null);
                        }
                    } catch (e) {
                        mkError(e, 'error in main');
                    }
                });
            }

            //@TODO: needs to ready as require plugin
            Lang.loadLanguage().then(loadMain);
        });
    }

    setTimeout(load,  true  ? 100 : 0);

    const Module = {};
    Module.getCustomApp = getCustomApp;
    return Module;
});

