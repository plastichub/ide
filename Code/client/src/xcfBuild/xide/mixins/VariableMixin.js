//>>built
define("xide/mixins/VariableMixin",["dcl/dcl","xdojo/declare","xide/utils"],function(e,a,f){var d={resolve:function(a,b,c){b=b||this.resourceVariables||this.ctx.getResourceManager().getResourceVariables()||null;c=c||this.variableDelimiters||null;return f.replace(a,null,b,c)}};a=a("xide/mixins/VariableMixin",null,d);a.dcl=e(null,d);return a});
//# sourceMappingURL=VariableMixin.js.map