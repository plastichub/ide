define("xide/mixins/PersistenceMixin", [
    "dcl/dcl",
    "xdojo/declare",
    "xide/utils",
    "dojo/cookie"
], function (dcl, declare, utils, cookie) {

    /**
     * Provides tools to deal with 'persistence' (open files, editors, ..etc to be restored). It also acts as interface.
     * @class module:xide/mixins/PersistenceMixin
     *
     **/
    const Implementation = {
        /***
         * soft override for getPreferencePersistence to allow using
         * another storage
         */
        persistenceFunction: null,
        /**
         * delete preferences in store
         */
        clearPreferences: function () {
            this.savePreferences(null, null, true);
        },
        /**
         * must be implemented
         */
        getDefaultPreferences: function () {
            return {};
        },
        /**
         * deserialize data with provider
         */
        deserializePreference: function (val) {
            return utils.getJson(val);
        },
        /**
         * serialize data with provider
         */
        serializePreference: function (val, safe) {
            try {
                return safe === true ? utils.stringify(val) : JSON.stringify(val);
            } catch (e) {
                logError(e, 'error serializing preference object');
            }
            return false;
        },
        /**
         * get a unique id for the store.
         *
         * @param prefix {string}
         **/
        toPreferenceId: function (prefix) {
            return (prefix || this.cookiePrefix || '') + '_ace';
        },
        /**
         * Return a function for persistence.apply(id,val)
         *
         * @param id {string} a unique id for the store.
         * @param val {string|null} the data. null when used for loading
         * @param provider {function|null} a default
         **/
        getPreferencePersistence: function (id, val, provider) {
            return this.persistenceFunction || provider || cookie;
        },
        /**
         * Load prefs by id (defaults to ::toPreferenceId)
         *
         * @param defaults {object|null} default data.
         * @param id {string|null} a unique id for the store.
         **/
        loadPreferences: function (defaults, id) {
            const _id = id || this.toPreferenceId();
            const persistence = this.getPreferencePersistence();
            let settings = null;
            try {
                settings = this.deserializePreference(persistence.apply(this, [_id]));
            } catch (e) {
                logError(e, 'error loading preferences, loading defaults');
                return this.getDefaultPreferences() || defaults;
            }
            return settings || defaults;
        },
        /**
         * Save prefs per id (defaults to ::toPreferenceId)
         *
         * @param prefs {object|} the data.
         * @param object {object|null} provide another context for the persistence function
         * @param clear {Boolean} dont use defaults = means it clears the store
         **/
        savePreferences: function (_prefs, object, clear, id) {
            const _id = id || this.toPreferenceId();
            const settings = _prefs || clear !== true ? _prefs : this.getDefaultPreferences();
            const persistence = this.getPreferencePersistence();

            return persistence.apply(object || this, [_id, this.serializePreference(settings),{
                expires:100
            }]);
        }
    };

    const Module = declare('xide.mixins.PreferenceMixin', null, Implementation);
    Module.dcl = dcl(null, Implementation);

    return Module;
});
