/** @mixin xide/mixin/_LogMixin **/
define("xide/mixins/_LogMixin", [
    'xdojo/declare'
], function (declare) {
    return declare("xide/utils/_LogMixin", null, {
        _logConfig: null,
        logger: null,
        initLogger: function (_logConfigig) {
            this._logConfig = _logConfigig;
        },
        log: function (msg, msg_context, level, data) {
            if (!msg_context) msg_context = this._debugContext()["main"];
            if (!msg_context || this.showDebugMsg(msg_context)) {
                if (this.logger) {
                    level = level || 'info';
                    this.logger.log(level, msg, data);
                }
            }
        },
        logEx: function (msg, level, type, data) {
            if (this.logger) {

                level = level || 'info';
                data = data || {};
                data.type = type || 'Unknown';
                data.time = new Date().getTime();
                this.logger.log(level, msg, data);
            }
        },

        showDebugMsg: function (msg_context) {
            if (this._logConfig != null) {
                if (this._logConfig["all"]) {
                    return true;
                }
                else {
                    if (this._logConfig[msg_context]) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

            } else {
                this._logConfig = {
                    "all": true
                };
                return true;
            }
        }
    });
});