/** @module xide/mixins/_LayoutActions**/
define("xide/mixins/_LayoutActions", [
    "dojo/_base/declare",
    'xide/action/Action',
    'xide/types'
], function (declare, Action, types) {
    /**
     * Generic layout switcher
     * @class module:xide/mixins/_LayoutActions
     */
    return declare("xide/mixins/_LayoutActions", null, {

        _getLayoutMainView: function () {
            return null;
        },
        _changeLayout: function (type) {
            /*
             var mainView = this._getLayoutMainView() || this.getMainView();
             var left = mainView.layoutLeft;
             var right = mainView.layoutRight;
             var bottom = mainView.layoutBottom;
             var thiz = this;
             switch(type){

             case types.LAYOUT_RIGHT_CENTER_BOTTOM:{

             //close left
             if (left && left._splitterWidget) {
             left._splitterWidget.set('state', 'closed');
             }

             //open right
             if (right &&right._splitterWidget) {
             right._splitterWidget.set('state', 'full');
             }

             //open bottom
             if (bottom && bottom._splitterWidget) {
             bottom._splitterWidget.set('state', 'full');
             }

             //switch to horizontal
             this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);


             break;
             }
             case types.LAYOUT_CENTER_BOTTOM:{

             //switch to horizontal
             this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);

             //close left
             if (left && left._splitterWidget) {
             left._splitterWidget.set('state', 'closed');
             }

             //close right
             if (right && right._splitterWidget) {
             right._splitterWidget.set('state', 'closed');
             }

             //open bottom
             if (bottom && bottom._splitterWidget) {
             bottom._splitterWidget.set('state', 'full');
             }
             break;
             }
             case types.LAYOUT_CENTER_RIGHT:{

             //switch to horizontal
             this.doSplit(types.VIEW_SPLIT_MODE.DESIGN);

             //close left
             if (left && left._splitterWidget) {
             left._splitterWidget.set('state', 'closed');
             }

             //open right
             if (right && right._splitterWidget) {
             right._splitterWidget.set('state', 'full');
             }

             //open bottom
             if (bottom && bottom._splitterWidget) {
             bottom._splitterWidget.set('state', 'closed');
             }

             break;
             }
             case types.LAYOUT_LEFT_CENTER_RIGHT:{

             //switch to horizontal
             this.doSplit(types.VIEW_SPLIT_MODE.DESIGN);

             //open left
             if (left && left._splitterWidget) {
             left._splitterWidget.set('state', 'full');
             }

             //open right
             if (right && right._splitterWidget) {
             right._splitterWidget.set('state', 'full');
             }

             //open bottom
             if (bottom && bottom._splitterWidget) {
             bottom._splitterWidget.set('state', 'closed');
             }

             break;
             }
             case types.LAYOUT_LEFT_CENTER_RIGHT_BOTTOM:{

             //switch to horizontal
             this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);

             //open left
             if (left && left._splitterWidget) {
             left._splitterWidget.set('state', 'full');
             }

             //open right
             if (right && right._splitterWidget) {
             right._splitterWidget.set('state', 'full');
             }

             //open bottom
             if (bottom && bottom._splitterWidget) {
             bottom._splitterWidget.set('state', 'full');
             }


             break;
             }
             }
             this.publish(types.EVENTS.RESIZE);
             setTimeout(function(){
             thiz.publish(types.EVENTS.RESIZE);
             },1000);

             */

        },
        /**
         *
         */
        getLayoutActions: function () {


            if (!this._getLayoutMainView()) {
                return null;
            }

            if (this._layoutActions) {
                return this._layoutActions;
            }
            const thiz = this;
            let idx = 0;

            function _createLayoutAction(label, command, icon, handler) {
                const _default = Action.create(label, icon, 'View/' + command + '_' + idx, false, null, types.ITEM_TYPE.WIDGET, 'viewActions', null, false, function () {
                    thiz._changeLayout(command);
                });

                _default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                    label: '',
                    widgetArgs: {
                        style: 'text-align:right;float:right;font-size:120%;'
                    },
                    permanent: function () {
                        return thiz.destroyed == false;
                    }
                });
                idx++;
                return _default;
            }


            const LAYOUT_RIGHT_CENTER_BOTTOM = _createLayoutAction('Center Bottom Right',
                types.LAYOUT_RIGHT_CENTER_BOTTOM, 'layoutIcon-rightCenterBottom');
            const LAYOUT_CENTER_BOTTOM = _createLayoutAction('Center Bottom',
                types.LAYOUT_CENTER_BOTTOM, 'layoutIcon-centerBottom');
            const LAYOUT_CENTER_RIGHT = _createLayoutAction('Center Right',
                types.LAYOUT_CENTER_RIGHT, 'layoutIcon-centerRight');

            const LAYOUT_LEFT_CENTER_RIGHT = _createLayoutAction('Left Center Right',
                types.LAYOUT_LEFT_CENTER_RIGHT, 'layoutIcon-leftCenterRight');

            const LAYOUT_LEFT_CENTER_RIGHT_BOTTOM = _createLayoutAction('Left Center Right Bottom',
                types.LAYOUT_LEFT_CENTER_RIGHT_BOTTOM, 'layoutIcon-leftCenterRightBottom');

            const out = [
                LAYOUT_RIGHT_CENTER_BOTTOM,// trouble!
                LAYOUT_CENTER_BOTTOM,//ok
                LAYOUT_CENTER_RIGHT,//ok

                LAYOUT_LEFT_CENTER_RIGHT,
                LAYOUT_LEFT_CENTER_RIGHT_BOTTOM];

            this._layoutActions = out;


            return out;
        },
        doSplit: function (mode) {
        },
        onSplit: function () {
        },
        getSplitTarget: function () {
            return this.containerNode;
        },
        getSplitContent: function () {
            return this.editor.domNode;
        }
    });
});