define("xide/factory/Widgets", [
    "dojo/dom-construct",
    'xide/factory',
    'xide/types',
    'xide/utils',
    'dojo/Deferred',
    "dojo/promise/all",
    'dojo/when',
    'xide/widgets/WidgetBase',
    'xide/widgets/NativeWidget',
    'xlang/i18',
    'xide/widgets/EditBox',
    'xide/widgets/StructuralWidget',
    "xide/$",
    "xide/lodash"
], function (domConstruct, factory, types, utils, Deferred, all, when, WidgetBase, NativeWidget, i18, EditBox,StructuralWidget,$,_) {
    const _defaultWidgetModule = WidgetBase;
    /**
     *
     * @param checked
     * @param name
     * @param label
     * @param attachTo
     * @returns {string}
     */
    factory.radioButtonString = function (checked, name, label, attachTo) {
        const id = name + "_" + utils.createUUID();
        let element = "";
        element += '<div class="radio radio-info">';
        element += '<input attachTo="' + attachTo + '" name="' + name + '" id="' + id + '" type="radio" ' + (checked == true ? 'checked' : '') + '/>';
        element += '<label for="' + id + '">';
        element += label;
        element += '</label>';
        element += '</div>';
        return element;
    };
    /**
     * Renders a CIS inline
     * @param CIS
     * @param where
     * @param owner
     * @returns {*}
     */
    factory.renderCIS = function (CIS, where, owner) {
        const widgetsHead = factory.createWidgetsFromArray(CIS, owner, null, false);
        const result = [];
        const dfd = new Deferred();

        when(widgetsHead, function (widgets) {
            if (widgets) {
                widgets.forEach(widget => {
                    widget.delegate = owner;
                    where.appendChild(widget.domNode);
                    if (where && where.lazy === true) {
                        widget._startOnShow = true;
                    } else {
                        widget.startup();
                    }
                    widget._on('valueChanged', function (evt) {
                        owner.onValueChanged && owner.onValueChanged(evt);
                    });
                    owner._emit && owner._emit('widget', {
                        widget: widget,
                        ci: widget.userData
                    })
                    result.push(widget);
                    widget.userData.view = owner;
                    widget.onAttached && widget.onAttached(where);
                    owner && owner.add && owner.add(widget);
                });

                dfd.resolve(result);
            }
        });
        return dfd;
    }

    /**
     *
     * @param type
     * @param options
     * @param dstField
     * @param typeMap
     * @returns {*}
     */
    factory.resolveWidget = function (type, options, dstField, typeMap) {
        const _debug = false;
        const _debugSuccess = false;
        _debug && console.error('resolve type : ' + type);

        //check in local type map, then global map
        const _customType = typeMap ? typeMap[type] : types.resolveType(type);
        if (_customType) {
            type = _customType;
        }

        options = options || {
                undef: false,
                reload: false
            };

        const _defaultModule = options.defaultModule;

        dstField = dstField || {}
        const mid = _.isString(type) ? utils.replaceAll('.', '/', type) : null;
        const _re = require;
        let _errorHandle = null;
        const deferred = new Deferred();

        if (_.isObject(type)) {
            dstField.result = type;
            deferred.resolve(type);

            return deferred;
        }

        if (options.reload) {
            try {
                require.undef(mid);
            } catch (e) {
            }
        }
        try {
            const _object = _re(mid);
            if (_object) {
                if (_.isObject(_object)) {
                    _debugSuccess && console.error('have module ' + mid, _object);
                    dstField.result = _.isEmpty(_object) ? _defaultModule : _object;
                    deferred.resolve(_object);
                    return deferred;
                }
            }
        } catch (e) {
            _debug && console.error('quick touch failed', e);
        }

        function handleError(error) {
            _debug && console.log(error.src, error.id);
            _debug && console.error('require error ' + mid, error);
            _errorHandle.remove();
            if (options.undef && _re.undef(mid)) {
            } else {
                _defaultModule && define(mid, _defaultModule);
            }
            dstField.result = _defaultModule;
            deferred.resolve(_defaultModule || type);
            return deferred;
        }

        _errorHandle = require.on("error", handleError);
        _debug && console.error('----require ----- ' + mid);
        //try loader
        _re([
            mid
        ], function (module) {
            if (module === 'not-a-module') {
                _errorHandle.remove();
                dstField.result = _defaultModule;
                deferred.resolve(_defaultModule || type);
            } else {
                _debugSuccess && console.error('re success', module);
                _errorHandle.remove();
                dstField.result = module;
                deferred.resolve(module)
            }
        });
        return deferred;
    }
    /**
     *
     * @param label
     * @param icon
     * @param btnClass
     * @param mixin
     * @param where
     * @returns {button}
     */
    factory.createSimpleButton = function (label, icon, btnClass, mixin, where) {
        const btn = domConstruct.create('button', utils.mixin({
            type: 'button',
            innerHTML: icon ? ('<i style="" class="' + icon + '"></i>' + label) : '' + label,
            "class": "btn " + (btnClass || '')
        }, mixin || {}), where);

        return btn;
    };
    /**
     *
     * @param json
     * @param onChangeHandler
     * @param storeItem
     * @param ci
     * @returns {*}
     */
    factory.createDijit = function (json, onChangeHandler, storeItem, ci) {
        if (json.type == null) {
            console.warn("type is missing for : " + json.title);
            return null;
        }
        const _ctrArgs = {
            delegate: onChangeHandler,
            intermediateChanges: ci.intermediateChanges != null ? ci.intermediateChanges : true,
            vertical: ci.vertical != null ? ci.vertical : false,
            userData: ci
        };
        utils.mixin(_ctrArgs, ci.widget);
        let type = json.type;
        if (json.nativeType) {
            json.props.intermediateChanges = true;
            type = json.nativeType;
        }

        const cls = json.result;
        if (!cls) {
            throw new Error("cannot find your widget type :" + type);
        }

        let myDijit = null;
        //ci provides additional widget properties, mix them into the constructor arguments
        utils.mixin(json.props, ci.widget);
        utils.mixin(_ctrArgs, {
            title: json.title,
            vertical: json.vertical
        });

        try {
            myDijit = new cls(utils.mixin(_ctrArgs, json.props), json.node);
        } catch (e) {
            console.error('constructing dijit widget failed : ' + e.message + ' for ' + type, json.props);
            return null;
        }
        const resultWidget = myDijit;
        let changeWidget = myDijit;
        if (resultWidget.nativeWidget) {
            changeWidget = null;
        }
        return resultWidget;
    };
    factory.createWidgetData = function (data, value) {
        const res = {
            type: "",
            nativeType: null,
            props: {
                options: data.options || []
            },
            title: "NoTitle",
            node: null,
            autocomplete: true,
            vertical: data.vertical != null ? data.vertical : false
        };
        res.type = utils.getWidgetType(data.type) || data.type;

        if (res.type) {
            var _type = types.resolveWidgetMapping(data.type);
            if (_type) {
                res.type = _type;
            }
        }

        let name = utils.toString(data.name);
        const _title = utils.toString(data.title);

        const namespace = 'xide.widgets.';

        //special sanity check regarding DS-CIs
        if (utils.isValidString(_title) && _title.length > 0 && _title[0].length > 0) {
            name = _title;
        }

        // is enum
        if (data.type == types.ECIType.ENUMERATION) {
            if (!data.options) {
                const options = types.resolveEnumeration(data.enumType);
                if (options && options.length > 0) {
                    res.props.options = options;
                }
            }
            res.props.value = data.value;
            res.props.prevValue = data.value;
            res.type = "xide.form.Select";
            res.nativeType = "xide.form.Select";
            res.autocomplete = true;
        }

        if (data.widget) {
            if (data.widget['class']) {
                res.nativeType = data.widget['class'];
            }
        }
        if (data.type == types.ECIType.STRING) {
            res.props.value = data.value;
            res.props.prevValue = data.value;
            res.type = namespace + "EditBox";
            res.props.title = name;
        }
        if (data.type == types.ECIType.JSON_DATA) {
            res.props.valueStr = data.value;
            res.type = "xide.widgets.JSONEditorWidget";
            res.props.newEntryTemplate = {
                isRoot: true,
                id: "root",
                schema: "{}"
            };
            res.props.initialTemplate = [];
            res.props.aceEditorOptions = {
                region: "center",
                value: '',
                style: "margin: 0; padding: 0; overflow: auto;position:relative;height:100%;width:100%;",
                theme: "twilight",
                mode: "json",
                readOnly: false,
                tabSize: 8,
                softTabs: true,
                wordWrap: false,
                printMargin: 120,
                showPrintMargin: true,
                highlightActiveLine: true,
                fontSize: '12px',
                showGutter: true
            }
        }

        if (data.type == types.ECIType.BOOL) {
            res.props['checked'] = utils.toBoolean(data.value);
            res.props.value = data.value;
            res.props.prevValue = data.value;
            res.type = NativeWidget;//"xide.widgets.NativeWidget";
            res.props.type = "CheckBox";
        }

        if (data.type == types.ECIType.IMAGE) {
            res.props.value = data.value;
            res.type = namespace + "ImageWidget";
            if (name == "BACKGROUND") {
                name = "Background";
            }
        }

        if (data.type == types.ECIType.FILE) {
            res.props.value = data.value;
            res.type = namespace + "FileWidget";
        }

        if (data.type == types.ECIType.FLAGS) {
            res.props.value = data.value;
            res.props.data = data.data;
            res.type = namespace + "FlagsWidget";
            res.props.title = name;
        }

        if (data.type == types.ECIType.SCRIPT) {
            res.props.value = data.value;
            res.type = namespace + "ScriptWidget";
        }

        if (data.type == types.ECIType.RICHTEXT) {
            res.props.value = data.value;
            res.type = namespace + "RichTextWidget";
        }

        if (data.type == types.ECIType.ICON) {
            res.props.value = data.value;
            res.type = namespace + "IconPicker";
        }

        if (data.type == types.ECIType.REFERENCE) {
            res.props.disabled = !data.enabled;
            res.props.innerHTML = "Select";
        }

        if (data.type == types.ECIType.STRUCTURE) {
            res.type = namespace + "StructuralWidget";
        }

        if (data.type == types.ECIType.INTEGER) {
            res.props.value = data.value;
            res.props.prevValue = data.value;
            res.type = namespace + "EditBox";
            res.props.title = name;
        }

        if (res.type === null || res.type == '') {
            var _type = types.resolveWidgetMapping(data.type);
            if (_type) {
                res.type = _type;
            }
        }
        res.title = name;
        return res;
    };

    /**
     *
     * @param cis
     * @returns {*}
     */
    factory.checkForCustomTypes = function (cis) {
        const addedCIS = [];
        const removedCIs = [];
        for (var i = 0; i < cis.length; i++) {
            const ci = cis[i];
            const ciType = utils.toInt(ci.type);
            if (ciType > types.ECIType.END) {//type is higher than core types, try to resolve it
                const resolved = types.resolveType(ciType);
                if (resolved) {
                    utils.mixin(addedCIS, resolved);
                    removedCIs.push(ci);
                }
            }
        }
        if (addedCIS.length > 0) {
            cis = cis.concat(addedCIS);
        }
        if (removedCIs) {
            for (var i in removedCIs) {
                cis.remove(removedCIs[i]);
            }
        }
        return cis;
    };
    /**
     *
     * @param data
     * @param changeHandler
     * @param storeItem
     * @param showInvisible
     * @param typeMap
     * @returns {*}
     */
    factory.createWidgetsFromArray = function (data, changeHandler, storeItem, showInvisible, typeMap, sort) {
        if (data == null){
            return null;
        }
        const res = [];
        const dfd = new Deferred();
        data = factory.checkForCustomTypes(data);
        const _widgetMeta = [];
        const options = {
            defaultModule: _defaultWidgetModule
        };
        for (let i = 0; i < data.length; i++) {
            var inDef = data[i];
            if (!inDef) {
                continue;
            }
            if (showInvisible == false && utils.toBoolean(inDef.visible) == false) {
                continue;
            }
            const wData = factory.createWidgetData(inDef, inDef.value);
            if (!wData) {
                continue;
            }

            if (!wData.type) {
                continue;
            }

            wData.module = factory.resolveWidget(wData.type, options, wData, typeMap);
            wData.CI = inDef;
            _widgetMeta.push(wData);
        }
        const _promises = _widgetMeta.map((m) => 'module');
        function complete(widgetData) {
            for (let i = 0; i < widgetData.length; i++) {
                const wData = widgetData[i];
                const CI = wData.CI;
                const widget = factory.createDijit(wData, changeHandler, storeItem, CI);
                if (!widget) {
                    console.error('have no widget', wData);
                    continue;
                }

                widget["userData"] = CI;
                widget["owner"] = changeHandler;
                widget["storeItem"] = storeItem;
                if (sort) {
                    widget["order"] = i;
                } else {
                    inDef.order = inDef.order || i;
                }
                inDef['_widget'] = widget;

                //tell everybody
                factory.publish(types.EVENTS.ON_CREATED_WIDGET, {
                    ci: inDef,
                    widget: widget,
                    storeItem: storeItem,
                    owner: changeHandler
                });
                if (widget.nativeWidget) {
                    widget.nativeWidget["userData"] = inDef;
                    widget.nativeWidget["storeItem"] = storeItem;
                }
                res.push(widget);
            }

            if (sort) {
                res.sort(function (a, b) {
                    const orderA = utils.toInt(a.userData.order);
                    const orderB = utils.toInt(b.userData.order);
                    return orderA - orderB;
                });
            }
            dfd.resolve(res);
        }
        all(_promises).then(function (args) {
            complete(_widgetMeta);
        });
        return dfd;
    };
    /**
     * Creates a button
     * @param dstNode
     * @param iconClass
     * @param style
     * @param label {string};
     * @param onClickCB {function=null}
     * @param delegate {object=null}
     * @return {HTMElement}
     */
    factory.createButton = function (dstNode, iconClass, buttonClass, style, label, onClickCB, delegate) {
        const button = factory.createSimpleButton(label, iconClass, buttonClass, {}, dstNode);
        if (onClickCB) {
            $(button).on('click', function (e) {
                onClickCB.apply(delegate, [e]);
            });
        }
        return button;
    };
    /**
     *
     * @param dstNode
     * @param style
     * @param labelText
     * @param value
     * @param validator
     * @param delegate
     * @param invalidMessage
     * @param missingMessage
     * @param toolTipPosition
     */
    factory.createValidationTextBox = function (dstNode, style, labelText, value, validator, delegate, invalidMessage, missingMessage, toolTipPosition) {
        const parent = utils.getDoc().createElement('DIV');
        const editBox = new EditBox({
            title: labelText,
            value: value,
            delegate: delegate,
            userData: {
                value: value
            }
        }, parent);
        dstNode.appendChild(editBox.domNode);
        editBox.startup();
        return editBox;
    };

    /**
     *
     * @param dstNode
     * @param style
     * @param labelText
     * @param value
     * @param checked
     * @param id
     * @returns {*}
     */
    factory.createCheckBox = function (dstNode, labelText, value, checked, id, title) {
        let element = '';
        title = title || labelText;
        //checkbox-circle
        element += '<div class="checkbox checkbox-success " title="' + title + '">';
        element += '<input id="' + id + '" type="checkbox" ' + (checked == true ? 'checked' : '') + '>';
        element += '<label for="' + id + '">';
        element += i18.localize(labelText) + '</label>';
        element += '</div>';
        const result = $(element);

        const checkBox = result.find('INPUT');
        $(dstNode).append(result);
        return checkBox;
    };
    factory.createRadioButton = function (dstNode, style, labelText, value, onClickCB, delegate, checked, toolTipText, className) {

    };
    return factory;
});