/** @module xideve/Template **/
define("xide/Template", [
    "dojo/_base/declare",       //@TODO: move to DCL
    "xide/model/Component",     //make it a plugin since some data points to additional resources to be loaded first
    "xide/model/Bean",          //make this a bean so the user can edit this
    "xide/mixins/VariableMixin" //some settings may require resolved against user IDE variables
], function (declare, Component, Bean, VariableMixin) {
 const _REG_EX_REQUIRE =
     // a regular expression to identify a require
     /\brequire\s*\(\s*\[\s*([\s\S]*?)\s*\]\s*\)/;

 const // a regular expression to identify a module id
 _REG_EX_MID = /[\w.\/]+/g;

 /**
  * Simple entity to collect data for a 'Document' template. This must be serializable and incorporate the
  * xide-component (plugin) interface.

  * @class module:xideve/Template
  * @extends module:xide/model/Components
  * @implements module:xide/model/Bean
  **/
 const Template = declare('xide/Template', [Component, Bean, VariableMixin], {
     /////////////////////////////////////////////////////////////////////////////////////////////
     //
     //  Data & properties
     //
     /**
      * IDE Context
      * @type {xide/manager/Context}
      */
     ctx:null,
     /**
      *  A class sting (AMD/Require-JS compatible)
      * @type {string[]}
      */
     contextClass: null,
     /**
      * The require-js baseUrl, that gets filled by the component loader, client or server side.
      * @type {string[]}
      * @default null
      */
     baseUrl: null,
     /**
      * Minimum requires
      * @type {string[]}
      */
     requires: [],
     /**
      * When the document is being loaded (not parsed yet), then this array of require-js modules
      * will be loaded before onLoad gets fired. This can be used to add debugging or user/project
      * space modules
      * @default empty
      */
     postRequires: [],
     /**
      * If the document type has any theme tech, this is the default theme to be loaded
      * @type {string}
      */
     defaultTheme: null,
     /**
      * Set of require-js mappings when the document is being switched to mobile output
      * @type {object}
      */
     mobileMap: {},
     /**
      * Capabilities describes what the tech provides, that might be dynamic and is
      * essentially a flag/enum to object/properties map
      * @type {xideve/types/Capabilities}
      */
     capabilities: {},
     /**
      * An error handler
      * @TODO: error mixin?
      */
     errorHandler: null,

     /**
      * A debug handler
      * @TODO: login mixin?
      */
     debugHandler: null,
     /**
      * A delegate to resolve resources in IDE mode.
      * @type {xide/manager/ResourceManager}
      */
     resourceDelegate: null,
     /**
      * A delegate to resolve resources variables, run-time and IDE mode.
      * @type {xide/mixin/VariableMixin}
      */
     variableDelegate: null,
     /**
      * When user opens a simple HTML fragment file, inject the document with this config and make it
      * into a full application
      */
     requireMinimumConfig: {},

     /**
      * Shared objects from IDE to app. This is an array of strings to instances which goes into
      * the document's 'global'
      * @type {Object[]}
      */
     sharedObjects: [],
     /**
      * Server side mixins into the prototypes.
      * @type {Object[]}
      */
     mixins: [],
     /**
      * New file template
      * @type {string} A require-text path to a text resource, containing the template for a new file
      * @default empty
      */
     newFileTemplate: '',
     /**
      * Body styling
      */
     bodyStyle:'',
     /////////////////////////////////////////////////////////////////////////////////////////////
     //
     //  Bean protocol implementation
     //
     beanType: 'Document Template',
     /**
      * Stub
      * @returns {Array}
      */
     getDependencies:function(){
         return [];
     }
 });
 // @TODO: statics ?
 return Template;
});

