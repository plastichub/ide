/** @module xide/data/Memory **/
define("xide/data/Memory", [
    "dojo/_base/declare",
    'dstore/Memory',
    'xide/data/_Base'
], function (declare, Memory,_Base) {
    /**
     * Base memory class
     * @class module:xide/data/Memory
     * @extends module:xide/data/_Base
     * @extends module:dstore/Memory
     */
    return declare('xide.data.Memory',[Memory, _Base], {
        /**
         * Get/Set toggle to prevent notifications for mass store operations. Without there will be performance drops.
         * @param silent {boolean|null}
         */
        silent: function (silent) {
            if (silent === undefined) {
                return this._ignoreChangeEvents;
            }
            if (silent === true || silent === false && silent !== this._ignoreChangeEvents) {
                this._ignoreChangeEvents = silent;
            }
        },
        /**
         * XIDE specific override to ensure the _store property. This is because the store may not use dmodel in some
         * cases like running server-side but the _store property is expected to be there.
         * @param item {object}
         * @returns {*}
         */
        putSync:function(item){
            const self = this;
            item = this.inherited(arguments);
            item && !item._store && Object.defineProperty(item, '_store', {
                get: function () {
                    return self;
                }
            });
            return item;
        }
    });
});
