//>>built
define("xideve/tests/Basics","xdojo/declare xdocker/Docker2 xfile/tests/TestUtils dcl/dcl xide/utils xide/editor/Registry module".split(" "),function(b,k,f,l,m,g,n){function h(a,b,c){var d=g.getEditors(a);(a=_.find(d,function(a){return"Visual Editor"===a.name&&"dhtml"===a.extensions}).onEdit(a,b,null))&&a.then&&a.then(function(a){c&&c(a)})}b=b("m",null,[]);b.createVisualEditor=function(a,b,c,d){var e=f.createStore(a||"workspace_user");e.fetchRange().then(function(a){setTimeout(function(){h(e._getItem(b||
"./marantz.dhtml"),c,d)},500)})};return b});
//# sourceMappingURL=Basics.js.map