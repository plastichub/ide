require({cache:{
'url:davinci/ve/widgets/templates/BackgroundDialog.html':"<div class=\"backgroundDialog\" style=\"position:relative\" dojoAttachPoint=\"_fileDialog\">\n\t<style type=\"text/css\">\n\t\t.backgroundDialog {\n\t\t\twidth:31em;\n\t\t}\n\t\t.bgdTopDiv {\n\t\t\tmargin-top: 4px;\n\t\t\tpadding-left: .75em;\n\t\t}\n\t\t.bgdTopTable {\n\t\t\twidth:100%;\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\t.bgdTopLabel, .bgdTopField, .bgdTopPreview {\n\t\t\twidth:1px;\n\t\t\twhite-space:nowrap;\n\t\t}\n\t\t.bgdTopLabel {\n\t\t\ttext-align:right;\n\t\t\tpadding-right:6px;\n\t\t}\n\t\t.bgdTopTable .dijitSelect .dijitButtonContents {\n\t\t\twidth:8em;\n\t\t}\n\t\t.bgdTopTable .dijitTextBox {\n\t\t\twidth: 9.8em;\n\t\t}\n\t\t.bgdPreview {\n\t\t\tdisplay:inline-block;\n\t\t\twidth: 6em;\n\t\t\theight: 2.8em;\n\t\t\tmargin-left:12px;\n\t\t\tborder:1px solid gray;\n\t\t\tvertical-align: middle;\n\t\t}\n\t\t.bgdOptionsDiv {\n\t\t\tmargin:0.3em 0em 1em 1em;\n\t\t}\n\t\t.bgdOptionsDiv > table {\n\t\t\twidth:27em;\n\t\t}\n\t\t.bgdSection {\n\t\t\tbackground-color:#d8d8d8;\n\t\t}\n\t\tdiv.bgdSection, .bgdSection td, .bgdSection.bgdUrlSectionLabel {\n\t\t\tpadding:2px 5px;\n\t\t}\n\t\t.bgdBeforeStopsLabel {\n\t\t\theight:6px;\n\t\t}\n\t\t.bgdStopsLabel {\n\t\t\tmargin-top:1em;\n\t\t}\n\t\t.bgdAfterStopsLabel {\n\t\t\theight:0px;\n\t\t}\n\t\t.bgdBeforeOptionsLabel {\n\t\t\theight:16px;\n\t\t}\n\t\t.bgdOptionsLabel {\n\t\t\tmargin-top:1em;\n\t\t}\n\t\t.bgdAfterOptionsLabel {\n\t\t\theight:4px;\n\t\t}\n\t\t.bgdCol1 {\n\t\t\twidth:10px;\n\t\t}\n\t\t.bgdPlusMinusButtons {\n\t\t\twhite-space:nowrap;\n\t\t\twidth:51px;\n\t\t}\n\t\t.bgdOptionsDiv th {\n\t\t\ttext-align:center;\n\t\t\tfont-style:italic;\n\t\t}\n\t\t.bgdOptsLabel {\n\t\t\ttext-align:right;\n\t\t\tpadding-right: 6px;\n\t\t\twhite-space:nowrap;\n\t\t}\n\t\t.bgdColor.dijitTextBox {\n\t\t\twidth:9em;\n\t\t}\n\t\t.bgdOptionsDiv .bgdPosition.dijitTextBox {\n\t\t\twidth:5em;\n\t\t}\n\t\t.bgdOptionsDiv .dijitTextBox {\n\t\t\twidth:10.6em;\n\t\t}\n\t\t.bgdStopColorTD .dijitTextBox {\n\t\t\twidth:14em;\n\t\t}\n\t\t.bgdOptionsDiv .dijitSelect {\n\t\t\tmargin:0;\n\t\t}\n\t\t.bgdOptionsDiv .dijitSelect .dijitButtonContents {\n\t\t\twidth:9em;\n\t\t}\n\t\t.bgdUrlContainerOuter {\n\t\t\tmargin-top:1em;\n\t\t\tmargin-left:1em;\n\t\t}\n\t\t.bgdUrlContainerInner {\n\t\t\tmargin:0 1em;\n\t\t}\n\t\t.bgdUrlSectionLabel {\n\t\t\tmargin:1em 0 .25em;\n\t\t}\n\t\t.bgdFileTreeContainer {\n\t\t\tmargin: 0 0 0 3px;\n\t\t\tborder: 1px solid #26A;\n\t\t}\n\t\t.bgdFileTreeContainer .dijitTree {\n\t\t\theight:125px;\n\t\t}\t\t\n\t\t.backgroundDialog .fileNameRow  {\n\t\t\tmargin:8px 0 .4em;\n\t\t}\n\t\t.backgroundDialog .fileNameRow label {\n\t\t\tvertical-align:middle;\n\t\t\tmargin-right:4px;\n\t\t}\n\t\t.backgroundDialog .fileNameRow .dijitTextBox {\n\t\t\twidth:16em;\n\t\t}\n\t\t.bgdOptionsDiv .bgdOtherRow .dijitTextBox {\n\t\t\twidth:15em;\n\t\t}\n\t\t\n\t</style>\n\n\t<div class=\"dijitDialogPaneContentArea\">\n\t\t<div class='bgdTopDiv'>\n\t\t\t<table class='bgdTopTable'>\n\t\t\t\t<tr>\n\t\t\t\t\t<td class='bgdTopLabel'><label class='bgdTypeDivTypeLabel'>${veNLS.bgdBackgroundColor}</label></td>\n\t\t\t\t\t<td class='bgdTopField'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdColorCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td rowspan='2' class='bgdTopPreview' style='border-collapse:collapse'><span class='bgdPreview'></span></td>\n\t\t\t\t\t<td class='bgdTopExpando'>&nbsp;</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr>\n\t\t\t\t\t<td class='bgdTopLabel'><label class='bgdTypeDivTypeLabel'>${veNLS.bgdBackgroundImageType}</label></td>\n\t\t\t\t\t<td class='bgdTopField'>\n\t\t\t\t\t\t<select dojoType='dijit.form.Select' value='linear' dojoAttachPoint='bgdTypeSelect'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t<!-- FIXME: Add plain text type-in box if unrecognized syntax -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td class='bgdTopExpando'>&nbsp;</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t</div>\n\t\t\n\t\t<div class=\"bgdUrlContainerOuter bgdImageOptRow\">\n\t\t\t<div class=\"bgdSection bgdOptionsLabel\">${veNLS.bgdImageUrl}</div>\n\t\t\t<div class=\"bgdUrlContainerInner\">\n\t\t\t\t<div class='fileNameRow'>\n\t\t\t\t\t<td class=\"fileDialogLabelCell\">\n\t\t\t\t\t<label>${veNLS.bgdUrl}</label>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t<div dojoType='davinci.ui.widgets.FileFieldDialog' dojoAttachPoint=\"_filePicker\"></div>\n\t\t\t\t\t</td>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"bgdOptionsDiv\">\n\t\t\t<table>\n\t\t\t\t<tr class=\"bgdGradOptRow bgdBeforeStopsLabel\"></tr>\n\t\t\t\t<tr class=\"bgdGradOptRow bgdSection bgdStopsLabel\">\n\t\t\t\t\t<td colspan=\"5\">${veNLS.bgdColorStops}</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr class=\"bgdGradOptRow bgdAfterStopsLabel\"></tr>\n\t\t\t\t<tr class='bgdGradOptRow bgdStopRow'>\n\t\t\t\t\t<th></th><th></th><th>${veNLS.bgdColor}</th><th>${veNLS.bgdPosition}</th><th></th>\n\t\t\t\t</tr>\n\t\t\t\t<!--  gradient stop rows added dynamically -->\n\t\t\t\t<tr class=\"bgdBeforeOptionsLabel bgdOptionsLabelRow\"></tr>\n\t\t\t\t<tr class=\"bgdSection bgdOptionsLabel bgdOptionsLabelRow\">\n\t\t\t\t\t<td colspan=\"5\">${veNLS.bgdOptions}</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr class=\"bgdAfterOptionsLabel bgdOptionsLabelRow\"></tr>\n\t\t\t\t<tr class=\"bgdGradOptRow bgdLinearOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'><label>${veNLS.bgdAngle}</label></td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdLinearAngleCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr class=\"bgdGradOptRow bgdRadialOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'>${veNLS.bgdPosition2}</td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdRadialPosCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdShapeRow\" class=\"bgdGradOptRow bgdRadialOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'><label>${veNLS.bgdShape}</label></td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdRadialShapeCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdExtentRow\" class=\"bgdGradOptRow bgdRadialOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'><label>${veNLS.bgdExtent}</label></td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdRadialExtentCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdRepeatRow\" class=\"bgdImageOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'><label>${veNLS.bgdBackgroundRepeat}</label></td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdRepeatCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdPositionRow\" class=\"bgdImageOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'>${veNLS.bgdBackgroundPosition}</td>\n\t\t\t\t\t<td colspan='3'>\n\t<!-- FIXME: regExp, invalidMessage -->\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdPositionCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdSizeRow\" class=\"bgdImageOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'>${veNLS.bgdBackgroundSize}</td>\n\t\t\t\t\t<td colspan='3'>\n\t<!-- FIXME: regExp, invalidMessage -->\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdSizeCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdOriginRow\" class=\"bgdImageOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'><label>${veNLS.bgdBackgroundOrigin}</label></td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdOriginCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdClipRow\" class=\"bgdImageOptRow\">\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'><label>${veNLS.bgdBackgroundClip}</label></td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<select dojoType=\"dijit.form.ComboBox\" dojoAttachPoint='bgdClipCB'>\n\t\t\t\t\t\t\t<!--  values added dynamically -->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr dojoAttachPoint=\"bgdOtherTypeInRow\" class=\"bgdOtherRow\">\t\n\t\n\t\t\t\t\t<td class='bgdCol1'></td>\n\t\t\t\t\t<td class='bgdOptsLabel'><label>${veNLS.bgdBackgroundImageValue}</label></td>\n\t\t\t\t\t<td colspan='3'>\n\t\t\t\t\t\t<textarea dojoType=\"dijit.form.Textarea\" dojoAttachPoint='bgdOtherTA'></textarea>\n\t\t\t\t\t</td>\n\t\n\t\t\t\t</tr>\n\t\n\t\t\t</table>\n\t\t</div>\n\t</div>\n\n\t<!--div class=\"dijitDialogPaneActionBar\">\n\t\t<span dojoType='dijit.form.Button' dojoAttachPoint=\"_okButton\" dojoAttachEvent='onClick:okButton' label='${buttonOk}' class=\"maqPrimaryButton\" type=\"submit\"></span> \n\t\t<span dojoType='dijit.form.Button' dojoAttachEvent='onClick:cancelButton' label='${buttonCancel}' class=\"maqSecondaryButton\"/></span> \n\t</div-->\n</div>\n"}});
/** @module xgrid/Base **/
define("xideve/tests/TestStyleView", [
    
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xide/factory',    
    'xgrid/Grid',

    "xide/tests/TestUtils",
    'xide/_base/_Widget',
    "module",
    "dojo/_base/lang",
    "davinci/lang/ve",
    "dijit/layout/ContentPane",

    "xideve/widgets/WidgetLite",
    "xide/mixins/ReloadMixin",
    "davinci/ve/widgets/HTMLStringUtil",
    "xide/layout/_TabContainer",
    "dojo/dom-class",
    "dojo/dom-attr",

    "xide/form/Select",
    "xide/mixins/EventedMixin",
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'xide/widgets/TemplatedWidgetBase',
    'dojo/parser',

    "dijit/_WidgetBase",
    "davinci/ve/widgets/ColorStore",
    "davinci/ve/widgets/ColorPickerFlat",

    "davinci/lang/ve",
    "xide/widgets/ColorPickerWidget",
    "dojo/Deferred",

    "davinci/ve/widgets/MutableStore",
    "davinci/ve/widgets/BackgroundDialog",
    "davinci/ve/utils/URLRewrite",
    "davinci/model/Path",
    "dijit/lang/_common",
    "davinci/ve/utils/CssUtils",
    "xide/views/_PanelDialog",

    'xide/lodash',

    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "dojo/text!davinci/ve/widgets/templates/BackgroundDialog.html",
    "davinci/ve/utils/CssUtils",
    "davinci/model/Path",
    "dijit/form/Button",
    "davinci/ve/widgets/ColorPicker",
    "davinci/ve/widgets/ColorPickerFlat",
    "dijit/form/Textarea",
    "davinci/ui/widgets/FileFieldDialog"


], function (
    dcl,  declare, types, utils, factory, Grid, 
    
    TestUtils, _Widget, module, lang, veNls, ContentPane,

    WidgetLite, ReloadMixin, HTMLStringUtil, _TabContainer, domClass, domAttr,
    
    Select, EventedMixin, 
    TreeMemory, ObservableStore, Trackable, TemplatedWidgetBase, parser,
    _WidgetBase, ColorStore, ColorPickerFlat,
    
    veNLS, ColorPickerWidget, Deferred,
    MutableStore, BackgroundDialog, URLRewrite, Path,
    
    commonNLS, CssUtils, _PanelDialog, _,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    templateString,
    CSSUtils) {
    console.clear();
    //////////////////////////////////////////////////////////////////////////////
    //
    //
    var getCSSForWorkspaceURL = function (baseLocation, relativeURLInside) {
        //var workspaceUrl = Runtime.getUserWorkspaceUrl();
        //Need to add project path (e.g., "project1") to the front of the image url
        //relativeURLInside = new Path(baseLocation).getParentPath().append(relativeURLInside).toString();
        var val = 'url(\'' + relativeURLInside + '\')';
        return val;
    };

    var BackgroundDialog = declare("davinci.ve.widgets.BackgroundDialog", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: templateString,
        widgetsInTemplate: true,
        _filePicker: null,
        context: null,
        veNLS: veNLS,
        stopRowTemplate: "<tr class='bgdGradOptRow bgdStopRow' style='display:none;'>" +
            "<td class='bgdCol1'></td>" +
            "<td class='bgdOptsLabel bdgStopLabel'>" + veNLS.bgdTemplate + "</td>" +
            "<td class='bgdStopColorTD'>" +
            "<select class='bgdColor' dojoType='dijit.form.ComboBox'>" +
            "</select>" +
            "</td>" +
            "<td>" +
            "<select class='bgdPosition' dojoType='dijit.form.ComboBox'>" +
            "</select>" +
            "</td>" +
            "<td class='bgdPlusMinusButtons'><span class='bgdPlusButton' dojoType='dijit.form.Button'>+</span><span class='bgdMinusButton' dojoType='dijit.form.Button'>-</span></td>" +
            "</tr>",
        postMixInProperties: function () {
            this.inherited(arguments);
            dojo.mixin(this, commonNLS);
        },
        postCreate: function () {
            this.inherited(arguments);
            var langObj = this.langObj = veNLS;
            this.stoppos_store = new MutableStore({
                values: ['0%', '100%', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%']
            });

            // bpProps will hold some useful info about the backgrounds rows in the Properties palette
            var bgProps = davinci.ve._BackgroundWidgets;

            console.log('back ground dialog', bgProps);


            //FIXME: Following code is mostly a copy/paste from ColorPicker.js
            //Should be refactored into a shared utility
            this._statics = ['', davinci.ve.widgets.ColorPicker.divider, langObj.colorPicker, langObj.removeValue];
            var colormenu_data = [{
                value: this._statics[0]
            }];
            colormenu_data.push({
                value: this._statics[2],
                action: '_colorpicker'
            });
            colormenu_data.push({
                value: this._statics[3],
                action: '_removevalue'
            });
            colormenu_data.push({
                value: this._statics[1],
                action: '_donothing'
            });
            colormenu_data.push({
                value: 'transparent'
            });
            colormenu_data.push({
                value: 'black'
            });
            colormenu_data.push({
                value: 'white'
            });
            colormenu_data.push({
                value: 'red'
            });
            colormenu_data.push({
                value: 'green'
            });
            colormenu_data.push({
                value: 'blue'
            });
            var bgcolor_displayValues = [];
            this._bgcolor_action = {};
            var stops_displayValues = [];
            this._stops_action = {};
            for (var i = 0; i < colormenu_data.length; i++) {
                bgcolor_displayValues.push(colormenu_data[i].value);
                if (colormenu_data[i].action) {
                    this._bgcolor_action[colormenu_data[i].value] = colormenu_data[i].action;
                }
                // For color stops, don't allow values to be removed
                if (colormenu_data[i].value != '' && colormenu_data[i].action != '_removevalue') {
                    stops_displayValues.push(colormenu_data[i].value);
                    if (colormenu_data[i].action) {
                        this._stops_action[colormenu_data[i].value] = colormenu_data[i].action;
                    }
                }
            }
            this._bgcolor_store = new ColorStore({
                values: bgcolor_displayValues,
                noncolors: this._statics
            });
            this._stops_store = new ColorStore({
                values: stops_displayValues,
                noncolors: this._statics
            });

            // Inject initial values for all of the dialog's various fields
            // Abbreviations: bgd == BackgroundDialog, pp = Properties palette, CB == ComboBox
            var ppValue;

            // First off, need to parse background-image property
            var ppImageCB = bgProps['background-image'].comboBox;
            ppValue = ppImageCB.get('value');
            var bgddata, type;
            if (typeof ppValue == 'string' && ppValue.length > 0) {
                bgddata = CSSUtils.parseBackgroundImage(ppValue);
                type = bgddata.type;
            } else {
                bgddata = {};
            }
            this.bgddata = bgddata;

            // Stuff value into "other" textarea
            this.bgdOtherTA.set('value', ppValue);

            // Initialize dialog, getting some of initial values from properties palette fields
            // and other initial values from bgddata object
            var optsType = [];
            ['emptystring', 'url', 'linear', 'radial', 'none', 'other'].forEach(function (val) {
                optsType.push({
                    value: val,
                    label: langObj['bgdType_' + val]
                });
            });
            this.bgdTypeSelect.addOption(optsType);
            this._updateBackgroundImageType(type);
            this.connect(this.bgdTypeSelect, 'onChange', dojo.hitch(this, function () {
                var val = this.bgdTypeSelect.get('value');
                this._updateBackgroundImageType(val);
                this._onFieldChanged();
            }));

            var ppColorCB = bgProps['background-color'].comboBox;
            ppValue = ppColorCB.get('value');
            var bgdColorCB = this.bgdColorCB;
            bgdColorCB.set('store', this._bgcolor_store);
            //FIXME: Add regexp for color
            //bgdColorCB.set('regExp', 'repeat|repeat-x|repeat-y|no-repeat');
            bgdColorCB.set('value', ppValue);
            bgddata.backgroundColor = ppValue;
            this.connect(this.bgdColorCB, "onChange", dojo.hitch(this, function (event) {
                this._onChangeColor(event, this.bgdColorCB, this._bgcolor_action);
            }));
            bgProps['background-color'].bgdWidget = this.bgdColorCB;


            var ppRepeatCB = bgProps['background-repeat'].comboBox;
            ppValue = ppRepeatCB.get('value');
            var bgdRepeatCB = this.bgdRepeatCB;
            bgdRepeatCB.set('store', ppRepeatCB.store);
            bgdRepeatCB.set('regExp', 'repeat|repeat-x|repeat-y|no-repeat');
            bgdRepeatCB.set('value', ppValue);
            bgddata.backgroundRepeat = ppValue;
            this.connect(this.bgdRepeatCB, 'onChange', dojo.hitch(this, function () {
                bgddata.backgroundRepeat = this.bgdRepeatCB.get('value');
                this._onFieldChanged();
            }));
            bgProps['background-repeat'].bgdWidget = this.bgdRepeatCB;

            var ppPositionCB = bgProps['background-position'].comboBox;
            ppValue = ppPositionCB.get('value');
            var bgdPositionCB = this.bgdPositionCB;
            bgdPositionCB.set('store', ppPositionCB.store);
            //FIXME: regexp is wrong
            bgdPositionCB.set('regExp', 'auto|contain|cover|' + CSSUtils.regstr_posn);
            bgdPositionCB.set('value', ppValue);
            bgddata.backgroundPosition = ppValue;
            this.connect(this.bgdPositionCB, 'onChange', dojo.hitch(this, function () {
                bgddata.backgroundPosition = this.bgdPositionCB.get('value');
                this._onFieldChanged();
            }));
            bgProps['background-position'].bgdWidget = this.bgdPositionCB;

            var ppSizeCB = bgProps['background-size'].comboBox;
            ppValue = ppSizeCB.get('value');
            var bgdSizeCB = this.bgdSizeCB;
            bgdSizeCB.set('store', ppSizeCB.store);
            bgdSizeCB.set('regExp', 'auto|contain|cover|' + CSSUtils.regstr_len_or_pct);
            bgdSizeCB.set('value', ppValue);
            bgddata.backgroundSize = ppValue;
            this.connect(this.bgdSizeCB, 'onChange', dojo.hitch(this, function () {
                bgddata.backgroundSize = this.bgdSizeCB.get('value');
                this._onFieldChanged();
            }));
            bgProps['background-size'].bgdWidget = this.bgdSizeCB;

            var ppOriginCB = bgProps['background-origin'].comboBox;
            ppValue = ppOriginCB.get('value');
            var bgdOriginCB = this.bgdOriginCB;
            bgdOriginCB.set('store', ppOriginCB.store);
            bgdOriginCB.set('regExp', 'border-box|padding-box|content-box');
            bgdOriginCB.set('value', ppValue);
            bgddata.backgroundOrigin = ppValue;
            this.connect(this.bgdOriginCB, 'onChange', dojo.hitch(this, function () {
                bgddata.backgroundOrigin = this.bgdOriginCB.get('value');
                this._onFieldChanged();
            }));
            bgProps['background-origin'].bgdWidget = this.bgdOriginCB;

            var ppClipCB = bgProps['background-clip'].comboBox;
            ppValue = ppClipCB.get('value');
            var bgdClipCB = this.bgdClipCB;
            bgdClipCB.set('store', ppClipCB.store);
            bgdClipCB.set('regExp', 'border-box|padding-box|content-box');
            bgdClipCB.set('value', ppValue);
            bgddata.backgroundClip = ppValue;
            this.connect(this.bgdClipCB, 'onChange', dojo.hitch(this, function () {
                bgddata.backgroundClip = this.bgdClipCB.get('value');
                this._onFieldChanged();
            }));
            bgProps['background-clip'].bgdWidget = this.bgdClipCB;

            var store = new MutableStore({
                values: ['to bottom', 'to top', 'to right', 'to left', '45deg', '-45deg']
            });
            this.bgdLinearAngleCB.set('store', store);
            this.bgdLinearAngleCB.set('regExp', CSSUtils.regstr_angle);
            this.bgdLinearAngleCB.set('value', (bgddata && bgddata.angle) ? bgddata.angle : 'to bottom');
            this.connect(this.bgdLinearAngleCB, 'onChange', dojo.hitch(this, function () {
                bgddata.angle = this.bgdLinearAngleCB.get('value');
                this._onFieldChanged();
            }));

            var store = new MutableStore({
                values: ['center', 'left top', 'center center', 'right bottom', '0% 0%', '0px 0px']
            });
            this.bgdRadialPosCB.set('store', store);
            this.bgdRadialPosCB.set('regExp', CSSUtils.regstr_posn);
            this.bgdRadialPosCB.set('value', (bgddata && bgddata.posn) ? bgddata.posn : 'center');
            this.connect(this.bgdRadialPosCB, 'onChange', dojo.hitch(this, function () {
                bgddata.posn = this.bgdRadialPosCB.get('value');
                this._onFieldChanged();
            }));

            var store = new MutableStore({
                values: ['circle', 'ellipse']
            });
            this.bgdRadialShapeCB.set('store', store);
            this.bgdRadialShapeCB.set('regExp', CSSUtils.regstr_shape);
            this.bgdRadialShapeCB.set('value', (bgddata && bgddata.shape) ? bgddata.shape : 'circle');
            this.connect(this.bgdRadialShapeCB, 'onChange', dojo.hitch(this, function () {
                bgddata.shape = this.bgdRadialShapeCB.get('value');
                this._onFieldChanged();
            }));

            var store = new MutableStore({
                values: ['farthest-corner', 'farthest-side', 'closest-corner', 'closest-side']
            });
            this.bgdRadialExtentCB.set('store', store);
            this.bgdRadialExtentCB.set('regExp', CSSUtils.regstr_extent);
            this.bgdRadialExtentCB.set('value', (bgddata && bgddata.extent) ? bgddata.extent : 'farthest-corner');
            this.connect(this.bgdRadialExtentCB, 'onChange', dojo.hitch(this, function () {
                bgddata.extent = this.bgdRadialExtentCB.get('value');
                this._onFieldChanged();
            }));

            var stops;
            if (bgddata && bgddata.stops && bgddata.stops.length >= 2) {
                stops = bgddata.stops; // {color:, pos:}
            } else {
                stops = [{
                    color: 'white',
                    pos: '0%'
                }, {
                    color: 'black',
                    pos: '100%'
                }];
            }
            this._initializeStops(stops);
        },
        startup: function () {
            this.inherited(arguments);
            /* back ground image box */

            var url = (this.bgddata && this.bgddata.url) ? this.bgddata.url : '';
            this._filePicker.set('value', url);
            this._filePicker.owner = this;
            this.bgddata.url = url;
            this.connect(this._filePicker, 'onChange', dojo.hitch(this, function () {
                var fpValue = this._filePicker.get('value');
                this.bgddata.url = fpValue;
                this._onFieldChanged();
            }));

        },
        _updateBackgroundImageType: function (type) {
            var domNode = this.domNode;
            if (!(type && (type == 'none' || type == 'url' || type == 'linear' || type == 'radial' || type == 'other'))) {
                type = 'emptystring';
            }
            this.bgddata.type = type;
            this.bgdTypeSelect.set('value', type);
            var bgdOptionsLabelRows = dojo.query('.bgdOptionsLabelRow', domNode);
            var bgdImageOptRows = dojo.query('.bgdImageOptRow', domNode);
            var bgdGradOptRows = dojo.query('.bgdGradOptRow', domNode);
            var bgdLinearOptRows = dojo.query('.bgdLinearOptRow', domNode);
            var bgdRadialOptRows = dojo.query('.bgdRadialOptRow', domNode);
            var bgdOtherRows = dojo.query('.bgdOtherRow', domNode);
            if (type == 'url') {
                bgdImageOptRows.concat(bgdOptionsLabelRows).forEach(function (row) {
                    dojo.removeClass(row, 'dijitHidden');
                });
                bgdGradOptRows.concat(bgdOtherRows).forEach(function (row) {
                    dojo.addClass(row, 'dijitHidden');
                });
            } else if (type == 'linear') {
                bgdGradOptRows.concat(bgdOptionsLabelRows).forEach(function (row) {
                    dojo.removeClass(row, 'dijitHidden');
                });
                bgdImageOptRows.concat(bgdRadialOptRows).concat(bgdOtherRows).forEach(function (row) {
                    dojo.addClass(row, 'dijitHidden');
                });
            } else if (type == 'radial') {
                bgdGradOptRows.concat(bgdOptionsLabelRows).forEach(function (row) {
                    dojo.removeClass(row, 'dijitHidden');
                });
                bgdImageOptRows.concat(bgdLinearOptRows).concat(bgdOtherRows).forEach(function (row) {
                    dojo.addClass(row, 'dijitHidden');
                });
            } else if (type == 'other') {
                bgdOtherRows.forEach(function (row) {
                    dojo.removeClass(row, 'dijitHidden');
                });
                bgdImageOptRows.concat(bgdGradOptRows).concat(bgdOptionsLabelRows).forEach(function (row) {
                    dojo.addClass(row, 'dijitHidden');
                });
            } else { // 'none'
                bgdImageOptRows.concat(bgdGradOptRows).concat(bgdOptionsLabelRows).concat(bgdOtherRows).forEach(function (row) {
                    dojo.addClass(row, 'dijitHidden');
                });
            }
        },
        _initializeStops: function (stops) {
            var langObj = this.langObj;

            if (!stops) {
                stops = this.bgddata.stops;
            }

            var bgdType = this.bgdTypeSelect.get('value');
            var stopRows = dojo.query('.bgdStopRow', this.domNode);
            // Destroy all existing stop rows except for the heading row (#0)
            for (var i = stopRows.length - 1; i > 0; i--) {
                var rowNode = stopRows[i];
                var colorSelectNodes = dojo.query('.bgdColor', rowNode);
                var colorSelect = dijit.byNode(colorSelectNodes[0]);
                colorSelect.destroyRecursive();
                var posSelectNodes = dojo.query('.bgdPosition', rowNode);
                var posSelect = dijit.byNode(posSelectNodes[0]);
                posSelect.destroyRecursive();
                dojo.destroy(rowNode);
            }
            // Create a TR for each stop in stops array and insert after gradient heading row
            var gradHeaderNode = stopRows[0];
            var gradHeaderParentNode = gradHeaderNode.parentNode;
            var gradHeaderNextSibling = gradHeaderNode.nextSibling;
            for (var i = 0; i < stops.length; i++) {
                var stop = stops[i];
                var newStopRow = dojo.create('tr', {
                    'className': 'bgdGradOptRow bgdStopRow'
                });
                newStopRow.innerHTML = this.stopRowTemplate;
                if (bgdType != 'linear' && bgdType != 'radial') {
                    dojo.addClass(newStopRow, 'dijitHidden');
                }
                gradHeaderParentNode.insertBefore(newStopRow, gradHeaderNextSibling);
                dojo.parser.parse(newStopRow);
                var labelNodes = dojo.query('.bdgStopLabel', newStopRow);
                labelNodes[0].innerHTML = this.langObj.bgdStop + ' #' + (i + 1) + ':';
                var colorSelectNodes = dojo.query('.bgdColor', newStopRow);
                var colorSelect = dijit.byNode(colorSelectNodes[0]);
                colorSelect.set('store', this._stops_store);
                colorSelect.set('regExp', CSSUtils.regstr_stop_color);
                colorSelect.set('value', stop.color);
                this.connect(colorSelect, "onChange", dojo.hitch(this, function (colorSelect, event) {
                    this._onChangeColor(event, colorSelect, this._stops_action);
                }, colorSelect));
                var posSelectNodes = dojo.query('.bgdPosition', newStopRow);
                var posSelect = dijit.byNode(posSelectNodes[0]);
                posSelect.set('store', this.stoppos_store);
                var pos = (typeof stop.pos == 'string' && stop.pos.length > 0) ? stop.pos : (i == 0) ? '0%' : '100%';
                posSelect.set('regExp', CSSUtils.regstr_stop_pos);
                posSelect.set('value', pos);
                this.connect(posSelect, 'onChange', dojo.hitch(this, function () {
                    this._updateDataStructureStops();
                    this._onFieldChanged();
                }));
                var plusNode = dojo.query('.bgdPlusButton', newStopRow)[0];
                var plusButton = dijit.byNode(plusNode);
                plusButton.set('title', langObj.bgdAddStop);
                this.connect(plusButton, 'onClick', dojo.hitch(this, function (rownum) {
                    var stop = this.bgddata.stops[rownum];
                    // Duplicate row <rownum>
                    this.bgddata.stops.splice(rownum + 1, 0, {
                        color: stop.color,
                        pos: stop.pos
                    });
                    this._initializeStops();
                }, i));
                var minusNode = dojo.query('.bgdMinusButton', newStopRow)[0];
                var minusButton = dijit.byNode(minusNode);
                minusButton.set('title', langObj.bgdRemoveStop);
                this.connect(minusButton, 'onClick', dojo.hitch(this, function (rownum) {
                    var stop = this.bgddata.stops[rownum];
                    // Remove row <rownum>
                    this.bgddata.stops.splice(rownum, 1);
                    this._initializeStops();
                }, i));
                var minusNodes = dojo.query('.bgdMinusButton', this.domNode);
                // If only 2 stops, disable the minus buttons
                for (var j = 0; j < minusNodes.length; j++) {
                    var minusNode = minusNodes[j];
                    var minusButton = dijit.byNode(minusNode);
                    if (stops.length <= 2) {
                        minusButton.set('disabled', true);
                    } else {
                        minusButton.set('disabled', false);
                    }
                }
            }
            this._updateDataStructureStops();
        },
        _onFieldChanged: function () {
            this._updateDialogValidity();
            this._updateBackgroundPreview();
        },
        /*
         * This is the base location for the file in question.  Used to caluclate relativity for url(...)
         */
        _setBaseLocationAttr: function (baseLocation) {
            this._baseLocation = baseLocation;
            this._filePicker.set("baseLocation", baseLocation);
        },
        _updateDialogValidity: function () {
            var bgddata = this.bgddata;
            if (!bgddata || !bgddata.type) {
                return;
            }
            var domNode = this.domNode;
            var type = bgddata.type;
            var validityCheckRows = [];
            var valid = true;

            if (type == 'url') {
                validityCheckRows = dojo.query('.bgdImageOptRow', domNode);
            } else if (type == 'linear') {
                validityCheckRows = dojo.query('.bgdStopRow', domNode).concat(dojo.query('.bgdLinearOptRow', domNode));
            } else if (type == 'radial') {
                validityCheckRows = dojo.query('.bgdStopRow', domNode).concat(dojo.query('.bgdRadialOptRow', domNode));
            }
            validityCheckRows.forEach(function (row) {
                var widgets = dojo.query("[widgetid]", row);
                widgets.forEach(function (w) {
                    if (typeof w.isValid == 'function' && !w.isValid()) {
                        valid = false;
                    }
                });
            });
            this.bdgValid = valid;
            //this._okButton.set('disabled', !valid);
        },
        _updateBackgroundPreview: function () {
            var previewSpan = dojo.query('.bgdPreview', this.domNode)[0];
            var styleText = '';
            var bgddata = this.bgddata;

            function addProp(propName, propNameCamelCase) {
                var propValue = bgddata[propNameCamelCase];
                if (typeof propValue == 'string' && propValue.length > 0) {
                    styleText += ';' + propName + ':' + propValue;
                }
            }

            addProp('background-color', 'backgroundColor');
            addProp('background-repeat', 'backgroundRepeat');
            addProp('background-position', 'backgroundPosition');
            addProp('background-size', 'backgroundSize');
            addProp('background-origin', 'backgroundOrigin');
            addProp('background-clip', 'backgroundClip');
            var a = CSSUtils.buildBackgroundImage(this.bgddata);
            for (var i = 0; i < a.length; i++) {
                var val = a[i];
                if (URLRewrite.containsUrl(val) && !URLRewrite.isAbsolute(val) && this.context) {
                    var urlInside = URLRewrite.getUrl(val);
                    var parts = utils.parse_url(urlInside);

                    urlInside = this.context.ctx.getFileManager().getImageUrl({
                        path: parts.host + parts.path,
                        mount: parts.scheme
                    }, false);


                    if (urlInside) {
                        val = getCSSForWorkspaceURL(this._baseLocation, urlInside);
                    }

                }
                styleText += ';background-image:' + val;
            }
            previewSpan.setAttribute('style', styleText);

            //FIXME: Refactor code so that we don't have to rely on updating background preview
            // to update this field
            // Stuff latest into "other" textarea
            this.bgdOtherTA.set('value', a[a.length - 1]);

        },
        _updateDataStructureStops: function () {
            // Update bdgdata.stops from latest values in dialog fields (note: "bgd"=background dialog)
            // NOTE: During dialog initialization, this routine gets called N times, where N=(#stoprows*2)+1
            // because Dojo appears to stuff values into form fields asynchronously, causing onChange events
            // on color stop form fields even though the code above sets the value before
            // establishing the onChange event handler
            var bgddata = this.bgddata;
            bgddata.stops = [];
            var stopRows = dojo.query('.bgdStopRow', this.domNode);
            // Ignore the heading row (#0)
            for (var i = 1; i < stopRows.length; i++) {
                var rowNode = stopRows[i];
                var colorSelectNodes = dojo.query('.bgdColor', rowNode);
                var colorSelect = dijit.byNode(colorSelectNodes[0]);
                var posSelectNodes = dojo.query('.bgdPosition', rowNode);
                var posSelect = dijit.byNode(posSelectNodes[0]);
                bgddata.stops.push({
                    color: colorSelect.get('value'),
                    pos: posSelect.get('value')
                });
            }
        },
        /**
         * The setter function for this dialog is only used to field values returned
         * by the color picker.
         * @param value  new color value
         */
        _setValueAttr: function (value) {
            if (value) {
                // This will trigger _onChangeColor
                this._colorPickerTargetWidget.set('value', value);
            }
        },
        _onChangeColor: function (event, targetComboBox, actions) {
            var action = actions[event];
            if (action) {
                if (action == '_removevalue') {
                    targetComboBox.set('value', '');
                } else {
                    // restore old value
                    targetComboBox.set('value', targetComboBox._currentcolorvalue);
                }
                if (action == '_colorpicker') {
                    var colorPickerFlat = new davinci.ve.widgets.ColorPickerFlat({});
                    var initialValue = targetComboBox.get("value");
                    var isLeftToRight = this.isLeftToRight();
                    this._colorPickerTargetWidget = targetComboBox;
                    davinci.ve.widgets.ColorPickerFlat.show(colorPickerFlat, initialValue, this, isLeftToRight);
                }
            }
            targetComboBox._currentcolorvalue = targetComboBox.get('value');

            // Update bgddata for all color fields even though only one of them has changed
            this.bgddata.backgroundColor = this.bgdColorCB.get('value');
            this._updateDataStructureStops();
            this._onFieldChanged();
        },
        okButton: function () {},
        cancelButton: function () {
            this.onClose();
        }

    });
    //Make helpers available as "static" functions
    BackgroundDialog.getCSSForWorkspaceURL = getCSSForWorkspaceURL;

    lang.setObject("davinci.ve.widgets.BackgroundDialog", BackgroundDialog);



    parser._clearCache();
    var ctx = window.sctx;

    //////////////////////////////////////////////////////////////////////////////
    //
    //
    var MultiInputDropDown = dcl(TemplatedWidgetBase, {
        numberDelta: 1,
        templateString: '<div ></div>',
        insertPosition: 1,
        declaredClass: "davinci.ve.widgets.MultiInputDropDown",
        data: null,
        postCreate: function () {},
        get: function (what) {
            if (what === 'value' && this._dropDown) {
                return this._getValueAttr();
            }
            return "";
        },
        startup: function () {
            var topSpan = dojo.doc.createElement("div");
            this._run = {};
            var self = this;
            if (!this.data) {
                this.data = [{
                        value: "auto"
                    },
                    {
                        value: "0px"
                    },
                    {
                        value: MultiInputDropDown.divider
                    },
                    {
                        value: "Remove Value",
                        run: function () {
                            this.set('value', '', false);
                            self._dropDown.set('value', '')
                        }
                    },
                    {
                        value: MultiInputDropDown.divider
                    },
                    {
                        value: "Help",
                        run: function () {
                            alert("help!")
                        }
                    }
                ];
            } else {
                this.data.push({
                    value: MultiInputDropDown.divider
                });
                this.data.push({
                    label: "Remove Value",
                    value: "Remove Value",
                    run: function () {
                        this.set('value', '', false);
                        self._dropDown.set('value', '', true);
                    }
                });
            }
            var displayValues = [];
            for (var i = 0; i < this.data.length; i++) {
                displayValues.push(this.data[i].value);
                if (this.data[i].run) {
                    this._run[this.data[i].value] = this.data[i].run;
                }
            }
            var options = [];
            _.each(this.data, function (data) {
                if (data.value || data.label === 'Remove Value') {
                    options.push({
                        label: data.label || data.value,
                        value: data.value,
                        run: data.run
                    })
                }
            })



            var SELECT_CLASS = Select;
            this._dropDown = new SELECT_CLASS({
                EDITABLE_CLASS: SELECT_CLASS.TYPEAHEAD,
                storeClass: declare('driverStore', [TreeMemory, Trackable, ObservableStore], {
                    setValues: function (values) {
                        var items = [];
                        var counter = 0;
                        if (values) {
                            this._values = values;
                        }
                        dojo.forEach(this.data, dojo.hitch(this, function (item) {
                            items.push({
                                label: item.label,
                                value: item.value,
                                id: counter++
                            });
                        }));

                        this._jsonData = {
                            identifier: "id",
                            items: items
                        };
                        this.setData(items);
                        this.root._emit('update', {
                            target: null
                        });
                    },
                    modifyItem: function (oldValue, newValue) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].value === oldValue) {
                                this.data[i].value = newValue;
                            }
                        }
                        self._dropDown.set('value', newValue);
                        this.setValues();
                    },
                    /* insert an item at the given index */
                    insert: function (atIndex, value) {
                        this.data.splice(atIndex, 0, {
                            value: value,
                            label: value
                        });
                        this.setValues();
                    },
                    contains: function (item) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].value == item) {
                                return true;
                            }
                        }
                        return false;
                    },
                    /* finds a value in the store that has the same units as specified value */
                    findSimilar: function (value) {
                        var numbersOnlyRegExp = new RegExp(/(\D*)(-?)(\d+)(\D*)/);
                        var numberOnly = numbersOnlyRegExp.exec(value);
                        if (!numberOnly) {
                            return;
                        }
                        var unitRegExp = new RegExp((numberOnly.length > 0 ? numberOnly[1] : "") + "(-?)(\\d+)" + (numberOnly.length > 3 ? numberOnly[4] : ""));
                        for (var i = 0; i < this.data.length; i++) {
                            if (unitRegExp.test(this.data[i].value)) {
                                return this.data[i].value;
                            }
                        }
                    },
                    getItemNumber: function (index) {
                        return this.data[index];
                    },
                    clearValues: function () {
                        this._loadFinished = false;
                    }
                }),
                required: false,
                title: "",
                editable: true,
                options: options,
                userData: {}
            });
            this._dropDown.startup();

            this._store = this._dropDown.store;
            var plus = factory.createButton(this._dropDown.previewNode, 'fa-plus', 'btn-default btn-xs2', null, '');
            this._plus = plus;
            dojo.style(plus, 'float', 'right');
            var minus = factory.createButton(this._dropDown.button0, 'fa-minus', 'btn-default btn-xs2', null, '');
            this._minus = minus;

            var div = dojo.create("div", {
                'class': "propInputWithIncrDecrButtons"
            });
            div.appendChild(this._dropDown.domNode);
            topSpan.appendChild(div);

            div = dojo.doc.createElement("div");
            dojo.style(div, "clear", "both");
            topSpan.appendChild(div);

            this._currentValue = this._store.data[0];
            dojo.connect(this._dropDown, "onKeyUp", this, "_updateSpinner");
            var thiz = this;

            if (this._dropDown._on) {
                this._dropDown._on('change', function (value) {
                    thiz._onChange(value);
                })
            } else {
                dojo.connect(this._dropDown, "onChange", this, "_onChange");
            }

            dojo.connect(this._plus, "onclick", this, "_plusButton", false);
            dojo.connect(this._minus, "onclick", this, "_minusButton", false);

            this._updateSpinner();
            this.domNode.appendChild(topSpan);
            this.add(this._dropDown);
            this.add(this._store);
        },
        _setReadOnlyAttr: function (isReadOnly) {
            this._isReadOnly = isReadOnly;
            if (this._dropDown) {
                this._dropDown.set("disabled", isReadOnly);
                dojo.attr(this._plus, "disabled", isReadOnly);
                dojo.attr(this._minus, "disabled", isReadOnly);
            }
        },
        onChange: function (event) {},
        _getValueAttr: function () {
            return this._dropDown.get("value");
        },
        _setValueAttr: function (value, priority) {
            this._dropDown.set("value", value, true);
            this._currentValue = this._dropDown.get("value");
            priority !== false && this._onChange(this._currentValue);
            if (!priority) {
                this.onChange();
            }
        },
        _changeValue: function (value, delta) {
            var split = value.split(" ");
            var result = "";
            for (var i = 0; i < split.length; i++) {
                if (i > 0) {
                    result += " ";
                }
                var bits = split[i].match(/([-\d\.]+)([a-zA-Z%]*)/);
                if (!bits) {
                    result += split[i];
                } else {
                    if (bits.length == 1) {
                        result += bits[0];
                    } else {
                        for (var z = 1; z < bits.length; z++) {
                            if (!isNaN(bits[z]) && bits[z] != "") {
                                result += parseFloat(bits[z]) + delta;
                            } else {
                                result += bits[z];
                            }
                        }
                    }
                }
            }
            return result;
        },
        _plusButton: function () {
            var oldValue = this._dropDown.get("value");
            var newString = this._changeValue(oldValue, this.numberDelta);
            this._store.modifyItem(oldValue, newString);
            this._dropDown.set("value", newString);
            this._onChange(newString);
        },
        _minusButton: function () {
            var oldValue = this._dropDown.get("value");
            var newString = this._changeValue(oldValue, -1 * this.numberDelta);
            this._store.modifyItem(oldValue, newString);
            this._dropDown.set("value", newString);
            this._onChange(newString);
        },
        _updateSpinner: function () {
            var value = this._dropDown.get("value");
            var numbersOnlyRegExp = /(-?)(\d+){1}/;
            var numberOnly = numbersOnlyRegExp.exec(value);
            if (numberOnly && numberOnly.length) {
                this._minus.disabled = this._plus.disabled = false;
                //dojo.removeClass(this._minus, "dijitHidden");
                //dojo.removeClass(this._plus, "dijitHidden");
            } else {
                //dojo.addClass(this._minus, "dijitHidden");
                //dojo.addClass(this._plus, "dijitHidden");
                this._minus.disabled = this._plus.disabled = true;
            }
            return true;
        },
        _onChange: function (event) {
            var similar;
            if (event in this._run) {
                this._dropDown.get("value", this._store.getItemNumber(0));
                dojo.hitch(this, this._run[event])();
            } else if (event == MultiInputDropDown.divider) {
                this._dropDown.get("value", this._store.getItemNumber(0));
            } else if (similar = this._store.findSimilar(event)) {
                this._store.modifyItem(similar, event);
            } else if (!this._store.contains(event)) {
                this._store.insert(this.insertPosition, event);
            }
            if (this._currentValue != this._dropDown.get("value")) {
                this._currentValue = this._dropDown.get("value");
                this.onChange(event);
            }
            this._updateSpinner();
        }

    });
    dojo.mixin(MultiInputDropDown, {
        divider: "---"
    });
    lang.setObject("davinci.ve.widgets.MultiInputDropDown", MultiInputDropDown);

    //////////////////////////////////////////////////////////////////////////////
    //
    //

    var colorPicker = dcl(TemplatedWidgetBase, {
        templateString: '<div ></div>',
        numberDelta: 1,
        insertPosition: 9,
        data: null,
        _onChange: function (event) {},
        _setValueAttr: function (value, priority) {
            this._dropDown.set("value", value.replace('#', ''), true);
            this._currentValue = this._dropDown.get("value");
            priority !== false && this._onChange(this._currentValue);
            if (!priority) {
                if (value !== this._currentValue) {
                    this.onChange();
                }
            }
        },
        set: function (what, value) {
            if (what === 'value' && this._dropDown) {
                return this._setValueAttr(value);
            }
        },
        get: function (what) {
            if (what === 'value' && this._dropDown) {
                return this._getValueAttr();
            }
            return "";
        },
        postCreate: function () {},
        startup: function () {
            var widget = utils.addWidget(ColorPickerWidget, {
                title: ""
            }, null, this.domNode, true);
            this._dropDown = widget;
            this._value = this._value || "";
            this._dropDown.set("value", this._value.replace('#', ""), true);
            var self = this;
            this._dropDown._on('change', function (evt) {
                self._value = self._getValueAttr();
                self.onChange(self._value);
            });
            this.add(widget);
        },
        _setReadOnlyAttr: function (isReadOnly) {
            this._isReadOnly = isReadOnly;
            this._dropDown.set("disabled", isReadOnly);
        },
        onChange: function () {
            this._value = this._dropDown.get("value");
        },
        _getValueAttr: function () {
            return this._dropDown.get("value");
        }
    });
    dojo.mixin(colorPicker, {
        divider: "---"
    });
    //lang.setObject("davinci.ve.widgets.ColorPicker", colorPicker);


    //////////////////////////////////////////////////////////////////////////////
    //
    //
    var idPrefix = "davinci_ve_widgets_properties_border_generated";
    var __id = 0,
        getId = function () {
            return idPrefix + (__id++);
        };
    //var Background = declare("davinci.ve.widgets.Background", [WidgetLite], {
    var Background = dcl(TemplatedWidgetBase, {
        declaredClass: "davinci.ve.widgets.Background",
        templateString: '<div ></div>',
        __id: 0,
        data: null,
        get: function (what) {
            if (this._comboBox) {
                return this._comboBox.get("value");
            }
        },
        startup: function () {
            //this.domNode =   dojo.doc.createElement("div",{style:"width:100%"});
            this._textFieldId = getId();
            this._buttonId = getId();
            var buttonDiv = dojo.create("div", {
                className: 'bgPropButtonDiv',
                style: 'float:right; width:28px;'
            });
            var button = dojo.create("button", {
                innerHTML: '...',
                id: this._buttonId,
                className: 'bgPropButton btn-info',
                style: 'font-size:1em;'
            }, buttonDiv);

            var marginRight = 38;
            var colorswatch = this._colorswatch = (this.colorswatch == 'true');
            //this._colorswatch = colorswatch = false;

            if (colorswatch) {
                marginRight = 56;
                var buttonDiv2 = dojo.doc.createElement("div");
                dojo.addClass(buttonDiv2, 'colorPicker');
                this._selectedColor = dojo.doc.createElement("div");
                this._selectedColor.innerHTML = "&nbsp;";
                dojo.addClass(this._selectedColor, 'colorPickerSelected');
                dojo.addClass(this._selectedColor, 'colorPickerSelectedSkinny');
                this._colorPickerFlat = new ColorPickerFlat({});
            }

            var comboDiv = dojo.create("div", {
                className: 'bgPropComboDiv',
                style: 'margin-right:' + marginRight + 'px;padding:1px 0;'
            });
            var values = dojo.isArray(this.data) ? this.data : [''];
            var langObjVE = this.langObjVE = veNLS;

            if (colorswatch) {
                //FIXME: Following code is mostly a copy/paste from ColorPicker.js
                //Should be refactored into a shared utility
                this._statics = ["", davinci.ve.widgets.ColorPicker.divider, langObjVE.colorPicker, langObjVE.removeValue];
                this._run = {};
                if (!this.data) {
                    this.data = [{
                        value: this._statics[0]
                    }];
                    this.data.push({
                        value: this._statics[2],
                        run: this._chooseColorValue
                    });
                    this.data.push({
                        value: this._statics[3],
                        run: function () {
                            this.set('value', '');
                        }
                    });
                    this.data.push({
                        value: this._statics[1]
                    });
                    this.data.push({
                        value: 'transparent'
                    });
                    this.data.push({
                        value: 'black'
                    });
                    this.data.push({
                        value: 'white'
                    });
                    this.data.push({
                        value: 'red'
                    });
                    this.data.push({
                        value: 'green'
                    });
                    this.data.push({
                        value: 'blue'
                    });
                } else {
                    this.data.push({
                        value: davinci.ve.widgets.ColorPicker.divider
                    });
                    this.data.push({
                        value: langObj.removeValue,
                        run: function () {
                            this.set('value', '');
                        }
                    });
                }
                var displayValues = [];
                for (var i = 0; i < this.data.length; i++) {
                    displayValues.push(this.data[i].value);
                    if (this.data[i].run) {
                        this._run[this.data[i].value] = this.data[i].run;
                    }
                }
                this._store = new ColorStore({
                    values: displayValues,
                    noncolors: this._statics
                });
            } else {
                this._store = new MutableStore({
                    values: values
                });
            }
            var options = [];
            _.each(this.data, function (data) {
                if (!data) {
                    return;
                }
                if (_.isString(data) && data.length) {
                    options.push({
                        label: data,
                        value: data,
                        run: data.run
                    })
                } else if (_.isObject(data)) {
                    if (_.isString(data.value) && !data.value.length) {
                        return;
                    }
                    options.push({
                        label: data.label || data.value,
                        value: data.value,
                        run: data.run
                    })
                }
            })


            var SELECT_CLASS = Select;
            this._comboBox = new SELECT_CLASS({
                EDITABLE_CLASS: SELECT_CLASS.TYPEAHEAD,
                storeClass: declare('driverStore', [TreeMemory, Trackable, ObservableStore], {
                    setValues: function (values) {
                        var items = [];
                        var counter = 0;
                        if (values) {
                            this._values = values;
                        }
                        dojo.forEach(this.data, dojo.hitch(this, function (item) {
                            items.push({
                                label: item.label,
                                value: item.value,
                                id: counter++
                            });
                        }));

                        this._jsonData = {
                            identifier: "id",
                            items: items
                        };
                        this.setData(items);
                        this.root._emit('update', {
                            target: null
                        });
                    },
                    modifyItem: function (oldValue, newValue) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].value === oldValue) {
                                this.data[i].value = newValue;
                            }
                        }
                        self._dropDown.set('value', newValue);
                        this.setValues();
                    },
                    /* insert an item at the given index */
                    insert: function (atIndex, value) {
                        this.data.splice(atIndex, 0, {
                            value: value,
                            label: value
                        });
                        this.setValues();
                    },
                    contains: function (item) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].value == item) {
                                return true;
                            }
                        }
                        return false;
                    },
                    /* finds a value in the store that has the same units as specified value */
                    findSimilar: function (value) {
                        var numbersOnlyRegExp = new RegExp(/(\D*)(-?)(\d+)(\D*)/);
                        var numberOnly = numbersOnlyRegExp.exec(value);
                        if (!numberOnly) {
                            return;
                        }
                        var unitRegExp = new RegExp((numberOnly.length > 0 ? numberOnly[1] : "") + "(-?)(\\d+)" + (numberOnly.length > 3 ? numberOnly[4] : ""));
                        for (var i = 0; i < this.data.length; i++) {
                            if (unitRegExp.test(this.data[i].value)) {
                                return this.data[i].value;
                            }
                        }
                    },
                    getItemNumber: function (index) {
                        return this.data[index];
                    },
                    clearValues: function () {
                        this._loadFinished = false;
                    }
                }),
                required: false,
                title: "",
                editable: true,
                options: options,
                userData: {},
                id: this._textFieldId
            });
            this._comboBox.startup();
            comboDiv.appendChild(this._comboBox.domNode);

            //this._store = this._comboBox.store;

            ///this._comboBox = new ComboBox({store:this._store, id:this._textFieldId, style:'width:100%;'});
            //comboDiv.appendChild(this._comboBox.domNode);

            this.domNode.appendChild(buttonDiv);
            if (colorswatch) {
                this.domNode.appendChild(buttonDiv2);
                dojo.connect(buttonDiv2, 'onclick', dojo.hitch(this, function (event) {
                    dojo.stopEvent(event);
                    this._chooseColorValue();
                }));
                buttonDiv2.appendChild(this._selectedColor);
                // Part of convoluted logic to make sure onChange logic doesn't trigger
                // ask user prompts for read-only themes or global theme changes to read/write themes
                // ColorPickerFlat.js uses this long-winded property
                this._colorPickerFlat_comboBoxUpdateDueTo = 'colorSwatch';
            }

            this.domNode.appendChild(comboDiv);

            if (typeof this.propname == 'string' && this._comboBox) {
                if (!davinci.ve._BackgroundWidgets) {
                    davinci.ve._BackgroundWidgets = {};
                }
                // Add to cross-reference table for all of the background properties.
                // This table is used by Background palette.
                davinci.ve._BackgroundWidgets[this.propname] = {
                    propPaletteWidget: this,
                    comboBox: this._comboBox
                };
            }
            this._button = dojo.byId(this._buttonId);
            dojo.connect(this._button, "onclick", this, function () {
                //FIXME: this._valueArray = widget._valueArrayNew;
                var context = (this._cascade && this._cascade._widget && this._cascade._widget.getContext) ?
                    this._cascade._widget.getContext() : null;

                var background = new BackgroundDialog({
                    context: context
                });
                var executor = dojo.hitch(this, function (background) {
                    if (!context) {
                        console.error('Background.js. no context');
                        //return;
                        context = {};
                    }

                    // Variables used to deal with complexities around attempting to change
                    // multiple properties at once given that the property changes might
                    // be targeting read-only CSS files or read-write theme CSS files,
                    // both of which generate an (async) modal dialog in Cascade.js.
                    // The logic in this routine ensures that each of the N properties
                    // that are changed are processed in a particular order, thus ensuring
                    // Cascade.js has prompted user (if necessary) on first property before we
                    // invoke logic to update other properties.
                    // There are actually two bits of async logic that make things difficult.
                    // First, dojo's onchange handlers are launched in a timeout.
                    // Second, Cascade.js modal dialogs are also async.
                    var cascadeBatch = context.cascadeBatch = {};
                    var propNum = 0;
                    var propList = cascadeBatch.propList = []; // Array of properties whose values actually changed
                    var actions = cascadeBatch.actions = {}; // per-prop: logic to change the combox box on properties palette
                    var deferreds = cascadeBatch.deferreds = {}; // per-prop: Deferred objects to help with managing async issues
                    cascadeBatch.askUserResponse = undefined;

                    // Call buildBackgroundImage to convert the bgddata object
                    // into an array of background-image property declarations
                    // which cause gradients to magically work across different browsers.
                    if (!background.cancel) {
                        var xref = davinci.ve._BackgroundWidgets;
                        for (var propName in xref) {
                            var o = xref[propName];
                            if (o.bgdWidget) {
                                var newValue = o.bgdWidget.get('value');
                                var oldValue = o.propPaletteWidget.get('value');
                                if (newValue !== oldValue) {
                                    propList.push(propName);
                                    actions[propName] = dojo.hitch(this, function (o, newValue) {
                                        o.propPaletteWidget._comboBoxUpdateDueTo = 'backgroundDialog';
                                        o.propPaletteWidget.set('value', newValue);
                                    }, o, newValue);

                                    deferreds[propName] = new Deferred();
                                }
                            }
                        }
                        var propName = 'background-image';
                        var o = xref[propName];
                        var a = CssUtils.buildBackgroundImage(background.bgddata);
                        for (var i = 0; i < a.length; i++) {
                            var val = a[i];
                            if (URLRewrite.containsUrl(val) && !URLRewrite.isAbsolute(val)) {
                                var urlInside = URLRewrite.getUrl(val);
                                if (urlInside) {
                                    var urlPath = new Path(urlInside);
                                    var relativeUrl = urlPath.toString();
                                    val = 'url(\'' + relativeUrl + '\')';
                                }
                                a[i] = val;
                            }
                        }
                        var newValue;
                        if (a.length == 0) {
                            newValue = '';
                        } else {
                            newValue = a[a.length - 1];
                        }
                        var oldValue = o.propPaletteWidget.get('value');
                        if (newValue !== oldValue) {
                            // Hack: put the values array onto the cascade object with assumption
                            // that cascade object's onChange callback will know how to deal with the array
                            // Used by Cascade.js:_onFieldChange()
                            if (o.propPaletteWidget._cascade) {
                                o.propPaletteWidget._cascade._valueArrayNew = a;
                            }
                            propList.push(propName);
                            actions[propName] = dojo.hitch(this, function (i, newValue) {
                                o.propPaletteWidget._comboBoxUpdateDueTo = 'backgroundDialog';
                                o.propPaletteWidget.set('value', newValue);
                            }, o, newValue);
                            deferreds[propName] = new Deferred();
                        }
                        for (var i = 0; i < propList.length; i++) {
                            var propName = propList[i];
                            var deferred = deferreds[propName];
                            deferred.then(dojo.hitch(this, function (propNum) {
                                // Trigger change in next property
                                var propName = propList[propNum + 1];
                                if (propName) {
                                    actions[propName].apply();
                                } else {
                                    // All done with all properties
                                    delete context.cascadeBatch;
                                }
                            }, i));
                        }
                        // Trigger change in first property
                        if (propList.length > 0) {
                            var propName = propList[0];
                            actions[propName].apply();
                        }
                    }
                    return true;
                }, background);
                var xref = davinci.ve._BackgroundWidgets;
                for (var propName in xref) {
                    var o = xref[propName];
                    var cascade = o.propPaletteWidget._cascade;
                    if (cascade) {
                        // Before launching dialog, make sure all _valueArrayNew properties
                        // are equivalenced to existing _valueArray so that we don't trigger
                        // changes when no changes have actually been made.
                        cascade._valueArrayNew = cascade._valueArray;
                    }
                }
                background.set('baseLocation', this._baseLocation);

                //Workbench.showModal(background, "Background", '', executor);
                var dialog = new _PanelDialog({
                    onOk: function () {
                        executor();
                    },
                    onShow: function (panel, contentNode, instance) {
                        utils.addChild(contentNode, background, true);
                        this.add(background, null, false);
                        this.startDfd.resolve();
                        return [background];
                    }
                });

                dialog.show();

            });
            var self = this;
            this._comboBox._on('change', function (evt) {
                self._onChange(evt);
            });
            this.add(this._comboBox);

            /*
            this.connect(this._comboBox, 'onChange', dojo.hitch(this, function(event){
                // If new value is divider or color picker, reset the value in the ComboBox to previous value
                //FIXME: This is a hack to overcome fact that choosing value in ComboBox menu
                //causes the textbox to get whatever was selected in menu, even when it doesn't represent
                //a valid color vlaue. Resetting it here resets the value before Cascade.js gets invoked
                //due to call to this.onChange()
                if(event == davinci.ve.widgets.ColorPicker.divider || event == this.langObjVE.colorPicker){
                    this._comboBox.set('value', this.value);
                }
                // If onChange was triggered by an internal update to the text field,
                // don't invoke onChange() function (which triggers update logic in Cascade.js).
                // You see, Cascade.js logic can't tell difference between user changes to a field
                // versus software updates to the field, such as due to a new widget selection.
                var dueTo = this._comboBoxUpdateDueTo;
                this._comboBoxUpdateDueTo = undefined;
                if(dueTo == 'setAttr'){
                    return;
                }
                this._onChange(event);
            }));
            */
            /*
            this.connect(this._comboBox, 'onFocus', dojo.hitch(this, function(event){
                // If focus goes into any of the background text fields, then
                // clear out any leftover _valueArrayNew values on cascade objects
                var xref = davinci.ve._BackgroundWidgets;
                for(var propName in xref){
                    var o = xref[propName];
                    var cascade = o.propPaletteWidget._cascade;
                    if(cascade){
                        cascade._valueArrayNew = undefined;
                    }
                }
                var context = (this._cascade && this._cascade._widget && this._cascade._widget.getContext)
                    ? this._cascade._widget.getContext() : null;
                if(context){
                    delete context.cascadeBatch;
                }
            }));
            */
            this._maqStartupComplete = true;
        },
        /*
         * This is the base location for the file in question.  Used to caluclate relativity for url(...)
         */
        _setBaseLocationAttr: function (baseLocation) {
            this._baseLocation = baseLocation;

        },
        _setValueAttr: function (value) {
            // value is now an array if there are more than one of a given property for the selected rule
            var oldComboBoxValue = this._comboBox.get('value');
            var newComboBoxValue;
            //this.value = value;
            if (this._colorswatch) {
                dojo.style(this._selectedColor, "backgroundColor", value);
            }
            /* check if array or single value.  If its a single value we'll just set the text box to that value */
            if (!dojo.isArray(value)) {
                newComboBoxValue = value;
            } else if (value.length > 0) {
                newComboBoxValue = value[value.length - 1];
            } else {
                newComboBoxValue = '';
            }
            if (oldComboBoxValue !== newComboBoxValue) {
                // Flag to tell onChange handler to not invoke onChange logic
                // within Cascade.js
                if (this._comboBoxUpdateDueTo !== 'backgroundDialog' && this._comboBoxUpdateDueTo !== 'colorSwatch') {
                    this._comboBoxUpdateDueTo = 'setAttr';
                }
                this._comboBox.set('value', newComboBoxValue);
            }

        },
        _onChange: function (event) {
            //FIXME: First part of this routine is largely copied/pasted code from ColorPicker.js
            //Maybe consolidate?
            if (this._colorswatch && typeof event == 'string') {
                if (event in this._run) {
                    dojo.hitch(this, this._run[event])();
                } else if (event == davinci.ve.widgets.ColorPicker.divider) {
                    this._comboBox.set("value", this._store.getItemNumber(0));
                    //}else if(!this._store.contains(event)){
                    //this._store.insert(this.insertPosition, event);
                    //dojo.style(this._selectedColor, "backgroundColor", event);
                } else {
                    dojo.style(this._selectedColor, "backgroundColor", event);
                }
            }

            var value = this._comboBox.get("value");
            this.value = value;
            this.onChange(event);
        },
        onChange: function (event) {},
        _getValueAttr: function () {
            return this._comboBox.get("value");
        },
        _setReadOnlyAttr: function (isReadOnly) {
            if (!this._maqStartupComplete) {
                return;
            }
            this._isReadOnly = isReadOnly;
            this._comboBox.set("disabled", isReadOnly);
            this._button.disabled = isReadOnly;
        },
        _chooseColorValue: function () {
            if (this._isReadOnly) {
                return;
            }
            var initialValue = this._comboBox.get("value");
            var isLeftToRight = this.isLeftToRight();
            davinci.ve.widgets.ColorPickerFlat.show(this._colorPickerFlat, initialValue, this, isLeftToRight);
        }
    });
    lang.setObject("davinci.ve.widgets.Background", Background);

    var StyleView = declare("xideve/views/StyleView2", [WidgetLite, ReloadMixin, EventedMixin], {
        /* FIXME: These won't expand into pageTemplate. Not sure if that's a JS issue or dojo.declare issue.
         * Temporary copied into each property below but would be better if could get reusable values working somehow.
         _paddingMenu:['', '0px', '1em'],
         _radiusMenu:['', '0px', '6px'],
         */
        showWidgetProperties: true,
        _editor: null, // selected editor
        _widget: null, // selected widget
        _widgets: null,
        _subWidget: null, // selected sub widget
        _titleBarDiv: "<div class='palette_titleBarDiv'><span class='paletteCloseBox'></span><span class='titleBarDivTitle'></span></div>",
        constructor: function (params, srcNodeRef) {
            this.subscribe("/davinci/ui/editorSelected", dojo.hitch(this, this._editorSelected));
            this._on("/davinci/ui/widgetSelected", dojo.hitch(this, this._widgetSelectionChanged));
            this.subscribe("/davinci/states/state/changed", dojo.hitch(this, this._stateChanged));
            this.subscribe("/maqetta/appstates/state/changed", dojo.hitch(this, this._stateChanged));
            this.subscribe("/davinci/ui/initialPerspectiveReady", dojo.hitch(this, this._initialPerspectiveReady));
            this.subscribe("/davinci/workbench/ready", dojo.hitch(this, this._workbenchReady));
        },
        _pageTemplate: [
            //Note: the keys here must match the propsect_* values in the supports() functions
            //in the various editors, such as PageEditor.js and ThemeEditor.js

            {
                key: "common",
                className: 'maqPropertySection page_editor_only',
                addCommonPropertiesAtTop: false,
                pageTemplate: {
                    html: "<div dojoType='davinci.ve.widgets.CommonProperties'></div>"
                }
            },

            // NOTE: the first section (widgetSpecific) is injected within buildRendering()
            {
                key: "widgetSpecific",
                className: 'maqPropertySection page_editor_only',
                addCommonPropertiesAtTop: false,
                html: "<div dojoType='davinci.ve.widgets.WidgetProperties'></div>"
            },

            {
                key: "layout",
                className: 'maqPropertySection',
                addCommonPropertiesAtTop: true,
                /* startsNewGroup:true, // This flag causes a few extra pixels between this and previous button */
                pageTemplate: [{
                        display: "width",
                        type: "multi",
                        target: ["width"],
                        values: ['', 'auto', '100%', '200px', '10em']
                    },
                    {
                        display: "height",
                        type: "multi",
                        target: ["height"],
                        values: ['', 'auto', '100%', '200px', '10em']
                    },
                    {
                        html: "&nbsp;"
                    },
                    {
                        key: "showMinMax",
                        display: "&nbsp;&nbsp;&nbsp;",
                        type: "toggleSection",
                        pageTemplate: [{
                                display: "min-height",
                                type: "multi",
                                target: ["min-height"],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "max-height",
                                type: "multi",
                                target: ["max-height"],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "min-width",
                                type: "multi",
                                target: ["min-width"],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "max-width",
                                type: "multi",
                                target: ["max-width"],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                html: "&nbsp;",
                                rowClass: "propertiesSectionHidden"
                            }
                        ]
                    },
                    {
                        display: "position",
                        type: "combo",
                        target: ["position"],
                        values: ['', 'absolute', 'fixed', 'relative', 'static']
                    },
                    {
                        display: "left",
                        type: "multi",
                        target: ["left"],
                        values: ['', '0px', '1em']
                    },
                    {
                        display: "top",
                        type: "multi",
                        target: ["top"],
                        values: ['', '0px', '1em']
                    },
                    {
                        display: "right",
                        type: "multi",
                        target: ["right"],
                        values: ['', '0px', '1em']
                    },
                    {
                        display: "bottom",
                        type: "multi",
                        target: ["bottom"],
                        values: ['', '0px', '1em']
                    },
                    {
                        display: "display",
                        type: "combo",
                        target: ["display"],
                        values: ['', 'none', 'block', 'inline', 'inline-block']
                    },
                    {
                        display: "opacity",
                        type: "multi",
                        target: ["opacity"],
                        values: ['0', '0.5', '1.0']
                    },
                    {
                        display: "box-shadow",
                        type: "text",
                        target: ["box-shadow"],
                        value: ['', 'none', '1px 1px rgba(0,0,0,.5)']
                    },
                    {
                        display: "float",
                        type: "combo",
                        target: ["float"],
                        values: ['', 'none', 'left', 'right']
                    },
                    {
                        display: "clear",
                        type: "combo",
                        target: ["clear"],
                        values: ['', 'none', 'left', 'right', 'both']
                    },
                    {
                        display: "overflow",
                        type: "combo",
                        target: ["overflow"],
                        values: ['', 'visible', 'hidden', 'scroll', 'auto']
                    },
                    {
                        display: "z-index",
                        type: "multi",
                        target: ["z-index"],
                        values: ['', 'auto', '0', '1', '100', '-1', '-100']
                    },
                    {
                        display: "box-sizing",
                        type: "combo",
                        target: ['box-sizing', '-webkit-box-sizing', '-ms-box-sizing', '-moz-box-sizing'],
                        values: ['', 'content-box', 'border-box']
                    }
                ]
            },
            {
                key: "padding",
                className: 'maqPropertySection',
                addCommonPropertiesAtTop: true,
                pageTemplate: [{
                        display: "<b>(padding)</b>",
                        type: "multi",
                        target: ["padding"],
                        values: ['', '0px', '1em']
                    },
                    {
                        key: "showtrbl",
                        display: "&nbsp;&nbsp;&nbsp;",
                        type: "toggleSection",
                        pageTemplate: [{
                                display: "padding-top",
                                type: "multi",
                                target: ["padding-top"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "padding-right",
                                type: "multi",
                                target: ["padding-right"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "padding-bottom",
                                type: "multi",
                                target: ["padding-bottom"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "padding-left",
                                type: "multi",
                                target: ["padding-left"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            }
                        ]
                    }
                ]
            },
            {
                key: "margins",
                className: 'maqPropertySection',
                addCommonPropertiesAtTop: true,
                pageTemplate: [{
                        display: "<b>(margin)</b>",
                        type: "multi",
                        target: ["margin"],
                        values: ['', '0px', '1em']
                    },
                    {
                        key: "showtrbl",
                        display: "&nbsp;&nbsp;&nbsp;",
                        type: "toggleSection",
                        pageTemplate: [{
                                display: "margin-top",
                                type: "multi",
                                target: ["margin-top"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "margin-right",
                                type: "multi",
                                target: ["margin-right"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "margin-bottom",
                                type: "multi",
                                target: ["margin-bottom"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "margin-left",
                                type: "multi",
                                target: ["margin-left"],
                                values: ['', '0px', '1em'],
                                rowClass: "propertiesSectionHidden"
                            }
                        ]
                    }
                ]
            },
            {
                key: "background",
                className: 'maqPropertySection',
                addCommonPropertiesAtTop: true,
                pageTemplate: [{
                        display: "background-color",
                        type: "background",
                        target: ['background-color'],
                        colorswatch: true
                    },
                    {
                        display: "background-image",
                        type: "background",
                        target: ['background-image'],
                        values: ['', 'none']
                    },
                    {
                        display: "background-repeat",
                        type: "background",
                        values: ['', 'repeat', 'repeat-x', 'repeat-y', 'no-repeat'],
                        target: ['background-repeat']
                    },
                    {
                        display: "background-position",
                        type: "background",
                        target: ['background-position'],
                        values: ['', '0px 0px', '0% 0%', 'left top', 'center center', 'right bottom']
                    },
                    {
                        display: "background-size",
                        type: "background",
                        target: ['background-size'],
                        values: ['', 'auto', 'contain', 'cover', '100%']
                    },
                    {
                        display: "background-origin",
                        type: "background",
                        target: ['background-origin'],
                        values: ['', 'border-box', 'padding-box', 'content-box']
                    },
                    {
                        display: "background-clip",
                        type: "background",
                        target: ['background-clip'],
                        values: ['', 'border-box', 'padding-box', 'content-box']
                    }
                ]
            },
            {

                key: "border",
                className: 'maqPropertySection',
                addCommonPropertiesAtTop: true,
                pageTemplate: [{
                        display: "<b>(border)</b>",
                        type: "multi",
                        target: ['border'],
                        values: ['', 'none', '1px solid black']
                    },
                    {
                        display: "show",
                        type: "combo",
                        values: ['none', 'props', 'sides', 'all'],
                        id: 'properties_show_select',
                        onchange: function (propIndex) {
                            if (typeof propIndex != "number") {
                                return;
                            }
                            var showRange = function (sectionData, first, last, begin, end) {
                                for (var k = first; k <= last; k++) {
                                    var thisProp = sectionData[k];
                                    var thisRow = dojo.byId(thisProp.rowId);
                                    if (k >= begin && k <= end) {
                                        dojo.removeClass(thisRow, "propertiesSectionHidden");
                                    } else {
                                        dojo.addClass(thisRow, "propertiesSectionHidden");
                                    }
                                }
                            };
                            if (this.pageTemplate) {
                                var propObj = this.pageTemplate[propIndex];
                                var value;
                                if (propObj && propObj.id) {
                                    value = dojo.byId(propObj.id).value;
                                }
                                // In some circumstances, value is undefined, which causes exception.
                                // Make sure it is a string.
                                if (dojo.isString(value)) {
                                    if (value === 'none') {
                                        showRange(this.pageTemplate, 2, 20, -1, -1);
                                    } else if (value === 'sides') {
                                        showRange(this.pageTemplate, 2, 20, 2, 5);
                                    } else if (value === 'props') {
                                        showRange(this.pageTemplate, 2, 20, 6, 8);
                                    } else if (value === 'all') {
                                        showRange(this.pageTemplate, 2, 20, 9, 20);
                                    }
                                }
                            }
                        }
                    },
                    {
                        display: "border-top",
                        type: "multi",
                        target: ['border-top'],
                        values: ['', 'none', '1px solid black'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-right",
                        type: "multi",
                        target: ['border-right'],
                        values: ['', 'none', '1px solid black'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-bottom",
                        type: "multi",
                        target: ['border-bottom'],
                        values: ['', 'none', '1px solid black'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-left",
                        type: "multi",
                        target: ['border-left'],
                        values: ['', 'none', '1px solid black'],
                        rowClass: 'propertiesSectionHidden'
                    },


                    {
                        display: "border-width",
                        type: "multi",
                        target: ['border-width'],
                        values: ['', '1px', '1em'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-style",
                        type: "multi",
                        target: ['border-style'],
                        values: ['', 'none', 'solid', 'dotted', 'dashed'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-color",
                        type: "color",
                        target: ['border-color'],
                        rowClass: 'propertiesSectionHidden'
                    },

                    {
                        display: "border-top-width",
                        type: "multi",
                        target: ['border-top-width'],
                        values: ['', '1px', '1em'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-top-style",
                        type: "multi",
                        target: ['border-top-style'],
                        values: ['', 'none', 'solid', 'dotted', 'dashed'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-top-color",
                        type: "color",
                        target: ['border-top-color'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-right-width",
                        type: "multi",
                        target: ['border-right-width'],
                        values: ['', '1px', '1em'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-right-style",
                        type: "multi",
                        target: ['border-right-style'],
                        values: ['', 'none', 'solid', 'dotted', 'dashed'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-right-color",
                        type: "color",
                        target: ['border-right-color'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-bottom-width",
                        type: "multi",
                        target: ['border-bottom-width'],
                        values: ['', '1px', '1em'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-bottom-style",
                        type: "multi",
                        target: ['border-bottom-style'],
                        values: ['', 'none', 'solid', 'dotted', 'dashed'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-bottom-color",
                        type: "color",
                        target: ['border-bottom-color'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-left-width",
                        type: "multi",
                        target: ['border-left-width'],
                        values: ['', '1px', '1em'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-left-style",
                        type: "multi",
                        target: ['border-left-style'],
                        values: ['', 'none', 'solid', 'dotted', 'dashed'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-left-color",
                        type: "color",
                        target: ['border-left-color'],
                        rowClass: 'propertiesSectionHidden'
                    },
                    {
                        display: "border-collapse",
                        type: "combo",
                        target: ['border-collapse'],
                        values: ['', 'separate', 'collapse']
                    },
                    {
                        display: "<b>(border-radius)</b>",
                        type: "multi",
                        target: ['border-radius', '-moz-border-radius'],
                        values: ['', '0px', '6px']
                    },
                    {
                        key: "showDetails",
                        display: "",
                        type: "toggleSection",
                        pageTemplate: [{
                                display: "border-top-left-radius",
                                type: "multi",
                                target: ["border-top-left-radius", '-moz-border-radius-topleft'],
                                values: ['', '0px', '6px'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "border-top-right-radius",
                                type: "multi",
                                target: ['border-top-right-radius', '-moz-border-radius-topright'],
                                values: ['', '0px', '6px'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "border-bottom-right-radius",
                                type: "multi",
                                target: ['border-bottom-right-radius', '-moz-border-radius-bottomright'],
                                values: ['', '0px', '6px'],
                                rowClass: "propertiesSectionHidden"
                            },
                            {
                                display: "border-bottom-left-radius",
                                type: "multi",
                                target: ['border-bottom-left-radius', '-moz-border-radius-bottomleft'],
                                values: ['', '0px', '6px'],
                                rowClass: "propertiesSectionHidden"
                            }
                        ]
                    }
                ]
            },
            {
                key: "fontsAndText",
                className: 'maqPropertySection',
                addCommonPropertiesAtTop: true,
                pageTemplate: [{
                        display: "font",
                        type: "text",
                        target: ["font"]
                    },
                    {
                        display: "font-family",
                        type: "font",
                        target: ["font-family"]
                    },
                    {
                        display: "size",
                        type: "multi",
                        target: ["font-size"],
                        values: ['', '100%', '1em', '10px', '10pt']
                    },
                    {
                        display: "color",
                        type: "color",
                        target: ["color"]
                    },
                    {
                        display: "font-weight",
                        type: "combo",
                        target: ["font-weight"],
                        values: ['', 'normal', 'bold']
                    },
                    {
                        display: "font-style",
                        type: "combo",
                        target: ["font-style"],
                        values: ['', 'normal', 'italic']
                    },
                    {
                        display: "text-decoration",
                        type: "combo",
                        target: ["text-decoration"],
                        values: ['', 'none', 'underline', 'line-through']
                    },
                    {
                        display: "text-align",
                        type: "combo",
                        target: ["text-align"],
                        values: ['', 'left', 'center', 'right', 'justify']
                    },
                    {
                        display: "vertical-align",
                        type: "combo",
                        target: ["vertical-align"],
                        values: ['', 'baseline', 'top', 'middle', 'bottom']
                    },
                    {
                        display: "white-space",
                        type: "combo",
                        target: ['white-space'],
                        values: ['', 'normal', 'nowrap', 'pre', 'pre-line', 'pre-wrap']
                    },
                    {
                        display: "text-indent",
                        type: "multi",
                        target: ["text-indent"],
                        values: ['', '0', '1em', '10px']
                    },
                    {
                        display: "line-height",
                        type: "multi",
                        target: ["line-height"],
                        values: ['', 'normal', '1.2', '120%']
                    }
                ]
            }
            /*,
                         {key: "shapesSVG",
                         className:'maqPropertySection',
                         addCommonPropertiesAtTop:true,
                         pageTemplate:[{display:"stroke", type:"color", target:["stroke"]},
                         {display:"stroke-width", type:"multi", target:["stroke-width"], values:['','1', '2', '3', '4', '5', '10']},
                         {display:"fill", type:"color", target:["fill"]}
                         ]}
                         */
            /*,
             {title:"Animations",
             pageTemplate:[{html:"in progress..."}]},
             {title:"Transistions",
             pageTemplate:[{html:"in progress..."}]},
             {title:"Transforms",
             pageTemplate:[{html:"in progress..."}]},*/

        ],
        getPageTemplate: function () {
            return [

                //Note: the keys here must match the propsect_* values in the supports() functions
                //in the various editors, such as PageEditor.js and ThemeEditor.js

                /*
                 {key: "common",
                 className:'maqPropertySection page_editor_only',
                 addCommonPropertiesAtTop:false,
                 pageTemplate:{html: "<div dojoType='davinci.ve.widgets.CommonProperties'></div>"}},
                 */
                // NOTE: the first section (widgetSpecific) is injected within buildRendering()
                {
                    key: "widgetSpecific",
                    className: 'maqPropertySection page_editor_only',
                    addCommonPropertiesAtTop: false,
                    html: "<div dojoType='davinci.ve.widgets.WidgetProperties'></div>"
                },

                {
                    key: "layout",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    /* startsNewGroup:true, // This flag causes a few extra pixels between this and previous button */
                    pageTemplate: [{
                            display: "width",
                            type: "multi",
                            target: ["width"],
                            values: ['', 'auto', '100%', '200px', '10em']
                        },
                        {
                            display: "height",
                            type: "multi",
                            target: ["height"],
                            values: ['', 'auto', '100%', '200px', '10em']
                        },
                        {
                            html: "&nbsp;"
                        },
                        {
                            key: "showMinMax",
                            display: "&nbsp;&nbsp;&nbsp;",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "min-height",
                                    type: "multi",
                                    target: ["min-height"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "max-height",
                                    type: "multi",
                                    target: ["max-height"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "min-width",
                                    type: "multi",
                                    target: ["min-width"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "max-width",
                                    type: "multi",
                                    target: ["max-width"],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    html: "&nbsp;",
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        },
                        {
                            display: "position",
                            type: "combo",
                            target: ["position"],
                            values: ['', 'absolute', 'fixed', 'relative', 'static']
                        },
                        {
                            display: "left",
                            type: "multi",
                            target: ["left"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "top",
                            type: "multi",
                            target: ["top"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "right",
                            type: "multi",
                            target: ["right"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "bottom",
                            type: "multi",
                            target: ["bottom"],
                            values: ['', '0px', '1em']
                        },
                        {
                            display: "display",
                            type: "combo",
                            target: ["display"],
                            values: ['', 'none', 'block', 'inline', 'inline-block']
                        },
                        {
                            display: "opacity",
                            type: "multi",
                            target: ["opacity"],
                            values: ['0', '0.5', '1.0']
                        },
                        {
                            display: "box-shadow",
                            type: "text",
                            target: ["box-shadow"],
                            value: ['', 'none', '1px 1px rgba(0,0,0,.5)']
                        },
                        {
                            display: "float",
                            type: "combo",
                            target: ["float"],
                            values: ['', 'none', 'left', 'right']
                        },
                        {
                            display: "clear",
                            type: "combo",
                            target: ["clear"],
                            values: ['', 'none', 'left', 'right', 'both']
                        },
                        {
                            display: "overflow",
                            type: "combo",
                            target: ["overflow"],
                            values: ['', 'visible', 'hidden', 'scroll', 'auto']
                        },
                        {
                            display: "z-index",
                            type: "multi",
                            target: ["z-index"],
                            values: ['', 'auto', '0', '1', '100', '-1', '-100']
                        },
                        {
                            display: "box-sizing",
                            type: "combo",
                            target: ['box-sizing', '-webkit-box-sizing', '-ms-box-sizing', '-moz-box-sizing'],
                            values: ['', 'content-box', 'border-box']
                        }
                    ]
                },
                {
                    key: "padding",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "<b>(padding)</b>",
                            type: "multi",
                            target: ["padding"],
                            values: ['', '0px', '1em']
                        },
                        {
                            key: "showtrbl",
                            display: "&nbsp;&nbsp;&nbsp;",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "padding-top",
                                    type: "multi",
                                    target: ["padding-top"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "padding-right",
                                    type: "multi",
                                    target: ["padding-right"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "padding-bottom",
                                    type: "multi",
                                    target: ["padding-bottom"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "padding-left",
                                    type: "multi",
                                    target: ["padding-left"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        }
                    ]
                },
                {
                    key: "margins",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "<b>(margin)</b>",
                            type: "multi",
                            target: ["margin"],
                            values: ['', '0px', '1em']
                        },
                        {
                            key: "showtrbl",
                            display: "&nbsp;&nbsp;&nbsp;",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "margin-top",
                                    type: "multi",
                                    target: ["margin-top"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "margin-right",
                                    type: "multi",
                                    target: ["margin-right"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "margin-bottom",
                                    type: "multi",
                                    target: ["margin-bottom"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "margin-left",
                                    type: "multi",
                                    target: ["margin-left"],
                                    values: ['', '0px', '1em'],
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        }
                    ]
                },
                {
                    key: "background",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "background-color",
                            type: "background",
                            target: ['background-color'],
                            colorswatch: true
                        },
                        {
                            display: "background-image",
                            type: "background",
                            target: ['background-image'],
                            values: ['', 'none']
                        },
                        {
                            display: "background-repeat",
                            type: "background",
                            values: ['', 'repeat', 'repeat-x', 'repeat-y', 'no-repeat'],
                            target: ['background-repeat']
                        },
                        {
                            display: "background-position",
                            type: "background",
                            target: ['background-position'],
                            values: ['', '0px 0px', '0% 0%', 'left top', 'center center', 'right bottom']
                        },
                        {
                            display: "background-size",
                            type: "background",
                            target: ['background-size'],
                            values: ['', 'auto', 'contain', 'cover', '100%']
                        },
                        {
                            display: "background-origin",
                            type: "background",
                            target: ['background-origin'],
                            values: ['', 'border-box', 'padding-box', 'content-box']
                        },
                        {
                            display: "background-clip",
                            type: "background",
                            target: ['background-clip'],
                            values: ['', 'border-box', 'padding-box', 'content-box']
                        }
                    ]
                },
                {
                    key: "border",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "<b>(border)</b>",
                            type: "multi",
                            target: ['border'],
                            values: ['', 'none', '1px solid black']
                        },
                        {
                            display: "show",
                            type: "combo",
                            values: ['none', 'props', 'sides', 'all'],
                            id: 'properties_show_select',
                            onchange: function (propIndex) {
                                if (typeof propIndex != "number") {
                                    return;
                                }
                                var showRange = function (sectionData, first, last, begin, end) {
                                    for (var k = first; k <= last; k++) {
                                        var thisProp = sectionData[k];
                                        var thisRow = dojo.byId(thisProp.rowId);
                                        if (k >= begin && k <= end) {
                                            dojo.removeClass(thisRow, "propertiesSectionHidden");
                                        } else {
                                            dojo.addClass(thisRow, "propertiesSectionHidden");
                                        }
                                    }
                                };
                                if (this.pageTemplate) {
                                    var propObj = this.pageTemplate[propIndex];
                                    var value;
                                    if (propObj && propObj.id) {
                                        value = dojo.byId(propObj.id).value;
                                    }
                                    // In some circumstances, value is undefined, which causes exception.
                                    // Make sure it is a string.
                                    if (dojo.isString(value)) {
                                        if (value === 'none') {
                                            showRange(this.pageTemplate, 2, 20, -1, -1);
                                        } else if (value === 'sides') {
                                            showRange(this.pageTemplate, 2, 20, 2, 5);
                                        } else if (value === 'props') {
                                            showRange(this.pageTemplate, 2, 20, 6, 8);
                                        } else if (value === 'all') {
                                            showRange(this.pageTemplate, 2, 20, 9, 20);
                                        }
                                    }
                                }
                            }
                        },
                        {
                            display: "border-top",
                            type: "multi",
                            target: ['border-top'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right",
                            type: "multi",
                            target: ['border-right'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom",
                            type: "multi",
                            target: ['border-bottom'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left",
                            type: "multi",
                            target: ['border-left'],
                            values: ['', 'none', '1px solid black'],
                            rowClass: 'propertiesSectionHidden'
                        },


                        {
                            display: "border-width",
                            type: "multi",
                            target: ['border-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-style",
                            type: "multi",
                            target: ['border-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-color",
                            type: "color",
                            target: ['border-color'],
                            rowClass: 'propertiesSectionHidden'
                        },

                        {
                            display: "border-top-width",
                            type: "multi",
                            target: ['border-top-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-top-style",
                            type: "multi",
                            target: ['border-top-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-top-color",
                            type: "color",
                            target: ['border-top-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right-width",
                            type: "multi",
                            target: ['border-right-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right-style",
                            type: "multi",
                            target: ['border-right-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-right-color",
                            type: "color",
                            target: ['border-right-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom-width",
                            type: "multi",
                            target: ['border-bottom-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom-style",
                            type: "multi",
                            target: ['border-bottom-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-bottom-color",
                            type: "color",
                            target: ['border-bottom-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left-width",
                            type: "multi",
                            target: ['border-left-width'],
                            values: ['', '1px', '1em'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left-style",
                            type: "multi",
                            target: ['border-left-style'],
                            values: ['', 'none', 'solid', 'dotted', 'dashed'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-left-color",
                            type: "color",
                            target: ['border-left-color'],
                            rowClass: 'propertiesSectionHidden'
                        },
                        {
                            display: "border-collapse",
                            type: "combo",
                            target: ['border-collapse'],
                            values: ['', 'separate', 'collapse']
                        },
                        {
                            display: "<b>(border-radius)</b>",
                            type: "multi",
                            target: ['border-radius', '-moz-border-radius'],
                            values: ['', '0px', '6px']
                        },
                        {
                            key: "showDetails",
                            display: "",
                            type: "toggleSection",
                            pageTemplate: [{
                                    display: "border-top-left-radius",
                                    type: "multi",
                                    target: ["border-top-left-radius", '-moz-border-radius-topleft'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "border-top-right-radius",
                                    type: "multi",
                                    target: ['border-top-right-radius', '-moz-border-radius-topright'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "border-bottom-right-radius",
                                    type: "multi",
                                    target: ['border-bottom-right-radius', '-moz-border-radius-bottomright'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                },
                                {
                                    display: "border-bottom-left-radius",
                                    type: "multi",
                                    target: ['border-bottom-left-radius', '-moz-border-radius-bottomleft'],
                                    values: ['', '0px', '6px'],
                                    rowClass: "propertiesSectionHidden"
                                }
                            ]
                        }
                    ]
                },
                {
                    key: "fontsAndText",
                    className: 'maqPropertySection',
                    addCommonPropertiesAtTop: true,
                    pageTemplate: [{
                            display: "font",
                            type: "text",
                            target: ["font"]
                        },
                        {
                            display: "font-family",
                            type: "font",
                            target: ["font-family"]
                        },
                        {
                            display: "size",
                            type: "multi",
                            target: ["font-size"],
                            values: ['', '100%', '1em', '10px', '10pt']
                        },
                        {
                            display: "color",
                            type: "color",
                            target: ["color"]
                        },
                        {
                            display: "font-weight",
                            type: "combo",
                            target: ["font-weight"],
                            values: ['', 'normal', 'bold']
                        },
                        {
                            display: "font-style",
                            type: "combo",
                            target: ["font-style"],
                            values: ['', 'normal', 'italic']
                        },
                        {
                            display: "text-decoration",
                            type: "combo",
                            target: ["text-decoration"],
                            values: ['', 'none', 'underline', 'line-through']
                        },
                        {
                            display: "text-align",
                            type: "combo",
                            target: ["text-align"],
                            values: ['', 'left', 'center', 'right', 'justify']
                        },
                        {
                            display: "vertical-align",
                            type: "combo",
                            target: ["vertical-align"],
                            values: ['', 'baseline', 'top', 'middle', 'bottom']
                        },
                        {
                            display: "white-space",
                            type: "combo",
                            target: ['white-space'],
                            values: ['', 'normal', 'nowrap', 'pre', 'pre-line', 'pre-wrap']
                        },
                        {
                            display: "text-indent",
                            type: "multi",
                            target: ["text-indent"],
                            values: ['', '0', '1em', '10px']
                        },
                        {
                            display: "line-height",
                            type: "multi",
                            target: ["line-height"],
                            values: ['', 'normal', '1.2', '120%']
                        }
                    ]
                }
                /*,
                                 {key: "shapesSVG",
                                 className:'maqPropertySection',
                                 addCommonPropertiesAtTop:true,
                                 pageTemplate:[{display:"stroke", type:"color", target:["stroke"]},
                                 {display:"stroke-width", type:"multi", target:["stroke-width"], values:['','1', '2', '3', '4', '5', '10']},
                                 {display:"fill", type:"color", target:["fill"]}
                                 ]}
                                 */
                /*,
                 {title:"Animations",
                 pageTemplate:[{html:"in progress..."}]},
                 {title:"Transistions",
                 pageTemplate:[{html:"in progress..."}]},
                 {title:"Transforms",
                 pageTemplate:[{html:"in progress..."}]},*/

            ];
        },
        buildRendering: function () {
            this.domNode = dojo.doc.createElement("div");
            dojo.addClass(this.domNode, 'propertiesContent');
            var template = "";
            template += this._titleBarDiv;
            template += "<div class='propertiesToolBar' dojoType='davinci.ve.widgets.WidgetToolBar'></div>";
            template += "<div dojoType='davinci.ve.widgets.WidgetProperties'></div>";
            template += "<div class='propScrollableArea'>";
            template += "<table class='propRootDetailsContainer'>";
            template += "<tr>";
            template += "<td class='propPaletteRoot'>";
            //run through pageTemplate to insert localized strings
            var langObj = veNls;

            this.pageTemplate = this.getPageTemplate();
            for (var i = 0; i < this.pageTemplate.length; i++) {
                this.pageTemplate[i].title = langObj[this.pageTemplate[i].key] ? langObj[this.pageTemplate[i].key] : "Key not found";
                if (this.pageTemplate[i].pageTemplate) {
                    for (var j = 0; j < this.pageTemplate[i].pageTemplate.length; j++) {
                        if (this.pageTemplate[i].pageTemplate[j].key) {
                            this.pageTemplate[i].pageTemplate[j].display += langObj[this.pageTemplate[i].pageTemplate[j].key] ? langObj[this.pageTemplate[i].pageTemplate[j].key] : "Key not found";
                        }
                    }
                }
            }
            this.domNode.innerHTML = template;

            this.inherited(arguments);
        },
        _widgetValuesChanged: function (event) {
            var currentPropSection = this._currentPropSection;
            if (currentPropSection) {
                var found = false;
                for (var propSectionIndex = 0; propSectionIndex < this.pageTemplate.length; propSectionIndex++) {
                    if (this.pageTemplate[propSectionIndex].key == currentPropSection) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    var visibleCascade = this._getVisibleCascade(propSectionIndex);
                    for (var i = 0; i < visibleCascade.length; i++)
                        visibleCascade[i]._widgetValuesChanged(event);
                }
            }
        },
        _getVisibleCascade: function (targetIndex) {
            if (targetIndex)
                return this.pageTemplate[targetIndex]['cascade'];
            var visibleCascade = [];
            var currentPropSection = this._currentPropSection;
            if (currentPropSection) {
                for (var i = 0; i < this.pageTemplate.length; i++) {
                    if (this.pageTemplate[i].key == currentPropSection) {
                        visibleCascade = visibleCascade.concat(this.pageTemplate[i]['cascade']);
                        break;
                    }
                }
            }
            return visibleCascade;
        },
        _updatePaletteValues: function (widgets) {
            if (!this._editor) {
                console.error('have no editor', widgets);
                return;
            }
            if (!lang.isArray(widgets)) {
                widgets = [widgets];
            }

            var widget = widgets[0];
            this._widget = widget;
            this._subwidget = widget && widget.subwidget;
            this.setReadOnly(!(this._widget || this._subwidget));
            var visibleCascade = this._getVisibleCascade();
            for (var i = 0; i < visibleCascade.length; i++) {
                visibleCascade[i]._widgetSelectionChanged(widgets);
            }
        },
        _widgetSelectionChanged: function (changeEvent) {
            this._updatePaletteValues(changeEvent);
        },
        _stateChanged: function () {
            var widgets = this._widget ? [this._widget] : [];
            this._updatePaletteValues(widgets);
        },
        _widgetPropertiesChanged: function (widgets) {
            /* Check to see that this is for the same widget
             * Some widget like Tree update the DataStore but not the widget it's self from smar input
             * Cant check the id, it may change.
             */
            if (!this._widget) {
                console.error('have no widget');
            }
            if ((!this._widget) || (this._widget.type === widgets[0].type)) {
                this._updatePaletteValues(widgets);
            }
        },
        _titlePaneOpen: function (index) {
            var visibleCascade = this._getVisibleCascade(index);
            for (var i = 0; i < visibleCascade.length; i++) {
                visibleCascade[i]._editorSelected({
                    'editor': this._editor
                });
            }

        },
        startup: function () {
            this.domNode.style.height = "100%";
            /*this._editor = Runtime.currentEditor;*/
            this.inherited(arguments);
            //FIXME: Do we need this?
            // Install any onchange handlers for specific input elements
            for (var i = 0; i < this.pageTemplate.length; i++) {
                var section = this.pageTemplate[i];
                if (section.pageTemplate) {
                    for (var j = 0; j < section.pageTemplate.length; j++) {
                        var propData = section.pageTemplate[j];
                        if (propData.onchange && propData.id) {
                            // onchange function gets invoked with "this" pointing to pageTemplate[i]
                            // and receives parameter j
                            dojo.connect(dojo.byId(propData.id), "onchange", dojo.hitch(section, propData.onchange, j));
                        }
                    }
                }
            }
            //FIXME: Do we need this?
            for (var v = 0; v < this.pageTemplate.length; v++) {
                this.pageTemplate[v]['cascade'] = [];
            }
            this.setReadOnly(true);
            this.onEditorSelected();
            this.subscribe("/davinci/ui/widgetValuesChanged", dojo.hitch(this, this._widgetValuesChanged));
            this.subscribe("/davinci/ui/widgetPropertiesChanged", dojo.hitch(this, this._widgetPropertiesChanged));
            //Don't need to subscribe here. ViewLite already does it for us.
            this.subscribe("/davinci/ui/widgetSelected", dojo.hitch(this, this._widgetSelectionChanged));
        },
        destroy: function () {
            for (var v = 0; v < this.pageTemplate.length; v++) {
                var page = this.pageTemplate[v]['pageTemplate'];
                if (!page) {
                    continue;
                }
                for (var i = 0; i < page.length; i++) {
                    var widget = page[i]['widget'];
                    if (widget) {
                        //widget.set("readOnly", isReadOnly);
                        if (widget._destroyed) {
                            page[i]['widget'] = null;
                        }
                    } else {
                        var node = page[i].domNode;
                    }
                }
            }
        },
        setReadOnly: function (isReadOnly) {
            for (var v = 0; v < this.pageTemplate.length; v++) {
                var page = this.pageTemplate[v]['pageTemplate'];
                if (!page)
                    continue;
                for (var i = 0; i < page.length; i++) {
                    var widget = page[i]['widget'];
                    if (widget)
                        widget.set("readOnly", isReadOnly);
                    else {
                        var node = page[i].domNode;
                        if (node)
                            dojo.attr(node, "disabled", isReadOnly);
                    }
                }
            }
        },
        _modelEntryById: function (id) {
            for (var v = 0; v < this.pageTemplate.length; v++) {
                var page = this.pageTemplate[v]['pageTemplate'];
                if (!page)
                    continue;
                for (var i = 0; i < page.length; i++) {
                    var sectionId = page[i]['id'];
                    if (id == sectionId) {
                        return page[i];
                    }
                }
            }
        },
        _editorSelected: function (editorChange) {
            this._editor = editorChange.editor;
            this.onEditorSelected(this._editor);
            var parentTabContainer = this.getParent();
            var selectedChild = parentTabContainer.selectedChildWidget;
            var updatedSelectedChild = false;
            var allSections = dojo.query('.maqPropertySection', parentTabContainer.domNode);
            if (this._editor) {
                if (this._editor.declaredClass != 'davinci.ve.PageEditor' &&
                    this._editor.declaredClass != 'davinci.ve.themeEditor.ThemeEditor') {
                    //Hide all sections
                    allSections.forEach(function (section) {
                        var contentPane = dijit.byNode(section);
                        if (contentPane.controlButton) {
                            contentPane.controlButton.domNode.style.display = 'none';
                        }
                        if (contentPane == selectedChild) {
                            updatedSelectedChild = true;
                        }
                    });
                } else {
                    //Show all sections
                    allSections.forEach(function (section) {
                        var contentPane = dijit.byNode(section);
                        if (contentPane.controlButton) {
                            contentPane.controlButton.domNode.style.display = '';
                        }
                    });
                    if (this._editor.declaredClass == 'davinci.ve.themeEditor.ThemeEditor') {
                        //Hide sections intended only for page editor
                        var pageEditorOnlySections = dojo.query('.page_editor_only', parentTabContainer.domNode);
                        pageEditorOnlySections.forEach(function (section) {
                            var contentPane = dijit.byNode(section);
                            if (contentPane.controlButton) {
                                contentPane.controlButton.domNode.style.display = 'none';
                            } else {

                            }
                            if (contentPane == selectedChild) {
                                updatedSelectedChild = true;
                            }
                        });
                    }
                }
            } else {
                //No editor, so bring back to default state by showing all sections
                allSections.forEach(function (section) {
                    var contentPane = dijit.byNode(section);
                    contentPane.controlButton.domNode.style.display = '';
                });
                updatedSelectedChild = true;
            }
            if (updatedSelectedChild) {
                this._selectFirstVisibleTab();
            }
        },
        onEditorSelected: function () {
            //we should clear selected widget from the old editor
            this._widget = null;
            this._subWidget = null;
            /* add the editors ID to the top of the properties pallete so you can target CSS rules based on editor */
            if (this._oldClassName) {
                if (this.domNode) {
                    dojo.removeClass(this.domNode.parentNode.parentNode, this._oldClassName); //remove the class from the  tab container
                } else {
                    console.error('have no dom node !');
                }
            }

            if (!this._editor) {
                return;
            }
            if (this._editor && this._editor.editorID) {
                this._oldClassName = this._editor.editorID.replace(/\./g, "_");
                if (this.domNode) {
                    dojo.addClass(this.domNode.parentNode.parentNode, this._oldClassName); //put the class on the  tab container
                } else {
                    console.error('have no dom node !');
                }
            }

            if (!this.domNode) {
                console.error('have no dom node !');
                return;
            }

            //FIXME: I'm pretty sure at least some of the code below is no longer necessary
            // Hide or show the various section buttons on the root pane
            var currentPropSection = this._currentPropSection;
            var sectionButtons = dojo.query(".propSectionButton", this.domNode);
            for (var i = 0; i < sectionButtons.length; i++) {
                var sectionButton = sectionButtons[i];
                if (this._editor && this._editor.supports && this._editor.supports('propsect_' + this.pageTemplate[i].key)) {
                    dojo.removeClass(sectionButton, 'dijitHidden');
                } else {
                    dojo.addClass(sectionButton, 'dijitHidden');
                    if (currentPropSection == this.pageTemplate[i].key) {
                        // If a hidden section is currently showing, then
                        // jump to Property palette's root view.
                        HTMLStringUtil.showRoot();
                    }
                }
            }
            //ENDOF FIXME COMMENT
            var visibleCascade = [];
            for (var i = 0; i < this.pageTemplate.length; i++) {
                var cascade = this.pageTemplate[i]['cascade'];
                if (cascade) {
                    visibleCascade = visibleCascade.concat(cascade);
                } else {
                    console.log("no cascade found");
                }
            }
            for (var i = 0; i < visibleCascade.length; i++) {
                var cascade = visibleCascade[i];
                if (cascade._editorSelected) {
                    cascade._editorSelected({
                        'editor': this._editor
                    });
                }
            }
        },
        _destroyContent: function () {
            var containerNode = (this.containerNode || this.domNode);
            dojo.forEach(dojo.query("[widgetId]", containerNode).map(dijit.byNode), function (w) {
                w.destroy();
            });
            while (containerNode.firstChild) {
                dojo._destroyElement(containerNode.firstChild);
            }
            dojo.forEach(this._tooltips, function (t) {
                t.destroy();
            });
            this._tooltips = undefined;
        },
        sectionTitleFromKey: function (key) {
            for (var i = 0; i < this.pageTemplate.length; i++) {
                if (this.pageTemplate[i].key == key) {
                    return this.pageTemplate[i].title;
                }
            }
        },
        _initialPerspectiveReady: function () {
            var cp = null;
            if (!this._alreadySplitIntoMultipleTabs) {
                var parentTabContainer = this.tabContainer;
                dojo.addClass(parentTabContainer.domNode, 'propRootDetailsContainer');
                dojo.addClass(parentTabContainer.domNode, 'propertiesContent');
                for (var i = 0; i < this.pageTemplate.length; i++) {
                    var key = this.pageTemplate[i].key;
                    var title = this.pageTemplate[i].title;
                    var className = this.pageTemplate[i].className;
                    if (!className) {
                        className = '';
                    }
                    var topContent = this._titleBarDiv;
                    topContent += "<div class='propertiesToolBar' dojoType='davinci.ve.widgets.WidgetToolBar'></div>";
                    topContent += "<div class='cascadeBackButtonDiv'><button onclick='davinci.ve.widgets.HTMLStringUtil.showSection(\"" + key + "\",\"" + title + "\")'>" + title + " " + veNls.properties + "</button></div>";
                    var paneContent = HTMLStringUtil.generateTemplate(this.pageTemplate[i]);
                    var content = topContent + paneContent;
                    if (i == 0) {
                        cp = this;
                        cp.set('title', title);
                        dojo.addClass(cp.domNode, className);
                    } else {


                        var contentPane = parentTabContainer.createTab(title, null, null, null, {
                            'class': className,
                            delegate: this
                        });
                        contentPane._maqPropGroup = this.pageTemplate[i].key;
                        //if(i==1){
                        cp = utils.addWidget(ContentPane, {
                            title: title,
                            content: content,
                            'class': className,
                            delegate: this,
                            style: "with:inherit;height:inherit"
                            //resizeToParent:true
                        }, null, contentPane, false);
                        contentPane.cp = cp;
                        contentPane._maqPropGroup = this.pageTemplate[i].key;
                        contentPane.add(cp, null, false);
                        // }
                    }

                    if (cp) {
                        cp._maqPropGroup = this.pageTemplate[i].key;
                    }

                    //FIXME: temp hack
                    var closeBoxNodes = dojo.query('.paletteCloseBox', cp.domNode);
                    if (closeBoxNodes.length > 0) {
                        var closeBox = closeBoxNodes[0];
                        dojo.connect(closeBox, 'click', this, function (event) {
                            davinci.Workbench.collapsePaletteContainer(event.currentTarget);
                        });
                    }
                }

                for (var v = 0; v < this.pageTemplate.length; v++) {
                    this.pageTemplate[v]['cascade'] = [];
                    var page = this.pageTemplate[v]['pageTemplate'];
                    if (!page)
                        continue;
                    for (var i = 0; i < page.length; i++) {
                        var id = page[i]['id'];
                        var widget = dijit.byId(id);
                        if (widget) {
                            page[i]['widget'] = widget;
                        } else {
                            widget = dojo.byId(id);
                            if (widget)
                                page[i]['domNode'] = widget;
                        }
                    }
                    var sectionId = this.pageTemplate[v].id;
                    var nodeList = dojo.query("#" + sectionId + " .CascadeTop");
                    nodeList.forEach(function (target) {
                        return function (p) {
                            var cascade = dijit.byId(p.id);
                            target['cascade'].push(cascade);
                        };
                    }(this.pageTemplate[v]));
                }
                var thiz = this;
                parentTabContainer._on('selectChild', function (tab) {
                    if (tab._maqPropGroup) {
                        this._currentPropSection = tab._maqPropGroup;
                        var context = (this._editor && this._editor.getContext) ? this._editor.getContext() : null;
                        var selection = (context && context.getSelection) ? context.getSelection() : [];
                        this._updatePaletteValues(selection);
                        HTMLStringUtil._initSection(this._currentPropSection);
                        parentTabContainer.resize();
                        tab.resize();
                        tab.cp.resize();
                    }
                }.bind(this));
                parentTabContainer.selectChild(0);
                this._alreadySplitIntoMultipleTabs = true;
            }

        },
        _workbenchReady: function () {
            this._updateToolBars();
        },
        _updateToolBars: function () {
            var propertyToolbarContainers = dojo.query('.propertiesToolBar');
            propertyToolbarContainers.forEach(function (container) {
                if (this._editor && this._editor.declaredClass == 'davinci.ve.PageEditor') {
                    dojo.removeClass(container, "dijitHidden");
                    container.style.display = '';
                } else {
                    dojo.addClass(container, 'dijitHidden');
                }
            }.bind(this));
        },
        _selectFirstVisibleTab: function () {
            var parentTabContainer = this.getParent();
            var children = parentTabContainer.getChildren();
            for (var i = 0; i < children.length; i++) {
                var cp = children[i];
                if (cp.controlButton.domNode.style.display != 'none') {
                    // This flag prevents Workbench.js logic from triggering expand/collapse
                    // logic based on selectChild() event
                    parentTabContainer._maqDontExpandCollapse = true;
                    parentTabContainer.selectChild(cp);
                    delete parentTabContainer._maqDontExpandCollapse;
                    break;
                }
            }
        }
    });
    if (ctx) {
        var parent = TestUtils.createTab(null, null, module.id);
        var widget = dcl(_Widget, {
            editNode: null,
            widgetNode: null,
            last: null,
            styleView: null,
            previewExtraStyle: 'max-width:300px;max-height:100px;height:inherit;width:inherit;',
            connectEditBox: function (editBox, onChanged) {
                var thiz = this;
                editBox._on("change", function (value) {
                    if (onChanged) {
                        onChanged(value);
                    }
                    var _valueNow = utils.toString(thiz.userData['value']);

                    if (value === _valueNow) {
                        return;
                    }
                    thiz.userData.changed = true;
                    thiz.userData.active = true;
                    if (thiz.serialize) {
                        value = thiz.serialize();
                    }
                    utils.setCIValueByField(thiz.userData, "value", value);

                    factory.publish(types.EVENTS.ON_CI_UPDATE, {
                        owner: thiz.delegate || thiz.owner,
                        ci: thiz.userData,
                        newValue: value,
                        storeItem: thiz.storeItem
                    }, thiz);
                });

            },
            templateString: '<div>' +
                "<div attachTo='editNode' style='width: 100%;'></div>" +
                "<div id='div_5' attachTo='widgetNode' style='width: 300px;margin:8px;height:100px;'>" +
                "<span class='dijitReset' style='text-align: center'>Some Text</span>" +
                "</div>" +
                "<div attachTo='last' style='height:400px'></div>" +
                '</div>',
            getStyleArray: function () {
                var value = utils.toString(this.userData['value']);
                var _values = value.split(';');
                var result = [];
                for (var i = 0; i < _values.length; i++) {
                    var obj = _values[i];
                    if (!obj || obj.length == 0 || !obj.split) {
                        continue;
                    }
                    var keyVal = obj.split(':');
                    if (!keyVal || !keyVal.length) {
                        continue;
                    }
                    var _prop = {};
                    _prop[keyVal[0]] = keyVal[1];
                    result.push(_prop);
                }
                return result;
            },
            onChangeStyleProperty: function (property, value) {
                if (!property || property === 'undefined') {
                    return;
                }
                if (!value || !value.length) {
                    delete this.values[property];
                    this._onChanged();
                    return;
                }
                this.values[property] = value;
                this._onChanged();
            },
            onStyleValuesChange: function (evt) {
                if (!evt || !evt.cascade || !evt.values) {
                    return;
                }
                if (this.domNode && evt.cascade.domNode && utils.isDescendant(this.domNode, evt.cascade.domNode)) {
                    this.onChangeStyleProperty(evt.cascade.target[0], evt.cascade._value);
                }
            },
            _toStyleString: function (values) {
                var _values = [];
                for (var prop in values) {
                    if (values[prop] !== 'Remove Value') {
                        _values.push(prop + ':' + values[prop]);
                    }
                }
                return _values.join(';') + ';';
            },
            _toObject: function (styleString) {
                var _result = {};
                var _values = styleString.split(';');
                for (var i = 0; i < _values.length; i++) {
                    var obj = _values[i];
                    if (!obj || obj.length == 0 || !obj.split) {
                        continue;
                    }
                    var keyVal = obj.split(':');
                    if (!keyVal || !keyVal.length) {
                        continue;
                    }
                    _result[keyVal[0]] = keyVal[1];
                }
                return _result;
            },
            createFakeContext: function () {
                var self = this;
                var ctx = {
                    editor: self,
                    blockChange: function () {},
                    getAppCssRelativeFile: function () {
                        return '';
                    },
                    getThemeMeta: function () {
                        return null;
                    },
                    getStyleAttributeValues: function (widget) {
                        return self.getStyleArray();
                    },
                    getSelection: function () {
                        return self._widgetSelection;
                    },
                    _cssCache: {},
                    _getCssFiles: function () {
                        return null;
                    },
                    getModel: function () {
                        return {
                            getRule: function () {
                                return [];
                            }
                        }
                    },
                    model: {
                        getMatchingRules: function () {
                            return {
                                rules: []
                            }
                        }
                    }
                };

                return ctx;
            },
            createFakeWidget: function (ctx) {
                var widgetEventData = {
                    owner: this,
                    isFake: true,
                    domNode: this.widgetNode,
                    type: 'html.div',
                    _srcElement: {
                        tag: 'div'
                    },
                    isHtmlWidget: true,
                    isWidget: false,
                    _skipAttrs: [],
                    _edit_context: {},
                    id: "div_5",
                    _params: null,
                    children: [],
                    getClassNames: function () {
                        return null;
                    },
                    getContext: function () {
                        return ctx;
                    },
                    metadata: {
                        '$ownproperty': {},
                        '$src': 'no source',
                        content: '<div></div>',
                        "description": {
                            "type": "text/html",
                            "value": "<p>The div element has no special meaning at all. It represents its children.</p>"
                        },
                        version: '1.0',
                        "id": "http://www.w3.org/html/div",
                        "name": "html.div",
                        "spec": "1.0",
                        "title": {
                            "type": "text/html",
                            "value": "<p>HTML block-level container element.</p>"
                        }

                    }
                };
                return widgetEventData;
            },
            _createEditBox: function () {
                var thiz = this;
                var value = utils.toString(this.userData['value']);
                this.editBox = factory.createValidationTextBox(this.editNode, "", "Value", value, this.filePathValidator, this.delegate, '');
                this.connectEditBox(this.editBox, function (value) {
                    if (value && value.length) {
                        thiz.values = thiz._toObject(value);
                        domAttr.set(thiz.widgetNode, 'style', value + thiz.previewExtraStyle);
                        thiz.styleView._updatePaletteValues(thiz._widgetSelection);
                    }
                });
            },
            createStyleView: function () {
                var pane = utils.addWidget(ContentPane, {
                    title: 'Widget Properties',
                    closable: false,
                    style: 'padding:0px;min-height:400px;'
                }, this, this.last, true, 'ui-widget-content');
                this.stylePane = pane;

                var tabContainer = utils.addWidget(_TabContainer, {
                    region: 'center',
                    id: 'palette-tabcontainer-right-top',
                    tabPosition: 'right' + '-h',
                    tabStrip: false,
                    'class': "davinciPalette davinciTopPalette widget",
                    style: 'height:inherit;width:inherit;',
                    splitter: false,
                    direction: 'right',
                    resizeToParent: true
                }, null, pane, true);
                domClass.add(tabContainer.domNode, "davinciPalette davinciTopPalette");

                tabContainer.startup();

                this.tabContainer = tabContainer;
                var _re = require;
                var thiz = this;
                var styleView = null;
                try {
                    styleView = new StyleView({
                        toolbarDiv: pane.containerNode,
                        //_editor: this.editor,
                        parentContainer: tabContainer,
                        tabContainer: tabContainer
                    });
                } catch (e) {
                    console.error('html_style_view crash ' + e);

                }
                this.add(styleView);
                var _ctx = this.createFakeContext();

                this.supports = function () {
                    return true;
                };
                var widgetEventData = this.createFakeWidget(_ctx);
                this._widgetSelection = [widgetEventData];
                styleView._initialPerspectiveReady();
                styleView._editor = this;
                this.editorID = 'davinci.ve.HTMLPageEditor';
                styleView._widgets = [widgetEventData];
                styleView._widget = widgetEventData;
                this.getContext = function () {
                    return _ctx;
                }
                this.getSelection = function () {
                    return thiz._widgetSelection;
                }
                this._emit("/davinci/ui/widgetSelected", this._widgetSelection);
                return styleView;
            },
            _onChanged: function () {
                var _value = this._toStyleString(this.values);
                if (_value === ";") {
                    _value = '';
                }
                this.userData.changed = true;
                this.userData.active = true;
                this.editBox.set('value', _value);
                var previewVal = _value + this.previewExtraStyle;
                domAttr.set(this.widgetNode, 'style', previewVal);
                this.userData.value = _value;
                console.log('onchanged', {
                    value: _value,
                    userData: this.userData
                })
            },
            startup: function () {
                this.userData = {
                    value: "padding:1em;width:200px;color:white;"
                }
                var value = this.userData.value;
                this.values = this._toObject(value);
                this._createEditBox();
                this.subscribe('/davinci/ui/styleValuesChange', this.onStyleValuesChange);
                this.styleView = this.createStyleView();
            }
        });
        utils.addWidget(widget, null, null, parent, true);
        return declare('a', null, {});
    }
    return Grid;
});