/** @module xideve/views/VisualEditorPalette **/
define("xideve/views/VisualEditorPalette", [
    "dcl/dcl",
    'xide/utils',
    'xide/factory',
    "./Palette"
], function (dcl,utils, factory,Palette) {
    
    /**
     *
     * @mixin module:xideve/views/VisualEditorPalette
     * @lends module:xideve/views/VisualEditor
     */
    return dcl(null, {
        declaredClass:"xideve/views/VisualEditorPalette",
        _cPalette:null,
        _cBPalettePane:null,
        _createPaletteSection:function(name,component,compType){
            var _section = {
                id:'ContainersSection_Dojo',
                iconBase64:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAg9JREFUeNp8ks1rE0EYxn9bE/xqikI0lITiQW+VImoONmCQtoonUbAglFDEUtlLxdo/wUNRD0Iw1kOIEmj6D9ir2FRQi4j0lILQJoimRfJB9mNmOx42iWsT+8Iw7DwPv32feUdTSuGtyeQKaT3GZHJlGEgA40AOyKT1WL6lt0rbC2hC9IFg7/a1c5HAib5Dc+WqOb/8pVjb3K4H03os6fVqXToYHgj2RiYun34KhD1S6c27jYeb2/ViWo/lW4c9dFYiPtgfMIQTNoRDw3YwbIeGcMLxwf5AM1a7fF0A436fL15pSNAUGqCUhqYp/P4Dc0AImNoPkNvaqX8MHDn4SkOBiwA0Kg1rHoh6zd0iZD4VyrWaKUpVQ1I1bSqGpGKI0udCuQZk/gu4+XhZpfXY2amRM88vnjq2vlMz7xn2biF41L8+FAl8f5a4cFw4u/mrj3KqK0BYNkDqzv3UyR6N32uFn9H334qhtY1fhyf0lzEgVd76oQDaEKVUe43NLirP97Rnn97rG5tdRCnldrCw9EFd0TNKNEwARmayCkg1tRdAavRBVo3OZAFwpMQR8m+E12+/YjcMzW4CpBsFF7AKgG1aCNM9l4bd9vjc7FZ73gDCtDpGIw27Oc5/9R6AuzeiSFsqRwjX7DH0hYK0fiKbHSSuDyEtq/MSu63zt56o/fRuL5HWxS4srTJ1+xL71Z8BAL+kU+9a36VRAAAAAElFTkSuQmCC",
                includes:['RunScript',"BorderContainer"], //BorderContainer all widgets belonging to 'ContainersSection_Dojo'
                items:[], // filled with _subsectionItem below
                name:name,//sub folder name
                preset:component.preset, //ref up link
                presetId:compType
            };
            component.subsections.push(_section);
            return _section;
        },
        _createPaletteComponent:function(name,compType){
            var _component = {
                id:'containers',
                name:name,
                preset:{
                    collections:[{id:'xblox',show:true},{id:'delite',show:true},{id:'dijit',show:true},{id:'dojo',show:true}], //contains items {id:dijit,show:true} ...seems the package filter
                    sections:'$'  +compType + 'Sections'
                },
                presetId:compType,
                subsections:[] // filled with '_subsection' below
            };
            return _component;
        },
        _createPaletteBlockItem:function(block,section){
            var name = block.shareTitle;

            var getDescriptorString = function(key) {
                //console.log('get key : ' + key);
                return name;
            };

            var _subsectionItem = {
                '$library':{
                    _maqGetString:getDescriptorString,
                    widgets:[],
                    collections:{}
                }, // meta-info stuff
                category:'containers',
                allowedChild:'', //dijit/layout/ContentPane cool!
                collection:'xblox',//makes sense
                description:'', // comes from block.description or shareDescription
                iconBase64:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAPJJREFUeNpi/P//P8PCgzf/bzv3nIFU4GUkycACYoA01wYZkGxA87oLEAP+//vHsPvWa5IN+P/vP8QAbxNphi0nHzL8R5UGYkY4jxEqgizvay4HMWDj0QcMs4qcgUKMqKqg+hmBYv8ZkYyDmp3WvwdiwL+/f8HiohzMRDv/zY+/DP9+/2VgAhvw5y8DOQBkMdQF/xh+Ac14/u0P3AfIfmaEOvs/VJAR6lOQAYywdLBq5zWSXRDmroVIB53pjuSng98/fpKVDkD6wAb4WykAXfGUZANA+qiTlKnihVU7r4DjC2f0IaVIWFSGumlBopESABBgAGY0fipQlZCHAAAAAElFTkSuQmCC",
                iconLarge:"dojo/1.8/resources/imagesLarge/AccordionContainer.png",
                iconLargeBase64:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAANxJREFUeNrs17EKwjAQxvGoIZPdi0N3wcFR6NhNn8Wn8VnqZMeimw6ikyCdSn0BwTSxRRzcuvkvXCD7j+9y5G6Q7vaKd3Rzl8kCZdpmh5ZVVQ9iWkEwJrK898LqzHIOyWrykrQ6s6y1NNZQIQ+6E52k1fe0jpc7jnVTE2gnruYhypSeypaljUHOW0YxO5HJ8kyWkiL2Py3m26odlOWkiH1fMWpmWs9XnV9LCCieht/1ddT+1esk+rtpkxUfDHQhE5awhCUsYQlLWGiW/hkqaKx4FuHSys8FrYhvAQYAB2RPPyLZy+IAAAAASUVORK5CYII=",
                iconHTML:block.getBlockIcon(),
                initialSize:'auto',
                inlineEdit:"",// ./dijit/layout/AccordionContainerInput : cool!
                name:name, // should come from block.name or similar
                properties:{
                    style:"position:relative",
                    ignorePosition:true,
                    blockid:block.id,
                    scopeid:block.scope.id,
                    targetevent:'click',
                    enabled:true,
                    stop:true,
                    script:block.name + '(' + block.shareTitle + ')',
                    block:block.scope.mount.replace('/','') + '://'+ block.scope.path.replace('./',''),
                    sourceevent:"onDriverVariableChanged",
                    sourceeventvaluepath:"item.value",
                    sourceeventnamepath:"item.title",
                    "bidirectional":true,
                    "targetvariable":"Volume"

                },
                section:{}, //uplink to _subsection
                tags:"Scripts",
                type:'xblox/RunScript', //declared class ?

                widgetClass:'dijit', //?

                children:[], //this seems to hold example widgets (2 content panes for 'Accordion)
                helper:'',//"./dijit/layout/LayoutContainerHelper"
                ignoreMeta:true,
                forceShow:true,
                userData:{
                    block:block,
                    widgetClass:'dijit'
                }
            };

            _subsectionItem.$library.widgets.push(_subsectionItem);
            _subsectionItem.$library.collections.dijit = {name:'Dijit'};
            _subsectionItem.$library.collections.xblox = {name:'XBLOX'};
            _subsectionItem.$library.collections.delite = {name:'DELITE'};

/*
            _subsectionItem.properties.userData = {
                block:block,
                delegate:this
            };
            */

            section.items.push(_subsectionItem);

            return _subsectionItem;
        },
        /**
         *
         * @param scope {xblox/model/Scope}
         * @private
         */
        _blockScopeToPalette:function(scope,compType){

            var store = scope.getBlockStore();
            var allGroups = scope.allGroups();

            //console.log('all groups',allGroups);

            var component = this._createPaletteComponent(scope.path,compType);


            for (var i = 0; i < allGroups.length; i++) {
                //pick blocks per group && sharing enabled
                var groupItemsShared = store.query({
                    group: allGroups[i],
                    shareTitle:/\S+/ //not empty
                });

                if(groupItemsShared.length){

                    var section = this._createPaletteSection(allGroups[i],component,compType);
                    for (var j = 0; j < groupItemsShared.length; j++) {
                        var block = groupItemsShared[j];
                        var item = this._createPaletteBlockItem(block,section);
                    }
                    //console.log('items' , groupItemsShared);

                }else{
                    continue;
                }
            }
            return component;
        },
        _buildBlockPalette:function(params){
            var main = this.getMainView();
            var left = this.getLeftContainer();
            //prepare
            if(!this._cBPalettePane){

                this._cBPalettePane = factory.createPane('Behaviours','fa-cube',left,{
                    parentContainer:left
                });

            }
            if(this._cPalette){
                utils.destroy(this._cPalette);
            }
            var _ctrArgs = {};
            var palette = utils.addWidget(Palette,_ctrArgs,this,this._cBPalettePane,true);
            this._cPalette = palette;
            palette._init();
            var _compType = params.context.getCompType();
            var _component = {
                id:'containers',
                name:'min.xblox',
                preset:{
                    collections:[{id:'xblox',show:true},{id:'delite',show:true},{id:'dijit',show:true},{id:'dojo',show:true}], //contains items {id:dijit,show:true} ...seems the package filter
                    sections:  '$' + _compType  + 'Sections'
                },
                presetId:_compType,
                subsections:[] // filled with '_subsection' below
            };
            var _subsection = {
                id:'ContainersSection_Dojo',
                iconBase64:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAg9JREFUeNp8ks1rE0EYxn9bE/xqikI0lITiQW+VImoONmCQtoonUbAglFDEUtlLxdo/wUNRD0Iw1kOIEmj6D9ir2FRQi4j0lILQJoimRfJB9mNmOx42iWsT+8Iw7DwPv32feUdTSuGtyeQKaT3GZHJlGEgA40AOyKT1WL6lt0rbC2hC9IFg7/a1c5HAib5Dc+WqOb/8pVjb3K4H03os6fVqXToYHgj2RiYun34KhD1S6c27jYeb2/ViWo/lW4c9dFYiPtgfMIQTNoRDw3YwbIeGcMLxwf5AM1a7fF0A436fL15pSNAUGqCUhqYp/P4Dc0AImNoPkNvaqX8MHDn4SkOBiwA0Kg1rHoh6zd0iZD4VyrWaKUpVQ1I1bSqGpGKI0udCuQZk/gu4+XhZpfXY2amRM88vnjq2vlMz7xn2biF41L8+FAl8f5a4cFw4u/mrj3KqK0BYNkDqzv3UyR6N32uFn9H334qhtY1fhyf0lzEgVd76oQDaEKVUe43NLirP97Rnn97rG5tdRCnldrCw9EFd0TNKNEwARmayCkg1tRdAavRBVo3OZAFwpMQR8m+E12+/YjcMzW4CpBsFF7AKgG1aCNM9l4bd9vjc7FZ73gDCtDpGIw27Oc5/9R6AuzeiSFsqRwjX7DH0hYK0fiKbHSSuDyEtq/MSu63zt56o/fRuL5HWxS4srTJ1+xL71Z8BAL+kU+9a36VRAAAAAElFTkSuQmCC",
                includes:[""], //BorderContainer all widgets belonging to 'ContainersSection_Dojo'
                items:[], // filled with _subsectionItem below
                name:'Scripts',//sub folder name
                preset:_component.preset, //ref up link
                presetId:_compType
            };

            var getDescriptorString = function(key) {
                console.log('get key : ' + key);
                return 'Run Script';//goes as palette item name
            };
            //sample item for _subsections.items (example accordion container:)
            var _subsectionItem = {
                '$library':{
                    _maqGetString:getDescriptorString,
                    widgets:[],
                    collections:{}
                }, // meta-info stuff
                __paletteItemGroup:999, //no idea
                category:'containers',
                allowedChild:'', //dijit/layout/ContentPane cool!
                collection:'xblox',//makes sense
                description:'Border Container', // comes from block.description or shareDescription
                iconBase64:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAPJJREFUeNpi/P//P8PCgzf/bzv3nIFU4GUkycACYoA01wYZkGxA87oLEAP+//vHsPvWa5IN+P/vP8QAbxNphi0nHzL8R5UGYkY4jxEqgizvay4HMWDj0QcMs4qcgUKMqKqg+hmBYv8ZkYyDmp3WvwdiwL+/f8HiohzMRDv/zY+/DP9+/2VgAhvw5y8DOQBkMdQF/xh+Ac14/u0P3AfIfmaEOvs/VJAR6lOQAYywdLBq5zWSXRDmroVIB53pjuSng98/fpKVDkD6wAb4WykAXfGUZANA+qiTlKnihVU7r4DjC2f0IaVIWFSGumlBopESABBgAGY0fipQlZCHAAAAAElFTkSuQmCC",
                iconLarge:"dojo/1.8/resources/imagesLarge/AccordionContainer.png",
                iconLargeBase64:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAANxJREFUeNrs17EKwjAQxvGoIZPdi0N3wcFR6NhNn8Wn8VnqZMeimw6ikyCdSn0BwTSxRRzcuvkvXCD7j+9y5G6Q7vaKd3Rzl8kCZdpmh5ZVVQ9iWkEwJrK898LqzHIOyWrykrQ6s6y1NNZQIQ+6E52k1fe0jpc7jnVTE2gnruYhypSeypaljUHOW0YxO5HJ8kyWkiL2Py3m26odlOWkiH1fMWpmWs9XnV9LCCieht/1ddT+1esk+rtpkxUfDHQhE5awhCUsYQlLWGiW/hkqaKx4FuHSys8FrYhvAQYAB2RPPyLZy+IAAAAASUVORK5CYII=",
                initialSize:'auto',
                inlineEdit:"",// ./dijit/layout/AccordionContainerInput : cool!
                name:"RunScript", // should come from block.name or similar
                properties:{
                    style:"min-width:1em; min-height:1em;"
                },
                section:{}, //uplink to _subsection
                tags:"BorderContainer",
                type:'xblox/RunScript', //declared class ?
                widgetClass:'dijit', //mhmmm seems to do what ?
                children:[], //this seems to hold example widgets (2 content panes for 'Accordion)
                helper:'',//"./dijit/layout/LayoutContainerHelper"
                ignoreMeta:true,
                userData:{

                }
            };


            /*
            _subsection.items.push(_subsectionItem);
            _subsectionItem.section = _subsection;
            _component.subsections.push(_subsection);//root
            //give palette its cookie!
            _subsectionItem.$library.widgets.push(_subsectionItem);
            _subsectionItem.$library.collections.dijit = {name:'Dijit'};
*/
            /*
            var _component = this._createPaletteComponent('min.blox');
            var section = this._createPaletteSection('Scripts',_component);
            var item = this._createPaletteBlockItem('Test',section);
            */
            var _scopes = params.blockScopes;
            var _components = [];
            if(_scopes && _scopes.length){
                for (var i = 0; i < _scopes.length; i++) {
                    var scope = _scopes[i];
                    var blockComponent = this._blockScopeToPalette(scope,_compType);
                    if(blockComponent){
                        _components.push(blockComponent);
                    }
                }
            }



            if(_components.length) {
                for (var i = 0; i < _components.length; i++) {
                    palette._createPalette(_components[i],_compType);
                }
            }

            palette._context = params.context;
            palette.context = params.context;

            palette._presetSections = {
                /*desktop:_components[0]*/
            };

            //fire
            //palette._createPalette(_component);
            //post work
            palette._filter();
            palette.updatePaletteVisibility();
            console.log('build block palette ! ' ,params);
        }
    });
});