/** @module xideve/views/VisualEditorAction **/
define("xideve/views/VisualEditorAction", [
    "dcl/dcl",
    "xdojo/has",
    "require",
    'xaction/Action',
    'xide/types',
    'xide/utils',
    'xide/factory',
    "davinci/Runtime",
    "davinci/Workbench",
    "davinci/ve/metadata",
    "xaction/ActionProvider",
    'xaction/DefaultActions',
    'dojo/Deferred',
    'xdojo/declare',
    "davinci/ve/tools/CreateTool",
    "xide/views/SimpleCIDialog",
    "xide/widgets/TemplatedWidgetBase"
], function (dcl, has, require, Action, types, utils, factory, Runtime, Workbench, Metadata, ActionProvider, DefaultActions, Deferred, declare, CreateTool, SimpleCIDialog,
    TemplatedWidgetBase) {
    var ACTION = types.ACTION;

    var debug = false;
    const __selected = (editor) => {
        if (editor.getSelection) {
            // console.log('selected', editor.getSelection());
            const widget = editor.getSelection()[0];
            if (widget) {
                const context = editor.getEditorContext();
                const w = context.getWidgetById(widget.id);
                // console.log('widget', w._srcElement.getText(context));
            }
        } else {
            // debugger;
        }
    }

    window.__selected = __selected;


    const _createStyleState = (widget, editor) => {
        const ctx = editor.getEditorContext();
        const cis = [
            utils.createCI('Name', types.ECIType.STRING, 'noname', {
                group: 'General'
            }),
            utils.createCI('Style', types.ECIType.DOM_PROPERTIES, '', {
                group: 'Style',
                widget: {
                    open: true
                }
            })
        ]
        const nameDlg = SimpleCIDialog.create('Create New Style State', cis, (changed) => {
            const ctrToolData = {
                context: ctx,
                type: 'xblox/StyleState',
                properties: {
                    name: cis[0].value || 'noname',
                    style: cis[1].value
                },
                userData: {}
            }
            const meta = Metadata.getMetadata(ctrToolData.type);
            Metadata.getHelper(widget.type, 'tool').then(function (ToolCtor) {
                // Copy the data in case something modifies it downstream -- what types can data.data be?
                const tool = new(ToolCtor || CreateTool)(ctrToolData, {});
                tool._context = ctx;
                ctx.setActiveTool(tool);
                tool._create({
                    position: {
                        x: 0,
                        y: 0
                    },
                    parent: widget,
                    userData: meta
                });
                ctx.setActiveTool(null);
            }.bind(this));
        });
        nameDlg.show();

    }
    const _removeStyleState = (widget, editor, ctx) => {
        const node = widget.domNode;
        const state = node.state;
        const context = editor.getEditorContext();
        const widgets = context.widgetHash;
        const states = node.getStates();
        let widgetStateNode = node.getState(state);
        let stateWidget = context.toWidget(widgetStateNode);
        node.removeState(widgetStateNode);
        context.select(stateWidget);
        editor.runAction('Edit/Delete');
        context.select(widget);
    }
    window._createStyleState = _createStyleState;

    if (window.sctx && window.vee) {
        var editor = window.vee;
        if (window._veeHandle) {
            window._veeHandle.remove();
        }

        window._runAction = function (action, editor) {

        };
        const _onchange = function (e) {
            return;
            console.log('change');
            const item = e.item;
            if (item && item.dummy) {
                return;
            }
            var ctx = this.ctx;
            var actionStore = this.getActionStore();
            var mainToolbar = ctx.getWindowManager().getToolbar();
            var itemActions = item && item.getActions ? item.getActions() : null;
            var globals = actionStore.query({
                global: true
            });
            if (globals && !itemActions) {
                itemActions = [];
            }
            _.each(globals, (globalAction) => {
                var globalItemActions = globalAction.getActions ? globalAction.getActions(this.permissions, this) : null;
                if (globalItemActions && globalItemActions.length) {
                    itemActions = itemActions.concat(globalItemActions);
                }
            });
            // contextMenu && contextMenu.removeCustomActions();
            mainToolbar && mainToolbar.removeCustomActions();
            var result = [];
            var mixin = {
                addPermission: true,
                custom: true,
                quick: true
            };
            /*
            result.push(this.createAction({
                label: 'State',
                command: 'Widget/State',
                icon: 'fa-magic',
                group: 'Widget',
                tab: 'Widget',
                mixin: mixin
            }));
            */

            var newStoredActions = this.addActions(result);
            actionStore._emit('onActionsAdded', newStoredActions);
        };
        window._veeHandle = vee._on('selectionChanged', _onchange.bind(editor));
    }

    function _float(docker, panel, x, y, w, h) {

        docker.movePanel(panel, types.DOCKER.DOCK.FLOAT, null, {
            x: 300,
            y: 300,
            w: 600,
            h: 400
        });
        var frame = panel._parent;
        if (frame && frame.instanceOf('wcFrame')) {
            frame.pos(x || 300, y || 300, true);
            frame._size.x = w || 600;
            frame._size.y = h || 400;
        }
        frame.__update();

        if (frame && frame.instanceOf('wcFrame')) {
            frame.pos(x || 300, y || 300, true);
        }
        frame.__update();
        docker.__focus(frame);
    }
    const Impl = {
        declaredClass: "xideve/views/VisualEditorAction",
        _layoutActions: null,
        _designToggleAction: null,
        _maqActions: null,
        _float: function () {
            var thiz = this;
            var pe = thiz.getPageEditor();
            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getSourcePane();
            var propertyPane = pe.getPropertyPane();
            _float(docker, sourcePane, 300, 300);
            docker.movePanel(propertyPane, types.DOCKER.DOCK.STACKED, sourcePane);
            docker.movePanel(pe.getBlocksPane(), types.DOCKER.DOCK.STACKED, sourcePane);
            docker.movePanel(this.cssPane, types.DOCKER.DOCK.STACKED, sourcePane);
            propertyPane.maxSize(null, null);
            propertyPane.minSize(null, null);
            this.cssPane.select();
        },
        _dock: function () {

            var thiz = this;

            var pe = thiz.getPageEditor();

            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getDesignPane();
            var propertyPane = pe.getPropertyPane();


            docker.movePanel(propertyPane, types.DOCKER.DOCK.RIGHT, designPane, {
                tabOrientation: types.DOCKER.TAB.TOP,
                location: types.DOCKER.TAB.RIGHT
            });
            propertyPane.maxSize(400);
            propertyPane.minSize(350);

            docker.movePanel(sourcePane, types.DOCKER.DOCK.BOTTOM, null, {
                tabOrientation: types.DOCKER.TAB.TOP
            });
            docker.movePanel(pe.getBlocksPane(), types.DOCKER.DOCK.STACKED, sourcePane, {});
            docker.movePanel(this.cssPane, types.DOCKER.DOCK.STACKED, sourcePane, {});

            pe.resize();
            docker.resize();
            setTimeout(function () {
                sourcePane.getSplitter().pos(0.5);
                propertyPane.maxSize(500);
                designPane.getSplitter().pos(0.8);
                thiz.cssPane.select();
            }, 1500);
        },
        getWidgetBlockActions: function () {
            var result = [],
                thiz = this;
            var defaultMixin = {
                addPermission: true,
                quick: true
            }
            result.push(this.createAction({
                label: "Add Block",
                command: "Widget/Add Block",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-magic',
                mixin: defaultMixin,
                shouldDisable: function () {
                    var ctx = thiz.getEditorContext();
                    if (!ctx) {
                        return true;
                    }
                    var selection = thiz.getSelection();
                    return selection.length !== 1;
                }
            }));
            return result;
        },
        getMaqettaActions: function () {
            return this._maqActions;
        },
        getMaqettaAction: function (id, items, recursive) {

            items = items || this.getMaqettaActions();

            var action = _.find(items, {
                id: id
            });

            if (!action) {
                action = _.find(items, {
                    command: id
                });
            }

            if (action || (recursive === false && action)) {
                return action;
            }

            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.menu) {
                    action = this.getMaqettaAction(id, item.menu, false);
                    if (action) {
                        return action;
                    }
                }
            }

            return action;

        },
        onReload: function () {
            var info = this.contextInfo;
            var ctx = info.appContext.destroy();
        },
        reload: function (content) {
            var thiz = this;
            var dfd = new Deferred();
            this.onReload();
            var context = this.getEditorContext();
            //var context = this.getEditor()
            var rootWidget = context ? $(context.rootNode) : null;
            if (rootWidget && rootWidget[0] && rootWidget[0]._destroyHandles) {
                rootWidget[0].__didEmitLoad = false;
                rootWidget[0]._destroyHandles();
            }

            var appContext = context.appContext;
            if (appContext) {
                appContext.didWireWidgets = false;
            }

            if (thiz.blockScopes) {
                for (var i = 0; i < thiz.blockScopes.length; i++) {
                    var scope = thiz.blockScopes[i];
                    thiz.ctx.getBlockManager().removeScope(scope.id);
                }
                delete this.blockScopes;
            }
            var onReady = function (content) {
                thiz.editor.setContent(thiz.item.path, content);
                dfd.resolve(content);
            };
            if (!content) {
                thiz.ctx.getFileManager().getContent(thiz.item.mount, thiz.item.path, onReady);
            } else {
                onReady(content);
            }

            return dfd;
        },
        showInOutline: function (widget) {
            if (!widget) {
                return;
            }
            var ctx = this.getEditorContext(),
                document = ctx.getDocument(),
                selection = this.getSelection(),
                firstItem = selection[0];


            widget = widget || firstItem;
            var outline = this.outlineGridView;
            outline.grid.select(widget.id, null, true, {
                focus: true,
                append: false,
                expand: true,
                delay: 1
            });
        },
        showCode: true,
        toogleCode: function () {
            return;
            if (this.showCode && this.getPageEditor()) {
                this.getPageEditor()._srcCP._parent.getSplitter().collapse();
            } else {
                this.getPageEditor()._srcCP._parent.getSplitter().expand();
            }
            this.showCode = !this.showCode;
            this.saveSettings();
        },
        runAction: function (action) {

            action = _.isString(action) ? this.getAction(action) : action;
            if (!action) {
                return;
            }
            debug && console.log('run VE - action ' + action.command, action);

            var self = this,
                command = action.command,
                parts = command.split('/'),
                last = parts[parts.length - 1],
                ctx = this.getEditorContext();

            if (command.indexOf('Widget/State/') !== -1) {
                switch (command) {
                    case 'Widget/State/Add':
                        {
                            const command = action.command;
                            const ctx = this.getEditorContext();
                            const selection = this.getSelection();
                            const fn = window._createStyleState || _createStyleState;
                            return fn(selection[0], editor, ctx);
                        }
                }
            }
            if (command.indexOf('View/Show/Float') !== -1) {
                return this._float();
            }
            if (command.indexOf('View/Show/Dock') !== -1) {
                return this._dock();
            }
            if (command.indexOf('View/Show/Code') !== -1) {
                return this.toogleCode();
            }

            if (command.indexOf('Widget/Focus') !== -1) {
                var selection = this.getSelection(),
                    firstItem = selection ? selection[0].domNode : null;
                if (firstItem) {
                    firstItem.scrollIntoView();
                }
                return
            }

            if (command.indexOf('View/Split/') !== -1) {
                this.doSplit(parseInt(last));
                this.updateFrame();
                return true;
            }

            if (command === "Widget/Add Block") {
                var bEditor = this.getBlockEditor();
                if (bEditor) {
                    return bEditor.runAction({
                        command: 'File/New Group'
                    });
                }
            }
            if (command === 'View/Layout Mode/Relative') {
                return self.getPageEditor().selectLayoutFlow();
            }

            if (command === 'File/Reload') {
                return this.reload();
            }
            if (command === 'Widget/Show In Outline') {
                var selection = this.getSelection(),
                    firstItem = selection ? selection[0] : null;
                return this.showInOutline(firstItem);
            }
            if (command === 'View/Layout Mode/Absolute') {
                return self.getPageEditor().selectLayoutAbsolute();
            }
            if (command === 'View/Show/Preview') {
                self._designMode = false;
                self.getPageEditor().getDesignPane().title("<span class='text-warning'>Preview</span>");
                self.getPageEditor().getDesignPane().icon('text-warning fa-eye');
                this._designToggle.set('value', true);
                return self.setDesignMode(self._designMode);
            }
            if (command === 'View/Show/Design') {
                self._designMode = true;
                self.getPageEditor().getDesignPane().title("Design");
                self.getPageEditor().getDesignPane().icon('text-info fa-edit');
                this._designToggle.set('value', false);
                return self.setDesignMode(self._designMode);
            }

            if (command === 'File/Open In Browser' || command === 'File/Preview') {

                var forceRelease = false;
                var css = this.item.name.replace('.dhtml', '.css');
                css = this.item.getParent().path + '/' + css;
                //console.error('css : ' + css);
                var VFS_GET_URL = this.editor.ctx.getResourceManager().getVariable('VFS_GET_URL');

                //subs['APP_CSS'] = VFS_GET_URL + '' + item.mount + '&path=' + encodeURIComponent(subs.SCENE_CSS);

                css = this.editor.ctx.getFileManager().base64_encode(VFS_GET_URL + '' + this.item.mount + '&path=' + css);

                //http://localhost/projects/x4mm/Code/xapp/xcf/index.php?view=smdCall&debug=true&run=run-release-debug&protocols=true&xideve=true&drivers=true&plugins=false&xblox=debug&files=true&dijit=debug&xdocker=debug&xfile=debug&davinci=debug&dgrid=debug&xgrid=debug&xace=debug&service=XCOM_Directory_Service.get2&callback=asdf&raw=html&attachment=0&send=1&mount=/workspace_user&path=default.css

                var params = {
                    template:  false  && forceRelease == false ? 'view.template.html' : 'view.template.release.html',
                    css: css,
                    baseOffset: encodeURIComponent(this.item.getParent().path.replace('./', '')),
                    userDirectory: encodeURIComponent(this.editor.ctx.getResourceManager().getVariable('USER_DIRECTORY'))
                };

                var url = this.ctx.getContextManager().getViewUrl(this.item, null, params);
                console.log('open scene with system : ' + url);
                if (has('electronx')) {
                    var _require = window['eRequire'];
                    if (!_require) {
                        console.error('have no electron');
                    }
                    var shell = _require("electron").shell;
                    return shell.openExternal(url);

                } else {
                    return window.open(url);
                }
            }

            if (command.indexOf('Widget/Align/') !== -1) {
                switch (command) {
                    case 'Widget/Align/Align Left':
                        return self._alignLeftVertical();
                    case 'Widget/Align/Align Center':
                        return self._alignCenterVertical();
                    case 'Widget/Align/Align Right':
                        return self._alignRightVertical();
                    case 'Widget/Align/Align Top':
                        return self._alignTopHorizontal();
                    case 'Widget/Align/Same Size':
                        return self._sameSize();
                    case 'Widget/Align/Same Width':
                        return self._sameWidth();
                    case 'Widget/Align/Same Height':
                        return self._sameHeight();
                }
                return true;
            }
            var mAction = action.action;
            if (!mAction) {
                switch (command) {
                    case 'File/Delete':
                    case 'Edit/Delete':
                        {
                            mAction = this.getMaqettaAction('delete');
                            break;
                        }
                    case 'Edit/Undo':
                        {
                            mAction = this.getMaqettaAction('undo');
                            break;
                        }
                    case 'Edit/Redo':
                        {
                            mAction = this.getMaqettaAction('redo');
                            break;
                        }
                    case 'Edit/Copy':
                        {
                            mAction = this.getMaqettaAction('copy');
                            break;
                        }
                    case 'Edit/Paste':
                        {
                            mAction = this.getMaqettaAction('paste');
                            break;
                        }
                    case 'Edit/Cut':
                        {
                            mAction = this.getMaqettaAction('cut');
                            break;
                        }
                    case "File/ManageStates":
                        {
                            mAction = this.getMaqettaAction('manageStates');
                            break;
                        }
                    case "File/Save As Widget":
                        {

                            mAction = this.getMaqettaAction('saveasdijit');
                            break;

                        }
                    case "Widget/SourroundWithDIV":
                        {
                            mAction = this.getMaqettaAction('Widget/SourroundWithDIV');
                            break;
                        }
                }
            }
            if (!mAction) {
                mAction = this.getMaqettaAction(command);
            }
            if (mAction) {
                var result = Workbench._runAction(mAction, ctx);
                return result;
            } else {
                console.warn('cant find maqetta action ' + command, [_.pluck(this._maqActions, 'id'), this._maqActions]);
            }
            return this.inherited(arguments);
        },
        _getIconClass: function (iconClassIn) {
            if (!iconClassIn) {
                return '';
            }

            switch (iconClassIn) {

                case "editActionIcon undoIcon":
                    {
                        return 'text-info fa-undo';
                    }
                case "editActionIcon redoIcon":
                    {
                        return 'text-info fa-repeat';
                    }

                case "editActionIcon editCutIcon":
                    {
                        return 'text-warning fa-scissors';
                    }
                case "editActionIcon editCopyIcon":
                    {
                        return 'text-warning fa-copy';
                    }
                case "editActionIcon editPasteIcon":
                    {
                        return 'text-warning fa-paste';
                    }
                case "editActionIcon editDeleteIcon":
                    {
                        return 'text-danger fa-remove';
                    }
            }
            return iconClassIn;
        },
        _toCommand: function (action) {
            switch (action) {
                case 'Undo':
                    return 'Edit/Undo';
                case 'Redo':
                    return 'Edit/Redo';
                case 'Cut':
                    return 'Edit/Cut';
                case 'Copy':
                    return 'Edit/Copy';
                case 'Paste':
                    return 'Edit/Paste';
                case 'Delete':
                    return 'Edit/Delete';
            }
        },
        __addAction: function (action) {
            switch (action.id) {
                case "openBrowser":
                    {
                        return false;
                    }
                case "documentSettings":
                    {
                        return false;
                    }
                case "stickynote":
                    {
                        return false;
                    }

                case "tableCommands":
                    {
                        return false;
                    }
            }
            return true;
        },
        _createEditToggleAction: function () {
            var thiz = this;
            var _toggle = Action.createDefault('Design', 'fa-toggle-on', 'View/Design Mode', 'viewActions', null, {
                widgetClass: 'xide/widgets/ToggleButton',
                widgetArgs: {
                    icon1: 'fa-toggle-on',
                    icon2: 'fa-toggle-off',
                    delegate: this,
                    checked: thiz._designMode
                },
                handler: function (command, item, owner, button) {
                    thiz._designMode = !thiz._designMode;
                    button.set('checked', thiz._designMode);
                    thiz.setDesignMode(thiz._designMode);

                }
            }).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null).
            setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                label: '',
                permanent: function () {
                    return thiz.destroyed == false;
                },
                widgetArgs: {
                    style: 'text-align:right;float:right;font-size:120%;'
                }
            }).
            setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {
                show: false
            });

            this._designToggleAction = _toggle;

            return _toggle;
        },
        _getItemActions: function () {
            try {

                if (!this.editor || !this.ready) {
                    return [];
                }

                this.restoreStyleView();

                var _actions = this._getViewActions();
                if (_actions && _actions[0]) {
                    _actions = _actions[0].actions;
                }


                var actions = [],
                    thiz = this;

                this._maqActions = _actions;

                debug && console.log('ve-------------    get item actions', _actions);

                actions.push(this.getSplitViewAction());

                if (!this._designToggleAction) {
                    actions.push(this._createEditToggleAction());
                }

                var _runFunction = function (item) {
                    Workbench._runAction(item, context);
                };

                var _wireAction = function (action) {
                    action.handler = function () {
                        try {
                            _runFunction(action.action);
                        } catch (e) {
                            console.error('action failed ' + e, action);
                        }
                    };
                };

                var context = null;
                var layoutAction = null;
                if (this.editor && this.editor.currentEditor && this.editor.currentEditor.context) {
                    context = this.editor.currentEditor.context;
                }
                for (var i = 0; i < _actions.length; i++) {
                    var action = _actions[i];
                    if (_.isString(action.action)) {
                        Workbench._loadActionClass(action);
                    }
                    var disabled = false;
                    if (action.action && action.action.isEnabled) {
                        disabled = !action.action.isEnabled(context);
                    }
                    if (action.id === 'layout') {
                        layoutAction = action;
                    }
                    var _a = this._toAction(action);
                    if (!_a) {
                        continue;
                    }
                    _a.action = action;
                    _wireAction(_a);
                    actions.push(_a);
                }
                actions.push(Action.createDefault('Reload', 'fa-refresh', 'File/Reload', 'viewActions', null, {
                    addPermission: true,
                    handler: function () {

                        if (thiz.blockScopes) {
                            for (var i = 0; i < thiz.blockScopes.length; i++) {
                                var scope = thiz.blockScopes[i];
                                thiz.ctx.getBlockManager().removeScope(scope.id);
                            }
                            delete this.blockScopes;
                        }

                        var onReady = function (content) {
                            thiz.editor.setContent(thiz.item.path, content);
                        };
                        thiz.ctx.getFileManager().getContent(thiz.item.mount, thiz.item.path, onReady);
                    }
                }));
                var _ctx = this.getEditorContext();
                var selection = _ctx.getSelection();
                if (selection.length) {
                    var widget = selection[0];
                    Metadata.getSmartInput(widget.type).then(function (inline) {
                        if (inline && inline.show) {

                            actions.push(Action.createDefault('Wizard', 'fa-magic', 'Edit/Wizard', 'viewActions', null, {
                                handler: function () {
                                    inline.show(widget.id);
                                }
                            }));
                        }
                    });
                    actions = actions.concat(this.getWidgetLayoutActions(actions));
                }

                if (layoutAction) {
                    var root = Action.create('Layout', 'fa-crosshairs', 'View/Layout Mode', false, null, types.ITEM_TYPE.WIDGET, 'docActions', null, false, function () {}, {
                        dummy: true
                    });
                    actions.push(root);
                    var relative = Action.create('Relative', '', 'View/Layout Mode/Relative', false, null, types.ITEM_TYPE.WIDGET, 'docActions', null, false, function () {
                        //
                        thiz.getPageEditor().selectLayoutFlow();
                    });
                    actions.push(relative);
                    var absolute = Action.create('Absolute', '', 'View/Layout Mode/Absolute', false, null, types.ITEM_TYPE.WIDGET, 'docActions', null, false, function () {
                        thiz.getPageEditor().selectLayoutAbsolute();
                    });
                    actions.push(absolute);
                }

                if (!this._layoutActions) {
                    actions = actions.concat(this.getLayoutActions());
                }
                actions.push(Action.createDefault('Preview', 'text-success fa-eye', 'File/Open In Browser', 'viewActions', null, {
                    /*
                    handler: function () {
                        window.open(thiz.ctx.getContextManager().getViewUrl(thiz.item));
                    },*/
                    mixin: {
                        quick: true
                    }
                }).setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                    label: '',
                    widgetArgs: {
                        style: 'text-align:right;float:right;font-size:120%;'
                    }
                }));
                return actions;
            } catch (e) {
                return [];
            }
        },
        getSplitViewActions: function () {

            var result = [];
            return result;
            /*
                        var self = this,
                            root = 'View/Split',
                            VIEW_SPLIT_MODE = types.VIEW_SPLIT_MODE;

                        var modes = [{
                                mode: VIEW_SPLIT_MODE.DESIGN,
                                label: 'Design',
                                icon: 'fa-eye'
                            },
                            {
                                mode: VIEW_SPLIT_MODE.SOURCE,
                                label: 'Code',
                                icon: 'fa-code'
                            },
                            {
                                mode: VIEW_SPLIT_MODE.SPLIT_HORIZONTAL,
                                label: 'Horizontal',
                                icon: 'layoutIcon-horizontalSplit'
                            },
                            {
                                mode: VIEW_SPLIT_MODE.SPLIT_VERTICAL,
                                label: 'Vertical',
                                icon: 'layoutIcon-layout293'
                            }
                        ];
                        _.each(modes, function (mode) {
                            result.push(self.createAction({
                                label: mode.label,
                                command: root + '/' + mode.mode,
                                icon: mode.icon,
                                tab: 'Home',
                                group: 'View',
                                //keycombo:['ctrl '+mode.mode],
                                mixin: {
                                    addPermission: true,
                                    quick: true
                                }
                            }));
                        });

                        return result;
                        */

        },
        getStatableSelection() {
            const ret = [];
            var selection = this.getSelection();
            if (selection && selection.length === 1) {
                const widget = selection[0].domNode;
                if (widget && widget.getStates) {
                    ret.push(widget);
                }
            }
            return ret;
        },
        getStatableWidgetsSelection() {
            return this.getSelection().filter((widget) => {
                if (widget && widget.domNode && widget.domNode.getStates) {
                    return widget.domNode.getStates;
                }
            });
        },
        getFlowActions: function () {
            var result = [],
                self = this,

                root = 'View/Layout Mode';

            var defaultMixin = {
                addPermission: true,
                quick: true
            }
            result.push(this.createAction({
                label: 'Flow',
                command: root,
                icon: 'fa-crosshairs',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Relative',
                command: root + '/Relative',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Absolute',
                command: root + '/Absolute',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));
            return result;
        },
        getFloatActions: function () {
            var result = [];
            result.push(this.createAction({
                label: 'Float',
                command: 'View/Show/Float',
                icon: 'fa-compress',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            result.push(this.createAction({
                label: 'Dock',
                command: 'View/Show/Dock',
                icon: 'fa-expand',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            result.push(this.createAction({
                label: 'View/Hide Code',
                command: 'View/Show/Code',
                icon: 'fa-code',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));
            return result;
        },
        getItemActions: function (defaultActions) {
            var result = [],
                self = this,
                ctx = this.getEditorContext();

            result = result.concat(this.getSplitViewActions());
            result = result.concat(this.getFloatActions());
            result = result.concat(this.getWidgetBlockActions());

            var defaultMixin = {
                addPermission: true,
                quick: true,
                localize: false
            };
            result.push(this.createAction({
                label: "Save",
                command: "File/Save",
                icon: 'fa-save',
                group: 'File',
                tab: 'Home',
                keycombo: ['ctrl s'],
                mixin: defaultMixin
            }));
            /*
                        result.push(this.createAction({
                            label: 'State',
                            command: 'Widget/State',
                            icon: 'fa-magic',
                            group: 'Widget',
                            tab: 'Home',
                            mixin: defaultMixin
                        }));
                        result.push(this.createAction({
                            label: 'Add State',
                            command: 'Widget/State/Add',
                            icon: 'fa-magic',
                            group: 'Widget',
                            tab: 'Home',
                            mixin: defaultMixin,
                            owner: this
                        }));
                        */



            /*
            result.push(this.createAction({
                label:"Show Properties",
                command:"View/Show/Properties",
                icon:'fa-cogs',
                group:'View',
                tab:'Home',
                keycombo:['ctrl enter'],
                mixin:defaultMixin
            }));
            */
            result.push(this.createAction({
                label: "Select",
                command: "Widget/Select",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-hand-o-up',
                mixin: defaultMixin
            }));


            result.push(this.createAction({
                label: "Show In Outline",
                command: "Widget/Show In Outline",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-crosshairs',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: "Move",
                command: "Widget/Move",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-reorder',
                mixin: defaultMixin
            }));


            result = result.concat(this.getWidgetLayoutActions());
            result = result.concat(this.getEditToggleActions());
            result = result.concat(this.getFlowActions());
            result.push(this.createAction({
                label: "Focus",
                command: "Widget/Focus",
                group: 'Widget',
                tab: 'Home',
                icon: 'fa-crosshairs',
                mixin: defaultMixin
            }));
            ///////////////////////////////////////////////////////////
            //
            //  Normal view actions
            //
            //
            var _viewActions = this._getViewActions(defaultActions);
            if (_viewActions && _viewActions[0]) {
                _viewActions = _viewActions[0].actions;
            }

            this._maqActions = _viewActions;

            debug && console.log('maq actions ', _viewActions);
            var context = this.getEditorContext();
            if (this.editor && this.editor.currentEditor && this.editor.currentEditor.context) {
                context = this.editor.currentEditor.context;
            }

            function completeAndAddAction(action) {
                var _a = self._toAction(action);
                if (!_a || !_a.command) {
                    return;
                }
                _a.tab = 'Home';
                _a.mixin = utils.mixin({}, defaultMixin);
                if (_a.command.indexOf("Widget/SourroundWith") !== -1) {
                    _a.mixin.quick = false;
                }
                _a.action = action;
                result.push(_a);

            }
            for (var i = 0; i < _viewActions.length; i++) {
                var action = _viewActions[i];
                Workbench._loadActionClass(action);
                var parms = {
                    showLabel: false /*, id:(id + "_toolbar")*/
                };
                ['label', 'showLabel', 'iconClass'].forEach(function (prop) {
                    if (action.hasOwnProperty(prop)) {
                        parms[prop] = action[prop];
                    }
                });
                if (action.className) {
                    parms['class'] = action.className;
                }
                if (action.menu && (action.type == 'DropDownButton' || action.type == 'ComboButton')) {
                    for (var ddIndex = 0; ddIndex < action.menu.length; ddIndex++) {
                        var menuItemObj = action.menu[ddIndex];
                        Workbench._loadActionClass(menuItemObj);
                        var menuItemParms = {};
                        var props = ['label', 'iconClass'];
                        props.forEach(function (prop) {
                            if (menuItemObj[prop]) {
                                menuItemParms[prop] = menuItemObj[prop];
                            }
                        });

                        completeAndAddAction(menuItemObj);
                    }

                } else if (action.toggle || action.radioGroup) {

                }
                var disabled = false;
                if (action.id === 'layout') {
                    layoutAction = action;
                }
                completeAndAddAction(action);

            }




            const widgetClass = dcl(TemplatedWidgetBase, {
                templateString: '<div class="CIActionWidget"></div>',
                widgetClass: ''
            });
            const updateState = (widget, value) => {
                const ctx = this.getEditorContext();
                const valuesObject = {};
                valuesObject['state'] = value;
                const command = new davinci.ve.commands.ModifyCommand(widget, valuesObject, null);
                dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                    source: ctx.editor_id,
                    command: command
                }]);

                let widgetStateNode = widget.domNode.getState(value);
                if (widgetStateNode) {
                    let stateWidget = ctx.toWidget(widgetStateNode);
                    if (stateWidget) {
                        ctx.select(stateWidget);
                    }
                }
            }

            const createActionWidgetClass = (action) => {
                return dcl(widgetClass, {
                    cis: [],
                    action: action,
                    startup: function () {
                        factory.renderCIS(this.cis, this.domNode, this).then((widgets) => {
                            widgets.forEach((w) => {
                                action.addReference(w);
                                w._on('change', (what) => {
                                    action.set('value', what);
                                    action.refresh();
                                    const selection = self.getStatableWidgetsSelection();
                                    selection.forEach((widget) => {
                                        if (what === 'No State') {
                                            //widget.domNode.setState('');
                                            updateState(widget, '');
                                        } else if (what === 'Add State' || what === 'Remove State') {

                                            if (what === 'Add State') {
                                                return _createStyleState(selection[0], self);
                                            }

                                            if (what === 'Remove State') {
                                                return _removeStyleState(selection[0], self);
                                            }

                                        } else {
                                            // widget.setState(what);
                                            updateState(widget, what);
                                        }
                                    });

                                });
                            });
                        });

                        setTimeout(() => {
                            action.refresh();
                        }, 1000)
                    },
                    render: function (data, $menu) {
                        console.log('render');
                    }
                })
            }
            result.push(this.createAction({
                label: 'State',
                command: 'Widget/State',
                icon: 'fa-magic',
                group: 'Widget',
                tab: 'Widget',
                mixin: defaultMixin,
                onCreate: function (action) {
                    const clz = createActionWidgetClass(action);
                    const viz = types.ACTION_VISIBILITY.QUICK_LAUNCH;
                    action.setVisibility(viz, {
                        widgetClass: clz,
                        closeOnClick: false,
                        widgetArgs: {
                            cis: [
                                utils.createCI('', types.ECIType.ENUMERATION, '', {
                                    widget: {
                                        inline: true,
                                        options: [{
                                            value: 'No state support',
                                            label: 'No state support'
                                        }],
                                        widgetClass: "",
                                        action: action
                                    }
                                })
                            ]
                        }
                    });
                    action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, false);
                    action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, false);
                }
            }));

            return result;
        },
        getEditToggleActions: function () {
            var thiz = this;

            var result = [],
                self = this,

                root = 'View/Show';

            var _createStyleStatedefaultMixin = {
                addPermission: true,
                quick: true
            }
            
            /*
            result.push(this.createAction({
                label: 'Mode',
                command: root,
                icon: 'fa-eye',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));*/
            var defaultMixin = {
                addPermission: true,
                quick: true,
                localize: false
            };
            
            result.push(this.createAction({
                label: 'Edit',
                command: root + '/Design',
                icon: 'fa-edit',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Preview',
                command: root + '/Preview',
                icon: 'fa-eye',
                tab: 'Home',
                group: 'View',
                mixin: defaultMixin
            }));
            /*
                        result.push(this.createAction({
                            label: 'Preview',
                            command: 'File' + '/Preview',
                            icon: 'fa-eye',
                            tab: 'Home',
                            group: 'View',
                            mixin: defaultMixin
                        }));*/

            /*

             var _toggle = Action.createDefault('Design', 'fa-toggle-on', 'View/Design Mode', 'viewActions', null, {
             widgetClass: 'xide/widgets/ToggleButton',
             widgetArgs: {
             icon1: 'fa-toggle-on',
             icon2: 'fa-toggle-off',
             delegate: this,
             checked: thiz._designMode
             },
             handler: function (command, item, owner, button) {
             thiz._designMode = !thiz._designMode;
             button.set('checked', thiz._designMode);
             thiz.setDesignMode(thiz._designMode);

             }
             }).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null).
             setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
             label: '',
             permanent:function(){
             return thiz.destroyed==false;
             },
             widgetArgs:{
             style:'text-align:right;float:right;font-size:120%;'
             }
             }).
             setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {show: false});

             this._designToggleAction = _toggle;
             */

            return result;
        },
        _toAction: function (action) {
            //var _default = Action.create(action.label, this._getIconClass(action.iconClass), this._toCommand(action.label), false, null, types.ITEM_TYPE.WIDGET, 'editActions', null, false);
            var cmd = action.command || this._toCommand(action.label) || action.id;
            if (!cmd) {
                return null;
            }
            var self = this;
            var _default = this.createAction({
                label: action.label,
                group: action.group || 'Ungrouped',
                icon: this._getIconClass(action.iconClass) || action.iconClass,
                command: cmd,
                tab: action.tab || 'Home',
                mixin: {
                    action: action,
                    addPermission: true,
                    quick: true,
                    actionId: action.id,
                    localize: false
                },
                shouldDisable: function () {
                    var ctx = self.getEditorContext();
                    if (ctx && action.action && action.action.isEnabled) {
                        return !action.action.isEnabled(ctx);
                    }
                    return false;
                }
            });

            switch (action.id) {
                case "documentSettings":
                    {
                        return null;
                    }
                case 'outline':
                    {
                        _default.group = 'Widget';
                        return _default;
                    }
                case 'undo':
                case 'redo':
                case 'cut':
                case 'copy':
                case 'paste':
                    {
                        _default.group = 'Edit';
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {show: false});
                        return _default;
                    }
                case 'delete':
                    {
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''});
                        _default.group = 'Organize';
                        return _default;

                    }
                case 'theme':
                case 'rotateDevice':
                case 'chooseDevice':
                case 'stickynote':
                case 'savecombo':
                case 'showWidgetsPalette':
                case 'layout':
                case 'sourcecombo':
                case 'design':
                case 'closeactiveeditor':
                case 'tableCommands':
                    {
                        return null;
                    }
            }
            debug && console.log('unknown action : ' + action.id, action);
            //_default.command=action.id;
            return _default;
        },
        getWidgetLayoutActions: function (actions) {
            var thiz = this;
            var result = [];

            function add(label, icon, command, handler) {
                result.push(thiz.createAction({
                    label: label,
                    icon: icon,
                    group: 'Widget',
                    tab: 'Home',
                    command: 'Widget/Align/' + command,
                    mixin: {
                        addPermission: true,
                        quick: true
                    }

                }));
            }

            var defaultMixin = {
                addPermission: true,
                quick: true,
                localize: false
            };

            result.push(this.createAction({
                label: "Align",
                command: "Widget/Align",
                icon: 'fa-arrows',
                group: 'Widget',
                tab: 'Home',
                mixin: defaultMixin,
                shouldDisable: function () {

                    var ctx = thiz.getEditorContext();
                    if (!ctx) {
                        return true;
                    }
                    var selection = thiz.getSelection();
                    return selection.length == 0 || selection.length < 2;
                }
            }));

            add('Align Left', 'layoutIcon-alignLeftVertical', 'Align Left', thiz._alignLeftVertical);
            add('Align Center', 'layoutIcon-alignCenterVertical', 'Align Center', thiz._alignCenterVertical);
            add('Align Right', 'layoutIcon-alignRightVertical', 'Align Right', thiz._alignRightVertical);
            add('Align Top', 'layoutIcon-alignTopHorizontal', 'Align Top', thiz._alignTopHorizontal);
            add('Align Bottom', 'layoutIcon-alignBottomHorizontal', 'Align Bottom', thiz._alignBottomHorizontal);
            add('Same Size', 'layoutIcon-sameSize', 'Same Size', thiz._sameSize);
            add('Same Width', 'layoutIcon-sameWidth', 'Same Width', thiz._sameWidth);
            add('Same Height', 'layoutIcon-sameHeight', 'Same Height', thiz._sameHeight);
            return result;
        },
        /**
         * _getViewActions extracts a xide compatible actions from maqetta's own action impl.
         * @returns {Array}
         * @private
         */
        _getViewActions: function () {
            var editorID = 'davinci.ve.HTMLPageEditor'; //this.editorExtension.id;
            var editorActions = [];
            var extensions = Runtime.getExtensions('davinci.editorActions', function (ext) {
                if (editorID == ext.editorContribution.targetID) {
                    editorActions.push(ext.editorContribution);
                    return true;
                }
            });
            if (editorActions.length == 0) {
                var extensions = Runtime.getExtension('davinci.defaultEditorActions', function (ext) {
                    editorActions.push(ext.editorContribution);
                    return true;
                });
            }
            var libraryActions = Metadata.getLibraryActions('davinci.editorActions', editorID);
            // Clone editorActions, otherwise, library actions repeatedly get appended to original plugin object
            editorActions = dojo.clone(editorActions);
            if (editorActions.length > 0 && libraryActions.length) {
                // We want to augment the action list, so let's clone the
                // action set before pushing new items onto the end of the
                // array
                dojo.forEach(libraryActions, function (libraryAction) {
                    var Workbench = require("davinci/Workbench");
                    if (libraryAction.action) {
                        Workbench._loadActionClass(libraryAction);
                    }
                    if (libraryAction.menu) {
                        for (var i = 0; i < libraryAction.menu.length; i++) {
                            var subAction = libraryAction.menu[0];
                            if (subAction.action) {
                                Workbench._loadActionClass(subAction);
                            }
                        }
                    }
                    editorActions[0].actions.push(libraryAction);
                });
            }


            return editorActions;
        },
        permissions: [
            ACTION.RENAME,
            ACTION.UNDO,
            ACTION.REDO,
            ACTION.RELOAD,
            // ACTION.DELETE,
            ACTION.CLIPBOARD,
            ACTION.LAYOUT,
            ACTION.COLUMNS,
            ACTION.SELECTION,
            ACTION.SAVE,
            ACTION.SEARCH,
            ACTION.TOOLBAR,
            'File/Properties',
            ACTION.GO_UP,
            ACTION.HEADER,
            ACTION.COPY,
            ACTION.CLOSE,
            ACTION.MOVE,
            ACTION.RENAME,
            ACTION.RELOAD,
            ACTION.CLIPBOARD,
            ACTION.LAYOUT,
            ACTION.SELECTION,
            ACTION.SEARCH,
            ACTION.OPEN_IN_TAB,
            ACTION.TOOLBAR,
            ACTION.SOURCE,
            ACTION.CLIPBOARD_COPY,
            ACTION.CLIPBOARD_PASTE,
            ACTION.CLIPBOARD_CUT
        ],
        _stateOptions: function (widgetStates) {
            const option = (label, value) => {
                return {
                    label: label,
                    value: value || label
                }
            };
            const all = [
                option('No State'),
                option('Add State'),
                option('Remove State')
            ]
            widgetStates.forEach((state) => {
                all.push(option(state.name));
            })
            return all;
        },
        refreshStateAction() {
            var action = this.getAction('Widget/State');
            var selection = this.getSelection() || [];
            const refs = action.getReferences();
            if (selection.length === 1) {
                const widget = selection[0].domNode;
                if (widget && widget.getStates) {
                    const states = widget.getStates();
                    const options = this._stateOptions(states);
                    refs.forEach((ref) => {
                        ref.set('options', options);
                    });
                    const state = widget.state || 'No State';
                    const found = _.find(options, {
                        value: state
                    })

                    refs.forEach((ref) => {
                        ref.set('value', found ? state : 'No State');
                    });

                } else {
                    refs.forEach((ref) => {
                        ref.set('options', [{
                            label: 'No state support',
                            value: -1
                        }]);
                        ref.set('value', -1);
                    });
                }
            } else {
                refs.forEach((ref) => {
                    ref.set('options', [{
                        label: 'No state support',
                        value: -1
                    }]);
                    ref.set('value', -1);
                    ref.set('disabled', true);
                });
            }
            // debugger;
        },
        startup: function () {
            var self = this;
            if (this._started) {
                //return;
            }
            // this._started = true;
            var _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
            _defaultActions = _defaultActions.concat(this.getItemActions(_defaultActions));

            // here to create the root branch ! 
            _defaultActions.push(this.createAction({
                label: 'Edit',
                command: 'Edit',
                icon: 'fa-edit',
                tab: 'Home',
                group: 'View',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));

            var _newActions = this.addActions(_defaultActions);
            debug && console.log('add actions ', [_defaultActions, _newActions]);
            debug && console.dir(_.pluck(_newActions, 'command'));

            this._on('selectionChanged', (e) => {
                window._vees = e;
                if (e.owner && e.owner.editor && e.owner.editor.getSelection) {
                    window.__selected(e.owner.editor);
                } else {
                    window.__selected(self);
                }
                this.refreshActions();
                this.refreshStateAction();
                if (this.updateFocus) {
                    setTimeout(() => {
                        this.updateFocus();
                    }, 500);
                }
            });

            if (this.inherited) {
                return this.inherited(arguments);
            }
        }
    }
    /*
        window['floatVE']=function(editor){

            var thiz = editor;
            var pe = thiz.getPageEditor();
            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getSourcePane();
            var propertyPane = pe.getPropertyPane();

            _float(docker,sourcePane,300,300);
            docker.movePanel(propertyPane,types.DOCKER.DOCK.STACKED,sourcePane);
            docker.movePanel(pe.getBlocksPane(),types.DOCKER.DOCK.STACKED,sourcePane);
            docker.movePanel(editor.cssPane,types.DOCKER.DOCK.STACKED,sourcePane);
            propertyPane.maxSize(null,null);
            propertyPane.minSize(null,null);
            //docker.movePanel(sourcePane,types.DOCKER.DOCK.STACKED,propertyPane);
        }

        window['dockVE']=function(editor) {

            var thiz = editor;
            var pe = thiz.getPageEditor();
            var docker = pe.getDocker();
            var sourcePane = pe.getSourcePane();
            var designPane = pe.getDesignPane();
            var propertyPane = pe.getPropertyPane();


            docker.movePanel(propertyPane,types.DOCKER.DOCK.RIGHT,designPane,{
                tabOrientation: types.DOCKER.TAB.TOP,
                location: types.DOCKER.TAB.RIGHT
            });
            propertyPane.maxSize(400);
            propertyPane.minSize(350);

            docker.movePanel(sourcePane,types.DOCKER.DOCK.BOTTOM,null,{
                tabOrientation: types.DOCKER.TAB.TOP
            });
            docker.movePanel(pe.getBlocksPane(),types.DOCKER.DOCK.STACKED,sourcePane,{});
            docker.movePanel(editor.cssPane,types.DOCKER.DOCK.STACKED,sourcePane,{});

            pe.resize();
            docker.resize();
            setTimeout(function(){
                sourcePane.getSplitter().pos(0.5);
                propertyPane.maxSize(500);
                designPane.getSplitter().pos(0.8);
            },1500);
        };
        */
    /**
     * @mixin module:xideve/views/VisualEditorAction
     */
    const Module = dcl(ActionProvider.dcl, Impl);
    Module.GridClass = declare('Actions', [ActionProvider], Impl);
    Module.Impl = Impl;
    return Module;
});