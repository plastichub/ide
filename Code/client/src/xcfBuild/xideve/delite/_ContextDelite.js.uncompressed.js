/**
 * @module xideve/delite/_ContextDelite
 */
define("xideve/delite/_ContextDelite", [
    "dojo/_base/declare"
], function(declare) {
    /**
     *
     * @mixin module:xideve/delite/_ContextDelite
     * @lends module:xideve/delite/Context
     */
    return declare(null,{});
});
