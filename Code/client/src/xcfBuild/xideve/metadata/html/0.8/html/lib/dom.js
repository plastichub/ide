//>>built
define("Core/DOM",["FBL"],function(b){b.getAncestorByClass=function(a,c){for(;a;a=a.parentNode)if(b.hasClass(a,c))return a;return null};return b});
//# sourceMappingURL=dom.js.map