/** @module xfile/model/File **/
define("xideve/palette/Model", [
    "dcl/dcl",
    "xide/data/Model",
    "xide/utils",
    "xide/types",
    "xide/lodash"
], function (dcl, Model, utils, types, _) {
    return dcl(Model, {
        declaredClass: 'xideve.palette.model',
        getFolder: function () {
            var path = this.getPath();
            if (this.directory) {
                return path;
            }
            return utils.pathinfo(path, types.PATH_PARTS.ALL).dirname;
        },
        getChildren: function () {
            return this.children;
        },
        isChildOfId: function (id) {
            const parent = this.getParent();
            if (parent) {
                if (parent.id === id) {
                    return parent;
                }
                if (parent.isChildOfId) {
                    return parent.isChildOfId(id);
                }
            }
            return null;
        },
        getParent: function () {
            //current folder:
            var store = this.getStore() || this._S;
            return store.getParent(this);
        },
        getChild: function (path) {
            return _.find(this.getChildren(), {
                path: path
            });
        },
        getStore: function () {
            return this._store || this._S;
        }
    });
});