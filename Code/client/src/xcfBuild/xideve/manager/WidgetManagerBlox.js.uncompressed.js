/** @module xideve/manager/WidgetManager **/
define("xideve/manager/WidgetManagerBlox", [
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xide/views/CIActionDialog'
], function (dcl,types, utils, CIActionDialog) {
    /**
     *
     *
     * @TODOs

     * 1. changes are not reflected in the event's combo box history
     *
     * 2. changes to nothing don't update the widget's dom node: done
     * 3. figure out what widgets do emit else, put that in the meta-database and feed the ve
     *
     * 4. embed the selected block in the ve designer's widget-property panel, just for better
     * tuning
     *
     * 5. support share titles for better readability
     * 6. when a block pool has been added to the scene, use a filter select for quicker navigation
     * 7. do the same directly on the combo-box
     *


     * 8. find a way to pass custom arguments to a blox: done
     * 9. build a better way to complete arguments for this process: done
     *
     * 10. dojo event handles are gone when parsed, how to get them back: done
     *
     * 11. support multiple actions per widget events
     *
     *
     *
     *
     * @mixin module:xide/manager/WidgetManagerBlox
     *
     * */

    return dcl(null, {
        declaredClass:"xideve/manager/WidgetManagerBlox",
        _removeEventHandlers: function (widget, type) {

        },
        _setEventHandler: function (widget, type, value) {

            if (widget._attachEvents) {

                //normalize to .on
                const _event = type.replace('on', '');

                //remove old handler, including those from the parser
                _.each(widget._attachEvents, function (cb) {
                    if (cb.type === _event) {
                        cb.remove();
                    }

                    widget._attachEvents.remove(cb);
                });

                //add a new one
                if (value) {
                    const _function = new Function("{" + value + "; }");
                    const _handle = widget.on(_event, function (e) {
                        _function.call(widget, e);
                    });
                    _handle.type = type;
                    widget._attachEvents.push(_handle);
                }
            }

        },
        onModifiedXBLOX: function (params) {

        },
        /**
         *
         * @param params
         */
        openBloxWizard: function (params) {


            const thiz = this;

            params = this._completeEventInfo(params);
            /***
             * There are 4 cases to taken into account when changing a event handler for a widget:
             *
             * 1. the widget has already an event handler, set by dojo.parser (from dijit/_AttachMixin)
             * 2. the widget has no event handler yet
             * 3. its not a widget, same applies here 1 & 2 except the connect setup differs!
             */

            //@TODO
            let bloxFile = 'min.xblox';
            let mount = 'workspace';

            if(params.appSettings && params.appSettings.xbloxScripts && params.appSettings.xbloxScripts.length>0){
                const scriptItem = params.appSettings.xbloxScripts[0];
                bloxFile = scriptItem.path.replace('./','');
                mount = scriptItem.mount;
            }

            let block = null;

            const updateElement = true;

            const complete = function (_block, _settings) {

                thiz.ctx.blockManager.ignoreItemSelection = false;
                const path = utils.buildPath(mount, bloxFile, false);
                if (block) {
                    let settingsOut = '';
                    if (_settings) {
                        settingsOut = ',' + _settings;
                    }
                    const value = "runBlox('" + path + "','" + block.id + "',this" + settingsOut + ")";
                    thiz._setEventHandler(params.dijitWidget || params.node, params.widgetProperty, value);
                    params.editor.setDirty(true);
                    params.widget.set('value', value);
                    thiz.onModifiedXBLOX(params, block);
                }
            };

            //get xblox scope via file
            this.ctx.getBlockManager().load(mount, bloxFile).then(function (scope) {

                thiz.ctx.blockManager.ignoreItemSelection = true;

                const _d = dojo.subscribe('onItemSelected', function (evt) {
                    block = evt.item;
                });

                const widgetValue = '' + params.value;
                const split = widgetValue.split(',');
                const blockId = utils.replaceAll("'", '', split[1]);
                const blockSettings = dojo.fromJson(split[3]);
                block = scope.getBlockById(blockId);

                const actionDialog = new CIActionDialog({
                    title: 'Select Block and override inputs',
                    style: 'width:800px;min-height:600px;',
                    resizeable: true,
                    delegate: {
                        onOk: function (dlg, data) {

                            const blockSettings = utils.getCIInputValueByName(data, 'Settings');
                            if (block) {
                                complete(block, blockSettings);
                                _d.remove();
                            }
                        }
                    },
                    cis: [
                        utils.createCI('Block', types.ECIType.BLOCK_REFERENCE, '', {
                            group: 'Block',
                            delegate: null,
                            showDialog: false,
                            scope: scope,
                            block: block
                        }),
                        utils.createCI('Settings', types.ECIType.BLOCK_SETTINGS, '', {
                            group: 'Settings',
                            scope: scope,
                            block: block,
                            settings: blockSettings
                        })
                    ]
                });
                actionDialog.show();

                /*

                 simple version but no settings involved;

                 var dialog = new CIActionDialog({
                 title: 'Select Block',
                 resizeable: true,
                 resizeToContent: false,
                 fitContent: true,
                 dialogClass: 'CIDialogRoot',
                 style: 'min-width:600px;min-height:500px',
                 delegate: {
                 onOk:function(){
                 if(block){
                 complete();
                 _d.remove();
                 }
                 }
                 }
                 });

                 dialog.show().then(function () {

                 var blockEditor = utils.addWidget(BlocksFileEditor, {
                 style: 'height:inherit;width:inherit;padding:0px;',
                 beanContextName: thiz.id
                 }, thiz, dialog, true);

                 blockEditor.initWithScope(scope);
                 });
                 */

            });

        },
        onXBlox: function (context) {

            let widget = context.row.widget;
            let val = '';//widget.get('value');
            if(!widget){
                widget = dijit.byId(context.row.id);
                if(widget){
                    val = widget.get('value');
                }
            }
            if(!widget){
                widget = dojo.byId(context.row.id);
                if(widget){
                    val = widget.value;
                }
            }

            //console.log('widget_value ' + val , context);
            //build complete info

            const props = {
                owner: this,
                delegate: this,
                widget: widget,
                value: val,
                target: context.widget,//actual scene widget
                widgetProperty: context.row.display,
                meta: {
                    context: context
                }
            };
            this.openBloxWizard(props);
        }
    });
});
