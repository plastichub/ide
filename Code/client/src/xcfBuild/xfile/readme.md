# Ignore all of this:


# xfile, advanced and Dojo based file widget   

This project provides a widgets and infrastructure for file browsers and managers in the web.  

The xfile package is based on the Dojo, Dijit, DGrid and DStore

The project supports the following browsers/platforms: FF31+, Chrome latest, IE9/10+, Safari 7+,  Android 4.1+, iOS6+, WindowsPhone8+. 

More details like screen shots, features and demos can be found [here](https://github.com/mc007/xbox-app).

##Screenshots

[See here] (https://github.com/mc007/xbox-app/tree/master/misc/screenshots/SCREENSHOTS.md)

## Status

No official release yet.


## Issues

Bugs and open issues are tracked in the
[github issues tracker](https://github.com/mc007/xfile-js/issues).

## Licensing

This project is distributed by the Dojo Foundation and licensed under the ["New" BSD License](./LICENSE).
All contributions require a [Dojo Foundation CLA](http://dojofoundation.org/about/claForm).

## Installation

_Bower_ release installation:

    $ bower install xfile

_Manual_ master installation:

    $ git clone git://github.com/mc007/xfile-js.git

Then install dependencies with bower (or manually from github if you prefer to):

	$ cd deliteful
	$ bower install


## Integration

- [In existing Dojo application with custom modules and PHP as server](docs/integration.md)



## Documentation

Documentation is available [here](http://ibm-js.github.io/deliteful/docs/master/index.html).


## Testing

Testing is done with Sitepen's 'intern'

automatic: node node_modules/intern-geezer/runner config=test/intern/intern.local

manual: http://localhost/projects/xbox-app/client/src/lib/xfile/node_modules/intern-geezer/client.html?config=test/intern/intern
