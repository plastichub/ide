/** module:xfile/FileActions **/
define("xfile/FileActions", [
    'dcl/dcl',
    'xdojo/declare',
    'xdojo/has',
    'dojo/Deferred',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xide/model/Path',
    'xaction/DefaultActions',
    'xide/editor/Registry',
    'xide/editor/Default',
    'dojo/aspect',
    'xfile/views/FileOperationDialog',
    'xfile/views/FilePreview',
    'xide/views/_CIDialog',
    "xide/views/_Dialog",
    "xide/views/_PanelDialog",
    'xfile/views/FilePicker'    
], function (dcl, declare, has, Deferred, utils, types, factory, Path, DefaultActions, Registry, Default, aspect, FileOperationDialog, FilePreview, _CIDialog, _Dialog, _PanelDialog, FilePicker, newscene) {
    // "dojo/text!xfile/newscene.html"
    var ACTION = types.ACTION;
    var _debug = false;
    var Module = null;

    function defaultCopyOptions() {
        /***
         * gather options
         */
        var result = {
            includes: ['*', '.*'],
            excludes: [],
            mode: 1501
        },
            flags = 4;

        switch (flags) {

            case 1 << 2:
                {
                    result.mode = 1502;//all
                    break;
                }
            case 1 << 4:
                {
                    result.mode = 1501;//none
                    break;
                }
            case 1 << 8:
                {
                    result.mode = 1504;//newer
                    break;
                }
            case 1 << 16:
                {
                    result.mode = 1503;//size
                    break;
                }
        }

        return result;
    }

    /**
     *
     * @param owner
     * @param inValue
     * @param callback
     * @param title
     * @param permissions
     * @param pickerOptions
     * @param dfd
     * @param Module
     * @returns {*}
     */
    function createFilePicker(owner, inValue, callback, title, permissions, pickerOptions, dfd, Module, storeOptions) {
        var dlg = new _PanelDialog({
            size: types.DIALOG_SIZE.SIZE_NORMAL,
            getContentSize: function () {
                return {
                    width: '600px',
                    height: '500px'
                }
            },
            bodyCSS: {
                'height': 'auto',
                'min-height': '500px',
                'padding': '8px',
                'margin-right': '16px'
            },
            picker: null,
            title: title,
            onOk: function () {
                var selected = this.picker._selection;
                if (selected && selected[0]) {
                    callback(selected[0]);
                    dfd && dfd.resolve(selected[0], selected, this.picker);
                }
            },
            onShow: function (panel, contentNode) {
                var picker = utils.addWidget(FilePicker, utils.mixin({
                    ctx: owner.ctx,
                    owner: owner,
                    selection: inValue || '.',
                    resizeToParent: true,
                    storeOptionsMixin: utils.mixin({
                        "includeList": "*,.*",
                        "excludeList": ""
                    }, storeOptions),
                    Module: Module,
                    permissions: permissions || utils.clone(types.DEFAULT_FILE_GRID_PERMISSIONS)
                }, pickerOptions), this, contentNode, true);

                this.picker = picker;
                this.add(picker);
                this.startDfd.resolve();
                return [this.picker];

            }
        });
        dlg.show();
        return dfd;
    }

    function openFilePicker(dlg, fileWidget) {

        var inValue = fileWidget.userData.value || '.';
        var dfd = new Deferred(),
            self = this;

        var result = new _PanelDialog({
            size: types.DIALOG_SIZE.SIZE_NORMAL,
            getContentSize: function () {
                return {
                    width: '600px',
                    height: '500px'
                }
            },
            bodyCSS: {
                'height': 'auto',
                'min-height': '500px',
                'padding': '8px',
                'margin-right': '16px'
            },
            picker: null,
            title: fileWidget.title,
            onOk: function () {
                var selected = this.picker._selection;
                if (selected && selected[0]) {
                    fileWidget.set('value', selected[0].path);
                    dfd.resolve(selected[0].path);
                    result.value = selected[0].path;
                }

            },
            onShow: function (panel, contentNode) {
                var picker = utils.addWidget(FilePicker, {
                    ctx: self.ctx,
                    owner: self,
                    selection: inValue || '.',
                    resizeToParent: true,
                    mount: self.collection.mount,
                    storeOptionsMixin: {
                        "includeList": "*,.*",
                        "excludeList": ""
                    },
                    Module: self.Module,
                    permissions: utils.clone(self.Module.DEFAULT_PERMISSIONS)
                }, this, contentNode, true);

                this.picker = picker;
                this.add(picker, null, false);
                this.startDfd.resolve();
                return [this.picker];

            }
        });
        result.show();
        return dfd;

    }

    /**
     * XFile actions
     * @class module:xfile/FileActions
     * @augments module:xfile/views/FileGrid
     */
    var Implementation = {
        __loading: true,
        /**
         * mkdir version:
         *
         *    1. open dialog with input box (value = currentItem)
         *
         * @TODO : remove leaks
         */
        touch: function () {
            var dfd = new Deferred();
            try {
                var self = this,
                    currentItem = this.getSelectedItem(),
                    currentFolder = this.getCurrentFolder(),
                    startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                    collection = this.collection;

                var CIS = {
                    inputs: [
                        utils.createCI('Name', 13, startValue, {
                            widget: {
                                instant: true,
                                validator: function (value) {
                                    if (currentFolder) {
                                        return collection.getSync(currentFolder.path + '/' + value) == null &&
                                            value.length > 0;
                                    } else {
                                        return true;
                                    }
                                }
                            }
                        })
                    ]
                },
                    defaultDfdArgs = {
                        select: currentItem,
                        focus: true,
                        append: false
                    };

                var dlg = new _CIDialog({
                    cis: CIS,
                    title: 'Create new File',
                    ctx: this.ctx,
                    size: types.DIALOG_SIZE.SIZE_SMALL,
                    bodyCSS: {
                        'height': 'auto',
                        'min-height': '80px'

                    },
                    _onError: function (title, suffix, message) {
                        title = title || this.title;
                        message = message || this.notificationMessage;
                        message && message.update({
                            message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                            type: 'error',
                            actions: false,
                            duration: 15000
                        });
                        this.onError && this.onError(suffix);
                    },
                    onCancel: function () {
                        dfd.resolve();
                    },
                    onOk: function () {
                        var val = this.getField('name');
                        if (val == null) {
                            dfd.resolve(defaultDfdArgs);
                            return;
                        }
                        var currentFolder = self.getCurrentFolder() || { path: '.' },
                            newFolder = currentFolder.path + '/' + val;

                        var fileDfd = self.ctx.getFileManager().mkfile(collection.mount, newFolder, {
                            checkErrors: true,
                            returnProm: false
                        });

                        // call server::mkdir, then reload, then resolve dfd with new newFolder as selection
                        fileDfd.then(function (data) {
                            self.runAction(ACTION.RELOAD).then(function () {
                                collection.getSync(newFolder) && dfd.resolve({
                                    select: newFolder,
                                    append: false,
                                    focus: true
                                });
                            });
                        }, function (e) {
                            dfd.resolve(defaultDfdArgs);
                            logError(e, 'error creating directory');
                        });
                    }
                });
                dlg.show();
            } catch (e) {
                logError(e, 'error creating dialog');
            }
            return dfd;

        },
        newTabArgs: null,
        openInOS: function (item) {
            var thiz = this,
                ctx = thiz.ctx,
                resourceManager = ctx.getResourceManager(),
                vfsConfig = resourceManager.getVariable('VFS_CONFIG') || {};

            var mount = item.mount.replace('/', '');
            if (!vfsConfig[mount]) {
                console.error('open in os failed: have no VFS config for ' + mount);
                return;
            }

            var _require = window['eRequire'];
            if (!_require) {
                console.error('have no electron');
            }


            var os = _require('os');
            var shell = _require('electron').shell;
            var path = _require("path");

            mount = vfsConfig[mount];
            mount = utils.replaceAll(' ', '', mount);

            var itemPath = item.path.replace('./', '/');
            itemPath = utils.replaceAll('/', path.sep, itemPath);
            var realPath = path.resolve(mount + path.sep + itemPath);
            realPath = utils.replaceAll(' ', '', realPath);
            if (os.platform() !== 'win32') {
                shell.openItem(realPath);
            } else {
                shell.openItem('file:///' + realPath);
            }

            console.log('open in system ' + mount + ':' + itemPath + ' = ' + realPath, vfsConfig);

        },
        getFileActions: function (permissions) {
            permissions = permissions || this.permissions;
            var result = [],
                ACTION = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                VISIBILITY = types.ACTION_VISIBILITY,
                thiz = this,
                ctx = thiz.ctx,
                container = thiz.domNode,
                resourceManager = ctx.getResourceManager(),
                vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') : {},//possibly resourceManager not there in some builds
                actionStore = thiz.getActionStore();

            ///////////////////////////////////////////////////
            //
            //  misc
            //
            ///////////////////////////////////////////////////
            result.push(thiz.createAction({
                label: 'Go up',
                command: ACTION.GO_UP,
                icon: ACTION_ICON.GO_UP,
                tab: 'Home',
                group: 'Navigation',
                keycombo: ['backspace'],
                mixin: {
                    quick: true
                }
            }));

            result.push(thiz.createAction({
                label: 'Copy',
                command: ACTION.COPY,
                icon: 'fa-paste',
                tab: 'Home',
                group: 'Organize',
                keycombo: ['f5'],
                shouldDisable: function () {
                    return thiz.getSelectedItem() == null;
                }
            }));

            result.push(thiz.createAction({
                label: 'Close',
                command: ACTION.CLOSE,
                icon: 'fa-close',
                tab: 'View',
                group: 'Organize',
                keycombo: ['ctrl 0']
            }));

            result.push(thiz.createAction({
                label: 'Move',
                command: ACTION.MOVE,
                icon: 'fa-long-arrow-right',
                tab: 'Home',
                group: 'Organize',
                keycombo: ['f6'],
                shouldDisable: function () {
                    return thiz.getSelectedItem() == null;
                }
            }));

            result.push(thiz.createAction({
                label: 'Create New Scene',
                command: 'File/New Scene',
                icon: 'fa-magic',
                tab: 'Home',
                group: 'Organize',
                mixin: {
                    addPermission: true,
                    quick: true
                }
            }));

            if ((location.href.indexOf('electron=true') !== -1 || has('electronx') &&  true ) && vfsConfig) {
                result.push(thiz.createAction({
                    label: 'Open with System',
                    command: 'File/OpenInOS',
                    icon: 'fa-share',
                    tab: 'Home',
                    group: 'Open',
                    keycombo: ['shift f4'],
                    mixin: {
                        addPermission: true,
                        quick: true
                    },
                    shouldDisable: function () {
                        var item = thiz.getSelectedItem();
                        if (!item) {
                            return;
                        }
                        return false;
                    }
                }));
            }

            result.push(this.createAction('Open In New Tab', 'File/OpenInNewTab', 'fa-share', ['alt enter'], 'Home', 'Open', 'item', null, null, { quick: true }, null, function () {
                var item = thiz.getSelectedItem();
                if (item && item.isDir) {
                    return false;
                }
                return true;
            }, permissions, container, thiz
            ));


            if (ctx && ctx.getMountManager && DefaultActions.hasAction(permissions, ACTION.SOURCE)) {
                permissions.push(ACTION.SOURCE);
                var mountManager = ctx.getMountManager(),
                    mountData = mountManager.getMounts(),
                    i = 0;

                if (mountData && mountData.length > 0) {
                    var currentValue = thiz.collection.mount;
                    result.push(this.createAction({
                        label: 'Source',
                        command: ACTION.SOURCE,
                        icon: 'fa-hdd-o',
                        keyCombo: ['ctrl f1'],
                        tab: 'Home',
                        group: 'Navigation',
                        mixin: {
                            data: mountData,
                            addPermission: true,
                            quick: true
                        },
                        onCreate: function (action) {
                            action.set('value', currentValue);
                        }
                    }));

                    function createSourceAction(item) {
                        var label = item.label;
                        permissions.push(ACTION.SOURCE + '/' + label);
                        var _sourceAction = thiz.createAction({
                            label: label,
                            command: ACTION.SOURCE + '/' + label,
                            icon: 'fa-hdd-o',
                            keyCombo: ['alt f' + i],
                            tab: 'Home',
                            group: 'Navigation',
                            mixin: {
                                data: mountData,
                                item: item,
                                closeOnClick: false,
                                quick: true
                            },
                            onCreate: function (action) {
                                action.set('value', item.name);
                                action._oldIcon = this.icon;
                                action.actionType = types.ACTION_TYPE.SINGLE_TOGGLE;
                            }
                        });
                        result.push(_sourceAction);
                        i++;
                    }
                    _.each(mountData, createSourceAction);
                }
            }

            return result.concat(this.getEditorActions(permissions));
        },
        rename: function (item) {
            var dfd = new Deferred();
            var self = this,
                currentItem = item || this.getSelectedItem(),
                selection = this.getSelection(),
                currentFolder = this.getCurrentFolder(),
                startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).basename : '',
                collection = this.collection,
                defaultDfdArgs = {
                    select: selection,
                    focus: true,
                    append: false
                };

            var CIS = {
                inputs: [
                    utils.createCI('name', types.ECIType.STRING, startValue, {
                        widget: {
                            instant: true,
                            title: 'New Name',
                            validator: function (value) {
                                return collection.getSync(currentFolder.path + '/' + value) == null &&
                                    value.length > 0;
                            }
                        }
                    })
                ]
            };
            var dlg = new _CIDialog({
                cis: CIS,
                title: 'Rename',
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                bodyCSS: {
                    'height': 'auto',
                    'min-height': '80px',
                    'width': '100%',
                    'min-width': '400px'
                },
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {
                    var val = this.getField('name');
                    if (val == null) {
                        dfd.resolve(defaultDfdArgs);
                        return;
                    }
                    var currentFolder = self.getCurrentFolder(),
                        newFolder = val,
                        fileDfd = self.ctx.getFileManager().rename(currentItem.mount, currentItem.path, val, {
                            checkErrors: true,
                            returnProm: false
                        });

                    fileDfd.then(function (data) {

                        self.runAction(ACTION.RELOAD).then(function () {
                            defaultDfdArgs.select = currentFolder.path + '/' + val;
                            dfd.resolve(defaultDfdArgs);
                        });

                    }, function (e) {
                        logError(e, '__error renaming file!');
                        dfd.resolve(defaultDfdArgs);
                    });
                }
            });
            dlg.show();

            return dfd;

        },
        move: function (items) {
            var dfd = new Deferred();
            var self = this,
                currentItem = this.getSelectedItem(),
                selection = this.getSelection(),
                currentFolder = this.getCurrentFolder(),
                startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                collection = this.collection,

                defaultDfdArgs = {
                    select: selection,
                    focus: true,
                    append: false
                };

            //file selected
            if (!currentItem.directory) {
                var _parent = currentItem.getParent();
                if (_parent) {
                    currentItem = _parent;
                    startValue = currentItem ? currentItem.path : './';
                }
            }
            var fileWidgetValue = currentItem.path;
            var CIS = {
                inputs: [
                    utils.createCI('Name', types.ECIType.FILE, fileWidgetValue, {
                        widget: {
                            instant: true,
                            title: 'Target',
                            validator: function (value) {
                                return collection.getSync(currentFolder.path + '/' + value) == null &&
                                    value.length > 0;
                            }
                        }
                    })
                ]
            }

            var dlg = new _CIDialog({
                cis: CIS,
                title: self.localize('Move'),
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {

                    var val = this.getField('name');

                    if (val == null) {
                        dfd.resolve(defaultDfdArgs);
                        return;
                    }

                    var serverArgs = self._buildServerSelection(selection);
                    var dstPathItem = self.collection.getSync(val);
                    var dstPath = dstPathItem ? utils.normalizePath(dstPathItem.mount + '/' + dstPathItem.path) : '';

                    if (dstPathItem) {
                        serverArgs.dstPath = dstPath;
                    }

                    var options = defaultCopyOptions();
                    var currentFolder = self.getCurrentFolder(),
                        newFolder = val,
                        fileDfd = self.ctx.getFileManager().moveItem(serverArgs.selection, serverArgs.dstPath, options.includes, options.excludes, options.mode, {
                            checkErrors: true,
                            returnProm: false
                        });

                    fileDfd.then(function (data) {
                        self.runAction(ACTION.RELOAD).then(function () {
                            dfd.resolve(defaultDfdArgs);
                        });

                    }, function (e) {
                        logError(e, '__error creating file!');
                        dfd.resolve(defaultDfdArgs);
                    })
                }
            });
            dlg._on('widget', function (e) {
                e.widget._onSelect = function (w) {
                    return openFilePicker.apply(self, [dlg, w]);
                }
            });
            dlg.show();
            return dfd;
        },
        copy: function (items) {
            var dfd = new Deferred();
            var self = this,
                currentItem = this.getSelectedItem(),
                selection = this.getSelection(),
                currentFolder = this.getCurrentFolder(),
                startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                collection = this.collection,
                defaultDfdArgs = {
                    select: selection,
                    focus: true,
                    append: false
                };

            //file selected
            if (!currentItem.directory) {
                var _parent = currentItem.getParent();
                if (_parent) {
                    currentItem = _parent;
                    startValue = currentItem ? currentItem.path : './';
                }
            }
            var fileWidgetValue = currentItem.path;
            var CIS = {
                inputs: [
                    utils.createCI('Name', types.ECIType.FILE, fileWidgetValue, {
                        widget: {
                            instant: true,
                            title: 'Target',
                            validator: function (value) {
                                return collection.getSync(currentFolder.path + '/' + value) == null &&
                                    value.length > 0;
                            }
                        }
                    })
                ]
            }
            var dlgClass = dcl([FileOperationDialog, _CIDialog], {});
            var dlg = new dlgClass({
                cis: CIS,
                title: self.localize('Copy'),
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                bodyCSS: {
                    'height': 'auto',
                    'min-height': '80px',
                    'width': '100%',
                    'min-width': '400px'
                },
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {
                    var val = this.getField('name');
                    if (val == null) {
                        dfd.resolve(defaultDfdArgs);
                        return;
                    }
                    var msg = this.showMessage(),
                        thiz = this,
                        serverArgs = self._buildServerSelection(selection),
                        dstPathItem = self.collection.getSync(val),
                        dstPath = dstPathItem ? utils.normalizePath(dstPathItem.mount + '/' + dstPathItem.path) : '';

                    if (dstPathItem) {
                        serverArgs.dstPath = dstPath;
                    } else {
                        serverArgs.dstPath = utils.normalizePath(currentItem.mount + '/' + val);
                    }

                    var currentFolder = self.getCurrentFolder(),
                        newFolder = val,
                        fileDfd = self.ctx.getFileManager().copyItem(serverArgs.selection, serverArgs.dstPath, defaultCopyOptions(), {
                            checkErrors: true,
                            returnProm: false
                        });

                    fileDfd.then(function (result) {
                        thiz._onSuccess(result);
                    }, function (err) {
                        thiz._onError();
                    });

                    fileDfd.then(function (data) {
                        self.runAction(ACTION.RELOAD).then(function () {
                            dfd.resolve(defaultDfdArgs);
                        });

                    }, function (e) {
                        logError(e, '__error creating file!');
                        dfd.resolve(defaultDfdArgs);
                    })
                }
            });
            dlg._on('widget', function (e) {
                e.widget._onSelect = function (w) {
                    return openFilePicker.apply(self, [dlg, w]);
                };
            });
            dlg.show();
            return dfd;


        },
        clipboardPaste: function () {
            var dfd = new Deferred();
            var isCut = this.currentCutSelection,
                items = isCut ? this.currentCutSelection : this.currentCopySelection,
                serverParams = this._buildServerSelection(items),
                serverFunction = isCut ? 'moveItem' : 'copyItem',
                self = this,
                defaultDfdArgs = {
                    select: items,
                    focus: true,
                    append: false
                };
            if (!items) {
                dfd.resolve();
                return dfd;
            }
            var options = defaultCopyOptions();
            self.ctx.getFileManager()[serverFunction](serverParams.selection, serverParams.dstPath, {
                include: options.includes,
                exclude: options.excludes,
                mode: options.mode
            }).then(function (data) {
                self.runAction(types.ACTION.RELOAD).then(function () {
                    dfd.resolve(defaultDfdArgs);
                });
            }, function (err) {
                dfd.resolve(defaultDfdArgs);
            }, function (progress) {
                dfd.resolve(defaultDfdArgs);
            });
            return dfd;
        },
        /**
         * mkdir version:
         *
         *    1. open dialog with input box (value = currentItem)
         *
         * @TODO : remove leaks
         */
        mkdir: function () {
            var dfd = new Deferred();
            try {
                var self = this,
                    currentItem = this.getSelectedItem(),
                    currentFolder = this.getCurrentFolder(),
                    startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                    collection = this.collection;

                var CIS = {
                    inputs: [
                        utils.createCI('Name', 13, startValue, {
                            widget: {
                                instant: true,
                                validator: function (value) {
                                    if (currentFolder) {
                                        return collection.getSync(currentFolder.path + '/' + value) == null &&
                                            value.length > 0;
                                    } else {
                                        return true;
                                    }
                                }
                            }
                        })
                    ]
                },
                    defaultDfdArgs = {
                        select: currentItem,
                        focus: true,
                        append: false
                    };

                var dlg = new _CIDialog({
                    cis: CIS,
                    title: 'Create new Directory',
                    ctx: this.ctx,
                    size: types.DIALOG_SIZE.SIZE_NORMAL,
                    bodyCSS: {
                        'height': 'auto',
                        'min-height': '80px',
                        'width': '100%',
                        'min-width': '400px'
                    },
                    _onError: function (title, suffix, message) {
                        title = title || this.title;
                        message = message || this.notificationMessage;
                        message && message.update({
                            message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                            type: 'error',
                            actions: false,
                            duration: 15000
                        });
                        this.onError && this.onError(suffix);
                    },
                    onCancel: function () {
                        dfd.resolve();
                    },
                    onOk: function () {
                        var val = this.getField('name');
                        if (val == null) {
                            dfd.resolve(defaultDfdArgs);
                            return;
                        }
                        var currentFolder = self.getCurrentFolder() || { path: '.' },
                            newFolder = currentFolder.path + '/' + val;

                        var fileDfd = self.ctx.getFileManager().mkdir(collection.mount, newFolder, {
                            checkErrors: true,
                            returnProm: false
                        });

                        // call server::mkdir, then reload, then resolve dfd with new newFolder as selection
                        fileDfd.then(function (data) {
                            self.runAction(ACTION.RELOAD).then(function () {
                                collection.getSync(newFolder) && dfd.resolve({
                                    select: newFolder,
                                    append: false,
                                    focus: true
                                });
                            });
                        }, function (e) {
                            dfd.resolve(defaultDfdArgs);
                            logError(e, 'error creating directory');
                        })
                    }
                });

                dlg.show();

            } catch (e) {
                logError(e, 'error creating dialog');
            }
            return dfd;

        },
        _buildServerSelection: function (items, dst) {

            !dst && (dst = this.getCurrentFolder());

            //normalize
            if (dst && !dst.directory) {
                dst = dst.getParent();
            }

            if (!_.isArray(items)) {
                items = [items];
            }

            //basics
            var selection = [],
                store = items[0].getStore(),
                dstPath = dst ? utils.normalizePath(dst.mount + '/' + dst.path) : '';

            //build selection
            _.each(items, function (item) {
                var _storeItem = store.getSync(item.path);
                if (_storeItem) {
                    selection.push(utils.normalizePath(item.mount + '/' + item.path));
                }
            });


            //compose output
            return {
                selection: selection,
                store: items[0]._S,
                dstPath: '/' + dstPath + '/',
                firstItem: items[0],
                parent: dst
            }
        },
        openPreview: function (item, parent) {

            var row = this.row(item);

            var el = row.element,
                self = this,
                dfd = new Deferred();

            if (this._preview) {
                this._preview.item = item;
                var _dfd = this._preview.open().then(function () {
                    self._preview.preview.trigger($.Event('update', { file: item }));
                });

                return _dfd;
            }


            var _prev = new FilePreview({
                node: $(el),
                item: item,
                delegate: this.ctx.getFileManager(),
                parent: $(el),
                ctx: self.ctx,
                container: parent ? parent.containerNode : null
            });

            parent ? _prev.buildRenderingEmbedded() : _prev.buildRendering();
            _prev.init();
            _prev.exec();
            this._preview = _prev;
            this._preview.handler = this;
            self._on('selectionChanged', function (e) {
                var _item = self.getSelectedItem();
                if (_item) {

                    _prev.item = _item;
                    _prev.preview.trigger($.Event('update', { file: _item }));
                }
            });

            this.add(_prev, null, false);
            _prev._emit('changeState', function (state) {
                if (state === 0) {
                    dfd.resolve({
                        select: item,
                        focus: true,
                        append: false
                    });
                }
            });

            return dfd;

        },
        deleteSelection: function (selection) {
            selection = selection || this.getSelection();
            var dfd = new Deferred(),
                _next = this.getNext(selection[0], null, true),
                _prev = this.getPrevious(selection[0], null, true),

                next = _next || _prev,
                serverParams = this._buildServerSelection(selection),
                dlgClass = FileOperationDialog,
                title = 'Delete ' + serverParams.selection.length + ' ' + 'items',
                thiz = this;

            var dlg = new dlgClass({
                ctx: thiz.ctx,
                notificationMessage: null,
                title: title,
                type: types.DIALOG_TYPE.DANGER,
                onBeforeOk: function () {

                },
                getOkDfd: function () {
                    var thiz = this;
                    return this.ctx.getFileManager().deleteItems(serverParams.selection, {
                        hints: [1]
                    }, {
                            checkErrors: false,
                            returnProm: false,
                            onError: function (err) {
                                thiz._onError(null, err.message);
                            }
                        });
                },
                onCancel: function () {
                    dfd.resolve({
                        select: selection,
                        focus: true,
                        append: false
                    });
                },
                onSuccess: function () {
                    thiz.runAction(types.ACTION.RELOAD).then(function () {
                        dfd.resolve({
                            select: next,
                            focus: true,
                            append: false
                        });
                    });
                }
            });
            dlg.show();
            return dfd;
        },
        /**
         *
         * @param item {module:xfile/model/File}
         */
        goUp: function (item) {
            item = this._history.getNow();
            _debug && console.log('go up ' + item);
            item = _.isString(item) ? this.collection.getSync(item) : item;
            var dfd = new Deferred(),
                self = this;
            var history = this.getHistory();
            var prev = history.getNow();


            if (!item) {
                var rows = this.getRows();
                if (rows[0] && rows[0]._S) {
                    var _parent = rows[0]._S.getParent(rows[0]);
                    if (_parent) {
                        _parent = _parent._S.getParent(_parent);
                        if (_parent) {
                            item = _parent;
                        }
                    }
                }
            }

            if (item) {
                //current folder:
                var _parent = item._S.getParent(item);
                if (_parent) {
                    var _dfd = this.openFolder(_parent, true, false);
                    _dfd && _dfd.then && _dfd.then(function () {
                        if (prev) {
                            //history.pop();
                            var select = self.collection.getSync(prev);
                            if (select) {
                                dfd.resolve({
                                    select: select,
                                    focus: true,
                                    delay: 1
                                });
                            } else {

                                console.warn('cant find back item ' + prev);
                            }
                        } else {
                            console.warn('cant find back item ' + prev + ' ' + item.path);
                        }
                    });
                }
            } else {
                if (prev) {
                    var select = this.collection.getSync(prev);
                    if (select) {
                        this.select([select], null, true, {
                            focus: true,
                            delay: 1
                        });
                    }
                } else {
                    var rows = this.getRows();
                    if (rows[0] && rows[0]._S) {
                        var _parent = rows[0]._S.getParent(rows[0]);
                        if (_parent) {
                            _parent = _parent._S.getParent(_parent);
                            if (_parent) {
                                this.openFolder(_parent, true);
                            }
                        }
                    }
                }
            }
            return dfd;
        },
        reload: function (item) {
            var dfd = new Deferred(),
                selection = this.getSelection();

            item = item || this.getRows()[0];

            //item could be a non-store item:
            var cwd = /*item.getParent ? item.getParent() : */this.getCurrentFolder() || { path: '.' },
                self = this;

            if (cwd.isBack) {
                cwd = this.collection.getSync(cwd.rPath) || {path:'.'};
            }
            var expanded = false;
            if (this.isExpanded && this.expand && item) {
                expanded = this._isExpanded(item);
                if (expanded) {
                    this.expand(item, null, false);
                }
            }
            self.collection.resetQueryLog();
            this.collection.loadItem(cwd, true).then(function (what) {
                if (cwd.path === '.') {
                    self.collection.resetQueryLog();
                }
                self.refresh().then(function () {
                    var _dfd = self.openItem(cwd, false, false);
                    if (_dfd && _dfd.then) {
                        _dfd.then(function () {
                            dfd.resolve({
                                select: selection,
                                append: false,
                                focus: true,
                                delay: 1
                            });
                            return;
                        });
                    }
                    self.deselectAll();
                    dfd.resolve({
                        select: selection,
                        append: false,
                        focus: true,
                        delay: 2
                    });
                });
            });
            return dfd;
        },
        /**
         * Opens folder
         * @param item {module:xfile.model.File}
         * @param isBack
         */
        toHistory: function (item) {
            var FolderPath = item.getFolder ? new Path(item.getFolder()) : new Path('.');
            var segs = FolderPath.getSegments();
            var addDot = this.collection.addDot;
            var root = this.collection.rootSegment;
            //var _last = addDot ? '.' : "";
            var _last = root;
            if (segs && segs[0] === '.') {
                segs[0] = root;
            }
            var out = [];

            _.each(segs, function (seg) {
                var segPath = seg !== _last ? _last + (_last.endsWith("/") ? "" : "/") + seg : seg;
                out.push(segPath);
                _last = segPath;
            });

            return out;

        },
        openFolder: function (item, isBack, select) {
            var cwd = this.getCurrentFolder(),
                store = this.collection,
                history = this.getHistory(),
                now = history.getNow(),
                prev = history.getNow(),
                self = this;

            if (_.isString(item)) {
                item = store.getSync(item);
            } else {
                item = store.getSync(item.path) || item;
            }

            if (!item) {
                console.warn('openFolder: no item! set to root:');
                item = store.getRootItem();
            }

            _debug && console.log('open folder ' + item.path + ' cwd : ' + this.getCurrentFolder().path);
            var _hItems = self.toHistory(item);

            _debug && console.log('history:\n' + _hItems.join('\n'));
            history.set(_hItems);
            if (!item) {
                return this.setQueryEx(store.getRootItem());
            }

            if (cwd && item && isBack && cwd.path === item.path || !item) {

                return;
            }

            this.deselectAll();
            var row = this.row(item),
                node = row ? row.element : null,
                loaded = item._S.isItemLoaded(item),
                iconNode;
            if (!loaded) {
                var _els = $(node).find("span.fa");
                if (_els && _els[0]) {
                    iconNode = $(_els[0]);
                }
                if (iconNode) {
                    iconNode.removeClass('fa fa-folder');
                    iconNode.addClass('fa-spinner fa-spin');

                }
            }
            var head = new Deferred();
            this._emit('openFolder', {
                item: item,
                back: isBack
            });

            var dfd = this.setQueryEx(item, null);
            dfd && dfd.then(function () {
                //remove spinner
                if (iconNode) {
                    iconNode.addClass('fa fa-folder');
                    iconNode.removeClass('fa-spinner fa-spin');
                }
                head.resolve({
                    select: select !== false ? (isBack ? item : self.getRows()[0]) : null,
                    focus: true,
                    append: false,
                    delay: 1
                });
                self._emit('openedFolder', {
                    item: item,
                    back: isBack
                });
            });

            return head;
        },
        changeSource: function (mountData, silent) {
            var dfd = new Deferred(),
                thiz = this,
                ctx = thiz.ctx || ctx,
                fileManager = ctx.getFileManager(),
                store = factory.createFileStore(mountData.name, null, fileManager.config, null, ctx),
                oldStore = this.collection,
                sourceAction = this.getAction(ACTION.SOURCE),
                label = mountData.label || mountData.name,
                mountAction = this.getAction(ACTION.SOURCE + '/' + label);

            oldStore.destroy();

            silent !== true && this._emit('changeSource', mountData);
            this.set('loading', true);
            this.set('collection', store.getDefaultCollection('./'));
            sourceAction.set('value', mountData.name);
            thiz.set('title', 'Files (' + mountData.label + ')');
            var sourceActions = sourceAction.getChildren();
            _.each(sourceActions, function (child) {
                child.set('icon', child._oldIcon);
            });

            sourceAction.set('label', label);
            mountAction && mountAction.set('icon', 'fa-spinner fa-spin');
            this.refresh().then(function () {
                thiz.set('loading', false);
                thiz.set('collection', store.getDefaultCollection('./'));
                mountAction && mountAction.set('icon', 'fa-check');
                silent !== true && thiz._emit('changedSource', mountData);
                thiz.select([0], null, true, {
                    append: false,
                    focus: true
                }).then(function () {
                    dfd.resolve();
                });
            });
            return dfd;

        },
        openInNewTab: function (item) {
            var thiz = this,
                ctx = thiz.ctx,
                wManager = ctx.getWindowManager(),
                dfd = new Deferred(),
                tab = null;

            if (_.isFunction(this.newTarget)) {
                tab = this.newTarget({
                    title: item.name,
                    icon: 'fa-folder',
                    target: thiz._parent,
                    location: null,
                    tabOrientation: null
                });

            } else {
                tab = wManager.createTab(item.name, 'fa-folder', this.newTarget || this);
            }
            var _store = this.collection;
            if (!item.isDir) {
                item = this.getCurrentFolder();
            }
            var store = factory.createFileStore(_store.mount, _store.options, _store.config, null, ctx);
            store.micromatch = _store.micromatch;

            var args = utils.mixin({
                showToolbar: this.showToolbar,
                collection: store,
                selectedRenderer: this.selectedRenderer,
                showHeader: this.showHeader,
                newTarget: this.newTarget,
                style: this.style,
                options: utils.clone(this.options),
                permissions: this.permissions,
                _columns: this._columns,
                attachDirect: true,
                    registerEditors: this.registerEditors,
                    hideExtensions: this.hideExtensions
            }, this.newTabArgs || {}),

                grid = utils.addWidget(this.getClass(), args, null, tab, false);

            dfd.resolve();
            grid.set('loading', true);
            store.initRoot().then(function (d) {
                var _dfd = store.getItem(item.path, true);
                _dfd && _dfd.then(function () {
                    grid.startup();
                    grid._showHeader(thiz.showHeader);
                    grid.openItem(item).then(function () {
                        grid.set('loading', false);
                        grid.select([0], null, true, {
                            focus: true,
                            append: false,
                            delay: 1
                        });

                    });
                    wManager.registerView(grid, true);
                });
            });
            return dfd;
        },
        openItem: function (item, isBack, select) {
            if (!item) {
                var _rows = this.getRows();
                if (_rows[0] && _rows[0].isBack) {
                    item = _rows[0];
                }
            }

            if (item && item.isBack) {
                item._S = null;
                return this.goUp();
            }

            item = _.isString(item) ? this.collection.getSync(item) : item;
            if (!item) {
                return;
            }
            if (item.directory === true) {
                return this.openFolder(item, isBack, select);
            } else {
                var editors = Registry.getEditors(item);
                var defaultEditor = _.find(editors, function (editor) {
                    return editor.isDefault === true;
                });
                if (defaultEditor) {
                    return defaultEditor.onEdit(item, this);
                }
                return this.ctx.getWindowManager().openItem(item, this.newTarget || this, {
                    register: true
                });

            }
        },
        getSelectedItem: function () {
            return this.getSelection()[0];
        },
        openDefaultEditor: function (item) {
            return Default.Implementation.open(item);
        },
        close: function () {
            var panel = this._parent;
            if (panel) {
                var docker = panel.docker();
                if (docker) {
                    this.onAfterAction = null;
                    docker.removePanel(panel);
                }
            }
            return false;
        },
        compress: function (items) {
            items = items || this.getSelection();
            var thiz = this;
            var serverParams = this._buildServerSelection(items);
            if (serverParams && serverParams.store && serverParams.selection) {
                thiz.ctx.getFileManager().compressItem(serverParams.firstItem.mount, serverParams.selection, 'zip').then(function (args) {
                    thiz.reload();
                });
            }

        },
        newScene: function () {
            var dfd = new Deferred();
            try {
                var self = this,
                    currentItem = this.getSelectedItem(),
                    currentFolder = this.getCurrentFolder(),
                    startValue = currentItem ? utils.pathinfo(currentItem.path, types.PATH_PARTS.ALL).filename : '',
                    collection = this.collection;

                startValue = 'my new scene.dhtml';

                var CIS = {
                        inputs: [
                            utils.createCI('Name', 13, startValue, {
                                widget: {
                                    instant: true,
                                    validator: function (value) {
                                        if (currentFolder) {
                                            return collection.getSync(currentFolder.path + '/' + value) == null &&
                                                value.length > 0;
                                        } else {
                                            return true;
                                        }
                                    }
                                }
                            })
                        ]
                    },
                    defaultDfdArgs = {
                        select: currentItem,
                        focus: true,
                        append: false
                    };

                var dlg = new _CIDialog({
                    cis: CIS,
                    title: 'Create new scene',
                    ctx: this.ctx,
                    size: types.DIALOG_SIZE.SIZE_SMALL,
                    bodyCSS: {
                        'height': 'auto',
                        'min-height': '80px'

                    },
                    _onError: function (title, suffix, message) {
                        title = title || this.title;
                        message = message || this.notificationMessage;
                        message && message.update({
                            message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                            type: 'error',
                            actions: false,
                            duration: 15000
                        });
                        this.onError && this.onError(suffix);
                    },
                    onCancel: function () {
                        dfd.resolve();
                    },
                    onOk: function () {
                        var val = this.getField('name');
                        if (val == null) {
                            dfd.resolve(defaultDfdArgs);
                            return;
                        }
                        var currentFolder = self.getCurrentFolder() || {
                                path: '.'
                            },
                            newFolder = currentFolder.path + '/' + val;


                        var fileDfd = self.ctx.getFileManager().mkfile(collection.mount, newFolder, newscene, {
                            checkErrors: true,
                            returnProm: false
                        });

                        // call server::mkdir, then reload, then resolve dfd with new newFolder as selection
                        fileDfd.then(function (data) {
                            self.runAction(ACTION.RELOAD).then(function () {
                                collection.getSync(newFolder) && dfd.resolve({
                                    select: newFolder,
                                    append: false,
                                    focus: true
                                });
                            });
                        }, function (e) {
                            dfd.resolve(defaultDfdArgs);
                            logError(e, 'error creating directory');
                        });
                    }
                });
                dlg.show();
            } catch (e) {
                logError(e, 'error creating dialog');
            }
            return dfd;
        },
        runAction: function (action, _item) {
            _.isString(action) && (action = this.getAction(action));
            if (!action || !action.command) {
                console.warn('invalid action');
                return;
            }
            var ACTION_TYPE = types.ACTION,
                item = this.getSelectedItem() || _item,
                sel = this.getSelection();

            if (action.command.indexOf(ACTION.SOURCE) != -1) {
                return this.changeSource(action.item);
            }
            switch (action.command) {

                case 'File/Compress':
                    {
                        return this.compress(sel);
                    }
                case ACTION_TYPE.PREVIEW:
                    {
                        return this.openPreview(item);
                    }
                case 'File/OpenInNewTab':
                    {
                        return this.openInNewTab(item || this.collection.getRootItem());
                    }
                case ACTION_TYPE.EDIT:
                    {
                        return this.openItem(item);
                    }
                case 'File/New Scene':
                    {
                        return this.newScene(item);
                    }
                case ACTION_TYPE.NEW_FILE:
                    {
                        return this.touch(item);
                    }
                case ACTION_TYPE.NEW_DIRECTORY:
                    {
                        return this.mkdir(item);
                    }
                case ACTION_TYPE.RELOAD:
                    {
                        return this.reload(item);
                    }
                case ACTION_TYPE.RENAME:
                    {
                        return this.rename(item);
                    }
                case ACTION_TYPE.GO_UP:
                    {
                        return this.goUp(null);
                    }
                case ACTION_TYPE.COPY:
                    {
                        return this.copy(null);
                    }
                case ACTION_TYPE.MOVE:
                    {
                        return this.move(null);
                    }
                case ACTION_TYPE.DELETE:
                    {
                        return this.deleteSelection(null);
                    }
                case ACTION_TYPE.OPEN_IN + '/Default Editor':
                    {
                        return this.openDefaultEditor(item);
                    }
                case ACTION_TYPE.DOWNLOAD:
                    {
                        return this.ctx.getFileManager().download(item);
                    }
                case ACTION_TYPE.CLOSE:
                    {
                        return this.close();
                    }
                case 'File/OpenInOS':
                    {
                        return this.openInOS(item);
                    }
            }

            if (action.command.indexOf(ACTION_TYPE.OPEN_IN + '/') != -1) {
                return action.editor.onEdit(item);
            }

            return this.inherited(arguments);
        },

        getEditorActions: function (permissions) {
            permissions = permissions || this.permissions;
            var result = [],
                ACTION = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                VISIBILITY = types.ACTION_VISIBILITY,
                thiz = this,
                ctx = thiz.ctx,
                container = thiz.domNode,
                actionStore = thiz.getActionStore(),
                openInAction = null,
                openInActionModel = null,
                dirty = false,
                selHandle = null;

            function getItem() {
                return thiz.getSelection()[0];
            }

            function selHandler(event) {

                var selection = event.selection;
                if (!selection || !selection[0]) {
                    return;
                }
                var item = selection[0];
                var permissions = this.permissions;
                var ACTION = types.ACTION,
                    ACTION_ICON = types.ACTION_ICON,
                    VISIBILITY = types.ACTION_VISIBILITY,
                    thiz = this,
                    container = thiz.domNode,
                    actionStore = thiz.getActionStore(),
                    contextMenu = this.getContextMenu ? this.getContextMenu() : null;

                function _wireEditor(editor, action) {
                    action.handler = function () {
                        editor.onEdit(thiz.getSelection()[0]);
                    };
                }
                function getEditorActions(item) {
                    var editors = Registry.getEditors(item) || [],
                        result = [];

                    for (var i = 0; i < editors.length; i++) {
                        var editor = editors[i];
                        if (editor.name === 'Default Editor') {
                            continue;
                        }
                        var editorAction = thiz.createAction(editor.name, ACTION.OPEN_IN + '/' + editor.name, editor.iconClass, null, 'Home', 'Open', 'item', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'Home',
                                editor: editor,
                                custom: true,
                                quick: true
                            }, null, null, permissions, container, thiz
                        );
                        _wireEditor(editor, editorAction);
                        result.push(editorAction);
                        if (editor.editorClass && editor.editorClass.getFileActions) {
                            result = result.concat(editor.editorClass.getFileActions(ctx, item));
                        }
                    }
                    return result;
                }
                if (event.why == 'deselect') {
                    return;
                }
                var action = actionStore.getSync(types.ACTION.OPEN_IN);
                var editorActions = thiz.addActions(getEditorActions(item));

                if (this._lastEditorActions) {
                    _.each(this._lastEditorActions, function (action) {
                        actionStore.removeSync(action.command);
                    });
                    delete this._lastEditorActions;
                }

                contextMenu && contextMenu.removeCustomActions();

                if (editorActions.length > 0) {
                    var newStoredActions = this.addActions(editorActions);
                    actionStore._emit('onActionsAdded', newStoredActions);
                    this._lastEditorActions = newStoredActions;
                }
            }

            if (!this._selHandle) {
                this._selHandle = this._on('selectionChanged', function (evt) {
                    selHandler.apply(thiz, [evt]);
                });
            }
            openInAction = this.createAction('Open In', ACTION.OPEN_IN, ACTION_ICON.EDIT, null, 'Home', 'Open', 'item',
                null,
                function () {
                },
                {
                    addPermission: true,
                    tab: 'Home',
                    quick: true
                }, null, DefaultActions.shouldDisableDefaultFileOnly, permissions, container, thiz
            );

            result.push(openInAction);
            var e = thiz.createAction('Default Editor', ACTION.OPEN_IN + '/Default Editor', 'fa-code', null, 'Home', 'Open', 'item', null,
                function () {
                },
                {
                    addPermission: true,
                    tab: 'Home',
                    forceSubs: true,
                    quick: true

                }, null, null, permissions, container, thiz
            );
            result.push(e);
            return result;
        }
    };

    //package via declare
    var _class = declare('xfile.FileActions', null, Implementation);
    _class.Implementation = Implementation;
    _class.createFilePicker = createFilePicker;
    return _class;

});