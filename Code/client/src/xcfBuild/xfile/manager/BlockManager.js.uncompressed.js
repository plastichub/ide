define("xfile/manager/BlockManager", [
    'dcl/dcl',
    'xide/manager/ManagerBase',
    'xide/mixins/ReloadMixin',
    'xblox/manager/BlockManager'
], function (dcl,ManagerBase, ReloadMixin, BlockManager) {
    return dcl([ManagerBase, BlockManager, ReloadMixin.dcl], {
        declaredClass:"xfile/manager/BlockManager",
        currentItem: null,
        /**
         * One time call per blox scope creation. This adds various functions
         * to the blox's owner object. This enables expressions to access the
         * object but also block specific functions like getVariable
         * @param obj
         * @param scope
         * @param owner
         */
        setScriptFunctions: function (obj, scope, owner) {
            this.inherited(arguments);
        }
    });
});