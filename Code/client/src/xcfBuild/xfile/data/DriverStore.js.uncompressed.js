/**
 * @module xfile/data/DriverStore
 **/
define("xfile/data/DriverStore", [
    "dojo/_base/declare",
    'dojo/Deferred',
    'xide/data/ObservableStore',
    'xide/data/TreeMemory',
    "xfile/data/Store",
    "xide/manager/ServerActionBase",
    "xide/utils"
], function (declare, Deferred, ObservableStore, TreeMemory, Store, ServerActionBase, utils) {
    var Implementation = Store.Implementation;

    return declare('driverFileStore', [TreeMemory, ObservableStore, ServerActionBase.declare], utils.mixin(Implementation(), {
        driver: null,
        addDot: false,
        rootSegment: "/",
        getRootItem: function () {
            var root = this._root;
            if (root) {
                return root;
            }
            this._root = {
                _EX: false,
                path: '/',
                name: '/',
                mount: this.mount,
                directory: true,
                virtual: true,
                _S: this,
                getPath: function () {
                    return this.path;
                }
            };
            return this._root;
        },
        _filter: function (items) {
            return items;
        },
        _request: function (path) {
            var collection = this;
            var self = this;
            if (path === '.' || path === '/') {
                var result = new Deferred();
                var dfd = self.driver.callCommand('LSProg', {
                    override: {
                        args: ["/*"]
                    }
                });
                dfd.then(function (data) {
                    var files = data.files;
                    _.each(files, function (file) {
                        file.parent = ".";
                    });
                    files = self._filter(files);
                    var response = {
                        items: [{
                            mount: "root",
                            path: "/",
                            children: files,
                            name: "/",
                            isDir: true
                        }]
                    };

                    var results = collection._normalize(response);
                    self._parse(results);
                    results = results.children || results;
                    result.resolve(results);
                });
                return result;
            } else {
                var result = new Deferred();
                var arg = path + ( self.glob || "/*");
                var dfd = self.driver.callCommand('LSProg', {
                    override: {
                        args: [arg]
                    }
                });
                dfd.then(function (data) {
                    var files = data.files;
                    _.each(files, function (file) {
                        file.parent = path;
                    });
                    files = self._filter(files);
                    var response = {
                        items: [{
                            mount: "root",
                            path: path,
                            children: files,
                            isDir: true
                        }]
                    };
                    var results = collection._normalize(response);
                    self._parse(results);
                    results = results.children || results;
                    result.resolve(results);
                });
                return result;
            }
        }
    }));
});