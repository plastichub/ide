define("xcf/layers", [
    "dstore/build/dstorer",
    "xide/build/xider",
    "xwire/build/xwirer",
    "xblox/build/xbloxr",
    "xcf/build/xcfr",
    "dpointer/build/dpointerr",
    "decor/build/decorr"
], function (dstorer,xider) {
    return {
        dstore:dstorer,
        xider:xider
    }
});