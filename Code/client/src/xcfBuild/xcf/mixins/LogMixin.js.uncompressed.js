/** @module xcf/mixins/LogMixing **/
define("xcf/mixins/LogMixin", [
    "dcl/dcl",
    'xcf/types/Types',
    'xide/utils'
], function (dcl, types, utils) {
    const DEFAULT_LOGGING_FLAGS = types.DEFAULT_DEVICE_LOGGING_FLAGS;
    const Module = dcl(null, {
        /**
         *
         * @param deviceInfo {module:xide/types~DeviceInfo}
         * @param flag {LOGGING_FLAGS}
         * @param source {DEVICE_LOGGING_SOURCE}
         * @returns {boolean}
         */
        hasFlagEx: function (deviceInfo, flag, source) {
            const LOGGING_FLAGS = types.LOGGING_FLAGS;
            const OUTPUT = types.LOG_OUTPUT;
            let flags = deviceInfo.loggingFlags;
            flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};

            const _flag = flags[source] ? flags[source] : DEFAULT_LOGGING_FLAGS[source];
            if (_flag == null) {
                return false;
            }

            if (!(_flag & flag)) {
                return false;
            }

            return true;
        }
    });
    Module.DEFAULT_LOGGING_FLAGS = DEFAULT_LOGGING_FLAGS;
    return Module;
});