//>>built
define("xcf/mixins/LogMixin",["dcl/dcl","xcf/types/Types","xide/utils"],function(b,e,f){var d=e.DEFAULT_DEVICE_LOGGING_FLAGS;b=b(null,{hasFlagEx:function(a,b,c){a=a.loggingFlags;a=_.isString(a)?f.fromJson(a):a||{};c=a[c]?a[c]:d[c];return null!=c&&c&b?!0:!1}});b.DEFAULT_LOGGING_FLAGS=d;return b});
//# sourceMappingURL=LogMixin.js.map