/** @module xgrid/Base **/
define("xcf/views/DriverView2", [
    "dcl/dcl",
    "xdojo/declare",
    "xdojo/has",
    "dojo/on",
    "dojo/debounce",
    'dojo/dom-construct',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/Grid',
    'xaction/DefaultActions',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xblox/views/BlockGrid',
    'xcf/model/Command',
    'xcf/model/Variable',
    'xide/layout/_Accordion',
    "xide/widgets/TemplatedWidgetBase",
    'dojo/promise/all',
    "dojo/Deferred",
    "xgrid/KeyboardNavigation",
    "xgrid/Search",
    'xide/_base/_Widget',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'xide/views/ConsoleView',
    'xlog/views/LogGrid',
    'xide/widgets/ExpressionEditor',
    "module",
    "xcf/views/ExpressionConsole",
    'xide/views/History',
    'xblox/views/ThumbRenderer',
    'xblox/views/BlockEditView',
    'xfile/views/FilePreview',
    'xide/views/_CIPanelDialog',
    'xide/widgets/EditBox',
    'xide/views/_CIDialog',
    "xdojo/has!debug?xide/tests/TestUtils",
    "require",
    "xcf/views/DriverView_Layout",
    'dojo/when',
    'xide/console'


], function (dcl, declare, has, on, debounce, domConstruct, types,
    utils, ListRenderer, Grid, DefaultActions, Defaults, Layout, Focus,
    OnDemandGrid, EventedMixin, factory, CIViewMixin, BlockGrid,
    Command, Variable,
    _Accordion, TemplatedWidgetBase,
    all, Deferred, KeyboardNavigation,
    Search, _Widget, _LayoutMixin, Docker, ConsoleView,
    LogGrid, ExpressionEditor, module, ExpressionConsole, History,
    ThumbRenderer, BlockEditView, FilePreview, _CIPanelDialog, EditBox, _CIDialog, TestUtils, require, DriverView_Layout, when, console) {
    ThumbRenderer.prototype.renderRow = function (obj) {
        if (obj.renderRow) {
            const _res = obj.renderRow.apply(this, [obj]);
            if (_res) {
                return _res;
            }
        }
        const thiz = this;

        const div = domConstruct.create('div', {
            className: "tile widget"
        });

        const icon = obj.icon;
        const isBack = false;
        const directory = obj.getChildren().length > 0;
        const useCSS = true;
        let label = '';
        const isImage = false;
        const no_access = false;


        this._doubleWidthThumbs = true;

        const iconStyle = 'color:inherit;margin-left:auto;float:left;text-align:left;left:10%;top:23%;text-shadow: 2px 2px 5px rgba(0,0,0,0.3);opacity: 0.7';
        const contentClass = 'icon';

        label = obj.name;


        const imageClass = obj.icon ? (obj.icon + ' fa-3x') : 'fa fa-folder fa-5x';

        const folderContent = '<span style="' + iconStyle + '" class=" ' + imageClass + '"></span>';

        const detailTop = obj.toText(true, true, false, false);
        let detailBottom = obj.toText(false, false, true, false);
        let detailBreak = '<br/> <br/>';
        if (detailTop.indexOf(detailBottom) !== -1) {
            detailBottom = '';
            detailBreak = '';
        }
        const statusIcon = obj.getStatusIcon ? obj.getStatusIcon() : obj._statusIcon;
        const html = '<div title="' + label + '" class="tile-content ' + contentClass + '">' + folderContent + '</div>' +

            '<div class="brand bg-dark opacity ellipsis" style="text-align:left;min-height: 50%;margin-left: 2px;margin-bottom: 2px;margin-right: 2px;padding: 4px;">' + detailTop +
            detailBreak + detailBottom +
            '</div>' +

            '<div class="tile-status">' +
            '<span style="right:1px" class="badge bg-transparent text-muted">' + obj.getChildren().length + '</span>' +
            '</div>' +

            '<div class="tile-status-top">' +
            '<div style="top:1px;right:20px" class="badge bg-transparent">' +
            '<span style="font-size: 140%"  class="text-success fa-play"></span>' +
            '<span style="font-size: 140%;margin-left: 8px" class="text-info fa-cogs "></span>' +
            (statusIcon ? '<span style="font-size: 110%;margin-left: 8px" class="' + statusIcon + '"></span>' : '') +
            '</div>' +
            '</div>' +
            '</div>';

        if (isImage || this._doubleWidthThumbs) {
            $(div).addClass('double');
        }
        if (useCSS) {
            div.innerHTML = html;
            return div;
        }
        return div;
    };

    //playground
    const ctx = window.sctx;

    const ACTION = types.ACTION;
    let root;
    let scope;
    let blockManager;
    let driverManager;
    const doTests = true;

    let view = null;
    const layoutData = null;

    function createGridClass() {
        const _debugHightlighting = false,
            _debugRun = false;

        return declare('DriverBlockGridClass', BlockGrid, {
            _console: null,
            _lastConsoleItem: null,
            toolbarInitiallyHidden: true,
            highlightDelay: 1000,
            propertyStruct: null,
            resizeToParent: false,
            cssClass: 'BlockGrid',
            saveLayout: function () {
                return this.owner.saveLayout();
            },
            restoreLayout: function () {
                return this.owner.restoreLayout();
            },
            formatOrder: function (value, block) {

                if (block) {
                    if (!block.index) {
                        console.log('error ' + block);
                    }
                    const parent = block.getParent ? block.getParent() : null;
                    let _index = block.index ? block.index() : 0;
                    if (parent) {
                        const parents = block.numberOfParents ? block.numberOfParents() : [];
                        let _out = '&nbsp;';
                        for (let i = 0; i < parents; i++) {
                            _out += '&nbsp;';
                        }
                        _out += '';

                        for (let j = 0; j < parents; j++) {
                            _out += '-';
                        }
                        _out += '&nbsp;';
                        _index = _out + _index;
                    } else {
                        return '&nbsp;' + _index;
                    }
                    return _index;
                }

                return '';
            },
            getHistory: function () {
                if (!this._history) {
                    this._history = new History();
                }
                return this._history;
            },
            editBlock2: function (item, changedCB, select) {
                if (!item) {
                    return;
                }
                if (!item.canEdit()) {
                    return;
                }
                let title = 'Edit ';
                const thiz = this;

                title += item.name;
                const cis = item.getFields();
                const self = this;

                function ok(changedCIS) {
                    _.each(changedCIS, function (evt) {
                        thiz.onCIChanged && thiz.onCIChanged(evt.ci, item, evt.oldValue, evt.newValue, evt.ci.dst, cis, evt.props);
                    });
                    self.showProperties(item, true);
                }
                const panel = new _CIPanelDialog({
                    title: title,
                    onOk: function (changedCIS) {
                        ok(changedCIS);
                        this.headDfd.resolve(changedCIS);
                    },
                    onCancel: function (changedCIS) {
                        this.headDfd.resolve(changedCIS);
                    },
                    cis: cis,
                    CIViewClass: BlockEditView,
                    CIViewOptions: {
                        delegate: this,
                        resizeToParent: true,
                        ciSort: false,
                        options: {
                            groupOrder: {
                                'General': 1,
                                'Send': 2,
                                'Advanced': 4,
                                'Description': 5
                            }
                        },
                        cis: cis
                    }
                });
                return panel.show();
            },
            onShowProperties: function (item) {
                const _console = this._console;
                if (_console && item) {
                    const value = item.send || item.method;
                    const editor = _console.getConsoleEditor();
                    editor.set('value', value);
                    this._lastConsoleItem = item;
                }
            },
            /**
             *
             * @param action {Action}
             * @param editor {ACEEditor}
             */
            openTerminal: function (action, editor) {
                const item = this.getSelectedItem() || {};
                // already open
                if (this._console) {
                    utils.destroy(this._console);
                    utils.destroy(this.__bottom);
                    this.__bottom = null;
                    this._console = null;
                    this.getDocker().resize();
                    return;
                }
                const ctx = this.ctx;
                const deviceManager = ctx.getDeviceManager();
                this.getRightPanel('Properties', null, 'DefaultTab', {});
                const bottomPanel = this.getBottomPanel('Console', null, 'DefaultTab', null, false);
                const value = item.send || item.method;
                const _console = deviceManager.openConsole(this.device, bottomPanel, {
                    value: value
                });
                this._lastConsoleItem = item;
                bottomPanel.closeable(false);
                this._console = _console;
            },
            openExpressionConsole: function (action, editor, expressionEditor) {
                const ctx = this.ctx,
                    deviceManager = ctx.getDeviceManager(),
                    docker = ctx.mainView.getDocker(),
                    device = this.device;

                if (!device) {
                    return;
                }

                const cInfo = deviceManager.toDeviceControlInfo(device),
                    where = docker.addTab(null, {
                        title: cInfo.host,
                        icon: 'fa-terminal'
                    }),
                    value = expressionEditor.userData.value;

                const console = utils.addWidget(ExpressionConsole, {
                    ctx: ctx,
                    device: device,
                    value: value
                }, this, where, true);

                this.add(where, null, false);
                return console;
            },
            getTypeMap: function () {
                if (this.typeMap) {
                    return this.typeMap;
                }
                const self = this;

                const ExpressionEditorExtra = dcl(ExpressionEditor, {
                    declaredClass: 'xide.widgets.ExpressionEditor2',
                    EditorClass: dcl(ConsoleView.Editor, {
                        permissions: [
                            ACTION.SAVE,
                            ACTION.TOOLBAR,
                            types.ACTION.FULLSCREEN
                        ],
                        runAction: function (action) {
                            if (action.command === 'Console/Terminal') {
                                return self.openTerminal(action, this);
                            }
                            if (action.command === 'Console/Editor') {
                                return self.openExpressionConsole(action, this, this.delegate.delegate);
                            }
                            if (action.command === 'Console/Send') {
                                const editor = this.getEditor(),
                                    value = editor.getValue(),
                                    delegate = this.delegate.delegate,
                                    block = delegate.item;

                                if (block) {
                                    return block.solve(null, {
                                        highlight: true
                                    }, true, value);
                                }
                            }
                            return this.inherited(arguments);
                        }
                    }),
                    onAddEditorActions: function (evt) {
                        const actions = evt.actions,
                            owner = evt.owner,
                            mixin = {
                                addPermission: true
                            },
                            _reload = _.find(actions, {
                                command: 'File/Reload'
                            });

                        actions.remove(_reload);
                        actions.push(owner.createAction({
                            label: 'Wizard',
                            command: 'Console/Editor',
                            icon: 'fa-magic',
                            group: 'Console',
                            tab: 'Home',
                            mixin: mixin,
                            handler: function () {

                            }
                        }));
                    },
                    onCreatedEditor: function () {},
                    onCreatedConsole: function () {}
                });

                const typeMap = {};

                typeMap['xide.widgets.ExpressionEditor'] = ExpressionEditorExtra;
                this.typeMap = typeMap;

            },
            showProperties: function (item, force) {
                const block = item || this.getSelection()[0],
                    thiz = this,
                    rightSplitPosition = thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);

                if (!block || rightSplitPosition === 1) {
                    console.log(' show properties: abort', [block, rightSplitPosition]);
                    return;
                }

                const right = this.getRightPanel('Properties', null, 'DefaultTab', {});
                right.closeable(false);
                let props = this.getPropertyStruct();
                if (block === props._lastItem && force !== true) {
                    return;
                }
                if (props) {
                    if (props.currentCIView) {
                        this._widgets.remove(props.currentCIView);
                    }

                    if (props.targetTop) {
                        this._widgets.remove(props.targetTop);
                    }
                }

                this.clearPropertyPanel();

                props = this.getPropertyStruct();
                props._lastItem = item;

                function propertyTabShown(tab, show) {
                    if (!tab) {
                        return;
                    }
                    const title = tab.title;
                    if (show) {
                        props.lastSelectedTopTabTitle = title;

                    } else if (!show && props.lastSelectedTopTabTitle === title) {
                        props.lastSelectedTopTabTitle = null;
                    }
                }

                let tabContainer = props.targetTop;
                if (!tabContainer) {
                    tabContainer = utils.addWidget(_Accordion, {
                        scrollable: true,
                        resizeToParent: true
                    }, null, right.containerNode, true);

                    $(tabContainer.domNode).addClass('CIView Accordion');
                    props.targetTop = tabContainer;
                    tabContainer._on('show', function (evt) {
                        propertyTabShown(evt.view, true);
                    });
                    tabContainer._on('hide', function (evt) {
                        propertyTabShown(evt.view, false);
                    });
                    $(right.containerNode).css('overflow-y', 'auto');
                    this.add(tabContainer);
                    $(tabContainer.domNode).css('width', $(right.containerNode).width() - 10);
                }
                _.each(tabContainer.getChildren(), function (tab) {
                    tabContainer.removeChild(tab);
                });

                if (props.currentCIView) {
                    props.currentCIView.empty();
                }

                if (!block.getFields) {
                    console.log('have no fields', block);
                    return;
                }

                const cis = block.getFields();
                for (let i = 0; i < cis.length; i++) {
                    cis[i] && (cis[i].vertical = true);
                }

                const ciView = new CIViewMixin({
                    getTypeMap: function () {
                        return thiz.getTypeMap();
                    },
                    ciSort: false,
                    tabContainerClass: _Accordion,
                    tabContainer: props.targetTop,
                    delegate: this,
                    viewStyle: 'padding:0px;',
                    autoSelectLast: true,
                    item: block,
                    source: this.callee,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Advanced': 2,
                            'Script': 3,
                            'Arguments': 4,
                            'Description': 5,
                            'Share': 6

                        }
                    },
                    cis: cis
                });
                ciView.initWithCIS(cis);
                props.currentCIView = ciView;
                this.add(ciView);
                if (block.onFieldsRendered) {
                    block.onFieldsRendered(block, cis);
                }
                setTimeout(function () {
                    ciView._on('valueChanged', function (evt) {
                        setTimeout(function () {
                            thiz.onCIChanged && thiz.onCIChanged(evt.ci, block, evt.oldValue, evt.newValue, evt.ci.dst, cis, props);
                        }, 10);
                    });
                }, 100);
                const tabToOpen = tabContainer.getTab(props.lastSelectedTopTabTitle);
                if (tabToOpen) {
                    tabContainer.selectChild(props.lastSelectedTopTabTitle, true);
                } else {
                    props.lastSelectedTopTabTitle = null;
                }
                this.setPropertyStruct(props);
                this.onShowProperties(block);
            },
            execute: function (_blocks) {
                const thiz = this;

                const //all Deferreds of selected blocks to run
                    dfds = [];

                let handles = [];

                const //shortcut
                    EVENTS = types.EVENTS;

                const //normalize selection to array
                    blocks = _.isArray(_blocks) ? _blocks : [_blocks];


                function run(block) {

                    const _runHandle = block._on(EVENTS.ON_RUN_BLOCK, function (evt) {
                            _debugHightlighting && console.error('active');
                            thiz.mark(thiz.toNode(evt), 'activeBlock', block, evt.timeout);
                        }),
                        //event handle "Success"
                        _successHandle = block._on(EVENTS.ON_RUN_BLOCK_SUCCESS, function (evt) {
                            _debugHightlighting && console.log('marke success', evt);
                            thiz.mark(thiz.toNode(evt), 'successBlock', block, evt.timeout);

                        }),
                        //event handle "Error"
                        _errHandle = block._on(EVENTS.ON_RUN_BLOCK_FAILED, function (evt) {
                            _debugHightlighting && console.error('failed', evt);
                            thiz.mark(thiz.toNode(evt), 'failedBlock', block, evt.timeout);
                        });

                    _debugRun && console.error('run block ');

                    if (!block || !block.scope) {
                        console.error('have no scope');
                        return;
                    }

                    try {
                        const blockDfd = block.scope.solveBlock(block, {
                            highlight: true,
                            force: true,
                            listener: thiz
                        }, true);

                        dfds.push(blockDfd);


                    } catch (e) {
                        logError(e, 'executing block -  ' + block.name + ' failed! : ');
                        thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                            text: 'Error running block:' + e.message,
                            type: 'error'
                        });
                    }
                    handles = handles.concat([_runHandle, _errHandle, _successHandle]);
                    return handles;
                }

                function _patch(block) {

                    block.runFrom = function (_blocks, index, settings) {
                        const thiz = this,
                            blocks = _blocks || this.items,
                            allDfds = [];

                        const onFinishBlock = function (block, results) {
                            block._lastResult = block._lastResult || results;
                            thiz._currentIndex++;
                            thiz.runFrom(blocks, thiz._currentIndex, settings);
                        };

                        const wireBlock = function (block) {
                            block._deferredObject.then(function (results) {
                                console.log('----def block finish');
                                onFinishBlock(block, results);
                            });
                        };

                        if (blocks.length) {
                            for (let n = index; n < blocks.length; n++) {
                                const block = blocks[n];
                                _patch(block);
                                if (block.deferred === true) {
                                    block._deferredObject = new Deferred();
                                    this._currentIndex = n;
                                    wireBlock(block);
                                    allDfds.push(block.solve(this.scope, settings));
                                    break;
                                } else {
                                    allDfds.push(block.solve(this.scope, settings));
                                }
                            }
                        } else {
                            this.onSuccess(this, settings);
                        }
                        return allDfds;
                    };

                    block.solve = function (scope, settings, run, error) {

                        this._currentIndex = 0;
                        this._return = [];

                        const _script = '' + this._get('method');

                        const thiz = this,
                            ctx = this.getContext(),
                            items = this[this._getContainer()],
                            //outer,head dfd
                            dfd = new Deferred(),
                            listener = settings.listener,
                            isDfd = thiz.deferred;

                        //moved to Contains#onRunThis
                        if (listener) {
                            listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                        }

                        //function when a block did run successfully,
                        // moved to Contains#onDidRunItem
                        function _finish(dfd, result, event) {

                            if (listener) {
                                listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                            }
                            dfd.resolve(result);


                        }

                        //function when a block did run successfully
                        function _error(result) {
                            dfd.reject(result);
                            if (listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                            }
                        }


                        //moved to Contains#onDidRunThis
                        function _headDone(result) {


                            //more blocks?
                            if (items.length) {
                                const subDfds = thiz.runFrom(items, 0, settings);

                                all(subDfds).then(function (what) {
                                    console.log('all solved!', what);
                                    _finish(dfd, result);
                                }, function (err) {
                                    console.error('error in chain', err);
                                    _finish(dfd, err);
                                });

                            } else {
                                _finish(dfd, result);
                            }
                        }


                        if (_script && _script.length) {

                            const runScript = function () {

                                const _function = new Function("{" + _script + "}");
                                const _args = thiz.getArgs() || [];
                                try {

                                    if (isDfd) {

                                        ctx.resolve = function (result) {
                                            console.log('def block done');
                                            if (thiz._deferredObject) {
                                                thiz._deferredObject.resolve();
                                            }
                                            _headDone(result);
                                        };
                                    }
                                    const _parsed = _function.apply(ctx, _args || {});
                                    thiz._lastResult = _parsed;
                                    if (run) {
                                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                                    }


                                    if (!isDfd) {
                                        console.log('root block done');
                                        //_headDone(_parsed);
                                        thiz.onDidRunThis(dfd, _parsed, items, settings);
                                    }

                                } catch (e) {
                                    e = e || {};
                                    _error(e);
                                    if (error) {
                                        error('invalid expression : \n' + _script + ': ' + e);
                                    }
                                    //thiz.onFailed(thiz, settings);
                                    //return [];
                                }
                            };

                            if (scope.global) {
                                (function () {
                                    window = scope.global;
                                    const _args = thiz.getArgs() || [];
                                    try {
                                        let _parsed = null;
                                        if (!ctx.runExpression) {
                                            const _function = new Function("{" + _script + "}").bind(this);
                                            _parsed = _function.apply(ctx, _args || {});
                                        } else {
                                            _parsed = ctx.runExpression(_script, null, _args);
                                        }
                                        thiz._lastResult = _parsed;
                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }
                                        if (_parsed !== 'false' && _parsed !== false) {
                                            thiz.onSuccess(thiz, settings);
                                        } else {
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }


                                    } catch (e) {

                                        thiz._lastResult = null;
                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        thiz.onFailed(thiz, settings);
                                        return [];
                                    }

                                }).call(scope.global);

                            } else {
                                runScript();
                            }

                        } else {
                            console.error('have no script');
                        }
                        return dfd;
                    };


                }

                //_.each(blocks,_patch);
                _.each(blocks, run);

                all(dfds).then(function () {
                    _debugRun && console.log('did run all selected blocks!', thiz);
                    // _.invoke(handles, 'remove');
                });
            },
            _itemChanged: function (type, item, store) {

                store = store || this.getStore(item);

                const thiz = this;



                function _refreshParent(item, silent) {
                    const parent = item.getParent();
                    if (parent) {
                        const args = {
                            target: parent
                        };
                        if (silent) {
                            this._muteSelectionEvents = true;
                        }
                        store.emit('update', args);
                        if (silent) {
                            this._muteSelectionEvents = false;
                        }
                    } else {
                        thiz.refresh();
                    }
                }

                function refresh() {
                    thiz.refreshRoot();
                    //thiz.refreshCurrent();
                    thiz.refresh();
                    thiz.refreshCurrent();
                }

                function select(item, reason) {
                    thiz.select(item, null, true, {
                        focus: true,
                        delay: 1,
                        append: false,
                        expand: true
                    }, reason);
                }



                switch (type) {

                    case 'added':
                        {

                            refresh();
                            select(item);
                            break;
                        }
                    case 'enabled':
                        {
                            refresh();
                            select(item);
                            break;
                        }
                    case 'changed':
                        {
                            //refresh();
                            select(item, 'update');
                            break;
                        }
                    case 'moved':
                        {
                            refresh();
                            break;
                        }
                    case 'deleted':
                        {
                            const parent = item.getParent();
                            const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                            if (parent) {
                                const _container = parent.getContainer();
                                if (_container) {
                                    _.each(_container, function (child) {
                                        if (child.id === item.id) {
                                            _container.remove(child);
                                        }
                                    });
                                }
                            }
                            refresh();
                            /*
                             if (_prev) {
                             select(_prev);
                             }
                             */
                            break;
                        }
                }
            },
            save: function () {
                const thiz = this;


                const driver = thiz._owner.driver;

                const ctx = thiz.ctx,
                    fileManager = ctx.getFileManager(),
                    //instance scope
                    scope = thiz.blockScope,
                    instance = scope.instance,
                    //original driver scope
                    originalScope = driver.blockScope,
                    path = driver.path.replace('.meta.json', '.xblox'),
                    scopeToSave = originalScope || scope,
                    mount = driver.scope,
                    isInstance = instance !== null;

                if (isInstance) {
                    //heavy:
                    setTimeout(function () {
                        driver.blockScope && driver.blockScope.fromScope(scope);
                    }, 0);
                }


                if (scope) {

                    const all = {
                        blocks: null,
                        variables: null
                    };
                    const blocks = scope.blocksToJson();
                    try {
                        //test integrity
                        utils.fromJson(JSON.stringify(blocks));
                    } catch (e) {
                        console.error('invalid data');
                        return;
                    }


                    all.blocks = blocks;
                    fileManager.setContent(mount, path, JSON.stringify(all, null, 2), function () {
                        ctx.getDriverManager().saveDriver(driver);
                    })
                    driver.blox.blocks = blocks;
                }
            },
            download: function () {
                const blocks = this.blockScope.blocksToJson();
                const blocksStr = JSON.stringify(blocks);
                utils.download("blocks.json", blocksStr);
                /*
                var w = window.open('', '', 'width=400,height=400,resizeable,scrollbars');
                w.document.write(blocksStr);
                w.document.close(); // needed for chrome and safari
                */

            },
            runAction: function (action) {
                const thiz = this;
                const sel = this.getSelection();

                function addItem(_class, group, parent) {
                    const dfd = new Deferred();

                    let cmd = null;

                    if (_class === Variable) {
                        cmd = factory.createBlock(_class, {
                            name: "No Title",
                            scope: thiz.blockScope,
                            group: group
                        });

                    } else {
                        cmd = thiz.addItem({
                            proto: _class,
                            ctrArgs: {
                                scope: thiz.blockScope,
                                group: group
                            }
                        });
                    }

                    const defaultDfdArgs = {
                        select: [cmd],
                        focus: true,
                        append: false
                    };

                    const ref = thiz.refresh();
                    ref && ref.then && ref.then(function () {
                        dfd.resolve(defaultDfdArgs);
                    });

                    _.each(thiz.grids, function (grid) {
                        if (grid && grid.refreshRoot) {
                            grid.refreshRoot();
                            grid.refresh();
                            grid.refreshCurrent();
                        }
                    });



                    //@todo: remove temp hack for dyna actions
                    /*
                    _.each(thiz.owner.grids,function(grid){
                        grid.onSelectionChanged && grid.onSelectionChanged({
                            selection:[]
                        });
                    });
                    */

                    return dfd;
                }

                if (action.command === 'New/Command') {
                    return addItem(Command, this.blockGroup, sel[0] || this.getCurrentParent());
                }

                if (action.command === 'View/Layout/Save Layout') {
                    return this.saveLayout();
                }
                if (action.command === 'View/Layout/Restore Layout') {
                    return this.restoreLayout();
                }

                if (action.command === ACTION.DOWNLOAD) {
                    return this.download();
                }

                if (action.command === 'Step/Back') {
                    return this.goBack();
                }

                if (action.command === 'New/Variable') {
                    return addItem(Variable, 'basicVariables');
                }
                if (action.command === 'File/Save') {
                    return this.save();
                }
                if (action.command === 'Step/Help') {
                    return this.showHelp(sel[0]);
                }
                return this.inherited(arguments);
            },
            mark2: function (element, cssClass, block, timeout) {

                block = block || {};
                const thiz = this;
                if (element) {


                    if (element._currentHighlightClass === cssClass) {
                        return;
                    }

                    element._currentHighlightClass = cssClass;
                    element.removeClass('failedBlock successBlock activeBlock');
                    element.addClass(cssClass);

                    if (element.hightlightTimer) {
                        clearTimeout(element.hightlightTimer);
                        element.hightlightTimer = null;
                    }

                    if (timeout !== false) {
                        element.hightlightTimer = setTimeout(function () {
                            element.removeClass(cssClass);
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.hightlightTimer = null;
                            element._currentHighlightClass = null;
                        }, thiz.highlightDelay);

                    }
                }
            },
            _observeStore: function (collection, container, options) {
                const self = this;
                const rows = options.rows;
                let row;

                const handles = [
                    collection._on('update', function (event) {
                        const from = event.previousIndex;
                        const to = event.index;

                        if (from !== undefined && rows[from]) {
                            if ('max' in rows && (to === undefined || to < rows.min || to > rows.max)) {
                                rows.max--;
                            }

                            row = rows[from];

                            // check to make the sure the node is still there before we try to remove it
                            // (in case it was moved to a different place in the DOM)
                            if (row.parentNode === container) {
                                self.removeRow(row, false, options);
                            }

                            // remove the old slot
                            rows.splice(from, 1);
                            if (event.type === 'delete' ||
                                (event.type === 'update' && (from < to || to === undefined))) {
                                // adjust the rowIndex so adjustRowIndices has the right starting point
                                rows[from] && rows[from].rowIndex--;
                            }
                        }
                        if (event.type === 'delete') {
                            // Reset row in case this is later followed by an add;
                            // only update events should retain the row variable below
                            row = null;
                        }
                    }),

                    collection._on('update', function (event) {
                        const from = event.previousIndex;
                        const to = event.index;
                        let nextNode;

                        function advanceNext() {
                            nextNode = (nextNode.connected || nextNode).nextSibling;
                        }

                        // When possible, restrict observations to the actually rendered range
                        if (to !== undefined && (!('max' in rows) || (to >= rows.min && to <= rows.max))) {
                            if ('max' in rows && (from === undefined || from < rows.min || from > rows.max)) {
                                rows.max++;
                            }
                            // Add to new slot (either before an existing row, or at the end)
                            // First determine the DOM node that this should be placed before.
                            if (rows.length) {
                                nextNode = rows[to];
                                if (!nextNode) {
                                    nextNode = rows[to - 1];
                                    if (nextNode) {
                                        // Make sure to skip connected nodes, so we don't accidentally
                                        // insert a row in between a parent and its children.
                                        advanceNext();
                                    }
                                }
                            } else {
                                // There are no rows.  Allow for subclasses to insert new rows somewhere other than
                                // at the end of the parent node.
                                nextNode = self._getFirstRowSibling && self._getFirstRowSibling(container);
                            }
                            // Make sure we don't trip over a stale reference to a
                            // node that was removed, or try to place a node before
                            // itself (due to overlapped queries)
                            if (row && nextNode && row.id === nextNode.id) {
                                advanceNext();
                            }
                            if (nextNode && !nextNode.parentNode) {
                                nextNode = document.getElementById(nextNode.id);
                            }
                            rows.splice(to, 0, undefined);
                            row = self.insertRow(event.target, container, nextNode, to, options);
                            self.highlightRow(row);
                        }
                        // Reset row so it doesn't get reused on the next event
                        row = null;
                    }),

                    collection._on('update', function (event) {

                        //console.error('update 3');

                        const from = (typeof event.previousIndex !== 'undefined') ? event.previousIndex : Infinity,
                            to = (typeof event.index !== 'undefined') ? event.index : Infinity,
                            adjustAtIndex = Math.min(from, to);
                        from !== to && rows[adjustAtIndex] && self.adjustRowIndices(rows[adjustAtIndex]);

                        // the removal of rows could cause us to need to page in more items
                        if (from !== Infinity && self._processScroll && (rows[from] || rows[from - 1])) {
                            self._processScroll();
                        }

                        // Fire _onNotification, even for out-of-viewport notifications,
                        // since some things may still need to update (e.g. Pagination's status/navigation)
                        self._onNotification(rows, event, collection);

                        // Update _total after _onNotification so that it can potentially
                        // decide whether to perform actions based on whether the total changed
                        if (collection === self._renderedCollection && 'totalLength' in event) {
                            self._total = event.totalLength;
                        }
                    })
                ];

                return {
                    remove: function () {
                        while (handles.length > 0) {
                            handles.pop().remove();
                        }
                    }
                };
            },
            destroy: function () {
                if (this.collection) {
                    const items = this.collection.query({
                        group: this.blockGroup
                    });
                    _.each(items, function (item) {
                        if (!item._lastSettings) {
                            item._lastSettings = {};
                        }
                        item._lastSettings.highlight = false;
                    });
                }
                if (this._destroyed) {
                    return;
                }
                this._destroyed = true;
                delete this.typeMap;
                if (this._history) {
                    this._history.destroy();
                    delete this._history;
                }
                this.clearPropertyPanel();
                return this.inherited(arguments);
            },
            startup: function () {
                if (this._started) {
                    return;
                }
                const res = this.inherited(arguments);
                const items = this.collection.query({
                    group: this.blockGroup
                });

                _.each(items, function (item) {
                    if (!item._lastSettings) {
                        item._lastSettings = {};
                    }
                    item._lastSettings.highlight = true;
                });
                return res;
            },
            paste: function (items, owner, cut) {
                if (!items) {
                    return;
                }
                const target = this.getSelection()[0];
                let _flatten;
                let source;
                const thiz = this;
                const scope = thiz.blockScope;

                if (!owner) {
                    return; //@TODO : support
                }

                //special case when paste on nothing
                if (!target) {
                    //save parentIds
                    for (let i = 0; i < items.length; i++) {
                        const obj = items[i];
                        if (obj.parentId) {
                            obj._parentOri = '' + obj.parentId;
                            obj.parentId = null;
                        }
                    }
                    //flatten them
                    _flatten = scope.flatten(items);

                    const _flat2 = _.uniq(_flatten, false, function (item) {
                        return item.id;
                    });
                    //clone them
                    const _clones = scope.cloneBlocks2(_flat2, this.newRootItemGroup);

                    const _flattenClones = scope.flatten(_clones);
                    let firstItem = null;
                    for (var j = 0; j < _clones.length; j++) {
                        const clone = _clones[j];
                        if (!firstItem) {
                            firstItem = clone;
                        }
                    }

                    //restore parentIds
                    for (let k = 0; k < items.length; k++) {
                        const obj2 = items[k];
                        if (obj2._parentOri) { //restore
                            obj2.parentId = obj2._parentOri;
                            delete obj2['_parentOri'];
                        }
                    }
                    return _clones;
                }

                const grid = owner;

                const srcScope = items[0].scope;
                const dstScope = target.scope;
                if (srcScope != dstScope) {
                    return;
                }
                const insert = target.canAdd() || false;
                let parent = srcScope.getBlockById(target.parentId);
                if (!parent) {
                    parent = target;
                }
                const targetState = 'Moved';
                const before = false;

                _flatten = scope.flatten(items);

                items = srcScope.cloneBlocks2(_flatten);
                const newItems = [];
                const parentTop = target.getTopRoot();

                for (var j = 0; j < items.length; j++) {

                    source = items[j];
                    const sourceParent = source.getTopRoot();
                    //
                    //Case source=target
                    //
                    if (source == target) {
                        console.log('source=target!');
                    }

                    if (source.parentId) {

                        if (!parentTop) {
                            const _parent = dstScope.getBlockById(source.parentId);

                            //the parent is part of the copy
                            if (_parent && _.find(items, {
                                    id: _parent.id
                                })) {

                            } else {
                                source.parentId = null;
                            }
                        }
                    }


                    if (source.parentId && parentTop) {
                        const sourceTop = source.getTopRoot();
                        //same roots

                        if (sourceTop.id === parentTop.id) {
                            //source.parentId = null;
                        }

                        if (sourceTop.id && parentTop.id && (sourceTop.id !== target.id)) {
                            source.parentId = null;
                        }
                    }

                    if (source.parentId) {
                        newItems.push(source);
                    } else {
                        source.parentId = parent.id;
                        parent.add(source);
                        const nItem = this.onDrop(source, target, before, grid, targetState, insert);
                        newItems.push(nItem);
                    }
                }
                return newItems;
            }
        });
    }

    const DriverViewClass = dcl([_Widget, DriverView_Layout, _LayoutMixin.dcl], {
        templateString: '<div style="with:inherit;height: inherit"/>',
        showBasicCommands: true,
        showConditionalCommands: false,
        showResponseBlocks: true,
        showLog: true,
        showConsole: true,
        showVariables: true,
        showSettings: true,
        showInitTab: true,
        logTab: null,
        consoleTab: null,
        basicCommandsTab: null,
        conditionalTab: null,
        variablesTab: null,
        initGrid: null,
        initTab: null,
        responseTab: null,
        settingsTab: null,
        gridClass: null,
        cssClass: 'Driver',
        completeGrid: function (grid, newItemLabel) {
            grid.userData = this.userData;
            const DriverModuleBlocks = [];

            function _completeGrid(_grid) {
                _grid._highlight = true;
                _grid.once('onAddActions', function (evt) {
                    const cmdAction = 'New/Command',
                        varAction = 'New/Variable',
                        result = evt.actions,
                        thiz = this,
                        defaultMixin = {
                            addPermission: true
                        };

                    result.push(thiz.createAction({
                        label: 'Highlight',
                        command: 'View/Show/Highlight',
                        icon: 'fa-on',
                        tab: 'View',
                        group: 'Show',
                        mixin: {
                            addPermission: true,
                            closeOnClick: false,
                            actionType: 'multiToggle'
                        },
                        onCreate: function (action) {
                            action.value = _grid._highlight === true;
                        },
                        onChange: function (property, value) {
                            _grid._highlight = value;
                        }
                    }));

                    result.push(thiz.createAction({
                        label: 'Command',
                        command: cmdAction,
                        icon: 'fa-plus',
                        tab: 'Home',
                        group: 'Insert',
                        mixin: utils.mixin({
                            quick: true
                        }, defaultMixin)
                    }));

                    result.push(thiz.createAction({
                        label: 'Variable',
                        command: varAction,
                        icon: 'fa-code',
                        tab: 'Home',
                        group: 'Insert',
                        mixin: utils.mixin({
                            quick: true
                        }, defaultMixin)
                    }));


                    result.push(thiz.createAction({
                        label: 'Save Layout',
                        command: 'View/Layout/Save Layout',
                        icon: 'fa-save',
                        tab: 'Home',
                        group: 'View',
                        mixin: defaultMixin
                    }));


                    result.push(thiz.createAction({
                        label: 'Restore Layout',
                        command: 'View/Layout/Restore Layout',
                        icon: 'fa-folder-open',
                        tab: 'Home',
                        group: 'View',
                        mixin: defaultMixin
                    }));


                });

                _grid._on('selectionChanged', function (evt) {
                    //since we can only add blocks to command and not
                    //at root level, disable the 'Block/Insert' root action and
                    //its widget //references
                    const thiz = this;

                    const selection = evt.selection;
                    let item = selection[0];
                    const blockInsert = thiz.getAction('Step/Insert');
                    const blockEnable = thiz.getAction('Step/Enable');
                    const newCommand = thiz.getAction('New/Command');

                    const disable = function (disable) {
                        blockInsert.set('disabled', disable);
                        setTimeout(function () {
                            blockInsert.getReferences().forEach(function (ref) {
                                ref.set('disabled', disable);
                            });
                        }, 100);

                    };
                    const history = thiz.getHistory();
                    const now = history.getNow();
                    if (!item) {
                        item = thiz.getCurrentParent();
                    }

                    let _disable = item ? false : true;
                    if (_grid.blockGroup === 'conditionalProcess') {
                        _disable = false;
                        newCommand.set('disabled', true);
                        setTimeout(function () {
                            newCommand.getReferences().forEach(function (ref) {
                                ref.set('disabled', true);
                            });
                        }, 100);
                    }

                    if (_grid.blockGroup === 'conditional') {
                        _disable = false;
                    }
                    if (_grid.blockGroup === 'basic') {
                        _disable = false;
                    }
                    disable(_disable);
                    if (item) {
                        blockEnable.value = item.enabled;
                    }
                });

            }
            _completeGrid(grid);
            this.add(grid);
            if (grid.registerView) {
                this.ctx.getWindowManager().registerView(grid, false);
            }
            return grid;
        },
        createGrid: function (tab, scope, store, group, newItemLabel, options) {
            let grid;
            const gridClass = this.getGridClass();
            const grids = this.grids;

            if (!tab) {
                console.error('invalid tab', options);
                return null;
            }
            const args = utils.mixin({
                device: this.device,
                driver: this.driver,
                owner: this,
                _owner: this,
                ctx: this.ctx,
                blockScope: scope,
                toolbarInitiallyHidden: true,
                blockGroup: group,
                attachDirect: true,
                shouldTrackCollection: true,
                resizeToParent: false,
                testt: 'xxxx ' + utils.createUUID(),
                propertyStruct: {
                    currentCIView: null,
                    targetTop: null,
                    _lastItem: null
                },
                collection: store.filter({
                    group: group
                }),
                onCIChanged: function (ci, block, oldValue, newValue, field, cis) {
                    if (oldValue === newValue) {
                        return;
                    }

                    const _col = this.collection;
                    block = this.collection.getSync(block.id);
                    if (field.indexOf('.') === -1) {
                        block.set(field, newValue);
                    }

                    this.refreshActions();
                    utils.setAt(block, field, newValue);

                    _col.emit('update', {
                        target: block,
                        type: 'update',
                        value: newValue
                    });
                    block._store.emit('update', {
                        target: block,
                        type: 'update',
                        value: newValue
                    });
                    this._itemChanged('changed', block, _col);

                    block.onChangeField(field, newValue, cis, this);

                    const cell = this.cell(block.id, '1');
                    if (cell) {
                        this.refreshCell(cell).then(function (e) {});
                    }
                    if (block.declaredClass.indexOf('model.Variable') !== -1) {
                        this.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, {
                            item: block,
                            scope: block.scope
                        });
                    }

                }
            }, options);
            try {
                grid = utils.addWidget(gridClass, args, null, tab, false);
                this.completeGrid(grid, newItemLabel);
                grids.push(grid);
            } catch (e) {
                logError(e);
            }
            tab.add(grid);
            return grid;
        },
        getGridClass: function (owner) {
            if (this.gridClass) {
                return this.gridClass;
            }
            this.gridClass = createGridClass(this);
            return this.gridClass;
        },
        createConsole: function (tab) {
            const view = this.ctx.getDeviceManager().openConsole(this.device, tab);
            this.consoleView = view;
            this.add(view);
            tab.add(view);
        },
        openLog: function (_item, where) {

            // default grid args
            const thiz = this,
                ctx = thiz.ctx,
                deviceManager = ctx.getDeviceManager(),
                logManager = ctx.getLogManager(),
                store = logManager.store,
                item = _item || this.device,
                info = item.info || deviceManager.toDeviceControlInfo(item),
                host = utils.replaceAll('/', '_', info.host);


            const pathSep = has('windows') ? '\\' : '/';
            let storeId = null;
            if ( true ) {
                storeId = info.deviceScope + pathSep + 'logs' + pathSep + host + '_' + info.port + '_' + info.protocol + '.json';
            } else {
                storeId = info[info.deviceScope] + pathSep + 'logs' + pathSep + host + '_' + info.port + '_' + info.protocol + '.json';
            }

            logManager.getStore(storeId).then(function (_store) {
                const logGridArgs = {
                    ctx: ctx,
                    storeId: storeId,
                    delegate: logManager,
                    collection: _store,
                    info: info,
                    isMyLog: function (evt) {
                        return evt.data && evt.data.device && evt.data.device.id === this.info.id;
                    },
                    __collection: _store.filter({}).sort([{
                        property: 'time',
                        descending: true
                    }])
                };

                let start = false;
                if (thiz.isSingle) {
                    start = true;
                }
                const grid = utils.addWidget(LogGrid, logGridArgs, null, where, start, 'logGridView');
                thiz.add(grid, null, false);
                thiz.grids.push(grid);
                where.add(grid, null, false);
            });

        },
        createSettingsWidget: function (ci, where) {
            const thiz = this;
            const parent = $(where.containerNode);
            parent.css({
                'overflow-y': 'auto'
            });
            const settingsWidget = dcl(TemplatedWidgetBase, {
                templateString: '<div style="height:auto;padding: 16px"/>',
                _getText: function (url) {
                    let result;
                    const def = dojo.xhrGet({
                        url: url,
                        sync: true,
                        handleAs: 'text',
                        load: function (text) {
                            result = text;
                        }
                    });
                    return '' + result + '';
                },
                resizeToParent: false,
                startup: function () {

                    const params = utils.getJson(ci['params']);
                    const settings = utils.getJson(ci['params']) || {
                        constants: {
                            start: '',
                            end: ''
                        },
                        send: {
                            mode: false,
                            interval: 500,
                            timeout: 500,
                            onReply: ''
                        }
                    };

                    const settingsPane = utils.templatify(
                        null,
                        this._getText(require.toUrl('xcf/widgets/templates/commandSettingsNew2.html')),
                        this.domNode, {
                            baseClass: 'settings',
                            start: settings.constants.start,
                            end: settings.constants.end,
                            interval: settings.send.interval,
                            timeout: settings.send.timeout,
                            sendMode: settings.send.mode,
                            onReply: settings.send.onReply,
                            settings: settings
                        }, null
                    );
                    if (settings.send.mode) {
                        $(settingsPane.rReply).prop('checked', true);
                    } else {
                        $(settingsPane.rInterval).prop('checked', true);
                    }

                    const _onSettingsChanged = function () {
                        //update params field of our ci
                        ci['params'] = JSON.stringify(settingsPane.settings);
                        thiz.ctx.getDeviceManager().onDriverSettingsChanged(ci, thiz.driver);
                    };



                    //start
                    $(settingsPane.start).on('change', function (e) {
                        settingsPane.settings.constants.start = e.target.value;
                        _onSettingsChanged();
                    });

                    //end
                    $(settingsPane.end).on('change', function (e) {
                        settingsPane.settings.constants.end = e.target.value;
                        _onSettingsChanged();
                    });

                    //mode
                    $(settingsPane.rReply).on("change", function (e) {
                        const value = e.target.value === 'on' ? 1 : 0;
                        settingsPane.settings.send.mode = value;
                        _onSettingsChanged();
                    });

                    $(settingsPane.rInterval).on("change", function (e) {
                        const value = e.target.value === 'on' ? 0 : 1;
                        settingsPane.settings.send.mode = value;
                        _onSettingsChanged();
                    });


                    //interval time
                    $(settingsPane.wInterval).on('change', function (e) {
                        settingsPane.settings.send.interval = e.target.value;
                        _onSettingsChanged();
                    });

                    //on reply value
                    $(settingsPane.wOnReply).on('change', function (e) {

                        settingsPane.settings.send.onReply = e.target.value;
                        _onSettingsChanged();
                    });

                    //onReply - timeout
                    $(settingsPane.wTimeout).on('change', function (e) {
                        settingsPane.settings.send.timeout = e.target.value;
                        _onSettingsChanged();
                    });
                }
            });

            const widget = utils.addWidget(settingsWidget, {
                userData: ci,
                resizeToParent: false
            }, null, where, true);

            where.add(widget, null, false);

        },
        createResonseSettingsWidget: function (ci, where) {
            const thiz = this;
            const parent = $(where.containerNode);
            parent.css({
                'overflow-y': 'auto'
            });
            const settingsWidget = dcl(TemplatedWidgetBase, {
                templateString: '<div style="padding: 16px"/>',
                _getText: function (url) {
                    let result;
                    const def = dojo.xhrGet({
                        url: url,
                        sync: true,
                        handleAs: 'text',
                        load: function (text) {
                            result = text;
                        }
                    });
                    return '' + result + '';
                },
                startup: function () {
                    const params = utils.getJson(ci['params']);
                    const settings = utils.getJson(ci['params']) || {
                        start: false, //cbStart
                        startString: 'test', //wStartString
                        cTypeByte: false, //cTypeByte
                        cTypePacket: false, //cTypePacket,
                        cTypeDelimiter: true,
                        cTypeCount: false,
                        delimiter: '',
                        count: 10
                    };
                    const settingsPane = utils.templatify(
                        null,
                        this._getText(require.toUrl('xcf/widgets/templates/responseSettingsNew2.html')),
                        this.domNode, {
                            baseClass: 'settings',
                            rID: this.id,
                            startString: settings.startString,
                            start: settings.start ? 'true' : 'false',
                            delimiter: settings.delimiter,
                            count: settings.count,
                            cTypeByte: settings.cTypeByte ? 'true' : 'false',
                            cTypePacket: settings.cTypePacket ? 'true' : 'false',
                            cTypeDelimiter: settings.cTypeDelimiter ? 'true' : 'false',
                            cTypeCount: settings.cTypeCount ? 'true' : 'false',
                            settings: settings
                        }, null
                    );
                    if (settings.cTypeByte) {
                        $(settingsPane.cTypeByte).prop('checked', true);
                    } else if (settings.cTypePacket) {
                        $(settingsPane.cTypePacket).prop('checked', true);
                    } else if (settings.cTypeDelimiter) {
                        $(settingsPane.cTypeDelimiter).prop('checked', true);
                    } else if (settings.cTypeCount) {
                        $(settingsPane.cTypeCount).prop('checked', true);
                    }
                    const _onSettingsChanged = function () {
                        //update params field of our ci
                        ci['params'] = JSON.stringify(settingsPane.settings);
                    };
                    //start
                    $(settingsPane.wStartString).on('change', function (e) {
                        settingsPane.settings.startString = e.target.value;
                        _onSettingsChanged();
                    });


                    //delimiter
                    $(settingsPane.wDelimiter).on('change', function (e) {
                        settingsPane.settings.delimiter = e.target.value;
                        _onSettingsChanged();
                    });

                    //count
                    $(settingsPane.wCount).on('change', function (e) {
                        settingsPane.settings.count = e.target.value;
                        _onSettingsChanged();
                    });

                    //per byte
                    $(settingsPane.cTypeByte).on("change", function (e) {
                        const value = e.target.value === 'on' ? 1 : 0;
                        settingsPane.settings.cTypeByte = value;
                        settingsPane.settings.cTypePacket = 0;
                        settingsPane.settings.cTypeDelimiter = 0;
                        settingsPane.settings.cTypeCount = 0;
                        _onSettingsChanged();

                    });

                    //per packet
                    $(settingsPane.cTypePacket).on("change", function (e) {
                        const value = e.target.value === 'on' ? 1 : 0;
                        settingsPane.settings.cTypePacket = value;
                        settingsPane.settings.cTypeByte = 0;
                        settingsPane.settings.cTypeDelimiter = 0;
                        settingsPane.settings.cTypeCount = 0;
                        _onSettingsChanged();
                    });


                    //per delimiter
                    $(settingsPane.cTypeDelimiter).on("change", function (e) {
                        const value = e.target.value === 'on' ? 1 : 0;
                        settingsPane.settings.cTypeDelimiter = value;
                        settingsPane.settings.cTypeByte = 0;
                        settingsPane.settings.cTypePacket = 0;
                        settingsPane.settings.cTypeCount = 0;
                        _onSettingsChanged();
                    });

                    //per count
                    $(settingsPane.cTypeCount).on("change", function (e) {
                        const value = e.target.value === 'on' ? 1 : 0;
                        settingsPane.settings.cTypeCount = value;
                        settingsPane.settings.cTypeByte = 0;
                        settingsPane.settings.cTypePacket = 0;
                        settingsPane.settings.cTypeDelimiter = 0;
                        _onSettingsChanged();

                    });
                }
            });

            const widget = utils.addWidget(settingsWidget, {
                userData: ci,
                resizeToParent: true
            }, null, where, true);

            where.add(widget, null, false);
        },
        getState: function () {

            const ctx = this.ctx;
            const settingsStore = ctx.getSettingsManager().getStore() || {
                getSync: function () {}
            };

            /*
            var grids = [this.basicCommandsGrid,this.responseGrid,this.variablesGrid];
            var ok = true;
            for (var i = 0; i < grids.length; i++) {
                var grid = grids[i];
                if(!grid){
                    ok = false;
                }
            }

            if(!ok){
                return;
            }

            for (var i = 0; i < grids.length; i++) {
                var grid = grids[i];
                if(!state[grid.blockGroup]) {
                    state[grid.blockGroup] = grid.getState();
                }else{
                    console.error('already have '+grid.blockGroup);
                }

            }
            ctx.getSettingsManager().write2(null, '.',
                {
                    id: 'driverView'
                },{
                    value:state
                }, true, null
            ).then(function(){

            })

            */
            const props = settingsStore.getSync('driverView');

            return props || {
                value: {
                    layouts: {}
                }
            };

        },
        createWidgets: function () {
            const self = this,
                ci = self.userData,
                ctx = this.ctx,
                device = self.device,
                driver = self.driver;
            //original or device instance

            //var scope = device && device.blockScope ? device.blockScope : driver.blockScope;
            const scope = device && device.getBlockScope() ? device.getBlockScope() : driver.blockScope || ctx.getDriverManager().createDriverBlockScope(driver);
            if (!scope) {
                console.error('have no scope!');
                return;
            }
            const store = scope.blockStore;
            let DriverModuleBlocks = [];
            if (this.driverModule && this.driverModule.getNewBlocks) {
                DriverModuleBlocks = this.driverModule.getNewBlocks();
                this.subscribe(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, function (evt) {
                    const owner = evt.owner;
                    if ((owner.device != device) || owner.driver != driver) {
                        return;
                    }
                    //console.log('get blocks : ',evt);
                    this.driverModule.getNewBlocks(evt.scope, evt.view, evt.item, evt.group, evt.items);
                });
            }

            this.grids = [];

            if (this.showBasicCommands && this.basicCommandsTab) {
                this.basicCommandsGrid = this.createGrid(this.basicCommandsTab, scope, store, 'basic', 'Command', {
                    registerView: true
                });
            }


            if (this.showConditionalCommands && this.conditionalTab) {
                this.conditionalCommandsGrid = this.createGrid(this.conditionalTab, scope, store, 'conditional', 'Command', {
                    registerView: true
                });
            }

            if (this.showInitTab && this.initTab) {
                this.initGrid = this.createGrid(this.initTab, scope, store, 'init', null, {
                    registerView: true
                });
            }

            //this.orphanGrid = this.createGrid(this.orhpanCommandsTab, scope, store, 'Orphan', 'Command', {
            //    registerView: true
            //});

            //console.log('orhpan', scope.getBlocks({
            //    group: 'Orhpan'
            //}))

            if (this.showVariables && this.variablesTab) {
                this.variablesGrid = this.createGrid(this.variablesTab, scope, store, 'basicVariables', 'Command', {
                    columns: [{
                            label: "Name",
                            field: "name",
                            sortable: true,
                            width: '20%',
                            editorArgs: {
                                required: true,
                                promptMessage: "Enter a unique variable name",
                                //validator: thiz.variableNameValidator,
                                //delegate: thiz.delegate,
                                intermediateChanges: false
                            },
                            formatter: function (value, block) {
                                let blockText = '';
                                if (block.toText) {
                                    blockText = block.toText();
                                    return '<span> ' + blockText + '</span>';
                                }
                                return value;
                            }
                        },
                        {

                            label: "Value",
                            field: "value",
                            sortable: false,
                            editOn: 'click',
                            formatter: function (value, block) {
                                const blockText = '';
                                return utils.markHex(value, "", " ");
                            },
                            editorArgs: {
                                autocomplete: 'on',
                                templateString: '<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"' +
                                    '><div class="dijitReset dijitInputField dijitInputContainer"' +
                                    '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"' +
                                    '${!nameAttrSetting} type="${type}"' +
                                    '/></div' +
                                    '></div>'
                            }
                        }
                    ],
                    registerView: true
                });

                this.variablesTab && this.variablesTab.select();

                $(this.variablesGrid && this.variablesGrid.domNode).addClass('VariablesGrid');

                this.variablesGrid && this.variablesGrid.on("dgrid-datachange", function (evt) {
                    console.error('variables changed', evt);
                });
            }

            this.responseTab && (this.responseGrid = this.createGrid(this.responseTab, scope, store, 'conditionalProcess', 'Variable', {
                _columns: [{
                        label: "Name",
                        field: "name",
                        sortable: true,
                        width: '20%',
                        editorArgs: {
                            required: true,
                            promptMessage: "Enter a unique variable name",
                            intermediateChanges: false
                        }

                    },
                    {
                        label: "Initialize",
                        field: "initial",
                        sortable: false,

                        editOn: 'click'
                    },
                    {
                        label: "Value",
                        field: "value",
                        sortable: false,
                        editOn: 'click',
                        formatter: function (value, object) {
                            return value;
                        },
                        editorArgs: {
                            autocomplete: 'on',
                            templateString: '<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"' +
                                '><div class="dijitReset dijitInputField dijitInputContainer"' +
                                '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"' +
                                '${!nameAttrSetting} type="${type}"' +
                                '/></div' +
                                '></div>'
                        }
                    }
                ],
                registerView: true
            }));

            if (this.showLog && !has('fiddle') && this.logTab) {
                if (this.device) {
                    //try because this doesn't work in jsfiddle
                    try {
                        this.logGrid = this.openLog(this.device, this.logTab);
                    } catch (e) {}
                }
            }

            this.showBasicCommands && this.basicCommandsTab && this.basicCommandsTab.select();
            if (this.showSettings && this.settingsTab) {
                this.createSettingsWidget(ci, this.settingsTab);
                const responseCI = utils.getCIByChainAndName(this.driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES);
                this.createResonseSettingsWidget(responseCI, this.settingsTab);
            }

            if (this.responseGrid) {
                self.grids.push(self.responseGrid);
            }
            if (this.conditionalCommandsGrid) {
                self.grids.push(self.conditionalCommandsGrid);
            }


            if (this.variablesGrid) {
                self.grids.push(self.variablesGrid);
            }

            if (this.basicCommandsGrid) {
                self.grids.push(self.basicCommandsGrid);
            }

            //self.grids.push(this.orphanGrid);
            
            
            if (this.showConsole && this.device && !has('fiddle') && self.consoleTab) {
                setTimeout(function () {
                    const _console = self.createConsole(self.consoleTab);
                    self.add(_console, null, false);
                }, 2000);
            }


            _.each(this.grids, function (grid) {
                grid.grids = self.grids;
            });

            //manually trigger resize on property panels
            _.each([this.basicCommandsTab, this.responseTab, this.variablesTab, this.logTab], function (tab) {
                if (tab) {
                    tab.on(types.DOCKER.EVENT.VISIBILITY_CHANGED, function (visible) {
                        if (visible && tab._widgets) {
                            const _grid = tab._widgets[0];
                            if (_grid) {

                                const props = _grid.getPropertyStruct ? _grid.getPropertyStruct() : {};
                                if (props.targetTop) {
                                    props.targetTop.resize();
                                }
                                if (props.currentCIView) {
                                    props.currentCIView.resize();
                                }
                            }
                        }
                    });
                }
            });

            function save() {
                const state = this.getState();
                const value = state.value;
                const grids = [this.basicCommandsGrid, this.responseGrid, this.variablesGrid];
                let ok = true;
                for (let i = 0; i < grids.length; i++) {
                    const grid = grids[i];
                    if (!grid) {
                        ok = false;
                    }
                }
                if (!ok) {
                    return;
                }
                for (let k = 0; k < grids.length; k++) {
                    const grid2 = grids[k];
                    value[grid2.blockGroup] = grid2.getState();
                }
                ctx.getSettingsManager().write2(null, '.', {
                    id: 'driverView'
                }, {
                    value: value
                }, true, null).then(function () {

                });
            }
            _.each([this.basicCommandsGrid, this.responseGrid, this.variablesGrid], function (grid) {
                if (grid) {
                    grid._on(types.EVENTS.ON_AFTER_ACTION, function (action) {
                        if (action.command === types.ACTION.TOOLBAR ||
                            action.command === types.ACTION.HEADER) {
                            try {
                                save.apply(self);
                            } catch (e) {}
                        }
                    });
                }
            });

            const settingsStore = ctx.getSettingsManager().getStore() || {
                getSync: function () {}
            };

            if (this.responseTab) {
                this.responseTab.select();
            }

            if (this.responseGrid) {
                this.responseGrid._on('showToolbar', function (toolbar) {

                    toolbar.$navBar.append('<input style="display: inline-block;width:100px;margin: 2px;" name="search" placeholder="Emit value" class="searchInput form-control input-transparent" type="text">');
                    const input = toolbar.$navBar.find('.searchInput');
                    this.searchInput = $(input);

                    const btn = factory.createSimpleButton('', 'fa-play', 'form-control input-transparent btn-sm btn-warning', null, toolbar.$navBar[0]);
                    $(btn).css('display', 'inline-block');
                    $(btn).css('width', '25px');

                    function send() {
                        const msg = utils.convertAllEscapes($(input).val(), 'none');
                        if (device) {
                            ctx.getDeviceManager().onDeviceServerMessage({
                                data: {
                                    data: {
                                        device: device.info,
                                        driverInstance: device.driverInstance,
                                        deviceMessage: msg,
                                        bytes: utils.stringToBufferStr(msg)
                                    }
                                }
                            });
                        }
                    }

                    $(input).keyup(function (e) {
                        if (e.keyCode === 13) {
                            send();
                        }
                    });
                    $(btn).on('click', function () {
                        send();
                    });
                });
            }


            if (settingsStore) {
                const state = this.getState();
                const props = state.value;
                if (props) {
                    if (props.basic && this.basicCommandsGrid) {
                        this.basicCommandsGrid.setState(props.basic);
                    }
                    if (props.basicVariables && this.variablesGrid) {
                        this.variablesGrid.setState(props.basicVariables);
                    }

                    if (props.conditionalProcess && this.responseGrid) {
                        this.responseGrid.setState(props.conditionalProcess);
                    }
                }
            }

            if (has('fiddle')) {
                _.each([this.basicCommandsGrid, this.responseGrid, this.variablesGrid], function (grid) {
                    grid.showToolbar(true);
                });
            }

            this.resize();

            this._parent && this._parent.set && this._parent.set('loading', false);
        },
        startup: function () {
            this._parent && this._parent.set && this._parent.set('loading', true);
            const self = this,
                ctx = this.ctx,
                deviceManager = ctx.getDeviceManager(),
                driverManager = ctx.getDriverManager(),
                device = self.device,
                driver = self.driver;

            const _docker = this.createLayout();
            self.isInstance = device && device.driverInstance !== null;
            self.lastInstance = device && device.driverInstance !== null ? device.driverInstance._id : null;
            this._docker = _docker;

            const dfd = new Deferred();
            let gotDriver = false;

            if (!device || !device.driverInstance) {
                driverManager.loadDriverModule(driver).then(function (driverModule) {
                    driver.Module = driverModule;
                    self.driverModule = driverModule;
                    if (!gotDriver) {
                        gotDriver = true;
                        self.createWidgets();
                        dfd.resolve();
                    }
                    console.log('got driver module: load ');
                });
            }
            driverManager.getDriverModule(driver).then(function (driverModule) {
                    self.driverModule = driverModule;
                    driver.Module = module;
                    if (!gotDriver) {
                        gotDriver = true;
                        self.createWidgets();
                        dfd.resolve();
                    }

                }),
                function (e) {
                    logError(e);
                };


            //refresh grid stores when device got reconnected
            self.isInstance && this.subscribe(types.EVENTS.ON_DEVICE_DRIVER_INSTANCE_READY, function (evt) {
                if (evt.device === self.device && self.isInstance) {
                    const driverInstance = evt.device.driverInstance;
                    if (self.lastInstance !== driverInstance._id) {
                        const scope = driverInstance.blockScope;
                        const store = scope.blockStore;
                        self.lastInstance = driverInstance._id;
                        _.each(self.grids, function (grid) {
                            if (grid.blockGroup) {
                                grid.blockScope = scope;
                                grid.set('collection', store.filter({
                                    group: grid.blockGroup
                                }));
                            }
                        });
                    }
                }
            });


            this._emit('startup');
            return dfd;
        }
    });

    function openDriverView(driver, device, name) {
        const parent = TestUtils.createTab(null, null, name || module.id);
        const title = 'Marantz Instance';
        const devinfo = null,
            deviceManager = ctx.getDeviceManager();
        parent.set('loading', true);

        //@Todo:driver, store device temporarly in Commands CI
        const commandsCI = utils.getCIByChainAndName(driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
        const driverView = utils.addWidget(DriverViewClass, {
            userData: commandsCI,
            driver: driver,
            device: device,
            ctx: ctx
        }, null, parent, true);
        parent.add(driverView);
        return driverView;
    }

    if (ctx && doTests) {
        driverManager = ctx.getDriverManager();
        let marantz = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        //var _marantz = driverManager.getItemById("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        const _marantz = driverManager.getDriverByPath("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        if (_marantz) {
            marantz = _marantz;
        }
        const driver = marantz.driver || marantz;
        const device = marantz.device;

        view = openDriverView(driver, device);
        return Grid;
    }

    DriverViewClass.BlockGridClass = createGridClass();
    return DriverViewClass;
});