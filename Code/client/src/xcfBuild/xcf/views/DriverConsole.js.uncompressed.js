/** @module xcf/DriverConsole **/
define("xcf/views/DriverConsole", [
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/views/ConsoleView',
    'xblox/model/Expression'
], function (dcl, types, utils, ConsoleView, Expression) {

    const EditorClass = dcl(ConsoleView.Editor, {
        didAddMCompleter: false,
        multiFileCompleter: null,
        blockScope: null,
        driverInstance: null,
        onACEReady: function (editor) {
            const add = true;
            const scope = this.blockScope;
            const driverInstance = this.driverInstance;
            const driver = this.driver;
            const device = this.device;

            let variables = scope.getVariables({
                group: 'basicVariables'
            });

            const _responseVariables = scope.getVariables({
                group: 'processVariables'
            });


            if (_responseVariables && _responseVariables.length) {
                variables = variables.concat(_responseVariables);
            }
            const completors = [];
            var path = driver.path;

            function createCompleter(text, value, help) {
                return {
                    word: text,
                    value: value,
                    meta: help || ''
                };
            }

            if (variables && variables.length > 0) {
                for (let i = 0; i < variables.length; i++) {
                    const obj = variables[i];

                    completors.push(createCompleter(obj.name, obj.value, 'Driver Variable'));

                }
            }
            for (var prop in driverInstance) {
                var val = driverInstance[prop];
                if (_.isFunction(val)) {
                    completors.push(createCompleter(prop, prop, 'Driver Method'));
                } else {

                    if (_.isString(val) || _.isObject(val)) {
                        completors.push(createCompleter(prop, prop, 'Driver Property'));
                    }
                }
                /*
                 if(_.isFunction(val) || _.isString(val) || _.isFunction(val)){
                 completors.push(createCompleter(prop, prop, 'Driver Method'));
                 }
                 */
            }
            return completors.length && this.addAutoCompleter(completors);

            const completer = this.addFileCompleter();
            const text = "var xxxTop = 2*3;";
            var path = driver.path;

            let _text = '';

            for (var prop in driverInstance) {

                var val = driverInstance[prop];
                if (_.isFunction(val)) {

                    _text += '\n';
                    _text += prop;
                    _text += ':';
                    _text += val.toString();
                    _text += ',\n';
                }
            }
            //var _text = driverInstance.toString();
            completer.addDocument(path, _text);
        },
        onEditorCreated: function (editor, options) {
            this.inherited(arguments);
            this.onACEReady(editor);
        }

    });
    const DriverConsole = dcl(ConsoleView, {
        EditorClass: EditorClass,
        declaredClass: "xcf/views/DriverConsole",
        runAction: function () {
            return this.inherited(arguments);
        },
        onAddEditorActions: dcl.superCall(function (sup) {
            return function (evt) {
                //console.error('-driver console onAddEditorActions');
                //grab the result from the handler
                const res = sup.call(this, evt);

                const thiz = this;
                const actions = evt.actions;
                const owner = evt.owner;

                actions.remove(_.find(actions, {
                    command: 'File/Save'
                }));

                actions.remove(_.find(actions, {
                    command: "File/Reload"
                }));

                const mixin = {
                    addPermission: true,
                    quick: true,
                    owner: owner
                };

                const _params = {
                    onCreate: function (action) {
                        const optionValue = action.value;
                        action.set('value', optionValue);

                    },
                    onChange: function (property, value) {
                        this.value = value;
                        thiz.runAction(this);
                    }
                };

                ///editor settings
                actions.push(owner.createAction({
                    label: 'Settings',
                    command: 'Console/Settings',
                    icon: 'fa-cogs',
                    group: "Settings",
                    mixin: mixin
                }));

                actions.push(owner.createAction(utils.mixin({
                    label: '%%Construct %%Response',
                    command: 'Console/Settings/Split',
                    icon: 'fa-filter',
                    group: 'Settings',
                    tab: 'Home',
                    value: true,
                    mixin: utils.mixin({
                        actionType: types.ACTION_TYPE.MULTI_TOGGLE,
                        value: true
                    }, mixin),
                    handler: function () {
                        console.error('filter');
                    }
                }, _params)));


                actions.push(owner.createAction(utils.mixin({
                    label: 'Display as Hex',
                    command: 'Console/Settings/HEX',
                    icon: 'fa-filter',
                    group: 'Settings',
                    tab: 'Home',
                    value: false,
                    mixin: utils.mixin({
                        actionType: types.ACTION_TYPE.MULTI_TOGGLE,
                        value: false
                    }, mixin),
                    handler: function () {
                        console.error('filter');
                    }
                }, _params)));

                actions.push(owner.createAction(utils.mixin({
                    label: '%%Replace %%Hex',
                    command: 'Console/Settings/ReplaceHEX',
                    icon: 'fa-filter',
                    group: 'Settings',
                    tab: 'Home',
                    value: true,
                    mixin: utils.mixin({
                        actionType: types.ACTION_TYPE.MULTI_TOGGLE,
                        value: true
                    }, mixin),
                    handler: function () {
                        console.error('filter');
                    }
                }, _params)));

                actions.push(owner.createAction(utils.mixin({
                    label: '%%Add %%Start/End Constants',
                    command: 'Console/Settings/AddConstants',
                    icon: 'fa-filter',
                    group: 'Settings',
                    tab: 'Home',
                    value: true,
                    mixin: utils.mixin({
                        actionType: types.ACTION_TYPE.MULTI_TOGGLE,
                        value: true
                    }, mixin),
                    handler: function () {
                        console.error('filter');
                    }
                }, _params)));

                actions.push(owner.createAction(utils.mixin({
                    label: '%%Is %%Expression',
                    command: 'Console/Settings/Expression',
                    icon: 'fa-filter',
                    group: 'Settings',
                    tab: 'Home',
                    value: true,
                    mixin: utils.mixin({
                        actionType: types.ACTION_TYPE.MULTI_TOGGLE,
                        value: true
                    }, mixin),
                    handler: function () {
                        console.error('filter');
                    }
                }, _params)));
            };
        }),
        logTemplate: '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>',
        _parse: function (scope, expression, errorCB) {
            let str = '' + expression;
            if (str.indexOf('{{') > 0 || str.indexOf('}}') > 0) {
                str = _parser.parse(types.EXPRESSION_PARSER.FILTREX,
                    str, this,
                    {
                        variables: scope.getVariablesAsObject(),
                        delimiters: {
                            begin: '{{',
                            end: '}}'
                        }
                    }
                );
            } else {
                const _text = scope.parseExpression(expression, null, null, null, errorCB);
                if (_text) {
                    str = _text;
                }
            }
            return str;
        },
        parse: function (str, errorCB) {
            const driverInstance = this.driverInstance;
            if (driverInstance && driverInstance.blockScope) {
                return this._parse(driverInstance.blockScope, str, errorCB);
            }
            return str;
        },
        getDeviceString: function (device, deviceInfo, suffix) {

            let out = (device.scope === 'user_devices' ? "User Device - " : "System Device - ");
            out += "<b>" + device.name + "</b>" + " - " + deviceInfo.host + ':' + deviceInfo.port + '@' + deviceInfo.protocol + '<b><span class="text-default">' + (suffix || "") + '</b></span>';
            return out;
        },
        onDeviceCommand: function (evt) {
            const LOGGING_FLAGS = types.LOGGING_FLAGS;
            const OUTPUT = types.LOG_OUTPUT;

            if (evt.__d3) {
                return;
            }
            evt.__d3 = true;
            const self = this;
            const device = evt.device;
            const deviceInfo = evt.deviceInfo;
            let flags = deviceInfo.loggingFlags;
            flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};

            if (!this.hasFlag(deviceInfo, OUTPUT.SEND_COMMAND) || device != this.device) {
                return;
            }

            const deviceString = this.getDeviceString(device, deviceInfo, " Command: ") + "<span class='text-default'>\"" + evt.name + "\"</span> ";
            const string = '<span class="text-info">' + deviceString + '</span><span>' + '<span class="text-warning">' + evt.command + '</span>';

            self.log(string, false, false);
        },
        startup: function () {
            this.subscribe(types.EVENTS.ON_DEVICE_COMMAND);
        }
    });
    return DriverConsole;

});