define("xcf/views/DriverTreeView_Actions", [
    'dojo/_base/declare',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xcf/views/ProtocolTreeView',
    "xide/views/_ActionMixin",
    'dstore/SimpleQuery',
    'dstore/Filter'
], function (declare, utils, types,
    factory,
    ProtocolTreeView, _ActionMixin,
    SimpleQuery, Filter) {

    return declare("xcf.views.DriverTreeView_Actions", null, {
        getActions: function (container, permissions, grid) {

            const actions = [];
            const thiz = this;
            const delegate = thiz.delegate;
            container = this.containerNode;
            
            const ACTION = types.ACTION;
            const item = thiz.getSelectedItem();

            function _selection() {
                const selection = grid.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                if (!item) {
                    console.error('have no item');
                    return null;
                }
                return selection;
            }

            const defaultMixin = {
                    addPermission: true,
                    quick: true
                },
                DEVICE_COMMAND_GROUP = 'Open';

            function shouldDisableEdit() {

                const selection = _selection(),
                    item = selection ? selection[0] : null;


                if (!item || item.isGroup) {
                    return true;
                }

                if (item.instances || ( /*item.device && */ item.blockScope)) {
                    return false;
                }
                if ((selection[0].device && selection[0].driver)) {
                    return false;
                }
                return true;
            }

            actions.push(this.createAction('Edit', ACTION.EDIT, types.ACTION_ICON.EDIT, ['f4', 'enter', 'dblclick'], 'Home', 'Open', 'item', null,
                function () {
                    return thiz.editItem(thiz.getSelectedItem());
                }, {
                    addPermission: true,
                    tab: 'Home',
                    quick: true
                }, null, shouldDisableEdit, permissions, container, grid
            ));


            actions.push(this.createAction('Code', 'File/Code', 'fa-code', ['shift f4'], 'Home', 'Open', 'item', null,
                function () {
                    return delegate.editDriver(null, item);
                }, {
                    addPermission: true,
                    tab: 'Home',
                    quick: true
                }, null, shouldDisableEdit, permissions, container, grid
            ));


            function shouldDisableConsole() {
                const selection = _selection();

                if (!selection || selection[0].isGroup) {
                    return true;
                }

                if ((selection[0].device && selection[0].driver)) {
                    return false;
                }
                return true;
            }

            actions.push(this.createAction('Console', 'File/Console', 'fa-terminal', ['f5', 'ctrl s'], 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                defaultMixin, null, shouldDisableConsole, permissions, container, grid
            ));

            actions.push(this.createAction('Log', 'File/Log', 'fa-calendar', ['f9'], 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                defaultMixin, null, shouldDisableConsole, permissions, container, grid
            ));

            function isRoot() {
                const selection = _selection();

                if (!selection || selection.length == 0) {
                    return false;
                }

                return true;
            }

            actions.push(this.createAction('New Group', 'File/New Group', types.ACTION_ICON.NEW_DIRECTORY, ['f7'], 'Home', 'New', 'item|view', null, null,
                defaultMixin, null, isRoot, permissions, container, grid
            ));

            function isNotGroup() {
                const selection = _selection();

                if (selection && selection[0] && selection[0].isDir) {
                    return false;
                }

                return true;
            }

            actions.push(this.createAction('New Driver', 'File/New Driver', types.ACTION_ICON.NEW_FILE, ['f4'], 'Home', 'New', 'item', null, null,
                defaultMixin, null, isNotGroup, permissions, container, grid
            ));

            actions.push(this.createAction('Source', 'View/Source', 'fa-hdd-o', ['f4'], 'Home', 'Navigation', 'item', null,
                function () {}, {
                    addPermission: true,
                    tab: 'Home',
                    dummy: true
                }, null, null, permissions, container, thiz
            ));

            let i = 0;

            function createSourceAction(item) {

                const label = item.label;

                actions.push(thiz.createAction(label, 'View/Source/' + label, 'fa-hdd-o', ['alt f' + i], 'Home', 'Navigation', 'item', null,
                    function () {}, {
                        addPermission: true,
                        tab: 'Home',
                        item: item
                    }, null, null, permissions, container, thiz
                ));

                i++;
            };

            const sources = [{
                    label: 'System',
                    scope: 'system_drivers'
                },
                {
                    label: 'User',
                    scope: 'user_drivers'
                }
            ];
            _.each(sources, createSourceAction);
            /*
             this.addAction(actions, _ActionMixin.createActionParameters('Edit', ACTION.EDIT, 'Open', types.ACTION_ICON.EDIT, function () {
             return thiz.editItem(thiz.getSelectedItem());
             }, 'Enter | F4', ['f4', 'enter'], null, container, thiz, {
             tab: 'Home',
             tooltip: {
             content: '<span><strong>This text is in bold case !</strong></span>'
             }
             }));
             */


            /*
             this.addAction(actions, _ActionMixin.createActionParameters('Delete', ACTION.DELETE, 'Organize', types.ACTION_ICON.DELETE, function () {
             delegate.onDeleteItem(thiz.getSelectedItem());
             }, 'Delete | F8', ['f8', 'delete'], null, container, thiz,{
             tab:'Home'
             }));
             */

            /*
             this.addAction(actions, _ActionMixin.createActionParameters('New Driver Group', 'File/New Driver Group', 'New', types.ACTION_ICON.NEW_DIRECTORY, function () {
             delegate.newGroup(thiz.getSelectedItem());
             }, 'F7', 'f7', null, container, thiz,{
             tab:"Home"
             }));


             this.addAction(actions, _ActionMixin.createActionParameters('Reload', ACTION.RELOAD, 'file', types.ACTION_ICON.RELOAD, function () {
             delegate.reload();
             }, 'Reload', 'ctrl l', null, container, thiz));


             this.addAction(actions, _ActionMixin.createActionParameters('Import Protocol', 'Edit/Import Protocol', 'New', 'fa-download', function () {
             thiz.createProtocolWizard(thiz.getSelectedItem());
             }, 'Import', 'ctrl i', null, container, thiz,{
             tab:"Home"
             }));
             */


            return actions;
        },
        changeSource: function (source) {
            const thiz = this;
            const delegate = thiz.delegate;
            const scope = source.scope;
            let store = delegate.getStore(scope);

            function wireStore(store) {

                if (thiz['_storeConnected' + scope]) {
                    return;
                }

                thiz['_storeConnected' + scope] = true;

                function updateGrid() {
                    setTimeout(function () {
                        thiz.grid.refresh();
                    }, 100);
                }
                store.on('update', function () {
                    console.warn(' store updated: ', arguments);
                    updateGrid();

                });
                store.on('added', function () {
                    //console.log(' added to store: ',arguments);
                    updateGrid();
                });

                store.on('remove', function () {
                    //console.log(' removed from store: ',arguments);
                    updateGrid();
                });

                store.on('delete', function () {
                    //console.log(' deleted from store: ',arguments);
                    updateGrid();
                });

            }

            function ready(store) {
                const collection = store.filter(thiz.getRootFilter());
                thiz.grid.set('collection', collection);
                thiz.set('title', 'Drivers (' + source.label + ')');
                thiz.scope = scope;
                wireStore(store);
            }
            if (store.then) {
                store.then(function () {
                    store = delegate.getStore(scope);
                    ready(store);
                });
            } else {
                //is store
                ready(store);
            }
        },
        ///////////////////////////////////////////////////////////////
        runAction: function (action, _item) {
            if (!action || !action.command) {
                return;
            }
            const ACTION = types.ACTION;
            const thiz = this;
            const delegate = thiz.delegate;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            let item = thiz.getSelectedItem() || _item;
            var device = item ? item.device : null;

            if (action.command.indexOf('View/Source') != -1) {
                return this.changeSource(action.item);
            }

            switch (action.command) {
                case 'File/Code':
                    {
                        var device = item.device || null;
                        if (device && device.driverInstance) {
                            item = device.driverInstance.driver;
                        }
                        return delegate.editDriver(null, item);
                    }
                case 'File/Console':
                    {
                        if (device && !device.driverInstance) {
                            deviceManager.startDevice(device).then(function () {
                                deviceManager.openConsole(item.device);
                            });
                            return;
                        }
                        return deviceManager.openConsole(item.device);
                    }
                case 'File/Log':
                    {
                        return deviceManager.openLog(item.device);
                    }

                case ACTION.EDIT:
                    {
                        return thiz.editItem(item);
                    }
                case ACTION.RELOAD:
                    {
                        return thiz.grid.refresh();
                    }
                case ACTION.DELETE:
                    {
                        return delegate.onDeleteItem(item);
                    }
                case 'File/New Driver':
                    {
                        delegate.newItem(item).then(function (newItem) {
                            thiz.refresh();
                        });
                    }
                case 'File/New Group':
                    {
                        return delegate.newGroup(item, thiz.scope);
                    }
            }
        },
        importProtocol: function (protocol, items, driver) {
            const variableProto = utils.getObject('xcf/model/Variable');
            const commandProto = utils.getObject('xcf/model/Command');
            const scope = driver.blockScope;

            function createDriverItem(blockProto) {}

            for (let i = 0; i < items.length; i++) {
                const protocolItem = items[i];
                const isVariable = protocolItem.type == 'protocolVariable';
                let ctrArgs = null;
                if (isVariable) {
                    ctrArgs = {
                        value: protocolItem.value,
                        title: protocolItem.name,
                        group: 'basicVariables'

                    }
                } else {
                    ctrArgs = {
                        send: protocolItem.value,
                        name: protocolItem.name,
                        group: 'basic'
                    }
                }

                const _proto = isVariable ? variableProto : commandProto;
                const block = factory.createBlock(_proto, ctrArgs);
                if (isVariable) {
                    scope.variableStore.putSync(block);
                } else {
                    scope.blockStore.putSync(block);
                }
            }
            this.publish(types.EVENTS.ON_DRIVER_MODIFIED, {
                driver: driver,
                owner: this
            });
            this.refreshGrid();
            this.delegate.saveDriver(driver);
        },
        createProtocolWizard: function () {
            const thiz = this;
            let selectView = null;
            let selectItemsView = null;
            let selectedItems = [];
            let selectedItem = null;
            const driver = thiz.getSelectedItem();
            const protocolMgr = thiz.delegate.ctx.getProtocolManager();
            const store = protocolMgr.getStore();

            const wizardStruct = Wizards.createWizard('Select Protocol', false, {});

            this._lastWizard = wizardStruct;
            const wizard = wizardStruct.wizard;
            const dialog = wizardStruct.dialog;
            const done = function () {
                wizard.destroy();
                dialog.destroy();
                thiz.importProtocol(selectedItem, selectedItems, driver);
            };
            dialog.show().then(function () {
                /**
                 * First protocol view, select item
                 */
                selectView = utils.addWidget(ProtocolTreeView, {
                    title: 'Protocols',
                    store: protocolMgr.getStore(),
                    delegate: protocolMgr,
                    beanContextName: '3', // thiz.ctx.mainView.beanContextName
                    _doneFunction: function () {
                        return false;
                    },
                    passFunction: function () {
                        return true;
                    },
                    canGoBack: true,
                    gridParams: {
                        showHeader: true
                    },
                    editItem: function () {

                        if (!this.isGroup(selectedItem)) {
                            wizard._forward();
                        }
                    }

                }, protocolMgr, wizard, true, null, [WizardPaneBase]).
                _on(types.EVENTS.ON_ITEM_SELECTED, function (evt) {
                    selectedItem = evt.item;
                    wizard.adjustWizardButton('next', selectView.isGroup(evt.item));

                });

                selectItemsView = utils.addWidget(ProtocolTreeView, {
                    title: 'Protocols',
                    store: protocolMgr.getStore(),
                    delegate: protocolMgr,
                    doneFunction: function () {
                        done();
                        return true;
                    },
                    passFunction: function () {
                        //not used
                        return true;
                    },
                    canGoBack: true,
                    editItem: function () {

                    },
                    getRootFilter: function () {
                        if (selectedItem) {
                            return {
                                parentId: selectedItem.path
                            }
                        } else {
                            return {
                                parentId: ''
                            }
                        }
                    },
                    canSelect: function (item) {
                        if (item.virtual === true && item.isDir === false) {
                            return true;
                        }
                        return false;
                    }
                }, protocolMgr, wizard, true, null, [WizardPaneBase], null, {
                    getColumns: function () {
                        const _res = this.inherited(arguments);
                        _res.push({
                            label: "Value",
                            field: "value",
                            sortable: false,
                            _formatter: function (name, item) {

                                if (!thiz.isGroup(item) && item['user']) {
                                    const meta = item['user'].meta;
                                    const _in = meta ? utils.getCIInputValueByName(meta, types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE) : null;
                                    if (meta) {
                                        return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                                    } else {
                                        return item.name;
                                    }
                                    return _in;
                                } else {
                                    return item.name;
                                }
                            }
                        });
                        return _res;
                    }
                }).
                _on(types.EVENTS.ON_ITEM_SELECTED, function (evt) {
                    selectedItems = selectItemsView.getSelection();
                    wizard.adjustWizardButton('done', selectedItems.length == 0);

                });


                wizard.onNext = function () {
                    dialog.set('title', 'Select Commands & Variables');
                    selectItemsView.grid.set('collection', store.filter(selectItemsView.getRootFilter()));
                    selectItemsView.grid.refresh();
                }
            });
        },
        editItem: function (item) {
            const thiz = this,
                device = item.device,
                delegate = thiz.delegate,
                ctx = thiz.ctx,
                deviceManager = ctx.getDeviceManager();

            function edit() {
                if (item.driver) {
                    item = item.driver;
                }
                thiz.openItem(item, device);
            }
            if (device && !device.driverInstance) {
                deviceManager.startDevice(device).then(function (instance) {
                    console.error('device was not started, did create driver instance', instance);
                    edit();
                });
                return;
            }
            if (item.type === types.ITEM_TYPE.BLOCK) {
                return thiz.blockManager.editBlock(item.ref.item, null, false);
            }
            edit();
        },
        changeScope: function (name) {
            this.delegate.ls(name + '_drivers').then(function (data) {});

        },
        shouldShowAction: function (action) {
            const thiz = this;
            const _cItem = thiz.grid.getSelection()[0];
            let result = true;
            const actionType = types.ACTION;
            if (_cItem) {

                if (action.command.indexOf('View/Settings') != -1) {
                    return true;
                }

                if (!_cItem.virtual) {
                    switch (action.command) {
                        case actionType.RENAME:
                        case actionType.DELETE:
                        case actionType.RELOAD:
                            {
                                result = true;
                                break;
                            }
                        case 'Edit/Import Protocol':
                        case actionType.EDIT:
                            {
                                result = thiz.isBeanItem(_cItem);
                            }
                    }
                } else {
                    switch (action.command) {
                        case "File/New Group":
                        case actionType.RENAME:
                        case actionType.DELETE:
                            {
                                return false;
                            }
                        case actionType.EDIT:
                            {
                                result = true;
                            }
                    }
                }
            } else {

                if (action.command.indexOf('View/Settings') != -1) {
                    return true;
                }
                switch (action.command) {
                    case actionType.RELOAD:
                    case "View/Scopes":
                    case "View/Scopes/System":
                    case "File/New Group":
                        {
                            return true;
                        }
                }
                return false;
            }
            return result;
        }
    });
});