define("xcf/views/_MainView", [
    'xide/types',
    'xdocker/Docker2',
    'dcl/dcl',
    'xide/utils',
    'xide/views/_LayoutMixin',
    'xide/views/_MainView',
    'xide/_base/_Widget',
    'xaction/ActionProvider',
    'xfile/Breadcrumb',
    'xide/widgets/MainMenu',
    'xdocker/types',
    'xdocker/Docker2',
    'xdocker/Panel2',
    'xdocker/Frame2',
    'wcDocker/types'
], function (types,Docker,dcl,utils,_LayoutMixin,_MainView,_Widget,ActionProvider,Breadcrumb,MainMenu) {

    const MainViewClass = dcl(_MainView, {
        getNewAlternateTarget: function () {
            return this.layoutCenter;
        },
        getNewDefaultTab: function (args) {
            utils.mixin(args, {
                target: this.layoutCenter,
                location: types.DOCKER.DOCK.STACKED
            });
            const tab = this.getDocker().addTab(null, args);
            return tab;
        },
        _onResize: function () {
            this.getDocker().resize();
        },
        resize: function () {
            this.getDocker().resize();
        },
        onOpenView:function(evt){
            this.onOpenView2 && this.onOpenView2(evt);
        },
        startup: function () {
            const self = this;
            function _resize() {
                self._resizeToWindow();
                setTimeout(function () {
                    self.publish(types.EVENTS.RESIZE, {}, this);
                }, 500);
            }
            $(window).resize(function () {
                return self.debounce('resize', _resize.bind(self), 1000, null);
            });

            setTimeout(function(){
                _resize();
            },5000);

            this.subscribe(types.EVENTS.ON_OPEN_VIEW,function(){
                setTimeout(function () {
                    self._resize();
                }, 500);
            });
        }
    });

    ////////////////////////////////////////////////////////////////
    //
    //  Old/Trash
    //
    //

    const ACTION = types.ACTION;
    const LayoutClass = dcl(null,{});
    const BorderLayoutClass = dcl(LayoutClass,{
        getMainMenu:function(){
            return this.mainMenu;
        },
        getToolbar: function () {
            return this.toolbar;
        },
        getNewAlternateTarget: function () {
            return this.layoutCenter;
        },
        getLayoutMain:function(args,create){},
        getLastTab:function(){
            const panels = this.getDocker().allPanels();
            const main = this.getLayoutMain();
            return _.find(panels,function(panel){
                const canAdd = panel.option('canAdd');
                return panel!=main && canAdd == true;
            });
        },
        getNewDefaultTab:function(args){
            utils.mixin(args, {
                target: this.layoutCenter,
                location: types.DOCKER.DOCK.STACKED
            });
            const tab = this.getDocker().addTab(null, args);
            return tab;
        },
        createLayout:function(docker,permissions){
            let top;
            if(this.hasAction(ACTION.TOOLBAR) || this.hasAction(ACTION.BREADCRUMB) || this.hasAction(ACTION.MAIN_MENU) || this.hasAction(ACTION.RIBBON)) {
                top = this.layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                    h: 50,
                    title: false,
                    canAdd:false,
                    mixin: {
                        resizeToChildren: true,
                        _scrollable: {
                            x: false,
                            y: false
                        }
                    }
                });
                if(this.hasAction(ACTION.MAIN_MENU)){
                    const _args = {
                        style: 'height:auto',
                        resizeToParent: false
                    };
                    this.mainMenu = utils.addWidget(MainMenu, _args, this, top, true);
                    $(this.mainMenu.domNode).css('height:auto');
                }
                if(this.hasAction(ACTION.BREADCRUMB)) {
                    const breadcrumb = utils.addWidget(Breadcrumb, {
                        resizeToParent: false
                    }, null, top, true);
                    breadcrumb.resizeToParent = false;
                    $(breadcrumb.domNode).css('height:auto');
                    this.breadcrumb = breadcrumb;
                }
            }

            //types.PANEL_TYPES.Collapsible
            this.layoutLeft = docker.addPanel('DefaultTab', types.DOCKER.DOCK.BOTTOM, top, {
                title:'&nbsp;&nbsp;',
                autoHideTabBar:true,
                titleStacked:"Navigation"
            });
            this.layoutLeft.closeable(false);
            this.layoutLeft.resizeToChildren=false;
            this.layoutLeft.getFrame().showTitlebar(false);
            docker.resize();
        }
    });
    const DockerView = dcl([_Widget,_LayoutMixin.dcl,ActionProvider.dcl,BorderLayoutClass],{
        templateString:'<div style="height: 100%;width: 100%;"></div>',
        permissions:[
            //ACTION.BREADCRUMB,
            //ACTION.RIBBON
            //ACTION.MAIN_MENU
        ],
        getBreadcrumb:function(){
            return this.breadcrumb;
        },
        getLayoutMain:function(){
            return this.layoutLeft;
        },
        onLastPanelClosed:function(){
            const main = this.getLayoutMain();
            if(main.option('autoHideTabBar')){
                main.title(main.option('title'));
                main.getFrame().showTitlebar(false);
            }
            this.getDocker().resize();
        },
        onPanelClosed:function(panel){
            const self = this;
            const main = self.getLayoutMain();
            const docker = self.getDocker();

            setTimeout(function(){
                const panels = docker.allPanels().filter(function(panel){
                    return panel.option('canAdd') !== false;
                });

                if(panels.length==1 && panels[0] == main){
                    self.onLastPanelClosed();
                }
            },1);
            this.getDocker().resize();
        },
        onOpenView:function(evt){},
        startup:function(){
            const docker = Docker.createDefault(this.domNode);
            this._docker = docker;
            const self = this;
            docker._on(types.DOCKER.EVENT.CLOSED,function(panel){
                self.onPanelClosed(panel);
            });
            this.createLayout(docker,this.permissions);
            this.subscribe(types.EVENTS.ON_OPEN_VIEW);
        }

    });


    return MainViewClass;
});