define("xcf/views/DriverView_Layout", [
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "dojo/Deferred",
    'xdocker/Docker2',
    'xide/views/_CIDialog'
], function (dcl, types, utils, Deferred, Docker, _CIDialog) {

    return dcl(null, {
        restoreLayout: function () {
            const dfd = new Deferred();
            const defaultDfdArgs = {
                select: [],
                focus: true,
                append: false
            };

            const state = this.getState();//actual value is in state.value
            let _layouts = state.value.layouts;
            if (!_layouts) {
                state.value.layouts = _layouts = {};
            }

            const options = [];
            for (const layout in _layouts) {
                options.push({
                    value: layout,
                    label: layout
                });
            }
            const docker = this._docker;
            const CIS = {
                inputs: [
                    utils.createCI('name', types.ECIType.ENUMERATION, options[0] ? options[0].value : "", {
                        widget: {
                            title: 'Layout',
                            options: options
                        }
                    })
                ]
            };

            const dlg = new _CIDialog({
                cis: CIS,
                title: 'Save Layout',
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                bodyCSS: {
                    'height': 'auto',
                    'min-height': '80px',
                    'width': '100%',
                    'min-width': '400px'
                },
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {
                    const layoutName = this.getField('name');
                    if (layoutName) {
                        const layoutData = _layouts[layoutName];
                        if (layoutData) {
                            docker.restore(JSON.stringify(layoutData));
                        }
                    }
                    dfd.resolve(defaultDfdArgs);
                }
            });
            dlg.show();
            return dfd;
        },
        onPanelsRestored: function () {
            const docker = this._docker;
            const panels = docker.getPanels();
            delete this.logTab;
            delete this.consoleTab;
            delete this.basicCommandsTab;
            delete this.variablesTab;
            delete this.responseTab;
            delete this.settingsTab;
            delete this.initTab;

            delete this.basicCommandsGrid;
            delete this.variablesGrid;
            delete this.responseGrid;
            delete this.initGrid;
            delete this.grids;

            this.grids = [];

            this.logTab = _.find(panels, {_title: 'Log'});
            this.consoleTab = _.find(panels, {_title: 'Console'});
            this.basicCommandsTab = _.find(panels, {_title: 'Commands'});
            this.variablesTab = _.find(panels, {_title: 'Variables'});
            this.responseTab = _.find(panels, {_title: 'Responses'});
            this.settingsTab = _.find(panels, {_title: 'Settings'});
            this.initTab = _.find(panels, {_title: 'Init'});
            this.createWidgets();
        },
        saveLayout: function () {
            const docker = this._docker;
            const layoutData = docker.save();
            const dfd = new Deferred();
            const defaultDfdArgs = {
                select: [],
                focus: true,
                append: false
            };
            let layoutName = 'Default';
            const ctx = this.ctx;
            const state = this.getState();//actual value is in state.value
            let layouts = state.value.layouts;

            if (!layouts) {
                state.value.layouts = layouts = {};
            }
            const CIS = {
                inputs: [
                    utils.createCI('name', types.ECIType.STRING, layoutName, {
                        widget: {
                            instant: false,
                            title: 'Layout Name'
                        }
                    })
                ]
            };
            const dlg = new _CIDialog({
                cis: CIS,
                title: 'Save Layout',
                ctx: this.ctx,
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                bodyCSS: {
                    'height': 'auto',
                    'min-height': '80px',
                    'width': '100%',
                    'min-width': '400px'
                },
                onCancel: function () {
                    dfd.resolve(defaultDfdArgs);
                },
                onOk: function () {
                    layoutName = this.getField('name');
                    if (layoutName.length) {
                        layouts[layoutName] = utils.getJson(layoutData);
                        ctx.getSettingsManager().write2(null, '.',
                            {
                                id: 'driverView'
                            }, {
                                value: state.value
                            }, true, null
                        ).then(function () {
                            dfd.resolve(defaultDfdArgs);
                        });
                    }
                }
            });
            dlg.show();
            return dfd;
        },
        createTab: function (type, args) {
            const DOCKER = types.DOCKER;
            const defaultTabArgs = {
                icon: false,
                closeable: true,
                moveable: true,
                tabOrientation: DOCKER.TAB.TOP,
                location: DOCKER.DOCK.STACKED
            };
            return this._docker.addTab(type || 'DefaultTab', utils.mixin(defaultTabArgs, args));
        },
        createLayout: function () {
            const DOCKER = types.DOCKER;
            const self = this;
            this._docker = Docker.createDefault(this.domNode, {
                extension: Docker
            });

            this._docker._on('endRestore', function (e) {
                self.onPanelsRestored();
            });

            const basicCommands = this.showBasicCommands ? this.createTab(null, {
                title: 'Commands',
                icon: 'fa-long-arrow-right'
            }) : null;

            

            const initTab = this.showInitTab ? this.createTab(null, {
                title: 'Init',
                tabOrientation: DOCKER.TAB.TOP,
                target: basicCommands,
                icon: 'fa-bell'
            }) : null;

            const settingsTab = this.showSettings ? this.createTab(null, {
                title: 'Settings',
                tabOrientation: DOCKER.TAB.TOP,
                target: basicCommands,
                icon: 'fa-cogs'
            }) : null;

            let log = null;

            if (this.showLog || this.showConsole) {
                log = this.showLog ? this.createTab(null, {
                    title: 'Log',
                    tabOrientation: DOCKER.TAB.BOTTOM,
                    location: DOCKER.TAB.BOTTOM,
                    icon: 'fa-calendar'
                }) : null;
            } else {
                log = this.showLog ? this.createTab(null, {
                    title: 'Log',
                    tabOrientation: DOCKER.TAB.TOP,
                    icon: 'fa-calendar',
                    target: basicCommands
                }) : null;
            }


            const consoleTab = this.showConsole ? this.createTab(null, {
                title: 'Console',
                tabOrientation: DOCKER.TAB.TOP,
                target: log,
                icon: 'fa-terminal'
            }) : null;

            const lastBottom = log || consoleTab || basicCommands;

            const responses = this.showResponseBlocks ? this.createTab(null, {
                title: 'Responses',
                tabOrientation: DOCKER.TAB.TOP,
                target: lastBottom,
                icon: 'fa-long-arrow-left'
            }) : null;

            const variables = this.showVariables ? this.createTab(null, {
                title: 'Variables',
                tabOrientation: DOCKER.TAB.TOP,
                target: lastBottom,
                icon: 'fa-list-alt'
            }) : null;

            this.logTab = log;
            this.consoleTab = consoleTab;
            this.basicCommandsTab = basicCommands;
            this.variablesTab = variables;
            this.responseTab = responses;
            this.settingsTab = settingsTab;
            this.initTab = initTab;

            return this._docker;
        },
        getState: function () {
            const ctx = this.ctx;
            const settingsStore = ctx.getSettingsManager().getStore() || {
                    getSync: function () {
                    }
                };
            const props = settingsStore.getSync('driverView');
            return props || {value: {layouts: {}}};
        }
    });
});