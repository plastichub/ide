/** @module xgrid/Base **/
define("xcf/views/ExpressionConsole", [
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/Grid',
    'xgrid/Defaults',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'xide/views/ConsoleView',
    'xide/widgets/WidgetBase',
    "xide/data/TreeMemory"
], function (dcl,declare, types,utils, ListRenderer, Grid, Defaults, Focus,OnDemandGrid, EventedMixin, _LayoutMixin,Docker,ConsoleView,WidgetBase,TreeMemory) {

    const DOCKER = types.DOCKER;

    const _ExpressionEditor = dcl([WidgetBase,_LayoutMixin.dcl,ConsoleView.HandlerClass],{
        type:'javascript',
        reparent:false,
        consoleTabTitle:'Editor',
        consoleTabOptions:null,
        consoleTabType:'DefaultTab',
        templateString: '<div class="" style="height:inherit"/>',
        onAddEditorActions:function(evt){

            const actions  = evt.actions, owner  = evt.owner;
            //console.error('onAddEditorActions-console');
            actions.push(owner.createAction({
                label: 'Send',
                command: 'Console/Send',
                icon: 'fa-paper-plane',
                group: 'Console',
                tab:'Home',
                mixin:{
                    addPermission:true
                }
            }));
        },
        getConsoleClass:function(){
            return ConsoleView.ConsoleWidget;
        },
        showDocumentation: function (text) {
            dojo.empty(this.helpContainer);
            const help = dojo.create('div', {
                innerHTML: '',
                className: 'ui-widget'
            });
            this.helpContainer.appendChild(help);
            help.appendChild(dojo.create('span', {
                innerHTML: text,
                className: 'ui-content'
            }));
        },
        createTab:function(type,args){
            return this._docker.addTab(type || 'DefaultTab',utils.mixin({
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:types.DOCKER.TAB.TOP,
                location:types.DOCKER.DOCK.STACKED
            },args));
        },
        createBranch: function (name, where) {
            const branch = {
                name: name,
                group: 'top',
                children: [],
                value: null
            };
            where.push(branch);
            return branch;
        },
        apiItemToLeaf: function (item) {
        },
        addAutoCompleterWord: function (text, help) {
            if (!this.autoCompleterWords) {
                this.autoCompleterWords = [];
            }
            this.autoCompleterWords.push({
                word: text,
                value: text,
                meta: help || ''
            });
        },
        createLeaf: function (label, value, parent, help, isFunction, where) {
            const leaf = {
                name: label,
                group: 'leaf',
                value: value,
                parent: parent,
                help: help,
                isFunction: isFunction != null ? isFunction : true
            };
            this.addAutoCompleterWord(value, help);
            if (where) {
                where.children.push({
                    _reference: label
                })
            }
            return leaf;
        },
        getAPIField: function (item, title) {

            for (let i = 0; i < item.sectionHTMLs.length; i++) {
                const obj = item.sectionHTMLs[i];
                if (obj.indexOf("id = \"" + title + "\"") != -1) {
                    return obj;
                }
            }
            return '';
        },
        insertApiItems: function (startString, dst, where) {

            const result = [];
            for (let i = 0; i < this.api.length; i++) {
                const item = this.api[i];

                if (item.title.indexOf(startString) != -1) {

                    const leafData = this.apiItemToLeaf(item);
                    const title = item.title.replace(startString, '');
                    let description = this.getAPIField(item, 'Description');
                    const parameters = this.getAPIField(item, 'Parameters');
                    const examples = this.getAPIField(item, 'Examples');
                    let syntax = this.getAPIField(item, 'Syntax');
                    description = description + '</br>' + parameters + '<br/>' + examples;
                    syntax = utils.strip_tags(syntax);
                    syntax = syntax.replace('Syntax', '');
                    syntax = utils.replaceAll('\n', '', syntax);
                    syntax = utils.replaceAll('\r', '', syntax);
                    where.push(this.createLeaf(title, syntax, startString.replace('.', ''), description, true, dst));
                }
            }
            return result;
        },
        _createExpressionData: function () {

            const data = {
                items: [],
                identifier: 'name',
                label: 'name'
            };

            const _Array = this.createBranch('Array', data.items);
            this.insertApiItems('Array.', _Array, data.items);

            const _Date = this.createBranch('Date', data.items);
            this.insertApiItems('Date.', _Date, data.items);

            const _Function = this.createBranch('Function', data.items);
            this.insertApiItems('Function.', _Function, data.items);

            const _Math = this.createBranch('Math', data.items);
            this.insertApiItems('Math.', _Math, data.items);

            const _Number = this.createBranch('Number', data.items);
            this.insertApiItems('Number.', _Number, data.items);

            const _Object = this.createBranch('Object', data.items);
            this.insertApiItems('Object.', _Object, data.items);

            const _String = this.createBranch('String', data.items);
            this.insertApiItems('String.', _String, data.items);

            const _RegExp = this.createBranch('RegExp', data.items);
            this.insertApiItems('RegExp.', _RegExp, data.items);

            this.publish(types.EVENTS.ON_EXPRESSION_EDITOR_ADD_FUNCTIONS, {
                root: data,
                widget: this,
                user: this.userData
            });
/*
            var _Variables = this.createBranch('Variables', data.items);
            data.items.push(this.createLeaf('getVariable', 'this.getVariable(\'variableName\')', 'Variables', 'gets a variable', true, _Variables));
            data.items.push(this.createLeaf('setVariable', 'this.setVariable(\'variableName\')', 'Variables', 'sets a variable by name', true, _Variables));
*/
            return data;
        },
        _createExpressionStore: function () {
            const store = declare('apiStore',[TreeMemory],{
                getChildren: function (parent, options) {
                    return this.query({parent: this.getIdentity(parent)});
                },
                mayHaveChildren: function (parent) {
                    return false;
                }
            });
            return new store({
                data: this._createExpressionData().items,
                idProperty:'name'
            });
        },
        insertSymbol: function (val, isFunction) {
            const editor = this.getEditor();
            if (editor) {
                const selection = editor.getSelectedText();
                if (selection && selection.length > 0) {
                    if (isFunction && val.indexOf('(') == -1) {
                        val = val + '(' + selection + ')';
                    } else {
                        val = ' ' + val + ' ';
                    }
                }
                editor.insert(val);
            }
        },
        getEditor:function(){
            return this.driverConsole.console.consoleEditor;
        },
        createWidgets:function(){

            const thiz = this, ctx = this.ctx, deviceManager = ctx.getDeviceManager();

            const DOCKER = types.DOCKER;

            this._docker = Docker.createDefault(this.domNode,{});

            const top = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false
            });

            const _console = deviceManager.openConsole(this.device,top,{
                value:this.value
            });
            this.driverConsole = _console;
            _console.startup();
            const docs = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM,
                h:'300px'

            });

            this.helpContainer = docs.containerNode;
            docs.minSize(200, 100);
            const bottom = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM
            });
            bottom.maxSize(200);
            bottom.getSplitter().pos(0.8);

            const bottomR = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.RIGHT,
                location:DOCKER.TAB.RIGHT,
                target:bottom
            });

            const _gridClass = Grid.createGridClass('driverTreeView',{
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {
                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD
                },
                {
                    //base flip
                },
                {
                    //args
                },
                {
                    GRID: OnDemandGrid,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );
            const store = this._createExpressionStore();
            const gridArgs = {
                ctx:ctx,
                attachDirect:true,
                collection: store.filter({
                    group:'top'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };

            //left:
            grid = utils.addWidget(_gridClass,gridArgs,null,bottom,true);
            /**************************************************************/
            const gridArgsRight = {
                ctx:ctx,
                noDataMessage:'',
                attachDirect:true,
                collection: store.filter({
                    group:'nada'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };

            const gridRight = utils.addWidget(_gridClass,gridArgsRight,null,bottomR,true);

            grid._on('selectionChanged',function(evt){
                const selection = evt.selection,
                      item = selection[0],
                      right = store.query({
                          parent:item.name
                      });

                gridRight.set("collection", store.filter({parent: 'xx'}));
                gridRight.renderArray([]);
                gridRight.renderArray(right);
            });

            gridRight._on('selectionChanged',function(evt){
                const selection = evt.selection;
                const item = selection[0];
                thiz.currentItem = item;
                if (item && item.help) {
                    thiz.showDocumentation(item.help || "");
                }
            });

            bottom.getSplitter().pos(0.5);
            docs.getSplitter().pos(0.8);

            const cBottom=_console.__bottom;
            if(cBottom) {
                cBottom.getSplitter().pos(0.2);
                cBottom.title(false);
                cBottom.title(false);
            }

            docs.$container.find('.panelParent').css('overflow','auto');
            docs.$container.parent().css({
                overflow:'auto'
            });

            this.add(grid,null,false);
            this.add(gridRight,null,false);
            this.add(_console,null,false);

            gridRight.on("dblclick", function (evt) {
                const selection = gridRight.getSelection();
                const item = selection[0];
                thiz.insertSymbol(item.value,item.isFunction);
            });
        },
        startup:function(){
            const thiz = this;
            this.inherited(arguments);
            //pull in ExpressionJavaScript syntax and documentation
            const _re = require;
            _re([
                'xide/widgets/ExpressionJavaScript'], function (ExpressionJavaScript) {
                thiz.api = ExpressionJavaScript.prototype.api;
                thiz.createWidgets();
            });
        }
    });

    return _ExpressionEditor;

});