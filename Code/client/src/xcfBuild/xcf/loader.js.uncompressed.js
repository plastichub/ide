define("xcf/loader", [
    'dojo/_base/declare',
    'xcf/Imports/DojoxImports', //  Extra - Dojox - IMPORTS
    'xcf/Imports/DijitImports', //  Extra - DIJIT - IMPORTS
    'xcf/Widgets',                  //  App Widgets
    'xcf/Managers',                 //  App Manager
    //'xfile/config',                 //  XFile config
    //'xfile/factory/model',          //  XFile  factories
    'xcf/Views',
    'xcf/Widgets',
    'xcf/XCFCommons',
    'xcf/manager/Context'
],function(declare){
    return declare("xcf.loader", null,{});
});