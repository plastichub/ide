define("xcf/types/Types", [
    'xaction/types',
    'xide/types/Types',
    'xide/types',
    'xide/utils/ObjectUtils',
    'xide/utils',
    'xide/utils/HexUtils'
], function (aTypes, cTypes, types, ObjectUtils, utils, HexUtils) {
    

    if (!String.prototype.setBytes) {
        String.prototype.setBytes = function (bytes) {
            this.bytes = bytes;
        };
    }
    if (!String.prototype.getBytes) {
        String.prototype.getBytes = function () {
            if (this.bytes) {
                return this.bytes;
            } else {
                return HexUtils.stringToBuffer(this);
            }
        };
    }
    if (!String.prototype.getString) {
        String.prototype.getString = function () {
            return this.string;
        };
    }
    if (!String.prototype.setString) {
        String.prototype.setString = function (string) {
            this.string = string;
        };
    }

    if (!String.prototype.hexString) {
        String.prototype.hexString = function () {
            const bytes = this.getBytes();
            return HexUtils.bufferToHexString(bytes);
        };
    }

    /**
     * Variable Flags
     *
     * @enum {int} VARIABLE_FLAGS
     * @global
     */
    types.VARIABLE_FLAGS = {
        PUBLISH: 0x00000002,
        PUBLISH_IF_SERVER: 0x00000004
    };
    /**
     * Flags to define logging outputs per device or view
     *
     * @enum {int} LOGGING_FLAGS
     * @global
     */
    types.LOGGING_FLAGS = {
        /**
         * No logging
         * @constant
         * @type int
         */
        NONE: 0x00000000,
        /**
         * Log in the IDE's global console
         * @constant
         * @type int
         */
        GLOBAL_CONSOLE: 0x00000001,
        /**
         * Log in the IDE's status bar
         * @constant
         * @type int
         */
        STATUS_BAR: 0x00000002,
        /**
         * Create notification popup in the IDE
         * @constant
         * @type int
         */
        POPUP: 0x00000004,
        /**
         * Log to file
         * @constant
         * @type int
         */
        FILE: 0x00000008,
        /**
         * Log into the IDE's dev tool's console
         * @constant
         * @type int
         */
        DEV_CONSOLE: 0x00000010,
        /**
         * Log into the device's IDE console
         * @constant
         * @type int
         */
        DEVICE_CONSOLE: 0x00000020
    };


    /**
     * Help struct for jsDoc
     * @typedef {object} module:xide/types~DeviceInfo
     * @type Object
     * @property {string} host The IP address
     * @property {string} port The port
     * @property {string} scope The scope of the device
     * @property {string} driverId The id of the driver
     * @property {string} protocol The protocol, ie: tcp, serial,..
     * @property {string} devicePath The absolute path to the device's meta file
     * @property {string} id The device model id
     * @property {string} title The title of the device
     * @property {string} source Additional field to carry a source. That might be 'ide' or 'server'.
     * @property {string} deviceScope Field to store the device's scope: user_devices or system_devices
     * @property {string} driverScope Field to store the device's driver scope: user_drivers or system_drivers
     * @property {string} user_devices Absolute path to the user's devices
     * @property {string} system_devices Absolute path to the system devices
     * @property {string} user_drivers Absolute path to the user's drivers
     * @property {string} system_drivers Absolute path to the drivers drivers
     * @property {string} loggingFlags Absolute path to the user's drivers
     * @property {int} serverSide The device's driver runs server side if 1, otherwise 0
     * @property {string} hash A hash for client side. Its build of MD5(host,port,protocol,driverId,driverScope,id,devicePath,deviceScope,source,user_devices,system_devices,system_drivers,user_drivers)
     * @property {DRIVER_FLAGS} driverOptions The driver flags
     * @property {LOGGING_FLAGS} loggingFlags The device's logging flags
     * @property {object} responseSettings Contains the constants for receiving data from a device its being set at initialization time and has this structure:
     * @property {boolean} responseSettings.start 
     */


    utils.mixin(types.ITEM_TYPE, {
        DEVICE: 'Device',
        DEVICE_GROUP: 'Device Group',
        DRIVER: 'Driver',
        DRIVER_GROUP: 'Driver Group',
        PROTOCOL: 'Protocol',
        PROTOCOL_GROUP: 'Protocol Group'
    });

    /**
     * Possible Node-JS service status modes.
     *
     * @constant {Integer.<module:xide/types~SERVICE_STATUS>}
     *     module:xide/types~SERVICE_STATUS
     */
    types.SERVICE_STATUS = {
        OFFLINE: "offline",
        ONLINE: "online",
        TIMEOUT: "timeout"
    };

    /**
     * @enum {string} CONNECTION_PROTOCOL
     * @global
     */
    types.PROTOCOL = {
        TCP: 'tcp',
        UDP: 'udp',
        SERIAL: 'serial',
        DRIVER: 'driver',
        SSH: 'ssh',
        MQTT: 'mqtt'
    };
    /**
     * Additional event keys
     * @enum {string} module:xcf/types/EVENTS
     * @extends module:xide/types/EVENTS
     */
    const Events = {
        ON_DEBUGGER_READY: 'onDebuggerReady',
        ON_DEVICE_SELECTED: 'onDeviceSelected',
        ON_DEVICE_GROUP_SELECTED: 'onDeviceGroupSelected',
        ON_PROTOCOL_SELECTED: 'onProtocolSelected',
        ON_PROTOCOL_GROUP_SELECTED: 'onProtocolGroupSelected',
        ON_PROTOCOL_CHANGED: 'onProtocolChanged',
        ON_MQTT_MESSAGE: 'onMQTTMessage',
        ON_DEVICE_MESSAGE: 'onDeviceMessage',
        ON_DEVICE_MESSAGE_EXT: 'onDeviceMessageExt',
        ON_COMMAND_FINISH: 'onCommandFinish',
        ON_COMMAND_PROGRESS: 'onCommandProgress',
        ON_COMMAND_PAUSED: 'onCommandPaused',
        ON_COMMAND_STOPPED: 'onCommandStopped',
        ON_COMMAND_ERROR: 'onCommandError',
        ON_DEVICE_DISCONNECTED: 'onDeviceDisconnected',
        ON_DEVICE_CONNECTED: 'onDeviceConnected',
        ON_DEVICE_COMMAND: 'onDeviceCommand',
        ON_DEVICE_STATE_CHANGED: 'onDeviceStateChanged',
        ON_DEVICE_DRIVER_INSTANCE_READY: 'onDeviceDriveInstanceReady',
        ON_DRIVER_SELECTED: 'onDriverSelected',
        ON_DRIVER_GROUP_SELECTED: 'onDriverGroupSelected',
        ON_DRIVER_VARIABLE_ADDED: 'onDriverVariableAdded',
        ON_DRIVER_VARIABLE_REMOVED: 'onDriverVariableRemoved',
        ON_DRIVER_VARIABLE_CHANGED: 'onDriverVariableChanged',
        ON_DRIVER_COMMAND_ADDED: 'onDriverCommandAdded',
        ON_DRIVER_COMMAND_REMOVED: 'onDriverCommandRemoved',
        ON_DRIVER_COMMAND_CHANGE: 'onDriverVariableChanged',
        ON_SCOPE_CREATED: 'onScopeCreated',
        ON_DRIVER_MODIFIED: 'onDriverModified',
        SET_DEVICE_VARIABLES: 'setDeviceVariables',
        ON_SERVER_LOG_MESSAGE: 'onServerLogMessage',
        ON_CLIENT_LOG_MESSAGE: 'onClientLogMessage',
        ON_DEVICE_SERVER_CONNECTED: 'onDeviceServerConnected',
        ON_RUN_CLASS_EVENT: 'onRunClassEvent'
    };

    utils.mixin(types.EVENTS, Events);

    /**
     * Enumeration to define a source type for variable change.
     * @enum module:xide/types/MESSAGE_SOURCE
     * @memberOf module:xide/types
     */
    types.MESSAGE_SOURCE = {
        DEVICE: 'DEVICE',
        GUI: 'GUI',
        BLOX: 'BLOX',
        CODE: 'CODE'
    };

    /**
     *
     * the device state
     * typedef {Object} xide.types~STATE
     *-@memberOf module:xide/types
     * -property {Number|String} [x] - X coordinate of the rectangle. Can be a pixel position, or a string with a 'px' or '%' suffix.
     */


    /**
     * Enumeration to define a device's status
     * @enum {String} module:xide/types/DEVICE_STATE
     * @memberOf module:xide/types
     */
    types.DEVICE_STATE = {
        CONNECTING: 'DeviceIsConnecting',
        CONNECTED: 'DeviceIsConnected',
        SYNCHRONIZING: 'DeviceIsSynchronizing',
        READY: 'DeviceIsReady',
        DISCONNECTED: 'DeviceIsDisconnected',
        DISABLED: 'DeviceIsDisabled',
        LOST_DEVICE_SERVER: 'LostDeviceServerConnection'
    };

    /**
     * Keys to define a driver meta property
     * @enum module:xide/types/DRIVER_PROPERTY
     * @memberOf module:xide/types
     */
    types.DRIVER_PROPERTY = {
        CF_DRIVER_NAME: 'CF_DRIVER_NAME',
        CF_DRIVER_ICON: 'CF_DRIVER_ICON',
        CF_DRIVER_CLASS: 'CF_DRIVER_CLASS',
        CF_DRIVER_ID: 'CF_DRIVER_ID',
        CF_DRIVER_COMMANDS: 'CF_DRIVER_COMMANDS',
        CF_DRIVER_VARIABLES: 'CF_DRIVER_VARIABLES',
        CF_DRIVER_RESPONSES: 'CF_DRIVER_RESPONSES'
    };

    /**
     * Keys to define protocol meta properties
     * @enum module:xide/types/PROTOCOL_PROPERTY
     * @memberOf module:xide/types
     */
    types.PROTOCOL_PROPERTY = {
        CF_PROTOCOL_TITLE: 'Title',
        CF_PROTOCOL_ICON: 'CF_PROTOCOL_ICON',
        CF_PROTOCOL_CLASS: 'CF_PROTOCOL_CLASS',
        CF_PROTOCOL_ID: 'CF_PROTOCOL_ID',
        CF_PROTOCOL_COMMANDS: 'CF_PROTOCOL_COMMANDS',
        CF_PROTOCOL_VARIABLES: 'CF_PROTOCOL_VARIABLES',
        CF_PROTOCOL_RESPONSES: 'CF_PROTOCOL_RESPONSES'
    };

    /**
     * Keys to define protocol meta properties
     * @enum module:xide/types/DEVICE_PROPERTY
     * @memberOf module:xide/types
     */
    types.DEVICE_PROPERTY = {
        CF_DEVICE_DRIVER: 'Driver',
        CF_DEVICE_HOST: 'Host',
        CF_DEVICE_PORT: 'Port',
        CF_DEVICE_PROTOCOL: 'Protocol',
        CF_DEVICE_TITLE: 'Title',
        CF_DEVICE_ID: 'Id',
        CF_DEVICE_ENABLED: 'Enabled',
        CF_DEVICE_OPTIONS: 'Options',
        CF_DEVICE_DRIVER_OPTIONS: 'DriverOptions',
        CF_DEVICE_LOGGING_FLAGS: 'Logging Flags'
    };


    /**
     * @enum {int} DEVICE_LOGGING_SOURCE
     * @global
     */
    types.LOG_OUTPUT = {
        DEVICE_CONNECTED: 'Device Connected',
        DEVICE_DISCONNECTED: 'Device Disonnected',
        RESPONSE: 'Response',
        SEND_COMMAND: 'Send Command',
        DEVICE_ERROR: 'Device Error'
    };

    /**
     * @enum {int} DEFAULT_DEVICE_LOGGING_FLAGS
     * @global
     */
    types.DEFAULT_DEVICE_LOGGING_FLAGS = {};

    const LOGGING_FLAGS = types.LOGGING_FLAGS;

    types.DEFAULT_DEVICE_LOGGING_FLAGS[types.LOG_OUTPUT.DEVICE_CONNECTED] = LOGGING_FLAGS.GLOBAL_CONSOLE | LOGGING_FLAGS.POPUP | LOGGING_FLAGS.STATUS_BAR | LOGGING_FLAGS.DEVICE_CONSOLE;
    types.DEFAULT_DEVICE_LOGGING_FLAGS[types.LOG_OUTPUT.DEVICE_DISCONNECTED] = LOGGING_FLAGS.GLOBAL_CONSOLE | LOGGING_FLAGS.POPUP | LOGGING_FLAGS.STATUS_BAR | LOGGING_FLAGS.DEVICE_CONSOLE;
    types.DEFAULT_DEVICE_LOGGING_FLAGS[types.LOG_OUTPUT.RESPONSE] = LOGGING_FLAGS.DEVICE_CONSOLE | LOGGING_FLAGS.GLOBAL_CONSOLE;
    types.DEFAULT_DEVICE_LOGGING_FLAGS[types.LOG_OUTPUT.SEND_COMMAND] = LOGGING_FLAGS.DEVICE_CONSOLE | LOGGING_FLAGS.GLOBAL_CONSOLE;
    types.DEFAULT_DEVICE_LOGGING_FLAGS[types.LOG_OUTPUT.DEVICE_ERROR] = LOGGING_FLAGS.GLOBAL_CONSOLE | LOGGING_FLAGS.POPUP | LOGGING_FLAGS.STATUS_BAR | LOGGING_FLAGS.DEV_CONSOLE | LOGGING_FLAGS.DEVICE_CONSOLE;

    /**
     * Bitmask or flags for device about its driver
     * @enum {int} DRIVER_FLAGS
     * @global
     */
    types.DRIVER_FLAGS = {
        /**
         * Mark the driver for "server side"
         */
        RUNS_ON_SERVER: 2,
        /**
         * Enable protocol's debug message on console
         */
        DEBUG: 4,
        /**
         * Enable protocol's debug message on console
         */
        SERVER: 16
    };


    const ITEM_TYPES = {
        /**
         *
         * @extends module:xide/types~ITEM_TYPE
         */
        CF_DRIVER_VARIABLE: 'DriverVariable',
        CF_DRIVER_BASIC_COMMAND: 'DriverBasicCommand',
        CF_DRIVER_CONDITIONAL_COMMAND: 'DriverConditionalCommand',
        CF_DRIVER_RESPONSE_VARIABLE: 'DriverResponseVariable'
    };

    utils.mixin(types.ITEM_TYPE, ITEM_TYPES);

    types.BLOCK_GROUPS =
        {
            CF_DRIVER_VARIABLE: 'DriverVariable',
            CF_DRIVER_BASIC_COMMAND: 'DriverBasicCommand',
            CF_DRIVER_CONDITIONAL_COMMAND: 'DriverConditionalCommand',
            CF_DRIVER_RESPONSE_VARIABLE: 'DriverResponseVariable',
            CF_DRIVER_RESPONSE_BLOCKS: 'conditionalProcess',
            CF_DRIVER_RESPONSE_VARIABLES: 'processVariables',
            CF_DRIVER_BASIC_VARIABLES: 'basicVariables'
        };

    types.COMMAND_TYPES =
        {
            BASIC_COMMAND: 'basic',
            CONDITIONAL_COMMAND: 'conditional',
            INIT_COMMAND: 'init'
        };


    /**
     * Mixin new Core types
     */
    utils.mixin(types.ECIType, {
        DEVICE_NETWORK_SETTINGS: types.ECIType.END + 1,
        DRIVER_COMMAND_SETTINGS: 'CommandSettings'
    });

    types.VFS_ROOTS = {

        SYSTEM_DRIVERS: 'system_drivers',
        USER_DRIVERS: 'user_drivers'
    };

    /**
     * Device Server Socket Commands
     * @enum {string} SERVER_COMMAND
     * @global SERVER_COMMAND
     */
    types.SOCKET_SERVER_COMMANDS =
        {
            SIGNAL_MANAGER: 'Manager_command',
            RUN_FILE: 'Run_File',
            RUN_CLASS: 'Run_Class',
            RUN_APP_SERVER_CLASS: 'Run_App_Server_Class',
            RUN_APP_SERVER_CLASS_METHOD: 'Run_App_Server_Class_Method',
            RUN_APP_SERVER_COMPONENT_METHOD: 'Run_App_Server_Component_Method',
            CANCEL_APP_SERVER_COMPONENT_METHOD: 'Cancel_App_Server_Component_Method',
            ANSWER_APP_SERVER_COMPONENT_METHOD_INTERRUPT: 'Answer_App_Server_Component_Method_Interrupt',
            SIGNAL_DEVICE: 'Device_command',
            SIGNAL_RESPONSE: 'WebSocket_response',
            MANAGER_TEST: 'Manager_Test',
            MANAGER_CLOSE_ALL: 'Close_All_Connections',
            MANAGER_STATUS: 'status',
            MANAGER_START_DRIVER: 'startDriver',
            START_DEVICE: 'startDevice',
            STOP_DEVICE: 'stopDevice',
            CREATE_CONNECTION: 'createConnection',
            MANAGER_STOP_DRIVER: 'stopDriver',
            DEVICE_SEND: 'Device_Send',
            CALL_METHOD: 'Call_Method',
            RUN_SHELL: 'Run_Shell',
            WATCH: 'Watch_Directory',
            MQTT_PUBLISH: 'MQTT_PUBLISH',
            MQTT_SUBSCRIBE: 'MQTT_SUBSCRIBE',
            GET_DEVICE_VARIABLES: 'getVariables',
            WRITE_LOG_MESSAGE: 'Write_Log_Message',
            INIT_DEVICES: 'INIT_DEVICES',
            PROTOCOL_METHOD: 'PROTOCOL_METHOD'
        };
    return types;
});
