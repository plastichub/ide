/** @module xcf/model/Device */
define("xcf/model/Device", [
    "dcl/dcl",
    "xide/data/Model",
    'xide/data/Source',
    'xide/types',
    'xide/utils',
    'xide/mixins/EventedMixin',
    "xcf/types/Types"
], function (dcl, Model, Source, types, utils, EventedMixin) {
    /**
     *
     * Model for a device. It extends the base model class
     * and acts a source.
     *
     * @module xcf/model/Device
     * @extends {module:xide/mixins/EventedMixin}
     * @augments module:xide/data/Model
     * @augments module:xide/data/Source
     */
    const Module = dcl([Model, Source.dcl, EventedMixin.dcl], {
        declaredClass: 'xcf.model.Device',
        _userStopped: false,
        /**
         * @type {module:xide/types~DEVICE_STATE}
         * @link module:xide/types/DEVICE_STATE
         * @see module:xide/types/DEVICE_STATE
         */
        state: types.DEVICE_STATE.DISCONNECTED,
        /**
         * The driver instance
         * @private
         */
        driverInstance: null,
        /**
         * The block scope of the driver instance (if the device is connected and ready)
         * @private
         */
        blockScope: null,
        getParent: function () {
            return this.getStore().getSync(this.parentId);
        },
        isServerSide: function () {
            const driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            return (1 << types.DRIVER_FLAGS.RUNS_ON_SERVER & driverOptions) ? true : false;
        },
        isServer: function () {
            const driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            return (1 << types.DRIVER_FLAGS.SERVER & driverOptions) ? true : false;
        },
        setServer: function (isServer) {
            const driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            driverOptions.value = driverOptions.value | (1 << types.DRIVER_FLAGS.SERVER);
            this.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS, driverOptions.value);
        },
        setServerSide: function (isServer) {
            let driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            driverOptions = driverOptions | (1 << types.DRIVER_FLAGS.RUNS_ON_SERVER);
            this.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS, driverOptions);
        },
        isDebug: function () {
            const driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            return (1 << types.DRIVER_FLAGS.DEBUG & driverOptions) ? true : false;
        },
        check: function () {
            if (this._startDfd && this._userStopped === true) {
                this.reset();
            }
        },
        getStore: function () {
            return this._store;
        },
        getScope: function () {
            const store = this.getStore();
            return store ? store.scope : this.scope;
        },
        isEnabled: function () {
            return this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED) === true;
        },
        setEnabled: function (enabled) {
            return this.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED, enabled);
        },
        shouldReconnect: function () {
            if (this._userStopped) {
                return false;
            }
            return this.isEnabled();
        },
        reset: function () {
            delete this._startDfd;
            this._startDfd = null;
            delete this['blockScope'];
            this['blockScope'] = null;
            delete this.serverVariables;
            this.serverVariables = null;
            delete this['driverInstance'];
            this['driverInstance'] = null;
            clearTimeout(this.reconnectTimer);
            delete this.lastReconnectTime;
            delete this.reconnectRetry;
            delete this.isReconnecting;
            this.setState(types.DEVICE_STATE.DISCONNECTED);
        },
        /**
         * @constructor
         * @alias module:xcf/model/Device
         */
        constructor: function () { },
        /**
         * Returns the block scope of the a driver's instance
         * @returns {module:xblox/model/Scope}
         */
        getBlockScope: function () {
            //return this.blockScope;
            return this.driverInstance && this.driverInstance.blockScope ? this.driverInstance.blockScope : this.blockScope;
        },
        /**
         * Returns the driver instance
         * @returns {model:xcf/driver/DriverBase}
         */
        getDriverInstance: function () {
            return this.driverInstance;
        },
        /**
         * Return the driver model item
         * @returns {module:xcf/model/Driver|null}
         */
        getDriver: function () {
            const scope = this.getBlockScope();
            if (scope) {
                return scope.driver;
            }
            return null;
        },
        /**
         * Return a value by field from the meta database
         * @param title
         * @returns {string|int|boolean|null}
         */
        getMetaValue: function (title) {
            return utils.getCIInputValueByName(this.user, title);
        },
        /**
         * Set a value in the meta database
         * @param title {string} The name of the CI
         * @returns {void|null}
         */
        setMetaValue: function (what, value, publish) {
            const item = this;
            const meta = this.user;
            const ci = utils.getCIByChainAndName(meta, 0, what);
            if (!ci) {
                return null;
            }
            const oldValue = this.getMetaValue(what);
            utils.setCIValueByField(ci, 'value', value);
            this[what] = value;
            if (publish !== false && oldValue != value) {
                const eventArgs = {
                    owner: this.owner,
                    ci: ci,
                    newValue: value,
                    oldValue: oldValue
                };
                return this.publish(types.EVENTS.ON_CI_UPDATE, eventArgs);
            }
        },
        /**
         * Return the internal state icon
         * @param state
         * @returns {string|null}
         */
        getStateIcon: function (state) {
            state = state || this.state;
            switch (state) {
                case types.DEVICE_STATE.DISCONNECTED:
                    {
                        return 'fa-unlink iconStatusOff';
                    }
                case types.DEVICE_STATE.READY:
                case types.DEVICE_STATE.CONNECTED:
                    {
                        return 'fa-link iconStatusOn';
                    }
                case types.DEVICE_STATE.SYNCHRONIZING:
                case types.DEVICE_STATE.CONNECTING:
                    {
                        return 'fa-spinner fa-spin';
                    }
                case types.DEVICE_STATE.LOST_DEVICE_SERVER:
                    {
                        return 'fa-spinner fa-spin';
                    }
            }
            return 'fa-unlink iconStatusOff';
        },
        /**
         * Set the state
         * @param state
         * @param silent
         */
        setState: function (state, silent) {
            if (state === this.state) {
                return;
            }
            const oldState = this.state;
            const icon = this.getStateIcon(state);
            this.state = state;
            this.set('iconClass', icon);
            this.set('state', state);
            this._emit(types.EVENTS.ON_DEVICE_STATE_CHANGED, {
                old: oldState,
                state: state,
                icon: icon,
                "public": true
            });
            this.refresh('state');
        }
    });
    Module.createSubclass = Model.createSubclass;
    return Module;
});
