/**
 * This file is used to reconfigure parts of the loader at runtime for this application. We’ve put this extra
 * configuration in a separate file, instead of adding it directly to index.html, because it contains options that
 * can be shared if the application is run on both the client and the server.
 *
 * If you aren’t planning on running your app on both the client and the server, you could easily move this
 * configuration into index.html (as a dojoConfig object) if it makes your life easier.
 */
function __getLocation(name,_default){
    if(typeof location !=='undefined'){
        if(location.href.indexOf(name +'=' + 'release')!==-1){
            return 'build/' + name + '-release/' + name;
        }else if(location.href.indexOf(name +'=' + 'debug')!==-1){
            return name;
        }
        if(_default === 'release'){
            return 'build/' + name + '-release/' + name;
        }
    }

    return _default || name;


}
function __getLocation2(name,_default,offset){
    if(typeof location !=='undefined'){
        offset  = offset || "";
        if(location.href.indexOf(name +'=' + 'release')!==-1){
            return name + '/build/';
        }else if(location.href.indexOf(name +'=' + 'debug')!==-1){
            return name + '/' + offset;
        }
        if(_default === 'release'){
            return '../build/' + name;
        }
    }

    return _default || name;


}
require({
    // The base path for all packages and modules. If you don't provide this, baseUrl defaults to the directory
    // that contains dojo.js. Since all packages are in the root, we just leave it blank. (If you change this, you
    // will also need to update app.profile.js).

    // A list of packages to register. Strictly speaking, you do not need to register any packages,
    // but you can't require "xbox" and get xbox/main.js if you do not register the "xbox" package (the loader will look
    // for a module at <baseUrl>/xbox.js instead). Unregistered packages also cannot use the packageMap feature, which
    // might be important to you if you need to relocate dependencies. TL;DR, register all your packages all the time:
    // it will make your life easier.
    packages: [
        {
            name: 'xide_ts',
            location: 'xide_ts/amd/'
        },
        // If you are registering a package that has an identical name and location, you can just pass a string
        // instead, and it will configure it using that string for both the "name" and "location" properties. Handy!
        {
            name: 'dojo',
            location: 'dojo',
            packageMap: {}
        },
        {
            name: 'core',
            location: 'core/_build/src',
            packageMap: {}
        },
        {
            name: 'xgrid',
            _location: 'xgrid',
            location: __getLocation2('xgrid','release'),
            packageMap: {}
        },
        {
            name: 'xlog',
            location: __getLocation2('xlog','release'),
            packageMap: {}
        },
        {
            name: 'ts-events',
            location: 'xide/node_modules/ts-events/src/lib',
            packageMap: {}
        },
        {
            name: 'xide-ts',
            location: 'xide-ts/dist/scripts/',
            packageMap: {}
        },
        {
            name: 'xnode',
            location: __getLocation2('xnode','release')
        },
        {
            name: 'dijit',
            //location: __getLocation2('dijit','release'),
            location: 'dijit'
        },
        {
            name: 'orion',
            _location: 'build/orion-release/orion',
            location: 'orion'
        },
        {
            name: 'dojox',
            location:__getLocation2('dojox','release')
        },
        {
            name: 'xapp',
            location: 'xapp'
        },
        {
            name: 'xbox',
            location: 'xbox'
        },
        {
            name: 'xide',
            location: 'xide'
        },
        {
            name: 'dgrid',
            location: __getLocation2('dgrid','release')
        },
        {
            name: 'xfile',
            _location: 'build/xfile-release/xfile',
            location: __getLocation2('xfile','release')
        },
        {
            name: 'xblox',
            location: __getLocation2('xblox','release')
        },
        {
            name: 'xblox-docs',
            location:'xblox/docs'
        },
        {
            name: 'xcf',
            location: 'xcf'
        },
        {
            name: 'davinci',
            location: __getLocation2('davinci','release')
        },
        {
            name: 'xideve',
            location: 'xideve'
        },
        {
            name: 'system',
            location: 'system'
        },
        {
            name: 'preview',
            location: 'preview'
        },
        {
            name: 'xwire',
            location: __getLocation2('xwire','release')
        },
        {
            name: 'xexpression',
            location: 'xexpression'
        },
        {
            name: 'ace',
            location: '../xfile/ext/ace'
        },
        {
            name: 'liaison',
            location: 'ibm-js/liaison'
        },
        {
            name: 'decor',
            location: 'ibm-js/decor'
        },
        {
            name: 'dmodel',
            location: 'dmodel'
        },
        {
            name: 'requirejs-dplugins',
            location: 'requirejs-dplugins'
        },
        {
            name: 'xdocker',
            location: __getLocation2('xdocker','release')
        },
        {
            name: 'xace',
            location: __getLocation2('xace','release','src')
        },
        {
            name: 'wcDocker',
            location: __getLocation2('wcDocker','release','src')
        },
        {
            name: 'x-markdown',
            _location: __getLocation2('x-markdown','release','src'),
            location: 'x-markdown/src'
        },
        {
            name: 'xlang',
            location: 'xlang',
            packageMap: {}
        },
        {
            name: 'xaction',
            location: __getLocation2('xaction','release','src')
        },
        {
            name: 'xtrack',
            location: __getLocation2('xtrack','release','src'),
            location:'xtrack/src'
        },
        {
            name: 'xconsole',
            location: 'xconsole/src'
        },
        {
            name: 'nxapp',
            location: '../../../../server/nodejs/nxapp'
        },
        'xcf'
    ],
    cache: {}
// Require 'xbox'. This loads the main application file, xbox/main.js.
}, ['xcf']);
