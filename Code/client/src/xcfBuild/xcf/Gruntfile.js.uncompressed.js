/*global module */
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        jshint: {
            src: [
                "**/*.js",
                "!./app.profile.js",
                "!./MaqettaPatch.js",
                "!{node_modules,dijit,form,layout,mobile}/**/*.js",

                // Note: skip this file since it gives a JSHint error about a character being silently deleted.
                // It will have to be fixed by the translators.
                "!nls/he/loading.js"
            ],
            options: {
                jshintrc: ".jshintrc"
            }
        },
        "jsdoc-amddcl": {
            "plugins": [
                "plugins/markdown"
            ],
            docs: {
                files: [
                    {
                        src: [
                            "./manager/*.js",
                            "./driver/*.js",
                            "!./node_modules"
                        ]

                    }
                ]
            },
            'export': {
                files: [
                    {
                        args: [
                            "-X"
                        ],
                        src: [
                            "./"

                        ],
                        dest: "/tmp/doclets.json"
                    }
                ]
            }
        }
    });

    // Load plugins
    grunt.loadNpmTasks("intern");
    grunt.loadNpmTasks("grunt-contrib-jshint");

    grunt.loadNpmTasks("jsdoc-amddcl");
    //grunt.loadTasks("themes/tasks");// Custom cssToJs task to convert CSS to JS

    // Aliases
    //grunt.registerTask("css", ["less", "cssToJs"]);
    grunt.registerTask("jsdoc", "jsdoc-amddcl");

    // Testing.
    // Always specify the target e.g. grunt test:remote, grunt test:remote
    // then add on any other flags afterwards e.g. console, lcovhtml.
    const testTaskDescription = "Run this task instead of the intern task directly! \n" +
        "Always specify the test target e.g. \n" +
        "grunt test:local\n" +
        "grunt test:remote\n\n" +
        "Add any optional reporters via a flag e.g. \n" +
        "grunt test:local:console\n" +
        "grunt test:local:lcovhtml\n" +
        "grunt test:local:console:lcovhtml";
    grunt.registerTask("test", testTaskDescription, function (target) {
        function addReporter(reporter) {
            const property = "intern." + target + ".options.reporters";
            const value = grunt.config.get(property);
            if (value.indexOf(reporter) !== -1) {
                return;
            }
            value.push(reporter);
            grunt.config.set(property, value);
        }

        if (this.flags.lcovhtml) {
            addReporter("lcovhtml");
        }

        if (this.flags.console) {
            addReporter("console");
        }
        grunt.task.run("intern:" + target);
    });
};