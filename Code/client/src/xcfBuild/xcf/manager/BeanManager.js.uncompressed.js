/** @module xcf/manager/BeanManager **/
define("xcf/manager/BeanManager", [
    'dcl/dcl',
    'xdojo/has',
    "dojo/_base/lang",
    'xide/types',
    'xide/utils',
    'xide/manager/BeanManager',
    "dojo/Deferred",
    "xide/noob",
    "xdojo/has!xtrack?xide/interface/Track",
    'xdojo/has!xcf-ui?xide/views/ActionDialog',
    'xdojo/has!xcf-ui?xide/views/CIActionDialog',
    'xdojo/has!xcf-ui?xide/views/CIGroupedSettingsView'
], function (dcl, has, lang, types, utils, BeanManager, Deferred, noob, Track, ActionDialog, CIActionDialog, CIGroupedSettingsView) {
    /**
     * @class module:xcf/manager/BeanManager_Base
     * @extends {module:xide/manager/BeanManager}
     * @augments {module:xide/manager/ManagerBase}
     */
    const Base = dcl(BeanManager, {
        /**
         * Return device model item by device id
         * @param path
         * @returns {module:xcf/model/Device} The device
         */
        getItemByPath: function (path) {
            function search(store) {
                return store.getSync(path);
            }
            for (const scope in this.stores) {
                const store = this.stores[scope];
                const result = search(store);
                if (result) {
                    return result;
                }
            }
        },
        setStore: function (scope, store) {
            const current = this.stores[scope];
            if (current) {
                console.error('setting existing store ' + scope);
                current.destroy();
                delete this.stores[scope];
            }
            this.stores[scope] = store;
            return store;
        },
        onCIUpdate: function (evt) {
            if (evt['owner'] === this) {
                this.updateCI(evt.ci, evt.newValue, evt.oldValue, evt.storeItem);
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        // std impl.
        //
        /////////////////////////////////////////////////////////////////////////////////////
        init: function () {
            this.stores = {};
            this.subscribe(types.EVENTS.ON_CI_UPDATE);
        }
    });

    if ( true ) {
        /**
         * @class module:xcf/manager/BeanManager
         * @extends {module:xcf/manager/BeanManager_Base}
         */
        return dcl(Base, {
            declaredClass: "xcf.manager.BeanManager",
            /**
             * Bean protocol impl.
             */
            getViewClass: function (trackImpl, ViewClass) {
                trackImpl = trackImpl || {};
                utils.mixin(trackImpl, {
                    startup: function () {
                        if (Track) {
                            this.getContext().getTrackingManager().track(
                                this.getTrackingCategory(),
                                this.getTrackingLabel(this.item),
                                this.getTrackingUrl(this.item),
                                types.ACTION.OPEN,
                                this.getContext().getUserDirectory()
                            );
                        }
                        if (this.inherited) {
                            return this.inherited(arguments);
                        }
                    }
                })
                return dcl([ViewClass || CIGroupedSettingsView, Track ? Track.dcl : noob.dcl], trackImpl);
            },
            /**
             *
             * @param bean
             * @returns {module:xfile/model/File|string}
             */
            getFile: function (bean) {
                const dfd = new Deferred();

                const ctx = this.getContext();
                const fileManager = ctx.getFileManager();
                const fileStore = fileManager.getStore(bean.scope);
                fileStore.initRoot().then(function () {
                    fileStore._loadPath('.', true).then(function () {
                        fileStore.getItem(bean.path, true).then(function (item) {
                            dfd.resolve(item);
                        });
                    });
                });
                return dfd;
            },
            /**
             * Url generator for device/driver/[command|block|variable]
             *
             * @param device {string} the device id
             * @param driver {string} the driver id
             * @param block {string} the block id
             * @param prefix {string} protocol schema
             * @returns {*}
             */
            toUrl: function (device, driver, block, prefix) {
                const pattern = (prefix || '') + "deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";
                return lang.replace(pattern,
                    {
                        deviceId: device.id,
                        deviceScope: device.scope,
                        driverId: driver.id,
                        driverScope: driver.scope,
                        block: block ? block.id : ""
                    });
            },
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Bean Management
            //
            /////////////////////////////////////////////////////////////////////////////////////
            /**
             * Creates new group item dialog
             */
            newGroup: function () {
                const thiz = this;
                const currentItem = this.getItem();
                const parent = currentItem ? currentItem.isDir === true ? currentItem.path : '' : '';

                const actionDialog = new CIActionDialog({
                    title: 'New ' + this.groupType,
                    delegate: {
                        onOk: function (dlg, data) {
                            const title = utils.getCIInputValueByName(data, 'Title');
                            const scope = utils.getCIInputValueByName(data, 'Scope');
                            const _final = parent + '/' + title;
                            thiz.createGroup(scope, _final, function () {
                                const newItem = thiz.createNewGroupItem(title, scope, parent);
                                thiz.store.putSync(newItem);
                                thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                    owner: thiz,
                                    store: thiz.store,
                                    action: types.NEW_DIRECTORY,
                                    item: newItem
                                });
                            });
                        }
                    },
                    cis: [
                        utils.createCI('Title', 13, ''),
                        utils.createCI('Scope', 3, this.defaultScope, {
                            "options": [
                                {
                                    label: 'System',
                                    value: this.defaultScope
                                },
                                {
                                    label: 'User',
                                    value: this.userScope
                                }
                            ]
                        })
                    ]
                });
                actionDialog.startup();
                actionDialog.show();
            },
            onDeleteItem: function (item) {
                const isDir = utils.toBoolean(item.isDir) === true;
                const removeFn = isDir ? 'removeGroup' : 'removeItem';
                const thiz = this;
                const actionDialog = new ActionDialog({
                    title: 'Remove ' + this.beanName + (isDir ? ' Group' : '') + ' ' + "\"" + item.name + "\"  ",
                    style: 'max-width:400px',
                    titleBarClass: 'text-danger',
                    delegate: {
                        isRemoving: false,
                        onOk: function () {
                            thiz[removeFn](
                                utils.toString(item.scope),
                                utils.toString(item.path),
                                utils.toString(item.name),
                                function () {
                                    thiz.onItemDeleted(item);
                                    thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                        owner: thiz,
                                        store: thiz.store,
                                        action: types.DELETE,
                                        item: item
                                    });
                                });
                        }
                    }
                });
                actionDialog.show();
            },
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Bean server protocol
            //
            /////////////////////////////////////////////////////////////////////////////////////
            createNewGroupItem: function (title, scope, parent) {
                return this.createItemStruct(title, scope, parent, title, true, this.groupType);
            },
            createNewItem: function (title, scope, parent) {
                return this.createItemStruct(title, scope, parent, parent + "/" + title, false, this.itemType);
            },
            createItem: function (scope, path, title, meta, code) {
                return this.runDeferred(null, 'createItem', [scope, path, title, meta, code]);
            },
            /***
             * ls is enumerating all drivers in a given scope
             * @param scope {string}
             * @param track {boolean}
             * @returns {Deferred}
             */
            ls: function (scope, track) {
                return this.runDeferred(null, 'ls', [scope]).then(function (data) {
                    try {
                        this.rawData = data;
                        this.initStore(data);
                        this.publish(types.EVENTS.ON_STORE_CREATED, {
                            data: data,
                            owner: this,
                            store: this.store,
                            type: this.itemType
                        });
                    } catch (e) {
                        logError(e, 'error ls');
                    }
                }.bind(this));
            }
        });
    } else {
        return Base;
    }
});
