define("xcf/manager/Application_UI", [
    'dcl/dcl',
    'require',
    'xide/utils',
    'xcf/views/DriverTreeView',
    'xcf/views/DeviceTreeView',
    'xcf/views/ProtocolTreeView',
    'xide/types',
    'xide/factory',
    'xace/views/Editor',
    'xide/Wizards',
    'xide/views/CIView',
    'dojo/has',
    'xide/editor/Default',
    'xide/layout/_Accordion',
    'xide/widgets/WidgetBase',
    'xide/views/ACEEditor',
    "xcf/views/_MainView",
    'xaction/Action',
    "xide/mixins/PersistenceMixin",
    "xide/views/JsonEditor",
    'xide/widgets/JSONDualEditorWidget',
    'xide/widgets/TemplatedWidgetBase',
    'xide/mixins/Electron',
    'dojo/Deferred',
    'dojo/promise/all',
    'xide/$',
    'xide/lodash',
    'xide/console',
    'xcf/views/GlobalConsole',
    'xcf/widgets/CommandPicker',
    'xdojo/has!xcf-ui?xide/editor/Registry',
    'xdojo/has!x-markdown?x-markdown/views/MarkdownGrid',
    'wcDocker/iframe',
    'xide/views/WelcomeGrid',
    "xide/views/_Dialog",
    "xide/views/_Panel",
    "xide/wizard/WizardBase",
    'xide/views/_CIPanelDialog'
    // , 'xide-ts/app'
], function (dcl, require, utils, DriverTreeView, DeviceTreeView, ProtocolTreeView, types, factory, Editor, Wizards, CIView, has, Default, _Accordion, WidgetBase, ACEEditor, _MainView, Action, PersistenceMixin, JSONEditor, JSONDualEditorWidget, TemplatedWidgetBase, _Electron, Deferred, all, $, _, console, GlobalConsole, CommandPicker, Registry, MarkdownGrid, iframe, WelcomeGrid, _Dialog, _Panel, WizardBase, _CIPanelDialog, app) {
    // console.error('app', app);
    const start_scope = "user_drivers";
    const start_scopeDevices = "user_devices";
    const welcomeGrid = false;
    const ctx = window.sctx;
    if (ctx) {
        return;

        function test_() {
            const application = ctx.getApplication();
            application.exportUser = function () {
                const dfd = new Deferred();
                let cis = [];
                const ctx = this.getContext();
                const self = this;
                const rm = ctx.getResourceManager();
                //defaults
                const ROOT = ctx.getMount('root');
                const USER = rm.getVariable('USER_DIRECTORY');
                const SYSTEM = ROOT + '/data/';
                const DIST = ROOT + '/server/nodejs/dist/';
                const TARGET = ROOT + '/mc007/';
                const linux32 = false;
                const linux64 = true;
                const osx = false;
                const arm = false;
                const windows = false;


                const arch = rm.getVariable('ARCH');
                const platform = rm.getVariable('PLATFORM');

                console.log('arch : ', arch);
                console.log('platform : ', platform);
                let platforms = 0;

                switch (platform) {
                    case 'osx':
                        {
                            platforms += (1 << 16);
                            break;
                        }
                    case 'windows':
                        {
                            platforms += (1 << 2);
                            break;
                        }
                    case 'linux':
                        {
                            if (arch === '64') {
                                platforms += (1 << 4);
                            }
                            if (arch === '32') {
                                platforms += (1 << 8);
                            }
                            break;
                        }
                }

                const options = {
                    linux32: linux32,
                    linux64: linux64,
                    osx: osx,
                    arm: arm,
                    windows: windows,
                    root: ROOT,
                    system: SYSTEM,
                    user: USER + '/',
                    nodeServers: DIST,
                    target: TARGET,
                    debug:  false ,
                    layout:  false  ? 'debug' : 'release',
                    platform: rm.getVariable('PLATFORM')
                };
                console.log('open export wizard', options);

                const platformCI = utils.createCI('Platforms', types.ECIType.FLAGS, platforms, {
                    group: 'Common',
                    "data": [{
                            value: 2,
                            label: 'Windows'
                        },
                        {
                            value: 4,
                            label: 'Linux 64'
                        },
                        {
                            value: 8,
                            label: 'Linux 32'
                        },
                        {
                            value: 16,
                            label: 'OSX 64'
                        }
                    ]
                });
                const rootCI = utils.createCI('Root Folder', types.ECIType.FILE, ROOT, {
                    group: 'Source paths',
                    visible: true,
                    description: "Absolute path to root",
                    filePickerOptions: {
                        allowElectron: true
                    }
                });
                const userCI = utils.createCI('User Scope', types.ECIType.FILE, USER, {
                    group: 'Source paths',
                    visible: true,
                    description: "Absolute path to the user folder",
                    filePickerOptions: {
                        allowElectron: true
                    }
                });
                const systemCI = utils.createCI('System Scope', types.ECIType.FILE, SYSTEM, {
                    group: 'Source paths',
                    visible: true,
                    description: "Absolute path to the system folder",
                    filePickerOptions: {
                        allowElectron: true
                    }
                });




                const targetCI = utils.createCI('Target Folder', types.ECIType.FILE, TARGET, {
                    group: 'Common',
                    visible: true,
                    description: "Absolute path to the output folder",
                    filePickerOptions: {
                        allowElectron: true
                    }
                });
                cis = cis.concat([
                    rootCI,
                    userCI,
                    systemCI,
                    platformCI,
                    targetCI
                ]);



                this.isExporting = false;

                cis.reverse();

                function done(changedCIS, dialog) {

                    if (self.isExporting) {
                        return dialog.close();
                    }


                    //dialog.close();
                    options.windows = (platformCI.value & (1 << 2)) ? true : false;
                    options.linux32 = platformCI.value & (1 << 8) ? true : false;
                    options.linux64 = platformCI.value & (1 << 4) ? true : false;
                    options.osx = platformCI.value & (1 << 16) ? true : false;

                    options.system = systemCI.value;
                    options.target = targetCI.value;
                    options.user = userCI.value;

                    utils.destroy(dialog.cisView);
                    dialog.content.empty();
                    dialog.content.css('padding', '32px');
                    dialog.content.append('<span class="fa-spinner fa-spin"></span>');
                    dialog.content.append('<span class="text-warning">' + 'Exporting application, please wait' + '</span>');

                    const delegate = {
                        onError: function (e) {
                            console.error('have error : ', e);
                            dialog.content.append('<div class="text-fatal">' + e + '</div>');
                        },
                        onProgress: function (e) {
                            console.info('have progress: ', e);
                            dialog.content.append('<div class="text-info">' + e + '</div>');
                        },
                        onFinish: function (e) {
                            console.info('have finish: ', e);
                            dialog.content.append('<div class="text-success">' + e + '</div>');
                        }
                    };
                    self.isExporting = true;
                    ctx.getDeviceManager().runAppServerClass('components/xideve/export/Exporter', {
                        options: options
                    }, delegate);

                }

                const panel = new _CIPanelDialog({
                    title: 'Export',
                    onOk: function (changedCIS, dialog) {

                        changedCIS = _.map(changedCIS, function (obj) {
                            return obj.ci;
                        });
                        done(changedCIS, dialog);
                        this.headDfd.resolve(changedCIS);

                        return false;
                    },
                    onCancel: function (changedCIS) {
                        this.headDfd.resolve(changedCIS);
                    },
                    cis: cis,
                    CIViewOptions: {
                        delegate: this,
                        ciSort: false,
                        cis: cis
                    }
                });
                panel.show();

                return dfd;
            };
            application.exportUser();
        }

        function bytesToSize(bytes) {
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes === 0) {
                return 'n/a';
            }
            const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            if (i === 0) {
                return bytes + ' ' + sizes[i];
            }
            return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
        }



        function createProgressDialog(id) {
            const panelClass = dcl(_CIPanelDialog, {
                title: 'Copy',
                type: types.DIALOG_TYPE.INFO,
                onOk: function (changedCIS, dialog) {
                    this.headDfd.resolve(changedCIS);

                    return true;
                },
                onCancel: function (changedCIS) {
                    this.headDfd.resolve(changedCIS);
                    ctx.getDeviceManager().cancelAppServerComponentMethod('xfile', id);
                },
                cis: [],
                contentNode: null,
                onShow: function (panel, contentNode, instance) {
                    utils.destroy(this.cisView);
                    this.contentNode = contentNode;
                    copy(this);
                    $(contentNode).css('padding', '16px');
                },
                addProgress: function (where, file) {
                    const template = '<div class="progress progress-striped">' +
                        '<div class="progress-bar progress-bar-warning" style="width: 0%;"></div>' +
                        '</div><label class="labelItem" style="word-break:break-word">' + file + '</label><div class="detail"></div>';

                    const $template = $(template);
                    where.append($template);
                    const progressItem = where.find('.progress-bar');
                    const progressRoot = where.find('.progress-striped');
                    const labelItem = where.find('.labelItem');
                    const detail = where.find('.detail');
                    return {
                        progress: progressItem,
                        label: labelItem,
                        root: $(progressRoot),
                        detail: $(detail)
                    };
                },
                CIViewOptions: {
                    delegate: this,
                    ciSort: false,
                    cis: []
                },
                getContentSize: function () {
                    return {
                        width: '500px',
                        height: '200px'
                    };
                }
            });
            const panel = new panelClass();
            panel.show();
        }


        function wireButton($btn, resolve, scope, done, dlg) {
            $btn.on('click', function () {
                done(resolve, scope);
                dlg.destroy();
            });
        }

        function createInterruptDialog(id, interrupt, progressPanel, done) {
            const panelClass = dcl(_CIPanelDialog, {
                type: types.DIALOG_TYPE.DANGER,
                title: 'Error',
                onOk: function (changedCIS, dialog) {
                    this.headDfd.resolve(changedCIS);
                    return true;
                },
                onCancel: function (changedCIS) {
                    this.headDfd.resolve(changedCIS);
                    progressPanel.destroy();
                    ctx.getDeviceManager().cancelAppServerComponentMethod('xfile', id);
                },
                cis: [],
                contentNode: null,
                did: false,
                onShow: function (panel, contentNode, instance) {
                    if (this.did) {
                        return;
                    }

                    this.did = true;
                    utils.destroy(this.cisView);
                    this.contentNode = contentNode;
                    $(contentNode).css('padding', '16px');
                    switch (interrupt.error) {
                        case 'EEXIST':
                            {
                                const template = '<div class="">' +
                                    '<label class="labelItem text-error" style="word-break:break-word">' + interrupt.path + ' Already exists!</label><div class="detail"></div>';

                                const $template = $(template);
                                $(contentNode).append($template);

                                $template.append('<hr/>');
                                $template.append('<span> Overwrite this target ? </span>');

                                // this
                                const btnYes = $(factory.createSimpleButton("Yes", "", "btn-danger", {}, $template[0]));
                                wireButton(btnYes, types.EResolveMode.OVERWRITE, types.EResolve.THIS, done, this);
                                const btnNo = $(factory.createSimpleButton("No", "", "btn-info", {}, $template[0])).css('margin-left', '16px');

                                wireButton(btnNo, types.EResolveMode.SKIP, types.EResolve.THIS, done, this);

                                $template.append('<hr/>');

                                // all
                                $template.append('<span> Overwrite all targets ? </span>');

                                const btnAll = $(factory.createSimpleButton("All", "", "btn-danger", {}, $template[0])).css('margin-left', '16px');


                                wireButton(btnAll, types.EResolveMode.OVERWRITE, types.EResolve.ALWAYS, done, this);

                                const btnAllNewer = $(factory.createSimpleButton("If newer", "", "btn-danger", {}, $template[0])).css('margin-left', '16px');

                                wireButton(btnAllNewer, types.EResolveMode.IF_NEWER, types.EResolve.ALWAYS, done, this);


                                const btnAllSize = $(factory.createSimpleButton("If size siffers", "", "btn-danger", {}, $template[0])).css('margin-left', '16px');

                                wireButton(btnAllSize, types.EResolveMode.IF_SIZE_DIFFERS, types.EResolve.ALWAYS, done, this);


                                const btnAllNone = $(factory.createSimpleButton("None", "", "btn-info", {}, $template[0])).css('margin-left', '16px');

                                wireButton(btnAllNone, types.EResolveMode.SKIP, types.EResolve.ALWAYS, done, this);



                            }
                    }
                    return [];

                },
                CIViewOptions: {
                    delegate: this,
                    ciSort: false,
                    cis: []
                },
                getContentSize: function () {
                    return {
                        width: '500px',
                        height: '300px'
                    };
                }
            });
            const panel = new panelClass();
            panel.show();
        }
        const options = {
            id: utils.createUUID()
        };

        function testCP() {
            console.clear();
            const index = 0;
            const cis = [
                utils.createCI('Name', types.ECIType.FILE, 'fileWidgetValue', {
                    widget: {
                        instant: true,
                        title: 'Target',
                        filePickerOptions: {
                            allowElectron: true
                        }
                    }
                })

            ];


            const panel = new _CIPanelDialog({
                title: 'Copy',
                getContentSize: function () {
                    return {
                        width: '500px',
                        height: '300px'
                    };
                },
                onOk: function (changedCIS, dialog) {
                    this.headDfd.resolve(changedCIS);
                    createProgressDialog(options.id);
                    return true;
                },
                onCancel: function (changedCIS) {
                    this.headDfd.resolve(changedCIS);
                },
                cis: cis,
                CIViewOptions: {
                    delegate: this,
                    ciSort: false,
                    cis: cis
                }
            });
            panel.show();
        }


        function copy(panel) {

            const progress = panel.addProgress($(panel.contentNode), "");
            let counter = 0;
            const size = 0;
            const delegate = {
                onError: function (e) {
                    console.error('have error : ', e);
                },
                onProgress: function (e) {
                    counter++;
                    const last = e[e.length - 1];
                    progress.label.html(last.path);
                    let timer = null;
                    clearTimeout(timer);
                    if (last.total && last.current) {
                        const p = (100.0 * last.current / last.total).toFixed(2) + "%";
                        progress.progress.css('width', p);
                        progress.detail.html(bytesToSize(last.current) + ' of ' + bytesToSize(last.total));
                    } else {
                        progress.detail.html('');
                        progress.progress.css('width', '0%');
                        timer = setTimeout(function () {
                            progress.progress.css('width', '100%');
                        }, 10);
                    }

                    ctx.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                        text: ' Current ' + last.current + ' of ' + last.total + ' :  ' + last.path,
                        type: 'warning'
                    });
                },
                onFinish: function (e) {
                    console.info('have finish: ', e);
                    progress.root.remove();
                    progress.label.html('Done, copied ' + counter + ' items.');
                    progress.label.addClass('text-info');
                    progress.detail.remove();
                    progress.progress.remove();
                },
                onInterrupt: function (interrupt) {
                    const answer = {
                        overwrite: types.EResolveMode.OVERWRITE,
                        mode: types.EResolve.THIS
                    };
                    if (interrupt.error === types.EError.PERMISSION) {
                        answer.overwrite = types.EResolveMode.SKIP;
                    }
                    //console.log('on in iterrupt', interrupt);
                    const send = function (overwrite, mode) {
                        console.log('resolved: ' + mode + ' with ' + overwrite);
                        ctx.getDeviceManager().answerAppServerComponentMethodInterrupt('xfile',
                            options.id, {
                                answer: {
                                    overwrite: overwrite,
                                    mode: mode
                                },
                                iid: interrupt.iid,
                                error: interrupt.error,
                                path: interrupt.path
                            });
                    };
                    createInterruptDialog(options.id, interrupt, panel, send);
                    return;

                    ctx.getDeviceManager().answerAppServerComponentMethodInterrupt('xfile',
                        options.id, {
                            answer: answer,
                            iid: interrupt.iid,
                            error: interrupt.error,
                            path: interrupt.path
                        });
                }
            };
            ctx.getDeviceManager().runAppServerComponentMethod('xfile', 'cp', ['src', 'dst'], options, delegate);
        }

        testCP();

    }



    if (has('electronx')) {
        /**
         var jet = Electron.srequire('fs-jetpack');
         var electron = Electron.getElectron();
         var remote = Electron.remote();
         var dialog = remote.dialog;*/
        //console.log(dialog.showOpenDialog({properties: ['openDirectory']}));
    }
    const Persistence = dcl([PersistenceMixin.dcl], {
        declaredClass: 'xcf.manager.ApplicationPersistence',
        defaultPrefenceTheme: 'idle_fingers',
        defaultPrefenceFontSize: 14,
        saveValueInPreferences: true,
        cookiePrefix: '_xcf_application',
        getDefaultPreferences: function () {
            return utils.mixin({
                    theme: this.defaultPrefenceTheme,
                    fontSize: this.defaultPrefenceFontSize
                },
                this.saveValueInPreferences ? {} : null);
        },
        onAfterAction: function (action) {
            this.savePreferences({});
            return this.inherited(arguments);
        },
        /**
         * Override id for pref store:
         * know factors:
         *
         * - IDE theme
         * - per bean description and context
         * - by container class string
         * - app / plugins | product / package or whatever this got into
         * -
         **/
        toPreferenceId: function (prefix) {
            prefix = '';
            return (prefix || this.cookiePrefix || '') + '_xcf_application';
        },
        getDefaultOptions: function () {
            //take our defaults, then mix with prefs from store,
            const _super = this.inherited(arguments);

            const _prefs = this.loadPreferences(null);

            (_prefs && utils.mixin(_super, _prefs) ||
                //else store defaults
                this.savePreferences(this.getDefaultPreferences()));
            return _super;
        }
    });

    return dcl(Persistence.dcl, {
        declaredClass: "xcf.manager.Application_UI",
        mainView: null,
        leftLayoutContainer: null,
        rightLayoutContainer: null,
        showGUI: true,
        showFiles: true,
        lastPane: null,
        _lastWizard: null,
        _maximized: false,
        /**
         * collected navigation grids
         * @type {module:xgrid/Base[]|null}
         */
        grids: null,
        /**
         * @type {module:xcf/views/DriverTreeView}
         */
        driverTreeView: null,
        /**
         * @type {module:xcf/views/DeviceTreeView}
         */
        deviceTreeView: null,
        openInOS: function (realPath) {
            const _require = window['eRequire'];
            if (!_require) {
                console.error('have no electron');
            }
            const os = _require('os');
            const shell = _require('electron').shell;
            const path = _require("path");
            if (os.platform() !== 'win32') {
                shell.openItem(realPath);
            } else {
                shell.openItem('file:///' + realPath);
            }

        },
        exportUser: function () {
            const dfd = new Deferred();
            let cis = [];
            const ctx = this.getContext();
            const self = this;
            const rm = ctx.getResourceManager();
            //defaults
            const ROOT = ctx.getMount('root');
            const USER = rm.getVariable('USER_DIRECTORY');
            const SYSTEM = ROOT + '/data/';

            let DIST = ROOT + '/server/nodejs/dist/';
            if (! false ) {
                DIST = ROOT + '/server/'
            }
            const TARGET = ROOT + '/mc007/';
            const linux32 = false;
            const linux64 = true;
            const osx = false;
            const arm = false;
            const windows = false;


            const arch = rm.getVariable('ARCH');
            const platform = rm.getVariable('PLATFORM');

            console.log('arch : ', arch);
            console.log('platform : ', platform);
            let platforms = 0;

            switch (platform) {
                case 'osx':
                    {
                        platforms += (1 << 16);
                        break;
                    }
                case 'win32':
                    {
                        platforms += (1 << 2);
                        break;
                    }
                case 'linux':
                    {
                        if (arch === '64') {
                            platforms += (1 << 4);
                        }
                        if (arch === '32') {
                            platforms += (1 << 8);
                        }
                        break;
                    }
            }

            const options = {
                linux32: linux32,
                linux64: linux64,
                osx: osx,
                arm: arm,
                windows: windows,
                root: ROOT,
                system: SYSTEM,
                user: USER + '/',
                nodeServers: DIST,
                target: TARGET,
                debug:  false ,
                layout:  false  ? 'debug' : 'release',
                platform: rm.getVariable('PLATFORM')
            };
            console.log('open export wizard', options);

            const platformCI = utils.createCI('Platforms', types.ECIType.FLAGS, platforms, {
                group: 'Common',
                "data": [{
                        value: 2,
                        label: 'Windows'
                    },
                    {
                        value: 4,
                        label: 'Linux 64'
                    },
                    {
                        value: 8,
                        label: 'Linux 32'
                    },
                    {
                        value: 16,
                        label: 'OSX 64'
                    }
                ]
            });
            const rootCI = utils.createCI('Root Folder', types.ECIType.FILE, ROOT, {
                group: 'Source paths',
                visible: true,
                description: "Absolute path to root",
                filePickerOptions: {
                    allowElectron: true
                }
            });
            const userCI = utils.createCI('User Scope', types.ECIType.FILE, USER, {
                group: 'Source paths',
                visible: true,
                description: "Absolute path to the user folder",
                filePickerOptions: {
                    allowElectron: true
                }
            });
            const systemCI = utils.createCI('System Scope', types.ECIType.FILE, SYSTEM, {
                group: 'Source paths',
                visible: true,
                description: "Absolute path to the system folder",
                filePickerOptions: {
                    allowElectron: true
                }
            });
            const targetCI = utils.createCI('Target Folder', types.ECIType.FILE, TARGET, {
                group: 'Common',
                visible: true,
                description: "Absolute path to the output folder",
                filePickerOptions: {
                    allowElectron: true
                }
            });
            cis = cis.concat([
                rootCI,
                userCI,
                systemCI,
                platformCI,
                targetCI
            ]);


            this.isExporting = false;

            cis.reverse();

            function done(changedCIS, dialog) {
                if (self.isExporting) {
                    return dialog.close();
                }
                options.windows = (platformCI.value & (1 << 2)) ? true : false;
                options.linux32 = platformCI.value & (1 << 8) ? true : false;
                options.linux64 = platformCI.value & (1 << 4) ? true : false;
                options.osx = platformCI.value & (1 << 16) ? true : false;

                options.system = systemCI.value;
                options.target = targetCI.value;
                options.user = userCI.value;

                utils.destroy(dialog.cisView);
                dialog.content.empty();
                dialog.content.css('padding', '32px');
                dialog.content.append('<span class="fa-spinner fa-spin"></span>');
                dialog.content.append('<span class="text-warning">' + 'Exporting application, please wait' + '</span>');

                const delegate = {
                    onError: function (e) {
                        console.error('have error : ', e);
                        dialog.content.append('<div class="text-fatal">' + e + '</div>');
                    },
                    onProgress: function (e) {
                        console.info('have progress: ', e);
                        dialog.content.append('<div class="text-info">' + e + '</div>');
                    },
                    onFinish: function (e) {
                        console.info('have finish: ', e);
                        dialog.content.append('<div class="text-success">' + e + '</div>');
                        if (has('electronx')) {
                            self.openInOS(targetCI.value);
                        }
                    }
                };
                self.isExporting = true;
                ctx.getDeviceManager().runAppServerClass('components/xideve/Exporter', {
                    options: options
                }, delegate);
            }


            const panel = new _CIPanelDialog({
                title: 'Export',
                onOk: function (changedCIS, dialog) {
                    done(changedCIS, dialog);
                    this.headDfd.resolve(changedCIS);
                    return false;
                },
                onCancel: function (changedCIS) {
                    this.headDfd.resolve(changedCIS);
                },
                cis: cis,
                CIViewOptions: {
                    delegate: this,
                    ciSort: false,
                    cis: cis
                }
            });
            panel.show();

            return dfd;
        },
        openGlobalConsole: function () {
            const parent = this.ctx.getWindowManager().createTab("Global Console");
            utils.addWidget(GlobalConsole, {
                ctx: this.ctx
            }, null, parent, true);
        },
        createAppActions: function () {

            const result = [];


            function openFolder() {
                const Electron = new _Electron();
                const app = Electron.getApp();
                const exec = Electron.srequire('child_process').spawn;
                const fs = Electron.srequire('fs');
                const remote = Electron.remote();
                const dialog = remote.dialog;
                const directory = dialog.showOpenDialog({
                    properties: ['openDirectory']
                });
                if (!directory) {
                    console.log('Open Folder: have no directory, abort');
                    return;
                }
                const args = remote.BrowserWindow.args;
                Electron.srequire('yargs-parser')(args);
                const _exce = process.execPath;
                const _cwd = Electron.cwd();
                console.log('reopen with folder ' + _exce + ' in ' + _cwd + ' @ ' + directory);
                exec(_exce, [_cwd, '--userDirectory=' + directory], {
                    cwd: Electron.cwd(),
                    detached: true
                });
                app.quit();
                //app.relaunch({args:['noob','--userDirectory='+directory]});
                /*app.exit(0);*/
            }

            result.push(this.ctx.createAction({
                label: 'Open Folder',
                command: 'Window/Open Folder',
                icon: 'fa-folder',
                tab: 'Home',
                group: 'Window',
                owner: this,
                handler: openFolder,
                mixin: {
                    addPermission: true
                }
            }));

            const thiz = this;
            result.push(this.ctx.createAction({
                label: 'Global Console',
                command: 'Window/Global Console',
                icon: 'fa-calendar',
                tab: 'Home',
                group: 'Window',
                owner: this,
                handler: function () {
                    return thiz.openGlobalConsole();
                },
                mixin: {
                    addPermission: true
                }
            }));

            result.push(this.ctx.createAction({
                label: 'Export',
                command: 'Window/Export',
                icon: 'fa-save',
                tab: 'Home',
                group: 'Window',
                owner: this,
                handler: function () {
                    return thiz.exportUser();
                },
                mixin: {
                    addPermission: true
                }
            }));

            return result;
        },
        openFile: function (args) {
            const self = this;
            const _editor = args.editor;
            const editor = Registry.getEditor(_editor) || Registry.getDefaultEditor({
                    path: args.file
                }) ||
                Registry.getEditor('Default Editor');
            const _require = window['eRequire'];
            const app = _require('electron').remote.app;
            const BrowserWindow = _require('electron').remote.BrowserWindow;
            const wind = BrowserWindow.getFocusedWindow() || BrowserWindow.mainWindow;
            wind && wind.setTitle(args.file);
            if (editor) {
                const editorInstance = editor.onEdit({
                    path: args.file,
                    mount: '__direct__',
                    getPath: function () {
                        return this.path;
                    }
                });
                if (editorInstance._on) {
                    this.ctx.getWindowManager().registerView(editorInstance, true);
                } else if (editorInstance.then) {
                    editorInstance.then(function (instance) {
                        self.ctx.getWindowManager().registerView(instance, true);
                    });
                }
            } else {
                console.error('--have no editor ', args.editor);
            }
        },
        /***
         * Register custom types
         */
        registerCustomTypes: function () {

            const ctx = this.ctx;

            const resourceManager = ctx.getResourceManager();

            const DEVICE_PROPERTY = types.DEVICE_PROPERTY;
            /***
             * Network settings
             */
            types.registerType(types.ECIType.DEVICE_NETWORK_SETTINGS, [
                utils.createCI(DEVICE_PROPERTY.CF_DEVICE_HOST, 13, '192.168.1.20', {
                    group: 'Network'
                }),

                utils.createCI('Protocol', 3, 'tcp', {
                    group: 'Network',
                    options: [{
                            label: "TCP",
                            value: "tcp"
                        },
                        {
                            label: "UDP",
                            value: "udp"
                        },
                        {
                            label: "Driver",
                            value: "driver"
                        },
                        {
                            label: "SSH",
                            value: "ssh"
                        },
                        {
                            label: "Serial",
                            value: "serial"
                        },
                        {
                            label: "MQTT",
                            value: "mqtt"
                        }
                    ]
                }),
                utils.createCI('Port', 13, '23', {
                    group: 'Network'
                }),
                utils.createCI('Options', 28, '{}', {
                    group: 'Network'
                })
            ]);
            types.registerWidgetMapping('CommandSettings', 'xcf.widgets.CommandSettings');
            const deviceManager = this.getContext().getDeviceManager();

            /**
             *
             * @param view {module:xide/views/CIViewMixin}
             * @param widget {module:xide/widgets/WidgetBase}
             * @param ci {module:xide/types/ConfigurableInformation}
             */
            function renderHostCI(view, widget, ci) {
                const cis = view.cis;
                const protocolCI = utils.getCIByChainAndName(cis, 0, types.DEVICE_PROPERTY.CF_DEVICE_PROTOCOL);
                if (!protocolCI) {
                    return;
                }
                const protocol = protocolCI.value;
                const item = protocolCI.device;

                if (widget._hostTypeHead) {
                    widget._hostTypeHead.destroy();
                    widget._hostTypeHead = null;
                }

                let query = {
                    ports: '22'
                };
                if (protocol === types.PROTOCOL.TCP) {
                    query = {
                        ports: '80'
                    };
                }
                if (protocol === types.PROTOCOL.MQTT) {
                    query = {
                        ports: '1883'
                    };
                }

                const input = widget.nativeWidget;

                const spinner = $('<span class="input-group-addon" style="text-align: left"><i class="fa fa-spinner fa-spin"></i></span>');
                $(widget.previewNode).append(spinner);

                deviceManager.protocolMethod(protocol, 'ls', [query], deviceManager.toDeviceControlInfo(item)).then(function (data) {
                    let items = data.result;
                    items = _.map(items, function (item) {
                        return {
                            label: item.host,
                            id: item.host || utils.createUUID(),
                            description: item.description || "No Description"
                        }
                    });
                    ci.value && items.push({
                        label: ci.value,
                        id: ci.value,
                        description: "Last Selected"
                    });
                    if (protocol === types.PROTOCOL.TCP || protocol === types.PROTOCOL.SSH || protocol === types.PROTOCOL.MQTT) {
                        items.push({
                            label: "localhost",
                            id: 'localhost',
                            description: "Default"
                        });
                        items.push({
                            label: "0.0.0.0",
                            id: '0.0.0.0',
                            description: "Default"
                        });
                    }

                    const substringMatcher = function (strs) {
                        return function findMatches(q, cb) {
                            let matches;
                            // an array that will be populated with substring matches
                            matches = [];
                            // regex used to determine if a string contains the substring `q`
                            //var substrRegex = new RegExp(q, 'i');
                            // iterate through the pool of strings and for any string that
                            // contains the substring `q`, add it to the `matches` array
                            $.each(strs, function (i, str) {
                                matches.push(str);
                            });
                            cb(matches);
                        };
                    };
                    const options = {};
                    let typeahead = widget.nativeWidget.typeahead(utils.mixin({
                        hint: true,
                        highlight: true,
                        minLength: 0,
                        name: 'states',
                        source: substringMatcher(items),
                        autoSelect: false,
                        showHintOnFocus: 'all',
                        appendTo: $("body"),
                        select: function () {
                            const val = this.$menu.find('.active').data('value');
                            this.$element.data('active', val);
                            if (this.autoSelect || val) {
                                let newVal = this.updater(val);
                                // Updater can be set to any random functions via "options" parameter in constructor above.
                                // Add null check for cases when updater returns void or undefined.
                                if (!newVal) {
                                    newVal = "";
                                }
                                this.$element
                                    .val(this.displayText(newVal, true) || newVal)
                                    .change();

                                this.afterSelect(newVal);
                            }
                            return this.hide();
                        },
                        highlighter: function (item) {
                            return item;
                        },
                        displayText: function (item, isValue) {
                            if (isValue) {
                                return item.label;
                            }
                            return item.id + ' - ' + '<span class="text-info">' + item.description + '</span>';
                        }
                    }, options));

                    typeahead = $(typeahead).data('typeahead');
                    widget._hostTypeHead = typeahead;
                    widget.add(typeahead);
                    spinner.remove();
                    view._hostWidget = widget;

                    if (!view.__setupHostCI) {
                        //noinspection JSUndefinedPropertyAssignment
                        view.__setupHostCI = true;
                        view._on('destroy', function () {
                            delete view._hostWidget;
                        });

                        view._on('valueChanged', function (e) {
                            if (e.oldValue === e.newValue) {
                                return;
                            }
                            if (e.ci == protocolCI) {
                                renderHostCI(view, view._hostWidget, ci);
                            }
                        });
                    }
                }, function (e) {
                    //server not there
                    spinner.remove();
                })
            }

            types.registerCICallbacks(DEVICE_PROPERTY.CF_DEVICE_HOST, {
                on: {
                    'render': function (view, widget, ci) {
                        renderHostCI(view, widget, ci);
                        $(factory.createSimpleButton('', 'fa-search', 'btn-default', null, widget.button0)).on('click', function () {
                            renderHostCI(view, widget, ci);
                        });


                        /*
                         $(utils.getNode(widget)).on('click',function(){
                         renderHostCI(view, widget, ci);
                         });*/
                    }
                }
            });

            /**
             *
             * @param view {module:xide/views/CIViewMixin}
             * @param widget {module:xide/widgets/WidgetBase}
             * @param ci {module:xide/types/ConfigurableInformation}
             */
            function renderOptionsCI(view, widget, ci) {
                const cis = view.cis;
                const protocolCI = utils.getCIByChainAndName(cis, 0, types.DEVICE_PROPERTY.CF_DEVICE_PROTOCOL);
                if (!protocolCI) {
                    console.error('no protocol ci');
                    return;
                }
                const protocol = protocolCI.value;
                const item = protocolCI.device;
                const spinner = $('<span class="input-group-addon" style="text-align: left"><i class="fa fa-spinner fa-spin"></i></span>');
                $(widget.valueNode).append(spinner);
                deviceManager.protocolMethod(protocol, 'options', [], deviceManager.toDeviceControlInfo(item)).then(function (data) {
                    spinner.remove();
                    const portCI = utils.getInputCIByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_PORT);
                    if (portCI) {
                        view.updateWidget(portCI.id, 'disabled', false);
                    }

                    _.each(cis, function (ci) {
                        try {
                            const optionsCI = utils.getInputCIByName(data.result, ci.name);
                            if (optionsCI) {
                                if ("disabled" in optionsCI) {
                                    view.updateWidget(optionsCI.id, 'disabled', optionsCI.disabled);
                                }
                                data.result.remove(optionsCI);
                            }
                        } catch (e) {
                            logError(e, 'error filtering options');
                        }

                    });
                    widget.render(data.result);

                }, function (e) {
                    //server not there, fallback to JSON widget
                    spinner.remove();
                    widget.$titleColumn.remove(); //remove the structural widget's title node, otherwise it renders "Options" twice
                    const optionsCI = utils.getInputCIByName(item.user, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
                    optionsCI.type = types.ECIType.JSON_DATA;
                    widget.render([optionsCI]);
                });

                view._optionsWidget = widget;
                if (!view.__setupOptionsCI) {
                    view.__setupOptionsCI = true;
                    view._on('valueChanged', function (e) {
                        if (e.oldValue === e.newValue) {
                            return;
                        }
                        if (e.ci == protocolCI) {
                            renderOptionsCI(view, view._optionsWidget, ci);
                        }
                    });
                }

            }

            types.registerCICallbacks(DEVICE_PROPERTY.CF_DEVICE_OPTIONS, {
                on: {
                    'render': function (view, widget, ci) {
                        renderOptionsCI(view, widget, ci);
                    }
                }
            })
        },
        onMainViewReady: function () {
            const thiz = this;
            if (this.logPanel || has('log') == false) {
                return;
            }
            thiz.registerEditorExtensions();
        },
        /**
         * Main entry point, does:
         *  - create the main view
         *  - registers custom types and enumerations
         *  - loads data and adds data views
         *  - initializes XFile
         */
        start: function (showGUI, rootSelector, args) {
            this.args = args;
            this.showGUI = showGUI;
            const ACTION = types.ACTION;
            const container = $(rootSelector || '#root')[0];
            let permissons = [
                //ACTION.BREADCRUMB,
                //ACTION.RIBBON,
                ACTION.MAIN_MENU,
                ACTION.NAVIGATION,
                ACTION.STATUSBAR,
                ACTION.TOOLBAR,
                ACTION.WELCOME
            ];

            let isEditor = false;
            if (has('electronx') && args) {
                if (args.file) {
                    permissons = [
                        ACTION.MAIN_MENU
                    ];
                    isEditor = true;
                }
            }
            const view = utils.addWidget(_MainView, {
                ctx: this.ctx,
                permissions: permissons,
                config: this.config,
                windowManager: this.ctx.getWindowManager(),
                container: container
            }, null, container, true);

            this.mainView = view;
            this.ctx.mainView = this.mainView;
            const leftContainer = view.layoutLeft;
            if (!isEditor) {
                const accContainer = utils.addWidget(_Accordion, {
                    tabHeight: '500px'
                }, null, leftContainer, true);
                this.mainView.leftLayoutContainer = accContainer;
                this.mainView.leftLayoutContainer.$containerNode.addClass('navigationPanel');
                this.leftLayoutContainer = accContainer;
            } else {
                this.mainView.layoutCenter = this.mainView.layoutLeft;
            }
            const self = this;
            this.doComponents().then(() => {
                self.registerCustomTypes();
                self.onComponentsReady();
                if (isEditor) {
                    self.openFile(args);
                } else {
                    self.initData().then(() =>{
                         true  && welcomeGrid && self.addWelcomeGrid();
                    });
                    self.onAppCreated();
                    self.subscribe(types.EVENTS.ON_DEVICE_SERVER_CONNECTED, function () {
                        if (self.args.userDirectory) {
                            self.ctx.getDeviceManager().watchDirectory(self.args.userDirectory, true);
                        }
                    });
                }
                view._resize();
            });
        },
        onAppCreated: function () {
            this.getContext().ready();
        },
        /**
         * Register new editors for xfile
         */
        registerEditorExtensions: function () {

            const ctx = this.ctx;

            Default.Implementation.ctx = ctx;
            Default.ctx = ctx;

            //sample default text editor open function, not needed
            const editInACE = function (item, owner) {
                let where = null;
                if (owner) {
                    //where = owner ? owner.newTarget : null;
                    if (_.isFunction(owner.newTarget)) {
                        where = owner.newTarget({
                            title: item.name,
                            icon: 'fa-code'
                        });
                    }
                }
                return Default.Implementation.open(item, where, null, null, owner);
            };
            ctx.registerEditorExtension('Default Editor', '*', 'fa-code', this, false, editInACE, ACEEditor, {
                updateOnSelection: false,
                leftLayoutContainer: this.leftLayoutContainer,
                ctx: ctx,
                defaultEditor: true
            });
            ctx.registerEditorExtension('JSON Editor', 'json', 'fa-code', this, true, null, JSONEditor, {
                updateOnSelection: false,
                leftLayoutContainer: this.leftLayoutContainer,
                ctx: ctx,
                registerView: true
            });
            types.registerCustomMimeIconExtension('cfhtml', 'fa-laptop');
            this.registerSplitEditors();
        },
        init: function () {
            this.ctx.addActions(this.getActions());
            this.ctx.addActions(this.getThemeActions());
            WidgetBase.prototype.ctx = this.ctx;
            const thiz = this;
            this.subscribe(types.EVENTS.ON_EXPRESSION_EDITOR_ADD_FUNCTIONS, this.onExpressionEditorAddFunctions);
            if (has('consoleError') && location.href.indexOf('consoleError') !== -1) {
                const console = (window.console = window.console || {});

                function consoleError(string) {
                    thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                        text: string,
                        type: 'error'
                    });
                }

                console.error = consoleError;
            }
            this.subscribe(types.EVENTS.ON_ALL_COMPONENTS_LOADED, (e) => {
                this.initXFile().then(() => {
                    this.onXBlocksReady().then(() => {
                        this.addDocumenation();
                        this.ctx.onComponentsReady();
                    });
                })


            });
        },
        getLayoutRightMain: function (clear, open) {
            const mainView = this.mainView;
            return mainView.getLayoutRightMain(clear, open);
        },
        getRightTopTarget: function (clear, open) {
            const mainView = this.mainView;
            return mainView.getRightTopTarget(clear, open);
        },
        onExpressionEditorAddFunctions: function (evt) {
            const widget = evt.widget;
            const root = evt.root;
            if (!widget || !root) {
                return;
            }
            const device = widget.device;
            if (!device) {
                return;
            }
            const driverInstance = device.driverInstance;
            const _Variables = widget.createBranch('Variables', root.items);

            root.items.push(widget.createLeaf('getVariable', 'this.getVariable(\'variableName\')', 'Variables', 'gets a variable', true, _Variables));
            root.items.push(widget.createLeaf('setVariable', 'this.setVariable(\'variableName\')', 'Variables', 'sets a variable by name', true, _Variables));
            if (driverInstance) {
                widget.createBranch('Driver', root.items);
            }


        },
        initWidgets: function () {},
        initElectron: function () {
            document.addEventListener('dragover', function (e) {
                e.preventDefault();
                e.stopPropagation();
            });
            const self = this;
            document.addEventListener('drop', function (event) {
                event.preventDefault();
                const pathsToOpen = Array.prototype.map.call(event.dataTransfer.files, function (file) {
                    return file.path;
                });
                if (pathsToOpen.length > 0) {

                    self.openFile({
                        file: pathsToOpen[0]
                    });
                }
                return false;
            }, false);

        },
        onComponentsReady: function () {
            const thiz = this;
            Editor.ctx = thiz.ctx;
            Editor.prototype.ctx = thiz.ctx;
            $('#loadingWrapper').remove();
            if (has('electronx')) {
                this.initElectron();
            }

        },
        addWelcomeGrid: function () {
            const ctx = this.getContext();
            const mainView = ctx.mainView;
            const target = mainView.welcomePage.containerNode;
            $(target).empty();
            const trackingMgr = ctx.getTrackingManager();
            const router = ctx.getRouter();
            trackingMgr._init().then(function () {
                const grid = utils.addWidget(WelcomeGrid, {
                    collection: new WelcomeGrid.STORE_CLASS({}),
                    _columns: {},
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS),
                    router: router,
                    ctx: ctx,
                    trackingStore: trackingMgr.getStore()
                }, null, target, true);
                mainView.getDocker().resize();
                mainView.layoutCenter.resize();
            });
        },
        addDocumenation: function () {
            if (!MarkdownGrid) {
                return;
            }
            const container = this.leftLayoutContainer;
            const _require = require;
            const thiz = this;
            const config = this.config;
            const mainView = thiz.mainView;
            const excludes = ["internal", "assets", "modules", "index",  false  ? "_index.md" : "__"];
            const FileStoreArgs = {
                micromatch: "(*.md)|(*.MD)|!(*.*)", // Only folders and markdown files
                _onAfterSort: function (data) {
                    data = data.filter(function (item) {
                        return excludes.indexOf(item.name) == -1;
                    });
                    return data;
                }
            };
            _require([
                'xfile/factory/Store',
                'xfile/types',
                'xfile/views/Grid',
                'xfile/views/FileGrid'
            ], function (factory) {
                const store = factory.createFileStore('docs', null, config, null, thiz.ctx, FileStoreArgs);
                const state = thiz.getNavigationState && thiz.getNavigationState() || {
                    documentation: false
                };

                let newTarget = mainView.layoutCenter;
                if (mainView.getNewDefaultTab) {
                    newTarget = function (args) {
                        return mainView.getNewDefaultTab(args);
                    }
                }
                const open = state.documentation;
                const tab = container.createTab('Documentation', 'fa-info', open);
                const grid = utils.addWidget(MarkdownGrid, {
                    ctx: thiz.ctx,
                    collection: store,
                    startFile: './01_Getting_Started.md',
                    editor: false,
                    resizeToParent: true,
                    open: open,
                    showHeader: false,
                    title: "Documentation",
                    style: 'height:100%;min-height:200px',
                    getRightPanel: function () {
                        return mainView.getNewDefaultTab({
                            title: "Documentation"
                        });
                    },
                    newTabArgs: {
                        showHeader: true
                    },
                    getEditorTarget: function () {
                        return mainView.getBottomPanel(false, 0.5, 'DefaultTab', null, mainView.layoutCenter);
                    },
                    newTarget: newTarget
                }, null, tab, open);
                tab.add(grid);
                thiz.addNavigationGrid && thiz.addNavigationGrid(grid);
                if (open) {
                    grid.set('collection', store.getDefaultCollection());
                }
                thiz.documentationGrid = grid;
            });

        },
        addDeviceView: function (open) {
            let view;
            const ctx = this.ctx;
            const deviceMgr = ctx.getDeviceManager();
            const windowManager = ctx.getWindowManager();
            const thiz = this;
            const container = thiz.leftLayoutContainer;

            view = deviceMgr.treeView = utils.addWidget(DeviceTreeView, {
                title: 'Devices',
                collection: deviceMgr.getStore(start_scopeDevices),
                delegate: deviceMgr,
                blockManager: thiz.ctx.getBlockManager(),
                ctx: thiz.ctx,
                icon: 'fa-sliders',
                showHeader: false,
                scope: start_scopeDevices,
                open: open
            }, null, container, open);

            windowManager.registerView(view, false);
            setTimeout(function () {
                view.startup();
                view.select([0], null, true, {
                    focus: true,
                    append: false,
                    delay: 1000
                }).then(function () {

                });
            }, 500);

            this.addNavigationGrid(view);
            this.deviceTreeView = view;
        },
        _saveNavigationState: function () {
            const settingsStore = this.ctx.getSettingsManager().getStore() || {
                getSync: function () {}
            };
            let state = {};
            if (settingsStore) {
                const props = settingsStore.getSync('navigationView');
                if (props && props.value) {
                    state = props.value;
                }
            }
            if (this.deviceTreeView) {
                state['devices'] = this.deviceTreeView.open;
            }

            if (this.driverTreeView) {
                state['drivers'] = !!this.driverTreeView.open;
            }

            if (this.fileGrid) {
                state['files'] = !!this.fileGrid.open;
            }
            if (this.documentationGrid) {
                state['documentation'] = !!this.documentationGrid.open;
            }
            this.ctx.getSettingsManager().write2(null, '.', {
                id: 'navigationView'
            }, {
                value: state
            }, true, null);
        },
        getNavigationState: function () {
            const settingsStore = this.ctx.getSettingsManager().getStore() || {
                getSync: function () {}
            };
            let state = {
                devices: true,
                drivers: false,
                files: false,
                documentation: false
            };
            if (settingsStore) {
                const props = settingsStore.getSync('navigationView');
                if (props && props.value) {
                    state = props.value;
                }
            }
            return state;
        },
        /**
         * Function to register a grid in the navigation accordiona
         * @param grid
         */
        addNavigationGrid: function (grid) {

            !this.grids && (this.grids = []);
            this.grids.push(grid);
            const thiz = this;

            grid._on('show', () => {
                thiz._saveNavigationState();
            });
            grid._on('hide', () => {
                thiz._saveNavigationState();
            });
        },
        initData: function () {
            const thiz = this;
            const ctx = thiz.ctx;
            const driverMgr = ctx.getDriverManager();
            const deviceMgr = ctx.getDeviceManager();
            const logMgr = ctx.getLogManager();
            const windowManager = ctx.getWindowManager();

            this.grids = [];
            const dfd = new Deferred();
            Editor.ctx = thiz.ctx;
            Editor.prototype.ctx = thiz.ctx;
            CommandPicker.prototype.ctx = thiz.ctx;


            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                text: "Load data, please wait",
                timeout: 8000
            });
            const state = this.getNavigationState();
            $('#loadingWrapper').remove();
            /**
             * Driver tree view, added as pane into the left accordion
             */
            if ( true ) {
                driverMgr.ls('system_drivers').then(function () {
                    driverMgr.ls('user_drivers').then(function () {
                        thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                            text: "Load Drivers, please wait..."
                        });
                        if ( true ) {
                            deviceMgr.ls("user_devices").then(function () {
                                deviceMgr.ls("system_devices").then(function () {
                                    thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                                        text: "Start Device View, please wait...",
                                        type: 'success'
                                    });
                                    if (thiz.showGUI) {
                                        thiz.addDeviceView(state.devices);
                                    }
                                });
                            });
                        }
                        if (thiz.showGUI) {
                            try {
                                const view = driverMgr.treeView = utils.addWidget(DriverTreeView, {
                                    title: 'Drivers',
                                    store: driverMgr.getStore(start_scope),
                                    delegate: driverMgr,
                                    blockManager: thiz.ctx.getBlockManager(),
                                    scope: start_scope,
                                    ctx: thiz.ctx,
                                    icon: 'fa-exchange',
                                    open: state.drivers

                                }, driverMgr, thiz.leftLayoutContainer, state.drivers);
                                windowManager.registerView(view, false);
                                thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                                    text: "Driver View Ready"
                                });
                                state.drivers && setTimeout(function () {
                                    view.startup();
                                }, 500);
                                dfd.resolve();
                                thiz.addNavigationGrid(view);
                                thiz.driverTreeView = view;
                            } catch (e) {
                                logError(e, 'Driver Tree-View creation failed');
                            }
                        }
                        if (has('log')) {
                            //logMgr.ls();
                        }
                    });
                });
            } else if ( true ) {
                deviceMgr.ls('system_devices');
            }
            if ( false ) {
                this.ctx.getProtocolManager().ls('system_protocols');
            }
            this.ctx.mainView.resize();
            this.ctx.mainView.layoutLeft.__update();
            return dfd;
        },
        registerSplitEditors: function () {},
        createProtocolWizard: function () {},
        createWizard: function () {}
    });
});