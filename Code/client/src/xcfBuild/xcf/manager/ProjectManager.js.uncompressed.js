define("xcf/manager/ProjectManager", [
    'dcl/dcl',
    'xide/manager/ManagerBase'
], function (dcl,ManagerBase){
    return dcl(ManagerBase,{
        declaredClass:"xcf.manager.ProjectManager"
    });
});

