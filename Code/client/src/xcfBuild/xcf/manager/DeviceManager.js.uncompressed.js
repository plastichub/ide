/** @module xcf/manager/DeviceManager */
define("xcf/manager/DeviceManager", [
    'dcl/dcl',
    "xdojo/declare",
    "dojo/_base/lang",
    'xide/encoding/MD5',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xcf/manager/BeanManager',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin'/*NMD:Ignore*/,
    './DeviceManager_Server',
    './DeviceManager_DeviceServer',
    'xide/data/Memory',
    'xide/data/TreeMemory',
    'dojo/has',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'xcf/model/Device',
    'dojo/Deferred',
    "xide/manager/ServerActionBase",
    "xide/data/Reference",
    'xide/utils/StringUtils',
    'xcf/mixins/LogMixin',
    'xdojo/has!xcf-ui?./DeviceManager_UI', /*NMD:Ignore*/
    'xdojo/has!xexpression?xexpression/Expression', /*NMD:Ignore*/
    'dojo/promise/all',
    "xide/console",
    "xide/lodash"
    //'xdojo/has!host-node?nxapp/utils/_console',
    //"xdojo/has!host-node?nxapp/utils"
], function (dcl, declare, lang, MD5,
    types, utils, factory, BeanManager, ReloadMixin, EventedMixin,
    DeviceManager_Server, DeviceManager_DeviceServer, Memory, TreeMemory, has,
    ObservableStore, Trackable, Device, Deferred, ServerActionBase, Reference, StringUtils,
    LogMixin,
    DeviceManager_UI, Expression, all, console, _, _console, xUtils) {
    /*
     var console = typeof window !== 'undefined' ? window.console : console;
     if(_console && _console.error && _console.warn){
     console = _console;
     }
     */
    const bases = [
              ServerActionBase,
              BeanManager,
              DeviceManager_Server,
              DeviceManager_DeviceServer,
              ReloadMixin.dcl,
              LogMixin
          ];

    const _debugMQTT = false;
    const _debug = false;
    const _debugLogging = false;
    const _debugConnect = false;
    const isServer = ! true ;
    const isIDE =  true ;
    const DEVICE_PROPERTY = types.DEVICE_PROPERTY;
    const runDrivers = has('runDrivers');
    const EVENTS = types.EVENTS;
     true  && bases.push(DeviceManager_UI);
    /**
     * Common base class, for server and client.
     * @class module:xcf/manager/DeviceManager
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xcf/manager/BeanManager
     * @extends module:xide/mixins/ReloadMixin
     * @extends module:xcf/manager/DeviceManager_DeviceServer
     */
    return dcl(bases, {
        declaredClass: "xcf.manager.DeviceManager",
        /***
         * The Bean-Manager needs a unique name of the bean:
         * @private
         */
        beanNamespace: 'device',
        /***
         * The Bean-Manager has some generic function like creating Dialogs for adding new items, please
         * provide a title for the interface.
         * @private
         */
        beanName: 'Device',
        beanUrlPattern: "{id}",
        breanScheme: "device://",
        beanPriority: 1,
        /**
         * the icon class for bean edit views
         * @private
         */
        beanIconClass: 'fa-sliders',
        /**
         * Bean group type
         * @private
         */
        groupType: types.ITEM_TYPE.DEVICE_GROUP,
        /**
         * Bean item type
         * @private
         */
        itemType: types.ITEM_TYPE.DEVICE,
        /**
         * The name of the CI in the meta database for the title or name.
         * @private
         */
        itemMetaTitleField: DEVICE_PROPERTY.CF_DEVICE_TITLE,
        /**
         * Name of the system scope
         * @private
         */
        systemScope: 'system_devices',
        /**
         * Name of the user scope
         * @private
         */
        userScope: 'user_devices',
        /**
         * Name of the app scope
         * @private
         */
        appScope: 'app_devices',
        /**
         * Name of the default scope for new created items
         * @private
         */
        defaultScope: 'system_devices',
        /***
         * The RPC server class:
         * @private
         */
        serviceClass: 'XCF_Device_Service',
        /***
         * A copy of all devices raw data from the server
         * @private
         */
        rawData: null,
        /***
         * @type {module:xide/data/_Base}
         * @private
         */
        store: null,
        /***
         * {xcf.views.DevicesTreeView}
         * @private
         */
        treeView: null,
        /***
         * {xide.client.WebSocket}
         */
        deviceServerClient: null,
        /***
         *  An array of started device instances.
         *  @private
         */
        deviceInstances: null,
        /***
         *  A map of scope names for module hot reloading
         *  @private
         */
        driverScopes: null,
        /***
         * autoConnectDevices does as it says, on app start, it connects to all known devices of the
         * project
         * @param autoConnectDevices
         * @private
         */
        autoConnectDevices: true,
        /**
         * Consoles is an array of {xide.views.ConsoleViews}. There is one console per device possible
         * @private
         */
        consoles: null,
        /**
         * lastUpTime is being used to recognize a computer suspend hibernate
         * @private
         */
        lastUpTime: null,
        /**
         * @private
         */
        reconnectDevice: 15000,
        /**
         * @private
         */
        reconnectDeviceServer: 5000,

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Device-Related
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onRunClassEvent: function (data) {
            const id = data.args.id || data.id;
            if (this.running && this.running[id]) {
                const runData = this.running[id];
                const delegate = runData.delegate;
                if (!delegate) {
                    return;
                }
                if (data.error) {
                    if (delegate.onError) {
                        delegate.onError(data.error);
                        delete this.running[id];
                    }
                }
                if (data.finish) {
                    if (delegate.onFinish) {
                        delegate.onFinish(data.finish);
                        delete this.running[id];
                    }
                }
                if (data.progress) {
                    if (delegate.onProgress) {
                        delegate.onProgress(data.progress);
                    }
                }
                
                if (data.interrupt) {
                    if (delegate.onInterrupt) {
                        delegate.onInterrupt(data.interrupt);
                    }
                }
            }
        },
        getInstanceByName: function (name) {
            const instances = this.deviceInstances;
            const self = this;
            for (const instance in instances) {
                const device = instances[instance].device;
                if (!device) {
                    continue;
                }
                const title = self.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_TITLE);
                if (title === name) {
                    return instances[instance];
                }
            }
        },
        /**
         * Returns the file object for a device
         * @param device
         * @returns {module:xfile/model/File}
         */
        getFile: function (device) {
            const dfd = new Deferred();
            const ctx = this.ctx;
            if (_.isString(device)) {
                device = this.getItemByPath(device + '.meta.json') || this.getItemByPath(device) || device;
            }
            const fileManager = ctx.getFileManager();
            const fileStore = fileManager.getStore(device.scope);
            const item = fileStore._getItem(device.path);
            if (item) {
                dfd.resolve(item);
                return dfd;
            }
            fileStore.initRoot().then(function () {
                fileStore._loadPath('.', true).then(function () {
                    fileStore.getItem(device.path, true).then(function (item) {
                        dfd.resolve(item);
                    });
                });
            });
            return dfd;
        },
        getSourceHash: function () {
            const userDirectory = this.ctx.getUserDirectory();
            return userDirectory || "no_user_directory";
        },
        /**
         * Make sure we've a connection to our device-server
         * @public
         */
        checkDeviceServerConnection: function () {
            if (!this.ctx.getNodeServiceManager) {
                return true;
            }
            const ctx = this.getContext();
            const nodeServiceManager = has('xnode') && ctx.getNodeServiceManager ? this.ctx.getNodeServiceManager() : null;
            if (!this.deviceServerClient && nodeServiceManager) {
                const store = nodeServiceManager.getStore();
                if (!store) {
                    return false;
                }
                this.createDeviceServerClient(store);
            } else {
                return false;
            }
            return true;
        },
        /**
         *
         * @param target
         * @param source
         * @private
         */
        addDriverFunctions: function (target, source) {
            for (const i in source) {
                if (i === 'constructor' ||
                    i === 'inherited' ||
                    i === 'getInherited' ||
                    i === 'isInstanceOf' ||
                    i === '__inherited' ||
                    i === 'onModuleReloaded' ||
                    i === 'start' ||
                    i === 'publish' ||
                    i === 'subscribe' ||
                    i === 'getInherited' ||
                    i === 'getInherited'
                ) {
                    continue;
                }
                if (_.isFunction(source[i]) && !target[i]) {
                    target[i] = source[i];//swap
                }
            }
        },
        /**
         *
         * @param driver
         * @param instance
         * @private
         */
        addLoggingFunctions: function (driver, instance) {
            const thiz = this;
            instance.log = function (level, type, message, data) {
                data = data || {};
                const oriData = lang.clone(data);
                data.type = data.type || type || 'Driver';
                if (instance.options) {
                    data.device = instance.options;
                }
                thiz.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, {
                    data: data,
                    level: level || 'info',
                    message: message,
                    details: oriData
                });
            };
        },
        /**
         * An instance of a driver class has been created.
         * We mixin new functions: callCommand, set/get-Variable, log
         * @param driver
         * @param instance
         * @param device
         * @private
         */
        completeDriverInstance: function (driver, instance, device) {
            _debug && console.info('complete driver instance');
            const thiz = this;
            const scope = instance.blockScope;
            const store = scope.blockStore;
            const parentId = device.path;
            const commandsRoot = parentId + '_commands';
            const variablesRoot = parentId + '_variables';

            store.on('delete', function (evt) {
                const _isVariable = evt.target.declaredClass.indexOf('Variable') !== -1;
                const _parent = _isVariable ? variablesRoot : commandsRoot;
                const referenceParent = device._store.getSync(_parent);

                if (referenceParent) {
                    referenceParent.refresh();
                }

                const referenceId = _parent + '_reference_' + evt.target.id;
                const reference = device._store.getSync(referenceId);

                if (reference) {
                    reference.refresh();
                }
            });
            function createReference(block, driver, title, icon) {
                const _isVariable = block.declaredClass.indexOf('Variable') !== -1;
                const _parent = _isVariable ? variablesRoot : commandsRoot;
                if (block.declaredClass.indexOf(_isVariable ? 'Variable' : 'Command') == -1) {
                    return;
                }

                let reference = new Reference({
                    enabled: true,
                    path: _parent + '_reference_' + block.id,
                    name: title,
                    id: block.id,
                    parentId: _parent,
                    _mayHaveChildren: false,
                    virtual: true,
                    tooltip: true,
                    icon: icon,
                    ref: {
                        driver: driver,
                        item: block,
                        device: device
                    },
                    type: types.ITEM_TYPE.BLOCK
                });

                reference = device._store.putSync(reference);

                block.addReference(reference, {
                    properties: {
                        "name": true,
                        "enabled": true,
                        "value": true
                    },
                    onDelete: true
                }, true);

                reference.refresh();
            }

            store.on('added', function (block) {
                createReference(block, driver, block.name, block.icon || 'fa-exclamation');
            });

            instance.setVariable = function (title, value, save, publish, highlight) {
                const _variable = this.blockScope.getVariable(title);
                if (_variable) {
                    _variable.value = value;
                    if (highlight === false) {
                        _variable.__ignoreChangeMark = true;
                    }
                    _variable.set('value', value, save, publish, highlight);
                    if (highlight === false) {
                        _variable.__ignoreChangeMark = false;
                    }
                } else {
                    _debug && console.log('no such variable : ' + title);
                    return;
                }
                thiz.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, {
                    item: _variable,
                    scope: this.blockScope,
                    driver: driver,
                    owner: thiz,
                    save: save === true,
                    publish: publish
                });
            };
            /**
             * Add getVariable
             * @param title
             */
            instance.getVariable = function (title) {
                const _variable = this.blockScope.getVariable(title);
                if (_variable) {
                    return _variable._getArg(_variable.value, false);
                }
                return '';
            };

            /**
             * add log function
             * @param level
             * @param type
             * @param message
             * @param data
             */
            instance.log = function (level, type, message, data) {
                data = data || {};
                const oriData = lang.clone(data);
                data.type = data.type || type || 'Driver';
                if (instance.options) {
                    data.device = instance.options;
                }
                thiz.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, {
                    data: data,
                    level: level || 'info',
                    message: message,
                    details: oriData
                });
            };
            for (const i in driver) {
                if (i === 'constructor' ||
                    i === 'inherited' ||
                    i === 'getInherited' ||
                    i === 'isInstanceOf' ||
                    i === '__inherited' ||
                    i === 'onModuleReloaded' ||
                    i === 'start' ||
                    i === 'publish' ||
                    i === 'subscribe' ||
                    i === 'getInherited' ||
                    i === 'getInherited'
                ) {
                    continue;
                }
                if (lang.isFunction(driver[i]) && !instance[i] /*&& lang.isFunction(target[i])*/) {
                    instance[i] = driver[i];//swap
                }
            }
        },
        getDevice: function (mixed) {
            let result = mixed;
            if (_.isString(mixed)) {
                const byId = this.getItemById(mixed);
                if (byId) {
                    result = byId;
                } else {
                    const byPath = this.getItemByPath(mixed);
                    if (byPath) {
                        result = byPath;
                    }
                }
            }
            return result;
        },
        /**
         * Stops a device with a device model item
         * @param _item {module:xcf/model/Device|string}
         */
        stopDevice: function (_item) {
            const device = this.getDevice(_item) || _item;
            if (!device) {
                console.error('cant find device ' + _item);
                return;
            }
            this.checkDeviceServerConnection();
            device._userStopped = true;
            const cInfo = this.toDeviceControlInfo(device);
            if (!cInfo) {
                _debugConnect && console.error('cant find device::no device info', device.toString && device.toString());
                return;
            }
            if (isServer && (!cInfo.serverSide && !device.isServer())) {
                return;
            }
            const hash = cInfo.hash;
            if (this.deviceInstances[hash]) {
                this._removeInstance(this.deviceInstances[hash], hash, device);
                delete this.deviceInstances[hash];
                _debugConnect && console.log('-- stop device ' + hash, this.deviceInstances);
            } else {
                _debugConnect && console.log('cant find instance ' + hash);
                return;
            }
            this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.MANAGER_STOP_DRIVER, cInfo);
            //this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.STOP_DEVICE, cInfo);
        },
        /**
         * @TODO: remove back compat
         * @param scope
         * @returns {*}
         */
        getStore: function (scope) {
            scope = scope || 'system_devices';
            const store = this.stores[scope];
            if (store) {
                return store;
            }
            if (scope) {
                return this.ls(scope);
            }
        },
        /**
         * Get all enabled devices
         * @param enabledOnly
         * @param addDriver
         * @returns {module:xcf/model/Device[]}
         */
        getDevices: function (enabledOnly, addDriver) {
            const store = this.getStore();
            if (!store) {
                return [];
            }
            let items = utils.queryStore(store, {
                isDir: false
            });
            if (items._S) {
                items = [items];
            }

            const result = [];
            for (let i = 0; i < items.length; i++) {

                const device = items[i];
                const enabled = this.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_ENABLED);

                if ((enabledOnly === true && enabled === true || enabled === null) || enabledOnly === false) {
                    result.push(device);
                    if (addDriver === true) {
                        const driverId = this.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                        if (!driverId) {
                            _debug && console.error('device has no driver id!');
                            continue;
                        }
                        const driver = this.ctx.getDriverManager().getItemById(driverId);
                        if (driver) {
                            device.driver = driver;
                        }
                    }
                }
            }
            return result;
        },
        getStores: function () {
            const stores = [];
            for (const scope in this.stores) {
                const store = this.stores[scope];
                if (store) {
                    stores.push(store);
                }
            }
            return stores;
        },
        getStorePath: function (scope) {
            const ctx = this.getContext();
            const resourceManager = ctx.getResourceManager();
            if (scope === 'user_devices') {
                return resourceManager.getVariable('USER_DIRECTORY');
            }
        },
        /**
         * Connect to all known devices
         * @private
         */
        connectToAllDevices: function () {
            if (!this.deviceServerClient) {
                this.checkDeviceServerConnection();
                return;
            }

            const stores = this.getStores();
            const thiz = this;

            if (!this.getStores().length) {
                return;
            }
            const all = [];

            function start(device) {
                const deviceDfd = thiz.startDevice(device);
                all.push(deviceDfd);
                _debugConnect && console.log('start device ' + thiz.toDeviceControlInfo(device).title);
            }

            function connect(store) {
                if (!store) {
                    console.error('have no device store');
                    return;
                }
                if (store.connected) {
                    return;
                }
                store.connected = true;
                let items = utils.queryStore(store, {
                    isDir: false
                });
                if (items._S) {
                    items = [items];
                }

                if (!_.isArray(items)) {
                    items = [items];
                }
                for (let i = 0; i < items.length; i++) {
                    const device = items[i];
                    const enabled = thiz.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_ENABLED);
                    if (enabled == true || enabled == null) {
                        start(device);
                    }
                }
            }

            if (this.deviceServerClient.dfd) {
                this.deviceServerClient.dfd.then(function () {
                    _.each(stores, connect, this);
                }.bind(this));
            } else {
                _.each(stores, connect, this);
            }
            return all;
        },
        /**
         *
         * @param clientOptions
         * @param localDevice {module:xcf/model/Device}
         */
        updateDevice: function (clientOptions, localDevice) {
            if (localDevice) {
                localDevice.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS, clientOptions.driverOptions);
                localDevice.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS, clientOptions.options);
                localDevice.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED, clientOptions.enabled);
            }
        },
        debug: function () {
            console.info('Debug info stores : ');
            _.each(this.stores, function (store) {
                console.log('have store ' + store.scope);
            });
        },
        /**
         *
         * @private
         */
        _getLogText: function (str) {
            return moment().format("HH:mm:ss:SSS") + ' ::   ' + str + '';
        },
        _parse: function (scope, expression) {
            let str = '' + expression;
            if (str.indexOf('{{') > 0 || str.indexOf('}}') > 0) {
                console.time('parse expression');
                const _parser = new Expression();
                str = _parser.parse(types.EXPRESSION_PARSER.FILTREX,
                    str, this, {
                        variables: scope.getVariablesAsObject(),
                        delimiters: {
                            begin: '{{',
                            end: '}}'
                        }
                    }
                );
                console.timeEnd('parse expression');
            } else {
                const _text = scope.parseExpression(expression);
                if (_text) {
                    str = _text;
                }
            }
            return str;
        },
        /**
         *
         * @param driver
         * @param device
         * @private
         */
        runCommand: function (driver, device) {
        },
        /**
         * @private
         */
        _lastActions: null,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Data related
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         *
         * @param rawData
         * @private
         */
        onStoreReloaded: function (rawData) {
            this.completeDeviceStore();
        },
        getInstance: function (mixed) {
            const deviceInfo = mixed ? mixed._store ? this.toDeviceControlInfo(mixed) : mixed : null;
            if (!deviceInfo) {
                return;
            }
            return this.getDriverInstance(deviceInfo, false);
        },
        /***
         * returns driver instance!
         * @param deviceInfo {module:xide/types~DeviceInfo}
         * @param fillSettings will convert and put CI settings into the driver's instance (member variable)
         * @returns {module:xcf/driver/DriverBase}
         * @private
         */
        getDriverInstance: function (deviceInfo, fillSettings) {
            if (!deviceInfo) {
                console.error('getDriverInstance::have no device info');
                return null;
            }
            for (const i in this.deviceInstances) {

                const instance = this.deviceInstances[i];
                const instanceOptions = instance.options;
                if (!instanceOptions) {
                    continue;
                }

                if (instanceOptions.port === deviceInfo.port &&
                    instanceOptions.host === deviceInfo.host &&
                    instanceOptions.protocol === deviceInfo.protocol &&
                    instanceOptions.isServer === deviceInfo.isServer) {

                    if (fillSettings && instance.sendSettings &&  true ) {
                        fillSettings = false;
                    }

                    if (fillSettings !== false) {
                        //get settings, if not cached already
                        if (instance && !instance.sendSettings) {
                            //pick driver
                            const driver = this.ctx.getDriverManager().getItemById(deviceInfo.driverId);//driverStore item
                            if (driver) {
                                const meta = driver['user'];
                                const commandsCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
                                if (commandsCI && commandsCI['params']) {
                                    instance.sendSettings = utils.getJson(commandsCI['params']);
                                }

                                const responseCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES);
                                if (responseCI && responseCI['params']) {
                                    instance.responseSettings = utils.getJson(responseCI['params']);
                                }
                            } else {
                                _debug && console.warn('getDriverInstance:: cant find driver');
                            }
                        }
                    }
                    return instance;
                }
            }
            return null;
        },
        _reconnectServerTimer: null,
        /**
         * @private
         */
        onDeviceServerConnectionLost: function () {

            if (this.deviceServerClient && this.deviceServerClient.pageUnloaded) {
                return;
            }
            if (this.deviceServerClient) {
                this.deviceServerClient.destroy();
                this.deviceServerClient = null;
            }

            if (this._reconnectServerTimer) {
                return;
            }
            const thiz = this;
            if (isIDE) {
                thiz.ctx.getNotificationManager().postMessage({
                    message: 'Lost connection to device server, try reconnecting in 5 seconds',
                    type: 'error',
                    showCloseButton: true,
                    duration: 1000
                });
            }
            this._reconnectServerTimer = setTimeout(function () {
                thiz.checkDeviceServerConnection();
                thiz._reconnectServerTimer = null;
            }, this.reconnectDeviceServer);

            console.log('lost device server connection');
            _.each(this.deviceInstances, function (instance) {

                if (instance && instance.device && !instance.domNode) {
                    const device = instance.device;
                    const driverInstance = device.driverInstance;
                    driverInstance && driverInstance.onLostServer && driverInstance.onLostServer();
                    device.setState(types.DEVICE_STATE.LOST_DEVICE_SERVER);
                }
            });

        },
        /**
         *
         * @param msg
         * @private
         */
        onMQTTMessage: function (msg) {
            const message = utils.getJson(msg.message);
            let isUs = false;
            const thiz = this;
            if (message) {
                const sourceHost = message.sourceHost;
                const sourcePort = message.sourcePort;
                const mqttHost = msg.host;
                const mqttPort = msg.port;
                if (sourceHost && sourcePort) {
                    if (sourceHost === mqttHost && sourcePort == mqttPort) {
                        isUs = true;
                    }
                }
            }

            if (!isUs) {

                _debugMQTT && console.info('onMQTTMessage:');
                //
                const parts = msg.topic.split('/');
                if (parts.length == 4 && parts[2] === 'Variable' && message.device) {
                    const _device = this.getDeviceStoreItem(message.device);
                    if (_device) {
                        _debugMQTT && console.info('\tonMQTTMessage: on mqtt variable topic ' + msg.topic);
                        const _deviceInfo = this.toDeviceControlInfo(message.device);
                        if (_deviceInfo) {
                            const driverInstance = this.getDriverInstance(_deviceInfo);
                            if (driverInstance) {
                                const scope = driverInstance.blockScope;
                                const _variable = scope.getVariable(parts[3]);
                                if (_variable) {
                                    _debugMQTT && console.info('     received MQTT variable ' + _variable.name + ' = ' + message.value);
                                    if ( true ) {
                                        _variable.set('value', message.value);
                                        _variable.refresh();
                                    } else {
                                        delete _variable.value;
                                        _variable.value = message.value;
                                    }
                                    thiz.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, {
                                        item: _variable,
                                        scope: _variable.scope,
                                        owner: thiz,
                                        save: false,
                                        publish: true,
                                        source: 'mqtt',
                                        publishMQTT: false
                                    });

                                }
                            } else {
                                _debugMQTT && console.error('cant find driver instance ' + msg.topic);
                            }
                        } else {
                            _debugMQTT && console.error('cant find device info');
                        }
                    } else {
                        console.error('cant find device for : ' + msg.topic);
                    }
                }
            } else {
                _debugMQTT && console.error('same source');
            }
        },
        /**
         * Find a block by url in all instances
         * @param url
         * @returns {*}
         */
        getBlock: function (url) {
            for (const id in this.deviceInstances) {
                const instance = this.deviceInstances[id];
                if (!instance || !instance.device) {
                    continue;
                }
                const scope = instance.blockScope;
                if (scope) {
                    const block = scope.resolveBlock(url);
                    if (block) {
                        return block;
                    }
                }
                
            }
            return this.ctx.getDriverManager().getBlock(url);
        },
        /***
         * Callback when the NodeJS service manager initialized its service store. That may
         * happen multiple times as user can reload the store.
         *
         * @param evt
         * @private
         */
        onNodeServiceStoreReady: function (evt) {
            if (this.deviceServerClient) {
                //this.deviceServerClient.destroy();
                return this.deviceServerClient;
            }
            const store = evt.store;
            const thiz = this;
            const client = this.createDeviceServerClient(store);
            const connect =  true  &&  true ;
        },
        /**
         *
         * @param instance
         * @param modulePath
         * @private
         */
        onDriverUpdated: function (instance, modulePath) {
            return;
        },
        /**
         * Some file has changed, update driver instance
         * @param evt
         * @private
         */
        onModuleReloaded: function (evt) {
            if (this.deviceInstances.length == 0) {//nothing to do
                return;
            }
            const modulePath = utils.replaceAll('//', '/', evt.module);
            const newModule = evt.newModule;
            let found = false;
            for (const i in this.deviceInstances) {

                const instance = this.deviceInstances[i];

                if (instance.modulePath === modulePath ||
                    instance.baseClass === modulePath) {
                    this.mergeFunctions(instance, newModule.prototype);
                    found = true;
                    _debug && console.log('Did update driver code : ' + modulePath, newModule.prototype);
                    if (instance.blockScope) {
                        instance.blockScope.expressionModel.expressionCache = {};
                    }
                    this.onDriverUpdated(instance, modulePath);
                }
            }
        },
        /**
         *
         * @param instance
         * @param hash
         * @param device
         * @private
         */
        _removeInstance: function (instance, hash, device) {
            if (instance.destroy) {
                instance.destroy();
            }
            instance.blockScope && instance.blockScope._destroy();
            delete this.deviceInstances[hash];
            this.ctx.getBlockManager().removeScope(instance.options.id);
            this.ctx.getDriverManager().removeDriverInstance(instance, device);
            device.reset();
        },
        /**
         *
         * @param deviceInfo
         * @private
         */
        removeDriverInstance: function (deviceInfo) {
            const instance = this.getDriverInstance(deviceInfo);
            if (instance) {
                this.ctx.getBlockManager().removeScope(instance.options.id);
                if (instance.blockScope) {
                    instance.blockScope.destroy();
                }
                instance.destroy();
            } else {
                _debugConnect && console.error('remove instance : cant find!');
            }
            for (const i in this.deviceInstances) {
                if (instance == this.deviceInstances[i]) {
                    delete this.deviceInstances[i];
                }
            }
        },
        /**
         *
         * @param item {module:xcf/model/Device} the device
         * @param name {string} the name of the CI
         * @returns {string|int|object|null}
         */
        getMetaValue: function (item, name) {
            const meta = item['user'];
            if (meta) {
                return utils.getCIInputValueByName(meta, name);
            }
            return null;
        },
        /**
         * Return device by host and port
         * @param host {string}
         * @param port {string}
         * @returns {module:xcf/model/Device|null}
         */
        getDeviceByHost: function (host, port) {
            const items = utils.queryStore(this.getStore(), {
                isDir: false
            });
            for (let i = 0; i < items.length; i++) {
                const device = items[i];
                const _host = this.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_HOST);
                const _port = this.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_PORT);
                if (_host == host && _port == port) {
                    return device;
                }
            }
            return null;
        },
        getDeviceByUrl: function (url) {
            let parts = utils.parse_url(url);
            parts = utils.urlArgs(parts.host);
            return this.getDeviceById(parts.device.value);
        },
        /**
         * Returns a device by id
         * @param id {string}
         * @returns {module:xcf/model/Device|null}
         */
        getDeviceById: function (id, store) {
            const self = this;

            function search(_store) {
                let items = utils.queryStore(_store, {
                    isDir: false
                });

                if (items._S) {
                    items = [items];
                }
                for (let i = 0; i < items.length; i++) {
                    const device = items[i];
                    const _id = self.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_ID);
                    if (_id == id) {
                        return device;
                    }
                }
                return null;
            }

            const _store = _.isString(store) ? this.getStore(store) : null;
            if (_store) {
                return search(_store);
            } else {
                for (const scope in this.stores) {
                    const item = search(this.stores[scope]);
                    if (item) {
                        return item;
                    }
                }
            }
            return null;
        },
        /**
         * Returns a device by id
         * @param title {string}
         * @returns {module:xcf/model/Device|null}
         */
        getDeviceByName: function (title, store) {
            const self = this;

            function search(_store) {
                let items = utils.queryStore(_store, {
                    isDir: false
                });

                if (items._S) {
                    items = [items];
                }
                for (let i = 0; i < items.length; i++) {
                    const device = items[i];
                    const _id = self.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_TITLE);
                    if (_id == title) {
                        return device;
                    }
                }
                return null;
            }

            const _store = _.isString(store) ? this.getStore(store) : null;
            if (_store) {
                return search(_store);
            } else {
                for (const scope in this.stores) {
                    const item = search(this.stores[scope]);
                    if (item) {
                        return item;
                    }
                }
            }
            return null;
        },
        /**
         * Returns all devices by driver id
         * @param id {string} the driver id
         * @returns {module:xcf/model/Device[]}
         */
        getDevicesByDriverId: function (id) {
            let items = utils.queryStore(this.getStore(), {
                isDir: false
            });
            if (items._S) {
                items = [items];
            }
            for (let i = 0; i < items.length; i++) {
                const device = items[i];
                const _id = this.getMetaValue(device, DEVICE_PROPERTY.CF_DEVICE_ID);
                if (_id == id) {
                    return device;
                }
            }
            return null;
        },
        _cachedItems: null,
        getDeviceStoreItem: function (deviceInfo) {
            if (!deviceInfo) {
                return;
            }

            if (deviceInfo.hash && this._cachedItems) {
                const _cached = this._cachedItems[deviceInfo.hash];
                if (_cached) {
                    return _cached;
                }
            }

            //already device
            //if (deviceInfo && deviceInfo._store) {
            //return deviceInfo;
            //}
            const scope = deviceInfo.deviceScope;
            const store = this.getStore(scope);
            if (!store) {
                return;
            }

            const byPath = deviceInfo.devicePath ? this.getItemByPath(deviceInfo.devicePath) : null;
            if (byPath) {
                return byPath;
            }
            let items = _.filter(store.data, function (item) {
                return item.isDir !== true;
            });
            if (!items) {
                _debug && !isServer && console.error('store returned nothing ' + deviceInfo.deviceScope);
                return null;
            }
            if (items && !_.isArray(items)) {
                items = [items];
            }
            for (let i = 0; i < items.length; i++) {
                let device = items[i];
                const meta = device['user'];
                const host = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_HOST);
                const port = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_PORT);
                const id = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_ID);
                const protocol = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_PROTOCOL);

                if (port === deviceInfo.port &&
                    host === deviceInfo.host &&
                    protocol === deviceInfo.protocol &&
                    device.isServer() === deviceInfo.isServer &&
                    id === deviceInfo.id) {

                    const deviceStoreItem = store.getSync(device.path);
                    if (deviceStoreItem) {
                        device = deviceStoreItem;
                    }

                    if (deviceInfo.hash) {
                        if (!this._cachedItems) {
                            this._cachedItems = {};
                        }
                        this._cachedItems[deviceInfo.hash] = device;
                    }
                    return device;
                }
            }
        },
        /**
         *
         * @param ci
         * @param storeRef
         * @private
         */
        onDriverSettingsChanged: function (ci, storeRef) {
            for (const i in this.deviceInstances) {

                const instance = this.deviceInstances[i];
                //get settings, if not cached already
                if (instance && instance.driver == storeRef) {
                    //pick driver
                    const driver = storeRef;// this.ctx.getDriverManager().getItemById(instance.options.id);//driverStore item
                    const meta = driver['user'];
                    const commandsCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
                    if (commandsCI && commandsCI['params'] && commandsCI == ci) {
                        instance.sendSettings = utils.getJson(commandsCI['params']);
                    }
                    const responseCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES);
                    if (responseCI && responseCI['params']) {
                        instance.responseSettings = utils.getJson(responseCI['params']);
                    }
                    break;
                }
            }
        },
        /**
         * @private
         */
        onDeviceStateChanged: function (item, silent) {
            if (item._userStopped !== true && silent !== true && item.info && item.state && item.state == types.DEVICE_STATE.DISCONNECTED) {
                this.ctx.getNotificationManager().postMessage({
                    message: 'Lost connection to ' + item.info.host + ', ...reconnecting',
                    type: 'error',
                    showCloseButton: false,
                    duration: 1500
                });
            }
            if (silent !== true && item.info && item.state && item.state == types.DEVICE_STATE.CONNECTED) {
                this.ctx.getNotificationManager().postMessage({
                    message: 'Connected to ' + item.info.host + '',
                    type: 'success',
                    showCloseButton: false,
                    duration: 2000
                });
            }
        },
        /**
         *
         * @param item
         * @returns {*}
         * @private
         */
        connectDevice: function (item) {
            this.checkDeviceServerConnection();
            const cInfo = this.toDeviceControlInfo(item);
            if (!cInfo) {
                console.error('couldnt start device, invalid control info');
                return;
            }
            const hash = cInfo.hash;
            if (this.deviceInstances[hash]) {
                _debugConnect && console.log('device already connected', cInfo);
                item.setState(types.DEVICE_STATE.CONNECTED);
                return this.deviceInstances[hash];
            }
            item.setState(types.DEVICE_STATE.CONNECTING);
            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                text: 'Trying to connect to ' + cInfo.toString(),
                type: 'info'
            });
            this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.MANAGER_START_DRIVER, cInfo);
        },
        /**
         * client application ready, mixin instances and block scopes
         * @param evt
         * @private
         */
        onAppReady: function (evt) {
            const appContext = evt.context;
            appContext.deviceManager = this;
            appContext.driverManager = this;
            if (appContext.blockManager) {
                utils.mixin(appContext.blockManager.scopes, this.ctx.getBlockManager().scopes);
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Server methods (NodeJs)
        //
        /////////////////////////////////////////////////////////////////////////////////////

        /**
         * @private
         */
        onHaveNoDeviceServer: function () {
            if (!this.ctx.getNotificationManager()) {
                return;
            }
            const thiz = this;
            const msg = this.ctx.getNotificationManager().postMessage({
                message: 'Have no device server connection',
                type: 'error',
                showCloseButton: true,
                duration: 1500,
                actions: {
                    reconnect: {
                        label: 'Reconnect',
                        action: function () {
                            thiz.checkDeviceServerConnection();
                            return msg.update({
                                message: 'Reconnecting...',
                                type: 'success',
                                actions: false,
                                duration: 1500
                            });
                        }
                    }
                }
            });
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Server methods (PHP)
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /***
         * setDriverScriptContent is storing a driver's actual code in a given scope on the server
         * @param scope {string}
         * @param path  {string}
         * @param content  {string}
         * @param readyCB   {function}
         * @param errorCB   {function}
         * @returns {*}
         * @private
         */
        setDriverScriptContent: function (scope, path, content, readyCB, errorCB) {
            return this.callMethodEx(null, 'setDriverContent', [scope, path, content], readyCB, true);
        },
        /***
         * getDriverScriptContent is receiving a driver's actual code in a given scope
         * @param scope {string}
         * @param path  {string}
         * @param readyCB   {function}
         * @returns {*}
         */
        getDriverScriptContent: function (scope, path, readyCB) {
            return this.callMethodEx(null, 'getDriverContent', [scope, path], readyCB, true);
        },
        /**
         *
         * @param data
         * @param scope
         * @param track
         */
        createStore: function (data, scope, track) {
            const storeClass =  true  ? declare('deviceStore', [TreeMemory, Trackable, ObservableStore], {}) : declare('deviceStore', [Memory], {});
            const self = this;
            const store = new storeClass({
                data: data.items,
                idProperty: 'path',
                parentProperty: 'parentId',
                Model: Device,
                id: utils.createUUID(),
                scope: scope,
                ctx: this.getContext(),
                mayHaveChildren: function (parent) {
                    if (parent._mayHaveChildren === false) {
                        return false;
                    }
                    if (parent.isDevice) {
                        const device = parent;
                        if (device.driverInstance) {
                            return true;
                        }
                        //no instance yet
                        const info = self.toDeviceControlInfo(device);
                        if (info) {
                            const driver = self.getContext().getDriverManager().getDriverById(info.driverId);
                            if (driver) {
                                if (!driver.blockScope &&  true ) {
                                    this.ctx.getDriverManager().createDriverBlockScope(driver);
                                    self.completeDevice(device, driver);
                                }
                                return true;
                            }
                        } else {
                            console.error('cant get device info for ', device);
                        }

                    }
                    return true;
                },
                observedProperties: [
                    "name",
                    "state",
                    "iconClass",
                    "enabled"
                ]
            });

            if (scope && track !== false) {
                this.setStore(scope, store);
            }
            return store;
        },
        /**
         *
         * @param data
         * @param scope
         * @param track
         * @returns {exports|module.exports|module:xide/data/_Base}
         * @private
         */
        initStore: function (data, scope, track) {
            return this.createStore(data, scope, track);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        _deviceInfoCache: null,
        /**
         * Return handy info for a device
         * @param {module:xcf/model/Device|null} item
         * @returns {module:xide/types~DeviceInfo|null}
         */
        toDeviceControlInfo: function (item) {
            if (!item) {
                return null;
            }
            const hash = item.hash;
            if (! true  && hash && ! false ) {
                if (this._deviceInfoCache[hash]) {
                    return this._deviceInfoCache[hash];
                }
            }
            if (!item._store && item.id) {
                var _item = this.getItemById(item.id);
                if (_item) {
                    item = _item;
                }
            }
            if (!item || !item.path) {
                _debug && console.error('not a device');
                var _item = this.getDeviceStoreItem(item);
                if (!_item) {
                    return null;
                }
            }
            _debug && !item && console.error('toDeviceControlInfo: invalid device item');
            _debug && !item.user && console.error('toDeviceControlInfo: invalid device item, has no meta');
            const meta = item['user'];
            const host = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_HOST);
            const port = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_PORT);
            const enabled = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_ENABLED);
            const title = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_TITLE);
            const protocol = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_PROTOCOL);
            const driverId = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_DRIVER);
            const options = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
            const loggingFlags = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_LOGGING_FLAGS);
            const driverOptions = utils.getCIInputValueByName(meta, DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            const serverSide = item.isServerSide();
            const isServer = item.isServer();
            let result = null;

            this.fixDeviceCI(item);
            const driver = this.ctx.getDriverManager().getDriverById(driverId);
            if (driver) {
                const driverMeta = driver['user'];
                const script = utils.getCIInputValueByName(driverMeta, types.DRIVER_PROPERTY.CF_DRIVER_CLASS);
                const responseCI = utils.getCIByChainAndName(driverMeta, 0, types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES);
                let responseSettings = {};
                const driverScope = driver['scope'];

                if (responseCI && responseCI['params']) {
                    responseSettings = utils.getJson(responseCI['params']);
                }
                result = {
                    host: host,
                    port: port,
                    protocol: protocol,
                    driver: script ? script.replace('./', '') : '',
                    driverId: driverId,
                    driverScope: driverScope,
                    id: item.id,
                    devicePath: item.path,
                    deviceScope: item.getScope(),
                    title: title,
                    options: options,
                    enabled: enabled,
                    driverOptions: driverOptions,
                    serverSide: serverSide,
                    isServer: isServer,
                    responseSettings: responseSettings,
                    source: isIDE ? 'ide' : 'server',
                    user_devices: this.ctx.getMount(item.getScope()),
                    system_devices: this.ctx.getMount('system_devices'),
                    system_drivers: this.ctx.getMount('system_drivers'),
                    user_drivers: this.ctx.getMount('user_drivers'),
                    loggingFlags: loggingFlags,
                    toString: function () {
                        return item.getScope() + '://' + this.host + ':' + this.port + '@' + this.protocol;
                    }
                };

                result.hash = MD5(JSON.stringify({
                    host: host,
                    port: port,
                    protocol: protocol,
                    driverId: driverId,
                    driverScope: driverScope,
                    id: item.id,
                    devicePath: item.path,
                    deviceScope: item.getScope(),
                    source: isIDE ? 'ide' : 'server',
                    user_devices: this.ctx.getMount(item.getScope()),
                    system_devices: this.ctx.getMount('system_devices'),
                    system_drivers: this.ctx.getMount('system_drivers'),
                    user_drivers: this.ctx.getMount('user_drivers')
                }), 1);
                const userDirectory = this.ctx.getUserDirectory();
                if (userDirectory) {
                    result.userDirectory = userDirectory;
                }
            } else {
                _debug && console.error('cant find driver ' + driverId + ' for ' + item.toString());
            }
            item.info = result;
            if (! true  && hash && ! false ) {
                this._deviceInfoCache[hash] = result;
            }

            return result;
        },
        /**
         * Return device model item by device id
         * @param itemId
         * @returns {module:xcf/model/Device} The device
         */
        getItemById: function (itemId) {
            function search(store) {
                const data = store.data;
                const device = _.find(data, {
                    id: itemId
                });
                if (!device) {
                    return null;
                }
                return store.getSync(device.path);
            }

            for (const scope in this.stores) {
                const store = this.stores[scope];
                const result = search(store);
                if (result) {
                    return result;
                }
            }
            _debug && console.error('Device Manager::getItemById : cant find device with id: ' + itemId);
        },
        /**
         *
         * @param evt
         * @private
         */
        onStoreCreated: function (evt) {
            const thiz = this;
            const ctx = thiz.ctx;
            const type = evt.type;
            const store = evt.store;
            let items = store ? utils.queryStore(store, { isDir: false }) : [];
            const driverManager = this.ctx.getDriverManager();

            if (items && !_.isArray(items)) {
                items = [items];
            }

            if (type !== types.ITEM_TYPE.DEVICE) {
                return;
            }
            for (let i = 0; i < items.length; i++) {
                const item = store.getSync(items[i].path);
                if (!item) {
                    console.error('cant find ' + items[i].path);
                    continue;
                }
                if (item._completed) {
                    continue;
                }

                item._completed = true;
                item.isDevice = true;
                const driverId = this.getMetaValue(item, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                if (!driverId) {
                    console.error('device has no driver id!');
                    continue;
                }
                const driver = driverManager.getItemById(driverId);
                const CIS = item.user;

                //complete CIS
                _.each(CIS.inputs, function (ci) {
                    ci.device = item;
                    ci.actionTarget = isIDE ? ctx.mainView.getToolbar() : null;
                    ci.ctx = ctx;
                });
                if (!_.isEmpty(driver)) {
                    if (isIDE) {
                        this.completeDevice(item, driver);
                        item.iconClass = item.getStateIcon();
                    }
                }
            }
            all(this.connectToAllDevices()).then(function () {
                thiz.publish('DevicesConnected', evt);
            });
        },
        /**
         *
         * indirect callback for ON_SERVER_LOG_MESSAGE which tells the device server to
         * log something for us. This might be triggered by xblox/model/logging/Log
         *
         * @param evt {object}
         * @param evt.data {object}
         * @param evt.data.device {module:xide/types~DeviceInfo|null}
         * @param evt.data.details {array}
         * @param evt.data.time {integer}
         * @param evt.data.type {string}
         * @param evt.data.level {string}
         */
        onClientMessage: function (evt) {
            this.checkDeviceServerConnection();
            if (this.deviceServerClient) {
                _debugLogging && console.log('WRITE_LOG_MESSAGE ', evt);
                this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.WRITE_LOG_MESSAGE, evt);
            }
        },
        /**
         *
         * @param evt
         */
        onClientLogMessage: function (evt) {
            if (!isServer) {
                this.onClientMessage(evt);
            }
        },
        /**
         *
         * @param evt
         * @private
         */
        onVariableChanged: function (evt) {
            const variable = evt.item;
            const scope = evt.scope;
            const name = variable.name;
            const publish = evt.publish !== false;
            //_debugMQTT && console.log('DeviceManager/onVariableChanged/variable changed: '+name + ' - publish : ' + publish);

            if (name === 'value' || publish === false || !variable) {
                return;
            }

            const value = variable.value;
            const driver = scope.driver;
            const device = scope.device;

            if (device && device.info) {
                const deviceInfo = device.info;
                const mqttTopic = deviceInfo.host + '/' + deviceInfo.port + '/Variable/' + name;
                _debugMQTT && evt.publishMQTT !== false && console.log('send mqtt message ' + mqttTopic);
                evt.publishMQTT !== false && this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.MQTT_PUBLISH, {
                    topic: mqttTopic,
                    data: {
                        value: value,
                        device: deviceInfo
                    }
                });
            } else {
                _debugMQTT && console.warn('onVariableChanged->MQTT : have no device');
            }
            //_debugMQTT && console.info('on variable changed ' + device.toString());
        },
        /***
         * Common manager function, called by the context of the application
         * @private
         */
        init: function () {
            const thiz = this;
            if (this.initUI) {
                this.initUI();
            }
            this.stores = {};
            this._deviceInfoCache = {};
            this.subscribe(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, this.onVariableChanged);
            this.subscribe(types.EVENTS.ON_DEVICE_SERVER_CONNECTED, function () {
                const connect =  true  &&  true ;
                if (thiz.autoConnectDevices && connect) {
                    all(thiz.connectToAllDevices()).then(function () {
                        thiz.publish('DevicesConnected');
                    });
                }
            });
            this.subscribe([
                EVENTS.ON_NODE_SERVICE_STORE_READY,
                EVENTS.ON_MODULE_RELOADED,
                EVENTS.ON_DEVICE_DISCONNECTED,
                EVENTS.ON_DEVICE_CONNECTED,
                EVENTS.ON_CLIENT_LOG_MESSAGE,
                EVENTS.ON_STORE_CREATED
            ]);
            this.deviceInstances = this.consoles = {};
            this.driverScopes = {
                "system_drivers": "system_drivers/",
                "user_drivers": "user_drivers/"
            };
            this.lastUpTime = (new Date()).getTime();
            setInterval(function () {
                const current = (new Date()).getTime();
                if (current - thiz.lastUpTime > 30000) {
                    thiz.lastUpTime = (new Date()).getTime();
                }
                thiz.lastUpTime = current;
            }, 1000);

            this.initReload();

        },
        onDeviceServerConnected: function () {
            const self = this;
            if (this.deviceInstances) {
                _.each(this.deviceInstances, function (instance) {
                    if (instance && instance.device && !instance.domNode) {
                        const device = instance.device;
                        self.startDevice(device);
                    }
                });
            }
        },
        //nulled in server mode
        addDeviceInstance: function (device, driver) {
        },
        /***
         * ls is enumerating all drivers in a given scope
         * @param scope {string}
         * @param track {boolean=true}
         * @returns {Deferred}
         */
        ls: function (scope, track) {
            const dfd = new Deferred();

            function data(data) {
                try {
                    const store = this.createStore(data, scope, track);
                    //track !== false && this.setStore(scope, store);
                    this.onStoreReady(store);
                    track !== false && this.publish(types.EVENTS.ON_STORE_CREATED, {
                        data: data,
                        owner: this,
                        store: store,
                        type: this.itemType
                    });
                    dfd.resolve(store);

                } catch (e) {
                    logError(e, 'error ls drivers');
                }
            }

            if (this.prefetch && this.prefetch[scope]) {
                data.apply(this, [this.prefetch[scope]]);
                delete this.prefetch[scope];
                return dfd;
            }

            if (has('php')) {
                this.runDeferred(null, 'ls', [scope]).then(data.bind(this));
            } else {
                if (!isServer) {
                    this._getText(require.toUrl(scope).replace('main.js', '') + scope + '.json', {
                        sync: false,
                        handleAs: 'json'
                    }).then(data.bind(this));
                } else {
                    dfd.resolve({ items: [] });
                }
            }
            return dfd;
        },
        /**
         *
         * @param scope
         * @returns {*}
         */
        hasStore: function (scope) {
            return this.stores[scope];
        },
        /**
         * @param item {module:xcf/model/Device}
         * @private
         */
        fixDeviceCI: function (item) {
            const DEVICE_PROPERTY = types.DEVICE_PROPERTY;
            const meta = item['user'];
            const driverOptions = utils.getCIByChainAndName(meta, 0, DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            if (!driverOptions) {
                meta.inputs.push({
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'Common',
                    "id": DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS,
                    "name": DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS,
                    "order": 1,
                    "params": null,
                    "platform": null,
                    "title": "Driver Options",
                    "type": 5,
                    "uid": "-1",
                    "value": 0,
                    "data": [
                        {
                            value: 2,
                            label: 'Runs Server Side'
                        },
                        {
                            value: 4,
                            label: 'Show Debug Messages'
                        },/*,
                        {
                            value: 8,
                            label: 'Allow Multiple Device Connections'
                        },*/
                        {
                            value: 16,
                            label: 'Make Server'
                        }

                    ],
                    "visible": true,
                    "device": item
                });
            } else {
                driverOptions.data = [
                    {
                        value: 2,
                        label: 'Runs Server Side'
                    },
                    {
                        value: 4,
                        label: 'Show Debug Messages'
                    },/*
                    {
                        value: 8,
                        label: 'Allow Multiple Device Connections'
                    },*/
                    {
                        value: 16,
                        label: 'Make Server'
                    }

                ];

                driverOptions.group = 'Common';
            }


            const loggingFlags = utils.getCIByChainAndName(meta, 0, DEVICE_PROPERTY.CF_DEVICE_LOGGING_FLAGS);
            if (!loggingFlags) {
                meta.inputs.push({
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'Logging',
                    "id": DEVICE_PROPERTY.CF_DEVICE_LOGGING_FLAGS,
                    "name": DEVICE_PROPERTY.CF_DEVICE_LOGGING_FLAGS,
                    "order": 1,
                    "params": null,
                    "platform": null,
                    "title": "Logging Flags",
                    "type": DEVICE_PROPERTY.CF_DEVICE_LOGGING_FLAGS,
                    "uid": "-1",
                    "value": 0,
                    "data": [
                        {
                            value: 2,
                            label: 'On Connected'
                        },
                        {
                            value: 4,
                            label: 'On Disconnected'
                        },
                        {
                            value: 8,
                            label: 'On Error'
                        },
                        {
                            value: 16,
                            label: 'Commands'
                        },
                        {
                            value: 32,
                            label: 'Responses'
                        }
                    ],
                    "visible": true,
                    "device": item
                });
            } else {
                loggingFlags.group = "Logging";
            }


            const protocolCI = utils.getCIByChainAndName(meta, 0, DEVICE_PROPERTY.CF_DEVICE_PROTOCOL);
            if (protocolCI) {
                protocolCI.type = 3;
                protocolCI.options = [
                    {
                        label: "TCP",
                        value: "tcp"
                    },
                    {
                        label: "UDP",
                        value: "udp"
                    },
                    {
                        label: "Driver",
                        value: "driver"
                    },
                    {
                        label: "SSH",
                        value: "ssh"
                    },
                    {
                        label: "Serial",
                        value: "serial"
                    },
                    {
                        label: "MQTT",
                        value: "mqtt"
                    }
                ];
            }

            if (!item.id) {
                item.id = utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_ID);
            }

            const optionsCI = utils.getCIByChainAndName(meta, 0, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
            if (!optionsCI) {
                meta.inputs.push({
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'Network',
                    "id": "options",
                    "name": "options",
                    "order": 1,
                    "params": null,
                    "platform": null,
                    "title": "Options",
                    "type": 28,
                    "uid": "-1",
                    "value": {},
                    "visible": true,
                    "device": item

                });
            } else {
                optionsCI.device = item;
            }
        }
    });
});