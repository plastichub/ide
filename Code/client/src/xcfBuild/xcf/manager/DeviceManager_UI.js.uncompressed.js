/** @module xide/manager/DeviceManager_UI */
define("xcf/manager/DeviceManager_UI", [
    'dcl/dcl',
    "xdojo/has"/*NMD:Ignore*/,
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xaction/Action'/*NMD:Ignore*/,
    'xide/registry',
    'xlog/views/LogGrid'/*NMD:Ignore*/,
    'xcf/views/DriverConsole',
    'xide/views/_CIDialog',
    "dojo/Deferred",
    "xcf/views/ExpressionConsole",
    "xcf/widgets/LoggingWidget"/*NMD:Ignore*/,
    "xide/lodash",
    "dojo/_base/kernel",
    "dojo/_base/lang",
    "xide/$",
    "xide/console",
    "xide/interface/Route",
    "xide/mixins/ActionProvider",
    "xide/views/CIGroupedSettingsView"/*NMD:Ignore*/,
    'xide/data/Reference',
    'xide/data/ReferenceMapping',
    'xcf/views/DeviceTreeView',
    "xcf/model/DefaultDevice",
    'xide/views/_CIPanelDialog',
    'xcf/widgets/CommandPicker',
    "xdojo/has!xtrack?xide/interface/Track",
    "xide/noob"
], function (dcl, has, types, utils, factory, Action, registry, LogGrid,
    DriverConsole, _CIDialog, Deferred,
    ExpressionConsole, LoggingWidget, _, dojo, lang, $, console, Route, ActionProvider, CIGroupedSettingsView, Reference, ReferenceMapping, DeviceTreeView, DefaultDevice, _CIPanelDialog, CommandPicker, Track, noob) {

        window['xhas'] = has;
        const debug = false;
        const debugReferencing = false;
        const REFERENCE_CLASS = dcl([Reference, ReferenceMapping], {});
        let TRACKING_IMPL = null;

        if (Track) {
            TRACKING_IMPL = {
                track: function () {
                    return true;
                },
                getTrackingCategory: function () {
                    return utils.capitalize(this.beanNamespace);
                },
                getTrackingEvent: function () {
                    return types.ACTION.OPEN;
                },
                getTrackingLabel: function (item) {
                    return this.getMetaValue(item, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                },
                getActionTrackingUrl: function (command) {
                    return lang.replace(
                        this.breanScheme + '?action={action}&' + this.beanUrlPattern,
                        {
                            //id: item.id,
                            action: command
                        });
                },
                getTrackingUrl: function (item) {
                    return lang.replace(
                        this.breanScheme + '{view}/' + this.beanUrlPattern,
                        {
                            id: item.id,
                            view: 'settings'
                        });
                }
            };
        }

        /**
         *
         * @param label
         * @param flags
         * @param device
         * @param extra
         * @param level
         * @param popUpArgs
         */
        function processFlags(label, flags, device, extra, level, popUpArgs) {
            const LOGGING_FLAGS = types.LOGGING_FLAGS;
            if (extra) {
                extra = '<br/>' + extra;
            } else {
                extra = "";
            }
            //noinspection JSBitwiseOperatorUsage
            if (flags & LOGGING_FLAGS.STATUS_BAR) {
                this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                    text: label + ' : ' + device.info.toString() + ' <span class="text-warning">' + utils.replaceAll('<br/>', '', extra) + '</span>',
                    type: level || 'info'
                });
            }

            //noinspection JSBitwiseOperatorUsage
            if (flags & LOGGING_FLAGS.DEV_CONSOLE) {
                level = level || 'debug';
                let text = label + ' : ' + device.info.toString() + extra;
                text = text.split('<br/>').join('\n');
                if (console[level]) {
                    console[level](text, device);
                }

            }
            //noinspection JSBitwiseOperatorUsage
            if (flags & LOGGING_FLAGS.POPUP) {
                this.ctx.getNotificationManager().postMessage(utils.mixin({
                    message: label + ' : ' + device.info.title + ' ' + device.info.toString() + extra,
                    type: level || 'info',
                    showCloseButton: true,
                    duration: 3000
                }, popUpArgs));
            }
        }

        /**
         *
         * @param event
         * @param data
         * @param filter
         * @param output
         * @param extra
         * @returns {*}
         * @private
         */
        function _handler(event, data, filter, output, extra, level) {
            const OUTPUT = types.LOG_OUTPUT;
            const LOGGING_FLAGS = types.LOGGING_FLAGS;

            if (filter(data) === false) {
                return false;
            }

            const device = this.getDeviceStoreItem(data.device) || (data.device && data.device.info ? this.getDeviceStoreItem(data.device.info) : null);
            if (!device) {
                console.error('have no device ', data.device);
                return;
            }
            const deviceInfo = device.info || this.toDeviceControlInfo(device);

            let flags = deviceInfo.loggingFlags;
            flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};

            switch (output) {
                case OUTPUT.DEVICE_CONNECTED: {
                    return processFlags.apply(this, ['Device Connected', this._getLoggingFlags(OUTPUT.DEVICE_CONNECTED, LOGGING_FLAGS.GLOBAL_CONSOLE | LOGGING_FLAGS.POPUP | LOGGING_FLAGS.STATUS_BAR | LOGGING_FLAGS.DEVICE_CONSOLE, flags), device, extra, level]);
                }
                case OUTPUT.DEVICE_DISCONNECTED: {
                    if (data.error) {
                        extra = "Code: " + data.error.code;
                    }
                    return processFlags.apply(this, [
                        'Device Disconnected', this._getLoggingFlags(OUTPUT.DEVICE_DISCONNECTED, LOGGING_FLAGS.GLOBAL_CONSOLE | LOGGING_FLAGS.POPUP | LOGGING_FLAGS.STATUS_BAR | LOGGING_FLAGS.DEVICE_CONSOLE, flags), device, extra
                        , level,
                        device._userStopped ? {
                            actions: {
                                reconnect: {
                                    label: 'Abort Reconnect',
                                    action: function () {
                                        device._userStopped = true;
                                        this.cancel();
                                    }
                                }
                            }
                        } : {}
                    ]);
                }
                case OUTPUT.RESPONSE: {
                    return processFlags.apply(this, ['Device Message', this._getLoggingFlags(OUTPUT.RESPONSE, LOGGING_FLAGS.DEVICE_CONSOLE | LOGGING_FLAGS.GLOBAL_CONSOLE, flags), device, extra, level]);
                }
                case OUTPUT.SEND_COMMAND: {
                    return processFlags.apply(this, ['Send Command', this._getLoggingFlags(OUTPUT.SEND_COMMAND, LOGGING_FLAGS.DEVICE_CONSOLE | LOGGING_FLAGS.GLOBAL_CONSOLE, flags), device, extra, level]);
                }
            }
        }

        /**
         * @class module:xcf/manager/DeviceManager_UI
         * @augments module:xcf/manager/DeviceManager
         * @extends module:xide/mixins/ActionProvider
         * @lends {module:xide/interface/Route}
         */
        return dcl([Route.dcl, ActionProvider.dcl, Track ? dcl(Track.dcl, TRACKING_IMPL) : noob.dcl], {
            declaredClass: "xcf.manager.DeviceManager_UI",
            /**
             * Callback when we are connected to a device.
             * We use this to fire all looping blocks
             * @param driverInstance {module:xcf/driver/DriverBase} the instance of the driver
             * @param device {module:xcf/model/Device} the device model item
             * @param driver {module:xcf/model/Driver} the driver model item
             */
            onDeviceStarted: function (driverInstance, device, driver) {
                this.completeDevice(device, driver);
            },
            onDeviceDisconnected: function (data) {
                if (data && data.device) {
                    const device = this.getDeviceStoreItem(data.device);
                    if (!device) {
                        return;
                    }
                    const info = this.toDeviceControlInfo(device);
                    const driver = this.getContext().getDriverManager().getDriverById(info.driverId);
                    this.completeDevice(device, driver);
                }
            },
            /**
             *
             * @param device {module:xcf/model/Device}
             * @param driver {module:xcf/model/Driver}
             * @param subscribe {boolean=true}
             */
            completeDevice: function (device, driver, subscribe) {
                if (!device || !driver) {
                    return;
                }
                debugReferencing && console.log('completeDevice');
                const store = device._store;
                const parentId = device.path;
                const ctx = this.ctx;
                const isInstance = device.driverInstance;
                device.directory = true;
                device.isDevice = true;
                this.fixDeviceCI(device);

                const driverScope = device.getBlockScope() || driver.blockScope /*|| this.ctx.getBlockManager().getScope(driver.id)*/;
                if (!driverScope) {
                    return;
                }

                const blockStore = driverScope.blockStore;
                let variables;
                let commands;

                const commandsRoot = parentId + '_commands';
                utils.removeFromStore(store, commandsRoot, true, null, null, true, true);
                commands = device.commandsItem = store.putSync(
                    {
                        path: commandsRoot,
                        name: 'Commands',
                        isDir: true,
                        type: 'leaf',
                        parentId: parentId,
                        virtual: true
                    }, false
                );

                const variableRoot = parentId + '_variables';
                utils.removeFromStore(store, variableRoot, true, null, null, true, true);
                variables = device.variablesItem = store.putSync({
                    path: variableRoot,
                    name: 'Variables',
                    isDir: true,
                    type: 'leaf',
                    parentId: parentId,
                    virtual: true
                }, false);

                let commandBlocks = driverScope.getBlocks({
                    group: types.COMMAND_TYPES.BASIC_COMMAND
                });

                commandBlocks = commandBlocks.concat(driverScope.getBlocks({
                    group: types.COMMAND_TYPES.CONDITIONAL_COMMAND
                }));

                function createReference(block, driver, title, icon) {
                    const _isVariable = block.isVariable == true;
                    const _parent = _isVariable ? variables.path : commands.path;

                    if (block.isCommand !== true && block.isVariable !== true) {
                        return;
                    }
                    let reference = new Reference({
                        enabled: true,
                        path: _parent + '_reference_' + block.id,
                        name: title,
                        id: block.id,
                        parentId: _parent,
                        _mayHaveChildren: false,
                        virtual: true,
                        icon: icon,
                        command: !_isVariable,
                        ref: {
                            driver: driver,
                            item: block,
                            device: device
                        },
                        type: types.ITEM_TYPE.BLOCK
                    });
                    reference = store.putSync(reference, false);
                    block.addReference(reference, {
                        properties: {
                            "name": true,
                            "enabled": true,
                            "value": true
                        },
                        onDelete: true
                    }, true);
                }

                _.each(commandBlocks, function (block) {
                    createReference(block, driver, block.name, block.icon || 'fa-exclamation');
                });

                subscribe !== false && blockStore.on('added', function (block) {
                    createReference(block, driver, block.name, block.icon || 'fa-exclamation');
                });
                subscribe !== false && blockStore.on('delete', function (evt) {
                    store.removeSync(evt.id);
                    evt.target.refresh();
                });


                let basicVariables = driverScope.getVariables({
                    group: types.BLOCK_GROUPS.CF_DRIVER_BASIC_VARIABLES
                });
                basicVariables = basicVariables.concat(driverScope.getVariables({
                    group: types.BLOCK_GROUPS.CF_DRIVER_RESPONSE_VARIABLES
                }));

                _.each(basicVariables, function (variable) {
                    createReference(variable, driver, variable.name, variable.icon || 'fa-code');
                });

                ctx.getDriverManager().addDeviceInstance(device, driver);
            },
            /***
             * Add driver related items to each device store item.
             */
            completeDeviceStore: function (_store) {
                const store = _store || this.getStore();
                const items = utils.queryStore(store, {
                    isDir: false
                });
                try {
                    for (let i = 0; i < items.length; i++) {
                        const device = items[i];
                        if (device['_completed'] != null) {
                            continue;
                        }
                        device['_completed'] = [true];
                        const driverId = this.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                        const driver = this.ctx.getDriverManager().getItemById(driverId);
                        if (driver) {
                            this.completeDevice(device, driver);
                        }
                    }
                } catch (e) {
                    logError(e);
                }
            },
            onDriverModified: function () {
                debugReferencing && console.log('on driver modified', this.getStore());
                const store = this.getStore();

                const items = store ? utils.queryStore(store, {
                    isDir: false
                }) : [];

                const driverManager = this.ctx.getDriverManager();
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    const driverId = this.getMetaValue(item, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                    const driver = driverManager.getItemById(driverId);
                    
                    if (item.commandsItem) {
                        utils.removeFromStore(store, item.commandsItem, true, 'path', 'parentId');
                    }

                    if (item.variablesItem) {
                        utils.removeFromStore(store, item.variablesItem, true, 'path', 'parentId');
                    }
                    this.completeDevice(item, driver);
                }
            },
            /**
             * Impl. router interface
             * @param {module:xide/interface/RouteDefinition} route
             */
            handleRoute: function (route) {
                switch (route.parameters.view) {
                    case 'settings': {
                        return this.openItemSettings(this.getDeviceById(route.parameters.id));
                    }
                }
            },
            registerRoutes: function () {
                this.getContext().registerRoute({
                    id: 'Devices',
                    path: 'device://{view}/{id}',
                    schemes: ['device'],
                    delegate: this
                });
                this.getContext().registerRoute({
                    id: 'New Device',
                    path: 'File/New Device',
                    schemes: [''],
                    delegate: this
                });
            },
            /**
             * @param {module:xide/interface/RouteDefinition} route
             */
            getRouteRenderParams: function (route) {
                return {
                    icon: this.getDeviceStatusIcon(this.getDeviceById(route ? route.parameters.id : null))
                };
            },
            routeToReference: function (route) {
                return this.getDeviceById(route ? route.parameters.id : null);
            },
            /**
             *
             * @param item
             * @returns {*}
             */
            referenceToRoute: function (item) {
                return lang.replace(
                    this.breanScheme + '{view}/' + this.beanUrlPattern,
                    {
                        id: item.id,
                        view: 'settings'
                    });
            },
            /**
             * @param device
             * @param target
             * @returns {*}
             */
            cloneDevice: function (device, target, newName) {
                const dfd = new Deferred();
                const self = this;
                const ctx = this.ctx;
                const fileManager = ctx.getFileManager();
                const targetStore = this.getStore(target.scope);
                this.getFile(target).then(function (parentFolder) {
                    self.getFile(device).then(function (sourceDeviceFile) {
                        if (sourceDeviceFile) {
                            const srcParentFolder = sourceDeviceFile.getParent();
                            if (srcParentFolder) {
                                //var targetFileStore = parentFolder._store;
                                //neighbour items
                                //var items = targetFileStore.getChildren(parentFolder);
                                const useDialog = newName === null;
                                newName = newName || fileManager.getNewName(sourceDeviceFile, parentFolder.children);
                                newName = newName.replace('.meta.json', '');
                                function proceed(newName) {
                                    fileManager.getContent(sourceDeviceFile.mount, sourceDeviceFile.path, function (content) {
                                        // update meta data
                                        const cis = utils.getJson(content);
                                        const DEVICE_PROPERTY = types.DEVICE_PROPERTY;
                                        const meta = cis['inputs'];
                                        const idCI = utils.getCIByChainAndName(meta, 0, DEVICE_PROPERTY.CF_DEVICE_ID);
                                        utils.setCIValueByField(idCI, 'value', utils.createUUID());
                                        const titleCI = utils.getCIByChainAndName(meta, 0, DEVICE_PROPERTY.CF_DEVICE_TITLE);
                                        utils.setCIValueByField(titleCI, 'value', newName.replace('.meta.json', ''));
                                        const enabledCI = utils.getCIByChainAndName(meta, 0, DEVICE_PROPERTY.CF_DEVICE_ENABLED);
                                        utils.setCIValueByField(enabledCI, 'value', false);

                                        //store it as new file
                                        const newPath = parentFolder.path + '/' + newName + '.meta.json';
                                        fileManager.mkfile(parentFolder.mount, newPath).then(function () {
                                            fileManager.setContent(parentFolder.mount, newPath, JSON.stringify(cis, null, 2), function () {
                                                //create a temporary store and move it to the originating
                                                self.ls(target.scope, false).then(function (store) {
                                                    let _device = store.query({
                                                        id: idCI.value
                                                    })[0];

                                                    if (_device) {

                                                        //remove it from temp store
                                                        store.removeSync(_device.path);

                                                        //remove it from target store (wtf?)
                                                        target._store.removeSync(_device.path);

                                                        //set to the target's store
                                                        _device._store = targetStore;
                                                        //add it to the target store
                                                        _device = targetStore.addSync(_device);
                                                        _device = targetStore.getSync(_device.path);

                                                        //trigger some events
                                                        _device.refresh();

                                                        const CIS = _device.user;
                                                        //complete CIS
                                                        _.each(CIS.inputs, function (ci) {
                                                            ci.device = _device;
                                                            ci.actionTarget = ctx.mainView.getToolbar();
                                                            ci.ctx = ctx;
                                                        });
                                                        const driverId = self.getMetaValue(_device, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                                                        if (!driverId) {
                                                            console.error('device has no driver id!');
                                                        }
                                                        const driver = self.ctx.getDriverManager().getItemById(driverId);
                                                        if (driver) {
                                                            self.completeDevice(_device, driver);
                                                        }

                                                        targetStore.emit('added', {
                                                            target: _device
                                                        });

                                                        console.error('device added to ' + targetStore.id, targetStore);

                                                        dfd.resolve(_device);
                                                    }
                                                });
                                            });
                                        });
                                    }, false);
                                }
                                if (useDialog) {
                                    const cis = [
                                        utils.createCI('Title', 13, newName, {
                                            group: 'Common'
                                        })
                                    ];
                                    const actionDialog = new _CIDialog({
                                        title: 'New Name',
                                        resizable: true,
                                        onOk: function () {
                                            proceed(utils.getInputCIByName(cis, "Title").value);
                                        },
                                        delegate: {},
                                        cis: cis
                                    });
                                    actionDialog.show();
                                } else {
                                    proceed(newName);
                                }

                            }
                        } else {
                            dfd.reject("Cant get file object for device");
                        }
                    });
                });
                return dfd;
            },
            editDeviceSource: function () {
            },
            /**
             * Return the device's status icon
             * @param item {module:xcf/model/Device|null} The device
             * @returns {string} an icon class string
             */
            getDeviceStatusIcon: function (item) {
                if (item) {
                    const DEVICE_STATE = types.DEVICE_STATE;
                    switch (item.state) {
                        case DEVICE_STATE.DISCONNECTED: {
                            return 'fa-unlink iconStatusOff';
                        }
                        case DEVICE_STATE.READY:
                        case DEVICE_STATE.CONNECTED: {
                            return 'fa-link iconStatusOn';
                        }
                        case DEVICE_STATE.SYNCHRONIZING:
                        case DEVICE_STATE.CONNECTING: {
                            return 'fa-spinner fa-spin';
                        }
                    }
                }
                return 'fa-unlink iconStatusOff';
            },
            _getLoggingFlags: function (output, defaultFlags, flags) {
                return output in flags ? flags[output] : defaultFlags;
            },
            debounce: function (methodName, _function, delay, options, now, args) {
                return utils.debounce(this, methodName, _function, delay, options, now, args);
            },
            init: function () {
                const self = this;
                const EVENTS = types.EVENTS;
                const OUTPUT = types.LOG_OUTPUT;

                DeviceTreeView.prototype.delegate = this;
                DeviceTreeView.prototype.ctx = this.getContext();
                function extract(evt) {
                    const device = evt.device && evt.device.info ? evt.device : self.getDeviceStoreItem(evt.device);
                    const deviceInfo = device && device.info ? device.info : self.toDeviceControlInfo(device);
                    return {
                        device: device,
                        info: deviceInfo
                    };
                }
                function _onDeviceConnected(evt) {
                    _handler.apply(self, [EVENTS.ON_DEVICE_CONNECTED, evt, function (evt) {
                        const data = extract.apply(self, [evt]);
                        if (!data.device) {
                            return false;
                        }
                        return !(evt.isReplay === true && data.device.state === types.DEVICE_STATE.READY);

                    }, OUTPUT.DEVICE_CONNECTED, null]);
                }

                this.subscribe(EVENTS.ON_DEVICE_CONNECTED, function (evt) {
                    _onDeviceConnected.apply(self, [evt]);
                });

                function _onDeviceDisconnected(evt) {
                    _handler.apply(self, [EVENTS.ON_DEVICE_DISCONNECTED, evt, function (evt) {
                        const data = extract.apply(self, [evt]);
                        return data.device;
                    }, OUTPUT.DEVICE_DISCONNECTED, null, 'error']);
                }

                this.subscribe(EVENTS.ON_DEVICE_DISCONNECTED, function (evt) {
                    _onDeviceDisconnected.apply(self, [evt]);
                });

                this.subscribe(EVENTS.ON_DEVICE_MESSAGE, function (evt) {
                    _handler.apply(self, [EVENTS.ON_DEVICE_MESSAGE, evt, function (evt) {
                        const data = extract.apply(self, [evt]);
                        return !(!data.device || !evt.deviceMessage);
                    }, OUTPUT.RESPONSE, evt.deviceMessage]);
                });

                this.subscribe(EVENTS.ON_DEVICE_COMMAND, function (evt) {
                    _handler.apply(self, [EVENTS.ON_DEVICE_COMMAND, evt, function (evt) {
                        const data = extract.apply(self, [evt]);
                        return !(!data.device || !evt.name);
                    }, OUTPUT.SEND_COMMAND, "<b>Command:</b>" + evt.name, 'success']);
                });

                this.subscribe(EVENTS.ON_RENDER_WELCOME_GRID_GROUP);
                this.registerRoutes();
            },
            /**
             * @param evt {object}
             * @param evt.item {object}
             * @param evt.items {array|null}
             * @param evt.store {module:xide/data/_Base}
             */
            onRenderWelcomeGridGroup: function (evt) {
                const ctx = this.getContext();
                const trackingManager = ctx.getTrackingManager();
                const router = ctx.getRouter();
                const store = evt.store;
                const self = this;
                const grid = evt.grid;

                switch (evt.item.group) {
                    case 'New': {
                        const actions = [];
                        actions.push(this.createAction({
                            label: 'New Device',
                            command: 'File/New Device',
                            icon: 'fa-magic',
                            mixin: {
                                addPermission: true
                            }
                        }));
                        _.map(actions, function (item) {
                            return store.putSync(utils.mixin(item, {
                                url: item.command,
                                parent: 'New',
                                group: null,
                                label: item.title,
                                owner: self,
                                runAction: function (action, item, from) {
                                    return DeviceTreeView.prototype.runAction.apply(DeviceTreeView.prototype, [{ command: this.url }, item, from]);
                                }
                            }));
                        });
                        break;
                    }
                    case 'Recent': {
                        if (!trackingManager) {
                            break;
                        }
                        //head
                        store.putSync({
                            group: 'Devices',
                            label: 'Devices',
                            url: 'Devices',
                            parent: 'Recent',
                            directory: true
                        });

                        //collect recent
                        let devices = trackingManager.filterMin({
                            category: 'Device'
                        }, 'counter', 1);//the '2' means here that those items had to be opened at least 2 times

                        //sort along usage
                        devices = trackingManager.most(null, devices);
                        //make last as first
                        trackingManager.last(devices);

                        //map
                        devices = _.map(devices, function (item) {
                            item.parent = 'Recent';
                            item.icon = self.getRouteRenderParams(router.match({ path: item.url })).icon;
                            return item;
                        });

                        //@TODO: find a way to split impl. from interface
                        const deviceActions = _.map(DeviceTreeView.getActions.apply(DeviceTreeView.prototype, [$('<div/>')[0], DeviceTreeView.DefaultPermissions,
                            grid
                        ]), function (action) {
                            action.custom = true;
                            return action;
                        });
                        let deviceStoreItem = null;
                        _.each(devices, function (device) {
                            const route = router.match({ path: device.url });
                            if (!route) {
                                console.warn('cant get a router for ' + device.url);
                                return;
                            }
                            deviceStoreItem = self.routeToReference(route);
                            if (!deviceStoreItem) {
                                return;
                            }
                            store.removeSync(device.url, true);
                            let reference = new REFERENCE_CLASS({
                                url: device.url,
                                enabled: true,
                                label: device.label,
                                parent: 'Devices',
                                _mayHaveChildren: false,
                                virtual: true,
                                icon: device.icon,
                                owner: self,
                                mapping: {
                                    update: [
                                        {
                                            from: 'iconClass',
                                            to: 'icon'
                                        }
                                    ]
                                },
                                getActions: function () {
                                    return deviceActions;
                                },
                                runAction: function (action, item, from) {
                                    return DeviceTreeView.prototype.runAction.apply(DeviceTreeView.prototype, [action, item, from]);
                                }
                            });
                            reference = store.putSync(reference, false);
                            if (deviceStoreItem && deviceStoreItem.addReference) {
                                deviceStoreItem.addReference(reference, {
                                    properties: {
                                        "name": true,
                                        "enabled": true,
                                        "value": true,
                                        "state": true,
                                        "iconClass": true
                                    },
                                    onDelete: true,
                                    onChange: true
                                }, true);
                            } else {
                                console.error('device store item has no addReference!', reference);
                            }
                        });
                        store.emit('added', { target: deviceStoreItem });
                        break;
                    }
                }
            },
            initUI: function () {
                const thiz = this;
                const EVENTS = types.EVENTS;

                this.subscribe(types.EVENTS.ON_APP_READY, function (evt) {
                    thiz.onAppReady(evt);
                });
                this.subscribe([
                    EVENTS.ON_MAIN_VIEW_READY,
                    EVENTS.ON_DRIVER_MODIFIED,
                    EVENTS.ON_WIDGET_READY]);
                this.subscribe([types.EVENTS.ON_DEVICE_GROUP_SELECTED, types.EVENTS.ON_DEVICE_SELECTED], this.onItemSelected);
            },
            onCIUpdate: function (evt) {
                const ci = evt.ci;
                let device = ci ? ci.device : null;
                const oldValue = evt.oldValue;
                const newValue = evt.newValue;
                const state = device ? device.get('state') : -1;
                const driverManager = this.ctx.getDriverManager();
                let result = false;

                if (device) {
                    this.updateCI(ci, evt.newValue, evt.oldValue, device);
                    const prop = types.DEVICE_PROPERTY;
                    if (oldValue === newValue) {
                        return;
                    }


                    device.refresh();
                    this.toDeviceControlInfo(device);
                    device._store.emit('update', {});
                    switch (ci.id) {
                        case prop.CF_DEVICE_ENABLED: {
                            device.set('enabled', newValue);
                            if (newValue === true) {

                                switch (state) {
                                    //ignore
                                    case types.DEVICE_STATE.READY:
                                    case types.DEVICE_STATE.CONNECTED:
                                    case types.DEVICE_STATE.CONNECTING: {
                                        break;
                                    }
                                    case types.DEVICE_STATE.DISCONNECTED: {
                                        result = this.startDevice(device, false);
                                    }
                                }
                            } else {
                                switch (state) {
                                    case types.DEVICE_STATE.READY:
                                    case types.DEVICE_STATE.CONNECTED:
                                    case types.DEVICE_STATE.CONNECTING: {
                                        result = this.stopDevice(device);
                                    }
                                }
                            }
                            device.set('enabled', newValue);
                            break;
                        }
                        case prop.CF_DEVICE_DRIVER:
                        case prop.CF_DEVICE_HOST:
                        case prop.CF_DEVICE_PROTOCOL:
                        case prop.CF_DEVICE_PORT: {
                        }
                    }

                    if (ci.id == 'Driver') {
                        const driverOld = driverManager.getItemById(oldValue);
                        const driverStore = driverOld ? driverOld._store : null;
                        if (driverStore) {
                            const preInstanceId = driverOld.path + '_instances' + '_instance_' + device.path;
                            const preInstanceItem = driverStore.getSync(preInstanceId);
                            if (preInstanceItem) {
                                driverStore.removeSync(preInstanceId);
                                driverOld.refresh();
                            }
                        }
                        const driver = driverManager.getItemById(newValue);
                        this.completeDevice(device, driver, false);
                        const devi = device._store.getSync(device.path);
                        if (!devi) {
                            device = device._store.putSync(device);
                            debug && console.log('- device was removed');
                            device.refresh();
                        }
                        if (driver) {
                            driver.refresh();
                        }
                    }
                }
                if (evt.owner && evt.owner.delegate == this) {
                    if (evt.didUpdate) {
                        return evt.didUpdate;
                    }
                    evt.didUpdate = true;
                }
                return result;
            },
            /**
             * Refresh store from server
             */
            reload: function () {
            },
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  UX related functions, @TODO : move it to somewhere else
            //
            /////////////////////////////////////////////////////////////////////////////////////
            /**
             *
             * @param title
             * @param scope
             * @param parent
             * @returns {*|{name, isDir, parentId, path, beanType, scope}|{name: *, isDir: *, parentId: *, path: *, beanType: *, scope: *}}
             */
            createNewGroupItem: function (title, scope, parent) {
                return this.createItemStruct(title, scope, parent, title, true, types.ITEM_TYPE.DEVICE_GROUP);
            },
            /**
             *
             * @param options
             * @param readyCB
             * @param errorCB
             * @returns {*}
             * @private
             */
            createItem: function (options, readyCB) {
                return this.callMethodEx(null, 'createItem', [dojo.toJson(options)], readyCB, true);
            },
            /**
             *
             * @param title
             * @param scope
             * @param parent
             * @returns {*|{name, isDir, parentId, path, beanType, scope}|{name: *, isDir: *, parentId: *, path: *, beanType: *, scope: *}}
             */
            createNewItem: function (title, scope, parent) {
                return this.createItemStruct(title, scope, parent, parent + "/" + title, false, types.ITEM_TYPE.DEVICE);
            },
            onItemSelected: function (item) {
                this.currentItem = item;
            },
            onReloaded: function () {
                this.checkDeviceServerConnection();
            },
            openExpressionConsole: function (device) {
                const ctx = this.ctx;
                const deviceManager = this;
                const docker = ctx.mainView.getDocker();

                if (!device) {
                    return;
                }
                const cInfo = deviceManager.toDeviceControlInfo(device);

                const where = docker.addTab(null, {
                    title: cInfo.host,
                    icon: 'fa-terminal'
                });

                const value = "";
                const console = utils.addWidget(ExpressionConsole, {
                    ctx: ctx,
                    device: device,
                    value: value
                }, this, where, true);
                where.add(where);
                return console;
            },
            /**
             * Open a device console, it used the current selected device item from the tree
             * widget
             * @returns {widgetProto}
             */
            openConsole: function (device, where, mixin) {
                const enabled = this.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_ENABLED);
                if (!enabled) {
                    return null;
                }
                this.startDevice(device);
                const cInfo = this.toDeviceControlInfo(device);
                const hash = cInfo.hash;
                const viewId = hash + '-Console';
                const thiz = this;
                const deviceInstance = this.deviceInstances[hash];
                const driver = deviceInstance.driver;
                const docker = this.ctx.mainView.getDocker();
                const self = this;

                debug && console.log('open console ' + hash, cInfo);

                const driverInstance = this.getDriverInstance(cInfo, true);
                const scope = driverInstance ? driverInstance.blockScope : driver.blockScope;
                const instance = scope.instance;
                const originalScope = instance ? instance.blockScope : null;
                const blockScope = originalScope || scope;
                const startup = where == null;

                where = where || docker.addTab(null, {
                    title: cInfo.host,
                    icon: 'fa-terminal'
                });
                const ConsoleViewClass = dcl(DriverConsole, {
                    destroy: function () {
                        self.consoles[viewId].remove(this);
                    }
                });
                const args = utils.mixin({
                    driver: driver,
                    device: device,
                    deviceInstance: deviceInstance,
                    driverInstance: driverInstance,
                    blockScope: blockScope,
                    value: mixin ? mixin.value : ''
                }, mixin);

                const view = utils.addWidget(ConsoleViewClass, utils.mixin({
                    ctx: this.ctx,
                    type: 'javascript',
                    value: 'return this.getVariable(\'Volume\');',
                    editorArgs: args,
                    owner: this,
                    hasFlag: function (deviceInfo, output) {
                        if (!deviceInfo || !output) {
                            return true;
                        }

                        const LOGGING_FLAGS = types.LOGGING_FLAGS;
                        let flags = deviceInfo.loggingFlags;

                        flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};
                        const flag = flags[output];

                        if (flag == null) {
                            return false;
                        }
                        //noinspection JSBitwiseOperatorUsage
                        if (!(flag & LOGGING_FLAGS.DEVICE_CONSOLE)) {
                            return false;
                        }
                        return true;
                    },
                    log: function (msg, split, markHex, channel) {
                        msg = msg.string || msg;
                        const info = this.device.info;
                        if (this.hasFlag(info, channel) === false) {
                            return;
                        }
                        //var printTemplate = '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>';
                        let out = '';
                        if (_.isString(msg)) {
                            if (split !== false) {
                                out += msg.replace(/\n/g, '<br/>');
                            } else {
                                out = "" + msg;
                                if (markHex) {
                                    out = utils.markHex(out, '<span class="">', '</span>');
                                }
                            }
                        } else if (_.isObject(msg) || _.isArray(msg)) {
                            out += JSON.stringify(msg, null, true);
                        }
                        const items = out.split('<br/>');
                        for (let i = 0; i < items.length; i++) {
                            this.printCommand('', items[i], this.logTemplate, true);
                        }
                    },
                    onConsoleEnter: function (command) {
                        const consoleWidget = this.getConsole();
                        const thiz = this;
                        let failed = false;

                        function expressionError(message, error) {
                            _resolved = '<span class="text-primary"><b>' + command + '</span></b><span class="text-info">' + '  failed: <span class="text-danger">' + error.message + '</span>';
                            thiz.printCommand(_resolved, '');
                            failed = true;
                            return null;
                        }

                        const _consoleEditor = consoleWidget.getTextEditor();
                        const addConstants = _consoleEditor.getAction("Console/Settings/AddConstants").value;
                        const parse = _consoleEditor.getAction("Console/Settings/Expression").value;
                        var _resolved = parse ? this.parse(command, expressionError) : command;
                        const replaceHex = _consoleEditor.getAction("Console/Settings/ReplaceHEX").value;
                        if (replaceHex) {
                            _resolved = utils.replaceHex(_resolved);
                        }
                        if (failed) {
                            return null;
                        }
                        if (addConstants) {
                            _resolved = this.deviceInstance.prepareMessage(_resolved);
                        }

                        this.owner.sendDeviceCommand(this.deviceInstance, _resolved, null, null, false);
                        _resolved = utils.stringFromDecString(_resolved);

                        if (_resolved == command) {
                            _resolved = '';
                        } else {
                            _resolved = '<span class="text-info"><b>' + command + '</span></b><span>' + '  evaluates to <span class="text-success">' + _resolved + '</span>';
                        }
                        this.printCommand(_resolved, '');
                        return _resolved;
                    }

                }, args), null, where, startup);

                where.add(view);
                view.consoleId = viewId;
                if (!this.consoles) {
                    this.consoles = [];
                }
                if (!this.consoles[viewId]) {
                    this.consoles[viewId] = [];
                }
                this.consoles[viewId].push(view);
                view._on('destroy', function () {
                    thiz.consoles[viewId].remove(view);
                });
                return view;
            },
            _getDevices: function () {
                const store = this.getStore();
                let items = utils.queryStore(store, {
                    isDir: false
                });
                if (items._S) {
                    items = [items];
                }
                return items;
            },
            /***
             * Callback when a file widget has been created. We extend this widget for one more buttons:
             *
             * @param widget
             * @param ci
             * @param storeItem
             */
            onDriverClassFileWidgetCreated: function (widget, ci, storeItem) {
                const thiz = this;
                widget.debugButton = factory.createButton(
                    widget.getFreeExtensionSlot(),//the widget knows where to add new items
                    "el-icon-puzzle",             //eluisve icon class
                    "elusiveButton",              //elusive button class adjustments
                    "",                           //no label
                    "",                           //no extra dom style markup
                    function () {                   //button click callback
                        thiz.debugDriver(ci, storeItem);
                    },
                    this);

                widget.editButton = factory.createButton(
                    widget.getFreeExtensionSlot(),//the widget knows where to add new items
                    "el-icon-file-edit",             //eluisve icon class
                    "elusiveButton",              //elusive button class adjustments
                    "",                           //no label
                    "",                           //no extra dom style markup
                    function () {                   //button click callback
                        thiz.editDriver(ci, storeItem);
                    },
                    this);
            },
            /**
             * Somebody opened a widget
             * @param evt
             */
            onWidgetReady: function (evt) {
                if (evt['owner'] == this                   // must come from us
                    && evt['ci'] != null                   // we need an CI
                    && utils.toString(evt['ci'].name) === types.DRIVER_PROPERTY.CF_DRIVER_CLASS    // we need an CI
                    && evt['widget'] != null               // we need a valid widget
                    && evt['storeItem'] != null            // we need a store reference
                    && utils.toInt(evt['ci'].type) === types.ECIType.FILE) //must be a file widget
                {
                    this.onDriverClassFileWidgetCreated(evt.widget, evt.ci, evt.storeItem);
                }
            },
            _newItem: function (cis) {
                const thiz = this;
                const nameCi = utils.getInputCIByName(cis, "Title");
                const scopeCi = utils.getInputCIByName(cis, "Scope");
                const groupCi = utils.getInputCIByName(cis, "In Group");
                if (_.isObject(groupCi.value)) {
                    groupCi.value = groupCi.value.path;
                }

                const options = utils.toOptions(cis);
                const dfd = new Deferred();
                thiz.createItem(options, function (newItemData) {
                    const newItem = thiz.createNewItem(nameCi.value, scopeCi.value, groupCi.value);
                    const store = thiz.getStore(scopeCi.value);
                    newItem.path += '.meta.json';
                    newItem.user = {
                        inputs: newItemData
                    };
                    const device = store.putSync(newItem);
                    _.each(device.user.inputs, function (ci) {
                        ci.device = device;
                    });

                    thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                        owner: thiz,
                        store: store,
                        action: types.NEW_FILE,
                        item: device
                    });
                    device.refresh();
                    dfd.resolve(device);
                });
                return dfd;
            },
            newItem: function (currentItem) {
                if (!currentItem) {
                    currentItem = {
                        path: ""
                    };
                }
                const parent = currentItem ? currentItem.isDir === true ? currentItem.path : '' : '';
                const parentScope = currentItem.scope || "user_devices";

                const ctx = this.getContext();
                const self = this;

                function transfer(changedCIS, prop, newDevice) {
                    _.find(changedCIS, { name: prop }) && newDevice.setMetaValue(prop, utils.getCIInputValueByName(changedCIS, prop));
                }

                function complete(newDevice, cis, changedCIS, driver) {
                    const DEVICE_PROPERTY = types.DEVICE_PROPERTY;
                    _.each([DEVICE_PROPERTY.CF_DEVICE_HOST,
                    DEVICE_PROPERTY.CF_DEVICE_PORT,
                    DEVICE_PROPERTY.CF_DEVICE_PROTOCOL,
                    DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS,
                    DEVICE_PROPERTY.CF_DEVICE_OPTIONS,
                    DEVICE_PROPERTY.CF_DEVICE_LOGGING_FLAGS,
                    DEVICE_PROPERTY.CF_DEVICE_ENABLED], function (prop) {
                        transfer(changedCIS, prop, newDevice);
                    });

                    if (driver) {
                        const driverCI = utils.getCIInputValueByName(cis, DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                        driverCI.group = "Common";
                        driverCI.enumType = "Driver";
                        newDevice.setMetaValue(DEVICE_PROPERTY.CF_DEVICE_DRIVER, driver.id);
                    }

                    newDevice.id = utils.getCIInputValueByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_ID);
                    const optionsCI = utils.getInputCIById(cis, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
                    const optionsCI2 = utils.getInputCIById(changedCIS, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
                    if (!optionsCI && optionsCI2) {
                        const ci = utils.createCI(DEVICE_PROPERTY.CF_DEVICE_OPTIONS, types.ECIType.JSON_DATA, "{}", {
                            group: 'Network'
                        });
                        //newDevice.user.inputs.push(ci);
                        //newDevice.setMetaValue()
                    }
                    if ( true ) {
                        ctx.getTrackingManager().track(
                            self.getTrackingCategory(),
                            self.getTrackingLabel(newDevice),
                            self.getTrackingUrl(newDevice),
                            types.ACTION.OPEN,
                            self.getContext().getUserDirectory()
                        );
                    }

                }
                function _createNewDevice(cis, changedCIS, driver) {
                    const targetCI = utils.getInputCIByName(cis, 'In Group');
                    const titleCI = utils.getInputCIByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                    const device = utils.getInputCIByName(cis, 'copyFrom');
                    const dfd = new Deferred();
                    const deviceManager = ctx.getDeviceManager();
                    if (device && _.isObject(device.value)) {
                        //clone device wants parent to be a model
                        if (_.isString(targetCI.value)) {
                            targetCI.value = deviceManager.getItemByPath(targetCI.value);
                        }
                        deviceManager.cloneDevice(device.value, targetCI.value, titleCI.value).then(function (newDevice) {
                            complete(newDevice, cis, changedCIS, driver);
                            dfd.resolve(newDevice);
                        });
                    } else {
                        deviceManager._newItem(cis).then(function (newDevice) {
                            complete(newDevice, cis, changedCIS, driver);
                            dfd.resolve(newDevice);
                        });
                    }
                    return dfd;
                }
                function createNewDevice(cis, changedCIS) {

                    const deviceManager = ctx.getDeviceManager();
                    const driverManager = ctx.getDriverManager();
                    const scope = utils.getCIInputValueByName(cis, 'scope');
                    const dfd = new Deferred();
                    function createDriver(parent, title) {
                        return driverManager.newItem(parent, title);
                    }
                    function getDriverGroup(scope, path, newTitle) {
                        const dfd = new Deferred();
                        const driverParentPathObject = driverManager.getItemByPath(path);
                        if (driverParentPathObject) {
                            dfd.resolve(driverParentPathObject)
                        } else {
                            const newDriverScope = scope === 'user_devices' ? 'user_drivers' : 'system_drivers';
                            driverManager.newGroup(null, newDriverScope, newTitle).then(function (newDriverGroup) {
                                dfd.resolve(newDriverGroup);
                            });
                        }
                        return dfd;
                    }
                    switch (utils.getCIInputValueByName(cis, 'Driver Creation')) {
                        case 'auto-create': {
                            const targetCI = utils.getInputCIByName(cis, 'In Group');
                            const titleCI = utils.getInputCIByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            if (_.isString(targetCI.value)) {
                                targetCI.value = deviceManager.getItemByPath(targetCI.value);
                            }
                            const root = targetCI.value ? targetCI.value.name : "";
                            getDriverGroup(scope, root, root).then(function (driverParent) {
                                createDriver(driverParent, titleCI.value).then(function (driver) {
                                    _createNewDevice(cis, changedCIS, driver).then(function (device) {
                                        dfd.resolve(device);
                                    });
                                });
                            });
                            return dfd;
                        }
                        default: {
                            _createNewDevice(cis, changedCIS, null).then(function (device) {
                                dfd.resolve(device);
                            });
                            return dfd;
                        }
                    }
                }

                const dfd = new Deferred();

                let cis = DefaultDevice(ctx);
                const optionsCI = utils.getInputCIByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
                optionsCI.type = types.ECIType.STRUCTURE;
                //optionsCI.name = 'Device Options';
                cis = cis.concat([
                    utils.createCI('Driver Creation', types.ECIType.ENUMERATION, "auto-create", {
                        group: 'Driver',
                        visible: true,
                        description: "Specify the way how the device's driver is created",
                        value: 'auto-create',
                        options: [
                            {
                                label: 'Create new driver',
                                value: 'auto-create',
                                description: "Create a driver with same name"
                            },
                            /*,
                             {
                             label: 'Copy driver',
                             value: 'copy',
                             description: "Copy the driver selected above"
                             },
                             */
                            {
                                label: 'None',
                                value: 'none',
                                description: "Don`t create any driver"
                            }
                        ]
                    }),
                    utils.createCI('copyFrom', CommandPicker, '', {
                        group: 'Common',
                        title: 'Copy from',
                        options: [],
                        pickerType: 'device',
                        value: ""
                    }),
                    utils.createCI('In Group', CommandPicker, parent, {
                        group: 'Common',
                        title: 'In Group',
                        options: [],
                        pickerType: 'group',
                        value: parent
                    }),
                    utils.createCI('Scope', types.ECIType.ENUMERATION, parentScope, {
                        group: 'Common',
                        value: parentScope,
                        options: [
                            {
                                label: 'System Devices',
                                value: 'system_devices'
                            },
                            {
                                label: 'User Devices',
                                value: 'user_devices'
                            }
                        ]
                    })
                ]);

                cis.reverse();

                const panel = new _CIPanelDialog({
                    title: 'New Device',
                    onOk: function (changedCIS) {
                        changedCIS = _.map(changedCIS, function (obj) {
                            return obj.ci;
                        });
                        createNewDevice(cis, changedCIS).then(function (newDevice) {
                            dfd.resolve(newDevice);
                        });

                        this.headDfd.resolve(changedCIS);
                    },
                    onCancel: function (changedCIS) {
                        this.headDfd.resolve(changedCIS);
                    },
                    cis: cis,
                    CIViewOptions: {
                        delegate: this,
                        ciSort: false,
                        cis: cis
                    }
                });
                panel.show();

                return dfd;
            },
            /**
             * Bean UX function to create a new item by opening a dialog, followed by a server call
             */
            newItem_old: function (currentItem, scope) {
                const thiz = this;
                if (!currentItem) {
                    currentItem = {
                        path: ""
                    };
                }
                const drivers = this.ctx.getDriverManager().getDriversAsEnumeration();
                const parent = currentItem ? currentItem.isDir === true ? currentItem.path : '' : '';
                const dfd = new Deferred();
                const cis = [
                    utils.createCI('In Group', 13, parent, {
                        widget: {
                            disabled: true
                        },
                        group: 'Common',
                        visible: false
                    }),
                    utils.createCI('Title', 13, 'MyNewDevice', {
                        group: 'Common'
                    }),
                    utils.createCI('Enabled', 0, '', {
                        group: 'Common',
                        value: false
                    }),
                    utils.createCI('Scope', 3, scope, {
                        group: 'Common',
                        options: [
                            {
                                label: 'System',
                                value: 'system_devices'
                            },
                            {
                                label: 'User',
                                value: 'user_devices'
                            }
                        ]
                    }),
                    utils.createCI('Driver', 3, drivers.length > 0 ? drivers[0].value : '', {
                        group: 'Common',
                        options: drivers,
                        name: types.DEVICE_PROPERTY.CF_DEVICE_DRIVER,
                        enumType: 'Driver'
                    }),
                    utils.createCI('Network-Settings', types.ECIType.DEVICE_NETWORK_SETTINGS, 'no value')
                ];

                const actionDialog = new _CIDialog({
                    title: 'New Device',
                    resizable: true,
                    onOk: function (data) {
                        const idCi = utils.createCI('Id', 13, utils.createUUID(), {
                            visible: false,
                            group: "Common"
                        });
                        data.push(idCi);
                        const options = utils.toOptions(data);
                        const nameCi = utils.getInputCIByName(cis, "Title");
                        const scopeCi = utils.getInputCIByName(cis, "Scope");

                        thiz.createItem(options, function (newItemData) {
                            const newItem = thiz.createNewItem(nameCi.value, scopeCi.value, parent);
                            const store = thiz.getStore(scopeCi.value);
                            newItem.path += '.meta.json';
                            newItem.user = {
                                inputs: newItemData
                            };
                            newItem.id = idCi.value;

                            const device = store.putSync(newItem);
                            _.each(device.user.inputs, function (ci) {
                                ci.device = device;
                            });

                            thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                owner: thiz,
                                store: store,
                                action: types.NEW_FILE,
                                item: device
                            });
                            device.refresh();
                            dfd.resolve(device);
                        });
                    },
                    delegate: {},
                    cis: cis
                });

                actionDialog.show();

                return dfd;
            },
            newGroup: function (item, scope) {
                const thiz = this;

                //var currentItem = this.getItem();
                //var parent = currentItem ? utils.toBoolean(currentItem.isDir) === true ? utils.toString(currentItem.path) : '' : '';
                const parent = '';
                const store = this.getStore(scope);
                const dfd = new Deferred();
                const actionDialog = new _CIDialog({
                    title: 'New Device Group',
                    onOk: function () {
                        const title = this.getField('Title');
                        const scope = this.getField('Scope');
                        const _final = parent + '/' + title;
                        thiz.createGroup(scope, _final, function () {
                            let newItem = thiz.createNewGroupItem(title, scope, parent);
                            newItem = store.putSync(newItem);
                            thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                owner: thiz,
                                store: store,
                                action: types.NEW_DIRECTORY,
                                item: newItem
                            });
                            store.refreshItem(newItem);
                            dfd.resolve(newItem);
                        });
                    },
                    delegate: {},
                    cis: [
                        utils.createCI('Title', 13, ''),
                        utils.createCI('Scope', 3, scope, {
                            "options": [
                                {
                                    label: 'System',
                                    value: 'system_devices'
                                },
                                {
                                    label: 'User',
                                    value: 'user_devices'
                                }

                            ]
                        })
                    ]
                });
                actionDialog.show();
                return dfd;
            },
            onDeleteItem: function (item) {
                const isDir = utils.toBoolean(item.isDir) === true;
                const name = utils.toString(item.name);
                const removeFn = isDir ? 'removeGroup' : 'removeItem';
                const thiz = this;
                const actionDialog = new _CIDialog({
                    title: 'Remove Device' + (isDir ? ' Group' : '') + ' ' + '\"' + name + '\"',
                    style: 'max-width:400px',
                    titleBarClass: 'text-danger',
                    onOk: function () {
                        const store = item._store || thiz.getStore(item.scope);
                        thiz[removeFn](
                            utils.toString(item.scope),
                            utils.toString(item.path),
                            utils.toString(item.name),
                            function () {
                                store.removeSync(item.path);
                                store.emit('delete', {
                                    target: item
                                });
                                thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                    owner: thiz,
                                    store: store,
                                    action: types.DELETE,
                                    item: item
                                });
                            });
                    },
                    delegate: {
                        isRemoving: false

                    },
                    inserts: [{
                        query: '.dijitDialogPaneContent',
                        insert: '<div><span class="fileManagerDialogText">Do you really want to remove  this item' + '?</span></div>',
                        place: 'first'
                    }]
                });
                actionDialog.show();
            },
            /***
             * openItemSettings creates a new settings view for a driver
             * @param item
             * @returns {module:xide/views/CIGroupedSettingsView}
             */
            openItemSettings: function (item) {
                const self = this;
                const userData = item.user;
                if (!userData || !userData.inputs) {
                    return null;
                }
                const viewId = this.getViewId(item);
                let view = registry.byId(viewId);
                try {
                    if (view) {
                        if (view._parent) {
                            view._parent.select(view);
                        }
                        return view;
                    }
                } catch (e) {
                    utils.destroy(view);
                }


                const docker = this.getContext().getMainView().getDocker();
                const parent = docker.addTab(null, {
                    title: utils.toString(item.name),
                    icon: this.beanIconClass
                });

                const title = this.getMetaValue(item, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                const meta = item.user;
                this.fixDeviceCI(item);
                const hostCI = utils.getCIByChainAndName(meta, 0, types.DEVICE_PROPERTY.CF_DEVICE_HOST);
                const protocolCI = utils.getCIByChainAndName(meta, 0, types.DEVICE_PROPERTY.CF_DEVICE_PROTOCOL);
                const optionsCI = utils.getInputCIByName(userData, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
                optionsCI.type = types.ECIType.STRUCTURE;

                view = utils.addWidget(this.getViewClass(
                    //tracking code
                    {
                        track: function () {
                            return true;
                        },
                        getTrackingCategory: function () {
                            return utils.capitalize(self.beanNamespace);
                        },
                        getTrackingEvent: function () {
                            return types.ACTION.OPEN;
                        },
                        getTrackingLabel: function () {
                            return self.getMetaValue(this.storeItem, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                        },
                        getActionTrackingUrl: function (command) {
                            return lang.replace(
                                self.breanScheme + '?action={action}&' + self.beanUrlPattern,
                                {
                                    id: item.id,
                                    action: command
                                });
                        },
                        getTrackingUrl: function (item) {
                            return lang.replace(
                                self.breanScheme + '{view}/' + self.beanUrlPattern,
                                {
                                    id: item.id,
                                    view: 'settings'
                                });
                        }
                    }), {
                        title: title || utils.toString(item.name),
                        cis: userData.inputs,
                        //@TODO: remove back compat
                        storeItem: item,
                        item: item,
                        id: this.getViewId(item),
                        iconClass: this.beanIconClass + ' ' + this.getDeviceStatusIcon(item),
                        delegate: this,
                        storeDelegate: this,
                        closable: true,
                        owner: this,
                        ctx: this.getContext()
                    }, this, parent, false, this.itemType + '_View');


                view.startup();
                return view;
            },
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Bean protocol impl.
            //
            /////////////////////////////////////////////////////////////////////////////////////
            _getIcon: function (item) {
                const state = item.state;
                const DEVICE_STATE = types.DEVICE_STATE;
                switch (state) {
                    case DEVICE_STATE.CONNECTING:
                        return 'iconStatusBusy';
                    case DEVICE_STATE.CONNECTED:
                        return 'fa-signal';
                    case DEVICE_STATE.DISABLED:
                    case DEVICE_STATE.DISCONNECTED:
                        return 'fa-dot-circle-o';
                }
                return 'fa-dot-circle-o';
            }
        });
    });
