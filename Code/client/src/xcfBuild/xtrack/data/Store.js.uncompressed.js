/**
 * @module xtrack/data/Store
 */
define("xtrack/data/Store", [
    'xdojo/declare',
    'xide/utils',
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable'
], function (declare, utils, TreeMemory, ObservableStore, Trackable) {

    /**
     * @class module:xtrack/data/Store
     * @extends module:xide/data/TreeMemory
     * @extends module:xide/data/ObservableStore
     * @extends module:dstore/Trackable
     */
    return declare('xtrack.data.TrackingStore', [TreeMemory, Trackable, ObservableStore], {
        parentField:'parent',
        idProperty:'url',
        /**
         * Override dstore/Memory for removing child tracks as well.
         * @param url {string}
         * @returns {boolean}
         */
        removeSync:function(url){
            var storeItem = this.getSync(url);
            storeItem && utils.removeSync(this,storeItem,url,'parent');
            return this.inherited(arguments);
        }
    });
});
