//>>built
define("xtrack/data/Store",["xdojo/declare","xide/utils","xide/data/TreeMemory","xide/data/ObservableStore","dstore/Trackable"],function(c,d,e,f,g){return c("xtrack.data.TrackingStore",[e,g,f],{parentField:"parent",idProperty:"url",removeSync:function(a){var b=this.getSync(a);b&&d.removeSync(this,b,a,"parent");return this.inherited(arguments)}})});
//# sourceMappingURL=Store.js.map