/**
 * @module xtrack/model/Item
 */
define("xtrack/model/Item", [
    'xdojo/declare',
    'xide/views/History',
    "xide/data/Model"
], function (declare, History, Model) {

    /**
     * A serializable tracking model.
     * @class: module:xtrack/model/Track
     * @example:
     * {
        "id": "3f903349-faf4-2eaf-4b99-d8ba225e4643",
        "time": 1480157185439,
        "url": "device://?view=settings&item=e5a06e24-6aa4-c8c5-3ffc-9d84d8528a91",
        "label": "Device",
        "category": "Marantz",
        "command": "File/Open",
        "context": "/PMaster/projects/x4mm/user/",
        "type": "added",
        "counter": 1,
        "_history": {
          "_history": [
            1480157202248 <--- time stamp
          ],
          "_index": 1
        }
      }
     */
    return declare('TrackingModel', Model, {
        /**
         * An ui friendly label.
         * @type {string}
         */
        label: null,
        /** Additional filter vector.
         * @type {string|null}
         */
        context: null,
        /**
         * An ui friendly category. We may want to group tracking along this vector.
         * @type {string}
         */
        category: null,
        /**
         * An unique url.
         * @type {url}
         */
        url: null,
        /**
         * An unique url to the parent's tracking url.
         * @type {url}
         */
        parent: null,
        /**
         * A random uuid.
         * @type {string}
         */
        id: '',
        /**
         * We track the action origin so we're able to determine more frequent used actions.
         * @type {string|null}
         */
        command: null,
        /**
         * The time of usage.
         * @type {number}
         */
        time: null,
        /**
         * The counter of usage.
         * @type {number}
         */
        counter: 1,
        /**
         * To track the usage in a time table in order to determine 'frequent' actions.
         * @type {module:xide/views/History}
         */
        _history: null,
        /**
         * Retain usage counter and track time of usage.
         * @returns {number}
         */
        retain: function () {
            this.counter++;
            this.time = new Date().getTime();
            this.getHistory().push(this.time);
        },
        /**
         *
         * @returns {module:xide/views/History}
         */
        getHistory: function () {
            //not yet fully deserialized
            if(this._history && !this._history.push){
                this._history = new History({_history:this._history._history || [] });
            }
            if (!this._history) {
                this._history = new History();
            }
            return this._history;
        },
        destroy:function(){
            this._history && this._history.destroy() && (this._history = null);
        }
    });
});
