/**
 * @module xtrack/component
 */
define("xtrack/component", [
    "dcl/dcl",
    "xide/model/Component",
    "require",
    "dojo/Deferred"
], function (dcl,Component,require,Deferred) {
    /**
     * @class xtrack.component
     * @extends module:xide/model/Component
     * @inheritDoc
     */
    return dcl(Component, {
        /**
         * @inheritDoc
         */
        beanType:'TRACK',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        getDependencies:function(){
            return [
                "xtrack/manager/TrackingManager"
            ];
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return this.beanType;
        },
        /**
         * @param ctx {module:xide/manager/ContextBase}
         * @inheritDoc
         */
        run:function(ctx){
            var _re = require;
            var head = new Deferred();
            _re(this.getDependencies(),function(TrackingManager){
                ctx.trackingManager = ctx.createManager(TrackingManager);
                var _d = ctx.trackingManager._init();
                _d.then(function(){
                    head.resolve();
                });
            });
            return head;
        }
    });
});