/** @module xaction/ActionStore **/
define("xaction/ActionStore", [
    "xdojo/declare",
    'xide/data/TreeMemory',
    'xide/utils',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'xaction/ActionModel'
], function (declare, TreeMemory, utils, ObservableStore, Trackable, ActionModel) {
    /**
     * Default properties to be observed (in ObservableStore)
     * @type {string[]}
     */
    const DEFAULT_ACTION_PROPERTIES = [
        "value",
        "icon",
        "disabled",
        "enabled"
    ];

    /**
     * Default factory
     * @param composer
     * @param bases
     * @param Model
     * @param defaults
     * @param mixin
     * @returns {*}
     */
    function createClass(composer, bases, Model, defaults, mixin) {
        /**
         * @class module:xaction/ActionStore
         */
        return (composer || declare)(bases || [TreeMemory, Trackable, ObservableStore], utils.mixin({
            idProperty: 'command',
            declaredClass: "xaction/ActionStore",
            Model: Model || ActionModel,
            renderers: null,
            observedProperties: defaults || DEFAULT_ACTION_PROPERTIES,
            getAll: function () {
                return this.data;
            },
            addRenderer: function (renderer) {
                !this.renderers && (this.renderers = []);
                !_.includes(this.renderers, renderer) && this.renderers.push(renderer);
            }
        }, mixin));
    }

    const Module = createClass(null, null, null, null, null);
    Module.createDefault = args => new Module(args);
    Module.createClass = createClass;
    Module.DEFAULT_ACTION_PROPERTIES = DEFAULT_ACTION_PROPERTIES;
    return Module;
});