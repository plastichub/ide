define("xaction/ActionProvider", [
    "xdojo/declare",
    'dcl/dcl',
    "xide/types",
    "xide/utils",
    "xide/model/Path",
    'xaction/ActionStore',
    'xaction/Action',
    'xide/Keyboard',
    'xide/mixins/EventedMixin',
    'xaction/DefaultActions',
    'xide/lodash'
], function (
    declare,
    dcl,
    types,
    utils,
    Path,
    ActionStore,
    Action,
    Keyboard,
    EventedMixin,
    DefaultActions,
    _
) {

    const Implementation = {
        /**
         * @type module:xaction/ActionStore
         */
        actionStore: null,
        actions: null,
        allowActionOverride: true,
        sortGroups: function (groups, groupMap) {
            groups = groups.sort((a, b) => {
                if (groupMap[a] != null && groupMap[b] != null) {
                    const orderA = groupMap[a];
                    const orderB = groupMap[b];
                    return orderB - orderA;
                }
                return 100;
            });
            return groups;
        },
        getItemsAtBranch: function (items, path) {
            return new Path(path).getChildren(_.map(items, 'command'), false);
        },
        /////////////////////////////////////////////////////
        //
        //  Store Based Extension -
        //
        /////////////////////////////////////////////////////
        /**
         * Update all actions referencing widgets
         */
        refreshActions: function () {
            const allActions = this.getActions();
            for (let i = 0; i < allActions.length; i++) {
                const action = allActions[i];
                if (action.refresh) {
                    action.refresh();
                }
            }
        },
        getAction: function (mixed) {
            if (_.isString(mixed)) {
                return this.getActionStore().getSync(mixed);
            }
            return mixed;
        },
        clearActions: function () {
            const store = this.getActionStore();
            const actions = store ? store.query() : [];

            _.each(actions, action => {
                action && store.removeSync(action.command);
            });
            store && store.setData([]);
        },
        destroy: function () {
            this.clearActions();
            return this.inherited(arguments);
        },
        /**
         *
         * @param title
         * @param command
         * @param group
         * @param icon
         * @param handler
         * @param accelKey
         * @param keyCombo
         * @param keyProfile
         * @param keyTarget
         * @param keyScope
         * @param mixin
         * @returns {xaction/Action}
         */
        __createAction: function (title, command, group, icon, handler, accelKey, keyCombo, keyProfile, keyTarget, keyScope, mixin) {
            icon = icon || types.ACTION_ICON[command];
            const args = {
                accelKey: accelKey
            };
            utils.mixin(args, mixin);
            const action = Action.createDefault(title, icon, command, group, handler, args);
            if (keyCombo) {
                let keyboardMappings;
                if (this.keyboardMappings) {
                    keyboardMappings = this.keyboardMappings;
                } else {
                    action.keyboardMappings = keyboardMappings = [];
                }
                let mapping = Keyboard.defaultMapping(keyCombo, handler, keyProfile || types.KEYBOARD_PROFILE.DEFAULT, keyTarget, keyScope, [action]);
                mapping = this.registerKeyboardMapping(mapping);
                keyboardMappings.push(mapping);
                action.keyboardMappings = keyboardMappings;
            }
            return action;
        },
        updateAction: function (action, what, value) {
            action = action || this.getAction(action);
            if (action) {
                action.set(what, value);
                setTimeout(() => {
                    action.getReferences().forEach(ref => {
                        ref.set(what, value);
                    });
                }, 100);
            }
        },
        _completeActions: function (actions) {
            const result = [];
            const keyTarget = this.getKeyTarget ? this.getKeyTarget() : null;
            for (let i = 0; i < actions.length; i++) {
                const config = actions[i];
                let action;

                if (!config) {
                    continue;
                }

                if (!(config instanceof Action)) {
                    action = this.__createAction(
                        config.title,
                        config.command,
                        config.group,
                        config.icon,
                        config.handler,
                        config.accelKey,
                        config.keyCombo,
                        config.keyProfile,
                        keyTarget || config.keyTarget,
                        config.keyScope,
                        config.mixin);

                    action.parameters = config;
                } else {
                    action = config;
                }
                this._addAction(result, action);
            }
            if (this.keyboardMappings) {
                console.error('have mappings');
            }
            _.each(this.keyboardMappings, function (mapping) {
                this.registerKeyboardMapping(mapping);
            }, this);
            return result;
        },
        createActionStore: function () {
            if (!this.actionStore) {
                const _actions = this._completeActions(this.actions || []);
                this.actionStore = new ActionStore({
                    id: utils.createUUID(),
                    data: _actions,
                    observedProperties: [
                        "value",
                        "icon",
                        "label"
                    ],
                    tabOrder: this.tabOrder,
                    groupOrder: this.groupOrder,
                    tabSettings: this.tabSettings,
                    menuOrder: this.menuOrder
                });
            }
            return this.actionStore;
        },
        /**
         * Get all actions via query from Action store
         * @param mixed
         * @param allowCache
         * @returns {*}
         */
        getActions: function (mixed, allowCache) {
            if (!mixed && allowCache !== false && this.__actions) {
                return this.__actions;
            }
            let query = mixed;
            //no query or function given
            if (!mixed) {
                query = {
                    command: /\S+/
                };
            }
            this.__actions = this.getActionStore().query(query);
            return this.__actions;

        },
        /**
         * Safe getter for action store
         * @returns {*}
         */
        getActionStore: function () {
            return this.createActionStore();
        },
        /**
         * Create action store upon construction
         */
        postMixInProperties: function () {
            this.inherited && this.inherited(arguments);
            this.createActionStore();
        },
        addActions: function (actions) {
            const store = this.getActionStore();
            if (!store['subscribedToUpdates_' + this.id]) {
                store['subscribedToUpdates_' + this.id] = true;
                this.addHandle('update', store.on('update', evt => {
                    const action = evt.target;
                    if (action._isCreating || !evt.property) {
                        return;
                    }
                    if (action && action.onChange) {
                        action.onChange(evt.property, evt.value, action);
                    }

                }));
            }
            const result = [];
            this._emit('onAddActions', {
                actions: actions,
                permissions: this.permissions,
                store: store
            });

            //remove existing
            this.allowActionOverride && _.each(actions, action => {
                if (action) {
                    const existing = store.getSync(action.command);
                    if (existing) {
                        store.removeSync(existing.command);
                    }
                }
            });
            actions = this._completeActions(actions);

            _.each(actions, action => {
                if (this.allowActionOverride && store.getSync(action.command)) {
                    store.removeSync(action.command);
                }
                const _action = store.putSync(action);
                result.push(_action);
                _action._isCreating = true;
                _action.onCreate && _action.onCreate(_action);
                this._emit('onAddAction', _action);
                _action._isCreating = false;
            });
            return result;

        },
        /**
         *
         * @param label
         * @param command
         * @param icon
         * @param props
         * @param mixin
         * @returns {*}
         */
        createActionShort: function (label, command, icon, props, mixin) {
            return this.createAction(_.extend({
                label: label,
                command: command,
                icon: icon,
                mixin: props && props.mixin ? props.mixin : mixin
            }, props));
        },
        /**
         *
         * @param options
         * @returns {*}
         */
        createAction2: function (options) {
            const thiz = this;
            let action = null;
            const mixin = options.mixin || {};
            const owner = options.owner || mixin.owner || thiz;
            const permissions = options.permissions || this.permissions || [];
            const command = options.command;
            let keycombo = options.keycombo;
            const label = options.label;
            const icon = options.icon;
            const tab = options.tab;
            const group = options.group;
            const filterGroup = options.filterGroup;
            const onCreate = options.onCreate;
            let handler = options.handler;
            const container = options.container || thiz.domNode;
            const shouldShow = options.shouldShow;
            const shouldDisable = options.shouldDisable;

            utils.mixin(mixin, {
                owner: owner,
                onChange: options.onChange

            });

            if (mixin.addPermission || DefaultActions.hasAction(permissions, command)) {

                handler = handler || DefaultActions.defaultHandler;
                if (keycombo) {
                    if (_.isString(keycombo)) {
                        keycombo = [keycombo];
                    }
                    mixin.tooltip = keycombo.join('<br/>').toUpperCase();
                }

                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, container || thiz.domNode);
                if (owner && action && owner.addAction) {
                    owner.addAction(null, action);
                }
                return action;
            }
        },
        /**
         * Create Action
         * @param label
         * @param command
         * @param icon
         * @param keycombo
         * @param tab
         * @param group
         * @param filterGroup
         * @param onCreate
         * @param handler
         * @param mixin
         * @param shouldShow
         * @param shouldDisable
         * @param permissions
         * @param container
         * @param owner
         * @returns {*}
         */
        createAction: function (label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, permissions, container, owner) {
            if (arguments.length == 1) {
                return this.createAction2(arguments[0]);
            }
            const thiz = this;
            let action = null;

            mixin = mixin || {};
            utils.mixin(mixin, {
                owner: owner || thiz
            });

            if (mixin.addPermission || DefaultActions.hasAction(permissions, command)) {
                if (!handler) {
                    handler = function (action) {
                        this.runAction && this.runAction.apply(this, [action]);
                    };
                }
                keycombo && _.isString(keycombo) && (keycombo = [keycombo]);

                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, container || thiz.domNode);

                if (owner && action && owner.addAction) {
                    owner.addAction(null, action);
                }
                return action;
            }
        },
        addAction: function (where, action) {
            const actions = where || [];
            const eventCallbackResult = this._emit('addAction', action);
            if (eventCallbackResult === false) {
                return false;
            } else if (_.isObject(eventCallbackResult)) {
                utils.mixin(action, eventCallbackResult);
            }
            actions.push(action);
            return true;
        },
        /**
         *
         * @param where
         * @param action
         * @returns {boolean}
         */
        _addAction: function (where, action) {
            const actions = where || [];
            const eventCallbackResult = this._emit('addAction', action);

            if (eventCallbackResult === false) {
                return false;
            } else if (utils.isObject(eventCallbackResult)) {
                utils.mixin(action, eventCallbackResult);
            }
            actions.push(action);
            return true;
        },
        hasAction: function (action) {
            return DefaultActions.hasAction(this.permissions, action);
        }
    };

    /**
     * Provides tools to deal with 'actions' (xaction/Action). This is the model part for actions which is being used
     * always together with the render part(xide/widgets/EventedMixin) in a subclass.
     *
     * @class module:xide/mixins/ActionProvider
     * @extends module:xide/mixins/EventedMixin
     */
    const Module = declare("xaction/ActionProvider", [EventedMixin, Keyboard], Implementation);
    Module.dcl = dcl([EventedMixin.dcl, Keyboard.dcl], Implementation);
    return Module;
});