/** @module xblox/BlockActions **/
define("xblox/BlockActions", [
    'xdojo/declare',
    'xdojo/has',
    'xide/utils',
    'xide/types',
    'xaction/DefaultActions',
    'xide/factory',
    'dojo/promise/all',
    "dojo/Deferred",
    'xide/views/History',
    'xfile/views/FilePreview',
    'xblox/views/BlockEditView',
    'xide/views/_CIPanelDialog',
    'xide/mixins/Electron',
    'xblox/model/variables/VariableAssignmentBlock',
    'xcf/model/Variable',
    'xblox/views/DeleteDialog',
    'xide/widgets/ArgumentsWidget'

], function (declare, has, utils, types, DefaultActions, factory, all, Deferred, History, FilePreview, BlockEditView,
    _CIPanelDialog, Electron, VariableAssignmentBlock, Variable, DeleteDialog) {

    var _debugHightlighting = false;
    var _debugRun = false;
    var BLOCK_INSERT_ROOT_COMMAND = 'Step/Insert';
    var GLOBAL_CLIPBOARD = {};
    /**
     * Action implementation for block grids.
     * @class module:xblox/BlockActions
     * @extends module:xblox/views/BlockGrid
     * @augments module:xide/mixins/EventedMixin
     */
    var Module = declare('xblox.BlockActions', [Electron], {
        clipboardCopy: function () {
            this.currentCutSelection = null;
            this.currentCopySelection = this.getSelection();
            GLOBAL_CLIPBOARD = null;
            GLOBAL_CLIPBOARD = this.currentCopySelection;
            var clipboard = this.getClipboard();
            if (clipboard) {
                var blocks = this.blockScope.blocksToJson(this.currentCopySelection);
                var blocksStr = JSON.stringify(blocks, null, 2);
                clipboard.writeText(blocksStr);
            }
        },
        download: function () {
            var blocks = this.blockScope.blocksToJson();
            var blocksStr = JSON.stringify(blocks, null, 2);
            utils.download("blocks.json", blocksStr);
        },
        save: function () {
            var driver = this.userData.driver,
                ctx = this.ctx,
                fileManager = ctx.getFileManager(),
                scope = this.blockScope,
                path = driver.path.replace('.meta.json', '.xblox'),
                mount = driver.scope;

            if (scope) {
                var _onSaved = function () {};
                fileManager.setContent(mount, path, scope.toString(), _onSaved);
            }
        },
        /***********************************************************************/
        /*
         * Shared Property View
         */
        propertyStruct: {
            currentCIView: null,
            targetTop: null,
            _lastItem: null
        },
        getPropertyStruct: function () {
            return this.propertyStruct;
        },
        setPropertyStruct: function (struct) {
            this.propertyStruct = struct;
        },
        /**
         *
         * @param source
         * @param target
         * @param before
         * @param grid
         * @param targetState
         * @param insert
         * @returns {*}
         */
        onDrop: function (source, target, before, grid, targetState, insert) {
            var ctrArgs = source.ctrArgs || {};
            var proto = source.proto;
            var newBlock = null;
            var isNewItem = false;

            //prepare args
            if (source.ctrArgs) { //comes from factory
                ctrArgs.scope = ctrArgs.scope || target.scope;
                ctrArgs.group = ctrArgs.group || target.group;
                ctrArgs.parentId = ctrArgs.parentId || target.id;
                isNewItem = true;
            }

            if (isNewItem) {
                //new item at root level
                if (!target.parentId && !insert) {
                    ctrArgs.parentId = null;
                    factory.createBlock(proto, ctrArgs); //root block
                } else if (insert && target.canAdd && target.canAdd() != null) { //new item at item level
                    target.add(proto, ctrArgs, null);
                }
            } else {
                //real item move, before or after
                if (targetState === 'Moved') {
                    if (source.scope && target.scope && source.scope == target.scope) {
                        source.group = null;
                        target.scope.moveTo(source, target, before, insert);
                        return source;
                    }
                }
            }
        },
        _paste: function (items, owner, cut) {
            if (!items) {
                return;
            }
            var target = this.getSelection()[0],
                _flatten,
                source,
                thiz = this,
                scope = thiz.blockScope;

            if (!owner) {
                return; //@TODO : support
            }

            //special case when paste on nothing
            if (!target) {
                //save parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj.parentId) {
                        obj._parentOri = '' + obj.parentId;
                        obj.parentId = null;
                    }
                }
                //flatten them
                _flatten = scope.flatten(items);
                var _flat2 = _.uniq(_flatten, false, function (item) {
                    return item.id;
                });
                //clone them
                var _clones = scope.cloneBlocks2(_flat2, this.newRootItemGroup);

                scope.flatten(_clones);
                var firstItem = null;
                for (var i = 0; i < _clones.length; i++) {
                    var clone = _clones[i];
                    if (!firstItem) {
                        firstItem = clone;
                    }
                }
                //restore parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj._parentOri) { //restore
                        obj.parentId = obj._parentOri;
                        delete obj['_parentOri'];
                    }
                }
                return _clones;
            }

            var grid = owner;

            var srcScope = items[0].scope;
            var dstScope = target.scope;
            if (srcScope != dstScope) {
                return;
            }
            var insert = target.canAdd() || false;
            var parent = srcScope.getBlockById(target.parentId);
            if (!parent) {
                parent = target;
            }
            var targetState = 'Moved';
            var before = false;
            _flatten = scope.flatten(items);
            _flatten = _.uniq(_flatten, false, function (item) {
                return item.id;
            });

            items = srcScope.cloneBlocks2(_flatten);
            var newItems = [];
            for (var i = 0; i < items.length; i++) {
                source = items[i];
                //Case source=target
                if (source.parentId) {
                    newItems.push(source);
                } else {
                    source.parentId = parent.id;
                    parent.add(source);
                    var nItem = this.onDrop(source, target, before, grid, targetState, insert);
                    newItems.push(nItem);
                }
            }
            return newItems;
        },
        paste: function (items, owner, cut) {
            if (!items) {
                return;
            }
            var target = this.getSelection()[0],
                _flatten,
                source,
                thiz = this,
                scope = thiz.blockScope;

            if (!owner) {
                return; //@TODO : support
            }

            //special case when paste on nothing
            if (!target) {
                //save parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj.parentId) {
                        obj._parentOri = '' + obj.parentId;
                        obj.parentId = null;
                    }
                }
                //flatten them
                _flatten = scope.flatten(items);

                var _flat2 = _.uniq(_flatten, false, function (item) {
                    return item.id;
                });
                //clone them
                var _clones = scope.cloneBlocks2(_flat2, this.newRootItemGroup);
                scope.flatten(_clones);
                var firstItem = null;
                for (var i = 0; i < _clones.length; i++) {
                    var clone = _clones[i];
                    if (!firstItem) {
                        firstItem = clone;
                    }
                }
                //restore parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj._parentOri) { //restore
                        obj.parentId = obj._parentOri;
                        delete obj['_parentOri'];
                    }
                }
                return _clones;
            }

            var grid = owner;
            var srcScope = items[0].scope;
            var dstScope = target.scope;
            if (srcScope != dstScope) {
                return;
            }
            var insert = target.canAdd() || false;
            var parent = srcScope.getBlockById(target.parentId);
            if (!parent) {
                parent = target;
            }
            var targetState = 'Moved';
            var before = false;

            _flatten = scope.flatten(items);

            items = srcScope.cloneBlocks2(_flatten);
            var newItems = [];
            var parentTop = target.getTopRoot();

            for (var i = 0; i < items.length; i++) {
                source = items[i];
                var sourceParent = source.getTopRoot();
                //Case source=target
                if (source.parentId) {

                    if (!parentTop) {
                        var _parent = dstScope.getBlockById(source.parentId);
                        //the parent is part of the copy
                        if (_parent && _.find(items, {
                                id: _parent.id
                            })) {

                        } else {
                            source.parentId = null;
                        }
                    }
                }
                if (source.parentId && parentTop) {
                    var sourceTop = source.getTopRoot();
                    //same roots
                    if (sourceTop.id && parentTop.id && (sourceTop.id !== target.id)) {
                        source.parentId = null;
                    }
                }

                if (source.parentId) {
                    newItems.push(source);
                } else {
                    source.parentId = parent.id;
                    parent.add(source);
                    var nItem = this.onDrop(source, target, before, grid, targetState, insert);
                    newItems.push(nItem);
                }
            }
            return newItems;
        },
        /**
         *
         * @param items
         * @param owner
         * @param cut
         * @returns {*}
         */
        defaultActionResult: function (items) {
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 0,
                select: items,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        setParentBlock: function (block) {
            block = _.isString(block) ? this.collection.getSync(block) : block;
            this.set('collection', this.collection.filter({
                parentId: block.id
            }));

            var history = this.getHistory();
            history.push(block.id);
        },
        refreshCurrent: function () {
            var history = this.getHistory();
            var now = history.getNow();
            if (now) {
                this.setParentBlock(now);
            }
        },
        /**
         * Action runner
         * @param action {module xaction/ActionModel| String }
         * @returns {boolean}
         */
        runAction: function (action) {
            action = this.getAction(action);
            if (!action) {
                return null;
            }
            var renderer = this.getSelectedRenderer();
            var _rendererResult = renderer && renderer.runAction ? renderer.runAction.apply(this, [action]) : null;
            if (_rendererResult) {
                return _rendererResult;
            }
            try {

                if (action.command.indexOf(BLOCK_INSERT_ROOT_COMMAND) !== -1) {
                    return this.addItem(action.item, action);
                }

                var selection = this.getSelection(),
                    item = selection[0],
                    thiz = this,
                    ACTION = types.ACTION;

                if (action.command == ACTION.DOWNLOAD) {
                    return this.download();
                }

                function addItem(_class, group, parent) {
                    var dfd = new Deferred();
                    var cmd = null;
                    if (_class == Variable) {
                        cmd = factory.createBlock(_class, {
                            name: "No Title",
                            scope: thiz.blockScope,
                            group: group
                        });

                    } else {
                        cmd = thiz.addItem({
                            proto: _class,
                            ctrArgs: {
                                scope: thiz.blockScope,
                                group: group
                            }
                        });
                    }

                    var defaultDfdArgs = {
                        select: [cmd],
                        focus: true,
                        append: false
                    };

                    var ref = thiz.refresh();
                    ref && ref.then && ref.then(function () {
                        dfd.resolve(defaultDfdArgs);
                    });

                    if (thiz.grids) {
                        _.each(thiz.grids, function (grid) {
                            if (grid && grid.refreshRoot) {
                                grid.refreshRoot();
                                grid.refresh();
                                grid.refreshCurrent();
                            }
                        });
                    } else {
                        if (thiz.refreshRoot) {
                            thiz.refreshRoot();
                            thiz.refresh();
                            thiz.refreshCurrent();
                        }
                    }
                    return dfd;
                }

                switch (action.command) {

                    case 'File/Delete':
                        {
                            return this.deleteBlock(selection);
                        }
                    case 'File/Reload':
                        {
                            return this.refresh();
                        }
                    case 'Step/Run':
                        {
                            return this.execute(selection);
                        }
                    case 'Step/Stop':
                        {
                            return this.stop(selection);
                        }
                    case 'Step/Pause':
                        {
                            return this.pause(selection);
                        }
                    case 'Step/Back':
                        {
                            return this.goBack();
                        }
                    case 'Step/Move Left':
                        {
                            return this.reParentBlock(-1);
                        }
                    case 'Step/Move Right':
                        {
                            return this.reParentBlock(1);
                        }
                    case 'Step/Move Up':
                        {
                            this.move(-1);
                            return this._itemChanged('moved', item);
                        }
                    case 'Step/Move Down':
                        {
                            this.move(1);
                            return this._itemChanged('moved', item);
                        }
                    case 'Step/Edit':
                        {
                            return this.editBlock();
                        }
                    case 'Step/Open':
                        {
                            return this.openBlock();
                        }
                    case 'New/Variable':
                        {
                            return addItem(Variable, 'Variables');
                        }
                    case 'File/Select/None':
                        {
                            return this.deselectAll();
                        }
                    case 'Step/Enable':
                        {
                            _.each(selection, function (item) {
                                item && (item.enabled = !item.enabled);
                                item.enabled ? item.activate() : item.deactivate();
                                item.set('enabled', item.enabled);
                            });
                            this.refreshRoot();
                            this.refreshCurrent();
                            return this.defaultActionResult(selection);
                        }
                    case 'View/Show/Properties':
                        {
                            var dfd = new Deferred();
                            var right = this.getRightPanel();

                            function collapse() {

                                var splitter = right.getSplitter();
                                if (splitter) {
                                    if (splitter.isCollapsed()) {
                                        splitter.expand();
                                        return true;
                                    } else {
                                        right.collapse();
                                        return false;
                                    }
                                }
                            }

                            var wasClosed = collapse();
                            if (wasClosed) {
                                this.showProperties(item);
                            }
                            var defaultSelectArgs = {
                                focus: true,
                                append: false,
                                delay: 500,
                                select: selection
                            };
                            dfd.resolve(defaultSelectArgs);
                            this.onAfterAction('View/Show/Properties');
                            return dfd;
                        }
                    case 'Step/Help':
                        {
                            return this.showHelp(item);
                        }

                }

            } catch (e) {
                logError(e);
            }
            return this.inherited(arguments);
        },
        /**
         * Step/Move Left & Step/Move Right action
         * @param dir
         * @returns {boolean}
         */
        reParentBlock: function (dir) {
            var item = this.getSelection()[0];
            if (!item) {
                console.log('cant move, no selection or parentId', item);
                return false;
            }
            var thiz = this;
            if (dir == -1) {
                item.unparent(thiz.blockGroup);
            } else {
                item.reparent();
            }


            thiz.deselectAll();
            thiz.refreshRoot();
            this.refreshCurrent();
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 100,
                select: item,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        /**
         * Step/Move Down & Step/Move Up action
         * @param dir
         */
        move: function (dir) {
            var items = this.getSelection();
            if (!items || !items.length /*|| !item.parentId*/ ) {
                console.log('cant move, no selection or parentId', items);
                return;
            }
            var thiz = this;
            _.each(items, function (item) {
                item.move(dir);
            }.bind(this));

            thiz.refresh();
            this.select(items, null, true, {
                focus: true,
                delay: 10
            }).then(function () {
                thiz.refreshActions();
            });
        },
        /**
         * Clipboard/Paste action
         */
        clipboardPaste: function () {
            var selection = this.currentCopySelection || this.currentCutSelection || GLOBAL_CLIPBOARD;
            var clipboard = this.getClipboard();

            if (clipboard && (!selection || !selection[0])) {
                var eClipboard = clipboard.readText();
                if (eClipboard) {
                    try {
                        var blocks = JSON.parse(eClipboard);
                        if (blocks && blocks.length) {
                            var tmpScope = this.blockScope.owner.getScope(utils.createUUID(), null, false);
                            var newBlocks = tmpScope.blocksFromJson(blocks); //add it to our scope
                            if (newBlocks && newBlocks.length) {
                                selection = newBlocks;
                            }
                        }

                    } catch (e) {
                        logError(e, 'clipboard extraction from electron failed');
                    }
                }
            }
            if (!selection || !selection[0]) {
                return;
            }

            var newItems = this.paste(selection, this, this.currentCutSelection != null);
            var dfd = new Deferred();
            if (newItems) {
                this.refreshRoot();
                this.refreshCurrent();
                var defaultSelectArgs = {
                    focus: true,
                    append: false,
                    delay: 2,
                    select: newItems,
                    expand: true
                };
                dfd.resolve(defaultSelectArgs);
            }
            return dfd;
        },
        /**
         * File/Delete Action
         * @param items
         */
        deleteBlock: function (items) {
            var self = this;
            var dfd = new Deferred();

            var item = items[0];
            var parent = item.getParent();
            var _prev = item.next(null, -1) || item.next(null, 1) || parent;

            function main() {
                function _delete(item) {
                    if (!item) {
                        return false;
                    }

                    //try individual item remove function
                    if (item.remove) {
                        item.remove();
                    }

                    if (item.destroy) {
                        item.destroy();
                    }

                    //this should be redundant as item.remove should do the same too
                    var store = this.getStore(item);
                    if (store) {
                        var _itm = store.getSync(item[store.idProperty]);
                        store.removeSync(item[store.idProperty]);
                    }

                    item._destroyed = true;
                    item.scope._emit(types.EVENTS.ON_ITEM_REMOVED, {
                        item: item
                    });

                    return true;
                }
                _.each(items, _delete, self);
                this.refreshRoot();
                this.refreshCurrent();
            }
            var title = 'Delete ' + items.length + ' ' + 'items ?';
            var dlg = new DeleteDialog({
                ctx: self.ctx,
                notificationMessage: null,
                title: title,
                type: types.DIALOG_TYPE.DANGER,
                onCancel: function () {
                    dfd.resolve({
                        select: items,
                        focus: true,
                        append: false
                    });
                },
                onOk: function () {
                    main.bind(self)();
                    dfd.resolve({
                        focus: true,
                        append: false,
                        select: _prev
                    });
                }
            });
            dlg.show();
            return dfd;
        },
        /**
         * File/Edit action
         * @param item
         * @param changedCB
         * @param select
         */
        editBlock2: function (item, changedCB, select) {
            if (!item) {
                return;
            }
            if (!item.canEdit()) {
                return;
            }
            var title = 'Edit ',
                thiz = this;

            title += item.name;
            var cis = item.getFields();
            var cisView = null;
            var self = this;

            function ok(changedCIS) {
                _.each(changedCIS, function (evt) {
                    thiz.onCIChanged && thiz.onCIChanged(evt.ci, item, evt.oldValue, evt.newValue, evt.ci.dst, cis, evt.props);
                });
                self.showProperties(item, true);
            }

            var panel = new _CIPanelDialog({
                title: title,
                containerClass: 'CIDialog',
                onOk: function (changedCIS) {
                    ok(changedCIS);
                    this.headDfd.resolve(changedCIS);
                },
                onCancel: function (changedCIS) {
                    this.headDfd.resolve(changedCIS);
                },
                cis: cis,
                CIViewClass: BlockEditView,
                CIViewOptions: {
                    delegate: this,
                    resizeToParent: true,
                    ciSort: true,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Send': 2,
                            'Advanced': 4,
                            'Description': 5
                        }
                    },
                    cis: cis
                }
            });
            var dfd = panel.show();
            return dfd;
        },
        _getText: function (url) {
            if ( false ) {
                url += '?time=' + new Date().getTime();
            }
            try {
                var result;
                dojo.xhrGet({
                    url: url,
                    sync: true,
                    handleAs: 'text',
                    cache: false,
                    load: function (text) {
                        result = text;
                    }
                });
                return (result || 'No help avaiable');
            } catch (e) {
                return false;
            }
        },
        _helpCache: {},
        getHelp: function (item) {

            var mid = utils.replaceAll('.', '/', item.declaredClass);
            //special treat for xcf
            mid = mid.replace('xcf', 'xblox');
            var rootNS = 'xblox/model/';
            mid = 'xblox/docs/' + mid.replace(rootNS, '');

            var cached = this._helpCache[mid];
            if (cached === false) {
                return false;
            } else if (_.isString(cached)) {
                return cached;
            }

            var helpText = this._getText(require.toUrl(mid) + '.md');
            if (!helpText) {
                return false;
            }
            //https://github.com/showdownjs/showdown
            if (typeof showdown !== 'undefined') {
                var converter = new showdown.Converter();
                helpText = converter.makeHtml(helpText);
                console.log('load from ' + require.toUrl(mid) + '.md');
            } else {
                console.warn('have no `showdown`, proceed without');
            }
            this._helpCache[mid] = helpText;
            return helpText;
        },
        showHelp: function (item, parent) {
            var row = this.row(item),
                el = row.element,
                self = this;

            function render(where, item) {
                var help = self.getHelp(item);
                if (help) {
                    where.html(help);
                }
                where.addClass('xbloxHelp');
            }

            if (this._preview) {
                this._preview.open();
                render(this._preview.preview, item);
                return;
            }
            var _prev = new FilePreview({
                node: $(el),
                parent: $(el),
                ctx: self.ctx,
                container: parent ? parent.containerNode : null,
                title: 'Help'
            });
            parent ? _prev.buildRenderingEmbedded() : _prev.buildRendering();
            _prev.init();
            _prev.exec();
            this._preview = _prev;
            this._preview.handler = this;
            _prev.onOpened = function () {
                render(_prev.preview, item);
            };
            this.add(_prev);
            self._on('selectionChanged', function (e) {
                if (self._preview.getstate() == 0) {
                    return;
                }
                var _item = self.getSelectedItem();
                if (_item) {
                    _prev.item = _item;
                    render(self._preview.preview, _item);
                }
            });
        },
        getHistory: function () {
            if (!this._history) {
                this._history = new History();
            }

            return this._history;
        },
        goBack: function () {
            var store = this.collection,
                history = this.getHistory(),
                now = null,
                prev = history.getNow(),
                self = this,
                head = new Deferred(),
                select = true;
            history.pop();
            now = history.getNow();

            function resolve(item) {
                head.resolve({
                    select: select !== false ? (item ? item : self.getRows()[0]) : null,
                    focus: true,
                    append: false,
                    delay: 250
                });
            }
            if (now && store.getSync(now)) {
                this.set('collection', this.collection.filter({
                    parentId: now
                }));
                resolve();
            } else {
                store.reset();
                this.refreshRoot();
                resolve(store.getSync(prev));
            }
            return head;

        },
        openBlock: function (item, changedCB, select) {
            var selection = this.getSelection();
            item = selection[0] || item;
            if (!item) {
                return;
            }
            var head = new Deferred();
            var self = this;
            var isBack = false;
            this.setParentBlock(item);
            select = true;
            var rows = self.getRows()[0];
            if (!rows) {
                setTimeout(function () {
                    self.contentNode.focus();
                }, 10);
            }
            head.resolve({
                select: select !== false ? (isBack ? item : rows) : null,
                focus: true,
                append: false,
                delay: 1
            });

            return head;
        },
        editBlock: function (item, changedCB, select) {
            var selection = this.getSelection();
            item = selection[0] || item;
            if (!item) {
                return;
            }
            var head = new Deferred();
            var children = item.getChildren(),
                self = this;

            var dlgDfd = this.editBlock2(item, null);
            if (!dlgDfd) {
                head.resolve();
                return;
            }
            dlgDfd.then(function () {
                head.resolve({
                    select: [item],
                    focus: true,
                    append: false,
                    delay: 1
                });
            });

            return head;
        },
        execute: function (_blocks) {
            var thiz = this,
                dfds = [], //all Deferreds of selected blocks to run
                handles = [],
                EVENTS = types.EVENTS,
                blocks = _.isArray(_blocks) ? _blocks : [_blocks];


            function run(block) {

                var _runHandle = block._on(EVENTS.ON_RUN_BLOCK, function (evt) {
                        _debugHightlighting && console.error('active');
                        thiz.mark(thiz.toNode(evt), 'activeBlock', block);
                    }),

                    //event handle "Success"
                    _successHandle = block._on(EVENTS.ON_RUN_BLOCK_SUCCESS, function (evt) {
                        _debugHightlighting && console.log('marke success', evt);
                        thiz.mark(thiz.toNode(evt), 'successBlock', block);

                    }),

                    //event handle "Error"
                    _errHandle = block._on(EVENTS.ON_RUN_BLOCK_FAILED, function (evt) {
                        _debugHightlighting && console.error('failed', evt);
                        thiz.mark(thiz.toNode(evt), 'failedBlock', block);
                    });

                _debugRun && console.error('run block ');


                if (!block || !block.scope) {
                    console.error('have no scope');
                    return;
                }

                try {
                    var blockDfd = block.scope.solveBlock(block, {
                        highlight: true,
                        force: true,
                        listener: thiz
                    });

                    dfds.push(blockDfd);

                } catch (e) {
                    logError(e, 'excecuting block -  ' + block.name + ' failed! : ');
                }
                handles = handles.concat([_runHandle, _errHandle, _successHandle]);
                return true;
            }

            function _patch(block) {

                block.runFrom = function (_blocks, index, settings) {

                    var thiz = this,
                        blocks = _blocks || this.items,
                        allDfds = [];

                    var onFinishBlock = function (block, results) {
                        block._lastResult = block._lastResult || results;
                        thiz._currentIndex++;
                        thiz.runFrom(blocks, thiz._currentIndex, settings);
                    };

                    var wireBlock = function (block) {
                        block._deferredObject.then(function (results) {
                            console.log('----def block finish');
                            onFinishBlock(block, results);
                        });
                    };

                    if (blocks.length) {

                        for (var n = index; n < blocks.length; n++) {
                            var block = blocks[n];
                            console.log('run child \n' + block.method);

                            _patch(block);

                            if (block.deferred === true) {
                                block._deferredObject = new Deferred();
                                this._currentIndex = n;
                                wireBlock(block);
                                //this.addToEnd(this._return, block.solve(this.scope, settings));
                                var blockDfd = block.solve(this.scope, settings);
                                allDfds.push(blockDfd);
                                break;
                            } else {
                                //this.addToEnd(this._return, block.solve(this.scope, settings));

                                var blockDfd = block.solve(this.scope, settings);
                                allDfds.push(blockDfd);
                            }

                        }

                    } else {
                        this.onSuccess(this, settings);
                    }

                    return allDfds;
                };

                block.solve = function (scope, settings, run, error) {

                    this._currentIndex = 0;
                    this._return = [];

                    var _script = '' + this._get('method');

                    var thiz = this,
                        ctx = this.getContext(),
                        items = this[this._getContainer()],

                        //outer,head dfd
                        dfd = new Deferred(),
                        listener = settings.listener,
                        isDfd = thiz.deferred;

                    //moved to Contains#onRunThis
                    if (listener) {
                        listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                    }

                    //function when a block did run successfully,
                    // moved to Contains#onDidRunItem
                    function _finish(dfd, result, event) {

                        if (listener) {
                            listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                        }
                        dfd.resolve(result);


                    }

                    //function when a block did run successfully
                    function _error(result) {
                        dfd.reject(result);
                        if (listener) {
                            listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                        }
                    }


                    //moved to Contains#onDidRunThis
                    function _headDone(result) {


                        //more blocks?
                        if (items.length) {
                            var subDfds = thiz.runFrom(items, 0, settings);

                            all(subDfds).then(function (what) {
                                console.log('all solved!', what);
                                _finish(dfd, result);
                            }, function (err) {
                                console.error('error in chain', err);
                                _finish(dfd, err);
                            });

                        } else {
                            _finish(dfd, result);
                        }
                    }


                    if (_script && _script.length) {

                        var runScript = function () {

                            var _function = new Function("{" + _script + "}");
                            var _args = thiz.getArgs() || [];
                            try {

                                if (isDfd) {
                                    ctx.resolve = function (result) {
                                        if (thiz._deferredObject) {
                                            thiz._deferredObject.resolve();
                                        }
                                        _headDone(result);
                                    };
                                }
                                var _parsed = _function.apply(ctx, _args || {});
                                thiz._lastResult = _parsed;
                                if (run) {
                                    run('Expression ' + _script + ' evaluates to ' + _parsed);
                                }
                                if (!isDfd) {
                                    thiz.onDidRunThis(dfd, _parsed, items, settings);
                                }
                            } catch (e) {
                                e = e || {};
                                _error(e);
                                if (error) {
                                    error('invalid expression : \n' + _script + ': ' + e);
                                }
                            }
                        };

                        if (scope.global) {
                            (function () {
                                window = scope.global;
                                var _args = thiz.getArgs() || [];
                                try {
                                    var _parsed = null;
                                    if (!ctx.runExpression) {
                                        var _function = new Function("{" + _script + "}").bind(this);
                                        _parsed = _function.apply(ctx, _args || {});
                                    } else {
                                        _parsed = ctx.runExpression(_script, null, _args);
                                    }

                                    thiz._lastResult = _parsed;

                                    if (run) {
                                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                                    }
                                    if (_parsed !== 'false' && _parsed !== false) {
                                        thiz.onSuccess(thiz, settings);
                                    } else {
                                        thiz.onFailed(thiz, settings);
                                        return [];
                                    }
                                } catch (e) {
                                    thiz._lastResult = null;
                                    if (error) {
                                        error('invalid expression : \n' + _script + ': ' + e);
                                    }
                                    thiz.onFailed(thiz, settings);
                                    return [];
                                }
                            }).call(scope.global);
                        } else {
                            runScript();
                        }
                    } else {
                        console.error('have no script');
                    }
                    return dfd;
                };
            }

            _.each(blocks, run);
            all(dfds).then(function () {
                _debugRun && console.log('did run all selected blocks!', thiz);
                // _.invoke(handles, 'remove');
            });
        },
        stop: function (_blocks) {
            var blocks = _.isArray(_blocks) ? _blocks : [_blocks];

            function stop(block) {
                if (!block || !block.scope) {
                    console.error('have no scope');
                    return;
                }
                try {
                    block.stop(false);
                } catch (e) {
                    logError(e, 'stop block -  ' + block.name + ' failed! : ');
                }
                return true;
            }

            _.each(blocks, stop);
        },
        pause: function (_blocks) {
            var blocks = _.isArray(_blocks) ? _blocks : [_blocks];

            function stop(block) {
                if (!block || !block.scope) {
                    console.error('have no scope');
                    return;
                }
                try {
                    block.pause(true);
                } catch (e) {
                    logError(e, 'pause block -  ' + block.name + ' failed! : ');
                }
                return true;
            }

            _.each(blocks, stop);
        },
        /**
         *
         * @param item
         * @returns {Array}
         */
        getAddActions: function (item) {
            var thiz = this;
            var items = this.inherited(arguments) || [];
            item = item || {};
            items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
            //tell everyone
            thiz.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this.owner,
                item: item,
                group: this.blockGroup,
                scope: this.blockScope
            });

            thiz._emit(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this.owner
            });
            return items;
        },
        getBlockActions: function (permissions) {
            var result = this.inherited(arguments) || [],
                ACTION = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                thiz = this,
                actionStore = thiz.getActionStore();

            var defaultMixin = {
                addPermission: true
            };

            result.push(thiz.createAction({
                label: 'Save',
                command: 'File/Save',
                icon: 'fa-save',
                tab: 'Home',
                group: 'File',
                keycombo: ['ctrl s'],
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Save As',
                command: 'File/Save As',
                icon: 'fa-save',
                tab: 'Home',
                group: 'File',
                mixin: defaultMixin
            }));

            result.push(this.createAction({
                label: 'Open',
                command: 'Step/Open',
                icon: 'fa-folder-open',
                keycombo: ['ctrl enter'],
                tab: 'Home',
                group: 'Navigation',
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Go up',
                command: 'Step/Back',
                icon: ACTION_ICON.GO_UP,
                tab: 'Home',
                group: 'Navigation',
                keycombo: ['backspace'],
                mixin: {
                    quick: true
                }
            }));

            function _selection() {
                var selection = thiz.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                var item = selection[0];
                if (!item) {
                    return null;
                }
                return selection;
            }

            function canParent() {
                var selection = thiz.getSelection();
                if (!selection) {
                    return true;
                }
                var item = selection[0];
                if (!item) {
                    return true;
                }
                if (this.command === 'Step/Move Left') {
                    return !item.getParent();
                } else {
                    return item.getParent();
                }
            }

            function isItem() {
                var selection = _selection();
                if (!selection) {
                    return true;
                }
                return false;
            }

            function canMove() {
                var selection = _selection();
                if (!selection) {
                    return true;
                }
                var item = selection[0];
                if (this.command === 'Step/Move Up') {
                    return !item.canMove(null, -1);
                } else {
                    return !item.canMove(null, 1);
                }
            }

            result.push(thiz.createAction({
                label: 'Run',
                command: 'Step/Run',
                icon: 'text-success fa-play',
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Stop',
                command: 'Step/Stop',
                icon: 'text-warning fa-stop',
                tab: 'Home',
                group: 'Step',
                keycombo: ['s'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Pause',
                command: 'Step/Pause',
                icon: 'text-info fa-pause',
                tab: 'Home',
                group: 'Step',
                keycombo: ['p'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            //////////////////////////////////////////////////////////////////
            //
            //  Step - Move
            //

            result.push(thiz.createAction({
                label: 'MoveUp',
                command: 'Step/Move Up',
                icon: 'text-info fa-arrow-up',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt up'],
                shouldDisable: canMove,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'MoveDown',
                command: 'Step/Move Down',
                icon: 'text-info fa-arrow-down',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt down'],
                shouldDisable: canMove,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'MoveLeft',
                command: 'Step/Move Left',
                icon: 'text-info fa-arrow-left',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt left'],
                shouldDisable: canMove,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'MoveRight',
                command: 'Step/Move Right',
                icon: 'text-info fa-arrow-right',
                tab: 'Home',
                group: 'Move',
                keycombo: ['alt right'],
                shouldDisable: canParent,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Help',
                command: 'Step/Help',
                icon: 'fa-question',
                tab: 'Home',
                group: 'Step',
                keycombo: ['h'],
                shouldDisable: isItem,
                mixin: defaultMixin
            }));


            result.push(thiz.createAction({
                label: 'On',
                command: 'Step/Enable',
                icon: 'fa-toggle-off',
                tab: 'Home',
                group: 'Step',
                keycombo: ['alt o'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    actionType: 'multiToggle',
                    quick: true
                }, defaultMixin),
                onCreate: function (action) {
                    //action.set('value',true);
                    action.value = true;
                    action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                        group: 'Common'
                    });
                },
                onChange: function (property, value) {
                    var item = thiz.getSelectedItem();
                    if (item) {
                        item.set('enabled', value);
                    }
                    this.value = value;
                    thiz.refreshActions();
                    thiz.refresh(true);
                    thiz.select(item);
                }
            }));

            result.push(this.createAction({
                label: 'Variable',
                command: 'New/Variable',
                icon: 'fa-code',
                tab: 'Home',
                group: 'Insert',
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(this.createAction({
                label: 'Edit',
                command: 'Step/Edit',
                icon: ACTION_ICON.EDIT,
                keycombo: ['f4', 'enter', 'dblclick'],
                tab: 'Home',
                group: 'Step',
                shouldDisable: isItem,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            result.push(thiz.createAction({
                label: 'Properties',
                command: 'View/Show/Properties',
                icon: 'fa-gears',
                tab: 'Home',
                group: 'Show',
                keycombo: ['alt p'],
                shouldDisable: isItem,
                mixin: utils.mixin({
                    actionType: 'multiToggle',
                    value: true
                }, defaultMixin),
                onCreate: function (action) {
                    var right = thiz.getRightPanel();
                    right.getSplitter();
                    //action.set('value',true);
                    action.value = true;
                },
                onChange: function (property, value) {
                    var right = thiz.getRightPanel();

                    function collapse() {
                        var splitter = right.getSplitter();
                        if (splitter) {
                            if (splitter.isCollapsed()) {
                                splitter.expand();
                                return true;
                            } else {
                                right.collapse();
                                return false;
                            }
                        }
                    }

                    var wasClosed = collapse();
                    var result = true;
                    if (wasClosed) {
                        thiz.showProperties(null);
                    }
                    thiz.onAfterAction('View/Show/Properties');
                    return result;
                }
            }));


            var newBlockActions = this.getAddActions();
            var levelName = '';

            var rootAction = BLOCK_INSERT_ROOT_COMMAND;
            result.push(thiz.createAction({
                label: 'Add Block',
                command: '' + rootAction,
                icon: 'fa-plus',
                tab: 'Home',
                group: 'Insert',
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            function addItems(commandPrefix, items) {

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];

                    levelName = item.name;

                    var path = commandPrefix + '/' + levelName;
                    var isContainer = !_.isEmpty(item.items);

                    result.push(thiz.createAction({
                        label: levelName,
                        command: path,
                        icon: item.iconClass,
                        tab: 'Home',
                        group: 'Insert',
                        mixin: utils.mixin({
                            item: item,
                            quick: true
                        }, defaultMixin)
                    }));
                    if (isContainer) {
                        addItems(path, item.items);
                    }
                }
            }

            addItems(rootAction, newBlockActions);
            ////////////////////////////////////////////////////////
            //
            //  Variables
            //
            var setVariableRoot = rootAction + '/Set Variable';

            function _getSetVariableActions(permissions, owner) {
                var variables = thiz.blockScope.getVariables();
                var result = [];
                for (var i = 0; i < variables.length; i++) {
                    var variable = variables[i];
                    result.push(thiz.createAction({
                        label: variable.name,
                        command: '' + setVariableRoot + '/' + variable.name,
                        icon: 'fa-paper-plane',
                        tab: 'Home',
                        group: 'Step',
                        mixin: utils.mixin({
                            item: variable,
                            proto: VariableAssignmentBlock,
                            custom: true,
                            ctrArgs: {
                                variable: variable.id,
                                variableId: variable.id,
                                scope: thiz.blockScope,
                                value: '0'
                            }
                        }, defaultMixin)
                    }));
                }
                return result;
            }

            result.push(thiz.createAction({
                label: 'Set Variable',
                command: '' + setVariableRoot,
                icon: 'fa-paper-plane',
                tab: 'Home',
                group: 'Step',
                mixin: utils.mixin({
                    global: true,
                    quick: true,
                    getActions: _getSetVariableActions
                }, defaultMixin)
            }));
            result.push(thiz.createAction({
                label: 'None',
                command: '' + setVariableRoot + '/None',
                icon: 'fa-paper-plane',
                tab: 'Home',
                group: 'Step',
                mixin: utils.mixin({
                    proto: VariableAssignmentBlock,
                    item: VariableAssignmentBlock,
                    quick: true,
                    ctrArgs: {
                        scope: this.blockScope,
                        value: '0'
                    }
                }, defaultMixin)
            }));

            this._emit('onAddActions', {
                actions: result,
                permissions: permissions,
                store: actionStore
            });
            return result;
        }

    });
    Module.BLOCK_INSERT_ROOT_COMMAND = BLOCK_INSERT_ROOT_COMMAND;
    return Module;
});