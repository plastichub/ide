//>>built
define("xblox/model/Referenced",["dcl/dcl","dojo/_base/declare","xide/mixins/ReferenceMixin","xide/utils"],function(d,a,b,e){var c={reference:null,deserialize:function(a){if(!a||0==a.length)return{};try{return e.fromJson(a)}catch(f){return{}}}};a=a("xblox.model.Referenced",[b],c);a.dcl=d(b.dcl,c);return a});
//# sourceMappingURL=Referenced.js.map