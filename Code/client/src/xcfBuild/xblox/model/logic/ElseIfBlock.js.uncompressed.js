define("xblox/model/logic/ElseIfBlock", [
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/Contains"
], function (dcl, Block, Contains) {
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The ElseIf Block model. Each ElseIf block contains a condition and a consequent to be run if the condition
    //          is true
    //
    //      This block should have an "IfBlock" parent

    // module:
    //		xblox.model.logic.ElseIfBlock
    return dcl([Block, Contains], {
        declaredClass: "xblox.model.logic.ElseIfBlock",
        //  condition: (String) expression to be evaluated
        condition: "",
        //  consequent: (Block) block to be run if the condition is true
        consequent: null,
        name: 'else if',
        icon: '',
        solve: function (scope, settings) {
            if (this._checkCondition(scope)) {
                return this._solve(scope, settings)
            }
            return false;
        },
        toText: function () {
            return "<span class='text-primary'>" + this.getBlockIcon('E') + this.name + " </span>" + "<span class='text-warning small'>" + (this.condition || "") + "<span>";
        },
        // checks the ElseIf condition
        _checkCondition: function (scope) {
            if (this.condition !== null) {
                return scope.parseExpression(this.condition);
            }
            return false;
        },
        getFields: function () {
            var thiz = this;
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(
                this.utils.createCI('condition', this.types.ECIType.EXPRESSION_EDITOR, this.condition, {
                    group: 'General',
                    title: 'Expression',
                    dst: 'condition',
                    delegate: {
                        runExpression: function (val, run, error) {
                            return thiz.scope.expressionModel.parse(thiz.scope, val, false, run, error);
                        }
                    }
                })
            );
            return fields;
        }
    });
});