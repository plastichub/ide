define("xblox/model/events/OnEvent", [
    'dcl/dcl',
    "dojo/_base/lang",
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xblox/model/Referenced',
    'xide/registry',
    'dojo/on',
    'xwire/_Base'
], function(dcl,lang,Deferred,Block,utils,types,EventedMixin,Referenced,registry,on,_Base){




    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    return dcl([Block,EventedMixin.dcl,Referenced.dcl,_Base],{
        declaredClass:"xblox.model.events.OnEvent",
        //method: (String)
        //  block action name
        name:'On Event',
        event:'',
        reference:'',
        references:null,
        sharable:true,
        _didSubscribe:false,
        filterPath:"item.name",
        filterValue:"",
        valuePath:"item.value",
        _nativeEvents:[
            "onclick",
            "ondblclick",
            "onmousedown",
            "onmouseup",
            "onmouseover",
            "onmousemove",
            "onmouseout",
            "onkeypress",
            "onkeydown",
            "onkeyup",
            "onfocus",
            "onblur",
            "onchange"
        ],

        stop:function(){

            this._destroy();

        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        solve:function(scope,settings,isInterface,error) {

            if(isInterface){
                this._destroy();
            }

            settings = this._lastSettings = settings || this._lastSettings;

            if(!this._didSubscribe){
                this._registerEvent(this.event);
                this.onSuccess(this, settings);
                return false;
            }

            this.onSuccess(this, settings);

            this._currentIndex=0;
            this._return=[];

            var ret=[], items = this[this._getContainer()];
            if(items.length) {
                //console.log('solve ',settings);
                var res = this.runFrom(items,0,settings);
                this.onSuccess(this, settings);
                return res;
            }else{
                this.onSuccess(this, settings);
            }
            return ret;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){
            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if(this.event){
                result+= this.event;
            }
            return result;
        },

        //  standard call from interface
        canAdd:function(){
            return [];
        },

        //  standard call for editing
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz=this;

            var _ref = this.deserialize(this.reference);
            var isNative = utils.contains(this._nativeEvents,this.event)>-1;
            var options = null;
            if(!isNative){
                options = this.scope.getEventsAsOptions(this.event);
            }else{

                options = [
                    {label:"onclick", value:"onclick"},
                    {label:"ondblclick",value:"ondblclick"},
                    {label:"onmousedown",value:"onmousedown"},
                    {label:"onmouseup",value:"onmouseup"},
                    {label:"onmouseover",value:"onmouseover"},
                    {label:"onmousemove",value:"onmousemove"},
                    {label:"onmouseout",value:"onmouseout"},
                    {label:"onkeypress",value:"onkeypress"},
                    {label:"onkeydown",value:"onkeydown"},
                    {label:"onkeyup",  value:"onkeyup"},
                    {label:"onfocus",  value:"onfocus"},
                    {label:"onblur",  value:"onblur"},
                    {label:"onchange",  value:"onchange"}
                ];

                //select the event we are listening to
                for (var i = 0; i < options.length; i++) {
                    var obj = options[i];
                    if(obj.value===this.event){
                        obj.selected=true;
                        break;
                    }
                }
            }


            fields.push(utils.createCI('Event',types.ECIType.ENUMERATION,this.event,{
                group:'General',
                options:options,
                dst:'event',
                widget:{
                    search:true
                }
            }));

            fields.push(utils.createCI('Filter Path',13,this.filterPath,{
                group:'General',
                dst:'filterPath'
            }));

            fields.push(utils.createCI('Filter Value',13,this.filterValue,{
                group:'General',
                dst:'filterValue'
            }));

            fields.push(utils.createCI('Value Path',13,this.valuePath,{
                group:'General',
                dst:'valuePath'
            }));


            fields.push(utils.createCI('Object/Widget',types.ECIType.WIDGET_REFERENCE,this.reference,{
                group:'Widget',
                dst:'reference',
                value:this.reference
            }));
            return fields;
        },

        getBlockIcon:function(){
            return '<span class="fa-bell"></span>';
        },
        onEvent:function(evt){

            this._lastResult=evt;

            /*
            if(this.scope && evt.scope && evt.scope!==this.scope){
                return;
            }*/

            if(this.filterPath && this.filterValue){
                var value = this.getValue(evt,this.filterPath);
                if(value && this.filterValue !==value){
                    return;
                }
            }

            var eventValue = null;
            if(this.valuePath){

                if(!this._lastSettings){
                    this._lastSettings = {};
                }
                eventValue = this.getValue(evt,this.valuePath);
                if(eventValue!==null){
                    !this._lastSettings.override && (this._lastSettings.override = {});
                    this._lastSettings.override.args = [eventValue];
                }
            }

            //console.log('on event ',this._lastSettings);
            this.solve(this.scope,this._lastSettings);
        },
        _subscribe:function(evt,handler,obj){

            if(!evt){
                return;
            }
            var isNative = utils.contains(this._nativeEvents,evt);
            if(isNative==-1){

                if(this.__events && this.__events[evt]) {
                    var _handles = this.__events[evt];

                    _.each(_handles, function (e) {
                        this.unsubscribe(e.type, e.handler);
                        e.remove();
                    }, this);

                    _.each(_handles, function (e) {
                        this.__events[evt].remove(e);
                    }, this);
                }

                this.subscribe(evt, this.onEvent);
            }else{

                if(obj) {
                    var _event = evt.replace('on', ''),
                        thiz = this;

                    var handle = on(obj, _event, function (e) {
                        thiz.onEvent(e)
                    });
                    console.log('wire native event : ' + _event);
                    this._events.push(handle);
                }

            }

        },
        _registerEvent:function(evt){

            try {
                if (!evt || !evt.length) {
                    return;
                }
                console.log('register event : ' + evt + ' for ' + this.reference);
                var objects = this.resolveReference(this.deserialize(this.reference));
                var thiz = this;
                if (objects && objects.length) {
                    for (var i = 0; i < objects.length; i++) {
                        var obj = objects[i];

                        //try widget
                        if (obj && obj.id) {
                            var _widget = registry.byId(obj.id);
                            if (_widget && _widget.on) {
                                var _event = this.event.replace('on', '');
                                console.log('found widget : ' + obj.id + ' will register event ' + _event);
                                var _handle = _widget.on(_event, lang.hitch(this, function (e) {
                                    console.log('event triggered : ' + thiz.event);
                                    thiz.onEvent(e);
                                }));
                                this._events.push(_handle);
                            } else {

                                this._subscribe(evt, this.onEvent, obj);
                            }
                        } else {

                            this._subscribe(evt, this.onEvent, obj);
                        }
                    }
                    console.log('objects found : ', objects);
                } else {
                    this._subscribe(evt, this.onEvent);
                }
            }catch(e){
                logError(e,'registering event failed');
            }
            this._didSubscribe=evt;
        },
        onLoad:function(){
            this._onLoaded=true;
            if(this.event && this.event.length && this.enabled){
                this._registerEvent(this.event);
            }
        },
        updateEventSelector:function(objects,cis){

            var options = [];

            if(!objects || !objects.length){
                options= this.scope.getEventsAsOptions(this.event);
            }else{

                options = [{label:"onclick", value:"onclick"},
                    {label:"ondblclick",value:"ondblclick"},
                    {label:"onmousedown",value:"onmousedown"},
                    {label:"onmouseup",value:"onmouseup"},
                    {label:"onmouseover",value:"onmouseover"},
                    {label:"onmousemove",value:"onmousemove"},
                    {label:"onmouseout",value:"onmouseout"},
                    {label:"onkeypress",value:"onkeypress"},
                    {label:"onkeydown",value:"onkeydown"},
                    {label:"onkeyup",  value:"onkeyup"},
                    {label:"onfocus",  value:"onfocus"},
                    {label:"onblur",  value:"onblur"},
                    {label:"onchange",  value:"onchange"}];

                //select the event we are listening to
                for (var i = 0; i < options.length; i++) {
                    var obj = options[i];
                    if(obj.value===this.event){
                        obj.selected=true;
                        break;
                    }
                }
            }

            for (var i = 0; i < cis.length; i++) {
                var ci = cis[i];
                if(ci['widget'] && ci['widget'].title==='Event'){
                    //console.log('event!');
                    var widget = ci['_widget'];
                    widget.nativeWidget.set('options',options);
                    widget.nativeWidget.reset();
                    widget.nativeWidget.set('value',this.event);
                    this.publish(types.EVENTS.RESIZE,{});
                    break;
                }
            }
        },
        onReferenceChanged:function(newValue,cis){

            this._destroy();//unregister previous event(s)

            this.reference = newValue;
            var objects = this.resolveReference(this.deserialize(newValue));
            this.updateEventSelector(objects,cis);
            this._registerEvent(this.event);

        },
        onChangeField:function(field,newValue,cis){

            if(field=='event'){
                this._destroy();    //unregister previous event
                if(this._onLoaded){ // we've have been activated at load time, so re-register our event
                    this.event = newValue;
                    this._registerEvent(newValue);
                }
            }
            if(field=='reference'){
                this.onReferenceChanged(newValue,cis);
            }

            this.inherited(arguments);
        },
        activate:function(){
            this._destroy();//you never know
            this._registerEvent(this.event);
        },
        deactivate:function(){
            this._destroy();
        },
        _destroy:function(){

            if(!this._events){this._events=[];}
            _.each(this._events, dojo.unsubscribe);
            this.unsubscribe(this.event,this.onEvent);
            this._lastResult=null;
            this._didSubscribe = false;
        }
    });
});