/** @module xblox/model/Block **/
define("xblox/model/Block", [
    'dcl/dcl',
    "dojo/Deferred",
    "dojo/_base/lang",
    "dojo/has",
    "xblox/model/ModelBase",
    "xide/factory",
    "xide/utils",
    "xide/types",
    "xide/utils/ObjectUtils",
    "xide/lodash",
    "xdojo/has!xblox-ui?xblox/model/Block_UI"
], function (dcl, Deferred, lang, has, ModelBase, factory, utils, types, ObjectUtils, _, Block_UI) {

    var bases = [ModelBase];

    function index(what, items) {
        for (var j = 0; j < items.length; j++) {
            if (what.id === items[j].id) {
                return j;
            }
        }
    }

    function compare(left, right) {
        return index(left) - index(right);
    }

    if (! true ) {
        bases.push(dcl(null, {
            getStatusIcon: function () {},
            getStatusClass: function () {},
            setStatusClass: function () {},
            onActivity: function () {},
            onRun: function () {},
            onFailed: function () {},
            onSuccess: function () {},
            getIconClass: function () {}
        }));
    }
    if (Block_UI) {
        bases.push(Block_UI);
    }

    /***
     * First things first, extend the core types for block flags:
     */
    utils.mixin(types, {
        /**
         * Flags to describe flags of the inner state of a block which might change upon the optimization. It also
         * contains some other settings which might be static, default or changed by the UI(debugger, etc...)
         *
         * @enum {integer} module:xide/types/BLOCK_FLAGS
         * @memberOf module:xide/types
         */
        BLOCK_FLAGS: {
            NONE: 0x00000000, // Reserved for future use
            ACTIVE: 0x00000001, // This behavior is active
            SCRIPT: 0x00000002, // This behavior is a script
            RESERVED1: 0x00000004, // Reserved for internal use
            USEFUNCTION: 0x00000008, // Block uses a function and not a graph
            RESERVED2: 0x00000010, // Reserved for internal use
            SINGLE: 0x00000020, // Only this block will excecuted, child blocks not.
            WAITSFORMESSAGE: 0x00000040, // Block is waiting for a message to activate one of its outputs
            VARIABLEINPUTS: 0x00000080, // Block may have its inputs changed by editing them
            VARIABLEOUTPUTS: 0x00000100, // Block may have its outputs changed by editing them
            VARIABLEPARAMETERINPUTS: 0x00000200, // Block may have its number of input parameters changed by editing them
            VARIABLEPARAMETEROUTPUTS: 0x00000400, // Block may have its number of output parameters changed by editing them
            TOPMOST: 0x00004000, // No other Block includes this one
            BUILDINGBLOCK: 0x00008000, // This Block is a building block (eg: not a transformer of parameter operation)
            MESSAGESENDER: 0x00010000, // Block may send messages during its execution
            MESSAGERECEIVER: 0x00020000, // Block may check messages during its execution
            TARGETABLE: 0x00040000, // Block may be owned by a different object that the one to which its execution will apply
            CUSTOMEDITDIALOG: 0x00080000, // This Block have a custom Dialog Box for parameters edition .
            RESERVED0: 0x00100000, // Reserved for internal use.
            EXECUTEDLASTFRAME: 0x00200000, // This behavior has been executed during last process. (Available only in profile mode )
            DEACTIVATENEXTFRAME: 0x00400000, // Block will be deactivated next frame
            RESETNEXTFRAME: 0x00800000, // Block will be reseted next frame

            INTERNALLYCREATEDINPUTS: 0x01000000, // Block execution may create/delete inputs
            INTERNALLYCREATEDOUTPUTS: 0x02000000, // Block execution may create/delete outputs
            INTERNALLYCREATEDINPUTPARAMS: 0x04000000, // Block execution may create/delete input parameters or change their type
            INTERNALLYCREATEDOUTPUTPARAMS: 0x08000000, // Block execution may create/delete output parameters or change their type
            INTERNALLYCREATEDLOCALPARAMS: 0x40000000, // Block execution may create/delete local parameters or change their type

            ACTIVATENEXTFRAME: 0x10000000, // Block will be activated next frame
            LOCKED: 0x20000000, // Block is locked for utilisation in xblox
            LAUNCHEDONCE: 0x80000000 // Block has not yet been launched...
        },

        /**
         *  Mask for the messages the callback function of a block should be aware of. This goes directly in
         *  the EventedMixin as part of the 'emits' chain (@TODO)
         *
         * @enum module:xide/types/BLOCK_CALLBACKMASK
         * @memberOf module:xide/types
         */
        BLOCK_CALLBACKMASK: {
            PRESAVE: 0x00000001, // Emits PRESAVE messages
            DELETE: 0x00000002, // Emits DELETE messages
            ATTACH: 0x00000004, // Emits ATTACH messages
            DETACH: 0x00000008, // Emits DETACH messages
            PAUSE: 0x00000010, // Emits PAUSE messages
            RESUME: 0x00000020, // Emits RESUME messages
            CREATE: 0x00000040, // Emits CREATE messages
            RESET: 0x00001000, // Emits RESET messages
            POSTSAVE: 0x00000100, // Emits POSTSAVE messages
            LOAD: 0x00000200, // Emits LOAD messages
            EDITED: 0x00000400, // Emits EDITED messages
            SETTINGSEDITED: 0x00000800, // Emits SETTINGSEDITED messages
            READSTATE: 0x00001000, // Emits READSTATE messages
            NEWSCENE: 0x00002000, // Emits NEWSCENE messages
            ACTIVATESCRIPT: 0x00004000, // Emits ACTIVATESCRIPT messages
            DEACTIVATESCRIPT: 0x00008000, // Emits DEACTIVATESCRIPT messages
            RESETINBREAKPOINT: 0x00010000, // Emits RESETINBREAKPOINT messages
            RENAME: 0x00020000, // Emits RENAME messages
            BASE: 0x0000000E, // Base flags :attach /detach /delete
            SAVELOAD: 0x00000301, // Base flags for load and save
            PPR: 0x00000130, // Base flags for play/pause/reset
            EDITIONS: 0x00000C00, // Base flags for editions of settings or parameters
            ALL: 0xFFFFFFFF // All flags
        }

    });
    /**
     * Base block class.
     *
     * @class module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     * @lends module:xblox/model/Block_UI
     */
    var Block = dcl(bases, {
        declaredClass: "xblox.model.Block",
        scopeId: null,
        isCommand: false,
        outlet: 0,
        _destroyed: false,
        /**
         * Switch to include the block for execution.
         * @todo, move to block flags
         * @type {boolean}
         * @default true
         */
        enabled: true,
        /**
         * Switch to include the block for serialization.
         * @todo, move to block flags
         * @type {boolean}
         * @default true
         */
        serializeMe: true,
        /**
         * Name is used for the interface, mostly as prefix within
         * this.toText() which includes also the 'icon' (as icon font).
         * This should be unique and expressive.
         *
         * This field can be changed by the user. Examples
         * 'Run Script' will result in result 'Run Script + this.script'
         *
         * @todo: move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {string|null}
         * @default null
         * @required false
         */
        name: null,
        /**
         * @todo: same as name, move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {string}
         * @default 'No Description'
         * @required true
         */
        shareTitle: '',
        /**
         * The blocks internal user description
         * Description is used for the interface. This should be short and expressive and supports plain and html text.
         *
         * @todo: same as name, move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {boolean}
         * @default 'No Description'
         * @required true
         */
        sharable: false,
        /**
         * Container holding a 'child' blocks. Subclassing block might hold that somewhere else.
         * @type {Block[]}
         * @default null
         * @required false
         */
        items: null,
        /**
         * Parent up-link
         * @type {string|Block}
         * @default null
         * @required false
         */
        parent: null,
        /**
         * @var temporary variable to hold remainin blocks to run
         */
        _return: null,
        /**
         * @var temporary variable to store the last result
         */
        _lastResult: null,
        _deferredObject: null,
        _currentIndex: 0,
        _lastRunSettings: null,
        _onLoaded: false,
        beanType: 'BLOCK',
        override: {},
        /**
         * ignore these due to serialization
         */
        ignoreSerialize: [
            '_didSubscribe',
            '_currentIndex',
            '_deferredObject',
            '_destroyed',
            '_return',
            'parent',
            '__started',
            'ignoreSerialize',
            '_lastRunSettings',
            '_onLoaded',
            'beanType',
            'sharable',
            'override',
            'virtual',
            '_scenario',
            '_didRegisterSubscribers',
            'additionalProperties',
            'renderBlockIcon',
            'serializeMe',
            '_statusIcon',
            '_statusClass',
            'hasInlineEdits',
            '_loop',
            'help',
            'owner',
            '_lastCommand',
            'allowActionOverride',
            'canDelete',
            'isCommand',
            'lastCommand',
            'autoCreateElse',
            '_postCreated',
            '_counter'
        ],
        _parseString: function (string, settings, block, flags) {
            try {
                settings = settings || this._lastSettings || {};
                flags = flags || settings.flags || types.CIFLAG.EXPRESSION;
                var scope = this.scope;
                var value = string;
                var parse = !(flags & types.CIFLAG.DONT_PARSE);
                var isExpression = (flags & types.CIFLAG.EXPRESSION);
                if (flags & types.CIFLAG.TO_HEX) {
                    value = utils.to_hex(value);
                }
                if (parse !== false) {
                    value = utils.convertAllEscapes(value, "none");
                }
                var override = settings.override || this.override || {};
                var _overrides = (override && override.variables) ? override.variables : null;
                var res = "";
                if (isExpression && parse !== false) {
                    res = scope.parseExpression(value, null, _overrides, null, null, null, override.args, flags);
                } else {
                    res = '' + value;
                }
            } catch (e) {
                console.error(e);
            }
            return res;
        },
        postCreate: function () {},
        /**
         *
         * @param clz
         * @returns {Array}
         */
        childrenByClass: function (clz) {
            var all = this.getChildren();
            var out = [];
            for (var i = 0; i < all.length; i++) {
                var obj = all[i];
                if (obj.isInstanceOf(clz)) {
                    out.push(obj);
                }
            }
            return out;
        },
        /**
         *
         * @param clz
         * @returns {Array}
         */
        childrenByNotClass: function (clz) {
            var all = this.getChildren();
            var out = [];
            for (var i = 0; i < all.length; i++) {
                var obj = all[i];
                if (!obj.isInstanceOf(clz)) {
                    out.push(obj);
                }
            }
            return out;
        },
        /**
         *
         * @returns {null}
         */
        getInstance: function () {
            var instance = this.scope.instance;
            if (instance) {
                return instance;
            }
            return null;
        },
        pause: function () {},
        mergeNewModule: function (source) {
            for (var i in source) {
                var o = source[i];
                if (o && _.isFunction(o)) {
                    this[i] = o; //swap
                }
            }
        },
        __onReloaded: function (newModule) {
            this.mergeNewModule(newModule.prototype);
            var _class = this.declaredClass;
            var _module = lang.getObject(utils.replaceAll('/', '.', _class)) || lang.getObject(_class);
            if (_module) {
                if (_module.prototype && _module.prototype.solve) {
                    this.mergeNewModule(_module.prototype);
                }
            }
        },
        reparent: function () {
            var item = this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            if (parent) {} else {
                var _next = item.next(null, 1) || item.next(null, -1);
                if (_next) {
                    item.group = null;
                    _next._add(item);
                }
            }
        },
        unparent: function (blockgroup, move) {
            var item = this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            if (parent && parent.removeBlock) {
                parent.removeBlock(item, false);
            }

            item.group = blockgroup;
            item.parentId = null;
            item.parent = null;
            if (move !== false) {
                item._place(null, -1, null);
                item._place(null, -1, null);
            }
        },
        move: function (dir) {
            var item = this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            var items = null;
            var store = item._store;
            if (parent) {
                items = parent[parent._getContainer(item)];
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }
                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var upperItem = items[cIndex + (dir)];
                if (!upperItem) {
                    return false;
                }
                items[cIndex + (dir)] = item;
                items[cIndex] = upperItem;
                return true;
            } else {
                item._place(null, dir);
                return true;
            }
        },
        _place: function (ref, direction, items) {
            var store = this._store,
                dst = this;
            ref = ref || dst.next(null, direction);
            if (!ref) {
                console.error('have no next', this);
                return;
            }
            ref = _.isString(ref) ? store.getSync(ref) : ref;
            dst = _.isString(dst) ? store.getSync(dst) : dst;
            items = items || store.storage.fullData;
            direction = direction == -1 ? 0 : 1;
            items.remove(dst);
            if (direction == -1) {
                direction = 0;
            }
            items.splice(Math.max(index(ref, items) + direction, 0), 0, dst);
            store._reindex();
        },
        index: function () {
            var item = this,
                parent = item.getParent(),
                items = null,
                group = item.group,
                store = this._store;

            if (parent) {
                items = parent[parent._getContainer(item)] || [];
                items = items.filter(function (item) {
                    return item.group === group;
                });
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }
                return this.indexOf(items, item);
            } else {
                items = store.storage.fullData;
                items = items.filter(function (item) {
                    return item.group === group;
                });
                return this.indexOf(items, item);
            }
        },
        numberOfParents: function () {
            var result = 0;
            var parent = this.getParent();
            if (parent) {
                result++;
                result += parent.numberOfParents();
            }
            return result;
        },
        getTopRoot: function () {
            var last = this.getParent();
            if (last) {
                var next = last.getParent();
                if (next) {
                    last = next;
                }
            }
            return last;
        },
        next: function (items, dir) {
            items = items || this._store.storage.fullData;

            function _next(item, items, dir, step, _dstIndex) {
                var start = item.indexOf(items, item);
                var upperItem = items[start + (dir * step)];
                if (upperItem) {
                    if (!upperItem.parentId && upperItem.group && upperItem.group === item.group) {
                        _dstIndex = start + (dir * step);
                        return upperItem;
                    } else {
                        step++;
                        return _next(item, items, dir, step, _dstIndex);
                    }
                }
                return null;
            }
            return _next(this, items, dir, 1, 0);
        },
        /**
         *
         * @param createRoot
         * @returns {module:xblox/model/Block|null}
         */
        getParent: function (createRoot) {
            if (this.parentId) {
                return this.scope.getBlockById(this.parentId);
            }
        },
        getScope: function () {
            var scope = this.scope;
            if (this.scopeId && this.scopeId.length > 0) {
                var owner = scope.owner;
                if (owner && owner.hasScope) {
                    if (owner.hasScope(this.scopeId)) {
                        scope = owner.getScope(this.scopeId);
                    } else {
                        console.error('have scope id but cant resolve it', this);
                    }
                }
            }
            return scope;
        },
        /**
         *
         * @returns {null}
         */
        canAdd: function () {
            return null;
        },
        getTarget: function () {
            var _res = this._targetReference;
            if (_res) {
                return _res;
            }
            var _parent = this.getParent();
            if (_parent && _parent.getTarget) {
                _res = _parent.getTarget();
            }
            return _res;
        },
        // adds array2 at the end of array1 => useful for returned "solve" commands
        addToEnd: function (array1, array2) {
            if (array2 && array1.length != null && array2.length != null) {
                array1.push.apply(array1, array2);
            }
            return array1;
        },
        /**
         *
         * @param what
         * @param del delete block
         */
        removeBlock: function (what, del) {
            if (what) {
                if (del !== false && what.empty) {
                    what.empty();
                }
                if (del !== false) {
                    delete what.items;
                }
                what.parent = null;
                what.parentId = null;
                if (this.items) {
                    this.items.remove(what);
                }
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Accessors
        //
        /////////////////////////////////////////////////////////////////////////////////////
        _getContainer: function (item) {
            return 'items';
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        empty: function (what) {
            try {
                this._empty(what)
            } catch (e) {

                debugger;
            }
        },
        /*
         * Empty : removes all child blocks, recursively
         * @param proto : prototype|instance
         * @param ctrArgs
         * @returns {*}
         */
        _empty: function (what) {
            var data = what || this.items;
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var subBlock = data[i];

                    if (subBlock && subBlock.empty) {
                        subBlock.empty();
                    }
                    if (subBlock && this.scope && this.scope.blockStore) {
                        this.scope.blockStore.remove(subBlock.id);
                    }
                }
            }
        },
        /**
         * This was needed. FF bug.
         * @param data
         * @param obj
         * @returns {boolean}
         */
        containsItem: function (data, obj) {
            var i = data.length;
            while (i--) {
                if (data[i].id === obj.id) {
                    return true;
                }
            }
            return false;
        },
        /**
         * This was needed. FF bug
         * @param data
         * @param obj
         * @returns {*}
         */
        indexOf: function (data, obj) {
            var i = data.length;
            while (i--) {
                if (data[i].id === obj.id) {
                    return i;
                }
            }
            return -1;
        },
        _getBlock: function (dir) {
            var item = this;
            if (!item || !item.parentId) {
                return false;
            }
            //get parent
            var parent = this.scope.getBlockById(item.parentId);
            if (!parent) {
                return null;
            }
            var items = parent[parent._getContainer(item)];
            if (!items || items.length < 2 || !this.containsItem(items, item)) {
                return null;
            }
            var cIndex = this.indexOf(items, item);
            if (cIndex + (dir) < 0) {
                return false;
            }
            var upperItem = items[cIndex + (dir)];
            if (upperItem) {
                return upperItem;
            }
            return null;
        },
        getPreviousBlock: function () {
            return this._getBlock(-1);
        },
        getNextBlock: function () {
            return this._getBlock(1);
        },
        _getPreviousResult: function () {
            var parent = this.getPreviousBlock() || this.getParent();
            if (parent && parent._lastResult != null) {
                if (this.isArray(parent._lastResult)) {
                    return parent._lastResult;
                } else {
                    return [parent._lastResult];
                }
            }
            return null;
        },
        getPreviousResult: function () {
            var parent = null;
            var prev = this.getPreviousBlock();
            if (!prev || !prev._lastResult || !prev.enabled) {
                parent = this.getParent();
            } else {
                parent = prev;
            }

            if (parent && !parent._lastResult) {
                var _newParent = parent.getParent();
                if (_newParent) {
                    parent = _newParent;
                }
            }

            if (parent && parent._lastResult != null) {
                if (this.isArray(parent._lastResult)) {
                    return parent._lastResult;
                } else {
                    return parent._lastResult;
                }
            }
            return null;
        },
        _getArg: function (val, escape) {
            var _float = parseFloat(val);
            if (!isNaN(_float)) {
                return _float;
            } else {
                if (val === 'true' || val === 'false') {
                    return val === 'true';
                } else if (val && escape && _.isString(val)) {
                    return '\'' + val + '\'';
                }
                return val;
            }
        },
        /**
         *
         * @returns {Array}
         */
        getArgs: function (settings) {
            var result = [];
            settings = settings || {};
            var _inArgs = settings.args || this._get('args');
            if (settings.override && settings.override.args) {
                _inArgs = settings.override.args;
            }
            if (_inArgs) { //direct json
                result = utils.getJson(_inArgs, null, false);
            }
            //try comma separated list
            if (result && result.length == 0 && _inArgs && _inArgs.length && _.isString(_inArgs)) {

                if (_inArgs.indexOf(',') !== -1) {
                    var splitted = _inArgs.split(',');
                    for (var i = 0; i < splitted.length; i++) {
                        //try auto convert to number
                        var _float = parseFloat(splitted[i]);
                        if (!isNaN(_float)) {
                            result.push(_float);
                        } else {
                            if (splitted[i] === 'true' || splitted[i] === 'false') {
                                result.push(utils.toBoolean(splitted[i]));
                            } else {
                                result.push(splitted[i]); //whatever
                            }
                        }
                    }
                    return result;
                } else {
                    result = [this._getArg(_inArgs)]; //single argument
                }
            }

            !_.isArray(result) && (result = []);
            //add previous result
            var previousResult = this.getPreviousResult();
            if (previousResult != null) {
                if (_.isArray(previousResult) && previousResult.length == 1) {
                    result.push(previousResult[0]);
                } else {
                    result.push(previousResult);
                }
            }

            return result || [_inArgs];
        },
        /*
         * Remove : as expected, removes a block
         * @param proto : prototype|instance
         * @param ctrArgs
         * @returns {*}
         */
        remove: function (what) {
            this._destroyed = true;
            if (this.parentId != null && this.parent == null) {
                this.parent = this.scope.getBlockById(this.parentId);
            }
            if (this.parent && this.parent.removeBlock) {
                this.parent.removeBlock(this);
                return;
            }
            what = what || this;
            if (what) {
                if (what.empty) {
                    what.empty();
                }
                delete what.items;
                what.parent = null;
                if (this.items) {
                    this.items.remove(what);
                }
            }

        },
        prepareArgs: function (ctorArgs) {
            if (!ctorArgs) {
                ctorArgs = {};
            }
            //prepare items
            if (!ctorArgs['id']) {
                ctorArgs['id'] = this.createUUID();
            }
            if (!ctorArgs['items']) {
                ctorArgs['items'] = [];
            }
        },
        /**
         * Private add-block function
         * @param proto
         * @param ctrArgs
         * @param where
         * @param publish
         * @returns {*}
         * @private
         */
        _add: function (proto, ctrArgs, where, publish) {
            var block = null;
            try {
                //create or set
                if (ctrArgs) {
                    //use case : normal object construction
                    this.prepareArgs(ctrArgs);
                    block = factory.createBlock(proto, ctrArgs, null, publish);
                } else {
                    //use case : object has been created so we only do the leg work
                    if (ctrArgs == null) {
                        block = proto;
                    }
                    //@TODO : allow use case to use ctrArgs as mixin for overriding
                }
                ///////////////////////
                //  post work

                //inherit scope
                block.scope = this.scope;
                //add to scope
                if (this.scope) {
                    block = this.scope.registerBlock(block, publish);
                }
                if ( false ) {
                    if (block.id === this.id) {
                        console.error('adding new block to our self');
                        debugger;
                    }
                }
                block.parent = this;
                block.parentId = this.id;
                block.scope = this.scope;

                var container = where || this._getContainer();
                if (container) {
                    if (!this[container]) {
                        this[container] = [];
                    }
                    var index = this.indexOf(this[container], block);
                    if (index !== -1) {
                        console.error(' have already ' + block.id + ' in ' + container);
                    } else {
                        if (this.id == block.id) {
                            console.error('tried to add our self to ' + container);
                            return;
                        }
                        this[container].push(block);
                    }
                }
                block.group = null;
                return block;
            } catch (e) {
                logError(e, '_add');
            }
            return null;

        },
        getStore: function () {
            return this.getScope().getStore();
        },
        /**
         * Public add block function
         * @param proto {}
         * @param ctrArgs
         * @param where
         * @returns {*}
         */
        add: function (proto, ctrArgs, where) {
            var block = this._add(proto, ctrArgs, where);
            return block.getStore().getSync(block.id);
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Run 
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getContext: function () {
            if (this.scope.instance && this.scope.instance) {
                return this.scope.instance;
            }
            return null;
        },
        resolved: function () {
            if (this._deferredObject) {
                this._deferredObject.resolve();
                delete this._deferredObject;
            }
        },
        /***
         * Solves all the commands into items[]
         *
         * @param manager   =>  BlockManager
         * @return  list of commands to send
         */
        _solve: function (scope, settings) {
            settings = settings || {
                highlight: false
            };
            var ret = [];
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];
                this.addToEnd(ret, block.solve(scope, settings));
            }
            return ret;
        },
        /***
         * Solves all the commands into items[]
         *
         * @param manager   =>  BlockManager
         * @return  list of commands to send
         */
        solve: function (scope, settings) {
            settings = settings || {
                highlight: false
            };
            var ret = [];
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];
                this.addToEnd(ret, block.solve(scope, settings));
            }
            return ret;
        },
        /***
         * Solves all the commands into items[]
         *
         * @param manager   =>  BlockManager
         * @return  list of commands to send
         */
        solveMany: function (scope, settings) {
            if (!this._lastRunSettings && settings) {
                this._lastRunSettings = settings;
            }
            settings = this._lastRunSettings || settings;
            this._currentIndex = 0;
            this._return = [];
            var ret = [],
                items = this[this._getContainer()];

            if (items.length) {
                var res = this.runFrom(items, 0, settings);
                this.onSuccess(this, settings);
                return res;
            } else {
                this.onSuccess(this, settings);
            }
            return ret;
        },
        runFrom: function (blocks, index, settings) {
            var thiz = this;
            blocks = blocks || this.items;
            if (!this._return) {
                this._return = [];
            }
            var onFinishBlock = function (block, results) {
                block._lastResult = block._lastResult || results;
                thiz._currentIndex++;
                thiz.runFrom(blocks, thiz._currentIndex, settings);
            };
            var wireBlock = function (block) {
                block._deferredObject.then(function (results) {
                    onFinishBlock(block, results);
                });
            };

            if (blocks.length) {
                for (var n = index; n < blocks.length; n++) {
                    var block = blocks[n];
                    if (block.deferred === true) {
                        block._deferredObject = new Deferred();
                        this._currentIndex = n;
                        wireBlock(block);
                        this.addToEnd(this._return, block.solve(this.scope, settings));
                        break;
                    } else {
                        this.addToEnd(this._return, block.solve(this.scope, settings));
                    }
                }
            } else {
                this.onSuccess(this, settings);
            }
            return this._return;
        },
        serializeField: function (name) {
            return this.ignoreSerialize.indexOf(name) == -1; //is not in our array
        },
        onLoad: function () {},
        activate: function () {},
        deactivate: function () {},
        _get: function (what) {
            if (this.override) {
                return (what in this.override ? this.override[what] : this[what]);
            }
        },
        onDidRun: function () {
            if (this.override) {
                this.override.args && delete this.override.args;
                delete this.override;
            }
        },
        destroy: function () {
            this.stop(true);
            this.reset();
            this._destroyed = true;
            delete this.virtual;
        },
        reset: function () {
            this._lastSettings = {};
            clearTimeout(this._loop);
            this._loop = null;
            delete this.override;
            this.override = null;
            delete this._lastResult;
            this.override = {};
        },
        stop: function () {
            this.reset();
            this.getItems && _.invoke(this.getItems(), 'stop');
        }

    });
    Block.FLAGS = types.BLOCK_FLAGS;
    Block.EMITS = types.BLOCK_CALLBACKMASK;
    //that's really weird: using dynamic base classes nor Block.extend doesnt work.
    //however, move dojo complete out of blox
    if (!Block.prototype.onSuccess) {
        Block.prototype.onSuccess = function () {};
        Block.prototype.onRun = function () {};
        Block.prototype.onFailed = function () {};
    }
    dcl.chainAfter(Block, 'stop');
    dcl.chainAfter(Block, 'destroy');
    dcl.chainAfter(Block, 'onDidRun');
    return Block;
});