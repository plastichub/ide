/** @module xblox/model/variables/Variable */
define("xblox/model/variables/Variable", [
    'dcl/dcl',
    'xide/types',
    "xblox/model/Block"
], function(dcl,types,Block){
    /**
     *  The command model. A 'command' consists out of a few parameters and a series of
     *  expressions. Those expressions need to be evaluated before send them to the device
     *
     * @class module:xblox.model.variables.Variable
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     */
    return dcl(Block,{
        declaredClass:"xblox.model.variables.Variable",
        //name: String
        //  the variable's name, it should be unique within a scope
        name:null,

        //value: Current variable value
        value:null,

        register:true,

        readOnly:false,

        initial:null,
        
        isVariable:true,
        flags: 0x000001000,
        getValue:function(){
            return this.value;
        },
        canDisable:function(){
            return false;
        },
        canMove:function(){
            return false;
        },
        getIconClass:function(){
            return 'el-icon-quotes-alt';
        },
        getBlockIcon:function(){
            return '<span class="'+this.icon+'"></span> ';
        },
        toText:function(){
            return "<span class='text-primary'>" + this.getBlockIcon() +  this.makeEditable('name','right','text','Enter a unique name','inline') +"</span>";
        },
        solve:function(){

            var _result = this.scope.parseExpression(this.getValue(),true);
            //console.log('resolved variable ' + this.title + ' to ' + _result);
            return [];

        },
        getFields:function(){
            var fields = this.getDefaultFields();
            var thiz=this,
                defaultArgs = {
                    allowACECache:true,
                    showBrowser:false,
                    showSaveButton:true,
                    editorOptions:{
                        showGutter:false,
                        autoFocus:false,
                        hasConsole:false
                    },
                    aceOptions:{
                        hasEmmet:false,
                        hasLinking:false,
                        hasMultiDocs:false
                    },
                    item:this
                };

            fields.push(this.utils.createCI('title',types.ECIType.STRING,this.name,{
                group:'General',
                title:'Name',
                dst:'name'
            }));

            fields.push(this.utils.createCI('value',types.ECIType.EXPRESSION,this.value,{
                group:'General',
                title:'Value',
                dst:'value',
                delegate:{
                    runExpression:function(val,run,error){
                        return thiz.scope.expressionModel.parse(thiz.scope,val,false,run,error);
                    }
                }
            }));

            


            //this.types.ECIType.EXPRESSION_EDITOR
            /*
            fields.push(this.utils.createCI('initial',this.types.ECIType.EXPRESSION,this.initial,{
                group:'General',
                title:'Initial',
                dst:'initial',
                widget:defaultArgs,
                delegate:{
                    runExpression:function(val,run,error){
                        if(thiz.group=='processVariables'){
                            var _val = thiz.scope.getVariable("value");
                            var extra = "";
                            if(_val) {
                                _val = _val.value;
                                if(!thiz.isNumber(_val)){
                                    _val = ''+_val;
                                    _val = "'" + _val + "'";
                                }
                                extra = "var value = " + _val +";\n";
                            }
                        }
                        return thiz.scope.expressionModel.parse(thiz.scope,extra + val,false,run,error);
                    }
                }
            }));
            */
            return fields;
        }
    });
});