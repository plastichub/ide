/** @module xblox/model/mqtt/Publish **/
define("xblox/model/mqtt/Publish", [
    'dcl/dcl',
    'xdojo/has',
    'xide/utils',
    'xide/types',
    'xcf/model/Command'
    //'xdojo/has!host-node?dojo/node!tracer',
    //'xdojo/has!host-node?nxapp/utils/_console'
], function (dcl, has, utils, types, Command, tracer, _console) {

    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */

    var isServer =  false ;
    var console = typeof window !== 'undefined' ? window.console : global.console;
    if (isServer && tracer && console && console.error) {
        console = _console;
    }

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    /**
     * Base block class.
     *
     * @class module:xblox/model/mqtt/Publish
     * @extends module:xblox/model/Block
     */
    return dcl(Command, {
        declaredClass: "xblox.model.mqtt.Publish",
        //method: (String)
        //  block action name
        name: 'Publish',
        //method: (String)
        //  block action name
        topic: '',
        args: '',
        deferred: false,
        sharable: true,
        context: null,
        icon: 'fa-send',
        isCommand: true,
        qos: 0,
        retain: false,
        /**
         * @type {string|null}
         */
        path: null,
        flags: 0,
        onData: function (message) {
            if (message && message.topic && message.topic == this.topic) {
                var thiz = this,
                    ctx = this.getContext(),
                    items = this[this._getContainer()];

                var settings = this._lastSettings;
                var ret = [];
                if (items.length > 0) {
                    var value = message;
                    this.path = 'value';
                    if (this.path && _.isObject(message)) {
                        value = utils.getAt(message, this.path, message);
                    }

                    for (var n = 0; n < this.items.length; n++) {
                        var block = this.items[n];
                        if (block.enabled) {
                            block.override = {
                                args: [value]
                            };
                            ret.push(block.solve(this.scope, settings));
                        }
                    }
                }
                this.onSuccess(this, this._lastSettings);
                return ret;
            }
        },
        observed: [
            'topic'
        ],
        getContext: function () {
            return this.context || (this.scope.getContext ? this.scope.getContext() : this);
        },
        /**
         *
         * @param scope
         * @param settings
         * @param isInterface
         * @returns {boolean}
         */
        solve: function (scope, settings, isInterface) {
            this._currentIndex = 0;
            this._return = [];
            settings = this._lastSettings = settings || this._lastSettings;

            var instance = this.getInstance();
            if (isInterface === true && this._loop) {
                this.reset();
            }

            var args = this.args;
            var inArgs = this.getArgs(settings);
            if (inArgs[0]) {
                args = inArgs[0];
            }

            if (instance) {
                var flags = settings.flags || this.flags;
                var parse = !(flags & types.CIFLAG.DONT_PARSE);
                var isExpression = (flags & types.CIFLAG.EXPRESSION);
                var value = utils.getJson(args, true, false);
                if (value === null || value === 0 || value === true || value === false || !_.isObject(value)) {
                    value = {
                        payload: this.args
                    };
                }
                var topic = scope.expressionModel.replaceVariables(scope, this.topic, false, false);
                
                if (value.payload && _.isString(value.payload) && parse) {
                    var override = settings.override || this.override || {};
                    var _overrides = (override && override.variables) ? override.variables : null;
                    if (parse !== false) {
                        value.payload = utils.convertAllEscapes(value.payload, "none");
                    }
                    if (isExpression && parse !== false) {
                        value.payload = scope.parseExpression(value.payload, null, _overrides, null, null, null, override.args);
                    }
                }
                instance.callMethod('publishTopic', utils.mixin({
                    topic: topic,
                    qos: this.qos,
                    retain: this.retain
                }, value), this.id, this.id);
            }

            settings = settings || {};
            this.onDidRun();
            this.onSuccess(this, settings);
            return true;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function (icon) {
            var out = '<span style="">' + this.getBlockIcon() + ' ' + this.makeEditable('name', 'top', 'text', 'Enter a name', 'inline') + ' :: ' + '</span>';
            if (this.topic) {
                out += this.makeEditable('topic', 'bottom', 'text', 'Enter a topic', 'inline');
                this.startup && (out += this.getIcon('fa-bell inline-icon text-warning', 'text-align:right;float:right;', ''));
                this.interval > 0 && (out += this.getIcon('fa-clock-o inline-icon text-warning', 'text-align:right;float:right', ''));
            }
            return out;
        },
        //  standard call from interface
        canAdd: function () {
            return [];
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(utils.createCI('qos', types.ECIType.ENUMERATION, this.qos, {
                group: 'General',
                title: 'QOS',
                dst: 'qos',
                widget: {
                    options: [
                        {
                            label: "0 - at most once",
                            value: 0
                        },
                        {
                            label: "1 - at least once",
                            value: 1
                        },
                        {
                            label: "2 - exactly once",
                            value: 2
                        }
                    ]
                }
            })
            );
            fields.push(utils.createCI('arguments', 27, this.args, {
                group: 'Arguments',
                title: 'Arguments',
                dst: 'args'
            }));
            fields.push(this.utils.createCI('flags', 5, this.flags, {
                group: 'Arguments',
                title: 'Flags',
                dst: 'flags',
                data: [
                    {
                        value: 0x000001000,
                        label: 'Dont parse',
                        title: "Do not parse the string and use it as is"
                    },
                    {
                        value: 0x00000800,//2048
                        label: 'Expression',
                        title: 'Parse it as Javascript'
                    }
                ],
                widget: {
                    hex: true
                }
            }));
            fields.push(utils.createCI('topic', types.ECIType.STRING, this.topic, {
                group: 'General',
                title: 'Topic',
                dst: 'topic',
                select: true
            }));
            fields.push(utils.createCI('retain', types.ECIType.BOOL, this.retain, {
                group: 'General',
                title: 'Retain',
                dst: 'retain'
            }));
            fields.remove(_.find(fields, {
                name: "send"
            }));

            fields.remove(_.find(fields, {
                name: "waitForResponse"
            }));
            return fields;
        }
    });
});