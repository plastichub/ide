define("xblox/model/transform/BezierTransform", [
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils'
], function(declare,lang,Deferred,Block,utils){
    return declare("xblox.model.transorm.Bezier",[Block],{
        name:'Run Script',
        method:'',
        args:'',
        deferred:false,
        sharable:true,
        context:null,
        description:"Transform a float value into another float value according to a 2d bezier curve shape, and given boundaries. ",
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){
            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if(this.method){
                result+= this.method.substr(0,50);
            }
            return result;
        },
        //  standard call from interface
        canAdd:function(){
            return [];
        },
        //  standard call for editing
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz=this;
            fields.push(
                this.utils.createCI('value',25,this.method,{
                    group:'General',
                    title:'Script',
                    dst:'method',
                    delegate:{
                        runExpression:function(val,run,error){
                            var old = thiz.method;
                            thiz.method=val;
                            var _res = thiz.solve(thiz.scope,null,run,error);
                        }
                    }
                }));
            fields.push(this.utils.createCI('value',27,this.args,{
                    group:'General',
                    title:'Arguments',
                    dst:'args'
                }));
            return fields;
        },
        getBlockIcon:function(){
            return '<span class="fa-code"></span>';
        }
    });
});