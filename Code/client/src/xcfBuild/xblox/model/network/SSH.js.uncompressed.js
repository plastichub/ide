define("xblox/model/network/SSH", [
    'dcl/dcl',
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xblox/model/Contains',
    'xide/types'
], function(dcl,Deferred,Block,utils,Contains,types){
    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    return dcl([Block,Contains],{
        declaredClass:"xblox.model.code.SSH",
        //method: (String)
        //  block action name
        name:'Run Script',
        //method: (String)
        //  block action name
        method:'',
        args:'',
        deferred:false,
        sharable:true,
        context:null,
        icon:'fa-code',
        description:"Runs a piece of code on a SSH connection",
        observed:[
            'method'
        ],
        getContext:function(){
            return this.context || (this.scope.getContext ?  this.scope.getContext() : this);
        },
        /***
         * Returns the block run result
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        /**
         *
         * @param scope
         * @param settings
         * @param run
         * @param error
         */
        solve:function(scope,settings,run,error){
            this._currentIndex = 0;
            this._return=[];
            var _script = '' + this._get('method');
            var thiz=this,
                ctx = this.getContext(),
                items = this[this._getContainer()],
                dfd = new Deferred(),
                isDfd = thiz.deferred;
            this.onRunThis(settings);
            var _function = new Function("{" + _script + "}");
            var _args = thiz.getArgs(settings) || [];
            try {

                if(isDfd){
                    ctx.resolve=function(result){
                        if(thiz._deferredObject) {
                            thiz._deferredObject.resolve();
                        }
                        thiz.onDidRunThis(dfd,result,items,settings);
                    }
                }
                var _parsed = _function.apply(ctx, _args || {});
                thiz._lastResult = _parsed;
                if (run) {
                    run('Expression ' + _script + ' evaluates to ' + _parsed);
                }
                if(!isDfd) {
                    thiz.onDidRunThis(dfd,_parsed,items,settings);
                }
                if (_parsed !== 'false' && _parsed !== false) {
                    thiz.onSuccess(thiz, settings);
                } else {
                    thiz.onFailed(thiz, settings);
                }
            } catch (e) {
                thiz.onDidRunItemError(dfd,e || {},settings);
                thiz.onFailed(thiz,settings);
                if (error) {
                    error('invalid expression : \n' + _script + ': ' + e);
                }
            }
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText:function(){
            var result = this.getBlockIcon() + ' ' + this.name + ' :: ';
            if(this.method){
                result+= this.method.substr(0,50);
            }
            return result;
        },
        //  standard call from interface
        canAdd:function(){
            return [];
        },
        //  standard call for editing
        getFields:function(){
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var thiz=this;
            fields.push(
                utils.createCI('name',13,this.name,{
                    group:'General',
                    title:'Name',
                    dst:'name'
                })
            );
            fields.push(
                utils.createCI('deferred',0,this.deferred,{
                    group:'General',
                    title:'Deferred',
                    dst:'deferred'
                })
            );
            fields.push(utils.createCI('arguments',27,this.args,{
                group:'Arguments',
                title:'Arguments',
                dst:'args'
            }));
            fields.push(
                utils.createCI('value',types.ECIType.EXPRESSION_EDITOR,this.method,{
                    group:'Script',
                    title:'Script',
                    dst:'method',
                    select:true,
                    widget:{
                        allowACECache:true,
                        showBrowser:false,
                        showSaveButton:true,
                        editorOptions:{
                            showGutter:true,
                            autoFocus:false
                        },
                        item:this
                    },
                    delegate:{
                        runExpression:function(val,run,error){
                            thiz.method=val;
                            thiz.solve(thiz.scope,null,run,error);
                        }
                    }
                }));
            return fields;
        }
    });
});