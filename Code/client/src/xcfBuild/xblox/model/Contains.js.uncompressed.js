define("xblox/model/Contains", [
    'dcl/dcl',
    "dojo/promise/all",
    "xide/types"
], function (dcl, all, types) {
    /**
     * Contains provides implements functions to deal with sub blocks.
     */
    return dcl(null, {
        declaredClass: 'xblox.model.Contains',
        runByType: function (outletType, settings) {
            var items = this.getItemsByType(outletType);
            if (items.length) {
                this.runFrom(items, 0, settings);
            }
        },
        getItemsByType: function (outletType) {
            var items = this.items;
            if (!outletType) {
                return items;
            }
            var result = [];
            _.each(items, function (item) {
                if (item.outlet & outletType) {
                    result.push(item);
                }
            });
            return result;
        },
        getContainer: function () {
            return this[this._getContainer()];
        },
        /**
         * Store is asking this!
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren: function (parent) {
            var items = this[this._getContainer()];
            return items != null && items.length > 0;
        },
        /**
         * Store function
         * @param parent
         * @returns {Array}
         */
        getChildren: function (parent) {
            return this[this._getContainer()];
        },
        //  standard call from interface
        canAdd: function () {
            return [];
        },
        /***
         * Generic: run sub blocks
         * @param scope
         * @param settings
         * @param run
         * @param error
         * @returns {Array}
         */
        _solve: function (scope, settings, run, error) {
            if (!this._lastRunSettings && settings) {
                this._lastRunSettings = settings;
            }
            settings = this._lastRunSettings || settings;
            this._currentIndex = 0;
            this._return = [];
            var ret = [], items = this[this._getContainer()];
            if (items.length) {
                var res = this.runFrom(items, 0, settings);
                this.onSuccess(this, settings);
                return res;
            } else {
                this.onSuccess(this, settings);
            }
            return ret;
        },
        onDidRunItem: function (dfd, result) {
            this._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, this);
            dfd.resolve(result);
        },
        onDidRunItemError: function (dfd, result) {
            dfd.reject(result);
        },
        onRunThis: function () {
            this._emit(types.EVENTS.ON_RUN_BLOCK, this);
        },
        onDidRunThis: function (dfd, result, items, settings) {
            var thiz = this;
            //more blocks?
            if (items && items.length) {
                var subDfds = thiz.runFrom(items, 0, settings);
                all(subDfds).then(function () {
                    thiz.onDidRunItem(dfd, result, settings);
                }, function (err) {
                    thiz.onDidRunItem(dfd, err, settings);
                });

            } else {
                thiz.onDidRunItem(dfd, result, settings);
            }
        },
        ___solve: function (scope, settings, run, error) {
        }
    });
});