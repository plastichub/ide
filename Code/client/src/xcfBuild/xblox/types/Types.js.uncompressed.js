/** @module xblox/types **/
define("xblox/types/Types", [
    'xide/types/Types',
    'xide/utils'
], function (types, utils) {

    /**
     * The block's capabilities. This will be evaluated in the interface but also
     * by the run-time (speed ups).
     *
     * @enum {integer} module:xide/types/BLOCK_CAPABILITIES
     * @memberOf module:xide/types
     */
    var BLOCK_CAPABILITIES = {
        /**
         * No other block includes this one.
         * @constant
         * @type int
         */
        TOPMOST: 0x00004000,
        /**
         * The block's execution context can be changed to another object.
         * @constant
         * @type int
         */
        TARGET: 0x00040000,
        /**
         * The block may create additional input terminals ('reset', 'pause', ...).
         * @constant
         * @type int
         */
        VARIABLE_INPUTS: 0x00000080,
        /**
         * The block may create additional output terminals ('onFinish', 'onError').
         * @constant
         * @type int
         */
        VARIABLE_OUTPUTS: 0x00000100,
        /**
         * The block may create additional ouput parameters ('result', 'error',...).
         * @constant
         * @type int
         */
        VARIABLE_OUTPUT_PARAMETERS: 0x00000200,
        /**
         * The block may create additional input parameters.
         * @constant
         * @type int
         */
        VARIABLE_INPUT_PARAMETERS: 0x00000400,
        /**
         * The block can contain child blocks.
         * @constant
         * @type int
         */
        CHILDREN: 0x00000020,
        /**
         * Block provides standard signals ('paused', 'error').
         * @constant
         * @type int
         */
        SIGNALS: 0x00000080
    }
    /**
     * Flags to describe a block's execution behavior.
     *
     * @enum {integer} module:xide/types/RUN_FLAGS
     * @memberOf module:xide/types
     */
    var RUN_FLAGS = {
        /**
         * The block can execute child blocks.
         * @constant
         * @type int
         */
        CHILDREN: 0x00000020,
        /**
         * Block is waiting for a message => EXECUTION_STATE::RUNNING
         * @constant
         * @type int
         */
        WAIT: 0x000008000
    };

    /**
     * Flags to describe a block's execution state.
     *
     * @enum {integer} module:xide/types/EXECUTION_STATE
     * @memberOf module:xide/types
     */
    var EXECUTION_STATE = {
        /**
         * The block is doing nothing and also has done nothing. The is the default state
         * @constant
         * @type int
         */
        NONE:0x00000000,
        /**
         * The block is running.
         * @constant
         * @type int
         */
        RUNNING: 0x00000001,
        /**
         * The block is an error state.
         * @constant
         * @type int
         */
        ERROR: 0x00000002,
        /**
         * The block is in an paused state.
         * @constant
         * @type int
         */
        PAUSED: 0x00000004,
        /**
         * The block is an finished state, ready to be cleared to "NONE" at the next frame.
         * @constant
         * @type int
         */
        FINISH: 0x00000008,
        /**
         * The block is an stopped state, ready to be cleared to "NONE" at the next frame.
         * @constant
         * @type int
         */
        STOPPED: 0x00000010,
        /**
         * The block has been launched once...
         * @constant
         * @type int
         */
        ONCE: 0x80000000,
        /**
         * Block will be reseted next frame
         * @constant
         * @type int
         */
        RESET_NEXT_FRAME: 0x00800000,
        /**
         * Block is locked and so no further inputs can be activated.
         * @constant
         * @type int
         */
        LOCKED: 0x20000000	// Block is locked for utilisation in xblox
    }

    types.BLOCK_MODE = {
        NORMAL: 0,
        UPDATE_WIDGET_PROPERTY: 1
    };

    /**
     * Flags to describe a block's belonging to a standard signal.
     * @enum {integer} module:xblox/types/BLOCK_OUTLET
     * @memberOf module:xblox/types
     */
    types.BLOCK_OUTLET = {
        NONE: 0x00000000,
        PROGRESS: 0x00000001,
        ERROR: 0x00000002,
        PAUSED: 0x00000004,
        FINISH: 0x00000008,
        STOPPED: 0x00000010
    };

    utils.mixin(types.EVENTS, {
        ON_RUN_BLOCK: 'onRunBlock',
        ON_RUN_BLOCK_FAILED: 'onRunBlockFailed',
        ON_RUN_BLOCK_SUCCESS: 'onRunBlockSuccess',
        ON_BLOCK_SELECTED: 'onItemSelected',
        ON_BLOCK_UNSELECTED: 'onBlockUnSelected',
        ON_BLOCK_EXPRESSION_FAILED: 'onExpressionFailed',
        ON_BUILD_BLOCK_INFO_LIST: 'onBuildBlockInfoList',
        ON_BUILD_BLOCK_INFO_LIST_END: 'onBuildBlockInfoListEnd',
        ON_BLOCK_PROPERTY_CHANGED: 'onBlockPropertyChanged',
        ON_SCOPE_CREATED: 'onScopeCreated',
        ON_VARIABLE_CHANGED: 'onVariableChanged',
        ON_CREATE_VARIABLE_CI: 'onCreateVariableCI'
    });


    types.BlockType = {
        AssignmentExpression: 'AssignmentExpression',
        ArrayExpression: 'ArrayExpression',
        BlockStatement: 'BlockStatement',
        BinaryExpression: 'BinaryExpression',
        BreakStatement: 'BreakStatement',
        CallExpression: 'CallExpression',
        CatchClause: 'CatchClause',
        ConditionalExpression: 'ConditionalExpression',
        ContinueStatement: 'ContinueStatement',
        DoWhileStatement: 'DoWhileStatement',
        DebuggerStatement: 'DebuggerStatement',
        EmptyStatement: 'EmptyStatement',
        ExpressionStatement: 'ExpressionStatement',
        ForStatement: 'ForStatement',
        ForInStatement: 'ForInStatement',
        FunctionDeclaration: 'FunctionDeclaration',
        FunctionExpression: 'FunctionExpression',
        Identifier: 'Identifier',
        IfStatement: 'IfStatement',
        Literal: 'Literal',
        LabeledStatement: 'LabeledStatement',
        LogicalExpression: 'LogicalExpression',
        MemberExpression: 'MemberExpression',
        NewExpression: 'NewExpression',
        ObjectExpression: 'ObjectExpression',
        Program: 'Program',
        Property: 'Property',
        ReturnStatement: 'ReturnStatement',
        SequenceExpression: 'SequenceExpression',
        SwitchStatement: 'SwitchStatement',
        SwitchCase: 'SwitchCase',
        ThisExpression: 'ThisExpression',
        ThrowStatement: 'ThrowStatement',
        TryStatement: 'TryStatement',
        UnaryExpression: 'UnaryExpression',
        UpdateExpression: 'UpdateExpression',
        VariableDeclaration: 'VariableDeclaration',
        VariableDeclarator: 'VariableDeclarator',
        WhileStatement: 'WhileStatement',
        WithStatement: 'WithStatement'
    };
    types.BLOCK_CAPABILITIES = BLOCK_CAPABILITIES;
    types.EXECUTION_STATE = EXECUTION_STATE;
    types.RUN_FLAGS = RUN_FLAGS;


    return types;
});