define("dojox/main", [
	"dojo/_base/kernel",
	"dijit/dijit",
	"dojox/html/entities",
	"dojox/html/ellipsis",
	"dojox/html/_base",
	"dojox/widget/ColorPicker"
], function(dojo) {
	return dojo.dojox;
});