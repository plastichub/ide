define("nxapp/server/FileServer", [
    'dojo/_base/declare',
    'dojo/_base/lang',
    'xide/types',
    'xide/factory',
    'nxapp/server/WebSocket',
    'nxapp/utils/_LogMixin',
    "dojo/node!http",
    "dojo/node!sockjs"
],function(declare,lang,types,factory,WebSocket,_LogMixin,http,sockjs){

    return declare("nxapp.server.WebSocket", [WebSocket,_LogMixin],
    {
        onFileChanged:function(evt){
            //console.log('file changed : ' + evt.modulePath,evt);
            this.broadCastMessage(types.EVENTS.ON_FILE_CHANGED,{
                path:evt.path,
                modulePath:evt.modulePath,
                mask:evt.mask,
                type:evt.type
            });
        },
        start:function(_options,profile) {

            this.profile=profile;
            this.options = lang.mixin(this.options, _options);
            this.clients=[];
            var thiz=this;
            factory.subscribe(types.EVENTS.ON_FILE_CHANGED,this.onFileChanged,this);

            var port = this.options.port;
            var host = this.options.host;


            if (!port) {
                console.error("Port must be provided into options");
                return false;
            } else {

                this.initLogger(profile.debug);

                this.log("File Socket server running in " + port, "socket_server");

                var socket_options = {};

                if (!this.showDebugMsg("socket_server")) {
                    socket_options.log = function() {};
                }
                var socket = sockjs.createServer(socket_options);
                var socket_server = http.createServer(this._handler);
                socket.installHandlers(socket_server);
                socket_server.listen(port, host);
                this._handleSocketEmits(socket);

                // Triggers ON_WEBSOCKET_START
                factory.publish(types.EVENTS.ON_WEBSOCKET_START,{
                    socket_server: {
                        port:port,
                        socket_handler:socket
                    }
                },this);

                socket.on('connection', function(conn) {
                    var index = thiz.clients.push(conn);

                    conn.on('close', function() {
                        delete thiz.clients[index];
                    });
                });

                return true;
            }
        },
        _handleSocketEmits:function(socket) {
            var port = this.options.port;
            var delegate = this.options.delegate;
            var self = this;

            socket.on('connection', function(conn) {
                self.socket = conn;

                // Triggers ON_WEBSOCKET_CONNECTION
                factory.publish(types.EVENTS.ON_WEBSOCKET_CONNECTION,{
                    connection: {
                        port:port,
                        socket_handler:conn
                    }
                },this);
                conn.on('close', function() {});
            });

        }
    });
});