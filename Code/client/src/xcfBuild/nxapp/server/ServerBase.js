//>>built
define("nxapp/server/ServerBase",["dcl/dcl","xide/model/Base","xide/utils","xide/mixins/EventedMixin"],function(a,b,c,d){return a([b.dcl,d.dcl],{options:null,delegate:null,ctx:null,declaredClass:"nxapp.server.ServerBase",_defaultOptions:function(){return{delegate:this}},init:function(a){this.options=c.mixin(this._defaultOptions(),a.options)}})});
//# sourceMappingURL=ServerBase.js.map