define("nxapp/xide/do/server/AtomizeServer", [
    'dojo/_base/declare',
    'dojo/Stateful',
    'dojo/node!http',
    'dojo/node!atomize-server',
    'dojo/node!net'
],function(declare,Stateful,http,atomize,net){

    return declare("xide.do.server.AtomizeServer", [Stateful],
    {
        options:null,
        atomize: null,

        /***
         * Default options
         */
        _defaultOptions:function(){
            return {
                host:"0.0.0.0",
                port:9999,
                channel:'atomize',
                preload: {
                    DO: {

                    }
                }
            };
        },

        ///////////////////////////////////////////////////
        //
        // Atomize data manipulation public methods
        //
        ///////////////////////////////////////////////////

        /***
         * Register a value in the namespace
         * @param ns
         * @param id
         * @param value
         * @param ownerMeta
         */
        register:function(ns,id,value,ownerMeta){
            var thiz = this;
            this.ls(ns,function(results) {

                results.push({
                    id: id,
                    value: value
                });

                thiz._makeTransaction(function() {
                        thiz._set("root."+ns+".ownerMeta",ownerMeta);
                        thiz._set("root."+ns+".inputs",results);
                    },
                    function() {});

            });
        },

        /***
         * Gets a value by ns and id. The value is passed to the callback function once the reading operation finishes
         * @param ns
         * @param id
         * @param callback  =>  callback function(value)
         */
        get:function(ns,id,callback) {
            var thiz = this;
            this.ls(ns,function(results) {
                for(input in results) {

                    if (results[input].id == id)
                    {
                        callback(results[input].value);
                    }
                }

            });

        },

        /***
         * Sets a value by ns and id
         * @param ns
         * @param id
         * @param value
         * @param ownerMeta
         */
        set:function(ns,id,value,ownerMeta) {
            var thiz = this;
            this.ls(ns,function(results) {

                for(input in results) {
                    if (results[input].id == id)
                    {
                        results[input].value = value;
                    }
                }

                thiz._makeTransaction(function() {
                    thiz._set("root."+ns+".ownerMeta",ownerMeta);
                    thiz._set("root."+ns+".inputs",results);
                },
                function() {});

            });

        },

        /***
         * Enumerates values into a ns. The values are passed to the callback function once the reading operation finishes
         * @param ns
         * @param callback
         */
        ls:function(ns,callback){
            var thiz = this;
            this._makeTransaction(function() {
                    return thiz._get("root."+ns+".inputs");
                },
                function(results) {
                    var converted = [];
                    for (input in results)
                    {
                        converted.push(results[input]);
                    }
                    callback(converted);
                }

            );
        },

        /***
         * Removes an id/value pair from the namespace
         * @param ns
         * @param id==null : removes ns
         */
        remove:function(ns,id){
            var thiz = this;
            this.ls(ns,function(results) {
                var reduced = [];
                for(input in results) {
                    if (results[input].id != id)
                    {
                        reduced.push(results[input]);
                    }
                }

                thiz._makeTransaction(function() {
                        thiz._set("root."+ns+".inputs",reduced);
                    },
                    function() {});

            });
        },

        /***
         * Load an entire array into a ns
         *
         * Each pair key=>value creates a new input with id=key and value=value
         *
         * @param ns
         * @param keyValueArray
         */
        load:function(ns,keyValueArray,ownerMeta){
            var thiz = this;
            this.ls(ns,function(inputs) {

                for(key in keyValueArray)
                {
                    inputs.push({
                        id: key,
                        value: keyValueArray[key]
                    });
                }

                thiz._makeTransaction(function() {
                        thiz._set("root."+ns+".ownerMeta",ownerMeta);
                        thiz._set("root."+ns+".inputs",inputs);
                },
                function() {});

            });


        },


        ///////////////////////////////////////////////////
        //
        // Service Control API
        //
        ///////////////////////////////////////////////////


        /***
         * init - initialize the Atomize Server with options
         * @param options
         */
        init:function(options) {
            this.options = this._defaultOptions();
            var myself = this;

            // Mix the default options and the options provided
            for(var property in this.options) {
                if (options[property]) {
                    this.options[property] = options[property];
                }
            }
        },

        /***
         * start - starts the Atomize Server
         */
        start:function() {
            var httpServer = http.createServer();
            var emitter = atomize.create(httpServer, '[/]'+this.options.channel,null,this.options.preload);
            httpServer.listen(this.options.port,this.options.host);
            this.atomize = emitter.client();
        },

        /***
         * check - checks if the server is up in the "host:port" defined by options
         *
         * @param callback  => callback function(isON: true/false)
         */
         check:function(callback) {
            var Socket = net.Socket;
            var socket = new Socket();

            // Socket connection established, port is open
            socket.on('connect', function() {
                socket.end();
                callback(true);
            });
            socket.setTimeout(1500);// If no response, assume port is not listening
            socket.on('timeout', function() {
                socket.destroy();
                callback(false);
            });
            socket.on('error', function(exception) {
                callback(false);
            });

            socket.connect(this.options.port, this.options.host);
         },

        /***
         * Returns the Atomize Server url
         * @returns {string}
         */
         url:function() {
            return this.options.host + ":" + this.options.port + "/" + this.options.channel;
         },


        ///////////////////////////////////////////////////
        //
        // Private data manipulation methods
        //
        ///////////////////////////////////////////////////

        /***
         * Gets an atomize value stored into a namespace. Needs to be inside a transaction
         * @param atomize_ns
         * @returns {*}
         * @private
         */
        _get:function(atomize_ns) {

            var path = atomize_ns.split(".");

            var obj = this.atomize.access(this.atomize,path[0]);
            path = path.slice(1);

            for (var n in path) {
                if (obj == undefined) {
                    return obj;
                }
                obj = this.atomize.access(obj,path[n]);
            }
            return obj;
        },

        /***
         * Sets an atomize value stored into a namespace. Needs to be inside a transaction
         * @param atomize_ns
         * @param value
         * @private
         */
        _set:function(atomize_ns,value) {
            var path = atomize_ns.split(".");

            var obj = this.atomize.access(this.atomize,path[0]);
            var toassign =  path[path.length-1];
            path = path.slice(1,-1);

            for (var n in path) {
                obj = this.atomize.access(obj,path[n]);
            }

            if (typeof value == "object") {
                value = this.atomize.access(this.atomize, "lift")(value);
            }

            this.atomize.assign(obj,toassign,value);
        },

        /***
         * Runs a transaction inside "action", and then the callback function onResult(result)
         * @param action
         * @param onResult
         * @private
         */
        _makeTransaction:function(action,onResult) {
            this.atomize.atomically(action,onResult);
        }

    });
});