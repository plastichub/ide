//>>built
define("nxapp/xide/debugger/DebuggerServer","dojo/_base/declare dojo/Stateful dojo/_base/lang dojo/_base/array dojo/node!fs dojo/node!child_process xide/debugger/utils/TCPUtils".split(" "),function(k,l,q,r,m,h,p){return k("xide.debugger.DebuggerServer",[l],{inspector_instance:null,debugger_url:null,app_instance:null,options:null,inspector_pid:null,_defaultOptions:function(){return{auto_start_debug_server:!1,check_server_ready:!0,web_port:8080,web_host:"0.0.0.0",debug_port:5858,no_live_edit:!1,inspector_path:"/node-inspector/bin/inspector",
working_path:"/PMaster/x4mm/Utils/nodejs/",node_modules_path:"node_modules"}},init:function(a){this.options=this._defaultOptions();for(var c in this.options)a[c]&&(this.options[c]=a[c])},start:function(a,c){this._createInspectorInstance(a,c)},run:function(a,c,d){if(this.options.auto_start_debug_server){var f=this;this._createInspectorInstance(function(g,b){f.debugger_url=g;f.inspector_pid=b;f._createAppInstance(a,c,d)})}else this.options.check_server_ready?(f=this,p.checkPort(this.options.web_port,
this.options.web_host,function(g,b){"closed"==b?"function"==typeof d&&d("Server instance is not ready in "+f.options.web_host+":"+f.options.web_port+".\nConsider using auto_start_debug_server option to start it automatically."):"open"==b&&f._createAppInstance(a,c,d)})):this._createAppInstance(a,c,d)},_createAppInstance:function(a,c,d){var f=["--debug\x3d"+this.options.debug_port,"--debug-brk"],g=[],b=a.split(" ");if(1<b.length)for(n in a=b[0],b)0<n&&g.push(b[n]);b=this.options.working_path+a;this._checkFileExists(b)?
(b=h.fork,this.app_instance=b(this.options.working_path+a,g,{silent:!1,cwd:this.options.working_path,execArgv:f}),this.app_instance.pid?"function"==typeof c&&c(this.debugger_url,this.app_instance.pid,this.inspector_pid):"function"==typeof d&&d("Unexpected error trying to run "+a),this.app_instance.on("error",function(a){"function"==typeof d&&d(a)})):"function"==typeof d&&d("Cannot find module: "+b)},_checkFileExists:function(a){-1==a.search("/.js")&&(a+=".js");return m.existsSync(a)},_createInspectorInstance:function(a,
c){var d=["--web-port\x3d"+this.options.web_port,"--web-host\x3d"+this.options.web_host,"--debug-port\x3d"+this.options.debug_port];this.options.no_live_edit||d.push("--save-live-edit");var f=h.fork,g=this.options.working_path+this.options.node_modules_path,b=g+this.options.inspector_path;if(this._checkFileExists(b)){this.inspector_instance=f(b,d,{silent:!0,cwd:g});var e=this;this.inspector_instance.on("message",function(b){switch(b.event){case "SERVER.LISTENING":e.inspector_pid=e.inspector_instance.pid;
e.debugger_url=b.address.url;"function"==typeof a&&a(e.debugger_url,e.inspector_pid);break;case "SERVER.ERROR":switch(b.error.code){case "EADDRINUSE":e.debugger_url="http://"+e.options.web_host+":"+e.options.web_port;e.debugger_url+="/debug?port\x3d"+e.options.debug_port;e.inspector_pid=null;"function"==typeof a&&a(e.debugger_url,e.inspector_pid);break;default:"function"==typeof c&&c("Unexpected error trying to run inspector server")}}});this.inspector_instance.on("error",function(a){"function"==
typeof c&&c(a)})}else"function"==typeof c&&c("Cannot find module: "+b)},stop:function(a){process.kill(a)}})});
//# sourceMappingURL=DebuggerServer.js.map