//>>built
define("nxapp/xide/debugger/utils/TCPUtils",["dojo/node!net"],function(e){return{checkPort:function(c,d,f){var a=new e.Socket,b=null;a.on("connect",function(){b="open";a.end()});a.setTimeout(1500);a.on("timeout",function(){b="closed";a.destroy()});a.on("error",function(a){b="closed"});a.on("close",function(a){f(null,b,d,c)});a.connect(c,d)}}});
//# sourceMappingURL=TCPUtils.js.map