require([
    "dcl/dcl",
    "dojo/node!util",
    "nxappmain/nxAppBase"
], function(dcl,util,nxAppBase){
    return dcl(nxAppBase,{
        declaredClass:"nxapp.manager.Application"
    });
});