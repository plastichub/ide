define("nxapp/manager/ContextBlox", [
    "dcl/dcl",
    './ConnectionManager',
    'xide/manager/ContextBase',
    'xblox/manager/BlockManager',
    'xblox/embedded'
], function (dcl, ConnectionManager, ContextBase, BlockManager, embedded) {
    return dcl(ContextBase, {
        declaredClass: "nxapp.manager.ContextBlox",
        connectionManager: null,
        driverManager: null,
        fileManager: null,
        blockManager: null,
        deviceServer: null,
        getBlockManager: function () {
            return this.blockManager;
        },
        getDeviceServer: function () {
            return this.deviceServer;
        },
        getFileManager: function () {
            return this.fileManager;
        },
        getConnectionManager: function () {
            return this.connectionManager;
        },
        initManagers: function (profile) {
            //this.connectionManager.init(profile);
        },
        constructManagers: function () {
            //this.connectionManager = new ConnectionManager({ctx: this});
            this.blockManager = new BlockManager({ctx: this});
        }
    });
});