/** @module nxapp/manager/TestManager */
define("nxapp/manager/TestManager", [
    'dcl/dcl',
    "dojo/node!fs",
    "xcf/manager/DeviceManager",
    "xide/utils",
    "xide/types",
    "nxapp/utils/_console",
    "nxapp/utils",
    "dojo/node!child_process",
    'nxapp/manager/ManagerBase',
    'require',
    'xide/mixins/ReloadMixin',
    "dojo/node!path",
    "xdojo/has"
], function (dcl, fs, DeviceManager, utils,types,_console, xUtils, child_process,ManagerBase,require,ReloadMixin,path,has) {
    var console = _console;
    if( true ){
        return dcl(null,{});
    }
    var CONFIG_DEFAULT = {
        // The port on which the instrumenting proxy will listen
        proxyPort: 9000,
        // A fully qualified URL to the Intern proxy
        proxyUrl: 'http://localhost:9000/',

        // Username and key should be set in the BROWSERSTACK_USERNAME and BROWSERSTACK_ACCESS_KEY environment variables
        tunnel: 'NullTunnel',

        // See https://www.browserstack.com/automate/capabilities for capabilities and environments
        capabilities: {
            name: 'dojo-loader',
            project: 'Dojo 2',
            fixSessionCapabilities: false
        },

        environments: [
            // Enter whichever browser you want to test here.
            // (It is unwise to test more than one simultaneously on one host,
            // due to potential for spurious focus test failures.)
            { browserName: 'internet explorer', version: ['9.0', '10.0', '11.0'], platform: 'Windows 7' } /*,
             { browserName: 'microsoftedge', platform: 'Windows 10' }*/,
            { browserName: 'firefox', platform: 'Windows 10' },
            { browserName: 'chrome', platform: 'Windows 10' },
            { browserName: 'safari', version: '9', platform: 'OS X 10.11' },
            { browserName: 'android', platform: 'Linux', version: '4.4', deviceName: 'Google Nexus 7 HD Emulator' }
            /*,{ browserName: 'safari', version: '7', platform: 'OS X 10.9' }*/
        ],

        // Maximum number of simultaneous integration tests that should be executed on the remote WebDriver service
        maxConcurrency: 2,
        loaderOptions: {
            // Packages that should be registered with the loader in each testing environment
            packages: [
                { name: 'dojo', location: './node_modules/dojo' },
                { name: 'dijit', location: 'dijit' },
                { name: 'dgrid', location: 'dgrid' },
                { name: 'dstore', location: 'dstore' },
                { name: 'xide', location: 'xide' },
                { name: 'xfile', location: 'xfile' },
                { name: 'dojox', location: 'dojox' },
                { name: 'xdojo', location: 'xdojo' },
                { name: 'xgrid', location: 'xgrid' },
                { name: 'xblox', location: 'xblox' },
                { name: 'xdocker', location: 'xdocker' },
                { name: 'wcDocker', location: 'wcDocker/src' },
                { name: 'dcl', location: './node_modules/dcl' },
                { name: 'tests', location: 'tests' }
            ]
        },

        // A regular expression matching URLs to files that should not be included in code coverage analysis
        //excludeInstrumentation: /^dojox?|^dijit|^dstore|\/node_modules\/|\/tests\/|\/nls\//,
        excludeInstrumentation : /(?:node_modules|bower_components|tests)[\/\\]/,

        // Non-functional test suite(s) to run in each browser
        suites: [ 'tests/all' ],

        // Functional test suite(s) to run in each browser once non-functional tests are completed
        functionalSuites: [],
        reporters:[
            {
                id:"tests/ConsoleEmbedded",
                delegate:{
                    onStart:function(suite){
                        console.error('on start '+suite.name);
                    },
                    onFail:function(test){
                        console.error('on fail '+test.name);
                    },
                    onEnd:function(suite){
                        console.error('on start '+suite.name);
                    }
                }
            },
            { id: 'JUnit', filename: 'report.xml' }
        ]
    };


    function doTests(){

        console.clear();


        var _require = require;
        _require([
            'intern/lib/executors/PreExecutorEmbedded',
            'intern/lib/reporters/Console'
        ], function (PreExecutorEmbedded,Console) {

            var executor = new PreExecutorEmbedded({
                executorId: 'client',
                config:utils.mixin(CONFIG_DEFAULT,{
                    suites: [ 'tests/devices/all' ]

                }),
                defaultLoaderOptions:dojoConfig
            });
            try {
                console.error('run tests');
                var promise = executor.run(function(what){
                    console.error('did tests ',what);
                });
                _require.undef('tests/devices/all');
            }catch(e){
                console.error('error running tests ',e);
            }
        });
    }


    var sctx = global.sctx;
    if(sctx){
        var testManager = sctx.getTestManager();
        //doTests();
    }

    /**
     * DeviceManager server side only part
     * @class module:nxapp/manager/TestManager
     * @augments module:xide/mixins/EventedMixin
     * @extends module:xide/manager/ManagerBase
     */
    return dcl([ManagerBase,ReloadMixin.dcl], {
        declaredClass: "nxapp.manager.TestManager",
        connection: null,
        test1:function(){
            return "   ha";
        },
        loadFixtures:function(){
            var fixturesPath = path.resolve('./tests/');
        },
        onReloaded:function(){
          console.error('on reloaded');
        },
        doTests:function(){

            return;
            var _require = require;
            _require([
                'intern/lib/executors/PreExecutorEmbedded'
            ], function (PreExecutorEmbedded ) {
                var executor = new PreExecutorEmbedded({
                    executorId: 'client',
                    config:utils.mixin(CONFIG_DEFAULT,{
                        suites: [ 'tests/devices/all' ]
                    }),
                    defaultLoaderOptions:dojoConfig
                });

                try {
                    var promise = executor.run(function(what){
                        console.error('did tests ',what);
                    });
                }catch(e){
                    console.error('error running tests ',e);
                }
            });
        },
        init:function(){
            //this.initReload();
            this.subscribe(types.EVENTS.ON_MODULE_RELOADED,function(e){
                if(e.module.indexOf('tests/devices')!==-1 && e.module.indexOf('.js')){
                    require.undef('tests/devices/Minimal');
                    doTests();
                }
            });
        }
    });
});

