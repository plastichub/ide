/** @module nxapp/manager/DeviceServerContext */
define("nxapp/manager/DeviceServerContext", [
	"dcl/dcl",
	'nxapp/manager/Context',
	'nxapp/Commons',
	'xcf/types/Types',
	"dojo/node!path",
	"dojo/node!fs",
	"nxapp/utils",
	'xide/types',
	'nxapp/manager/DriverManager',
	'xcf/manager/DeviceManager',
	'nxapp/manager/FileManager',
	'xcf/manager/Context',
	"nxapp/manager/DeviceManager",
	'xcf/manager/BlockManager',
	'xide/manager/ResourceManager',
	'xblox/factory/Blocks',
	"dojo/node!tracer",
	"nxapp/utils/_console",
	'nxapp/manager/_Drivers_Devices',
	'dojo/_base/lang',
	'dojo/promise/all',
	'require',
	'dojo/has',
	'nxapp/manager/TestManager'
], function (dcl, Context, Commons, Types, path, fs, utils,
	types, DriverManager, DeviceManager, FileManager,
	XCFContext, XDeviceManager, BlockManager, ResourceManager, Blocks, tracer,
	_console, _Drivers_Devices, lang, all, _require, has, TestManager, globalEvent) {

		var console = _console;
		/**
		 * @class module:nxapp/manager/FakeDeviceServerClass
		 */
		var DeviceServerClientClass = dcl(null, {
			declaredClass: 'DeviceServerClientClass',
			write: function (data) {
				console.error('device server client write', data);
			},
			emit: function (data) {
				console.error('device server client emit', data);
			}
		});
		var debug = false;
		var startTestDevice = false;
		var bootManagers = true;
		var startDevices = true;
		var process = true;
		var debugMessages = false;
		var profileMem = false;

		var instance = null;


		/**
		 * @class module:nxapp/manager/FakeConnection
		 */
		var FakeConnectionClass = dcl(null, {
			owner: null,
			remoteAddress: 'fake - address',
			remotePort: 'fake - port',
			isExternal: function () {
				return false;
			},
			constructor: function (args) {
				utils.mixin(this, args);
			},
			write: function (data) {
				if (!process) {
					return;
				}
				var ctx = this.owner;
				var deviceManager = ctx.getDeviceManager();
				var thiz = deviceManager;
				var dataIn = data['data'];

				var msg = utils.getJson(data);
				debug && !msg && console.error('invalid incoming message');
				msg = msg || {};

				if (msg.data && msg.data.device) {
					var deviceStoreItem = deviceManager.getDeviceStoreItem(msg.data.device);
					if (deviceStoreItem) {
						var info = deviceManager.toDeviceControlInfo(deviceStoreItem);
						if (info) {
							if (!info.serverSide) {
								return;
							}
						} else {
							console.error('no info');
						}
					} else {
						debug && console.error('cant find device');
					}
				}
				//debugMessages = true;
				debugMessages && console.log('Fake Connection : write ', msg.event);
				//utils.stack();
				if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_FINISH) {
					thiz.onCommandFinish(msg.data.device, msg.data.deviceMessage);
					return;
				}
				if (msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_ERROR) {
					thiz.onCommandError(msg.data.device, msg.data.deviceMessage);
					return;
				}
				if (msg.event === types.EVENTS.ON_DEVICE_DISCONNECTED) {
					thiz.publish(types.EVENTS.ON_DEVICE_DISCONNECTED, msg.data);
					return;
				}

				if (msg.event === types.EVENTS.SET_DEVICE_VARIABLES) {
					return thiz.onSetDeviceServerVariables(msg.data);
				}

				if (msg.event === types.EVENTS.ON_DEVICE_CONNECTED) {
					thiz.publish(types.EVENTS.ON_DEVICE_CONNECTED, msg.data);
					return;
				}
				if (msg.event === types.EVENTS.ON_SERVER_LOG_MESSAGE) {
					//thiz.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, msg.data);
					return;
				}
				if (msg.event === types.EVENTS.ON_MQTT_MESSAGE) {
					thiz.publish(types.EVENTS.ON_MQTT_MESSAGE, msg.data);
					thiz.onMQTTMessage(msg.data);
					return;
				}
				if (msg.event === types.EVENTS.ON_FILE_CHANGED) {
					return;
				}
				thiz.onDeviceServerMessage({
					data: data
				});

			},
			emit: function () {
				console.error('device server client emit', arguments);
			}
		});
		/**
		 * @class module:nxapp/manager/DeviceServerContext
		 * @lends module:nxapp/manager/Context
		 * @augments module:xide/mixins/EventedMixin
		 * @extends module:xide/manager/ContextBase
		 * @extends module:xcf/manager/Context
		 * @extends module:nxapp/manager/_Drivers_Devices
		 */
		return dcl([Context, XCFContext, _Drivers_Devices], {
			setAppServer: function (server) {
				this.appServer = server;
			},
			declaredClass: "nxapp.manager.DeviceServerContext",
			profile: null,
			appServer: null,
			/**
			 * @type {module:nxapp/manager/TestManager}
			 */
			testManager: null,
			/**
			 *
			 * @returns {module:nxapp/manager/TestManager}
			 */
			getTestManager: function () {
				return this.testManager;
			},
			doTests: function () {
				return this.getTestManager().doTests();
			},
			/**
			 * std impl.
			 * @param profile
			 */
			initManagers: function (profile) {
				profile && (this.profile = profile);
				if (!bootManagers) {
					return;
				}
				if (this.testManager) {
					this.testManager.init(profile);
				}
				this.deviceManager.init();
				this.driverManager.init();
				this.blockManager.init();
				this.fileManager.init(profile);
				this.blockManager.onReady();
				var thiz = this;
				this.subscribe(types.EVENTS.ON_FILE_CHANGED, function (evt) {
					thiz.onXIDEMessage({
						event: types.EVENTS.ON_FILE_CHANGED,
						data: evt,
						type: evt.type
					}, false);

					thiz.fileChanged(evt);
					thiz.deviceManager.onFileChanged(evt);
					thiz.driverManager.onFileChanged(evt);

					thiz.getDeviceServer().broadCastMessage(types.EVENTS.ON_FILE_CHANGED, {
						path: evt.path,
						modulePath: evt.modulePath,
						mask: evt.mask,
						type: evt.type
					});
				});
			},
			getModule: function (_module) {
				return lang.getObject(utils.replaceAll('/', '.', _module)) || lang.getObject(_module) || (dcl.getObject ? dcl.getObject(_module) || dcl.getObject(utils.replaceAll('/', '.', _module)) : null);
			},
			_reloadModule: function (_module, reload) {
				if (!_require.undef) {
					return;
				}

				var _errorHandle = null;

				if (_module.indexOf('.json') !== -1) {
					return;
				}
				if (_module.indexOf('nxapp') !== -1) {
					return;
				}
				if (_module.indexOf('.js') == -1) {
					return;
				}
				if (_module.indexOf('/build/') !== -1) {
					return;
				}
				_module = _module.replace('0/8', '0.8');
				var debugModuleReload = true;
				function handleError(error) {
					debugModuleReload && console.log(error.src, error.id);
					debugModuleReload && console.error('require error ' + _module, error);
					_errorHandle.remove();
				}

				var obj = lang.getObject(utils.replaceAll('/', '.', _module)) || lang.getObject(_module);
				if (obj && obj.prototype && obj.prototype.reloadModule) {
					obj.prototype.reloadModule();
					return;
				}

				_errorHandle = _require.on("error", handleError);

				var oldModule = this.getModule(_module);
				if (!oldModule) {
					oldModule = typeof _module !== 'undefined' ? oldModule : null;
					if (!oldModule && typeof window !== 'undefined') {
						//try global namespace
						oldModule = utils.getAt(window, utils.replaceAll('/', '.', _module), null);
						if (oldModule) {
							obj = oldModule;
						} else {
							try {
								oldModule = _require(utils.replaceAll('.', '/', _module));
							} catch (e) {

							}
						}
					}
				}
				if (oldModule) {
					obj = oldModule;
				}

				_require.undef(_module);
				var thiz = this;
				if (reload) {
					setTimeout(function () {
						_require({
							//cacheBust: 'time=' + new Date().getTime(),
							waitSeconds: 5
						});
						try {
							_require([_module], function (moduleLoaded) {
								debugModuleReload && console.log('reloaded module : ' + _module + (oldModule !== null ? " have obj" : "have no obj"));
								_require({
									cacheBust: 'time=' + new Date().getTime()
								});

								if (_.isString(moduleLoaded)) {
									console.error('module reloaded failed : ' + moduleLoaded + ' for module : ' + _module);
									return;
								}
								moduleLoaded.modulePath = _module;
								if (obj) {
									thiz.mergeFunctions(obj.prototype, moduleLoaded.prototype);
									if (obj.prototype && obj.prototype._onReloaded) {
										obj.prototype._onReloaded(moduleLoaded);
									}
								}
								thiz.publish(types.EVENTS.ON_MODULE_RELOADED, {
									module: _module,
									newModule: moduleLoaded
								});
								if (moduleLoaded.prototype && moduleLoaded.prototype.declaredClass) {
									thiz.publish(types.EVENTS.ON_MODULE_UPDATED, {
										moduleClass: moduleLoaded.prototype.declaredClass,
										moduleProto: moduleLoaded.prototype
									});
								}
							});

						} catch (e) {
							console.error('error reloading module ' + _module, e.stack);
							logError(e, 'error reloading module ' + _module);
						}
					}, 10);
				}
			},
			fileChanged: function (evt) {
				return;
				var path = evt.path;
				if (evt.type === 'changed') {
					this._reloadModule(path, true);
				}
			},
			onAllDevicesStarted: function () {
				if (this.profile.test) {
					this.doTests();
				}
			},
			onReady: function () {
				var deviceServer = this.getDeviceServer();
				if (!deviceServer) {
					console.error('have no device server');
					return;
				}
				var fakeConnection = this.connection;

				if (!deviceServer.clients) {
					deviceServer.clients = [];
				}
				if (fakeConnection) {
					deviceServer.clients.push(fakeConnection);
					deviceServer.onClientConnection(fakeConnection);
				}

				global.process.emit('device-server-ready', this);
				this.initDevices();

				instance = this;
				global.sctx = instance;
			},
			/**
			 * std impl.
			 * @returns {*}
			 */
			getUserDirectory: function () {
				return this.profile.user;
			},
			destroy: function () {
				console.log('destroy dsc');
				this.deviceServer.destroy();
				if (this.mqttManager) {
					this.mqttManager.destroy();
				}
			},
			/**
			 * std impl.
			 */
			constructManagers: function () {
				DeviceManager = dcl([DeviceManager, XDeviceManager], {});
				this.driverManager = this.createManager(DriverManager, null);
				this.fileManager = this.createManager(FileManager, null);
				this.blockManager = this.createManager(BlockManager, null);
				TestManager && (this.testManager = this.createManager(TestManager, null));
				this.connection = new FakeConnectionClass({ owner: this });
				this.deviceManager = this.createManager(DeviceManager, null, {
					deviceServerClient: new DeviceServerClientClass({}),
					connection: this.connection
				});

				if (this.profile) {
					var dataRoot = path.resolve(this.profile.system);
					var VFS_CONFIG = {
						"system_drivers": path.resolve(dataRoot + '/system/drivers'),
						"system_devices": path.resolve(dataRoot + '/system/devices')
					};
					if (this.profile.user) {
						var userRoot = path.resolve(this.profile.user);
						VFS_CONFIG['user_drivers'] = path.resolve(userRoot + '/drivers');
						VFS_CONFIG['user_devices'] = path.resolve(userRoot + '/devices');
						this.userDirectory = userRoot;
					}
					this.resourceManager = this.createManager(ResourceManager);
					this.resourceManager.resourceVariables = {
						VFS_CONFIG: VFS_CONFIG
					};
				}
			},
			onLoaded: function () { },
			initDevices: function () {
				var profile = this.profile;
				var dataRoot = path.resolve(profile.system);
				var deviceManager = this.getDeviceManager();
				var driverManager = this.getDriverManager();
				var serverSide = profile.serverSide;

				driverManager.createStore({
					items: this.getDrivers(path.resolve(dataRoot + '/system/drivers'), 'system_drivers')
				}, 'system_drivers', true);

				deviceManager.initStore({
					items: this.getDevices(path.resolve(dataRoot + '/system/devices'), 'system_devices', { serverSide: serverSide })
				}, 'system_devices', true);


				if (profile.user) {
					driverManager.createStore({
						items: this.getDrivers(path.resolve(profile.user + '/drivers'), 'user_drivers')
					}, 'user_drivers', true);

					deviceManager.initStore({
						items: this.getDevices(path.resolve(profile.user + '/devices'), 'user_devices', { serverSide: serverSide })
					}, 'user_devices', true);
				} else {
					debug && console.log('have no user specified');
				}
				all(deviceManager.connectToAllDevices()).then(function () {
					this.onAllDevicesStarted();
				}.bind(this));
			}
		});
	});
