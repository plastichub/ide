/** @module nxapp/utils/FileUtils */
define("nxapp/utils/FileUtils", [
    'dojo/_base/json',
    "dojo/node!path",
    "dojo/node!fs",
    "dojo/node!util",
    "nxapp/utils",
    "xdojo/has!host-node?dojo/node!glob-fs",
    "xdojo/has!host-node?dojo/node!glob-base",
    "xdojo/has!host-node?dojo/node!glob"
], function(json,path, fs, util, utils,glob,base,nGlob){

    utils.list=function(_path){
        try {
            var options = {};
            var root = base(_path);
            var match = "";
            if (root.base) {
                options.cwd = root.base;
                if (root.isGlob) {
                    _path = root.glob
                }
            }
            var _glob = new glob();
            var files = _glob.readdirSync(_path, options);
            var ret = [];
            _.each(files, function (file) {
                ret.push((options.cwd ? options.cwd + path.sep : "" ) + file)
            });
        }catch(e){
            return null;
        }
        return ret;
    }
    
    utils.__toObject=function(_path){

        try {
            var result = fs.statSync(_path, function (error) {
            });

            var isDirectory = result.isDirectory();
            return {
                path: _path,
                sizeBytes: result.size,
                size: isDirectory ? 'Folder' : '',
                owner: result.uid,
                group: result.gid,
                mode: result.mode,
                isDir: isDirectory,
                directory: isDirectory,
                name: path.win32.basename(_path),
                fileType: isDirectory ? 'folder' : 'file',
                modified: result.mtime.getTime() / 1000
            };
        }catch(e){
            return {
                path: _path,
                sizeBytes: 0,
                size: 0 ,
                owner: 0,
                group: 0,
                mode: 0,
                directory: false,
                mime: 'unknown',
                name: path.win32.basename(_path),
                fileType: 'file',
                modified: 0
            };
        }
    }


    utils.ls=function(_path){
        var self = this;
        var _options = {};
        var isWin       = process.platform === 'win32';
        if(_path ==='/*' && isWin){
            var out = [];
            flop.read('/', function(error, data) {
                _.each(data.files,function(item){
                    out.push({
                        path: '/' + item.name + ':',
                        sizeBytes: 0,
                        size: 0 ,
                        owner: 0,
                        group: 0,
                        mode: 0,
                        directory: true,
                        mime: 'unknown',
                        name: item.name,
                        fileType: 'folder',
                        modified: 0,
                        drive:true
                    })
                });
            });
            return;
        }

        var _root = "";

        if (isWin) {
            if(_path.length===5){//   /C:/*
                var _parts = _path.split(':/');
                if(_parts.length==2){
                    _root = _parts[0][1] + ":/";
                    _options = {
                        root:_root
                    };
                    _path = "/*"
                }
            }else{
                var _parts = _path.split(':/');
                if(_parts.length==2){
                    _root = _parts[0][1] + ":/";
                    _options = {
                        root:_root
                    };
                    _path = '' + _parts[1];
                }
            }
        }
        function pathFromWin(current, root) {

            if(!current){
                return "";
            }
            if (isWin && (!root || root === '/')) {
                current = '' + current;
            } else {
                current = _path.join(root, current);
            }

            return current;
        }


        var files = nGlob.sync(_path, _options);

        if(!_.isArray(files)){
            files = [files];
        }
        var out = [];
        _.each(files,function(file){
            var object = utils.__toObject(file);
            if(isWin && object && object.path) {
                object.path = path.resolve(pathFromWin(object.path));
            }
            object.realPath = path.resolve(object.path);
            object && out.push(object);
        });
        return out;
    }

    utils.resolve=function(_path){
        var here = path.resolve(_path);
        try {
            if (fs.statSync(here)) {
                return here;
            }
        }catch(e){
            return here;
        }

        try {
            if (fs.statSync(_path)) {
                return _path;
            } else {
                var __path = process.cwd() + path.sep + _path;
                if (fs.statSync(__path)) {
                    return __path;
                }
            }
        }catch(e){
        }
        return null;
    };
    utils.isDirectory=function(_path){

        var path = utils.resolve(_path);
        if(path){
            var stats = fs.statSync(path);
            if(stats && stats.isDirectory()){
                return true;
            }
        }
        return false;
    };

    /***
     *
     * @param filename
     * @returns {*}
     */
    utils.getExtension=function(filename) {
        var ext = path.extname(filename||'').split('.');
        return ext[ext.length - 1];
    };

    /***
     *
     * @param file {string}
     * @memberOf module:nxapp/utils
     * @returns {string}
     */
    utils.readFile=function(file) {
        //file = file;
        var size = fs.statSync(file).size,
            buf = new Buffer(size),
            fd = fs.openSync(file, 'r');
        if (!size)
            return "";
        fs.readSync(fd, buf, 0, size, 0);
        fs.closeSync(fd);
        return buf.toString();
    };

    /***
     *
     * @param file
     * @param content
     */
    utils.writeFile=function(file,content){
        fs.writeFile(file, content, function(err) {
            if(err) {
                console.log(err);
            } else {
                //console.log("The file was saved! : \n");
            }
        });
    };

    /***
     *
     * @param file
     * @param content
     */
    utils.writeJSONFile=function(file,content) {
        var contentStr = JSON.stringify(content, null, 4);
        fs.writeFile(file, contentStr, function(err) {
            if(err) {
                console.log(err);
            } else {
                //console.log("The file was saved! : \n");
            }
        });
    };

    //xutils.resolve = utils.resolve;
    
    //xutils.mixin(utils,utils);

    /*
    module.exports.readFile=readFile;
    module.exports.writeFile=writeFile;
    module.exports.writeJSONFile=writeJSONFile;
    */
    return utils;
});