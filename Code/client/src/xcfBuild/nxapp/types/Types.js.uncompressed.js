define("nxapp/types/Types", [
    "dojo/_base/lang",
    'xide/types/Types',
    'xcf/types/Types'
],function(lang,types){

    lang.mixin(types.EVENTS,{
        ON_FILE_CHANGED:'fileChanged',
        ON_CONNECTION_CREATED:'connectionCreated',
        ON_CONNECTION_ERROR:'connectionError',
        ON_WEBSOCKET_START:'webSocketStart',
        ON_WEBSOCKET_CONNECTION:'webSocketConnection',
        ON_MQTT_MESSAGE:'onMQTTMessage',
        ON_DEVICE_MESSAGE:'onDeviceMessage',
        ON_COMMAND_FINISH:'onCommandFinish',
        ON_COMMAND_ERROR:'onCommandError',
        ON_DEVICE_DISCONNECTED:'onDeviceDisconnected',
        ON_DEVICE_CONNECTED:'onDeviceConnected',
        STORE_CHANGED:'storeChange',
        BEFORE_STORE_CHANGE:'beforeStoreChange',
        STORE_REFRESHED:'storeRefreshed',
        ON_DID_OPEN_ITEM:'onDidOpenItem',
        ON_SHOW_PANEL:'onShowPanel',
        ITEM_SELECTED:'itemSelected',
        ERROR:'fileOperationError',
        STATUS:'fileOperationStatus',
        IMAGE_LOADED:'imageLoaded',
        IMAGE_ERROR:'imageError',
        RESIZE:'resize',
        UPLOAD_BEGIN:'uploadBegin',
        UPLOAD_PROGRESS:'uploadProgress',
        UPLOAD_FINISH:'uploadFinish',
        ON_CLIPBOARD_COPY:'onClipboardCopy',
        ON_CLIPBOARD_PASTE:'onClipboardPaste',
        ON_CLIPBOARD_CUT:'onClipboardCut',
        ON_CONTEXT_MENU_OPEN:'onContextMenuOpen',
        ON_PLUGIN_LOADED:'onPluginLoaded',
        ON_PLUGIN_READY:'onPluginReady',
        ON_MAIN_VIEW_READY:'onMainViewReady2',
        REGISTER_EDITOR:'registerEditor',
        REGISTER_ACTION:'registerAction',
        ON_FILE_CONTENT_CHANGED:'onFileContentChanged',
        ON_PANEL_CLOSED:'onPanelClosed',
        ON_PANEL_CREATED:'onPanelCreated',
        SET_DEVICE_VARIABLES:'setDeviceVariables'
    });
    types.LOG_CONTEXT={
        DEVICE:'Device'
    };
    return types;
});