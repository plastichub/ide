define("nxapp/client/WebSocket", [
    'dojo/_base/declare',
    'dojo/_base/lang',
    'nxapp/utils/_LogMixin',
    'nxapp/client/ClientBase',
    /*'dojo/node!sockjs-client-ws',*/
    'dojo/node!sockjs-client'
],function(declare,lang,_logMixin,ClientBase,sjsc2){

    return declare("nxapp.client.WebSocket", [ClientBase,_logMixin],
    {
        _socket:null,
        debugConnect:true,
        connect:function(_options) {
            this.options = lang.mixin(this.options, _options);

            var host = this.options.host;
            var port = this.options.port;

            if (this.options.debug)
            {
                this.initLogger(this.options.debug);
            }

            this.log("Sock-JS-Client Connecting to "+host+':'+port, "socket_client");

            var socket = sjsc2.create(host+":"+port);

            var self = this;

            socket.on('close', function(){
                console.log('on close!',self.options);
            });

            socket.on('connection', function(){
                self.log("socket client connected", "socket_client");

                socket.on('error', function(e){
                    console.error('Socket client error ');
                });
                socket.on('end', function(){
                    self.log("socket client connection end", "socket_client");
                });

            });
            this._socket = socket;
        },
        emit:function(signal,data) {
            if(this.showDebugMsg("socket_client")){
                console.log("Emiting: "+signal+" with: ");
                console.dir(data);
            }
            this._socket.write(data);
        },
        onSignal:function(signal,callback) {
          this._socket.on('data',callback);
        }

    });
});