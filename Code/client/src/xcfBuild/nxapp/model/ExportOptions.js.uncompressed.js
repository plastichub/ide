/** @module nxapp/model/ExportOptions */
define("nxapp/model/ExportOptions", [
    'dcl/dcl',
    'xide/model/Base',
    'xide/mixins/EventedMixin',
    'xide/encoding/MD5',
    'xide/utils'
], function (dcl,Base,EventedMixin,MD5,utils) {

    var Module = dcl([Base.dcl,EventedMixin.dcl],{
        connected:false,
        __registered:false,
        declaredClass:'nxapp/model/ExportOptions',
        toString:function(){
            var options = this.options;
            return options.deviceScope +'://' + options.host + ':'+options.port+'@'+options.protocol;
        },
        constructor:function(options){
            this.options = options;
        }
    });
    return Module;
});