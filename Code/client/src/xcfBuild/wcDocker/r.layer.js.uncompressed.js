require.config({
    baseUrl: "./bower_components",
    packages: [
        {
            name: 'dcl',
            location: './dcl'   //points to bower_components/dcl
        },
        {
            name: 'wcDocker',
            location: '../src'
        }
    ]
});

require.config({
    config: {}
});

//require docker
require([
    "wcDocker/docker",
    "wcDocker/splitter",
    "wcDocker/tabframe"
], function (wcDocker, wcSplitter, wcTabFrame) {

    //export
    window['wcDocker'] = wcDocker;
    window['wcSplitter'] = wcSplitter;
    window['wcTabFrame'] = wcTabFrame;
    console.log('exported wcDocker');
}, undefined, true);    // Force synchronous loading so we don't have to wait.