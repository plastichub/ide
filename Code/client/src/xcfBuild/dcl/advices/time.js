//>>built
(function(a){"undefined"!=typeof define?define("dcl/advices/time",[],a):"undefined"!=typeof module?module.exports=a():dclAdvicesTime=a()})(function(){var a=0;return function(c){var b=0;c||a++;return{before:function(){b++},after:function(){--b}}}});
//# sourceMappingURL=time.js.map