define("xconsole/types", [
    'xide/types'
],function(types){
    /**
     * Enumeration to select built-in lanugages for an expression explicit.
     *
     * @enum {string} module:xconsole/types/EXPRESSION_LANGUAGE
     * @memberOf module:xide/types
     */
    types.EXPRESSION_LANGUAGE={
        /**
         * Custom language, might resolved at run-time via plugin or similar.
         * @constant
         * @type string
         */
        CUSTOM:'custom',
        /**
         * Vanilla Javascript
         * @constant
         * @type string
         */
        JAVA_SCRIPT:'js',
        /**
         * Jerry - Script (Samsung's version)
         *
         * @constant
         * @type string
         */
        JERRY_SCRIPT:'qs',
        /**
         * Pseudo -  Idiomatic code generation. This AST.
         *
         * @link https://github.com/alehander42/pseudo
         * @constant
         * @type string
         */
        PSEUDO:'ps',
        /**
         * Typescript
         * @constant
         * @type string
         */
        TYPE_SCRIPT:'ts',
        /**
         * Rust
         * @constant
         * @type string
         */
        RUST:'rs',
        /**
         * Haskell
         * @constant
         * @type string
         */
        HASKELL:'hs',
        /**
         * Use file extension hint to resolve language
         * @constant
         * @type string
         */
        FILE_EXTENSION:'mime'
    }
    return types;
});