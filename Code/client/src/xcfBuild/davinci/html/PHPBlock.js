//>>built
define("davinci/html/PHPBlock",["dojo/_base/declare","davinci/html/HTMLItem"],function(b,c){return b("davinci.html.PHPBlock",c,{constructor:function(a){this.elementType="PHPBlock";this.value=a||""},getText:function(a){return a.excludeIgnoredContent?"":this.value}})});
//# sourceMappingURL=PHPBlock.js.map