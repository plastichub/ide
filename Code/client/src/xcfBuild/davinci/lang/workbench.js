//>>built
define("davinci/lang/workbench",[],function(){return{fileHasUnsavedChanges:"The file '${0}' has unsaved changes. Are you sure you want to close WITHOUT saving?",outlineNotAvailable:"An outline is not available",preferences:"Preferences",noUserPref:"no user preferences...",restoreDefaults:"Restore Defaults"}});
//# sourceMappingURL=workbench.js.map