define("davinci/ve/actions/_ReorderAction", [
		"dojo/_base/declare",
		"dojo/_base/array",
		"dojo/dom-style",
		"davinci/ve/actions/ContextAction"
], function(declare, Array, DomStyle, ContextAction){


return declare("davinci.ve.actions._ReorderAction", [ContextAction], {
	
	/**
	 * Only show reorder commands on context menu for visual page editor
	 */
	shouldShow: function(context){
		context = this.fixupContext(context);
		return context && context.editor && context.editor.editorID == "davinci.ve.HTMLPageEditor";
	},
	
	/**
	 * Return true if all items in selection share the same parent
	 * and are all absolutely positioned
	 * @param {Object} selection  currently list of selected widgets
	 */
	selectionSameParentAllAbsolute: function(selection){
		if(selection.length === 0){
			return false;
		}
		const firstParent = selection[0].getParent();
		if(!firstParent){
			return false;
		}
		for(let i=0; i<selection.length; i++){
			const widget = selection[i];
			if(widget.getParent() != firstParent){
				return false;
			}
			const position = (widget && widget.domNode) ? DomStyle.get(widget.domNode, 'position') : null;
			if(position != 'absolute'){
				return false;
			}
		}
		return true;
	},
	
	/**
	 * Return true if all items in selection share the same parent
	 * and are all absolutely positioned, and all selected widgets are adjacent children
	 * @param {Object} selection  currently list of selected widgets
	 */
	selectionSameParentAllAbsoluteAdjacent: function(selection){
		if(!selection || selection.length === 0 || !this.selectionSameParentAllAbsolute(selection)){
			return false;
		}
		const parent = selection[0].getParent();
		if(!parent || !parent.length){
			return false;
		}
		const children = parent.getChildren();
		let minIndex = Number.MAX_VALUE;
		let maxIndex = Number.MIN_VALUE;
		for(let i=0; i<children.length; i++){
			const child = children[i];
			const index = selection.indexOf(child);
			if(index >=0){
				minIndex = (i < minIndex) ? i : minIndex;
				maxIndex = (i > maxIndex) ? i : maxIndex;
			}
		}
		return maxIndex - minIndex + 1 === selection.length;
	},

	/**
	 * Return all widget siblings that are absolutely positioned. 
	 * The list will include widget if it is absolutely positioned.
	 * @param {Object} widget  A dvWidget object
	 * @returns {Array[_dvWidget]}  all absolutely positioned siblings, possible including  widget
	 */
	getAbsoluteSiblings: function(widget){
		const parent = (widget && widget.getParent && widget.getParent());
		if(!parent){
			return [];
		}
		const siblings = (parent.getChildren && parent.getChildren());
		if(!siblings){
			return [];
		}
		const absSiblings = Array.filter(siblings, function(item){
			const position = (item && item.domNode) ? DomStyle.get(item.domNode, 'position') : null;
			return (position == 'absolute');
		});
		return absSiblings;
	}

});
});