define("davinci/ve/actions/ViewSourceAction", [
    	"dojo/_base/declare",
    	"davinci/ve/actions/ContextAction",
    	"davinci/lang/ve"
], function(declare, ContextAction, veNls){


return declare("davinci.ve.actions.ViewSourceAction", [ContextAction], {

	run: function(context){
		context = this.fixupContext(context);
		if(context && context.editor && context.editor.switchDisplayModeSourceLatest){
			editor = context.editor;
			editor.switchDisplayModeSourceLatest();
		}
	},
	
	updateStyling: function(){
		const editor = davinci.Workbench.getOpenEditor();
		if(editor && editor.getDisplayMode){
			const displayMode = editor.getDisplayMode();
			const sourceDisplayMode = editor.getSourceDisplayMode();
			const sourceComboButtonNode = dojo.query('.maqSourceComboButton')[0];
			if(sourceComboButtonNode){
				const sourceComboButton = dijit.byNode(sourceComboButtonNode);
				if(sourceComboButton){
					sourceComboButton.set('label', veNls['SourceComboButton-'+sourceDisplayMode]);
				}
				if (displayMode=="design") {
					dojo.removeClass(sourceComboButtonNode, 'maqLabelButtonSelected');
				}else{
					dojo.addClass(sourceComboButtonNode, 'maqLabelButtonSelected');
				}
			}
		}
	}

});
});