define("davinci/ve/actions/SelectAncestorAction", [
	"dojo/_base/declare",
	"davinci/ve/actions/_SelectAncestorAction",
	'xide/views/_CIPanelDialog',
	'xide/utils',
	'xide/types',
	'xide/lodash'
], function (declare, _SelectAncestorAction, _CIPanelDialog, utils, types, _) {

	return declare("davinci.ve.actions.SelectAncestorAction", [_SelectAncestorAction], {

		run: function (context) {
			context = this.fixupContext(context);
			const selection = (context && context.getSelection());
			console.log('s');

			if (this.selectionSameParentNotBody(selection)) {
				const ancestors = [];
				let ancestor = selection[0].getParent();

				while (ancestor.domNode.tagName != 'BODY') {
					ancestors.push(ancestor);
					ancestor = ancestor.getParent();
				}
				if (ancestor.domNode.tagName === 'BODY') {
					ancestors.push(ancestor);
				}
				var options = [];
				for (let i = 0; i < ancestors.length; i++) {
					var widget = require("davinci/ve/widget");
					const label = widget.getLabel(ancestors[i]);
					options.push({
						value: _.unescape(label),
						label: label,
						widget: ancestors[i]
					});
				}


				var cis = [
					utils.createCI('New Parent', types.ECIType.ENUMERATION, '', {
						options: options
					})
				];


				var actionDialog = new _CIPanelDialog({
					title: 'Select new parent',
					resizeable: true,
					onOk: function (changedCIS) {
						const w = _.find(options, {
							value: cis[0].value
						});
						context.select(w.widget);
					},
					cis: cis
				});
				actionDialog.show();
				/*

				const dialog = Workbench.showMessage(langObj.selectAncestorTitle, formHtml);

				const selWidget = dijit.byId('SelectAncestor');
				selWidget._selectAncestor = this._selectAncestor;
				dojo.connect(selWidget, "onChange", function (ancestor) {
					context.select(ancestors[ancestor]);
				});
				*/
			}
		},

		isEnabled: function (context) {
			context = this.fixupContext(context);
			const selection = (context && context.getSelection());
			return this.selectionSameParentNotBody(selection);
		}

	});
});