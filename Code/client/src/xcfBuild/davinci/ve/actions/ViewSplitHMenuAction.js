//>>built
define("davinci/ve/actions/ViewSplitHMenuAction",["dojo/_base/declare","davinci/ve/actions/ContextAction"],function(b,c){return b("davinci.ve.actions.ViewSplitHMenuAction",[c],{run:function(a){(a=this.fixupContext(a))&&a.editor&&a.editor.switchDisplayModeSplitHorizontal&&a.editor.switchDisplayModeSplitHorizontal()}})});
//# sourceMappingURL=ViewSplitHMenuAction.js.map