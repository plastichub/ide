define("davinci/ve/actions/RotateDeviceAction", [
    	"dojo/_base/declare",
	"davinci/actions/Action"
], function(declare, Action){


return declare("davinci.ve.actions.RotateDeviceAction", [Action], {

	run: function(selection){
		const e = davinci.Workbench.getOpenEditor();
		const context = e.getContext();
		context.visualEditor.toggleOrientation();		
	},
	
	isEnabled: function(selection){
		const e = davinci.Workbench.getOpenEditor();
		if (e && e.getContext){
			const context = e.getContext();
			if(context.getMobileDevice){
				const device = context.getMobileDevice();
				return (device && device != '' && device != 'none' && device != 'desktop');
			}else{
				return false;
			}
		}else{
			return false;
		}
	},
	
	updateStyling: function(){
		let landscape = false;
		const editor = davinci.Workbench.getOpenEditor();
		if(editor){
			const visualEditor = editor.visualEditor;
			if(visualEditor && visualEditor.getOrientation){
				const orientation = visualEditor.getOrientation();
				landscape = (orientation == 'landscape');
			}
		}
		const landscapeClass = 'orientationLandscape';
		if(landscape){
			dojo.addClass(document.body, landscapeClass);
		}else{
			dojo.removeClass(document.body, landscapeClass);
		}
	}
});
});