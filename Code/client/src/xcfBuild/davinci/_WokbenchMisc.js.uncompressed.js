define("davinci/_WokbenchMisc", [
    "dcl/dcl",
    "require",
    "./Runtime",
    "./ui/Dialog",
    "davinci/lang/webContent",
    "./ve/metadata",
    "dojo/_base/connect",
    "xide/manager/ServerActionBase"
], function(
    dcl,
    require,
    Runtime,
    Dialog,
    webContent,
    metadata,
    connect,
    ServerActionBase
) {
    function getWorkbench(){
        return davinci.Workbench;
    }

    var filename2id = function(fileName) {
        return "editor-" + encodeURIComponent(fileName.replace(/[\/| |\t]/g, "_")).replace(/%/g, ":");
    };

    var handleIoError = function (deferred, reason) {
        /*
         *  Called by the subscription to /dojo/io/error , "
         *  /dojo/io/error" is sent whenever an IO request has errored.
         *	It passes the error and the dojo.Deferred
         *	for the request with the topic.
         */

        if (reason.status == 401 || reason.status == 403) {
            sessionTimedOut();
            // Only handle error if it is as of result of a failed XHR connection, not
            // (for example) if a callback throws an error. (For dojo.xhr, def.cancel()
            // is only called if connection fails or if it times out.)
        } else if (deferred.canceled === true) {
            // Filter on XHRs for maqetta server commands.  Possible values which we
            // test for:
            //     cmd/findResource
            //     ./cmd/createResource
            //     http://<host>/maqetta/cmd/getComments
            var reCmdXhr = new RegExp('(^|\\.\\/|' + document.baseURI + '\\/)cmd\\/');
            var url = deferred.ioArgs.url;
            if (reCmdXhr.test(url)) {
                // Make exception for "getBluePageInfo" because it regularly gets cancelled
                // by the type ahead searching done from the combo box on the 3rd panel of
                // the R&C wizard. The cancellation is not really an error.
                if (url.indexOf("getBluePageInfo") >= 0) {
                    return;
                }
            } else {
                // Must not be a Maqetta URL (like for JSONP on GridX), so skip
                return;
            }

            Runtime.handleError(reason.message);
            console.warn('Failed to load url=' + url + ' message=' + reason.message +
                ' status=' + reason.status);
        }
    };

    var sessionTimedOut = function(){
        var loginHref = "welcome";
        var dialog = new Dialog({
            title: webContent.sessionTimedOut
            //,  style: "width: 300px"
        });
        var message =  dojo.string.substitute(webContent.sessionTimedOutMsg, {hrefLoc: loginHref});
        dialog.set("content", message);
        dojo.connect(dialog, "onCancel", null, function(){window.location.href = loginHref;});
        setTimeout(function(){window.location.href=loginHref;}, 10000); // redirect to login in 10 sec
        dialog.show();
    };

    var Module = dcl(null,{
        run: function() {

            var Workbench = getWorkbench();
            console.error('run ');
            Runtime.run();
            Workbench._initKeys();
            Workbench._baseTitle = dojo.doc.title;
            try{

                Workbench.initService();
            }catch(e){
                debugger;
            }

            // Set up top banner region. (Top banner is an extensibility point)
            if(window.maqetta && maqetta.TopBanner && maqetta.TopBanner.setup){
                maqetta.TopBanner.setup();
            }
            

            Runtime.subscribe("/davinci/resource/resourceChanged",
                function (type, changedResource) {
                    if (type == 'deleted') {
                        var editorId = filename2id(changedResource.getPath());
                        var shadowId = editorIdToShadowId(editorId);
                        var editorContainer = dijit.byId(editorId);
                        var shadowTab = dijit.byId(shadowId);
                        if (editorContainer && !editorContainer._isClosing) {
                            var editorsContainer = dijit.byId("editors_container");
                            var shadowTabContainer = dijit.byId("davinci_file_tabs");
                            editorsContainer.removeChild(editorContainer);
                            editorContainer.destroyRecursive();
                            shadowTabContainer.removeChild(shadowTab);
                            shadowTab.destroyRecursive();
                        }
                    }
                }
            );
            Runtime.subscribe('/dojo/io/error', handleIoError); // /dojo/io/error" is sent whenever an IO request has errored.
                                                               // requires djConfig.ioPublish be set to true in pagedesigner.html

            Runtime.subscribe("/davinci/states/state/changed",
                function(e) {
                    // e:{node:..., newState:..., oldState:...}
                    var currentEditor = Runtime.currentEditor;
                    // ignore updates in theme editor and review editor
                    if ((currentEditor.declaredClass != "davinci.ve.themeEditor.ThemeEditor" &&
                            currentEditor.declaredClass != "davinci.review.editor.ReviewEditor") /*"davinci.ve.VisualEditor"*/) {
                        currentEditor.visualEditor.onContentChange.apply(currentEditor.visualEditor, arguments);
                    }
                }
            );
            Runtime.subscribe("/davinci/ui/widgetPropertiesChanges",
                function() {
                    var ve = Runtime.currentEditor.visualEditor;
                    ve._objectPropertiesChange.apply(ve, arguments);
                }
            );

            // bind overlay widgets to corresponding davinci states. singleton; no need to unsubscribe
            connect.subscribe("/davinci/states/state/changed", function(args) {
                //FIXME: This is page editor-specific logic within Workbench.
                var context = (Runtime.currentEditor && Runtime.currentEditor.declaredClass == "davinci.ve.PageEditor" &&
                        Runtime.currentEditor.visualEditor && Runtime.currentEditor.visualEditor.context);
                if(!context){
                    return;
                }
                var prefix = "_show:", widget, dvWidget, helper;
                var thisDijit = context ? context.getDijit() : null;
                var widgetUtils = require("davinci/ve/widget");
                if (args.newState && !args.newState.indexOf(prefix)) {
                    widget = thisDijit.byId(args.newState.substring(6));
                    dvWidget = widgetUtils.getWidget(widget.domNode);
                    helper = dvWidget.getHelper();
                    helper && helper.popup && helper.popup(dvWidget);
                }
                if (args.oldState && !args.oldState.indexOf(prefix)) {
                    widget = thisDijit.byId(args.oldState.substring(6));
                    dvWidget = widgetUtils.getWidget(widget.domNode);
                    helper = dvWidget.getHelper();
                    helper && helper.tearDown && helper.tearDown(dvWidget);
                }
            });

            // bind overlay widgets to corresponding davinci states. singleton; no need to unsubscribe
            connect.subscribe("/davinci/ui/repositionFocusContainer", function(args) {
                Workbench._repositionFocusContainer();
            });


            //ice.serviceObject[this.serviceClass]['getInfo']().
            var d = this.service.serviceObject[this.serviceClass]['getInfo']().then(
                function(result){
                    Runtime._initializationInfo = result;

                    var userInfo = result.userInfo;
                    Runtime.isLocalInstall = userInfo.userId == 'maqettaUser';

                    // Needed by review code
                    Runtime.userName = userInfo.userId;
                    Runtime.userEmail = userInfo.email;
                    return metadata.init();

                }).then(function(){
                    var perspective = Runtime.initialPerspective || "davinci.ui.main";
                    dojo.query('.loading').orphan();
                    Workbench.showPerspective(perspective);
                    Workbench._updateTitle();
                    initializeWorkbenchState();
                }).otherwise(function(result){
                    dojo.query('#load_screen').addContent(dojo.string.substitute(webContent.startupError, [result.message]), "only");
                });

            /*
            var d = xhr.get({
                url: "cmd/getInitializationInfo",
                handleAs: "json"
            }).then(function(result){
                Runtime._initializationInfo = result;

                var userInfo = result.userInfo;
                Runtime.isLocalInstall = userInfo.userId == 'maqettaUser';

                // Needed by review code
                Runtime.userName = userInfo.userId;
                Runtime.userEmail = userInfo.email;
                return metadata.init();
            }).then(function(){
                var perspective = Runtime.initialPerspective || "davinci.ui.main";
                dojo.query('.loading').orphan();
                Workbench.showPerspective(perspective);
                Workbench._updateTitle();
                initializeWorkbenchState();
            }).otherwise(function(result){
                dojo.query('#load_screen').addContent(dojo.string.substitute(webContent.startupError, [result.message]), "only");
            });
            */

            //dojo.query('#load_screen').addContent(dojo.string.substitute(webContent.startupError, [result.message]), "only").addClass("error");

            Workbench._lastAutoSave = Date.now();
            setInterval(dojo.hitch(this,"_autoSave"),30000);
            return d;
        },
        initService:function(){
            if(!this.service){
                this.service = new ServerActionBase({
                    serviceClass:'XApp_XIDE_Workbench_Service',
                    serviceUrl:'./xcf/xui.php?debug=true&view=rpc',
                    singleton:false
                });
                this.service.init();
            }
        },
        findView: function (viewID) {
            var domNode=dijit.byId(viewID);
            if (domNode) {
                return domNode;
            }
        },
        //FIXME: remove. Use Runtime.location() instead.
        location: function() {
            return Runtime.location();
        },
        logoff: function(args) {
            return;
        },
        unload: function () {
            getWorkbench()._autoSave();
        },
        /*
         * running in single project mode or multi project mode
         */
        singleProjectMode: function() {
            return true;
        },

        getProject: function() {
            var Workbench = getWorkbench();
            return Workbench.getActiveProject() || Workbench._DEFAULT_PROJECT;
        },
        loadProject: function(projectName) {
            var Workbench = getWorkbench();
            Workbench.setActiveProject(projectName);
            return Workbench.updateWorkbenchState().then(function(){
                // make sure the server has maqetta setup for the project
                location.href="cmd/configProject?configOnly=true&project=" + encodeURIComponent(projectName);
            });
            // if the project was set via URL parameter, clear it off.
        },
        getActiveProject: function() {
            /* need to check if there is a project in the URL.  if so, it takes precidence
             * to the workbench setting
             */
            //xmaqhack
            return 'project1';
            /*

            if (!Workbench._state){
                Workbench._state = Runtime.getWorkbenchState();
            }
            var urlProject = dojo.queryToObject(dojo.doc.location.search.substr((dojo.doc.location.search[0] === "?" ? 1 : 0))).project;

            if(urlProject){
                Workbench.loadProject(urlProject);
            }

            if (Workbench._state.hasOwnProperty("project")) {
                return Workbench._state.project;
            }

            return Workbench._DEFAULT_PROJECT;
            */
        },

        setActiveProject: function(project){
            var Workbench = getWorkbench();
            if(!Workbench._state){
                Workbench._state = {};
            }
            Workbench._state.project = project;
            Workbench.saveState = true;
        },
        setActionScope: function(scopeID,scope) {
            var Workbench = getWorkbench();
            Workbench.actionScope[scopeID]=scope;
        },
        clearWorkbenchState: function() {
            var Workbench = getWorkbench();
            Workbench._state = {};
            return Workbench.updateWorkbenchState();
        },

        updateWorkbenchState: function(){
            var Workbench = getWorkbench();
            delete Workbench.saveState;
            return null;
            return this.service.serviceObject[this.serviceClass]['setState'](JSON.stringify(Workbench._state));
            /*
             return xhr.get({
             url: "cmd/setWorkbenchState",
             putData: JSON.stringify(Workbench._state),
             handleAs:"text"
             });
             */
        },
        getDefaultMount:function(){
        },
        /**
         * Retrieves a custom property from current workbench state
         * @param {string} propName  Name of custom property
         * @return {any} propValue  Any JavaScript value.
         */
        workbenchStateCustomPropGet: function(propName){
            var Workbench = getWorkbench();
            if(typeof propName == 'string'){
                return Workbench._state[propName];
            }
        },
        /**
         * Assign a custom property to current workbench state and persist new workbench state to server
         * @param {string} propName  Name of custom property
         * @param {any} propValue  Any JavaScript value. If undefined, then remove given propName from current workbench state.
         */
        workbenchStateCustomPropSet: function(propName, propValue) {
            var Workbench = getWorkbench();
            if (typeof propName == 'string') {
                if (typeof propValue == 'undefined') {
                    delete Workbench._state[propName];
                } else {
                    Workbench._state[propName] = propValue;
                }
                Workbench.saveState = true;
            }
        },
        _autoSave: function(){
            var Workbench = getWorkbench();
            var lastSave = Workbench._lastAutoSave;
            var anyErrors = false;
            function saveDirty(editor){
                if (editor.isReadOnly /*|| !editor.isDirty*/) {
                    console.error('workbench : autosave failed : isReadyOnly or is not dirty' );
                    return;
                }

                var modified = editor.lastModifiedTime;
                var force = true;
                if (force ||( modified && modified > lastSave)){
                    try {
                        editor.save(true);
                    }catch(ex){
                        console.error("Error while autosaving file:" + ex);
                        anyErrors = true;
                    }
                }
            }
            if(this.currentEditor){
                saveDirty(this.currentEditor);
            }

            /*
             if(Workbench.editorTabs){
             dojo.forEach(Workbench.editorTabs.getChildren(), saveDirty);
             }
             */
            if(!anyErrors){
                Workbench._lastAutoSave = Date.now();
            }

        },
        _updateTitle: function(currentEditor) {
            var Workbench = getWorkbench();
            var newTitle=Workbench._baseTitle || "";
            if (currentEditor) {
                newTitle = newTitle + "";
                if (currentEditor.isDirty) {
                    newTitle=newTitle+"*";
                }
                newTitle=newTitle+currentEditor.item.name;
            }
            dojo.doc.title=newTitle;
        },
        _initActionsKeys: function(actionSets, args) {
            var Workbench = getWorkbench();

            var keysDomNode = args.keysDomNode || args.domNode,
                keys = {},
                wasKey;
            dojo.forEach(actionSets, function(actionSet){
                dojo.forEach(actionSet.actions, function(action){
                    if (action.keySequence) {
                        keys[action.keySequence]=action;
                        wasKey=true;
                    }
                });
            });
            if (wasKey) {
                var context=args.context;
                dojo.connect(keysDomNode, "onkeydown", function (e){
                    var seq = Workbench._keySequence(e),
                        actionItem = keys[seq];
                    if (actionItem) {
                        if (actionItem.action.shouldShow && !actionItem.action.shouldShow(context)) {
                            return;
                        }
                        if (actionItem.action.isEnabled(context)) {
                            Workbench._runAction(actionItem,context);
                        }
                    }
                });
            }
        },

        _initKeys: function () {
            var Workbench = getWorkbench();
            var keys={all: []};
            var keyExtensions=Runtime.getExtensions("davinci.keyBindings");
            dojo.forEach(keyExtensions, function(keyExt){
                var contextID= keyExt.contextID || "all";
                var keyContext=keys[contextID];
                if (!keyContext) {
                    keyContext=keys[contextID]=[];
                }

                keyContext[keyExt.sequence]=keyExt.commandID;
            });

            Workbench.keyBindings=keys;
        },

        handleKey: function (e) {
            var Workbench = getWorkbench();

            if (!Workbench.keyBindings) {
                return;
            }
            var seq=Workbench._keySequence(e);
            var cmd;
            if (Workbench.currentContext && Workbench.keyBindings[Workbench.currentContext]) {
                cmd=Workbench.keyBindings[Workbench.currentContext][seq];
            }
            if (!cmd) {
                cmd=Workbench.keyBindings.all[seq];
            }
            if (cmd) {
                Runtime.executeCommand(cmd);
                return true;
            }
        },

        _keySequence: function (e) {
            var Workbench = getWorkbench();

            var seq=[];
            if (window.event)
            {
                if (window.event.ctrlKey) {
                    seq.push("M1");
                }
                if (window.event.shiftKey) {
                    seq.push("M2");
                }
                if (window.event.altKey) {
                    seq.push("M3");
                }
            }
            else
            {
                if (e.ctrlKey || (e.modifiers==2) || (e.modifiers==3) || (e.modifiers>5)) {
                    seq.push("M1");
                }
                if (e.shiftKey || (e.modifiers>3)) {
                    seq.push("M2");
                }
                if(e.modifiers) {
                    if (e.altKey || (e.modifiers % 2)) {
                        seq.push("M3");
                    }
                }
                else {
                    if (e.altKey) {
                        seq.push("M3");
                    }
                }
            }

            var letter=String.fromCharCode(e.keyCode);
            if (/[A-Z0-9]/.test(letter)) {
                //letter=e.keyChar;
            } else {
                var keyTable = {
                    46: "del",
                    114: "f3"
                };

                letter = keyTable[e.keyCode] || "xxxxxxxxxx";
            }
            letter=letter.toUpperCase();
            if (letter==' ') {
                letter="' '";
            }

            seq.push(letter);
            return seq.join("+");
        },
        setupGlobalKeyboardHandler: function() {
            var actionSets = Runtime.getExtensions('davinci.actionSets');
            dojo.forEach(actionSets, function(actionSet) {
                if (actionSet.id == "davinci.ui.main" || actionSet.id == "davinci.ui.editorActions") {
                    dojo.forEach(actionSet.actions, function(action) {
                        if (action.keyBinding) {
                            Runtime.registerKeyBinding(action.keyBinding, action);
                        }
                    });
                }
            });
        }
    });

    return Module;

});