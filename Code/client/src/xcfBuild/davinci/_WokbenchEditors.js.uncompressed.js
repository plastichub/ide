define("davinci/_WokbenchEditors", [
    "dcl/dcl",
    "./Runtime",
    "dojo/promise/all"
], function(
    dcl,
    Runtime,
    all) {

    function getWorkbench(){
        return davinci.Workbench;
    }

    // Convert the result from filename2id into a different ID string that replaces "editor" with "shadow"
    var editorIdToShadowId = function(editorFileName) {
        return editorFileName.replace(/^editor/, "shadow");
    };
//Convert the result from filename2id into a different ID string that replaces "editor" with "shadow"
    var shadowIdToEditorId = function(shadowFileName) {
        return shadowFileName.replace(/^shadow/, "editor");
    };


    var Module = dcl(null,{
        closeAllEditors: function() {
            var editorsContainer = dijit.byId("editors_container");
            if (editorsContainer) {
                editorsContainer.getChildren().forEach(function(child){
                    editorsContainer.closeChild(child);
                });
            }
        },
        getOpenEditor: function(resource) {

            if(this.currentEditor){
                return this.currentEditor;
            }

            if(resource!=null){
                var tab = dijit.byId(filename2id(resource.getPath()));
                if (tab) {
                    return tab.editor;
                }
                return null; // no editor found for given resource
            }


            var editorsContainer = dijit.byId("editors_container");
            if (editorsContainer && editorsContainer.selectedChildWidget && editorsContainer.selectedChildWidget.editor) {
                return editorsContainer.selectedChildWidget.editor;
            }
            return null;
        },

        closeActiveEditor: function() {
            var editorsContainer = dijit.byId("editors_container");
            var shadowTabContainer = dijit.byId("davinci_file_tabs");

            if (editorsContainer && editorsContainer.selectedChildWidget && editorsContainer.selectedChildWidget.editor) {
                var editorId = selectedChildWidget.id;
                var shadowId = editorIdToShadowId(editorId);
                editorsContainer.closeChild(editorsContainer.selectedChildWidget);
                var shadowTab = dijit.byId(shadowId);
                if(shadowTab){
                    shadowTabContainer.closeChild(shadowTab);
                }
            }
        },
        getAllOpenEditorIds: function() {},
        _showEditorTopPanes: function(){
            this._hideShowEditorTopPanes('block');
        },
        _hideEditorTopPanes: function(){
            this._hideShowEditorTopPanes('none');
        },
        _hideShowEditorTopPanes: function(displayPropValue){
            //xmaqhack : doesn't exists :
            /*
             var davinci_app = dijit.byId('davinci_app');
             var davinci_file_tabs = dijit.byId('davinci_file_tabs');
             var davinci_toolbar_pane = dijit.byId('davinci_toolbar_pane');

             davinci_file_tabs.domNode.style.display = displayPropValue;
             davinci_toolbar_pane.domNode.style.display = displayPropValue;
             davinci_app.resize();
             */
        },
        /**
         * @obselete
         * With standard TabContainer setup, this callback is invoked
         * whenever an editor tab is closed via user action.
         * But if we are using the "shadow" approach where there is a shadow
         * TabContainer that shows tabs for the open files, and a StackContainer
         * to hold the actual editors, then this callback is invoked indirectly
         * via a removeChild() call in routine _shadowTabClosed() below.
         * @param page  The child widget that is being closed.
         */
        _editorTabClosed: function(page) {
            if(!davinci.Workbench._editorTabClosing[page.id]){
                davinci.Workbench._editorTabClosing[page.id] = true;
                if (page && page.editor && page.editor.fileName) {
                    var editorId = page.id;
                    var shadowId = editorIdToShadowId(editorId);
                    var shadowTabContainer = dijit.byId("davinci_file_tabs");
                    var shadowTab = dijit.byId(shadowId);
                    var i = Workbench._state.editors.indexOf(page.editor.fileName);
                    if (i != -1) {
                        Workbench._state.editors.splice(i, 1);
                    }
                    Workbench.saveState = true;
                    if(!davinci.Workbench._shadowTabClosing[shadowId]){
                        shadowTabContainer.removeChild(shadowTab);
                        shadowTab.destroyRecursive();
                    }
                }
                var editors=dijit.byId("editors_container").getChildren();
                if (!editors.length) {
                    Workbench._switchEditor(null);
                    this._expandCollapsePaletteContainers(null);
                    var editorsStackContainer = dijit.byId('editorsStackContainer');
                    var editorsWelcomePage = dijit.byId('editorsWelcomePage');
                    if (editorsStackContainer && editorsWelcomePage){
                        editorsStackContainer.selectChild(editorsWelcomePage);
                    }
                    this._hideEditorTopPanes();
                }
                delete davinci.Workbench._editorTabClosing[page.id];
            }
        },
        /**
         * @obselete
         * @param keywordArgs
         * @param newHtmlParams
         */
        openEditor: function (keywordArgs, newHtmlParams) {
            try{
                var fileName=keywordArgs.fileName,
                    fileExtension;
                if (typeof fileName=='string') {
                    fileExtension=fileName.substr(fileName.lastIndexOf('.')+1);
                } else {
                    fileExtension=fileName.getExtension();
                    fileName=fileName.getPath();
                }

                var editorContainer = dijit.byId(filename2id(fileName)),
                    editorsContainer = dijit.byId("editors_container");

                if (editorContainer) {
                    // already open
                    editorsContainer.selectChild(editorContainer);
                    var editor=editorContainer.editor;
                    if (keywordArgs.startOffset) {
                        editor.select(keywordArgs);
                    }
                    return;
                }
                var editorCreateCallback=keywordArgs.editorCreateCallback;

                var editorExtensions=Runtime.getExtensions("davinci.editor", function (extension){
                    if (typeof extension.extensions =="string") {
                        extension.extensions=extension.extensions.split(',');
                    }
                    return dojo.some(extension.extensions, function(e){
                        return e.toLowerCase() == fileExtension.toLowerCase();
                    });
                });

                var editorExtension = editorExtensions[0];
                if (editorExtensions.length>1){
                    dojo.some(editorExtensions, function(extension){
                        editorExtension = extension;
                        return extension.isDefault;
                    });
                }

                Workbench._createEditor(editorExtension, fileName, keywordArgs, newHtmlParams).then(function(editor) {
                    if(editorCreateCallback){
                        editorCreateCallback.call(window, editor);
                    }

                    if(!keywordArgs.noSelect) {
                        Runtime.currentEditor = editor;
                    }
                }, function(error) {
                    console.error("Error opening editor for filename: " + fileName, error);
                });
            } catch (ex) {
                console.error("Exception opening editor for filename: "+ keywordArgs && keywordArgs.fileName);
                console.error(ex);
            }
        },
        _switchEditor: function(newEditor, startup) {
            var oldEditor = Runtime.currentEditor;
            Runtime.currentEditor = newEditor;
            var Workbench = getWorkbench();
            if(Workbench._state) {
                Workbench._state.activeEditor = newEditor ? newEditor.fileName : null;
            }
            this._removeFocusContainerChildren();	//FIXME: Visual editor logic bleeding into Workbench
            this._showEditorTopPanes();
            try {
                dojo.publish("/davinci/ui/editorSelected", [{
                    editor: newEditor,
                    oldEditor: oldEditor
                }]);
            } catch (ex) {
                console.error(ex);
            }
            Workbench._updateTitle(newEditor);

            setTimeout(function(){
                // kludge: if there is a visualeditor and it is already populated, resize to make Dijit visualEditor contents resize
                // If editor is still starting up, there is code on completion to do a resize
                // seems necessary due to combination of 100%x100% layouts and extraneous width/height measurements serialized in markup
                if (newEditor && newEditor.visualEditor && newEditor.visualEditor.context && newEditor.visualEditor.context.isActive()) {
                    newEditor.visualEditor.context.getTopWidgets().forEach(function (widget) { if (widget.resize) { widget.resize(); } });
                }

                this._repositionFocusContainer();
            }.bind(this), 1000);

            all(this._showViewPromises).then(function() {
                if(newEditor && newEditor.focus) {
                    newEditor.focus();
                }

                //Rearrange palettes based on new editor
                this._rearrangePalettes(newEditor);

                //Collapse/expand the left and right-side palettes
                //depending on "expandPalettes" properties
                this._expandCollapsePaletteContainers(newEditor);
            }.bind(this));

            if(!startup) {
                Workbench.saveState = true;
            }
        }
    });

    return Module;

});