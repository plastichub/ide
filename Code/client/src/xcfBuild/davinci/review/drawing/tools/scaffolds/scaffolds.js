//>>built
define("davinci/review/drawing/tools/scaffolds/scaffolds",["dojo/_base/declare","./ArrowScaffold","./RectangleScaffold","./EllipseScaffold","./TextScaffold"],function(a,b,c,d,e){a={};a.ArrowScaffold=b;a.RectangleScaffold=c;a.EllipseScaffold=d;a.TextScaffold=e;return dojo.setObject("davinci.review.drawing.tools.scaffolds",a)});
//# sourceMappingURL=scaffolds.js.map