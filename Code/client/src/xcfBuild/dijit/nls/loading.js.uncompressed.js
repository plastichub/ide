define("dijit/nls/loading", [], function () {
    return {
        loadingState: "Loading...",
        errorState: "Sorry, an error occurred"
    }
});
