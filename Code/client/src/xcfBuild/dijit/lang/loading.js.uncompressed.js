define("dijit/lang/loading", [], function () {
    return {
        loadingState: "Loading...",
        errorState: "Sorry, an error occurred"
    }
});
