define("dijit/Toolbar", [
    "dojo/_base/declare", // declare
    "./_Widget",
    "./_TemplatedMixin"
], function(declare, _Widget, _TemplatedMixin){
	return declare("dijit.Toolbar", [_Widget, _TemplatedMixin], {

	});
});
