define("xdocker/tests/TestUtils", [
    'xide/types',
    'xide/utils',
    'xdocker/Docker2',
    'dcl/dcl',
    'module'
], function (types,utils,Docker,dcl,module) {

    var Module = dcl(null,{});
    /**
     *
     * @param parent
     * @param mixin
     * @returns {Docker.createDefault}
     */
    Module.createDocker = function(parent,mixin){
        return new Docker.createDefault(parent,utils.mixin({
            uuid:module.id
        },mixin));
    };


    /**
     *
     * @param docker
     * @returns {{basic: *, cond: *, variables: *, logs: *, response: *, right: *}}
     */
    Module.createDefaultPanes = function(docker){

        var defaultTabArgs = {
            icon:'fa-folder',
            closeable:false,
            movable:false,
            moveable:true,
            tabOrientation:types.DOCKER.TAB.TOP,
            location:types.DOCKER.DOCK.STACKED
        };

        // 'Basic' commands tab
        var basicCommandsTab = docker.addTab('DefaultFixed',utils.mixin(defaultTabArgs,
            {
                title: 'Basic Commands',
                h:'90%'
            }));

        // 'Conditional' commands tab
        var condCommandsTab = docker.addTab('DefaultFixed',utils.mixin(defaultTabArgs,{
            title: 'Conditional Commands',
            target:basicCommandsTab,
            select:false,
            h:'90%',
            tabOrientation:types.DOCKER.TAB.TOP
        }));



        // 'Variables' tab
        var variablesTab = docker.addTab(null,
            utils.mixin(defaultTabArgs,{
                title: 'Variables',
                target:condCommandsTab,
                select:false,
                h:100,
                tabOrientation:types.DOCKER.TAB.BOTTOM,
                location:types.DOCKER.TAB.BOTTOM
            }));


        // 'Response' tab
        var responsesTab = docker.addTab(null,
            utils.mixin(defaultTabArgs,{
                title: 'Responses',
                target:variablesTab,
                select:false,
                h:100,
                icon:null,
                tabOrientation: types.DOCKER.TAB.BOTTOM,
                location: types.DOCKER.DOCK.STACKED
            }));

        var logsTab = docker.addTab(null,
            utils.mixin(defaultTabArgs, {
                title: 'Log',
                target: responsesTab,
                select: false,
                icon:'fa-calendar',
                tabOrientation: types.DOCKER.TAB.BOTTOM,
                location: types.DOCKER.DOCK.STACKED
            }));

        var right = docker.addPanel('Collapsible', types.DOCKER.DOCK.RIGHT, condCommandsTab, {
            w: '30%',
            title:' props '
        });

        return {
            basic:basicCommandsTab,
            cond:condCommandsTab,
            variables:variablesTab,
            logs:logsTab,
            response:responsesTab,
            right:right
        }

    };

    return Module;
});