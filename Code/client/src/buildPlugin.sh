#!/bin/bash
#set -x
# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/
# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"

PLUGIN_ROOT="$BASEDIR/../../../xapp/commander/plugins/"
#PLUGIN_ROOT="$BASEDIR/../../xapp/commander/plugins/"


# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"



LAYER=$1
echo $LAYER

#build with  sh buildCustomPlugin.sh xappext/disqus xapp disqus release

PLUGIN_PATH="$PLUGIN_ROOT/$LAYER/client/xfile"




DST_DIRECTORY="$PLUGIN_PATH/buildTemp/"
DISTDIR="$PLUGIN_ROOT/build/$LAYER-release"



rm -rf "${DST_DIRECTORY}"
mkdir -p "${DST_DIRECTORY}"

#rm -rf $SRCDIR/$PLUGIN_PACKAGE_PATH/$PLUGIN_MODULE/$RT_CONFIG/

#DISTDIR_FINAL="$BASEDIR/customXApp"

#rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="run"

# Main application package loader configuration
LOADERCONF="$PLUGIN_PATH/$LOADERMID.js"

echo "use : " $LOADERCONF

# Main application package build configuration
PROFILE="$PLUGIN_PATH/plugin.profile.js"




#echo base dir : $BASEDIR and DISTDIR $DISTDIR  and srcDir  : $SRCDIR and plugin path : $PLUGIN_PATH

# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

#echo -n "Cleaning old files..."
#rm -rf "$DISTDIR"
#cp -r $SRCDIR/$PLUGIN_PACKAGE_PATH/$PLUGIN_MODULE

########################################################################################
#
#   Create Symbolic Files to main CSS directories
#
set -x
cd "$TOOLSDIR"


#if which node >/dev/null; then
#    node ../../dojo/dojo.js load=build --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
if which java >/dev/null; then
    java -Xms256m -Xmx256m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR"
else
    echo "Need node.js or Java to build!"
    exit 1
fi

##Re-adjust plugin root
PLUGIN_ROOT="$BASEDIR/../../xapp/commander/plugins/"
PLUGIN_PATH="$PLUGIN_ROOT/$LAYER/client/xfile"
BUILD_DIR="$PLUGIN_ROOT/build/$LAYER-release/$LAYER"
DISTDIR="$PLUGIN_ROOT/$LAYER/client/xfile-release"

find $BUILD_DIR -name *.uncompressed.js -exec rm '{}' ';'
find $BUILD_DIR -name *.consoleStripped.js -exec rm '{}' ';'
find $BUILD_DIR -name *.map -exec rm '{}' ';'
find $BUILD_DIR -name *.svn -exec rm -rf '{}' ';'

rm -rf $DISTDIR
cp -rf $BUILD_DIR $DISTDIR

rm -rf $DISTDIR/build

#cp -rf $DST_DIRECTORY/$PLUGIN_NAME/ $SRCDIR/$PLUGIN_PACKAGE_PATH/$PLUGIN_MODULE/$RT_CONFIG/
#exit 1
#
#   clean up css temp dirs
#
#rm $SRCDIR/xapp/cssCommon
########################################################################################

#cd "$BASEDIR"

#LOADERMID=${LOADERMID//\//\\\/}

echo "Build complete"