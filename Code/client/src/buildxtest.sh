#!/bin/bash

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"

# Destination directory for built code
DISTDIR="$BASEDIR/xtestBuild"

DISTDIR_FINAL="$BASEDIR/xtest"

rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="xtest/run-release"

# Main application package loader configuration
LOADERCONF="$SRCDIR/$LOADERMID.js"

# Main application package build configuration
PROFILE="$SRCDIR/xtest/app.profile.js"

CONFIG="$SRCDIR/xtest/dojoConfig.js"



# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

echo -n "Cleaning old files..."
rm -rf "$DISTDIR"
echo " Done"

########################################################################################
#
#   Create Symbolic Files to main CSS directories
#
rm $BASEDIR/lib/xtest/xtest.min.js

cd "$TOOLSDIR"

if which java >/dev/null; then
    java -Xms1024m -Xmx2048m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build  --dojoConfig "$CONFIG" --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
else
    echo "Need node.js or Java to build!"
    exit 1
fi
#
#
#
########################################################################################

cd "$BASEDIR"
echo "Build complete"

########################################################################################
#
#   Copy dojo i8ln files
#
cp -rf "$DISTDIR/dojo/nls/" $DISTDIR/nls

######################
#
#   Clean up temp files
#

#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xtest/dojo/xtest.js
#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xcf/dojo/xcf.js
#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xfile/dojo/xbox.js

if [ ! -f $DISTDIR/dojo/dojo.js ]; then
    echo "Cant find dojo.js ----- incomplete build !!!!"
fi

cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xtest/dojo/xtest.js

cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/lib/xtest/xtest.min.js

#cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xfile/dojo/xbox_min.js
find $DISTDIR -name *.uncompressed.js -exec rm '{}' ';'

find $DISTDIR -name *.map -exec rm '{}' ';'
find $DISTDIR -name *.svn -exec rm -rf '{}' ';'


ls -l --block-size=K xtest/dojo/
#
# POST STEPS FINISH
#
########################################################################################
