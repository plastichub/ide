#!/bin/bash


# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib/"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/"

# Destination directory for built code
DISTDIR="$BASEDIR/xfile/ext/"

OUTFILE_NAME="xfileExternals.js"

cd "$TOOLSDIR"
pwd
if which java >/dev/null; then
    java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar \
        --js $SRCDIR/external/Keypress/keypress.js \
        --js $SRCDIR/external/moment.js \
        --js $SRCDIR/external/lodash.min.js \
        --js $SRCDIR/external/jquery-1.9.1.min.js \
        --js $SRCDIR/external/easyXDM.js \
        --js $SRCDIR/external/jquery-ui-1.10.1.custom.min.js \
        --js $SRCDIR/external/stacktrace.js \
        --js $SRCDIR/external/jsoneditor/jsoneditor.js \
        --js $SRCDIR/external/cflow.js \
        --js_output_file $DISTDIR/$OUTFILE_NAME
else
    echo "Need node.js or Java to build!"
    exit 1
fi
