#!/bin/bash

##set -e

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"

# Destination directory for built code
DISTDIR="$BASEDIR/xfileBuild"

DISTDIR_FINAL="$BASEDIR/xfile"

rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="xwordpress/run-release"

# Main application package loader configuration
LOADERCONF="$SRCDIR/$LOADERMID.js"

# Main application package build configuration
PROFILE="$SRCDIR/xwordpress/app.profile.js"



# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

echo -n "Cleaning old files..."
rm -rf "$DISTDIR"
echo " Done"

set -x
rm -rf $SRCDIR/xwordpress/css
cp -rf $BASEDIR/css $SRCDIR/xwordpress/css


rm -rf $SRCDIR/xwordpress/xfileTheme
##cp -rf $BASEDIR/xfileTheme $SRCDIR/xwordpress/xfileTheme
cp -rf $SRCDIR/dijit/themes $SRCDIR/xwordpress/xfileTheme


########################################################################################
#
#   Create Symbolic Files to main CSS directories
#

cd "$TOOLSDIR"

if which java >/dev/null; then
    java -Xms1024m -Xmx2048m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
else
    echo "Need node.js or Java to build!"
    exit 1
fi

########################################################################################

cd "$BASEDIR"

echo "Build complete"


########################################################################################
#
#   Copy dojo i8ln files
#
cp -rf "$DISTDIR/dojo/nls/" $DISTDIR/nls


#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xfile/dojo/dojowp.js
find $DISTDIR -name *.uncompressed.js -exec rm '{}' ';'
find $DISTDIR -name *.consoleStripped.js -exec rm '{}' ';'
find $DISTDIR -name *.map -exec rm '{}' ';'
find $DISTDIR -name *.svn -exec rm -rf '{}' ';'



set -x

rm -rf $BASEDIR/xfile/xwordpress/images
cp -rf $BASEDIR/images $BASEDIR/xfile/xwordpress/images

rm -rf $BASEDIR/xfile/images/xas
mkdir -p $BASEDIR/xfile/images/
cp -rf $BASEDIR/images/xas $BASEDIR/xfile/images/xas
cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xfile/dojo/dojowp.js

cp -rf $DISTDIR/xwordpress/resources/app.css $BASEDIR/xfile/xwordpress/resources/app.css


#
# POST STEPS FINISH
#
########################################################################################
