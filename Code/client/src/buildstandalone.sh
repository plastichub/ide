#!/bin/bash

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"

# Destination directory for built code
DISTDIR="$BASEDIR/xfileBuild"

DISTDIR_FINAL="$BASEDIR/xfile"

rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="xbox/run-release"

# Main application package loader configuration
LOADERCONF="$SRCDIR/$LOADERMID.js"

# Main application package build configuration
PROFILE="$SRCDIR/xbox/app.profile.js"

CONFIG="$SRCDIR/xbox/dojoConfig.js"



# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

echo -n "Cleaning old files..."
rm -rf "$DISTDIR"
echo " Done"


rm -rf $SRCDIR/xbox/css
cp -rf $BASEDIR/css $SRCDIR/xbox/css
#rm -rf $SRCDIR/xbox/css/fonts/layoutIcons
rm -rf $SRCDIR/xbox/css/xcf

########################################################################################
#
#   Create Symbolic Files to main CSS directories
#

cd "$TOOLSDIR"

if which java >/dev/null; then
    java -Xms1024m -Xmx2048m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build  --dojoConfig "$CONFIG" --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
else
    echo "Need node.js or Java to build!"
    exit 1
fi
#
#
#
########################################################################################

cd "$BASEDIR"
echo "Build complete"

########################################################################################
#
#   Copy dojo i8ln files
#
cp -rf "$DISTDIR/dojo/nls/" $DISTDIR/nls

######################
#
#   Clean up temp files
#

#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xfile/dojo/xbox_d.js
#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xfile/dojo/xbox.js

cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xbox/dojo/xbox.js
#cp -rf $BASEDIR/lib/xide/layout/ContentPane.js $BASEDIR/xfile/xide/layout/ContentPane.js
#cp -rf $BASEDIR/lib/xide/layout/_TabContainer.js $BASEDIR/xfile/xide/layout/_TabContainer.js
#cp -rf $BASEDIR/lib/xide/views/_LayoutMixin.js $BASEDIR/xfile/xide/views/_LayoutMixin.js

#cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xfile/dojo/xbox_min.js
find $DISTDIR -name *.uncompressed.js -exec rm '{}' ';'
#find $DISTDIR -name *.consoleStripped.js -exec rm '{}' ';'
find $DISTDIR -name *.map -exec rm '{}' ';'
find $DISTDIR -name *.svn -exec rm -rf '{}' ';'


#rm -rf $BASEDIR/xfile/xideve
#cp -rf $DISTDIR/xideve $BASEDIR/xfile/xideve

#rm -rf $BASEDIR/xfile/xblox
#cp -rf $SRCDIR/xblox-release/xblox/xblox.js $BASEDIR/xfile/xblox/xblox.js

#cp -rf $SRCDIR/xide-release/xide/xide.js $BASEDIR/xfile/xide/xide.js


#cp -rf $SRCDIR/orion-release/orion/* $BASEDIR/xfile/orion/

#cp -rf $SRCDIR/davinci-release/davinci/* $BASEDIR/xfile/davinci/

#cp -rf $SRCDIR/system $BASEDIR/xfile/system
#cp -rf $SRCDIR/preview $BASEDIR/xfile/preview

cp -rf $DISTDIR/xbox/resources/app.css $BASEDIR/xbox/resources/app.css
ls -l --block-size=K xfile/dojo/
#
# POST STEPS FINISH
#
########################################################################################
