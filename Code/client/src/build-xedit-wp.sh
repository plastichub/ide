#!/bin/bash

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"

# Destination directory for built code
DISTDIR="$BASEDIR/xfileBuild"

DISTDIR_FINAL="$BASEDIR/xedit-wp"

rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="xedit/run-release"

# Main application package loader configuration
LOADERCONF="$SRCDIR/$LOADERMID.js"

# Main application package build configuration
PROFILE="$SRCDIR/xedit/app.profile.js"

CONFIG="$SRCDIR/xedit/dojoConfig.js"



# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

echo -n "Cleaning old files..."
rm -rf "$DISTDIR"
echo " Done"

set -x

rm -rf $SRCDIR/xedit/css
cp -rf $BASEDIR/css $SRCDIR/xedit/css

rm -rf $SRCDIR/xedit/xfileTheme
cp -rf $SRCDIR/dijit/themes $SRCDIR/xedit/xfileTheme

rm -rf $SRCDIR/xedit/css/xfileTheme/tundra/
rm -rf $SRCDIR/xedit/css/xfileTheme/nihilo/
rm -rf $SRCDIR/xedit/css/xfileTheme/soria/


########################################################################################
#
#   Create Symbolic Files to main CSS directories
#

cd "$TOOLSDIR"

if which java >/dev/null; then
    java -Xms1024m -Xmx2048m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build  --dojoConfig "$CONFIG" --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
else
    echo "Need node.js or Java to build!"
    exit 1
fi
#
#
#
########################################################################################

cd "$BASEDIR"
echo "Build complete"

########################################################################################
#
#   Copy dojo i8ln files
#
cp -rf "$DISTDIR/dojo/nls/" $DISTDIR/nls

######################
#
#   Clean up temp files
#

cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xfile/dojo/xedit.js
#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xfile/dojo/xedit.js

find $DISTDIR -name *.uncompressed.js -exec rm '{}' ';'
find $DISTDIR -name *.consoleStripped.js -exec rm '{}' ';'
find $DISTDIR -name *.map -exec rm '{}' ';'
find $DISTDIR -name *.svn -exec rm -rf '{}' ';'



cp -rf $DISTDIR/xedit/resources/app.css $BASEDIR/xfile/xedit/resources/app-xedit.css

#
# POST STEPS FINISH
#
########################################################################################
