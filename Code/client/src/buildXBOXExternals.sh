#!/bin/bash


# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib/"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/"

# Destination directory for built code
DISTDIR="$BASEDIR/ext/"

OUTFILE_NAME="xamiro-externals.js"

cd "$TOOLSDIR"
pwd
if which java >/dev/null; then
    java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar \
        --js $SRCDIR/external/Keypress/keypress.js \
        --js $SRCDIR/external/lodash.min.js \
        --js $SRCDIR/external/jquery-2.1.4.min.js \
        --js $SRCDIR/../theme/bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js \
        --js $SRCDIR/external/jquery-ui-1.10.1.custom.min.js \
        --js $SRCDIR/external/position-calculator.min.js \
        --js $SRCDIR/external/messenger_latest/build/js/messenger.js \
        --js $SRCDIR/external/bootstrap-select/dist/js/bootstrap-select.js \
        --js $SRCDIR/external/bootstrap3-dialog/dist/js/bootstrap-dialog.js \
        --js $SRCDIR/external/bootstrap-dropdown-hover/hover.js \
        --js $SRCDIR/external/jspanel3/source/jquery.jspanel-compiled.js \
        --js_output_file $DISTDIR/$OUTFILE_NAME
else
    echo "Need node.js or Java to build!"
    exit 1
fi
