#!/bin/bash


# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib/"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/"

# Destination directory for built code
DISTDIR="$BASEDIR/xfile/ext/"

OUTFILE_NAME="xfileAceExternals.js"

cd "$TOOLSDIR"
pwd
if which java >/dev/null; then
    java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar \
        --js $SRCDIR/external/ace/emmet.js \
        --js_output_file $DISTDIR/$OUTFILE_NAME
else
    echo "Need node.js or Java to build!"
    exit 1
fi
