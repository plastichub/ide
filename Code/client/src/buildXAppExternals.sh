#!/bin/bash


# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib/"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/"

# Destination directory for built code
DISTDIR="$BASEDIR/xcf/ext/"

OUTFILE_NAME="xapp-externals.js"

cd "$TOOLSDIR"
pwd
if which java >/dev/null; then
    java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar \
        --js $SRCDIR/external/cssParserFixed.js \
        --js $SRCDIR/external/Keypress/keypress.js \
        --js $SRCDIR/external/micromatch/dist/micromatch-browser.js \
        --js $SRCDIR/external/moment.js \
        --js $SRCDIR/external/sockjs-0.3.min.js \
        --js $SRCDIR/external/jquery-1.9.1.min.js \
        --js_output_file $DISTDIR/$OUTFILE_NAME
else
    #--js $SRCDIR/../xfile/ext/d3.v3.min.js \
    echo "Need node.js or Java to build!"
    exit 1
fi
