#!/bin/bash


# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib/"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/"

# Destination directory for built code
DISTDIR="$BASEDIR/xcf/ext/"

OUTFILE_NAME="$BASEDIR/lib/xapp/build/xapp.min.js"

cd "$TOOLSDIR"

if which java >/dev/null; then
    java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar \
        --language_in=ECMASCRIPT6 \
        --language_out=ECMASCRIPT5 \
        --js $SRCDIR/xapp/build/main_build.js \
        --js_output_file $OUTFILE_NAME
else
    echo "Need node.js or Java to build!"
    exit 1
fi
