#!/bin/bash
#set -x
# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/
# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"
PLUGIN_ROOT=$SRCDIR

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"


LAYER=$1
echo $LAYER

#build with  sh buildCustomPlugin.sh xappext/disqus xapp disqus release

DST_DIRECTORY=$SRCDIR/buildTemp/$LAYER
DISTDIR=$SRCDIR/$LAYER-release

rm -rf "${DST_DIRECTORY}"
mkdir -p "${DST_DIRECTORY}"

#rm -rf $SRCDIR/$PLUGIN_PACKAGE_PATH/$PLUGIN_MODULE/$RT_CONFIG/

#DISTDIR_FINAL="$BASEDIR/customXApp"

#rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="run"

# Main application package loader configuration
LOADERCONF="$SRCDIR/$LAYER/$LOADERMID.js"

# Main application package build configuration
PROFILE="$SRCDIR/$LAYER/layer.profile.js"

echo base dir : $BASEDIR and DISTDIR $DISTDIR  and srcDir  : $SRCDIR and plugin path : $PLUGIN_PATH

# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

#echo -n "Cleaning old files..."
#rm -rf "$DISTDIR"
#cp -r $SRCDIR/$PLUGIN_PACKAGE_PATH/$PLUGIN_MODULE

########################################################################################
#
#   Create Symbolic Files to main CSS directories
#
set -x
cd "$TOOLSDIR"

#if which node >/dev/null; then
#    node ../../dojo/dojo.js load=build --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
if which java >/dev/null; then
    java -Xms256m -Xmx256m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR"
else
    echo "Need node.js or Java to build!"
    exit 1
fi

echo " Done" $DST_DIRECTORY

find $DISTDIR -name *.uncompressed.js -exec rm '{}' ';'
find $DISTDIR -name *.consoleStripped.js -exec rm '{}' ';'
find $DISTDIR -name *.map -exec rm '{}' ';'
find $DISTDIR -name *.svn -exec rm -rf '{}' ';'



#cp -rf $DST_DIRECTORY/$PLUGIN_NAME/ $SRCDIR/$PLUGIN_PACKAGE_PATH/$PLUGIN_MODULE/$RT_CONFIG/
#exit 1
#
#   clean up css temp dirs
#
#rm $SRCDIR/xapp/cssCommon
########################################################################################

#cd "$BASEDIR"

#LOADERMID=${LOADERMID//\//\\\/}

echo "Build complete"