#!/bin/bash


# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib/"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/"

# Destination directory for built code
DISTDIR="$BASEDIR/xcf/ext/"

OUTFILE_NAME="xcf-externals.js"

cd "$TOOLSDIR"
pwd
if which java >/dev/null; then
    java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar \
        --language_in ECMASCRIPT6 \
        --language_out ECMASCRIPT5 \
        --js $SRCDIR/xide-ts/dist/scripts/app.js \
        --js $SRCDIR/external/Keypress/keypress.js \
        --js $SRCDIR/external/lodash.min.js \
        --js $SRCDIR/external/jquery-2.1.4.min.js \
        --js $SRCDIR/external/jquery-ui-1.10.1.custom.min.js \
        --js $SRCDIR/external/position-calculator.min.js \
        --js $SRCDIR/external/sockjs-0.3.min.js \
        --js $SRCDIR/external/stacktrace.min.js \
        --js $SRCDIR/external/vogue-client.js \
        --js $SRCDIR/external/cssParserFixed.js \
        --js $SRCDIR/external/moment.js \
        --js $SRCDIR/external/micromatch/dist/micromatch-browser.js \
        --js $SRCDIR/external/hightlight/highlight.pack.js \
        --js $SRCDIR/../xcf/ext/showdown/dist/showdown.js \
        --js $SRCDIR/../theme/bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js \
        --js $SRCDIR/external/bootstrap-select/dist/js/bootstrap-select.js \
        --js $SRCDIR/external/bootstrap3-dialog/dist/js/bootstrap-dialog.js \
        --js $SRCDIR/external/font-awesome-picker/src/js/jquery.ui.pos.js \
        --js $SRCDIR/external/font-awesome-picker/src/js/iconpicker.js \
        --js $SRCDIR/external/wysihtml/dist/wysihtml.js \
        --js $SRCDIR/external/wysihtml/dist/wysihtml-toolbar.js \
        --js $SRCDIR/external/velocity/velocity.min.js \
        --js $SRCDIR/external/velocity/velocity.ui.min.js \
        --js $SRCDIR/external/typeahead/typeahead.jquery.js \
        --js $SRCDIR/external/messenger_latest/build/js/messenger.js \
        --js $SRCDIR/external/bootstrap3-editable/js/bootstrap-editable.js \
        --js $SRCDIR/external/bootstrap-dropdown-hover/hover.js \
        --js $SRCDIR/external/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js \
        --js $SRCDIR/external/selectize/dist/js/standalone/selectize.js \
        --js $SRCDIR/external/jspanel3/source/jquery.jspanel-compiled.js \
        --js_output_file $DISTDIR/$OUTFILE_NAME
else
    #--js $SRCDIR/../xfile/ext/d3.v3.min.js \
    echo "Need node.js or Java to build!"
    exit 1
fi
