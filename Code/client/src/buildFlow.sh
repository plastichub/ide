#!/bin/bash

RDIR=/liferay/tomcat-7.0.27/webapps/XApp-portlet/

set -x
UUID=$1
APPID=$2


#echo $UUID "and" $APPID
#exit 1


#rm -rf "$RDIR/release"

#set -e

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)/

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib/"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/"

# Destination directory for built code
DISTDIR="$BASEDIR/lib/external/"


cd "$TOOLSDIR"

#if which node >/dev/null; then
#    node ../../dojo/dojo.js load=build --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
pwd
if which java >/dev/null; then
    #java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar --js ../external/qr/qrcode.js --js ../external/klass.min.js --js ../external/jshashtable.min.js --js ../external/soundmanager2.js --js ../external/cssParserFixed.js --js ../external/code.photoswipe-3.0.5.min.js --js ../external/log4javascript.js --js ../external/stacktrace.js --js ../external/bookmark_bubble.js  --js_output_file ../externals.js
    #java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar --js $SRCDIR/external/qr/qrcode.js --js $SRCDIR/external/klass.min.js --js $SRCDIR/external/jshashtable.min.js --js $SRCDIR/external/soundmanager2.js --js $SRCDIR/external/cssParserFixed.js --js $SRCDIR/external/code.photoswipe-3.0.5.js --js $SRCDIR/external/log4javascript.js --js $SRCDIR/external/stacktrace.js --js $SRCDIR/external/bookmark_bubble.js  --js_output_file $SRCDIR/external/externals.js
    java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar \
        --js $SRCDIR/external/cflow/js/FWDS3DCovCategoriesButton.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovCategoriesMenu.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovComplexButton.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovContextMenu.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovData.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovDisplayObject3D.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovDisplayObject.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovEventDispatcher.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovInfo.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovInfoWindow.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovLightBox.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovModTweenMax.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovPreloader.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovScrollbar.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovSimpleButton.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovSimpleDisplayObject.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovSlideshowButton.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovSlideShowPreloader.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovThumb.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovThumbsManager.js  \
        --js $SRCDIR/external/cflow/js/FWDS3DCovTimerManager.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovTooltip.js \
        --js $SRCDIR/external/cflow/js/FWDS3DCovUtils.js \
        --js $SRCDIR/external/cflow/js/FWDSimple3DCoverflow.js \
        --js_output_file $SRCDIR/external/cflow.js
    ##java -Xms256m -Xmx256m  -jar ./closureCompiler/compiler.jar --js $SRCDIR/external/qr/qrcode.js  --js_output_file ../externals.js
else
    echo "Need node.js or Java to build!"
    exit 1
fi
