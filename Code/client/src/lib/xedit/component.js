define([
    "dojo/_base/declare",
    "xide/model/Component"
], function (declare, Component) {

    /**
     * @class xblox.component
     */
    return declare([Component], {
        /**
         * Array of typical JS packages
         * @member {Array} packages
         */
        packages: null,
        /**
         * Array of resources. A components has typically a bunch of resources like CSS.
         * @member {Array} resources
         */
        resources: [],
        /**
         * Impl. base
         */
        getLabel: function () {
            return "xblox";
        },
        load:function(){
        }

    });
});

