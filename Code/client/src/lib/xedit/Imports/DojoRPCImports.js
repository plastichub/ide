define([
    'dojo/_base/declare',
    'dojo/_base/xhr', // dojo.xhrGet
    'dojox/rpc/Service',
    'dojox/data/ServiceStore',
    'dojo/rpc/JsonService',
    'dojox/rpc/JsonRPC'
],function(declare)
{
    return declare("xbox.DojoRPCImports", null,{});
});