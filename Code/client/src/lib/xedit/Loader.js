var xappMainTemplate = "<div id=\"main\"  " +
    "class=\"xasMain dTuned %BODY_PREFIX% \" " +
    "data-dojo-type=\"dijit\/layout\/BorderContainer\" " +
    "data-dojo-props=\'design:\"headline\", liveSplitters:false,\r\n\t\tstyle:\"border: 0px solid black;\"\'>\r\n\r\n    " +
    "<script type=\"dojo\/method\">\r\n\t\t\tthis._splitterClass = \"dojox.layout.ToggleSplitter\";\r\n    <\/script>\r\n\r\n    " +
    "<div data-dojo-type=\"dijit.layout.AccordionContainer\" data-dojo-props=\"region:\'leading\', splitter:true, minSize:20\" style=\"padding-left:0px;width: 320px;\" id=\"settingsRoot\" tabIndex=\"-1\" splitter=\"true\" toggleSplitterCollapsedSize=\"20px\" region=\"left\" toggleSplitterState=\"full\"><\/div>" +
    "<div class=\"topMainTabs\" data-dojo-type=\"dijit.layout.TabContainer\" data-dojo-props=\"region:\'center\', tabStrip:true\" id=\"topTabs\" style=\"padding-left:0px;min-width:400px;width:55%;height:600px\" splitter=\"true\">\r\n\r\n        " +
        "<div class=\"welcomePane ui-state-default\" data-dojo-type=\"dijit\/layout\/ContentPane\" data-dojo-props=\'minSize:400,splitter:true, region:\"center\",style:\"background-color: #fff; padding:8px;\"\' title=\"Welcome\"><h1 >\r\n                This is a beta release. Please drop us a message to support@xapp-studio.com.\r\n            <\/h1>\r\n\r\n            <h2>Keyboard <\/h2>\r\n            <ul>\r\n                <li>F2                  : Rename<\/li>\r\n                <li>CTRL\/CMD + Enter    : Open selection in main window<\/li>\r\n                <li>F5                  : Copy (If main window is open, the destination is set automatically)<\/li>\r\n                <li>F6                  : Move<\/li>\r\n                <li>BACKSPACE (Firefox) : Go back in history<\/li>\r\n                <li>DEL                 : Delete selection<\/li>\r\n                <li>CTRL+W (Firefox)    : Close last window<\/li>\r\n            <\/ul>\r\n\r\n            <h2>Mouse <\/h2>\r\n            <ul>\r\n                <li>Right-Click         : Open context menu<\/li>\r\n            <\/ul>\r\n\r\n            <h2>Soon<\/h2>\r\n            <ul>\r\n                <li>Lots of improvements !<\/li>\r\n                <li>Lots of editors !<\/li>\r\n                <li>Better keyboard handling for Chrome and IE<\/li>\r\n            <\/ul>\r\n\r\n        <\/div>\r\n    <\/div>\r\n\r\n    <div class=\"bottomTab\"  dojoType=\"dijit.layout.TabContainer\" attachParent=\"true\" tabPosition=\"bottom\" tabStrip=\"true\" id=\"bottomTabs\" splitter=\"true\" region=\"bottom\" toggleSplitterFullSize=\"150px\" toggleSplitterCollapsedSize=\"100px\" toggleSplitterState=\"close\">\r\n        <div id=\"bottomTabLog\" class=\"bottomTabLog\" dojoType=\"dijit.layout.ContentPane\" title=\"Log\" style=\"padding:0;background-color: transparent;\">\r\n        <\/div>" +
    "<\/div><\/div><div id=\"lightbox\" dojoType=\"dojox.image.LightboxDialog\" group=\"flickrStore\"><\/div>\r\n\r\n<div dojoType=\"xfile.widgets.Toaster\" id=\"errorToast\"\r\n     positionDirection=\"br-left\" duration=\"0\"\r\n     messageTopic=\"errorMessageTopic\"><\/div>\r\n\r\n<div dojoType=\"dojox.widget.Toaster\" id=\"statusToast\"\r\n     positionDirection=\"br-left\" duration=\"0\"\r\n     messageTopic=\"statusMessageTopic\"><\/div>"

var dojoConfig = {
    async: true,
    parseOnLoad: false,
    isDebug: 0,
    baseUrl: './components/com_xappcommander/client/lib/',
    tlmSiblingOfDojo: 0,
    useCustomLogger:false,
    packages: [
        { name: "dojo", location: "dojo" },
        { name: "dojox", location: "dojox" },
        { name: "dijit", location: "dijit" },
        { name: "cbtree",location: "cbtree" },
        { name: "xbox",location: "xbox" },
        { name: "xfile",location: "xfile" }
    ],
    has:{
        'dojo-undef-api': true,
        'dojo-firebug': false
    },
    locale:'en'
};

var isMaster = true;
var debug=true;
var device=null;
var sctx=null;
var ctx=null;
var cctx=null;
var mctx=null;
var rtConfig="debug";
var returnUrl= "";
var dataHost ="./components/com_xappcommander/client/../server/service/";
var xFileConfig={
    FILES_STORE_URL:"./components/com_xappcommander/../../index.php?option=com_xappcommander&view=cbtree",
    CODDE_MIRROR:"./components/com_xappcommander/client/lib//external/codemirror-3.20/",
    THEME_ROOT:"components/com_xappcommander/client/themes/",
    WEB_ROOT:"./components/com_xappcommander/client/",
    FILE_SERVICE:"./components/com_xappcommander/index.php?option=com_xappcommander&view=rpc",
    REPO_URL:"%SITE_URL%",
    MEDIA_PICKER:{
        showPreview:true,
        editorNode:'jform_articletext_parent',
        editorTextNode:'jform_articletext',
        editorNodeAfter:'editor-xtd-buttons',
        toolbarClass:'.ui-state-default',
        editorPreviewTarget:'topTabs',
        editorPreviewLayoutZone:'center',
        editorPreviewLayoutContainerClass:'contentPreviewPane'
    },
    ACTION_TOOLBAR_MODE:'self'
};
var xappFileStoreUrl ="./components/com_xappcommander/../../index.php?option=com_xappcommander&view=cbtree";
var xappCodeMirrorRoot="./components/com_xappcommander/client/lib//external/codemirror-3.20/";
var xappThemeRoot="components/com_xappcommander/client/themes/";
var xappRootPrefix="./components/com_xappcommander/client/";
var xappIsJoomla3 = %IS_JOOMLA_3%;

var _TaskViewerClz={

    // _dlg: Object
    //		jQuery dialog widget
    _dlg:null,

    // scriptFrame: Object
    //		easyXDM created iframe node
    scriptFrame:null,

    // maxRatio: Number
    //		Maximum size to allow the dialog to expand to, relative to viewport size
    maxRatio: 0.9,

    // sizeUnit: String
    //		the unit to be used
    sizeUnit: 'px',

    // title: String
    //		jQuery dialog title
    title:null,

    // closable: Boolean
    //		Dialog show [x] icon to close itself, and ESC key will close the dialog.
    closable: true,

    // attachTo: String
    //      jQuery selector for dialog creation. the dom node will be wrapped by the dialog
    attachTo:'#taskDiv',

    // attachTo: String
    //
    relativeLocation:'taskviewer',

    // taskViewerEntry: String
    //
    taskViewerEntry:'index.html',

    // theme path: String
    //
    themePath:'./css/theme/default/',

    // help path: String
    //
    helpPath:'provadis/resources/help/',

    // task content path: String
    //
    taskContent:'../taskcontent',

    //E_SCORM_EVENT : int/enumeration (Scorm - Event - Type)
    //
    E_SCORM_EVENT:
    {
        INIT:'onInit',
        PRE:'onPreTask',
        POST:'onPostTask'
    },
    //  ET_INTERACTION_TYPE : int/enumeration (Task Type)
    //
    ET_INTERACTION_TYPE:
    {
        MULTIPLE_CHOICE1:1,
        PLACE_HOLDER:4,
        YES_NO:5,
        ASSIGN:6,
        MATRIX:8,
        SEQUENCE:9
    },
    _size:function(){
        // summary:
        //		If necessary, shrink dialog contents so dialog fits in viewport.
        // tags:
        //		private
        // todo!

    },
    onTaskMessage:function(message){

        // summary:
        //		easyXDM message entry
        // tags:
        //		private

        console.info('onTaskViewerMessage');
        console.info(message);

        /***
         * Info : Scorm Message struct
         *
         * var message = {
                    "command":message,                      @see E_SCORM_EVENT
         "taskID":taskId,                        @see TaskId
         "chapter":taskPoolId,                   @see TaskPoolId
         "type":type,                            @see ET_INTERACTION_TYPE
         "valued":valued,                        @see valued
         "description":description || '',        @see Task-Question
         "timestamp":new Date().getTime(),       @see timeStamp
         "result":taskCorrect                    @see taskCorrect
         };
         *
         */


        var thiz=this;
        var result  = JSON.parse(message);

        //old messages, still backward compatible
        if(result && result.command && result.command==='onTaskCompleted')
        {
            thiz.onTaskResult(result.taskId,result.parameter);
        }
        if(result && result.command && result.command==='onAllTasksCompleted')
        {
            $("#taskDiv").dialog("destroy");
            $("#taskDiv").html("");
        }
        if(result && result.command && result.command==='onTaskPoolCompleted')
        {
            thiz.onTaskPoolCompleted(result.taskPoolId);
        }

    },
    onTaskResult:function(taskId,taskResult)
    {
        // summary:
        //		callback when task has been finish
        // tags:
        //		public

        var taskCount = PROVADIS.presentation.getTaskCount(poolId);
        if ( taskCount != "-1" ) {
            count += 1;
            if ( taskResult == true ) countTrue += 1;
            if ( count == taskCount ) {
                $("#taskDiv").dialog("destroy");
                PROVADIS.variableManager.setUserVariable("var_chapter_main_exercise_correct", countTrue.toString());
                if ( countTrue >= 4 ) status = "success"; else status = "fail";
                PROVADIS.presentation.open(PROVADIS.presentation.getTaskEndPage(status));
            }
        }

        console.error('task completed, id : ' + taskId + ' with result ' + taskResult);
    },
    onTaskPoolCompleted:function(taskPoolId)
    {
        // summary:
        //		callback when task has been finish
        // tags:
        //		public
        console.error('task pool completed, id : ' + taskPoolId);
    },
    buildRendering:function()
    {
        // summary:
        //		Common Widget callback to prepare rendering
        // tags:
        //		private
        $(".audios").html("");
        $("#taskDiv").html("");

        return true;

    },
    init : function (ProvadisGlobal) {

        // summary:
        //		Setup
        // tags:
        //		public
        _provadis = ProvadisGlobal;

        this.buildRendering();


    },
    _calcSize:function(ratio){

        // summary:
        //		calculates dlg size
        //      @TODO : calc quirks-mode and scroll-offset
        // tags:
        //		public
        return {
            width:$(window).width()*ratio,
            height:$(window).height()*ratio
        };
    },
    _calcPos:function(width,height){

        // summary:
        //		calculates dlg position
        //      @TODO : scroll-offset and local offset (drag)
        // tags:
        //		public
        var _viewPort = this._calcSize(1);
        return {
            left:(_viewPort.width - width)*2,
            top: (_viewPort.height- height)*2
        };
    },
    _createDialog:function(node,dlgClass,title,width,height){

        // summary:
        //		create bloody jQuery
        // tags:
        //		private
        var _pos  = this._calcPos(width,height);
        return node.dialog({
            modal:true,
            autoOpen:true,
            width:width,
            closeOnEscape:true,
            resizeable:false,
            title:title||'',
            dialogClass:dlgClass ||'alert',
            closeText: "",
            position: [_pos.left/2, _pos.top/2]
        });
    },
    show : function (params)
    {
        var _dlgSize = this._calcSize(this.maxRatio);//int vector

        if(!this._dlg){
            this._dlg = this._createDialog($(this.attachTo),'alert',this.title,_dlgSize.width,_dlgSize.height);
        }

        var xdmTarget = encodeURI('' + window.location.href);

        var taskViewerIndexFile = this.taskViewerEntry;



        //poolId = params[1];
        poolId=5;

        var useDebugVersion=false;
        var taskViewerLocation = this.relativeLocation + '/' + taskViewerIndexFile + '?themePath=' + this.themePath + '&mediaFolderPrefix=' + this.taskContent + '&taskPoolId=' + poolId + '&debug='+useDebugVersion   + '&xdmTarget='+encodeURIComponent('' + window.location.href);

        countTrue = 0;
        count = 0;
        //console.dir(params);
        //console.error('opening task viewer at ' + taskViewerLocation);

        var thiz=this;//self

        this.scriptFrame = new easyXDM.Socket({
            remote:taskViewerLocation,
            container:this._dlg[0],
            props:{
                style:{
                },
                width:_dlgSize.width+this.sizeUnit,
                height:_dlgSize.height+this.sizeUnit
            },
            onLoad:function () {
            },
            onReady:function () {
            },
            onMessage:function (message, origin)
            {
                thiz.onTaskMessage(message);
            }
        });

        $(document).keyup(function (e) {
            if (e.keyCode == 27) {
                $('ui-dialog').close();
            }
        });
    }
};

function loadXAppCommander(dstId,el) {

    //var baseHref = document.getElementsByTagName('base')[0].href
    //console.error('loading xapp commander : ' + dstId + ' at ');

/*
    var dstContainer = dojo.byId(dstId,el);

    */
    /*
    var myElement = $('sbox-overlay');
    if(myElement){
        myElement.destroy();
    }
    */

    //debugger;
    //var _jQuery =jQuery;

    jQuery.noConflict();

    if(typeof dojo==='undefined'){
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = false;
        po.src = xFileConfig.WEB_ROOT + 'lib/dojo/dojo.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    }

    //setTimeout(function(){
        var po2 = document.createElement('script');
        po2.type = 'text/javascript';
        po2.async = false;
        po2.src = xFileConfig.WEB_ROOT + 'lib/xbox/run.js';
        var s2 = document.getElementsByTagName('script')[0];
        s2.parentNode.insertBefore(po2, s2);
    //},1550);




    /*
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "http://somedomain.com/somescript";
    $("head").append(s);
    */


    /*
    if(dstContainer){
        //el.adopt(el.modalTitle,el.contents,el.closeControls);
        el.adopt(
            new Element('div',{
                html:'bla'
            })
        );

    }
    */


}