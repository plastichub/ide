define([
    'dojo/_base/declare',
/***
 *  xide
 */
    'xide/manager/Context',
    'xide/manager/Application',
    'xide/manager/WorkflowManager',
    'xide/manager/SettingsManager',
    'xide/manager/ManagerBase',

/***
 *  xbox
 */

    'xfile/manager/Context',
    'xfile/manager/FileManager',
    'xfile/manager/PanelManagerBase',
    'xfile/manager/PanelManager',
    'xfile/manager/NotificationManager',
    'xide/manager/PluginManager',
    'xfile/manager/Application'

],function(declare)
{
    return declare("xbox.manager", null,{});
});