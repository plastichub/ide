define(['dojo/has', 'require', 'dojo/_base/declare'], function (has, require, declare) {

    has.add("debug", function (global, document, anElement) {
        return location.href.indexOf('debug') != -1;
    });

    has.add("mobile", function (global, document, anElement) {
        return (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase()));
    });

    window.require = require;
    //dijit and dojox might be pre-compiled, wait its fully loaded and go on
    require(['dijit/dijit', 'dojox/dojox'], function (_dijit, _dojox) {

        if (has('host-browser')) {
        }
        require([
            'dojo/_base/declare',
            'xedit/XBoxCommons',
            'dojo/_base/lang',
            'dojo/dom-class',
            'dojo/_base/html',
            'dojo/dom-construct',
            'xide/views/ACEEditor',
            'xide/widgets/ActionToolbar',
            'dojo/_base/window',
            'dojo/domReady!'], function (declare,
                                         XBoxCommons,
                                         lang,
                                         domClass,
                                         html,
                                         domConstruct,
                                         ACEEditor,
                                         ActionToolbar,
                                         win) {


            if(!window['$']){
                window['$'] = jQuery;
            }


            var newcontent = jQuery('#newcontent');
            newcontent.parent().attr('id', 'editor');
            var filename = document.template.file.value;
            var ctx ={
                getResourceManager:function(){
                    return {
                        getVariable:function(variable){
                            return resourceVariables[variable];
                        }
                    }
                }
            };

            var textarea = document.getElementById('newcontent');
            textarea.style.display = 'none';

            ACEEditor.prototype.ctx=ctx;

            var container = domConstruct.create('div',{
                style:''
            });

            var actionToolbar = new ActionToolbar({

            });
            actionToolbar.startup();


            var target = newcontent.parent()[0];
            target.appendChild(container);

            container.appendChild(actionToolbar.domNode);

            var aceContainer = domConstruct.create('div',{
                style:'',
                id:'wp-ace-editor'
            });

            container.appendChild(aceContainer);


            var aceEditor = new ACEEditor({
                value:textarea.value,
                style:'',
                hasSave:false,
                hasReload:false,
                fileName:filename,
                onMaximized:function(maximed){
                    if(!maximed) {
                        var actions = aceEditor.getItemActions();
                        actionToolbar.setItemActions({}, actions, {});
                        $('#wp-ace-editor').css('height','500px');
                        jQuery(window).trigger('resize');
                    }else{
                        $('#wp-ace-editor').css('height','inherit');
                    }
                },
                onReady:function(){

                }
            },aceContainer);

            aceEditor.startup();

            var actions = aceEditor.getItemActions();
            actionToolbar.setItemActions({},actions,{});


            window['AceEditor']=aceEditor;


            var editor = aceEditor.getAce();

            window['AceInstance']=editor;

            //editor.resize();
            jQuery(window).resize(function() {
                aceEditor.resize();
            });

            editor.setAnimatedScroll(true);

            editor.setOption('spellcheck', true);


            //editor.setAutoScrollEditorIntoView();

            var modelist = ace.require('ace/ext/modelist');
            var mode = modelist.getModeForPath(filename).mode;
            console.log('start in mode ' + mode);
            mode = mode.replace('ace/mode/','');
            aceEditor.setMode(mode);

            jQuery('#template').submit(function () {
                textarea.value = aceEditor.getValue();
                return true;
            });

            editor.resize();
            aceEditor.resize();
            setTimeout(function(){
                jQuery(window).trigger('resize');
               aceEditor.resize();
            },1000);

        });
    });
});
