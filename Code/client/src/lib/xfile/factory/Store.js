define([
    'xide/types',
    'xide/factory',
    'xide/utils',
    'xfile/data/Store'
], function (types, factory,utils,Store){
    /**
     *
     * @param mount
     * @param options
     * @param config
     * @param optionsMixin
     * @param ctx
     * @param args
     * @returns {*}
     */
    factory.createFileStore = function (mount,options,config,optionsMixin,ctx,args){
        var storeClass = Store;
        options = options || {
            fields:
            types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            //types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME |
            types.FIELDS.SHOW_MEDIA_INFO
        };

        utils.mixin(options,optionsMixin);
        var store = new storeClass(utils.mixin({
            data:[],
            ctx:ctx,
            config:config,
            url:config.FILE_SERVICE,
            serviceUrl:config.serviceUrl,
            serviceClass:config.FILES_STORE_SERVICE_CLASS,
            mount:mount,
            options:options
        },args));
        store._state = {
            initiated:false,
            filter:null,
            filterDef:null
        };
        store.reset();
        store.setData([]);
        store.init();
        ctx && ctx.getFileManager().addStore(store);
        return store;
    };
    return factory;

});