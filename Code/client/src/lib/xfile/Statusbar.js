/** @module xfile/Statusbar **/
define([
    "xdojo/declare",
    'xide/types',
    'xfile/Breadcrumb'
], function (declare, types, Breadcrumb) {

    //package via declare
    var _class = declare('xgrid.Statusbar', null, {
        statusbar: null,
        setState: function (state) {
            this.showStatusbar(state.statusBar);
            return this.inherited(arguments);
        },
        getState: function (state) {
            state = this.inherited(arguments) || {};
            state.statusBar = this.statusbar != null;
            return state;
        },
        runAction: function (action) {
            if (action.command == types.ACTION.STATUSBAR) {
                this.showStatusbar(this.statusbar ? false : true);
            }
            return this.inherited(arguments);
        },
        showStatusbar: function (show,priorityChange) {

            if (!show && this.statusbar) {
                this._destroyStatusbar();
            } else if (show) {
                this.getStatusbar();
            }

            this.resize();

            if(this._statusbarAction && priorityChange!==true){
                this._statusbarAction.set('value',show);
            }

        },
        _destroyStatusbar: function () {

            this.statusbarRoot && this.statusbarRoot.remove();
            this.statusbar = null;
            this.statusbarRoot = null;
        },
        _statusbarAction:null,
        buildRendering: function () {

            this.inherited(arguments);

            var self = this,
                node = this.domNode;

            this._on('onAddActions', function (evt) {

                var actions = evt.actions,
                    permissions = evt.permissions,
                    action = types.ACTION.STATUSBAR;

                if (!evt.store.getSync(action)) {

                    var _action = self.createAction({
                        label: 'Statusbar',
                        command: action,
                        icon: types.ACTION_ICON.STATUSBAR,
                        tab: 'View',
                        group: 'Show',
                        mixin:{
                            actionType:'multiToggle'
                        },
                        onCreate:function(action){
                            action.set('value',self.statusbar!==null);
                            self._statusbarAction = action;
                        },
                        onChange:function(property,value,action){
                            self.showStatusbar(value,true);

                        }
                    });

                    actions.push(_action);


                    /*
                    var _action = grid.createAction('Statusbar', action,
                        types.ACTION_ICON.STATUSBAR, null, 'View', 'Show', 'item|view', null,
                        null, null, null, null, permissions, node, grid);
                    if (!_action) {

                        return;
                    }
                    */
                    //actions.push(_action);
                }
            });
        },
        getStatusbar: function () {

            if (this.statusbar) {
                return this.statusbar;
            } else {
                return this.createStatusbar();
            }
        },
        onRenderedStatusBar: function (statusbar, root, text) {

            var bc = this.__bc;
            if (!bc && root && root.append) {
                bc = new Breadcrumb({}, $('<div>'));
                root.append(bc.domNode);

                $(bc.domNode).css({
                    "float": "right",
                    "padding": 0,
                    "margin-right": 10,
                    "top": 0,
                    "right": 50,
                    /*"right":0,*/
                    "position": "absolute"
                });

                this.__bc = bc;
                //bc.startup();
                bc.setSource(this);
            }
            if (bc) {

                bc.clear();


                var store = this.collection,
                    cwdItem = this.getCurrentFolder(),
                    cwd = cwdItem ? cwdItem.path : '';

                bc.setPath('.', store.getRootItem(), cwd, store);
            }

        },
        onStatusbarCollapse:function(e){

        },
        createStatusbar: function (where) {

            where = where = this.footer;
            var statusbar = this.statusbar,
                self = this;

            if (!statusbar) {


                var root = $('<div class="statusbar widget" style="width:inherit;padding: 0;margin:0;padding-left: 4px;"></div>')[0];

                where.appendChild(root);



                statusbar = $('<div class="status-bar-text ellipsis" style="display: inline-block;">0 items selected</div>')[0];

                root.appendChild(statusbar);

                var $collapser = $('<div class="status-bar-collapser fa-caret-up" style="" ></div>');
                $collapser.click(function(e){
                    self.onStatusbarCollapse($collapser);
                });
                var collapser  = $collapser[0];
                root.appendChild(collapser);
                this.statusbar = statusbar;
                this.statusbarCollapse = collapser;
                this.statusbarRoot = root;

                this._emit('createStatusbar', {
                    root: root,
                    statusbar: statusbar,
                    collapser: collapser
                })

            }

            return statusbar;

        },
        startup: function () {

            this.inherited(arguments);


            function bytesToSize(bytes) {
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Byte';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            }


            function calculateSize(items) {


                var bytes = 0;
                var sizeStr = '0 KB';
                _.each(items, function (item) {

                    var _bytes = item.sizeBytes || 0;
                    bytes += _bytes;
                });

                if (bytes > 0) {
                    sizeStr = bytesToSize(bytes);
                }

                return {
                    bytes: bytes,
                    sizeStr: sizeStr
                }


            }

            if (!this.hasPermission(types.ACTION.STATUSBAR)) {
                return;
            }
            var statusbar = this.getStatusbar(),
                self = this;

            if (statusbar) {

                function selectionChanged(evt) {
                    var selection = evt.selection || [],
                        nbItems = selection.length,
                        size = calculateSize(selection),
                        text = nbItems + '  ' +self.localize('selected') + ' (' + size.sizeStr + ')';

                    statusbar.innerHTML = text;

                    if (self.onRenderedStatusBar) {
                        self.onRenderedStatusBar(statusbar, self.statusbarRoot, text);
                    }

                }

                this._on('selectionChanged', selectionChanged);
            }
        }
    });

    return _class;
});