/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/on',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/ListRenderer',
    'xgrid/ThumbRenderer',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xide/data/Model',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xfile/data/Store',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xfile/views/FileGrid',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    'xgrid/Keyboard',
    'xfile/tests/ThumbRenderer',
    'dojo/dom-construct',
    'xgrid/Renderer',
    'xfile/model/File',
    'xide/widgets/TemplatedWidgetBase',
    'dijit/registry',
    'xide/tests/TestUtils',
    'module',
    "xfile/tests/TestUtils",
    'xide/widgets/_Search'
], function (declare, on, types,
    utils, factory, ListRenderer, ThumbRenderer, TreeRenderer,
    Trackable, ObservableStore, Model, _ActionMixin,
    Grid, Store, MultiRenderer, RadioButton, FileGrid, Ribbon, Registry, DefaultActions, Action, Keyboard,
    ThumbRenderer2, domConstruct, Renderer, File, TemplatedWidgetBase, registry, TestUtils, module, FTestUtils, _Search) {


    //console.clear();
    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;



    function doTests(tab) {
        var grid = FTestUtils.createFileGrid('workspace_user',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {
                startup: function () {
                    console.log('ss');
                    return this.inherited(arguments);
                }

            }, 'TestGrid', module.id, true, tab);
        grid.showToolbar(true);
    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {

        var parent = TestUtils.createTab(null, null, module.id);

        doTests(parent);

        return declare('a', null, {});

    }

    return Grid;


});