/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",
    "dojo/dom-construct",
    "dojo/_base/lang",
    "dojo/string",
    "xide/_base/_Widget",
    "xide/views/_Dialog",
    "xfile/views/FileOperationDialog",
    "dojo/Deferred",
    'xide/views/CIView',
    'xide/views/_CIDialog',
    'xfile/views/FilePicker',
    'xfile/Breadcrumb'
], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,_Dialog,FileOperationDialog,Deferred,CIView,_CIDialog,FilePicker,Breadcrumb
) {


    console.clear();





    console.log('--do-tests');
    BootstrapDialog.closeAll();

    function openFilePicker(dlg,fileWidget){


        var inValue = fileWidget.userData.value || './AA/xfile_last/data/su.jpg';

        console.log('open file picker with ' + inValue);

        var dfd = new Deferred(),
            self = this;


        var dlg = new _Dialog({
            size: types.DIALOG_SIZE.SIZE_NORMAL,
            bodyCSS: {
                'height':'auto',
                'min-height':'500px',
                'padding':'8px',
                'margin-right':'16px'
            },
            
            picker:null,
            onOk:function(){


                var selected =this.picker._selection;
                if(selected && selected[0]){
                    fileWidget.set('value',selected[0].path);
                    console.error('as',this.picker._selection);
                    dfd.resolve(selected[0].path);
                    dlg.value=selected[0].path;

                }

            },
            message:function(dlg){

                var thiz = dlg.owner;


                var picker = new FilePicker({
                    ctx: self.ctx,
                    selection:inValue|| './AA/xfile_last/data/su.jpg',
                    resizeToParent:true,
                    storeOptionsMixin : {
                        "includeList": "*,.*",
                        "excludeList": ""
                    },
                    owner:thiz,
                    Module:self.Module,
                    defaultGridArgs: {
                        registerEditors: false,
                        _columns: {
                            "Name": true,
                            "Path": false,
                            "Size": false,
                            "Modified": false
                        },
                        permissions: utils.clone(self.Module.DEFAULT_PERMISSIONS)
                    }
                },$('<div/>'));
                thiz.picker = picker;
                thiz.add(picker,null,false);
                return $(picker.domNode);

            },
            onShow:function(dlg){

                var picker = this.picker,
                    self = this;

                picker.startup();

                this.startDfd.resolve();

            }
        });



        dlg.show();



        return dfd;

    }

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    function createCIS(){

        var CIS = {
            inputs:[
                utils.createCI('Name',13,'asd',{
                    widget:{
                        instant:true,
                        validator:function(value){
                            return value == 'asd';
                            return false;
                        }
                    }
                })
            ]
        }
        return CIS;
    }

    function createDialogClass(title,type){

        return dcl(_CIDialog,{

        });
    }
    function runAction(_action){

        var action  = this.getAction(_action);
        switch (action.command){
            case ACTION_TYPE.MOVE:{
                return move.apply(this,[]);
            }

        }
    }

    function defaultCopyOptions(){


            /***
             * gather options
             */
            var result = {
                    include:[],
                    exclude:[],
                    mode:1501
                },
                flags = 4;







        /*
            if(this.maskPane && this.maskPane.treeView){
                result.includes = this.maskPane.treeView.getSelected();
                result.excludes = this.maskPane.treeView.getUnselected();
            }
        */

            switch(flags){

                case 1<<2:{
                    result.mode=1502;//all
                    break;
                }
                case 1<<4:{
                    result.mode=1501;//none
                    break;
                }
                case 1<<8:{
                    result.mode=1504;//newer
                    break;
                }
                case 1<<16:{
                    result.mode=1503;//size
                    break;
                }
            }

        return result;
    }

    function move(){

        var dlgClass = createDialogClass('test');

        var dfd = new Deferred();
        var self = this,
            currentItem = this.getSelectedItem(),
            selection = this.getSelection(),
            currentFolder = this.getCurrentFolder(),
            startValue = currentItem ? utils.pathinfo(currentItem.path,types.PATH_PARTS.ALL).filename : '',
            collection = this.collection,
            defaultDfdArgs = {
            	select:selection,
            	focus:true,
            	append:false
            };

        if(!currentItem.directory){
            var _parent = currentItem.getParent();
            if(_parent) {
                currentItem = _parent;
                startValue = currentItem ? currentItem.path : './';
            }
        }


        console.log('Copy to : start-value : ' + startValue + ' current folder : ' + currentFolder.path + ' | selected item : ' + currentItem.path);
        self.localize('searchTarget');

        var fileWidgetValue = currentItem.path;

        //fileWidgetValue = './AA/xblox-1.xblox';

        var CIS = {
            inputs:[
                utils.createCI('Name',types.ECIType.FILE, fileWidgetValue,{
                    widget:{
                        instant:true,
                        title:self.localize('searchTarget'),
                        validator:function(value){
                            return collection.getSync(currentFolder.path + '/' + value) == null &&
                                value.length >0;
                        }
                    }
                })
            ]
        }



        var dlg = new dlgClass({
            cis:CIS,
            title:self.localize('Move'),
            ctx:this.ctx,
            size: types.DIALOG_SIZE.SIZE_NORMAL,
            onCancel:function(){
                dfd.resolve(defaultDfdArgs);
            },
            onOk:function(){

                var val= this.getField('name');

                if(val==null){
                    dfd.resolve(defaultDfdArgs);
                    return;
                }





                var serverArgs = self._buildServerSelection(selection);
                var dstPathItem  = self.collection.getSync(val);
                var dstPath = dstPathItem ? utils.normalizePath(dstPathItem.mount + '/' + dstPathItem.path) : '';

                if(dstPathItem) {
                    serverArgs.dstPath = dstPath;
                }else{
                    serverArgs.dstPath = utils.normalizePath(currentItem.mount + '/' + val);
                }
                console.error('dialog : '+val + ' to : ' +serverArgs.dstPath,selection);
//thiz.ctx.getFileManager().moveItem(selection, dlg.getValue(), options.includes, options.excludes, options.mode, cb);
                var options = defaultCopyOptions();
                var currentFolder = self.getCurrentFolder(),
                    newFolder = val,
                    fileDfd = self.ctx.getFileManager().moveItem(serverArgs.selection,serverArgs.dstPath,options.includes, options.excludes, options.mode,{
                        checkErrors:true,
                        returnProm:false
                    });


                fileDfd.then(function(data){
                    self.runAction(ACTION.RELOAD).then(function(){
                    	  //defaultDfdArgs.select
                        dfd.resolve(defaultDfdArgs);
                        /*
                        var _newItem = collection.getSync(newFolder);
                        if(_newItem){
                            self.deselectAll();
                            self.select(newFolder,null,true,{
                                append:false,
                                focus:true
                            })
                        }
                        */
                    });

                },function(e){
                    logError(e,'__error creating file!');
                    dfd.resolve(defaultDfdArgs);
                })
            }
        });
        dlg._on('widget',function(e){
            e.widget._onSelect=function(w){
                return openFilePicker.apply(self,[dlg,w]);
            }
        });
        dlg.show();
        return dfd;

    }

    function doTests(tab,grid){


/*

        grid.onRenderedStatusBar=function(statusbar,root,text){

            var bc = this.__bc;
            if(!bc){
                bc = new Breadcrumb({},$('<div>'));
                root.append(bc.domNode);

                $(bc.domNode).css({
                    "float":"right",
                    "padding":0,
                    "margin-right":10,
                    "top":0,
                    "right":50,

                    "position":"absolute"
                });

                this.__bc = bc;
                //bc.startup();
                bc.setSource(this);
            }





            bc.clear();


            var store = this.collection,
                cwdItem = this.getCurrentFolder(),
                cwd = cwdItem ? cwdItem.getPath() : '';

            bc.setPath('.',store.getRootItem(),cwd,store);

        }
        */

        grid.refresh().then(function(){
            grid.select([0]).then(function(sel){
                //var bc = new Breadcrumb({},$('<div/>'));
                grid.runAction(ACTION_TYPE.EDIT).then(function(){
                    grid.deselectAll();
                    grid.select([1],null,true,{
                        append:false,
                        delay:0
                    }).then(function() {
                        return grid.runAction(ACTION_TYPE.MOVE);
                    });
                });

            });

        })

    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {

        var parent = TestUtils.createTab(null,null,module.id);

        var grid = FTestUtils.createFileGrid('root',
            {


            },

            //overrides
            {
                reload:function(item){





                    var dfd = new Deferred();

                    item = item || this.getRows()[0];

                    //item could be a non-store item:
                    var cwd = item.getParent ? item.getParent() : this.getCurrentFolder(),
                        self = this;

                    this.collection.loadItem(cwd,true).then(function(what){

                        self.refresh().then(function(){
                            //self.openItem(cwd,true);
                            dfd.resolve(what);
                        });
                    });

                    return dfd;
                },
                runAction:function(action) {
                    //return runAction.apply(this,[action]);
                    var res = this.inherited(arguments);
                    var _resInner = runAction.apply(this,[action]);
                    return _resInner || res;
                }

            },'TestGrid',module.id,true,parent);

        doTests(parent,grid);

        return declare('a',null,{});

    }

    return Grid;

});