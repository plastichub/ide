/** @module xgrid/ThumbRenderer **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'dojo/dom-construct',
    'xgrid/ThumbRenderer'
], function (declare,types,utils,domConstruct,ThumbRenderer) {



    /**
     * The list renderer does nothing since the xgrid/Base is already inherited from
     * dgrid/OnDemandList and its rendering as list already.
     *
     * @class module:xgrid/ThumbRenderer
     * @extends module:xgrid/Renderer
     */
    //package via declare
    var _class = declare('xfile.ThumbRendererMetro',[ThumbRenderer],{
        thumbSize: "100",
        resizeThumb: true,
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {


            var thiz = this,
                div = domConstruct.create('div', {
                    className: "tile widget"
                }),
                icon = obj.icon,
                no_access = obj.read === false && obj.write === false,
                isBack = obj.name == '..',
                directory = obj && !!obj.directory,
                imageClass = '',
                useCSS = false,
                label = '';
            imageClass = 'fa fa-folder fa-5x';
            isImage = false;

            var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;opacity: 0.7';
            var contentClass = 'icon';


            if (directory) {

                if (isBack) {
                    imageClass = 'fa fa-level-up fa-5x itemFolder';
                    useCSS = true;
                } else if (!no_access) {
                    imageClass = 'fa fa-folder fa-5x itemFolder';
                    useCSS = true;
                } else {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                }

            } else {

                imageClass = 'itemIcon';

                if (no_access) {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                } else {

                    if (utils.isImage(obj.path)) {

                        var url = this.getImageUrl(obj);
                        if (url) {
                            obj.icon = url;
                        } else {
                            obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                        }

                        imageClass = 'imageFile';

                    } else {
                        imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                        useCSS = true;
                    }
                }

            }

            label = obj.name;

            var folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';

            if (utils.isImage(obj.path)) {

                var url = this.getImageUrl(obj);
                if (url) {
                    obj.icon = url;
                } else {
                    obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                }

                imageClass = '';
                contentClass = 'image';
                //folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';
                folderContent = '<div style="padding:1px" class="tile-content image">' +
                    '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                    '</div>';

                useCSS = true;
                isImage = true;


            }


            //var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);left:40px;text-align:left;font-size: 72px;margin-top:-45px;opacity: 0.7';


            var html = '<div class="tile-content ' + contentClass +'">'+
                folderContent +
                '</div>'+

                '<div class="brand bg-dark opacity">'+
                '<span class="text bg-dark opacity fg-white" style="font-size: 90%;">'+
                label +
                '</span>'+
                '</div>';

            if(isImage){
                $(div).addClass('double');
            }

            if (useCSS) {
                //div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name" style="">' + label + '</div>';
                div.innerHTML = html;
                return div;
            }





            if (directory) {

                //div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name">' + obj.name + '</div>';


                div.innerHTML = html;


            } else {
                div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
            }
            return div;

        },

        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRowN: function (obj) {




            var thiz = this,
                div = domConstruct.create('span', {
                    className: "cell"
                }),
                icon = obj.icon,
                no_access = obj.read === false && obj.write === false,
                isBack = obj.name == '..',
                directory = obj && !!obj.directory,
                imageClass = '',
                useCSS = false,
                label = '';


            if (directory) {

                if (isBack) {
                    imageClass = 'fa fa-level-up fa-5x itemFolder';
                    useCSS = true;
                } else if (!no_access) {
                    imageClass = 'fa fa-folder fa-5x itemFolder';
                    useCSS = true;
                } else {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                }

            } else {

                imageClass = 'itemIcon';

                if (no_access) {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                } else {

                    if (utils.isImage(obj.path)) {

                        var url = this.getImageUrl(obj);
                        if (url) {
                            obj.icon = url;
                        } else {
                            obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                        }
                        imageClass = 'imageFile';

                    } else {
                        imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                        useCSS = true;
                    }
                }

            }

            label = obj.showPath === true ? obj.path : obj.name;
            if (useCSS) {
                div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name" style="">' + label + '</div>';
                return div;
            }

            if (directory) {

                div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name">' + obj.name + '</div>';

            } else {
                div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
            }
            return div;

        },
        renderRow2: function (obj) {




            var thiz = this,
                div = domConstruct.create('span', {
                    className: "cell"
                }),
                icon = obj.icon,
                no_access = obj.read === false && obj.write === false,
                isBack = obj.name == '..',
                directory = obj && !!obj.directory,
                imageClass = '',
                useCSS = false,
                label = '';


            if (directory) {

                if (isBack) {
                    imageClass = 'fa fa-level-up fa-5x itemFolder';
                    useCSS = true;
                } else if (!no_access) {
                    imageClass = 'fa fa-folder fa-5x itemFolder';
                    useCSS = true;
                } else {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                }

            } else {

                imageClass = 'itemIcon';

                if (no_access) {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                } else {

                    if (utils.isImage(obj.path)) {

                        var url = this.getImageUrl(obj);
                        if (url) {
                            obj.icon = url;
                        } else {
                            obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                        }
                        imageClass = 'imageFile';

                    } else {
                        imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                        useCSS = true;
                    }
                }

            }

            label = obj.showPath === true ? obj.path : obj.name;
            if (useCSS) {
                div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name" style="">' + label + '</div>';
                return div;
            }

            if (directory) {
                div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name">' + obj.name + '</div>';
            } else {
                div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
            }
            return div;



            if (obj.render) {
                return obj.render(obj, this.inherited);
            }
            return domConstruct.create('span', {
                className: "fileGridCell",
                innerHTML: '<span class=\"' + 'fa-cube fa-5x' + '\""></span> <div class="name">' + obj.name + '</div>',
                style: 'color:blue;max-width:200px;float:left;margin:18px;padding:18px;'
            });
        },
        getImageUrl: function (item) {

            var fileManager = this.ctx.getFileManager();
            if (fileManager && fileManager) {
                var params = null;
                if (this.resizeThumb) {
                    params = {
                        width: this.thumbSize
                    }
                }
                return fileManager.getImageUrl(item, null, params);
            }
            return null;
        },
        startup:function(){

            if(this._started){
                return;
            }
            this.inherited(arguments);

            $(this.domNode.parentNode).addClass('metro');
            $(this.domNode.parentNode).addClass('container');




            $(this.domNode).css('padding','8px');
            $(this.domNode).css('padding-top','16px');

        }
    });

    return _class;
});