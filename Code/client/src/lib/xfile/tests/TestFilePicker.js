/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xfile/tests/TestUtils',
    'module',
    'xide/widgets/ActionToolbar',
    'xaction/ActionContext',
    'dijit/Toolbar',
    'dijit/ToolbarSeparator',
    'xide/widgets/ActionToolbarMixin',
    "xide/mixins/ActionMixin",
    "xide/mixins/EventedMixin",
    "xide/widgets/_MenuMixin",
    'xide/widgets/ActionToolbarButton',
    'dijit/Menu',
    'dijit/MenuItem',
    'xide/data/Reference',
    'dijit/form/DropDownButton',
    'xaction/DefaultActions',
    'xide/editor/Registry',

    'dojo/aspect',
    'xide/tests/TestUtils',
    'xide/_base/_Widget',
    'xdocker/Docker2',
    'xfile/ThumbRenderer',
    'dojo/promise/all',
    'xfile/Breadcrumb',
    'xide/model/Path',
    'xfile/views/FileGrid',
    'xfile/views/FilePicker'


], function (declare, dcl, types, utils, factory,
             XFileTestUtils, module, ActionToolbar, ActionContext,
             Toolbar, ToolbarSeparator, ActionToolbarMixin, ActionMixin, EventedMixin, _MenuMixin,
             ActionToolbarButton,
             Menu, MenuItem, Reference,
             DropDownButton,
             DefaultActions,
             Registry,
             aspect, TestUtils,
             _Widget, Docker2, ThumbRenderer, all, Breadcrumb, Path, FileGrid,FilePicker) {

    function createPickerClass() {





        var Module = dcl(FilePicker,{

            Module:FileGrid,
            onError:function(err,where){
                console.error('File Picker Error: '+ where + ' : ' + err,err);
            },
            startGrids:function(left,right){




                var self = this,
                    srcStore = left.collection,
                    targetStore = right.collection,
                    _selectArgs = {
                        focus: true,
                        append: false
                    },
                    both = [left.refresh(), right.refresh()];

                var selected = this.selection || './AA/xfile_last/Selection_002.png';

                var pathInfo = utils.pathinfo(selected, types.PATH_PARTS.ALL);

                var path = pathInfo.dirname;

                left.showStatusbar(false);


                left._on('changeSource',function(mountdata){
                    right.changeSource(mountdata,true);
                });

                left._on('changedSource',function(mountdata){
                    left.select([0],null,true,_selectArgs);
                });

                right._on('changeSource',function(mountdata){
                    left.changeSource(mountdata,true);
                });

                right._on('changedSource',function(mountdata){
                    left.select([0],null,true,_selectArgs);
                });

                all(both).then(function () {


                    //left.set('loading',true);

                    //left.set('loading',false);
                    self.onLeftGridReady(left);

                    self.onRightGridReady(right);

                    //load the selected item in the store


                    left.resize();
                    right.resize();


                    all([srcStore.getItem(path, true), targetStore.getItem(path, true)]).then(function () {


                        //when loaded, open that folder!
                        all(_.invoke([left, right], 'openFolder', path)).then(function () {

                            //when opened, select the selected item
                            all(_.invoke([left, right], 'select', [selected], null, true, _selectArgs)).then(function () {

                                //when selected, complete grids
                                self.connectGrids(left, right);
                                self.setupBreadcrumb(left, right,selected);




                            },function(err){
                                self.onError(err,'selecting items');
                            });

                        },function(err){
                            self.onError(err,'open folder');
                        });
                    },function(err){

                        self.onError(err,'loading items');
                    });
                },function(error){
                    self.onError(err,'refresh grid');
                });
            }
        });

        return Module;
    }





    /***
     *
     * playground
     */
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;

    function testMain(panel) {

        var ACTION_TYPE = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            ribbon,
            mainView = ctx.mainView;

        var LayoutClass = createPickerClass();

        var layoutMain = utils.addWidget(LayoutClass, {
            ctx: ctx,
            selection:'./default.dhtml'
        }, null, panel, true);

        panel.add(layoutMain, null, false);

        panel.resize();


        return;

        ctx.getWindowManager().registerView(grid, true);

        grid.refresh().then(function () {

        });



    }

    if (ctx && ctx.mainView) {

        var doTest = true;
        if (doTest) {

            var parent = TestUtils.createTab(null, null, module.id);
            testMain(parent);
        }
    }

    var _module = declare('maeh', null, {});


    return module;

});
