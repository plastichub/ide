/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    "xide/views/_Dialog",
    "xfile/views/FileOperationDialog",
    "xide/Keyboard"
], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,_Dialog,FileOperationDialog,Keyboard
) {

    console.clear();
    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    function createDialogClass(title,type){

        return dcl(_Dialog, {
            title:title,
            type: type || types.DIALOG_TYPE.INFO,
            size: types.DIALOG_SIZE.SIZE_SMALL,
            bodyCSS: {},
            failedText:' Failed!',
            successText:': Success!',
            showSpinner:true,
            spinner:'  <span class="fa-spinner fa-spin"/>',
            notificationMessage:null,
            doOk:function(dfd){

                this.onBeforeOk && this.onBeforeOk();

                var msg  = this.showMessage(),
                    thiz = this;

                dfd.then(function (result) {
                    thiz._onSuccess(result);
                }, function (err) {
                    thiz._onError();
                });

            },
            _onSuccess:function(title,suffix,message){

                title = title  || this.title;
                message = message || this.notificationMessage;

                message && message.update({
                        message: title + this.successText + (suffix ?  '<br/>' + suffix  : ''),
                        type: 'info',
                        actions: false,
                        duration: 1500
                    });

                this.onSuccess && this.onSuccess();
            },
            _onError:function(title,suffix,message){

                title = title  || this.title;

                message = message || this.notificationMessage;

                message && message.update({
                        message: title + this.failedText + (suffix ?  '<br/>' + suffix  : ''),
                        type: 'error',
                        actions: false,
                        duration: 15000
                });





                this.onError && this.onError(suffix);
            },
            onOk:function() {
                var msg = this.showMessage(),
                    thiz = this;
                this.doOk(this.getOkDfd());
            },
            showMessage:function(title){

                if(this.notificationMessage){
                    return this.notificationMessage;
                }
                title = title || this.title;

                var msg = this.ctx.getNotificationManager().postMessage({
                    message:title + (this.showSpinner ? this.spinner : ''),
                    type:'info',
                    showCloseButton: true,
                    duration:4500
                });

                this.notificationMessage = msg;

                return msg;
            }

        });
    }

    function deleteSelection(){

        var selection = this.getSelection();


        var _next = this.getNext(selection[0],null,true);
        var _prev = this.getPrevious(selection[0],null,true);

        var next = _next || _prev;


        var serverParams = this._buildServerSelection(selection);


        //var dlgClass = createDialogClass(title,types.DIALOG_TYPE.DANGER);
        var dlgClass = FileOperationDialog;

        var title = 'Delete ' + serverParams.selection.length + ' ' + 'items';
        var thiz = this;



        var dlg = new dlgClass({
            ctx:thiz.ctx,
            notificationMessage:null,
            title:title,
            type:types.DIALOG_TYPE.DANGER,
            onBeforeOk:function(){
                thiz.deselectAll();
            },
            getOkDfd:function(){
                var thiz = this;
                return this.ctx.getFileManager().deleteItems(serverParams.selection, {
                    hints: serverParams.hints
                },{
                    checkErrors:false,
                    returnProm:false,
                    onError:function(err){
                        thiz._onError(null,err.message);
                    }
                });
            },
            onSuccess:function(){

                thiz.runAction(types.ACTION.RELOAD).then(function(){
                    /*setTimeout(function(){*/
                        //thiz.focus();
                        thiz.select([next],null,true,{
                            focus:true,
                            append:false,
                            delay:0
                        });
                    /*},300);*/
                });
            }
        });





        dlg.show();


        //console.log(_selection);
    }

    function runAction(_action){

        var action  = this.getAction(_action);

        //console.error('run action : ' + action.command);

        switch (action.command){
            case ACTION_TYPE.CLIPBOARD_PASTE:{
                clipboardPaste.apply(this,[]);
                return;
            }
        }
    }

    function defaultCopyOptions(){


            /***
             * gather options
             */
            var result = {
                    includes:[],
                    excludes:[],
                    mode:1501
                },
                flags = 4;

        /*
            if(this.maskPane && this.maskPane.treeView){
                result.includes = this.maskPane.treeView.getSelected();
                result.excludes = this.maskPane.treeView.getUnselected();
            }
        */

            switch(flags){

                case 1<<2:{
                    result.mode=1502;//all
                    break;
                }
                case 1<<4:{
                    result.mode=1501;//none
                    break;
                }
                case 1<<8:{
                    result.mode=1504;//newer
                    break;
                }
                case 1<<16:{
                    result.mode=1503;//size
                    break;
                }
            }

        return result;
    }

    function clipboardPaste(){

        var isCut = this.currentCutSelection,
            items = isCut ? this.currentCutSelection : this.currentCopySelection,
            serverParams = this._buildServerSelection(items),
            serverFunction = isCut ? 'moveItem' : 'copyItem',
            self = this;


        if(!items){
            console.error('abort copy');
            return;
        }

        console.error('paste ! is cut ' + isCut,serverParams);

        var options = defaultCopyOptions();

        self.ctx.getFileManager()[serverFunction](serverParams.selection, serverParams.dstPath, {
            include: options.includes,
            exclude: options.excludes,
            mode: options.mode,
            hints: serverParams.hints
        }).then(function (data) {
            console.error('pasted don!',data);
            self.runAction(types.ACTION.RELOAD).then(function(){

            });

            //thiz.didFileOperation(types.OPERATION.COPY, view, serverParams.selection, serverParams.store);


        },function(err){
            console.error('pasted err!',err);
        },function(progress){
            console.error('paste progress',progress);
        });
    }

    function doTests3(){

        var parent = this.getParent();

        //console.dir(this._parent);

        /*
        this._getIconClassAttr=function(){
            debugger;
        }
        var _icn = this.get('iconClass');
        console.log('_i'+_icn);

        */
        //this.set('loading',false);
        //parent.startLoading('',0.5);

    }

    function doTests(tab,grid){


        grid.refresh().then(function(){
            grid.select([0]).then(function(){
                grid.runAction(ACTION_TYPE.EDIT).then(function(){
                    grid.deselectAll();
                    grid.select([1],null,true,{
                        append:false,
                        delay:0
                    }).then(function() {
                        grid.deselectAll();
                        grid.select([1]).then(function(){

                            doTests2.apply(grid);

                            //grid.runAction(ACTION_TYPE.CLIPBOARD_COPY);
                            //grid.runAction(ACTION_TYPE.CLIPBOARD_PASTE);
                        });

                    });
                });
            });

        })

    }
    function doTests3(tab,grid){
        setTimeout(function(){

            console.dir(Keyboard.listeners);
            grid.destroy();
            var listeners = Keyboard.listeners;
            var byNode = Keyboard.byNode;
            console.dir(Keyboard.listeners);
            //console.dir(byNode);

        },5000);
        /*
        grid.refresh().then(function(){
        });
        */

    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {


        var parent = TestUtils.createTab(null,null,module.id);

        var grid = FTestUtils.createFileGrid('root',
            //args
            {


            },

            //overrides
            {
                runAction:function(action) {
                    //return runAction.apply(this,[action]);
                    var res = this.inherited(arguments);
                    runAction.apply(this,[action]);
                    return res;
                }

            },'TestGrid',module.id,true,parent);

        doTests3(parent,grid);

        return declare('a',null,{});

    }

    return Grid;

});