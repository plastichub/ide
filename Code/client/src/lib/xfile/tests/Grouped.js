/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xgrid/Grid',
    'xfile/data/Store',
    'xgrid/MultiRenderer',
    'xfile/views/FileGrid',
    'xaction/DefaultActions',
    'dojo/dom-construct',
    'xgrid/Renderer',
    'xfile/model/File',
    'xide/tests/TestUtils',
    'xfile/ThumbRenderer',
    'module'
], function (declare, types,
             utils, ListRenderer, TreeRenderer,
             Trackable, ObservableStore, Grid, Store, MultiRenderer, FileGrid, DefaultActions, domConstruct, Renderer, File,TestUtils,ThumbRenderer,module) {


    /***
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;


    console.clear();

    function testMain(grid) {

    }

    function createGroupRenderClass() {

        var _class = declare('xfile.GroupRenderer', TreeRenderer, {
            groupRenderer:ListRenderer,
            itemRenderer:ThumbRenderer,
            deactivateRenderer:function(){

            },
            renderRow: function (obj) {
                var thiz = this;

                console.error('render',obj);
                if(!obj.group){
                    return ThumbRenderer.prototype.renderRow.apply(this,arguments);
                }else{
                    return TreeRenderer.prototype.renderRow.apply(this,arguments);
                }
            },
            activateRenderer:function(){

                var subRows = this.subRows,
                    srLength, cLength, sr, c,
                    thiz = this;

                var collection = this.collection;

                console.error('activate',collection.data);

                for (sr = 0, srLength = subRows.length; sr < srLength; sr++) {
                    for (c = 0, cLength = subRows[sr].length; c < cLength; c++) {

                        var col = subRows[sr][c],
                            id = col.id;

                        console.error('columns',col);

                        if (col.hidden===true) {
                            // Hide the column (reset first to avoid short-circuiting logic)
                            col.hidden = false;
                            thiz._hideColumn(id);
                            col.hidden = true;
                        }

                        var group = collection.putSync({
                            name:col.label,
                            path:'',
                            group:true,
                            parent:'none',
                            directory:true,
                            col:col,
                            modified:1,
                            sizeBytes:0,
                            size:'',
                            mount:collection.mount,
                            fileType:''
                        });

                    }
                }

            }
        });

        return _class;
    }


    function getFileActions(permissions) {

        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();

        permissions = permissions || this.permissions;

        return result;
    }

    function createStore(mount) {

        var storeClass = declare.classFactory('fileStore', [Store, Trackable, ObservableStore]);
        //var MyModel = declare(Model, {});
        var config = types.config;

        var options = {
            fields: types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME
        };

        var store = new storeClass({
            idProperty: 'path',
            Model: File,
            data: [],
            config: config,
            url: types.config.FILE_SERVICE,
            serviceUrl: types.config.FILE_SERVICE,
            serviceClass: types.config.FILES_STORE_SERVICE_CLASS,
            mount: mount,
            options: options
        });

        store._state = {
            initiated: false,
            filter: null,
            filterDef: null
        };

        store.reset();
        store.config = config;
        store.setData([]);
        store.init();

        return store;
    }

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var fileStore = createStore('root');

            var parent = TestUtils.createTab(null,null,module.id);

            var groupedRenderer = createGroupRenderClass();

            var renderers = [ListRenderer, TreeRenderer];


            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var mainView = ctx.mainView;

            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon,
                store = fileStore;

            var _gridClass = declare('File2', FileGrid, {
                selectedSource: 'Root',
                renderers: renderers
            });


            /**
             *
             * @param item
             * @param where
             * @param mixin
             * @param select
             * @returns {*}
             */
            grid = new _gridClass({
                selectedRenderer: groupedRenderer,
                collection: store.getDefaultCollection(),
                showHeader: true,
                _parent: parent
            }, parent.containerNode);
            grid.startup();

            /*var _actions = [];

            var _defaultActions = DefaultActions.getDefaultActions(_actions, grid);
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid));
            _defaultActions = _defaultActions.concat(grid.getFileActions(_actions));
            grid.addActions(_defaultActions);

            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();
            toolbar.setActionEmitter(grid);
            */

            ctx.getWindowManager().registerView(grid);


            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);
            setTimeout(function () {
                testMain(grid);
            }, 2000);


        }
    }

    return Grid;

});