/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    "xide/views/_Dialog",

    "xfile/views/FileOperationDialog",
    'xide/encoding/SHA1'


], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,_Dialog,FileOperationDialog,SHA1
) {

    console.clear();
    console.log('--do-tests');

    function FileSaver(){
        /* FileSaver.js
         * A saveAs() FileSaver implementation.
         * 1.1.20151003
         *
         * By Eli Grey, http://eligrey.com
         * License: MIT
         *   See https://github.com/eligrey/FileSaver.js/blob/master/LICENSE.md
         */

        /*global self */
        /*jslint bitwise: true, indent: 4, laxbreak: true, laxcomma: true, smarttabs: true, plusplus: true */

        /*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */

        var saveAs = saveAs || (function(view) {
                "use strict";
                // IE <10 is explicitly unsupported
                if (typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
                    return;
                }
                var
                    doc = view.document
                // only get URL when necessary in case Blob.js hasn't overridden it yet
                    , get_URL = function() {
                        return view.URL || view.webkitURL || view;
                    }
                    , save_link = doc.createElementNS("http://www.w3.org/1999/xhtml", "a")
                    , can_use_save_link = "download" in save_link
                    , click = function(node) {
                        var event = new MouseEvent("click");
                        node.dispatchEvent(event);
                    }
                    , is_safari = /Version\/[\d\.]+.*Safari/.test(navigator.userAgent)
                    , webkit_req_fs = view.webkitRequestFileSystem
                    , req_fs = view.requestFileSystem || webkit_req_fs || view.mozRequestFileSystem
                    , throw_outside = function(ex) {
                        (view.setImmediate || view.setTimeout)(function() {
                            throw ex;
                        }, 0);
                    }
                    , force_saveable_type = "application/octet-stream"
                    , fs_min_size = 0
                // See https://code.google.com/p/chromium/issues/detail?id=375297#c7 and
                // https://github.com/eligrey/FileSaver.js/commit/485930a#commitcomment-8768047
                // for the reasoning behind the timeout and revocation flow
                    , arbitrary_revoke_timeout = 500 // in ms
                    , revoke = function(file) {
                        var revoker = function() {
                            if (typeof file === "string") { // file is an object URL
                                get_URL().revokeObjectURL(file);
                            } else { // file is a File
                                file.remove();
                            }
                        };
                        if (view.chrome) {
                            revoker();
                        } else {
                            setTimeout(revoker, arbitrary_revoke_timeout);
                        }
                    }
                    , dispatch = function(filesaver, event_types, event) {
                        event_types = [].concat(event_types);
                        var i = event_types.length;
                        while (i--) {
                            var listener = filesaver["on" + event_types[i]];
                            if (typeof listener === "function") {
                                try {
                                    listener.call(filesaver, event || filesaver);
                                } catch (ex) {
                                    throw_outside(ex);
                                }
                            }
                        }
                    }
                    , auto_bom = function(blob) {
                        // prepend BOM for UTF-8 XML and text/* types (including HTML)
                        if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
                            return new Blob(["\ufeff", blob], {type: blob.type});
                        }
                        return blob;
                    }
                    , FileSaver = function(blob, name, no_auto_bom) {
                        if (!no_auto_bom) {
                            blob = auto_bom(blob);
                        }
                        // First try a.download, then web filesystem, then object URLs
                        var
                            filesaver = this
                            , type = blob.type
                            , blob_changed = false
                            , object_url
                            , target_view
                            , dispatch_all = function() {
                                dispatch(filesaver, "writestart progress write writeend".split(" "));
                            }
                        // on any filesys errors revert to saving with object URLs
                            , fs_error = function() {
                                if (target_view && is_safari && typeof FileReader !== "undefined") {
                                    // Safari doesn't allow downloading of blob urls
                                    var reader = new FileReader();
                                    reader.onloadend = function() {
                                        var base64Data = reader.result;
                                        target_view.location.href = "data:attachment/file" + base64Data.slice(base64Data.search(/[,;]/));
                                        filesaver.readyState = filesaver.DONE;
                                        dispatch_all();
                                    };
                                    reader.readAsDataURL(blob);
                                    filesaver.readyState = filesaver.INIT;
                                    return;
                                }
                                // don't create more object URLs than needed
                                if (blob_changed || !object_url) {
                                    object_url = get_URL().createObjectURL(blob);
                                }
                                if (target_view) {
                                    target_view.location.href = object_url;
                                } else {
                                    var new_tab = view.open(object_url, "_blank");
                                    if (new_tab == undefined && is_safari) {
                                        //Apple do not allow window.open, see http://bit.ly/1kZffRI
                                        view.location.href = object_url
                                    }
                                }
                                filesaver.readyState = filesaver.DONE;
                                dispatch_all();
                                revoke(object_url);
                            }
                            , abortable = function(func) {
                                return function() {
                                    if (filesaver.readyState !== filesaver.DONE) {
                                        return func.apply(this, arguments);
                                    }
                                };
                            }
                            , create_if_not_found = {create: true, exclusive: false}
                            , slice
                            ;
                        filesaver.readyState = filesaver.INIT;
                        if (!name) {
                            name = "download";
                        }
                        if (can_use_save_link) {
                            object_url = get_URL().createObjectURL(blob);
                            save_link.href = object_url;
                            save_link.download = name;
                            setTimeout(function() {
                                click(save_link);
                                dispatch_all();
                                revoke(object_url);
                                filesaver.readyState = filesaver.DONE;
                            });
                            return;
                        }
                        // Object and web filesystem URLs have a problem saving in Google Chrome when
                        // viewed in a tab, so I force save with application/octet-stream
                        // http://code.google.com/p/chromium/issues/detail?id=91158
                        // Update: Google errantly closed 91158, I submitted it again:
                        // https://code.google.com/p/chromium/issues/detail?id=389642
                        if (view.chrome && type && type !== force_saveable_type) {
                            slice = blob.slice || blob.webkitSlice;
                            blob = slice.call(blob, 0, blob.size, force_saveable_type);
                            blob_changed = true;
                        }
                        // Since I can't be sure that the guessed media type will trigger a download
                        // in WebKit, I append .download to the filename.
                        // https://bugs.webkit.org/show_bug.cgi?id=65440
                        if (webkit_req_fs && name !== "download") {
                            name += ".download";
                        }
                        if (type === force_saveable_type || webkit_req_fs) {
                            target_view = view;
                        }
                        if (!req_fs) {
                            fs_error();
                            return;
                        }
                        fs_min_size += blob.size;
                        req_fs(view.TEMPORARY, fs_min_size, abortable(function(fs) {
                            fs.root.getDirectory("saved", create_if_not_found, abortable(function(dir) {
                                var save = function() {
                                    dir.getFile(name, create_if_not_found, abortable(function(file) {
                                        file.createWriter(abortable(function(writer) {
                                            writer.onwriteend = function(event) {
                                                target_view.location.href = file.toURL();
                                                filesaver.readyState = filesaver.DONE;
                                                dispatch(filesaver, "writeend", event);
                                                revoke(file);
                                            };
                                            writer.onerror = function() {
                                                var error = writer.error;
                                                if (error.code !== error.ABORT_ERR) {
                                                    fs_error();
                                                }
                                            };
                                            "writestart progress write abort".split(" ").forEach(function(event) {
                                                writer["on" + event] = filesaver["on" + event];
                                            });
                                            writer.write(blob);
                                            filesaver.abort = function() {
                                                writer.abort();
                                                filesaver.readyState = filesaver.DONE;
                                            };
                                            filesaver.readyState = filesaver.WRITING;
                                        }), fs_error);
                                    }), fs_error);
                                };
                                dir.getFile(name, {create: false}, abortable(function(file) {
                                    // delete file if it already exists
                                    file.remove();
                                    save();
                                }), abortable(function(ex) {
                                    if (ex.code === ex.NOT_FOUND_ERR) {
                                        save();
                                    } else {
                                        fs_error();
                                    }
                                }));
                            }), fs_error);
                        }), fs_error);
                    }
                    , FS_proto = FileSaver.prototype
                    , saveAs = function(blob, name, no_auto_bom) {
                        return new FileSaver(blob, name, no_auto_bom);
                    }
                    ;
                // IE 10+ (native saveAs)
                if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
                    return function(blob, name, no_auto_bom) {
                        if (!no_auto_bom) {
                            blob = auto_bom(blob);
                        }
                        return navigator.msSaveOrOpenBlob(blob, name || "download");
                    };
                }

                FS_proto.abort = function() {
                    var filesaver = this;
                    filesaver.readyState = filesaver.DONE;
                    dispatch(filesaver, "abort");
                };
                FS_proto.readyState = FS_proto.INIT = 0;
                FS_proto.WRITING = 1;
                FS_proto.DONE = 2;

                FS_proto.error =
                    FS_proto.onwritestart =
                        FS_proto.onprogress =
                            FS_proto.onwrite =
                                FS_proto.onabort =
                                    FS_proto.onerror =
                                        FS_proto.onwriteend =
                                            null;

                return saveAs;
            }(
                typeof self !== "undefined" && self
                || typeof window !== "undefined" && window
                || this.content
            ));
// `self` is undefined in Firefox for Android content script context
// while `this` is nsIContentFrameMessageManager
// with an attribute `content` that corresponds to the window

        if (typeof module !== "undefined" && module.exports) {
            module.exports.saveAs = saveAs;
        } else if ((typeof define !== "undefined" && define !== null) && (define.amd != null)) {
            define([], function() {
                return saveAs;
            });
        }
    }

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;



    function createDialogClass(title,type){

        return dcl(_Dialog, {
            title:title,
            type: type || types.DIALOG_TYPE.INFO,
            size: types.DIALOG_SIZE.SIZE_SMALL,
            bodyCSS: {},
            failedText:' Failed!',
            successText:': Success!',
            showSpinner:true,
            spinner:'  <span class="fa-spinner fa-spin"/>',
            notificationMessage:null,
            doOk:function(dfd){

                this.onBeforeOk && this.onBeforeOk();

                var msg  = this.showMessage(),
                    thiz = this;

                dfd.then(function (result) {
                    thiz._onSuccess(result);
                }, function (err) {
                    thiz._onError();
                });

            },
            _onSuccess:function(title,suffix,message){

                title = title  || this.title;
                message = message || this.notificationMessage;

                message && message.update({
                        message: title + this.successText + (suffix ?  '<br/>' + suffix  : ''),
                        type: 'info',
                        actions: false,
                        duration: 1500
                    });

                this.onSuccess && this.onSuccess();
            },
            _onError:function(title,suffix,message){

                title = title  || this.title;

                message = message || this.notificationMessage;

                message && message.update({
                        message: title + this.failedText + (suffix ?  '<br/>' + suffix  : ''),
                        type: 'error',
                        actions: false,
                        duration: 15000
                });





                this.onError && this.onError(suffix);
            },
            onOk:function() {
                var msg = this.showMessage(),
                    thiz = this;
                this.doOk(this.getOkDfd());
            },
            showMessage:function(title){

                if(this.notificationMessage){
                    return this.notificationMessage;
                }
                title = title || this.title;

                var msg = this.ctx.getNotificationManager().postMessage({
                    message:title + (this.showSpinner ? this.spinner : ''),
                    type:'info',
                    showCloseButton: true,
                    duration:4500
                });

                this.notificationMessage = msg;

                return msg;
            }

        });
    }

    function deleteSelection(){

        var selection = this.getSelection();


        var _next = this.getNext(selection[0],null,true);
        var _prev = this.getPrevious(selection[0],null,true);

        var next = _next || _prev;


        var serverParams = this._buildServerSelection(selection);


        //var dlgClass = createDialogClass(title,types.DIALOG_TYPE.DANGER);
        var dlgClass = FileOperationDialog;

        var title = 'Delete ' + serverParams.selection.length + ' ' + 'items';
        var thiz = this;



        var dlg = new dlgClass({
            ctx:thiz.ctx,
            notificationMessage:null,
            title:title,
            type:types.DIALOG_TYPE.DANGER,
            onBeforeOk:function(){
                thiz.deselectAll();
            },
            getOkDfd:function(){
                var thiz = this;
                return this.ctx.getFileManager().deleteItems(serverParams.selection, {
                    hints: serverParams.hints
                },{
                    checkErrors:false,
                    returnProm:false,
                    onError:function(err){
                        thiz._onError(null,err.message);
                    }
                });
            },
            onSuccess:function(){

                thiz.runAction(types.ACTION.RELOAD).then(function(){
                    /*setTimeout(function(){*/
                        //thiz.focus();
                        thiz.select([next],null,true,{
                            focus:true,
                            append:false,
                            delay:0
                        });
                    /*},300);*/
                });
            }
        });





        dlg.show();


        //console.log(_selection);
    }

    function download(src){



        var selection = [];
        selection.push(src.path);
        var thiz = this;
        var downloadUrl = this.config.FILE_SERVICE;
        downloadUrl = downloadUrl.replace('view=rpc', 'view=smdCall');
        if (downloadUrl.indexOf('?') != -1) {
            downloadUrl += '&';
        } else {
            downloadUrl += '?';
        }
        var serviceClass = this.serviceClass || 'XCOM_Directory_Service';
        var path = utils.buildPath(src.mount, src.path, true);
        path = this.serviceObject.base64_encode(path);
        downloadUrl += 'service=' + serviceClass + '.get&path=' + path + '&callback=asdf';



        if (this.config.DOWNLOAD_URL != null) {
            downloadUrl = '' + this.config.DOWNLOAD_URL;
            downloadUrl += '&path=' + path + '&callback=asdf';
        }
        downloadUrl += '&raw=html';
        downloadUrl += '&attachment=1';
        var aParams = utils.getUrlArgs(location.href);

        lang.mixin(aParams, {
            "service": serviceClass + ".get",
            "path": path,
            "callback": "asdf",
            "raw": "html",
            "attachment": "1"
        });


        var pStr = dojo.toJson(aParams);
        var signature = SHA1._hmac(pStr, this.config.RPC_PARAMS.rpcSignatureToken, 1);
        downloadUrl += '&' + this.config.RPC_PARAMS.rpcUserField + '=' + this.config.RPC_PARAMS.rpcUserValue;
        downloadUrl += '&' + this.config.RPC_PARAMS.rpcSignatureField + '=' + signature;
        window.open(downloadUrl);
    }

    function runAction(_action){

        var action  = this.getAction(_action);

        console.error('run action : ' + action.command);

        var selection = this.getSelection();
        if(!selection){
            return;
        }

        switch (action.command){
            case ACTION_TYPE.DOWNLOAD:{

                return download.apply(this.ctx.getFileManager(),[selection[0]]);

            }
            case ACTION_TYPE.CLIPBOARD_PASTE:{
                clipboardPaste.apply(this,[]);
                return;
            }
        }
    }

    function defaultCopyOptions(){


            /***
             * gather options
             */
            var result = {
                    includes:[],
                    excludes:[],
                    mode:1501
                },
                flags = 4;

        /*
            if(this.maskPane && this.maskPane.treeView){
                result.includes = this.maskPane.treeView.getSelected();
                result.excludes = this.maskPane.treeView.getUnselected();
            }
        */

            switch(flags){

                case 1<<2:{
                    result.mode=1502;//all
                    break;
                }
                case 1<<4:{
                    result.mode=1501;//none
                    break;
                }
                case 1<<8:{
                    result.mode=1504;//newer
                    break;
                }
                case 1<<16:{
                    result.mode=1503;//size
                    break;
                }
            }

        return result;
    }

    function doTests(tab,grid){


        grid.refresh().then(function(){

            grid.select([0]).then(function(){

                grid.runAction(ACTION_TYPE.EDIT).then(function(){

                    grid.deselectAll();
                    grid.select([1],null,true,{
                        append:false,
                        delay:0
                    }).then(function() {
                        grid.deselectAll();
                        grid.select([1]).then(function(){
                            grid.runAction(ACTION_TYPE.DOWNLOAD);
                        });

                    });
                });
            });

        })

    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {


        

        var parent = TestUtils.createTab(null,null,module.id);

        var grid = FTestUtils.createFileGrid('root',
            //args
            {


            },

            //overrides
            {
                runAction:function(action) {
                    //return runAction.apply(this,[action]);
                    var res = this.inherited(arguments);

                    runAction.apply(this,[action]);

                    return res;
                }

            },'TestGrid',module.id,true,parent);

        doTests(parent,grid);

        return declare('a',null,{});

    }

    return Grid;

});