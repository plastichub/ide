define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "module",
    "dojo/Deferred",
    'xide/factory'
], function (declare, dcl, types,
    utils, Grid, TemplatedWidgetBase, TestUtils, FTestUtils, module,
    Deferred, factory
) {

    console.clear();
    var ACTION_TYPE = types.ACTION;

    function createCIS() {

        var CIS = {
            inputs: [
                utils.createCI('Name', 13, 'asd', {
                    widget: {
                        instant: true,
                        validator: function (value) {
                            return value == 'asd';
                            return false;
                        }
                    }
                })
            ]
        }
        return CIS;
    }

    function runAction(_action) {
        console.log('run action ', _action);
        var action = this.getAction(_action);
        switch (action.command) {
            case ACTION_TYPE.RENAME:
                {
                    return rename.apply(this, []);
                }

        }
    }

    function rename() {
        var dfd = new Deferred();
        dfd.resolve();
        return dfd;
    }

    function doTests(tab, grid) {
        grid.refresh().then(function () {
            grid.select([0]).then(function (sel) {
                /*
                 grid.runAction(ACTION_TYPE.RENAME).then(function(){
                     console.error('did run renam');
                 });
                 */
            });

        })

    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    const widgetClass = dcl(TemplatedWidgetBase, {
        templateString: '<div class="CIActionWidget"></div>',
        widgetClass: ''
    });

    const createActionWidgetClass = (action) => {
        return dcl(widgetClass, {
            cis: [],
            action: action,
            startup: function () {
                factory.renderCIS(this.cis, this.domNode, this).then((widgets) => {
                    widgets.forEach((w) => {
                        action.addReference(w);
                        w._on('change', (what) => {
                            action.set('value', what);
                            action.refresh();
                        });
                    });
                });
                
                setTimeout(() => {
                    action.refresh();
                }, 1000)
            },
            render: function (data, $menu) {
                console.log('render');
            }
        })
    }

    if (ctx) {

        var parent = TestUtils.createTab(null, null, module.id);


        var grid = FTestUtils.createFileGrid('root', {},
            //overrides
            {
                getFileActions: function (permissions) {
                    permissions = permissions || this.permissions;
                    var result = [],
                        ACTION = types.ACTION,
                        ACTION_ICON = types.ACTION_ICON,
                        VISIBILITY = types.ACTION_VISIBILITY,
                        thiz = this,
                        ctx = thiz.ctx,
                        container = thiz.domNode,
                        resourceManager = ctx.getResourceManager(),
                        vfsConfig = resourceManager ? resourceManager.getVariable('VFS_CONFIG') : {}, //possibly resourceManager not there in some builds
                        actionStore = thiz.getActionStore();

                    ///////////////////////////////////////////////////
                    //
                    //  misc
                    //
                    ///////////////////////////////////////////////////
                    result.push(thiz.createAction({
                        label: 'CIAction',
                        command: 'File/CI',
                        icon: ACTION_ICON.GO_UP,
                        tab: 'Home',
                        group: 'Navigation',
                        mixin: {
                            quick: true,
                            addPermission: true
                        },
                        onCreate: function (action) {
                            const clz = createActionWidgetClass(action);
                            const viz = types.ACTION_VISIBILITY_ALL;
                            action.setVisibility(viz, {
                                widgetClass: clz,
                                closeOnClick: false,
                                widgetArgs: {
                                    cis: [
                                        utils.createCI('', types.ECIType.ENUMERATION, '2', {
                                            widget: {
                                                inline: true,
                                                options: [{
                                                    value: 2,
                                                    label: '2121'
                                                }, {
                                                    value: 3,
                                                    label: '34'
                                                }],
                                                widgetClass: "",
                                                action: action
                                            }
                                        })
                                    ]
                                }
                            });
                            // action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, false);
                            action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, false);
                        }
                    }));
                    return result;
                },
                runAction: function (action) {
                    console.log('run action');
                    var res = this.inherited(arguments);
                    var _resInner = runAction.apply(this, [action]);
                    return _resInner || res;
                }

            }, 'TestGrid', module.id, true, parent);

        doTests(parent, grid);
        return declare('a', null, {});
    }
    return Grid;

});