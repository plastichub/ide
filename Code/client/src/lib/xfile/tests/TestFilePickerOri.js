/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xfile/tests/TestUtils',
    'module',
    'xide/widgets/ActionToolbar',
    'xaction/ActionContext',
    'dijit/Toolbar',
    'dijit/ToolbarSeparator',
    'xide/widgets/ActionToolbarMixin',
    "xide/mixins/ActionMixin",
    "xide/mixins/EventedMixin",
    "xide/widgets/_MenuMixin",
    'xide/widgets/ActionToolbarButton',
    'dijit/Menu',
    'dijit/MenuItem',
    'xide/data/Reference',
    'dijit/form/DropDownButton',
    'xaction/DefaultActions',
    'xide/editor/Registry',

    'dojo/aspect',
    'xide/tests/TestUtils',
    'xide/_base/_Widget',
    'xdocker/Docker2',
    'xfile/ThumbRenderer',
    'dojo/promise/all',
    'xfile/Breadcrumb',
    'xide/model/Path',
    'xfile/views/FileGrid'


], function (declare, dcl, types, utils, factory,
             XFileTestUtils, module, ActionToolbar, ActionContext,
             Toolbar, ToolbarSeparator, ActionToolbarMixin, ActionMixin, EventedMixin, _MenuMixin,
             ActionToolbarButton,
             Menu, MenuItem, Reference,
             DropDownButton,
             DefaultActions,
             Registry,
             aspect, TestUtils,
             _Widget, Docker2, ThumbRenderer, all, Breadcrumb, Path, FileGrid) {

    console.clear();


    /**
     *
     * A wc-docker based layout container
     *
     */
    function createLayoutClass() {
        var Module = dcl(_Widget, {
            templateString: '<div></div>',
            resizeToParent: true,
            docker: null,
            breadcrumb: null,
            FileGridClass: FileGrid,

            leftStore: null,
            rightStore: null,

            storeOptionsMixin: null,
            storeOptions: null,

            leftGrid: null,
            leftGridArgs: null,

            rightGrid: null,
            rightGridArgs: null,

            defaultGridArgs: {

                showHeader: false,
                registerEditors: false,
                _columns: {
                    "Name": true,
                    "Path": false,
                    "Size": false,
                    "Modified": false
                }
            },

            mount: 'root',
            config: null,

            createLayout: function () {},
            /**
             *
             * @param store
             * @param parent
             * @param args
             * @returns {*|widgetProto}
             */
            createFileGrid: function (store, parent, args) {

                var GridClass = this.FileGridClass || FileGrid;

                return GridClass.createDefault(ctx, args, parent, false, true, store);


            },
            startup: function () {

                this.inherited(arguments);

                var dockerOptions = {
                    resizeToParent: true
                };

                var docker = Docker2.createDefault(this.domNode, dockerOptions);
                docker.resizeToParent = true;
                this.add(docker, null, false);
                this.docker = docker;



                /*
                 var layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                 w: '30%',
                 h:'90%',
                 title:'&nbsp;&nbsp;'
                 });*/


                var layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                    h: 50,
                    w: '100px',
                    title: false
                });

                var breadcrumb = utils.addWidget(Breadcrumb, {}, null, layoutTop, true);
                var layoutLeft = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.BOTTOM, null, {
                    w: '30%',
                    title: false
                });
                var layoutMain = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.RIGHT, layoutLeft, {
                    w: '70%',
                    title: false
                });


                this.breadcrumb = breadcrumb;

                /**
                 * Selected item
                 */

                var selected = './AA/xfile_last/Selection_002.png';
                var pathInfo = utils.pathinfo(selected, types.PATH_PARTS.ALL);
                var path = pathInfo.dirname;

                var ctx = this.ctx;


                /**
                 * File Stores
                 */

                var mount = this.mount || 'root';

                var storeOptions = this.storeOptions;

                var storeOptionsMixin = this.storeOptionsMixin || {
                        "includeList": "*.jpg,*.png",
                        "excludeList": "*"
                    }

                var config = this.config;

                var leftStore = this.leftStore || factory.createFileStore(mount, storeOptions, config, storeOptionsMixin, ctx);
                var rightStore = this.rightStore || factory.createFileStore(mount, storeOptions, config, storeOptionsMixin, ctx);



                /**
                 *  Left - Grid :
                 */
                var leftGridArgs = this.leftGridArgs || {
                        showHeader: false,
                        registerEditors: false,
                        _columns: {
                            "Name": true,
                            "Path": false,
                            "Size": false,
                            "Modified": false
                        },
                        newTarget: layoutMain
                    }


                var gridLeft = this.leftGrid || this.createFileGrid(leftStore, layoutLeft.containerNode, leftGridArgs);

                /*
                 var gridLeft = XFileTestUtils.createFileGrid('root',
                 //args
                 leftGridArgs,
                 //overrides
                 {

                 },'TestGrid',module.id,false,layoutLeft.containerNode,storeOptionsMixin);
                 */


                /**
                 *  Right - Grid :
                 */
                var rightGridArgs = this.rightGridArgs || {
                        showHeader: false,
                        registerEditors: true,
                        selectedRenderer: ThumbRenderer,
                        _columns: {
                            "Name": true,
                            "Path": false,
                            "Size": false,
                            "Modified": false
                        }
                    }


                var gridMain = this.rightGrid || this.createFileGrid(rightStore, layoutMain.containerNode, rightGridArgs);


                /*
                var gridMain = XFileTestUtils.createFileGrid('root',
                    //args
                    {
                        showHeader: false,
                        registerEditors: true,
                        selectedRenderer: ThumbRenderer,
                        _columns: {
                            "Name": true,
                            "Path": false,
                            "Size": false,
                            "Modified": false
                        }

                    },
                    //overrides
                    {}, 'TestGrid', module.id, false, layoutMain.containerNode, storeOptionsMixin);
                */



                var srcStore = leftStore,
                    targetStore = rightStore;
                

                function connectGrid(src, target) {

                    src._on('selectionChanged', function (event) {


                        console.log('selection changed');


                        if (event.why === 'deselect') {
                            return;
                        }


                        var selection = src.getSelection(),
                            item = src.getSelectedItem();


                        if (!item) {
                            return;
                        }

                        var targetFolder = null;

                        if (item.isDir) {
                            targetFolder = item;
                            //target.runAction(types.ACTION.EDIT);
                        } else {
                            targetFolder = item.getParent();// srcStore.getSync(item.parent);
                        }


                        console.log('open ' + item.path + ' item path = ' + item.getPath());


                        if (targetFolder) {

                            console.log('target ' + target.collection.id);
                            console.log('source ' + src.collection.id);
                            target.openFolder(targetFolder.path, true);
                        }


                    });


                }


                function gridsReady(src, target) {

                    console.log('set breadcrumb : ' + selected);
                    var cwd = src.getCurrentFolder();
                    var sourceLabel = (src.selectedSource || 'Root' ) + '://';
                    sourceLabel = '.';
                    var rootLabel = sourceLabel;

                    //console.log('cwd',rootLabel);

                    var srcHistory = src.getHistory();

                    //console.log('history : ' , srcHistory._history);


                    var _path = new Path(path);

                    var segs = _path.getSegments();


                    //add the very root item!
                    breadcrumb.add(rootLabel, true, srcStore.getRootItem());

                    function toHistory(grid, item) {
                        var FolderPath = new Path(item.getFolder());
                        var segs = FolderPath.getSegments();
                        var _last = '.';
                        var out = ['.'];
                        _.each(segs, function (seg) {
                            var segPath = _last + '/' + seg;
                            //breadcrumb.add(seg,true, srcStore.getSync(segPath));
                            out.push(segPath);
                            _last = segPath;
                        });
                        return out;
                    }


                    //var _history = toHistory(src,cwd);

                    //srcHistory._history=_history;
                    //console.log(' new history : ' , srcHistory._history);


                    //now add breadcrum items for each path segment

                    var _last = '.';
                    _.each(segs, function (seg) {
                        var segPath = _last + '/' + seg;
                        breadcrumb.add(seg, true, srcStore.getSync(segPath));
                        _last = segPath;
                    });


                    breadcrumb._on('click', function (e) {
                        var data = e.element[0].data;
                        breadcrumb.unwind(data);
                        src.openFolder(data);
                    });


                    src._on('openFolder', function (evt) {

                        var isBack = evt.back,
                            item = evt.item;

                        if (isBack) {
                            breadcrumb.pop();
                        } else {
                            item.name !== '.' && breadcrumb.add(item.name, true, item);
                        }
                    });


                }

                //init grids!
                var both = [gridLeft.refresh(), gridMain.refresh()];

                all(both).then(function () {


                    all([srcStore.getItem(path, true), targetStore.getItem(path, true)]).then(function () {
                        all(_.invoke([gridLeft, gridMain], 'openFolder', path)).then(function () {
                            all(_.invoke([gridLeft, gridMain], 'select', [selected], null, true, {
                                focus: true,
                                append: false
                            })).then(function () {
                                //console.error('selected!');
                                connectGrid(gridLeft, gridMain);
                                gridsReady(gridLeft, gridMain);
                            });
                        });
                        return;


                        //open folder in left:
                        var folder = srcStore.getSync(path);
                        if (folder) {
                            gridLeft.openFolder(folder);

                            var file = targetStore.getSync(selected);
                            gridMain.openFolder(folder).then(function () {
                                console.error('main ready', file);
                                if (file) {
                                    gridMain.select([file], null, true, {
                                        focus: true,
                                        append: false,
                                        delay: 1
                                    })
                                }
                            })
                        }


                    });
                    /*
                     srcStore.getItem(path,true).then(function(data){


                     var folder = srcStore.getSync(path);
                     //console.error('loaded!',folder);
                     if(folder){
                     gridLeft.openFolder(folder);
                     }else{
                     console.error('cant find item!');
                     }


                     });
                     */

                });


                //var layout = utils.getJson(docker.save());


            }
        });
        return Module;


    }


    /***
     *
     * playground
     */
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;

    function testMain(panel) {

        var ACTION_TYPE = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            ribbon,
            mainView = ctx.mainView;


        var LayoutClass = createLayoutClass();

        var layoutMain = utils.addWidget(LayoutClass, {
            ctx: ctx,
            config: types.config
        }, null, panel, true);

        panel.add(layoutMain, null, false);

        panel.resize();


        return;

        ctx.getWindowManager().registerView(grid, true);

        grid.refresh().then(function () {

        });


    }

    if (ctx && ctx.mainView) {

        var doTest = true;
        if (doTest) {

            var parent = TestUtils.createTab(null, null, module.id);
            testMain(parent);
        }
    }

    var _module = declare('maeh', null, {});


    return module;

});
