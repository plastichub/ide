/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xgrid/Grid',
    'xfile/data/Store',
    'xgrid/MultiRenderer',
    'xfile/views/FileGrid',
    'xaction/DefaultActions',
    'dojo/dom-construct',
    'xgrid/Renderer',
    'xfile/model/File',
    'xide/widgets/TemplatedWidgetBase',
    'dcl/dcl',
    'xide/_base/_Widget',
    'xide/model/Path'
], function (declare,types,utils,factory,
             ListRenderer, TreeRenderer,
             Trackable, ObservableStore, Grid, Store, MultiRenderer, FileGrid, DefaultActions, domConstruct,Renderer,File,TemplatedWidgetBase,dcl,_Widget,Path) {


    /***
     * playground
     */
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;

    var breadCrumbClass = dcl(_Widget,{
        templateString:"<ul attachTo='root' style='-webkit-app-region:drag' class='breadcrumb'></ul>",
        grid:null,
        destroy:function(){
            this.clear();
            this.grid = null;
        },
        setSource:function(src){

            if(this.grid==src){
                return;
            }

            this.grid = src;
            this.clear();
            var store = src.collection,
                cwdItem = src.getCurrentFolder(),
                cwd = cwdItem ? cwdItem.path : '';
            this.setPath('.',store.getRootItem(),cwd,store);
        },
        setPath : function(rootLabel,rootItem,path,store,_init){

            this.clear();
            this.addSegment(rootLabel, true, rootItem);
            var _path = new Path(path);
            var segs = _path.getSegments();
            var _last = _init || '.';
            _.each(segs, function (seg) {
                var segPath = _last + '/' + seg;
                this.addSegment(seg, true, store.getSync(segPath));
                _last = segPath;
            },this);
        },
        /**
         * Event delegation
         * @param label
         * @param e
         * @param el
         */
        onClick:function(label,e,el){
            this._emit('click',{
                element:el,
                event:e,
                label:label,
                data:el.data
            });
        },
        /**
         * Add a new breadcrumb item
         * @param label
         * @param active
         * @param data
         * @returns {*|jQuery|HTMLElement}
         */
        addSegment:function(label,active,data){

            if(data && this.has(data)){
                console.log('already have ' + data.path);
                return null;
            }

            var _class = active ? 'active' : '',
                self = this,
                el = $("<li class='" +_class + "'><a href='#'>" + label + "</a></li>");

            //clear active state of all previous elements
            _.each(this.all(),function(e){
                e.el.removeClass('active');
            });

            $(this.domNode).append(el);

            el.on('click',function(e){
                self.grid && self.grid.openFolder(data);
            });
            el[0].data = data;

            return el;
        },
        /**
         * Remove last breadcrumb item
         */
        pop:function(){
            this.last().remove();
        },
        /**
         * Return last breadcrumb item
         * @returns {*|jQuery}
         */
        last:function(){
            return $(this.root).children().last();
        },
        /**
         * Returns true when data is found
         * @param data
         * @returns {boolean}
         */
        has:function(data){

            var all = this.all();
            for (var i = 0; i < all.length; i++) {
                if(all[i].data.path === data.path){
                    return true;
                }
            }
            return false;
        },
        /**
         * Return all breadcrumb items
         * @returns {Array}
         */
        all:function(){

            var result = [];

            _.each(this.$root.children(),function(el){
                result.push({
                    el:$(el),
                    label:el.outerText,
                    data:el.data
                })
            });

            return result;

        },
        clear:function(){

            var all = this.all();
            while(all.length){
                var item =all[all.length-1];
                item.el.remove();
                item.data = null;
                all.remove(item);
            }
        },
        /**
         * Removes all breadcrumb items from back to beginning, until data.path matches
         * @param data
         */
        unwind:function(data){
            var rev = this.all().reverse();
            _.each(rev,function(e){
                if(e.data && e.data.path !== (data && data.path) && e.data.path!=='.'){
                    this.pop();
                }
            },this);
        },
        startup:function(){

            this._on('click',function(e){

                var data = e.element[0].data,
                    grid = this.grid;

                if(data) {
                    this.unwind(data);
                    grid && grid.openFolder(data);
                }

            }.bind(this));
        }
    });


    function createBreadCrumb(grid){

        var mainView = ctx.mainView;
        //var target = grid.statusbar ? grid.statusbar.parentNode : grid.header;

        var target = grid.header;
        var widget = utils.addWidget(breadCrumbClass,{},null,target,true);
        var cwd = grid.getCurrentFolder();
        var sourceLabel = (grid.selectedSource || 'Root' ) + '://';
        sourceLabel = '';
        var rootLabel = sourceLabel + (cwd ? cwd.name : '.');

        //console.log('cwd',rootLabel);

        widget.add(rootLabel,true,cwd);

        widget._on('click',function(e){
            var data = e.element[0].data;
            widget.unwind(data);
            grid.openFolder(data);

        });
        grid._on('openFolder',function(evt){

            var isBack = evt.back,
                item = evt.item;

            if(isBack){
                widget.pop();
            }else{
                item.name!=='.' && widget.add(item.name, true, item);
            }
        });
    }

    function createBreadCrumb2(grid){

        var mainView = ctx.mainView;
        //var target = grid.statusbar ? grid.statusbar.parentNode : grid.header;

        var target = grid.header;
        var widget = utils.addWidget(breadCrumbClass,{},null,target,true);
        var cwd = grid.getCurrentFolder();
        var sourceLabel = (grid.selectedSource || 'Root' ) + '://';
        sourceLabel = '';
        var rootLabel = sourceLabel + (cwd ? cwd.name : '.');

        //console.log('cwd',rootLabel);

        widget.add(rootLabel,true,cwd);

        widget._on('click',function(e){
            var data = e.element[0].data;
            widget.unwind(data);
            grid.openFolder(data);

        });
        grid._on('openFolder',function(evt){

            var isBack = evt.back,
                item = evt.item;

            if(isBack){
                widget.pop();
            }else{
                item.name!=='.' && widget.add(item.name, true, item);
            }
        });
    }

    function testMain(grid){
        createBreadCrumb(grid);
    }

    function createThumbRenderClass(){
        /**
         * The list renderer does nothing since the xgrid/Base is already inherited from
         * dgrid/OnDemandList and its rendering as list already.
         *
         * @class module:xgrid/ThumbRenderer
         * @extends module:xgrid/Renderer
         */
        //package via declare

        var _class = declare('xfile.ThumbRendererLarge',[Renderer],{

            _getLabel:function(){ return "Thumb"; },
            _getIcon:function(){ return "el-icon-th-large"; },
            sizeClass:'',
            startup:function(){


                if(this._started){
                    return;
                }
                var thiz = this;
            },
            thumbSize: "100",
            resizeThumb: true,
            _doubleWidthThumbs:true,
            deactivateRenderer:function(){

                $(this.domNode.parentNode).removeClass('metro');
                //$(this.domNode.parentNode).removeClass('container');


                $(this.domNode).css('padding','');/*
                $(this.contentNode).css('margin-top','0');*/


            },
            activateRenderer:function(){

                $(this.domNode.parentNode).addClass('metro');
                $(this.contentNode).css('padding','8px');


                /*
                $(this.domNode.parentNode).addClass('container');
                $(this.contentNode).css('padding','8px');
                $(this.contentNode).css('margin-top','16px');
                */
                //this._showHeader(false);

            },
            /**
             * Override renderRow
             * @param obj
             * @returns {*}
             */
            renderRow: function (obj) {
                var thiz = this,
                    div = domConstruct.create('div', {
                        className: "tile widget"
                    }),
                    icon = obj.icon,
                    no_access = obj.read === false && obj.write === false,
                    isBack = obj.name == '..',
                    directory = obj && !!obj.directory,
                    useCSS = false,
                    label = '',
                    imageClass = 'fa fa-folder fa-5x',
                    isImage = false,
                    sizeClass = this.sizeClass;


                this._doubleWidthThumbs = true;


                var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;';
                var contentClass = 'icon';



                if (directory) {

                    if (isBack) {
                        imageClass = 'fa fa-level-up fa-5x thumbIcon';
                        useCSS = true;
                    } else if (!no_access) {
                        imageClass = 'fa fa-folder fa-5x thumbIcon';
                        useCSS = true;
                    } else {
                        imageClass = 'fa fa-lock fa-5x thumbIcon';
                        useCSS = true;
                    }

                } else {

                    imageClass = 'itemIcon';

                    if (no_access) {

                        imageClass = 'fa fa-lock fa-5x thumbIcon';

                        useCSS = true;
                    } else {

                        if (utils.isImage(obj.path)) {

                            var url = this.getImageUrl(obj);
                            if (url) {
                                obj.icon = url;
                            } else {
                                obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                            }

                            imageClass = 'imageFile';

                        } else {
                            imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                            useCSS = true;
                        }
                    }

                }

                label = obj.name;

                var folderContent =  '<span style="' + iconStyle + '" class="fa '+imageClass +' thumbIcon"></span>';

                if (utils.isImage(obj.path)) {

                    var url = this.getImageUrl(obj);
                    if (url) {
                        obj.icon = url;
                    } else {
                        obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                    }


                    imageClass = '';
                    contentClass = 'image';
                    //folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';
                    folderContent = '<div style="padding:1px" class="tile-content image">' +
                        '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                        '</div>';

                    useCSS = true;
                    isImage = true;


                }


                //var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);left:40px;text-align:left;font-size: 72px;margin-top:-45px;opacity: 0.7';


                var html = '<div class="tile-content ' + contentClass +'">'+
                    folderContent +
                    '</div>'+

                    '<div class="brand bg-dark opacity">'+
                        '<span class="iconText text bg-dark opacity fg-white" style="">'+
                    label +
                    '</span>'+
                    '</div>';


                if(isImage || this._doubleWidthThumbs){
                    $(div).addClass(sizeClass);
                }

                if (useCSS) {
                    div.innerHTML = html;
                    return div;
                }


                if (directory) {

                    //div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name">' + obj.name + '</div>';


                    div.innerHTML = html;


                } else {
                    div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
                }
                return div;

            },
            getImageUrl: function (item) {

                var fileManager = this.ctx.getFileManager();
                if (fileManager && fileManager) {
                    var params = null;
                    if (this.resizeThumb) {
                        params = {
                            width: this.thumbSize
                        }
                    }
                    return fileManager.getImageUrl(item, null, params);
                }
                return null;
            }
        });

        return _class;
    }


    function getFileActions(permissions) {

        console.log('get file actions');

        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();

        permissions = permissions || this.permissions;

        function addAction(label,command,icon,keycombo,tab,group,filterGroup,onCreate,handler,mixin,shouldShow,shouldDisable){

            var action = null;
            if(DefaultActions.hasAction(permissions,command)){

                mixin = mixin || {};

                utils.mixin(mixin,{owner:this});

                if(!handler){

                    handler = function(action){
                        console.log('log run action',arguments);
                        var who = thiz;
                        if(who.runAction){
                            who.runAction.apply(who,[action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label,command,icon,keycombo,tab,group,filterGroup,onCreate,handler,mixin,shouldShow,shouldDisable,thiz.domNode);

                result.push(action);
                return action;

            }
        }

        function shouldDisableDefaultFileExtension(selection,reference,visibility){

            selection = thiz.getSelection();

            //console.log('selection',selection);

            if(selection==null){
                console.error('sel is nukk');
            }
            if(!selection || !selection.length || selection[0].isDir==true){
                return true;
            }

            var item   = selection[0],
                path = item.getPath();
            if(!path.match(/^(.*\.(?!(jpg|png)$))?[^.]*$/i)) {
                return false;
            }
            return true;
        }


        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////


        //addAction('Go Up',ACTION_TYPE.GO_UP,ACTION_ICON.GO_UP,['alt up','backspace'],'Home','Navigation','item|view',null,null,null,null,null);

        result.push(this.createAction('Source', 'View/Source','fa-hdd-o', ['f4'],'Home','Navigation','item',null,
            function () {},
            {
                addPermission:true,
                tab: 'Home',
                dummy:true
            },null,null,permissions,thiz.domNode,thiz
        ));

        var i = 0;


        function createSourceAction(item){

            var label = item.label;

            result.push(thiz.createAction(label, 'View/Source/'+label,'fa-hdd-o', ['alt f'+i],'Home','Navigation','item',null,
                function () {},
                {
                    addPermission:true,
                    tab: 'Home',
                    item:item
                },null,null,permissions,thiz.domNode,thiz
            ));

            i++;
        };

        var mountManager = ctx.getMountManager();
        var mountData = mountManager.getMounts();

        _.each(mountData,createSourceAction);





        return result;



    }

    function createStore(mount) {

        var storeClass = declare.classFactory('fileStore', [Store, Trackable, ObservableStore]);
        //var MyModel = declare(Model, {});
        var config = types.config;

        var options = {
            fields: types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME
        };

        var store = new storeClass({
            idProperty: 'path',
            Model: File,
            data: [],
            config: config,
            url: types.config.FILE_SERVICE,
            serviceUrl: types.config.FILE_SERVICE,
            serviceClass: types.config.FILES_STORE_SERVICE_CLASS,
            mount: mount,
            options: options
        });

        store._state = {
            initiated: false,
            filter: null,
            filterDef: null
        };

        store.reset();
        store.config = config;
        store.setData([]);
        store.init();

        return store;
    }

    if (ctx) {


        var doTest = true;
        if (doTest) {


            if(window['___bc']){
                window['___bc'].remove();
                window['___bc']=null;
            }

            var mainView = ctx.mainView,
                breadcrumb = mainView && mainView.getBreadcrumb ? mainView.getBreadcrumb() : null;

            function patch(view){

                view.getBreadcrumbPath = function(){


                    console.log('item ',this.item);
                    if(this.item){

                        return {
                            //root:utils.replaceAll('/','',this.item.mount),
                            path:utils.replaceAll('/','',this.item.mount) + ':/' + this.item.path.replace('./','/')
                        }
                    }


                    /*
                    var store = this.collection,
                        cwdItem = this.getCurrentFolder(),
                        cwd = cwdItem ? cwdItem.path : '';
                    */


                };

            }

            breadcrumb.setSource=function(src){

                if(!src || (!src.collection || !src.getBreadcrumbPath)){
                    return;
                }

                if(this.grid==src){
                    return;
                }

                this.grid = src;
                this.clear();

                var customPath = src.getBreadcrumbPath ? src.getBreadcrumbPath():null;
                if(customPath===false){
                    return;
                }
                if(customPath){
                    this.setPath(customPath.root, null, customPath, null);
                }else {
                    var store = src.collection,
                        cwdItem = src.getCurrentFolder(),
                        cwd = cwdItem ? cwdItem.path : '';

                    this.setPath('.', store.getRootItem(), cwd, store);
                }
            };

            breadcrumb.setPath =function(rootLabel,rootItem,path,store,_init){

                this.clear();
                rootItem && rootLabel && this.addSegment(rootLabel, true, rootItem);
                var _path = new Path(path);
                var segs = _path.getSegments();
                var _last = _init || '.';
                _.each(segs, function (seg) {
                    var segPath = _last + '/' + seg;
                    this.addSegment(seg, true, store ? store.getSync(segPath) : null);
                    _last = segPath;
                },this);
            },

            window['___bc'] = mainView.subscribe(types.EVENTS.ON_OPEN_VIEW, function (e) {

                var view = e.view;
                //patch(view);
                var canBreadcrumb = view.getBreadcrumbPath;
                if(canBreadcrumb){
                    console.log('-asdasd');
                    breadcrumb.setSource(view);
                    /*
                    view.addHandle('click',view.on('click',function(){
                        breadcrumb.setSource(view);
                    }));

                    breadcrumb.setSource(view);
                    */
                }
            });




            setTimeout(function () {


            }, 1000);


        }
    }


    return Grid;

});