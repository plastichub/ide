/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",
    "dojo/dom-construct",
    "dojo/_base/lang",
    "dojo/string",
    "xide/_base/_Widget",
    "xide/views/_Dialog",
    "xfile/views/FileOperationDialog",
    "dojo/Deferred",
    'xide/views/CIView',
    'xide/views/_CIDialog'

], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,_Dialog,FileOperationDialog,Deferred,CIView,_CIDialog
) {




    console.clear();
    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    function createCIS(){

        var CIS = {
            inputs:[
                utils.createCI('Name',13,'asd',{
                    widget:{
                        instant:true,
                        validator:function(value){
                            return value == 'asd';
                            return false;
                        }
                    }
                })
            ]
        }
        return CIS;
    }



    /*
    $(document).ready(function() {
        $('.input-group input[required], .input-group textarea[required], .input-group select[required]').on('keyup change', function() {
            var $form = $(this).closest('form'),
                $group = $(this).closest('.input-group'),
                $addon = $group.find('.input-group-addon'),
                $icon = $addon.find('span'),
                state = false;

            if (!$group.data('validate')) {
                state = $(this).val() ? true : false;
            }else if ($group.data('validate') == "email") {
                state = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($(this).val())
            }else if($group.data('validate') == 'phone') {
                state = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/.test($(this).val())
            }else if ($group.data('validate') == "length") {
                state = $(this).val().length >= $group.data('length') ? true : false;
            }else if ($group.data('validate') == "number") {
                state = !isNaN(parseFloat($(this).val())) && isFinite($(this).val());
            }

            if (state) {
                $addon.removeClass('danger');
                $addon.addClass('success');
                $icon.attr('class', 'glyphicon glyphicon-ok');
            }else{
                $addon.removeClass('success');
                $addon.addClass('danger');
                $icon.attr('class', 'glyphicon glyphicon-remove');
            }

            if ($form.find('.input-group-addon.danger').length == 0) {
                $form.find('[type="submit"]').prop('disabled', false);
            }else{
                $form.find('[type="submit"]').prop('disabled', true);
            }
        });

        $('.input-group input[required], .input-group textarea[required], .input-group select[required]').trigger('change');


    });
    */







    function createDialogClass(title,type){

        return dcl(_CIDialog,{

        });

        return dcl(_Dialog, {
            type: type || types.DIALOG_TYPE.INFO,
            size: types.DIALOG_SIZE.SIZE_WIDE,
            cssClass:'bootstrap3-dialog CIDialog',
            bodyCSS: {
                'height':'auto',
                'min-height':'auto'
            },
            failedText:' Failed!',
            successText:': Success!',
            showSpinner:true,
            spinner:'  <span class="fa-spinner fa-spin"/>',
            notificationMessage:null,
            cisView:null,
            onOk:function(){

                var cis = this.cisView.cis;


                console.log('final',utils.toOptions(cis));


            },
            onCIValueChanged:function(ci,newValue,oldValue){

                console.log('ci changed');
            },
            onShow:function(dlg){


                var cisView = this.cisView,
                    self = this;

                this.cisView.startup();

                cisView._on('valueChanged',function(e){
                    self.onCIValueChanged(e.ci, e.newValue, e.oldValue);
                })

                this.startDfd.resolve();
            },
            onReady:function(){
                console.log('dlg on ready2');
            },
            message:function(dlg){
                var thiz = dlg.owner;
                var cisView = thiz.initWithCIS(thiz.cis);
                thiz.cisView = cisView;
                return $(cisView.domNode);

            },
            initWithCIS: function (cis) {

                var viewArgs = {
                    delegate: this,
                    options: {},
                    cis: cis.inputs,
                    style: 'height:inherit',
                    tabContainerStyle: 'height:inherit'

                };
                utils.mixin(viewArgs, this.viewArgs);
                var view = utils.addWidget(CIView, viewArgs, this.containerNode, null, false);
                this.add(view,null,false);
                return view;
            }

        });
    }


    function runAction(_action){

        var action  = this.getAction(_action);

        console.error('run action : ' + action.command);

        switch (action.command){

            case ACTION_TYPE.NEW_DIRECTORY:{
                return mkdir.apply(this,[]);
            }
            case ACTION_TYPE.CLIPBOARD_PASTE:{
                clipboardPaste.apply(this,[]);
                return;
            }
        }
    }

    function clipboardPaste(){


        var isCut = this.currentCutSelection,
            items = isCut ? this.currentCutSelection : this.currentCopySelection,
            serverParams = this._buildServerSelection(items),
            serverFunction = isCut ? 'moveItem' : 'copyItem',
            self = this;


        if(!items){
            console.error('abort copy');
            return;
        }





        var options = defaultCopyOptions();

        console.error('paste : ' + serverParams.dstPath,serverParams);

        self.ctx.getFileManager()[serverFunction](serverParams.selection, serverParams.dstPath, {
            include: options.includes,
            exclude: options.excludes,
            mode: options.mode,
            hints: serverParams.hints
        }).then(function (data) {


            /*
            self.runAction(types.ACTION.RELOAD).then(function(){

            });
            */

            //thiz.didFileOperation(types.OPERATION.COPY, view, serverParams.selection, serverParams.store);


        },function(err){
            console.error('pasted err!',err);
        },function(progress){
            console.error('paste progress',progress);
        });

        return true;
    }

    function mkdir(){


        var dlgClass = createDialogClass('test');





        var dfd = new Deferred();


        var self = this,
            currentItem = this.getSelectedItem(),
            currentFolder = this.getCurrentFolder(),
            startValue = currentItem ? utils.pathinfo(currentItem.path,types.PATH_PARTS.ALL).filename : '',
            collection = this.collection;


        var CIS = {
            inputs:[
                utils.createCI('Name',13,startValue,{
                    widget:{
                        instant:true,
                        validator:function(value){

                            return collection.getSync(currentFolder.path + '/' + value) == null &&
                                value.length >0;

                        }
                    }
                })
            ]
        }

        var dlg = new dlgClass({
            cis:CIS,
            title:'Create new Directory',
            ctx:this.ctx,
            _onError: function (title, suffix, message) {

                title = title || this.title;

                message = message || this.notificationMessage;

                message && message.update({
                    message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                    type: 'error',
                    actions: false,
                    duration: 15000
                });

                this.onError && this.onError(suffix);

            },
            onCancel:function(){

                dfd.resolve();
            },
            onOk:function(){

                var val= this.getField('name');

                if(val==null){
                    dfd.resolve();
                    return;
                }


                var currentFolder = self.getCurrentFolder(),
                    newFolder = currentFolder.path +'/'+val;


                var fileDfd = self.ctx.getFileManager().mkdir(collection.mount, newFolder ,{
                    checkErrors:true,
                    returnProm:false,
                    __onError:function(err){
                        thiz._onError(null,err.message);
                    }
                });

                fileDfd.then(function(data){
                    self.runAction(ACTION.RELOAD).then(function(){
                        dfd.resolve();
                        var _newItem = collection.getSync(newFolder);
                        if(_newItem){
                            self.deselectAll();

                            self.select(newFolder,null,true,{
                                append:false,
                                focus:true
                            })
                        }
                    });

                },function(e){
                    console.error('__error creating file!',e);
                    dfd.resolve();
                })


            }
        });

        dlg.show().then(function(){
            console.error('ready');
        });

        return dfd;

    }

    function doTests(tab,grid){




        grid.refresh().then(function(){
            grid.select([0]).then(function(sel){


                grid.runAction(ACTION_TYPE.EDIT).then(function(){
                    grid.deselectAll();

                    grid.select([1],null,true,{
                        append:false,
                        delay:0
                    }).then(function() {
                        grid.runAction(ACTION_TYPE.NEW_DIRECTORY);
                    });
                });

            });

        })

    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {

        var parent = TestUtils.createTab(null,null,module.id);

        var grid = FTestUtils.createFileGrid('root',
            //args
            {


            },

            //overrides
            {
                reload:function(item){





                    var dfd = new Deferred();

                    item = item || this.getRows()[0];

                    //item could be a non-store item:
                    var cwd = item.getParent ? item.getParent() : this.getCurrentFolder(),
                        self = this;

                    console.log('reload cwd'  + cwd.path);

                    this.collection.loadItem(cwd,true).then(function(what){

                        console.error('load-item done ' + cwd.path);
                        self.refresh().then(function(){
                            //self.openItem(cwd,true);
                            dfd.resolve(what);
                        });
                    });

                    return dfd;
                },
                runAction:function(action) {
                    //return runAction.apply(this,[action]);
                    var res = this.inherited(arguments);

                    var _resInner = runAction.apply(this,[action]);

                    return _resInner || res;
                }

            },'TestGrid',module.id,true,parent);

        doTests(parent,grid);

        return declare('a',null,{});

    }

    return Grid;

});