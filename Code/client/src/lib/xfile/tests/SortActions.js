/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/on',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/ListRenderer',
    'xgrid/ThumbRenderer',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xide/data/Model',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xfile/data/Store',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xfile/views/FileGrid',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    'xgrid/Keyboard',
    'xfile/tests/ThumbRenderer',
    'dojo/dom-construct',
    'xgrid/Renderer',
    'xfile/model/File',
    'xide/widgets/TemplatedWidgetBase'

], function (declare,on,types,
             utils,factory,ListRenderer, ThumbRenderer, TreeRenderer,
             Trackable, ObservableStore, Model, _ActionMixin,
             Grid, Store, MultiRenderer, RadioButton, FileGrid, Ribbon, Registry, DefaultActions, Action,Keyboard,ThumbRenderer2,domConstruct,Renderer,File,TemplatedWidgetBase) {





    /***
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;



    var breadCrumbClass = declare('breadcrumb',[TemplatedWidgetBase],{

        buildRendering:function() {

            this.inherited(arguments);

            var grid = this,
                node = grid.domNode.parentNode;

            this._on('onAddActions', function (evt) {

                var actions = evt.actions,
                    permissions = evt.permissions;

                var _action = grid.createAction('Breadcrumb', types.ACTION.BREADCRUMB, types.ACTION_ICON.BREADCRUMB, null , 'View', 'Show', 'item|view', null, null, null, null, null, permissions, node, grid);
                if (!_action) {
                    return;
                }
                actions.push(_action);
            });

        },
        templateString:"<ul data-dojo-attach-point='root' class='breadcrumb'></ul>",
        /**
         * Event delegation
         * @param label
         * @param e
         * @param el
         */
        onClick:function(label,e,el){
            this._emit('click',{
                element:el,
                event:e,
                label:label,
                data:el.data
            });
        },
        /**
         * Add a new breadcrumb item
         * @param label
         * @param active
         * @param data
         * @returns {*|jQuery|HTMLElement}
         */
        add:function(label,active,data){

            if(data && this.has(data)){
                console.log('already have ' + data.path);
                return null;
            }

            var _class = active ? 'active' : '',
                self = this,
                el = $("<li class='" +_class + "'><a href='#'>" + label + "</a></li>");

            //clear active state of all previous elements
            _.each(this.all(),function(e){
                e.el.removeClass('active');
            });

            $(this.domNode).append(el);

            el.on('click',function(e){
                self.onClick(label,e,el);
            });
            el[0].data = data;

            return el;
        },
        /**
         * Remove last breadcrumb item
         */
        pop:function(){
            this.last().remove();
        },
        /**
         * Return last breadcrumb item
         * @returns {*|jQuery}
         */
        last:function(){
            return $(this.root).children().last();
        },
        /**
         * Returns true when data is found
         * @param data
         * @returns {boolean}
         */
        has:function(data){

            var all = this.all();
            for (var i = 0; i < all.length; i++) {
                if(all[i].data.path === data.path){
                    return true;
                }
            }
            return false;
        },
        /**
         * Return all breadcrumb items
         * @returns {Array}
         */
        all:function(){

            var result = [];

            _.each($(this.root).children(),function(el){
                result.push({
                    el:$(el),
                    label:el.outerText,
                    data:el.data
                })
            });

            return result;

        },
        /**
         * Removes all breadcrumb items from back to beginning, until data.path matches
         * @param data
         */
        unwind:function(data){
            var rev = this.all().reverse(),
                self = this;

            _.each(rev,function(e){
                if(e.data.path !==data.path && e.data.path!=='.'){
                    self.pop();
                }
            });
        }
    });

    function createBreadCrumb(grid){

        var mainView = ctx.mainView;

        //var target = grid.statusbar ? grid.statusbar.parentNode : grid.header;

        var target = grid.header;
        var widget = utils.addWidget(breadCrumbClass,{},null,target,true);
        var cwd = grid.getCurrentFolder();

        var sourceLabel = (grid.selectedSource || 'Root' ) + '://';

        sourceLabel = '';

        var rootLabel = sourceLabel + (cwd ? cwd.name : '.');

        //console.log('cwd',rootLabel);

        widget.add(rootLabel,true,cwd);
        widget._on('click',function(e){
            var data = e.element[0].data;
            widget.unwind(data);
            grid.openFolder(data);

        });
        grid._on('openFolder',function(evt){

            var isBack = evt.back,
                item = evt.item;

            if(isBack){
                widget.pop();
            }else{
                item.name!=='.' && widget.add(item.name, true, item);
            }
        });
    }



    function testMain(grid){

        //console.clear();
        //grid.getRightPanel('test',0.5);
        //createBreadCrumb(grid);

        //download('test.js','asdfasdfsdf');

    }

    function createThumbRenderClass(){
        /**
         * The list renderer does nothing since the xgrid/Base is already inherited from
         * dgrid/OnDemandList and its rendering as list already.
         *
         * @class module:xgrid/ThumbRenderer
         * @extends module:xgrid/Renderer
         */
        //package via declare

        var _class = declare('xfile.ThumbRendererLarge',[Renderer],{

            _getLabel:function(){ return "Thumb"; },
            _getIcon:function(){ return "el-icon-th-large"; },
            sizeClass:'',
            startup:function(){


                if(this._started){
                    return;
                }
                var thiz = this;
            },
            thumbSize: "100",
            resizeThumb: true,
            _doubleWidthThumbs:true,
            deactivateRenderer:function(){

                $(this.domNode.parentNode).removeClass('metro');
                //$(this.domNode.parentNode).removeClass('container');


                $(this.domNode).css('padding','');/*
                $(this.contentNode).css('margin-top','0');*/


            },
            activateRenderer:function(){

                $(this.domNode.parentNode).addClass('metro');
                $(this.contentNode).css('padding','8px');


                /*
                $(this.domNode.parentNode).addClass('container');
                $(this.contentNode).css('padding','8px');
                $(this.contentNode).css('margin-top','16px');
                */
                //this._showHeader(false);

            },
            /**
             * Override renderRow
             * @param obj
             * @returns {*}
             */
            renderRow: function (obj) {
                var thiz = this,
                    div = domConstruct.create('div', {
                        className: "tile widget"
                    }),
                    icon = obj.icon,
                    no_access = obj.read === false && obj.write === false,
                    isBack = obj.name == '..',
                    directory = obj && !!obj.directory,
                    useCSS = false,
                    label = '',
                    imageClass = 'fa fa-folder fa-5x',
                    isImage = false,
                    sizeClass = this.sizeClass;


                this._doubleWidthThumbs = true;


                var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;';
                var contentClass = 'icon';



                if (directory) {

                    if (isBack) {
                        imageClass = 'fa fa-level-up fa-5x thumbIcon';
                        useCSS = true;
                    } else if (!no_access) {
                        imageClass = 'fa fa-folder fa-5x thumbIcon';
                        useCSS = true;
                    } else {
                        imageClass = 'fa fa-lock fa-5x thumbIcon';
                        useCSS = true;
                    }

                } else {

                    imageClass = 'itemIcon';

                    if (no_access) {

                        imageClass = 'fa fa-lock fa-5x thumbIcon';

                        useCSS = true;
                    } else {

                        if (utils.isImage(obj.path)) {

                            var url = this.getImageUrl(obj);
                            if (url) {
                                obj.icon = url;
                            } else {
                                obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                            }

                            imageClass = 'imageFile';

                        } else {
                            imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                            useCSS = true;
                        }
                    }

                }

                label = obj.name;

                var folderContent =  '<span style="' + iconStyle + '" class="fa '+imageClass +' thumbIcon"></span>';

                if (utils.isImage(obj.path)) {

                    var url = this.getImageUrl(obj);
                    if (url) {
                        obj.icon = url;
                    } else {
                        obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                    }

                    imageClass = '';
                    contentClass = 'image';
                    //folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';
                    folderContent = '<div style="padding:1px" class="tile-content image">' +
                        '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                        '</div>';

                    useCSS = true;
                    isImage = true;


                }


                //var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);left:40px;text-align:left;font-size: 72px;margin-top:-45px;opacity: 0.7';


                var html = '<div class="tile-content ' + contentClass +'">'+
                    folderContent +
                    '</div>'+

                    '<div class="brand bg-dark opacity">'+
                        '<span class="iconText text bg-dark opacity fg-white" style="">'+
                    label +
                    '</span>'+
                    '</div>';


                if(isImage || this._doubleWidthThumbs){
                    $(div).addClass(sizeClass);
                }

                if (useCSS) {
                    div.innerHTML = html;
                    return div;
                }


                if (directory) {

                    //div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name">' + obj.name + '</div>';


                    div.innerHTML = html;


                } else {
                    div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
                }
                return div;

            },
            getImageUrl: function (item) {

                var fileManager = this.ctx.getFileManager();
                if (fileManager && fileManager) {
                    var params = null;
                    if (this.resizeThumb) {
                        params = {
                            width: this.thumbSize
                        }
                    }
                    return fileManager.getImageUrl(item, null, params);
                }
                return null;
            }
        });

        return _class;
    }


    function getFileActions(permissions) {





        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();

        permissions = permissions || this.permissions;

        function addAction(label,command,icon,keycombo,tab,group,filterGroup,onCreate,handler,mixin,shouldShow,shouldDisable){

            var action = null;
            if(DefaultActions.hasAction(permissions,command)){

                mixin = mixin || {};

                utils.mixin(mixin,{owner:this});

                if(!handler){

                    handler = function(action){
                        console.log('log run action',arguments);
                        var who = thiz;
                        if(who.runAction){
                            who.runAction.apply(who,[action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label,command,icon,keycombo,tab,group,filterGroup,onCreate,handler,mixin,shouldShow,shouldDisable,thiz.domNode);

                result.push(action);
                return action;

            }
        }

        function shouldDisableDefaultFileExtension(selection,reference,visibility){

            selection = thiz.getSelection();

            //console.log('selection',selection);

            if(selection==null){
                console.error('sel is nukk');
            }
            if(!selection || !selection.length || selection[0].isDir==true){
                return true;
            }

            var item   = selection[0],
                path = item.getPath();
            if(!path.match(/^(.*\.(?!(jpg|png)$))?[^.]*$/i)) {
                return false;
            }
            return true;
        }



        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////


        //addAction('Go Up',ACTION_TYPE.GO_UP,ACTION_ICON.GO_UP,['alt up','backspace'],'Home','Navigation','item|view',null,null,null,null,null);

        /*
        result.push(this.createAction('Source', 'View/Source','fa-hdd-o', ['f4'],'Home','Navigation','item',null,
            function () {},
            {
                addPermission:true,
                tab: 'Home',
                dummy:true
            },null,null,permissions,thiz.domNode,thiz
        ));
        */

        return result;



    }

    function createStore(mount) {

        var storeClass = declare.classFactory('fileStore', [Store, Trackable, ObservableStore]);
        //var MyModel = declare(Model, {});
        var config = types.config;

        var options = {
            fields: types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME
        };

        var store = new storeClass({
            idProperty: 'path',
            Model: File,
            data: [],
            config: config,
            url: types.config.FILE_SERVICE,
            serviceUrl: types.config.FILE_SERVICE,
            serviceClass: types.config.FILES_STORE_SERVICE_CLASS,
            mount: mount,
            options: options
        });

        store._state = {
            initiated: false,
            filter: null,
            filterDef: null
        };

        store.reset();
        store.config = config;
        store.setData([]);
        store.init();

        return store;
    }

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var fileStore = createStore('root');

            var thumb = createThumbRenderClass();


            var renderers = [ListRenderer, thumb , TreeRenderer];
            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var mainView = ctx.mainView;

            var docker = mainView.getDocker();
            if(window._lastGrid){
                docker.removePanel(window._lastGrid);
            }
            parent = docker.addTab(null, {
                title: 'Documentation',
                icon: 'fa-folder'
            });

            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon,
                store = fileStore;




            //var gridclass = declare('File2', [breadCrumbClass,FileGrid],{});





            var _gridClass = declare('File2', FileGrid, {
                //selectedRenderer:thumb,
                selectedSource:'Root',
                renderers:renderers,
                runAction:function(action){

                    if(action.command=='View/Download Data'){
                        //console.dir([this,this.collection]);
                        utils.download(JSON.stringify(this.getData()));
                    }
                    return this.inherited(arguments);
                },
                startup:function(){

                    if(this._started){
                        return;
                    }

                    this.inherited(arguments);

                    var thiz = this;


                    this._on('onAddActions', function (evt) {

                        var actions = evt.actions,
                            permissions = evt.permissions,
                            container = thiz.domNode;

                        actions.push(thiz.createAction('Large Icons', 'View/Icon Size', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Layout', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                        actions.push(thiz.createAction('Download Data', 'View/Download Data', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Save', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                    });
                }
            });


            /**
             *
             * @param item
             * @param where
             * @param mixin
             * @param select
             * @returns {*}
             */
            grid = new _gridClass({
                //selectedRenderer: thumb,
                renderers : [ListRenderer, thumb , TreeRenderer],
                collection: store.getDefaultCollection(),
                showHeader: true,
                _parent: parent
            }, parent.containerNode);
            grid.startup();

            //grid.onContainerClick();
            window._lastGrid = parent;
            var _actions = [];

            var _defaultActions = DefaultActions.getDefaultActions(_actions, grid);
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid));
            _defaultActions = _defaultActions.concat(grid.getFileActions(_actions));
            grid.addActions(_defaultActions);

            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();

            if (!toolbar) {
                window._lastRibbon = ribbon = toolbar = utils.addWidget(Ribbon, {
                    store: actionStore,
                    flat: true,
                    currentActionEmitter: grid
                }, this, mainView.layoutTop, true);

            } else {
                toolbar.addActionEmitter(grid);
            }

            toolbar.setActionEmitter(grid);


            setTimeout(function () {
                mainView.resize();
                grid.resize();

            }, 1000);


            var copy = actionStore.getSync('Edit/Copy');

            var c1 = actionStore.getSync('View/Columns/Show Path');


            function test() {

                testMain(grid);
                try {

                } catch (e) {

                }


                return;


            }

            function test2() {
                return;
            }

            setTimeout(function () {
                test();
            }, 2000);
            setTimeout(function () {
                test2();
            }, 4000);

        }
    }

    return Grid;

});