/** @module xfile/tests/TestUtils **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xfile/data/Store',
    'xfile/model/File',
    'xide/tests/TestUtils',
    'xfile/views/FileGrid'

], function (declare, types, utils,Trackable, ObservableStore, Store, File, TestUtils, FileGrid) {

    var ctx = window.sctx;
    /**
     *
     * @param mount
     * @returns {*}
     */
    function createStore(mount,storeOptionsMixin,mixin) {

        var ctx = window.sctx;
        var storeClass = declare.classFactory('fileStore', [Store, Trackable, ObservableStore]);
        var config = types.config;

        var options = {
            fields: types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME |
            types.FIELDS.SHOW_MEDIA_INFO
        };

        utils.mixin(options,storeOptionsMixin);

        var store = new storeClass(utils.mixin({
            data: [],
            config: config,
            url: types.config.FILE_SERVICE,
            serviceUrl: types.config.FILE_SERVICE,
            serviceClass: types.config.FILES_STORE_SERVICE_CLASS,
            mount: mount,
            options: options
        },mixin));

        store._state = {
            initiated: false,
            filter: null,
            filterDef: null
        };

        store.reset();
        store.config = config;
        store.setData([]);
        store.init();

        return store;
    }

    /**
     * Create default file grid
     * @param mount
     * @param args
     * @param tabTitle
     * @param gridClass
     * @param globalVariable
     * @returns {module:xfile/views/FileGrid}
     */

    function createFileGrid(mount, args, gridClass, tabTitle, globalVariable, register,parent,storeOptionsMixin,storeMixin,startup) {
        var ctx = window.sctx;
        args = args || {};

        var store = args.collection || createStore(mount || 'root',storeOptionsMixin,storeMixin);

        parent = parent || TestUtils.createTab(tabTitle, null, globalVariable);

        var GridClass = FileGrid;
        if(gridClass){
            if(gridClass._meta){
                GridClass = gridClass;
            }else{
                GridClass = declare('xfile.views.FileGrid', FileGrid, gridClass)
            }
        }

        args = utils.mixin({
            collection: store.getDefaultCollection(),
            _parent: parent
        }, args || {});

        var grid = utils.addWidget(GridClass, args, null, parent, true, null, null, true, null);
        if (register) {
            ctx.getWindowManager().registerView(grid,false);
        }
        return grid;
    }

    var module = declare('xfile.tests.TestUtils', null, {});
    module.createStore = createStore;
    module.createFileGrid = createFileGrid;
    module.FileGrid=FileGrid;
    return module;

});