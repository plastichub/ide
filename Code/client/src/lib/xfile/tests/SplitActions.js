/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/on',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/ListRenderer',
    'xgrid/ThumbRenderer',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xide/data/Model',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xfile/data/Store',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xfile/views/FileGrid',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    'xgrid/Keyboard',
    'xfile/tests/ThumbRenderer',
    'dojo/dom-construct',
    'xgrid/Renderer',
    'xfile/model/File',
    'xide/widgets/TemplatedWidgetBase',
    'dijit/registry'

], function (declare,on,types,
             utils,factory,ListRenderer, ThumbRenderer, TreeRenderer,
             Trackable, ObservableStore, Model, _ActionMixin,
             Grid, Store, MultiRenderer, RadioButton, FileGrid, Ribbon, Registry, DefaultActions, Action,Keyboard,ThumbRenderer2,domConstruct,Renderer,File,TemplatedWidgetBase,registry) {





    /***
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;





    function testMain(grid,panel){


        console.clear();


        grid._on('selectionChanged',function(e){

            console.log('selectionChanged ',e);
            var _s = e.selection;
            if(_s){
                _.each(_s,function(item){

                    if(item.name =='grunt'){
                        //debugger;
                    }
                })
            }
        });


        /*
        {
            var mainView = ctx.mainView;
            var docker = mainView.getDocker();
            var layout = utils.getJson(docker.save());
            //var layout = docker.save();

            console.error('save docker:');

            console.dir(layout);
            setTimeout(function () {
                docker.restore(docker.save());
            }, 3000);

        }
        */

    }

    function getFileActions(permissions) {

        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();

        permissions = permissions || this.permissions;

        return result;
    }

    function createStore(mount) {

        var storeClass = declare.classFactory('fileStore', [Store, Trackable, ObservableStore]);
        //var MyModel = declare(Model, {});
        var config = types.config;

        var options = {
            fields: types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME
        };

        var store = new storeClass({
            idProperty: 'path',
            Model: File,
            data: [],
            config: config,
            url: types.config.FILE_SERVICE,
            serviceUrl: types.config.FILE_SERVICE,
            serviceClass: types.config.FILES_STORE_SERVICE_CLASS,
            mount: mount,
            options: options
        });

        store._state = {
            initiated: false,
            filter: null,
            filterDef: null
        };

        store.reset();
        store.config = config;
        store.setData([]);
        store.init();

        return store;
    }

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var fileStore = createStore('root');

            var renderers = [ListRenderer,TreeRenderer];

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var mainView = ctx.mainView;

            var docker = mainView.getDocker();
            if(window._lastGrid){
                docker.removePanel(window._lastGrid);
            }

            parent = docker.addTab(null, {
                title: 'Documentation',
                icon: 'fa-folder'
            });

            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon,
                store = fileStore;

            var _gridClass = declare('xfile.views.FileGrid', FileGrid, {

                selectedSource:'Root',
                renderers:renderers,
                runAction:function(action){

                    if(action.command=='View/Download Data'){
                        //console.dir([this,this.collection]);
                        utils.download(JSON.stringify(this.getData()));
                    }
                    return this.inherited(arguments);
                },
                startup:function(){

                    if(this._started){
                        return;
                    }

                    this.inherited(arguments);

                    var thiz = this;


                    this._on('onAddActions', function (evt) {

                        var actions = evt.actions,
                            permissions = evt.permissions,
                            container = thiz.domNode;

                        actions.push(thiz.createAction('Large Icons', 'View/Icon Size', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Layout', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                        actions.push(thiz.createAction('Download Data', 'View/Download Data', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Save', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                    });
                }
            });



            ////////////////////////////////////
            //
            //

            grid = new _gridClass({
                collection: store.getDefaultCollection(),
                showHeader: true,
                _parent: parent
            }, parent.containerNode);
            grid.startup();

            window._lastGrid = parent;
            var _actions = [];

            var _defaultActions = DefaultActions.getDefaultActions(_actions, grid);
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid));
            _defaultActions = _defaultActions.concat(grid.getFileActions(_actions));
            grid.addActions(_defaultActions);

            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();
            toolbar.setActionEmitter(grid);



            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {
                testMain(grid,parent);
                return;


            }
            setTimeout(function () {
                test();
            }, 2000);



        }
    }

    return Grid;

});