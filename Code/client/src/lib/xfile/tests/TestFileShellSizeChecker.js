/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",

    "xide/mixins/EventedMixin",
    "xide/mixins/PersistenceMixin",
    "xide/tests//TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",
    "dojo/cookie",
    "dojo/dom-construct",
    "dojo/_base/lang",
    "dojo/string",
    "xide/_base/_Widget",
    'xide/views/ConsoleView',
    'xide/views/ConsoleKeyboardDelegate',
    'xide/views/History',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'xide/layout/_TabContainer',
    'xide/views/_Console',
    'xide/views/_ConsoleWidget'

], function (declare, dcl, types,
             utils, Grid, TemplatedWidgetBase, EventedMixin,
             PersistenceMixin,
             TestUtils, FTestUtils, _Widget, registry, module,
             cache, cookie, domConstruct, lang, string, _XWidget, ConsoleView,
             ConsoleKeyboardDelegate, History, ACEEditor, Editor, _TabContainer,
             Console, ConsoleWidget) {



    console.clear();
    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;



    /***
     * Default editor persistence for peferences in cookies!
     **/
    function createConsoleAceWidgetClass() {

        return dcl([ConsoleWidget, PersistenceMixin.dcl], {
            createEditor: function () {
                var editor = createEditor(this.consoleParent, this.value, this, {
                    options: this.options
                });
                return editor;

            }
        });
    }

    function createEditor(root, value, owner, mixin) {

        var item = {
            filePath: '',
            fileName: ''
        };
        var title = "No Title";


        var args = {
            _permissions: [],
            item: item,
            value: value,
            style: 'padding:0px;top:0 !important',
            iconClass: 'fa-code',
            options: utils.mixin(mixin, {
                filePath: item.path,
                fileName: item.name
            }),
            ctx: ctx,
            /***
             * Provide a text editor store delegate
             */
            storeDelegate: {},
            title: title
        };


        utils.mixin(args, mixin);

        var editor = utils.addWidget(Editor, args, owner, root, true, null, null, false);

        editor.resize();

        return editor;
    }
    function createShellViewDelegate() {

        return dcl(null, {

            owner: null,
            onServerResponse: function (theConsole, data, addTimes) {


                if (theConsole && data && theConsole.owner && theConsole.owner.onServerResponse) {
                    theConsole.owner.onServerResponse(data, addTimes);
                }
            },
            runBash: function (theConsole, value, cwd) {


                var thiz = this;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['sh', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole,response, false);
                });
            },

            runPHP: function (theConsole, value, cwd) {
                var thiz = this;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['php', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole, response, false);
                });

                
            },
            runJavascript: function (theConsole, value, context, args) {

                var _function = new Function("{" + value + "; }");
                var response = _function.call(context, args);
                if (response != null) {
                    //console.error('response : ' + response);
                    this.onServerResponse(theConsole, response);
                    return response;
                }
                return value;
            },
            onConsoleCommand: function (data, value) {


                var thiz = this,
                    theConsole = data.console;


                if (theConsole.type === 'sh') {

                    value = value.replace(/["'`]/g, "");



                    thiz.onServerResponse(theConsole,"<pre style='font-weight: bold'># " + value + "</pre>", true);

                    var dstPath = null;

                    if (this.owner && this.owner.getCurrentFolder) {
                        var cwd = this.owner.getCurrentFolder();
                        if (cwd) {
                            dstPath = utils.buildPath(cwd.mount, cwd.path, false);
                        }
                    }
                    console.log('run bash in ' + dstPath);
                    if (theConsole.isLinked()) {
                        //dstPath = this.getCurrentPath();
                    }
                    return this.runBash(theConsole, value, dstPath);
                }

                if (theConsole.type === 'php') {

                    value = value.replace(/["'`]/g, "");

                    var dstPath = null
                    if (theConsole.isLinked()) {
                        dstPath = this.getCurrentPath();
                    }
                    return this.runPHP(theConsole, value, dstPath);
                }

                if (theConsole.type === 'javascript') {
                    return this.runJavascript(theConsole, value);
                }
            },
            onConsoleEnter: function (data, input) {
                return this.onConsoleCommand(data, input);
            }
        });
    }
    function createShellViewClass() {

        return Module = dcl(Console, {
            lazy: true,
            consoleClass: createConsoleAceWidgetClass(),
            getServer: function () {
                return this.server || ctx.fileManager;
            },
            log: function (msg, addTimes) {

                //console.error('log');

                utils.destroy(this.progressItem);

                var out = '',
                    isHTML = false;
                if (_.isString(msg)) {

                    if(msg.indexOf('<body')!=-1 || /<[a-z][\s\S]*>/i.test(msg)) {

                        console.error('isHTML');
                        isHTML = true;
                        out = msg;

                    }else{
                        out += msg.replace(/\n/g, '<br/>');
                    }

                } else if (_.isObject(msg) || lang.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var thiz = this;
                if(isHTML){

                    dst.appendChild(dojo.create("div", {
                        className: 'html_response',
                        innerHTML: out

                    }));

                    last = dst.appendChild(dojo.create("div", {
                        innerHTML: '&nbsp;',
                        style:'height:1px;font-size:1px'
                    }));


                }else {

                    for (var i = 0; i < items.length; i++) {
                        var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');

                        var item = items[i];
                        if (!item || !item.length) {
                            continue
                        }
                        last = dst.appendChild(dojo.create("div", {
                            className: _class,
                            innerHTML: this._toString(items[i], addTimes)

                        }));
                        this.lastIndex++;
                    }
                }

                if (last) {
                    last.scrollIntoViewIfNeeded();
                }
            }
        });

        var Module = dcl([_XWidget], {
            templateString: '<div attachTo="containerNode" class="widget" style="height: 100%;width: 100%;">' +
            '<div attachTo="logView" style="overflow: auto"></div></div>',
            value: "return 2;",
            resizeToParent: true,
            serverClass: 'XShell',
            consoleClass: createConsoleAceWidgetClass(),
            server: null,
            showProgress: false,
            jsContext: null,
            onButton: function () {
                var dst = this.getLoggingContainer();
                if (dst) {
                    dojo.empty(dst);
                }
            },
            onConsoleExpanded: function () {
                this._resizeLogView();
            },
            _resizeLogView: function () {
                if (this.console) {
                    var total = this.$containerNode.height();
                    var consoleH = $(this.console.domNode).height();
                    $(this.logView).height(total - consoleH + 'px');
                }
            },
            resize: function () {

                console.log('resize shell view');
                utils.resizeTo(this, this._parent);
                this._resizeLogView();
                return this.inherited(arguments);

            },
            _scrollToEnd: function () {


                var thiz = this;
                setTimeout(function () {
                    var container = thiz.getLoggingContainer();
                    container.lastChild.scrollIntoViewIfNeeded();
                }, 10);

                return;


            },
            onServerResponse: function (data, addTimes) {

                var container = this.getLoggingContainer();
                container.children.length > 100 && dojo.empty(container);
                this._resizeLogView();
                this.log(data, addTimes);
                this._scrollToEnd();

            },
            getLoggingContainer: function () {
                return this.logView;
            },
            onEnter: function (value, print) {

                console.log('on enter');
                if (this.showProgress) {
                    this.progressItem = this.createLogItem(value, this.getLoggingContainer());
                }


                var _resolved = '';

                if (this.delegate.onConsoleEnter) {
                    _resolved = this.delegate.onConsoleEnter({
                        view: this,
                        console: this.console
                    }, value, print);
                }


                if (this.showLastInput) {
                    var dst = this.getLoggingContainer();

                    print !== false && dst.appendChild(dojo.create("div", {
                        innerHTML: '# ' + (_resolved || value),
                        className: 'widget'
                    }));

                }
            },
            getServer: function () {
                return this.server || ctx.fileManager;
            },
            _toString: function (str, addTimes) {
                if (addTimes !== false) {
                    return this.addTime(str);
                } else {
                    return str;
                }
            },
            addTime: function (str) {
                return moment().format("HH:mm:SSS") + ' ::   ' + str + '';
            },
            log: function (msg, addTimes) {

                utils.destroy(this.progressItem);
                var out = '';
                if (_.isString(msg)) {
                    out += msg.replace(/\n/g, '<br/>');
                } else if (_.isObject(msg) || lang.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }
                ;

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var thiz = this;

                for (var i = 0; i < items.length; i++) {
                    var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');

                    var item = items[i];
                    if (!item || !item.length) {
                        continue
                    }
                    last = dst.appendChild(dojo.create("div", {
                        className: _class,
                        innerHTML: this._toString(items[i], addTimes)

                    }));
                    this.lastIndex++;
                }

                if (last) {
                    last.scrollIntoViewIfNeeded();
                }

                setTimeout(function () {
                    thiz._scrollToEnd();
                }, 10);


            },
            startup: function () {
                if (this._started) {
                    return;
                }
                this.createWidgets();
            },
            createWidgets: function () {

                this.console = utils.addWidget(this.consoleClass, {
                    style: 'width:inherit',
                    delegate: this,
                    type: this.type,
                    owner: this,
                    className: 'consoleWidget',
                    value: this.value
                }, this, this.containerNode, true);

                this.console.startup();
                this.add(this.console, null, false);
            }
        });
        return Module;
    }
    function addActions(permissions) {

        //	adds 3 shells
        var target = ctx,
            source = this,
            result = [],
            shells = [
                ["Javacript", "fa-terminal", 'javascript'],
                ["Bash-Shell", "fa-terminal", 'sh'],
                ["PHP-Shell", "fa-code", 'php']
            ];


        function createShell(action, target, args) {
            console.error('create shell');
            var parent = target || TestUtils.createTab(null, null, module.id);
            var viewClass = createShellViewClass();
            var view = new viewClass(args);

            return view;
            //return createShellView(parent, action.shellType);
        }


        function mkAction(title, icon, type, owner, handler) {

            var _action =
            {
                label: title,
                command: "Window/" + title,
                icon: icon || 'fa-code',
                tab: "Home",
                group: 'View',
                handler: handler || createShell,
                mixin: {
                    addPermission: true,
                    shellType: type
                },
                owner: owner || source
            };
            result.push(source.createAction(_action));
        }
        for(var i = 0 ; i < shells.length ; i++){
            mkAction.apply(source,shells[i]);
        }



        target.addActions(result);

        return result;
    }


    function onOpenStatusPanel(panel){

        this._on('openedFolder',function(data){
            paint();
        })

        var self = this,
            display = null,
            vis = null;

        this._on('selectionChanged',function(evt){

            if(evt.why=='clear'){
                return;
            }
            var selection = evt.selection;
            var first = selection ? selection[0] : null;
            var row = self.row(first);
            var element = row ? row.element : null;
            if(!first || !element){
                return;
            }
            var path = first.path.replace('./','');
            if(!display || !display.d3Nodes){
                return;
            }
            display.select(path);
        })

        function createWidget(){

            var widget = dcl(_XWidget,{
                _find:function(path,nodes){
                    var res = _.find(nodes,{
                            path:path
                        }),
                        self = this;

                    if(res && res.children && res.children){
                        var _child = self._find(path,res.children);
                        if(_child){
                            res = _child;
                        }
                    }
                    if(!res){
                        _.each(nodes,function(node){
                            var _inner = null;
                            if(node.children && node.children.length){
                                _inner = self._find(path,node.children);
                                if(_inner){
                                    res=_inner;
                                }
                            }
                        });
                    }
                    return res;
                },
                select:function(path){
                    var d = this._find(path,this.d3Nodes);
                    if(d) {
                        this.highlight(d);
                    }

                },
                direction:'right',
                templateString:'<div class="sizeDisplay tabbable tabs-${!direction}" attachTo="containerNode" style="width: inherit;height: 100%;padding: 4px">' +

                    '<ul class="nav nav-tabs tabs-right" role="tablist">'+


                            '<li class="active">'+
                                '<a href="#_tab-content2_${!id}" data-toggle="tab">Overall</a>'+
                            '</li>'+

                            '<li>'+
                                '<a href="#_tab-content1_${!id}" data-toggle="tab">By Type</a>'+
                            '</li>'+



                    '</ul>'+
                    '<div class="body tab-content">'+

                        '<div attachTo="tab2" id="_tab-content2_${!id}" class="tab-pane active clearfix">'+

                            '<div class="chart1" attachTo="chart1" style="width: 100%;height: inherit;min-height:250px">'+
                                '<div class="" style="" id="detailInfo"></div>'+
                                '<div class="widget" id="explanation" >'+
                                    '<span id="percentage"></span><br/>'+
                                    '<span id="percentageDetail"></span>'+
                                '</div>'+
                            '</div>'+

                        '</div>'+

                        '<div attachTo="tab1" id="_tab-content1_${!id}" class="tab-pane">'+

                                '<svg class="chart2" style="width: 100%;height: inherit;min-height:250px">'+

                                '</svg>'+
                                '<div id="tooltip" class="hidden">'+
                                    '<span id="chart2Per1">A</span> <br>'+
                                    '<span id="chart2Per2">100</span>'+
                                '</div>'+

                        '</div>'+


                    '</div>'+

                '</div>',
                _templateString:'<div class="sizeDisplay" attachTo="containerNode" style="width: inherit;height: inherit;padding: 4px">' +
                '<ul class="tabs">'+

                    '<li>'+
                        '<input type="radio"  name="${!id}-tabs" id="tab1" >'+

                            '<label for="tab1">By Type</label>'+

                            '<div attachTo="tab1" id="tab-content1" class="tab-content animated fadeIn">'+
                                '<svg  id="chart2" style="width: 100%;height: 100%;min-height:300px">'+

                                '</svg>'+
                                '<div id="tooltip" class="hidden">'+
                                    '<span id="chart2Per1">A</span> <br>'+
                                    '<span id="chart2Per2">100</span>'+
                                '</div>'+

                            '</div>'+
                    '</li>'+

                    '<li>'+
                        '<input type="radio" checked name="${!id}-tabs" id="tab2" data-toggle="tab">'+

                            '<label for="tab2">Overall</label>'+
                            '<div attachTo="tab2" id="tab-content2" class="tab-content animated fadeIn">'+

                                '<div id="chart1" attachTo="chart1" style="width: 100%;height: 100%;min-height:300px">'+
                                    '<div class="" style="" id="detailInfo"></div>'+
                                    '<div class="widget" id="explanation" >'+
                                        '<span id="percentage"></span><br/>'+
                                        '<span id="percentageDetail"></span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                    '</li>'+
                '</ul>'+
                '</div>',
                data:null,
                owner:self,
                vis:null,
                d3Nodes:null,
                colors : {
                    "dir": "#5687d1",
                    /*pictures same color*/
                    "jpeg":"#9b59b6",
                    "jpg":"#9b59b6",
                    "png":"#9b59b6",
                    "gif":"#9b59b6",
                    "psd":"#9b59b6",
                    /*audios same color*/
                    "mp3":"#3498db",
                    "wav":"#3498db",
                    "wma":"#3498db",
                    /*videos same color*/
                    "wmv":"#2ecc71",
                    "3gp":"#2ecc71",
                    "mp4":"#2ecc71",
                    "plv":"#2ecc71",
                    "mpg":"#2ecc71",
                    "mpeg":"#2ecc71",
                    "mkv":"#2ecc71",
                    "rm":"#2ecc71",
                    "rmvb":"#2ecc71",
                    "mov":"#2ecc71",
                    /*office products same color*/
                    "doc":"#1abc9c",
                    "xls":"#1abc9c",
                    "ppt":"#1abc9c",
                    "docx":"#1abc9c",
                    "xlsx":"#1abc9c",
                    "pptx":"#1abc9c",
                    /*mac products same color*/
                    "pages":"#e74c3c",
                    "key":"#e74c3c",
                    "numbers":"#e74c3c",

                    "pdf": "#7b615c",
                    "epub": "#7b615c",
                    /*programming langs*/
                    "c":"#f1c40f",
                    "cpp":"#f1c40f",
                    "h":"#f1c40f",
                    "html":"#f1c40f",
                    "js":"#f1c40f",
                    "css":"#f1c40f",
                    "pl":"#f1c40f",
                    "py":"#f1c40f",
                    "php":"#f1c40f",
                    "sql":"#f1c40f",
                    "csv":"#de783b",
                    "odp":"#de783b",
                    /*zip files*/
                    "gz":"#6ab975",
                    "tar":"#6ab975",
                    "rar":"#6ab975",
                    "zip":"#6ab975",
                    "7z":"#6ab975",
                    "default": "#34495e"
                },
                totalSize : 0,
                result:{},
                overallSize:0,
                buildHierarchy:function(csv) {

                    var root = {"name": "root", "children": []};
                    for (var i = 0; i < csv.length; i++) {
                        var sequence = csv[i][0];

                        var size = +csv[i][1];
                        if (isNaN(size)) { // e.g. if this is a header row
                            continue;
                        }
                        var parts = sequence.split("/");
                        var currentNode = root;
                        for (var j = 0; j < parts.length; j++) {
                            var children = currentNode["children"];
                            var nodeName = parts[j];
                            var childNode;
                            if (j + 1 < parts.length) {
                                // Not yet at the end of the sequence; move down the tree.
                                var foundChild = false;
                                for (var k = 0; k < children.length; k++) {
                                    if (children[k]["name"] == nodeName) {
                                        childNode = children[k];
                                        foundChild = true;
                                        break;
                                    }
                                }
                                // If we don't already have a child node for this branch, create it.
                                if (!foundChild) {
                                    childNode = {"name": nodeName, "type": "dir", "children": [],"path":sequence};
                                    children.push(childNode);
                                }
                                currentNode = childNode;
                            } else {
                                var filetype = nodeName.split('.').pop();
                                if (filetype.length < 7) {
                                    if (filetype in this.result) {
                                        this.result[filetype] += size;
                                    }
                                    else {
                                        this.result[filetype] = size;
                                    }
                                }
                                childNode = {"name": nodeName, "type": filetype, "size": size,"path":sequence};

                                this.overallSize += size;
                                children.push(childNode);
                            }
                        }
                    }
                    return root;
                },
                bytesToSize:function(bytes) {
                    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                    if (bytes == 0) return 'n/a';
                    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                    if (i == 0) return bytes + ' ' + sizes[i];
                    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
                },
                getAncestors:function(node) {
                    var path = [];
                    var current = node;
                    while (current.parent) {
                        path.unshift(current);
                        current = current.parent;
                    }
                    return path;
                },
                highlight:function(d){

                    if(d){

                        var percentage = (100 * d.value / this.totalSize).toPrecision(3);
                        var percentageString = percentage + "%";
                        if (percentage < 0.1) {
                            percentageString = "< 0.1%";
                        }
                        var percentageDetail = this.bytesToSize(d.value) + '/' + this.bytesToSize(this.totalSize);
                        var colors = this.colors;

                        d3.select("#percentage")
                            .text(percentageString);
                        d3.select("#percentageDetail")
                            .text(percentageDetail);

                        d3.select("#explanation")
                            .style("visibility", "");

                        //console.log('mouse over : ',d);

                        var sequenceArray = this.getAncestors(d);

                        var arrayLength = sequenceArray.length;
                        var htmlString = '';

                        for (var i = 0; i < arrayLength; i++) {

                            var nodeType = sequenceArray[i].type;
                            if (nodeType == 'dir'){
                                htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'/</span>';
                            }
                            else {
                                htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'</span>';
                            }
                        }

                        $("#detailInfo").html(htmlString);

                        // Fade all the segments.
                        d3.selectAll("path").style("opacity", 0.3);

                        // Then highlight only those that are an ancestor of the current segment.
                        var foundNode = this.vis.selectAll("path")
                            .filter(function(node) {
                                return (sequenceArray.indexOf(node) >= 0);
                            })
                            .style("opacity", 1);
                    }else{
                        console.error('cant find '+path);
                    }
                },
                createChart1:function(json,vis,width,height,radius) {

                    vis.append("svg:circle")
                        .attr("r", radius)
                        .style("opacity", 0);

                    var colors = this.colors,
                        self = this;


                    var partition = d3.layout.partition(radius)
                            .size([2 * Math.PI, radius * radius])
                            .value(function(d) { return d.size; });


                    function mouseleave(d) {

                        //return;

                        // Hide the breadcrumb trail
                        d3.select("#trail")
                            .style("visibility", "hidden");

                        // Deactivate all segments during transition.
                        d3.selectAll("path").on("mouseover", null);

                        // Transition each segment to full opacity and then reactivate it.
                        d3.selectAll("path")
                            .transition()
                            .duration(1000)
                            .style("opacity", 1)
                            .each("end", function() {
                                d3.select(this).on("mouseover", mouseover);
                            });

                        d3.select("#explanation")
                            .transition()
                            .duration(1000)
                            .style("visibility", "hidden");

                        $("#detailInfo").html("");

                    }

                    function mouseover(d) {


                        var percentage = (100 * d.value / self.totalSize).toPrecision(3);
                        var percentageString = percentage + "%";
                        if (percentage < 0.1) {
                            percentageString = "< 0.1%";
                        }
                        var percentageDetail = self.bytesToSize(d.value) + '/' + self.bytesToSize(self.totalSize);

                        d3.select("#percentage")
                            .text(percentageString);
                        d3.select("#percentageDetail")
                            .text(percentageDetail);

                        d3.select("#explanation")
                            .style("visibility", "");

                        //console.log('mouse over : ',d);

                        var sequenceArray = self.getAncestors(d);

                        var arrayLength = sequenceArray.length;
                        var htmlString = '';
                        for (var i = 0; i < arrayLength; i++) {
                            var nodeType = sequenceArray[i].type;
                            if (nodeType == 'dir'){
                                htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'/</span>';
                            }
                            else {
                                htmlString+='<span style="color:'+colors[nodeType]+'">' + sequenceArray[i].name +'</span>';
                            }
                        }
                        $("#detailInfo").html(htmlString);


                        // Fade all the segments.
                        d3.selectAll("path")
                            .style("opacity", 0.3);

                        // Then highlight only those that are an ancestor of the current segment.
                        vis.selectAll("path")
                            .filter(function(node) {
                                return (sequenceArray.indexOf(node) >= 0);
                            })
                            .style("opacity", 1);

                    }


                    // For efficiency, filter nodes to keep only those large enough to see.
                    var nodes = partition.nodes(json,radius)
                        .filter(function(d) {
                            return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
                        });

                    var arc = d3.svg.arc()
                        .startAngle(function(d) { return d.x; })
                        .endAngle(function(d) { return d.x + d.dx; })
                        .innerRadius(function(d) { return Math.sqrt(d.y); })
                        .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

                    var path = vis.data([json]).selectAll("path")
                        .data(nodes)
                        .enter().append("svg:path")
                        .attr("display", function(d) { return d.depth ? null : "none"; })
                        .attr("d", arc)
                        .attr("fill-rule", "evenodd")
                        .style("fill", function(d) { return colors[d.type]?colors[d.type]:colors["default"]; })
                        .style("opacity", 1)
                        .on("mouseover", mouseover);

                    // Add the mouseleave handler to the bounding circle.
                    d3.select("#container").on("mouseleave", mouseleave);

                    // Get total size of the tree = value of root node from partition.
                    this.totalSize = path.node().__data__.value;

                    return nodes;
                },
                render:function(data){

                    var node = $(this.tab2);

                    var width = node.height();;//node.width() / 1.5;
                    var height = node.height();
                    var radius = height;
                    var visParent = this.chart1;
                    var vis = d3.select(visParent).append("svg:svg")
                        .attr("width", width)
                        .attr("height", height)
                        .append("svg:g")
                        .attr("id", "container")
                        .attr("transform", "translate(" + height/2 + "," + height/2  + ")");

                    var nodes = this.createChart1(data,vis,width,height,radius/4,vis);


                    var filetypeJSON = [];
                    for (var x in this.result) {
                        var tempF = this.result[x] / this.overallSize;
                        if (tempF < 0.001) {
                            //console.error('skip');
                            continue;
                        }
                        var tempN = this.bytesToSize(this.result[x]) + '/' + this.bytesToSize(this.overallSize);
                        var temp = {filetype: x, "usage": tempF, "detail": tempN};
                        filetypeJSON.push(temp);
                    }

                    /*
                    console.dir(this.result);
                    console.error('file types');
                    console.dir(filetypeJSON);
                    */
                    this.vis = vis;
                    this.d3Nodes = nodes;

                    var node2 = $(this.tab1);
                    width = node2.width();
                    height = node.height();
                    this.createChart2(filetypeJSON,width,node.height()-100);

                },
                createChart2:function(inputJson,width,height) {

                    var margin = {top: 20, right: 30, bottom: 30, left: 40};

                    var x = d3.scale.ordinal()
                        .rangeRoundBands([0, width], .1);

                    var y = d3.scale.linear()
                        .range([height, 0]);

                    var xAxis = d3.svg.axis()
                        .scale(x)
                        .orient("bottom");

                    var yAxis = d3.svg.axis()
                        .scale(y)
                        .orient("left");


                    var node = this.tab1.children[0],
                        colors = this.colors;

                    var chart = d3.select(node)
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    var data = inputJson;

                    x.domain(inputJson.map(function(d) {return d.filetype; }));

                    y.domain([0, d3.max(inputJson, function(d) { return d.usage; })]);

                    chart.append("g")
                        .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    chart.append("g")
                        .attr("class", "y axis")
                        .call(yAxis);

                    chart.selectAll(".bar")
                        .data(inputJson)
                        .enter()
                        .append("rect")
                        .attr("class", "bar")
                        .attr("id", function(d) { return 'bar'+d.filetype;})
                        .attr("x", function(d) { return x(d.filetype); })
                        .attr("y", function(d) { return y(d.usage); })
                        .attr("height", function(d) { return height - y(d.usage); })
                        .attr("width", x.rangeBand())
                        .attr("fill", function(d) { return colors[d.filetype]?colors[d.filetype]:colors["default"]; })
                        .on("mouseover", function(d) {
                            d3.select(this)
                                .transition()
                                .duration(50)
                                .attr("fill", "#7f8c8d");

                            //Get this bar's x/y values, then augment for the tooltip
                            var xPosition = parseFloat(d3.select(this).attr("x")) + x.rangeBand() / 2;
                            var yPosition = parseFloat(d3.select(this).attr("y")) / 2 + height / 2;

                            var usageString = parseFloat(d.usage * 100).toFixed(2) + '%('+ d.filetype +')';
                            //Update the tooltip position and value
                            d3.select("#tooltip")
                                .style("left", xPosition + "px")
                                .style("top", yPosition + "px")
                                .select("#chart2Per2")
                                .text(d.detail);

                            d3.select("#chart2Per1")
                                .text(usageString);

                            //Show the tooltip
                            d3.select("#tooltip").classed("hidden", false);
                        })


                        .on("mouseout", function() {
                            d3.select(this)
                                .transition()
                                .delay(100)
                                .duration(250)
                                .attr("fill", function(d) { return colors[d.filetype]?colors[d.filetype]:colors["default"]; })

                            //Hide the tooltip
                            d3.select("#tooltip").classed("hidden", true);

                        });
                }

            });

            var display = utils.addWidget(widget,{
                owner:self

            },null,panel,true);
            panel.__display = display;
            tab.add(display,null,false);
            return display;
        }

        function paint(){

            if(panel.__display){
                utils.destroy(panel.__display);
                panel.__display=null;
            }

            display = createWidget();




            utils.resizeTo(display,panel,true,true);
            var visParent = display.domNode.children[0];
            var tooltip = display.domNode.children[1];

            var rows = self.getRows();
            var csv = [];

            _.each(rows,function(row){
                csv.push([row.path.replace('./',''),"" + row.sizeBytes]);
            });

            var json = display.buildHierarchy(csv);

            display.render(json);

            return;

            var node = $(display.domNode);
            var width = node.height();;//node.width() / 1.5;
            var height = node.height();
            var radius = height;



            vis = d3.select(visParent).append("svg:svg")
                .attr("width", width)
                .attr("height", height)
                .append("svg:g")
                .attr("id", "container")
                .attr("transform", "translate(" + height/2 + "," + height/2  + ")");

            nodes = createChart1(json,vis,width,height,radius/4,vis);

            //console.dir(d);



        }

        setTimeout(function(){
            paint();
        },2000);


    }

    function run(tab,grid){

        //grid.onStatusbarCollapse();
    }

    function doTests(tab, grid) {


        grid.onStatusbarCollapse();
        grid.set('loading',false);
        tab.finishLoading();
        return;

        return require([d3],function(e){
            window.d3 = e;
            grid.onStatusbarCollapse();
            grid.set('loading',false);
            tab.finishLoading();
        });


    }


    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];


    if (ctx) {
        var tab = TestUtils.createTab(null, null, module.id);
        var grid = FTestUtils.createFileGrid('workspace', {}, {


            isStatusbarOpen: false,
            resizeToParent: true,
            onCloseStatusPanel:function(e){

            },
            onOpenStatusPanelBak: function (panel) {

                if (!panel._tabs) {

                    var tabContainer = utils.addWidget(_TabContainer, {
                        direction: 'below'
                    }, null, panel, true);
                    panel._tabs = tabContainer;

                    var consoleViewClass = createShellViewClass(),
                        handlerClass = createShellViewDelegate(),
                        delegate = new handlerClass();


                    delegate.owner = this;

                    var tab = tabContainer.createTab('Bash', 'fa-terminal'),
                        bashShell = tab.add(consoleViewClass, {
                            type: 'sh',
                            title: 'Bash',
                            icon: 'fa-terminal'
                        }, null, true);

                    bashShell.delegate = delegate;
                    tab._onShown();



                    var tab2 = tabContainer.createTab('Javascript', 'fa-code');
                    var jsShell = tab2.add(consoleViewClass, {
                        type: 'javascript',
                        title: 'Javascript'
                    }, null, false);
                    jsShell.delegate = delegate;


                    var tab3 = tabContainer.createTab('PHP', 'fa-terminal');
                    var jsShell = tab3.add(consoleViewClass, {
                        type: 'php',
                        title: 'PHP'
                    }, null, false);

                    panel.add(tabContainer, null, false);

                }


            },
            onOpenStatusPanel: function (panel) {

                return onOpenStatusPanel.apply(this,[panel]);

                if (!panel._tabs) {

                    var tabContainer = utils.addWidget(_TabContainer, {
                        direction: 'below'
                    }, null, panel, true);
                    panel._tabs = tabContainer;

                    var consoleViewClass = createShellViewClass(),
                        handlerClass = createShellViewDelegate(),
                        delegate = new handlerClass();


                    delegate.owner = this;

                    var tab = tabContainer.createTab('Bash', 'fa-terminal'),
                        bashShell = tab.add(consoleViewClass, {
                            type: 'sh',
                            title: 'Bash',
                            icon: 'fa-terminal'
                        }, null, true);

                    bashShell.delegate = delegate;
                    tab._onShown();



                    var tab2 = tabContainer.createTab('Javascript', 'fa-code');
                    var jsShell = tab2.add(consoleViewClass, {
                        type: 'javascript',
                        title: 'Javascript'
                    }, null, false);
                    jsShell.delegate = delegate;


                    var tab3 = tabContainer.createTab('PHP', 'fa-terminal');
                    var jsShell = tab3.add(consoleViewClass, {
                        type: 'php',
                        title: 'PHP'
                    }, null, false);

                    panel.add(tabContainer, null, false);

                }


            },
            onStatusbarCollapse: function (collapser) {

                var panel = null;
                if (this.isStatusbarOpen) {
                    panel = this.getBottomPanel(false, 0.2);
                    panel.collapse();
                    this.onCloseStatusPanel(collapser);
                    collapser && collapser.removeClass('fa-caret-down');
                    collapser && collapser.addClass('fa-caret-up');
                } else {
                    panel = this._getBottom();
                    if (!panel) {
                        panel = this.getBottomPanel(false, 0.2);
                    } else {
                        panel.expand();
                    }
                    this.onOpenStatusPanel(panel);
                    collapser && collapser.removeClass('fa-caret-up');
                    collapser && collapser.addClass('fa-caret-down');
                }


                this.isStatusbarOpen = !this.isStatusbarOpen;

                panel.resize();

            },
            __runAction: function (action) {
                //return runAction.apply(this,[action]);
                var res = this.inherited(arguments);
                var _resInner = runAction.apply(this, [action]);

                return _resInner || res;
            }

        }, 'TestGrid', module.id, true, tab);

        doTests(tab,grid);
        return Grid;

        doTests(tab, grid);
        return declare('a', null, {});
    }
    return Grid;
});