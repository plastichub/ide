/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/on',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/ListRenderer',
    'xgrid/ThumbRenderer',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xide/data/Model',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xfile/data/Store',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xfile/views/FileGrid',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    'xgrid/Keyboard',
    'xfile/tests/ThumbRenderer',
    'dojo/dom-construct',
    'xgrid/Renderer',
    'xfile/model/File',
    'xide/widgets/TemplatedWidgetBase'

], function (declare,on,types,
             utils,factory,ListRenderer, ThumbRenderer, TreeRenderer,
             Trackable, ObservableStore, Model, _ActionMixin,
             Grid, Store, MultiRenderer, RadioButton, FileGrid, Ribbon, Registry, DefaultActions, Action,Keyboard,ThumbRenderer2,domConstruct,Renderer,File,TemplatedWidgetBase) {




    /***
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;



    function testMain(grid){

    }

    function createThumbRenderClass(){
        /**
         * The list renderer does nothing since the xgrid/Base is already inherited from
         * dgrid/OnDemandList and its rendering as list already.
         *
         * @class module:xgrid/ThumbRenderer
         * @extends module:xgrid/Renderer
         */
        //package via declare

        var _class = declare('xfile.ThumbRendererLarge',[Renderer],{

            _getLabel:function(){ return "Thumb"; },
            _getIcon:function(){ return "el-icon-th-large"; },
            sizeClass:'',
            startup:function(){


                if(this._started){
                    return;
                }
                var thiz = this;
            },
            thumbSize: "100",
            resizeThumb: true,
            _doubleWidthThumbs:true,
            deactivateRenderer:function(){

                $(this.domNode.parentNode).removeClass('metro');
                //$(this.domNode.parentNode).removeClass('container');


                $(this.domNode).css('padding','');/*
                $(this.contentNode).css('margin-top','0');*/


            },
            activateRenderer:function(){

                $(this.domNode.parentNode).addClass('metro');
                $(this.contentNode).css('padding','8px');


                /*
                $(this.domNode.parentNode).addClass('container');
                $(this.contentNode).css('padding','8px');
                $(this.contentNode).css('margin-top','16px');
                */
                //this._showHeader(false);

            },
            /**
             * Override renderRow
             * @param obj
             * @returns {*}
             */
            renderRow: function (obj) {
                var thiz = this,
                    div = domConstruct.create('div', {
                        className: "tile widget"
                    }),
                    icon = obj.icon,
                    no_access = obj.read === false && obj.write === false,
                    isBack = obj.name == '..',
                    directory = obj && !!obj.directory,
                    useCSS = false,
                    label = '',
                    imageClass = 'fa fa-folder fa-5x',
                    isImage = false,
                    sizeClass = this.sizeClass;


                this._doubleWidthThumbs = true;


                var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;';
                var contentClass = 'icon';
                if (directory) {
                    if (isBack) {
                        imageClass = 'fa fa-level-up fa-5x thumbIcon';
                        useCSS = true;
                    } else if (!no_access) {
                        imageClass = 'fa fa-folder fa-5x thumbIcon';
                        useCSS = true;
                    } else {
                        imageClass = 'fa fa-lock fa-5x thumbIcon';
                        useCSS = true;
                    }

                } else {

                    imageClass = 'itemIcon';

                    if (no_access) {

                        imageClass = 'fa fa-lock fa-5x thumbIcon';

                        useCSS = true;
                    } else {

                        if (utils.isImage(obj.path)) {

                            var url = this.getImageUrl(obj);
                            if (url) {
                                obj.icon = url;
                            } else {
                                obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                            }

                            imageClass = 'imageFile';

                        } else {
                            imageClass = 'fa fa-5x ' + utils.getIconClass(obj.path);
                            useCSS = true;
                        }
                    }

                }

                label = obj.name;

                var folderContent =  '<span style="' + iconStyle + '" class="fa '+imageClass +' thumbIcon"></span>';

                if (utils.isImage(obj.path)) {

                    var url = this.getImageUrl(obj);
                    if (url) {
                        obj.icon = url;
                    } else {
                        obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                    }

                    imageClass = '';
                    contentClass = 'image';
                    //folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';
                    folderContent = '<div style="padding:1px" class="tile-content image">' +
                        '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                        '</div>';

                    useCSS = true;
                    isImage = true;


                }


                //var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);left:40px;text-align:left;font-size: 72px;margin-top:-45px;opacity: 0.7';


                var html = '<div class="tile-content ' + contentClass +'">'+
                    folderContent +
                    '</div>'+

                    '<div class="brand bg-dark opacity">'+
                        '<span class="iconText text bg-dark opacity fg-white" style="">'+
                    label +
                    '</span>'+
                    '</div>';


                if(isImage || this._doubleWidthThumbs){
                    $(div).addClass(sizeClass);
                }

                if (useCSS) {
                    div.innerHTML = html;
                    return div;
                }


                if (directory) {

                    //div.innerHTML = '<span class=\"' + imageClass + '\""></span> <div class="name">' + obj.name + '</div>';


                    div.innerHTML = html;


                } else {
                    div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
                }
                return div;

            },
            getImageUrl: function (item) {

                var fileManager = this.ctx.getFileManager();
                if (fileManager && fileManager) {
                    var params = null;
                    if (this.resizeThumb) {
                        params = {
                            width: this.thumbSize
                        }
                    }
                    return fileManager.getImageUrl(item, null, params);
                }
                return null;
            }
        });

        return _class;
    }


    function getFileActions(permissions) {

        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();

        permissions = permissions || this.permissions;

        return result;
    }

    function createStore(mount) {

        var storeClass = declare.classFactory('fileStore', [Store, Trackable, ObservableStore]);
        //var MyModel = declare(Model, {});
        var config = types.config;

        var options = {
            fields: types.FIELDS.SHOW_ISDIR |
            types.FIELDS.SHOW_OWNER |
            types.FIELDS.SHOW_SIZE |
            types.FIELDS.SHOW_FOLDER_SIZE |
            types.FIELDS.SHOW_MIME |
            types.FIELDS.SHOW_PERMISSIONS |
            types.FIELDS.SHOW_TIME
        };

        var store = new storeClass({
            idProperty: 'path',
            Model: File,
            data: [],
            config: config,
            url: types.config.FILE_SERVICE,
            serviceUrl: types.config.FILE_SERVICE,
            serviceClass: types.config.FILES_STORE_SERVICE_CLASS,
            mount: mount,
            options: options
        });

        store._state = {
            initiated: false,
            filter: null,
            filterDef: null
        };

        store.reset();
        store.config = config;
        store.setData([]);
        store.init();

        return store;
    }

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var fileStore = createStore('root');

            var thumbRenderer = createThumbRenderClass();
            var renderers = [thumbRenderer,ListRenderer,TreeRenderer];

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var mainView = ctx.mainView;

            var docker = mainView.getDocker();
            if(window._lastGrid){
                docker.removePanel(window._lastGrid);
            }
            parent = docker.addTab(null, {
                title: 'Documentation',
                icon: 'fa-folder'
            });

            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon,
                store = fileStore;

            var _gridClass = declare('File2', FileGrid, {
                selectedSource:'Root',
                renderers:renderers,
                selectedRenderer:thumbRenderer,
                runAction:function(action){

                    if(action.command=='View/Download Data'){
                        //console.dir([this,this.collection]);
                        utils.download(JSON.stringify(this.getData()));
                    }
                    return this.inherited(arguments);
                },
                startup:function(){

                    if(this._started){
                        return;
                    }

                    this.inherited(arguments);

                    var thiz = this;


                    this._on('onAddActions', function (evt) {

                        var actions = evt.actions,
                            permissions = evt.permissions,
                            container = thiz.domNode;

                        actions.push(thiz.createAction('Large Icons', 'View/Icon Size', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Layout', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                        actions.push(thiz.createAction('Download Data', 'View/Download Data', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Save', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                    });
                }
            });


            /**
             *
             * @param item
             * @param where
             * @param mixin
             * @param select
             * @returns {*}
             */
            grid = new _gridClass({
                //selectedRenderer: thumb,
                collection: store.getDefaultCollection('./'),
                showHeader: true,
                _parent: parent
            }, parent.containerNode);
            grid.startup();

            window._lastGrid = parent;
            var _actions = [];

            var _defaultActions = DefaultActions.getDefaultActions(_actions, grid);
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid));
            _defaultActions = _defaultActions.concat(grid.getFileActions(_actions));
            grid.addActions(_defaultActions);

            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();
            toolbar.setActionEmitter(grid);


            setTimeout(function () {
                mainView.resize();
                grid.resize();

            }, 1000);


            var copy = actionStore.getSync('Edit/Copy');

            var c1 = actionStore.getSync('View/Columns/Show Path');


            function test() {
                testMain(grid);

            }


            setTimeout(function () {
                test();
            }, 2000);




        }
    }

    return Grid;

});