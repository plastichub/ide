/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xfile/tests/TestUtils',
    'module',
    'xide/widgets/ActionToolbar',
    'xaction/ActionContext',
    'xide/widgets/ActionToolbarMixin',
    "xide/mixins/ActionMixin",
    "xide/mixins/EventedMixin",
    "xide/widgets/_MenuMixin",
    'xide/widgets/ActionToolbarButton',
    'xide/data/Reference',
    'xaction/DefaultActions',
    'xide/editor/Registry',
    'xlang/i18',
    'dojo/aspect',
    "xide/model/Path"

], function (declare,types,utils,
             XFileTestUtils,module,ActionToolbar,ActionContext,
             ActionToolbarMixin, ActionMixin,EventedMixin,_MenuMixin,
             ActionToolbarButton,
             Reference,
             DefaultActions,
             Registry,
             i18,
             aspect,
             Path) {

    var createCallback = function (func, menu, item) {
        return function (event) {
            func(event, menu, item);
        };
    }
    /***
     *
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        ACTION = types.ACTION;


    function doToolbarTests(grid){
        grid.showToolbar(true);
        grid.deselectAll();
        var contextMenu = grid.getContextMenu();

        contextMenu.buildMenuItems=function ($menu, data, id, subMenu, addDynamicTag) {
            this._debugMenu && console.log('build - menu items ', arguments);
            var linkTarget = '',
                self = this,
                visibility = this.visibility;

            for (var i = 0; i < data.length; i++) {
                var item = data[i],
                    $sub,
                    widget = item.widget;

                if (typeof item.divider !== 'undefined' && !item.widget) {

                    var divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    divider += '"></li>';
                    item.widget = divider;
                    $menu.append(divider);
                } else if (typeof item.header !== 'undefined' && !item.widget) {
                    var header = '<li class="nav-header';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + item.header + '</li>';
                    item.widget = header;
                    $menu.append(header);
                } else if (typeof item.menu_item_src !== 'undefined') {
                    var funcName;
                    if (typeof item.menu_item_src === 'function') {
                        if (item.menu_item_src.name === "") { // The function is declared like "foo = function() {}"
                            for (var globalVar in window) {
                                if (item.menu_item_src == window[globalVar]) {
                                    funcName = globalVar;
                                    break;
                                }
                            }
                        } else {
                            funcName = item.menu_item_src.name;
                        }
                    } else {
                        funcName = item.menu_item_src;
                    }
                    $menu.append('<li class="dynamic-menu-src" data-src="' + funcName + '"></li>');
                } else {

                    if (!widget && typeof item.href == 'undefined') {
                        item.href = '#';
                    }
                    if (!widget && typeof item.target !== 'undefined') {
                        linkTarget = ' target="' + item.target + '"';
                    }
                    if (typeof item.subMenu !== 'undefined' && !widget) {

                        var sub_menu = '<li class="dropdown-submenu';

                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a tabindex="-1" href="' + item.href + '">';

                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }

                        sub_menu += item.text + '';

                        sub_menu += '</a></li>';

                        $sub = $(sub_menu);

                    } else {


                        if (!widget) {

                            if (item.render) {
                                $sub = item.render(item, $menu);

                            } else {

                                var element = '<li class="" ';
                                element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                                element += '><a tabindex="-1" href="' + item.href + '"' + linkTarget + '>';
                                if (typeof data[i].icon !== 'undefined') {
                                    element += '<span class="' + item.icon + '"></span> ';

                                }
                                element += item.text + '</a></li>';
                                $sub = $(element);

                                if (item.postRender) {
                                    item.postRender($sub);
                                }

                            }
                        }
                    }

                    if (typeof item.action !== 'undefined' && !item.widget) {

                        if (item.addClickHandler && item.addClickHandler() === false) {

                        } else {
                            var $action = item.action;
                            if ($sub && $sub.find) {
                                $sub.find('a')
                                    .addClass('context-event')
                                    .on('click', createCallback($action, item, $sub));
                            }
                        }
                    }


                    if ($sub && !widget) {

                        if (item.index == 0) {
                            $menu.prepend($sub);
                        } else {
                            $menu.append($sub);
                        }

                        item.widget = $sub;

                        $sub.menu = $menu;

                    }

                    if (typeof item.subMenu != 'undefined' && !item.subMenuData) {

                        var subMenuData = self.buildMenu(item.subMenu, id, true);

                        $menu.subMenuData = subMenuData;

                        item.subMenuData = subMenuData;

                        $menu.find('li:last').append(subMenuData);

                    }else{

                        if(item.command==='File/Open In') {

                            //var subMenuData = self.buildMenu(item.subMenu, id, true,true);
                            //debugger;

                        }
                        if(item.subMenu && item.subMenuData) {
                            this.buildMenuItems(item.subMenuData, item.subMenu, id, true);
                        }

                    }
                }

                if (!$menu._didOnClick) {
                    $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                        e.stopPropagation()
                    });
                    $menu._didOnClick = true;
                }
            }
            return $menu;
        };

        contextMenu.setActionStore=function (store, owner,subscribe,update,itemActions) {
            if(!update){
                this._clear();
                this.addActionStore(store);
            }

            var self = this,
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());
            var tree = update ? self.lastTree : self.buildActionTree(store,owner);

            var allActions = tree.allActions,
                rootActions = tree.rootActions,
                allActionPaths = tree.allActionPaths,
                oldMenuData = self.menuData;

            this.store = store;

            if(subscribe!==false) {
                this.addHandle('added', store._on('onActionsAdded', function (actions) {
                    self.onActionAdded(actions);
                }));

                this.addHandle('delete', store.on('delete', function (evt) {
                    self.onActionRemoved(evt);
                }));
            }
            var data = [];
            

            if(!update) {
                _.each(tree.root, function (menuActions, level) {

                    var root = self.onRootAction(level, rootContainer),
                        lastGroup = '',
                        lastHeader = {
                            header: ''
                        },
                        groupedActions = menuActions.grouped;


                    _.each(menuActions, function (command) {

                        var action = self.getAction(command, store),
                            isDynamicAction = false;

                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }

                        if (action) {

                            var renderData = self.getActionData(action);
                            var icon = renderData.icon,
                                label = renderData.label,
                                visibility = renderData.visibility,
                                group = renderData.group;

                            if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 2) {
                                if (lastGroup !== group) {
                                    lastHeader = {header: i18.localize(group)};
                                    data.push(lastHeader);
                                    lastGroup = group;
                                }
                            }


                            var item = self.toMenuItem(action, owner, label, icon, visibility || {});

                            data.push(item);

                            visibility.widget = item;

                            self.addReference(action, item);

                            function parseChildren(command, parent) {

                                var childPaths = new Path(command).getChildren(allActionPaths, false),
                                    isContainer = childPaths.length > 0,
                                    childActions = isContainer ? self.toActions(childPaths, store) : null;

                                if (childActions) {

                                    var subs = [];

                                    _.each(childActions, function (child) {

                                        var _renderData = self.getActionData(child);

                                        var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility);

                                        self.addReference(child, _item);

                                        subs.push(_item);

                                        var _childPaths = new Path(child.command).getChildren(allActionPaths, false),
                                            _isContainer = _childPaths.length > 0,
                                            _childActions = _isContainer ? self.toActions(_childPaths, store) : null;

                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }

                            parseChildren(command, item);
                        }
                    });
                });
                var menu = self.attach($('body'), data);
                self.onDidRenderActions(store, owner);
            }else{

                if(itemActions || !_.isEmpty(itemActions)) {

                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            var action = self.getAction(newAction.command);
                            if (action) {

                                var renderData = self.getActionData(action),
                                    icon = renderData.icon,
                                    label = renderData.label,
                                    aVisibility = renderData.visibility,
                                    group = renderData.group,
                                    item = self.toMenuItem(action, owner, label, icon, aVisibility || {});

                                aVisibility.widget = item;
                                self.addReference(newAction, item);
                                var parent = _.find(oldMenuData,{
                                    command:action.getParentCommand()
                                });

                                if(parent){
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });

                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }
        }



        function selHandler(event){

            var selection = event.selection;
            if(!selection || !selection[0] ){
                return;
            }

            var item = selection[0];

            var permissions = this.permissions;

            var ACTION = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                VISIBILITY = types.ACTION_VISIBILITY,
                thiz = this,
                container = thiz.domNode,
                actionStore = thiz.getActionStore(),
                contextMenu = this.getContextMenu(),
                oldMenuData = contextMenu.menuData,
                oldTree = contextMenu.lastTree;

            function _wireEditor(editor, action) {
                action.handler = function () {
                    editor.onEdit(thiz.getSelection()[0]);
                };
            };
            function getEditorActions(item){

                var editors = Registry.getEditors(item) || [],
                    result = [];

                for (var i = 0; i < editors.length; i++) {
                    var editor = editors[i];
                    //var editorAction = Action.create(editor.name, editor.iconClass, 'File/Open In/' + editor.name, false, null, 'BTFILE', 'editActions', null, false);
                    if(editor.name ==='Default Editor'){
                        continue;
                    }
                    var editorAction = thiz.createAction(editor.name, ACTION.OPEN_IN + '/' + editor.name, editor.iconClass, null, 'Home', 'Open', 'item', null,
                        function () {
                        },
                        {
                            addPermission: true,
                            tab: 'Home',
                            editor:editor,
                            custom:true
                        }, null, null, permissions, container, thiz
                    );
                    _wireEditor(editor, editorAction);
                    result.push(editorAction);
                }
                return result;
            }

            if (event.why == 'deselect') {
                return;
            }

            var action = actionStore.getSync(types.ACTION.OPEN_IN);
            var editorActions = thiz.addActions(getEditorActions(item));
            if(editorActions.length>0) {
                var newStoredActions = grid.addActions(editorActions);
                actionStore._emit('onActionsAdded', newStoredActions);
            }else{
                contextMenu.removeCustomActions();
            }
        }

        grid._on('selectionChanged',function(evt){
            selHandler.apply(grid,[evt]);
        })

        grid.select(['./test.json'],null,true,{
            focus:true,
            append:false
        })
        var actionStore = grid.getActionStore();
    }
    function getFileActions(permissions) {
        permissions = permissions || this.permissions;
        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            ctx = thiz.ctx,
            container = thiz.domNode,
            actionStore = thiz.getActionStore(),
            openInAction = null,
            openInActionModel = null,

            dirty = false,
            selHandle = null;

        var editorStore = Registry.getStore();

        function _wireEditor(editor,action){
            action.handler = function(){
                editor.onEdit(thiz.getSelection()[0]);
            };
        };
        function getItem(){
            var selection = thiz.getSelection(),
                item = selection[0],
                isFile = item && !item.isDir;

            return item;
        }
        function populateEditors(widget){

            //console.log('   populated editors ', _.pluck(_renderers,'declaredClass'));
            var item = getItem();
            if(!item){
                return;
            }
            var action = actionStore.getSync(types.ACTION.OPEN_IN),
                subs = action.getChildren();


            //clean up old actions
            _.each(subs,function(s){
                if(s.command != ACTION.OPEN_IN + '/Default Editor'){
                    actionStore.removeSync(s.command);
                }
            });

            var editorActions = thiz.addActions(getEditorActions(item));

            /*
            var toolbar = thiz.getToolbar();
            if(toolbar){

                toolbar.refreshActions=function(actions){

                    //console.log('refresh actions',this);
                    var _self = this;

                    _.each(actions,function(action){
                        //this.renderAction(action)
                        //_self.renderAction(action,this,null,null,null,null);
                        _self.renderAction(action, null , null , null , null);
                    });
                }
            }
            */

            var _renderers = actionStore.renderers;
            _.each(_renderers,function(renderer){
                renderer.refreshActions(editorActions);
            });
        }
        function setDisabled(action,disable){

            if(!action || !action.set){
                debugger;
            }
            action.set('disabled',disable);

            var references = (action && action.getReferences) ? action.getReferences() : null;
            if(references){
                _.each(references,function(ref){
                    //console.log('update ref ' + ref.get('label' ) + ' ' + disable);
                    if(ref._destroyed){
                        console.error('reference destroyed!');
                    }
                    ref.set('disabled',disable);
                    if(ref.dropDown){

                        if(ref.dropDown._destroyed){
                            console.error('reference-s dropdown destroyed ');
                            thiz._selHandle.remove();
                            thiz._selHandle = createSelectionHandle();

                        }else {
                            ref.dropDown.set('disabled', disable);
                        }
                        //console.log('update ref ' + ref.dropDown.get('label'));
                    }else{
                        console.error('ref has no drop-down');
                    }
                });
            }
        }
        function _patchMenu(widget) {

            if(widget) {

                var target = widget.popup || widget ;

                aspect.after(target, 'onOpen', function () {
                    var action = actionStore.getSync(types.ACTION.OPEN_IN);
                    if(widget.dirty===true){
                        var selection = thiz.getSelection(),
                            item = selection[0],
                            isFile = item && !item.isDir;

                        setDisabled(action,!isFile);
                        if(isFile){
                            populateEditors(widget);
                        }
                        widget.dirty=false;

                    }
                });
            }
        }
        function _createAction(options){

            var thiz = this,
                action = null,
                mixin = options.mixin || {},
                owner = options.owner || mixin.owner || thiz,
                permissions = options.permissions || [],
                command = options.command,
                keycombo = options.keycombo,
                label = options.label,
                icon = options.icon,
                tab = options.tab,
                group = options.group,
                filterGroup = options.filterGroup,
                onCreate = options.onCreate,
                handler = options.handler,
                container = options.container || thiz.domNode,
                shouldShow = options.shouldShow,
                shouldDisable = options.shouldDisable;

            utils.mixin(mixin, {owner: owner});

            if (mixin.addPermission || DefaultActions.hasAction(permissions, command)) {

                if (!handler) {
                    handler = function (action) {
                        console.log('log run action', arguments);
                        var who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }

                if(keycombo){

                    if(_.isString(keycombo)){
                        keycombo = [keycombo];
                    }
                    mixin.tooltip = keycombo.join('<br/>').toUpperCase();
                }

                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable,container || thiz.domNode);
                if(owner && action && owner.addAction){
                    owner.addAction(null,action);
                }
                return action;
            }
        }
        function getEditorActions(item){

            var editors = Registry.getEditors(item) || [],
                result = [];

            for (var i = 0; i < editors.length; i++) {
                var editor = editors[i];
                //var editorAction = Action.create(editor.name, editor.iconClass, 'File/Open In/' + editor.name, false, null, 'BTFILE', 'editActions', null, false);
                var editorAction = thiz.createAction(editor.name, ACTION.OPEN_IN + '/' + editor.name, editor.iconClass, null, 'Home', 'Open', 'item', null,
                    function () {
                    },
                    {
                        addPermission: true,
                        tab: 'Home',
                        editor:editor
                    }, null, null, permissions, container, thiz
                );
                _wireEditor(editor, editorAction);
                result.push(editorAction);
            }
            return result;
        }
        function createSelectionHandle(){

            /**
             *
             * 1. create root action 'File/Open In'
             * 1.1 onActionWidgetCreated('File/Open In')
             * 1.1.1 wireWidget for onOpen
             *
             *
             */


            return thiz._on('selectionChanged', function (event) {

                if (event.why == 'deselect') {
                    return;
                }

                var action = actionStore.getSync(types.ACTION.OPEN_IN);

                if(action && !action.getReferences){
                    console.error('not action model ' + action.declaredClass);
                }


                var selection = event.selection,
                    item = selection[0],
                    isFile = item && !item.isDir,
                    references = (action && action.getReferences) ? action.getReferences() : null;

                if(references){
                    //action.set('disabled',true);
                    setDisabled(action,!isFile);
                    _.each(references,function(ref){
                        if(ref.dropDown){
                            ref.dropDown.dirty = true;
                        }
                    });


                    //setDisabled(action,!isFile);

                }else{
                    console.error('have no refs');
                }

            });

        }

        if(thiz._selHandle) {
            thiz._selHandle.remove();
            thiz._selHandle=null;
        }

        if(!thiz._selHandle) {
            //thiz._selHandle = createSelectionHandle();
        }


        openInAction = this.createAction('Open In', ACTION.OPEN_IN, ACTION_ICON.EDIT, null, 'Home', 'Open', 'item',
            null,
            function () {
            },
            {
                addPermission: true,
                tab: 'Home',
                _wired:false,
                //dummy: true,
                onCreate:function(action){

                    action._on('WIDGET_CREATED', function (evt) {

                        var owner = evt.owner;
                        var widget = evt.widget;
                        var menu = widget.dropDown;
                        console.log('File/Open in -  Widget created ' +  owner.visibility);
                        if(menu) {
                            console.log('   patch menu ' + owner.visibility,widget);
                            //_patchMenu(menu);
                            menu.dirty = true;
                        }else{

                            console.log('widget has no drop-down ' + owner.visibility);
                        }
                    });

                    action._on(VISIBILITY.ACTION_TOOLBAR + '_WIDGET_CREATED', function (evt) {
                        return;
                        var scope = this,
                        //the ribbon
                            visibilityWidget = evt.owner,
                            //ribbon toolbar group
                            parent = evt.parent,
                            menu = evt.widget;

                        //console.log('widget created ' +menu.visibility);

                        thiz._on('selectionChanged', function (event) {

                            if (event.why == 'deselect') {
                                return;
                            }


                            var items = scope.getChildren(),
                                selection = event.selection,
                                isFile = selection.length == 1 && !selection[0].isDir,
                                item = selection[0],
                                editors = Registry.getEditors(item) || [],
                                editorActions = [];


                            //clear chache
                            actionStore._all = null;
                            action = actionStore.getSync(action.command);



                            //clean old
                            items.forEach(function (action) {
                                if(action.command == ACTION.OPEN_IN + '/Default Editor'){

                                }else {
                                    console.log('remove ' +action.command);
                                    //actionStore.removeSync(action.command);
                                }
                            });

                            if (isFile) {

                                if(!editors.length){
                                    action.set('disabled', true);
                                    return;
                                }else{
                                    action.set('disabled', false);
                                }

                                for (var i = 0; i < editors.length; i++) {
                                    var editor = editors[i];
                                    console.log('add editor action : ' + editor.name);
                                    //var editorAction = Action.create(editor.name, editor.iconClass, 'File/Open In/' + editor.name, false, null, 'BTFILE', 'editActions', null, false);
                                    var editorAction = thiz.createAction(editor.name, ACTION.OPEN_IN + '/' + editor.name, editor.iconClass, null, 'Home', 'Open', 'item', null,
                                        function () {
                                        },
                                        {
                                            addPermission: true,
                                            tab: 'Home'
                                        }, null, null, permissions, container, thiz
                                    )
                                    _wireEditor(editor, editorAction);
                                    editorActions.push(editorAction);
                                }

                                thiz.addActions(editorActions);

                                console.log('editorActions ',editorActions);

                            } else {
                                action.set('disabled', true);
                            }
                        });
                    });
                }
            }, null, DefaultActions.shouldDisableDefaultFileOnly, permissions, container, thiz
        );

        result.push(openInAction);


        var e = thiz.createAction('Default Editor', ACTION.OPEN_IN  + '/Default Editor', 'fa-code', null, 'Home', 'Open', 'item', null,
            function () {
            },
            {
                addPermission: true,
                tab: 'Home',
                forceSubs:true

            }, null, null, permissions, container, thiz
        );


        result.push(e);

        return result;
    }
    function testMain(grid,panel){
        var ACTION_TYPE = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            mainView = ctx.mainView;

        ctx.getWindowManager().registerView(grid,true);
        grid.refresh().then(function(){
            setTimeout(function(e){
                doToolbarTests(grid);
            },1000);
        });
    }

    if (ctx && ctx.mainView) {

        var doTest = true;
        if (doTest) {

            var ACTION = types.ACTION;

            var grid = XFileTestUtils.createFileGrid('root',
                //args
                {

                    _columns: {
                        "Name": true,
                        "Path": false,
                        "Size": false,
                        "Modified": false,
                        "Owner":false
                    },
                    permissions: [
                        ACTION.EDIT,
                        /*
                        ACTION.EDIT,
                        ACTION.RENAME,
                        ACTION.DOWNLOAD,
                        ACTION.COLUMNS,
                        ACTION.GO_UP,
                        ACTION.CLIPBOARD
                        */
                        //ACTION.COLUMNS,
                        ACTION.LAYOUT,
                        ACTION.COLUMNS,
                        ACTION.CONTEXT_MENU,
                        ACTION.GO_UP,
                        ACTION.NEW_FILE,
                        ACTION.OPEN_IN,
                        ACTION.TOOLBAR,
                        ACTION.NEW_DIRECTORY
                    ],
                    tabOrder: {
                        'Home': 100,
                        'View': 50,
                        'Settings': 20
                    },
                    tabSettings: {
                        'Step': {
                            width:'190px'
                        },
                        'Show': {
                            width:'130px'
                        },
                        'Settings': {
                            width:'100%'
                        }
                    },
                    groupOrder: {
                        'Clipboard': 110,
                        'File': 100,
                        'Step': 80,
                        'Open': 70,
                        'Organize': 60,
                        'Insert': 10,
                        'Select': 5,
                        'Navigation':4
                    }
                },

                //overrides
                {
                    getFileActions:function(permissions) {
                        return getFileActions.apply(this,[permissions]);
                    }

                },'TestGrid',module.id,false);



            function test() {
                testMain(grid,grid._parent);
            }

            setTimeout(function () {
                test();
            }, 1000);
        }
    }

    var _module = declare('maeh',null,{});

    _module.createEditorActions = getFileActions;

    return module;

});
