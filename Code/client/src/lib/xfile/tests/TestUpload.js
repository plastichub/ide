/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    "xide/views/_Dialog",

    "xfile/views/FileOperationDialog",
    "dojo/promise/all"


], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,_Dialog,FileOperationDialog,all
) {








    console.clear();

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    function runAction(_action){

        var action  = this.getAction(_action);
        console.error('run action : ' + action.command);
        switch (action.command){
            case ACTION_TYPE.DELETE :{

                return;
            }
        }
    }


    function createTemplate(name,progress,obj){

        var _textClass = obj.failed ? 'text-danger' : 'text-warning';

        return '<div class="" style="position: relative;margin: 0.1em;height: auto">' +

           '<div style="margin:0;vertical-align: middle;padding: 1px;" class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">'+
            '<div class="progress-bar progress-bar-info" style="width:' + progress + ';background-color: rgba(102, 161, 179, 0.35);height:auto;magin:0;">' +
                '<span class="'+_textClass +'" style="left: 0px">' + name +'</span>'+
            '</div>'+
            '</div>' +
            '</div>';
    }

    function toUploadItemsfunction(files){

        var thiz = this;
        var store = this.collection;

        var parent = this.getCurrentFolder();



        function createItem(file){


            var _item = {
                sizeBytes: utils.humanFileSize(file.size),
                size: utils.humanFileSize(file.size),
                name: file.name,
                type: file.type,
                path: parent.path + '/' +file.name+'__uploading__'+Math.random(2),
                isDir:false,
                mount:parent.mount,
                parent:parent.path,
                progress:0,
                modified:file.lastModified/1000,
                loaded:0,
                total:0,
                renderColumn:function(field, value, obj){

                    console.log('render column : ' + field);
                    if(field ==='name') {
                        return createTemplate(obj.name, obj.progress + '%',obj);
                    }
                    if(field ==='sizeBytes') {
                        if(obj.progress<100){
                            return utils.humanFileSize(obj.loaded) + ' of ' +utils.humanFileSize(obj.total);
                        }
                    }
                }
            }
            //_item._S = store;
            store.putSync(_item);
            _item = store.getSync(_item.path);
            file._item = _item;
            return _item;
        }

        var items = [];
        _.each(files,function(file){
            items.push(createItem(file));
        });
        return items;

    }


    function doFakeUppload(){


        var thiz = this;
        var store = this.collection;

        var parent = this.getCurrentFolder();

        //progress-bar progress-bar-danger

        var template = '<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100">'+
            '<div class="progress-bar progress-bar-danger" style="width:30%;"></div>'
            '</div>';

        //'<div style="">asdfasdf</div>'+





        function createItem(name){
            return {
                sizeBytes: 550000,
                size: '550000 kb',
                name: name,
                type: 'image/jpeg',
                path: parent.path + '/' +name,
                directory:false,
                mount:'root',
                parent:parent.path,
                progress:50,
                modified:new Date().getTime()/1000,
                renderColumn:function(field, value, obj){

                    //console.log('render field:'+field);

                    //return thiz.formatColumn(field,value,obj);

                    if(field ==='name') {

                        var tpl = this._tpl;

                        if (tpl) {
                            //var _inner = tpl.find('.progress-bar');
                            //console.log(_inner);
                            //return tpl;
                        }
                        tpl = createTemplate(obj.name, obj.progress + '%');
                        return tpl;
                    }

                },
                _render:function(item){
                    //console.log('renderer !!!',item);

                    var tpl = this._tpl;

                    if(tpl){
                        //var _inner = tpl.find('.progress-bar');
                        //console.log(_inner);
                        //return tpl;
                    }
                    tpl = $(createTemplate(this.name,this.progress +'%'))[0];
                    this._tpl = tpl;
                    return tpl;
                }
            }
        }

        function getFiles(parent) {

            var files = [
                createItem('a0.jpg'),
                createItem('a1.jpg')
            ]

            return files;
        }

        var files = getFiles(parent);

        var items = [];
        _.each(files,function(file){
            items.push(store.putSync(file));
        });


        this.refresh();


        return;
        setInterval(function(){

            _.each(items,function(item){
                item.progress++;
                store.refreshItem(item);
            });

        },2000);

    }


    function doTests(tab,grid){

        grid.refresh().then(function(){
            grid.select([0]).then(function(){
                return;
                grid.runAction(ACTION_TYPE.EDIT).then(function(){
                    console.log('did open folder');
                    //doFakeUppload.apply(grid,[]);
                });
            });


        })



        return;


        /*
        grid.refresh().then(function(){
            grid.select([0,1]).then(function(){
               grid.runAction(ACTION_TYPE.DELETE);
            });

        })
        */

    }


    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];






    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);
        var grid = FTestUtils.createFileGrid('root',
            //args
            {},
            //overrides
            {

                showInlineUpload:true,
                __createUploadItemTemplate:function(name,progress,obj){

                    var _textClass = obj.failed ? 'text-danger' : 'text-warning';

                    return '<div class="" style="position: relative;margin: 0.1em;height: auto">' +

                        '<div style="margin:0;vertical-align: middle;padding: 1px;" class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">'+
                        '<div class="progress-bar progress-bar-info" style="width:' + progress + ';background-color: rgba(102, 161, 179, 0.35);height:auto;magin:0;">' +
                        '<span class="'+_textClass +'" style="left: 0px">' + name +'</span>'+
                        '</div>'+
                        '</div>' +
                        '</div>';
                },
                _createUploadItemTemplate:function(name,progress,obj){

                    progress = parseInt(progress.replace('%',''));
                    if(progress==100){
                        progress = 98;
                    }


                    var result = '<div class="radial-progress widget tile" data-progress="' + progress + '">'+
                        '<div class="circle">'+
                        '<div class="mask full">'+
                        '<div class="fill"></div>'+
                        '</div>'+
                        '<div class="mask half">'+
                        '<div class="fill"></div>'+
                        '<div class="fill fix"></div>'+
                        '</div>'+
                        '<div class="shadow"></div>'+
                        '</div>'+
                        '<div class="inset">'+
                        '<div class="percentage">'+
                        '<div class="numbers"><span>-</span>';

                    for(var i= 0 ; i< 99 ; i++){

                        result+=('<span>' + i +'%' + '</span>');

                    };

                    result+='</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>';

                },
                _createUploadItems:function(files){




                    var thiz = this;
                    var store = this.collection;

                    var parent =  this.getCurrentFolder();
                    if(this.__currentDragOverRow){
                        parent = this.__currentDragOverRow.data;
                    }



                    function createItem(file){

                        var _item = {
                            sizeBytes: utils.humanFileSize(file.size),
                            size: utils.humanFileSize(file.size),
                            name: file.name,
                            type: file.type,
                            path: parent.path + '/' +file.name+'__uploading__'+Math.random(2),
                            isDir:false,
                            mount:parent.mount,
                            parent:parent.path,
                            progress:0,
                            isUpload:true,
                            modified:file.lastModified/1000,
                                /*
                            render:function(){
                                console.error(arguments);
                            },
                            */
                            __renderRow:function(obj){
                                return $(thiz._createUploadItemTemplate(obj.name, obj.progress + '%',obj))[0];
                                if(this._tp){
                                    this._tp.attr('data-progress', obj.progress);
                                    return this._tp[0];
                                }else{
                                    //this._tp =
                                }
                                //return this._tp[0];
                            },
                            renderColumn:function(field, value, obj){

                                if(field ==='name') {
                                    return thiz._createUploadItemTemplate(obj.name, obj.progress + '%',obj);
                                }
                                if(field ==='sizeBytes') {
                                    if(obj.progress<100){
                                        return utils.humanFileSize(obj.loaded) + ' of ' +utils.humanFileSize(obj.total);
                                    }
                                }
                            }
                        }
                        //_item._S = store;
                        store.putSync(_item);
                        _item = store.getSync(_item.path);
                        file._item = _item;
                        return _item;
                    }

                    var items = [];
                    _.each(files,function(file){
                        items.push(createItem(file));
                    });
                    return items;

                },
                runAction:function(action) {
                    var res = this.inherited(arguments);
                    runAction.apply(this,[action]);
                    return res;
                }

            },'TestGrid',module.id,true,parent);

        doTests(parent,grid);

        return declare('a',null,{});

    }

    return Grid;

});