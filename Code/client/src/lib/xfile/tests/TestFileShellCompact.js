/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",

    "xide/mixins/EventedMixin",
    "xide/mixins/PersistenceMixin",
    "xide/tests//TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",
    "dojo/cookie",
    "dojo/dom-construct",
    "dojo/_base/lang",
    "dojo/string",
    "xide/_base/_Widget",
    'xide/views/ConsoleView',
    'xide/views/ConsoleKeyboardDelegate',
    'xide/views/History',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'xide/layout/_TabContainer',
    'xide/views/_Console',
    'xide/views/_ConsoleWidget',
    'xide/views/_ConsoleHandler',
    "xfile/views/FileSize",
    "xfile/views/FileConsole",
    'xlang/i18',
    'xlang/de'

], function (declare, dcl, types,
             utils, Grid, TemplatedWidgetBase, EventedMixin,
             PersistenceMixin,
             TestUtils, FTestUtils, _Widget, registry, module,
             cache, cookie, domConstruct, lang, string, _XWidget, ConsoleView,
             ConsoleKeyboardDelegate, History, ACEEditor, Editor, _TabContainer,
             Console, ConsoleWidget,_ConsoleHandler,FileSize,FileConsole,i18,de) {




    console.clear();
    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;






    /***
     * Default editor persistence for peferences in cookies!
     **/








    function createConsoleAceWidgetClass() {

        return dcl([ConsoleWidget, PersistenceMixin.dcl], {
            createEditor: function () {
                var editor = createEditor(this.consoleParent, this.value, this, {
                    options: this.options
                });
                return editor;

            }
        });
    }
    function createEditor(root, value, owner, mixin) {
        var item = {
            filePath: '',
            fileName: ''
        };
        var title = "No Title";


        var args = {
            _permissions: [],
            item: item,
            value: value,
            style: 'padding:0px;top:0 !important',
            iconClass: 'fa-code',
            options: utils.mixin(mixin, {
                filePath: item.path,
                fileName: item.name
            }),
            ctx: ctx,
            /***
             * Provide a text editor store delegate
             */
            storeDelegate: {},
            title: title
        };


        utils.mixin(args, mixin);

        var editor = utils.addWidget(Editor, args, owner, root, true, null, null, false);

        editor.resize();

        return editor;
    }
    function createShellViewDelegate() {

        return dcl(null, {

            owner: null,
            onServerResponse: function (theConsole, data, addTimes) {


                if (theConsole && data && theConsole.owner && theConsole.owner.onServerResponse) {
                    theConsole.owner.onServerResponse(data, addTimes);
                }
            },
            runBash: function (theConsole, value, cwd) {


                var thiz = this;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['sh', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole,response, false);
                });
            },

            runPHP: function (theConsole, value, cwd) {
                var thiz = this;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['php', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole, response, false);
                });


            },
            runJavascript: function (theConsole, value, context, args) {

                var _function = new Function("{" + value + "; }");
                var response = _function.call(context, args);
                if (response != null) {
                    //console.error('response : ' + response);
                    this.onServerResponse(theConsole, response);
                    return response;
                }
                return value;
            },
            onConsoleCommand: function (data, value) {


                var thiz = this,
                    theConsole = data.console;


                if (theConsole.type === 'sh') {

                    value = value.replace(/["'`]/g, "");



                    thiz.onServerResponse(theConsole,"<pre style='font-weight: bold'># " + value + "</pre>", true);

                    var dstPath = null;

                    if (this.owner && this.owner.getCurrentFolder) {
                        var cwd = this.owner.getCurrentFolder();
                        if (cwd) {
                            dstPath = utils.buildPath(cwd.mount, cwd.path, false);
                        }
                    }
                    console.log('run bash in ' + dstPath);
                    if (theConsole.isLinked()) {
                        //dstPath = this.getCurrentPath();
                    }
                    return this.runBash(theConsole, value, dstPath);
                }

                if (theConsole.type === 'php') {

                    value = value.replace(/["'`]/g, "");

                    var dstPath = null
                    if (theConsole.isLinked()) {
                        dstPath = this.getCurrentPath();
                    }
                    return this.runPHP(theConsole, value, dstPath);
                }

                if (theConsole.type === 'javascript') {
                    return this.runJavascript(theConsole, value);
                }
            },
            onConsoleEnter: function (data, input) {
                return this.onConsoleCommand(data, input);
            }
        });
    }
    function createShellViewClass() {

        return Module = dcl(Console, {
            lazy: true,
            consoleClass: createConsoleAceWidgetClass(),
            getServer: function () {
                return this.server || ctx.fileManager;
            },
            log: function (msg, addTimes) {

                utils.destroy(this.progressItem);

                var out = '',
                    isHTML = false;
                if (_.isString(msg)) {

                    if(msg.indexOf('<body')!=-1 || /<[a-z][\s\S]*>/i.test(msg)) {

                        console.error('isHTML');
                        isHTML = true;
                        out = msg;

                    }else{
                        out += msg.replace(/\n/g, '<br/>');
                    }

                } else if (_.isObject(msg) || lang.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var thiz = this;
                if(isHTML){

                    dst.appendChild(dojo.create("div", {
                        className: 'html_response',
                        innerHTML: out

                    }));

                    last = dst.appendChild(dojo.create("div", {
                        innerHTML: '&nbsp;',
                        style:'height:1px;font-size:1px'
                    }));


                }else {

                    for (var i = 0; i < items.length; i++) {
                        var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');

                        var item = items[i];
                        if (!item || !item.length) {
                            continue
                        }
                        last = dst.appendChild(dojo.create("div", {
                            className: _class,
                            innerHTML: this._toString(items[i], addTimes)

                        }));
                        this.lastIndex++;
                    }
                }

                if (last) {
                    last.scrollIntoViewIfNeeded();
                }
            }
        });
    }
    function addActions(permissions) {

        //	adds 3 shells
        var target = ctx,
            source = ctx,
            self = this,
            result = [],
            shells = [
                ["Javacript", "fa-terminal", 'javascript'],
                ["Bash-Shell", "fa-terminal", 'sh'],
                ["PHP-Shell", "fa-code", 'php']
            ];


        function createShell(action, target, args) {

            var consoleViewClass = FileConsole.createShellViewClass(),
                handlerClass = FileConsole.createShellViewDelegate(),
                delegate = new handlerClass();

            delegate.ctx = self;
            delegate.owner = this;

            var parent = self.getWindowManager().createTab(action.title,action.icon);
            var shell = utils.addWidget(consoleViewClass, {
                type: action.shellType,
                title: action.title,
                icon: action.icon,
                ctx:self
            }, null, parent, false);

            shell.delegate = delegate;
            shell.startup();
            return shell;
        }




        function mkAction(title, icon, type, owner, handler) {
            var _action ={
                label: title,
                command: "Window/" + title,
                icon: icon || 'fa-code',
                tab: "Home",
                group: 'View',
                handler: handler || createShell,
                mixin: {
                    addPermission: true,
                    shellType: type
                },
                owner: owner || source
            };

            result.push(source.createAction(_action));
        }

        for(var i = 0 ; i < shells.length ; i++){
            mkAction.apply(source,shells[i]);
        }




        target.addActions(result);

        return result;
    }

    function doTests(tab, grid) {

        addActions.apply(ctx, []);

        return;
        setTimeout(function () {
            grid.onStatusbarCollapse();
            grid.resize();
            setTimeout(function () {
                grid.runAction('View/Layout/Thumb');
            }, 1000);
        }, 1000);




    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];


    if (ctx) {
        var tab = TestUtils.createTab(null, null, module.id);
        var grid = FTestUtils.createFileGrid('workspace', {}, {


            isStatusbarOpen: false,
            resizeToParent: true,
            __bottomTabContainer:null,
            onCloseStatusPanel:function(e){


            },
            onOpenStatusPanel: function (panel) {

                var tabContainer = this.getBottomTabContainer();
                if (!panel._tabs) {

                    var thiz = this;

                    var tabContainer = utils.addWidget(_TabContainer, {
                        direction: 'below'
                    }, null, panel, true);

                    panel._tabs = tabContainer;



                    var consoleViewClass = createShellViewClass(),
                        handlerClass = createShellViewDelegate(),
                        delegate = new handlerClass();



                    delegate.owner = this;

                    var tab = tabContainer.createTab('Bash', 'fa-terminal'),
                        bashShell = tab.add(consoleViewClass, {
                            type: 'sh',
                            title: 'Bash',
                            icon: 'fa-terminal'
                        }, null, true);

                    bashShell.delegate = delegate;
                    tab._onShown();





                    var tab2 = tabContainer.createTab('Javascript', 'fa-code');
                    var jsShell = tab2.add(consoleViewClass, {
                        type: 'javascript',
                        title: 'Javascript'
                    }, null, false);
                    jsShell.delegate = delegate;


                    var tab3 = tabContainer.createTab('PHP', 'fa-terminal');
                    var jsShell = tab3.add(consoleViewClass, {
                        type: 'php',
                        title: 'PHP'
                    }, null, false);



                    var tab4 = tabContainer.createTab('Sizes', 'fa-bar-chart');

                    var fileSize = tab4.add(FileSize, {
                        owner:this
                    }, null, false);

                    this.__sizeDisplay = fileSize;
                    this._on('openedFolder',function(data){
                        utils.destroy(thiz.__sizeDisplay);
                        tab4.resize();
                        thiz.resize();
                        thiz.__sizeDisplay = null;
                        thiz.__sizeDisplay = utils.addWidget(FileSize, {
                            owner:thiz
                        }, null,tab4,false);
                        utils.resizeTo(thiz.__sizeDisplay,tab4,true,true);
                        thiz.__sizeDisplay.startup();

                        //thiz.fileSize.startup();

                    });


                    panel.add(tabContainer, null, false);

                }
            },
            onStatusbarCollapse: function (collapser) {

                var panel = null;
                if (this.isStatusbarOpen) {
                    panel = this.getBottomPanel(false, 0.2);
                    panel.collapse();
                    this.onCloseStatusPanel(collapser);
                    collapser && collapser.removeClass('fa-caret-down');
                    collapser && collapser.addClass('fa-caret-up');
                } else {
                    panel = this._getBottom();
                    if (!panel) {
                        panel = this.getBottomPanel(false, 0.2);
                    } else {
                        panel.expand();
                    }
                    this.onOpenStatusPanel(panel);
                    collapser && collapser.removeClass('fa-caret-up');
                    collapser && collapser.addClass('fa-caret-down');
                }


                this.isStatusbarOpen = !this.isStatusbarOpen;

                panel.resize();

            },
            __runAction: function (action) {
                //return runAction.apply(this,[action]);
                var res = this.inherited(arguments);
                var _resInner = runAction.apply(this, [action]);

                return _resInner || res;
            }

        }, 'TestGrid', module.id, true, tab);

        doTests(tab, grid);
        return declare('a', null, {});
    }
    return Grid;
});