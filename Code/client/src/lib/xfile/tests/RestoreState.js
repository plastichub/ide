/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/on',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/ListRenderer',
    'xgrid/ThumbRenderer',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xide/data/Model',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xfile/data/Store',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xfile/views/FileGrid',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    'xgrid/Keyboard',
    'xfile/tests/ThumbRenderer',
    'dojo/dom-construct',
    'xgrid/Renderer',
    'xfile/model/File',
    'xide/widgets/TemplatedWidgetBase',
    'dijit/registry',
    './TestUtils',
    'xide/tests/TestUtils',
    'module'

], function (declare,on,types,
             utils,factory,ListRenderer, ThumbRenderer, TreeRenderer,
             Trackable, ObservableStore, Model, _ActionMixin,
             Grid, Store, MultiRenderer, RadioButton, FileGrid, Ribbon, Registry, DefaultActions, Action,Keyboard,ThumbRenderer2,domConstruct,Renderer,File,TemplatedWidgetBase,registry,TestUtils,XIDETestUtils,module) {





    




    /***
     *
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;




    function _restorep(e){

        var data = e.data,
            widgets = data.widgets,
            parent = this,
            _ctx = this.ctx;




        if(widgets){

            for (var i = 0; i < widgets.length; i++) {

                var widget = widgets[i];
                var _class = widget.widget;
                var obj = dojo.getObject(_class);
                if(obj){

                    var widgetInstance = utils.addWidget(obj,{
                        ctx:ctx,
                        state:widget.state
                    },null,parent,true);


                    if(widgetInstance){
                        if(widgetInstance.setState){
                            widgetInstance.setState(widget.state);
                        }
                    }

                }
            }
        }
    }

    window._restorep = _restorep;



    function testMain(grid,panel){

        grid._onSaveLayout=function(e){
            var customData = e.data;
            var gridState = grid.getState();
            console.log('save grid ' + this.declaredClass,customData);
            var data = {
                widget:grid.declaredClass,
                state:gridState
            }
            customData.widgets.push(data);
        };


        //var panel = utils.getParentWidget(grid,'Panel');

        //console.log('panel ',panel);

        //grid._on(types.EVENTS.SAVE_LAYOUT,function(e){});
        //panel._on(types.DOCKER.EVENT.RESTORE_LAYOUT,function(e){});
        var mainView = ctx.mainView;
        var docker = mainView.getDocker();
        var layout = utils.getJson(docker.save());
        //var layout = docker.save();
        console.error('save docker:');
        //console.dir(layout);
        setTimeout(function(){
            docker.restore(docker.save());
        },3000);


    }

    function getFileActions(permissions) {

        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();

        permissions = permissions || this.permissions;

        return result;
    }

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var fileStore = TestUtils.createStore('root');

            var renderers = [ListRenderer,TreeRenderer];

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var mainView = ctx.mainView;



            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon,
                store = fileStore,
                parent;



            var _gridClass = declare('xfile.views.FileGrid2', FileGrid, {
                selectedSource:'Root',
                declaredClass:'xfile.views.FileGrid',
                renderers:renderers,
                runAction:function(action){

                    if(action.command=='View/Download Data'){
                        //console.dir([this,this.collection]);
                        utils.download(JSON.stringify(this.getData()));
                    }
                    return this.inherited(arguments);
                },
                startup:function(){

                    if(this._started){
                        return;
                    }

                    this.inherited(arguments);

                    var thiz = this;


                    this._on('onAddActions', function (evt) {

                        var actions = evt.actions,
                            permissions = evt.permissions,
                            container = thiz.domNode;

                        actions.push(thiz.createAction('Large Icons', 'View/Icon Size', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Layout', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                        actions.push(thiz.createAction('Download Data', 'View/Download Data', 'fa-hdd-o', /*shortcuts*/null, 'View', 'Save', 'item|view', null,
                            function () {
                            },
                            {
                                addPermission: true,
                                tab: 'View'
                            }, null, null, permissions, container, thiz
                        ));

                    });
                }
            });

            /**
             *
             * @param item
             * @param where
             * @param mixin
             * @param select
             * @returns {*}
             */
            /*
            grid = new FileGrid({
                //selectedRenderer: thumb,
                collection: store.getDefaultCollection(),
                showHeader: true
            }, parent.containerNode);

            grid.startup();
            */

            grid = {};

            window._lastGrid = parent;

            /*
            var _actions = [];

            var _defaultActions = DefaultActions.getDefaultActions(_actions, grid);
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid));
            _defaultActions = _defaultActions.concat(grid.getFileActions(_actions));
            grid.addActions(_defaultActions);

            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();
            toolbar.setActionEmitter(grid);
            */



            setTimeout(function () {
                mainView.resize();
                //grid.resize();

            }, 1000);

            function test() {
                testMain(grid,parent);
            }


            setTimeout(function () {
                test();
            }, 2000);



        }
    }

    return Grid;

});