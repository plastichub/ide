/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "dijit/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    "xide/views/_Dialog",

    "xfile/views/FileOperationDialog",
    "dojo/promise/all"


], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,_Dialog,FileOperationDialog,all
) {


    console.clear();

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    function getDlgTemplate(){

        var string =

            '<div class="row">'+
            '<div class="col-md-8 col-md-offset-1">'+
            '<section class="widget">'+
            '<div class="body">'+

            '<form id="fileupload" action="server/php" method="POST" enctype="multipart/form-data">'+
            '<div class="row">'+
            '<div class="col-md-12">'+
            '<div id="dropzone" class="dropzone">'+
            'Drop files here'+
            '<i class="fa fa-download-alt pull-right"></i>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="row">'+
            '<div class="col-md-12 fileupload-progress fade">'+
            '<!-- The global progress bar -->'+
            '<div class="progress progress-xs active" role="progressbar" aria-valuemin="0" aria-valuemax="100">'+
            '<div class="progress-bar progress-bar-inverse" style="width:0%;"></div>'+
            '</div>'+
            '<!-- The extended global progress state -->'+
            '<div class="progress-extended">&nbsp;</div>'+
            '</div>'+
            '</div>'+
            '<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->'+
            '<div class="form-actions fileupload-buttonbar no-margin">'+
            '<!-- The fileinput-button span is used to style the file input field as button -->'+
            '<span class="btn btn-default fileinput-button mr-xs">'+
            '<i class="glyphicon glyphicon-plus"></i>'+
            '<span>Add files...</span>'+
            '<input type="file" name="files[]" multiple="">'+
            '</span>'+
            '<button type="submit" class="btn btn-primary start mr-xs">'+
            '<i class="glyphicon glyphicon-upload"></i>'+
            '<span>Start upload</span>'+
            '</button>'+
            '<button type="reset" class="btn btn-inverse cancel">'+
            '<i class="glyphicon glyphicon-ban-circle"></i>'+
            '<span>Cancel upload</span>'+
            '</button>'+
            '</div>'+
            '<!-- The table listing the files available for upload/download -->'+
            '<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>'+
            '</form>'+

            '</div >'+
            '</div >'+
            '</section >'+
            '<div>';

        return string;
    }


    function createDialogClass(title,type){

        return dcl(_Dialog, {
            title:title,
            type: type || types.DIALOG_TYPE.INFO,
            size: types.DIALOG_SIZE.SIZE_WIDE,
            bodyCSS: {},
            failedText:' Failed!',
            successText:': Success!',
            showSpinner:true,
            spinner:'  <span class="fa-spinner fa-spin"/>',
            notificationMessage:null,
            message:getDlgTemplate(),
            doOk:function(dfd){

                this.onBeforeOk && this.onBeforeOk();

                var msg  = this.showMessage(),
                    thiz = this;

                dfd.then(function (result) {
                    thiz._onSuccess(result);
                }, function (err) {
                    thiz._onError();
                });

            },
            _onSuccess:function(title,suffix,message){

                title = title  || this.title;
                message = message || this.notificationMessage;

                message && message.update({
                    message: title + this.successText + (suffix ?  '<br/>' + suffix  : ''),
                    type: 'info',
                    actions: false,
                    duration: 1500
                });

                this.onSuccess && this.onSuccess();
            },
            _onError:function(title,suffix,message){

                title = title  || this.title;

                message = message || this.notificationMessage;

                message && message.update({
                    message: title + this.failedText + (suffix ?  '<br/>' + suffix  : ''),
                    type: 'error',
                    actions: false,
                    duration: 15000
                });





                this.onError && this.onError(suffix);
            },
            onOk:function() {
                var msg = this.showMessage(),
                    thiz = this;
                this.doOk(this.getOkDfd());
            },
            showMessage:function(title){

                if(this.notificationMessage){
                    return this.notificationMessage;
                }
                title = title || this.title;

                var msg = this.ctx.getNotificationManager().postMessage({
                    message:title + (this.showSpinner ? this.spinner : ''),
                    type:'info',
                    showCloseButton: true,
                    duration:4500
                });

                this.notificationMessage = msg;

                return msg;
            }

        });
    }



    function doUploadAction(){


        var dlgClass = createDialogClass('Upload!');

        var dlg =new dlgClass();

        dlg.show();


    }


    function runAction(_action){
        var action  = this.getAction(_action);

        console.error('run action : ' + action.command);

        switch (action.command){
            case ACTION_TYPE.UPLOAD:{
                return doUploadAction.apply(this);
            }
        }
    }

    function createTemplate(name,progress,obj){

        var _textClass = obj.failed ? 'text-danger' : 'text-warning';

        return '<div class="" style="position: relative;margin: 0.1em;height: auto">' +

           '<div style="margin:0;vertical-align: middle;padding: 1px;" class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">'+
            '<div class="progress-bar progress-bar-info" style="width:' + progress + ';background-color: rgba(102, 161, 179, 0.35);height:auto;magin:0;">' +
                '<span class="'+_textClass +'" style="left: 0px">' + name +'</span>'+
            '</div>'+
            '</div>' +
            '</div>';
    }
    function toUploadItemsfunction(files){

        var thiz = this;
        var store = this.collection;

        var parent = this.getCurrentFolder();

        function createItem(file){

            var _item = {
                sizeBytes: utils.humanFileSize(file.size),
                size: utils.humanFileSize(file.size),
                name: file.name,
                type: file.type,
                path: parent.path + '/' +file.name+'__uploading__'+Math.random(2),
                isDir:false,
                mount:parent.mount,
                parent:parent.path,
                progress:0,
                modified:file.lastModified/1000,
                renderColumn:function(field, value, obj){
                    if(field ==='name') {
                        return createTemplate(obj.name, obj.progress + '%',obj);
                    }
                }
            }
            //_item._S = store;
            store.putSync(_item);
            _item = store.getSync(_item.path);
            file._item = _item;
            return _item;
        }

        var items = [];
        _.each(files,function(file){
            items.push(createItem(file));
        });
        return items;

    }
    function doFakeUppload(){


        var thiz = this;
        var store = this.collection;

        var parent = this.getCurrentFolder();

        //progress-bar progress-bar-danger

        var template = '<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100">'+
            '<div class="progress-bar progress-bar-danger" style="width:30%;"></div>'
            '</div>';

        //'<div style="">asdfasdf</div>'+




        function createItem(name){
            return {
                sizeBytes: 550000,
                size: '550000 kb',
                name: name,
                type: 'image/jpeg',
                path: parent.path + '/' +name,
                directory:false,
                mount:'root',
                parent:parent.path,
                progress:50,
                modified:new Date().getTime()/1000,
                renderColumn:function(field, value, obj){

                    //console.log('render field:'+field);

                    //return thiz.formatColumn(field,value,obj);

                    if(field ==='name') {

                        var tpl = this._tpl;

                        if (tpl) {
                            //var _inner = tpl.find('.progress-bar');
                            //console.log(_inner);
                            //return tpl;
                        }
                        tpl = createTemplate(obj.name, obj.progress + '%');
                        return tpl;
                    }

                },
                _render:function(item){
                    //console.log('renderer !!!',item);

                    var tpl = this._tpl;

                    if(tpl){
                        //var _inner = tpl.find('.progress-bar');
                        //console.log(_inner);
                        //return tpl;
                    }
                    tpl = $(createTemplate(this.name,this.progress +'%'))[0];
                    this._tpl = tpl;
                    return tpl;
                }
            }
        }

        function getFiles(parent) {

            var files = [
                createItem('a0.jpg'),
                createItem('a1.jpg')
            ]

            return files;
        }

        var files = getFiles(parent);

        var items = [];
        _.each(files,function(file){
            items.push(store.putSync(file));
        });


        this.refresh();


        return;
        setInterval(function(){

            _.each(items,function(item){
                item.progress++;
                store.refreshItem(item);
            });

        },2000);

    }
    function doTests(tab,grid){
        grid.refresh().then(function(){
            grid.select([0]).then(function(){
                grid.runAction(ACTION_TYPE.EDIT).then(function(){
                    console.log('did open folder');
                    grid.runAction(ACTION_TYPE.UPLOAD);
                    //doFakeUppload.apply(grid,[]);
                });
            });
        });

        return;
    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];


    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);
        var grid = FTestUtils.createFileGrid('root',
            //args
            {},
            //overrides
            {

                showInlineUpload:true,
                runAction:function(action) {
                    //return runAction.apply(this,[action]);
                    var res = this.inherited(arguments);
                    runAction.apply(this,[action]);

                    return res;
                }

            },'TestGrid',module.id,true,parent);

        doTests(parent,grid);

        return declare('a',null,{});

    }

    return Grid;

});