define([
    'dojo/_base/declare',
    'xide/types'
],function(declare,types){

    types.config={

        /**
         * Root node id to for xfile main application, can be ignored for embedded mode.
         */
        ROOT_NODE:'xapp',
        /***
         * The absolute url to server rpc end-point
         */
        FILE_SERVICE:'',
        /***
         * Default start path
         */
        START_PATH:'./',
        /***
         * A pointer to a resolved AMD prototype. This should be xfile/data/FileStore
         */
        FILES_STORE_SERVICE_CLASS:'XCOM_Directory_Service',
        /***
         *  Enables file picker controls, obsolete at the moment
         */
        IS_MEDIA_PICKER:null,
        EDITOR_NODE:null,
        EDITOR_AFTER_NODE:null,
        ACTION_TOOLBAR:null,

        ALLOWED_DISPLAY_MODES:{
            TREE:1,
            LIST:1,
            THUMB:1,
            IMAGE_GRID:1
        },

        LAYOUT_PRESET:null,

        beanContextName:null

    };
    return types
});
