define([
    "dojo/_base/kernel",
    'dojo/Stateful',
    'dojo/query',
    'dojo/cache',
    'dojo/window',
	'xfile/types',
    'xfile/component',
    'xfile/views/Grid',
    'xfile/views/FileGrid',
    'xfile/FileActions',
    'xfile/Statusbar',
    'xfile/ThumbRenderer',
    'xfile/Breadcrumb',
    'xfile/types',
    'xfile/component',
    'xfile/config',
    'xfile/model/File',
    'xfile/manager/FileManager',
    'xfile/manager/MountManager',
    'xfile/manager/BlockManager',
    'xfile/views/FileConsole',
    'xfile/data/DriverStore',
    'xfile/views/FileGridLight',
    'xfile/manager/Electron'
], function(){

});
