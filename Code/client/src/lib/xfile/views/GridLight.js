/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/ListRenderer',
    'xfile/ThumbRenderer',
    'xgrid/TreeRenderer',
    'xgrid/GridLite',
    'xgrid/MultiRenderer',
    'xfile/FileActions',
    'xide/views/_LayoutMixin',
    'xgrid/KeyboardNavigation',
    'xgrid/Selection',
    'xide/mixins/_State',
    "xide/widgets/_Widget"
], function (declare, types,ListRenderer, ThumbRenderer, TreeRenderer,Grid, MultiRenderer,FileActions,_LayoutMixin,KeyboardNavigation,Selection,_State,_Widget) {
    
    /**
     * A grid feature
     * @class module:xgrid/GridActions
     */
    var Implementation = {

        },
        renderers = [ListRenderer,ThumbRenderer,TreeRenderer],
        multiRenderer = declare.classFactory('multiRenderer',{},renderers,MultiRenderer.Implementation);

    var GridClass = Grid.createGridClass('xfile.views.GridLight', Implementation, {
            SELECTION: {
                CLASS:Selection
            },
            KEYBOARD_SELECTION: true,
            COLUMN_HIDER: true,
            COLUMN_REORDER: false,
            ACTIONS:types.GRID_FEATURES_LITE.ACTIONS,
            ITEM_ACTIONS: {
                CLASS:FileActions
            },
            SPLIT:{
                CLASS:_LayoutMixin
            },
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            STATE:{
                CLASS:_State
            },
            WIDGET:{
                CLASS:_Widget
            }

        },
        {
            RENDERER: multiRenderer
        },
        {
            renderers: renderers,
            selectedRenderer: TreeRenderer
        }
    );
    GridClass.DEFAULT_RENDERERS = renderers;
    GridClass.DEFAULT_MULTI_RENDERER = multiRenderer;
    return GridClass;

});