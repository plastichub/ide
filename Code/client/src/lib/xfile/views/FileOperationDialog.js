/** @module xfile/views/FileOperationDialog **/
define([
    "dcl/dcl",
    'xide/types',
    "xide/views/_Dialog"
], function (dcl, types, _Dialog) {


    var Module = dcl(_Dialog, {

        title: '',
        type: types.DIALOG_TYPE.INFO,
        size: types.DIALOG_SIZE.SIZE_SMALL,
        bodyCSS: {},
        failedText: ' Failed!',
        successText: ': Success!',
        showSpinner: true,
        spinner: '  <span class="fa-spinner fa-spin"/>',
        notificationMessage: null,
        doOk: function (dfd) {

            this.onBeforeOk && this.onBeforeOk();

            var msg = this.showMessage(),
                thiz = this;

            dfd.then(function (result) {
                thiz._onSuccess(result);
            }, function (err) {
                thiz._onError();
            });

        },
        _onSuccess: function (title, suffix, message) {

            title = title || this.title;
            message = message || this.notificationMessage;

            message && message.update({
                message: title + this.successText + (suffix ? '<br/>' + suffix : ''),
                type: 'info',
                actions: false,
                duration: 1500
            });

            this.onSuccess && this.onSuccess();
        },
        _onError: function (title, suffix, message) {

            title = title || this.title;

            message = message || this.notificationMessage;

            message && message.update({
                message: title + this.failedText + (suffix ? '<br/>' + suffix : ''),
                type: 'error',
                actions: false,
                duration: 15000
            });


            this.onError && this.onError(suffix);

        },
        onOk: function () {

            var msg = this.showMessage(),
                thiz = this;
            this.doOk(this.getOkDfd());
        },
        showMessage: function (title) {

            if (this.notificationMessage) {
                return this.notificationMessage;
            }
            title = title || this.title;

            var msg = this.ctx.getNotificationManager().postMessage({
                message: title + (this.showSpinner ? this.spinner : ''),
                type: 'info',
                showCloseButton: true,
                duration: 4500
            });

            this.notificationMessage = msg;

            return msg;
        }

    });

    return Module;

});