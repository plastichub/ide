/** @module xfile/views/FileGrid **/
define([
    "xdojo/declare",
    'dojo/Deferred',
    'xide/types',
    'xide/utils',
    'xide/views/History',
    'xaction/DefaultActions',
    'xfile/views/Grid',
    'xfile/factory/Store',
    'xide/model/Path',
    'xfile/model/File',
    "xfile/types",
    'xlang/i18',
    'xide/$'
], function (declare, Deferred, types, utils, History, DefaultActions, Grid, factory, Path, File, fTypes, il8,$) {
    var DEFAULT_PERMISSIONS = fTypes.DEFAULT_FILE_GRID_PERMISSIONS;
    /**
     * A grid feature
     * @class module:xfile/views/FileGrid
     */
    var GridClass = declare('xfile.views.FileGrid', Grid, {
        resizeAfterStartup: true,
        formatters:{},
        hideExtensions:false,
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 50,
            'Settings': 20,
            'Navigation': 10,
            'Window': 5
        },
        groupOrder: {
            'Clipboard': 110,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'Navigation': 5,
            'Select': 0
        },
        tabOrder: {
            'Home': 100,
            'View': 50,
            'Settings': 20,
            'Navigation': 10
        },
        /**
         *
         */
        noDataMessage: '<span/>',
        /**
         * history {module:xide/views/History}
         */
        _history: null,
        options: utils.clone(types.DEFAULT_GRID_OPTIONS),
        _columns: {},
        toolbarInitiallyHidden: true,
        itemType: types.ITEM_TYPE.FILE,
        permissions: DEFAULT_PERMISSIONS,
        contextMenuArgs: {
            limitTo: null,
            actionFilter: {
                quick: true
            }
        },
        /**
         *
         * @param state
         * @returns {*}
         */
        setState: function (state) {
            this.inherited(arguments);
            var self = this,
                collection = self.collection,
                path = state.store.path,
                item = collection.getSync(path),
                dfd = self.refresh();

            dfd.then(function () {
                item = collection.getItem(path, true).then(function (item) {
                    self.openFolder(item);
                });
            });
            return dfd;
        },
        postMixInProperties: function () {
            var state = this.state;
            if (state) {
                if (state._columns) {
                    this._columns = state._columns;
                }
            }
            if (!this.columns) {
                this.columns = this.getColumns();
            }
            if (!this.collection && this.state) {
                var _store = this.state.store,
                    ctx = this.ctx,
                    store = factory.createFileStore(_store.mount, _store.storeOptions, ctx.config);
                this.collection = store.getDefaultCollection();
            }
            return this.inherited(arguments);
        },
        /**
         *
         * @param state
         * @returns {object}
         */
        getState: function (state) {
            state = this.inherited(arguments) || {};
            state.store = {
                mount: this.collection.mount,
                path: this.getCurrentFolder().path,
                storeOptions: this.collection.options
            };
            state._columns = {};
            _.each(this._columns, function (c) {
                state._columns[c.label] = !this.isColumnHidden(c.id);
            }, this);
            return state;
        },
        onSaveLayout: function (e) {
            var customData = e.data,
                gridState = this.getState(),
                data = {
                    widget: this.declaredClass,
                    state: gridState
                };
            customData.widgets.push(data);

            return customData;
        },
        formatColumn: function (field, value, obj) {
            var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
            if (renderer.formatColumn) {
                var result = renderer.formatColumn.apply(arguments);
                if (result) {
                    return result;
                }
            }
            if (obj.renderColumn) {
                var rendered = obj.renderColumn.apply(this, arguments);
                if (rendered) {
                    return rendered;
                }
            }
            switch (field) {
                case "fileType": {
                    if (value == 'folder') {
                        return il8.localize('kindFolder');
                    } else {
                        if (obj.mime) {
                            var mime = obj.mime.split('/')[1] || "unknown";
                            var key = 'kind' + mime.toUpperCase();
                            var _translated = il8.localize(key);
                            return key !== _translated ? _translated : value;
                        }
                    }
                }
                case "name": {
                    var directory = obj && obj.directory != null && obj.directory === true;
                    var no_access = obj.read === false && obj.write === false;
                    var isBack = obj.name == '..';
                    var folderClass = 'fa-folder';
                    var icon = '';
                    var imageClass = '';
                    var useCSS = false;
                    if (directory) {
                        if (isBack) {
                            imageClass = 'fa fa-level-up itemFolderList';
                            useCSS = true;
                        } else if (!no_access) {
                            imageClass = 'fa ' + folderClass + ' itemFolderList';

                            useCSS = true;
                        } else {
                            imageClass = 'fa fa-lock itemFolderList';
                            useCSS = true;
                        }
                    } else {
                        if (!no_access) {
                            imageClass = 'itemFolderList fa ' + utils.getIconClass(obj.path);
                            useCSS = true;
                        } else {
                            imageClass = 'fa fa-lock itemFolderList';
                            useCSS = true;
                        }
                    }
                    var label = obj.showPath === true ? obj.path : value;
                        if (this.hideExtensions) {
                            label = utils.pathinfo(label).filename;
                        }

                    if (!useCSS) {
                        return '<img class="fileGridIconCell" src="' + icon + ' "/><span class="fileGridNameCell">' + label + '</span>';
                    } else {
                        return '<span class=\"' + imageClass + '\""></span><span class="name fileGridNameNode" style="vertical-align: middle;padding-top: 0px">' + label + '</span>';
                    }
                }
                case "sizeBytes": {
                    return obj.size;
                }
                case "fileType": {
                    return utils.capitalize(obj.fileType || 'unknown');
                }
                case "mediaInfo": {
                    return obj.mediaInfo || 'unknown';
                }
                case "owner": {
                    if (obj) {
                        var owner = obj.owner;
                        if (owner && owner.user) {
                            return owner.user.name;
                        }
                    }
                    return "";
                }
                case "modified": {
                    if (value === '') {
                        return value;
                    }
                    var directory = !obj.directory == null;
                    if (!directory) {
                        if(il8.translations.dateFormat){
                            return il8.formatDate(value).replace('ms', '');
                        }
                    }
                    return '';
                }
            }
            return value;
        },
        /**
         *
         * @param formatters
         * @returns {object|null}
         */
        getColumns: function (formatters) {
            formatters = formatters || this.formatters;
            var self = this;
            this.columns = [];
            function createColumn(label, field, sortable, hidden) {
                if (self._columns[label] != null) {
                    hidden = !self._columns[label];
                }
                self.columns.push({
                    renderExpando: label === 'Name',
                    label: label,
                    field: field,
                    sortable: sortable,
                    formatter: function (value, obj) {
                        return label in formatters ? formatters[label].apply(self,[field, value, obj]) : self.formatColumn(field, value, obj);
                    },
                    hidden: hidden
                });
            }
            createColumn('Name', 'name', true, false);
            createColumn('Type', 'fileType', true, true);
            createColumn('Path', 'path', true, true);
            createColumn('Size', 'sizeBytes', true, false);
            createColumn('Modified', 'modified', true, false);
            createColumn('Owner', 'owner', true, true);
            createColumn('Media', 'mediaInfo', true, true);
            return this.columns;
        },
        _focus: function () {
            var rows = this.getRows();
            if (rows[0]) {
                this.focus(this.row(rows[0]));
            }
        },
        /**
         *
         * @param item
         * @returns {boolean}
         */
        setQueryEx: function (item) {
            if (!item || !item.directory) {
                return false;
            }
            this._lastPath = item.getPath();
            var self = this, dfd = new Deferred();
            var col = self.collection;
            if (item.path === '.') {
                col.resetQueryLog();
                self.set("collection", col.getDefaultCollection(item.getPath()));
                dfd.resolve();
            } else {
                col.open(item).then(function (items) {
                    col.resetQueryLog();
                    self.set("collection", col.getDefaultCollection(item.getPath()));
                    dfd.resolve(items);
                });
            }
            return dfd;
        },
        getCurrentFolder: function () {
            var renderer = this.getSelectedRenderer();
            if (renderer && renderer.getCurrentFolder) {
                var _result = renderer.getCurrentFolder.apply(this);
                if (_result) {
                    if (_result.isBack) {
                        var __result = this.collection.getSync(_result.rPath);
                        if (__result) {
                            _result = __result;
                        }
                    }
                    return _result;
                }
            }

            var item = this.getRows()[0];
            if (item && (item._S || item._store)) {
                if (item.isBack === true) {
                    var _now = this.getHistory().getNow();
                    if (_now) {
                        return this.collection.getSync(_now);
                    }
                }
                //current folder:
                var _parent = item._S.getParent(item);
                if (_parent) {
                    return _parent;
                }
            }
            return null;
        },
        getClass: function () {
            return GridClass;
        },
        getHistory: function () {
            if (!this._history) {
                this._history = new History();
            }
            return this._history;
        },
        _createBackItem:function(path){
            return {
                name: '..',
                path: '..',
                rPath: path,
                sizeBytes: 0,
                size: '',
                icon: 'fa-level-up',
                isBack: true,
                modified: '',
                _S: this.collection,
                directory: true,
                _EX: true,
                children: [],
                mayHaveChildren: false
            }
        },
        startup: function () {
            if (this._started) {
                return;
            }
            var res = this.inherited(arguments);
            $(this.domNode).addClass('xfileGrid');
            this.set('loading', true);
            if (this.permissions) {
                this.addActions([].concat(DefaultActions.getDefaultActions(this.permissions, this, this).concat(this.getFileActions(this.permissions))));
            }
            this._history = new History();
            var self = this;
            this.subscribe(types.EVENTS.ON_CLIPBOARD_COPY, function (evt) {
                if (evt.type === self.itemType) {
                    self.currentCopySelection = evt.selection;
                    self.refreshActions();
                }
            });
            self._on('noData', function () {
                if (self._total > 0) {
                    return;
                }
                var _history = self._history,
                    now = _history.getNow();

                if (!now || now === './.') {
                    return;
                }
                self.renderArray([this._createBackItem(now)]);
            });
            this.on('dgrid-refresh-complete', function () {
                var rows = self.getRows();
                if (rows && rows.length > 1) {
                    var back = _.find(rows, {
                        isBack: true
                    });
                    if (back) {
                        self.removeRow(back);
                        self.refresh();
                    }
                }
            });

            this._on('openFolder', function (evt) {
                self.set('title', evt.item.name);
            });

            if (self.selectedRenderer) {
                res = this.refresh();
                res && res.then(function () {
                    self.set('loading', false);
                    self.setRenderer(self.selectedRenderer, false);
                });
            }
            this._on('onChangeRenderer',this.refresh);
            this._emit('startup');
            res.then(this.resize.bind(this));
            return res;
        }
    });

    /**
     *
     * @param ctx
     * @param args
     * @param parent
     * @param register
     * @param startup
     * @param store
     * @returns {module:xfile/views/Grid}
     */
    function createDefault(ctx, args, parent, register, startup, store) {
        var defaults = {
            collection: store.getDefaultCollection(),
            _parent: parent,
            Module: GridClass,
            ctx: ctx
        };
        utils.mixin(defaults, args || {});
        var grid = utils.addWidget(GridClass, defaults, null, parent, startup, null, null, true, null);
        register &&ctx.getWindowManager().registerView(grid, false);
        return grid;
    }

    GridClass.prototype.Module = GridClass;
    GridClass.Module = GridClass;
    GridClass.createDefault = createDefault;
    GridClass.DEFAULT_PERMISSIONS = DEFAULT_PERMISSIONS;
    return GridClass;
});