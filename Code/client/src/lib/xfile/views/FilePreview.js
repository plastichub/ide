define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "xide/mixins/EventedMixin",
    "xide/widgets/_Widget",
    "dojo/cache",
    "dojo/Deferred",
    "dijit/registry",
    "xide/editor/Default",
    "xaction/DefaultActions"
], function (dcl, types,
             utils, EventedMixin,
             _Widget, cache, Deferred, registry, Default, DefaultActions) {

    var plugins = [
        /**
         * Images preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var mimes = ['image/jpeg', 'image/png', 'image/gif'],
                preview = ql.preview;

            // what kind of images we can display
            $.each(navigator.mimeTypes, function (i, o) {
                var mime = o.type;

                if (mime.indexOf('image/') === 0 && $.inArray(mime, mimes)) {
                    mimes.push(mime);
                }
            });

            preview.bind('update', function (e) {
                var file = e.file,
                    img;
                if (!ql.opened()) {
                    return;
                }

                if ($.inArray(file.mime, mimes) !== -1) {
                    // this is our file - stop event propagation
                    e.stopImmediatePropagation();
                    var params = null;
                    if (ql.resizeToPreview) {
                        params = {
                            width: preview.width()
                        }
                    }
                    img = $('<img/>')
                        .hide()
                        .appendTo(preview)
                        .load(function () {
                            // timeout - because of strange safari bug -
                            // sometimes cant get image height 0_o
                            setTimeout(function () {
                                var prop = (img.width() / img.height()).toFixed(2);
                                preview.bind('changesize', function () {
                                    var pw = parseInt(preview.width()),
                                        ph = parseInt(preview.height()),
                                        w, h;

                                    if (prop < (pw / ph).toFixed(2)) {
                                        h = ph;
                                        w = Math.floor(h * prop);
                                    } else {
                                        w = pw;
                                        h = Math.floor(w / prop);
                                    }
                                    img.width(w).height(h).css('margin-top', h < ph ? Math.floor((ph - h) / 2) : 0);

                                }).trigger('changesize');

                                // hide info/icon
                                ql.hideinfo();
                                //show image
                                img.fadeIn(100);

                            }, 1)
                        });
                    img.attr('src', ql.fm.url(file, null, params));
                }
            });
        },

        /**
         * HTML preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var mimes = ['text/html', 'application/xhtml+xml'],
                preview = ql.preview,
                fm = ql.fm;
            preview.bind('update', function (e) {
                var file = e.file, jqxhr;

                if (!ql.opened()) {
                    return;
                }

                if ($.inArray(file.mime, mimes) !== -1) {
                    e.stopImmediatePropagation();
                    var ctx = ql.ctx;
                    var fm = ctx.getFileManager();
                    fm.getContent(file.mount, file.path, function (content) {
                        ql.hideinfo();
                        doc = $('<iframe class="elfinder-quicklook-preview-html"/>').appendTo(preview)[0].contentWindow.document;
                        doc.open();
                        doc.write(content);
                        doc.close();
                    });
                }
            })
        },

        /**
         * Texts preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var fm = ql.fm,
                preview = ql.preview;


            var mimes = [
                'application/x-empty',
                'application/javascript',
                'application/xhtml+xml',
                'audio/x-mp3-playlist',
                'application/x-web-config',
                'application/docbook+xml',
                'application/x-php',
                'application/x-perl',
                'application/x-awk',
                'application/x-config',
                'application/x-csh',
                'application/xml',
                'application/x-empty',
                'text/html',
                'text/x-c',
                'text/x-php',
                'text/plain',
                'text/x-c++',
                'text/x-lisp'
            ];

            preview.bind('update', function (e) {
                var file = e.file,
                    mime = file.mime,
                    jqxhr;

                if (!ql.opened()) {
                    return;
                }
                if (mime.indexOf('text/') === 0 || $.inArray(mime, mimes) !== -1) {
                    e.stopImmediatePropagation();
                    if (ql.useAce) {
                        ql.hideinfo();
                        var _node = $('<div class="elfinder-quicklook-preview-text-wrapper"></div>');

                        _node.appendTo(preview);

                        var editor = ql._editor,
                            wasCached = editor;

                        if (editor) {

                        } else {
                            editor = Default.Implementation.open(file, _node[0], ql.editorOptions, false, ql);
                            ql._editor = editor;
                            //add to _widgets
                            ql.add(editor, null, false);

                            if (DefaultActions.hasAction(ql.editorOptions.permissions, types.ACTION.TOOLBAR)) {
                                var toolbar = editor.getToolbar();
                                if (toolbar) {
                                    $(toolbar.domNode).addClass('bg-opaque');
                                }
                            }
                        }
                        if (wasCached) {
                            _node.append(editor.domNode);
                            editor.set('item', file);
                        }

                        preview.bind('changesize', function () {
                            var pw = parseInt(preview.width()),
                                ph = parseInt(preview.height());
                            utils.resizeTo(editor.domNode, preview[0], true, true);
                            editor.resize();
                        });
                    }
                }
            });
        },
        /**
         * PDF preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var fm = ql.fm,
                mime = 'application/pdf',
                preview = ql.preview,
                active = false;

            active = false;
            var isActive = false;
            if (isActive) {
                active = true;
            } else {
                $.each(navigator.plugins, function (i, plugins) {
                    $.each(plugins, function (i, plugin) {
                        if (plugin.type == mime) {
                            return !(active = true);
                        }
                    });
                });
            }
            active && preview.bind('update', function (e) {
                var file = e.file, node;
                if (!ql.opened()) {
                    return;
                }

                if (file.mime == mime) {
                    e.stopImmediatePropagation();
                    preview.one('change', function () {
                        node.unbind('load').remove();
                    });

                    node = $('<iframe class="elfinder-quicklook-preview-pdf"/>')
                        .hide()
                        .appendTo(preview)
                        .load(function () {
                            ql.hideinfo();
                            node.show();
                        })
                        .attr('src', ql.fm.url(file));
                }
            })
        },

        /**
         * Flash preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var fm = ql.fm,
                mime = 'application/x-shockwave-flash',
                preview = ql.preview,
                active = false;

            $.each(navigator.plugins, function (i, plugins) {
                $.each(plugins, function (i, plugin) {
                    if (plugin.type == mime) {
                        return !(active = true);
                    }
                });
            });

            active && preview.bind('update', function (e) {
                var file = e.file,
                    node;
                if (!ql.opened()) {
                    return;
                }
                if (file.mime == mime) {
                    e.stopImmediatePropagation();
                    ql.hideinfo();
                    preview.append((node = $('<embed class="elfinder-quicklook-preview-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="' + fm.url(file) + '" quality="high" type="application/x-shockwave-flash" />')));
                }
            });
        },
        /**
         * HTML5 audio preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var preview = ql.preview,
                autoplay = !!ql.options['autoplay'],
                mimes = {
                    'audio/mpeg': 'mp3',
                    'audio/mpeg3': 'mp3',
                    'audio/mp3': 'mp3',
                    'audio/x-mpeg3': 'mp3',
                    'audio/x-mp3': 'mp3',
                    'audio/x-wav': 'wav',
                    'audio/wav': 'wav',
                    'audio/x-m4a': 'm4a',
                    'audio/aac': 'm4a',
                    'audio/mp4': 'm4a',
                    'audio/x-mp4': 'm4a',
                    'audio/ogg': 'ogg'
                },
                node;

            preview.bind('update', function (e) {
                if (!ql.opened()) {
                    return;
                }
                var file = e.file,
                    type = mimes[file.mime];

                if (ql.support.audio[type]) {
                    e.stopImmediatePropagation();

                    node = $('<audio class="elfinder-quicklook-preview-audio" controls preload="auto" autobuffer><source src="' + ql.fm.url(file) + '" /></audio>')
                        .appendTo(preview);
                    autoplay && node[0].play();
                }
            }).bind('change', function () {
                if (node && node.parent().length) {
                    node[0].pause();
                    node.remove();
                    node = null;
                }
            });
        },

        /**
         * HTML5 video preview plugin
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {

            var preview = ql.preview,
                autoplay = !!ql.options['autoplay'],
                mimes = {
                    'video/mp4': 'mp4',
                    'video/x-m4v': 'mp4',
                    'video/ogg': 'ogg',
                    'application/ogg': 'ogg',
                    'video/webm': 'webm'
                },
                node;

            preview.bind('update', function (e) {
                if (!ql.opened()) {
                    return;
                }
                var file = e.file,
                    type = mimes[file.mime];

                if (ql.support.video[type]) {
                    e.stopImmediatePropagation();

                    ql.hideinfo();
                    node = $('<video class="elfinder-quicklook-preview-video" controls preload="auto" autobuffer><source src="' + ql.fm.url(file) + '" /></video>').appendTo(preview);
                    autoplay && node[0].play();

                }
            }).bind('change', function () {
                if (node && node.parent().length) {
                    node[0].pause();
                    node.remove();
                    node = null;
                }
            });
        },
        /**
         * Audio/video preview plugin using browser plugins
         *
         * @param elFinder.commands.quicklook
         **/
            function (ql) {
            var preview = ql.preview,
                mimes = [],
                node;

            if (!ql.opened()) {
                return;
            }

            $.each(navigator.plugins, function (i, plugins) {
                $.each(plugins, function (i, plugin) {
                    (plugin.type.indexOf('audio/') === 0 || plugin.type.indexOf('video/') === 0) && mimes.push(plugin.type);
                });
            });

            preview.bind('update', function (e) {
                if (!ql.opened()) {
                    return;
                }
                var file = e.file,
                    mime = file.mime,
                    video;
                if ($.inArray(file.mime, mimes) !== -1) {
                    e.stopImmediatePropagation();
                    (video = mime.indexOf('video/') === 0) && ql.hideinfo();
                    node = $('<embed src="' + ql.fm.url(file) + '" type="' + mime + '" class="elfinder-quicklook-preview-' + (video ? 'video' : 'audio') + '"/>')
                        .appendTo(preview);
                }
            }).bind('change', function () {
                if (node && node.parent().length) {
                    node.remove();
                    node = null;
                }
            });
        }
    ];
    /**
     * window closed state
     *
     * @type Number
     **/
    var closed = 0,
        /**
         * window opened state
         *
         * @type Number
         **/
        opened = 2,
        /**
         * window animated state
         *
         * @type Number
         **/
        animated = 1;

    var previewClass = dcl([_Widget.dcl, EventedMixin.dcl], {
        resizeToPreview: true,
        useAce: true,
        registerEditors: false,
        editorPermissions: [],
        container: null,
        destroy: function () {
            this.window.empty();
            utils.destroy(this.window[0]);
            this.inherited(arguments);
        },
        /**
         * Init command.
         * Add default plugins and init other plugins
         *
         * @return Object
         **/
        init: function () {
            this.options = {
                autoplay: true,
                jplayer: "extensions/jplayer",
                ui: 'button'
            };
            var o = this.options,
                self = this,
                win = this.window,
                preview = this.preview,
                container = this.container,
                i, p;

            width = o.width > 0 ? parseInt(o.width) : 450;
            height = o.height > 0 ? parseInt(o.height) : 300;
            if (!container) {
                win.appendTo('body').zIndex(20);
            }
            // close window on escape
            $(document).keydown(function (e) {
                e.keyCode == 27 && self.opened() && win.trigger('close')
            });

            if (!container && $.fn.resizable) {
                win.resizable({
                    handles: 'se',
                    minWidth: 350,
                    minHeight: 120,
                    resize: function () {
                        // use another event to avoid recursion in fullscreen mode
                        // may be there is clever solution, but i cant find it :(
                        preview.trigger('changesize');
                    }
                });
            }
            $.each(plugins || [], function (i, plugin) {
                if (typeof(plugin) == 'function') {
                    new plugin(self)
                }
            });

            preview.bind('update', function () {
                self.info.show();
            });
        },
        buildRenderingEmbedded: function () {
            var self = this,
                navicon = this.navicon,
                container = this.container ? $(this.container) : null,
                title = $('<div class="elfinder-quicklook-title" style="display: none">' + self.title + '</div>'),
                icon = $('<div/>'),
                info = $('<div class="elfinder-quicklook-info"/>');//.hide(),


            self.openDfd = new Deferred();

            self.fsicon = $('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrows-alt"/>')
                .mousedown(function (e) {
                    var win = self.window,
                        full = win.is('.' + self.fullscreen),
                        scroll = "scroll.elfinder-finder",
                        $window = $(window);

                    e.stopPropagation();

                    if (full) {
                        win.css(win.data('position')).unbind('mousemove');
                        $window.unbind(scroll).trigger(self.resize).unbind(self.resize);
                        self.navbar.unbind('mouseenter').unbind('mousemove');
                    } else {

                        win.data('position', {
                            left: win.css('left'),
                            top: win.css('top'),
                            width: win.width(),
                            height: win.height()
                        }).css({
                            width: '100%',
                            height: '100%'
                        });

                        $(window).bind(scroll, function () {
                                win.css({
                                    left: parseInt($(window).scrollLeft()) + 'px',
                                    top: parseInt($(window).scrollTop()) + 'px'
                                })
                            })
                            .bind(self.resize, function (e) {
                                self.preview.trigger('changesize');
                            })
                            .trigger(scroll)
                            .trigger(self.resize);

                        win.bind('mousemove', function (e) {
                                self.navbar.stop(true, true).show().delay(3000).fadeOut('slow');
                            })
                            .mousemove();

                        self.navbar.mouseenter(function () {
                                self.navbar.stop(true, true).show();
                            })
                            .mousemove(function (e) {
                                e.stopPropagation();
                            });
                    }

                    self.navbar.attr('style', '').draggable(full ? 'destroy' : {});
                    win.toggleClass(self.fullscreen);
                    $(this).toggleClass(navicon + '-fullscreen-off');
                    if (!parent) {
                        parent = $('.xapp');
                    }
                });

            self.navbar = $('<div class="" style="display: none;"/>');
            self.preview = $('<div class="elfinder-quicklook-preview ui-helper-clearfix"/>')
            // clean info/icon
                .bind('change', function (e) {
                    self.info.attr('style', '').hide();
                    icon.removeAttr('class').attr('style', '');
                    info.html('');

                })
                // update info/icon
                .bind('update', function (e) {
                    var fm = self.fm,
                        preview = self.preview,
                        file = e.file,
                        tpl = '<div class="elfinder-quicklook-info-data">{value}</div>',
                        tmb;

                    if (file) {
                        !file.read && e.stopImmediatePropagation();
                        //self.window.data('hash', file.path);
                        self.preview.unbind('changesize').trigger('change').children().remove();
                        self.info.delay(100).fadeIn(10);

                    } else {
                        e.stopImmediatePropagation();
                    }
                });


            self.info = $('<div class="elfinder-quicklook-info-wrapper"/>')
                .append(icon)
                .append(info);


            self.window = $('<div id="' + self.id + '" class="ui-helper-reset ui-widget elfinder-quicklook widget bg-opaque" style=""/>')
                .click(function (e) {
                    e.stopPropagation();
                })
                .append(self.preview)
                .append(self.info.hide())
                .bind('open', function (e) {
                    self._open = true;
                    var win = self.window,
                        file = self.value,
                        node = self.node;

                    if (self.closed()) {
                        self.update(1, self.value);
                        self.navbar.attr('style', '');
                        self.state = animated;
                        node && node.trigger('scrolltoview');
                        win.css(self.closedCss(node))
                            .show()
                            .animate(self.openedCss(), 550, function () {
                                self.state = opened;
                                self.update(2, self.value);
                                self.openDfd.resolve();
                            });
                    }
                })
                .bind('close', function (e) {
                    var win = self.window,
                        preview = self.preview.trigger('change'),
                        file = self.value,
                        node = {},
                        close = function () {
                            self._open = false;
                            self.state = closed;
                            win.hide();
                            preview.children().remove();
                            self.update(0, self.value);

                        };

                    if (self.opened()) {
                        state = animated;
                        win.is('.' + self.fullscreen) && self.fsicon.mousedown();
                        node.length
                            ? win.animate(self.closedCss(node), 500, close)
                            : close();
                    }
                });

            if (container) {
                container.append(self.window);
            }
            var support = this.supportTest;
            self.support = {

                audio: {
                    ogg: support('audio/ogg; codecs="vorbis"'),
                    mp3: support('audio/mpeg;'),
                    wav: support('audio/wav; codecs="1"'),
                    m4a: support('audio/x-m4a;') || support('audio/aac;')
                },
                video: {
                    ogg: support('video/ogg; codecs="theora"'),
                    webm: support('video/webm; codecs="vp8, vorbis"'),
                    mp4: support('video/mp4; codecs="avc1.42E01E"') || support('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')
                }
            };


        },
        buildRendering: function () {
            var self = this,
                navicon = this.navicon,
                container = this.container ? $(this.container) : null,
                title = $('<div class="elfinder-quicklook-title">' + self.title + '</div>'),
                icon = $('<div/>'),
                info = $('<div class="elfinder-quicklook-info"/>');//.hide(),


            self.openDfd = new Deferred();

            self.fsicon = $('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrows-alt"/>')
                .mousedown(function (e) {
                    var win = self.window,
                        full = win.is('.' + self.fullscreen),
                        scroll = "scroll.elfinder-finder",
                        $window = $(window);

                    e.stopPropagation();

                    if (full) {
                        win.css(win.data('position')).unbind('mousemove');
                        $window.unbind(scroll).trigger(self.resize).unbind(self.resize);
                        self.navbar.unbind('mouseenter').unbind('mousemove');
                    } else {

                        win.data('position', {
                            left: win.css('left'),
                            top: win.css('top'),
                            width: win.width(),
                            height: win.height()
                        }).css({
                            width: '100%',
                            height: '100%'
                        });

                        $(window).bind(scroll, function () {
                                win.css({
                                    left: parseInt($(window).scrollLeft()) + 'px',
                                    top: parseInt($(window).scrollTop()) + 'px'
                                })
                            })
                            .bind(self.resize, function (e) {
                                self.preview.trigger('changesize');
                            })
                            .trigger(scroll)
                            .trigger(self.resize);

                        win.bind('mousemove', function (e) {
                                self.navbar.stop(true, true).show().delay(3000).fadeOut('slow');
                            })
                            .mousemove();

                        self.navbar.mouseenter(function () {
                                self.navbar.stop(true, true).show();
                            })
                            .mousemove(function (e) {
                                e.stopPropagation();
                            });
                    }

                    self.navbar.attr('style', '').draggable(full ? 'destroy' : {});
                    win.toggleClass(self.fullscreen);
                    $(this).toggleClass(navicon + '-fullscreen-off');
                    if (!parent) {
                        parent = $('.xapp');
                    }
                });


            self.navbar = $('<div class="elfinder-quicklook-navbar"/>')
                .append($('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrow-left"/>').mousedown(function () {
                    self.navtrigger(37);
                }))
                .append(self.fsicon)
                .append($('<div class="' + navicon + ' ' + navicon + ' fa-2x fa-arrow-right"/>').mousedown(function () {
                    self.navtrigger(39);
                }))
                .append('<div class="elfinder-quicklook-navbar-separator"/>')
                .append($('<div class="' + navicon + ' ' + navicon + '-close"/>').mousedown(function () {
                    self.window.trigger('close');
                }));


            self.preview = $('<div class="elfinder-quicklook-preview ui-helper-clearfix"/>')
            // clean info/icon
                .bind('change', function (e) {
                    self.info.attr('style', '').hide();
                    icon.removeAttr('class').attr('style', '');
                    info.html('');
                })
                // update info/icon
                .bind('update', function (e) {
                    var fm = self.fm,
                        preview = self.preview,
                        file = e.file,
                        tpl = '<div class="elfinder-quicklook-info-data">{value}</div>',
                        tmb;

                    if (file) {
                        !file.read && e.stopImmediatePropagation();
                        //self.window.data('hash', file.path);
                        self.preview.unbind('changesize').trigger('change').children().remove();
                        self.info.delay(100).fadeIn(10);

                    } else {
                        e.stopImmediatePropagation();
                    }
                });

            self.info = $('<div class="elfinder-quicklook-info-wrapper"/>')
                .append(icon)
                .append(info);


            self.window = $('<div id="' + self.id + '" class="ui-helper-reset ui-widget elfinder-quicklook widget bg-opaque" style="position:absolute"/>')
                .click(function (e) {
                    e.stopPropagation();
                })
                .append(
                    $('<div class="elfinder-quicklook-titlebar"/>')
                        .append(title)
                        .append($('<span class="fa-close" style="margin-left: 4px; color: white; font-size: 16px; text-align: center; position: absolute; left: 0px; top: 2px;"/>').mousedown(function (e) {
                                e.stopPropagation();
                                self.window.trigger('close');
                            }
                            )
                        )
                )
                .append(self.preview.add(self.navbar))
                .append(self.info.hide())
                .draggable({handle: 'div.elfinder-quicklook-titlebar'})
                .bind('open', function (e) {

                    self._open = true;

                    var win = self.window,
                        file = self.value,
                        node = self.node;

                    if (self.closed()) {
                        self.update(1, self.value);
                        self.navbar.attr('style', '');
                        self.state = animated;
                        node.trigger('scrolltoview');
                        win.css(self.closedCss(node))
                            .show()
                            .animate(self.openedCss(), 550, function () {
                                self.state = opened;
                                self.update(2, self.value);
                                self.openDfd.resolve();
                            });
                    }
                })
                .bind('close', function (e) {

                    var win = self.window,
                        preview = self.preview.trigger('change'),
                        file = self.value,
                        node = {},
                        close = function () {
                            self._open = false;
                            self.state = closed;
                            win.hide();
                            preview.children().remove();
                            self.update(0, self.value);

                        };

                    if (self.opened()) {
                        state = animated;
                        win.is('.' + self.fullscreen) && self.fsicon.mousedown();
                        node.length
                            ? win.animate(self.closedCss(node), 500, close)
                            : close();
                    }
                });


            if (container) {
                container.append(self.window);
            }
            var support = this.supportTest;
            self.support = {

                audio: {
                    ogg: support('audio/ogg; codecs="vorbis"'),
                    mp3: support('audio/mpeg;'),
                    wav: support('audio/wav; codecs="1"'),
                    m4a: support('audio/x-m4a;') || support('audio/aac;')
                },
                video: {
                    ogg: support('video/ogg; codecs="theora"'),
                    webm: support('video/webm; codecs="vp8, vorbis"'),
                    mp4: support('video/mp4; codecs="avc1.42E01E"') || support('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')
                }
            };


        },
        constructor: function (args) {
            utils.mixin(this, args);
        },
        editorOptions: {
            permissions: [
                types.ACTION.TOOLBAR,
                types.ACTION.RELOAD,
                'Editor/Settings',
                'View/Increase Font Size',
                'View/Decrease Font Size',
                'View/Themes',
                'File/Search'
            ]
        },
        title: 'Preview',
        /**
         * Opened window width (from config)
         *
         * @type Number
         **/
        width: 450,
        /**
         * Opened window height (from config)
         *
         * @type Number
         **/
        height: 300,
        fm: null,
        /**
         * window state
         *
         * @type Number
         **/
        state: 0,
        /**
         * navbar icon class
         *
         * @type Number
         **/
        navicon: 'elfinder-quicklook-navbar-icon',
        /**
         * navbar "fullscreen" icon class
         *
         * @type Number
         **/
        fullscreen: 'elfinder-quicklook-fullscreen',
        /**
         * elFinder node
         *
         * @type jQuery
         **/
        parent: null,
        /**
         * elFinder current directory node
         *
         * @type jQuery
         **/

        cwd: null,
        resize: "resize.elfinder-finder",
        /**
         * This command cannot be disable by backend
         *
         * @type Boolean
         **/
        alwaysEnabled: true,
        /**
         * Selected file
         *
         * @type Object
         **/
        value: null,
        handlers: {
            // save selected file
            select: function () {
                this.update(void(0), this.fm.selectedFiles()[0]);
            },
            error: function () {
                self.window.is(':visible') && self.window.data('hash', '').trigger('close');
            },
            'searchshow searchhide': function () {
                this.opened() && this.window.trigger('close');
            }
        },
        shortcuts: [
            {
                pattern: 'space'
            }
        ],
        /**
         * Triger keydown/keypress event with left/right arrow key code
         *
         * @param  Number  left/right arrow key code
         * @return void
         **/
        navtrigger: function (code) {
            $(document).trigger($.Event('keydown', {
                keyCode: code,
                ctrlKey: false,
                shiftKey: false,
                altKey: false,
                metaKey: false
            }));
        },
        /**
         * Return css for closed window
         *
         * @param  jQuery  file node in cwd
         * @return void
         **/
        closedCss: function (node) {
            return {
                opacity: 0,
                width: 20,//node.width(),
                height: 20,
                top: node ? node.offset().top + 'px' : 0,
                left: node ? node.offset().left + 'px' : 0
            }
        },
        /**
         * Return css for opened window
         *
         * @return void
         **/
        openedCss: function () {

            var container = this.container ? $(this.container) : null,
                win = container || $(window);

            var w = container ? "100%" : Math.min(this.width, win.   width() - 10);
            var h = container ? "100%" : Math.min(this.height, win.height() - 80);
            return {
                opacity: 1,
                width: w,
                height: h,
                top: parseInt((win.height() - h - 60) / 2 + win.scrollTop()),
                left: parseInt((win.width() - w) / 2 + win.scrollLeft())
            }
        },
        supportTest: function (codec) {
            var media = document.createElement(codec.substr(0, codec.indexOf('/'))),
                value = false;

            try {
                value = media.canPlayType && media.canPlayType(codec);
            } catch (e) {

            }

            return value && value !== '' && value != 'no';
        },
        changeView: function (item) {
            if (this.currentView) {
                utils.destroy(this.currentView);
                if (this.currentView.destroy) {
                    this.currentView.destroy();
                }
            }
        },
        update: function (state) {
            this._open = state;
            this._emit('changeState', state);
            if (state == animated && this.onAnimate) {
                this.onAnimate();
            }
            if (state == opened && this.onOpened) {
                this.onOpened();
            }
            if (state == closed && this.onClosed) {
                this.onClosed();
            }
        },
        /**
         * Return true if quickLoock window is visible and not animated
         *
         * @return Boolean
         **/
        closed: function () {
            return this.state == closed;
        },
        /**
         * Return true if quickLoock window is hidden
         *
         * @return Boolean
         **/
        opened: function () {
            return this.state == opened;
        },
        /**
         * Attach listener to events
         * To bind to multiply events at once, separate events names by space
         *
         * @param  String  event(s) name(s)
         * @param  Object  event handler
         * @return elFinder
         */
        bind: function (event, callback) {
            var i;
            if (typeof(callback) == 'function') {
                event = ('' + event).toLowerCase().split(/\s+/);

                for (i = 0; i < event.length; i++) {
                    if (listeners[event[i]] === void(0)) {
                        listeners[event[i]] = [];
                    }
                    listeners[event[i]].push(callback);
                }
            }
            return this;
        },
        one: function (event, callback) {
            var self = this,
                h = $.proxy(callback, function (event) {
                    setTimeout(function () {
                        self.unbind(event.type, h);
                    }, 3);
                    return callback.apply(this, arguments);
                });
            return this.bind(event, h);
        },
        open: function (e) {
            var self = this,
                win = self.window,
                file = self.value,
                node = self.node;
            self._open = true;
            self.openDfd = new Deferred();
            if (self.closed()) {

                self.navbar.attr('style', '');
                self._open = true;
                self.state = animated;
                node && node.trigger('scrolltoview');
                win.css(self.closedCss(node))
                    .show()
                    .animate(self.openedCss(), 550, function () {
                        self.state = opened;
                        self.update(1, self.value);
                        self.openDfd.resolve();
                    });
            } else {
                self.openDfd.resolve();
            }
            return self.openDfd;
        },
        close: function (e) {
            this._open = false;
            var self = this,
                win = self.window,
                preview = self.preview.trigger('change'),
                file = self.value,
                node = self.node,
                close = function () {
                    self.state = closed;
                    win.hide();
                    preview.children().remove();
                    self.update(0, self.value);
                    self.closeDfd && self.closeDfd.resolve();
                };

            if (self.opened()) {
                self.state = animated;
                win.is('.' + self.fullscreen) && self.fsicon.mousedown();
                node && node.length
                    ? win.animate(self.closedCss(node), 500, close)
                    : close();
            }


            return self.closeDfd;
        },
        getstate: function () {
            return this.state == opened ? 1 : 0;
        },

        exec: function () {
            this.window.trigger(this.opened() ? 'close' : 'open');
        },
        hideinfo: function () {
            this.info.stop(true).hide();
        }
    });
    var filePreviewClass = dcl(previewClass, {
        destroy: function () {
            this.close();
            this.preview.trigger($.Event('update', {file: null}));
            this.inherited(arguments);
            utils.destroy(this._editor);
            utils.destroy(this.preview);
            utils.destroy(this.window);

        },
        onOpened: function () {
            var self = this;
            self.fm = {
                url: function (what, cache, params) {
                    return self.delegate.getImageUrl(what, cache, params);
                }
            }
            self.preview.trigger($.Event('update', {file: self.item}));
        },
        onClosed: function () {
        },
        onAnimate: function () {
        }
    });

    var editorClass = dcl(filePreviewClass, {
        resize: function () {
            this.preview.trigger('changesize');
        },
        constructor: function (args) {
            this.parent = $(args._parent.containerNode);
            this.container = args._parent.containerNode;
            this.resizeToParent = true;
            this.delegate = args.ctx.getFileManager();
            this.item = args.item;
            this.buildRenderingEmbedded();
            console.log('this',this);
            this.domNode = this.info[0];
        },
        openItem: function (item) {
            this.id = utils.createUUID();
            registry.add(this);
            this.item = item || this.item;

            this.init();
            this.exec();
            this.window.css({
                height: '100%',
                width: '100%'
            });
            this._parent.add(this, null, false);
            this.preview.trigger($.Event('update', {file: this.item}));
            this._parent.resize();
            this.resize();
        }
    });

    filePreviewClass.EditorClass = editorClass;
    return filePreviewClass;

});