/** @module xfile/views/FilePicker **/
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'module',
    'xide/_base/_Widget',
    'xdocker/Docker2',
    'xfile/ThumbRenderer',
    'dojo/promise/all',
    'xfile/Breadcrumb'
], function (dcl, types, utils, factory,
             module, _Widget, Docker2, ThumbRenderer, all, Breadcrumb) {

    var ACTION = types.ACTION;
    var Module = dcl(_Widget, {
        declaredClass:'xfile.views.FilePicker',
        templateString: '<div class="FilePicker"></div>',
        resizeToParent: true,
        docker: null,
        dockerOptions : {
            resizeToParent: true
        },
        breadcrumb: null,
        leftStore: null,
        rightStore: null,

        storeOptionsMixin: null,
        storeOptions: null,

        leftGrid: null,
        leftGridArgs: null,

        rightGrid: null,
        rightGridArgs: null,
        removePermissions:[
            ACTION.OPEN_IN_TAB
        ],
        defaultGridArgs: {
            registerEditors: false,
            _columns: {
                "Name": true,
                "Path": false,
                "Size": false,
                "Modified": false
            }
        },
        selection:null,
        mount: 'root',
        _selection:null,
        getDocker:function(){
            if(this.docker){
                return this.docker;
            }
            var dockerOptions = this.dockerOptions || {
                resizeToParent: true
            };
            var docker = this.docker || Docker2.createDefault(this.domNode, dockerOptions);
            docker.resizeToParent = true;
            this.add(docker, null, false);

            this.docker = docker;
            return docker;
        },
        createLayout: function () {
            var docker = this.getDocker();
            this.layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                h: 50,
                title: false
            });
            this.layoutTop._minSize.y = 50;
            this.layoutTop._maxSize.y = 50;
            this.layoutTop._scrollable = {
                x: false,
                y: false
            };
            this.breadcrumb = utils.addWidget(Breadcrumb, {}, null, this.layoutTop, true);
            this.layoutLeft = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.BOTTOM, null, {
                w: 100,
                title: false
            });
            this.layoutLeft._minSize.x = 150;
            this.layoutLeft.moveable(true);
            this.layoutMain = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.RIGHT, this.layoutLeft, {
                w: '70%',
                title: false
            });
            this.layoutTop.initSize(100,'100%');
            this.layoutLeft.initSize('100%',150);
            docker.resize();
        },
        setSelection:function(selection){
            this._selection=selection;
        },
        connectGrids:function(src,target){

            var self = this;

            target._on('selectionChanged', function (event) {
                if(event.why !=='deselect') {
                    var selection = target.getSelection(),
                        item = target.getSelectedItem();

                    if(item){
                        self.setSelection(selection);
                    }
                }
            });
            src._on('selectionChanged', function (event) {
                var selection = src.getSelection(),
                    item = src.getSelectedItem();
                if (!item) {
                    item = src.getCurrentFolder();
                }
                if(!item){
                    return;
                }
                var targetFolder = null;
                if (item.directory) {
                    targetFolder = item;
                } else {
                    if(!item.getParent){
                        console.error('item has no item.getParent!');
                    }
                    targetFolder = item.getParent ? item.getParent() : null;
                }
                if (targetFolder) {
                    target.openFolder(targetFolder.path, false);
                }

                if(item){
                    self.setSelection(selection);
                }
            });

        },
        createFileGrid: function (store, parent, args) {
            var GridClass = this.Module /*|| FileGrid*/;
            return GridClass.createDefault(this.ctx, args, parent, false, true, store);
        },
        onLeftGridReady:function(grid){},
        onRightGridReady:function(grid){},
        onError:function(err,where){
            //console.error('File Picker Error: '+ where + ' : ' + err,err);
        },
        startGrids:function(left,right){
            var self = this,
                srcStore = left.collection,
                targetStore = right.collection,
                _selectArgs = {
                    focus: true,
                    append: false
                },
                both = [left.refresh(), right.refresh()];
            var selected = this.selection;// './AA/xfile_last/Selection_002.png';
            var pathInfo = utils.pathinfo(selected, types.PATH_PARTS.ALL);
            var path = pathInfo.dirname || "";
            if(path==='.'){
                path = selected;
            }
            left.showStatusbar && left.showStatusbar(false);

            left._on('changeSource',function(mountdata){
                right.changeSource(mountdata,true);
            });
            left._on('changedSource',function(mountdata){
                left.select([0],null,true,_selectArgs);
            });
            right._on('changeSource',function(mountdata){
                left.changeSource(mountdata,true);
            });
            right._on('changedSource',function(mountdata){
                left.select([0],null,true,_selectArgs);
            });

            all(both).then(function () {
                self.onLeftGridReady(left);
                self.onRightGridReady(right);
                self.getDocker().resize();
                //load the selected item in the store
                all([srcStore.getItem(path, true), targetStore.getItem(path, true)]).then(function (what) {
                    //when loaded, open that folder!
                    all(_.invoke([left], 'openFolder', path)).then(function () {
                        //when opened, select the selected item
                        all(_.invoke([left,right], 'select', [selected], null, true, _selectArgs)).then(function () {
                            //when selected, complete grids
                            selected._selection = selected;
                            self.connectGrids(left, right);
                            //self.setupBreadcrumb(left, right,selected);
                        },function(err){
                            self.onError(err,'selecting items');
                        });

                    },function(err){
                        self.onError(err,'open folder');
                    });
                },function(err){
                    self.onError(err,'loading items');
                });
            },function(error){
                self.onError(err,'refresh grid');
            });
        },
        /**
         * Function called right after left & right grid is ready (opened folders,...)
         * @param src
         * @param target
         */
        setupBreadcrumb:function(src,target,openPath){
            
            var breadcrumb = this.breadcrumb,
                cwd = src.getCurrentFolder(),
                srcStore = src.collection,
                targetStore = target.collection;
            breadcrumb.setSource(src);

            function _onChangeFolder(store,item,grid){
                if(breadcrumb.grid!=grid){
                    breadcrumb.setSource(grid);
                }
                breadcrumb.clear();
                breadcrumb.setPath('.',srcStore.getRootItem(),item.getPath(),store);
            }

            this.addHandle('click',src.on('click',function(){
                breadcrumb.setSource(src);
            }));
            this.addHandle('click',target.on('click',function(){
                breadcrumb.setSource(target);
            }));
            src._on('openFolder', function (evt) {
                _onChangeFolder(srcStore,evt.item,src);
            });
            target._on('openFolder', function (evt) {
                _onChangeFolder(targetStore,evt.item,target);
            });

        },
        destroy:function(){          
            utils.destroy(this.leftStore);
            utils.destroy(this.rightStore);
            utils.destroy(this.leftGrid);
            utils.destroy(this.rightGrid);
            utils.destroy(this.breadcrumb);
            utils.destroy(this.docker);            
        },
        startup: function () {
            this.createLayout();
            var leftParent = this.layoutLeft;
            var rightParent = this.layoutMain;
            var self = this,
                ctx = self.ctx,
                config = ctx.config;

            /**
             * File Stores
             */
            var mount = this.mount || 'root';
            var storeOptions = this.storeOptions;
            var storeOptionsMixin = this.storeOptionsMixin || {
                    "includeList": "*.jpg,*.png",
                    "excludeList": "*"
                };

            var leftStore = this.leftStore || factory.createFileStore(mount, storeOptions, config, storeOptionsMixin, ctx,storeOptionsMixin);
            var rightStore = this.rightStore || factory.createFileStore(mount, storeOptions, config, storeOptionsMixin, ctx,storeOptionsMixin);
            var newTabTarget = this.layoutMain;
            var defaultGridArgs = utils.mixin({},utils.clone(this.defaultGridArgs));
            var removePermissions = this.removePermissions;
            /**
             *  Left - Grid :
             */
            var leftGridArgs = utils.mixin(defaultGridArgs, this.leftGridArgs || {
                    newTarget: newTabTarget
                });
            _.remove(leftGridArgs.permissions, function (permission) {
                return _.indexOf(removePermissions, permission) !== -1;
            });
            //eg: leftGridArgs.permissions.remove(types.ACTION.STATUSBAR);
            var gridLeft = this.leftGrid || this.createFileGrid(leftStore, leftParent, leftGridArgs);
            this.leftGrid = gridLeft;
            /**
             *  Right - Grid :
             */
            var rightGridArgs = utils.mixin(defaultGridArgs,this.rightGridArgs || {
                    selectedRenderer: ThumbRenderer
                });
            _.remove(rightGridArgs.permissions, function (permission) {
                return _.indexOf(removePermissions, permission) !== -1;
            });
            var gridMain = this.rightGrid || this.createFileGrid(rightStore, rightParent, rightGridArgs);
            this.rightGrid = gridMain;
            return this.startGrids(gridLeft,gridMain);
        }
    });

    return Module;

});
