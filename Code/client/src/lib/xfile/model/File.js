/** @module xfile/model/File **/
define([
    "dcl/dcl",
    "xide/data/Model",
    "xide/utils",
    "xide/types",
    "xide/lodash"
], function (dcl, Model, utils, types, _) {
    /**
     * @class module:xfile/model/File
     */
    return dcl(Model, {
        declaredClass: 'xfile.model.File',
        getFolder: function () {
            var path = this.getPath();
            if (this.directory) {
                return path;
            }
            return utils.pathinfo(path, types.PATH_PARTS.ALL).dirname;
        },
        getChildren: function () {
            return this.children;
        },
        getParent: function () {
            //current folder:
            var store = this.getStore() || this._S;
            return store.getParent(this);
        },
        getChild: function (path) {
            return _.find(this.getChildren(), {
                path: path
            });
        },
        getStore: function () {
            return this._store || this._S;
        }
    });
});