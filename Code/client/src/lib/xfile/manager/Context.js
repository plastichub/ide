define([
    'dcl/dcl',
    'xide/manager/Context',
    'xide/types'
], function (dcl, Context) {
    return dcl(Context,{
        declaredClass:"xfile.manager.Context"
    });
});