define([
    'dojo/_base/declare',
    'xfile/manager/FileManager',
    'xfile/manager/Context',
    'dojo/Deferred',
    'require'
], function (declare, FileManager, Context,Deferred,require) {


    return declare("xfile/embedded", null, {


        start: function (config) {

            var xFileConfiguration = config || typeof(xFileConfig) !== 'undefined' ? xFileConfig : null;

            xfile.manager.Context.prototype.namespace = 'xfile.manager.';
            xfile.manager.Context.prototype.defaultNamespace = 'xfile.manager.';

            var xasContext = new xfile.manager.Context(xFileConfiguration);

            try {

                if (xFileConfiguration.mixins) {
                    xasContext.doMixins(xFileConfiguration.mixins);
                }
                //construct managers
                xasContext.constructManagers();

                //init managers
                xasContext.initManagers();
                //init locals!
                xasContext.getLocals("dijit", "common");

                var xFileApp = xasContext.getApplication();

                xFileApp.boot();

                return xasContext;
            } catch (e) {
                debugger;
            }


        },
        /**
         * Returns promise when all xfile dependencies are loaded
         * @returns {Deferred.promise|*}
         */
        load: function () {

            var dfd = new Deferred(),
                _require  = require;    // hide from compiler

            _require([

                './Imports/DojoRPCImports',
                './Imports/DojoxImports',       //  Extra - Dojox - IMPORTS
                './Imports/DijitImports',       //  Extra - DIJIT - IMPORTS

                './Managers',                   //  XFile managers
                './config',                     //  XFile config
                './factory/model',              //  XFile factories
                './Views',                      //  XFile Views
                './Widgets',                    //  XFile widgets
                './Commons'                     //  XFile commons
            ], function () {

                dfd.resolve();
            });

            return dfd.promise;
        }
    });
});