/**
 * @module xfile/data/FileStore
 **/
define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    'dojo/Deferred',
    "xide/mixins/ReloadMixin",
    "xide/manager/ServerActionBase",
    'dstore/Cache',
    'dstore/QueryResults',
    'xide/types',
    'xide/utils',
    'dojo/when',
    'xide/data/TreeMemory',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xfile/model/File',
    'xide/lodash'
], function (declare, lang, Deferred, ReloadMixin, ServerActionBase, Cache, QueryResults, types, utils, when, TreeMemory, Trackable, ObservableStore, File, _) {
    var _debug = false;
    /**
     * Constants
     * @type {string}
     */
    var C_ITEM_EXPANDED = "_EX"; // Attribute indicating if a directory item is fully expanded.
    /**
     *
     * A store based on dstore/Memory and additional dstore mixins, used for all xfile grids.
     *
     * ###General
     *
     * - This store is only fetching file listings from the server! Any other file operation is done in
     * by file manager. However, this store is the central database for all files.
     *
     *
     * ###Common features and remarks
     *
     * - works with for multiple grid instances: lists/thumb or tree grids
     * - caches all directory listings, use _loadPath(path,force=true) to drop the cache
     * - all server methods return dojo/Deferred handles
     * - loads path segments recursivly, ie: getItem('./path/subPath/anotherSub');
     *
     * ### Server related
     * #### Requests

     * This part is done in the {link:xide/manager/ServerActionBase} ServerActionBase mixin, wrapped in here as
     * '_request'. This will take care about the possible transports : JSON-RPC-2.0 with a Dojo-SMD envelope or JSONP.
     * Both requests will be signed upon its payload, using this.config.RPC_PARAMETERS(server controlled), if required.
     * At the time of writing, this library has been developed for a PHP and Java-Struts which both support Dojo - SMD.
     * However, you may adjust to REST based environments yourself. Also, when using the
     * supplied XPHP - JSON-RPC-2.0-SMD implementation you can make use of 'batch-requests' to avoid further requests.
     *
     * #### Responses
     *
     * The response arrives here parsed already, no need to do anything but adding it to the store.
     *
     * @class module:xfile/data/FileStore
     * @augments module:xide/manager/ServerActionBase
     * @augments module:dgrid/Memory
     * @augments module:dgrid/Tree
     * @augments module:dgrid/Cache
     * @augments module:xide/mixins/ReloadMixin
     */
    function Implementation() {
        return {
            addDot: true,
            /**
             * 'recursive' will tell the server to run the directory listing recursive for server method 'ls'
             * @type {boolean}
             */
            recursive: false,
            rootSegment: ".",
            Model: File,
            /**
             * @member idProperty {string} sets the unique identifier for store items is set to the 'path' of a file.
             * @public
             */
            idProperty: "path",
            parentProperty: "path",
            parentField: 'parent',
            /**
             * @member mount {string} sets the 'mount' prefix for a VFS. This is needed to simplify the work
             * with paths and needs to be handled separated. By default any request expects full paths as:
             * - root/_my_path
             * - root://_my_path/even_deeper
             * - dropbox/_my_folder
             *
             * This property is is being used to complete a full file path automatically in the request so that only the
             * actual inner path needs to be specified in all methods.
             * By default xfile supports multiple stores in the same application.
             * Each store is created upon mount + store options hash.
             * @public
             */
            mount: 'path',
            /**
             * @member options{Object[]} specifies the store options. The store options are tied to the server. As the store
             * is only relavant for enumerating files through a server method, there are only a couple of options needed:
             *
             * <b>options.fields</b> {int} A enumeration bitmask which specifies the fields to be added for a file or
             * directory node. This is described in link:xfile/types/Types#Fields and again, this is being processed by
             * the server.
             *
             * options.includeList {string} a comma separated list of allowed file extensions, this has priority the
             * exclusion mask.
             *
             * options.excludeList {string} a comma separated list of excluded file extensions
             *
             * This options are set through xfile/manager/Context#defaultStoreOptions which might be overriden
             * through the server controlled xFileConfiguration.mixins global header script tag.
             *
             * @example {
                "fields": 1663,
                "includeList": "*",
                "excludeList": "*"
            }
             * @public
             */
            options: {
                fields: 1663,
                includeList: "*",
                excludeList: "*" // XPHP actually sets this to ".svn,.git,.idea" which are compatible to PHP's 'glob'
            },
            /**
             * @member serviceUrl {string} the path to the service entry point. There are 2 modes this end-point must
             * provide:
             * - a Dojo compatible SMD
             * - post/get requests with parameters. Currently JSONP & JSON-RPC-1/2.0 is supported.
             *
             * You can set also static SMD (see xide/manager/ServerActionBase) to bypass the SMD output. However,
             * this is being set usually by a server side mixin (in HTML header tag) on the fly!
             * @default null
             * @public
             */
            serviceUrl: null,
            /**
             * @member serviceClass {string} is the server class to be called. By default, this store creates a
             * JSON-RPC-2.0 post request where the methods looks like "XApp_Directory_Service.ls". Other formats are
             * supported in XPHP as well and support also composer autoloaders. If this is a singleton, you can also call
             * this.serviceObject.SERVER_CLASS.SERVER_METHOD(args) through here!
             * @public
             *
             */
            serviceClass: null,
            /**
             * @member singleton {boolean} Sets the ServerActionBase as 'Singleton' at the time of the construction. If there
             * is any other ServerActionBase subclass with the same service url, this will avoid additional requests to
             * fetch a SMD, furthermore, you can call other methods on the server through this here
             * as well: this.serviceObject.SERVER_CLASS.SERVER_METHOD(args)
             * @default true
             * @public
             */
            singleton: true,
            /////////////////////////////////////////////////////////////////////////////
            //
            //  dstore/Tree implementation
            //
            /////////////////////////////////////////////////////////////////////////////
            queryAccessors: true,
            autoEmitEvents: false, // this is handled by the methods themselves
            /*
             * test
             * @param object
             * @returns {boolean}
             */
            mayHaveChildren: function (object) {
                // summary:
                //		Check if an object may have children
                // description:
                //		This method is useful for eliminating the possibility that an object may have children,
                //		allowing collection consumers to determine things like whether to render UI for child-expansion
                //		and whether a query is necessary to retrieve an object's children.
                // object:
                //		The potential parent
                // returns: boolean
                if (object.mayHaveChildren == false) {
                    return false;
                }
                return object.directory === true;
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  Private part, might be trashed
            //
            /////////////////////////////////////////////////////////////////////////////
            _lastFilter: null,
            _lastFilterDef: null,
            _lastFilterItem: null,
            _initiated: {
                value: false
            },
            id: null,
            _state: {
                initiated: false,
                filter: null,
                filterDef: null
            },
            isInitiated: function () {
                return this._state.initiated;
            },
            setInitiated: function (initiated) {
                this._state.initiated = initiated;
            },
            _extraSortProperties: {
                name: {
                    ignoreCase: true
                }
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  Sorting
            //
            /////////////////////////////////////////////////////////////////////////////

            constructor: function () {
                this.id = utils.createUUID();
            },
            onSorted: function (sorted, data) {
                if (sorted.length == 1 && sorted[0].property === 'name') {
                    var upperCaseFirst = true,
                        directoriesFirst = true,
                        descending = sorted[0].descending;

                    if (directoriesFirst) {
                        function _sort(item) {
                            return upperCaseFirst ? item.name : item.name.toLowerCase();
                        }
                        var grouped = _.groupBy(data, function (item) {
                            return item.directory === true;
                        }, this);

                        data = _.sortBy(grouped['true'], _sort);
                        data = data.concat(_.sortBy(grouped['false'], _sort));
                        if (descending) {
                            data.reverse();
                        }
                    }
                }
                var _back = _.find(data, {
                    name: '..'
                });
                if (_back) {
                    data.remove(_back);
                    data.unshift(_back);
                }

                data = this.onAfterSort(data);

                return data;
            },
            onAfterSort: function (data) {
                var micromatch = this.micromatch;
                if (typeof mm !== 'undefined' && micromatch && data && data[0]) {
                    var what = data[0].realPath ? 'realPath' : 'path';
                    what = 'name';
                    var _items = _.pluck(data, what);
                    var matching = mm(_items, micromatch);
                    data = data.filter(function (item) {
                        if (matching.indexOf(item[what]) === -1) {
                            return null;
                        }
                        return item;
                    });
                    if (this._onAfterSort) {
                        data = this._onAfterSort(data);
                    }
                }
                return data;
            },
            /**
             * Overrides dstore method to add support for case in-sensitive sorting. This requires
             * ignoreCase: true in a this.sort(..) call, ie:
             *
             *  return [{property: 'name', descending: false, ignoreCase: true}]
             *
             * this will involve this._extraSortProperties and is being called by this.getDefaultSort().
             *
             * @param sorted
             * @returns {Function}
             * @private
             */
            _createSortQuerier: function (sorted) {
                var thiz = this;
                return function (data) {
                    data = data.slice();
                    data.sort(typeof sorted == 'function' ? sorted : function (a, b) {
                        for (var i = 0; i < sorted.length; i++) {
                            var comparison;
                            if (typeof sorted[i] == 'function') {
                                comparison = sorted[i](a, b);
                            } else {
                                var property = sorted[i].property;
                                if (thiz._extraSortProperties[property]) {
                                    utils.mixin(sorted[i], thiz._extraSortProperties[property]);
                                }
                                var descending = sorted[i].descending;
                                var aValue = a.get ? a.get(property) : a[property];
                                var bValue = b.get ? b.get(property) : b[property];
                                var ignoreCase = !!sorted[i].ignoreCase;
                                aValue != null && (aValue = aValue.valueOf());
                                bValue != null && (bValue = bValue.valueOf());

                                if (ignoreCase) {
                                    aValue.toUpperCase && (aValue = aValue.toUpperCase());
                                    bValue.toUpperCase && (bValue = bValue.toUpperCase());
                                }
                                comparison = aValue === bValue ? 0 : (!!descending === (aValue === null || aValue > bValue) ? -1 : 1);
                            }
                            if (comparison !== 0) {
                                return comparison;
                            }
                        }
                        return 0;
                    });
                    return thiz.onSorted(sorted, data);
                };
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  Public API section
            //
            /////////////////////////////////////////////////////////////////////////////
            _getItem: function (path, allowNonLoaded) {
                //try instant and return when loaded
                //this.getSync(path.replace('./',''))
                if (path === '/') {
                    path = '.';
                }
                var item = this.getSync(path) || this.getSync('./' + path);
                if (item && (this.isItemLoaded(item) || allowNonLoaded == true)) {
                    return item;
                }
                if (path === '.') {
                    return this.getRootItem();
                }
                return null;
            },
            /**
             * Returns a promise or a store item. This works recursively for any path and
             * results in one request per path segment or a single request when batch-requests
             * are enabled on the server.

             * @param path {string} a unique path, ie: ./ | . | ./myFolder | ./myFolder/and_deeper. If the item isn't
             * fully loaded yet, it just returns the item, if you enable 'load' and does the full load.
             * @param load {boolean} load the item if not already
             * @param options {object|null} request options
             *
             * @returns {Object|Deferred|null}
             */
            getItem: function (path, load, options) {
                path = path.replace('./', '');
                if (load == false) {
                    return this._getItem(path);
                } else if (load == true) {
                    //at this point we have to load recursively
                    var parts = path.split('/'),
                        thiz = this,
                        partsToLoad = [],
                        item = thiz.getSync(path);

                    if (item && this.isItemLoaded(item)) {
                        return item;
                    }

                    //new head promise for all underlying this.getItem calls
                    var deferred = new Deferred();
                    var _loadNext = function () {
                        //no additional lodash or array stuff please, keep it simple
                        var isFinish = !_.find(partsToLoad, {
                            loaded: false
                        });
                        if (isFinish) {
                            deferred.resolve(thiz._getItem(path));
                        } else {
                            
                            for (var i = 0; i < partsToLoad.length; i++) {
                                if (!partsToLoad[i].loaded) {
                                    var _item = thiz.getSync(partsToLoad[i].path);
                                    if (_item) {
                                        if (_item.directory === true && thiz.isItemLoaded(_item)) {
                                            partsToLoad[i].loaded = true;
                                            continue;
                                        } else if (_item.directory == null) {
                                            deferred.resolve(_item);
                                            break;
                                        }
                                    }
                                    thiz._loadPath(partsToLoad[i].path, false, options).then(function (items) {
                                        partsToLoad[i].loaded = true;
                                        _loadNext();
                                    }, function (err) {
                                        var _i = Math.abs(Math.min(0, i - 1));
                                        var nextPart = partsToLoad[_i];
                                        var parts = partsToLoad;
                                        if (!nextPart) {
                                            _i = partsToLoad.length - 1;
                                            nextPart = partsToLoad[_i];
                                        }
                                        var _item = thiz.getItem(nextPart.path);
                                        when(thiz.getItem(partsToLoad[_i].path, false), function (_item) {
                                            deferred.resolve(_item, partsToLoad[_i].path);
                                        });
                                    });
                                    break;
                                }
                            }


                            var isFinish = !_.find(partsToLoad, {
                                loaded: false
                            });
                            if (isFinish) {
                                deferred.resolve(thiz._getItem(path));
                            }
                        }
                    };

                    //prepare process array
                    var itemStr = '.';
                    for (var i = 0; i < parts.length; i++) {
                        if (parts[i] == '.') {
                            continue;
                        }
                        if (parts.length > 0) {
                            itemStr += '/';
                        }
                        itemStr += parts[i];
                        partsToLoad.push({
                            path: itemStr,
                            loaded: false
                        });
                    }

                    //fire
                    _loadNext();
                    return deferred;
                }
                if (path === '.') {
                    return this.getRootItem();
                }
                return null;
            },
            /**
             * Return the root item, is actually private
             * @TODO: root item unclear
             * @returns {{path: string, name: string, mount: *, directory: boolean, virtual: boolean, _S: (xfile|data|FileStore), getPath: Function}}
             */
            getRootItem: function () {
                return {
                    _EX: true,
                    path: '.',
                    name: '.',
                    mount: this.mount,
                    directory: true,
                    virtual: true,
                    _S: this,
                    getPath: function () {
                        return this.path + '/';
                    }
                };
            },
            /**
             * back compat, trash
             */
            getItemByPath: function () {
                //console.log('FILESTORE::getItemByPath',arguments);
            },
            /*
             * */
            getParents: function () {
                return null;
            },
            /**
             * Return parent object in sync mode, default to root item
             * @TODO fix root problem
             * @param mixed {string|object}
             */
            getParent: function (mixed) {
                if (!mixed) {
                    return null;
                }
                var item = mixed,
                    result = null;

                if (_.isString(item)) {
                    item = this.getSync(mixed);
                }

                if (item && item.parent) {
                    result = this.getSync(item.parent);
                }
                return result || this.getRootItem();
            },
            /**
             * Return 'loaded' state
             * @param item
             * @returns {boolean}
             */
            isItemLoaded: function (item) {
                return item && (!item.directory || this._isLoaded(item));
            },
            /**
             * Wrap loadItem
             * @TODO yeah, what?
             * @param item
             * @param force
             * @returns {*}
             */
            loadItem: function (item, force) {
                return this._loadItem(item, force);
            },
            /**
             * Fix an incoming item for our needs, adds the _S(this) attribute and
             * a function to safely return a its path since there are items with fake paths as: './myPath_back_'
             * @param item
             * @private
             */
            _parse: function (item) {
                item._S = this;
                if (!_.isEmpty(item.children)) {
                    _.each(item.children, function (_item) {
                        if (!_item.parent) {
                            _item.parent = item.path;
                        }
                        this._parse(_item);
                    }, this);

                    item._EX = true;
                    item.children = this.addItems(item.children);
                }
                item.getPath = function () {
                    return this.path;
                };
                if (!this.getSync(item.path)) {
                    this.putSync(item);
                }
            },
            /////////////////////////////////////////////////////////////////////////////
            //
            //  True store impl.
            //
            /////////////////////////////////////////////////////////////////////////////
            /**
             * Here to load an item forcefully (reload/refresh)
             * @param path
             * @param force
             * @param options {object|null}
             * @returns {*}
             * @private
             */
            _loadPath: function (path, force, options) {
                var thiz = this;
                var result = this._request(path, options);
                //console.log('load path : ' + path);
                result.then(function (items) {
                        //console.log('got : items for ' + path, items);
                        var _item = thiz._getItem(path, true);
                        if (_item) {
                            if (force) {
                                if (!_.isEmpty(_item.children)) {
                                    thiz.removeItems(_item.children);
                                }
                            }
                            _item._EX = true;
                            thiz.addItems(items, force);
                            _item.children = items;
                            return items;
                        } else {
                            if (options && options.onError) {
                                options.onError('Error Requesting path on server : ' + path);
                            } else {
                                throw new Error('cant get item at ' + path);
                            }
                        }
                    }.bind(this),
                    function (err) {
                        console.error('error in load');
                    });

                return result;

            },
            /**
             * Creates an object, throws an error if the object already exists.
             * @param object {Object} The object to store.
             * @param options {Object} Additional metadata for storing the data.  Includes an 'id' property if a specific
             * id is to be used. dstore/Store.PutDirectives?
             * @returns {Number|Object}
             */
            addSync: function (object, options) {
                (options = options || {}).overwrite = false;
                // call put with overwrite being false
                return this.putSync(object, options);
            },
            /**
             * @TODO: what?
             * @param item
             * @param force
             * @returns {Deferred}
             * @private
             */
            _loadItem: function (item, force) {
                var deferred = new Deferred(),
                    thiz = this;
                if (!item) {
                    deferred.reject('need item');
                    return deferred;
                }
                if (force) {
                    //special case on root
                    if (item.path === '.') {
                        thiz.setInitiated(false);
                        thiz.fetchRange().then(function (items) {
                            deferred.resolve({
                                store: thiz,
                                items: items,
                                item: item
                            });
                        });
                    } else {
                        this._loadPath(item.path, true).then((items)=> {
                            var _item = this.getSync(item.path);
                            deferred.resolve(_item);
                        }, function (err) {
                            console.error('error occured whilst loading items');
                            deferred.reject(err);
                        });
                    }
                }
                return deferred;
            },
            _normalize: function (response) {
                if (response && response.items) {
                    return response.items[0];
                }
                return [];
            },
            _isLoaded: function (item) {
                return item && item[C_ITEM_EXPANDED] === true;
            },
            fetch: function () {

            },
            put: function () {

            },
            add: function (item) {
                var _item = this.getSync(item.path);
                if (!_item) {
                    _item = this.addSync(item);
                    _item._S = this;
                    _item.getPath = function () {
                        return this.path;
                    };
                }
                return _item;
            },
            removeItems: function (items) {
                _.each(items, function (item) {
                    if (this.getSync(item.path)) {
                        this.removeSync(item.path);
                    }
                }, this);
            },
            getSync: function (id) {
                id = id || '';
                var data = this.storage.fullData;
                return data[this.storage.index[id]] || data[this.storage.index[id.replace('./', '')]];
            },
            addItems: function (items) {
                var result = [];
                _.each(items, function (item) {
                    var storeItem = this.getSync(item.path);
                    if (storeItem) {
                        this.removeSync(item.path);
                    }
                    result.push(this.add(item));
                }, this);
                return result;
            },
            open: function (item) {
                var thiz = this;
                if (!this._isLoaded(item)) {
                    item.isLoading = true;
                    return thiz._request(item.path).then(function (items) {
                        item.isLoading = false;
                        item._EX = true;
                        thiz.addItems(items);
                        item.children = items;
                        return items;
                    });
                } else {
                    var deferred = new Deferred();
                    thiz.resetQueryLog();
                    deferred.resolve(item.children);
                    return deferred;
                }
            },
            getDefaultSort: function () {
                return [{
                    property: 'name',
                    descending: false,
                    ignoreCase: true
                }];
            },
            filter: function (data) {
                if (data && typeof data === 'function') {
                    return this.inherited(arguments);
                }
                if (data.parent) {
                    this._state.path = data.parent;
                }
                var item = this.getSync(data.parent);
                if (item) {
                    if (!this.isItemLoaded(item)) {
                        item.isLoading = true;
                        this._state.filterDef = this._loadPath(item.path);
                        this._state.filterDef.then(function () {
                            item.isLoading = false;
                        })
                    } else {
                        /*
                        if(item.children) {
                            var total = new Deferred();
                            total.resolve(item.children);
                            this._state.filterDef = total;
                            this._state.filter = data;
                        }
                        */
                        this._state.filterDef = null;
                    }
                }
                delete this._state.filter;
                this._state.filter = data;
                return this.inherited(arguments);
            },
            _request: function (path, options) {
                var collection = this;
                const onError = ((e) => {
                    if (options && options.displayError === false) {
                        return;
                    }
                    logError(e, 'error in FileStore : ' + this.mount + ' :' + e);
                });
                const promise = this.runDeferred(null, 'ls', {
                        path: path,
                        mount: this.mount,
                        options: this.options,
                        recursive: this.recursive
                    },
                    utils.mixin({
                        checkErrors: false,
                        displayError: true
                    }, options), onError).then(function (response) {
                    var results = collection._normalize(response);
                    collection._parse(results);
                    // support items in the results
                    results = results.children || results;
                    return results;
                }, function (e) {
                    if (options && options.displayError === false) {
                        return;
                    }
                    logError(e, 'error in FileStore : ' + this.mount + ' :' + e);
                });
                return promise;
            },
            fetchRangeSync: function () {
                var data = this.fetchSync();
                var total = new Deferred();
                total.resolve(data.length);
                return new QueryResults(data, {
                    totalLength: total
                });
            },
            reset: function () {
                this._state.filter = null;
                this._state.filterDef = null;
                this.resetQueryLog();
            },
            resetQueryLog: function () {
                this.queryLog = [];
            },
            fetchRange: function () {
                // dstore/Memory#fetchRange always uses fetchSync, which we aren't extending,
                // so we need to extend this as well.
                var results = this._fetchRange();
                return new QueryResults(results.then(function (data) {
                    return data;
                }), {
                    totalLength: results.then(function (data) {
                        return data.length;
                    })
                });
            },
            initRoot: function () {
                //first time load
                var _path = '.';
                var thiz = this;
                //business as usual, root is loaded
                if (!this.isInitiated()) {
                    return thiz._request(_path).then(function (data) {
                        if (!thiz.isInitiated()) {
                            _.each(data, thiz._parse, thiz);
                            thiz.setData(data);
                            thiz.setInitiated(true);
                            thiz.emit('loaded');
                        }
                        return thiz.fetchRangeSync(arguments);
                    }.bind(this));
                }
                var dfd = new Deferred();
                dfd.resolve();
                return dfd;
            },
            _fetchRange: function () {
                //special case for trees
                if (this._state.filter) {
                    var def = this._state.filterDef;
                    if (def) {
                        def.then(function (items) {
                            this.reset();
                            if (def && def.resolve) {
                                def.resolve(items);
                            }
                        }.bind(this));
                        return def;
                    }
                }
                //first time load
                var _path = '.';
                var thiz = this;
                //business as usual, root is loaded
                if (this.isInitiated()) {
                    var _def = thiz.fetchRangeSync(arguments);
                    var resultsDeferred = new Deferred();
                    var totalDeferred = new Deferred();
                    resultsDeferred.resolve(_def);
                    totalDeferred.resolve(_def.length);
                    thiz.emit('loaded');
                    return new QueryResults(resultsDeferred, {
                        totalLength: _def.totalLength
                    });
                }
                return thiz._request(_path).then(function (data) {
                    if (!thiz.isInitiated()) {
                        _.each(data, thiz._parse, thiz);
                        thiz.setData(data);
                        thiz.setInitiated(true);
                        thiz.emit('loaded');
                    }
                    return thiz.fetchRangeSync(arguments);
                }.bind(this));
            },
            getDefaultCollection: function (path) {
                var _sort = this.getDefaultSort();
                if (path == null) {
                    return this.sort(_sort);
                } else {
                    /*
                    return this.filter({
                        parent: path
                    }).sort(_sort);
                    */
                    return this.filter(function (item) {
                        return item.parent === path;
                        return res;
                    }).sort(_sort);
                }
            },
            getChildren: function (object) {
                // summary:
                //		Get a collection of the children of the provided parent object
                // object:
                //		The parent object
                // returns: dstore/Store.Collection
                return this.root.filter({
                    parent: this.getIdentity(object)
                });
            },
            spin: true
        };
    }
    var Module = declare("xfile/data/Store", [TreeMemory, Cache, Trackable, ObservableStore, ServerActionBase.declare, ReloadMixin], Implementation());
    Module.Implementation = Implementation;
    return Module;
});