/** @module xfile/Breadcrumb **/
define([
    "dcl/dcl",
    'xide/widgets/TemplatedWidgetBase',
    'xide/model/Path',
    'xide/_base/_Widget'
], function (dcl,TemplatedWidgetBase,Path,_Widget) {

    return dcl(_Widget,{
        templateString:"<ul attachTo='root' style='-webkit-app-region:drag' class='breadcrumb'></ul>",
        grid:null,
        destroy:function(){
            this.clear();
            this.grid = null;
        },
        setSource:function(src){

            /*
            if(!src || (!src.getBreadcrumbPath || !src.collection)){
                return;
            }
            */

            if(this.grid==src){
                return;
            }

            this.grid = src;
            var collection = src.collection
            this.clear();

            var customPath = src.getBreadcrumbPath ? src.getBreadcrumbPath():null;
            if(customPath===false){
                return;
            }
            if(customPath){
                this.setPath(customPath.root, null, customPath, null);
            }else if(collection && src.getCurrentFolder){

                var store = collection,
                    cwdItem = src.getCurrentFolder(),
                    cwd = cwdItem ? cwdItem.path : '';

                this.setPath('.', store.getRootItem(), cwd, store);
            }

        },
        setPath : function(rootLabel,rootItem,path,store,_init){

            this.clear();
            rootItem && rootLabel && this.addSegment(rootLabel, true, rootItem);
            var _path = new Path(path);
            var segs = _path.getSegments();
            var _last = _init || '.';
            _.each(segs, function (seg) {
                var segPath = _last + '/' + seg;
                this.addSegment(seg, true, store ? store.getSync(segPath) : null);
                _last = segPath;
            },this);
        },
        /**
         * Event delegation
         * @param label
         * @param e
         * @param el
         */
        onClick:function(label,e,el){
            this._emit('click',{
                element:el,
                event:e,
                label:label,
                data:el.data
            });
        },
        /**
         * Add a new breadcrumb item
         * @param label
         * @param active
         * @param data
         * @returns {*|jQuery|HTMLElement}
         */
        addSegment:function(label,active,data){

            if(data && this.has(data)){
                return null;
            }

            var _class = active ? 'active' : '',
                self = this,
                el = $("<li class='" +_class + "'><a href='#'>" + label + "</a></li>");

            //clear active state of all previous elements
            _.each(this.all(),function(e){
                e.el.removeClass('active');
            });

            $(this.domNode).append(el);

            if(data) {
                el.on('click', function (e) {
                    self.grid && self.grid.openFolder(data);
                });
                el[0].data = data;
            }

            return el;
        },
        /**
         * Remove last breadcrumb item
         */
        pop:function(){
            this.last().remove();
        },
        /**
         * Return last breadcrumb item
         * @returns {*|jQuery}
         */
        last:function(){
            return $(this.root).children().last();
        },
        /**
         * Returns true when data is found
         * @param data
         * @returns {boolean}
         */
        has:function(data){

            var all = this.all();
            for (var i = 0; i < all.length; i++) {
                if(all[i].data.path === data.path){
                    return true;
                }
            }
            return false;
        },
        /**
         * Return all breadcrumb items
         * @returns {Array}
         */
        all:function(){

            var result = [];

            _.each(this.$root.children(),function(el){
                result.push({
                    el:$(el),
                    label:el.outerText,
                    data:el.data
                })
            });

            return result;

        },
        clear:function(){

            var all = this.all();
            while(all.length){
                var item =all[all.length-1];
                item.el.remove();
                item.data = null;
                all.remove(item);
            }
        },
        /**
         * Removes all breadcrumb items from back to beginning, until data.path matches
         * @param data
         */
        unwind:function(data){
            var rev = this.all().reverse();
            _.each(rev,function(e){
                if(e.data && e.data.path !== (data && data.path) && e.data.path!=='.'){
                    this.pop();
                }
            },this);
        },
        startup:function(){

            this._on('click',function(e){

                var data = e.element[0].data,
                    grid = this.grid;

                if(data) {
                    this.unwind(data);
                    grid && grid.openFolder(data);
                }

            }.bind(this));
        }
    });
});