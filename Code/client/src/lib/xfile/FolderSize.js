/** @module xfile/Statusbar **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xfile/Breadcrumb',
    'xide/layout/_TabContainer',
    'xfile/views/FileSize'
], function (declare, types,utils,Breadcrumb,_TabContainer,FileSize) {

    //package via declare

    var _class = declare('xfile.FolderSize', null, {

        __sizeDisplay:null,
        __bottomTabContainer:null,
        __bottomPanel:null,
        __sizesTab:null,

        showStats:function(show){

            var bottomTabContainer = this.getBottomTabContainer(),
                tab = this.__sizesTab,
                display = this.__sizeDisplay,
                self = this,
                bottom = this._getBottom(),
                open = (bottom && bottom.isExpanded());

            if(open==true && bottom){
                show=false;
            }else if(!open && bottom){
                return bottom.expand();
            }


            if(show){


                if(!bottom){
                    bottom = this.getBottomPanel(false, 0.2);
                }

                if(bottom && display){
                    return bottom.expand();
                }

                if(!bottomTabContainer){

                    var tabContainer = utils.addWidget(_TabContainer, {
                        direction: 'below'
                    }, null, bottom, true);

                    this.__bottomTabContainer = bottomTabContainer  = tabContainer;
                    bottom._tabs = tabContainer;
                }

                if(!tab){
                    tab = this.__sizesTab = bottomTabContainer.createTab('Sizes', 'fa-bar-chart');
                }

                if(!display){

                    display = this.__sizeDisplay = tab.add(FileSize, {
                        owner:self
                    }, null, false);

                    display.startup();


                    this._on('openedFolder',function(data){

                        utils.destroy(self.__sizeDisplay);

                        tab.resize();
                        self.resize();
                        self.__sizeDisplay = null;
                        self.__sizeDisplay = utils.addWidget(FileSize, {
                            owner:self
                        }, null,tab,false);

                        utils.resizeTo(self.__sizeDisplay,tab,true,true);
                        self.__sizeDisplay.startup();

                    });
                }




            }else{
                bottom && bottom.collapse();
            }

        },
        _runAction: function (action) {


            var _action = this.getAction(action);
            if (_action && _action.command == types.ACTION.SIZE_STATS) {
                return this.showStats(this.__sizeDisplay ? false : true);
            }
            return this.inherited(arguments);
        },
        buildRendering: function () {

            this.inherited(arguments);

            var grid = this,
                node = this.domNode;


            this._on('onAddActions', function (evt) {

                var actions = evt.actions,
                    permissions = evt.permissions,
                    action = types.ACTION.SIZE_STATS;

                /*
                if (!evt.store.getSync(action) &&typeof d3 !=='undefined') {

                    var _action = grid.createAction('Sizes', action, 'fa-bar-chart', null, 'View', 'Show', 'item|view', null, null, null, null, null, permissions, node, grid);
                    if (!_action) {

                        return;
                    }
                    actions.push(_action);
                }
                */
            });
        }

    });

    return _class;
});