"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function StingEnum(o) {
    return o.reduce((res, key) => {
        res[key] = key;
        return res;
    }, Object.create(null));
}
exports.StingEnum = StingEnum;
//# sourceMappingURL=utils.js.map