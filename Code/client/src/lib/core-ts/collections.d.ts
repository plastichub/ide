export interface Key {
    toString(): string;
}
export interface Entry<K, T> {
    next?: Entry<K, T>;
    prev?: Entry<K, T>;
    key: K;
    value: T;
}
/**
 * A simple map to store value by a key object. Key can be any object that has toString() function to get
 * string value of the key.
 */
export declare class LinkedMap<K extends Key, T> {
    protected map: {
        [key: string]: Entry<K, T>;
    };
    protected _size: number;
    constructor();
    readonly size: number;
    get(k: K): T;
    getOrSet(k: K, t: T): T;
    keys(): K[];
    values(): T[];
    entries(): Entry<K, T>[];
    set(k: K, t: T): boolean;
    delete(k: K): T;
    has(k: K): boolean;
    clear(): void;
    protected push(key: K, value: T): void;
    protected pop(k: K): void;
    protected peek(k: K): T;
}
/**
 * A simple Map<T> that optionally allows to set a limit of entries to store. Once the limit is hit,
 * the cache will remove the entry that was last recently added. Or, if a ratio is provided below 1,
 * all elements will be removed until the ratio is full filled (e.g. 0.75 to remove 25% of old elements).
 */
export declare class BoundedLinkedMap<T> {
    private limit;
    protected map: {
        [key: string]: Entry<string, T>;
    };
    private head;
    private tail;
    private _size;
    private ratio;
    constructor(limit?: number, ratio?: number);
    readonly size: number;
    set(key: string, value: T): boolean;
    get(key: string): T;
    getOrSet(k: string, t: T): T;
    delete(key: string): T;
    has(key: string): boolean;
    clear(): void;
    protected push(entry: Entry<string, T>): void;
    private trim();
}
/**
 * A subclass of Map<T> that makes an entry the MRU entry as soon
 * as it is being accessed. In combination with the limit for the
 * maximum number of elements in the cache, it helps to remove those
 * entries from the cache that are LRU.
 */
export declare class LRUCache<T> extends BoundedLinkedMap<T> {
    constructor(limit: number);
    get(key: string): T;
}
/**
 * A trie map that allows for fast look up when keys are substrings
 * to the actual search keys (dir/subdir-problem).
 */
export declare class TrieMap<E> {
    static PathSplitter: (s: string) => string[];
    private _splitter;
    private _root;
    constructor(splitter: (s: string) => string[]);
    insert(path: string, element: E): void;
    lookUp(path: string): E;
    findSubstr(path: string): E;
    findSuperstr(path: string): TrieMap<E>;
}
export declare class ArraySet<T> {
    private _elements;
    constructor(elements?: T[]);
    readonly size: number;
    set(element: T): void;
    contains(element: T): boolean;
    unset(element: T): void;
    readonly elements: T[];
}
/**
 * Returns the last element of an array.
 * @param array The array.
 * @param n Which element from the end (default ist zero).
 */
export declare function tail<T>(array: T[], n?: number): T;
export declare function equals<T>(one: T[], other: T[], itemEquals?: (a: T, b: T) => boolean): boolean;
export declare function binarySearch<T>(array: T[], key: T, comparator: (op1: T, op2: T) => number): number;
/**
 * Takes a sorted array and a function p. The array is sorted in such a way that all elements where p(x) is false
 * are located before all elements where p(x) is true.
 * @returns the least x for which p(x) is true or array.length if no element fullfills the given function.
 */
export declare function findFirst<T>(array: T[], p: (x: T) => boolean): number;
/**
 * Returns the top N elements from the array.
 *
 * Faster than sorting the entire array when the array is a lot larger than N.
 *
 * @param array The unsorted array.
 * @param compare A sort function for the elements.
 * @param n The number of elements to return.
 * @return The first n elemnts from array when sorted with compare.
 */
export declare function top<T>(array: T[], compare: (a: T, b: T) => number, n: number): T[];
/**
 * @returns a new array with all undefined or null values removed. The original array is not modified at all.
 */
export declare function coalesce<T>(array: T[]): T[];
/**
 * Moves the element in the array for the provided positions.
 */
export declare function move(array: any[], from: number, to: number): void;
/**
 * @returns {{false}} if the provided object is an array
 * 	and not empty.
 */
export declare function isFalsyOrEmpty(obj: any): boolean;
/**
 * Removes duplicates from the given array. The optional keyFn allows to specify
 * how elements are checked for equalness by returning a unique string for each.
 */
export declare function distinct<T>(array: T[], keyFn?: (t: T) => string): T[];
export declare function uniqueFilter<T>(keyFn: (t: T) => string): (t: T) => boolean;
export declare function firstIndex<T>(array: T[], fn: (item: T) => boolean): number;
export declare function first<T>(array: T[], fn: (item: T) => boolean, notFoundValue?: T): T;
export declare function commonPrefixLength<T>(one: T[], other: T[], equals?: (a: T, b: T) => boolean): number;
export declare function flatten<T>(arr: T[][]): T[];
export declare function range(to: number, from?: number): number[];
export declare function fill<T>(num: number, valueFn: () => T, arr?: T[]): T[];
export declare function index<T>(array: T[], indexer: (t: T) => string): {
    [key: string]: T;
};
export declare function index<T, R>(array: T[], indexer: (t: T) => string, merger?: (t: T, r: R) => R): {
    [key: string]: R;
};
/**
 * Inserts an element into an array. Returns a function which, when
 * called, will remove that element from the array.
 */
export declare function insert<T>(array: T[], element: T): () => void;
