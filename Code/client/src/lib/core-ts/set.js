"use strict";
/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
Object.defineProperty(exports, "__esModule", { value: true });
class ArraySet {
    constructor(elements = []) {
        this._elements = elements.slice();
    }
    get size() {
        return this._elements.length;
    }
    set(element) {
        this.unset(element);
        this._elements.push(element);
    }
    contains(element) {
        return this._elements.indexOf(element) > -1;
    }
    unset(element) {
        const index = this._elements.indexOf(element);
        if (index > -1) {
            this._elements.splice(index, 1);
        }
    }
    get elements() {
        return this._elements.slice();
    }
}
exports.ArraySet = ArraySet;
//# sourceMappingURL=set.js.map