declare module '@xblox/core/primitives' {
	/**
	 * @returns whether the provided parameter is a JavaScript Array or not.
	 */
	export function isArray(array: any): array is any[];
	/**
	 * @returns whether the provided parameter is a JavaScript String or not.
	 */
	export function isString(str: any): str is string;
	/**
	 * @returns whether the provided parameter is a JavaScript Array and each element in the array is a string.
	 */
	export function isStringArray(value: any): value is string[];
	/**
	 *
	 * @returns whether the provided parameter is of type `object` but **not**
	 *	`null`, an `array`, a `regexp`, nor a `date`.
	 */
	export function isObject(obj: any): boolean;
	/**
	 * In **contrast** to just checking `typeof` this will return `false` for `NaN`.
	 * @returns whether the provided parameter is a JavaScript Number or not.
	 */
	export function isNumber(obj: any): obj is number;
	/**
	 * @returns whether the provided parameter is a JavaScript Boolean or not.
	 */
	export function isBoolean(obj: any): obj is boolean;
	/**
	 * @returns whether the provided parameter is undefined.
	 */
	export function isUndefined(obj: any): boolean;
	/**
	 * @returns whether the provided parameter is undefined or null.
	 */
	export function isUndefinedOrNull(obj: any): boolean;
	/**
	 * @returns whether the provided parameter is an empty JavaScript Object or not.
	 */
	export function isEmptyObject(obj: any): obj is any;
	/**
	 * @returns whether the provided parameter is a JavaScript Function or not.
	 */
	export function isFunction(obj: any): obj is Function;
	/**
	 * @returns whether the provided parameters is are JavaScript Function or not.
	 */
	export function areFunctions(...objects: any[]): boolean;
	export type TypeConstraint = string | Function;
	export function validateConstraints(args: any[], constraints: TypeConstraint[]): void;
	export function validateConstraint(arg: any, constraint: TypeConstraint): void;
	/**
	 * Creates a new object of the provided class and will call the constructor with
	 * any additional argument supplied.
	 */
	export function create(ctor: Function, ...args: any[]): any;
	export interface IFunction0<T> {
	    (): T;
	}
	export interface IFunction1<A1, T> {
	    (a1: A1): T;
	}
	export interface IFunction2<A1, A2, T> {
	    (a1: A1, a2: A2): T;
	}
	export interface IFunction3<A1, A2, A3, T> {
	    (a1: A1, a2: A2, a3: A3): T;
	}
	export interface IFunction4<A1, A2, A3, A4, T> {
	    (a1: A1, a2: A2, a3: A3, a4: A4): T;
	}
	export interface IFunction5<A1, A2, A3, A4, A5, T> {
	    (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5): T;
	}
	export interface IFunction6<A1, A2, A3, A4, A5, A6, T> {
	    (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6): T;
	}
	export interface IFunction7<A1, A2, A3, A4, A5, A6, A7, T> {
	    (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7): T;
	}
	export interface IFunction8<A1, A2, A3, A4, A5, A6, A7, A8, T> {
	    (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8): T;
	}
	export interface IAction0 extends IFunction0<void> {
	}
	export interface IAction1<A1> extends IFunction1<A1, void> {
	}
	export interface IAction2<A1, A2> extends IFunction2<A1, A2, void> {
	}
	export interface IAction3<A1, A2, A3> extends IFunction3<A1, A2, A3, void> {
	}
	export interface IAction4<A1, A2, A3, A4> extends IFunction4<A1, A2, A3, A4, void> {
	}
	export interface IAction5<A1, A2, A3, A4, A5> extends IFunction5<A1, A2, A3, A4, A5, void> {
	}
	export interface IAction6<A1, A2, A3, A4, A5, A6> extends IFunction6<A1, A2, A3, A4, A5, A6, void> {
	}
	export interface IAction7<A1, A2, A3, A4, A5, A6, A7> extends IFunction7<A1, A2, A3, A4, A5, A6, A7, void> {
	}
	export interface IAction8<A1, A2, A3, A4, A5, A6, A7, A8> extends IFunction8<A1, A2, A3, A4, A5, A6, A7, A8, void> {
	}
	export type NumberCallback = (index: number) => void;
	export function count(to: number, callback: NumberCallback): void;
	export function count(from: number, to: number, callback: NumberCallback): void;
	export function countToArray(to: number): number[];
	export function countToArray(from: number, to: number): number[];

}
declare module '@xblox/core/collections' {
	export interface Key {
	    toString(): string;
	}
	export interface Entry<K, T> {
	    next?: Entry<K, T>;
	    prev?: Entry<K, T>;
	    key: K;
	    value: T;
	}
	/**
	 * A simple map to store value by a key object. Key can be any object that has toString() function to get
	 * string value of the key.
	 */
	export class LinkedMap<K extends Key, T> {
	    protected map: {
	        [key: string]: Entry<K, T>;
	    };
	    protected _size: number;
	    constructor();
	    readonly size: number;
	    get(k: K): T;
	    getOrSet(k: K, t: T): T;
	    keys(): K[];
	    values(): T[];
	    entries(): Entry<K, T>[];
	    set(k: K, t: T): boolean;
	    delete(k: K): T;
	    has(k: K): boolean;
	    clear(): void;
	    protected push(key: K, value: T): void;
	    protected pop(k: K): void;
	    protected peek(k: K): T;
	}
	/**
	 * A simple Map<T> that optionally allows to set a limit of entries to store. Once the limit is hit,
	 * the cache will remove the entry that was last recently added. Or, if a ratio is provided below 1,
	 * all elements will be removed until the ratio is full filled (e.g. 0.75 to remove 25% of old elements).
	 */
	export class BoundedLinkedMap<T> {
	    private limit;
	    protected map: {
	        [key: string]: Entry<string, T>;
	    };
	    private head;
	    private tail;
	    private _size;
	    private ratio;
	    constructor(limit?: number, ratio?: number);
	    readonly size: number;
	    set(key: string, value: T): boolean;
	    get(key: string): T;
	    getOrSet(k: string, t: T): T;
	    delete(key: string): T;
	    has(key: string): boolean;
	    clear(): void;
	    protected push(entry: Entry<string, T>): void;
	    private trim();
	}
	/**
	 * A subclass of Map<T> that makes an entry the MRU entry as soon
	 * as it is being accessed. In combination with the limit for the
	 * maximum number of elements in the cache, it helps to remove those
	 * entries from the cache that are LRU.
	 */
	export class LRUCache<T> extends BoundedLinkedMap<T> {
	    constructor(limit: number);
	    get(key: string): T;
	}
	/**
	 * A trie map that allows for fast look up when keys are substrings
	 * to the actual search keys (dir/subdir-problem).
	 */
	export class TrieMap<E> {
	    static PathSplitter: (s: string) => string[];
	    private _splitter;
	    private _root;
	    constructor(splitter: (s: string) => string[]);
	    insert(path: string, element: E): void;
	    lookUp(path: string): E;
	    findSubstr(path: string): E;
	    findSuperstr(path: string): TrieMap<E>;
	}
	export class ArraySet<T> {
	    private _elements;
	    constructor(elements?: T[]);
	    readonly size: number;
	    set(element: T): void;
	    contains(element: T): boolean;
	    unset(element: T): void;
	    readonly elements: T[];
	}
	/**
	 * Returns the last element of an array.
	 * @param array The array.
	 * @param n Which element from the end (default ist zero).
	 */
	export function tail<T>(array: T[], n?: number): T;
	export function equals<T>(one: T[], other: T[], itemEquals?: (a: T, b: T) => boolean): boolean;
	export function binarySearch<T>(array: T[], key: T, comparator: (op1: T, op2: T) => number): number;
	/**
	 * Takes a sorted array and a function p. The array is sorted in such a way that all elements where p(x) is false
	 * are located before all elements where p(x) is true.
	 * @returns the least x for which p(x) is true or array.length if no element fullfills the given function.
	 */
	export function findFirst<T>(array: T[], p: (x: T) => boolean): number;
	/**
	 * Returns the top N elements from the array.
	 *
	 * Faster than sorting the entire array when the array is a lot larger than N.
	 *
	 * @param array The unsorted array.
	 * @param compare A sort function for the elements.
	 * @param n The number of elements to return.
	 * @return The first n elemnts from array when sorted with compare.
	 */
	export function top<T>(array: T[], compare: (a: T, b: T) => number, n: number): T[];
	/**
	 * @returns a new array with all undefined or null values removed. The original array is not modified at all.
	 */
	export function coalesce<T>(array: T[]): T[];
	/**
	 * Moves the element in the array for the provided positions.
	 */
	export function move(array: any[], from: number, to: number): void;
	/**
	 * @returns {{false}} if the provided object is an array
	 * 	and not empty.
	 */
	export function isFalsyOrEmpty(obj: any): boolean;
	/**
	 * Removes duplicates from the given array. The optional keyFn allows to specify
	 * how elements are checked for equalness by returning a unique string for each.
	 */
	export function distinct<T>(array: T[], keyFn?: (t: T) => string): T[];
	export function uniqueFilter<T>(keyFn: (t: T) => string): (t: T) => boolean;
	export function firstIndex<T>(array: T[], fn: (item: T) => boolean): number;
	export function first<T>(array: T[], fn: (item: T) => boolean, notFoundValue?: T): T;
	export function commonPrefixLength<T>(one: T[], other: T[], equals?: (a: T, b: T) => boolean): number;
	export function flatten<T>(arr: T[][]): T[];
	export function range(to: number, from?: number): number[];
	export function fill<T>(num: number, valueFn: () => T, arr?: T[]): T[];
	export function index<T>(array: T[], indexer: (t: T) => string): {
	    [key: string]: T;
	};
	export function index<T, R>(array: T[], indexer: (t: T) => string, merger?: (t: T, r: R) => R): {
	    [key: string]: R;
	};
	/**
	 * Inserts an element into an array. Returns a function which, when
	 * called, will remove that element from the array.
	 */
	export function insert<T>(array: T[], element: T): () => void;

}
declare module '@xblox/core/map' {
	export interface Key {
	    toString(): string;
	}
	export interface Entry<K, T> {
	    next?: Entry<K, T>;
	    prev?: Entry<K, T>;
	    key: K;
	    value: T;
	}
	/**
	 * A simple map to store value by a key object. Key can be any object that has toString() function to get
	 * string value of the key.
	 */
	export class LinkedMap<K extends Key, T> {
	    protected map: {
	        [key: string]: Entry<K, T>;
	    };
	    protected _size: number;
	    constructor();
	    readonly size: number;
	    get(k: K): T;
	    getOrSet(k: K, t: T): T;
	    keys(): K[];
	    values(): T[];
	    entries(): Entry<K, T>[];
	    set(k: K, t: T): boolean;
	    delete(k: K): T;
	    has(k: K): boolean;
	    clear(): void;
	    protected push(key: K, value: T): void;
	    protected pop(k: K): void;
	    protected peek(k: K): T;
	}
	/**
	 * A simple Map<T> that optionally allows to set a limit of entries to store. Once the limit is hit,
	 * the cache will remove the entry that was last recently added. Or, if a ratio is provided below 1,
	 * all elements will be removed until the ratio is full filled (e.g. 0.75 to remove 25% of old elements).
	 */
	export class BoundedLinkedMap<T> {
	    private limit;
	    protected map: {
	        [key: string]: Entry<string, T>;
	    };
	    private head;
	    private tail;
	    private _size;
	    private ratio;
	    constructor(limit?: number, ratio?: number);
	    readonly size: number;
	    set(key: string, value: T): boolean;
	    get(key: string): T;
	    getOrSet(k: string, t: T): T;
	    delete(key: string): T;
	    has(key: string): boolean;
	    clear(): void;
	    protected push(entry: Entry<string, T>): void;
	    private trim();
	}
	/**
	 * A subclass of Map<T> that makes an entry the MRU entry as soon
	 * as it is being accessed. In combination with the limit for the
	 * maximum number of elements in the cache, it helps to remove those
	 * entries from the cache that are LRU.
	 */
	export class LRUCache<T> extends BoundedLinkedMap<T> {
	    constructor(limit: number);
	    get(key: string): T;
	}
	/**
	 * A trie map that allows for fast look up when keys are substrings
	 * to the actual search keys (dir/subdir-problem).
	 */
	export class TrieMap<E> {
	    static PathSplitter: (s: string) => string[];
	    private _splitter;
	    private _root;
	    constructor(splitter: (s: string) => string[]);
	    insert(path: string, element: E): void;
	    lookUp(path: string): E;
	    findSubstr(path: string): E;
	    findSuperstr(path: string): TrieMap<E>;
	}

}
declare module '@xblox/core/iterator' {
	export interface IIterator<T> {
	    next(): T;
	}
	export class ArrayIterator<T> implements IIterator<T> {
	    private items;
	    protected start: number;
	    protected end: number;
	    protected index: number;
	    constructor(items: T[], start?: number, end?: number);
	    first(): T;
	    next(): T;
	    protected current(): T;
	}
	export class ArrayNavigator<T> extends ArrayIterator<T> implements INavigator<T> {
	    constructor(items: T[], start?: number, end?: number);
	    current(): T;
	    previous(): T;
	    first(): T;
	    last(): T;
	    parent(): T;
	}
	export class MappedIterator<T, R> implements IIterator<R> {
	    protected iterator: IIterator<T>;
	    protected fn: (item: T) => R;
	    constructor(iterator: IIterator<T>, fn: (item: T) => R);
	    next(): R;
	}
	export interface INavigator<T> extends IIterator<T> {
	    current(): T;
	    previous(): T;
	    parent(): T;
	    first(): T;
	    last(): T;
	    next(): T;
	}
	export class MappedNavigator<T, R> extends MappedIterator<T, R> implements INavigator<R> {
	    protected navigator: INavigator<T>;
	    constructor(navigator: INavigator<T>, fn: (item: T) => R);
	    current(): R;
	    previous(): R;
	    parent(): R;
	    first(): R;
	    last(): R;
	    next(): R;
	}

}
declare module '@xblox/core/set' {
	export class ArraySet<T> {
	    private _elements;
	    constructor(elements?: T[]);
	    readonly size: number;
	    set(element: T): void;
	    contains(element: T): boolean;
	    unset(element: T): void;
	    readonly elements: T[];
	}

}
declare module '@xblox/core/numbers' {
	export type NumberCallback = (index: number) => void;
	export function count(to: number, callback: NumberCallback): void;
	export function count(from: number, to: number, callback: NumberCallback): void;
	export function countToArray(to: number): number[];
	export function countToArray(from: number, to: number): number[];

}
declare module '@xblox/core/charCode' {
	/**
	 * An inlined enum containing useful character codes (to be used with String.charCodeAt).
	 * Please leave the const keyword such that it gets inlined when compiled to JavaScript!
	 */
	export const enum CharCode {
	    Null = 0,
	    /**
	     * The `\t` character.
	     */
	    Tab = 9,
	    /**
	     * The `\n` character.
	     */
	    LineFeed = 10,
	    /**
	     * The `\r` character.
	     */
	    CarriageReturn = 13,
	    Space = 32,
	    /**
	     * The `!` character.
	     */
	    ExclamationMark = 33,
	    /**
	     * The `"` character.
	     */
	    DoubleQuote = 34,
	    /**
	     * The `#` character.
	     */
	    Hash = 35,
	    /**
	     * The `$` character.
	     */
	    DollarSign = 36,
	    /**
	     * The `%` character.
	     */
	    PercentSign = 37,
	    /**
	     * The `&` character.
	     */
	    Ampersand = 38,
	    /**
	     * The `'` character.
	     */
	    SingleQuote = 39,
	    /**
	     * The `(` character.
	     */
	    OpenParen = 40,
	    /**
	     * The `)` character.
	     */
	    CloseParen = 41,
	    /**
	     * The `*` character.
	     */
	    Asterisk = 42,
	    /**
	     * The `+` character.
	     */
	    Plus = 43,
	    /**
	     * The `,` character.
	     */
	    Comma = 44,
	    /**
	     * The `-` character.
	     */
	    Dash = 45,
	    /**
	     * The `.` character.
	     */
	    Period = 46,
	    /**
	     * The `/` character.
	     */
	    Slash = 47,
	    Digit0 = 48,
	    Digit1 = 49,
	    Digit2 = 50,
	    Digit3 = 51,
	    Digit4 = 52,
	    Digit5 = 53,
	    Digit6 = 54,
	    Digit7 = 55,
	    Digit8 = 56,
	    Digit9 = 57,
	    /**
	     * The `:` character.
	     */
	    Colon = 58,
	    /**
	     * The `;` character.
	     */
	    Semicolon = 59,
	    /**
	     * The `<` character.
	     */
	    LessThan = 60,
	    /**
	     * The `=` character.
	     */
	    Equals = 61,
	    /**
	     * The `>` character.
	     */
	    GreaterThan = 62,
	    /**
	     * The `?` character.
	     */
	    QuestionMark = 63,
	    /**
	     * The `@` character.
	     */
	    AtSign = 64,
	    A = 65,
	    B = 66,
	    C = 67,
	    D = 68,
	    E = 69,
	    F = 70,
	    G = 71,
	    H = 72,
	    I = 73,
	    J = 74,
	    K = 75,
	    L = 76,
	    M = 77,
	    N = 78,
	    O = 79,
	    P = 80,
	    Q = 81,
	    R = 82,
	    S = 83,
	    T = 84,
	    U = 85,
	    V = 86,
	    W = 87,
	    X = 88,
	    Y = 89,
	    Z = 90,
	    /**
	     * The `[` character.
	     */
	    OpenSquareBracket = 91,
	    /**
	     * The `\` character.
	     */
	    Backslash = 92,
	    /**
	     * The `]` character.
	     */
	    CloseSquareBracket = 93,
	    /**
	     * The `^` character.
	     */
	    Caret = 94,
	    /**
	     * The `_` character.
	     */
	    Underline = 95,
	    /**
	     * The ``(`)`` character.
	     */
	    BackTick = 96,
	    a = 97,
	    b = 98,
	    c = 99,
	    d = 100,
	    e = 101,
	    f = 102,
	    g = 103,
	    h = 104,
	    i = 105,
	    j = 106,
	    k = 107,
	    l = 108,
	    m = 109,
	    n = 110,
	    o = 111,
	    p = 112,
	    q = 113,
	    r = 114,
	    s = 115,
	    t = 116,
	    u = 117,
	    v = 118,
	    w = 119,
	    x = 120,
	    y = 121,
	    z = 122,
	    /**
	     * The `{` character.
	     */
	    OpenCurlyBrace = 123,
	    /**
	     * The `|` character.
	     */
	    Pipe = 124,
	    /**
	     * The `}` character.
	     */
	    CloseCurlyBrace = 125,
	    /**
	     * The `~` character.
	     */
	    Tilde = 126,
	    U_Combining_Grave_Accent = 768,
	    U_Combining_Acute_Accent = 769,
	    U_Combining_Circumflex_Accent = 770,
	    U_Combining_Tilde = 771,
	    U_Combining_Macron = 772,
	    U_Combining_Overline = 773,
	    U_Combining_Breve = 774,
	    U_Combining_Dot_Above = 775,
	    U_Combining_Diaeresis = 776,
	    U_Combining_Hook_Above = 777,
	    U_Combining_Ring_Above = 778,
	    U_Combining_Double_Acute_Accent = 779,
	    U_Combining_Caron = 780,
	    U_Combining_Vertical_Line_Above = 781,
	    U_Combining_Double_Vertical_Line_Above = 782,
	    U_Combining_Double_Grave_Accent = 783,
	    U_Combining_Candrabindu = 784,
	    U_Combining_Inverted_Breve = 785,
	    U_Combining_Turned_Comma_Above = 786,
	    U_Combining_Comma_Above = 787,
	    U_Combining_Reversed_Comma_Above = 788,
	    U_Combining_Comma_Above_Right = 789,
	    U_Combining_Grave_Accent_Below = 790,
	    U_Combining_Acute_Accent_Below = 791,
	    U_Combining_Left_Tack_Below = 792,
	    U_Combining_Right_Tack_Below = 793,
	    U_Combining_Left_Angle_Above = 794,
	    U_Combining_Horn = 795,
	    U_Combining_Left_Half_Ring_Below = 796,
	    U_Combining_Up_Tack_Below = 797,
	    U_Combining_Down_Tack_Below = 798,
	    U_Combining_Plus_Sign_Below = 799,
	    U_Combining_Minus_Sign_Below = 800,
	    U_Combining_Palatalized_Hook_Below = 801,
	    U_Combining_Retroflex_Hook_Below = 802,
	    U_Combining_Dot_Below = 803,
	    U_Combining_Diaeresis_Below = 804,
	    U_Combining_Ring_Below = 805,
	    U_Combining_Comma_Below = 806,
	    U_Combining_Cedilla = 807,
	    U_Combining_Ogonek = 808,
	    U_Combining_Vertical_Line_Below = 809,
	    U_Combining_Bridge_Below = 810,
	    U_Combining_Inverted_Double_Arch_Below = 811,
	    U_Combining_Caron_Below = 812,
	    U_Combining_Circumflex_Accent_Below = 813,
	    U_Combining_Breve_Below = 814,
	    U_Combining_Inverted_Breve_Below = 815,
	    U_Combining_Tilde_Below = 816,
	    U_Combining_Macron_Below = 817,
	    U_Combining_Low_Line = 818,
	    U_Combining_Double_Low_Line = 819,
	    U_Combining_Tilde_Overlay = 820,
	    U_Combining_Short_Stroke_Overlay = 821,
	    U_Combining_Long_Stroke_Overlay = 822,
	    U_Combining_Short_Solidus_Overlay = 823,
	    U_Combining_Long_Solidus_Overlay = 824,
	    U_Combining_Right_Half_Ring_Below = 825,
	    U_Combining_Inverted_Bridge_Below = 826,
	    U_Combining_Square_Below = 827,
	    U_Combining_Seagull_Below = 828,
	    U_Combining_X_Above = 829,
	    U_Combining_Vertical_Tilde = 830,
	    U_Combining_Double_Overline = 831,
	    U_Combining_Grave_Tone_Mark = 832,
	    U_Combining_Acute_Tone_Mark = 833,
	    U_Combining_Greek_Perispomeni = 834,
	    U_Combining_Greek_Koronis = 835,
	    U_Combining_Greek_Dialytika_Tonos = 836,
	    U_Combining_Greek_Ypogegrammeni = 837,
	    U_Combining_Bridge_Above = 838,
	    U_Combining_Equals_Sign_Below = 839,
	    U_Combining_Double_Vertical_Line_Below = 840,
	    U_Combining_Left_Angle_Below = 841,
	    U_Combining_Not_Tilde_Above = 842,
	    U_Combining_Homothetic_Above = 843,
	    U_Combining_Almost_Equal_To_Above = 844,
	    U_Combining_Left_Right_Arrow_Below = 845,
	    U_Combining_Upwards_Arrow_Below = 846,
	    U_Combining_Grapheme_Joiner = 847,
	    U_Combining_Right_Arrowhead_Above = 848,
	    U_Combining_Left_Half_Ring_Above = 849,
	    U_Combining_Fermata = 850,
	    U_Combining_X_Below = 851,
	    U_Combining_Left_Arrowhead_Below = 852,
	    U_Combining_Right_Arrowhead_Below = 853,
	    U_Combining_Right_Arrowhead_And_Up_Arrowhead_Below = 854,
	    U_Combining_Right_Half_Ring_Above = 855,
	    U_Combining_Dot_Above_Right = 856,
	    U_Combining_Asterisk_Below = 857,
	    U_Combining_Double_Ring_Below = 858,
	    U_Combining_Zigzag_Above = 859,
	    U_Combining_Double_Breve_Below = 860,
	    U_Combining_Double_Breve = 861,
	    U_Combining_Double_Macron = 862,
	    U_Combining_Double_Macron_Below = 863,
	    U_Combining_Double_Tilde = 864,
	    U_Combining_Double_Inverted_Breve = 865,
	    U_Combining_Double_Rightwards_Arrow_Below = 866,
	    U_Combining_Latin_Small_Letter_A = 867,
	    U_Combining_Latin_Small_Letter_E = 868,
	    U_Combining_Latin_Small_Letter_I = 869,
	    U_Combining_Latin_Small_Letter_O = 870,
	    U_Combining_Latin_Small_Letter_U = 871,
	    U_Combining_Latin_Small_Letter_C = 872,
	    U_Combining_Latin_Small_Letter_D = 873,
	    U_Combining_Latin_Small_Letter_H = 874,
	    U_Combining_Latin_Small_Letter_M = 875,
	    U_Combining_Latin_Small_Letter_R = 876,
	    U_Combining_Latin_Small_Letter_T = 877,
	    U_Combining_Latin_Small_Letter_V = 878,
	    U_Combining_Latin_Small_Letter_X = 879,
	    /**
	     * Unicode Character 'LINE SEPARATOR' (U+2028)
	     * http://www.fileformat.info/info/unicode/char/2028/index.htm
	     */
	    LINE_SEPARATOR_2028 = 8232,
	    U_CIRCUMFLEX = 94,
	    U_GRAVE_ACCENT = 96,
	    U_DIAERESIS = 168,
	    U_MACRON = 175,
	    U_ACUTE_ACCENT = 180,
	    U_CEDILLA = 184,
	    U_MODIFIER_LETTER_LEFT_ARROWHEAD = 706,
	    U_MODIFIER_LETTER_RIGHT_ARROWHEAD = 707,
	    U_MODIFIER_LETTER_UP_ARROWHEAD = 708,
	    U_MODIFIER_LETTER_DOWN_ARROWHEAD = 709,
	    U_MODIFIER_LETTER_CENTRED_RIGHT_HALF_RING = 722,
	    U_MODIFIER_LETTER_CENTRED_LEFT_HALF_RING = 723,
	    U_MODIFIER_LETTER_UP_TACK = 724,
	    U_MODIFIER_LETTER_DOWN_TACK = 725,
	    U_MODIFIER_LETTER_PLUS_SIGN = 726,
	    U_MODIFIER_LETTER_MINUS_SIGN = 727,
	    U_BREVE = 728,
	    U_DOT_ABOVE = 729,
	    U_RING_ABOVE = 730,
	    U_OGONEK = 731,
	    U_SMALL_TILDE = 732,
	    U_DOUBLE_ACUTE_ACCENT = 733,
	    U_MODIFIER_LETTER_RHOTIC_HOOK = 734,
	    U_MODIFIER_LETTER_CROSS_ACCENT = 735,
	    U_MODIFIER_LETTER_EXTRA_HIGH_TONE_BAR = 741,
	    U_MODIFIER_LETTER_HIGH_TONE_BAR = 742,
	    U_MODIFIER_LETTER_MID_TONE_BAR = 743,
	    U_MODIFIER_LETTER_LOW_TONE_BAR = 744,
	    U_MODIFIER_LETTER_EXTRA_LOW_TONE_BAR = 745,
	    U_MODIFIER_LETTER_YIN_DEPARTING_TONE_MARK = 746,
	    U_MODIFIER_LETTER_YANG_DEPARTING_TONE_MARK = 747,
	    U_MODIFIER_LETTER_UNASPIRATED = 749,
	    U_MODIFIER_LETTER_LOW_DOWN_ARROWHEAD = 751,
	    U_MODIFIER_LETTER_LOW_UP_ARROWHEAD = 752,
	    U_MODIFIER_LETTER_LOW_LEFT_ARROWHEAD = 753,
	    U_MODIFIER_LETTER_LOW_RIGHT_ARROWHEAD = 754,
	    U_MODIFIER_LETTER_LOW_RING = 755,
	    U_MODIFIER_LETTER_MIDDLE_GRAVE_ACCENT = 756,
	    U_MODIFIER_LETTER_MIDDLE_DOUBLE_GRAVE_ACCENT = 757,
	    U_MODIFIER_LETTER_MIDDLE_DOUBLE_ACUTE_ACCENT = 758,
	    U_MODIFIER_LETTER_LOW_TILDE = 759,
	    U_MODIFIER_LETTER_RAISED_COLON = 760,
	    U_MODIFIER_LETTER_BEGIN_HIGH_TONE = 761,
	    U_MODIFIER_LETTER_END_HIGH_TONE = 762,
	    U_MODIFIER_LETTER_BEGIN_LOW_TONE = 763,
	    U_MODIFIER_LETTER_END_LOW_TONE = 764,
	    U_MODIFIER_LETTER_SHELF = 765,
	    U_MODIFIER_LETTER_OPEN_SHELF = 766,
	    U_MODIFIER_LETTER_LOW_LEFT_ARROW = 767,
	    U_GREEK_LOWER_NUMERAL_SIGN = 885,
	    U_GREEK_TONOS = 900,
	    U_GREEK_DIALYTIKA_TONOS = 901,
	    U_GREEK_KORONIS = 8125,
	    U_GREEK_PSILI = 8127,
	    U_GREEK_PERISPOMENI = 8128,
	    U_GREEK_DIALYTIKA_AND_PERISPOMENI = 8129,
	    U_GREEK_PSILI_AND_VARIA = 8141,
	    U_GREEK_PSILI_AND_OXIA = 8142,
	    U_GREEK_PSILI_AND_PERISPOMENI = 8143,
	    U_GREEK_DASIA_AND_VARIA = 8157,
	    U_GREEK_DASIA_AND_OXIA = 8158,
	    U_GREEK_DASIA_AND_PERISPOMENI = 8159,
	    U_GREEK_DIALYTIKA_AND_VARIA = 8173,
	    U_GREEK_DIALYTIKA_AND_OXIA = 8174,
	    U_GREEK_VARIA = 8175,
	    U_GREEK_OXIA = 8189,
	    U_GREEK_DASIA = 8190,
	    U_OVERLINE = 8254,
	    /**
	     * UTF-8 BOM
	     * Unicode Character 'ZERO WIDTH NO-BREAK SPACE' (U+FEFF)
	     * http://www.fileformat.info/info/unicode/char/feff/index.htm
	     */
	    UTF8_BOM = 65279,
	}

}
declare module '@xblox/core/strings' {
	/**
	 * The empty string.
	 */
	export const empty = "";
	export function isFalsyOrWhitespace(str: string): boolean;
	/**
	 * @returns the provided number with the given number of preceding zeros.
	 */
	export function pad(n: number, l: number, char?: string): string;
	/**
	 * Helper to produce a string with a variable number of arguments. Insert variable segments
	 * into the string using the {n} notation where N is the index of the argument following the string.
	 * @param value string to which formatting is applied
	 * @param args replacements for {n}-entries
	 */
	export function format(value: string, ...args: any[]): string;
	/**
	 * Converts HTML characters inside the string to use entities instead. Makes the string safe from
	 * being used e.g. in HTMLElement.innerHTML.
	 */
	export function escape(html: string): string;
	/**
	 * Escapes regular expression characters in a given string
	 */
	export function escapeRegExpCharacters(value: string): string;
	/**
	 * Removes all occurrences of needle from the beginning and end of haystack.
	 * @param haystack string to trim
	 * @param needle the thing to trim (default is a blank)
	 */
	export function trim(haystack: string, needle?: string): string;
	/**
	 * Removes all occurrences of needle from the beginning of haystack.
	 * @param haystack string to trim
	 * @param needle the thing to trim
	 */
	export function ltrim(haystack?: string, needle?: string): string;
	/**
	 * Removes all occurrences of needle from the end of haystack.
	 * @param haystack string to trim
	 * @param needle the thing to trim
	 */
	export function rtrim(haystack?: string, needle?: string): string;
	export function convertSimple2RegExpPattern(pattern: string): string;
	export function stripWildcards(pattern: string): string;
	/**
	 * Determines if haystack starts with needle.
	 */
	export function startsWith(haystack: string, needle: string): boolean;
	/**
	 * Determines if haystack ends with needle.
	 */
	export function endsWith(haystack: string, needle: string): boolean;
	export function indexOfIgnoreCase(haystack: string, needle: string, position?: number): number;
	export interface RegExpOptions {
	    matchCase?: boolean;
	    wholeWord?: boolean;
	    multiline?: boolean;
	    global?: boolean;
	}
	export function createRegExp(searchString: string, isRegex: boolean, options?: RegExpOptions): RegExp;
	export function regExpLeadsToEndlessLoop(regexp: RegExp): boolean;
	/**
	 * The normalize() method returns the Unicode Normalization Form of a given string. The form will be
	 * the Normalization Form Canonical Composition.
	 *
	 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize}
	 */
	export let canNormalize: boolean;
	export function normalizeNFC(str: string): string;
	/**
	 * Returns first index of the string that is not whitespace.
	 * If string is empty or contains only whitespaces, returns -1
	 */
	export function firstNonWhitespaceIndex(str: string): number;
	/**
	 * Returns the leading whitespace of the string.
	 * If the string contains only whitespaces, returns entire string
	 */
	export function getLeadingWhitespace(str: string): string;
	/**
	 * Returns last index of the string that is not whitespace.
	 * If string is empty or contains only whitespaces, returns -1
	 */
	export function lastNonWhitespaceIndex(str: string, startIndex?: number): number;
	export function compare(a: string, b: string): number;
	export function compareIgnoreCase(a: string, b: string): number;
	export function equalsIgnoreCase(a: string, b: string): boolean;
	/**
	 * @returns the length of the common prefix of the two strings.
	 */
	export function commonPrefixLength(a: string, b: string): number;
	/**
	 * @returns the length of the common suffix of the two strings.
	 */
	export function commonSuffixLength(a: string, b: string): number;
	export function isHighSurrogate(charCode: number): boolean;
	export function isLowSurrogate(charCode: number): boolean;
	/**
	 * Returns true if `str` contains any Unicode character that is classified as "R" or "AL".
	 */
	export function containsRTL(str: string): boolean;
	/**
	 * Returns true if `str` contains only basic ASCII characters in the range 32 - 126 (including 32 and 126) or \n, \r, \t
	 */
	export function isBasicASCII(str: string): boolean;
	export function isFullWidthCharacter(charCode: number): boolean;
	/**
	 * Computes the difference score for two strings. More similar strings have a higher score.
	 * We use largest common subsequence dynamic programming approach but penalize in the end for length differences.
	 * Strings that have a large length difference will get a bad default score 0.
	 * Complexity - both time and space O(first.length * second.length)
	 * Dynamic programming LCS computation http://en.wikipedia.org/wiki/Longest_common_subsequence_problem
	 *
	 * @param first a string
	 * @param second a string
	 */
	export function difference(first: string, second: string, maxLenDelta?: number): number;
	/**
	 * Returns an array in which every entry is the offset of a
	 * line. There is always one entry which is zero.
	 */
	export function computeLineStarts(text: string): number[];
	/**
	 * Given a string and a max length returns a shorted version. Shorting
	 * happens at favorable positions - such as whitespace or punctuation characters.
	 */
	export function lcut(text: string, n: number): string;
	export function removeAnsiEscapeCodes(str: string): string;
	export const UTF8_BOM_CHARACTER: string;
	export function startsWithUTF8BOM(str: string): boolean;
	/**
	 * Appends two strings. If the appended result is longer than maxLength,
	 * trims the start of the result and replaces it with '...'.
	 */
	export function appendWithLimit(first: string, second: string, maxLength: number): string;
	export function safeBtoa(str: string): string;
	export function repeat(s: string, count: number): string;

}
declare module '@xblox/core/assert' {
	/**
	 * Throws an error with the provided message if the provided value does not evaluate to a true Javascript value.
	 */
	export function ok(value?: any, message?: string): void;

}
declare module '@xblox/core/arrays' {
	/**
	 * Returns the last element of an array.
	 * @param array The array.
	 * @param n Which element from the end (default ist zero).
	 */
	export function tail<T>(array: T[], n?: number): T;
	export function equals<T>(one: T[], other: T[], itemEquals?: (a: T, b: T) => boolean): boolean;
	export function binarySearch<T>(array: T[], key: T, comparator: (op1: T, op2: T) => number): number;
	/**
	 * Takes a sorted array and a function p. The array is sorted in such a way that all elements where p(x) is false
	 * are located before all elements where p(x) is true.
	 * @returns the least x for which p(x) is true or array.length if no element fullfills the given function.
	 */
	export function findFirst<T>(array: T[], p: (x: T) => boolean): number;
	/**
	 * Returns the top N elements from the array.
	 *
	 * Faster than sorting the entire array when the array is a lot larger than N.
	 *
	 * @param array The unsorted array.
	 * @param compare A sort function for the elements.
	 * @param n The number of elements to return.
	 * @return The first n elemnts from array when sorted with compare.
	 */
	export function top<T>(array: T[], compare: (a: T, b: T) => number, n: number): T[];
	/**
	 * @returns a new array with all undefined or null values removed. The original array is not modified at all.
	 */
	export function coalesce<T>(array: T[]): T[];
	/**
	 * Moves the element in the array for the provided positions.
	 */
	export function move(array: any[], from: number, to: number): void;
	/**
	 * @returns {{false}} if the provided object is an array
	 * 	and not empty.
	 */
	export function isFalsyOrEmpty(obj: any): boolean;
	/**
	 * Removes duplicates from the given array. The optional keyFn allows to specify
	 * how elements are checked for equalness by returning a unique string for each.
	 */
	export function distinct<T>(array: T[], keyFn?: (t: T) => string): T[];
	export function uniqueFilter<T>(keyFn: (t: T) => string): (t: T) => boolean;
	export function firstIndex<T>(array: T[], fn: (item: T) => boolean): number;
	export function first<T>(array: T[], fn: (item: T) => boolean, notFoundValue?: T): T;
	export function commonPrefixLength<T>(one: T[], other: T[], equals?: (a: T, b: T) => boolean): number;
	export function flatten<T>(arr: T[][]): T[];
	export function range(to: number, from?: number): number[];
	export function fill<T>(num: number, valueFn: () => T, arr?: T[]): T[];
	export function index<T>(array: T[], indexer: (t: T) => string): {
	    [key: string]: T;
	};
	export function index<T, R>(array: T[], indexer: (t: T) => string, merger?: (t: T, r: R) => R): {
	    [key: string]: R;
	};
	/**
	 * Inserts an element into an array. Returns a function which, when
	 * called, will remove that element from the array.
	 */
	export function insert<T>(array: T[], element: T): () => void;

}
declare module '@xblox/core/objects' {
	export function clone<T>(obj: T): T;
	export function deepClone<T>(obj: T): T;
	export function cloneAndChange(obj: any, changer: (orig: any) => any): any;
	/**
	 * Copies all properties of source into destination. The optional parameter "overwrite" allows to control
	 * if existing properties on the destination should be overwritten or not. Defaults to true (overwrite).
	 */
	export function mixin(destination: any, source: any, overwrite?: boolean): any;
	export function assign(destination: any, ...sources: any[]): any;
	export function toObject<T, R>(arr: T[], keyMap: (T: any) => string, valueMap?: (T: any) => R): {
	    [key: string]: R;
	};
	export function equals(one: any, other: any): boolean;
	export function ensureProperty(obj: any, property: string, defaultValue: any): void;
	export function arrayToHash(array: any[]): any;
	/**
	 * Given an array of strings, returns a function which, given a string
	 * returns true or false whether the string is in that array.
	 */
	export function createKeywordMatcher(arr: string[], caseInsensitive?: boolean): (str: string) => boolean;
	/**
	 * Started from TypeScript's __extends function to make a type a subclass of a specific class.
	 * Modified to work with properties already defined on the derivedClass, since we can't get TS
	 * to call this method before the constructor definition.
	 */
	export function derive(baseClass: any, derivedClass: any): void;
	/**
	 * Calls JSON.Stringify with a replacer to break apart any circular references.
	 * This prevents JSON.stringify from throwing the exception
	 *  "Uncaught TypeError: Converting circular structure to JSON"
	 */
	export function safeStringify(obj: any): string;
	export function getOrDefault<T, R>(obj: T, fn: (obj: T) => R, defaultValue?: R): R;

}
declare module '@xblox/core/utils' {
	export function StingEnum<T extends string>(o: Array<T>): {
	    [K in T]: K;
	};

}
declare module '@xblox/core/index' {
	export interface Hash<T> {
	    [id: string]: T;
	}
	export interface List<T> {
	    [index: number]: T;
	    length: number;
	}
	/**
	 * Interface of the simple literal object with any string keys.
	 */
	export interface IObjectLiteral {
	    [key: string]: any;
	}
	export type JSONPathExpression = string;

}
declare module '@xblox/core/hasLoader' {
	export interface Config {
	    baseUrl?: string;
	    map?: ModuleMap;
	    packages?: Package[];
	    paths?: {
	        [path: string]: string;
	    };
	    pkgs?: {
	        [path: string]: Package;
	    };
	}
	export interface ModuleMap extends ModuleMapItem {
	    [sourceMid: string]: ModuleMapReplacement;
	}
	export interface ModuleMapItem {
	    [mid: string]: any;
	}
	export interface ModuleMapReplacement extends ModuleMapItem {
	    [findMid: string]: string;
	}
	export interface Package {
	    location?: string;
	    main?: string;
	    name?: string;
	}
	export interface Require {
	    (dependencies: string[], callback: RequireCallback): void;
	    <ModuleType>(moduleId: string): ModuleType;
	    toAbsMid(moduleId: string): string;
	    toUrl(path: string): string;
	}
	export interface Has {
	    (name: string): any;
	    add(name: string, value: (global: Window, document?: HTMLDocument, element?: HTMLDivElement) => any, now?: boolean, force?: boolean): void;
	    add(name: string, value: any, now?: boolean, force?: boolean): void;
	}
	export type SignalType = 'error';
	export interface RootRequire extends Require {
	    has: Has;
	    on(type: SignalType, listener: any): {
	        remove: () => void;
	    };
	    config(config: Config): void;
	    inspect?(name: string): any;
	    nodeRequire?(id: string): any;
	    undef(moduleId: string): void;
	}
	export interface RequireCallback {
	    (...modules: any[]): void;
	}

}
declare module '@xblox/core/has' {
	import { Require, Config } from '@xblox/core/hasLoader';
	/**
	 * The valid return types from a feature test
	 */
	export type FeatureTestResult = boolean | string | number | undefined;
	/**
	 * A function that tests for a feature and returns a result
	 */
	export type FeatureTest = () => FeatureTestResult;
	export type FeatureTestThenable = {
	    then<U>(onFulfilled?: (value: FeatureTestResult) => U | FeatureTestThenable, onRejected?: (error: any) => U | FeatureTestThenable): FeatureTestThenable;
	    then<U>(onFulfilled?: (value: FeatureTestResult) => U | FeatureTestThenable, onRejected?: (error: any) => void): FeatureTestThenable;
	};
	/**
	 * A cache of results of feature tests
	 */
	export const testCache: {
	    [feature: string]: FeatureTestResult;
	};
	/**
	 * A cache of the un-resolved feature tests
	 */
	export const testFunctions: {
	    [feature: string]: FeatureTest;
	};
	export interface StaticHasFeatures {
	    [feature: string]: FeatureTestResult;
	}
	export interface DojoHasEnvironment {
	    /**
	     * Static features defined in the enviornment that should be used by the `has` module
	     * instead of run-time detection.
	     */
	    staticFeatures?: StaticHasFeatures | (() => StaticHasFeatures);
	} global  {
	    interface Window {
	        /**
	         * The `dojo/has` enviornment which provides configuration when the module is
	         * loaded.
	         */
	        DojoHasEnvironment?: DojoHasEnvironment;
	    }
	}
	export function load(resourceId: string, require: Require, load: (value?: any) => void, config?: Config): void;
	/**
	 * AMD plugin function.
	 *
	 * Resolves resourceId into a module id based on possibly-nested tenary expression that branches on has feature test
	 * value(s).
	 *
	 * @param resourceId The id of the module
	 * @param normalize Resolves a relative module id into an absolute module id
	 */
	export function normalize(resourceId: string, normalize: (moduleId: string) => string): string | null;
	/**
	 * Check if a feature has already been registered
	 *
	 * @param feature the name of the feature
	 */
	export function exists(feature: string): boolean;
	/**
	 * Register a new test for a named feature.
	 *
	 * @example
	 * has.add('dom-addeventlistener', !!document.addEventListener);
	 *
	 * @example
	 * has.add('touch-events', function () {
	 *    return 'ontouchstart' in document
	 * });
	 *
	 * @param feature the name of the feature
	 * @param value the value reported of the feature, or a function that will be executed once on first test
	 * @param overwrite if an existing value should be overwritten. Defaults to false.
	 */
	export function add(feature: string, value: FeatureTest | FeatureTestResult | FeatureTestThenable, overwrite?: boolean): void;
	/**
	 * Return the current value of a named feature.
	 *
	 * @param feature The name (if a string) or identifier (if an integer) of the feature to test.
	 */
	export default function has(feature: string): FeatureTestResult;

}
declare module '@xblox/core/promisify' {
	export function promisify<T>(f: (cb: (err: any, res: T) => void) => void, thisContext?: any): () => Promise<T>;
	export function promisify<A, T>(f: (arg: A, cb: (err: any, res: T) => void) => void, thisContext?: any): (arg: A) => Promise<T>;
	export function promisify<A, A2, T>(f: (arg: A, arg2: A2, cb: (err: any, res: T) => void) => void, thisContext?: any): (arg: A, arg2: A2) => Promise<T>;
	export function promisify<A, A2, A3, T>(f: (arg: A, arg2: A2, arg3: A3, cb: (err: any, res: T) => void) => void, thisContext?: any): (arg: A, arg2: A2, arg3: A3) => Promise<T>;
	export function promisify<A, A2, A3, A4, T>(f: (arg: A, arg2: A2, arg3: A3, arg4: A4, cb: (err: any, res: T) => void) => void, thisContext?: any): (arg: A, arg2: A2, arg3: A3, arg4: A4) => Promise<T>;
	export function promisify<A, A2, A3, A4, A5, T>(f: (arg: A, arg2: A2, arg3: A3, arg4: A4, arg5: A5, cb: (err: any, res: T) => void) => void, thisContext?: any): (arg: A, arg2: A2, arg3: A3, arg4: A4, arg5: A5) => Promise<T>;
	export function map<T, U>(elts: PromiseLike<PromiseLike<T>[]>, f: (t: T) => U | PromiseLike<U>): Promise<U[]>;
	export function map<T, U>(elts: PromiseLike<T[]>, f: (t: T) => U | PromiseLike<U>): Promise<U[]>;
	export function map<T, U>(elts: PromiseLike<T>[], f: (t: T) => U | PromiseLike<U>): Promise<U[]>;
	export function map<T, U>(elts: T[], f: (t: T) => U | PromiseLike<U>): Promise<U[]>;
	export function _try<T>(f: () => T): Promise<T>;
	export function _try<T>(f: (arg: any) => T, arg: any): Promise<T>;
	export function _try<T>(f: (arg: any, arg2: any) => T, arg: any, arg2: any): Promise<T>;
	export function _try<T>(f: (arg: any, arg2: any, arg3: any) => T, arg: any, arg2: any, arg3: any): Promise<T>;
	export function _try<T>(f: (arg: any, arg2: any, arg3: any, arg4: any) => T, arg: any, arg2: any, arg3: any, arg4: any): Promise<T>;

}
declare module '@xblox/core/io/json' {
	import { IObjectLiteral } from '@xblox/core/index';
	import 'reflect-metadata';
	export function read(file: string): string;
	export const to: (data: any[] | IObjectLiteral, path?: string) => any;
	export const serialize: (value: any, replacer?: (key: string, value: any) => any, space?: string | number) => string;
	export const deserialize: (value: string) => any;
	/**
	 * Calls JSON.Stringify with a replacer to break apart any circular references.
	 * This prevents JSON.stringify from throwing the exception
	 *  "Uncaught TypeError: Converting circular structure to JSON"
	 */
	export const safe: (obj: any) => string;
	export interface IJsonMetaData<T> {
	    name?: string;
	    clazz?: {
	        new (): T;
	    };
	}
	/**
	 * Decorator to map/support dashed property names
	 *
	 * @export
	 * @template T
	 * @param {(IJsonMetaData<T> | string)} [metadata]
	 * @returns {*}
	 */
	export function JsonProperty<T>(metadata?: IJsonMetaData<T> | string): any;
	export function getClazz(target: any, propertyKey: string): any;
	export function getJsonProperty<T>(target: any, propertyKey: string): IJsonMetaData<T>;
	/**
	* @example
	// Example - JSON-Mapping
	class Address {
	    @JsonProperty('first-line')
	    firstLine: string;
	    @JsonProperty('second-line')
	    secondLine: string;
	    city: string;

	    // Default constructor will be called by mapper
	    constructor() {
	        this.firstLine = undefined;
	        this.secondLine = undefined;
	        this.city = undefined;
	    }
	}

	class Person {
	    name: string;
	    surname: string;
	    age: number;
	    @JsonProperty('address')
	    address: Address;

	    // Default constructor will be called by mapper
	    constructor() {Array.
	        this.name = undefined;
	        this.surname = undefined;
	        this.age = undefined;
	        this.address = undefined;
	    }
	}
	let example = {
	    "name": "Mark",
	    "surname": "Galea",
	    "age": 30,
	    "address": {
	        "first-line": "Some where",
	        "second-line": "Over Here",
	        "city": "In This City"
	    }
	};
	const Person2 = Map.deserialize(Person, example);
	*/
	export class Map {
	    static isPrimitive(obj: any): boolean;
	    static isArray(object: any): boolean;
	    static deserialize<T>(clazz: {
	        new (): T;
	    }, jsonObject: any): T;
	}

}
declare module '@xblox/core/io/base64' {
	import { JSONPathExpression, IObjectLiteral } from '@xblox/core/index';
	export function to(data: Array<any> | IObjectLiteral, path?: JSONPathExpression): any;
	export function encode(data: string): string;

}
declare module '@xblox/core' {
	}
