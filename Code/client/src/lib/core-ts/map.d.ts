export interface Key {
    toString(): string;
}
export interface Entry<K, T> {
    next?: Entry<K, T>;
    prev?: Entry<K, T>;
    key: K;
    value: T;
}
/**
 * A simple map to store value by a key object. Key can be any object that has toString() function to get
 * string value of the key.
 */
export declare class LinkedMap<K extends Key, T> {
    protected map: {
        [key: string]: Entry<K, T>;
    };
    protected _size: number;
    constructor();
    readonly size: number;
    get(k: K): T;
    getOrSet(k: K, t: T): T;
    keys(): K[];
    values(): T[];
    entries(): Entry<K, T>[];
    set(k: K, t: T): boolean;
    delete(k: K): T;
    has(k: K): boolean;
    clear(): void;
    protected push(key: K, value: T): void;
    protected pop(k: K): void;
    protected peek(k: K): T;
}
/**
 * A simple Map<T> that optionally allows to set a limit of entries to store. Once the limit is hit,
 * the cache will remove the entry that was last recently added. Or, if a ratio is provided below 1,
 * all elements will be removed until the ratio is full filled (e.g. 0.75 to remove 25% of old elements).
 */
export declare class BoundedLinkedMap<T> {
    private limit;
    protected map: {
        [key: string]: Entry<string, T>;
    };
    private head;
    private tail;
    private _size;
    private ratio;
    constructor(limit?: number, ratio?: number);
    readonly size: number;
    set(key: string, value: T): boolean;
    get(key: string): T;
    getOrSet(k: string, t: T): T;
    delete(key: string): T;
    has(key: string): boolean;
    clear(): void;
    protected push(entry: Entry<string, T>): void;
    private trim();
}
/**
 * A subclass of Map<T> that makes an entry the MRU entry as soon
 * as it is being accessed. In combination with the limit for the
 * maximum number of elements in the cache, it helps to remove those
 * entries from the cache that are LRU.
 */
export declare class LRUCache<T> extends BoundedLinkedMap<T> {
    constructor(limit: number);
    get(key: string): T;
}
/**
 * A trie map that allows for fast look up when keys are substrings
 * to the actual search keys (dir/subdir-problem).
 */
export declare class TrieMap<E> {
    static PathSplitter: (s: string) => string[];
    private _splitter;
    private _root;
    constructor(splitter: (s: string) => string[]);
    insert(path: string, element: E): void;
    lookUp(path: string): E;
    findSubstr(path: string): E;
    findSuperstr(path: string): TrieMap<E>;
}
