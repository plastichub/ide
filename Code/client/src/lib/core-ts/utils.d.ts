export declare function StingEnum<T extends string>(o: Array<T>): {
    [K in T]: K;
};
