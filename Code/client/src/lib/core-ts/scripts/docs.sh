rm -rf docs
typedoc --excludeExternals --ignoreCompilerErrors --out ./docs/ src/**/*.ts
git add -A ./docs/*
gitc "docs"
