define([
    'dcl/dcl',
    'xwire/Source',
    'xide/mixins/EventedMixin'
],function(dcl,Source,EventedMixin){
    /**
     * Event based binding source
     */
    return dcl([Source,EventedMixin.dcl],{
        declaredClass:"xwire.EventSource",
        /**
         * Trigger specifies the event name
         * {String|Array}
         */
        trigger:null,
        /**
         * Path to the value within the triggered event.
         * {String}
         */
        path:null,
        /**
         * An array of filters, specified by path and value :
         * {
         *      path:string,
         *      value:string (path value must match this)
         * }
         *
         */
        filters:null,

        _started:false,
        /**
         * _matches evaluates a number of filters on an object
         * @param object {Object}
         * @param filters {Array}
         * @returns {boolean}
         * @private
         */
        _matches:function(object,filters){
            for(var i =0 ; i<filters.length;i++){
                var value = this.getValue(object,filters[i].path);
                if(value!==filters[i].value){
                    return false;
                }
            }
            return true;
        },
        /**
         *
         * @param evt
         */
        onTriggered:function(evt){
            /**
             * Run event filters if specified
             */
            if(this.filters && !this._matches(evt,this.filters)){
                return;
            }
            if(this.path){

                var value = this.getValue(evt,this.path);
                /**
                 * forward to owner
                 */
                if(this.binding){
                    this.binding.trigger({
                        value:value,
                        source:this
                    })
                }
            }
        },
        /***
         * Start will subscribe to event specified in trigger
         */
        start:function(){
            if(this._started){
                return;
            }
            this.subscribe(this.trigger,this.onTriggered);

            this._started = true;
        }
    });
});