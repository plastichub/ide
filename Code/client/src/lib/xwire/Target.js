/** @module xwire/Target */
define([
    'dcl/dcl',
    'xwire/_Base'
],function(dcl,_Base){
    /**
     * Abstract binding target
     * @class xwire/Target
     */
    var Module = dcl(_Base,{
        declaredClass:"xwire.Target",
        /**
         * The binding we belong to
         * {xwire.Binding}
         */
        binding:null,
        /**
         * The object instance we're operating on
         */
        object:null,
        /**
         * The path within the object
         */
        path:null,
        /**
         * run is performed when a source is being triggered
         */
        run:function(){

        },
        /**
         * start the target, this is not really needed
         */
        start:function(){

        }
    });
    dcl.chainAfter(Module,'run');
    return Module
});