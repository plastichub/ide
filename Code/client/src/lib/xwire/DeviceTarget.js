define([
    'dcl/dcl',
    'xide/types',
    'xwire/Target'
],function(dcl,types,Target){
    /**
     * Widget based binding target
     */
    return dcl(Target,{
        declaredClass:"xwire.DeviceTarget",
        /***
         * An optional variable to set before calling a command
         */
        variable:null,
        /**
         * The command to call (uses this.object(Block)) {String}
         */
        command:null,
        /**
         * Run the action
         */
        run:function(data){
            this.inherited(arguments);
            if(this.object){
                if(this.variable){
                    this.object.setVariable(this.variable,data.value,false,false,types.MESSAGE_SOURCE.GUI);
                }
                if(this.command){
                    this.object.callCommand(this.command);
                }
            }
        }
    });
});