/** @module xwire/Source */
define([
    'dcl/dcl',
    'xide/types',
    'xwire/_Base'
],function(dcl,types,_Base){
    /**
     * Abstract binding source
     * @class xwire/Source
     * @extends {module:xwire/_Base}
     */
    return dcl(_Base,{
        declaredClass:"xwire.Source"
    });
});