/** @module xwire/Binding */
define([
    'dcl/dcl',
    'xwire/_Base'
],function(dcl,_Base){
    /**
     * @class xwire/Binding
     */
    return dcl(_Base,{
        declaredClass:"xwire.Binding",
        /**
         * @type {module:xdeliteful/Source}
         */
        source:null,
        /**
         * @type {module:xwire/Target}
         */
        target:null,
        /**
         * @type {module:xdeliteful/Script}
         */
        accept:null,
        /**
         * @type {module:xdeliteful/Script}
         */
        transform:null,
        destroy:function(){
            this.source && this.source.destroy();
            this.target && this.target.destroy();
            delete this.transform;
            delete this.accept;
        },
        /**
         * trigger is being invoked by the source
         */
        trigger:function(data){
            if(this.target){
                this.target.run(data);
            }
        },
        /**
         *
         */
        start:function(){
            if(this.source){
                this.source.binding=this;
                this.source.start();
            }
            if(this.target){
                this.target.binding=this;
                this.target.start();
            }
        }
    });
});