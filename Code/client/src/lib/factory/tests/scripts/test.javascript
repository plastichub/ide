var variablesToJavascript = function (skipVariable) {

    var result = '';
    var data = this.variableStore ? this.variableStore.data : this.variables;
    for (var i = 0; i < data.length; i++) {
        var _var = data[i];
        if (_var == skipVariable) {
            continue;
        }
        var _varVal = '' + _var.value;
        if (_varVal.length == 0) {
            continue;
        }
        if (!this.isScript(_varVal) && _varVal.indexOf("'") == -1) {
            _varVal = "'" + _varVal + "'";
        }
        /*
         else if(this.isScript(_varVal)){
         _varVal = "''";
         }
         */
        result += "var " + _var.title + " = " + _varVal + ";";
        result += "\n";
    }

    return result;
};
var out = 0;
if (value.indexOf('MV') != -1 &&
    value.indexOf('MVMAX') == -1) {
    //var _volume = '/MV "([^\"]+)"/i'
    var _volume = value.substring(2, value.length);
    //normalize
    _volume = parseInt(_volume.substring(0, 2));
    if (!isNaN(_volume)) {
        this.setVariable('Volume', _volume);
        out = _volume;
    }
}
return out;

var value = this.getVariable('value');
var out = 0;
if (value.indexOf('MV') != -1 && value.indexOf('MVMAX') == -1) {
    var _volume = value.substring(2, value.length);
    _volume = parseInt(_volume.substring(0, 2));
    if (!isNaN(_volume)) {
        this.setVariable('Volume', _volume);
        out = _volume;
    }else{
        return null;
    }
}else{
    return null;
}
return out;