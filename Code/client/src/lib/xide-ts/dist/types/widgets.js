"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * To be moved
 * @type {{SIZE_NORMAL: string, SIZE_SMALL: string, SIZE_WIDE: string, SIZE_LARGE: string}}
 */
exports.DIALOG_SIZE = {
    SIZE_NORMAL: 'size-normal',
    SIZE_SMALL: 'size-small',
    SIZE_WIDE: 'size-wide',
    SIZE_LARGE: 'size-large'
};
/**
 * To be moved
 * @type {{DEFAULT: string, INFO: string, PRIMARY: string, SUCCESS: string, WARNING: string, DANGER: string}}
 */
exports.DIALOG_TYPE = {
    DEFAULT: 'type-default',
    INFO: 'type-info',
    PRIMARY: 'type-primary',
    SUCCESS: 'type-success',
    WARNING: 'type-warning',
    DANGER: 'type-danger'
};
//# sourceMappingURL=widgets.js.map