"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("./styles/base.scss");
var greeter_1 = require("./greeter");
var greeter = new greeter_1.Greeter("xide-ts");
document.getElementById("greeting").innerHTML = greeter.greet();
//# sourceMappingURL=app.js.map