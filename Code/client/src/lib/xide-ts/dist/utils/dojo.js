"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash = require("lodash");
/**
 * Ask require registry at this path
 * @param mixed
 * @returns {*}
 */
exports.hasObject = function (mixed) {
    var result = null;
    var _re = require;
    try {
        result = _re(mixed);
    }
    catch (e) {
        console.error('error in hasObject ', e);
    }
    return result;
};
exports.debounce = function (who, methodName, fn, delay, options, now, args) {
    var _var = methodName + '_debounced';
    var where = who[_var];
    if (!where) {
        where = who[_var] = lodash.debounce(fn, delay, options);
    }
    if (now === true) {
        if (!who[_var]) {
            who[_var] = true;
            fn.apply(who, args);
        }
    }
    return where();
};
/**
 * Safe require.toUrl
 * @param mid {string}
 */
exports.toUrl = function (mid) {
    var _require = require;
    // make sure cache bust is off otherwise it appends ?time
    _require({
        cacheBust: null,
        waitSeconds: 5
    });
    return _require.toUrl(mid);
};
//# sourceMappingURL=dojo.js.map