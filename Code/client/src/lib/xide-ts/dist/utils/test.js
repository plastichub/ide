"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _hasOwnProperty = Object.prototype.hasOwnProperty;
/////////////////////////////////////////////////////////////////////////////////////////////
//
//  True object utils
//
//////////////////////////////////////////////////////////////////////////////////////////////
exports.toArray2 = function (obj) {
    var result = [];
    for (var c in obj) {
        result.push({
            name: c,
            value: obj[c]
        });
    }
    return result;
};
//# sourceMappingURL=test.js.map