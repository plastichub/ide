define(["require", "exports", "./greeter"], function (require, exports, greeter_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    describe("Greeter", function () {
        var greeter;
        beforeEach(function () {
            greeter = new greeter_1.Greeter("testing!");
        });
        it("should greet", function () {
            expect(greeter.greet()).toBe("<h1>testing!</h1>");
        });
    });
});
//# sourceMappingURL=greeter.spec.js.map