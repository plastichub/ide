define(["require", "exports", "lodash"], function (require, exports, lodash) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var _debug = false;
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Loader utils
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    exports.debounce = function (who, methodName, fn, delay, options, now, args) {
        var place = who[methodName + '_debounced'];
        if (!place) {
            place = who[methodName + '_debounced'] = lodash.debounce(fn, delay, options);
        }
        if (now === true) {
            if (!who[methodName + '_debouncedFirst']) {
                who[methodName + '_debouncedFirst'] = true;
                fn.apply(who, args);
            }
        }
        return place();
    };
    exports.pluck = function (items, prop) { return lodash.map(items, prop); };
    /**
     * Ask require registry at this path
     * @param mixed
     * @returns {*}
     */
    exports.hasObject = function (mixed) {
        var result = null;
        var _re = require;
        try {
            result = _re(mixed);
        }
        catch (e) {
            console.error('error in hasObject ', e);
        }
        return result;
    };
    /**
     * Safe require.toUrl
     * @param mid {string}
     */
    exports.toUrl = function (mid) {
        var _require = require;
        // make sure cache bust is off otherwise it appends ?time
        _require({
            cacheBust: null,
            waitSeconds: 5
        });
        return _require.toUrl(mid);
    };
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  True object utils
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    exports.toArray = function (obj) {
        var result = [];
        for (var c in obj) {
            result.push({
                name: c,
                value: obj[c]
            });
        }
        return result;
    };
    /**
     * Gets an object property by string, eg: byString(someObj, 'part3[0].name');
     * @deprecated, see objectAtPath below
     * @param o {Object}    : the object
     * @param s {String}    : the path within the object
     * @param defaultValue {Object|String|Number} : an optional default value
     * @returns {*}
     */
    exports.byString = function (o, s, defaultValue) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, ''); // strip a leading dot
        var a = s.split('.');
        while (a.length) {
            var n = a.shift();
            if (n in o) {
                o = o[n];
            }
            else {
                return;
            }
        }
        return o;
    };
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Object path
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Internals
     */
    //cache
    var toStr = Object.prototype.toString;
    var _hasOwnProperty = Object.prototype.hasOwnProperty;
    /**
     * @private
     * @param type
     * @returns {*}
     */
    function toString(type) {
        return toStr.call(type);
    }
    /**
     * @private
     * @param key
     * @returns {*}
     */
    function getKey(key) {
        var intKey = parseInt(key, 10);
        if (intKey.toString() === key) {
            return intKey;
        }
        return key;
    }
    /**
     * internal set value at path in object
     * @private
     * @param obj
     * @param path
     * @param value
     * @param doNotReplace
     * @returns {*}
     */
    function set(obj, path, value, doNotReplace) {
        if (lodash.isNumber(path)) {
            path = [path];
        }
        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isString(path)) {
            return set(obj, path.split('.').map(getKey), value, doNotReplace);
        }
        var currentPath = path[0];
        if (path.length === 1) {
            var oldVal = obj[currentPath];
            if (oldVal === void 0 || !doNotReplace) {
                obj[currentPath] = value;
            }
            return oldVal;
        }
        if (obj[currentPath] === void 0) {
            //check if we assume an array
            // tslint:disable-next-line:prefer-conditional-expression
            if (lodash.isNumber(path[1])) {
                obj[currentPath] = [];
            }
            else {
                obj[currentPath] = {};
            }
        }
        return set(obj[currentPath], path.slice(1), value, doNotReplace);
    }
    /**
     * deletes an property by a path
     * @param obj
     * @param path
     * @returns {*}
     */
    function del(obj, path) {
        if (lodash.isNumber(path)) {
            path = [path];
        }
        if (lodash.isEmpty(obj)) {
            return void 0;
        }
        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isString(path)) {
            return del(obj, path.split('.'));
        }
        var currentPath = getKey(path[0]);
        var oldVal = obj[currentPath];
        if (path.length === 1) {
            if (oldVal !== void 0) {
                if (lodash.isArray(obj)) {
                    obj.splice(currentPath, 1);
                }
                else {
                    delete obj[currentPath];
                }
            }
        }
        else {
            if (obj[currentPath] !== void 0) {
                return del(obj[currentPath], path.slice(1));
            }
        }
        return obj;
    }
    /**
     * Private helper class
     * @private
     * @type {{}}
     */
    var objectPath = {};
    objectPath.has = function (obj, path) {
        if (lodash.isEmpty(obj)) {
            return false;
        }
        if (lodash.isNumber(path)) {
            path = [path];
        }
        else if (lodash.isString(path)) {
            path = path.split('.');
        }
        if (lodash.isEmpty(path) || path.length === 0) {
            return false;
        }
        for (var i = 0; i < path.length; i++) {
            var j = path[i];
            if ((lodash.isObject(obj) || lodash.isArray(obj)) && _hasOwnProperty.call(obj, j)) {
                obj = obj[j];
            }
            else {
                return false;
            }
        }
        return true;
    };
    /**
     * Define private public 'ensure exists'
     * @param obj
     * @param path
     * @param value
     * @returns {*}
     */
    objectPath.ensureExists = function (obj, path, value) {
        return set(obj, path, value, true);
    };
    /**
     * Define private public 'set'
     * @param obj
     * @param path
     * @param value
     * @param doNotReplace
     * @returns {*}
     */
    objectPath.set = function (obj, path, value, doNotReplace) {
        return set(obj, path, value, doNotReplace);
    };
    /**
     Define private public 'insert'
     * @param obj
     * @param path
     * @param value
     * @param at
     */
    objectPath.insert = function (obj, path, value, at) {
        var arr = objectPath.get(obj, path);
        at = ~~at;
        if (!lodash.isArray(arr)) {
            arr = [];
            objectPath.set(obj, path, arr);
        }
        arr.splice(at, 0, value);
    };
    /**
     * Define private public 'empty'
     * @param obj
     * @param path
     * @returns {*}
     */
    objectPath.empty = function (obj, path) {
        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isEmpty(obj)) {
            return void 0;
        }
        var value, i;
        if (!(value = objectPath.get(obj, path))) {
            return obj;
        }
        if (lodash.isString(value)) {
            return objectPath.set(obj, path, '');
        }
        else if (lodash.isBoolean(value)) {
            return objectPath.set(obj, path, false);
        }
        else if (lodash.isNumber(value)) {
            return objectPath.set(obj, path, 0);
        }
        else if (lodash.isArray(value)) {
            value.length = 0;
        }
        else if (lodash.isObject(value)) {
            for (i in value) {
                if (_hasOwnProperty.call(value, i)) {
                    delete value[i];
                }
            }
        }
        else {
            return objectPath.set(obj, path, null);
        }
    };
    /**
     * Define private public 'push'
     * @param obj
     * @param path
     */
    objectPath.push = function (obj, path /*, values */) {
        var arr = objectPath.get(obj, path);
        if (!lodash.isArray(arr)) {
            arr = [];
            objectPath.set(obj, path, arr);
        }
        arr.push.apply(arr, Array.prototype.slice.call([obj, path], 2));
    };
    /**
     * Define private public 'coalesce'
     * @param obj
     * @param paths
     * @param defaultValue
     * @returns {*}
     */
    objectPath.coalesce = function (obj, paths, defaultValue) {
        var value;
        for (var i = 0, len = paths.length; i < len; i++) {
            if ((value = objectPath.get(obj, paths[i])) !== void 0) {
                return value;
            }
        }
        return defaultValue;
    };
    /**
     * Define private public 'get'
     * @param obj
     * @param path
     * @param defaultValue
     * @returns {*}
     */
    objectPath.get = function (obj, path, defaultValue) {
        if (lodash.isNumber(path)) {
            path = [path];
        }
        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isEmpty(obj)) {
            // lodash doesnt seem to work with html nodes
            if (obj && obj.innerHTML === null) {
                return defaultValue;
            }
        }
        if (lodash.isString(path)) {
            return objectPath.get(obj, path.split('.'), defaultValue);
        }
        var currentPath = getKey(path[0]);
        if (path.length === 1) {
            if (obj && obj[currentPath] === void 0) {
                return defaultValue;
            }
            if (obj) {
                return obj[currentPath];
            }
        }
        if (!obj) {
            return defaultValue;
        }
        return objectPath.get(obj[currentPath], path.slice(1), defaultValue);
    };
    /**
     * Define private public 'del'
     * @param obj
     * @param path
     * @returns {*}
     */
    objectPath.del = function (obj, path) {
        return del(obj, path);
    };
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Object path public xide/utils mixin
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  Returns a value by a give object path
     *
     *  //works also with arrays
     *    objectPath.get(obj, "a.c.1");  //returns "f"
     *    objectPath.get(obj, ["a","c","1"]);  //returns "f"
     *
     * @param obj {object}
     * @param path {string}
     * @param _default {object|null}
     * @returns {*}
     */
    exports.getAt = function (obj, path, _default) {
        return objectPath.get(obj, path, _default);
    };
    /**
     * Sets a value in an object/array at a given path.
     * @example
     *
     * setAt(obj, "a.h", "m"); // or setAt(obj, ["a","h"], "m");
     *
     * //set will create intermediate object/arrays
     * objectPath.set(obj, "a.j.0.f", "m");
     *
     * @param obj{Object|Array}
     * @param path {string}
     * @param value {mixed}
     * @returns {Object|Array}
     */
    exports.setAt = function (obj, path, value) {
        return objectPath.set(obj, path, value);
    };
    /**
     * Returns there is anything at given path within an object/array.
     * @param obj
     * @param path
     */
    exports.hasAt = function (obj, path) {
        return objectPath.has(obj, path);
    };
    /**
     * Ensures at given path, otherwise _default will be placed
     * @param obj
     * @param path
     * @returns {*}
     */
    exports.ensureAt = function (obj, path, _default) {
        return objectPath.ensureExists(obj, path, _default);
    };
    /**
     * Deletes at given path
     * @param obj
     * @param path
     * @returns {*}
     */
    exports.deleteAt = function (obj, path) {
        return objectPath.del(obj, path);
    };
    /**
     *
     * @param to
     * @param from
     * @returns {*}
     */
    exports.merge = function (to, from) {
        for (var n in from) {
            if (typeof to[n] !== 'object') {
                to[n] = from[n];
            }
            else if (typeof from[n] === 'object') {
                to[n] = exports.merge(to[n], from[n]);
            }
        }
        return to;
    };
    /**
     * Type guard that ensures that the value can be coerced to Object
     * to weed out host objects that do not derive from Object.
     * This function is used to check if we want to deep copy an object or not.
     * Note: In ES6 it is possible to modify an object's Symbol.toStringTag property, which will
     * change the value returned by `toString`. This is a rare edge case that is difficult to handle,
     * so it is not handled here.
     * @param  value The value to check
     * @return       If the value is coercible into an Object
     */
    // tslint:disable-next-line:ban-types
    function shouldDeepCopyObject(value) {
        return Object.prototype.toString.call(value) === '[object Object]';
    }
    function copyArray(array, inherited) {
        return array.map(function (item) {
            if (Array.isArray(item)) {
                return copyArray(item, inherited);
            }
            return !shouldDeepCopyObject(item) ?
                item :
                _mixin({
                    deep: true,
                    inherited: inherited,
                    sources: [item],
                    target: {}
                });
        });
    }
    function _mixin(kwArgs) {
        var deep = kwArgs.deep;
        var inherited = kwArgs.inherited;
        var target = kwArgs.target;
        var copied = kwArgs.copied || [];
        var copiedClone = copied.slice();
        for (var _i = 0, _a = kwArgs.sources; _i < _a.length; _i++) {
            var source = _a[_i];
            if (source === null || source === undefined) {
                continue;
            }
            for (var key in source) {
                if (inherited || _hasOwnProperty.call(source, key)) {
                    var value = source[key];
                    if (copiedClone.indexOf(value) !== -1) {
                        continue;
                    }
                    if (deep) {
                        if (Array.isArray(value)) {
                            value = copyArray(value, inherited);
                        }
                        else if (shouldDeepCopyObject(value)) {
                            var targetValue = target[key] || {};
                            copied.push(source);
                            value = _mixin({
                                deep: true,
                                inherited: inherited,
                                sources: [value],
                                target: targetValue,
                                copied: copied
                            });
                        }
                    }
                    target[key] = value;
                }
            }
        }
        return target;
    }
    function deepMixin(target) {
        var sources = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            sources[_i - 1] = arguments[_i];
        }
        return _mixin({
            deep: true,
            inherited: true,
            sources: sources,
            target: target
        });
    }
    exports.deepMixin = deepMixin;
    /**
     * Creates a new object using the provided source's prototype as the prototype for the new object, and then
     * deep copies the provided source's values into the new target.
     *
     * @param source The object to duplicate
     * @return The new object
     */
    function clone(source) {
        var target = Object.create(Object.getPrototypeOf(source));
        return deepMixin(target, source);
    }
    exports.clone = clone;
    exports.cloneKeys = function (defaults, skipEmpty) {
        var result = {};
        for (var _class in defaults) {
            if (skipEmpty === true && !(_class in defaults)) {
                continue;
            }
            result[_class] = defaults[_class];
        }
        return result;
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  STD
    exports.isArray = function (what) { return lodash.isArray(what); };
    exports.isObject = function (what) { return lodash.isObject(what); };
    exports.isString = function (what) { return lodash.isString(what); };
    exports.isNumber = function (what) { return lodash.isNumber(what); };
    exports.isFunction = function (it) { return lodash.isFunction(it); };
});
//# sourceMappingURL=ObjectUtils.js.map