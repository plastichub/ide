define(["require", "exports", "./greeter", "./styles/base.scss"], function (require, exports, greeter_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var greeter = new greeter_1.Greeter("xide-ts");
    document.getElementById("greeting").innerHTML = greeter.greet();
});
//# sourceMappingURL=app.js.map