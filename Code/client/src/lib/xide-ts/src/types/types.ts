export const GRID_FEATURE = {
    KEYBOARD_NAVIGATION: 'KEYBOARD_NAVIGATION',
    KEYBOARD_SELECT: 'KEYBOARD_SELECT',
    SELECTION: 'SELECTION',
    ACTIONS: 'ACTIONS',
    CONTEXT_MENU: 'CONTEXT_MENU'
};

export const VIEW_FEATURE = {
    KEYBOARD_NAVIGATION: 'KEYBOARD_NAVIGATION',
    KEYBOARD_SELECT: 'KEYBOARD_SELECT',
    SELECTION: 'SELECTION',
    ACTIONS: 'ACTIONS',
    CONTEXT_MENU: 'CONTEXT_MENU'
};

export const KEYBOARD_PROFILE = {
    DEFAULT: {
        prevent_default: true,
        prevent_repeat: false
    },
    PASS_THROUGH: {
        prevent_default: false,
        prevent_repeat: false
    },
    SEQUENCE: {
        prevent_default: true,
        is_sequence: true,
        prevent_repeat: true
    }
};
/////////////////////////////////////////////////////////////////////////////
//
// CORE TYPES
//
/////////////////////////////////////////////////////////////////////////////

/**
* Stub for registered bean types. This value is needed to let the UI switch between configurations per such type.
* At the very root is the bean action context which may include more contexts.
* @enum {string} module:xide/types/ITEM_TYPE
* @memberOf module:xide/types
*/
export const ITEM_TYPE = {
    /**
     * Bean type 'file' is handled by the xfile package
     * @constant
     */
    FILE: 'BTFILE', //file object
    /**
     * Bean type 'widget' is handled by the xide/ve and davinci package
     * @constant
     */
    WIDGET: 'WIDGET', //ui designer
    /**
     * Bean type 'block' is handled by the xblox package
     * @constant
     */
    BLOCK: 'BLOCK', //xblox
    /**
     * Bean type 'text' is used for text editors
     * @constant
     */
    TEXT: 'TEXT', //xace
    /**
     * Bean type 'xexpression' is used for user expressions
     * @constant
     */
    EXPRESSION: 'EXPRESSION' //xexpression
};
/**
 * Component names stub, might be extended by sub-classing applications
 * @constant xide.types.COMPONENT_NAMES
 */
export const COMPONENT_NAMES = {
    XIDEVE: 'xideve',
    XNODE: 'xnode',
    XBLOX: 'xblox',
    XFILE: 'xfile',
    XACE: 'xace',
    XEXPRESSION: 'xexpression',
    XCONSOLE: 'xconsole',
    XTRACK: 'xtrack'
};

/**
 * All client resources are through variables on the server side. Here the minimum variables for an xjs application.
 *
 * @constant {Array.<module:xide/types~RESOURCE_VARIABLES>}
 *     module:xide/types~RESOURCE_VARIABLES
 */
export const RESOURCE_VARIABLES = {
    ACE: 'ACE',
    APP_URL: 'APP_URL',
    SITE_URL: 'SITE_URL'
};
/**
 * Events of xide.*
 * @enum {string} module:xide/types/EVENTS
 * @memberOf module:xide/types
 * @extends xide/types
 */
export const EVENTS = {
    /**
     * generic
     */
    ERROR: 'onError', //xhr
    STATUS: 'onStatus', //xhr
    ON_CREATED_MANAGER: 'onCreatedManager', //context

    /**
     * item events, to be renoved
     */
    ON_ITEM_SELECTED: 'onItemSelected',
    ON_ITEM_REMOVED: 'onItemRemoved',
    ON_ITEM_CLOSED: 'onItemClosed',
    ON_ITEM_ADDED: 'onItemAdded',
    ON_ITEM_MODIFIED: 'onItemModified',
    ON_NODE_SERVICE_STORE_READY: 'onNodeServiceStoreReady',
    /**
     * old, to be removd
     */
    ON_FILE_STORE_READY: 'onFileStoreReady',
    ON_CONTEXT_MENU_OPEN: 'onContextMenuOpen',
    /**
     * CI events
     */
    ON_CI_UPDATE: 'onCIUpdate',

    /**
     * widgets
     */
    ON_WIDGET_READY: 'onWidgetReady',
    ON_CREATED_WIDGET: 'onWidgetCreated',
    RESIZE: 'onResize',
    /**
     * Event to notify classes about a reloaded module
     * @constant
     * @type string
     */
    ON_MODULE_RELOADED: 'onModuleReloaded',
    ON_MODULE_UPDATED: 'onModuleUpdated',

    ON_DID_OPEN_ITEM: 'onDidOpenItem', //remove
    ON_DID_RENDER_COLLECTION: 'onDidRenderCollection', //move

    ON_PLUGIN_LOADED: 'onPluginLoaded',
    ON_PLUGIN_READY: 'onPluginReady',
    ALL_PLUGINS_READY: 'onAllPluginsReady',

    /**
     * editors
     */
    ON_CREATE_EDITOR_BEGIN: 'onCreateEditorBegin', //move to xedit
    ON_CREATE_EDITOR_END: 'onCreateEditorEnd', //move to xedit
    REGISTER_EDITOR: 'registerEditor', //move to xedit
    ON_EXPRESSION_EDITOR_ADD_FUNCTIONS: 'onExpressionEditorAddFunctions', //move to xedit
    ON_ACE_READY: 'onACEReady', //remove

    /**
     * Files
     */
    ON_UNSAVED_CONTENT: 'onUnSavedContent',
    ON_FILE_CHANGED: 'fileChanged',
    ON_FILE_DELETED: 'fileDeleted',
    IMAGE_LOADED: 'imageLoaded',
    IMAGE_ERROR: 'imageError',
    UPLOAD_BEGIN: 'uploadBegin',
    UPLOAD_PROGRESS: 'uploadProgress',
    UPLOAD_FINISH: 'uploadFinish',
    UPLOAD_FAILED: 'uploadFailed',
    ON_FILE_CONTENT_CHANGED: 'onFileContentChanged',
    ON_COPY_BEGIN: 'onCopyBegin',
    ON_COPY_END: 'onCopyEnd',
    ON_DELETE_BEGIN: 'onDeleteBegin',
    ON_DELETE_END: 'onDeleteEnd',
    ON_MOVE_BEGIN: 'onMoveBegin',
    ON_MOVE_END: 'onMoveEnd',
    ON_CHANGED_CONTENT: 'onChangedContent',
    ON_COMPRESS_BEGIN: 'onCompressBegin',
    ON_COMPRESS_END: 'onCompressEnd',

    ON_COMPONENT_READY: 'onComponentReady',
    ON_ALL_COMPONENTS_LOADED: 'onAllComponentsLoaded',
    ON_APP_READY: 'onAppReady',
    /**
     * Store
     */
    ON_CREATE_STORE: 'onCreateStore',
    ON_STORE_CREATED: 'onStoreCreated',
    ON_STORE_CHANGED: 'onStoreChanged',
    ON_STATUS_MESSAGE: 'onStatusMessage',
    /**
     * layout
     */
    SAVE_LAYOUT: 'layoutSave',
    RESTORE_LAYOUT: 'layoutRestore',
    /**
     * views, panels and 'main view'
     */
    ON_SHOW_PANEL: 'onShowPanel',
    ON_PANEL_CLOSED: 'onPanelClosed',
    ON_PANEL_CREATED: 'onPanelCreated',

    ON_MAIN_VIEW_READY: 'onMainViewReady',
    ON_MAIN_MENU_READY: 'onMainMenuReady',
    ON_MAIN_MENU_OPEN: 'onMainMenuOpen',
    ON_VIEW_REMOVED: 'onViewRemoved',
    ON_VIEW_SHOW: 'onViewShow',
    ON_VIEW_HIDE: 'onViewHide',
    ON_VIEW_ADDED: 'onViewAdded',
    ON_OPEN_VIEW: 'onOpenView',
    ON_VIEW_MAXIMIZE_START: 'onViewMaximizeStart',
    ON_VIEW_MAXIMIZE_END: 'onViewMaximizeEnd',
    ON_CONTAINER_ADDED: 'onContainerAdded',
    ON_CONTAINER_REMOVED: 'onContainerRemoved',
    ON_REMOVE_CONTAINER: 'onRemoveContainer',
    ON_CONTAINER_REPLACED: 'onContainerReplaced',
    ON_CONTAINER_SPLIT: 'onContainerSplit',
    ON_RENDER_WELCOME_GRID_GROUP: 'onRenderWelcomeGridGroup',

    ON_DND_SOURCE_OVER: '/dnd/source/over',
    ON_DND_START: '/dnd/start',
    ON_DND_DROP_BEFORE: '/dnd/drop/before',
    ON_DND_DROP: '/dnd/drop',
    ON_DND_CANCEL: '/dnd/cancel'
};
