/**
 * To be moved
 * @type {{SIZE_NORMAL: string, SIZE_SMALL: string, SIZE_WIDE: string, SIZE_LARGE: string}}
 */
export const DIALOG_SIZE = {
    SIZE_NORMAL: 'size-normal',
    SIZE_SMALL: 'size-small',
    SIZE_WIDE: 'size-wide', // size-wide is equal to modal-lg
    SIZE_LARGE: 'size-large'
};

/**
 * To be moved
 * @type {{DEFAULT: string, INFO: string, PRIMARY: string, SUCCESS: string, WARNING: string, DANGER: string}}
 */
export const DIALOG_TYPE = {
    DEFAULT: 'type-default',
    INFO: 'type-info',
    PRIMARY: 'type-primary',
    SUCCESS: 'type-success',
    WARNING: 'type-warning',
    DANGER: 'type-danger'
};
