import * as lodash from 'lodash';
declare let require: any;
/**
 * Ask require registry at this path
 * @param mixed
 * @returns {*}
 */
export const hasObject = (mixed: any) => {
    let result = null;
    const _re = require;
    try {
        result = _re(mixed);
    } catch (e) {
        console.error('error in hasObject ', e);
    }
    return result;
};

export const debounce = (who: any, methodName, fn, delay, options, now, args) => {
    const _var = methodName + '_debounced';
    let where = who[_var];
    if (!where) {
        where = who[_var] = lodash.debounce(fn, delay, options);
    }
    if (now === true) {
        if (!who[_var]) {
            who[_var] = true;
            fn.apply(who, args);
        }
    }
    return where();
};
/**
 * Safe require.toUrl
 * @param mid {string}
 */
export const toUrl = (mid: string) => {
    const _require = require;
    // make sure cache bust is off otherwise it appends ?time
    _require({
        cacheBust: null,
        waitSeconds: 5
    });
    return _require.toUrl(mid);
};
