import * as lodash from 'lodash';
const toStr = Object.prototype.toString;
const _hasOwnProperty = Object.prototype.hasOwnProperty;

/**
 * Gets an object property by string, eg: byString(someObj, 'part3[0].name');
 * @deprecated, see objectAtPath below
 * @param o {Object}    : the object
 * @param s {String}    : the path within the object
 * @param defaultValue {Object|String|Number} : an optional default value
 * @returns {*}
 */
export const byString = (o, s, defaultValue) => {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    const a = s.split('.');
    while (a.length) {
        const n = a.shift();
        if (n in o) {
            o = o[n];
        } else {
            return;
        }
    }
    return o;
};

/**
 * @private
 * @param type
 * @returns {*}
 */
function toString(type) {
    return toStr.call(type);
}

/**
 * @private
 * @param key
 * @returns {*}
 */
function getKey(key) {
    const intKey = parseInt(key, 10);
    if (intKey.toString() === key) {
        return intKey;
    }
    return key;
}

/**
 * internal set value at path in object
 * @private
 * @param obj
 * @param path
 * @param value
 * @param doNotReplace
 * @returns {*}
 */
function set(obj, path, value, doNotReplace) {
    if (lodash.isNumber(path)) {
        path = [path];
    }
    if (lodash.isEmpty(path)) {
        return obj;
    }
    if (lodash.isString(path)) {
        return set(obj, path.split('.').map(getKey), value, doNotReplace);
    }
    const currentPath = path[0];

    if (path.length === 1) {
        const oldVal = obj[currentPath];
        if (oldVal === void 0 || !doNotReplace) {
            obj[currentPath] = value;
        }
        return oldVal;
    }

    if (obj[currentPath] === void 0) {
        //check if we assume an array
        // tslint:disable-next-line:prefer-conditional-expression
        if (lodash.isNumber(path[1])) {
            obj[currentPath] = [];
        } else {
            obj[currentPath] = {};
        }
    }
    return set(obj[currentPath], path.slice(1), value, doNotReplace);
}

/**
 * deletes an property by a path
 * @param obj
 * @param path
 * @returns {*}
 */
function del(obj, path) {
    if (lodash.isNumber(path)) {
        path = [path];
    }
    if (lodash.isEmpty(obj)) {
        return void 0;
    }

    if (lodash.isEmpty(path)) {
        return obj;
    }
    if (lodash.isString(path)) {
        return del(obj, path.split('.'));
    }

    const currentPath = getKey(path[0]);
    const oldVal = obj[currentPath];

    if (path.length === 1) {
        if (oldVal !== void 0) {
            if (lodash.isArray(obj)) {
                obj.splice(currentPath, 1);
            } else {
                delete obj[currentPath];
            }
        }
    } else {
        if (obj[currentPath] !== void 0) {
            return del(obj[currentPath], path.slice(1));
        }
    }
    return obj;
}

/**
 * Private helper class
 * @private
 * @type {{}}
 */
const objectPath: any = {};

objectPath.has = (obj, path) => {
    if (lodash.isEmpty(obj)) {
        return false;
    }
    if (lodash.isNumber(path)) {
        path = [path];
    } else if (lodash.isString(path)) {
        path = path.split('.');
    }

    if (lodash.isEmpty(path) || path.length === 0) {
        return false;
    }

    for (var i = 0; i < path.length; i++) {
        const j = path[i];
        if ((lodash.isObject(obj) || lodash.isArray(obj)) && _hasOwnProperty.call(obj, j)) {
            obj = obj[j];
        } else {
            return false;
        }
    }

    return true;
};

/**
 * Define private public 'ensure exists'
 * @param obj
 * @param path
 * @param value
 * @returns {*}
 */
objectPath.ensureExists = (obj, path, value) => {
    return set(obj, path, value, true);
};

/**
 * Define private public 'set'
 * @param obj
 * @param path
 * @param value
 * @param doNotReplace
 * @returns {*}
 */
objectPath.set = (obj, path, value, doNotReplace) => {
    return set(obj, path, value, doNotReplace);
};

/**
 Define private public 'insert'
 * @param obj
 * @param path
 * @param value
 * @param at
 */
objectPath.insert = (obj, path, value, at) => {
    var arr = objectPath.get(obj, path);
    at = ~~at;
    if (!lodash.isArray(arr)) {
        arr = [];
        objectPath.set(obj, path, arr);
    }
    arr.splice(at, 0, value);
};

/**
 * Define private public 'empty'
 * @param obj
 * @param path
 * @returns {*}
 */
objectPath.empty = (obj, path) => {
    if (lodash.isEmpty(path)) {
        return obj;
    }
    if (lodash.isEmpty(obj)) {
        return void 0;
    }

    let value, i;
    if (!(value = objectPath.get(obj, path))) {
        return obj;
    }

    if (lodash.isString(value)) {
        return objectPath.set(obj, path, '');
    } else if (lodash.isBoolean(value)) {
        return objectPath.set(obj, path, false);
    } else if (lodash.isNumber(value)) {
        return objectPath.set(obj, path, 0);
    } else if (lodash.isArray(value)) {
        value.length = 0;
    } else if (lodash.isObject(value)) {
        for (i in value) {
            if (_hasOwnProperty.call(value, i)) {
                delete value[i];
            }
        }
    } else {
        return objectPath.set(obj, path, null);
    }
};

/**
 * Define private public 'push'
 * @param obj
 * @param path
 */
objectPath.push = (obj, path /*, values */) => {
    var arr = objectPath.get(obj, path);
    if (!lodash.isArray(arr)) {
        arr = [];
        objectPath.set(obj, path, arr);
    }
    arr.push.apply(arr, Array.prototype.slice.call([obj, path], 2));
};

/**
 * Define private public 'coalesce'
 * @param obj
 * @param paths
 * @param defaultValue
 * @returns {*}
 */
objectPath.coalesce = (obj, paths, defaultValue) => {
    var value;
    for (var i = 0, len = paths.length; i < len; i++) {
        if ((value = objectPath.get(obj, paths[i])) !== void 0) {
            return value;
        }
    }
    return defaultValue;
};

/**
 * Define private public 'get'
 * @param obj
 * @param path
 * @param defaultValue
 * @returns {*}
 */
objectPath.get = (obj, path, defaultValue) => {
    if (lodash.isNumber(path)) {
        path = [path];
    }
    if (lodash.isEmpty(path)) {
        return obj;
    }
    if (lodash.isEmpty(obj)) {
        // lodash doesnt seem to work with html nodes
        if (obj && obj.innerHTML === null) {
            return defaultValue;
        }
    }
    if (lodash.isString(path)) {
        return objectPath.get(obj, path.split('.'), defaultValue);
    }
    const currentPath = getKey(path[0]);
    if (path.length === 1) {
        if (obj && obj[currentPath] === void 0) {
            return defaultValue;
        }
        if (obj) {
            return obj[currentPath];
        }
    }
    if (!obj) {
        return defaultValue;
    }
    return objectPath.get(obj[currentPath], path.slice(1), defaultValue);
};

/**
 * Define private public 'del'
 * @param obj
 * @param path
 * @returns {*}
 */
objectPath.del = (obj, path) => {
    return del(obj, path);
};
/////////////////////////////////////////////////////////////////////////////////////////////
//
//  Object path public xide/utils mixin
//
//////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Returns a value by a give object path
 *
 *  //works also with arrays
 *    objectPath.get(obj, "a.c.1");  //returns "f"
 *    objectPath.get(obj, ["a","c","1"]);  //returns "f"
 *
 * @param obj {object}
 * @param path {string}
 * @param _default {object|null}
 * @returns {*}
 */
export const getAt = (obj, path, _default) => {
    return objectPath.get(obj, path, _default);
};

/**
 * Sets a value in an object/array at a given path.
 * @example
 *
 * setAt(obj, "a.h", "m"); // or setAt(obj, ["a","h"], "m");
 *
 * //set will create intermediate object/arrays
 * objectPath.set(obj, "a.j.0.f", "m");
 *
 * @param obj{Object|Array}
 * @param path {string}
 * @param value {mixed}
 * @returns {Object|Array}
 */
export const setAt = (obj, path, value) => {
    return objectPath.set(obj, path, value);
};

/**
 * Returns there is anything at given path within an object/array.
 * @param obj
 * @param path
 */
export const hasAt = (obj, path) => {
    return objectPath.has(obj, path);
};

/**
 * Ensures at given path, otherwise _default will be placed
 * @param obj
 * @param path
 * @returns {*}
 */
export const ensureAt = (obj, path, _default) => {
    return objectPath.ensureExists(obj, path, _default);
};
/**
 * Deletes at given path
 * @param obj
 * @param path
 * @returns {*}
 */
export const deleteAt = (obj, path) => {
    return objectPath.del(obj, path);
};
