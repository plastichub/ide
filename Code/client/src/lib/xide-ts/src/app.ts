import "./styles/base.scss";

import { Greeter } from "./greeter";

const greeter: Greeter = new Greeter("xide-ts");

document.getElementById("greeting").innerHTML = greeter.greet();
