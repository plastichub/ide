define([
    'dcl/dcl',
    'xdojo/has',
    'xdojo/declare',
    'xide/utils',
    'dojo/Deferred'
],function(dcl,has,declare,utils,Deferred){
    const Implementation = {
        declaredClass:"xide.mixins.Electron",
        getElectron:function(){
            return this.require('electron');
        },
        require:function(){
            return window['eRequire'] ? window['eRequire'].apply(null,arguments) : null;
        },
        getApp:function(){
            return this.remote().app;
        },
        srequire:function(){
            return this.remote().require.apply(null,arguments);
        },
        remote:function(){
            const _require = window['eRequire'];
            const remote = _require('electron').remote;
            return remote;
        },
        getClipboard:function(){
            const _e = this.getElectron();
            return _e ? _e.clipboard : null;
        },
        cwd:function(){
            const path = this.srequire('path');
            const app = this.getApp();
            if(has('debug')) {
                return path.resolve(app.getAppPath()+'');
            }
        }
    };

    const Module = declare("xide/mixins/Electron",null,Implementation);
    Module.dcl = dcl(null,Implementation);
    return Module;
});

