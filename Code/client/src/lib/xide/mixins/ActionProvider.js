define([
    'dcl/inherited',
    'xaction/DefaultActions',
    'xaction/ActionProvider'
], function (inherited,DefaultActions,ActionProvider) {
    return ActionProvider;
});
