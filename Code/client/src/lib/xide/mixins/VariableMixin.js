/** @mixin xide/mixin/VariableMixin **/
define([
    'dcl/dcl',
    'xdojo/declare',
    'xide/utils'
], function (dcl,declare,utils) {

    const Implementation = {
        /**
         *
         * @param what
         * @param variables
         * @param delimitters
         * @returns {*}
         */
        resolve:function(what,variables,delimitters){
            variables = variables || this.resourceVariables || this.ctx.getResourceManager().getResourceVariables() || null;
            delimitters = delimitters || this.variableDelimiters || null;
            return utils.replace(what,null,variables,delimitters);
        }
    };

    /**
     * Mixin to resolve resource variables in strings.
     * Currently stub
     */
    const Module = declare("xide/mixins/VariableMixin", null, Implementation);
    Module.dcl = dcl(null,Implementation);
    return Module;
});