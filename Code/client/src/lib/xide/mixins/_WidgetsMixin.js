/** @module xide/mixins/_WidgetsMixin**/
define([
    "xdojo/declare",
    "xide/registry"
], function (declare,registry) {
    /**
     * Generic state implementation
     * @class module:xide/mixins/_WidgetsMixin
     */
    return declare("xide/mixins/_WidgetsMixin",null,{
        _getNode:function(){
            return this.containerNode || this.domNode;
        },
        _getParentWidget:function(declaredClass){
            let i = 0;
            let element = this._getNode();
            let widget = null;

            while (i < 2 && !widget) {
                if (element) {
                    element = element.parentNode;
                    widget = registry.getEnclosingWidget(element);
                    if (!widget) {
                        widget = registry.getEnclosingWidget(element);
                    }
                }
                i++;
            }

            return widget;
        },
        /**
         * Search upwards for a parent by class string or module
         *
         * @param className {string|Object}
         * @returns {*}
         *
         * @private
         */
        _parentByClass: function(className) {
            let parent = this._getParentWidget(className);
            if(_.isString(className)) {
                while (parent && !(parent.declaredClass.includes(className))) {
                    parent = parent._getParentWidget();
                }
            }
            return parent;
        }
    });
});