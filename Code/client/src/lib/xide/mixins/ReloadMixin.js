/** @module xide/mixins/ReloadMixin **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/mixins/EventedMixin'
], function (declare,dcl,types, utils,EventedMixin) {

    /**
     * Mixin which adds functions for module reloading to the consumer. All functions
     * of the reloaded module will be overridden with the re-loaded module's version.
     * Recommended: turn off web-cache and use turn on 'cacheBust' in your Dojo-Config.
     *
     *
     *  <b>Usage</b> :
     *  1. call initReload manually (subscribes to types.EVENTS.ON_MODULE_RELOADED which is triggered by module:xide/manager/Context )
     *  2. that's it actually.
     *  3. optionally add a function 'onReloaded' to your sub class and refresh yourself, ie: re-create widgets or simply test
     *  a piece of code
     *
     *
     * @mixin module:xide/mixins/ReloadMixin
     * @requires module:xide/mixins/EventedMixin
     */
    const Impl = {
        /**
         *
         * Not used yet, but at @TODO: some flags to describe the hot-replace for reloaded modules
         *
         */
        _mergeFunctions: true,
        _mergeMissingVariables: true,
        /**
         * Cross instanceOf equal check, tries:
         *
         *  1. native
         *  2. Dojo::isInstanceOf
         *  3. baseClasses
         *
         * @param cls {Object}
         * @returns {boolean}
         */
        isInstanceOf_: function (cls) {

            try {

                //Try native and then Dojo declare's _Stateful::isInstanceOf
                if (!!this instanceof cls
                    || (this.isInstanceOf && this.isInstanceOf(cls))) {

                    return true;

                }else{
                    //manual lookup, browse all base classes and their superclass
                    const //save get, return base::at[path] or empty array
                    bases = utils.getAt(this, 'constructor._meta.bases', []);    //cache

                    const _clsClass = cls.prototype.declaredClass;

                    //save space
                    return _.findWhere(bases, function (_base) {

                        return _base == cls    //direct cmp
                        || utils.getAt(_base, 'superclass.declaredClass') === _clsClass   //mostly here
                        || utils.getAt(_base, 'prototype.declaredClass') === _clsClass;    //sometimes

                    });
                }

            } catch (e) {
                //may crash, no idea why!
                console.log('ReloadMixin :: this.isInstanceOf_ crashed ' + e);
            }

            return false;

        },
        /**
         * @TODO: use flag guarded mixin
         *
         * @param target
         * @param source
         */
        mergeFunctions: function (target, source) {
            for (const i in source) {
                const o = source[i];
                if (i === 'constructor' || i === 'inherited') {
                    continue;
                }
                if (_.isFunction(source[i])) {
                    target[i] = null;//be nice
                    target[i] = source[i];//override
                }
                //support missing properties
                if (_.isArray(source[i])) {
                    if (target[i] == null) {
                        target[i] = source[i];
                    }
                }
            }
        },
        /**
         * Event callback for xide/types/EVENTS/ON_MODULE_RELOADED when a module has been reloaded.
         * @member
         * @param evt
         */
        onModuleReloaded: function (evt) {
            //console.log('on module reloaded');
            const newModule = evt.newModule;
            if (!newModule || !newModule.prototype || evt._processed) {
                return;
            }
            const moduleProto = newModule.prototype;
            const moduleClass = moduleProto.declaredClass;
            let matchedByClass = false;
            const thisClass = this.declaredClass;
            const thiz=this;

            if(!moduleClass){
                return;
            }
            if (moduleClass && thisClass) {
                //determine by dotted normalized declaredClass
                matchedByClass = utils.replaceAll('/', '.', thisClass) === utils.replaceAll('/', '.', moduleClass);
            }
            if (matchedByClass) {
                thiz.mergeFunctions(thiz, moduleProto);
                if (thiz.onReloaded) {
                    evt._processed = true;
                    thiz.onReloaded(newModule);
                }
            } else if (evt.module && utils.replaceAll('//', '/', evt.module) === thisClass) {//not sure this needed left
                //dcl module!
                thiz.mergeFunctions(thiz, moduleProto);
            }
        },
        /**
         * Public entry; call that in your sub-class to init this functionality!
         */
        initReload: function () {
            this.subscribe(types.EVENTS.ON_MODULE_RELOADED);
        }
    };

    //package via declare
    const _class = declare(null,Impl);

    //static access to Impl.
    _class.Impl = Impl;

    _class.dcl = dcl(EventedMixin.dcl,Impl);

    return _class;

});