define([
    "dcl/dcl",
    "xdojo/declare",
    "xide/utils"
], function (dcl, declare, utils) {
    const Implementation = {
        store: null,
        getActionStore: function () {
            return this.store;
        },
        setActionStore: function (store) {
            return this.store = store;
        },
        /**
         * Sort
         * @param groups
         * @param groupMap
         * @returns {*}
         */
        sortGroups: function (groups, groupMap) {
            groups = groups.sort(function (a, b) {
                if (a.label && b.label && groupMap[a.label] != null && groupMap[b.label] != null) {
                    const orderA = groupMap[a.label];
                    const orderB = groupMap[b.label];
                    return orderB - orderA;
                }
                return 100;
            });
            return groups;
        },
        /**
         * The visibility filter. There are the events "REGISTER_ACTION" and "SET_ITEM_ACTIONS" a sub-class might listening too.
         * When receiving a new set of actions, in most cases any income action needs to be filtered agains this value.         *
         * Please @see {model:xide/action/Action for more}.
         *
         * @member visibility {module:xide/types/ACTION_VISIBILITY}
         * @type {string|null}
         */
        visibility: null,
        /**
         * All actions, there is also _incomingActions for the last set
         * type {xide/action/Action[]}
         */
        _actions: [],
        /**
         *
         * @param visibility {string|null}
         */
        clearActions: function (visibility, _store) {
            visibility = visibility || this.visibility;
            const store = _store || this.getActionStore();
            const actions = store.data;

            if (!store) {
                return;
            }
            actions && actions.forEach((action) => {
                const actionVisibility = action.getVisibility != null ? action.getVisibility(visibility) : null;
                if (actionVisibility) {
                    const widget = actionVisibility.widget;
                    if (widget) {
                        //remove action reference widget
                        action.removeReference && action.removeReference(widget);
                        widget.destroy();
                        this.setVisibilityField(action, 'widget', null);
                    }
                }
            });
        },
        /**
         * computeList modifies a set of actions in that way:
         *
         * 1. prepare the incoming list of 'actions' by grouping them using the actions.command first path element.
         * 2. order the inner branches created above, using the action's 'order', 'group' and 'command' field
         * 3. composite the the entire tree by overriding each top level's 'item' attribute
         *
         * @param items {xide/action/Action[]}
         * @returns {xide/action/Action[]} the modified version of the input
         * @private
         */
        _computeList: function (items, add) {
            return this._actions;
        },
        /**
         * Return a field from the object's given visibility store
         * @param action
         * @param field
         * @param _default
         * @returns {*}
         */
        getVisibilityField: function (action, field, _default) {
            const actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return actionVisibility[field] != null ? actionVisibility[field] : action[field] || _default;
        },
        /**
         * Sets a field in the object's given visibility store
         * @param action
         * @param field
         * @param value
         * @returns {*}
         */
        setVisibilityField: function (action, field, value) {
            const _default = {};
            if (action.getVisibility) {
                var actionVisibility = action.getVisibility(this.visibility) || _default;
                actionVisibility[field] = value;
            }
            return actionVisibility;
        },
        shouldShowAction: function (action) {
            if (this.getVisibilityField(action, 'show') == false) {
                return false;
            } else if (action.getVisibility && action.getVisibility(this.visibility) == null) {
                return false;
            }
            return true;
        }
    };

    /**
     * Provides tools to deal with 'actions' (xide/action/Action). This is the model part for actions which is being used
     * always together with the render part(xide/widgets/_MenuMixin) in a subclass.
     *
     * @mixin module:xide/mixins/ActionMixin
     */
    const Module = declare("xide/mixins/ActionMixin", null, Implementation);
    Module.dcl = dcl(null, Implementation);
    return Module;
});