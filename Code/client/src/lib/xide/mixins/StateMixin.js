define([
    "dojo/_base/declare",
    "xide/utils"
], function (declare, utils) {

    /**
     * Provides tools to deal with 'persistence' (open files, editors, ...etc to be restored). It also acts as interface.
     *
     * @mixin module:xide/mixins/StateMixin
     * examples:
     * Suggestion:
     * path and option separation by : |
     *
     */
    return declare("xide/mixins/StateMixin", null, {});
});
