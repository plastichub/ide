define([
    'dcl/dcl',
    'xdojo/declare',
    'xide/factory',
    'xide/types',
    'xide/data/Memory'
], function (dcl,declare, factory, types,Memory) {

    //exported mixin
    let editorMixin;

    //singleton store
    let store;

    const debug=false;

    editorMixin = dcl(null, {
        declaredClass:"xide.editor.Registry",
        delegate: null,
        editors: null,
        _started: false,
        startup: function () {
            this.subscribe(types.EVENTS.REGISTER_EDITOR, function (data) {
                editorMixin.onRegisterEditor(data);
            });
        }
    });

    editorMixin.editors = [];

    const editors = editorMixin.editors;

    editorMixin.getStore = function(){
        if(!store){
            store = new Memory({
                idProperty:'name'
            })
        }
        return store;
    };

    editorMixin.getExtension = function (fname) {
        if (fname != null) {
            return fname.substr((~-fname.lastIndexOf(".") >>> 0) + 2);
        }
        return "";
    };

    editorMixin._hasEditor = function (name,extensions) {
        return _.find(editorMixin.editors,{
            name:name,
            extensions:extensions
        });
    };

    editorMixin.unregisterEditor = function (name) {
        const _store = editorMixin.getStore();
        const editors = _store._find({
            name:name
        });
        function unregister(editor){
            _store.removeSync(editor.name);
            _.each(editors,function(_editor){
                if(_editor && _editor.name === editor.name){
                    editors.remove(_editor);
                }
            });
        }
        _.each(editors,unregister);
    };

    editorMixin.onRegisterEditor = function (eventData) {
        const _store = editorMixin.getStore();
        const allEditors = _store._find({
            name:eventData.name
        });
       if(allEditors.length>0){
           debug && console.warn('Editor already registered',eventData);
           _.each(allEditors,function(editor){
               _store.removeSync(editor.name);
               editorMixin.unregisterEditor(editor.name);
           });
        }
        _store.putSync(eventData);
        if (!editorMixin._hasEditor(eventData.name,eventData.extensions)) {
            editors.push(eventData);
        }
    };

    editorMixin.getDefaultEditor = function (item) {
        const editors = editorMixin.getEditors(item);
        if (!editors) {
            return null;
        }

        for (let i = 0; i < editors.length; i++) {
            const obj = editors[i];
            if (obj.isDefault === true) {
                return obj;
            }
        }
        return null;
    };

    editorMixin.getEditor = function(name){
        return _.find(editors,{
            name:name
        });
    };

    editorMixin.getEditors = function (item) {
        if(!item){
            return editors;
        }
        let extension = editorMixin.getExtension(item.path);
        if (extension != null && extension.length == 0) {
            return null;
        }
        extension=extension.toLowerCase();
        const result = [];
        if (!editorMixin.editors) {
            return null;
        }

        const store = editorMixin.getStore();
        const _defaultEditor = store._find({
            defaultEditor:true
        });

        editors.forEach(editor => {
            if (editor) {
                //check multiple extensions
                if (editor.extensions.includes('|')) {
                    const supportedPluginExtensions = editor.extensions.split('|');

                    supportedPluginExtensions.forEach(supportedPluginExtension => {
                        if (supportedPluginExtension === extension) {
                            result.push(editor);
                        }
                    });
                } else {
                    //otherwise single extension match
                    if (editor.extensions === extension) {
                        result.push(editor);
                    }
                }
            }
        });

        if(_defaultEditor.length==1){
            result.push(_defaultEditor[0]);
        }

        if (result.length > 0) {
            return result;
        }
        return null;
    };

    return editorMixin;
});