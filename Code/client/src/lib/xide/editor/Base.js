/** @module xide/editor/Base **/
define([
    "dcl/dcl",
    'xide/types',
    'xaction/ActionProvider',
    'xide/layout/Container',
    'dojo/Deferred'
], function (dcl, types, ActionProvider, Container, Deferred) {
    return dcl([Container, ActionProvider.dcl], {
        menuOrder: {
            'File': 110,
            'Edit': 100,
            'View': 90,
            'Block': 50,
            'Settings': 20,
            'Navigation': 10,
            'Editor': 9,
            'Step': 5,
            'New': 4,
            'Window': 3,
            'Help': 2
        },
        declaredClass: 'xide/views/Editor',
        options: null,
        /**
         * The icon class when doing any storage operation
         * @member loadingIcon {string}
         */
        loadingIcon: 'fa-spinner fa-spin',
        /**
         * The original icon class
         * @member iconClassNormal {string}
         */
        iconClassNormal: 'fa-code',
        //////////////////////////////////////////////////////////////////
        //
        // Model - GET/PUT
        //
        getContent: function (item, onSuccess, onError) {
            if (!this.storeDelegate) {
                onError && onError('Editor::getContent : Have no store delegate!');
            } else {
                this.storeDelegate.getContent(function (content) {
                    onSuccess(content);
                }, item || this.item);
            }
        },
        saveContent: function (value, onSuccess, onError) {
            const thiz = this;
            this.set('iconClass', 'fa-spinner fa-spin');
            const _value = value || this.get('value');
            if (!_value) {
                console.log('Editor::saveContent : Have nothing to save, editor seems empty');
            }
            if (!this.storeDelegate) {
                if (onError) {
                    onError('Editor::saveContent : Have no store delegate!');
                }
                return false;
            } else {
                const _s = function () {
                    thiz.set('iconClass', thiz.iconClassNormal);
                    thiz.lastSavedContent = _value;
                    thiz.onContentChange && thiz.onContentChange(false);
                    thiz.publish(types.EVENTS.ON_FILE_CONTENT_CHANGED,{
                        item: thiz.item,
                        content: _value,
                        editor: thiz
                    }, thiz);
                };
                return this.storeDelegate.saveContent(_value, _s, null, thiz.item);
            }
        },
        onLoaded: function () {
            this.set('iconClass', this.iconClassNormal);
        },
        startup: function () {
            //save icon class normal
            this.iconClassNormal = '' + this.iconClass;
            this.set('iconClass', 'fa-spinner fa-spin');
            const self = this;
            const options = this.options || {};
            const item = this.item;
            const ctx = this.ctx;
            const result = new Deferred();

            this.storeDelegate = {
                getContent: function (onSuccess) {
                    ctx.getFileManager().getContent(item.mount, item.path, onSuccess);
                },
                saveContent: function (value, onSuccess, onError) {
                    ctx.getFileManager().setContent(item.mount, item.path, value, onSuccess);
                }
            };

            if (!this.item && this.value == null) {
                return result;
            }

            function createEditor(options, value) {
                self.createEditor(self.options || options, value);
            }

            if (this.value != null) {
                createEditor(null, this.value);
            } else {
                //we have no content yet, call in _TextEditor::getContent, this will be forwarded
                //to our 'storeDelegate'
                this.getContent(
                    this.item,
                    function (content) {//onSuccess
                        self.set('iconClass', self.iconClassNormal);
                        self.lastSavedContent = content;
                        createEditor(options, content);
                    },
                    function (e) {//onError
                        createEditor(null, '');
                        logError(e, 'error loading content from file');
                    }
                );
            }
            return result;
        }
    });
});