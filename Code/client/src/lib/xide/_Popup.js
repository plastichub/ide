define([
    'dcl/dcl'
],function (dcl){

    let zIndexStart = 200;

    const Module = dcl(null,{});

    Module.nextZ = function(incr){
        zIndexStart++;
        if(incr){
            zIndexStart+=incr;
        }
        return zIndexStart;
    }

    if(typeof window !=='undefined'){
        window['__nextZ'] = Module.nextZ;
    }

    Module.setStartIndex=function(index){
        zIndexStart = index;
    }

    return Module;
});