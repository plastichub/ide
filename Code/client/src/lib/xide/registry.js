/** module:xide/registry **/
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/window", // win.body
    "xdojo/has"
], function(array, win, has){
    /**
	 * @TODOS:
	 * - add namespaces
	 * - remove window
	 * - augment consumer API
	 * - use std array
	 * - add framework constraint
	 * - move dom api out of here
	 * - define widget.id better
	 * - add search by class
     */
    const _widgetTypeCtr = {};

    const hash = {};
    const registry =  {
		// summary:
		//		Registry of existing widget on page, plus some utility methods.

		// length: Number
		//		Number of registered widgets
		length: 0,
		add: function(widget){
			// summary:
			//		Add a widget to the registry. If a duplicate ID is detected, a error is thrown.
			// widget: dijit/_WidgetBase
			//		Any dijit/_WidgetBase subclass.
			if(this._hash[widget.id]){
                if(has('xblox')) {
                    this.remove(widget.id);
                    this.add(widget);
                }else{
                    throw new Error("Tried to register widget with id==" + widget.id + " but that id is already registered");
                }
			}
			hash[widget.id] = widget;
			this.length++;
		},
		/**
		 * Remove a widget from the registry. Does not destroy the widget; simply
		 * removes the reference.
		 * @param id
         */
		remove: function(id){
			if(hash[id]){
				delete hash[id];
				this.length--;
			}
		},
		/**
		 *
		 * @param id {String|Widget}
		 * @returns {String|Widget}
         */
		byId: function( id){
			// summary:
			//		Find a widget by it's id.
			//		If passed a widget then just returns the widget.
			return typeof id == "string" ? hash[id] : id;	// dijit/_WidgetBase
		},
		byNode: function(/*DOMNode*/ node){
			// summary:
			//		Returns the widget corresponding to the given DOMNode
			return hash[node.getAttribute("widgetId")]; // dijit/_WidgetBase
		},

		/**
		 * Convert registry into a true Array
		 * @example:
		 *	Work with the widget .domNodes in a real Array
		 *	array.map(registry.toArray(), function(w){ return w.domNode; });
		 * @returns {obj[]}
         */
		toArray: function(){
			return _.values(_.mapKeys(hash, function(value, key) { value.id = key; return value; }));
		},
		/**
		 * Generates a unique id for a given widgetType
		 * @param widgetType {string}
		 * @returns {string}
         */
		getUniqueId: function(widgetType){
			let id;
			do{
				id = widgetType + "_" +
					(widgetType in _widgetTypeCtr ?
						++_widgetTypeCtr[widgetType] : _widgetTypeCtr[widgetType] = 0);
			}while(hash[id]);
			return id;
		},
		/**
		 * Search subtree under root returning widgets found.
		 * Doesn't search for nested widgets (ie, widgets inside other widgets).
		 * @param root {HTMLElement} Node to search under.
		 * @param skipNode {HTMLElement} If specified, don't search beneath this node (usually containerNode).
         * @returns {Array}
         */
		findWidgets: function(root, skipNode){
			const outAry = [];
			function getChildrenHelper(root){
				for(let node = root.firstChild; node; node = node.nextSibling){
					if(node.nodeType == 1){
						const widgetId = node.getAttribute("widgetId");
						if(widgetId){
							const widget = hash[widgetId];
							if(widget){	// may be null on page w/multiple dojo's loaded
								outAry.push(widget);
							}
						}else if(node !== skipNode){
							getChildrenHelper(node);
						}
					}
				}
			}
			getChildrenHelper(root);
			return outAry;
		},
		_destroyAll: function(){
			// summary:
			//		Code to destroy all widgets and do other cleanup on page unload

			// Clean up focus manager lingering references to widgets and nodes
			// Destroy all the widgets, top down
			_.each(registry.findWidgets(win.body()),function(widget){
				// Avoid double destroy of widgets like Menu that are attached to <body>
				// even though they are logically children of other widgets.
				if(!widget._destroyed){
					if(widget.destroyRecursive){
						widget.destroyRecursive();
					}else if(widget.destroy){
						widget.destroy();
					}
				}
			});
		},
		getEnclosingWidget: function(/*DOMNode*/ node){
			// summary:
			//		Returns the widget whose DOM tree contains the specified DOMNode, or null if
			//		the node is not contained within the DOM tree of any widget
			while(node){
				const id = node.nodeType == 1 && node.getAttribute("widgetId");
				if(id){
					return hash[id];
				}
				node = node.parentNode;
			}
			return null;
		},

		// In case someone needs to access hash.
		// Actually, this is accessed from WidgetSet back-compatibility code
		_hash: hash
	};
    return registry;
});
