define([
    'dojo/_base/declare',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin'
], function (declare, ReloadMixin, EventedMixin) {
    return declare("xide.client.AtomizeClient", [ReloadMixin, EventedMixin],
        {
            atomize: null,
            DO: null,
            watchers: [],
            ///////////////////////////////////////////////////
            //
            // Base API
            //
            ///////////////////////////////////////////////////
            destroy: function () {
                if (this.atomize) {
                    console.log('destroy atomize client');
                    this.atomize.close();
                }

                //this.onClosed();//call base class
            },
            onInit: function () {
                if (this.delegate && this.delegate.onInit) {
                    this.delegate.onInit(this);
                }
            },

            /***
             * Default options
             */
            _defaultOptions: function () {
                return {
                    host: "0.0.0.0",
                    port: 9999,
                    channel: "atomize"
                };
            },
            onConnectionReady: function (who) {
                if (this.delegate && this.delegate.onConnectionReady) {
                    this.delegate.onConnectionReady(who);
                }
            },
            ///////////////////////////////////////////////////
            //
            // Atomize data manipulation public methods
            //
            ///////////////////////////////////////////////////


            /***
             * Performs a transaction with a "get" operation
             * @param atomize_ns
             * @param callback
             */
            get: function (atomize_ns, callback) {
                const thiz = this;
                this.makeTransaction(function () {
                        return thiz._get("root." + atomize_ns);
                    },
                    callback);
            },

            /***
             * Performs a transaction with a "set" operation
             *
             * @param atomize_ns
             * @param value
             */
            set: function (atomize_ns, value) {
                const thiz = this;
                this.makeTransaction(function () {
                        thiz._set("root." + atomize_ns, value);
                    },
                    function () {
                    });
            },

            /***
             * Deletes a value from an atomize distributed object
             * @param atomize_ns
             */
            unset: function (atomize_ns) {
                let path = atomize_ns.split(".");
                let obj = this.atomize.access(this.atomize, path[0]);
                const end = path[path.length - 1];
                path = path.slice(1, -1);

                for (let i = 0; i < path.length; i++) {
                    obj = this.atomize.access(obj, path[i]);
                }

                this.atomize.erase(obj, end);
            },

            /***
             * Performs a "transaction"
             *
             * @param action
             * @param onResult
             */
            makeTransaction: function (action, onResult) {
                const _fn = this.atomize.atomically;
                if (_fn) {
                    _fn(action, onResult);
                } else {

                    console.warn('makeTransaction failed');
                }
            },

            ///////////////////////////////////////////////////
            //
            // Client Control API
            //
            ///////////////////////////////////////////////////

            /***
             * Initializes the client with options
             *
             * @param options
             */
            init: function (options) {
                this.options = this._defaultOptions();
                const myself = this;


                // Mix the default options and the options provided
                for (const property in this.options) {
                    if (options[property]) {
                        this.options[property] = options[property];
                    }
                }
                this.onInit();
                this.initReload();

            },

            /***
             * Registers a watcher
             *
             * @param inspect       =>  value to watch
             * @param onChangeCB    =>  callback function
             */
            registerWatcher: function (inspect, onChangeCB, begin) {

                const watcher = {
                    inspect: inspect,
                    lastValue: null,
                    callback: onChangeCB
                };
                this.watchers.push(watcher);
                if (begin === true) {
                    this._beginWatch(watcher);
                }

            },
            /***
             * Connects with the atomize server
             *
             * @param callback
             */
            connect: function (callback) {
                const url = "http://" + this.options.host + ":" + this.options.port + "/" + this.options.channel;

                this.atomize = new Atomize("http://" + this.options.host + ":" + this.options.port + "/" + this.options.channel);
                const thiz = this;

                // Starts when the connection begins
                this.atomize.assign(this.atomize, "onAuthenticated",
                    function () {
                        console.log('atomize client : onAuthenticated');
                        thiz.onConnectionReady(thiz);

                        // Calls the callback
                        if (callback) {
                            callback(this);
                        }

                        // Starts the watchers
                        thiz.makeTransaction(
                            function () {
                                // empty "action" for the transaction
                            },
                            function (result) {
                                for (let i = 0; i < thiz.watchers.length; i++) {
                                    thiz._beginWatch(thiz.watchers[i]);
                                }
                            }
                        );
                    }
                );

                // Connects to the Atomize Server
                this.atomize.access(this.atomize, "connect")();

            },
            ///////////////////////////////////////////////////
            //
            // Private methods
            //
            ///////////////////////////////////////////////////
            /***
             * Watches a value
             *
             * @param watcher
             * @private
             */
            _beginWatch: function (watcher) {

                const thiz = this;
                this.get(watcher.inspect, function (result) {
                    watcher.lastValue = result;
                });

                this.makeTransaction(
                    function () {
                        const readed = thiz._get("root." + watcher.inspect);
                        if (watcher.lastValue != readed) {
                            watcher.callback(readed);
                            watcher.lastValue = readed;
                        }
                        thiz.atomize.access(thiz.atomize, "retry")();
                    },
                    function (result) {
                    }
                );
            },

            /***
             * Get a value from an atomize distributed object
             * @param atomize_ns
             * @param value
             */
            _get: function (atomize_ns) {

                let path = atomize_ns.split(".");
                let obj = this.atomize.access(this.atomize, path[0]);

                path = path.slice(1);

                for (let i = 0; i < path.length; i++) {
                    if (obj == undefined) {
                        return obj;
                    }
                    obj = this.atomize.access(obj, path[i]);
                }
                /*
                 for (var n in path) {
                 if (obj == undefined) {
                 return obj;
                 }
                 obj = this.atomize.access(obj,path[n]);
                 }
                 */

                return obj;
            },

            /***
             * Set a value into an atomize distributed object
             * @param atomize_ns
             * @param value
             */
            _set: function (atomize_ns, value) {
                let path = atomize_ns.split(".");

                let obj = this.atomize.access(this.atomize, path[0]);

                const toassign = path[path.length - 1];
                path = path.slice(1, -1);

                for (let i = 0; i < path.length; i++) {
                    obj = this.atomize.access(obj, path[i]);
                }

                if (typeof value == "object") {
                    value = this.atomize.access(this.atomize, "lift")(value);
                }

                this.atomize.assign(obj, toassign, value);
            },
            postscript: function (/*Object?*/ params) {

            }
        });
});