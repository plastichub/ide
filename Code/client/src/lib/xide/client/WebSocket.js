define([
    "dcl/dcl",
    'xide/utils',
    'xide/utils/_LogMixin',
    'xide/client/ClientBase',
    'xdojo/has'
], function (dcl, utils, _logMixin, ClientBase,has) {
    const debug = false;
    return dcl([ClientBase, _logMixin], {
        declaredClass:"xide.client.WebSocket",
        _socket: null,
        debugConnect: true,
        autoReconnect: true,
        reconnect: function () {
            debug && console.log('reconnect!');
        },
        close: function () {
            this.destroy();
        },
        onLostConnection:function(){
            debug && console.log('lost connection');
        },
        onConnected:function(){
            debug && console.log('on connected');
        },
        connect_sock_js: function (_options) {
            this.options = utils.mixin(this.options, _options);
            const host = this.options.host;
            //host = host.replace('http://','wss://');
            const port = this.options.port;
            if (this.options.debug) {
                this.initLogger(this.options.debug);
            }
            if (!host) {
                console.error('something wrong with data!',_options);
                return;
            }
            debug && console.log("Connecting to " + host + ':' + port, "socket_client");
            const protocol = [
                'websocket',
                'xdr-streaming',
                'xhr-streaming',
                'iframe-eventsource',
                'iframe-htmlfile',
                'xdr-polling',
                'xhr-polling',
                'iframe-xhr-polling',
                'jsonp-polling'
            ];

            const options = {
                debug: debug,
                devel: true,
                noCredentials:true,
                nullOrigin:true
            };
            options.transports = protocol;
            const sock = new SockJS(host + ":" + port, null, options);
            const thiz = this;

            sock.onopen = function () {
                thiz.onConnected();
                if (thiz.delegate.onConnected) {
                    thiz.delegate.onConnected();
                }
            };

            sock.onmessage = function (e) {
                if (thiz.delegate.onServerResponse) {
                    thiz.delegate.onServerResponse(e);
                }
            };

            sock.onerror=function(){
                console.error('error');
            }
            sock.onclose = function (e) {
                if (thiz.autoReconnect) {
                    debug &&  console.log('closed ' + host + ' try re-connect');
                    if (thiz.delegate.onLostConnection) {
                        thiz.delegate.onLostConnection(e);
                    }
                    thiz.reconnect();
                }else{
                    debug && console.log('closed ' + host);
                }
            };
            this._socket = sock;
        },
        connect_socket_io: function (_options) {
            this.options = utils.mixin(this.options, _options);
            const host = this.options.host;
            //host = host.replace('http://','wss://');
            const port = this.options.port;
            if (this.options.debug) {
                this.initLogger(this.options.debug);
            }
            if (!host) {
                console.error('something wrong with data!',_options);
                return;
            }
            debug && console.log("Connecting to " + host + ':' + port, "socket_client");
            const protocol = [
                'websocket',
                'xdr-streaming',
                'xhr-streaming',
                'iframe-eventsource',
                'iframe-htmlfile',
                'xdr-polling',
                'xhr-polling',
                'iframe-xhr-polling',
                'jsonp-polling'
            ];

            const options = {
                debug: debug,
                devel: true,
                noCredentials:true,
                nullOrigin:true
            };
            options.transports = protocol;
            const sock = new SockJS(host + ":" + port, null, options);
            const thiz = this;

            sock.onopen = function () {
                thiz.onConnected();
                if (thiz.delegate.onConnected) {
                    thiz.delegate.onConnected();
                }
            };

            sock.onmessage = function (e) {
                if (thiz.delegate.onServerResponse) {
                    thiz.delegate.onServerResponse(e);
                }
            };

            sock.onerror=function(){
                console.error('error');
            }
            sock.onclose = function (e) {
                if (thiz.autoReconnect) {
                    debug &&  console.log('closed ' + host + ' try re-connect');
                    if (thiz.delegate.onLostConnection) {
                        thiz.delegate.onLostConnection(e);
                    }
                    thiz.reconnect();
                }else{
                    debug && console.log('closed ' + host);
                }
            };
            this._socket = sock;
        },
        connect: function (_options) {
            return this.connect_sock_js(_options);
            /*
            this.options = utils.mixin(this.options, _options);
            //debugger;
            var host = this.options.host;
            //host = host.replace('http://','wss://');
            var port = this.options.port;
            if (this.options.debug) {
                this.initLogger(this.options.debug);
            }
            if (!host) {
                console.error('something wrong with data!',_options);
                return;
            }
            debug && console.log("Connecting to " + host + ':' + port, "socket_client");
            var protocol = [
                'websocket',
                'xdr-streaming',
                'xhr-streaming',
                'iframe-eventsource',
                'iframe-htmlfile',
                'xdr-polling',
                'xhr-polling',
                'iframe-xhr-polling',
                'jsonp-polling'
            ];


            var sock = io(host + ":" + port);

            sock.on('connect', function(){
                thiz.onConnected();
                if (thiz.delegate.onConnected) {
                    thiz.delegate.onConnected();
                }
            });
            sock.on('event', function(data){

            });
            sock.on('disconnect', function(){});


            var options = {
                debug: debug,
                devel: true,
                noCredentials:true,
                nullOrigin:true
            };
            options.transports = protocol;

            //var sock = new SockJS(host + ":" + port, null, options);
            var thiz = this;

            sock.onopen = function () {

            };

            sock.onmessage = function (e) {
                if (thiz.delegate.onServerResponse) {
                    thiz.delegate.onServerResponse(e);
                }
            };

            sock.onerror=function(){
                console.error('error');
            }
            sock.onclose = function (e) {
                if (thiz.autoReconnect) {
                    debug &&  console.log('closed ' + host + ' try re-connect');
                    if (thiz.delegate.onLostConnection) {
                        thiz.delegate.onLostConnection(e);
                    }
                    thiz.reconnect();
                }else{
                    debug && console.log('closed ' + host);
                }
            };
            this._socket = sock;
            */
        },
        emit: function (signal, dataIn, tag) {
            dataIn.tag = tag || 'notag';
            const data = JSON.stringify(dataIn);
            const res = this._socket.send(data);
            debug && console.log('send ',res);
        },

        onSignal: function (signal, callback) {
            this._socket.on('data', callback);
        }
    });
});