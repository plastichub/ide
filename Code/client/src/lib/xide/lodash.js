/** @module xide/lodash **/
define([],function(app){
    /**
     * temp. wanna be shim for lodash til dojo-2/loader lands here
     */
    if(typeof _ !=="undefined"){
        return _;
    }else if(app && app._){
        return app._;
    }else{
        console.error('error loading lodash',global['_']);
    }
});
