/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/tests/TestUtils",
    'xide/_base/_Widget',
    'xide/widgets/_Ribbon',
    'xide/widgets/_MenuMixin2',
    "xide/mixins/ActionMixin",
    'xide/widgets/TemplatedWidgetBase',
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    "module",
    "dojo/dom-construct"
], function (dcl,declare,types,utils,
             Grid, TestUtils,_Widget,_Ribbon,_MenuMixin,ActionMixin,
             TemplatedWidgetBase,
             ActionContext,ActionStore,
             module,domConstruct) {
    //console.clear();
    console.log('--do-tests');

    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;



    const _RibbonClass = dcl([TemplatedWidgetBase, _Ribbon, ActionContext.dcl, ActionMixin.dcl, _MenuMixin], {
        declaredClass:'xide.widgets.Ribbon',
        /**
         *
         * @param highlight
         * @private
         */
        _onMouseOver:function(highlight){
            const emitter = this.getCurrentEmitter();
            if(emitter && emitter.highlight){

                if(emitter.isActive && !highlight && emitter.isActive()==true){
                    return;
                }
                emitter.highlight(highlight);
            }
        },
        _tabs: null,
        ribbonNode: null,
        templateString: '<div class="dijitToolbar ui-state-default" tabindex="-1">' +
        '<div attachTo="ribbonNode"/>' +
        '</div>',
        visibility: types.ACTION_VISIBILITY.RIBBON,
        ribbon: null,
        config: null,
        flat: false,
        cloneActions: false,
        isOpen:true,
        /**
         * The separator class for separating actions
         */
        /**
         *
         * @param store
         */
        setActionStore: function (store) {

            if(this.store && store &&  this.store==store){
                console.error('same store! ');
                return;
            }


            const thiz = this;

            const bBox = this.getBoundingBox();

            bBox.empty();

            bBox.off();


            utils.destroy(this.ribbonNode);

            if(this.store) {
                this.clearActions();
            }



            this.ribbonNode = domConstruct.create('div');

            this.domNode.appendChild(this.ribbonNode);
            this.boundingBox = $(this.ribbonNode);

            delete this.config;

            this.store = store;

            if(!store){
                return;
            }

            this._tabs = {};

            this.config = this.toConfig(store);

            this.ready = false;

            this._isRefreshing = true;

            this._init();

            this._isRefreshing = false;


        },
        _renderAction: function (action, where, widgetClass, showLabel) {

            const thiz = this;
            //if (action && action.show !== false) {

            const actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};

            //pre-rendered widget
            if (actionVisibility._widget) {

                utils.addChild(where, actionVisibility._widget);
                $(actionVisibility._widget.domNode).addClass(utils.replaceAll('/', '-', action.command));

                actionVisibility.widget = actionVisibility._widget;//for clean up

                thiz._onDidCreateWidget(action, actionVisibility, actionVisibility._widget);

                return;
            }

            //render only a widget for this action if not already done and the actions isn't a widget itself=dirty yet
            if (!actionVisibility.widget && !action.domNode) {

                if (!action.label) {//huh, really? @TODO: get rid of label needs in action:render
                } else {
                    actionVisibility.widget = thiz._addMenuItem(action, where, actionVisibility.widgetClass || widgetClass || thiz.widgetClass, showLabel);
                    actionVisibility.widget.visibility = thiz.visibility;
                }
            } else {
                console.log('have already rendered action', action);
            }

            return actionVisibility.widget;


        },
        /**
         *
         * @param action
         * @param where
         * @param widgetClass
         * @param showLabel
         * @returns {*}
         */
        renderAction: function (action, where, widgetClass, showLabel) {
            //console.log('render action : '+action.command);
            if(action.command.includes('File/Open In')) {
                //console.log('render open in! ');
            }

            /*

             if(action.command.indexOf('File/New/File')!=-1) {
             var items = action.getChildren ? action.getChildren() : action.items;
             }

             if(action.command.indexOf('File/Edit')!=-1) {
             var items = action.getChildren();
             }
             */
            /**
             * Collect variables
             */

            const thiz = this;

            const _items = action.getChildren ? action.getChildren() : action.items;
            const _hasItems = _items && _items.length;
            const isContainer = _hasItems;
            const _expand = thiz.getVisibilityField(action, 'expand') == true;
            const _forceSub = action.forceSubs;
            const widget = thiz.getVisibilityField(action, 'widget');
            const parentAction = action.getParent ? action.getParent() : null;
            const parentWidget = parentAction ? thiz.getVisibilityField(parentAction, 'widget') : null;
            const actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};

            /**
             * Cleanup variables, sanitize
             */

            //case when visibility is not an object, upgrade visibility to an object
            /*
             if (!_.isObject(actionVisibility) && action.setVisibility) {

             actionVisibility = {};
             action.setVisibility(this.visibility, actionVisibility);
             }
             */

            //further variables
            const label = actionVisibility.label || action.label;

            const widgetArgs = {
                iconClass: action.icon,
                label: label,
                item: action
            };

            if (actionVisibility.widgetArgs) {
                utils.mixin(widgetArgs, actionVisibility.widgetArgs);
            }


            /**
             * Determine widget class to use, resolve
             */
            widgetClass =
                //override
                actionVisibility.widgetClass ||
                    //argument
                widgetClass ||
                    //visibility's default
                thiz.widgetClass;


            // case one: isContainer = true
            if (!widget) {

                if (isContainer) {

                    if (_expand) {
                        return null;
                    }

                    //console.log('   render container ' + action.command,_items);

                    if (!parentWidget) {

                        widgetArgs.focus = function(){};
                        widgetArgs.blur = function(){};
                        widgetArgs._onFocus = function(){};



                        const menu = utils.addWidget(DropDownButton, widgetArgs, this, where, true, null, [Reference]);

                        var pSubMenu = utils.addWidget(Menu, {
                            item: action
                        }, this, null, false);

                        menu.dropDown = pSubMenu;
                        actionVisibility.widget = pSubMenu;

                        thiz._publishActionWidget(menu,action,where);

                        actionVisibility.dropDown=menu;


                        return menu;

                    } else {

                        //parent widget there

                        var pSubMenu = new Menu({parentMenu: parentWidget});

                        const popup = new dijit.PopupMenuItem({
                            label: label,
                            popup: pSubMenu,
                            iconClass: action.icon
                        });

                        parentWidget.addChild(popup);

                        actionVisibility.widget = pSubMenu;
                        return pSubMenu;

                    }
                }
                if (parentWidget) {
                    where = parentWidget;
                    widgetClass = MenuItem;
                }
            } else {
                console.log('widget already rendered!');
            }


            /*
             console.log('render action ' + action.command, {
             widgetClass: widgetClass ? widgetClass.prototype.declaredClass : 'no class',
             hasItems: _hasItems,
             expand: _expand,
             widget: widget,
             parentAction: parentAction,
             parentWidget: parentWidget,
             where: where
             });
             */


            if (!actionVisibility.widget && !action.domNode) {

                if (!action.label) {//huh, really? @TODO: get rid of label needs in action:render
                } else {
                    //actionVisibility.widget = thiz._addMenuItem(action, where, widgetClass, showLabel);
                    //actionVisibility.widget.visibility = thiz.visibility;
                }
            } else {
                console.log('have already rendered action', action);
            }


            /*var _lastWidget = thiz._renderAction(action, where, widgetClass, showLabel);*/


            return actionVisibility.widget;
        },

        /**
         * Renders a set of actions into is appropriate widgets. This utilizes this._renderAction and
         * puts the action's widget into its visibility store for global access.
         *
         * @param actions {xide/action/Action[]}
         * @param where {dijit/_Widget}
         * @param widgetClass {Module}
         * @private
         */
        _renderActions: function (actions, where, widgetClass, showLabel, separatorClass, force) {
            //track the last action group for adding a separator
            let _lastGroup = null;

            const thiz = this;
            const widgets = [];
            let _lastWidget;

            if (!actions) {
                console.error('strange, have no actions!');
                return;
            }

            for (let i = 0; i < actions.length; i++) {
                let action = actions[i];
                const sAction = action._store ? action._store.getSync(action.command) : null;

                if (sAction && sAction != action) {
                    console.log('weird!');
                }
                if (sAction) {
                    action = sAction;
                }

                if (!action) {
                    console.error('invalid action!');
                    continue;
                }


                //pick command group
                if (i == 0 && !_lastGroup && action.group) {
                    _lastGroup = action.group;
                }

                //skip if action[visibility].show is false
                if (thiz.getVisibilityField(action, 'show') === false) {
                    continue;
                }

                const _items = action.getChildren ? action.getChildren() : action.items;
                const _hasItems = _items && _items.length;
                const _expand = thiz.getVisibilityField(action, 'expand');
                const _forceSub = action.forceSubs;


                //action has its own set of actions

                /*
                 if ((_hasItems || _forceSub ) && !thiz.getVisibilityField(action, 'widget')) {
                 if (_expand == true) {

                 } else {
                 thiz.renderSubActions(action, where);
                 }
                 continue;
                 }
                 */


                force = force != null ? force : this.forceRenderSubActions;


                /*
                 //check action for a parent widget
                 var parent = action.getParent ? action.getParent() : null;
                 if (parent) {

                 var parentWidget = thiz.getVisibilityField(parent, 'widget');

                 if (parentWidget && force !== true && _expand !== true) {
                 console.log('skip action, has parent and force not TRUE');
                 continue;
                 }
                 }
                 */


                //console.log('render action: '+action.command,[actions,action]);

                /*if (action && action.show !== false) {*/


                //  Insert separator as soon there is a 'group' and the group actually changed
                /*
                 if (!_.isEmpty(action.group) && action.group !== _lastGroup) {

                 var children = where.getChildren();

                 if (children.length > 0 && children[children.length - 1].prototype != thiz.separatorClass.prototype) {

                 var separator = utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true);

                 var widgetArgs = thiz.getVisibilityField(action, 'widgetArgs');
                 if (widgetArgs && widgetArgs.style && widgetArgs.style.indexOf('float:right') != -1) {
                 domStyle.set(separator.domNode, {
                 float: 'right'
                 });
                 }

                 }
                 _lastGroup = action.group;
                 }
                 */


                _lastWidget = thiz.renderAction(action, where, widgetClass, showLabel);


                /*
                 _lastWidget = thiz._renderAction(action, where, widgetClass, showLabel);
                 if (_lastWidget) {
                 widgets.push(_lastWidget);
                 }
                 */

                /*}*/
            }

            return widgets;
        },
        /**
         *
         * @param node {HTMLElement}
         * @param tab {string}
         * @param group {string}
         * @private
         */
        _renderTabGroup: function (node, tab, group) {


            //tab group's - actions
            const actions = this.store.query({
                group: group,
                tab: tab
            });

            const _Toolbar = utils.addWidget(TemplatedWidgetBase, {
                templateString:"<div></div>",
                "class": ""
            }, this, node, true);

            this._renderActions(actions, _Toolbar);
        },
        _renderTabGroups: function () {

            const thiz = this;
            $('.tabGroup').each(function (id, element) {
                const el = $(element);
                const group = el.attr('group');
                const tab = el.attr('tab');

                if (thiz._tabs[tab]) {
                    thiz._tabs[tab]['node'] = element;
                }

                thiz._renderTabGroup(element, tab, group);
            });
        },
        /*
         * Initialize the ribbon
         * @method init
         * @public
         * @return void
         **/
        _init: function () {
            if (this.ready) {
                return;
            }

            const bBox = this.boundingBox;
            const classes = this.CSS_CLASSES;
            const base = classes.base;
            let cssClasses = [classes.ui, base];
            const ng = this.ng;



            /*
             * Append the "flat" style class if config.flat is true (since version 4.1.0)
             **/
            if (this.flat) {
                cssClasses.push(classes.flat);
            }

            /*
             * Set AngularJS directive(s) to the boundingBox if config.ng object is defined (since version 4.3.0)
             **/
            if (ng) {
                $.each(ng, function (key, value) {
                    bBox.attr(key.includes("data-") ? key : "ng-" + key, value);
                });
            }

            cssClasses = cssClasses.concat(this.cssClasses || []);
            //var r = this.collapse();


            const wasClosed = this.isOpen == false;

            //console.error('is opem : ' + bBox.is("[open]") + ' this '  + this.isOpen);

            //console.log('was closed ' + wasClosed);


            bBox.addClass(cssClasses.join(" "));
            if(!wasClosed){

            }
            bBox.html(this._template("base"))
                .attr("open", "")
                .css({
                    maxWidth: this.width,
                    minWidth: this.minwidth,
                    visibility: "hidden"
                });

            //console.log('is ope 2 : ' + bBox.is("[open]") + ' this '  + this.isOpen);


            this.renderTabs();

            this._bind();

            this._renderTabGroups();

            if(wasClosed){
                this.collapse();
                bBox.removeAttr(this.ATTRS.open);
                bBox.addClass('ribbonClose');
            }else{
                bBox.addClass('ribbonOpen');
            }

            bBox.addClass('widget');
        },
        sortGroups: function (groups, groupMap) {
            groups = groups.sort(function (a, b) {
                if (a.label && b.label && groupMap[a.label] != null && groupMap[b.label] != null) {
                    const orderA = groupMap[a.label];
                    const orderB = groupMap[b.label];
                    return orderB - orderA;
                }
                return 100;
            });
            return groups;
        },
        toConfig: function (store) {


            const thiz = this;

            const toCommand = function (action) {
                return {
                    name: action.command,
                    label: action.label,
                    //icon: action.icon,
                    icon: "cut.png",
                    props: {
                        action: action
                    }
                };
            };
            /**
             *
             * @param label - tab
             * @param actions - tab - actions
             */
            const toTab = function (label, actions) {

                const tab = {
                    label: label,
                    name: '',
                    ribbons: [],
                    hint: label,
                    rRype: 'tab'
                };

                //find groups
                const groups = _.groupBy(actions, function (action) {
                    return action.group;
                });

                //build ribbons per group
                _.each(groups, function (items, groupLabel) {

                    let tabWidth = '160px';
                    if(store.tabSettings && store.tabSettings[groupLabel]){

                        const settings = store.tabSettings[groupLabel];
                        if(settings.width){
                            tabWidth = settings.width;
                        }
                    }


                    const ribbon = {
                        label: groupLabel,
                        width: tabWidth,
                        /*minWidth: "160px",*/
                        rRype: 'tabRibbon (group)',
                        props: {
                            tab: label,
                            group: groupLabel,
                            items: items
                        },
                        tools: [
                            //custom tool: tab-group
                            {
                                type: "tabGroup",
                                size: "small",
                                /*items: "break",*/
                                group: groupLabel,
                                tab: label,
                                props: {
                                    items: items
                                },
                                commands: []
                            }
                        ]
                    };

                    _.each(items, function (item) {
                        ribbon.tools[0].commands.push(toCommand(item));
                    });

                    tab.ribbons.push(ribbon);

                });
                return tab;

            };


            //1. get tabs

            //none empty tab field in action
            const allActions = store.getAll();
            const tabbedActions = allActions.filter(function (action) {
                return action.tab != null;
            });
            const groupedTabs = _.groupBy(tabbedActions, function (action) {
                return action.tab;
            });

            let tabs = [];
            _.each(groupedTabs, function (items, label) {
                const tab = toTab(label, items);
                thiz._tabs[label] = tab;
                tabs.push(tab);
            });

            if (store.tabOrder) {
                tabs = this.sortGroups(tabs, store.tabOrder);
            }



            if (store.groupOrder) {

                _.each(tabs, function (tab) {
                    tab.ribbons = thiz.sortGroups(tab.ribbons, store.groupOrder);
                });

            }

            return {
                tabs: tabs
            };


        },
        startup: function () {

            this.inherited(arguments);


            this._tabs = {};
            const ctx = sctx;
            const app = ctx.getApplication();
            /*
            var deviceTreeview = app.deviceTreeView;
            this.store = deviceTreeview.getActionStore();*/

            if (!this.config && this.store) {
                this.config = this.toConfig(this.store);
            }




            this.store = this.store || new ActionStore({});

            const config = this.config || {
                    tabs: [{
                        label: "Home",
                        hint: "Go home",
                        name: "tab-home",
                        ribbons: [
                            {
                                //group:
                                label: "Clipboard",
                                width: "10%",
                                minWidth: "160px",
                                tools: [
                                    {
                                        type: "buttons",
                                        size: "small",
                                        items: "break",
                                        group: 'bla',
                                        commands: [
                                            {
                                                name: "cut",
                                                hint: "Cut (Ctrl+X)",
                                                label: "Cut",
                                                icon: "cut.png",
                                                props: {
                                                    a: 1,
                                                    b: "cde"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }]
                };

            this.config = config;
            this.boundingBox = $(this.ribbonNode);
            this._init();
            this.boundingBox.addClass('widget');
        }
    });



    /*
     * playground
     */
    const ctx = window.sctx;

    const ACTION = types.ACTION;
    let root;



    const _actions = [
        ACTION.RENAME
    ];

    if (ctx) {
        const parent = TestUtils.createTab(null,null,module.id);
        utils.addWidget(_RibbonClass,null,null,parent,true);
        return declare('a',null,{});

    }

    return Grid;
});