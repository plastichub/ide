define([
    "dcl/dcl",
    "dojo/_base/connect",
    "dojo/dom-attr",
    "dojo/dom-construct",
    'xide/widgets/WidgetBase'
    //'dijit/form/RadioButton'
], function (dcl,connect, domAttribute, domConstruct, WidgetBase) {

    return dcl([WidgetBase], {
        cls:"xide.form.Radio",
        backgroundClass: null,
        iconClass: null,
        itemClass: null,
        delegate: null,
        title: null,
        fieldData: null,
        fieldWidgetRoot: null,
        fieldWidget: null,
        isSubmit: false,
        opener: null,
        scrollview: null,
        listview: null,
        heading: null,
        done: null,
        roundrectlist: null,
        didItems: false,
        options: null,
        labelValue: null,
        tbody: null,
        optionWidgets: null,
        value: null,
        templateString: "<div class='fieldrow RadioWidget' style='padding: 8px'>" +
        "<span class='field-label'>${!title}</span>" +
        "<table border='0' cellpadding='5px' width='100%'>" +
        "<tbody data-dojo-attach-point='tbody' align='left'>" +
        "</tbody>" +
        "</table><br/>" +
        "</div>" +
        "</div>",
        onRadioValueChanged: function () {
            for (let i = 0; i < this.optionWidgets.length; i++) {
                if (this.optionWidgets[i].get('checked')) {
                    this.userData['value'] = this.optionWidgets[i].value;
                }
            }
            this.setValue(this.userData['value']);
        },
        startup: function () {
            if(this._started){
                return;
            }
            this.inherited(arguments);


            this.optionWidgets = [];

            const thiz = this;
            const options = this.userData.options;
            const data = this.userData;

            for (let i = 0; i < options.length; i++) {

                /***
                 * build a table row
                 */
                const _tr = domConstruct.create('tr', {
                    'class': 'RadioWidgetRow'
                });

                this.tbody.appendChild(_tr);

                const _td = dojo.doc.createElement('td', {});
                _tr.appendChild(_td);

                if (data.optionRadioColumnWidth) {
                    domAttribute.set(_td, "width", data.optionRadioColumnWidth)
                } else {
                    domAttribute.set(_td, "width", '20px');
                }


                const option = new RadioButton({
                    name: data.name,
                    value: options[i].value,
                    checked: options[i].value == data.value,
                    /*className:"mblRadioButton " + this.fieldData.optionRadioClass || "",*/
                    style: data.optionRadioStyle ? data.optionRadioStyle : '',
                    "aria-checked": false
                }, dojo.doc.createElement('input'), {});

                option["type"] = "radio";

                domAttribute.set(option.domNode, "type", "radio");

                _td.appendChild(option.domNode);
                this.optionWidgets.push(option);

                connect.connect(option, "onChange", null, function (item) {
                    thiz.onRadioValueChanged(item);
                });

                const _tdLabel = dojo.doc.createElement('td');
                _tr.appendChild(_tdLabel);

                const span = dojo.create("span", {
                    className: data.optionLabelClass || "RadioWidgetLabel",
                    innerHTML: options[i].label,
                    style: data.optionLabelStyle || ""
                });
                _tdLabel.appendChild(span);

                option.startup();
            }
        }
    });
});
