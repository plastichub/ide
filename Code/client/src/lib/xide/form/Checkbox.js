/** @module xgrid/Base **/
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/widgets/WidgetBase'
], function (dcl, types, utils, _Widget) {

    return dcl(_Widget, {
        declaredClass: "xide/form/Checkbox",
        title: "Checkbox Label",
        checked: false,
        checkboxClass: "checkbox-success",
        checkbox: null,
        //userData:{},
        templateString: '<div>' +
            '<div class="checkbox ${!checkboxClass}">' +
            '<input attachTo="checkbox" id="${!id}_checkbox" type="checkbox">' +
            '<label for="${!id}_checkbox">${!title}</label>' +
            '</div>' +
            '</div>',
        get: function (what) {
            if (what == 'value') {
                return this.checked;
            }
        },
        set: function (what, value) {
            if (what == 'value') {
                this.checked = value;
                if (value) {
                    this.$checkbox.prop("checked", true)
                } else {
                    this.$checkbox.removeAttr("checked");
                }
            };
        },
        startup: function () {
            if (this.userData && 'value' in this.userData) {
                this.checked = this.userData.value;
            }
            this.$checkbox.attr("checked", this.checked);
            this.$checkbox.on('change', (val) => {
                this.checked = this.$checkbox[0].checked;
                this.userData && this.setValue(this.checked);
                this.emit('change', this.checked);
            });
        }
    });

});