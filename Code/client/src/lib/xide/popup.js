/** @module xide/popup **/
define([
    "dojo/dom-geometry", // domGeometry.isBodyLtr
    "dojo/dom-style", // domStyle.set
    "dojo/_base/lang", // lang.hitch
    "dijit/place",
    "xide/$",
    "dcl/dcl"
], function (domGeometry, domStyle,lang,place,$,dcl) {
    /**
     * jQuery port of dijit/popup and deals with native HTML elements only.
     * @class module:xide/popup
     */
    let instance = null;
    const Module = dcl(null, {
        // _stack: dijit/_WidgetBase[]
        //		Stack of currently popped up widgets.
        //		(someone opened _stack[0], and then it opened _stack[1], etc.)
        _stack: [],
        // _beginZIndex: Number
        //		Z-index of the first popup.   (If first popup opens other
        //		popups they get a higher z-index.)
        _beginZIndex: 1000,
        _idGen: 1,
        _repositionAll: function(){
            // summary:
            //		If screen has been scrolled, reposition all the popups in the stack.
            //		Then set timer to check again later.

            if(this._firstAroundNode){
                // guard for when clearTimeout() on IE doesn't work
                const oldPos = this._firstAroundPosition;

                const newPos = domGeometry.position(this._firstAroundNode, true);
                const dx = newPos.x - oldPos.x;
                const dy = newPos.y - oldPos.y;

                if(dx || dy){
                    this._firstAroundPosition = newPos;
                    for(let i = 0; i < this._stack.length; i++){
                        const style = this._stack[i].wrapper.style;
                        style.top = (parseFloat(style.top) + dy) + "px";
                        if(style.right == "auto"){
                            style.left = (parseFloat(style.left) + dx) + "px";
                        }else{
                            style.right = (parseFloat(style.right) - dx) + "px";
                        }
                    }
                }

                this._aroundMoveListener = setTimeout(lang.hitch(this, "_repositionAll"), dx || dy ? 10 : 50);
            }
        },
        /**
         * Initialization for widgets that will be used as popups.
         * Puts widget inside a wrapper DIV (if not already in one),and returns pointer to that wrapper DIV.
         * @param node
         * @returns {HTMLElement}
         * @private
         */
        _createWrapper: function(node,args){
            let wrapper = $(node).data('_popupWrapper');
            const owner = $(node).data('owner') || (_.isObject(args) && args.owner ? args.owner : null);
            if(!wrapper){
                const $wrapper = $("<div class='xPopup' style='display:none' role='region'></div>" );
                $('body').append(wrapper);
                $wrapper.append($(node));
                wrapper = $wrapper[0];
                const s = node.style;
                s.display = "";
                s.visibility = "";
                s.position = "";
                s.top = "0px";
                if(owner){
                    if(owner._on){
                        owner._on('destroy',function(e){
                            $wrapper.remove();
                        });
                    }
                }
                $(node).data('_popupWrapper',wrapper);
            }
            return wrapper;
        },
        /**
         * Moves the popup widget off-screen.
         * Do not use this method to hide popups when not in use, because
         * that will create an accessibility issue: the offscreen popup is
         * still in the tabbing order.
         * @param node {HTMLElement}
         * @returns {*}
         */
        moveOffScreen: function(node,args){
            // Create wrapper if not already there
            const wrapper = this._createWrapper(node,args);

            // Besides setting visibility:hidden, move it out of the viewport, see #5776, #10111, #13604
            const ltr = true;

            const style = {
                visibility: "hidden",
                top: "-9999px",
                display: ""
            };

            style[ltr ? "left" : "right"] = "-9999px";
            style[ltr ? "right" : "left"] = "auto";
            $(wrapper).css(style);
            return wrapper;
        },
        /**
         * Hide this popup widget (until it is ready to be shown).
         * Initialization for widgets that will be used as popups.
         * Also puts widget inside a wrapper DIV (if not already in one)
         * If popup widget needs to layout it should
         * do so when it is made visible, and popup._onShow() is called.
         * @param widget {HTMLElement}
         */
        hide: function(widget,args){
            // Create wrapper if not already there
            const wrapper = this._createWrapper(widget,args);
            $(wrapper).css({
                display: "none",
                height: "auto",			// Open() may have limited the height to fit in the viewport,
                overflowY: "visible",	// and set overflowY to "auto".
                border: ""			// Open() may have moved border from popup to wrapper.
            });
            // Open() may have moved border from popup to wrapper.  Move it back.
            const node = widget;
            if("_originalStyle" in node){
                node.style.cssText = node._originalStyle;
            }
        },
        getTopPopup: function(){
            // summary:
            //		Compute the closest ancestor popup that's *not* a child of another popup.
            //		Ex: For a TooltipDialog with a button that spawns a tree of menus, find the popup of the button.
            const stack = this._stack;
            for(var pi = stack.length - 1; pi > 0 && stack[pi].parent === stack[pi - 1].widget; pi--){
                /* do nothing, just trying to get right value for pi */
            }
            return stack[pi];
        },
        /**
         * Popup the widget at the specified position
         * example:
         *   opening at the mouse position
         *      popup.open({popup: menuWidget, x: evt.pageX, y: evt.pageY});
         *
         * example:
         *  opening the widget as a dropdown
         *      popup.open({parent: this, popup: menuWidget, around: this.domNode, onClose: function(){...}});
         *
         *  Note that whatever widget called dijit/popup.open() should also listen to its own _onBlur callback
         *  (fired from _base/focus.js) to know that focus has moved somewhere else and thus the popup should be closed.
         * @param args
         * @returns {*}
         */
        open: function(args){
            // summary:
            //		Popup the widget at the specified position
            //

            const last = null;



            const isLTR = true;
            const self = this;
            const stack = this._stack;
            const widget = args.popup;
            const node = args.popup;
            const orient = args.orient || ["below", "below-alt", "above", "above-alt"];
            const ltr = args.parent ? args.parent.isLeftToRight() : isLTR;
            const around = args.around;
            const owner = $(node).data('owner');
            const extraClass = args.extraClass || "";
            const id = (args.around && args.around.id) ? (args.around.id + "_dropdown") : ("popup_" + this._idGen++);

            // If we are opening a new popup that isn't a child of a currently opened popup, then
            // close currently opened popup(s).   This should happen automatically when the old popups
            // gets the _onBlur() event, except that the _onBlur() event isn't reliable on IE, see [22198].
            while(stack.length && (!args.parent || $.contains(args.parent.domNode,stack[stack.length - 1].widget.domNode))){
                this.close(stack[stack.length - 1].widget);
            }

            // Get pointer to popup wrapper, and create wrapper if it doesn't exist.  Remove display:none (but keep
            // off screen) so we can do sizing calculations.
            const wrapper = this.moveOffScreen(widget,args);
            const $wrapper = $(wrapper);

            // Limit height to space available in viewport either above or below aroundNode (whichever side has more
            // room), adding scrollbar if necessary. Can't add scrollbar to widget because it may be a <table> (ex:
            // dijit/Menu), so add to wrapper, and then move popup's border to wrapper so scroll bar inside border.
            let maxHeight;

            const popupSize = domGeometry.position(node);
            if("maxHeight" in args && args.maxHeight != -1){
                maxHeight = args.maxHeight || Infinity;	// map 0 --> infinity for back-compat of _HasDropDown.maxHeight
            }else{
                const viewport = {
                    t:0,
                    l:0,
                    h:$(window).height(),
                    w:$(window).width()
                };
                const aroundPos = around ? domGeometry.position(around, false) : {y: args.y - (args.padding||0), h: (args.padding||0) * 2};
                maxHeight = Math.floor(Math.max(aroundPos.y, viewport.h - (aroundPos.y + aroundPos.h)));
            }
            //maxHeight = 300;
            if(popupSize.h > maxHeight){
                // Get style of popup's border.  Unfortunately domStyle.get(node, "border") doesn't work on FF or IE,
                // and domStyle.get(node, "borderColor") etc. doesn't work on FF, so need to use fully qualified names.
                const cs = domStyle.getComputedStyle(node);

                const borderStyle = cs.borderLeftWidth + " " + cs.borderLeftStyle + " " + cs.borderLeftColor;

                $wrapper.css({
                    'overflow-y': "scroll",
                    height: maxHeight + "px",
                    border: borderStyle	// so scrollbar is inside border
                });
                node._originalStyle = node.style.cssText;
                node.style.border = "none";
            }
            $wrapper.attr({
                id: id,
                "class": "xPopup " + (widget.baseClass || widget["class"] || "").split(" ")[0] + "Popup" + ' ' + (args.css ? args.css : ""),
                dijitPopupParent: args.parent ? args.parent.id : ""
            });
            $wrapper.css('z-index',this._beginZIndex + stack.length);
            if(stack.length === 0 && around){
                // First element on stack. Save position of aroundNode and setup listener for changes to that position.
                this._firstAroundNode = around;
                //this._firstAroundPosition = domGeometry.position(around, true);
                const offset = $(around).offset();
                this._firstAroundPosition = {
                    w:$(around).width(),
                    h:$(around).height(),
                    x:offset.left,
                    y:offset.top
                };
                //this._aroundMoveListener = setTimeout(lang.hitch(this, "_repositionAll"), 50);
                this._aroundMoveListener = setTimeout(function(){
                    self._repositionAll();
                }, 50);
            }

            // position the wrapper node and make it visible
            const layoutFunc = null; //widget.orient ? lang.hitch(widget, "orient") : null;
            const best = around ?
                place.around(wrapper, around, orient, ltr, layoutFunc) :
                place.at(wrapper, args, orient == 'R' ? ['TR', 'BR', 'TL', 'BL'] : ['TL', 'BL', 'TR', 'BR'], args.padding,
                    layoutFunc);

            wrapper.style.visibility = "visible";
            node.style.visibility = "visible";	// counteract effects from _HasDropDown


            const handlers = [];
            $(wrapper).on('keydown',function(evt){
                if(evt.keyCode == 27 && args.onCancel){//esape
                    evt.stopPropagation();
                    evt.preventDefault();
                    args.onCancel();
                }else if(evt.keyCode == 9){//tab
                    evt.stopPropagation();
                    evt.preventDefault();
                    const topPopup = self.getTopPopup();
                    if(topPopup && topPopup.onCancel){
                        topPopup.onCancel();
                    }
                }
            });
            // watch for cancel/execute events on the popup and notify the caller
            // (for a menu, "execute" means clicking an item)
            if(widget.onCancel && args.onCancel){
                handlers.push(widget.on("cancel", args.onCancel));
            }

            $(node).css('display','block');
            /*
             handlers.push(widget.on(widget.onExecute ? "execute" : "change", lang.hitch(this, function(){
             var topPopup = this.getTopPopup();
             if(topPopup && topPopup.onExecute){
             topPopup.onExecute();
             }
             })));
             */
            stack.push({
                widget: widget,
                wrapper: wrapper,
                parent: args.parent,
                onExecute: args.onExecute,
                onCancel: args.onCancel,
                onClose: args.onClose,
                handlers: handlers
            });
            if(widget.onOpen){
                // TODO: in 2.0 standardize onShow() (used by StackContainer) and onOpen() (used here)
                widget.onOpen(best);
            }
            $(wrapper).addClass(extraClass);
            return best;
        },
        close: function(/*Widget?*/ popup){
            // summary:
            //		Close specified popup and any popups that it parented.
            //		If no popup is specified, closes all popups.
            const stack = this._stack;
            // Basically work backwards from the top of the stack closing popups
            // until we hit the specified popup, but IIRC there was some issue where closing
            // a popup would cause others to close too.  Thus if we are trying to close B in [A,B,C]
            // closing C might close B indirectly and then the while() condition will run where stack==[A]...
            // so the while condition is constructed defensively.
            while((popup && _.some(stack, function(elem){
                return elem.widget == popup;
            })) ||

            (!popup && stack.length)){
                const top = stack.pop();
                const widget = top.widget;
                const onClose = top.onClose;

                if(widget.onClose){
                    widget.onClose();
                }

                let h;

                while(h = top.handlers.pop()){
                    h.remove();
                }

                // Hide the widget and it's wrapper unless it has already been destroyed in above onClose() etc.
                if(widget && widget){
                    this.hide(widget);
                }
                if(onClose){
                    onClose();
                }
            }

            $(popup).css('display','none');

            if(stack.length === 0 && this._aroundMoveListener){
                clearTimeout(this._aroundMoveListener);
                this._firstAroundNode = this._firstAroundPosition = this._aroundMoveListener = null;
            }
        }
    });
    instance  = new Module();
    return instance;
});