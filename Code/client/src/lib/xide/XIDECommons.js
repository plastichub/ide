define([
    'dojo/_base/declare',
    'xide/utils',
    'xide/utils/StringUtils',
    'xide/utils/HTMLUtils',
    'xide/utils/WidgetUtils',
    'xide/utils/ObjectUtils',
    'xfile/config',
    'xide/types',
    'xide/types/Types',
    'xide/factory',
    'xide/factory/Objects',
    'xide/factory/Events'
], function (declare) {
    return declare("xide.XIDECommons_", null, {});
});