/** @module xide/manager/ManagerBase **/
define([
    'dcl/dcl',
    'xide/mixins/EventedMixin',
    'xide/model/Base',
    'xide/utils',
    "dojo/_base/xhr",
    "dojo/_base/kernel"
], function (dcl,EventedMixin,Base,utils,xhr,dojo) {
    /**
     * @class module:xide/manager/ManagerBase
     * @augments module:dojo/Stateful
     * @augments module:xide/mixins/EventedMixin
     * @interface
     */
    const Module =dcl([Base.dcl,EventedMixin.dcl],{
        declaredClass:"xide.manager.ManagerBase",
        /**
         * @type module:xide/manager/ContextBase
         * @member ctx A pointer to a xide context
         */
        ctx: null,
        init: function () {
            this.initReload && this.initReload();
        },
        /**
         *
         * @param title
         * @param scope
         * @param parent
         * @returns {*|{name, isDir, parentId, path, beanType, scope}|{name: *, isDir: *, parentId: *, path: *, beanType: *, scope: *}}
         */
        _getText: function (url,options) {
            let result;
            options = utils.mixin({
                url: url,
                sync: true,
                handleAs: 'text',
                load: function (text) {
                    result = text;
                }
            },options);

            const def = xhr.get(options);
            if(!options.sync){
                return def;
            }
            return '' + result + '';
        },
        /**
         * Return context
         * @returns {module:xcf/manager/Context}
         */
        getContext:function(){
            return this.ctx;
        }
    });

    dcl.chainAfter(Module,'init');
    return Module;
});