/**
 * @module xide/manager/TrackingManager
 */
define([
    'dcl/dcl',
    'xdojo/declare',
    'xide/manager/ManagerBase',
    'xide/utils',
    'xide/views/History',
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    "xide/data/Model"
], function (dcl, declare, ManagerBase, utils, History, TreeMemory, ObservableStore, Trackable, Model) {

    /**
     * @class module:xtrack/data/Store
     */
    const storeClass = declare('TrackingStore', [TreeMemory, Trackable, ObservableStore], {
        parentField:'parent',
        parentProperty:'parent',
        idProperty:'url',
        /**
         * Override dstore/Memory for removing child tracks as well.
         * @param url {string}
         * @returns {*}
         */
        removeSync:function(url){
            const storeItem = this.getSync(url);
            storeItem && utils.removeSync(this,storeItem,url,'parent');
            return this.inherited(arguments);
        }
    });

    /**
     * A serializable tracking model.
     * @class: module:xtrack/model/Track
     * @example:
     * {
        "id": "3f903349-faf4-2eaf-4b99-d8ba225e4643",
        "time": 1480157185439,
        "url": "device://?view=settings&item=e5a06e24-6aa4-c8c5-3ffc-9d84d8528a91",
        "label": "Device",
        "category": "Marantz",
        "command": "File/Open",
        "context": "/PMaster/projects/x4mm/user/",
        "type": "added",
        "counter": 1,
        "_history": {
          "_history": [
            1480157202248
          ],
          "_index": 1
        }
      }
     */
    const storeItemModel = declare('TrackingModel', Model, {
        /**
         * @type {string} An ui friendly label.
         */
        label: null,
        /**
         * @type {string} Additional filter vector.
         */
        context: null,
        /**
         * @type {string} An ui friendly category. We may want to group tracking along this vector.
         */
        category: null,
        /**
         * @type {url} An unique url.
         */
        url: null,
        /**
         * @type {url} An unique url to the parent's tracking url.
         */
        parent: null,
        /**
         * @type {string} A random uuid.
         */
        id: null,
        /**
         * @type {string} We track the action origin so we're able to determine more frequent used actions.
         */
        command: null,
        /**
         * @type {integer} The time of usage.
         */
        time: null,
        /**
         * @type {integer} The counter of usage.
         */
        counter: 0,
        /**
         * @type {module:xide/views/History} To track the usage in a time table in order to determine 'frequent' actions.
         */
        _history: null,
        /**
         * Retain usage counter and track time of usage.
         * @returns {number}
         */
        retain: function () {
            this.counter++;
            this.getHistory().push(new Date().getTime())
        },
        /**
         *
         * @returns {module:xide/views/History}
         */
        getHistory: function () {
            if (!this._history) {
                this._history = new History();
            }
            return this._history;
        },
        destroy:function(){
            this._history && this._history.destroy() && (this._history = null);
        }
    });
    /**
     *
     * Goals:
     *
     * - tracking of actions & item usage for building a history of user activities
     * - being able to retrieve a user's most used items
     * - being able to retrieve a user's most done actions
     *
     * @class module:xide/manager/TrackingManager
     * @extends module:xide/manager/ManagerBase
     */
    return dcl(ManagerBase, {
        declaredClass: "xide.manager.TrackingManager",
        /**
         * Override store class
         * @type {module:xide/data/_Base|null}
         */
        STORE_CLASS:null,
        /**
         * Override item model class
         * @type {module:xide/data/Model|null}
         */
        MODEL_CLASS:null,
        /**
         * @type {module:xtrack/data/Store}
         */
        _store:null,
        /**
         *
         * @returns {module:xtrack/data/Store}
         */
        getStore:function(){
            return this._store;
        },
        /**
         *
         * @param category
         * @param label
         * @param url
         * @param command {string|null}
         * @param context {string|null}
         * @returns {module:xtrack/model/Track}
         */
        track: function (category, label, url, command,context) {
            let storeItem = this._store.getSync(url);
            if (storeItem) {
                storeItem.retain();
                return storeItem;
            }
            storeItem = {
                id: utils.createUUID(),
                time: new Date().getTime(),
                url: url,
                label: label,
                category: category,
                counter: 0,
                command: command,
                context:context
            }
            return this._store.putSync(storeItem);
        },
        drop: function (url) {
            this._store.removeSync(url);
        },
        /**
         * impl. std interface
         * @inheritDoc
         */
        init: function () {
            this._store = new (this.STORE_CLASS || storeClass)({
                Model: this.MODEL_CLASS || storeItemModel
            });
        },
        /**
         * impl. std interface
         * @inheritDoc
         */
        serialize: function () {
            const items = this._store.query();
            return JSON.stringify(items, null, 2);
        },
        /**
         * impl. std interface
         * @param data {string|object[]}
         * @inheritDoc
         */
        deserialize: function (data) {
            this._store.setData(utils.fromJson(data));
        },
        /**
         * std query
         * @returns {*}
         */
        current: function () {
            return this._store.query();
        },
        /**
         * @inheritDoc
         */
        destroy:function(){
            this._store.destroy();
        }
    });
});
