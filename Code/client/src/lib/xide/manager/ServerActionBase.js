/** @module xide/manager/ServerActionBase */
define([
    'dcl/dcl',
    'dojo/_base/declare',
    'xdojo/has',
    'dojo/Deferred',
    'xide/manager/RPCService',
    'xide/manager/ManagerBase',
    'xide/types',
    'xide/utils'
], function (dcl, declare, has, Deferred, RPCService, ManagerBase, types, utils) {
    let Singleton = null;
    /**
     * Class dealing with JSON-RPC-2, used by most xide managers
     * @class module:xide.manager.ServerActionBase
     * @augments {module:xide/manager/ManagerBase}
     */
    const Implementation = {
        declaredClass: "xide.manager.ServerActionBase",
        serviceObject: null,
        serviceUrl: null,
        singleton: true,
        serviceClass: null,
        defaultOptions: {
            omit: true,
            checkMessages: true,
            checkErrors: true
        },
        base64_encode: function (data) {
            // From: http://phpjs.org/functions
            // +   original by: Tyler Akins (http://rumkin.com)
            // +   improved by: Bayron Guevara
            // +   improved by: Thunder.m
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Pellentesque Malesuada
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   improved by: Rafał Kukawski (http://kukawski.pl)
            // *     example 1: base64_encode('Kevin van Zonneveld');
            // *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
            // mozilla has this native
            // - but breaks in 2.0.0.12!
            //if (typeof this.window.btoa === 'function') {
            //    return btoa(data);
            //}
            const b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
            let o1;
            let o2;
            let o3;
            let h1;
            let h2;
            let h3;
            let h4;
            let bits;
            let i = 0;
            let ac = 0;
            let enc = '';
            const tmp_arr = [];

            if (!data) {
                return data;
            }

            do { // pack three octets into four hexets
                o1 = data.charCodeAt(i++);
                o2 = data.charCodeAt(i++);
                o3 = data.charCodeAt(i++);

                bits = o1 << 16 | o2 << 8 | o3;

                h1 = bits >> 18 & 0x3f;
                h2 = bits >> 12 & 0x3f;
                h3 = bits >> 6 & 0x3f;
                h4 = bits & 0x3f;

                // use hexets to index into b64, and append result to encoded string
                tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
            } while (i < data.length);

            enc = tmp_arr.join('');

            const r = data.length % 3;

            return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
        },
        runDeferred: function (serviceClassIn, method, args, options, onError) {
            if (this.serviceObject.__init) {
                if (this.serviceObject.__init.isResolved()) {
                    return this._runDeferred(serviceClassIn, method, args, options, onError);
                }
                const dfd = new Deferred();
                this.serviceObject.__init.then(() => {
                    this._runDeferred(serviceClassIn, method, args, options, onError).then(() => {
                        dfd.resolve(arguments);
                    });
                });
                return dfd;
            }
            return this._runDeferred(serviceClassIn, method, args, options);
        },
        /**
         * Public main entry, all others below are deprecated
         * @param serviceClassIn
         * @param method
         * @param args
         * @param options
         * @returns {Deferred}
         */
        _runDeferred: function (serviceClassIn, method, args, options, onError) {
            const deferred = new Deferred();
            let promise;
            options = options || this.defaultOptions;
            //check we the RPC method is in the SMD
            this.check();

            //check this method exists
            if (!this.checkCall(serviceClassIn, method, options.omit)) {
                return deferred.reject('method doesnt exists: ' + method + ' for service class:' + this.serviceClass + ' in ' + this.declaredClass);
            }

            //setup signing in serviceObject
            this.prepareCall();

            //variable shortcuts
            const service = this.getService();

            const serviceClass = this.getServiceClass(serviceClassIn);
            const thiz = this;

            const resolve = function (data, error) {
                let dfd = deferred;
                if (options.returnProm) {
                    dfd = promise;
                }
                dfd._data = data;
                if (error) {
                    if (options.onError) {
                        return options.onError(error);
                    }
                }
                dfd.resolve(data);
            };
            promise = service[serviceClass][method](args);
            promise.then(function (res) {
                res = res || {};
                const error = res.error || {};
                //the server has some messages for us
                if (options.checkMessages) {
                    if (error && error.code == 3) {
                        thiz.onMessages(error);
                    }
                }
                //check for error messages (non-fatal) and abort
                if (options.checkErrors) {
                    if (error.code == 1) {
                        options.displayError && thiz.onError(error, serviceClass + '::' + method);
                        deferred.reject(error);
                        return;
                    }
                } else {
                    if (error.code == 1 && options.displayError) {
                        thiz.onError(error, serviceClass + '::' + method);
                    }
                    if (error && error.code && error.code !== 0) {
                        resolve(res, error);
                        return;
                    }
                }
                //until here all is ok, tell everybody
                if (options.omit) {
                    thiz.publish(types.EVENTS.STATUS, {
                        message: 'Ok!',
                        what: arguments
                    }, this);
                }
                resolve(res);
            }, function (err) {
                onError && onError(err);
                thiz.onError({
                    code: 1,
                    message: 'Rest Error for ' + method
                });
            });
            if (options.returnProm) {
                return promise;
            }
            return deferred;
        },
        getService: function () {
            return this.serviceObject;
        },
        getServiceClass: function (serviceClassIn) {
            return serviceClassIn || this.serviceClass;
        },
        hasMethod: function (method, serviceClass) {
            const _service = this.getService();
            const _serviceClass = serviceClass || this.getServiceClass();

            return _service &&
                _serviceClass &&
                _service[_serviceClass] != null &&
                _service[_serviceClass][method] != null;
        },
        findServiceUrl: function (declaredClass) {
            const config = window['xFileConfig'];
            if (config && config.mixins) {
                for (let i = 0; i < config.mixins.length; i++) {
                    const obj = config.mixins[i];
                    if (obj.declaredClass === declaredClass && obj.mixin && obj.mixin.serviceUrl) {
                        return decodeURIComponent(obj.mixin.serviceUrl);
                    }
                }
            }
            return null;
        },
        init: function () {
            this.check();
        },
        _initService: function () {
            const thiz = this;
            if (!has('host-browser')) {
                return false;
            }
            try {
                const obj = Singleton;
                if (this.singleton) {
                    if (obj && obj.serviceObject) {
                        this.serviceObject = obj.serviceObject;
                        return;
                    }
                    if (!this.options) {
                        this.options = {};
                    }
                    this.options.singleton = this.singleton;
                }
                if (!this.serviceObject) {
                    if (!this.serviceUrl) {
                        console.error('have no service url : ' + this.declaredClass);
                        return;
                    }
                    const url = decodeURIComponent(this.serviceUrl);
                    this.serviceObject = new RPCService(decodeURIComponent(this.serviceUrl), this.options);

                    this.serviceObject.runDeferred = function () {
                        return thiz.runDeferred.apply(thiz, arguments);
                    };


                    this.serviceObject.sync = this.sync;

                    if (this.singleton) {
                        obj.serviceObject = this.serviceObject;
                    }
                    if (this.config) {
                        obj.serviceObject.config = this.config;
                    }!this.ctx.serviceObject && (this.ctx.serviceObject = this.serviceObject);
                }
            } catch (e) {
                console.error('error in rpc service creation : ' + e);
                logError(e);
            }
        },
        check: function () {
            if (!this.serviceObject) {
                this._initService();
            }
        },
        onError: function (err, suffix) {
            if (err) {
                if (err.code === 1) {
                    if (err.message && _.isArray(err.message)) {
                        this.publish(types.EVENTS.ERROR, {
                            message: err.message.join('<br/>')
                        });
                        return;
                    }
                } else if (err.code === 0) {
                    this.publish(types.EVENTS.STATUS, 'Ok');
                }
            }
            if (suffix) {
                err.message = suffix + ' -> ' + err.message;
            }
            this.publish(types.EVENTS.ERROR, {
                error: err
            }, this);
        },
        checkCall: function (serviceClass, method, omit) {
            serviceClass = this.getServiceClass(serviceClass);
            if (!this.getService()) {
                return false;
            }
            if (!this.hasMethod(method, serviceClass) && omit === true) {
                this.onError({
                    code: 1,
                    message: ['Sorry, server doesnt know ' + method + ' in ' + serviceClass]
                });
                return false;
            }
            return true;
        },
        prepareCall: function () {
            let params = {};
            //Mixin mandatory fields
            if (this.config && this.config.RPC_PARAMS) {
                params = utils.mixin(params, this.config.RPC_PARAMS.rpcFixedParams);
                this.serviceObject.extraArgs = params;
                //if (this.config.RPC_PARAMS.rpcUserField) {
                //    params[this.config.RPC_PARAMS.rpcUserField] = this.config.RPC_PARAMS.rpcUserValue;
                //    this.serviceObject.signatureField = this.config.RPC_PARAMS.rpcSignatureField;
                //    this.serviceObject.signatureToken = this.config.RPC_PARAMS.rpcSignatureToken;
                //}
            }
        },
        callMethodEx: function (serviceClassIn, method, args, readyCB, omitError) {
            serviceClassIn = serviceClassIn || this.serviceClass;
            if (!serviceClassIn) {
                console.error('have no service class! ' + this.declaredClass, this);
            }
            //init smd
            this.check();

            //check this method exists
            if (!this.checkCall(serviceClassIn, method, omitError)) {
                return;
            }
            //setup signing in serviceObject
            this.prepareCall();
            const thiz = this;
            return this.serviceObject[this.getServiceClass(serviceClassIn)][method](args).then(function (res) {
                try {
                    if (readyCB) {
                        readyCB(res);
                    }
                } catch (e) {
                    console.error('bad news : callback for method ' + method + ' caused a crash in service class ' + serviceClassIn);
                    logError(e, 'server method failed ' + e);

                }
                //rpc batch results
                if (res && res.error && res.error.code == 3) {
                    thiz.onMessages(res.error);
                }

                if (res && res.error && res.error && res.error.code != 0) {
                    thiz.onError(res.error);
                    return;
                }
                if (omitError == true) {
                    thiz.publish(types.EVENTS.STATUS, {
                        message: 'Ok!'
                    }, this);
                }

            }, function (err) {
                thiz.onError(err);
            });
        },
        callMethodEx2: function (serverClassIn, method, args, readyCB, omitError) {
            this.check();
            //check this method exists
            if (!this.checkCall(serverClassIn, method, omitError)) {
                return;
            }
            //setup signing in serviceObject
            this.prepareCall();
            return this.serviceObject[this.getServiceClass(serverClassIn)][method](args);
        },
        callMethod: function (method, args, readyCB, omitError) {
            args = args || [
                []
            ];
            const serviceClass = this.serviceClass;
            try {
                var thiz = this;
                //method not listed in SMD
                if (this.serviceObject[serviceClass][method] == null) {
                    if (omitError === true) {
                        this.onError({
                            code: 1,
                            message: ['Sorry, server doesnt know ' + method + ' in ' + serviceClass]
                        });
                    }
                    return null;
                }
                /***
                 * Build signature
                 */
                let params = {};
                params = utils.mixin(params, this.config.RPC_PARAMS.rpcFixedParams);
                /**
                 * Mixin mandatory fields
                 */
                params[this.config.RPC_PARAMS.rpcUserField] = this.config.RPC_PARAMS.rpcUserValue;
                this.serviceObject.extraArgs = params;
                this.serviceObject.signatureField = this.config.RPC_PARAMS.rpcSignatureField;
                this.serviceObject.signatureToken = this.config.RPC_PARAMS.rpcSignatureToken;
                this.serviceObject[this.serviceClass][method](args).then(function (res) {
                    try {
                        if (readyCB) {
                            readyCB(res);
                        }
                    } catch (e) {
                        logError(e, "Error calling RPC method");
                    }
                    //rpc batch call
                    if (res && res.error && res.error.code == 3) {
                        this.onMessages(res.error);
                    }
                    if (res && res.error && res.error && res.error.code == 1) {
                        this.onError(res.error);
                        return;
                    }
                    if (omitError !== false) {
                        const struct = {
                            message: 'Ok!'
                        };
                        this.publish(types.EVENTS.STATUS, struct, this);
                    }
                }.bind(this), function (err) {
                    this.onError(err);
                }.bind(this));
            } catch (e) {
                thiz.onError(e);
                logError(e, "Error calling RPC method");
            }
        }
    };

    const Module = dcl(ManagerBase, Implementation);
    Module.declare = declare(null, Implementation);
    Singleton = Module;
    return Module;
});