define([
    'dojo/_base/declare',
    'dojo/_base/kernel',
    'dojo/_base/lang',
    'xide/rpc/Service',
    'xide/rpc/JsonRPC',
    'dojo/has',
    'dojo/Deferred',
    'xide/utils',
    'xide/types',
    'xide/mixins/EventedMixin',
    'xide/encoding/SHA1'
], function (declare,dojo,lang, Service, JsonRPC, has, Deferred,utils,types,EventedMixin,SHA1) {
    /**
     * Provides tools to deal with 'persistence' (open files, editors, ..etc to be restored). It also acts as interface.
     * @class module:xide/manager/RPCService
     * @extends module:xide/mixins/EventedMixin
     *
     **/
    return declare("xide.manager.RPCService", [Service,EventedMixin], {
        extraArgs: null,
        signatureField: 'sig',
        signatureToken: null,
        correctTarget: true,
        sync: false,
        defaultOptions: {
            omit: true,
            checkMessages: true,
            checkErrors: true
        },
        onError: function (err) {
            if (err) {
                if (err.code === 1) {
                    if (err.message && _.isArray(err.message)) {
                        this.publish(types.EVENTS.ERROR, {message: err.message.join('<br/>')});
                        return;
                    }
                } else if (err.code === 0) {
                    this.publish(types.EVENTS.STATUS, 'Ok');
                }
            }
            const struct = {
                error: err
            };
            this.publish(types.EVENTS.ERROR, struct, this);
        },
        prepareCall: function () {
            let params = {};
            if (this.config && this.config.RPC_PARAMS) {
                params = utils.mixin(params, this.config.RPC_PARAMS.rpcFixedParams);
                this.extraArgs = params;
                if (this.config.RPC_PARAMS.rpcUserField) {
                    params[this.config.RPC_PARAMS.rpcUserField] = this.config.RPC_PARAMS.rpcUserValue;

                    this.signatureField = this.config.RPC_PARAMS.rpcSignatureField;
                    this.signatureToken = this.config.RPC_PARAMS.rpcSignatureToken;
                }
            }
        },
        runDeferred: function (serviceClassIn, method, args, options) {
            const deferred = new Deferred();
            options = options || this.defaultOptions;

            //check this method exists
            if (!this.checkCall(serviceClassIn, method, options.omit)) {
                return deferred.reject('method doesnt exists: ' + method + ' for service class:' + this.serviceClass + ' in ' + this.declaredClass);
            }

            //setup signing in serviceObject
            this.prepareCall();

            //variable shortcuts
            const service = this;

            const serviceClass = this.getServiceClass(serviceClassIn);
            const thiz = this;

            const resolve = function (data) {
                deferred.resolve(data);
            };

            const promise = service[serviceClass][method](args);
            promise.then(function (res) {

                //the server has some messages for us
                if (options.checkMessages) {
                    if (res && res.error && res.error.code == 3) {
                        thiz.onMessages(res.error);
                    }
                }

                //check for error messages (non-fatal) and abort
                if (options.checkErrors) {
                    if (res && res.error && res.error && res.error.code != 0) {
                        thiz.onError(res.error);
                        deferred.reject(res.error);
                        return;
                    }
                }

                //until here all is ok, tell everybody
                if (options.omit) {
                    thiz.publish(types.EVENTS.STATUS, {
                        message: 'Ok!',
                        what: arguments
                    }, this);
                }


                //final delivery
                resolve(res);


            }, function (err) {
                thiz.onError(err);
            });

            return deferred;
        },
        getParameterMap: function (serviceClass, serviceClassMethod) {

            const services = this._smd.services;
            const smd = services[serviceClass + '.' + serviceClassMethod];
            if (smd && smd.parameters) {
                return smd.parameters;
            }
            return [];
        },
        _getRequest: function (method, args) {
            const smd = this._smd;
            const envDef = Service.envelopeRegistry.match(method.envelope || smd.envelope || "NONE");
            const parameters = (method.parameters || method.params || []).concat(smd.parameters || []);

            if (envDef.namedParams) {
                // the serializer is expecting named params
                if ((args.length == 1) && lang.isObject(args[0])) {
                    // looks like we have what we want
                    args = args[0];
                } else {
                    // they provided ordered, must convert
                    const data = {};
                    const params = method.parameters || method.params;
                    for (var i = 0; i < params.length; i++) {
                        if (typeof args[i] != "undefined" || !params[i].optional) {
                            data[params[i].name] = args[i];
                        }
                    }
                    args = data;
                }
                if (method.strictParameters || smd.strictParameters) {
                    //remove any properties that were not defined
                    for (i in args) {
                        let found = false;
                        for (let j = 0; j < parameters.length; j++) {
                            if (parameters[i].name == i) {
                                found = true;
                            }
                        }
                        if (!found) {
                            delete args[i];
                        }
                    }

                }
                // setting default values
                for (i = 0; i < parameters.length; i++) {
                    const param = parameters[i];
                    if (!param.optional && param.name && args != null && !args[param.name]) {
                        if (param["default"]) {
                            args[param.name] = param["default"];
                        } else if (!(param.name in args)) {
                            throw new Error("Required parameter " + param.name + " was omitted");
                        }
                    }
                }
            } else if (parameters && parameters[0] && parameters[0].name && (args.length == 1) && dojo.isObject(args[0])) {
                // looks like named params, we will convert
                if (envDef.namedParams === false) {
                    // the serializer is expecting ordered params, must be ordered
                    args = Service.toOrdered(parameters, args);
                } else {
                    // named is ok
                    args = args[0];
                }
            }

            if (lang.isObject(this._options)) {
                args = dojo.mixin(args, this._options);
            }

            const schema = method._schema || method.returns; // serialize with the right schema for the context;
            const request = envDef.serialize.apply(this, [smd, method, args]);
            request._envDef = envDef;// save this for executeMethod
            const contentType = (method.contentType || smd.contentType || request.contentType);

            // this allows to mandate synchronous behavior from elsewhere when necessary, this may need to be changed to be one-shot in FF3 new sync handling model
            return dojo.mixin(request, {
                sync: this.sync,//dojox.rpc._sync,
                contentType: contentType,
                headers: method.headers || smd.headers || request.headers || {},
                target: request.target || Service.getTarget(smd, method),
                transport: method.transport || smd.transport || request.transport,
                envelope: method.envelope || smd.envelope || request.envelope,
                timeout: method.timeout || smd.timeout,
                callbackParamName: method.callbackParamName || smd.callbackParamName,
                rpcObjectParamName: method.rpcObjectParamName || smd.rpcObjectParamName,
                schema: schema,
                handleAs: request.handleAs || "auto",
                preventCache: method.preventCache || smd.preventCache,
                frameDoc: this._options.frameDoc || undefined
            });
        },
        _executeMethod: function (method) {
            let args = [];
            let i;
            if (arguments.length == 2 && lang.isArray(arguments[1])) {
                args = arguments[1];
            } else {
                for (i = 1; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }
            }
            const request = this._getRequest(method, args);
            if (this.correctTarget) {
                request.target = this._smd.target;
            }
            const deferred = Service.transportRegistry.match(request.transport).fire(request);
            deferred.addBoth(function (results) {
                return request._envDef.deserialize.call(this, results);
            });
            return deferred;
        },
        getServiceClass: function (serviceClassIn) {
            return serviceClassIn || this.serviceClass;
        },
        hasMethod: function (method,serviceClass) {
            const _service = this;
            const _serviceClass = serviceClass || this.getServiceClass();

            return _service &&
                _serviceClass &&
                _service[_serviceClass] != null &&
                _service[_serviceClass][method] != null;
        },
        checkCall: function (serviceClass, method, omit) {
            serviceClass = this.getServiceClass(serviceClass);
            if (!this.hasMethod(method,serviceClass) && omit === true) {
                debugger;
                this.onError({
                    code: 1,
                    message: ['Sorry, server doesnt know ' + method + ' in ' + serviceClass]
                });
                return false;
            }
            return true;
        },


        /************************************************
         *
         * @param data
         * @returns {*}
         */
        base64_encode: function (data) {
            // From: http://phpjs.org/functions
            // +   original by: Tyler Akins (http://rumkin.com)
            // +   improved by: Bayron Guevara
            // +   improved by: Thunder.m
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Pellentesque Malesuada
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   improved by: Rafał Kukawski (http://kukawski.pl)
            // *     example 1: base64_encode('Kevin van Zonneveld');
            // *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
            // mozilla has this native
            // - but breaks in 2.0.0.12!
            //if (typeof this.window.btoa === 'function') {
            //    return btoa(data);
            //}
            const b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
            let o1;
            let o2;
            let o3;
            let h1;
            let h2;
            let h3;
            let h4;
            let bits;
            let i = 0;
            let ac = 0;
            let enc = '';
            const tmp_arr = [];

            if (!data) {
                return data;
            }

            do { // pack three octets into four hexets
                o1 = data.charCodeAt(i++);
                o2 = data.charCodeAt(i++);
                o3 = data.charCodeAt(i++);

                bits = o1 << 16 | o2 << 8 | o3;

                h1 = bits >> 18 & 0x3f;
                h2 = bits >> 12 & 0x3f;
                h3 = bits >> 6 & 0x3f;
                h4 = bits & 0x3f;

                // use hexets to index into b64, and append result to encoded string
                tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
            } while (i < data.length);

            enc = tmp_arr.join('');

            const r = data.length % 3;

            return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
        },
        callMethodEx: function (serviceClass, method, args, readyCB, errorCB, omitError) {

            /***
             * Check we the RPC method is in the SMD
             */

            const thiz = this;
            if (!this[serviceClass] || this[serviceClass][method] == null) {
                if (omitError === true && errorCB) {
                    errorCB({
                        code: 1,
                        message: ['Sorry, server doesnt know ' + method + ' in class' + serviceClass]
                    });
                }
                return null;
            }

            /***
             * Build signature
             */
            let params = {};


            /**
             * Mixin mandatory fields
             */
            if (this.config && this.config.RPC_PARAMS) {
                params = lang.mixin(params, this.config.RPC_PARAMS.rpcFixedParams);
                params[this.config.RPC_PARAMS.rpcUserField] = this.config.RPC_PARAMS.rpcUserValue;
                this.extraArgs = params;
                this.signatureField = this.config.RPC_PARAMS.rpcSignatureField;
                this.signatureToken = this.config.RPC_PARAMS.rpcSignatureToken;
            }


            this[serviceClass][method](args).then(function (res) {
                try {
                    if (readyCB) {
                        readyCB(res);
                    }
                } catch (e) {
                    console.error('bad news : callback for method ' + method + ' caused a crash in service class ' + serviceClass);
                }

                if (res && res.error && res.error && res.error.code != 0 && errorCB) {
                    errorCB(res.error);
                    return;
                }
                if (omitError == true) {

                }

            }, function (err) {
                errorCB(err);
            });
        },
        callMethod: function (serviceClass, method, args, readyCB, errorCB, omitError) {
            /***
             * Check we the RPC method is in the SMD
             */
            try {
                const thiz = this;
                if (this[serviceClass][method] == null) {
                    if (omitError === true && errorCB) {
                        errorCB({
                            code: 1,
                            message: ['Sorry, server doesnt know ' + method + ' in ' + serviceClass]
                        });
                    }
                    return null;
                }
                /***
                 * Build signature
                 */
                let params = {};
                params = lang.mixin(params, this.config.RPC_PARAMS.rpcFixedParams);
                /**
                 * Mixin mandatory fields
                 */
                this[serviceClass][method](args).then(function (res) {
                    try {
                        if (readyCB) {
                            readyCB(res);
                        }
                    } catch (e) {
                        console.error('crashed in ' + method);
                        console.dir(e);

                    }
                    if (res && res.error && res.error && res.error.code == 1 && errorCB) {
                        errorCB(res.error);
                        return;
                    }

                    if (omitError !== false) {
                        const struct = {
                            message: 'Ok!'
                        };
                        //thiz.publish(types.EVENTS.STATUS,struct ,this);
                    }

                }, function (err) {
                    thiz.onError(err);
                });
            } catch (e) {
                console.error('crash! ' + e);
            }
        }

    });
});