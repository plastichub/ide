define([
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xide/manager/ManagerBase',
    'xide/encoding/MD5'
],function(dcl,types,utils,ManagerBase,MD5){

    return dcl([ManagerBase],{
        declaredClass:"xide.manager.NotificationManager",
        onError:function(err){
            let mess = '';
            if(err && err&& err.error && err.error.message){
                mess = err.error.message;
            }else if(err && _.isString(err)){
                mess = err;
            }else if(err && _.isString(err.message)){
                mess = err.message;
            }
            const args = {
                message: mess,
                type:'error',
                duration: 500,
                showCloseButton: true
            };

            if(mess.includes('Stack trace')){
                args.message = mess.substr(0,mess.indexOf('Stack trace'));
                args.duration = 4000;
            }

            if(err.messageArgs){
                utils.mixin(args,err.messageArgs);
            }
            this.postMessage(args);
        },
        _lastMessageTime:null,
        _lastMessageHash:null,
        onStatus:function(err){
            let mess = '';

            if(err.did!=null){
                return;
            }
            err.did=true;

            if(err && err&& err.message){
                mess = err.message;
            }else if(err && _.isString(err)){
                mess = err;
            }


            if(mess=='Ok!'){
                return ;
            }
            const args = {
                message: mess,
                type:'success',
                duration: 500,
                showCloseButton: true
            };

            utils.mixin(args,err.messageArgs);

            //var timeNow = new Date().getTime();
            /*
            if (thiz.fileUpdateTimes[_path]) {
                var last = thiz.fileUpdateTimes[_path];
                var diff = timeNow - last;
                if (diff < 1000) {
                    thiz.fileUpdateTimes[_path] = timeNow;
                    return;
                }
            }



            thiz.fileUpdateTimes[_path] = timeNow;
            */

            //console.log('print ' + hash,args);

            this.postMessage(args);
        },
        postMessage:function(msg){
            const hash = MD5(JSON.stringify(msg),1);
            const self = this;

            if(this._lastMessageHash===hash){
                setTimeout(function () {
                    self._lastMessageHash=null;
                },2000);
                return;
            }
            this._lastMessageHash = hash;
            if(this._lastMessageTime){
                clearTimeout(this._lastMessageTime);
            }


            if(!this._lastMessageTime) {
                this._lastMessageTime = setTimeout(function () {
                    self._lastMessageHash = null;
                    delete self._lastMessageTime;
                    self._lastMessageTime = null;
                }, 1000);
            }
            return Messenger().post(msg);
        },
        init:function(){
            if($.globalMessenger && typeof Messenger!=='undefined') {
                this.subscribe(types.EVENTS.STATUS,this.onStatus);
                this.subscribe(types.EVENTS.ERROR,this.onError);
                const theme = 'flat';
                $.globalMessenger({
                    theme: theme,
                    extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right'
                });
                Messenger.options = {
                    theme: theme,
                    extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right'
                };
            }else{
                this.postMessage=function(){}
            }
        }
    });
});