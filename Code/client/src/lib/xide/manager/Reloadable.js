/** @module xide/manager/Reloadable **/
define([
    'dcl/dcl',
    "dojo/_base/lang",
    "xide/utils",
    "xide/types"
], function (dcl,lang,utils,types) {
    /**
     * @class xide.manager.Reloadable
     * @augments module:xide/mixins/EventedMixin
     */
    return dcl(null, {
        declaredClass:"xide.manager.Reloadable",
        vfsMounts: null,
        xideServiceClient: null,
        fileUpdateTimes:{},
        onXIDELoaded: function (min, WebSocket) {
            return;
            const thiz = this;
            const _ctorArgs = {
                delegate: {
                    onServerResponse: function (e) {
                        thiz.onXIDEMessage(utils.fromJson(e.data));
                    }
                }
            };
            try {
                const client = new WebSocket(_ctorArgs);
                utils.mixin(client, _ctorArgs);
                client.init({
                    options: {
                        host: 'http://0.0.0.0',
                        port: 9993,
                        channel: '',
                        debug: {
                            "all": false,
                            "protocol_connection": true,
                            "protocol_messages": true,
                            "socket_server": true,
                            "socket_client": true,
                            "socket_messages": true,
                            "main": true
                        }
                    }
                });
                client.connect();

            } catch (e) {
                console.error('create client with store : failed : ' + e);
            }

        },
        loadXIDE: function () {
            const thiz = this;
            this.subscribe(types.EVENTS.ON_NODE_SERVICE_STORE_READY, this.onNodeServiceStoreReady);
            require(['xide/min', 'xide/client/WebSocket'], function (min, WebSocket) {
                if (thiz._didXIDE) {
                    return;
                }
                thiz._didXIDE = true;
                thiz.onXIDELoaded(min, WebSocket);
            });
        },
        onXIDEMessage: function (data) {
            const thiz = this;
            thiz.publish(data.event, data);
            if (data.event === types.EVENTS.ON_FILE_CHANGED) {
                const _path = data.data.path;
                const timeNow = new Date().getTime();
                if (thiz.fileUpdateTimes[_path]) {
                    const last = thiz.fileUpdateTimes[_path];
                    const diff = timeNow - last;

                    if (diff < 30) {
                        thiz.fileUpdateTimes[_path] = timeNow;
                        return;
                    }
                }
                thiz.fileUpdateTimes[_path] = timeNow;

                //path is absolute and might look like: /PMaster/projects/xbox-app/client/src/lib/xfile/Views.js
                //fix windows path
                let path = utils.replaceAll('\\', '/', data.data.path);
                path = utils.replaceAll('//', '/', data.data.path);
                /**
                 * Check its a css file
                 */
                if (path == null || path.indexOf == null) {
                    console.error('on file changed : have no path, aborting');
                    return;
                }
                if (path.match(/\.css$/)) {
                    const newEvt = {
                        path: path
                    };
                    thiz.onCSSChanged(newEvt);
                }

                //generic
                if (path.match(/\.js$/)) {
                    let modulePath = data.data.modulePath;
                    if (modulePath) {
                        modulePath = modulePath.replace('.js', '');
                        const _re = require;//hide from gcc
                        //try pre-amd module
                        let module = null;
                        try {
                            module = _re(modulePath);
                        } catch (e) {

                        }
                        const _start = 'node_modules';
                        if (path.includes(_start)) {
                            let libPath = path.substr(path.indexOf(_start) + _start.length, path.length);
                            libPath = libPath.replace('.js', '');
                            if (path.includes('xcfnode')) {
                                path = libPath;
                                modulePath = path.replace('/xcfnode', 'xcfnode');
                            }
                        }
                        modulePath = utils.replaceAll('.', '/', modulePath);
                        setTimeout(function () {
                            thiz._reloadModule(modulePath, true);
                        }, 400);
                    }
                }
            }
        },
        onNodeServiceStoreReady: function (evt) {

        },
        mergeFunctions: function (target, source) {
            for (const i in source) {
                if (_.isFunction(source[i]) /*&& lang.isFunction(target[i])*/) {
                    target[i] = source[i];//swap
                }

            }
        },
        /**
         * Special case when module has been reloaded : update all functions in our singleton
         * managers!
         * @param evt
         */
        onModuleReloaded: function (evt) {
            if(evt._didHandle){
                return;
            }
            evt._didHandle=true;
            this.inherited(arguments);
            if(this.managers) {
                const newModule = evt.newModule;
                for (let i = 0; i < this.managers.length; i++) {
                    const manager = this.managers[i];
                    if (newModule.prototype
                        && newModule.prototype.declaredClass
                        && newModule.prototype.declaredClass === manager.declaredClass) {
                        this.mergeFunctions(manager, newModule.prototype);
                        if (manager.onReloaded) {
                            manager.onReloaded(newModule);
                        }
                        break;
                    }
                }
            }
        },
        _reloadModule: function (module, reload) {


            require.undef(module);
            const thiz = this;
            if (reload) {
                setTimeout(function () {
                    require([module], function (moduleLoaded) {
                        if(!moduleLoaded){
                            console.warn('invalid module');
                            return;
                        }
                        if (_.isString(moduleLoaded)) {
                            console.error('module reloaded failed : ' + moduleLoaded + ' for module : ' + module);
                            return;
                        }
                        console.log('did - re-require module : ' + module);
                        moduleLoaded.modulePath = module;
                        const obj = lang.getObject(utils.replaceAll('/', '.', module));
                        if (obj) {
                            thiz.mergeFunctions(obj.prototype, moduleLoaded.prototype);
                        }
                        thiz.publish(types.EVENTS.ON_MODULE_RELOADED, {
                            module: module,
                            newModule: moduleLoaded
                        });
                        thiz.publish(types.EVENTS.ON_MODULE_UPDATED,{
                            moduleClass:moduleLoaded.prototype.declaredClass,
                            moduleProto:moduleLoaded.prototype
                        });
                    });
                }, 500);
            }
        },
        onCSSChanged: function (evt) {
            if (evt['didProcess']) {
                return;
            }
            evt['didProcess'] = true;
            let path = evt.path;
            path = utils.replaceAll('//', '/', path);
            path = path.replace('/PMaster/', '');
            const reloadFn = window['xappOnStyleSheetChanged'];
            if (reloadFn) {
                reloadFn(path);
            }
        },
        onDidChangeFileContent: function (evt) {
            if (evt['didProcess']) {
                return;
            }
            evt['didProcess'] = true;
            if (!this.vfsMounts) {
                return;
            }
            if (!evt.path) {
                return;
            }
            const path = evt.path;
            if (path.includes('.css')) {
                this.onCSSChanged(evt);
                return;
            }

            if (path.includes('resources') ||
                path.includes('meta') ||
                !path.includes('.js')) {
                return;
            }

            const mount = evt.mount.replace('/', '');
            let module = null;
            if (!this.vfsMounts[mount]) {
                return;
            }
            module = '' + evt.path;
            module = module.replace('./', '');
            module = module.replace('/', '.');
            module = module.replace('.js', '');
            module = utils.replaceAll('.', '/', module);
            const thiz = this;
            setTimeout(function () {
                thiz._reloadModule(module, true);
            }, 500);
        }
    });
});



