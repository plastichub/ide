/** module:xide/manager/Router **/
define(['xide/utils'], function (utils) {
    /**
     * @param {Object} object
     * @param {Function} callback
     * @param {Object} [ctx]
     * @returns {Object}
     */
    const map = function (object, callback, ctx) {
        const copy = {};
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                copy[key] = callback.call(ctx, object[key], key, object);
            }
        }
        return copy;
    };

    /**
     * merges objects
     * @returns {{}}
     */
    const merge = function () {
        const result = {};
        Array.prototype.forEach.call(arguments, function (o) {
            for (const key in o) {
                if (o.hasOwnProperty(key)) {
                    result[key] = o[key];
                }
            }
        });
        return result;
    };

    /**
     * @param {*} o
     * @returns {*}
     */
    const clone = function (o) {
        if (Array.isArray(o)) {
            return o.map(clone);
        }
        if (o instanceof RegExp || [
                'string',
                'number',
                'boolean',
                'object'
            ].includes(typeof o) || o == null) {
            return o;
        }
        return map(o, clone);
    };

    /**
     * @typedef {Object} module:xide/interface/RouteDefinition
     * @property {String} id
     * @property {String} path
     * @property {String} [host]
     * @property {String} [prefix]
     * @property {Object.<String|Number>} [defaults]
     * @property {Object.<String>} [requirements]
     * @property {Array.<String>} [methods]
     * @property {Array.<String>} [schemes]
     * @property {module:xide/interface/Route} delegate
     */

    /**
     * @typedef {Object} RoutesDefinition
     * @property {String} [prefix]
     * @property {Array.<RouteDefinition|RoutesDefinition>} routes
     */

    /**
     * @typedef {Object} RouteDefinitionParsed
     * @property {String} id
     * @property {RegExp} pattern
     * @property {String} path
     * @property {Array.<String>} parameters
     * @property {RegExp} hostPattern
     * @property {String} host
     * @property {Array.<String>} hostParameters
     * @property {Object.<RegExp>} requirements
     * @property {Object.<String>} defaults
     * @property {Array.<String>} methods
     * @property {Array.<String>} schemes
     * @property {{RouteDefinition}} definition
     */

    /**
     * @typedef {Object} Route
     * @property {String} id
     * @property {RouteDefinition} definition
     * @property {Object.<String>} [parameters]
     */

    /**
     * @class module:xide/manager/Router
     * @param {Object} options
     * @param {Array.<RouteDefinition|RoutesDefinition>} options.routes
     * @param {Array.<String>} [options.defaultMethods=['GET', 'POST', 'PUT', 'DELETE']]
     * @param {Array.<String>} [options.defaultSchemes=['http', 'https']]
     * @param {Object} [options.symbols]
     * @param {String} [options.symbols.defaultParameterRequirement='[^\\/.]*']
     * @param {String} [options.symbols.parametersDelimiters='\.|\/']
     * @param {String} [options.symbols.parameterStart='\{']
     * @param {String} [options.symbols.parameterMatcher='.*?']
     * @param {String} [options.symbols.parameterEnd='\}']
     */
    const RouterBase = function (options) {
        const symbols = options.symbols || {};
        const paramsDelimiters = symbols.parametersDelimiters || '\\.|\\/';
        const paramStart = symbols.parameterStart || '\\{';
        const paramMatcher = symbols.parameterMatcher || '.*?';
        const paramEnd = symbols.parameterEnd || '\\}';

        /**
         * @const {{regexp: RegExp, parameters: Array}}
         * @private
         */
        this._NO_HOST = {
            regexp: /.*/,
            parameters: []
        };

        /**
         * @type {string}
         * @private
         */
        this._defaultParameterRequirement = symbols.defaultParameterRequirement || '[^\\/.]*';

        /**
         * @type {RegExp}
         * @private
         */
        this._paramsReplaceRegExp = new RegExp('(' + paramsDelimiters + ')?' + paramStart +
            '(' + paramMatcher + ')' + paramEnd, 'g');

        /**
         * @type {Array.<String>}
         * @private
         */
        this._defaultMethods = options.defaultMethods || ['GET', 'POST', 'PUT', 'DELETE'];

        /**
         * @type {Array.<String>}
         * @private
         */
        this._defaultSchemes = options.defaultSchemes || ['http', 'https'];

        /**
         * @type {Array.<RouteDefinition>}
         * @private
         */
        this._routesDefinitions = this._unifyRoutes(options.routes);

        /**
         * @type {Array.<RouteDefinitionParsed>}
         * @private
         */
        this._routes = this._parseRoutes(this._routesDefinitions);

        /**
         * @type {Object.<RouteDefinitionParsed>}
         * @private
         */
        this._routeIdToParsedDefinitionMap = {};
    };

    /**
     * @param {Array.<RouteDefinition|RoutesDefinition>} routes
     * @param {RouteDefinition} [parentRoute]
     * @returns {Array.<RouteDefinition>}
     * @private
     */
    RouterBase.prototype._unifyRoutes = function (routes, parentRoute) {
        let unifiedRoutes = [];
        routes.forEach(function (route) {
            unifiedRoutes = unifiedRoutes.concat(this._unifyRoute(route, parentRoute));
        }, this);
        return unifiedRoutes;
    };

    /**
     * @param {RouteDefinition|RoutesDefinition} route
     * @param {RouteDefinition} [parentRoute]
     * @returns {RouteDefinition|Array.<RouteDefinition>}
     * @private
     */
    RouterBase.prototype._unifyRoute = function (route, parentRoute) {
        parentRoute = parentRoute || {};
        route.defaults = map(merge(parentRoute.defaults, route.defaults), function (prop) {
            return String(prop);
        });
        route.requirements = merge(parentRoute.requirements, route.requirements);
        if (parentRoute.prefix) {
            route.path = parentRoute.prefix + route.path;
            route.prefix = route.prefix ? parentRoute.prefix + route.prefix : parentRoute.prefix;
        }
        route.host = route.host || parentRoute.host;
        route.methods = route.methods || parentRoute.methods || this._defaultMethods;
        route.schemes = route.schemes || parentRoute.schemes || this._defaultSchemes;
        for (const key in parentRoute) {
            if (parentRoute.hasOwnProperty(key) && key !== 'routes' && !route[key]) {
                route[key] = parentRoute[key];
            }
        }
        return route.routes ? this._unifyRoutes(route.routes, route) : route;
    };

    /**
     * @param {Array.<RouteDefinition|RoutesDefinition>} routes
     * @returns {Array.<RouteDefinitionParsed>}
     * @private
     */
    RouterBase.prototype._parseRoutes = function (routes) {
        return routes.map(this._parseRoute, this);
    };

    /**
     * @param {RouteDefinition} route
     * @param {Number} i
     * @returns {RouteDefinitionParsed}
     * @private
     */
    RouterBase.prototype._parseRoute = function (route, i) {
        const defaults = route.defaults;
        const requirements = route.requirements;
        const path = route.path;
        const host = route.host;
        const parsedHost = host ? this._parsePath(host, requirements, defaults) : this._NO_HOST;
        const parsedPath = this._parsePath(path, requirements, defaults);
        return {
            id: route.id,
            pattern: parsedPath.regexp,
            path: path,
            parameters: parsedPath.parameters,
            hostPattern: parsedHost.regexp,
            host: host,
            hostParameters: parsedHost.parameters,
            requirements: map(requirements, function (prop) {
                return new RegExp('^' + prop + '$');
            }),
            defaults: defaults,
            methods: route.methods,
            schemes: route.schemes,
            definition: this._routesDefinitions[i]
        };
    };

    /**
     * @param {String} path
     * @param {Object} [requirements]
     * @param {Object} [defaults]
     * @returns {{regexp: RegExp, parameters: Array.<String>}}
     * @private
     */
    RouterBase.prototype._parsePath = function (path, requirements, defaults) {
        const parameters = [];
        const defaultParameterRequirement = this._defaultParameterRequirement;

        const regexp = new RegExp('^' +
            path
                .replace(/([\/\.\|])/g, '\\$1')
                .replace(this._paramsReplaceRegExp, function (match, delimiter, parameterName) {
                    const optional = parameterName in defaults;
                    parameters.push(parameterName);
                    return (delimiter || '') + (optional && delimiter ? '?' : '') +
                        '(' + (requirements[parameterName] ? requirements[parameterName] : defaultParameterRequirement) + ')' +
                        (optional ? '?' : '');
                }) +
            '$');

        return {
            regexp: regexp,
            parameters: parameters
        };
    };

    /**
     * @param {{path: String, method: String, host: String|undefined, scheme: String|undefined}} request
     * @returns {?Route}
     * @public
     */
    RouterBase.prototype.match = function (request) {
        const routes = this._routes;
        let route;
        const pathParts = request.path.split('?');
        const path = pathParts[0];
        const query = pathParts[1] ? this._parseQuery(pathParts[1]) : null;
        let execPathResult;
        let execHostResult;
        let isMethodValid;
        let isSchemeValid;

        request.method = 'GET';

        for (let i = 0, l = routes.length; i < l; i++) {
            route = routes[i];
            execPathResult = route.pattern.exec(path);
            if (!execPathResult) {
                continue;
            }
            execHostResult = route.hostPattern.exec(request.host);
            if (!execHostResult) {
                continue;
            }
            isMethodValid = route.methods.includes(request.method);
            if (!isMethodValid) {
                continue;
            }
            isSchemeValid = !request.scheme || route.schemes.includes(request.scheme);
            if (isSchemeValid) {
                execPathResult.shift();
                execHostResult.shift();
                return {
                    id: route.id,
                    parameters: this._retrieveParameters(route, execPathResult, execHostResult, query),
                    definition: clone(this._routesDefinitions[i])
                };
            }
        }
        return null;
    };

    /**
     * @param {String} query
     * @returns {{}}
     * @protected
     */
    RouterBase.prototype._parseQuery = function (query) {
        const obj = {};

        if (typeof query !== 'string' || query.length === 0) {
            return obj;
        }

        const regexp = /\+/g;
        query = query.split('&');

        const len = query.length;

        for (let i = 0; i < len; ++i) {
            const x = query[i].replace(regexp, '%20');
            const idx = x.indexOf('=');
            let kstr;
            let vstr;
            let k;
            let v;

            if (idx >= 0) {
                kstr = x.substr(0, idx);
                vstr = x.substr(idx + 1);
            } else {
                kstr = x;
                vstr = '';
            }

            k = decodeURIComponent(kstr);
            v = decodeURIComponent(vstr);

            if (!obj.hasOwnProperty(k)) {
                obj[k] = v;
            } else if (Array.isArray(obj[k])) {
                obj[k].push(v);
            } else {
                obj[k] = [obj[k], v];
            }
        }

        return obj;
    };

    /**
     * @param {RouteDefinitionParsed} route
     * @param {Array.<String>} parsedParameters
     * @param {Array.<String>} parsedHostParameters
     * @param {Object.<String>} [query={}]
     * @returns {Object.<String>}
     * @private
     */
    RouterBase.prototype._retrieveParameters = function (route, parsedParameters, parsedHostParameters, query) {
        const parameters = query || {};
        for (const key in route.defaults) {
            if (route.defaults.hasOwnProperty(key)) {
                parameters[key] = route.defaults[key];
            }
        }
        this._extendParameters(parsedParameters, route.parameters, parameters);
        this._extendParameters(parsedHostParameters, route.hostParameters, parameters);
        return parameters;
    };

    /**
     * @param {Array.<String>} input parameters values
     * @param {Array.<String>} names parameters names
     * @param {Object.<String>} output parameters names: parameters values
     * @private
     */
    RouterBase.prototype._extendParameters = function (input, names, output) {
        for (let i = 0, l = names.length; i < l; i++) {
            if (input[i] === undefined) {
                break;
            }
            output[names[i]] = decodeURIComponent(input[i]);
        }
    };

    /**
     * @param {String} id
     * @param {Object.<String>} [params]
     * @returns {String}
     * @throws Error if required parameter is not given
     * @throws Error if parameter value is not suits requirements
     * @throws Error if route is not defined
     * @throws URIError
     * @public
     */
    RouterBase.prototype.generate = function (id, params) {
        const routes = this._routes;
        let route;
        for (let i = 0, l = routes.length; i < l; i++) {
            route = routes[i];
            if (route.id === id) {
                return this._generate(route, merge({}, params));
            }
        }
        throw new Error('No such route: ' + id);
    };

    /**
     * @param {RouteDefinitionParsed} route
     * @param {Object.<String>} [params]
     * @returns {String}
     * @throws Error if required parameter is not given
     * @throws Error if parameter value is not suits requirements
     * @private
     */
    RouterBase.prototype._generate = function (route, params) {
        const path = route.path;
        let generatedPath;
        const host = route.host;
        let generatedHost;
        const defaults = route.defaults;
        const requirements = route.requirements;
        const _this = this;
        generatedPath = path.replace(this._paramsReplaceRegExp, function (match, delimiter, parameterName) {
            delimiter = delimiter || '';
            const optional = parameterName in defaults;
            const exists = params && parameterName in params;
            if (exists) {
                return _this._getParameterValue(parameterName, params, requirements, delimiter);
            } else if (!optional) {
                _this._throwParameterNeededError(parameterName, route.id);
            }

            let hasFilledParams = false;
            for (const key in params) {
                if (params.hasOwnProperty(key)) {
                    if (route.parameters.includes(key)) {
                        hasFilledParams = true;
                        break;
                    }
                }
            }

            return hasFilledParams ? delimiter + defaults[parameterName] : '';
        });

        if (host) {
            generatedHost = host.replace(this._paramsReplaceRegExp, function (match, delimiter, parameterName) {
                const optional = parameterName in defaults;
                const exists = params && parameterName in params;
                delimiter = delimiter || '';
                if (exists) {
                    return _this._getParameterValue(parameterName, params, requirements, delimiter);
                } else if (optional) {
                    return delimiter + defaults[parameterName];
                } else {
                    _this._throwParameterNeededError(parameterName, route.id);
                }
            });
        }

        const query = this._generateQuery(params);

        if (query) {
            generatedPath += '?' + query;
        }

        return generatedHost ? route.schemes[0] + '://' + generatedHost + generatedPath : generatedPath;
    };

    /**
     * @param {Object.<String>} params
     * @returns {String}
     * @protected
     */
    RouterBase.prototype._generateQuery = function (params) {
        let query = '';
        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                query += (query.length ? '&' : '') + encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
            }
        }
        return query;
    };

    /**
     * @param {String} parameterName
     * @param {Object.<String>} params
     * @param {Object.<RegExp>} requirements
     * @param {String} delimiter
     * @returns {String}
     * @throws Error if parameter value is invalid
     * @private
     */
    RouterBase.prototype._getParameterValue = function (parameterName, params, requirements, delimiter) {
        const value = params[parameterName];
        delete params[parameterName];
        if (requirements[parameterName] && !requirements[parameterName].test(value)) {
            throw new Error('Parameter "' + parameterName + '" has bad value "' + value + '", not suitable for path generation');
        }
        return delimiter + encodeURIComponent(value);
    };

    /**
     * @param {String} parameterName
     * @param {String} routeId
     * @private
     */
    RouterBase.prototype._throwParameterNeededError = function (parameterName, routeId) {
        throw new Error('Parameter "' + parameterName + '" is needed for route "' + routeId + '" generation');
    };

    /**
     * @param {String} routeId
     * @returns {?RouteDefinitionParsed}
     * @public
     * @throws if route is not defined
     */
    RouterBase.prototype.getRouteInfo = function (routeId) {
        if (!this._routeIdToParsedDefinitionMap[routeId]) {
            for (let i = 0, l = this._routes.length; i < l; i++) {
                if (this._routes[i].id === routeId) {
                    this._routeIdToParsedDefinitionMap[routeId] = this._routes[i];
                    break;
                }
            }
        }

        if (!this._routeIdToParsedDefinitionMap[routeId]) {
            throw new Error('No such route: ' + routeId);
        }

        return clone(this._routeIdToParsedDefinitionMap[routeId]);
    };

    return RouterBase;
});