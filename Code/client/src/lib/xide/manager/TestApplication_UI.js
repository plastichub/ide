define([
    'dcl/dcl',
    'xide/utils',
    'xide/types',
    'xace/views/Editor',
    'dojo/has',
    'xide/layout/_Accordion',
    'xide/widgets/WidgetBase',
    "xcf/views/_MainView",
    "xide/mixins/PersistenceMixin",
    'dojo/Deferred'
], function (dcl, utils,
             types, Editor, has, _Accordion,
             WidgetBase, _MainView, PersistenceMixin, Deferred) {


    const Persistence = dcl([PersistenceMixin.dcl], {

        declaredClass: 'xcf.manager.ApplicationPersistence',
        defaultPrefenceTheme: 'idle_fingers',
        defaultPrefenceFontSize: 14,
        saveValueInPreferences: true,
        cookiePrefix: '_xcf_application',
        getDefaultPreferences: function () {
            return utils.mixin(
                {
                    theme: this.defaultPrefenceTheme,
                    fontSize: this.defaultPrefenceFontSize
                },
                this.saveValueInPreferences ? {} : null);

        },
        onAfterAction: function (action) {
            //var _theme = this.getEditor().getTheme();
            this.savePreferences({
                /*
                 theme: _theme.replace('ace/theme/', ''),
                 fontSize: this.getEditor().getFontSize()
                 */
            });
            return this.inherited(arguments);
        },
        /**
         * Override id for pref store:
         * know factors:
         *
         * - IDE theme
         * - per bean description and context
         * - by container class string
         * - app / plugins | product / package or whatever this got into
         * -
         **/
        toPreferenceId: function (prefix) {
            prefix = '';
            return (prefix || this.cookiePrefix || '') + '_xcf_application';
        },
        getDefaultOptions: function () {
            //take our defaults, then mix with prefs from store,
            const _super = this.inherited(arguments);

            const _prefs = this.loadPreferences(null);

            (_prefs && utils.mixin(_super, _prefs) ||
            //else store defaults
            this.savePreferences(this.getDefaultPreferences()));
            return _super;
        }
    });

    return dcl(Persistence.dcl, {
        declaredClass: "xide.manager.TestApplication_UI",
        mainView: null,
        leftLayoutContainer: null,
        rightLayoutContainer: null,
        showGUI: true,
        showFiles: true,
        lastPane: null,
        _lastWizard: null,
        _maximized: false,
        grids: null,
        driverTreeView: null,
        createAppActions: function () {
            return [];
        },
        /***
         * Register custom types
         */
        registerCustomTypes: function () {



        },
        /**
         *
         * @param clear
         * @param open
         * @returns {*}
         */
        onMainViewReady: function (view) {
        },
        /**
         * Main entry point, does:
         *  - create the main view
         *  - registers custom types and enumerations
         *  - loads data and adds data views
         *  - initializes XFile
         */
        start: function (showGUI, rootSelector, args) {

            const thiz = this;

            this.args = args;
            this.showGUI = showGUI;
            const ACTION = types.ACTION;
            const container = $(rootSelector || '#root')[0];
            let permissons = [
                //ACTION.BREADCRUMB,
                //ACTION.RIBBON,
                ///ACTION.MAIN_MENU,
                //ACTION.NAVIGATION,
                //ACTION.STATUSBAR,
                //ACTION.TOOLBAR,
                //ACTION.WELCOME
            ];

            let isEditor = false;
            if (has('electronx') && args) {
                if (args.file) {
                    permissons = [
                        ACTION.MAIN_MENU
                    ];
                    isEditor = true;
                }
            }

            const view = utils.addWidget(_MainView, {
                ctx: this.ctx,
                permissions: permissons,
                config: this.config,
                windowManager: this.ctx.getWindowManager(),
                container: container
            }, null, container, true);

            this.mainView = view;
            this.ctx.mainView = this.mainView;
            const leftContainer = view.layoutLeft;
            if (!isEditor) {

            } else {
                this.mainView.layoutCenter = this.mainView.layoutLeft;
            }


            const self = this;
            this.doComponents().then(function () {
                self.registerCustomTypes();
                self.onComponentsReady();
                if (isEditor) {
                    self.openFile(args);
                } else {
                    self.initData();
                    self.onAppCreated();
                    self.subscribe(types.EVENTS.ON_DEVICE_SERVER_CONNECTED, function () {
                        if (self.args.userDirectory) {
                            self.ctx.getDeviceManager().watchDirectory(self.args.userDirectory, true);
                        }
                    });
                }
                view._resize();
            });
        },
        onAppCreated: function () {
        },
        /**
         * Register new editors for xfile
         */
        registerEditorExtensions: function () {

            /*
            var ctx = this.ctx,
                thiz = this;

            Default.Implementation.ctx = ctx;
            Default.ctx = ctx;

            //sample default text editor open function, not needed
            var editInACE = function (item, owner) {
                var where = null;
                if (owner) {
                    //where = owner ? owner.newTarget : null;
                    if (_.isFunction(owner.newTarget)) {
                        where = owner.newTarget({
                            title: item.name,
                            icon: 'fa-code'
                        });
                    }
                }
                return Default.Implementation.open(item, where, null, null, owner);
            };
            ctx.registerEditorExtension('Default Editor', '*', 'fa-code', this, false, editInACE, ACEEditor, {
                updateOnSelection: false,
                leftLayoutContainer: this.leftLayoutContainer,
                ctx: ctx,
                defaultEditor: true
            });
            ctx.registerEditorExtension('JSON Editor', 'json', 'fa-code', this, true, null, JSONEditor, {
                updateOnSelection: false,
                leftLayoutContainer: this.leftLayoutContainer,
                ctx: ctx,
                registerView: true
            });
            types.registerCustomMimeIconExtension('cfhtml', 'fa-laptop');
            */
        },
        init: function () {

            this.ctx.addActions(this.getActions());
            this.ctx.addActions(this.getThemeActions());
            WidgetBase.prototype.ctx = this.ctx;
            const thiz = this;

            this.subscribe(types.EVENTS.ON_EXPRESSION_EDITOR_ADD_FUNCTIONS, this.onExpressionEditorAddFunctions);
            if (has('consoleError') && location.href.includes('consoleError')) {
                const console = (window.console = window.console || {});
                function consoleError(string) {
                    thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                        text: string,
                        type: 'error'
                    });
                }
                console.error = consoleError;
            }
        },
        getLayoutRightMain: function (clear, open) {
            const mainView = this.mainView;
            return mainView.getLayoutRightMain(clear, open);
        },
        getRightTopTarget: function (clear, open) {
            const mainView = this.mainView;
            return mainView.getRightTopTarget(clear, open);
        },
        onExpressionEditorAddFunctions: function (evt) {
            const widget = evt.widget;
            const root = evt.root;
            if (!widget || !root) {
                return;
            }
            const device = widget.device;
            if (!device) {
                return;
            }
            //var driverInstance = device.driverInstance;
            const _Variables = widget.createBranch('Variables', root.items);

            root.items.push(widget.createLeaf('getVariable', 'this.getVariable(\'variableName\')', 'Variables', 'gets a variable', true, _Variables));
            root.items.push(widget.createLeaf('setVariable', 'this.setVariable(\'variableName\')', 'Variables', 'sets a variable by name', true, _Variables));
        },
        initWidgets: function () {
            //factory.showStandBy(false);
        },
        initElectron: function () {
            document.addEventListener('dragover', function (e) {
                e.preventDefault();
                e.stopPropagation();
            });
            const self = this;
            document.addEventListener('drop', function (event) {
                event.preventDefault();
                const pathsToOpen = Array.prototype.map.call(event.dataTransfer.files, function (file) {
                    return file.path;
                });
                if (pathsToOpen.length > 0) {

                    self.openFile({
                        file: pathsToOpen[0]
                    });
                }
                return false;
            }, false);

        },
        onComponentsReady: function () {
            const thiz = this;
            const ctx = thiz.ctx;
            const windowManager = ctx.getWindowManager();
            const mainView = ctx.mainView;
            const left = mainView.layoutLeft;
            const container = thiz.leftLayoutContainer;


            Editor.ctx = thiz.ctx;
            Editor.prototype.ctx = thiz.ctx;
            $('#loadingWrapper').remove();
            if (has('electronx')) {
                this.initElectron();
            }
        },
        _saveNavigationState: function () {

            const settingsStore = this.ctx.getSettingsManager().getStore() || {
                    getSync: function () {
                    }
                };

            let state = {};
            if (settingsStore) {
                const props = settingsStore.getSync('navigationView');
                if (props && props.value) {
                    state = props.value;
                }
            }

            if (this.fileGrid) {
                state['files'] = !!this.fileGrid.open;
            }
            this.ctx.getSettingsManager().write2(null, '.', {
                    id: 'navigationView'
                }, {
                    value: state
                }, true, null
            );
        },
        getNavigationState: function () {
            const settingsStore = this.ctx.getSettingsManager().getStore() ||
                {
                    getSync: function () {}
                };

            let state = {
                devices: true,
                drivers: false,
                files: false
            };
            if (settingsStore) {
                const props = settingsStore.getSync('navigationView');
                if (props && props.value) {
                    state = props.value;
                }
            }
            return state;
        },
        /**
         * Function to register a grid in the navigation accordion
         * @param grid
         */
        addNavigationGrid: function (grid) {

            !this.grids && (this.grids = []);
            this.grids.push(grid);
            const thiz = this;

            grid._on('bounced', function (evt) {
                const accordion = thiz.leftLayoutContainer;
                const _parent = this._parent;
                const tabs = accordion._widgets;

                tabs.forEach((tab, i) => {
                    if ($.contains(tab.containerNode, grid.domNode)) {
                        const nextTab = tabs[i + evt.direction];
                        if (nextTab) {
                            const open = nextTab.open;
                            !open && nextTab.show();
                            setTimeout(function () {
                                nextTab.resize();
                                if (nextTab._widgets) {
                                    const nextGrid = nextTab._widgets[0];
                                    nextGrid.focus();

                                    const rows = nextGrid.getRows();
                                    const toSelect = evt.direction == 1 ? 0 : rows[rows.length - 1];
                                    nextGrid.select([toSelect], null, true, {
                                        append: false,
                                        clear: true,
                                        focus: true,
                                        delay: 10
                                    });
                                    nextGrid.resize();
                                    nextGrid._emit('onViewShow', nextGrid);
                                }
                            }, open ? 0 : 800);
                        }
                    }
                });
            });
        },
        initData: function () {
            const thiz = this;
            const ctx = thiz.ctx;
            this.grids = [];
            const dfd = new Deferred();
            Editor.ctx = thiz.ctx;
            Editor.prototype.ctx = thiz.ctx;
            $('#loadingWrapper').remove();
            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                text: "Load data, please wait",
                timeout: 8000
            });
            dfd.resolve();
            this.ctx.mainView.resize();
            this.ctx.mainView.layoutLeft && this.ctx.mainView.layoutLeft.__update();
            return dfd;
        }
    });
});
