define([
    'dcl/dcl',
    'xide/manager/Application',
    'xdojo/has',
    'xide/manager/TestApplication_UI'
], function (dcl,Application,has,TestApplication_UI) {
    const bases =  has('host-browser') ? [Application, TestApplication_UI] : Application;
    return dcl(bases, {
        declaredClass:"xide.manager.TestApplication",
        vfsItems: null
    });
});
