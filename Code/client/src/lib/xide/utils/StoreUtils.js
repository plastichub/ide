define([
    'xide/utils',
    'xide/data/Memory',
    'dojo/_base/kernel',
    'xide/lodash'
], function (utils, Memory,dojo,lodash) {
    "use strict";
    /**
     *
     * @param store {module:xide/data/_Base|null}
     * @param mixed {object|string|null}
     * @param recursive {boolean|null}
     * @param idAttribute {string|null}
     * @param parentAttr {string|null}
     * @param destroy {boolean|null}
     */
    utils.removeFromStore = function (store, mixed, recursive, idAttribute, parentAttr,silent,destroy) {
        if(mixed==null || !store){
            return;
        }
        mixed = lodash.isString(mixed) ? store.getSync(mixed) || mixed : mixed;
        idAttribute = idAttribute || store.idProperty;
        parentAttr = parentAttr || store.parentProperty;
        //remove the item itself
        mixed && store.removeSync(mixed[idAttribute],silent);
        //remove children recursively
        const query = {};
        query[parentAttr] = mixed[idAttribute] ? mixed[idAttribute] : mixed;
        destroy ===true && lodash.isObject(mixed) && utils.destroy(mixed,true);
        if(recursive===true) {
            const items = store.query(query);
            if (items && items.length) {
                for (let i = 0; i < items.length; i++) {
                    utils.removeFromStore(store, items[i], recursive, idAttribute, parentAttr,silent,destroy);
                }
            }
        }
    };
    /**
     * CI related tools.
     * @param val {string|array|null}
     * @returns {string|null}
     */
    utils.toString = function (val) {
        if (val != null) {
            if (!dojo.isArray(val)) {
                return '' + val;
            }
            if (val && val.length == 1 && val[0] == null) {
                return null;
            }
            return '' + (val[0] != null ? val[0] : val);
        }
        return null;
    };
    utils.toBoolean = function (data) {
        let resInt = false;
        if (data != null) {
            const _dataStr = data[0] ? data[0] : data;
            if (_dataStr != null) {
                resInt = !!(( _dataStr === true || _dataStr === 'true' || _dataStr === '1'));
            }
        }
        return resInt;
    };
    utils.toObject = function (data) {
        if (data != null) {
            return data[0] ? data[0] : data;
        }
        return null;
    };
    utils.toInt = function (data) {
        if(_.isNumber(data)){
            return data;
        }
        let resInt = -1;
        if (data!=null) {
            const _dataStr = data.length > 1 ? data : data[0] ? data[0] : data;
            if (_dataStr != null) {
                resInt = parseInt(_dataStr, 10);
            }
        }
        return resInt;
    };
    /***
     *
     * @param store
     * @param id
     * @return {null}
     */
    utils.getStoreItemById = function (store, id) {
        return utils.queryStoreEx(store, {id: id},null,null);
    };
    /***
     *
     * @param store
     * @param id
     * @param type
     * @return {null}
     */
    utils.getAppDataElementByIdAndType = function (store, id, type) {
        return utils.queryStore(store, {uid: id, type: type},null,null);
    };
    /***
     *
     * @param store
     * @param type
     * @return {null}
     */
    utils.getElementsByType = function (store, type) {
        return utils.queryStoreEx(store, {type: type});
    };
    /***
     * @param store {module:xide/data/_Base} Store to query
     * @param query {object} Literal to match
     * @param nullEmpty {boolean|null} Return null if nothing has been found
     * @param single {boolean|null} Return first entry
     * @returns {*}
     */
    utils.queryStoreEx = function (store, query, nullEmpty, single) {
        if (!store) {
            console.error('utils.queryStoreEx: store = null');
            return null;
        }
        if (store instanceof Memory) {
            const result = utils.queryMemoryStoreEx(store, query);
            if (single && result && result[0]) {
                return result[0];
            }
            return result;
        }
        let res = null;
        if (store.query) {
            res = store.query(query);
        }
        if (nullEmpty === true) {
            if (res && res.length === 0) {
                return null;
            }
        }
        if (single === true) {
            if (res && res.length == 1) {
                return res[0];
            }
        }
        return res;
    };
    /**
     *
     * @param store
     * @param query
     * @param nullEmpty {boolean|null}
     * @returns {*}
     */
    utils.queryStore = function (store, query, nullEmpty) {
        const res = utils.queryStoreEx(store, query,null,null);
        if (res && res.length == 1) {
            return res[0];
        }
        if (nullEmpty === true) {
            if (res && res.length === 0) {
                return null;
            }
        }
        return res;
    };

    /**
     *
     * @param store
     * @param query
     * @returns {Array}
     */
    utils.queryMemoryStoreEx = function (store, query) {
        const result = [];
        store.query(query).forEach(function (entry) {
            result.push(entry);
        });
        return result;
    };

    utils.queryMemoryStoreSingle = function (store, query) {
        const result = utils.queryMemoryStoreEx(store, query);
        if (result.length == 1) {
            return result[0];
        }
        return result;
    };

    return utils;
});