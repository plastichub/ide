/** @module xide/utils/StringUtils
 *  @description All string related functions
 */
define([
    'xide/utils',
    'xide/types',
    'dojo/json',
    'xide/lodash'
], function (utils, types, json, _) {
    "use strict";

    /**
     *
     * @param replacer
     * @param cycleReplacer
     * @returns {Function}
     */
    function serializer(replacer, cycleReplacer) {
        const stack = [];
        const keys = [];

        if (cycleReplacer == null) cycleReplacer = function (key, value) {
            if (stack[0] === value) return "[Circular ~]";
            return "[Circular ~." + keys.slice(0, stack.indexOf(value)).join(".") + "]"
        };

        return function (key, value) {
            if (stack.length > 0) {
                const thisPos = stack.indexOf(this);
                ~thisPos ? stack.splice(thisPos + 1) : stack.push(this);
                ~thisPos ? keys.splice(thisPos, Infinity, key) : keys.push(key);
                if (~stack.indexOf(value)) value = cycleReplacer.call(this, key, value)
            }
            else stack.push(value);

            return replacer == null ? value : replacer.call(this, key, value)
        };
    }

    /**
     *
     * @param obj
     * @returns {*}
     */
    utils.stringify = function (obj) {
        return JSON.stringify(obj, serializer(), 2);
    };

    function stringify(obj, replacer, spaces, cycleReplacer) {
        return JSON.stringify(obj, serializer(replacer, cycleReplacer), spaces)
    }

    /**
     * Takes a number and returns a rounded fixed digit string.
     * Returns an empty string if first parameter is NaN, (-)Infinity or not of type number.
     * If parameter trailing is set to true trailing zeros will be kept.
     *
     * @param {number} num the number
     * @param {number} [digits=3] digit count
     * @param {boolean} [trailing=false] keep trailing zeros
     * @memberOf module:xide/utils/StringUtils
     *
     * @example
     *
     test(fsuxx(-6.8999999999999995), '-6.9');
     test(fsuxx(0.020000000000000004), '0.02');
     test(fsuxx(0.199000000000000004), '0.199');
     test(fsuxx(0.199000000000000004, 2), '0.2');
     test(fsuxx(0.199000000000000004, 1), '0.2');
     test(fsuxx(0.199000000000000004, 2, true), '0.20');
     test(fsuxx('muh'), '');
     test(fsuxx(false), '');
     test(fsuxx(null), '');
     test(fsuxx(), '');
     test(fsuxx(NaN), '');
     test(fsuxx(Infinity), '');
     test(fsuxx({bla: 'blub'}), '');
     test(fsuxx([1,2,3]), '');
     test(fsuxx(6.8999999999999995), '6.9');
     test(fsuxx(0.199000000000000004), '0.199');
     test(fsuxx(0.199000000000000004, 2), '0.2');
     test(fsuxx(0.199000000000000004, 2, true), '0.20');
     *
     *
     * @returns {string}
     *
     */
    utils.round = function (num, digits, trailing) {

        if (typeof num !== 'number' || isNaN(num) || num === Infinity || num === -Infinity) return '';

        digits = ((typeof digits === 'undefined') ? 3 : (parseInt(digits, 10) || 0));

        const f = Math.pow(10, digits);
        let res = (Math.round(num * f) / f).toFixed(digits);

        // remove trailing zeros and cast back to string
        if (!trailing) res = '' + (+res);

        return res;
    };




    /**
     *
     * @param bytes
     * @param si
     * @returns {string}
     */
    utils.humanFileSize = function (bytes, si) {
        const thresh = si ? 1000 : 1024;
        if (Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }
        const units = si
            ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
            : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
        let u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while (Math.abs(bytes) >= thresh && u < units.length - 1);
        return bytes.toFixed(1) + ' ' + units[u];
    };

    if (typeof String.prototype.startsWith != 'function') {
        // see below for better implementation!
        String.prototype.startsWith = function (str) {
            return this.indexOf(str) === 0;
        };
    }

    if ( typeof String.prototype.endsWith != 'function' ) {
        String.prototype.endsWith = function( str ) {
            return this.substring( this.length - str.length, this.length ) === str;
        }
    }

    /**
     *
     * @param str
     * @returns {boolean}
     */
    utils.isNativeEvent = function (str) {
        const //just for having an optimized object map for a native event lookup below
        _foo = null;

        let _nativeEvents = {
            "onclick": _foo,
            "ondblclick": _foo,
            "onmousedown": _foo,
            "onmouseup": _foo,
            "onmouseover": _foo,
            "onmousemove": _foo,
            "onmouseout": _foo,
            "onkeypress": _foo,
            "onkeydown": _foo,
            "onkeyup": _foo,
            "onfocus": _foo,
            "onblur": _foo,
            "onchange": _foo
        };

        if (str in _nativeEvents) {
            return true;
        }
        _nativeEvents = {
            "click": _foo,
            "dblclick": _foo,
            "mousedown": _foo,
            "mouseup": _foo,
            "mouseover": _foo,
            "mousemove": _foo,
            "mouseout": _foo,
            "keypress": _foo,
            "keydown": _foo,
            "keyup": _foo,
            "focus": _foo,
            "blur": _foo,
            "change": _foo
        };

        return str in _nativeEvents;
    };
    /**
     *
     * @param str
     * @returns {boolean}
     *
     * @memberOf module:xide/utils/StringUtils
     */
    utils.isSystemEvent = function (str) {
        for (const t in types.EVENTS) {
            if (types.EVENTS[t] === str) {
                return true;
            }
        }
        return false;
    };

    /**
     *
     * @param arr
     * @param val
     * @returns {number}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.contains = function (arr, val) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === val) {
                return i;
            }
        }
        return -1;
    };
    /**
     *
     * @param obj
     * @param val
     * @returns {*}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.getObjectKeyByValue = function (obj, val) {
        if (obj && val) {
            for (const prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    if (obj[prop] === val)
                        return prop;
                }
            }
        }
        return null;
    };

    /**
     *
     * @param url
     * @param parameter
     * @returns {*}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.removeURLParameter = function (url, parameter) {
        //prefer to use l.search if you have a location/link object
        const urlparts = url.split('?');
        if (urlparts.length >= 2) {

            const prefix = encodeURIComponent(parameter) + '=';
            const pars = urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (let i = pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + '?' + pars.join('&');
            return url;
        } else {
            return url;
        }
    };

    /**
     *
     * @param url
     * @param paramName
     * @param paramValue
     * @returns {*}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.replaceUrlParam = function (url, paramName, paramValue) {
        if (url.indexOf(paramName) == -1) {
            url += (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue;
            return url;
        }
        const pattern = new RegExp('(' + paramName + '=).*?(&|$)');
        let newUrl = url.replace(pattern, '$1' + paramValue + '$2');
        if (newUrl == url) {
            newUrl = newUrl + (newUrl.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
        }
        return newUrl
    };

    /**
     *
     * @param mount
     * @param path
     * @param encode
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.buildPath = function (mount, path, encode) {

        //fix mount
        let _mount = '' + mount;
        _mount = utils.replaceAll('/', '', mount);
        let _path = '' + path;
        _path = _path.replace('./', '/').replace(/^\/|\/$/g, '');

        const _res = _mount + '://' + _path;
        if (encode === true) {
            return encodeURIComponent(_res);
        }
        return _res;
    };

    /**
     *
     * @param string
     * @returns {boolean}
     * @memberOf module:xide/utils/StringUtils
     *
     */
    utils.isImage = function (string) {
        return string.toLowerCase().match(/\.(jpeg|jpg|gif|png)$/) != null;
    };

    /**
     *
     * @param field
     * @param enumValue
     * @returns {boolean}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.hasFlag3 = function (field, enumValue) {
        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage,JSBitwiseOperatorUsage,JSBitwiseOperatorUsage,JSBitwiseOperatorUsage,JSBitwiseOperatorUsage,JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        return ((1 << enumValue) & field) ? true : false;
    };

    /**
     *
     * @param field
     * @param enumValue
     * @returns {boolean}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.hasFlag = function (field, enumValue) {
        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        return ((1 << enumValue) & field) ? true : false;
    };

    /**
     *
     * @param enumValue
     * @param field
     * @returns {int|*}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.disableFlag = function (enumValue, field) {
        enumValue &= ~(1 << field);
        return enumValue;
    };
    /**
     * XApp specific url string cleaner
     * @param string
     * @returns {*}
     */
    utils.cleanUrl = function (string) {
        if (string) {
            string = string.replace('//', '/');
            string = string.replace('./', '/');
            string = string.replace('http:/', 'http://');
            string = string.replace('./', '/');
            string = string.replace('////', '/');
            string = string.replace('///', '/');
            return string;
        }
        return string;
    };
    /**
     * Return data from JSON
     * @param inData
     * @param validOnly
     * @param imit
     * @memberOf module:xide/utils/StringUtils
     * @returns {*}
     */
    utils.getJson = function (inData, validOnly, ommit) {
        try {
            return _.isString(inData) ? json.parse(inData, false) : validOnly === true ? null : inData;
        } catch (e) {
            ommit !== false && console.error('error parsing json data ' + inData + ' error = ' + e);
        }
        return null;
    };

    /**
     * Hard Dojo override to catch malformed JSON.
     * @param js
     * @returns {*}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.fromJson = function (js) {
        if (!_.isString(js)) {
            return js;
        }
        let res = null;
        let didFail = false;
        try {
            res = eval("(" + js + ")", {});
        } catch (e) {
            didFail = true;
        }
        if (didFail) {
            js = js.substring(js.indexOf('{'), js.lastIndexOf('}') + 1);
            try {
                res = eval("(" + js + ")", {});
            } catch (e) {
                throw new Error(js);
            }
        }
        return res;
    };

    /**
     * String Replace which works with multiple found items. Native aborts on the first needle.
     * @param find
     * @param replace
     * @param str
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.replaceAll = function (find, replace, str) {
        return str ? str.split(find).join(replace) : '';
    };

    /**
     * CI compatible string check for null and length>0
     * @param input
     * @returns {boolean}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.isValidString = function (input) {
        return input != null && input.length != null && input.length > 0 && input != "undefined"; //Boolean
    };

    /**
     * Dojo style template replacer
     * @param template
     * @param obj
     * @returns {*}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.substituteString = function (template, obj) {
        return template.replace(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g, function (match, key) {
            return obj[key];
        });
    };

    /**
     *
     * @param expression
     * @param delimiters
     * @returns {*}
     * @private
     * @memberOf module:xide/utils/StringUtils
     */
    utils.findOcurrences = function (expression, delimiters) {
        const d = {
            begin: utils.escapeRegExp(delimiters.begin),
            end: utils.escapeRegExp(delimiters.end)
        };
        return expression.match(new RegExp(d.begin + "([^" + d.end + "]*)" + d.end, 'g'));
    };

    /**
     * Escape regular expressions in a string
     * @param string
     * @returns {*}
     * @private
     * @memberOf module:xide/utils/StringUtils
     */
    utils.escapeRegExp = function (string) {
        const special = ["[", "]", "(", ")", "{", "}", "*", "+", ".", "|", "||"];
        for (let n = 0; n < special.length; n++) {
            string = string.replace(special[n], "\\" + special[n]);
        }

        return string;
    };
    /**
     *
     * @param str {string} haystack
     * @param hash {Object}
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.multipleReplace = function (str, hash) {
        //to array
        const a = [];
        for (const key in hash) {
            a[a.length] = key;
        }
        return str.replace(new RegExp(a.join('\\b|\\b'), 'g'), function (m) {
            return hash[m] || hash["\\" + m];
        });
    };

    /**
     * Flexible replacer, supports multiple replace and safe replace
     *
     * @param str {string} the haystack

     * @param needle {string|null} optional, only needed for simple cases, otherwise its using the 'what' map
     *
     * @param what {string|Object}. When string, its replacing 'needle' with 'what'. If its a hash-map:
     * variable:value, its replacing occurrences of all variables in 'haystack'. In such case, you can specify
     * delimiters to make sure that 'unresolved' variables will be stripped of in the result.
     *
     * @param delimiters {Object} Delimiters to identify variables. This is used to eliminate unresolved variables from
     * the result.
     *
     * @param delimiters.begin {string}
     * @param delimiters.end {string}
     *
     * @returns {string}
     *
     *
     * @example:
     *
     * 1. simple case: replace all '/' with ''
     *
     * return utils.replace('/foo/','/','') //returns 'foo'
     *
     * 2. simple case with multiple variables:
     *
     * return utils.replace('darling, i miss you so much',null,{'miss':'kiss','much':'little'})
     * # darling, i kiss you so little
     *
     * @memberOf module:xide/utils
     * @extends xide/utils
     */
    utils.replace = function (str, needle, what, delimiters) {
        if (!str) {
            return '';
        }
        if (what && _.isObject(what) || _.isArray(what)) {
            if (delimiters) {
                const ocurr = utils.findOcurrences(str, delimiters);
                const replaceAll = utils.replaceAll;
                if (ocurr) {

                    for (let i = 0, j = ocurr.length; i < j; i++) {
                        const el = ocurr[i];

                        //strip off delimiters
                        let _variableName = replaceAll(delimiters.begin, '', el);
                        _variableName = replaceAll(delimiters.end, '', _variableName);
                        str = replaceAll(el, what[_variableName], str);
                    }
                } else {
                    return str;
                }
            } else {
                //fast case
                return utils.multipleReplace(str, what)
            }
            return str;
        }
        //fast case
        return utils.replaceAll(needle, what, str);
    };

    /**
     * Capitalize
     * @param word
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.capitalize = function (word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    };

    /**
     * vsprintf impl. of PHP
     * @param format
     * @param args
     * @example
     // example 1: vsprintf('%04d-%02d-%02d', [1988, 8, 1]);
     // returns 1: '1988-08-01'
     * @memberOf module:xide/utils/StringUtils
     */
    utils.vsprintf = function (format, args) {
        return utils.sprintf.apply(this, [format].concat(args));
    };
    /**
     * PHP.js version of sprintf
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     * @link http://kevin.vanzonneveld.net
     */
    utils.sprintf = function () {
        const regex = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
        const a = arguments;
        let i = 0;
        const format = a[i++];

        // pad()
        const pad = function (str, len, chr, leftJustify) {
            if (!chr) {
                chr = ' ';
            }
            const padding = (str.length >= len) ? '' : Array(1 + len - str.length >>> 0).join(chr);
            return leftJustify ? str + padding : padding + str;
        };

        // justify()
        const justify = function (value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
            const diff = minWidth - value.length;
            if (diff > 0) {
                if (leftJustify || !zeroPad) {
                    value = pad(value, minWidth, customPadChar, leftJustify);
                } else {
                    value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
                }
            }
            return value;
        };

        // formatBaseX()
        const formatBaseX = function (value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
            // Note: casts negative numbers to positive ones
            const number = value >>> 0;
            prefix = prefix && number && {
                    '2': '0b',
                    '8': '0',
                    '16': '0x'
                }[base] || '';
            value = prefix + pad(number.toString(base), precision || 0, '0', false);
            return justify(value, prefix, leftJustify, minWidth, zeroPad);
        };

        // formatString()
        const formatString = function (value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
            if (precision != null) {
                value = value.slice(0, precision);
            }
            return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
        };

        // doFormat()
        const doFormat = function (substring, valueIndex, flags, minWidth, _, precision, type) {
            let number;
            let prefix;
            let method;
            let textTransform;
            let value;

            if (substring === '%%') {
                return '%';
            }

            // parse flags
            let leftJustify = false;

            let positivePrefix = '';
            let zeroPad = false;
            let prefixBaseX = false;
            let customPadChar = ' ';
            const flagsl = flags.length;
            for (let j = 0; flags && j < flagsl; j++) {
                switch (flags.charAt(j)) {
                    case ' ':
                        positivePrefix = ' ';
                        break;
                    case '+':
                        positivePrefix = '+';
                        break;
                    case '-':
                        leftJustify = true;
                        break;
                    case "'":
                        customPadChar = flags.charAt(j + 1);
                        break;
                    case '0':
                        zeroPad = true;
                        break;
                    case '#':
                        prefixBaseX = true;
                        break;
                }
            }

            // parameters may be null, undefined, empty-string or real valued
            // we want to ignore null, undefined and empty-string values
            if (!minWidth) {
                minWidth = 0;
            } else if (minWidth === '*') {
                minWidth = +a[i++];
            } else if (minWidth.charAt(0) == '*') {
                minWidth = +a[minWidth.slice(1, -1)];
            } else {
                minWidth = +minWidth;
            }

            // Note: undocumented perl feature:
            if (minWidth < 0) {
                minWidth = -minWidth;
                leftJustify = true;
            }

            if (!isFinite(minWidth)) {
                throw new Error('sprintf: (minimum-)width must be finite');
            }

            if (!precision) {
                precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
            } else if (precision === '*') {
                precision = +a[i++];
            } else if (precision.charAt(0) == '*') {
                precision = +a[precision.slice(1, -1)];
            } else {
                precision = +precision;
            }

            // grab value using valueIndex if required?
            value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

            switch (type) {
                case 's':
                    return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
                case 'c':
                    return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
                case 'b':
                    return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'o':
                    return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'x':
                    return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'X':
                    return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad).toUpperCase();
                case 'u':
                    return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'i':
                case 'd':
                    number = +value || 0;
                    number = Math.round(number - number % 1); // Plain Math.round doesn't just truncate
                    prefix = number < 0 ? '-' : positivePrefix;
                    value = prefix + pad(String(Math.abs(number)), precision, '0', false);
                    return justify(value, prefix, leftJustify, minWidth, zeroPad);
                case 'e':
                case 'E':
                case 'f': // Should handle locales (as per setlocale)
                case 'F':
                case 'g':
                case 'G':
                    number = +value;
                    prefix = number < 0 ? '-' : positivePrefix;
                    method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
                    textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
                    value = prefix + Math.abs(number)[method](precision);
                    return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
                default:
                    return substring;
            }
        };

        return format.replace(regex, doFormat);
    };
    /***
     *
     * @param str
     * @returns {string | null}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.cleanString = function (str) {
        if (!str) {
            return null;
        }
        str = str.replace(/[\r]/g, '')
            .replace(/[\b]/g, '')
            .replace(/[\f]/g, '')
            .replace(/[\n]/g, '')
            .replace(/\\/g, '');
        return str;
    };
    /***
     *
     * @param str {string}
     * @returns {string | null}
     */
    utils.normalizePath = function (str) {
        if (!str) {
            return null;
        }
        str = utils.cleanString(str);//control characters
        str = str.replace('./', '');//file store specifics
        str = str.replace(/([^:]\/)\/+/g, "$1");//double slashes
        return str;
    };

    /**
     *
     * @enum
     * @global
     * @memberOf module:xide/types
     */
    types.PATH_PARTS = {
        'DIRNAME': 1,
        'BASENAME': 2,
        'EXTENSION': 4,
        'FILENAME': 8,
        'PATHINFO_ALL': 0
    };
    /**
     * PHP.js version of basename
     * @param path {string}
     * @param suffix {string}
     * @example
     //   example 1: basename('/www/site/home.htm', '.htm');
     //   returns 1: 'home'
     //   example 2: basename('ecra.php?p=1');
     //   returns 2: 'ecra.php?p=1'
     //   example 3: basename('/some/path/');
     //   returns 3: 'path'
     //   example 4: basename('/some/path_ext.ext/','.ext');
     //   returns 4: 'path_ext'
     * @returns {*}
     * @memberOf module:xide/utils/StringUtils
     * @link http://phpjs.org/functions/basename/
     */
    utils.basename = function (path, suffix) {
        let b = path;
        const lastChar = b.charAt(b.length - 1);

        if (lastChar === '/' || lastChar === '\\') {
            b = b.slice(0, -1);
        }

        b = b.replace(/^.*[\/\\]/g, '');

        if (typeof suffix === 'string' && b.substr(b.length - suffix.length) == suffix) {
            b = b.substr(0, b.length - suffix.length);
        }
        return b;
    };

    /**
     *
     * @param path
     * @param options
     * @example
     //   example 1: pathinfo('/www/htdocs/index.html', 1);
     //   returns 1: '/www/htdocs'
     //   example 2: pathinfo('/www/htdocs/index.html', 'PATHINFO_BASENAME');
     //   returns 2: 'index.html'
     //   example 3: pathinfo('/www/htdocs/index.html', 'PATHINFO_EXTENSION');
     //   returns 3: 'html'
     //   example 4: pathinfo('/www/htdocs/index.html', 'PATHINFO_FILENAME');
     //   returns 4: 'index'
     //   example 5: pathinfo('/www/htdocs/index.html', 2 | 4);
     //   returns 5: {basename: 'index.html', extension: 'html'}
     //   example 6: pathinfo('/www/htdocs/index.html', 'PATHINFO_ALL');
     //   returns 6: {dirname: '/www/htdocs', basename: 'index.html', extension: 'html', filename: 'index'}
     //   example 7: pathinfo('/www/htdocs/index.html');
     //   returns 7: {dirname: '/www/htdocs', basename: 'index.html', extension: 'html', filename: 'index'}
     * @returns {object}
     * @link http://phpjs.org/functions/pathinfo/
     * @memberOf module:xide/utils/StringUtils
     */
    utils.pathinfo = function (path, options) {
        //  discuss at:
        // original by: Nate
        //  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // improved by: Brett Zamir (http://brett-zamir.me)
        // improved by: Dmitry Gorelenkov
        //    input by: Timo
        //        note: Inspired by actual PHP source: php5-5.2.6/ext/standard/string.c line #1559
        //        note: The way the bitwise arguments are handled allows for greater flexibility
        //        note: & compatability. We might even standardize this code and use a similar approach for
        //        note: other bitwise PHP functions
        //        note: php.js tries very hard to stay away from a core.js file with global dependencies, because we like
        //        note: that you can just take a couple of functions and be on your way.
        //        note: But by way we implemented this function, if you want you can still declare the PATHINFO_*
        //        note: yourself, and then you can use: pathinfo('/www/index.html', PATHINFO_BASENAME | PATHINFO_EXTENSION);
        //        note: which makes it fully compliant with PHP syntax.
        //  depends on: basename
        let opt = '';

        let real_opt = '';
        let optName = '';
        let optTemp = 0;
        let tmp_arr = {};
        let cnt = 0;
        let i = 0;
        let have_basename = false;
        let have_extension = false;
        let have_filename = false;

        // Input defaulting & sanitation
        if (!path) {
            return false;
        }
        if (!options) {
            options = 'PATHINFO_ALL';
        }

        // Initialize binary arguments. Both the string & integer (constant) input is
        // allowed
        const OPTS = {
            'PATHINFO_DIRNAME': 1,
            'PATHINFO_BASENAME': 2,
            'PATHINFO_EXTENSION': 4,
            'PATHINFO_FILENAME': 8,
            'PATHINFO_ALL': 0
        };
        // PATHINFO_ALL sums up all previously defined PATHINFOs (could just pre-calculate)
        for (optName in OPTS) {
            if (OPTS.hasOwnProperty(optName)) {
                OPTS.PATHINFO_ALL = OPTS.PATHINFO_ALL | OPTS[optName];
            }
        }
        if (typeof options !== 'number') {
            // Allow for a single string or an array of string flags
            options = [].concat(options);
            for (i = 0; i < options.length; i++) {
                // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
                if (OPTS[options[i]]) {
                    optTemp = optTemp | OPTS[options[i]];
                }
            }
            options = optTemp;
        }

        // Internal Functions
        const __getExt = function (path) {
            const str = path + '';
            const dotP = str.lastIndexOf('.') + 1;
            return !dotP ? false : dotP !== str.length ? str.substr(dotP) : '';
        };

        // Gather path infos
        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_DIRNAME) {
            const dirName = path.replace(/\\/g, '/')
                .replace(/\/[^\/]*\/?$/, ''); // dirname
            tmp_arr.dirname = dirName === path ? '.' : dirName;
        }

        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_BASENAME) {
            if (false === have_basename) {
                have_basename = utils.basename(path);
            }
            tmp_arr.basename = have_basename;
        }

        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_EXTENSION) {
            if (false === have_basename) {
                have_basename = utils.basename(path);
            }
            if (false === have_extension) {
                have_extension = __getExt(have_basename);
            }
            if (false !== have_extension) {
                tmp_arr.extension = have_extension;
            }
        }

        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_FILENAME) {
            if (false === have_basename) {
                have_basename = utils.basename(path);
            }
            if (false === have_extension) {
                have_extension = __getExt(have_basename);
            }
            if (false === have_filename) {
                have_filename = have_basename.slice(0, have_basename.length - (have_extension ? have_extension.length + 1 :
                        have_extension === false ? 0 : 1));
            }

            tmp_arr.filename = have_filename;
        }

        // If array contains only 1 element: return string
        cnt = 0;
        for (opt in tmp_arr) {
            if (tmp_arr.hasOwnProperty(opt)) {
                cnt++;
                real_opt = opt;
            }
        }
        if (cnt === 1) {
            return tmp_arr[real_opt];
        }

        // Return full-blown array
        return tmp_arr;
    };

    /**
     * PHP.js version of parse_url
     * @param str {string}
     * @param component {string} enum
     * @returns {object}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.parse_url = function (str, component) {
        //       discuss at: http://phpjs.org/functions/parse_url/
        //      improved by: Brett Zamir (http://brett-zamir.me)
        //             note: original by http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
        //             note: blog post at http://blog.stevenlevithan.com/archives/parseuri
        //             note: demo at http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
        //             note: Does not replace invalid characters with '_' as in PHP, nor does it return false with
        //             note: a seriously malformed URL.
        //             note: Besides function name, is essentially the same as parseUri as well as our allowing
        //             note: an extra slash after the scheme/protocol (to allow file:/// as in PHP)
        //        example 1: parse_url('http://username:password@hostname/path?arg=value#anchor');
        //        returns 1: {scheme: 'http', host: 'hostname', user: 'username', pass: 'password', path: '/path', query: 'arg=value', fragment: 'anchor'}
        let query;

        const key = ['source', 'scheme', 'authority', 'userInfo', 'user', 'pass', 'host', 'port',
                'relative', 'path', 'directory', 'file', 'query', 'fragment'
            ];

        const ini = (this.php_js && this.php_js.ini) || {};

        const mode = (ini['phpjs.parse_url.mode'] &&
            ini['phpjs.parse_url.mode'].local_value) || 'php';

        let parser = {
            php: /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
            strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
            loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-scheme to catch file:/// (should restrict this)
        };

        const m = parser[mode].exec(str);
        const uri = {};
        let i = 14;
        while (i--) {
            if (m[i]) {
                uri[key[i]] = m[i];
            }
        }

        if (component) {
            return uri[component.replace('PHP_URL_', '')
                .toLowerCase()];
        }
        if (mode !== 'php') {
            const name = (ini['phpjs.parse_url.queryKey'] &&
                ini['phpjs.parse_url.queryKey'].local_value) || 'queryKey';
            parser = /(?:^|&)([^&=]*)=?([^&]*)/g;
            uri[name] = {};
            query = uri[key[12]] || '';
            query.replace(parser, function ($0, $1, $2) {
                if ($1) {
                    uri[name][$1] = $2;
                }
            });
        }
        delete uri.source;
        return uri;
    };

    /***
     *
     * @deprecated
     */
    utils.getMimeTable = function () {
        return {};
    };

    /***
     * @deprecated
     * @returns {object}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.getMimeTable2 = function () {
        return {
            "mid": "fa-file-audio-o",
            "txt": "fa-file-text-o",
            "sql": "fa-cube",
            "js": "fa-cube",
            "gif": "fa-file-picture-o",
            "jpg": "fa-file-picture-o",
            "html": "fa-cube",
            "htm": "fa-cube",
            "rar": "fa-file-zip-o",
            "gz": "fa-file-zip-o",
            "tgz": "fa-file-zip-o",
            "z": "fa-file-zip-o",
            "ra": "fa-file-movie-o",
            "ram": "fa-file-movie-o",
            "rm": "fa-file-movie-o",
            "pl": "source_pl.png",
            "zip": "fa-file-zip-o",
            "wav": "fa-file-audio-o",
            "php": "fa-cube",
            "php3": "fa-cube",
            "phtml": "fa-cube",
            "exe": "fa-file-o",
            "bmp": "fa-file-picture-o",
            "png": "fa-file-picture-o",
            "css": "fa-cube",
            "mp3": "fa-file-audio-o",
            "m4a": "fa-file-audio-o",
            "aac": "fa-file-audio-o",
            "xls": "fa-file-excel-o",
            "xlsx": "fa-file-excel-o",
            "ods": "fa-file-excel-o",
            "sxc": "fa-file-excel-o",
            "csv": "fa-file-excel-o",
            "tsv": "fa-file-excel-o",
            "doc": "fa-file-word-o",
            "docx": "fa-file-word-o",
            "odt": "fa-file-word-o",
            "swx": "fa-file-word-o",
            "rtf": "fa-file-word-o",
            "md": "fa-file-word-o",
            "ppt": "fa-file-powerpoint-o",
            "pps": "fa-file-powerpoint-o",
            "odp": "fa-file-powerpoint-o",
            "sxi": "fa-file-powerpoint-o",
            "pdf": "fa-file-pdf-o",
            "mov": "fa-file-movie-o",
            "avi": "fa-file-movie-o",
            "mpg": "fa-file-movie-o",
            "mpeg": "fa-file-movie-o",
            "mp4": "fa-file-movie-o",
            "m4v": "fa-file-movie-o",
            "ogv": "fa-file-movie-o",
            "webm": "fa-file-movie-o",
            "wmv": "fa-file-movie-o",
            "swf": "fa-file-movie-o",
            "flv": "fa-file-movie-o",
            "tiff": "fa-file-picture-o",
            "tif": "fa-file-picture-o",
            "svg": "fa-file-picture-o",
            "psd": "fa-file-picture-o",
            "ers": "horo.png"
        };
    };
    /***
     *
     * @deprecated
     * @memberOf module:xide/utils/StringUtils
     * @returns {object}
     */
    utils.getIconTable = function () {
        return {};
    };


    /**
     *
     * @param string
     * @param overwrite
     * @returns {object}
     * @memberOf module:xide/utils/StringUtils
     * @deprecated
     */
    utils.urlDecode = function (string, overwrite) {
        if (!string || !string.length) {
            return {}
        }
        const obj = {};
        const pairs = string.split("&");
        let pair;
        let name;
        let value;
        for (let i = 0, len = pairs.length; i < len; i++) {
            pair = pairs[i].split("=");
            name = decodeURIComponent(pair[0]);
            value = decodeURIComponent(pair[1]);
            if (value != null && value === 'true') {
                value = true;
            } else if (value === 'false') {
                value = false;
            }
            if (overwrite !== true) {
                if (typeof obj[name] == "undefined") {
                    obj[name] = value
                } else {
                    if (typeof obj[name] == "string") {
                        obj[name] = [obj[name]];
                        obj[name].push(value)
                    } else {
                        obj[name].push(value)
                    }
                }
            } else {
                obj[name] = value
            }
        }
        return obj;
    };
    /**
     *
     * @param string {string}
     * @returns {object}
     * @deprecated
     * @memberOf module:xide/utils/StringUtils
     */
    utils.getUrlArgs = function (string) {
        const args = {};
        if (string && (string.indexOf('?') != -1 || string.indexOf('&') != -1)) {

            const query = string.substr(string.indexOf("?") + 1) || location.search.substring(1);
            const pairs = query.split("&");
            for (let i = 0; i < pairs.length; i++) {
                const pos = pairs[i].indexOf("=");
                const name = pairs[i].substring(0, pos);
                let value = pairs[i].substring(pos + 1);
                value = decodeURIComponent(value);
                args[name] = value;
            }
        }
        return args;
    };

    /**
     *
     * @param url {string}
     * @returns {object}
     * @deprecated
     */
    utils.urlArgs = function (url) {
        const query = utils.getUrlArgs(url);
        const map = {};
        for (const param in query) {
            let value = query[param];

            const options = utils.findOcurrences(value, {
                begin: "|",
                end: "|"
            });

            let parameterOptions = null;

            if (options && options.length) {
                //clean value:
                value = value.replace(options[0], '');

                //parse options
                const optionString = options[0].substr(1, options[0].length - 2);

                const optionSplit = optionString.split(',');
                const optionsData = {};

                for (let i = 0; i < optionSplit.length; i++) {
                    const keyValue = optionSplit[i];
                    const pair = keyValue.split(':');

                    optionsData[pair[0]] = pair[1];
                }
                parameterOptions = optionsData;
            }

            if (value && value.length) {
                map[param] = {
                    value: value,
                    options: parameterOptions
                }
            }
        }
        return map;
    };

    /**
     *
     * @param fileName {string}
     * @returns {string}
     * @deprecated
     * @memberOf module:xide/utils/StringUtils
     */
    utils.getIcon = function (fileName) {
        if (!fileName) {
            return 'txt2.png';
        }
        const extension = utils.getFileExtension(fileName);
        if (extension) {
            const mime = utils.getMimeTable();
            if (mime[extension] != null) {
                return mime[extension];
            }
        }
        return 'txt2.png';
    };
    /**
     *
     * @param fileName
     * @returns {string}
     * @deprecated
     * @memberOf module:xide/utils/StringUtils
     */
    utils.getIconClass = function (fileName) {
        if (!fileName) {
            return 'fa-file-o';
        }
        const extension = utils.getFileExtension(fileName);
        if (types.customMimeIcons[extension]) {
            return types.customMimeIcons[extension];
        }
        if (extension) {
            const mime = utils.getMimeTable2();
            if (mime[extension] != null) {
                return mime[extension];
            }
        }
        return 'fa-file-o';
    };
    /**
     * File extension
     * @deprecated
     * @param fileName {string}
     * @returns {string}
     */
    utils.getFileExtension = function (fileName) {
        if (!fileName || fileName == "") return "";
        const split = utils.getBaseName(fileName).split('.');
        if (split.length > 1) return split[split.length - 1].toLowerCase();
        return '';
    };
    /**
     * Create a basic UUID via with Math.Random
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.createUUID = function () {
        const S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()); //String
    };
    /**
     * Basename
     * @param fileName {string}
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     */
    utils.getBaseName = function (fileName) {
        if (fileName == null) return null;
        let separator = "/";
        if (fileName.indexOf("\\") !==-1)
            separator = "\\";
        return fileName.substr(fileName.lastIndexOf(separator) + 1, fileName.length);
    };
    /**
     * PHP.js version of basename
     * @param path {string}
     * @param suffix
     * @returns {string}
     * @memberOf module:xide/utils/StringUtils
     * @example

     //   example 1: basename('/www/site/home.htm', '.htm')
     //   returns 1: 'home'
     //   example 2: basename('ecra.php?p=1')
     //   returns 2: 'ecra.php?p=1'
     //   example 3: basename('/some/path/')
     //   returns 3: 'path'
     //   example 4: basename('/some/path_ext.ext/','.ext')
     //   returns 4: 'path_ext'

     * @memberOf module:xide/utils/StringUtils
     */
    utils.basename = function basename(path, suffix) {
        //  discuss at: http://locutus.io/php/basename/
        // original by: Kevin van Zonneveld (http://kvz.io)
        // improved by: Ash Searle (http://hexmen.com/blog/)
        // improved by: Lincoln Ramsay
        // improved by: djmix
        // improved by: Dmitry Gorelenkov
        let b = path;
        const lastChar = b.charAt(b.length - 1);

        if (lastChar === '/' || lastChar === '\\') {
            b = b.slice(0, -1)
        }

        b = b.replace(/^.*[\/\\]/g, '');

        if (typeof suffix === 'string' && b.substr(b.length - suffix.length) === suffix) {
            b = b.substr(0, b.length - suffix.length)
        }

        return b
    };
    /**
     *
     * @param path {string}
     * @param options
     * @memberOf module:xide/utils/StringUtils
     * @returns {object}
     *
     * @example
     *
     //   example 1: pathinfo('/www/htdocs/index.html', 1)
     //   returns 1: '/www/htdocs'
     //   example 2: pathinfo('/www/htdocs/index.html', 'PATHINFO_BASENAME')
     //   returns 2: 'index.html'
     //   example 3: pathinfo('/www/htdocs/index.html', 'PATHINFO_EXTENSION')
     //   returns 3: 'html'
     //   example 4: pathinfo('/www/htdocs/index.html', 'PATHINFO_FILENAME')
     //   returns 4: 'index'
     //   example 5: pathinfo('/www/htdocs/index.html', 2 | 4)
     //   returns 5: {basename: 'index.html', extension: 'html'}
     //   example 6: pathinfo('/www/htdocs/index.html', 'PATHINFO_ALL')
     //   returns 6: {dirname: '/www/htdocs', basename: 'index.html', extension: 'html', filename: 'index'}
     //   example 7: pathinfo('/www/htdocs/index.html')
     //   returns 7: {dirname: '/www/htdocs', basename: 'index.html', extension: 'html', filename: 'index'}
     */
    utils.pathinfo = function (path, options) {
        //  discuss at: http://locutus.io/php/pathinfo/
        // original by: Nate
        //  revised by: Kevin van Zonneveld (http://kvz.io)
        // improved by: Brett Zamir (http://brett-zamir.me)
        // improved by: Dmitry Gorelenkov
        //    input by: Timo
        //      note 1: Inspired by actual PHP source: php5-5.2.6/ext/standard/string.c line #1559
        //      note 1: The way the bitwise arguments are handled allows for greater flexibility
        //      note 1: & compatability. We might even standardize this
        //      note 1: code and use a similar approach for
        //      note 1: other bitwise PHP functions
        //      note 1: Locutus tries very hard to stay away from a core.js
        //      note 1: file with global dependencies, because we like
        //      note 1: that you can just take a couple of functions and be on your way.
        //      note 1: But by way we implemented this function,
        //      note 1: if you want you can still declare the PATHINFO_*
        //      note 1: yourself, and then you can use:
        //      note 1: pathinfo('/www/index.html', PATHINFO_BASENAME | PATHINFO_EXTENSION);
        //      note 1: which makes it fully compliant with PHP syntax.


        let basename = utils.basename;
        let opt = '';
        let realOpt = '';
        let optName = '';
        let optTemp = 0;
        let tmpArr = {};
        let cnt = 0;
        let i = 0;
        let haveBasename = false;
        let haveExtension = false;
        let haveFilename = false;

        // Input defaulting & sanitation
        if (!path) {
            return false
        }
        if (!options) {
            options = 'PATHINFO_ALL'
        }

        // Initialize binary arguments. Both the string & integer (constant) input is
        // allowed
        const OPTS = {
            'PATHINFO_DIRNAME': 1,
            'PATHINFO_BASENAME': 2,
            'PATHINFO_EXTENSION': 4,
            'PATHINFO_FILENAME': 8,
            'PATHINFO_ALL': 0
        };
        // PATHINFO_ALL sums up all previously defined PATHINFOs (could just pre-calculate)
        for (optName in OPTS) {
            if (OPTS.hasOwnProperty(optName)) {
                OPTS.PATHINFO_ALL = OPTS.PATHINFO_ALL | OPTS[optName]
            }
        }
        if (typeof options !== 'number') {
            // Allow for a single string or an array of string flags
            options = [].concat(options);
            for (i = 0; i < options.length; i++) {
                // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
                if (OPTS[options[i]]) {
                    optTemp = optTemp | OPTS[options[i]]
                }
            }
            options = optTemp
        }

        // Internal Functions
        const _getExt = function (path) {
            const str = path + '';
            const dotP = str.lastIndexOf('.') + 1;
            return !dotP ? false : dotP !== str.length ? str.substr(dotP) : ''
        };

        // Gather path infos
        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_DIRNAME) {
            const dirName = path
                .replace(/\\/g, '/')
                .replace(/\/[^\/]*\/?$/, ''); // dirname
            tmpArr.dirname = dirName === path ? '.' : dirName
        }

        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_BASENAME) {
            if (haveBasename === false) {
                haveBasename = basename(path)
            }
            tmpArr.basename = haveBasename
        }

        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_EXTENSION) {
            if (haveBasename === false) {
                haveBasename = basename(path)
            }
            if (haveExtension === false) {
                haveExtension = _getExt(haveBasename)
            }
            if (haveExtension !== false) {
                tmpArr.extension = haveExtension
            }
        }

        //noinspection JSBitwiseOperatorUsage,JSBitwiseOperatorUsage
        if (options & OPTS.PATHINFO_FILENAME) {
            if (haveBasename === false) {
                haveBasename = basename(path)
            }
            if (haveExtension === false) {
                haveExtension = _getExt(haveBasename)
            }
            if (haveFilename === false) {
                haveFilename = haveBasename.slice(0, haveBasename.length - (haveExtension
                            ? haveExtension.length + 1
                            : haveExtension === false
                            ? 0
                            : 1
                    )
                )
            }

            tmpArr.filename = haveFilename
        }

        // If array contains only 1 element: return string
        cnt = 0;
        for (opt in tmpArr) {
            if (tmpArr.hasOwnProperty(opt)) {
                cnt++;
                realOpt = opt
            }
        }
        if (cnt === 1) {
            return tmpArr[realOpt]
        }

        // Return full-blown array
        return tmpArr
    };

    /**
     *
     * @param input {string}
     * @param allowed {string}
     * @example
     //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
     //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
     //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
     //   returns 2: '<p>Kevin van Zonneveld</p>'
     //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
     //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
     //   example 4: strip_tags('1 < 5 5 > 1');
     //   returns 4: '1 < 5 5 > 1'
     //   example 5: strip_tags('1 <br/> 1');
     //   returns 5: '1  1'
     //   example 6: strip_tags('1 <br/> 1', '<br>');
     //   returns 6: '1 <br/> 1'
     //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
     //   returns 7: '1 <br/> 1'
     * @returns {string}
     */
    utils.strip_tags = function (input, allowed) {
        //  discuss at: http://phpjs.org/functions/strip_tags/
        allowed = (((allowed || '') + '')
            .toLowerCase()
            .match(/<[a-z][a-z0-9]*>/g) || [])
            .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
        const tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
        const commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
        return input.replace(commentsAndPhpTags, '')
            .replace(tags, function ($0, $1) {
                return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            });
    };
    return utils;
});
