define([
    'xide/utils',
    'xide/types',
    'xide/factory',
    'dojo/has',
    'xide/lodash'
],function(utils,types,factory,has,_){
    "use strict";
    /**
     *
     * @param cis
     * @returns {Array}
     */
    utils.toOptions  = function(cis){
        cis = utils.flattenCIS(cis);
        const result = [];
        for (let i = 0; i < cis.length; i++) {
            const ci = cis[i];
            result.push({
                name:utils.toString(ci['name']),
                value:utils.getCIValue(ci),
                type: utils.toInt(ci['type']),
                enumType:utils.toString(ci['enumType']),
                visible:utils.toBoolean(ci['visible']),
                active:utils.toBoolean(ci['active']),
                changed:utils.toBoolean(ci['changed']),
                group:utils.toString(ci['group']),
                user:utils.toObject(ci['user']),
                dst:utils.toString(ci['dst']),
                id:utils.toString(ci['id']),
                params:utils.toString(ci['params'])
            })
        }
        return result;
    };

    if(has('xideve') || has('xblox-ui')) {
        utils.getEventsAsOptions = function (selected) {
            let result = [
                {label: "Select Event", value: ""}
            ];
            for (const e in types.EVENTS) {
                const label = types.EVENTS[e];

                const item = {
                    label: label,
                    value: types.EVENTS[e]
                };
                result.push(item);
            }
            result = result.concat(
                [{label: "onclick", value: "onclick"},
                    {label: "ondblclick", value: "dblclick"},
                    {label: "onmousedown", value: "mousedown"},
                    {label: "onmouseup", value: "mouseup"},
                    {label: "onmouseover", value: "mouseover"},
                    {label: "onmousemove", value: "mousemove"},
                    {label: "onmouseout", value: "mouseout"},
                    {label: "onkeypress", value: "keypress"},
                    {label: "onkeydown", value: "keydown"},
                    {label: "onkeyup", value: "keyup"},
                    {label: "onfocus", value: "focus"},
                    {label: "onblur", value: "blur"},
                    {label: "On Load", value: "Load"}
                ]);
            //select the event we are listening to
            for (let i = 0; i < result.length; i++) {
                const obj = result[i];
                if (obj.value === selected) {
                    obj.selected = true;
                    break;
                }
            }
            return result;
        };
    }

    utils.flattenCIS  = function(cis){
        const addedCIS = [];
        const removedCIs = [];
        for (var i = 0; i < cis.length; i++) {
            const ci = cis[i];
            const ciType = utils.toInt(ci.type);
            if(ciType > types.ECIType.END){//type is higher than core types, try to resolve it
                const resolved = types.resolveType(ciType);
                if(resolved){
                    utils.mixin(addedCIS,resolved);
                    removedCIs.push(ci);
                }
            }
        }
        if(addedCIS.length>0){
            cis = cis.concat(addedCIS);
        }
        if(removedCIs){
            for(var i in removedCIs){
                cis.remove(removedCIs[i]);
            }
        }
        return cis;
    };

    utils.arrayContains=function(array,element){
        for (let i = 0; i < array.length; i++){
            const _e = array[i];
            if(_e===element){
                return true;
            }
        }
        return false;
    };

    utils.setStoreCIValueByField = function (d, field, value) {
        if (d[field] == null) {
            d[field] = [];
        }
        d[field][0] = utils.getStringValue(value);
        return d;
    };
    /**
     *
     * @param label
     * @param value
     * @param extra
     * @returns {Object}
     */
    utils.createOption=function(label,value,extra){
        return utils.mixin({
            label:label,
            value:value !=null ? value : label
        },extra);
    };
    /**
     *
     * @param name
     * @param type
     * @param value
     * @param args
     * @param settings
     * @returns {{dataRef: null, dataSource: null, name: *, group: number, id: *, title: *, type: *, uid: number, value: *, visible: boolean, enumType: number, class: string}}
     */
    utils.createCI = function (name, type, value,args,settings) {
        const res = {
            dataRef:null,
            dataSource:null,
            name:name,
            group:-1,
            id:name,
            title:name,
            type:type,
            uid:-1,
            value: value!=null ? value : -1,
            visible:true,
            enumType:-1,
            "class":"cmx.types.ConfigurableInformation"
        };
        utils.mixin(res,args);
        if(settings){
            if(settings.publish){
                factory.publish(settings.publish,{
                    CI:res,
                    owner:settings.owner
                },settings.owner);
            }
        }
        return res;
    };

    utils.createCIAsArray = function (name, type, chain,value) {
        return {
            chainType:[chain ? chain : 0],
            dataRef:[null],
            dataSource:[null],
            params:[],
            name:[name],
            group:[-1],
            id:[name],
            title:[name],
            type:[type],
            uid:[-1],
            value: [value ? value : -1],
            visible:[true],
            enumType:[-1],
            parentId:[-1],
            "class":["cmx.types.ConfigurableInformation"]
        };
    };

    utils.hasValue = function (data){
        return data.value &&  data.value[0] !=null && data.value[0].length > 0 && data.value[0] !="0" && data.value[0] !="undefined" && data.value[0] !="Unset";
    };

    utils.hasValueAndDataRef = function (data){
        return data.value &&  data.value[0] !=null && data.value[0].length > 0 && data.value[0] !="0" && data.value[0] !="undefined" && data.value[0] !="Unset" &&
            data.dataRef &&  data.dataRef[0] !=null && data.dataRef[0].length > 0 && data.dataRef[0] !="0" && data.dataRef[0] !="undefined";
    };

    utils.getInputCIByName = function (data,name){
        if(!data ||!name){
            return null;
        }
        const chain = 0;
        let dstChain = chain == 0 ? data.inputs : chain == 1 ? data.outputs : null;
        if(!dstChain){//has no chains, be nice
            dstChain=data;
        }
        if (dstChain != null) {
            for (let i = 0; i < dstChain.length; i++) {
                const ci = dstChain[i];
                const _n = utils.getStringValue(ci.name);
                if (_n!=null && _n.toLowerCase() === name.toLowerCase()){
                    return ci;
                }
            }
        }
        return null;
    };
    utils.getInputCIById = function (data,name){
        if(!data){
            return null;
        }
        const chain = 0;
        let dstChain = chain == 0 ? data.inputs : chain == 1 ? data.outputs : null;
        if(!dstChain){//has no chains, be nice
            dstChain=data;
        }
        if (dstChain != null) {
            for (let i = 0; i < dstChain.length; i++) {
                const ci = dstChain[i];
                const _n = utils.getStringValue(ci.id);
                if (_n!=null && _n.toLowerCase() === name.toLowerCase()){
                    return ci;
                }
            }
        }
        return null;
    };
    /***
     *
     * @param data
     * @param chain
     * @param name
     * @returns {*}
     */
    utils.getCIByChainAndName = function (data, chain, name) {
        if(!data){
            return null;
        }
        let dstChain = chain == 0 ? data.inputs : chain == 1 ? data.outputs : null;
        if(!dstChain){//has no chains
            dstChain=data;
        }
        if (dstChain != null) {
            for (let i = 0; i < dstChain.length; i++) {
                const ci = dstChain[i];
                const _n = utils.getStringValue(ci.name);
                if (_n!=null && _n.toLowerCase() === name.toLowerCase()){
                    return ci;
                }
            }
        }
        return null;
    };
    utils.getCIByUid= function (dstChain, uid) {
        if (dstChain != null) {
            for (let i = 0; i < dstChain.length; i++) {
                const ci = dstChain[i];
                const _n = utils.getStringValue(ci.uid);
                if (_n!=null && _n === uid)
                {
                    return ci;
                }
            }
        }
        return null;
    };
    utils.getCIById= function (data, chain, id) {
        const dstChain = chain == 0 ? data.inputs : chain == 1 ? data.outputs : null;
        if (dstChain != null) {
            for (let i = 0; i < dstChain.length; i++) {
                const ci = dstChain[i];
                if (ci.id[0] == id[0]  )
                    return ci;
            }
        }
        return null;
    };
    utils.getCIInputValueByName = function (data, name) {
        const ci = utils.getCIByChainAndName(data, 0, name);
        if (ci) {
            return ci.value;
        }
        return null;
    };
    utils.getCIValue = function (data){
        return utils.getCIValueByField(data,"value");
    };
    utils.getStringValue = function (d){
        return utils.toString(d);
    };
    utils.toString = function (d){
        if (d != null) {
            if(!_.isArray(d))
            {
                return ''+ d;
            }
            if(d && d.length==1 && d[0]==null)
            {
                return null;
            }
            return '' + (d[0] !=null ? d[0] : d);
        }
        return null;
    };
    /**
     *
     * @param data
     * @param value
     */
    utils.setIntegerValue = function (data,value){
        if (data != null) {

            if(dojo.isArray(data))
            {
                data[0]=value;
            }else{
                data=value;
            }
        }
    };
    /**
     *
     * @param data
     * @param field
     * @returns {*}
     */
    utils.getCIValueByField = function (data, field) {
        if (data[field] != null) {
            if(_.isArray(data[field])){
                return data[field][0] ? data[field][0] : data[field];
            }else{
                return data[field];
            }
        }
        return null;
    };
    /**
     *
     * @param data
     * @param field
     * @param value
     * @returns {*}
     */
    utils.setCIValueByField = function (data, field, value) {
        if(!data){
            return data;
        }
        if (data[field] == null) {
            data[field] = [];
        }
        data[field]=value
        return data;
    };

    utils.setCIValue = function (data, field, value) {
        const ci = utils.getInputCIByName(data,field);
        if(ci){
            utils.setCIValueByField(ci,'value',value);
        }
        return ci;
    };
    /**
     *
     * @param data
     * @param name
     * @param field
     * @returns {*}
     */
    utils.getCIInputValueByNameAndField = function (data, name, field) {
        const ci = utils.getCIByChainAndName(data, 0, name);
        if (ci) {
            return ci["" + field];
        }
        return null;
    };
    /**
     *
     * @param data
     * @param name
     * @param field
     * @returns {null}
     */
    utils.getCIInputValueByNameAndFieldStr = function (data, name, field) {
        const rawValue = utils.getCIInputValueByNameAndField(data,name,field);
        if(rawValue){
            return utils.getStringValue(rawValue);
        }
        return null;
    };
    /**
     *
     * @param data
     * @param name
     * @param field
     * @returns {null}
     */
    utils.getCIInputValueByNameAndFieldBool = function (data, name, field) {
        const rawValue = utils.getCIInputValueByNameAndField(data,name,field);
        if(rawValue){
            return utils.toBoolean(rawValue);
        }
        return null;
    };
    /**
     *
     * @param cis
     * @param name
     * @returns {*}
     */
    utils.getCIWidgetByName=function(cis,name){

        for (let i = 0; i < cis.length; i++) {
            const ci = cis[i];
            if(ci['_widget'] && ci.name===name){
                return ci['_widget'];
            }
        }
        return null;
    };
    return utils;
});