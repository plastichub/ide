/** @module xide/utils/HTMLUtils **/
define([
    'xide/utils',
    'xide/types',
    'dcl/dcl',
    'xdojo/declare',
    "dojo/dom-construct",
    'dojo/has',
    'dojo/dom-class',
    "dojo/_base/window",
    'xide/lodash',
    'xide/$'
], function (utils, types, dcl, declare, domConstruct, has, domClass, win, _, $) {
    /**
     * @TODO: remove
     * #Maqetta back compat tool
     * @returns {*}
     */
    utils.getDoc = function () {
        return win.doc;
    };
    /**
     * @TODO: remove
     * Save empty a node or a widget
     * @param mixed {HTMLElement|xide/_base/_Widget || dijit/_WidgetBase}
     * @returns void
     */
    utils.empty = function (mixed) {
        //seems widget
        if (mixed.getChildren && mixed.removeChild) {
            const children = mixed.getChildren();
            _.each(children, function (widget) {
                mixed.removeChild(widget);
            });
        }
        //now remove anything non-widget
        const _el = mixed.containerNode || mixed.domNode || _.isElement(mixed) ? mixed : null;
        if (_el) {
            domConstruct.empty(_el);
        }
    };
    /**
     *
     * @TODO: remove, not needed since 3.0
     * @param startNode
     * @param mustHaveClass
     * @returns {*}
     */
    utils.findEmptyNode = function (startNode, mustHaveClass) {
        if (!startNode || startNode.children == null || startNode.children.length == null) {
            return null;
        }
        let children = startNode.children;
        if (mustHaveClass !== null) {
            children = utils.find(mustHaveClass, startNode, false);
        }
        for (const i in children) {
            const child = children[i];
            if (child.innerHTML === '') {
                return child;
            }
        }
        return null;
    };
    /**
     *
     * @param tag {string}
     * @param options {object}
     * @param where {HTMLElement}
     * @memberOf module:xide/utils
     * @returns {HTMLElement}
     */
    utils.create = function (tag, options, where) {
        let doc = win.doc;
        if (where) {
            doc = where.ownerDocument;
        }

        if (typeof tag == "string") {
            tag = doc.createElement(tag);
        }
        options && $(tag).attr(options);
        where && $(where).append(tag);
        return tag;
    };

    /**
     * Returns true when a node is child of another node
     * @param parent {HTMLElement}
     * @param child {HTMLElement}
     * @memberOf module:xide/utils
     * @returns {boolean}
     */
    utils.isDescendant = function (parent, child) {
        let node = child.parentNode;
        while (node !== null) {
            if (node == parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    };
    utils.find = function (clss, parent) {
        return $(clss, parent)[0];
    }
    /**
     * Finds and returns a widgets instance in a stack-container by name
     * @param name
     * @param container
     * @memberOf module:xide/utils
     * @returns {module:xide/layout/_Container}
     */
    utils.hasChild = function (name, container) {
        if (!!name || !container && container.getChildren) {
            return _.find(container.getChildren(), {
                title: name
            });
        }
    };
    /**
     *
     * @param proto {Module} a module
     * @param args {Object} the constructor arguments
     * @param node {HTMLElement|null}
     * @param extraBaseClasses {Module[]} additional base classes
     * @param classExtension
     * @memberOf module:xide/utils
     * @returns {Object}
     */
    utils.createInstanceSync = function (proto, args, node, extraBaseClasses, classExtension) {
        //extra bases and/or class extension, create a dynamic class and fill extra-bases
        if (extraBaseClasses || classExtension) {
            extraBaseClasses = extraBaseClasses || [];

            if (classExtension) {
                extraBaseClasses.push(declare(proto, classExtension));
            }
        }
        if (extraBaseClasses) {
            extraBaseClasses = _.isArray(extraBaseClasses) ? extraBaseClasses : [extraBaseClasses];
            extraBaseClasses.push(proto);
            extraBaseClasses.reverse();
            proto = proto.extend ? declare(extraBaseClasses, {}) : dcl(extraBaseClasses, {});
        }
        return new proto(args || {}, node || win.doc.createElement('div'));

    };
    /***
     * addWidget
     * @param widgetProto {module:xide/_base/_Widget|module:xide/widgets/_Widget}
     * @param ctrArgsIn {object|null}
     * @param delegate {*|null}
     * @param parent {HTMLElement|module:xide/_base/_Widget|module:xide/widgets/_Widget}
     * @param startup {boolean}
     * @param cssClass {string} CSS class to be added
     * @param baseClasses {null|object[]}
     * @param select {boolean} call select (ie: a tab in a container)
     * @param classExtension {object} one more mixin
     * @memberOf module:xide/utils
     * @returns {module:xide/_base/_Widget|module:xide/widgets/_Widget|null}
     */
    utils.addWidget = function (widgetProto, ctrArgsIn, delegate, parent, startup, cssClass, baseClasses, select, classExtension) {
        const ctrArgs = {
            delegate: delegate
        };
        ctrArgsIn = ctrArgsIn || {};
        utils.mixin(ctrArgs, ctrArgsIn);

        //deal with class name
        if (_.isString(widgetProto)) {
            const _widgetProto = utils.getObject(widgetProto);
            if (_widgetProto) {
                widgetProto = _widgetProto;
            }
        }

        parent = _.isString(parent) ? domConstruct.create(parent) : parent == null ? win.doc.createElement('div') : parent;
        const isDirect = ctrArgsIn.attachDirect ? ctrArgsIn.attachDirect : (widgetProto && widgetProto.prototype ? widgetProto.prototype.attachDirect : false);
        ctrArgs._parent = parent;

        const _target = utils.getNode(parent);

        //@TODO: remove
        if (parent && parent.finishLoading) {
            parent.finishLoading();
        }
        //@TODO: remove
        if (ctrArgs.attachChild && parent.addChild) {
            delete ctrArgs.attachChild;
            return parent.addChild(widgetProto, ctrArgs, startup);
        }


        //@TODO: replace
        if (parent.addWidget && ctrArgs.ignoreAddChild !== true) {
            return parent.addWidget(widgetProto, ctrArgsIn, delegate, parent, startup, cssClass, baseClasses, select, classExtension);
        }

        const widget = utils.createInstanceSync(widgetProto, ctrArgs, isDirect ? _target : null, baseClasses, classExtension); // new widgetProto(ctrArgs, dojo.doc.createElement('div'));
        if (!widget) {
            console.error('widget creation failed! ', arguments);
            return null;
        }

        if (parent) {
            if (!isDirect) {
                utils.addChild(parent, widget, startup, select);
            } else {
                startup && widget.startup();
            }
        } else {
            return widget;
        }

        if (cssClass) {
            domClass.add(widget.domNode, cssClass);
        }

        if (parent.resize || parent.startup) {
            widget._parent = parent;
        }
        return widget;
    };

    /***
     * addChild is a Dojo abstraction. It tries to call addChild on the parent when the client is fitted for this case.
     * @param parent {HTMLElement|module:xide/widgets/_Widget}
     * @param child {HTMLElement|module:xide/widgets/_Widget}
     * @param startup {boolean} call startup() on the child
     * @param select {boolean} select the widget if parent has such method
     * @memberOf module:xide/utils
     */
    utils.addChild = function (parent, child, startup, select) {
        if (!parent || !child) {
            console.error('error! parent or child is invalid!');
            return;
        }
        try {
            const parentIsWidget = typeof parent.addChild === 'function';
            const _parentNode = parentIsWidget ? parent : utils.getNode(parent);
            const _childNode = parentIsWidget ? child : child.domNode || child;
            if (_parentNode && _childNode) {
                if (!parent.addChild) {
                    if (_childNode.nodeType) {
                        _parentNode.appendChild(_childNode);
                        if (startup === true && child.startup) {
                            child.startup();
                        }
                    } else {
                        logError('child is not html');
                    }
                } else {
                    let insertIndex = -1;
                    if (parent.getChildren) {
                        insertIndex = parent.getChildren().length;
                    }
                    try {
                        //@TODO: this has wrong signature in beta3
                        parent.addChild(_childNode, insertIndex, select !== null ? select : startup);
                    } catch (e) {
                        logError(e, 'add child failed for some reason!' + e);
                    }
                }
            } else if (has('debug')) {
                console.error("utils.addChild : invalid parameters :: parent or child domNode is null");
            }
        } catch (e) {
            logError(e, 'addWidget : crashed : ');
        }
    };
    /***
     *
     * 'templatify' creates a sort of dynamic widget which behaves just like a normal templated widget. Its
     * there for simple widget creation which goes beyond the string substitute alternative. This can
     * avoid also to carry around too many widget modules in your app.
     *
     * @param baseClass {xide/widgets/TemplatedWidgetBase|dijit/_TemplatedMixin} a base class to use, this must be
     * anything which is a dijit/_TemplatedMixin.
     *
     * @param templateString {string} template string as usual, can have all tags like  data-dojo-attach-point and so
     * forth
     *
     * @param parentNode {HTMLNode} the node where the 'dynamic widget' is being added to.

     * @param templateArguments {Object} some parameters mixed into the widget. In the example below you might use
     * {iconClass:'fa-play'} to insert the icon class 'fa-play' into the widget's template
     *
     * @param baseClasses {Object[]=} optional, a number of additional base classes you want make the 'dynamic widget'
     * to be inherited from.
     *
     * @param startup {boolean} call startup on the widget
     *
     * @returns {Widget} returns the templated widget
     *
     *
     * @example var _tpl =  "<div>" +
     "<div class='' data-dojo-type='dijit.form.DropDownButton' data-dojo-props=\"iconClass:'${!iconClass}'\" +
     "data-dojo-attach-point='wButton'>" +
     "<span></span>" +
     "<div data-dojo-attach-point='menu' data-dojo-type='dijit.Menu' style='display: none;'></div>" +
     "</div></div>";

     var widget  = this.templatify(xide/widgets/TemplatedWidgetBase,_tpl, parent , {
            iconClass:'fa-play'
        },[xide/mixins/ReloadMixin]);

     * @memberOf module:xide/utils
     * @extends xide/utils

     */
    utils.templatify = function (baseClass, templateString, parentNode, templateArguments, baseClasses, startup) {
        const widgetClassIn = baseClass || 'xide/widgets/TemplatedWidgetBase';
        let widgetProto = null;
        if (baseClasses) {
            widgetProto = declare([baseClass].concat(baseClasses));
        } else {
            widgetProto = utils.getObject(widgetClassIn);
        }
        if (!widgetProto) {
            return null;
        }
        const ctrArgs = {
            templateString: templateString
        };
        utils.mixin(ctrArgs, templateArguments);
        const widget = new widgetProto(ctrArgs, dojo.doc.createElement('div'));
        utils.addChild(parentNode, widget, startup);
        return widget;
    };
    /**
     * XIDE specific
     * @param prop
     * @param owner
     * @memberOf module:xide/utils
     * @private
     */
    utils._clearProperty = function (prop, owner) {
        let _key = null;
        if (owner) {
            _key = utils.getObjectKeyByValue(owner, prop);
        }
        if (_key) {
            owner[_key] = null;
        }
    };
    /**
     * XIDE specific destroy
     * @param view
     * @param callDestroy
     * @param owner
     * @memberOf module:xide/utils
     * @private
     */
    utils._destroyWidget = function (view, callDestroy, owner) {
        try {
            _.isString(view) && (view = $(view)[0]);
            if (view) {
                if (view.parentContainer &&
                    view.parentContainer.removeChild &&
                    view.domNode) {
                    if (view.destroy && callDestroy !== false) {
                        try {
                            view.destroy();
                        } catch (e) {
                            console.error('error destroying view');
                        }
                    }
                    view.parentContainer.removeChild(view);
                    if (owner) {
                        utils._clearProperty(view, owner);
                    }
                    return;
                }
                view.destroyRecursive && view.destroyRecursive();
                view.destroy && view._destroyed !== true && view.destroy();
                view._destroyed = true;
                if (view.domNode || view["domNode"]) {
                    if (view.domNode) {
                        domConstruct.destroy(view.domNode);
                    }
                } else {
                    const doc = view.ownerDocument;
                    // cannot use _destroyContainer.ownerDocument since this can throw an exception on IE
                    if (doc) {
                        domConstruct.destroy(view);
                    }
                }
                utils._clearProperty(view, owner);
            }

        } catch (e) {
            logError(e, 'error in destroying widget ' + e);
        }
    };
    /**
     * Destroys a widget or HTMLElement safely. When an owner
     * is specified, 'widget' will be nulled in owner
     * @param widget {Widget|HTMLElement|object}
     * @param callDestroy instruct to call 'destroy'
     * @memberOf module:xide/utils
     * @param owner {Object=}
     */
    utils.destroy = function (widget, callDestroy, owner) {
        if (widget) {
            if (_.isArray(widget)) {
                for (let i = 0; i < widget.length; i++) {
                    const obj1 = widget[i];
                    let _key = null;
                    if (owner) {
                        _key = utils.getObjectKeyByValue(owner, obj1);
                    }
                    utils._destroyWidget(obj1, callDestroy);
                    if (_key) {
                        owner[_key] = null;
                    }
                }
            } else {
                utils._destroyWidget(widget, callDestroy, owner);
            }
        }
    };

    /**
     * Get a widet's default append target, dojo specfic
     * @param target
     * @memberOf module:xide/utils
     * @returns {*}
     */
    utils.getNode = function (target) {
        if (target) {
            return target.containerNode || target.domNode || target;
        }
        return target;
    };

    /**
     * Return the total height for widgets
     * @param widgets {module:xide/widgets/_Widget|module:xide/widgets/_Widget[]}
     * @memberOf module:xide/utils
     * @returns {number}
     */
    utils.getHeight = function (widgets) {
        if (!_.isArray(widgets)) {
            widgets = [widgets];
        }
        let total = 0;
        _.each(widgets, function (w) {
            total += $(utils.getNode(w)).outerHeight();
        });
        return total;

    };
    /**
     *
     * @param source
     * @param target
     * @param height
     * @param width
     * @param force
     * @param offset
     * @memberOf module:xide/utils
     */
    utils.resizeTo = function (source, target, height, width, force, offset) {
        target = utils.getNode(target);
        source = utils.getNode(source);
        if (height === true) {
            let targetHeight = $(target).height();
            if (offset && offset.h !== null) {
                targetHeight += offset.h;
            }
            $(source).css('height', targetHeight + 'px' + (force === true ? '!important' : ''));
        }
        if (width === true) {
            const targetWidth = $(target).width();
            $(source).css('width', targetWidth + 'px' + (force === true ? '!important' : ''));
        }
    };

    return utils;
});