define([
    'xide/utils',
    'require',
    "dojo/Deferred",
    'xide/lodash'
], function (utils, require, Deferred, lodash) {
    const _debug = false;
    "use strict";

    utils.delegate = (function () {
        // boodman/crockford delegation w/ cornford optimization
        function TMP() {
        }

        return function (obj, props) {
            TMP.prototype = obj;
            const tmp = new TMP();
            TMP.prototype = null;
            if (props) {
                lang._mixin(tmp, props);
            }
            return tmp; // Object
        };
    })();

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Loader utils
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    utils.debounce = function (who, methodName, _function, delay, options, now, args) {
        let _place = who[methodName + '_debounced'];
        if (!_place) {
            _place = who[methodName + '_debounced'] = lodash.debounce(_function, delay, options);
        }
        if (now === true) {
            if (!who[methodName + '_debouncedFirst']) {
                who[methodName + '_debouncedFirst'] = true;
                _function.apply(who, args);
            }
        }
        return _place();
    };


    utils.pluck = function (items, prop) {
        return lodash.map(items, prop);
    };

    /**
     * Trigger downloadable file
     * @param filename
     * @param text
     */
    utils.download = function (filename, text) {
        const element = document.createElement('a');
        text = lodash.isString(text) ? text : JSON.stringify(text, null, 2);
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    };

    /**
     * Ask require registry at this path
     * @param mixed
     * @returns {*}
     */
    utils.hasObject = function (mixed) {
        let result = null;
        const _re = require;
        try {
            result = _re(mixed);
        } catch (e) {
            console.error('error in utils.hasObject ', e);
        }
        return result;
    };
    /**
     * Safe require.toUrl
     * @param mid {string}
     */
    utils.toUrl = function (mid) {
        const _require = require;
        //make sure cache bust is off otherwise it appends ?time
        _require({
            cacheBust: null,
            waitSeconds: 5
        });
        return _require.toUrl(mid);
    }
    /**
     * Returns a module by module path
     * @param mixed {String|Object}
     * @param _default {Object} default object
     * @returns {Object|Promise}
     */
    utils.getObject = function (mixed, _default) {
        let result = null;
        if (utils.isString(mixed)) {
            const _re = require;
            try {
                result = _re(mixed);
            } catch (e) {
                _debug && console.warn('utils.getObject::require failed for ' + mixed);
            }
            //not a loaded module yet
            try {
                if (!result) {
                    const deferred = new Deferred();
                    //try loader
                    result = _re([
                        mixed
                    ], function (module) {
                        deferred.resolve(module);
                    });
                    return deferred.promise;
                }
            } catch (e) {
                _debug && console.error('error in requiring ' + mixed, e);
            }
            return result;

        } else if (utils.isObject(mixed)) {
            return mixed;//reflect
        }
        return result !== null ? result : _default;
    };


    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  True object utils
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    utils.toArray = function (obj) {
        const result = [];
        for (const c in obj) {
            result.push({
                name: c,
                value: obj[c]
            });
        }
        return result;
    };
    /**
     * Array to object conversion
     * @param arr
     * @returns {Object}
     */
    utils.toObject = function (arr, lodash) {
        if (!arr) {
            return {};
        }
        if (lodash !== false) {
            return lodash.object(lodash.map(arr, lodash.values));
        } else {
            //CI related back compat hack
            if (utils.isObject(arr) && arr[0]) {
                return arr[0];
            }

            const rv = {};
            for (let i = 0; i < arr.length; ++i) {
                rv[i] = arr[i];
            }
            return rv;
        }
    };

    /**
     * Gets an object property by string, eg: utils.byString(someObj, 'part3[0].name');
     * @deprecated, see objectAtPath below
     * @param o {Object}    : the object
     * @param s {String}    : the path within the object
     * @param defaultValue {Object|String|Number} : an optional default value
     * @returns {*}
     */
    utils.byString = function (o, s, defaultValue) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        while (a.length) {
            const n = a.shift();
            if (n in o) {
                o = o[n];
            } else {
                return;
            }
        }
        return o;
    };

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Object path
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Internals
     */

    //cache
    const toStr = Object.prototype.toString;

    const _hasOwnProperty = Object.prototype.hasOwnProperty;

    /**
     * @private
     * @param type
     * @returns {*}
     */
    function toString(type) {
        return toStr.call(type);
    }

    /**
     * @private
     * @param key
     * @returns {*}
     */
    function getKey(key) {
        const intKey = parseInt(key, 10);
        if (intKey.toString() === key) {
            return intKey;
        }
        return key;
    }

    /**
     * internal set value at path in object
     * @private
     * @param obj
     * @param path
     * @param value
     * @param doNotReplace
     * @returns {*}
     */
    function set(obj, path, value, doNotReplace) {
        if (lodash.isNumber(path)) {
            path = [path];
        }
        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isString(path)) {
            return set(obj, path.split('.').map(getKey), value, doNotReplace);
        }
        const currentPath = path[0];

        if (path.length === 1) {
            const oldVal = obj[currentPath];
            if (oldVal === void 0 || !doNotReplace) {
                obj[currentPath] = value;
            }
            return oldVal;
        }

        if (obj[currentPath] === void 0) {
            //check if we assume an array
            if (lodash.isNumber(path[1])) {
                obj[currentPath] = [];
            } else {
                obj[currentPath] = {};
            }
        }
        return set(obj[currentPath], path.slice(1), value, doNotReplace);
    }

    /**
     * deletes an property by a path
     * @param obj
     * @param path
     * @returns {*}
     */
    function del(obj, path) {
        if (lodash.isNumber(path)) {
            path = [path];
        }
        if (lodash.isEmpty(obj)) {
            return void 0;
        }

        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isString(path)) {
            return del(obj, path.split('.'));
        }

        const currentPath = getKey(path[0]);
        const oldVal = obj[currentPath];

        if (path.length === 1) {
            if (oldVal !== void 0) {
                if (lodash.isArray(obj)) {
                    obj.splice(currentPath, 1);
                } else {
                    delete obj[currentPath];
                }
            }
        } else {
            if (obj[currentPath] !== void 0) {
                return del(obj[currentPath], path.slice(1));
            }
        }
        return obj;
    }

    /**
     * Private helper class
     * @private
     * @type {{}}
     */
    const objectPath = {};

    objectPath.has = function (obj, path) {
        if (lodash.isEmpty(obj)) {
            return false;
        }
        if (lodash.isNumber(path)) {
            path = [path];
        } else if (lodash.isString(path)) {
            path = path.split('.');
        }

        if (lodash.isEmpty(path) || path.length === 0) {
            return false;
        }

        for (let i = 0; i < path.length; i++) {
            const j = path[i];
            if ((lodash.isObject(obj) || lodash.isArray(obj)) && _hasOwnProperty.call(obj, j)) {
                obj = obj[j];
            } else {
                return false;
            }
        }

        return true;
    };

    /**
     * Define private public 'ensure exists'
     * @param obj
     * @param path
     * @param value
     * @returns {*}
     */
    objectPath.ensureExists = function (obj, path, value) {
        return set(obj, path, value, true);
    };

    /**
     * Define private public 'set'
     * @param obj
     * @param path
     * @param value
     * @param doNotReplace
     * @returns {*}
     */
    objectPath.set = function (obj, path, value, doNotReplace) {
        return set(obj, path, value, doNotReplace);
    };

    /**
     Define private public 'insert'
     * @param obj
     * @param path
     * @param value
     * @param at
     */
    objectPath.insert = function (obj, path, value, at) {
        let arr = objectPath.get(obj, path);
        at = ~~at;
        if (!lodash.isArray(arr)) {
            arr = [];
            objectPath.set(obj, path, arr);
        }
        arr.splice(at, 0, value);
    };

    /**
     * Define private public 'empty'
     * @param obj
     * @param path
     * @returns {*}
     */
    objectPath.empty = function (obj, path) {
        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isEmpty(obj)) {
            return void 0;
        }

        let value;
        let i;
        if (!(value = objectPath.get(obj, path))) {
            return obj;
        }

        if (lodash.isString(value)) {
            return objectPath.set(obj, path, '');
        } else if (lodash.isBoolean(value)) {
            return objectPath.set(obj, path, false);
        } else if (lodash.isNumber(value)) {
            return objectPath.set(obj, path, 0);
        } else if (lodash.isArray(value)) {
            value.length = 0;
        } else if (lodash.isObject(value)) {
            for (i in value) {
                if (_hasOwnProperty.call(value, i)) {
                    delete value[i];
                }
            }
        } else {
            return objectPath.set(obj, path, null);
        }
    };

    /**
     * Define private public 'push'
     * @param obj
     * @param path
     */
    objectPath.push = function (obj, path /*, values */) {
        let arr = objectPath.get(obj, path);
        if (!lodash.isArray(arr)) {
            arr = [];
            objectPath.set(obj, path, arr);
        }
        arr.push.apply(arr, Array.prototype.slice.call(arguments, 2));
    };

    /**
     * Define private public 'coalesce'
     * @param obj
     * @param paths
     * @param defaultValue
     * @returns {*}
     */
    objectPath.coalesce = function (obj, paths, defaultValue) {
        let value;
        for (let i = 0, len = paths.length; i < len; i++) {
            if ((value = objectPath.get(obj, paths[i])) !== void 0) {
                return value;
            }
        }
        return defaultValue;
    };

    /**
     * Define private public 'get'
     * @param obj
     * @param path
     * @param defaultValue
     * @returns {*}
     */
    objectPath.get = function (obj, path, defaultValue) {
        if (lodash.isNumber(path)) {
            path = [path];
        }
        if (lodash.isEmpty(path)) {
            return obj;
        }
        if (lodash.isEmpty(obj)) {
            //lodash doesnt seem to work with html nodes
            if (obj && obj.innerHTML === null) {
                return defaultValue;
            }
        }
        if (lodash.isString(path)) {
            return objectPath.get(obj, path.split('.'), defaultValue);
        }
        const currentPath = getKey(path[0]);
        if (path.length === 1) {
            if (obj && obj[currentPath] === void 0) {
                return defaultValue;
            }
            if (obj) {
                return obj[currentPath];
            }
        }
        if (!obj) {
            return defaultValue;
        }
        return objectPath.get(obj[currentPath], path.slice(1), defaultValue);
    };

    /**
     * Define private public 'del'
     * @param obj
     * @param path
     * @returns {*}
     */
    objectPath.del = function (obj, path) {
        return del(obj, path);
    };
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Object path public xide/utils mixin
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  Returns a value by a give object path
     *
     *  //works also with arrays
     *    objectPath.get(obj, "a.c.1");  //returns "f"
     *    objectPath.get(obj, ["a","c","1"]);  //returns "f"
     *
     * @param obj {object}
     * @param path {string}
     * @param _default {object|null}
     * @returns {*}
     */
    utils.getAt = function (obj, path, _default) {
        return objectPath.get(obj, path, _default);
    };

    /**
     * Sets a value in an object/array at a given path.
     * @example
     *
     * utils.setAt(obj, "a.h", "m"); // or utils.setAt(obj, ["a","h"], "m");
     *
     * //set will create intermediate object/arrays
     * objectPath.set(obj, "a.j.0.f", "m");
     *
     * @param obj{Object|Array}
     * @param path {string}
     * @param value {mixed}
     * @returns {Object|Array}
     */
    utils.setAt = function (obj, path, value) {
        return objectPath.set(obj, path, value);
    };

    /**
     * Returns there is anything at given path within an object/array.
     * @param obj
     * @param path
     */
    utils.hasAt = function (obj, path) {
        return objectPath.has(obj, path);
    };

    /**
     * Ensures at given path, otherwise _default will be placed
     * @param obj
     * @param path
     * @returns {*}
     */
    utils.ensureAt = function (obj, path, _default) {
        return objectPath.ensureExists(obj, path, _default);
    };
    /**
     * Deletes at given path
     * @param obj
     * @param path
     * @returns {*}
     */
    utils.deleteAt = function (obj, path) {
        return objectPath.del(obj, path);
    };

    /**
     *
     * @param to
     * @param from
     * @returns {*}
     */
    utils.merge = function (to, from) {
        for (const n in from) {
            if (typeof to[n] != 'object') {
                to[n] = from[n];
            } else if (typeof from[n] == 'object') {
                to[n] = utils.merge(to[n], from[n]);
            }
        }

        return to;
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Dojo's most wanted
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Clones objects (including DOM nodes) and all children.
     * Warning: do not clone cyclic structures.
     * @param src {*} The object to clone.
     * @returns {*}
     */
    utils.clone = function (src) {
        if (!src || typeof src != "object" || utils.isFunction(src)) {
            // null, undefined, any non-object, or function
            return src; // anything
        }
        if (src.nodeType && "cloneNode" in src) {
            // DOM Node
            return src.cloneNode(true); // Node
        }
        if (src instanceof Date) {
            // Date
            return new Date(src.getTime()); // Date
        }
        if (src instanceof RegExp) {
            // RegExp
            return new RegExp(src); // RegExp
        }
        let r;
        let i;
        let l;
        if (utils.isArray(src)) {
            // array
            r = [];
            for (i = 0, l = src.length; i < l; ++i) {
                if (i in src) {
                    r.push(utils.clone(src[i]));
                }
            }
            // we don't clone functions for performance reasons
            // }else if(d.isFunction(src)){
            // // function
            // r = function(){ return src.apply(this, arguments); };
        } else {
            // generic objects
            r = src.constructor ? new src.constructor() : {};
        }
        return utils._mixin(r, src, utils.clone);
    };

    /**
     * Copies/adds all properties of source to dest; returns dest.
     * @description All properties, including functions (sometimes termed "methods"), excluding any non-standard extensions
     * found in Object.prototype, are copied/added to dest. Copying/adding each particular property is
     * delegated to copyFunc (if any); copyFunc defaults to the Javascript assignment operator if not provided.
     * Notice that by default, _mixin executes a so-called "shallow copy" and aggregate types are copied/added by reference.
     * @param dest {object} The object to which to copy/add all properties contained in source.
     * @param source {object} The object from which to draw all properties to copy into dest.
     * @param copyFunc {function} The process used to copy/add a property in source; defaults to the Javascript assignment operator.
     * @returns {object} dest, as modified
     * @private
     */
    utils._mixin = function (dest, source, copyFunc) {
        let name;
        let s;
        let i;
        const empty = {};
        for (name in source) {
            // the (!(name in empty) || empty[name] !== s) condition avoids copying properties in "source"
            // inherited from Object.prototype.	 For example, if dest has a custom toString() method,
            // don't overwrite it with the toString() method that source inherited from Object.prototype
            s = source[name];
            if (!(name in dest) || (dest[name] !== s && (!(name in empty) || empty[name] !== s))) {
                dest[name] = copyFunc ? copyFunc(s) : s;
            }
        }

        return dest; // Object
    };
    /**
     * Copies/adds all properties of one or more sources to dest; returns dest.
     * @param dest {object} The object to which to copy/add all properties contained in source. If dest is falsy, then
     * a new object is manufactured before copying/adding properties begins.
     *
     * @param sources One of more objects from which to draw all properties to copy into dest. sources are processed
     * left-to-right and if more than one of these objects contain the same property name, the right-most
     * value "wins".
     *
     * @returns {object} dest, as modified
     *
     * @example
     * make a shallow copy of an object
     * var copy = utils.mixin({}, source);
     *
     * @example
     *
     * many class constructors often take an object which specifies
     *        values to be configured on the object. In this case, it is
     *        often simplest to call `lang.mixin` on the `this` object:
     *        declare("acme.Base", null, {
    *			constructor: function(properties){
    *				//property configuration:
    *				lang.mixin(this, properties);
    *				console.log(this.quip);
    *			},
    *			quip: "I wasn't born yesterday, you know - I've seen movies.",
    *			* ...
    *		});
     *
     *        //create an instance of the class and configure it
     *        var b = new acme.Base({quip: "That's what it does!" });
     *
     */
    utils.mixin = function (dest, sources) {
        if (sources) {
            if (!dest) {
                dest = {};
            }
            const l = arguments.length;
            for (let i = 1; i < l; i++) {
                utils._mixin(dest, arguments[i]);
            }
            return dest; // Object
        }
        return dest;
    };

    /**
     * Clone object keys
     * @param defaults
     * @returns {{}}
     */
    utils.cloneKeys = function (defaults, skipEmpty) {
        const result = {};
        for (const _class in defaults) {
            if (skipEmpty === true && !(_class in defaults)) {
                continue;
            }
            result[_class] = defaults[_class];
        }
        return result;
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  STD
    /**
     *
     * @param what
     * @returns {*}
     */
    utils.isArray = function (what) {
        return lodash.isArray(what);
    };
    /**
     *
     * @param what
     * @returns {*}
     */
    utils.isObject = function (what) {
        return lodash.isObject(what);
    };
    /**
     *
     * @param what
     * @returns {*}
     */
    utils.isString = function (what) {
        return lodash.isString(what);
    };
    /**
     *
     * @param what
     * @returns {*}
     */
    utils.isNumber = function (what) {
        return lodash.isNumber(what);
    };
    /**
     * Return true if it is a Function
     * @param it
     * @returns {*}
     */
    utils.isFunction = function (it) {
        return lodash.isFunction(it);
    };
    return utils;
});