/*global module */
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        jshint: {
            src: [
                "**/*.js",
                "!**/node_modules/**",
                "!**/out/**",
                "!handlebars.js",
                "!**/*.bak.*",
                "!**/nls/**",
                "!**/tests/**",
                "!ace/**",
                "!editor/ace/**",
                "!widgets/ExpressionJavaScript.js"
            ],
            options: {
                jshintrc: "./.jshintrc",
                reporter: require('jshint-html-reporter'),
                "-W041": false,
                "-W110": false,
                "-W098":false,
                reporterOutput:'./xide_jshint_report.html'
            }
        },
        intern: {
            local: {
                options: {
                    runType: "runner",
                    config: "tests/intern/intern",
                    reporters: ["runner"]
                }
            },
            remote: {
                options: {
                    runType: "runner",
                    config: "tests/intern",
                    reporters: ["runner"]
                }
            }
        },
        "jsdoc-amddcl": {
            "plugins": [
                "plugins/markdown"
            ],
            docs: {
                files: [
                    {
                        src: [
                            "./manager/*.js",
                            "./action/*.js",
                            "./mixins/*.js",
                            "./widgets/*.js",
                            "./views/*.js",
                            "./factory/*.js",
                            "./grid/*.js",
                            "./views/*.js",
                            "./utils/*.js",
                            "./Keyboard.js",
                            "./model/*.js",
                            "./bean/*.js",
                            "!./MaqettaPatch.js",
                            "!./node_modules",
                            "../xide/types/Types.js",
                            "./package.json",
                            "!**.bak"
                        ]

                    }
                ]
            },
            'export': {
                files: [
                    {
                        args: [
                            "-X"
                        ],
                        src: [
                            ".",
                            "../xide/types/Types.js",
                            "./README.md",
                            "./package.json"
                        ],
                        dest: "/tmp/doclets.json"
                    }
                ]
            }
        }
    });

    // Load plugins
    grunt.loadNpmTasks("intern");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("jsdoc-amddcl");

    // Aliases
    //grunt.registerTask("css", ["less", "cssToJs"]);
    grunt.registerTask("jsdoc", "jsdoc-amddcl");

    // Testing.
    // Always specify the target e.g. grunt test:remote, grunt test:remote
    // then add on any other flags afterwards e.g. console, lcovhtml.
    const testTaskDescription = "Run this task instead of the intern task directly! \n" +
        "Always specify the test target e.g. \n" +
        "grunt test:local\n" +
        "grunt test:remote\n\n" +
        "Add any optional reporters via a flag e.g. \n" +
        "grunt test:local:console\n" +
        "grunt test:local:lcovhtml\n" +
        "grunt test:local:console:lcovhtml";

    grunt.registerTask("test", testTaskDescription, function (target) {
        function addReporter(reporter) {
            const property = "intern." + target + ".options.reporters";
            const value = grunt.config.get(property);
            if (value.includes(reporter)) {
                return;
            }
            value.push(reporter);
            grunt.config.set(property, value);
        }

        if (this.flags.lcovhtml) {
            addReporter("lcovhtml");
        }

        if (this.flags.console) {
            addReporter("console");
        }
        grunt.task.run("intern:" + target);
    });
};