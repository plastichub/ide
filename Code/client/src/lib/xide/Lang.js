define([
    'xdojo/has',
    'xdojo/declare',
    'xlang/i18',
    'xide/utils/StringUtils',
    'dojo/Deferred',
    'require'
],function(has,declare,i18,StringUtils,Deferred,require){


    const Module = declare('xide/Lang',null,{});
    const validLanguages = ['ar','bg','ca','cs','da','de','el','en','es','fa','fr','he','hu','id','it','jp','ko','nl','no','pl','pt_BR','ro','ru','sk','sl','sr','sv','tr','uk','vi','zh_CN','zh_TW'];
    function isValidLanguage(lang){
        return validLanguages.includes(lang);
    }

    Module.getLanguage = function(){
        const _lang = navigator.languages ? navigator.languages[0] : (navigator.language || navigator.userLanguage);
        let language = 'en';
        if (typeof navigator !== 'undefined') {
            if (navigator.appName == 'Netscape')
                language = navigator.language.substring(0, 2);
            else
                language = navigator.browserLanguage.substring(0, 2);
        }

        if (language != _lang && _lang) {
            language = _lang;
        }

        const urlParts = StringUtils.urlArgs(location.href);
        if(urlParts.lang){
            language = urlParts.lang.value;
        }

        if(isValidLanguage(language)){
            return language;
        }else{
            const _short = language.substring(0,2);
            if(isValidLanguage(_short)){
                return _short;
            }
        }
        return 'en';
    };

    Module.loadLanguage = function(_lang){
        _lang = _lang || Module.getLanguage();
        const dfd = new Deferred();
        const re = require;
        if(_lang!=='en'){
            re(['xlang/'+_lang],function(langModule){
                i18.setLanguage(langModule);
                dfd.resolve(langModule);
            });
        }else{
            dfd.resolve();
        }
        return dfd;
    };
    return Module;
});