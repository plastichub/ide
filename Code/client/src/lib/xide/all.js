define([
    'dojo/_base/declare',
    'xide/utils',
    'xide/types',
    'xide/factory'
], function (declare,utils,types,factory) {
    const module = declare("xide.all", null,{});
    module.utils = utils;
    module.types = types;
    module.factory = factory;
    return module;
});