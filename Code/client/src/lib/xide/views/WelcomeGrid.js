define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/TreeRenderer',
    'dstore/Trackable',
    'xide/data/ObservableStore',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xaction/DefaultActions',
    'dojo/dom-construct',
    'xgrid/ThumbRenderer',
    'module',
    'xgrid/Selection',
    "xide/widgets/_Widget",
    'xide/data/TreeMemory',
    'dojo/Deferred',
    'xgrid/KeyboardNavigation',
    'xide/data/Model',
    'xide/data/Reference',
    'xide/data/ReferenceMapping',
    "xide/mixins/EventedMixin",
    "xide/lodash",
    "xide/$",
    'xdojo/has!debug?xide/tests/TestUtils'
], function (dcl, declare, types,
    utils, TreeRenderer,
    Trackable, ObservableStore, Grid, MultiRenderer,
    DefaultActions, domConstruct, ThumbRenderer, module, Selection, _XWidget, TreeMemory, Deferred,
    KeyboardNavigation, Model,
    Reference, ReferenceMapping, EventedMixin, _, $, TestUtils) {

    const ThumbClass = declare('xfile.ThumbRenderer2', [ThumbRenderer], {
        thumbSize: "400",
        resizeThumb: true,
        __type: 'thumb',
        deactivateRenderer: function () {
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding', '');
            this.isThumbGrid = false;

        },
        activateRenderer: function () {
            $(this.contentNode).css('padding', '8px');
            this.isThumbGrid = true;
            this.refresh();
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            const div = domConstruct.create('div', {
                    className: "tile widget",
                    style: 'max-width:200px;float:left;width:120px;height:80px;'
                });

            const icon = obj.icon;
            const label = obj.label;
            const imageClass = obj.icon || 'fa fa-folder fa-4x';

            const iconStyle = 'text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 36px;opacity: 0.7';
            const contentClass = 'icon';
            const folderContent = '<span style="' + iconStyle + '" class="fa fa-4x ' + imageClass + '"></span>';
            const html = '<div title="' + obj.label + '" class="tile-content ' + contentClass + '">' +
                folderContent + '</div><div class="brand opacity"><span class="thumbText text opacity ellipsis" style="text-align:center">' +
                label +
                '</span></div>';

            div.innerHTML = html;
            return div;
        }
    });
    const GroupRenderer = declare('xfile.GroupRenderer', TreeRenderer, {
        itemClass: ThumbClass,
        groupClass: TreeRenderer,
        renderRow: function (obj) {
            return (!obj.group ? this.itemClass : this.groupClass).prototype.renderRow.apply(this, arguments);
        }
    });
    const Implementation = {};
    const renderers = [GroupRenderer, ThumbClass, TreeRenderer];
    const multiRenderer = declare.classFactory('xgrid/MultiRendererGroups', {}, renderers, MultiRenderer.Implementation);

    /**
     * silly popup toolbar, origin https://github.com/paulkinzett/toolbar.git
     */
    /*
    var TOOLTIP_TOOLBAR = dcl(EventedMixin.dcl, {
        init: function (options, elem) {
            var self = this;
            self.elem = elem;
            self.$elem = $(elem);
            self.options = $.extend({}, options);
            self.metadata = self.$elem.data();
            self.overrideOptions();
            self.toolbar = $('<div class="tool-container" />')
                .addClass('tool-' + self.options.position)
                .addClass('toolbar-' + self.options.style)
                .append('<div class="tool-items" />')
                .append('<div class="arrow" />')
                .appendTo('body')
                .css('opacity', 0)
                .hide();
            self.toolbar_arrow = self.toolbar.find('.arrow');
            self.initializeToolbar();
        },

        overrideOptions: function () {
            var self = this;
            $.each(self.options, function ($option) {
                if (typeof (self.$elem.data('toolbar-' + $option)) != "undefined") {
                    self.options[$option] = self.$elem.data('toolbar-' + $option);
                }
            });
        },

        initializeToolbar: function () {
            var self = this;
            self.populateContent();
            self.setTrigger();
            self.toolbarWidth = self.toolbar.width();
        },
        setTrigger: function () {
            var self = this;
            if (self.options.event !== 'click') {
                var moveTime;

                function decideTimeout() {
                    if (self.$elem.hasClass('pressed')) {
                        moveTime = setTimeout(function () {
                            self.hide();
                        }, 150);
                    } else {
                        clearTimeout(moveTime);
                    }
                }

                self.$elem.on({
                    mouseenter: function (event) {
                        if (self.$elem.hasClass('pressed')) {
                            clearTimeout(moveTime);
                        } else {
                            self.show();
                        }
                    }
                });

                self.$elem.on({
                    mouseleave: function (event) {
                        decideTimeout();
                    }
                });
                var body = $('body');
                self.__on(body, 'mouseenter', '.tool-container', function () {
                    clearTimeout(moveTime);
                });
                self.__on(body, 'mouseleave', '.tool-container', function () {
                    decideTimeout();
                });
            }


            if (self.options.event == 'click') {
                self.$elem.on('click', function (event) {
                    event.preventDefault();
                    if (self.$elem.hasClass('pressed')) {
                        self.hide();
                    } else {
                        self.show();
                    }
                });
            }
        },
        populateContent: function () {
            var self = this;
            var location = self.toolbar.find('.tool-items');
            var content = $(self.options.content).clone(true).find('a').addClass('tool-item');
            location.html(content);
            location.find('.tool-item').on('click', function (event) {
                event.preventDefault();
                self.$elem.trigger('toolbarItemClick', this);
            });
        },
        calculatePosition: function () {
            var self = this;
            self.arrowCss = {};
            self.toolbarCss = self.getCoordinates(self.options.position, self.options.adjustment);
            self.toolbarCss.position = 'absolute';
            self.toolbarCss.zIndex = self.options.zIndex;
            self.collisionDetection();
            self.toolbar.css(self.toolbarCss);
            self.toolbar_arrow.css(self.arrowCss);
        },
        getCoordinates: function (position, adjustment) {
            var self = this;
            self.coordinates = self.$elem.offset();

            if (self.options.adjustment && self.options.adjustment[self.options.position]) {
                adjustment = self.options.adjustment[self.options.position] + adjustment;
            }

            switch (self.options.position) {
                case 'top':
                    return {
                        left: self.coordinates.left - (self.toolbar.width() / 2) + (self.$elem.outerWidth() / 2),
                        top: self.coordinates.top - self.$elem.outerHeight() - adjustment,
                        right: 'auto'
                    };
                case 'left':
                    return {
                        left: self.coordinates.left - (self.toolbar.width() / 2) - (self.$elem.outerWidth() / 2) - adjustment,
                        top: self.coordinates.top - (self.toolbar.height() / 2) + (self.$elem.outerHeight() / 2),
                        right: 'auto'
                    };
                case 'right':
                    return {
                        left: self.coordinates.left + (self.toolbar.width() / 2) + (self.$elem.outerWidth() / 2) + adjustment,
                        top: self.coordinates.top - (self.toolbar.height() / 2) + (self.$elem.outerHeight() / 2),
                        right: 'auto'
                    };
                case 'bottom':
                    return {
                        left: self.coordinates.left - (self.toolbar.width() / 2) + (self.$elem.outerWidth() / 2),
                        top: self.coordinates.top + self.$elem.outerHeight() + adjustment,
                        right: 'auto'
                    };
            }
        },
        collisionDetection: function () {
            var self = this;
            var edgeOffset = 20;
            if (self.options.position == 'top' || self.options.position == 'bottom') {
                self.arrowCss = {
                    left: '50%',
                    right: '50%'
                };
                if (self.toolbarCss.left < edgeOffset) {
                    self.toolbarCss.left = edgeOffset;
                    self.arrowCss.left = self.$elem.offset().left + self.$elem.width() / 2 - (edgeOffset);
                } else if (($(window).width() - (self.toolbarCss.left + self.toolbarWidth)) < edgeOffset) {
                    self.toolbarCss.right = edgeOffset;
                    self.toolbarCss.left = 'auto';
                    self.arrowCss.left = 'auto';
                    self.arrowCss.right = ($(window).width() - self.$elem.offset().left) - (self.$elem.width() / 2) - (edgeOffset) - 5;
                }
            }
        },
        show: function () {
            var self = this;
            self.$elem.addClass('pressed');
            self.calculatePosition();
            self.options.render(self, self.toolbar.find('.tool-items'));
            self.toolbar.show().css({
                'opacity': 1
            }).addClass('animate-' + self.options.animation);
            self.$elem.trigger('toolbarShown');
        },
        hide: function () {
            var self = this;
            var animation = {
                'opacity': 0
            };

            self.$elem.removeClass('pressed');

            switch (self.options.position) {
                case 'top':
                    animation.top = '+=20';
                    break;
                case 'left':
                    animation.left = '+=20';
                    break;
                case 'right':
                    animation.left = '-=20';
                    break;
                case 'bottom':
                    animation.top = '-=20';
                    break;
            }

            self.toolbar.animate(animation, 200, function () {
                self.toolbar.hide();
            });

            self.$elem.trigger('toolbarHidden');
        },
        getToolbarElement: function () {
            return this.toolbar.find('.tool-items');
        },
        destroy: function () {
            this.toolbar.remove();
        }
    });
    */

    const GridClass = Grid.createGridClass('xfile.views.Grid', Implementation, {
        SELECTION: {
            CLASS: Selection
        },
        KEYBOARD_SELECTION: true,
        CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
        ACTIONS: types.GRID_FEATURES.ACTIONS,
        WIDGET: {
            CLASS: _XWidget
        },
        KEYBOARD_NAVIGATION: {
            CLASS: KeyboardNavigation
        }
    }, {
        RENDERER: multiRenderer
    }, {
        renderers: renderers,
        selectedRenderer: GroupRenderer
    });

    const Module = declare('xide/views/WelcomeGrid', GridClass, {
        expandOnClick: true,
        permissions: [
            types.ACTION.OPEN,
            types.ACTION.CONTEXT_MENU
        ],
        formatColumn: function (field, value, obj) {
            const renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
            if (renderer.formatColumn_) {
                const result = renderer.formatColumn.apply(arguments);
                if (result) {
                    return result;
                }
            }
            if (obj.renderColumn_) {
                const rendered = obj.renderColumn.apply(this, arguments);
                if (rendered) {
                    return rendered;
                }
            }
            switch (field) {
                case "label":
                    {
                        value = obj.group ? ('<span style="float:left;margin-right: 10px">' + value + '</span><hr style="margin-top: 13px;margin-left: 4px"/>') : value;
                    }
            }
            return value;
        },
        _columns: {},
        router: null,
        getColumns: function (formatters) {
            formatters = formatters || this.formatters || {};
            const self = this;
            this.columns = [];

            function createColumn(label, field, sortable, hidden) {
                if (self._columns[label] != null) {
                    hidden = !self._columns[label];
                }
                self.columns.push({
                    renderExpando: label === 'Name',
                    label: label,
                    field: field,
                    sortable: sortable,
                    formatter: function (value, obj) {
                        return label in formatters ? formatters[label].apply(self, [field, value, obj]) : self.formatColumn(field, value, obj);
                    },
                    hidden: hidden
                });
            }

            createColumn('Name', 'label', true, false);
            return this.columns;
        },
        postMixInProperties: function () {
            const state = this.state;
            if (state) {
                if (state._columns) {
                    this._columns = state._columns;
                }
            }
            if (!this.columns) {
                this.columns = this.getColumns();
            }

            return this.inherited(arguments);
        },
        wireStore: function (store) {
            const thiz = this;

            function updateGrid(what) {
                thiz._refreshTimer && clearTimeout(thiz._refreshTimer) && (thiz._refreshTimer = null);
                thiz._refreshTimer = setTimeout(function () {
                    thiz.refresh();
                }, 100);
            }
            _.each(['update', 'added', 'remove', 'delete'], function (evt) {
                this.addHandle(evt, store.on(evt, updateGrid));
            }, this);
        },
        /**
         * Override this to retrieve the original source. That is being called by the reference's owner impl.
         * @returns {*}
         */
        getSelection: function () {
            let res = this.inherited(arguments);
            res = _.map(res, function (item) {
                if (!item.group && item.owner) {
                    const ref = item.owner.routeToReference(this.router.match({
                        path: item.url
                    }));
                    if (ref) {
                        return ref;
                    }
                }
                return item;
            }, this);

            return res;
        },
        onSelectionChanged: function (evt) {
            const selection = evt.selection;
            const item = selection[0];
            const thiz = this;
            const actionStore = thiz.getActionStore();
            const contextMenu = this.getContextMenu();

            const itemActions = item && item.getActions ? item.getActions() : null;
            contextMenu && contextMenu.removeCustomActions();
            if (!itemActions || _.isEmpty(itemActions)) {
                return;
            }

            const newStoredActions = this.addActions(itemActions);
            // this.ctx.getWindowManager().clearView(null, false);
            // this.ctx.getWindowManager().registerView(grid, true);
            if (contextMenu) {
                contextMenu.setActionStore(null, this, false, false);
                contextMenu.setActionStore(actionStore, this, false, false);
            }
        },
        runAction: function (action) {
            const item = this._getSelection()[0];
            const itemRef = this.getSelection()[0];
            if (itemRef && item && item.runAction) {
                return item.runAction(action, itemRef, this);
            }
        },
        startup: function () {
            this._patchTreeClick();
            if (this.permissions) {
                const defaultActions = [].concat(DefaultActions.getDefaultActions(this.permissions, this, this));
                this.addActions(defaultActions);
            }
            const res = this.inherited(arguments);
            const store = this.collection;

            this.wireStore(store);

            this._on('selectionChanged', this.onSelectionChanged, this);

            const $node = $(this.domNode);

            $node.addClass('xFileGrid metro welcomeGrid');
            $node.css('padding', '16px');
            $node.css('height', '30px');

            this._showHeader(false);

            //collect store for removal
            this.add(store);
            this.wireTrackingStore(this.trackingStore);

            const recent = store.putSync({
                group: 'Recent',
                label: 'Recent',
                url: 'Recent',
                parent: 'root',
                directory: true
            });

            this.publish(types.EVENTS.ON_RENDER_WELCOME_GRID_GROUP, {
                item: recent,
                store: store,
                grid: this
            });

            const newItemsGroup = store.putSync({
                group: 'New',
                label: 'New',
                url: 'New',
                parent: 'root',
                directory: true
            });

            this.publish(types.EVENTS.ON_RENDER_WELCOME_GRID_GROUP, {
                item: newItemsGroup,
                store: store,
                grid: this
            });

            this.set('collection', store.filter({
                parent: 'root'
            }));

            this.refresh().then(function () {
                this.expand(this.row('Recent'));
                this.expand(this.row('Devices'));
                this.expand(this.row('New'));
            }.bind(this));
            return res;
        },
        _renderRow: function (obj) {
            /*
            var res = this.inherited(arguments);
            
            var toolBar = $(res).data('tt');
            var tt = false;
            
            if (tt && !obj.group && !toolBar) {
                var toolBar = new TOOLTIP_TOOLBAR({});
                toolBar.init({
                    content: '<div class="hidden">' +
                        '<a href="#"><i class="fa fa-plane"></i></a>' +
                        '<a href="#"><i class="fa fa-car"></i></a>' +
                        '<a href="#"><i class="fa fa-bicycle"></i></a>' +
                        '</div>',
                    render: function (toolbar, parent) {
                        if (!toolbar.didRender) {
                            toolbar.didRender = true;
                        }
                    },
                    position: 'bottom',
                    zIndex: 120,
                    hover: false,
                    style: 'default',
                    animation: 'standard',
                    adjustment: 10
                }, res);
                
                $(res).data('tt', toolBar);
                $(res).on('remove', function () {
                    toolBar.destroy();
                });
            }
            return res;
            */
        },
        refreshItems: function () {
            const store = this.collection;
            const recent = store.getSync('Recent');
            if (recent) {
                recent && this.publish(types.EVENTS.ON_RENDER_WELCOME_GRID_GROUP, {
                    item: recent,
                    store: store,
                    grid: this
                });
                this.refresh();
            }
        },
        wireTrackingStore: function (store) {
            const self = this;
            _.each(['added', 'updated'], function (e) {
                this.addHandle(e, store.on(e, function (evt) {
                    self.refreshItems();
                }));
            }, this);
        }
    });
    const WelcomeStore = declare('xide/views/WelcomeStore', [TreeMemory, Trackable, ObservableStore], {
        idProperty: 'url',
        parentProperty: 'parent',
        Model: dcl([Model, Reference, ReferenceMapping], {}),
        id: utils.createUUID(),
        mayHaveChildren: function (object) {
            if (object.group) {
                return true;
            }
            return false;
        },
        _fetchRange: function (kwArgs) {
            const deferred = new Deferred();
            let _res = this.fetchRangeSync(kwArgs);
            if (this._state.filter) {
                if (this._state.filter['category']) {
                    const _items = _.filter(this.data, this._state.filter);
                    deferred.resolve(_items);
                    _res = _items;
                }
            }
            deferred.resolve(_res);
            return deferred;
        },
        getChildren: function (object) {
            const filter = {};
            filter[this.parentProperty] = this.getIdentity(object);
            return this.root.filter(filter);
        }
    });

    /*
    var ctx = typeof window !== 'undefined' ? window.sctx : null;
    var root;

    
    if (ctx) {
        var mainView = ctx.mainView;
        var target = mainView.welcomePage.containerNode;
        $(target).empty();
        var trackingMgr = ctx.getTrackingManager();
        var router = ctx.getRouter();
        var store = new WelcomeStore({});
        var parent = TestUtils.createTab(null, null, module.id);
        var grid = utils.addWidget(Module, {
            collection: store,
            _columns: {},
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            router: router,
            ctx: ctx,
            trackingStore: trackingMgr.getStore()
        }, null, target, true);
        return Module;
    }
    */

    Module.STORE_CLASS = WelcomeStore;
    return Module;
});