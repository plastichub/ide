define([
    "dcl/dcl",
    "xide/views/_CIPanelDialog",
    "xide/utils",
    "xide/types",
    "dojo/when",
    "dojo/Deferred"
], function (dcl, _CIPanelDialog, utils, types, when, Deferred) {
    const Module = dcl(null, {
        declaredClass: 'SimmpleCIDialog'
    });
    Module.create = (title, cis, ok, cancel) => {
        const actionDialog = new _CIPanelDialog({
            title: title || 'Untitled',
            resizeable: true,
            onOk: function (changedCIS) {
                ok(changedCIS);
            },
            onCancel() {
                cancel && cancel()
            },
            cis: cis
        });
        return actionDialog;
    }
    return Module;
});