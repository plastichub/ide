define([
    "dojo/_base/declare",
    "require",
    "exports",
    "module",
    "xide/ace/base_handler",
    "xide/ace/complete_util"
],function (declare,require,exports,module,baseLanguageHandler,completeUtil){


    /*
    var completeUtil = require("plugins/c9.ide.language/complete_util");

    var baseLanguageHandler = require('plugins/c9.ide.language/base_handler');*/

    const analysisCache = {}; // path => {identifier: 3, ...}
    const globalWordIndex = {}; // word => frequency
    const globalWordFiles = {}; // word => [path]
    let precachedPath;
    let precachedDoc;

    const completer = module.exports = Object.create(baseLanguageHandler);

    completer.handlesLanguage = function(language) {
        return true;
    };

    completer.handlesEditor = function() {
        return this.HANDLES_ANY;
    };

    completer.getMaxFileSizeSupported = function() {
        return 1000 * 1000;
    };

    function frequencyAnalyzer(path, text, identDict, fileDict) {
        const identifiers = text.split(/[^a-zA-Z_0-9\$]+/);
        for (let i = 0; i < identifiers.length; i++) {
            const ident = identifiers[i];
            if (!ident)
                continue;

            if (Object.prototype.hasOwnProperty.call(identDict, ident)) {
                identDict[ident]++;
                fileDict[ident][path] = true;
            }
            else {
                identDict[ident] = 1;
                fileDict[ident] = {};
                fileDict[ident][path] = true;
            }
        }
        return identDict;
    }

    function removeDocumentFromCache(path) {
        const analysis = analysisCache[path];
        if (!analysis) return;

        for (const id in analysis) {
            globalWordIndex[id] -= analysis[id];
            delete globalWordFiles[id][path];
            if (globalWordIndex[id] === 0) {
                delete globalWordIndex[id];
                delete globalWordFiles[id];
            }
        }
        delete analysisCache[path];
    }

    function analyzeDocument(path, allCode) {
        if (!analysisCache[path]) {
            if (allCode.size > 80 * 10000) {
                delete analysisCache[path];
                return;
            }
            // Delay this slightly, because in Firefox document.value is not immediately filled
            analysisCache[path] = frequencyAnalyzer(path, allCode, {}, {});
            // may be a bit redundant to do this twice, but alright...
            frequencyAnalyzer(path, allCode, globalWordIndex, globalWordFiles);
        }
    }

    completer.onDocumentOpen = function(path, doc, oldPath, callback) {
        if (!analysisCache[path]) {
            analyzeDocument(path, doc.getValue());
        }
        callback();
    };

    completer.addDocument = function(path, value) {
        if (!analysisCache[path]) {
            analyzeDocument(path, value);
        }

    };

    completer.onDocumentClose = function(path, callback) {
        removeDocumentFromCache(path);
        if (path == precachedPath)
            precachedDoc = null;
        callback();
    };

    completer.analyze = function(doc, ast, callback, minimalAnalysis) {
        if (precachedDoc && this.path !== precachedPath) {
            removeDocumentFromCache(precachedPath);
            analyzeDocument(precachedPath, precachedDoc);
            precachedDoc = null;
        }
        precachedPath = this.path;
        precachedDoc = doc;
        callback();
    };

    completer.complete = function(editor, fullAst, pos, currentNode, callback) {

        const doc = editor.getSession();
        const line = doc.getLine(pos.row);
        const identifier = completeUtil.retrievePrecedingIdentifier(line, pos.column, this.$getIdentifierRegex());
        const identDict = globalWordIndex;

        const allIdentifiers = [];
        for (const ident in identDict) {
            allIdentifiers.push(ident);
        }
        let matches = completeUtil.findCompletions(identifier, allIdentifiers);

        const currentPath = this.path;
        matches = matches.filter(function(m) {
            return !globalWordFiles[m][currentPath];
        });

        matches = matches.slice(0, 100); // limits results for performance

        callback(matches.filter(function(m) {
            return !m.match(/^[0-9$_\/]/);
        }).map(function(m) {
            const path = Object.keys(globalWordFiles[m])[0] || "[unknown]";
            const pathParts = path.split("/");
            const foundInFile = pathParts[pathParts.length-1];
            return {
                name: m,
                value: m,
                icon: null,
                score: identDict[m],
                meta: foundInFile,
                priority: 0,
                isGeneric: true
            };
        }));
    };

    completer.getCompletions=function (editor, session, pos, prefix, callback) {
        let completions = null;
        const _c = function(_completions){
            completions = _completions;
        };
        completer.complete(editor,null,pos,null,_c);
        callback(null,completions);
    };

    return declare("xide.views._AceMultiDocs",null,{

        didAddMCompleter:false,
        multiFileCompleter:null,
        addFileCompleter:function(){
            let compl = null;
            if(!this.didAddMCompleter) {
                compl = completer;
                this.multiFileCompleter= compl;
                const langTools = ace.require("ace/ext/language_tools");
                langTools.addCompleter(compl);
                this.didAddMCompleter=true;
            }
            return compl;
        }
    });
});