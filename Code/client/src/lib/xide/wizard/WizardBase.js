define([
    'dcl/dcl',
    'xide/utils',
    'xide/factory',
    'xide/_base/_Widget',
    'xide/layout/_TabContainer',
    "xide/wizard/WizardBase"
], function (dcl,utils, factory,_XWidget,_TabContainer) {

    const DefaultEvents = [
        'onInit',
        'onShow',
        'onNext',
        'onPrevious',
        'onFirst',
        'onLast',
        'onBack',
        'onFinish',
        'onTabChange',
        'onTabClick',
        'onTabShow'
    ];

    return dcl([_XWidget],{
        containerClass:_TabContainer,
        containerArgs:{
            direction:'above',
            navBarClass:'',
            resizeToParent:true
        },
        options:null,
        paneClass:null,
        panes:[],
        buttons:[],
        wizard:null,
        $wizard:null,
        buttonBar:null,
        templateString:'<div attachTo="domNode" style="height: inherit;position: relative">' +
        '<div attachTo="containerNode" style="height: auto;min-height: 300px"></div>'+
        '<div style="min-height:initial;position: absolute" attachTo="buttonBar" class="navbar bg-opaque navbar-fixed-bottom"></div>'+
        '</div>',
        getDefaultOptions:function(){

            const events = this.events || DefaultEvents;

            //result
            const options = {};

            //handler, emits & calls instance method if aviable
            function handler(eventKey,args){
                this._emit(eventKey,args);
                _.isFunction(this[eventKey]) && this[eventKey].apply(this,args);
            }

            const self = this;
            _.each(events,function(event){
                options[event]=function(){
                    handler.apply(self,[event,arguments]);
                }
            });
            return options;
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Stub functions, being overriden by the actual implementation of  https://github.com/VinceG/twitter-bootstrap-wizard
        //
        next:function(){},
        previous:function(){},
        first:function(){},
        last:function(){},
        finish:function(){},
        back:function(){},
        currentIndex:function(){},
        firstIndex:function(){},
        lastIndex:function(){},
        getIndex:function(){},
        nextIndex:function(){},
        previousIndex:function(){},
        navigationLength:function(){},
        activeTab:function(){},
        nextTab:function(){},
        previousTab:function(){},
        show:function(){},
        hide:function(){},
        display:function(){},
        enable:function(){},
        disable:function(){},
        remove:function(){},
        resetWizard:function(){},
        onWidgetAdded:function(what){
            this.add(what,null,false);
            this.resize();
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Added API
        //
        disableButton:function(selector,disabled){
            const btn = $(this.domNode).find(selector);
            btn.toggleClass('disabled', disabled);
        },
        hideButton:function(selector,hidden){
            $(this.domNode).find(selector).toggleClass('hidden', hidden);
        },
        onTabShow:function(a,b,index){
            const pane = this.panes[index];
            if(pane){
                if(!pane.widget) {
                    const parent = pane.container;
                    if (pane.create) {

                        const what =pane.create.apply(this, [parent, pane]);
                        this.onWidgetAdded(what);
                    }
                }
                pane.onShow && pane.onShow.apply(this,[pane]);
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  STD API
        //
        startup:function(){
            
            const container = utils.addWidget(this.containerClass, this.containerArgs, this, this.$containerNode[0], true);
            
            this.add(container);
            
            const self = this;

            _.each(this.panes, function (pane) {
                pane.container = container._createTab(this.paneClass || pane.paneClass, pane);
            }, this);

            const buttonBarParent = this.buttonParent || this.$buttonBar[0];
            this.buttons && _.each(this.buttons, function (button) {
                if(!button.widget) {
                    button.widget = factory.createSimpleButton(button.title, button.icon, button.cls, button.mixin, buttonBarParent);
                    button.name && $(button.widget).attr('name', button.name);
                    button.value && $(button.widget).attr('value', button.value);
                }
            }, this);

            const options = utils.mixin(this.getDefaultOptions(),this.options);
            const wizard = this.$domNode.bootstrapWizard(options);
            this.$wizard = wizard;
            this.wizard = wizard.data('bootstrapWizard');

            this.options = options;
            utils.mixin(this,this.wizard);

            this.buttons && _.each(this.buttons, function (button) {
                $(button.widget).toggleClass('disabled', button.disabled);
            });

            this.resize();
        }

    });
});
