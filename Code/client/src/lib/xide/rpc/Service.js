define([
    "dojo/_base/kernel",
    "dojo/_base/lang",
    "dojo/_base/xhr",
    "dojo/_base/declare",
    "xide/rpc/AdapterRegistry",
    "dojo/_base/url",
    "xide/utils",
    "xide/lodash"
], function(dojo,lang,xhr,declare,AdapterRegistry,url,utils,_){
    const transportRegistry = new AdapterRegistry(true);
    const envelopeRegistry = new AdapterRegistry(true);
    let _nextId  = 1;
    const _sync = false;

    function getTarget(smd, method){
        let dest=smd._baseUrl;
        if(smd.target){
            dest = new dojo._Url(dest,smd.target) + '';
        }
        if(method.target){
            dest = new dojo._Url(dest,method.target) + '';
        }
        return dest;
    }

    function toOrdered(parameters, args){
        if(dojo.isArray(args)){ return args; }
        const data=[];
        for(let i=0;i<parameters.length;i++){
            data.push(args[parameters[i].name]);
        }
        return data;
    }

    const service = declare("xide.rpc.Service", null, {
        constructor: function(smd, options){

            // summary:
            //		Take a string as a url to retrieve an smd or an object that is an smd or partial smd to use
            //		as a definition for the service
            // description:
            //		dojox.rpc.Service must be loaded prior to any plugin services like dojox.rpc.Rest
            //		dojox.rpc.JsonRpc in order for them to register themselves, otherwise you get
            //		a "No match found" error.
            // smd: object
            //		Takes a number of properties as kwArgs for defining the service.  It also
            //		accepts a string.  When passed a string, it is treated as a url from
            //		which it should synchronously retrieve an smd file.  Otherwise it is a kwArgs
            //		object.  It accepts serviceUrl, to manually define a url for the rpc service
            //		allowing the rpc system to be used without an smd definition. strictArgChecks
            //		forces the system to verify that the # of arguments provided in a call
            //		matches those defined in the smd.  smdString allows a developer to pass
            //		a jsonString directly, which will be converted into an object or alternatively
            //		smdObject is accepts an smdObject directly.

            let url;
            var self = this;
            const singleton = options ? options.singleton : false;
            function processSmd(smd){
                smd._baseUrl = new dojo._Url((dojo.isBrowser ? location.href : dojo.config.baseUrl) ,url || '.') + '';
                self._smd = smd;
                if(options && options.services==='methods'){
                    smd.services = smd.methods;
                    delete smd.methods;
                    smd.transport = "POST";
                    if(options.mixin){
                        lang.mixin(smd,options.mixin);
                    }
                    options = null;
                }

                //generate the methods
                for(const serviceName in self._smd.services){
                    const pieces = serviceName.split("."); // handle "namespaced" services by breaking apart by .
                    let current = self;
                    for(let i=0; i< pieces.length-1; i++){
                        // create or reuse each object as we go down the chain
                        current = current[pieces[i]] || (current[pieces[i]] = {});
                    }
                    current[pieces[pieces.length-1]]=	self._generateService(serviceName, self._smd.services[serviceName]);
                }
            }
            if(smd){
                //ifthe arg is a string, we assume it is a url to retrieve an smd definition from
                if( (_.isString(smd)) || (smd instanceof dojo._Url)){
                    if(smd instanceof dojo._Url){
                        url = smd + "";
                    }else{
                        url = smd;
                    }

                    this.__init = xhr.getText(url);
                    var self = this;
                    this.__init.then(function(data){
                        processSmd(utils.fromJson(data));
                    });
                    /*

                    if(!text){
                        throw new Error("Unable to load SMD from " + smd);
                    }else{
                        processSmd(utils.fromJson(text));
                    }
                    */

                }else{
                    processSmd(smd);
                }
            }
            this._options = (options ? options : {});
            this._requestId = 0;
        },

        _generateService: function(serviceName, method){
            if(this[method]){
                throw new Error("WARNING: "+ serviceName+ " already exists for service. Unable to generate function");
            }
            method.name = serviceName;

            let func = dojo.hitch(this, "_executeMethod",method);

            const transport = transportRegistry.match(method.transport || this._smd.transport);
            if(transport.getExecutor){
                func = transport.getExecutor(func,method,this);
            }
            const schema = method.returns || (method._schema = {}); // define the schema
            const servicePath = '/' + serviceName +'/';
            // schemas are minimally used to track the id prefixes for the different services
            schema._service = func;
            func.servicePath = servicePath;
            func._schema = schema;
            func.id = _nextId++;
            return func;
        },
        _getRequest: function(method,args){
            const smd = this._smd;
            const envDef = envelopeRegistry.match(method.envelope || smd.envelope || "NONE");
            const parameters = (method.parameters || []).concat(smd.parameters || []);
            if(envDef.namedParams){
                // the serializer is expecting named params
                if((args.length==1) && dojo.isObject(args[0])){
                    // looks like we have what we want
                    args = args[0];
                }else{
                    // they provided ordered, must convert
                    const data={};
                    for(var i=0;i<method.parameters.length;i++){
                        if(typeof args[i] != "undefined" || !method.parameters[i].optional){
                            data[method.parameters[i].name]=args[i];
                        }
                    }
                    args = data;
                }
                if(method.strictParameters||smd.strictParameters){
                    //remove any properties that were not defined
                    for(i in args){
                        let found=false;
                        for(let j=0; j<parameters.length;j++){
                            if(parameters[j].name==i){ found=true; }
                        }
                        if(!found){
                            delete args[i];
                        }
                    }

                }
                // setting default values
                for(i=0; i< parameters.length; i++){
                    const param = parameters[i];
                    if(!param.optional && param.name && !args[param.name]){
                        if(param["default"]){
                            args[param.name] = param["default"];
                        }else if(!(param.name in args)){
                            throw new Error("Required parameter " + param.name + " was omitted");
                        }
                    }
                }
            }else if(parameters && parameters[0] && parameters[0].name && (args.length==1) && dojo.isObject(args[0])){
                // looks like named params, we will convert
                if(envDef.namedParams === false){
                    // the serializer is expecting ordered params, must be ordered
                    args = toOrdered(parameters, args);
                }else{
                    // named is ok
                    args = args[0];
                }
            }

            if(dojo.isObject(this._options)){
                args = dojo.mixin(args, this._options);
            }
            delete args['mixin'];

            const schema = method._schema || method.returns; // serialize with the right schema for the context;
            const request = envDef.serialize.apply(this, [smd, method, args]);
            request._envDef = envDef;// save this for executeMethod
            const contentType = (method.contentType || smd.contentType || request.contentType);

            // this allows to mandate synchronous behavior from elsewhere when necessary, this may need to be changed to be one-shot in FF3 new sync handling model
            return dojo.mixin(request, {
                sync: _sync,
                contentType: contentType,
                headers: method.headers || smd.headers || request.headers || {},
                target: request.target || getTarget(smd, method),
                transport: method.transport || smd.transport || request.transport,
                envelope: method.envelope || smd.envelope || request.envelope,
                timeout: method.timeout || smd.timeout,
                callbackParamName: method.callbackParamName || smd.callbackParamName,
                rpcObjectParamName: method.rpcObjectParamName || smd.rpcObjectParamName,
                schema: schema,
                handleAs: request.handleAs || "auto",
                preventCache: method.preventCache || smd.preventCache,
                frameDoc: this._options.frameDoc || undefined
            });
        },
        _executeMethod: function(method){
            const args = [];
            let i;
            for(i=1; i< arguments.length; i++){
                args.push(arguments[i]);
            }
            const request = this._getRequest(method,args);
            const deferred = transportRegistry.match(request.transport).fire(request);

            deferred.addBoth(function(results){
                return request._envDef.deserialize.call(this,results);
            });
            return deferred;
        }
});

    service.transportRegistry = transportRegistry;
    service.envelopeRegistry = envelopeRegistry;
    service._nextId = _nextId;
    service.getTarget = getTarget;
    service.toOrdered= toOrdered;
    service._sync = _sync;
    envelopeRegistry.register("URL", function(str){ return str == "URL"; },{
		serialize:function(smd, method, data ){
			const d = dojo.objectToQuery(data);
			return {
				data: d,
				transport:"POST"
			};
		},
		deserialize:function(results){
			return results;
		},
		namedParams: true
	});
    envelopeRegistry.register("JSON",function(str){ return str == "JSON"; },{
        serialize: function(smd, method, data){
            const d = dojo.toJson(data);

            return {
                data: d,
                handleAs: 'json',
                contentType : 'application/json'
            };
        },
        deserialize: function(results){
            return results;
        }
    });
    envelopeRegistry.register("PATH",function(str){ return str == "PATH"; },{
        serialize:function(smd, method, data){
			let i;
			let target = getTarget(smd, method);
			if(dojo.isArray(data)){
				for(i = 0; i < data.length;i++){
					target += '/' + data[i];
				}
			}else{
				for(i in data){
					target += '/' + i + '/' + data[i];
				}
			}
			return {
				data:'',
				target: target
			};
		},
		deserialize:function(results){
			return results;
		}
	});
    //post is registered first because it is the default;
    transportRegistry.register("POST",function(str){ return str == "POST"; },{
		fire:function(r){
			r.url = r.target;
			r.postData = r.data;
			return dojo.rawXhrPost(r);
		}
	});
    transportRegistry.register("GET",function(str){ return str == "GET"; },{
		fire: function(r){
			r.url=  r.target + (r.data ? '?' + ((r.rpcObjectParamName) ? r.rpcObjectParamName + '=' : '') + r.data : '');
			return xhr.get(r);
		}
	});
    //only works ifyou include dojo.io.script
    /*
    transportRegistry.register("JSONP",function(str){ return str == "JSONP"; },{
        fire: function(r){
            r.url = r.target + ((r.target.indexOf("?") == -1) ? '?' : '&') + ((r.rpcObjectParamName) ? r.rpcObjectParamName + '=' : '') + r.data;
            r.callbackParamName = r.callbackParamName || "callback";
            return dojo.io.script.get(r);
        }
    });*/
    if(dojo._contentHandlers) {
        dojo._contentHandlers.auto = function (xhr) {
            // automatically choose the right handler based on the returned content type
            const handlers = dojo._contentHandlers;
            const retContentType = xhr.getResponseHeader("Content-Type");
            const results = !retContentType ? handlers.text(xhr) :
                retContentType.match(/\/.*json/) ? handlers.json(xhr) :
                    retContentType.match(/\/javascript/) ? handlers.javascript(xhr) :
                        retContentType.match(/\/xml/) ? handlers.xml(xhr) : handlers.text(xhr);
            return results;
        };
    }
    return service;
});
