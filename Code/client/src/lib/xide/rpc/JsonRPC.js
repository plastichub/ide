define([
	"./Service",
    "dojo/errors/RequestError",
	"xide/utils/StringUtils"
], function(Service, RequestError,utils){
	function jsonRpcEnvelope(version){
		return {
			serialize: function(smd, method, data, options){
				//not converted to json it self. This  will be done, if
				//appropriate, at the transport level
	
				const d = {
					id: this._requestId++,
					method: method.name,
					params: data
				};
				if(version){
					d.jsonrpc = version;
				}
				return {
					data: JSON.stringify(d),
					handleAs:'json',
					contentType: 'application/json',
					transport:"POST"
				};
			},
			deserialize: function(obj){
				if ('Error' == obj.name // old xhr
					|| obj instanceof RequestError // new xhr
				){
					obj = utils.fromJson(obj.responseText);
				}
				if(obj.error) {
					const e = new Error(obj.error.message || obj.error);
					e._rpcErrorObject = obj.error;
					return e;
				}
				return obj.result;
			}
		};
	}
    Service.envelopeRegistry.register(
		"JSON-RPC-1.0",
		function(str){
			return str == "JSON-RPC-1.0";
		},
		utils.mixin({namedParams:false}, jsonRpcEnvelope()) // 1.0 will only work with ordered params
	);
    Service.envelopeRegistry.register(
		"JSON-RPC-2.0",
		function(str){
			return str == "JSON-RPC-2.0";
		},
        utils.mixin({namedParams:true }, jsonRpcEnvelope("2.0")) // 2.0 supports named params
	);

});
