/**
 * module:xide/interface/Route
 **/
define(['dcl/dcl','xdojo/declare'], function (dcl,declare) {
    /**
     * Rough interface to be implemented by sub class in order to collect a view
     * in a history.
     * @interface module:xide/interface/Track
     */
    const Interface = {
        declaredClass:'xide.interface.Route',
        /**
         * @param {module:xide/interface/RouteDefinition} route
         */
        handleRoute:function(route){},
        /**
         * @param {module:xide/interface/RouteDefinition} route
         */
        routeToReference:function(route){},
        /**
         * @param {module:xide/interface/RouteDefinition} route
         */
        getRouteRenderParams:function(route){},
        /**
         * @param {module:xide/interface/RouteDefinition} route
         */
        getRouteActions:function(route){}
    };
    const Module = declare('xide.interface.Route',null,Interface);
    Module.dcl = dcl(null,Interface);
    return Module;
});