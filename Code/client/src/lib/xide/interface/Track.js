/** module:xide/interface/Track **/
define(['dcl/dcl','xdojo/declare'], function (dcl,declare) {
    /**
     * Rough interface to be implemented by sub class in order to collect a view
     * in a history.
     * @interface module:xide/interface/Track
     */
    const Interface = {
        declaredClass:'xide.interface.Track',
        getTrackingGroup:function(){},
        getTrackingPath:function(){},
        track:function(){return false;},
        registerUrls:function(){}
    };
    const Module = declare('xide.interface.Track',null,Interface);
    Module.dcl = dcl(null,Interface);
    return Module;
});