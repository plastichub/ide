/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry',
    'xide/_base/_Widget',
    "dojo/text!./Navbar.html",
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    'xide/action/DefaultActions',
    "xaction/ActionProvider",
    "xgrid/Clipboard",
    "xide/model/Path",
    'xide/action/Action'

], function (declare,dcl,types,utils,TestUtils,_Widget,module,registry,_XWidget,MenuBarTemplate,ActionMixin,ActionContext,ActionStore,DefaultActions,ActionProvider,Clipboard,Path,Action){
    /**
     *
     * description
     */


    var ACTION = types.ACTION;
    var options = {
        fadeSpeed: 100,
        filter: function ($obj) {
            // Modify $obj, Do not return
        },
        above: 'auto',
        left: 'auto',
        preventDoubleContext: true,
        compress: false
    };

    function initialize(opts) {

        options = $.extend({}, options, opts);

        $(document).on('click', function () {
            $('.dropdown-context').fadeOut(options.fadeSpeed, function(){
                $('.dropdown-context').css({display:''}).find('.drop-left').removeClass('drop-left');
            });
        });
        if(options.preventDoubleContext){
            $(document).on('contextmenu', '.dropdown-context', function (e) {
                e.preventDefault();
            });
        }
        $(document).on('mouseenter', '.dropdown-submenu', function(){
            var $sub = $(this).find('.dropdown-context-sub:first'),
                subWidth = $sub.width(),
                subLeft = $sub.offset().left,
                collision = (subWidth+subLeft) > window.innerWidth;
            if(collision){
                $sub.addClass('drop-left');
            }
        });

    }
    function updateOptions(opts){
        options = $.extend({}, options, opts);
    }
    function buildMenu(data, id, subMenu) {
        //console.log('build menu',arguments);
        var subClass = (subMenu) ? ' dropdown-context-sub' : '',
            compressed = options.compress ? ' compressed-context' : '',
            $menu = $('<ul class="dropdown-menu dropdown-context' + subClass + compressed +'" id="dropdown-' + id + '"></ul>');

        return buildMenuItems($menu, data, id, subMenu);
    }
    function buildMenuItems($menu, data, id, subMenu, addDynamicTag) {
        var linkTarget = '';
        for(var i = 0; i<data.length; i++) {

            var item = data[i];

            if (typeof data[i].divider !== 'undefined') {
                var divider = '<li class="divider';
                divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                divider += '"></li>';
                $menu.append(divider);
            } else if (typeof data[i].header !== 'undefined') {
                var header = '<li class="nav-header';
                header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                header += '">' + data[i].header + '</li>';
                $menu.append(header);
            } else if (typeof data[i].menu_item_src !== 'undefined') {
                var funcName;
                if (typeof data[i].menu_item_src === 'function') {
                    if (data[i].menu_item_src.name === "") { // The function is declared like "foo = function() {}"
                        for (var globalVar in window) {
                            if (data[i].menu_item_src == window[globalVar]) {
                                funcName = globalVar;
                                break;
                            }
                        }
                    } else {
                        funcName = data[i].menu_item_src.name;
                    }
                } else {
                    funcName = data[i].menu_item_src;
                }
                $menu.append('<li class="dynamic-menu-src" data-src="' + funcName + '"></li>');
            } else {

                if (typeof data[i].href == 'undefined') {
                    data[i].href = '#';
                }
                if (typeof data[i].target !== 'undefined') {
                    linkTarget = ' target="'+data[i].target+'"';
                }
                if (typeof data[i].subMenu !== 'undefined') {
                    var sub_menu = '<li class="dropdown-submenu';
                    sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    sub_menu += '"><a tabindex="-1" href="' + data[i].href + '">';

                    if (typeof item.icon !== 'undefined') {
                        sub_menu+= '<span class=" ' + item.icon + '"></span> ';

                    }

                    sub_menu += data[i].text + '';
                    sub_menu+='</a></li>';
                    $sub = (sub_menu);
                } else {
                    var element = '<li';
                    element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                    element += '><a tabindex="-1" href="' + data[i].href + '"'+linkTarget+'>';
                    if (typeof data[i].icon !== 'undefined') {
                        element += '<span class="glyphicon ' + data[i].icon + '"></span> ';

                    }
                    element += data[i].text + '</a></li>';
                    $sub = $(element);
                }
                if (typeof data[i].action !== 'undefined') {
                    $action = data[i].action;
                    $sub
                        .find('a')
                        .addClass('context-event')
                        .on('click', createCallback($action));
                }
                $menu.append($sub);
                if (typeof data[i].subMenu != 'undefined') {
                    var subMenuData = buildMenu(data[i].subMenu, id, true);
                    $menu.find('li:last').append(subMenuData);
                }
            }
            if (typeof options.filter == 'function') {
                options.filter($menu.find('li:last'));
            }
        }
        return $menu;
    }
    function addContext(selector, data) {

        if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
            var id = data.id;
            $menu = $('body').find('#dropdown-' + id)[0];
            if (typeof $menu === 'undefined') {
                $menu = buildMenu(data.data, id);
                $('body').append($menu);
            }
        } else {
            var d = new Date(),
                id = d.getTime(),
                $menu = buildMenu(data, id);
            $('body').append($menu);
        }


        $(selector).on('contextmenu', function (e) {
            e.preventDefault();
            e.stopPropagation();

            currentContextSelector = $(this);


            $('.dropdown-context:not(.dropdown-context-sub)').hide();

            $dd = $('#dropdown-' + id);

            $dd.find('.dynamic-menu-item').remove(); // Destroy any old dynamic menu items
            $dd.find('.dynamic-menu-src').each(function(idx, element) {
                var menuItems = window[$(element).data('src')]($(selector));
                $parentMenu = $(element).closest('.dropdown-menu.dropdown-context');
                $parentMenu = buildMenuItems($parentMenu, menuItems, id, undefined, true);
            });

            if (typeof options.above == 'boolean' && options.above) {
                $dd.addClass('dropdown-context-up').css({
                    top: e.pageY - 20 - $('#dropdown-' + id).height(),
                    left: e.pageX - 13
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.above == 'string' && options.above == 'auto') {
                $dd.removeClass('dropdown-context-up');
                var autoH = $dd.height() + 12;
                if ((e.pageY + autoH) > $('html').height()) {
                    $dd.addClass('dropdown-context-up').css({
                        top: e.pageY - 20 - autoH,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                } else {
                    $dd.css({
                        top: e.pageY + 10,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                }
            }

            if (typeof options.left == 'boolean' && options.left) {
                $dd.addClass('dropdown-context-left').css({
                    left: e.pageX - $dd.width()
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.left == 'string' && options.left == 'auto') {
                $dd.removeClass('dropdown-context-left');
                var autoL = $dd.width() - 12;
                if ((e.pageX + autoL) > $('html').width()) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width() + 13
                    });
                }
            }
        });

        return $menu;
    }
    function destroyContext(selector) {
        $(document).off('contextmenu', selector).off('click', '.context-event');
    }

    var createCallback = function(func) {
        return function(event) { func(event, currentContextSelector) };
    }

    function addContext2(selector, data) {

        var id,
            $menu;

        if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
            id = data.id;
            $menu = $('body').find('#dropdown-' + id)[0];
            if (typeof $menu === 'undefined') {
                $menu = buildMenu(data.data, id);
                selector.append($menu);
            }
        } else {
            var d = new Date();

            id = d.getTime();
            $menu = buildMenu(data, id);
            selector.append($menu);

        }

        return $menu;

        $(selector).on('contextmenu', function (e) {


            var currentContextSelector = $(this);


            $('.dropdown-context:not(.dropdown-context-sub)').hide();

            $dd = $('#dropdown-' + id);

            $dd.find('.dynamic-menu-item').remove(); // Destroy any old dynamic menu items
            $dd.find('.dynamic-menu-src').each(function(idx, element) {
                var menuItems = window[$(element).data('src')]($(selector));
                $parentMenu = $(element).closest('.dropdown-menu.dropdown-context');
                $parentMenu = buildMenuItems($parentMenu, menuItems, id, undefined, true);
            });

            if (typeof options.above == 'boolean' && options.above) {
                $dd.addClass('dropdown-context-up').css({
                    top: e.pageY - 20 - $('#dropdown-' + id).height(),
                    left: e.pageX - 13
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.above == 'string' && options.above == 'auto') {
                $dd.removeClass('dropdown-context-up');
                var autoH = $dd.height() + 12;
                if ((e.pageY + autoH) > $('html').height()) {
                    $dd.addClass('dropdown-context-up').css({
                        top: e.pageY - 20 - autoH,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                } else {
                    $dd.css({
                        top: e.pageY + 10,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                }
            }

            if (typeof options.left == 'boolean' && options.left) {
                $dd.addClass('dropdown-context-left').css({
                    left: e.pageX - $dd.width()
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.left == 'string' && options.left == 'auto') {
                $dd.removeClass('dropdown-context-left');
                var autoL = $dd.width() - 12;
                if ((e.pageX + autoL) > $('html').width()) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width() + 13
                    });
                }
            }

        });

        return $menu;
    }

    console.clear();

    var ContextMenu = dcl(_Widget.dcl,{
        target:null,
        constructor:function(options,node){
            this.target = node;
        },
        init: initialize,
        settings: function(){
        },
        attach: function(selector,data){
            this.target = selector;
            this.menu = addContext2(selector,data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
            return this.menu;
        },
        destroy: function(){
            try {

                if(this.domNode) {
                    registry.remove(this.domNode.id);
                }
                destroyContext(this.target);
                utils.destroy(this.domNode);
                this.inherited(arguments);
            }catch(e){
                //logError(e);
            }
        }
    });

    var MenuBar = dcl(_XWidget,{
        templateString:MenuBarTemplate
    });

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    //action source class
    var ActionProviderClass = dcl([ActionProvider.dcl],{
        getItemsAtBranch:function(items, path) {
            return new Path(path).getChildren(_.pluck(items,'command'),false);
        },
        permissions:[
            ACTION.EDIT,
            ACTION.COPY,
            ACTION.MOVE,
            ACTION.RENAME,
            ACTION.DOWNLOAD,
            ACTION.RELOAD,
            ACTION.DELETE,
            ACTION.NEW_FILE,
            ACTION.NEW_DIRECTORY,
            ACTION.CLIPBOARD,
            ACTION.CLIPBOARD_COPY,
            ACTION.CLIPBOARD_PASTE,
            ACTION.CLIPBOARD_CUT
        ]
    });

    //action source class instance
    var ActionProviderInstance = new ActionProviderClass({

    });

    //action renderer class
    var ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        visibility: types.ACTION_VISIBILITY.MAIN_MENU,
        templateString:'<div>'+
            '<nav attachTo="navigation" class="navbar navbar-default navbar-static-top marginBottom-0" role="navigation">'+

                    '<ul attachTo="navBar" class="nav navbar-nav">'+
                    '</ul>'+

            '</nav>'+
        '</div>',

        templateString_:'<div>'+
            '<nav attachTo="navigation" class="navbar navbar-default navbar-static-top marginBottom-0" role="navigation">'+

                    '<ul attachTo="navBar" class="nav navbar-nav">'+
                        '<li attachTo="first" class="dropdown">' +
                            '<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>'+
                    '</ul>'+

            '</nav>'+
        '</div>'
    });

    function doTests_start(tab){

        var doContextMenu = false;
        var doMenuBar = false;
        var doMenu = true;
        if(doMenu){


            var _defaultActions = DefaultActions.getDefaultActions(ActionProviderInstance.permissions, ActionProviderInstance,ActionProviderInstance);

            //console.dir(_defaultActions);
            ActionProviderInstance.addActions(_defaultActions);


            var container = utils.addWidget(ContainerClass,{

            },null,tab.containerNode,true);

            var context = new ContextMenu({}, tab.containerNode);
            tab.add(context, null, false);
            context.init({preventDoubleContext: true});
            var items = [
                {
                    text: 'Single',
                    icon: 'fa-play'
                },
                {divider: true},
                {header: 'Download'},
                {
                    text: 'The Script',
                    icon: 'fa-play',
                    subMenu: [
                        {header: 'Requires jQuery'},
                        {
                            text: 'context.js',
                            href: 'http://lab.jakiestfu.com/contextjs/context.js',
                            target: '_blank',
                            action: function (e) {
                                _gaq.push(['_trackEvent', 'ContextJS Download', this.pathname, this.innerHTML]);
                            }
                        }
                    ]
                },
                {divider: true},
                {header: 'Meta'},
                {
                    text: 'The Author', subMenu: [
                    {header: '@jakiestfu'},
                    {text: 'Website', href: 'http://jakiestfu.com/', target: '_blank'},
                    {text: 'Forrst', href: 'http://forrst.com/people/jakiestfu', target: '_blank'},
                    {text: 'Twitter', href: 'http://twitter.com/jakiestfu', target: '_blank'},
                    {
                        text: 'Donate?', action: function (e) {
                        e.preventDefault();
                        $('#donate').submit();
                    }
                    }
                ]
                }
            ];
            var menu = context.attach(container.$first,items);

            //////////////////////////////////////////////////

            var self = this,
                store = ActionProviderInstance.getActionStore(),
                allActions = store.query();

            //return all actions with non-empty tab field
            var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
                groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),

            //now flatten them
                _actionsFlattened = [];

            //console.dir(allActions);

            ///return;

            _.each(groupedTabs,function(items,name){
                _actionsFlattened = _actionsFlattened.concat(items);
                //console.log('group : ' + name,items);
            });

            //@type {string []} | example : ['File','Edit','View']
            var rootActions = [];
            _.each(tabbedActions,function(action){
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            var _debug = true;
            if(_debug){
                _.each(rootActions,function(action) {
                    console.log('   action ' + action);
                });
            }




            //console.dir(groupedTabs);


        }

        if(doContextMenu) {
            var context = new ContextMenu({}, tab.containerNode);
            tab.add(context, null, false);
            context.init({preventDoubleContext: true});
            context.attach($(tab.containerNode), [
                {
                    text: 'Single',
                    icon: 'fa-play'
                },
                {divider: true},
                {header: 'Download'},
                {
                    text: 'The Script',
                    icon: 'fa-play',
                    subMenu: [
                        {header: 'Requires jQuery'},
                        {
                            text: 'context.js',
                            href: 'http://lab.jakiestfu.com/contextjs/context.js',
                            target: '_blank',
                            action: function (e) {
                                _gaq.push(['_trackEvent', 'ContextJS Download', this.pathname, this.innerHTML]);
                            }
                        }
                    ]
                },
                {divider: true},
                {header: 'Meta'},
                {
                    text: 'The Author', subMenu: [
                    {header: '@jakiestfu'},
                    {text: 'Website', href: 'http://jakiestfu.com/', target: '_blank'},
                    {text: 'Forrst', href: 'http://forrst.com/people/jakiestfu', target: '_blank'},
                    {text: 'Twitter', href: 'http://twitter.com/jakiestfu', target: '_blank'},
                    {
                        text: 'Donate?', action: function (e) {
                        e.preventDefault();
                        $('#donate').submit();
                    }
                    }
                ]
                }
            ]);

        }

        if(doMenuBar) {
            var bar = utils.addWidget(MenuBar, {
                resizeToParent: true
            }, null, tab, true);
            tab.add(bar, null, false);
            $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).parent().siblings().removeClass('open');
                $(this).parent().toggleClass('open');
            });
            //var menuBar = new MenuBar({});
            //menuBar.startup();
        }


    }
    function doTests(tab){

        var doContextMenu = false;
        var doMenuBar = false;
        var doMenu = true;
        if(doMenu){


            var _defaultActions = DefaultActions.getDefaultActions(ActionProviderInstance.permissions, ActionProviderInstance,ActionProviderInstance);

            //console.dir(_defaultActions);
            ActionProviderInstance.addActions(_defaultActions);


            var container = utils.addWidget(ContainerClass,{

            },null,tab.containerNode,true);

            var context = new ContextMenu({}, tab.containerNode);
            tab.add(context, null, false);
            context.init({preventDoubleContext: true});
            var items = [
                {
                    text: 'Single',
                    icon: 'fa-play'
                },
                {divider: true},
                {header: 'Download'},
                {
                    text: 'The Script',
                    icon: 'fa-play',
                    subMenu: [
                        {header: 'Requires jQuery'},
                        {
                            text: 'context.js',
                            href: 'http://lab.jakiestfu.com/contextjs/context.js',
                            target: '_blank',
                            action: function (e) {
                                _gaq.push(['_trackEvent', 'ContextJS Download', this.pathname, this.innerHTML]);
                            }
                        }
                    ]
                },
                {divider: true},
                {header: 'Meta'},
                {
                    text: 'The Author', subMenu: [
                    {header: '@jakiestfu'},
                    {text: 'Website', href: 'http://jakiestfu.com/', target: '_blank'},
                    {text: 'Forrst', href: 'http://forrst.com/people/jakiestfu', target: '_blank'},
                    {text: 'Twitter', href: 'http://twitter.com/jakiestfu', target: '_blank'},
                    {
                        text: 'Donate?', action: function (e) {
                        e.preventDefault();
                        $('#donate').submit();
                    }
                    }
                ]
                }
            ];
            //var menu = context.attach(container.$first,items);

            //////////////////////////////////////////////////

            var self = this,
                store = ActionProviderInstance.getActionStore(),
                allActions = store.query();

                //return all actions with non-empty tab field
                var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
                groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),

            //now flatten them
                _actionsFlattened = [];

            //console.dir(allActions);

            ///return;

            _.each(groupedTabs,function(items,name){
                _actionsFlattened = _actionsFlattened.concat(items);
                //console.log('group : ' + name,items);
            });

            //@type {string []} | example : ['File','Edit','View']
            var rootActions = [];
            _.each(tabbedActions,function(action){
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            var _debug = true;
            if(_debug){
                _.each(rootActions,function(action) {
                    console.log('   action ' + action);
                });
            }

            var rootMap = {};
            var rootItems = [];


            function toActions(paths){
                var result = [];
                _.each(paths,function(path){
                    result.push(getAction(path));
                });
                return result;
            }
            function getAction(command){
                return store.getSync(command);
            }

            function toItem(action){

                var actionVisibility = action.getVisibility != null ? action.getVisibility(container.visibility) : {};
                var label = actionVisibility.label !=null ? actionVisibility.label : action.label;
                var icon = actionVisibility.icon !=null ? actionVisibility.icon : action.icon;
                var item = {
                    text:label,
                    icon:icon
                }

                return item;
            }

            _.each(rootActions,function(level){

                //now add a root item
                /*
                var item = self._addLevel({
                    label:level
                }, self.rootMenu);
                */

                var item =$('<li class="dropdown">' +
                    '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + level +'<b class="caret"></b></a>'+
                    '</li>');

                container.$navBar.append(item);
                rootItems.push(item);
                rootMap[level] = item;


                var dropdown = item;



                var menuActions = ActionProviderInstance.getItemsAtBranch(allActions,level);

                var data = [];



                var allActionPaths = _.pluck(allActions,'command');
                console.dir(allActionPaths);

                _.each(menuActions,function(command){

                    var action = getAction(command);
                    if (!action) {

                        var segments = command.split('/');
                        var lastSegment = segments[segments.length - 1];
                        action = new Action({
                            command: command,
                            label:lastSegment
                        });

                    }
                    if(action){
                        var actionVisibility = action.getVisibility != null ? action.getVisibility(container.visibility) : {};
                        var label = actionVisibility.label !=null ? actionVisibility.label : action.label;
                        var icon = actionVisibility.icon !=null ? actionVisibility.icon : action.icon;

                        var item = toItem(action);
                        data.push(item);


                        var childPaths = new Path(command).getChildren(allActionPaths,false),
                            isContainer = childPaths.length> 0,
                            childActions = isContainer ? toActions(childPaths) : null ;

                        if(childActions){

                            var subs = [];

                            _.each(childActions,function(child){
                                subs.push(toItem(child));
                            });

                            item.subMenu = subs;

                        }

                        if(action.getChildren){



                            //var subActions = action.getChildren();
                            //data.push({divider: true});
                            //console.error('ss',subActions);
                        }

                        //console.dir(subActions);
                        //console.dir(action._store);
                    }
                });



                //console.error('attache');
                var menu = context.attach(dropdown,data);


            });








            //console.dir(groupedTabs);


        }

        if(doContextMenu) {
            var context = new ContextMenu({}, tab.containerNode);
            tab.add(context, null, false);
            context.init({preventDoubleContext: true});
            context.attach($(tab.containerNode), [
                {
                    text: 'Single',
                    icon: 'fa-play'
                },
                {divider: true},
                {header: 'Download'},
                {
                    text: 'The Script',
                    icon: 'fa-play',
                    subMenu: [
                        {header: 'Requires jQuery'},
                        {
                            text: 'context.js',
                            href: 'http://lab.jakiestfu.com/contextjs/context.js',
                            target: '_blank',
                            action: function (e) {
                                _gaq.push(['_trackEvent', 'ContextJS Download', this.pathname, this.innerHTML]);
                            }
                        }
                    ]
                },
                {divider: true},
                {header: 'Meta'},
                {
                    text: 'The Author', subMenu: [
                    {header: '@jakiestfu'},
                    {text: 'Website', href: 'http://jakiestfu.com/', target: '_blank'},
                    {text: 'Forrst', href: 'http://forrst.com/people/jakiestfu', target: '_blank'},
                    {text: 'Twitter', href: 'http://twitter.com/jakiestfu', target: '_blank'},
                    {
                        text: 'Donate?', action: function (e) {
                        e.preventDefault();
                        $('#donate').submit();
                    }
                    }
                ]
                }
            ]);

        }

        if(doMenuBar) {
            var bar = utils.addWidget(MenuBar, {
                resizeToParent: true
            }, null, tab, true);
            tab.add(bar, null, false);
            $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).parent().siblings().removeClass('open');
                $(this).parent().toggleClass('open');
            });
            //var menuBar = new MenuBar({});
            //menuBar.startup();
        }


    }



    var ctx = window.sctx,
        root;

    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);
        doTests(parent);
        return declare('a',null,{});

    }
    return _Widget;

});