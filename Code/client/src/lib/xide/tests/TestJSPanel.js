/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "./TestUtils",
    'xide/_base/_Widget',
    "module",
    "xfile/tests/TestUtils",
    "xide/mixins/EventedMixin",
    "xide/widgets/_Widget",
    'dojo/Deferred'
], function (dcl, declare, types, utils,
             Grid, TestUtils, _Widget, module,FTestUtils,EventedMixin,
             _XWidget,Deferred) {


    console.clear();

    console.log('--do-tests');


    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    ////////////////////////////////////////////////////////////
    //
    //
    var Module = dcl([_XWidget.dcl,EventedMixin.dcl],{
        containerClass:'',
        type: types.DIALOG_TYPE.WARNING,
        size: types.DIALOG_SIZE.SIZE_WIDE,
        defaultOptions:BootstrapDialog.defaultOptions,
        panel:null,
        bodyCSS:null,
        okButtonClass:'btn-danger',
        startDfd:null,
        _ready:false,

        title:'No Title',
        /**
         * jsPanelOptions
         * @link http://beta.jspanel.de/api/#defaults
         * @type {object}
         */
        options:null,
        getInstance:function(args){
            if(this.panel){
                return this.panel;
            }

            var self = this;


            this.panel = $.jsPanel(utils.mixin({
                position: {
                    left: 200,
                    top: 100
                },
                title: self.title || 'jsPanel theme info',
                theme: self.theme || 'bootstrap-default',
                onbeforeclose:function(){
                    self.destroy(false);
                },
                callback: function () {
                    self.domNode = this.content[0];
                    //this.content.css('padding', '10px');
                    //this.content.append$('<div class="widget"/>')
                    self.onshown(this,this.content[0]);
                    //self.startDfd.resolve();
                }
            },args));
            return this.panel;
        },
        onshown:function(panelInstance,content){
            var self = this;
            for (var prop in panelInstance){
                if(_.isFunction(panelInstance[prop]) && !self[prop]){
                    self[prop]=panelInstance[prop];
                }
            }
            if(this.onShow){
                var result = this.onShow(panelInstance,content);
                function ready(what){
                    self.startDfd.resolve(what);
                    self._ready = true;
                }
                if(result && result.then){
                    result.then(ready);
                }else if(_.isArray(result)){
                    _.each(result,function(widget){
                        self.add(widget,null,false);
                    });
                    ready(result);
                }
            }
        },
        onReady:function(){
            var self = this;
            setTimeout(function(){
                self._ready = true;
            },100);

        },
        startup:function(){
            var dlg = this.getInstance();
        },
        destroy:function(destroyPanel){
            destroyPanel !==false && this.panel && this.panel.close();
        },
        show:function(options){
            var self=this;
            if(!this.startDfd){
                this.startDfd = new Deferred();
                this.startDfd.then(function(){
                    self.onReady();
                });
            }
            var panel = this.getInstance(options || this.options);
            return this.startDfd;
        },
        constructor:function(args){
            args = args || {};
            this.options = args.options;
            utils.mixin(this,args);
        },
        onOk:function(){},
        onCancel:function(){}
    });

    dcl.chainAfter(Module, "onReady");
    dcl.chainAfter(Module, "onOk");
    dcl.chainAfter(Module, "onCancel");
    dcl.chainAfter(Module, "resize");

    /////////////////////////////////////////////////////////////////////////////
    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {

        $.jsPanel.defaults.headerControls.iconfont = 'font-awesome';

        var panel = new Module({
            options:{
                "contentSize": {
                    width: '600px',
                    height: '500px'
                }
            },
            onShow:function(panel,contentNode){
                return [FTestUtils.createFileGrid(null,{},null,null,null,true,contentNode)];
            }
        });

        panel.show();

        return;



        /*
        $.jsPanel.defaults = {
            "autoclose": false,
            "callback": false,
            "container": 'body',
            "content": false,
            "contentAjax": false,
            "contentIframe": false,
            "contentOverflow": 'hidden',
            "contentSize": {
                width: 400,
                height: 200
            },
            "custom": false,
            "dblclicks": false,
            "draggable": {
                handle: 'div.jsPanel-hdr, div.jsPanel-ftr',
                opacity: 0.8
            },
            "footerToolbar": false,
            "headerControls": {
                buttons: true,
                iconfont: 'jsglyph',
                close: false,
                maximize: false,
                minimize: false,
                normalize: false,
                smallify: false
            },
            "headerRemove": false,
            "headerTitle": 'jsPanel',
            "headerToolbar": false,

            "maximizedMargin": {
                top: 5,
                right: 5,
                bottom: 5,
                left: 5
            },
            "onbeforeclose": false,
            "onbeforemaximize": false,
            "onbeforeminimize": false,
            "onbeforenormalize": false,
            "onclosed": false,
            "onmaximized": false,
            "onminimized": false,
            "onnormalized": false,
            "paneltype": false,
            "resizable": {
                handles: 'n, e, s, w, ne, se, sw, nw',
                autoHide: false,
                minWidth: 40,
                minHeight: 40
            },
            "rtl": false,
            "setstatus": false,
            "show": false,
            "template": false,
            "theme": 'bluegrey'
        };

// deviating defaults for modal jsPanels
        $.jsPanel.modaldefaults = {
            "draggable": "disabled",
            "headerControls": {buttons: "closeonly"},
            "position": {
                my: 'center',
                at: 'center'
            },
            "resizable": "disabled",
        };

// deviating defaults for jsPanel tooltips
        $.jsPanel.tooltipdefaults = {
            "draggable": "disabled",
            "headerControls": {buttons: "closeonly"},
            "position": {fixed: false},
            "resizable": "disabled",
        };

// deviating defaults for jsPanel hints
        $.jsPanel.hintdefaults = {
            "autoclose": 8000,
            "draggable": "disabled",
            "headerControls": {buttons: "closeonly"},
            "resizable": "disabled",
        };
*/

        var content = null;
        $.jsPanel({
            position: {
                left: 200,
                top: 100
            },
            title: 'jsPanel theme info',
            theme: 'bootstrap-default',
            __content:function(e,b,c){
                content = $('<div class="widget"/>');
                return content;
            },
            callback: function () {
                //this.content.css('padding', '10px');
                //this.content.append$('<div class="widget"/>')
                FTestUtils.createFileGrid(null,{},null,null,null,true,this.content[0]);
            }
        });

        return;
        var parent = TestUtils.createTab(null, null, module.id);
        var widget = dcl(_Widget, {
            templateString: '<div>' +
            '<input attachTo="input" data-provide="typeahead" class="typeahead form-control input-transparent" type="text" placeholder="States of USA">' +
            '</div>',
            startup: function () {

            }
        });

        utils.addWidget(widget, null, null, parent, true);


        return declare('a', null, {});

    }

    return Grid;

});