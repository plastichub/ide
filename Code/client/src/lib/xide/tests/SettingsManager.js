define([
    'dcl/dcl',
    "xide/manager/ServerActionBase",
    "xide/utils",
    "xide/manager/ManagerBase",
    "xide/data/Memory"
], function (dcl,ServerActionBase, utils, ManagerBase,Memory) {

    var Module = dcl([ManagerBase, ServerActionBase], {
        declaredClass:"xide.manager.SettingsManager",
        serviceClass: 'XApp_Store',
        settingsStore: null,
        settingsDataAll: null,
        section: 'settings',
        store:null,
        has: function (section, path, query, data, readyCB) {},
        getStore: function () {
            return this.settingsStore;
        },
        _createStore: function (data) {
            var store = new Memory({
                data: data,
                idProperty: 'id'
            });
            return store;
        },
        onSettingsReceived: function (data) {
            this.settingsDataAll = data;
            if (!data) {
                this._createStore([]);
                return;
            }
            var _a = data['' + this.section];
            this.settingsStore = this._createStore(_a);
        },
        replace: function (path, query, operation, newValue, readyCB) {
            var thiz = this;
            var defered = this.serviceObject[this.serviceClass].set(
                this.store,
                path || '.', query, operation, newValue
            );
            defered.addCallback(function (res) {
                if (readyCB) {
                    readyCB(res);
                }
            });
        },
        read: function (section, path, query, readyCB) {
            return this.runDeferred(null, 'get', [section, path, query]).then(function (data) {
                readyCB && readyCB(data);
            }.bind(this));
        },
        update: function (section, path, query, data, decode, readyCB) {
            return this.callMethodEx(this.serviceClass, 'update', [section || this.section, path, query, data, decode], readyCB, false);
        },
        write: function (section, path, query, data, decode, readyCB) {
            try {
                var itemLocal = utils.queryStoreEx(this.settingsStore, {id: data.id}, true, true);
                if (itemLocal) {
                    return this.update(section, path, {id: data.id}, data.data, decode, readyCB);
                } else {
                    return this.callMethodEx(this.serviceClass, 'set', [section || this.section, path, query, data, decode], readyCB, false);
                }
            } catch (e) {
                logError(e, 'update');
            }
        },
        write2: function (section, path, query, data, decode, readyCB) {
            try {
                var itemLocal = utils.queryStoreEx(this.settingsStore, {id: data.id}, true, true);
                if (itemLocal) {
                    return this.update(section, path, query, data, decode, readyCB);
                } else {
                    return this.callMethodEx(this.serviceClass, 'set', [section || this.section, path, query, data, decode], readyCB, false);
                }
            } catch (e) {
                logError(e, 'update');
            }
        },
        initStore: function () {
            return this.read(this.section, '.', null, this.onSettingsReceived.bind(this));
        },
        init:function(){
            this.initStore();
        }
    });


    var ctx = sctx,
        doTests = true;

    if(ctx && doTests){
        var manager = ctx.createManager(Module);
        manager.init();
        manager.initStore().then(function(){
            manager.write2(null, '.',
                {
                    id: 'theme'
                },{
                    value:"gray"
                }, true, null
            )
            //console.error(manager.store);
            //console.error(manager.store.getSync('theme').value);
        });

    }

    return Module;
});