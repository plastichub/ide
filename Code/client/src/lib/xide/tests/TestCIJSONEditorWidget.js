/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',

    
    'xide/views/CIGroupedSettingsView',
    'xide/views/CIActionDialog',
    'xide/widgets/WidgetBase',
    'xide/widgets/_Widget',
    'xide/widgets/_CacheMixin',
    'xide/widgets/ActionToolbar',
    'xide/action/ActionModel',
    'xaction/ActionProvider',
    "dstore/Memory",
    'xide/widgets/ExpressionsGridView',
    'xide/tests/TestUtils',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'module',
    'xide/json/JSONEditor'

], function (dcl,declare, types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,CIActionDialog,
             WidgetBase,_Widget,_CacheMixin,ActionToolbar,ActionModel,ActionProvider,
             Memory,ExpressionsGridView,
             TestUtils,ACEEditor,Editor,module,
             JSONEditor

    ) {




    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    function createDriverCIS(driver,actionTarget){

        var CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID",
                    "name": "CF_DRIVER_ID",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": "JSON",
                    "uid": "-1",
                    "value": {
                        test:2
                    },
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General21',
                    "id": "CF_DRIVER_ID",
                    "name": "CF_DRIVER_ID",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 13,
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true
                }
            ]
        };


        _.each(CIS.inputs,function(ci){

        });
        return CIS;
    }
    function onWidgetCreated(basicTab,condTab,varTab,logTab){}
    function doTests(){}

    function createJSON_EditorWidgetClass(){





        var JSON_EditorWidget = dcl(WidgetBase, {
            declaredClass:"xide.widgets.JSONEditorWidget",
            didLoad: false,
            minHeight: "525px",
            editorHeight: "470px",
            jsonEditorHeight: "270px",
            aceEditorHeight: "200px",
            newLabel: "New Entry",
            newEntryTemplate: "",
            wNew: null,
            initialTemplate: [],
            wSaveButton: null,
            showACEEditor: true,
            aceEditorOptions: null,
            aceEditor: null,
            aceNode: null,
            wBorderContainer: null,
            wJSONPane: null,
            wACEPane: null,
            wToAceButton: null,
            wFromAceButton: null,
            hasSave: true,
            hasNew: false,
            editor: null,
            title:"Options",
            templateString: "<div class='widgetContainer widgetBorder widgetTable widget' style=''>" +
            "<table border='0' cellpadding='5px' width='100%'>" +
            "<tbody align='left'>" +
            "<tr attachTo='extensionRoot' valign='middle' style='height:90%'>" +
            "<td attachTo='titleColumn' width='15%' class='widgetTitle'><b><span attachTo='titleNode'>${!title}</span></b></td>" +
            "<td valign='middle' class='widgetValue widget jsonEditorWidget' attachTo='valueNode' width='100px'></td>" +
            "<td class='extension' attachTo='previewNode'></td>" +
            "<td class='extension' attachTo='button0'></td>" +
            "<td class='extension' attachTo='button1'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
            getValue: function () {
                if (this.editor) {
                    var json = this.editor.get();
                    var val = JSON.stringify(json);
                    return val;
                }

                return this.inherited(arguments);
            },
            resize: function () {




                this.inherited(arguments);
                return;
                this.wBorderContainer.resize();
                var thiz = this;
                try {
                    if (this.showACEEditor && !this.aceEditor) {
                        this._createACEEditor(this.aceNode);
                    }
                } catch (e) {
                    console.error('constructing ace editor widget failed : ' + e.message);

                }
                thiz.wJSONPane.resize();
                thiz.wACEPane.resize();
                thiz.wBorderContainer.resize();
            },
            setupNativeWidgetHandler: function () {
                if (this.nativeWidget) {
                    var thiz = this;
                    connect.connect(this.nativeWidget, "onChange", function (item) {
                        if (thiz['isSaving']) {
                            // return;
                        }
                        thiz['isSaving'] = true;
                        thiz.widgetChanged(this);
                    });
                }
            },
            _createJSONEditor: function (parentNode,options) {
                this.editor = new JSONEditor(options,parentNode);
                this.editor.startup()
            },
            appendNewTemplatedEntry: function () {
                var json = this.editor.get();

                if (!lang.isArray(json)) {
                    lang.mixin(json, {}, this.newEntryTemplate);
                } else {
                    json.push(this.newEntryTemplate);
                }
                this.editor.set(json);
            },
            onFromAce: function () {

                if (this.aceEditor) {
                    var value = this.aceEditor.get("value");
                    this.editor.set(dojo.fromJson(value));
                }
            },
            onToAce: function () {
                var json = this.editor.get();
                var val = JSON.stringify(json, null, 2);
                if (this.aceEditor) {
                    this.aceEditor.set("value", val);
                }
            },
            onSave: function (value) {

                console.log('on save',value);

                this.setValue(JSON.stringify(value));
            },
            setupEventHandlers: function () {
                var thiz = this;

                if (this.wNew) {
                    connect.connect(this.wNew, "onClick", function (item) {
                        if (thiz.newEntryTemplate) {
                            thiz.appendNewTemplatedEntry(thiz.newEntryTemplate);
                        }
                    });
                }
                if (this.wSaveButton) {
                    connect.connect(this.wSaveButton, "onClick", function (item) {
                        var json = thiz.editor.get();
                        thiz.setValue(JSON.stringify(json));
                        thiz.onSave(JSON.stringify(json));
                    });
                }



                if (this.wFromAceButton) {
                    connect.connect(this.wFromAceButton, "onClick", function (item) {
                        thiz.onFromAce();
                    });
                }

                if (this.wToAceButton) {
                    connect.connect(this.wToAceButton, "onClick", function (item) {
                        thiz.onToAce();
                    });
                }
            },
            _createACEEditor: function (parentNode) {

                var _options = {
                    region: "center",
                    value: '',//this.getValue(),
                    style: "margin: 0; padding: 0; position:relative;overflow: auto;height:inherit;width:inherit;",
                    mode: 'javascript',
                    readOnly: false,
                    tabSize: 2,
                    softTabs: false,
                    wordWrap: false,
                    showPrintMargin: false,
                    highlightActiveLine: false,
                    fontSize: '13px',
                    showGutter: true,
                    className: 'editor-ace ace_editor ace-codebox-dark ace_dark'
                };

                var editorPane = new TextEditor(_options, dojo.doc.createElement('div'));

                parentNode.appendChild(editorPane.domNode);
                editorPane.startup();
                this.aceEditor = editorPane;
                this.aceEditor.setOptions();
            },
            __startup: function () {



                if (!this.hasNew) {
                    this.wNew = null;
                    utils.destroy(this.wNew);
                }
                if (!this.hasSave) {
                    this.wSaveButton = null;
                    utils.destroy(this.wSaveButton);
                }

                domClass.remove(this.wFromAceButton.iconNode, 'dijitReset');
                domClass.add(this.wFromAceButton, 'actionToolbarButtonElusive');

                domClass.remove(this.wToAceButton.iconNode, 'dijitReset');
                domClass.add(this.wToAceButton, 'actionToolbarButtonElusive');



                try {
                    if (!this.editor) {
                        var pdiv = dojo.doc.createElement('div');

                        this.valueNode.appendChild(pdiv);
                        this._createJSONEditor(pdiv);
                        this._createACEEditor(this.aceNode);
                    }
                } catch (e) {
                    console.error('constructing json editor widget failed : ' + e.message);
                }

                var jsonStr = utils.toString(this.valueStr);
                if (!jsonStr || jsonStr.length == 0) {
                    jsonStr = '[]';
                }

                this.valueStr = jsonStr;

                if (this.editor && this.valueStr) {

                    if (this.valueStr != 'Unset') {

                        try {

                            var _json = dojo.fromJson(jsonStr);
                            this.editor.set(_json);
                        } catch (e) {

                            if (jsonStr.includes(',')) {
                                var _splitted = jsonStr.split(',');
                                this.editor.set(_splitted);
                            } else {
                                this.editor.set(dojo.fromJson('[]'));
                            }
                        }

                    } else {
                        this.editor.set(this.initialTemplate);
                    }

                    this.editor.expandAll();
                }
                this.setupEventHandlers();
            },
            startup: function () {

                //this.inherited(arguments);
                var data = utils.getJson(this.userData.value);


                var editor = utils.addWidget(JSONEditor,{
                    options:{
                        save:true
                    }
                },this,this.valueNode,true);

                editor.setData(data);

                
                this.editor = editor;

            }
        });
        return JSON_EditorWidget;
    }

    function testWidget(){

        var toolbar = ctx.mainView.getToolbar();

        var docker = ctx.mainView.getDocker();

        var parent  = TestUtils.createTab(null,null,module.id);

        var cisRenderer = dcl(CIGroupedSettingsView,{
            typeMap:null,
            getTypeMap:function(){

                if (this.typeMap) {
                    return this.typeMap;
                }

                var self = this;
                var typeMap = {}

                typeMap['JSON'] = createJSON_EditorWidgetClass();


                this.typeMap = typeMap;


                return typeMap;
            }
        });

        //types.registerWidgetMapping('Arguments', createArgumentsWidget());

        //types.registerWidgetMapping('Script', createScriptWidget());

        //types.registerWidgetMapping('Expression', createExpressionWidget());

        //types.registerWidgetMapping(types.ECIType.EXPRESSION_EDITOR, createExpressionWidget());


        var ciView = utils.addWidget(cisRenderer,{
            cis:createDriverCIS().inputs
        },null,parent,true);


        docker.resize();
    }


/*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return blockManager.toScope(data);
    }

    if (ctx) {

        blockManager = ctx.getBlockManager();
        //driverManager = ctx.getDriverManager();
        //marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        //openDriverSettings(null);
        //openCISDialog();

        testWidget();

        return declare('a',null,{});
    }

    return Grid;

});