/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry',
    'xide/_base/_Widget',
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/DefaultActions',
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    "xblox/tests/TestUtils",
    "xide/_Popup",
    "dojo/node!util",
    "dojo/_base/kernel",
    "nxapp/protocols/Tcp"

], function (declare,dcl,types,utils,TestUtils,_Widget,module,registry,_XWidget,
             ActionMixin,ActionContext,DefaultActions,Path,Action,FTestUtils,i18,_TestBlockUtils,_Popup,util,kernel,FileUtils){



    var ACTION = types.ACTION;
    var _debug = false;
    var _debugWidgets = true;
    var _a = FileUtils;
    var OPEN_DELAY = 200;
    function correctPosition(_root,$sub){
        var level = 0;
        var _levelAttr = $sub.attr('level');
        if(!_levelAttr){
            _levelAttr = parseInt($sub.parent().attr('level'));
        }

        var _left = 0;
        if(_levelAttr==0){
            $sub.css({
                left:0
            })
        }else{

        }
        var parent = _root.parent();
        var parentTopAbs = parent.offset().top;

        var rootOffset = _root.offset();
        var rootPos = _root.position();//top:0
        var rootHeight = _root.height();

        var deltaTop = rootOffset.top - rootPos.top;
        var parentDelta = parentTopAbs - rootOffset.top;

        var newTop = (rootPos.top + rootHeight) + parentDelta;

        var offset = -20;
        if(_levelAttr!==0) {
            newTop += offset;
        }
        $sub.css({
            top:newTop
        })

        var autoH = $sub.height() + 0;
        var totalH = $('html').height();
        var pos = $sub.offset();
        var overlapYDown = totalH - (pos.top + autoH);
        if((pos.top + autoH) > totalH){
            $sub.css({
                top: overlapYDown - 30
            }).fadeIn(options.fadeSpeed);
        }
        ////////////////////////////////////////////////////////////
        var subWidth = $sub.width(),
            subLeft = $sub.offset().left,
            collision = (subWidth+subLeft) > window.innerWidth;

        if(collision){
            $sub.addClass('drop-left');
        }
    }
    var createCallback = function(func,menu,item) {
        return function(event) {
            func(event, menu,item);
        };
    };
    var _ctx = {}
    //action renderer class
    var ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        templateString:'<div attachTo="navigation" class="actionToolbar">'+
            '<nav attachTo="navigationRoot" class="" role="navigation">'+
                '<ul attachTo="navBar" class="nav navbar-nav"/>'+
            '</nav>'+
        '</div>'
    });
    var MenuMixinClass =  dcl(null, {
        actionStores: null,
        correctSubMenu: false,
        _didInit: null,
        actionFilter:null,
        hideSubsFirst:false,
        onActionAdded:function(actions){
            this.setActionStore(this.getActionStore(), actions.owner || this,false,true,actions);
        },
        onActionRemoved:function(evt){
            this.clearAction(evt.target);
            //console.log('onActionRemoved ' + this.visibility,evt);
        },
        clearAction : function(action){
            var self = this;
            if(action) {
                var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                if (actionVisibility) {
                    var widget = actionVisibility.widget;
                    widget && action.removeReference && action.removeReference(widget);
                    if (widget && widget.destroy) {
                        widget.destroy();
                    }
                    delete actionVisibility.widget;
                    actionVisibility.widget = null;
                }
            }
        },
        removeCustomActions:function(){
            var oldStore = this.store;
            var oldActions = oldStore.query({
                    custom:true
                }),
                menuData=this.menuData;
            _.each(oldActions,function(action){
                oldStore.removeSync(action.command);
                var oldMenuItem = _.find(menuData,{
                    command:action.command
                });
                oldMenuItem && menuData.remove(oldMenuItem);
            });
        },
        /**
         * Return a field from the object's given visibility store
         * @param action
         * @param field
         * @param _default
         * @returns {*}
         */
        getVisibilityField: function (action, field, _default) {
            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return actionVisibility[field] != null ? actionVisibility[field] : action[field] || _default;
        },
        /**
         * Sets a field in the object's given visibility store
         * @param action
         * @param field
         * @param value
         * @returns {*}
         */
        setVisibilityField: function (action, field, value) {
            var _default = {};
            if (action.getVisibility) {
                var actionVisibility = action.getVisibility(this.visibility) || _default;
                actionVisibility[field] = value;
            }
            return actionVisibility;
        },
        shouldShowAction: function (action) {
            if (this.getVisibilityField(action, 'show') == false) {
                return false;
            } else if (action.getVisibility && action.getVisibility(this.visibility) == null) {
                return false;
            }
            return true;
        },
        addActionStore: function (store) {
            if (!this.actionStores) {
                this.actionStores = [];
            }
            if (!this.actionStores.includes(store)) {
                this.actionStores.push(store);
            }
        },
        /**

         tree structure :

         {
            root: {
                Block:{
                    grouped:{
                        Step:[action,action]
                    }
                }
            },
            rootActions: string['File','Edit',...],

            allActionPaths: string[command],

            allActions:[action]
         }

         * @param store
         * @param owner
         * @returns {{root: {}, rootActions: Array, allActionPaths: *, allActions: *}}
         */
        buildActionTree: function (store, owner) {
            var self = this,
                allActions = self.getActions(),
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            if(!store){
                console.error('buildActionTree : invalid store');
                return null;
            }

            self.wireStore(store, function (evt) {
                if (evt.type === 'update') {
                    var action = evt.target;
                    if (action.refreshReferences) {
                        var refs = action.getReferences();
                        action.refreshReferences(evt.property, evt.value);
                    }
                }
            });

            //return all actions with non-empty tab field
            var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
                groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),
            //now flatten them
                _actionsFlattened = [];

            _.each(groupedTabs, function (items, name) {
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            var rootActions = [];
            _.each(tabbedActions, function (action) {
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            if (store.menuOrder) {
                rootActions = owner.sortGroups(rootActions, store.menuOrder);
            }

            //collect all existing actions as string array
            var allActionPaths = _.pluck(allActions, 'command');
            var tree = {};

            _.each(rootActions, function (level) {
                // collect all actions at level (File/View/...)
                var menuActions = owner.getItemsAtBranch(allActions, level);
                // convert action command strings to Action references
                var grouped = self.toActions(menuActions, store);
                //apt-get install automake autopoint bison build-essential ccache cmake curl cvs default-jre fp-compiler gawk gdc gettext git-core gperf libasound2-dev libass-dev libavcodec-dev libavfilter-dev libavformat-dev libavutil-dev libbluetooth-dev libbluray-dev libbluray1 libboost-dev libboost-thread-dev libbz2-dev libcap-dev libcdio-dev libcrystalhd-dev libcrystalhd3 libcurl3 libcurl4-gnutls-dev libcwiid-dev libcwiid1 libdbus-1-dev libenca-dev libflac-dev libfontconfig-dev libfreetype6-dev libfribidi-dev libglew-dev libiso9660-dev libjasper-dev libjpeg-dev libltdl-dev liblzo2-dev libmad0-dev libmicrohttpd-dev libmodplug-dev libmp3lame-dev libmpeg2-4-dev libmpeg3-dev libmysqlclient-dev libnfs-dev libogg-dev libpcre3-dev libplist-dev libpng-dev libpostproc-dev libpulse-dev libsamplerate-dev libsdl-dev libsdl-gfx1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libshairport-dev libsmbclient-dev libsqlite3-dev libssh-dev libssl-dev libswscale-dev libtiff-dev libtinyxml-dev libtool libudev-dev libusb-dev libva-dev libva-egl1 libva-tpi1 libvdpau-dev libvorbisenc2 libxml2-dev libxmu-dev libxrandr-dev libxrender-dev libxslt1-dev libxt-dev libyajl-dev mesa-utils nasm pmount python-dev python-imaging python-sqlite swig unzip yasm zip zlib1g-dev
                // group all actions by group
                var groupedActions = _.groupBy(grouped, function (action) {
                    var _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                    if (action) {
                        return _vis.group || action.group;
                    }
                });

                //temp array
                var _actions = [];
                _.each(groupedActions, function (items, level) {
                    if (level !== 'undefined') {
                        _actions = _actions.concat(items);
                    }
                });

                //flatten out again
                menuActions = _.pluck(_actions, 'command');
                menuActions.grouped = groupedActions;
                tree[level] = menuActions;
            });

            var result = {
                root: tree,
                rootActions: rootActions,
                allActionPaths: _.pluck(allActions, 'command'),
                allActions: allActions
            }

            this.lastTree = result;

            return result;
        },
        constructor: function (options, node) {
            this.target = node;
            utils.mixin(this, options);
        },
        onClose: function (e) {
            this._rootMenu && this._rootMenu.parent().removeClass('open');
        },
        onOpen: function () {
            this._rootMenu && this._rootMenu.parent().addClass('open');
        },
        init: function (opts) {
            if (this._didInit) {
                return;
            }
            this._didInit = true;
            var options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root, 'click', null, function (e) {
                if (!self.isOpen) {
                    return;
                }
                self.isOpen = false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display: ''
                }).find('.drop-left').removeClass('drop-left');
            });
            if (options.preventDoubleContext) {
                this.__on(root, 'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            this.__on(root, 'mouseenter', '.dropdown-submenu', function (e) {
                try {
                    var _root = $(e.currentTarget);
                    var $sub = _root.find('.dropdown-context-sub:first');
                    if ($sub.length == 0) {
                        return;
                    }
                    if (self.correctSubMenu == false) {
                        return;
                    }
                    if (self.menu) {
                        if (!$.contains(self.menu[0], _root[0])) {
                            return;
                        }
                    }
                    var _disabled = _root.hasClass('disabled');
                    if(_disabled){
                        $sub.css('display','none');
                        return;
                    }else{
                        $sub.css('display','');
                    }

                    //reset top
                    $sub.css({
                        top: 0
                    });

                    var autoH = $sub.height() + 0;
                    var totalH = $('html').height();
                    var pos = $sub.offset();
                    var overlapYDown = totalH - (pos.top + autoH);
                    if ((pos.top + autoH) > totalH) {
                        $sub.css({
                            top: overlapYDown - 30
                        }).fadeIn(options.fadeSpeed);
                    }
                    ////////////////////////////////////////////////////////////
                    var subWidth = $sub.width(),
                        subLeft = $sub.offset().left,
                        collision = (subWidth + subLeft) > window.innerWidth;

                    if (collision) {
                        $sub.addClass('drop-left');
                    }
                } catch (e) {
                    logError(e);
                }
            });
        },
        getDefaultOptions: function () {
            return options = {
                fadeSpeed: 0,
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: true
            };
        },
        buildMenuItems:function ($menu, data, id, subMenu, addDynamicTag) {

            var linkTarget = '',
                self = this,
                visibility = this.visibility;

            for (var i = 0; i < data.length; i++) {
                var item = data[i],
                    $sub,
                    widget = item.widget;

                if (typeof item.divider !== 'undefined' && !item.widget) {
                    var divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '' ;
                    divider += '"></li>';
                    item.widget = divider;
                    $menu.append(divider);
                } else if (typeof item.header !== 'undefined' && !item.widget) {
                    var header = item.vertical ? '<li class="divider-vertical' : '<li class="nav-header';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + item.header + '</li>';
                    item.widget = header;
                    $menu.append(header);
                } else if (typeof item.menu_item_src !== 'undefined') {

                } else {
                    if (!widget && typeof item.target !== 'undefined') {
                        linkTarget = ' target="' + item.target + '"';
                    }
                    if (typeof item.subMenu !== 'undefined' && !widget) {


                        var sub_menu = '<li class="dropdown-submenu';
                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a>';

                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }
                        sub_menu += item.text + '';
                        sub_menu += '</a></li>';
                        $sub = $(sub_menu);

                    } else {
                        if (!widget) {
                            if (item.render) {
                                $sub = item.render(item, $menu);
                            } else {
                                var element = '<li tabindex="2" class="" ';
                                element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                                element += '><a >';
                                if (typeof data[i].icon !== 'undefined') {
                                    element += '<span class="' + item.icon + '"></span> ';
                                }
                                element += item.text + '</a></li>';
                                $sub = $(element);
                                if (item.postRender) {
                                    item.postRender($sub);
                                }
                            }
                        }
                    }

                    if (typeof item.action !== 'undefined' && !item.widget) {
                        if (item.addClickHandler && item.addClickHandler() === false) {
                        } else {
                            var $action = item.action;
                            if ($sub && $sub.find) {
                                $sub.find('a')
                                    .addClass('context-event')
                                    .on('click', createCallback($action, item, $sub));
                            }
                        }
                    }

                    if ($sub && !widget) {
                        item.widget = $sub;
                        $sub.menu = $menu;
                        $sub.data('item', item);

                        item.$menu = $menu;
                        item.$sub = $sub;

                        item._render = function(){
                            if (item.index == 0) {
                                this.$menu.prepend(this.$sub);
                            } else {
                                this.$menu.append(this.$sub);
                            }

                        }
                        if(!item.lazy) {
                            item._render();
                        }
                    }

                    if($sub){
                        $sub.attr('level',item.level);
                    }

                    if (typeof item.subMenu != 'undefined' && !item.subMenuData) {
                        var subMenuData = self.buildMenu(item.subMenu, id, true);
                        $menu.subMenuData = subMenuData;
                        item.subMenuData = subMenuData;
                        $menu.find('li:last').append(subMenuData);
                        subMenuData.attr('level',item.subMenu.level);
                        if(self.hideSubsFirst) {
                            subMenuData.css('display','none');
                        }

                        var labelLocalized = self.localize(item.text);
                        var title = labelLocalized;
                        $menu.data('item',item);
                        //subMenuData.attr("title",labelLocalized);

                    }else{
                        if(item.subMenu && item.subMenuData) {
                            this.buildMenuItems(item.subMenuData, item.subMenu, id, true);
                        }
                    }
                }

                if (!$menu._didOnClick) {
                    $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                        e.stopPropagation()
                    });
                    $menu._didOnClick = true;
                }

            }
            return $menu;
        },
        buildMenu: function (data, id, subMenu) {
            var subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ',
                $menu = $('<ul tabindex="1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>');
            if (!subMenu) {
                this._rootMenu = $menu;
            }
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        createNewAction: function (command) {
            var segments = command.split('/');
            var lastSegment = segments[segments.length - 1];
            var action = new Action({
                command: command,
                label: lastSegment,
                group: lastSegment
            });
            return action;
        },
        findAction: function (command) {
            var stores = this.actionStores,
                action = null;
            _.each(stores, function (store) {
                var _action = store ? store.getSync(command) : null;
                if (_action) {
                    action = _action;
                }
            });

            return action;
        },
        getAction: function (command, store) {
            store = store || this.store;
            var action = null;
            if (store) {
                action = this.findAction(command);
                if (!action) {
                    action = this.createNewAction(command);
                }
            }
            return action;
        },
        getActions: function (query) {
            var result = [];
            var stores = this.actionStores,
                self = this,
                visibility = this.visibility;
            query = query || this.actionFilter;

            _.each(stores, function (store) {
                if(store) {//tmpFix
                    result = result.concat(store.query(query));
                }
            });
            result = result.filter(function (action) {
                var actionVisibility = action.getVisibility != null ? action.getVisibility(visibility) : {};
                if (action.show === false || actionVisibility === false || actionVisibility.show === false) {
                    return false;
                }
                return true;
            });
            return result;
        },
        toActions: function (commands, store) {
            var result = [],
                self = this;
            _.each(commands, function (path) {
                var _action = self.getAction(path, store);
                _action && result.push(_action);
            });
            return result;
        },
        onRunAction: function (action, owner, e) {
            var command = action.command;
            action = this.findAction(command);
            //_debug && console.log('run action ', action.command);
            return DefaultActions.defaultHandler.apply(action.owner || owner, [action, e]);
        },
        getActionProperty: function (action, visibility, prop) {
            var value = prop in action ? action[prop] : null;
            if (visibility && prop in visibility) {
                value = visibility[prop];
            }
            return value;
        },
        toMenuItem: function (action, owner, label, icon, visibility, showKeyCombo,lazy) {
            var self = this,
                labelLocalized = self.localize(label),
                widgetArgs = visibility.widgetArgs,
                actionType = visibility.actionType || action.actionType;

            var item = {
                text: labelLocalized,
                icon: icon,
                data: action,
                owner: owner,
                command: action.command,
                lazy:lazy,
                addClickHandler: function (item) {
                    var action = this.data;
                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        return false;
                    }
                    return true;
                },
                render: function (data, $menu) {

                    var action = this.data;
                    var parentAction = action.getParent ? action.getParent() : null;
                    var closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
                    var keyComboString = ' \n';
                    if (action.keyboardMappings && showKeyCombo !== false) {
                        var mappings = action.keyboardMappings;
                        var keyCombos = _.pluck(mappings, ['keys']);
                        if (keyCombos && keyCombos[0]) {
                            keyCombos = keyCombos[0];
                            if (keyCombos.join) {
                                var keycombo = [];
                                keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                            }
                        }
                    }

                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        var element = '<li class="" >';
                        var id = action._store.id + '_' + action.command + '_' + self.id;
                        var checked = action.get('value');
                        //checkbox-circle
                        element += '<div class="checkbox checkbox-success ">';
                        element += '<input id="' + id + '" type="checkbox" ' + (checked == true ? 'checked' : '') + '>';
                        element += '<label for="' + id + '">';
                        element += self.localize(data.text);
                        element += '</label>';
                        element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                        element += '</div>';

                        $menu.addClass('noclose');
                        var result = $(element);
                        var checkBox = result.find('INPUT');
                        checkBox.on('change', function (e) {
                            action._originReference = data;
                            action._originEvent = e;
                            action.set('value', checkBox[0].checked);
                            action._originReference = null;
                        });
                        self.setVisibilityField(action, 'widget', data);
                        return result;
                    }
                    closeOnClick === false && $menu.addClass('noclose');
                    if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                        var value = action.value || action.get('value');
                        var parentValue = parentAction.get('value');
                        if (value == parentValue) {
                            icon = 'fa fa-check';
                        }
                    }
                    var title = data.text || labelLocalized || self.localize(action.title);

                    //default:
                    var element = '<li><a title="' + title + ' ' +keyComboString + '">';
                    var _icon = data.icon || icon;

                    //icon
                    if (typeof _icon !== 'undefined') {
                        //already html string
                        if (/<[a-z][\s\S]*>/i.test(_icon)) {
                            element += _icon;
                        } else {
                            element += '<span class="icon ' + _icon + '"/> ';
                        }
                    }
                    element += data.text;
                    element += '<span style="max-width:100px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span></a></li>';
                    self.setVisibilityField(action, 'widget', data);
                    return $(element);
                },
                get: function (key) {
                },
                set: function (key, value) {
                    //_debugWidgets && _.isString(value) && console.log('set ' + key + ' ' + value);
                    var widget = this.widget,
                        action = this.data;
                    function updateCheckbox(widget, checked) {
                        var _checkBoxNode = widget.find('INPUT');
                        var what = widget.find("input[type=checkbox]");
                        if (what) {
                            if (checked) {
                                what.prop("checked", true);
                            } else {
                                what.removeAttr('checked');
                            }
                        }
                    }
                    if (widget) {
                        if (key === 'disabled') {
                            if (widget.toggleClass) {
                                widget.toggleClass('disabled', value);
                            }
                        }
                        if (key === 'icon') {
                            var _iconNode = widget.find('.icon');
                            if (_iconNode) {
                                _iconNode.attr('class', 'icon');
                                this._lastIcon = this.icon;
                                this.icon = value;
                                _iconNode.addClass(value);
                            }
                        }
                        if (key === 'value') {
                            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE ||
                                actionType === types.ACTION_TYPE.SINGLE_TOGGLE) {
                                updateCheckbox(widget, value);
                            }
                        }
                    }
                },
                action: function (e, data, menu) {
                    _debug && console.log('menu action', data);
                    return self.onRunAction(data.data, owner, e);
                },
                destroy: function () {
                    if (this.widget) {
                        this.widget.remove();
                    }
                }
            }
            return item;
        },
        attach: function (selector, data) {
            this.target = selector;
            this.menu = this.addContext(selector, data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
            return this.menu;
        },
        addReference: function (action, item) {
            if (action.addReference) {
                action.addReference(item, {
                    properties: {
                        "value": true,
                        "disabled": true,
                        "enabled": true
                    }
                }, true);
            }
        },
        onDidRenderActions: function (store, owner) {
            if (owner && owner.refreshActions) {
                owner.refreshActions();
            }
        },
        getActionData: function (action) {
            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label: actionVisibility.label != null ? actionVisibility.label : action.label,
                icon: actionVisibility.icon != null ? actionVisibility.icon : action.icon,
                command: actionVisibility.command != null ? actionVisibility.command : action.command,
                visibility: actionVisibility,
                group: actionVisibility.group != null ? actionVisibility.group : action.group,
                widget: actionVisibility.widget
            }
        },
        _clearAction: function (action) {

        },
        _findParentData:function(oldMenuData,parentCommand){
            var result = null;
            var parent = _.find(oldMenuData,{
                command:parentCommand
            });

            if(parent){
                return parent;
            }
            for (var i = 0; i < oldMenuData.length; i++) {
                var data = oldMenuData[i];
                if(data.subMenu){
                    var found = this._findParentData(data.subMenu,parentCommand);
                    if(found){
                        return found;
                    }
                }
            }
            return null;
        },
        _clear: function () {
            var actions = this.getActions();
            var store = this.store;
            if (store) {
                this.actionStores.remove(store);
            }
            var self = this;
            actions = actions.concat(this._tmpActions);
            _.each(actions, function (action) {
                if (action) {
                    var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        var widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                        delete actionVisibility.widget;
                        actionVisibility.widget = null;
                    }
                }
            });
            this.$navBar && this.$navBar.empty();
        }
    });

    var ActionRendererClass = dcl(null,{
        renderTopLevel:function(name,where){
            where = where || $(this.getRootContainer());
            var item =$('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) +'<b class="caret"></b></a>'+
                '</li>');

            where.append(item);
            return item;
        },
        getRootContainer:function(){
            return this.navBar;
        },
        getActionData:function(action){
            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label:actionVisibility.label !=null ? actionVisibility.label : action.label,
                icon:actionVisibility.icon !=null ? actionVisibility.icon : action.icon,
                visibility: actionVisibility
            }
        }
    });


    /**
     * @extends module:xide/widgets/MainMenuActionRenderer
     */
    var MainMenu =dcl([ContainerClass,MenuMixinClass,ActionRendererClass,_XWidget.StoreMixin],{
        declaredClass:'xide.widgets.Actiontoolbar',
        target:null,
        attachToGlobal:true,
        _isFollowing:false,
        _followTimer:null,
        _zIndex:1,
        hideSubsFirst:true,
        visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
        menuData:null,
        init2: function(opts){
            var options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root,'click',null,function(e){
                if(!self.isOpen){
                    return;
                }
                self.isOpen=false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display:''
                }).find('.drop-left').removeClass('drop-left');
            });

            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            var _root2 = this.$navigationRoot;
            this.__on(_root2,'mouseleave', '.dropdown-submenu', function(e){
                var _root = $(e.currentTarget);
                var $sub = _root.find('.dropdown-context-sub:first');
                if (self.menu) {
                    if (!$.contains(self.menu[0], _root[0])) {
                        return;
                    }
                }
                $sub.css('display','none');
                $sub.data('left',true);
                clearTimeout($sub.data('openTimer'));
            });
            function onOpened($sub){
                console.log('open');
                /*
                 var desc = ' li:not(.divider):visible a'
                 //var $items = $sub.find('[role="menu"]' + desc + ', [role="listbox"]' + desc);
                 var $items = $sub.find(desc);
                 //console.log('items',$items);
                 if($items.length){
                 $($items[0]).focus();
                 }
                 */
            }

            this.__on(_root2,'mouseenter', '.dropdown-submenu', function(e){

                var _root = $(e.currentTarget);
                if (self.menu) {
                    if (!$.contains(self.menu[0], _root[0])) {
                        return;
                    }
                }
                var $sub = _root.find('.dropdown-context-sub:first');

                $sub.css('display', 'none');
                if(self.correctSubMenu!==false){
                    correctPosition(_root,$sub);
                }
                $sub.data('left',false);

                //console.log('data: ',$sub.data('item'));
                var data = $sub.data('item');

                if(data){
                    console.log('sub open '+data.command + ' ' + data.lazy,$sub);
                    $sub.css('z-index',_Popup.nextZ());
                }

                
                if(data && data.subMenu && data.lazy){
                    data.lazy = false;
                    $sub.data('item',data);
                    var items = data.subMenu;
                    _.each(items,function(item){
                        if(item.lazy){
                            item.lazy=false;
                            item._render();
                        }
                    })
                }


                clearTimeout($sub.data('openTimer'));
                $sub.data('openTimer',setTimeout(function(){
                    if($sub.data('left')!==true) {
                        $sub.css('display', 'block');
                        if(self.correctSubMenu!==false){
                            correctPosition(_root,$sub);
                        }
                    }else{
                        $sub.css('display', 'none');
                    }
                },OPEN_DELAY));

            });
        },
        resize:function(){
            utils.resizeTo(this.navigation,this.navBar,true,false);
        },
        destroy:function(){
            utils.destroy(this.$navBar[0]);
            utils.destroy(this.$navigation[0]);
            clearTimeout(this._followTimer);
        },
        buildMenu:function (data, id, subMenu,update) {
            var subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ',
                menuString = '<ul aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>',
                $menu = update ? (this._rootMenu || this.$navBar || $(menuString)) : $(menuString);

            if(!subMenu){
                this._rootMenu = $menu;
            }
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        setActionStore: function (store, owner,subscribe,update,itemActions) {

            if(!update && store && this.store && store!=this.store){
                this.removeCustomActions();
            }

            if(!update){
                this._clear();
                this.addActionStore(store);
            }

            if(!store){
                return;
            }

            this.store = store;

            var self = this,
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            var tree = update ? self.lastTree : self.buildActionTree(store,owner);

            var allActions = tree.allActions,
                rootActions = tree.rootActions,
                allActionPaths = tree.allActionPaths,
                oldMenuData = self.menuData;


            if(subscribe!==false) {

                if(!this['_handleAdded_' + store.id]) {
                    this.addHandle('added', store._on('onActionsAdded', function (actions) {
                        self.onActionAdded(actions);
                    }));

                    this.addHandle('delete', store.on('delete', function (evt) {
                        self.onActionRemoved(evt);
                    }));
                    this['_handleAdded_' + store.id]=true;
                }
            }

            // final menu data
            var data = [];
            if(!update) {
                _.each(tree.root, function (menuActions, level) {

                    var groupedActions = menuActions.grouped;

                    //temp group string of the last rendered action's group
                    var lastGroup = '';

                    _.each(menuActions, function (command) {
                        var action = self.getAction(command, store);
                        var isDynamicAction = false;
                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }
                        if (action) {

                            var renderData = self.getActionData(action),
                                icon = renderData.icon,
                                label = renderData.label,
                                visibility = renderData.visibility,
                                group = renderData.group,
                                lastHeader = {
                                    header: ''
                                };


                            if (visibility === false) {
                                return;
                            }
                            var item = self.toMenuItem(action, owner, '', icon, visibility || {}, false);
                            data.push(item);
                            visibility.widget = item;
                            self.addReference(action, item);

                            var childPaths = new Path(command).getChildren(allActionPaths, false),
                                isContainer = childPaths.length > 0,
                                childActions = isContainer ? self.toActions(childPaths, store) : null;

                            function parseChildren(command, parent) {

                                var childPaths = new Path(command).getChildren(allActionPaths, false),
                                    isContainer = childPaths.length > 0,
                                    childActions = isContainer ? self.toActions(childPaths, store) : null;

                                if (childActions) {

                                    var subs = [];


                                    _.each(childActions, function (child) {

                                        var _renderData = self.getActionData(child);
                                        var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility);

                                        var parentLevel = parent.level || 0;

                                        _item.level = parentLevel + 1;

                                        self.addReference(child, _item);
                                        subs.push(_item);

                                        var _childPaths = new Path(child.command).getChildren(allActionPaths, false),
                                            _isContainer = _childPaths.length > 0,
                                            _childActions = _isContainer ? self.toActions(_childPaths, store) : null;

                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }

                            parseChildren(command, item);

                            item.level = 0;
                            self.buildMenuItems(rootContainer, [item], "-" + new Date().getTime());

                        }
                    });
                });
                self.onDidRenderActions(store, owner);
                this.menuData = data;
            }else{
                if(itemActions || !_.isEmpty(itemActions)) {
                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            var action = self.getAction(newAction.command);
                            if (action){

                                var renderData = self.getActionData(action),
                                    icon = renderData.icon,
                                    label = renderData.label,
                                    aVisibility = renderData.visibility,
                                    group = renderData.group,
                                    item = self.toMenuItem(action, owner, label, icon, aVisibility || {},null,false);

                                if(aVisibility.widget){
                                    return;
                                }

                                aVisibility.widget = item;

                                self.addReference(newAction, item);
                                if(!action.getParentCommand){
                                    return;
                                }
                                var parentCommand = action.getParentCommand();
                                var parent = self._findParentData(oldMenuData,parentCommand);
                                if(parent && parent.subMenu){
                                    parent.lazy = true;
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });
                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }

            var parent = $(this.domNode).parent();
            if(parent[0] && parent[0].id!=='staticTopContainer') {
                this._height = this.$navigation.height();
                if (this._height) {
                    parent.css({
                        height: this._height
                    })
                }
            }
            this.resize();
        },
        _follow:function(){
            clearTimeout(this._followTimer);
            this._isFollowing=true;
            this._zIndex =_Popup.nextZ();
            this._zIndex +=2;
            var parent = $(this.domNode).parent();
            var node = $('#staticTopContainer');
            if (!node[0]) {
                var body = $('body');
                node = $('<div id="staticTopContainer" class=""></div>');
                body.prepend(node);
            }

            var what = this.$navigation,
                heightSource = this.$navBar,
                self = this;

            node.append(what);

            function follow(){
                var offset = parent.offset(),
                    currentHeight = what.height();

                if (parent.offset().top == 0 || parent.offset().left == 0 && self._hide!==false) {
                    what.css({
                        display: 'none'
                    })
                    return;
                }
                what.css({
                    top: offset.top,
                    left: offset.left,
                    position: 'absolute',
                    zIndex: self._zIndex,
                    display: 'inherit'
                });

                if (currentHeight) {
                    parent.css({
                        height: currentHeight
                    })
                    utils.resizeTo(what[0], heightSource[0], true, null);
                    utils.resizeTo(what[0], parent[0], null, true);
                    self._height = currentHeight;
                }
            }

            follow();
            this._followTimer = setInterval(function () {
                follow();
            }, 500);
        },
        _unfollow:function(newParent){
            clearTimeout(this._followTimer);
            this._isFollowing=false;
            var what = this.$navigation;
            $(newParent || this.domNode).append(what);
            what.css({
                top: 0,
                left: 0,
                position: 'relative'
            });
        },
        startup:function(){

            var parent = $(this.domNode).parent();

            this.correctSubMenu = true;
            this.init2({
                preventDoubleContext: false
            });
            this.menu = this.$navigation;
            this._height = this.$navigation.height();

            if(this.attachToGlobal) {
                this._follow();
                this.correctSubMenu = true;
            }
        }
    });
    dcl.chainAfter(MainMenu,'destroy');

    var actions = [],
        thiz = this,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        CIS;

    function doTests(tab){
        
        var grid = FTestUtils.createFileGrid('root',{},{

        },'TestGrid',module.id,false,tab);

        var menuNode = grid.header;
        //var context = new MainMenu({}, container.domNode);
        var mainMenu = utils.addWidget(MainMenu,{

        },null,menuNode,true);
        mainMenu.init({preventDoubleContext: false});
        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);


        return;
    }
    function doBlockTests(tab){


        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            __permissions:[]
        });


        var menuNode = grid.header;
        //var context = new MainMenu({}, container.domNode);
        /*
        var mainMenu = utils.addWidget(MainMenu,{
        },null,menuNode,true);
        mainMenu.init({preventDoubleContext: false});
        */
        grid.resize();
        //////////////////////////////////////////////////
        /*
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);
        */
        var windowManager = ctx.getWindowManager();
        grid.showToolbar(true,MainMenu,menuNode,true);


        windowManager.registerView(grid,true);

        setTimeout(function(){
            grid.select([0],null,true,{
                focus:true,
                delay:10
            });
        },1000);

        //grid.add(mainMenu);
        return;
    }
    function doGlobalTests(tab){


        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            //permissions:[]
        });


        var menuNode = grid.header;
        var mainView = ctx.mainView;
        var where = mainView.layoutTop;
        var windowManager = ctx.getWindowManager();
        var application = ctx.getApplication();


        utils.destroy(mainView.toolbar);


        var id = '__' + module.id;
        if(window[id]){
            utils.destroy(window[id]);
        }

        var mainMenu = utils.addWidget(MainMenu,{
            id:id,
            attachToGlobal:false,
            actionFilter:{
                quick:true
            }
        },null,where,true);

        window[id] = mainMenu;


        windowManager.addActionReceiver(mainMenu);
        $(mainMenu.domNode).css({
            //'float':'right',
            'height':'40px',
            'width':'50%'
        });



        mainMenu.init({preventDoubleContext: false});

        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);

        grid.showToolbar(true);

        mainMenu._registerActionEmitter(application.fileGrid);

        setTimeout(function(){
            grid.select([0],null,true,{
                focus:true,
                delay:10
            });
        },1000);





        //grid.add(mainMenu,null,false);



        return;
    }

    var ctx = window.sctx,
        root;

    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);

        //doTests(parent);
        //doGlobalTests(parent);
        doBlockTests(parent);
        return declare('a',null,{});

    }
    return _Widget;

});

