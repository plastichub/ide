/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',
    "xide/widgets/TemplatedWidgetBase",

    "xide/tests/TestUtils",

    "xide/_base/_Widget",
    "module"

], function (declare,dcl,types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase,
             TestUtils,_Widget,module) {



    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    function createTabClass(){

        return declare(TemplatedWidgetBase,{
            iconClass:null,
            open:true,
            titleBar:null,
            toggleNode:null,
            containerNode:null,
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);


                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            postMixInProperties:function(){


                var closed = !this.open;
                this.ariaOpen = closed ? 'true' : 'false';
                this.containerClass = closed ? 'collapse' : 'collapse in';
                this.titleClass = closed ? 'collapsed' : '';

                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';


                var toggleNodeStr =
                    '<a data-dojo-attach-point="toggleNode" href="#${!id}-Collapse" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                        iconStr +   ' ${!title}'+
                    '</a>';

                this.templateString = '<div class="panel widget">'+

                    '<div class="panel-heading" data-dojo-attach-point="titleBar">'+

                        toggleNodeStr +

                    '</div>'+

                    '<div data-dojo-attach-point="containerRoot" class="panel-collapse ${!containerClass}" id="${!id}-Collapse" aria-expanded="${!ariaOpen}" style="">'+
                        '<div class="panel-body" data-dojo-attach-point="containerNode">'+
                    '</div>'+
                    '</div>'+

                    '</div>';

                this.inherited(arguments);
            }
        });
    };

    function createTabPaneClass(){

        return declare(TemplatedWidgetBase,{

            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);

                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            postMixInProperties:function(){


                var closed = !this.open;

                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';

                var active = this.selected ? 'active' : '';

                this.templateString = '<div class="tab-pane ' + active + '"></div>';

                this.inherited(arguments);
            }
        });
    };

    function doTabContainerTests(tabContainer){
        var pane = tabContainer.createTab('bla bla2');
        var pane2 = tabContainer.createTab('bla bla2','fa-cogs');
    }

    function _createTabContainer(){

        var tabClass = declare(_Widget,{
            tabs:null,
            tabBar:null,
            tabContentNode:null,
            templateString:'<div class="widget" data-dojo-attach-point="containerNode">' +

                '<header><ul data-dojo-attach-point="tabBar" class="nav nav-tabs"/></header>'+
                '<div data-dojo-attach-point="tabContentNode" class="tab-content" />'+

            '</div',
            startup:function(){
                this.tabs = [];
                this.inherited(arguments);
            },

            createTab:function(title,icon){


                //var tabId = this.id + '_tab_'+ this.tabs.length;

                var active = this.tabs.length == 0 ? 'active' : '';

                var pane = utils.addWidget(createTabPaneClass,{

                },null,this.tabContentNode,true);

                var tabId = pane.id;

                var iconStr = icon ? ' ' +icon : '';

                var toggleNodeStr =
                    '<li class="' +active + '">' + '<a href="#'+tabId +'" data-toggle="tab" class="' +iconStr  +'"> '+
                    title+
                    '</a></li>';

                var tabButton = $(toggleNodeStr);

                $(this.tabBar).append(tabButton);

                this.tabs.push({
                    id:tabId,
                    pane: pane,
                    button:tabButton[0]
                })

                return;




                /*
                return utils.addWidget(createTabClass(),{
                    title:title,
                    iconClass:icon
                },null,this.domNode,true);
                */

            }

        });

        return tabClass;

    }

    var ctx = window.sctx,
        doTests = true;




    if (ctx && doTests) {

        var parent = TestUtils.createTab('Tab-Container-Tests',null,module.id);

        var cls = _createTabContainer();

        var tabContainer = utils.addWidget(cls,{},null,parent,true);

        //doTabContainerTests(tabContainer);

        return declare('a',null,{});

    }

    return Grid;

});