/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry'

], function (declare,dcl,types,utils,TestUtils,_Widget,module,registry)
{
    var options = {
        fadeSpeed: 100,
        filter: function ($obj) {
            // Modify $obj, Do not return
        },
        above: 'auto',
        left: 'auto',
        preventDoubleContext: true,
        compress: false
    };



    function initialize(opts) {

        options = $.extend({}, options, opts);

        $(document).on('click', function () {
            $('.dropdown-context').fadeOut(options.fadeSpeed, function(){
                $('.dropdown-context').css({display:''}).find('.drop-left').removeClass('drop-left');
            });
        });
        if(options.preventDoubleContext){
            $(document).on('contextmenu', '.dropdown-context', function (e) {
                e.preventDefault();
            });
        }
        $(document).on('mouseenter', '.dropdown-submenu', function(){
            var $sub = $(this).find('.dropdown-context-sub:first'),
                subWidth = $sub.width(),
                subLeft = $sub.offset().left,
                collision = (subWidth+subLeft) > window.innerWidth;
            if(collision){
                $sub.addClass('drop-left');
            }
        });

    }

    function updateOptions(opts){
        options = $.extend({}, options, opts);
    }

    function buildMenu(data, id, subMenu) {
        //console.log('build menu',arguments);
        var subClass = (subMenu) ? ' dropdown-context-sub' : '',
            compressed = options.compress ? ' compressed-context' : '',
            $menu = $('<ul class="dropdown-menu dropdown-context' + subClass + compressed +'" id="dropdown-' + id + '"></ul>');

        return buildMenuItems($menu, data, id, subMenu);
    }

    function buildMenuItems($menu, data, id, subMenu, addDynamicTag) {
        var linkTarget = '';
        for(var i = 0; i<data.length; i++) {

            var item = data[i];

            if (typeof data[i].divider !== 'undefined') {
                var divider = '<li class="divider';
                divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                divider += '"></li>';
                $menu.append(divider);
            } else if (typeof data[i].header !== 'undefined') {
                var header = '<li class="nav-header';
                header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                header += '">' + data[i].header + '</li>';
                $menu.append(header);
            } else if (typeof data[i].menu_item_src !== 'undefined') {
                var funcName;
                if (typeof data[i].menu_item_src === 'function') {
                    if (data[i].menu_item_src.name === "") { // The function is declared like "foo = function() {}"
                        for (var globalVar in window) {
                            if (data[i].menu_item_src == window[globalVar]) {
                                funcName = globalVar;
                                break;
                            }
                        }
                    } else {
                        funcName = data[i].menu_item_src.name;
                    }
                } else {
                    funcName = data[i].menu_item_src;
                }
                $menu.append('<li class="dynamic-menu-src" data-src="' + funcName + '"></li>');
            } else {

                if (typeof data[i].href == 'undefined') {
                    data[i].href = '#';
                }
                if (typeof data[i].target !== 'undefined') {
                    linkTarget = ' target="'+data[i].target+'"';
                }
                if (typeof data[i].subMenu !== 'undefined') {
                    var sub_menu = '<li class="dropdown-submenu';
                    sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    sub_menu += '"><a tabindex="-1" href="' + data[i].href + '">';

                    if (typeof item.icon !== 'undefined') {
                        sub_menu+= '<span class=" ' + item.icon + '"></span> ';

                    }

                    sub_menu += data[i].text + '';
                    sub_menu+='</a></li>';
                    $sub = (sub_menu);
                } else {
                    var element = '<li';
                    element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                    element += '><a tabindex="-1" href="' + data[i].href + '"'+linkTarget+'>';
                    if (typeof data[i].icon !== 'undefined') {
                        element += '<span class="glyphicon ' + data[i].icon + '"></span> ';

                    }
                    element += data[i].text + '</a></li>';
                    $sub = $(element);
                }
                if (typeof data[i].action !== 'undefined') {
                    $action = data[i].action;
                    $sub
                        .find('a')
                        .addClass('context-event')
                        .on('click', createCallback($action));
                }
                $menu.append($sub);
                if (typeof data[i].subMenu != 'undefined') {
                    var subMenuData = buildMenu(data[i].subMenu, id, true);
                    $menu.find('li:last').append(subMenuData);
                }
            }
            if (typeof options.filter == 'function') {
                options.filter($menu.find('li:last'));
            }
        }
        return $menu;
    }

    function addContext(selector, data) {

        if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
            var id = data.id;
            $menu = $('body').find('#dropdown-' + id)[0];
            if (typeof $menu === 'undefined') {
                $menu = buildMenu(data.data, id);
                $('body').append($menu);
            }
        } else {
            var d = new Date(),
                id = d.getTime(),
                $menu = buildMenu(data, id);
            $('body').append($menu);
        }


        $(selector).on('contextmenu', function (e) {
            e.preventDefault();
            e.stopPropagation();

            currentContextSelector = $(this);


            $('.dropdown-context:not(.dropdown-context-sub)').hide();

            $dd = $('#dropdown-' + id);

            $dd.find('.dynamic-menu-item').remove(); // Destroy any old dynamic menu items
            $dd.find('.dynamic-menu-src').each(function(idx, element) {
                var menuItems = window[$(element).data('src')]($(selector));
                $parentMenu = $(element).closest('.dropdown-menu.dropdown-context');
                $parentMenu = buildMenuItems($parentMenu, menuItems, id, undefined, true);
            });

            if (typeof options.above == 'boolean' && options.above) {
                $dd.addClass('dropdown-context-up').css({
                    top: e.pageY - 20 - $('#dropdown-' + id).height(),
                    left: e.pageX - 13
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.above == 'string' && options.above == 'auto') {
                $dd.removeClass('dropdown-context-up');
                var autoH = $dd.height() + 12;
                if ((e.pageY + autoH) > $('html').height()) {
                    $dd.addClass('dropdown-context-up').css({
                        top: e.pageY - 20 - autoH,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                } else {
                    $dd.css({
                        top: e.pageY + 10,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                }
            }

            if (typeof options.left == 'boolean' && options.left) {
                $dd.addClass('dropdown-context-left').css({
                    left: e.pageX - $dd.width()
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.left == 'string' && options.left == 'auto') {
                $dd.removeClass('dropdown-context-left');
                var autoL = $dd.width() - 12;
                if ((e.pageX + autoL) > $('html').width()) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width() + 13
                    });
                }
            }
        });

        return $menu;
    }

    function destroyContext(selector) {
        $(document).off('contextmenu', selector).off('click', '.context-event');
    }
    var createCallback = function(func) {
        return function(event) { func(event, currentContextSelector) };
    }
    console.clear();

    var ContextMenu = dcl(_Widget.dcl,{
        target:null,
        constructor:function(options,node){
            this.target = node;
        },
        init: initialize,
        settings: function(){
        },
        attach: function(selector,data){
            this.target = selector;
            this.menu = addContext(selector,data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
        },
        destroy: function(){
            console.error('destroy');
            registry.remove(this.domNode.id);
            destroyContext(this.target);
            utils.destroy(this.domNode);
            this.inherited(arguments);
        }
    });


    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;





    function doTests(tab){
        var context = new ContextMenu({

        },tab.containerNode);
        tab.add(context,null,false);
        context.init({preventDoubleContext: true});
        context.attach($(tab.containerNode), [

            {header: 'Download'},
            {
                text: 'The Script',
                icon:'fa-play',
                subMenu: [
                {header: 'Requires jQuery'},
                {text: 'context.js', href: 'http://lab.jakiestfu.com/contextjs/context.js', target:'_blank', action: function(e){
                    _gaq.push(['_trackEvent', 'ContextJS Download', this.pathname, this.innerHTML]);
                }}
            ]},
            {
                text: 'The Styles',
                icon: 'fa-play',
                subMenu: [

                {text: 'context.bootstrap.css',
                    icon: 'fa-play',
                    href: 'http://lab.jakiestfu.com/contextjs/context.bootstrap.css', target:'_blank', action: function(e){
                    _gaq.push(['_trackEvent', 'ContextJS Bootstrap CSS Download', this.pathname, this.innerHTML]);
                }},

                {text: 'context.standalone.css', href: 'http://lab.jakiestfu.com/contextjs/context.standalone.css', target:'_blank', action: function(e){
                    _gaq.push(['_trackEvent', 'ContextJS Standalone CSS Download', this.pathname, this.innerHTML]);
                }}
            ]},
            {divider: true},
            {header: 'Meta'},
            {text: 'The Author', subMenu: [
                {header: '@jakiestfu'},
                {text: 'Website', href: 'http://jakiestfu.com/', target: '_blank'},
                {text: 'Forrst', href: 'http://forrst.com/people/jakiestfu', target: '_blank'},
                {text: 'Twitter', href: 'http://twitter.com/jakiestfu', target: '_blank'},
                {text: 'Donate?', action: function(e){
                    e.preventDefault();
                    $('#donate').submit();
                }}
            ]},
            {
                text: 'Hmm?',
                icon:'fa-play',
                subMenu: [
                {header: 'Well, thats lovely.'},
                {

                    text: '2nd Level',
                    icon:'fa-play',
                    subMenu: [
                    {header: 'You like?'},
                    {text: '3rd Level!?', subMenu: [
                        {header: 'Of course you do'},
                        {text: 'MENUCEPTION', subMenu: [
                            {header:'FUCK'},
                            {text: 'MAKE IT STOP!', subMenu: [
                                {header: 'NEVAH!'},
                                {text: 'Shieeet', subMenu: [
                                    {header: 'WIN'},
                                    {text: 'Dont Click Me', href: 'http://bit.ly/1dH1Zh1', target:'_blank', action: function(){
                                        console.log(this);
                                    }}
                                ]}
                            ]}
                        ]}
                    ]}
                ]}
            ]}
        ]);
    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);
        doTests(parent);
        return declare('a',null,{});

    }
    return _Widget;

});