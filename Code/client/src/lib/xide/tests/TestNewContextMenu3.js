/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    
    'xide/registry',
    'xide/_base/_Widget',
    "dojo/text!./Navbar.html",
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    'xide/action/DefaultActions',
    "xaction/ActionProvider",
    "xgrid/Clipboard",
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    'xide/mixins/EventedMixin',
    "xide/widgets/_MenuMixin2",
    "xblox/tests/TestUtils",
    "xide/_Popup",
    'xcf/model/Variable',
    'xide/factory',
    "xblox/views/BlockGrid",
    'xblox/model/variables/VariableAssignmentBlock'

], function (declare, dcl, types, utils, TestUtils, _Widget, module, registry,
             _XWidget, MenuBarTemplate,
             ActionMixin, ActionContext, ActionStore, DefaultActions, ActionProvider,
             Clipboard, Path, Action, FTestUtils, i18, EventedMixin,_MenuMixin2,_TestBlockUtils,_Popup,Variable,factory,BlockGrid,VariableAssignmentBlock) {




    /**
     *
     * description
     */
    var ACTION = types.ACTION;

    var createCallback = function (func, menu, item) {

        return function (event) {
            func(event, menu, item);
        };
    }
    console.clear();

    var _ctx = {};
    var _debug = false;
    var _debugActionRendering = false;
    var grid = null;


    var MenuMixinClass = dcl(null, {
        actionStores:null,
        correctSubMenu:false,
        visibility:types.ACTION_VISIBILITY.CONTEXT_MENU,
        _didInit:null,
        /**
         * Return a field from the object's given visibility store
         * @param action
         * @param field
         * @param _default
         * @returns {*}
         */
        getVisibilityField:function(action,field,_default){

            var actionVisibility = action.getVisibility !=null ? action.getVisibility(this.visibility) : {};

            return actionVisibility[field] !=null ? actionVisibility[field] : action[field] || _default;
        },
        /**
         * Sets a field in the object's given visibility store
         * @param action
         * @param field
         * @param value
         * @returns {*}
         */
        setVisibilityField:function(action,field,value){

            var _default = {};
            if(action.getVisibility) {
                var actionVisibility = action.getVisibility(this.visibility) || _default;
                actionVisibility[field] = value;
            }
            return actionVisibility;
        },
        shouldShowAction:function(action){

            if(this.getVisibilityField(action,'show')==false){
                return false;
            }else if(action.getVisibility && action.getVisibility(this.visibility)==null){
                return false;
            }

            return true;
        },
        addActionStore:function(store){
            if(!this.actionStores){
                this.actionStores = [];
            }

            if(!this.actionStores.includes(store)){
                this.actionStores.push(store);
            }
        },
        /**

         tree structure :

         {
            root: {
                Block:{
                    grouped:{
                        Step:[action,action]
                    }
                }
            },
            rootActions: string['File','Edit',...],

            allActionPaths: string[command],

            allActions:[action]
         }

         * @param store
         * @param owner
         * @returns {{root: {}, rootActions: Array, allActionPaths: *, allActions: *}}
         */
        buildActionTree:function(store,owner){

            var self = this,
                allActions = self.getActions(),
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());


            if(_debug){
                console.dir(_.pluck(allActions,'command'));
            }

            self.wireStore(store,function(evt){

                if(evt.type==='update'){
                    var action = evt.target;
                    if(action.refreshReferences){
                        var refs = action.getReferences();
                        action.refreshReferences(evt.property,evt.value);
                    }
                }
            });

            //return all actions with non-empty tab field
            var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
                groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),
            //now flatten them
                _actionsFlattened = [];

            _.each(groupedTabs, function (items, name) {
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            var rootActions = [];
            _.each(tabbedActions, function (action) {
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            if (store.menuOrder) {
                rootActions = owner.sortGroups(rootActions, store.menuOrder);
            }

            //collect all existing actions as string array
            var allActionPaths = _.pluck(allActions, 'command');

            var tree = {};

            _.each(rootActions, function (level) {



                // collect all actions at level (File/View/...)
                var menuActions = owner.getItemsAtBranch(allActions, level);

                // convert action command strings to Action references
                var grouped = self.toActions(menuActions, store);
                //apt-get install automake autopoint bison build-essential ccache cmake curl cvs default-jre fp-compiler gawk gdc gettext git-core gperf libasound2-dev libass-dev libavcodec-dev libavfilter-dev libavformat-dev libavutil-dev libbluetooth-dev libbluray-dev libbluray1 libboost-dev libboost-thread-dev libbz2-dev libcap-dev libcdio-dev libcrystalhd-dev libcrystalhd3 libcurl3 libcurl4-gnutls-dev libcwiid-dev libcwiid1 libdbus-1-dev libenca-dev libflac-dev libfontconfig-dev libfreetype6-dev libfribidi-dev libglew-dev libiso9660-dev libjasper-dev libjpeg-dev libltdl-dev liblzo2-dev libmad0-dev libmicrohttpd-dev libmodplug-dev libmp3lame-dev libmpeg2-4-dev libmpeg3-dev libmysqlclient-dev libnfs-dev libogg-dev libpcre3-dev libplist-dev libpng-dev libpostproc-dev libpulse-dev libsamplerate-dev libsdl-dev libsdl-gfx1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libshairport-dev libsmbclient-dev libsqlite3-dev libssh-dev libssl-dev libswscale-dev libtiff-dev libtinyxml-dev libtool libudev-dev libusb-dev libva-dev libva-egl1 libva-tpi1 libvdpau-dev libvorbisenc2 libxml2-dev libxmu-dev libxrandr-dev libxrender-dev libxslt1-dev libxt-dev libyajl-dev mesa-utils nasm pmount python-dev python-imaging python-sqlite swig unzip yasm zip zlib1g-dev
                // group all actions by group
                var groupedActions = _.groupBy(grouped, function (action) {
                    if(action && action.command ==='Window/Navigation'){
                        //debugger;
                    }
                    var _vis = (action.visibility_ || {})[ visibility +'_val' ] || {};
                    if (action) {
                        return _vis.group || action.group;
                    }
                });

                //temp array
                var _actions = [];
                _.each(groupedActions, function (items, level) {
                    if (level !== 'undefined') {
                        _actions = _actions.concat(items);
                    }
                });

                //flatten out again
                menuActions = _.pluck(_actions, 'command');
                menuActions.grouped = groupedActions;
                tree[level] = menuActions;
            });

            var result = {
                root:tree,
                rootActions:rootActions,
                allActionPaths:_.pluck(allActions, 'command'),
                allActions:allActions
            }

            this.lastTree = result;

            return result;
        },
        constructor:function(options,node){
            this.target = node;
            utils.mixin(this,options);
        },
        onClose:function(e){
            this._rootMenu && this._rootMenu.parent().removeClass('open');
        },
        onOpen:function(){

            console.log('is open',this._rootMenu);
            var thiz=this;
            thiz._rootMenu && thiz._rootMenu.parent().addClass('open');
            /*
             setTimeout(function(){
             //thiz._rootMenu.focus();
             },1000);
             */

        },
        init: function(opts){


            console.error('init');
            if(this._didInit){
                console.error('did already init');
                return;
            }
            this._didInit=true;

            console.error('init ' + this.declaredClass + ' id ' + this.id);
            var options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root,'click',null,function(e){
                if(!self.isOpen){
                    return;
                }

                self.isOpen=false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display:''
                }).find('.drop-left').removeClass('drop-left');

                /*
                 $('.dropdown-context').fadeOut(options.fadeSpeed, function(e){
                 console.error('dropb');
                 self.isOpen=false;
                 self.onClose(e)
                 $('.dropdown-context').css({
                 display:''
                 }).find('.drop-left').removeClass('drop-left');
                 });
                 */

            });
            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }

            this.__on(root,'mouseenter', '.dropdown-submenu', function(e){

                try {
                    var _root = $(e.currentTarget);
                    var $sub = _root.find('.dropdown-context-sub:first');

                    if($sub.length==0){
                        console.error('--have no sub',_root);
                        return;
                    }

                    if (self.correctSubMenu == false) {

                        //console.log('abort mouse enter ' + self.declaredClass + ' id ' + self.id,self);
                        return;
                    }


                    //console.log('mouse enter ' + self.declaredClass + ' id ' + self.id + ' ' + self.menu[0].id,[self.menu ? self.menu[0] : null,_root[0]]);
                    console.log('mouse enter ', [self.menu[0],_root[0]]);
                    if (self.menu) {
                        //console.log('--------contains ' + $.contains(self.menu[0],_root[0]) + ' ||| ' + $.contains(self.menu[0],$sub[0]));

                        if (!$.contains(self.menu[0], _root[0])) {
                            //console.warn('--- not my menu!');
                            return;
                        }

                    } else {
                        //console.error('menu missing ' + self.delegate.declaredClass);
                        return;
                    }

                    var _disabled = _root.hasClass('disabled');

                    console.error('correct submenu '+_disabled);

                    if(_disabled){
                        $sub.css('display','none');
                        return;
                    }else{
                        $sub.css('display','');
                    }


                    //reset top
                    $sub.css({
                        top: 0
                    });

                    var autoH = $sub.height() + 0;
                    var totalH = $('html').height();
                    var pos = $sub.offset();
                    var overlapYDown = totalH - (pos.top + autoH);
                    if ((pos.top + autoH) > totalH) {
                        $sub.css({
                            top: overlapYDown - 30
                        }).fadeIn(options.fadeSpeed);
                    }

                    ////////////////////////////////////////////////////////////
                    var subWidth = $sub.width(),
                        subLeft = $sub.offset().left,
                        collision = (subWidth + subLeft) > window.innerWidth;

                    if (collision) {
                        //console.error('drop left ');
                        $sub.addClass('drop-left');
                    }
                }catch(e){
                    logError(e);
                }

            });
        },
        getDefaultOptions:function(){
            return options = {
                fadeSpeed: 0,
                filter: function ($obj) {
                    // Modify $obj, Do not return
                },
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: true

            };
        },
        buildMenuItems:function ($menu, data, id, subMenu, addDynamicTag) {

            this._debugMenu && console.log('build - menu items ',arguments);

            var linkTarget = '',
                self = this,
                visibility = this.visibility;

            for (var i = 0; i < data.length; i++) {

                var item = data[i],
                    $sub,
                    widget = item.widget;


                if (typeof item.divider !== 'undefined' && !item.widget) {

                    var divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    divider += '"></li>';
                    item.widget = divider;
                    $menu.append(divider);
                } else if (typeof item.header !== 'undefined' && !item.widget) {
                    var header = '<li class="nav-header';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + item.header + '</li>';
                    item.widget = header;
                    $menu.append(header);
                } else if (typeof item.menu_item_src !== 'undefined') {
                    var funcName;
                    if (typeof item.menu_item_src === 'function') {
                        if (item.menu_item_src.name === "") { // The function is declared like "foo = function() {}"
                            for (var globalVar in window) {
                                if (item.menu_item_src == window[globalVar]) {
                                    funcName = globalVar;
                                    break;
                                }
                            }
                        } else {
                            funcName = item.menu_item_src.name;
                        }
                    } else {
                        funcName = item.menu_item_src;
                    }
                    $menu.append('<li class="dynamic-menu-src" data-src="' + funcName + '"></li>');
                } else {

                    if (!widget && typeof item.href == 'undefined') {
                        item.href = '#';
                    }
                    if (!widget && typeof item.target !== 'undefined') {
                        linkTarget = ' target="' + item.target + '"';
                    }
                    if (typeof item.subMenu !== 'undefined' && !widget) {

                        var sub_menu = '<li class="dropdown-submenu';

                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a tabindex="-1" href="' + item.href + '">';

                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }

                        sub_menu += item.text + '';

                        sub_menu += '</a></li>';

                        $sub = $(sub_menu);

                    } else {


                        if(!widget) {

                            if (item.render) {
                                $sub = item.render(item, $menu);

                            } else {

                                var element = '<li class="" ';
                                element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                                element += '><a tabindex="-1" href="' + item.href + '"' + linkTarget + '>';
                                if (typeof data[i].icon !== 'undefined') {
                                    element += '<span class="glyphicon ' + item.icon + '"></span> ';

                                }
                                element += item.text + '</a></li>';
                                $sub = $(element);

                                if (item.postRender) {
                                    item.postRender($sub);
                                }

                            }
                        }
                    }

                    if (typeof item.action !== 'undefined' && !item.widget) {

                        if (item.addClickHandler && item.addClickHandler() === false) {

                        } else {
                            var $action = item.action;
                            if ($sub && $sub.find) {
                                $sub.find('a')
                                    .addClass('context-event')
                                    .on('click', createCallback($action, item, $sub));
                            }
                        }
                    }


                    if($sub && !widget) {

                        if(item.index==0) {
                            $menu.prepend($sub);
                        }else{
                            $menu.append($sub);
                        }

                        item.widget = $sub;

                        $sub.menu = $menu;

                    }

                    if (typeof item.subMenu != 'undefined' && !item.subMenuData) {

                        var subMenuData = self.buildMenu(item.subMenu, id, true);

                        $menu.subMenuData = subMenuData;

                        item.subMenuData = subMenuData;

                        $menu.find('li:last').append(subMenuData);

                    }
                }

                if(!$menu._didOnClick) {
                    $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                        e.stopPropagation()
                    });
                    $menu._didOnClick=true;
                }

            }
            return $menu;
        },
        buildMenu: function (data, id, subMenu) {
            var subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ',
                $menu = $('<ul tabindex="1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>');

            if(!subMenu){
                if(this._rootMenu){
                    console.error('have already root menu');
                }
                this._rootMenu =$menu;
            }
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        createNewAction:function(command){

            if(!command || !command.split){
                debugger;
            }
            var segments = command.split('/');
            var lastSegment = segments[segments.length - 1];
            var action = new Action({
                command: command,
                label: lastSegment,
                group: lastSegment
            });
            return action;
        },
        findAction:function(command){
            var stores = this.actionStores,
                action = null;

            _.each(stores,function(store){

                var _action = store.getSync(command);
                if(_action){
                    action = _action;
                }
            });

            return action;
        },
        getAction:function(command,store){
            store = store || this.store;
            var action = null;
            if(store) {
                action = this.findAction(command);
                if(!action){
                    action = this.createNewAction(command);
                }
            }

            return action;
        },
        getActions:function(query){
            var result = [];
            var stores = this.actionStores,
                self = this,
                visibility = this.visibility;

            _.each(stores,function(store){
                result = result.concat(store.query(query));
            });
            result = result.filter(function(action){
                var actionVisibility = action.getVisibility != null ? action.getVisibility(visibility) : {};
                if(action.show===false || actionVisibility === false || actionVisibility.show === false){
                    return false;
                }
                return true;
            });

            return result;
        },
        toActions:function(commands,store){
            var result = [],
                self = this;
            _.each(commands, function (path) {
                var _action = self.getAction(path,store);
                _action && result.push(_action);
            });
            return result;
        },
        onRunAction:function(action,owner,e){
            var command = action.command;
            action = this.findAction(command);
            _debug && console.log('run action ',action.command);
            return DefaultActions.defaultHandler.apply(action.owner||owner,[action,e]);
        },
        getActionProperty:function(action,visibility,prop){
            var value = prop in action ? action[prop] : null;
            if(visibility && prop in visibility){
                value = visibility[prop];
            }
            return value;
        },
        toMenuItem:function(action, owner, label, icon, visibility,showKeyCombo) {
            var self = this,
                labelLocalized = self.localize(label),
                widgetArgs = visibility.widgetArgs,
                actionType = visibility.actionType || action.actionType;



            var item = {
                text: labelLocalized,
                icon: icon,
                data: action,
                owner:owner,
                command:action.command,
                addClickHandler: function (item) {

                    var action = this.data;
                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        return false;
                    }

                    return true;
                },
                render: function (data, $menu) {

                    var action = this.data;

                    var parentAction = action.getParent ? action.getParent() : null;
                    var closeOnClick = self.getActionProperty(action,visibility,'closeOnClick');

                    var keyComboString = ' \n';
                    if (action.keyboardMappings && showKeyCombo!==false) {

                        var mappings = action.keyboardMappings;
                        var keyCombos = _.pluck(mappings, ['keys']);
                        if (keyCombos && keyCombos[0]) {
                            keyCombos = keyCombos[0];

                            if (keyCombos.join) {
                                var keycombo = [];
                                keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                            }
                        }
                    }


                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {


                        var element = '<li class="" >';
                        var id = action._store.id + '_' + action.command + '_' + self.id;
                        var checked = action.get('value');

                        //checkbox-circle
                        element += '<div class="checkbox checkbox-success ">';
                        if (typeof data.icon !== 'undefined') {
                            //element += '<span style="float: left" class="' + data.icon + '"></span>';
                        }
                        element += '<input id="' + id + '" type="checkbox" ' + (checked == true ? 'checked' : '') + '>';
                        element += '<label for="' + id + '">';
                        element += self.localize(data.text) + '</label>';
                        element += '</div>';

                        $menu.addClass('noclose');
                        var result = $(element);
                        var checkBox = result.find('INPUT');
                        checkBox.on('change', function (e) {

                            console.warn('change ' + checkBox[0].checked);

                            action._originReference = data;
                            action._originEvent = e;

                            action.set('value', checkBox[0].checked);

                            action._originReference = null;

                        });

                        self.setVisibilityField(action,'widget',data);

                        return result;
                    }



                    closeOnClick===false && $menu.addClass('noclose');
                    if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                        var value = action.value || action.get('value');
                        var parentValue = parentAction.get('value');
                        if (value == parentValue) {
                            icon = 'fa-check';
                        }
                    }

                    //default:
                    var element = '<li ';
                    element += '';
                    element += '><a title="' + data.text + '\n' + keyComboString + '" href="' + data.href + '"' + '#' + '>';


                    //icon
                    if (typeof data.icon !== 'undefined') {
                        //already html string
                        if(/<[a-z][\s\S]*>/i.test(data.icon)){
                            element +=data.icon;
                        }else
                        {
                            element += '<span class="icon ' + icon + '"></span> ';
                        }

                    }

                    element += data.text;

                    element += '<span style="max-width: 100px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';

                    element += '</a></li>';


                    self.setVisibilityField(action,'widget',data);


                    return $(element);


                },
                get: function (key) {},
                set: function (key, value) {

                    var widget = this.widget,
                        action = this.data;



                    function updateCheckbox(widget,checked){

                        //var _checkBoxNode = widget.find('INPUT');
                        var what = widget.find("input[type=checkbox]");
                        if(checked) {
                            what.prop("checked", true);
                        }else{
                            what.removeAttr('checked');
                        }

                    }


                    if (widget) {
                        if (key === 'disabled') {
                            if (widget.toggleClass) {
                                widget.toggleClass('disabled', value);
                            }
                        }
                        if (key === 'icon') {
                            var _iconNode = widget.find('.icon');
                            if(_iconNode) {
                                _iconNode.attr('class', 'icon');
                                this._lastIcon = this.icon;
                                this.icon = value;
                                _iconNode.addClass(value);
                            }else{
                                console.error('cant find icon node',widget);
                            }
                        }
                        if (key === 'value') {
                            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE||
                                actionType === types.ACTION_TYPE.SINGLE_TOGGLE)
                            {
                                if(action.command=='Step/Enable') {
                                    //_debugWidgets && console.log('set ' + key + ' ' + value);
                                    //console.error('---set checkbox ' + key + ' ' + value);
                                }
                                updateCheckbox(widget,value);
                            }
                        }
                    }
                },
                action: function (e, data, menu) {
                    _debug && console.log('menu action', data);
                    return self.onRunAction(data.data,owner,e);
                },
                destroy: function () {
                    if(this.widget){
                        this.widget.remove();
                    }
                }
            }
            return item;
        },
        attach: function(selector,data){
            this.target = selector;
            this.menu = this.addContext(selector,data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
            return this.menu;
        },
        addReference:function(action,item){
            if (action.addReference) {
                action.addReference(item, {
                    properties: {
                        "value": true,
                        "disabled": true,
                        "enabled": true
                    }
                }, true);
            }
        },
        onDidRenderActions:function(store,owner){
            if(owner && owner.refreshActions){
                owner.refreshActions();
            }
        },
        getActionData: function (action) {

            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label: actionVisibility.label != null ? actionVisibility.label : action.label,
                icon: actionVisibility.icon != null ? actionVisibility.icon : action.icon,
                command:actionVisibility.command != null ? actionVisibility.command : action.command,
                visibility: actionVisibility,
                group:actionVisibility.group != null ? actionVisibility.group : action.group,
                widget:actionVisibility.widget
            }
        },
        _clearAction:function(action){

        },
        _clear:function(){

            var actions = this.getActions();
            var store = this.store;
            if(store){
                this.actionStores.remove(store);
            }
            var self = this;

            actions = actions.concat(this._tmpActions);
            _.each(actions,function(action){
                if(action) {
                    var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        var widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        //console.log('remove reference ' + action.command,widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                        delete actionVisibility.widget;
                        actionVisibility.widget = null;
                    }
                }

            });

            this.$navBar && this.$navBar.empty();
        }
    });
    dcl.chainAfter(MenuMixinClass,'destroy');

    var _debugTree =false;
    var _debugMenuData = false
    var _debugOldMenuData = false;
    var _debugWidgets = true;
    /**
     *
     * @class module:xide/widgets/MainMenuActionRenderer
     */
    var ActionRendererClass =dcl(null, {
        renderTopLevel: function (name, where) {
            where = where || $(this.getRootContainer());
            var item = $('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) + '<b class="caret"></b></a>' +
                '</li>');

            where.append(item);
            return item;

        },
        getRootContainer: function () {
            return this.navBar;
        }

    });
    /**
     * @extends module:xide/widgets/MainMenuActionRenderer
     */
    var ContextMenu = dcl([_Widget.dcl, ActionContext.dcl, ActionMixin.dcl, ActionRendererClass,MenuMixinClass,_XWidget.StoreMixin], {
        target: null,
        openTarget:null,
        visibility: types.ACTION_VISIBILITY.CONTEXT_MENU,
        correctSubMenu:true,
        limitTo:null,
        declaredClass:'xide.widgets.ContextMenu',
        menuData:null,
        addContext: function (selector, data) {

            this.menuData = data;
            var id,
                $menu,
                self = this,
                target = this.openTarget ? (this.openTarget) : $(self.target);

            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                console.error('-add context 0');
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = self.buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {

                var d = new Date();
                id = d.getTime();
                $menu = self.buildMenu(data, id);
                selector.append($menu);
                this._rootMenu = $menu;
                this._rootId = id;

            }

            var root = $(document);



            this.__on(target, 'contextmenu', null, function (e) {
                if(self.limitTo){
                    var $target = $(e.target);
                    if($target.hasClass(self.limitTo)){

                    }
                    $target = $target.parent();
                    if(!$target.hasClass(self.limitTo)){
                        return;
                    }
                }
                
                self.openEvent = e;
                self.isOpen = true;
                self.onOpen(e);
                e.preventDefault();
                e.stopPropagation();
                var currentContextSelector = $(this);
                $('.dropdown-context:not(.dropdown-context-sub)').hide();

                var $dd = $('#dropdown-' + id);
                $dd.focus();
                $dd.css('zIndex',_Popup.nextZ(1));
                if (typeof options.above == 'boolean' && options.above) {
                    $dd.css({
                        top: e.pageY - 20 - $('#dropdown-' + id).height(),
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);

                } else if (typeof options.above == 'string' && options.above == 'auto') {
                    $dd.removeClass('dropdown-context-up');
                    var autoH = $dd.height() + 0;
                    var totalH = $('html').height();
                    var preferUp = true;
                    if ((e.pageY + autoH) > $('html').height()) {
                        var top = e.pageY - 20 - autoH;
                        if(top < 0){
                            top = 20;
                        }
                        $dd.css({
                            top: top,
                            left: e.pageX - 13
                        }).fadeIn(options.fadeSpeed);

                    } else {
                        $dd.css({
                            top: e.pageY + 10,
                            left: e.pageX - 13
                        }).fadeIn(options.fadeSpeed);
                    }
                }

                if (typeof options.left == 'boolean' && options.left) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width()
                    }).fadeIn(options.fadeSpeed);
                } else if (typeof options.left == 'string' && options.left == 'auto') {
                    $dd.removeClass('dropdown-context-left');
                    var autoL = $dd.width() - 12;
                    if ((e.pageX + autoL) > $('html').width()) {
                        $dd.addClass('dropdown-context-left').css({
                            left: e.pageX - $dd.width() + 13
                        });
                    }
                }
            });

            return $menu;
        },
        onRootAction:function(level,container){
            return null;
        },
        buildMenu:function (data, id, subMenu,update) {

            //console.error('-build ' + id);
            var subClass = subMenu ? ' dropdown-context-sub' : ' scrollable-menu ';
            var menuString = '<ul aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>';

            var $menu = null;

            if(update && this._rootMenu){
                //console.error('is update, use root menu');
                $menu = this._rootMenu;
            }
            if(update && !this._rootMenu){
                //console.error('is update : -have no root menu');
            }

            if(!update && !subMenu && !this._rootMenu){
                //console.error('first build');
                $menu = $(menuString);
                //this._rootMenu = $menu;
            }

            if(subMenu && !$menu){

                //var existing = $('dropdown-' + id);
                var existing = $('body').find('#dropdown-' + id)[0];
                //console.error('-build new menu for '+ id  + ' is update' + update,existing);
                $menu = $(menuString);
            }



            //update ? (this._rootMenu || $(menuString)) : $(menuString);

            if(!subMenu){
                //this._rootMenu = $menu;
            }
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        onActionAdded:function(actions){
            this.setActionStore(this.getActionStore(), this,false,true,actions);
        },
        clearAction : function(action){
            var self = this;
            if(action) {
                var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                if (actionVisibility) {
                    var widget = actionVisibility.widget;
                    action.removeReference && action.removeReference(widget);
                    if (widget && widget.destroy) {
                        widget.destroy();
                    }
                    delete actionVisibility.widget;
                    actionVisibility.widget = null;
                }
            }
        },
        onActionRemoved:function(evt){
            this.clearAction(evt.target);
        },
        removeCustomActions:function(){

            var oldStore = this.store;
            var oldActions = oldStore.query({
                    custom:true
                }),
                menuData=this.menuData;

            _.each(oldActions,function(action){

                oldStore.removeSync(action.command);

                var oldMenuItem = _.find(menuData,{
                    command:action.command
                });
                oldMenuItem && menuData.remove(oldMenuItem);
            });
        },
        setActionStore: function (store, owner,subscribe,update,itemActions) {
            if(!update){
                this._clear();
                this.addActionStore(store);
            }else{
                console.error('update');
            }

            var self = this,
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());
            var tree = update ? self.lastTree : self.buildActionTree(store,owner);

            var allActions = tree.allActions,
                rootActions = tree.rootActions,
                allActionPaths = tree.allActionPaths,
                oldMenuData = self.menuData;

            this.store = store;

            var data = [];


            if(subscribe!==false) {
                this.addHandle('added', store._on('onActionsAdded', function (actions) {
                    self.onActionAdded(actions);
                }));

                this.addHandle('delete', store.on('delete', function (evt) {
                    self.onActionRemoved(evt);
                }));
            }

            if(!update) {
                _.each(tree.root, function (menuActions, level) {
                    var root = self.onRootAction(level, rootContainer),
                        lastGroup = '',
                        lastHeader = {
                            header: ''
                        },
                        groupedActions = menuActions.grouped;


                    _.each(menuActions, function (command) {

                        var action = self.getAction(command, store),
                            isDynamicAction = false;

                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }

                        if (action) {

                            var renderData = self.getActionData(action);
                            var icon = renderData.icon,
                                label = renderData.label,
                                visibility = renderData.visibility,
                                group = renderData.group;


                            if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 2) {
                                if (lastGroup !== group) {
                                    lastHeader = {header: i18.localize(group)};
                                    data.push(lastHeader);
                                    lastGroup = group;
                                }
                            }


                            var item = self.toMenuItem(action, owner, label, icon, visibility || {});

                            data.push(item);

                            visibility.widget = item;

                            self.addReference(action, item);

                            function parseChildren(command, parent) {

                                var childPaths = new Path(command).getChildren(allActionPaths, false),
                                    isContainer = childPaths.length > 0,
                                    childActions = isContainer ? self.toActions(childPaths, store) : null;

                                if (childActions) {

                                    var subs = [];

                                    _.each(childActions, function (child) {

                                        var _renderData = self.getActionData(child);

                                        var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility);

                                        self.addReference(child, _item);

                                        subs.push(_item);

                                        var _childPaths = new Path(child.command).getChildren(allActionPaths, false),
                                            _isContainer = _childPaths.length > 0,
                                            _childActions = _isContainer ? self.toActions(_childPaths, store) : null;

                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }

                            parseChildren(command, item);
                        }
                    });
                });
                var menu = self.attach($('body'), data);
                self.onDidRenderActions(store, owner);
            }else{

                if(itemActions || !_.isEmpty(itemActions)) {

                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            var action = self.getAction(newAction.command);
                            if (action) {

                                var renderData = self.getActionData(action),
                                    icon = renderData.icon,
                                    label = renderData.label,
                                    aVisibility = renderData.visibility,
                                    group = renderData.group,
                                    item = self.toMenuItem(action, owner, label, icon, aVisibility || {});

                                aVisibility.widget = item;

                                self.addReference(newAction, item);

                                var parent = _.find(oldMenuData,{
                                    command:action.getParentCommand()
                                });

                                if(parent){
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });

                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }
        }
    });

    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_ICON = types.ACTION_ICON,
        ribbon,
        CIS;

    var ctx = window.sctx,root;
    function doTests(tab) {



        grid = FTestUtils.createFileGrid('root', {}, {}, 'TestGrid', module.id, true, tab);

        var menuNode = grid.header;
        var context = new ContextMenu({
            //actionStores:[ctx.getActionStore()]
        }, grid.domNode);

        context.openTarget = grid.domNode;
        context.init({preventDoubleContext: false});
        grid.resize();

        tab.add(context, null, false);

        //////////////////////////////////////////////////
        context._registerActionEmitter(grid);

        return;
    }

    function doTestsBlocks(tab) {
        var mixin = {
            onCIChanged: function (ci, block, oldValue, newValue, field, cis) {

                if (oldValue === newValue) {
                    return;
                }
                console.error('on ci change ' + field + ' ' + newValue);
                var _col = this.collection;

                block = this.collection.getSync(block.id);
                block.set(field, newValue);

                this.refreshActions();

                block[field] = newValue;
                _col.emit('update', {
                    target: block,
                    type: 'update'
                });

                block._store.emit('update', {
                    target: block
                });
                this._itemChanged('changed', block, _col);
                block.onChangeField(field, newValue, cis, this);
            }
        }

        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            permissions: [
                ACTION.CLIPBOARD,
                'Step/Properties',
                ACTION.TOOLBAR,
                ACTION.LAYOUT
            ]
        });

        utils.mixin(grid,mixin);

        var menuNode = grid.header;
        var context = new ContextMenu({
        }, grid.domNode);

        context.openTarget = grid.domNode;
        context.init({preventDoubleContext: false});
        grid.resize();

        grid.getContextMenu = function(){
            return context;
        }

        tab.add(context, null, false);

        //////////////////////////////////////////////////
        context._registerActionEmitter(grid);

        context.setActionEmitter(grid, 'click');



        function addItem(scope,name){

            var thiz = this;
            var cmd = factory.createBlock(Variable, {
                    name: name || "%%No Title",
                    scope: scope,
                    group: 'click'
                });

            grid.refresh();
        }

        var scope = grid.blockScope,
            store = scope.blockStore;

        var root  = 'Block/Insert';

        var thiz = grid;

        store.on("added",function(block){

            if(block.declaredClass.includes('model.Variable')){

                var setVariableRoot= root+'/Set Variable';
                var defaultMixin = { addPermission: true };

                var permissions = thiz.permissions;
                var result = [];

                result.push(thiz.createAction({
                    label: block.name,
                    command: ''  + setVariableRoot+'/'+block.name,
                    icon: 'fa-paper-plane',
                    tab: 'Home',
                    group: 'Step',
                    mixin: utils.mixin({
                        item:block,
                        proto:VariableAssignmentBlock,
                        quick:true,
                        ctrArgs: {
                            variable: block.id,
                            variableId: block.id,
                            scope: thiz.blockScope,
                            value: '0'
                        }
                    },defaultMixin)
                }));

                var newStoredActions = thiz.addActions(result);
                var actionStore = grid.getActionStore();
                context.onActionAdded(newStoredActions);
            }
        });


        var actionStore = grid.getActionStore();

        return;
        var blockInsert = grid.getAction('Block/Insert');
        var disable = function (disable) {

            blockInsert.set('disabled', disable);
            setTimeout(function () {
                blockInsert.getReferences().forEach(function (ref) {
                    ref.set('disabled', disable);
                });
            }, 100);

            blockInsert.refresh();

        };



        disable(true);
        setTimeout(function(){
            disable(true);
            return;
        },3000);
        return;
    }



    if (ctx) {
        var parent = TestUtils.createTab(null, null, module.id);
        doTestsBlocks(parent);
        return declare('a', null, {});

    }
    return _Widget;

});




