/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    "dcl/inherited",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "./TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "xide/registry",

    "module",

    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/on",
    "dojo/sniff", // has("ie")
    "dojo/string" // string.substitute string.trim


], function (declare,dcl,inherited,types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, on, has, string
) {




    console.clear();
    console.log('--do-tests');




    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    var propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };

    function createClass_AttachMixin(){

        "use strict";

        /**
         *
         */

        var attachAttribute = 'attachTo';

        /**
         *		Mixin for widgets to attach to dom nodes and setup events via
         *		convenient data-dojo-attach-point and data-dojo-attach-event DOM attributes.
         *
         *		Superclass of _TemplatedMixin, and can also be used standalone when templates are pre-rendered on the
         *		server.
         *
         *		Does not [yet] handle widgets like ContentPane with this.containerNode set.   It should skip
         *		scanning for data-dojo-attach-point and data-dojo-attach-event inside this.containerNode, but it
         *		doesn't.


         * _attachPoints: [private] String[]
         *		List of widget attribute names associated with data-dojo-attach-point=... in the
         *		template, ex: ["containerNode", "labelNode"]
         _attachPoints: [],

         * _attachEvents: [private] Handle[]
         *		List of connections associated with data-dojo-attach-event=... in the
         *		template
         _attachEvents: [],

         * attachScope: [public] Object
         *		Object to which attach points and events will be scoped.  Defaults
         *		to 'this'.
         attachScope: undefined,
         */
        var _AttachMixin = dcl(null, {
            __attachAsjQueryObject:true,
            declaredClass:"xide/_base/_AttachMixin",
            /**
             * Attach to DOM nodes marked with special attributes.
             */
            buildRendering: function(){
                this._attachPoints = [];
                console.log('AttachMixin:: buildRendering');
                // recurse through the node, looking for, and attaching to, our
                // attachment points and events, which should be defined on the template node.
                this._attachTemplateNodes(this.domNode);
                this._beforeFillContent();		// hook for _WidgetsInTemplateMixin
            },

            _beforeFillContent: function(){},            
            /**
             * Iterate through the dom nodes and attach functions and nodes accordingly.
             * @description Map widget properties and functions to the handlers specified in
             *		the dom node and it's descendants. This function iterates over all
             *		nodes and looks for these properties:
             *	    - attachTo
             *
             * @param rootNode {HTMLElement}
             **/ 
            _attachTemplateNodes: function(rootNode){

                // DFS to process all nodes except those inside of this.containerNode
                var node = rootNode;


                while(true){

                    if  (
                            node.nodeType == 1 &&
                            ( this._processTemplateNode(node,function(n,p){return n.getAttribute(p);},this._attach)) &&
                            node.firstChild
                        )
                    {
                        node = node.firstChild;
                    }else{


                        if(node == rootNode){
                            return;
                        }

                        while(!node.nextSibling){

                            node = node.parentNode;

                            if(node == rootNode){
                                return;
                            }
                        }

                        node = node.nextSibling;
                    }
                }
            },

            _processTemplateNode: function(baseNode, getAttrFunc, attachFunc){
                // summary:
                //		Process data-dojo-attach-point and data-dojo-attach-event for given node or widget.
                //		Returns true if caller should process baseNode's children too.

                var ret = true;

                // Process data-dojo-attach-point
                var _attachScope = this.attachScope || this,
                    attachPoint = getAttrFunc(baseNode, attachAttribute);

                if(attachPoint){
                    var point, points = attachPoint.split(/\s*,\s*/);
                    while((point = points.shift())){
                        if(_.isArray(_attachScope[point])){
                            _attachScope[point].push(baseNode);
                        }else{
                            _attachScope[point] = baseNode;                            
                            this.__attachAsjQueryObject &&  (_attachScope['$'+point] = $(baseNode));
                        }
                        ret = (point != "containerNode");
                        this._attachPoints.push(point);
                    }
                }
                return ret;
            },
            /**
             * Detach and clean up the attachments made in _attachtempalteNodes.
             * @private
             */
            _detachTemplateNodes: function() {
                
                // Delete all attach points to prevent IE6 memory leaks.
                var _attachScope = this.attachScope || this;
                _.each(this._attachPoints, function(point){
                    delete _attachScope[point];
                });
                this._attachPoints = [];
            },

            destroyRendering: function(){
                this._detachTemplateNodes();
                this.inherited(arguments);
            }
        });
        return _AttachMixin;
    }

    function createClass_Templated(){


        "use strict";

        var _base = createClass_AttachMixin();


        // module:
        //		dijit/_TemplatedMixin

        var _TemplatedMixin = dcl(_base, {

            declaredClass:"xide/_base/_TemplatedMixin",
            // summary:
            //		Mixin for widgets that are instantiated from a template

            // templateString: [protected] String
            //		A string that represents the widget template.
            //		Use in conjunction with dojo.cache() to load from a file.
            templateString: null,

            // templatePath: [protected deprecated] String
            //		Path to template (HTML file) for this widget relative to dojo.baseUrl.
            //		Deprecated: use templateString with require([... "dojo/text!..."], ...) instead
            templatePath: null,

            // skipNodeCache: [protected] Boolean
            //		If using a cached widget template nodes poses issues for a
            //		particular widget class, it can set this property to ensure
            //		that its template is always re-built from a string
            _skipNodeCache: true,

            /*=====
             // _rendered: Boolean
             //		Not normally use, but this flag can be set by the app if the server has already rendered the template,
             //		i.e. already inlining the template for the widget into the main page.   Reduces _TemplatedMixin to
             //		just function like _AttachMixin.
             _rendered: false,
             =====*/

            _stringRepl: function(tmpl){
                // summary:
                //		Does substitution of ${foo} type properties in template string
                // tags:
                //		private
                var className = this.declaredClass, _this = this;
                // Cache contains a string because we need to do property replacement
                // do the property replacement
                return string.substitute(tmpl, this, function(value, key){
                    if(key.charAt(0) == '!'){ value = lang.getObject(key.substr(1), false, _this); }
                    if(typeof value == "undefined"){ throw new Error(className+" template:"+key); } // a debugging aide
                    if(value == null){ return ""; }

                    // Substitution keys beginning with ! will skip the transform step,
                    // in case a user wishes to insert unescaped markup, e.g. ${!foo}
                    return key.charAt(0) == "!" ? value : this._escapeValue("" + value);
                }, this);
            },

            _escapeValue: function(/*String*/ val){
                // summary:
                //		Escape a value to be inserted into the template, either into an attribute value
                //		(ex: foo="${bar}") or as inner text of an element (ex: <span>${foo}</span>)

                // Safer substitution, see heading "Attribute values" in
                // http://www.w3.org/TR/REC-html40/appendix/notes.html#h-B.3.2
                // and also https://www.owasp.org/index.php/XSS_%28Cross_Site_Scripting%29_Prevention_Cheat_Sheet#RULE_.231_-_HTML_Escape_Before_Inserting_Untrusted_Data_into_HTML_Element_Content
                return val.replace(/["'<>&]/g, function(val){
                    return {
                        "&": "&amp;",
                        "<": "&lt;",
                        ">": "&gt;",
                        "\"": "&quot;",
                        "'": "&#x27;"
                    }[val];
                });
            },
            /*
             *
             * @description Construct the UI for this widget from a template, setting this.domNode.
             */
            buildRendering: dcl.superCall(function(sup){

                return function(){

                    if(!this._rendered){

                        console.log('render template ' + this.id + ' \nclass '  + this.declaredClass  + '\ntemplate ' + this.templateString);

                        if(!this.templateString){
                            this.templateString = cache(this.templatePath, {sanitize: true});
                        }
                        // Lookup cached version of template, and download to cache if it
                        // isn't there already.  Returns either a DomNode or a string, depending on
                        // whether or not the template contains ${foo} replacement parameters.


                        var cached = _TemplatedMixin.getCachedTemplate(this.templateString, this._skipNodeCache, this.ownerDocument);
                        var node;
                        if(_.isString(cached)){
                            //node =  domConstruct.toDom(this._stringRepl(cached), this.ownerDocument);
                            node =  $(this._stringRepl(cached))[0];
                            if(node.nodeType != 1){
                                // Flag common problems such as templates with multiple top level nodes (nodeType == 11)
                                throw new Error("Invalid template: " + cached);
                            }
                        }else{
                            // if it's a node, all we have to do is clone it
                            node = cached.cloneNode(true);
                        }

                        this.domNode = node;
                    }

                    // Call down to _WidgetBase.buildRendering() to get base classes assigned
                    // TODO: change the baseClass assignment to _setBaseClassAttr
                    sup.call(this, arguments);////this.inherited(arguments);
                    if(!this._rendered){
                        this._fillContent(this.srcNodeRef);
                    }
                    this._rendered = true;
                };

            }),
            /**
             *
             * @param source {HTMLElement}
             * @private
             */
            _fillContent: function(source){
                // summary:
                //		Relocate source contents to templated container node.
                //		this.containerNode must be able to receive children, or exceptions will be thrown.
                // tags:
                //		protected
                var dest = this.containerNode;
                if(source && dest){
                    while(source.hasChildNodes()){
                        dest.appendChild(source.firstChild);
                    }
                }
            }

        });

        // key is templateString; object is either string or DOM tree
        _TemplatedMixin._templateCache = {};

        _TemplatedMixin.getCachedTemplate = function(templateString, alwaysUseString, doc){
            // summary:
            //		Static method to get a template based on the templatePath or
            //		templateString key
            // templateString: String
            //		The template
            // alwaysUseString: Boolean
            //		Don't cache the DOM tree for this template, even if it doesn't have any variables
            // doc: Document?
            //		The target document.   Defaults to document global if unspecified.
            // returns: Mixed
            //		Either string (if there are ${} variables that need to be replaced) or just
            //		a DOM tree (if the node can be cloned directly)

            // is it already cached?
            var tmplts = _TemplatedMixin._templateCache;
            var key = templateString;
            var cached = tmplts[key];
            if(cached){
                try{
                    // if the cached value is an innerHTML string (no ownerDocument) or a DOM tree created within the
                    // current document, then use the current cached value
                    if(!cached.ownerDocument || cached.ownerDocument == (doc || document)){
                        // string or node of the same document
                        return cached;
                    }
                }catch(e){ /* squelch */ } // IE can throw an exception if cached.ownerDocument was reloaded
                domConstruct.destroy(cached);
            }


            templateString = _.trim(templateString);



            if(alwaysUseString || templateString.match(/\$\{([^\}]+)\}/g)){
                // there are variables in the template so all we can do is cache the string
                return (tmplts[key] = templateString); //String
            }else{
                // there are no variables in the template so we can cache the DOM tree
                var node = domConstruct.toDom(templateString, doc);
                if(node.nodeType != 1){
                    throw new Error("Invalid template: " + templateString);
                }
                return (tmplts[key] = node); //Node
            }
        };

        return _TemplatedMixin;

    }

    function createClass_WidgetBase(){


        "use strict";

        var tagAttrs = {};

        function getAttrs(obj){
            var ret = {};
            for(var attr in obj){
                ret[attr.toLowerCase()] = true;
            }
            return ret;
        }

        function nonEmptyAttrToDom(attr){
            // summary:
            //		Returns a setter function that copies the attribute to this.domNode,
            //		or removes the attribute from this.domNode, depending on whether the
            //		value is defined or not.
            return function(val){
                $(this.domNode)[val ? "attr" : "removeAttr"](attr, val);
                this._set(attr, val);
            };
        }

        function isEqual(a, b){
            //	summary:
            //		Function that determines whether two values are identical,
            //		taking into account that NaN is not normally equal to itself
            //		in JS.

            return a === b || (/* a is NaN */ a !== a && /* b is NaN */ b !== b);
        }

        var Implementation = {
            _attrPairNames: {}, // shared between all widgets
            attributeMap: {},
            declaredClass:'xide/_base/_Widget',
            on:function(type,handler){
                return this._on(type,handler);
            },
            emit:function(type,args){
                return this._emit(type,args);
            },
            _set: function(/*String*/ name, /*anything*/ value){
                // summary:
                //		Helper function to set new value for specified property, and call handlers
                //		registered with watch() if the value has changed.
                var oldValue = this[name];
                this[name] = value;

                if(this._created && !isEqual(oldValue, value)){

                    this._watchCallbacks && this._watchCallbacks(name, oldValue, value);

                    this.emit("attrmodified-" + name, {
                        detail: {
                            prevValue: oldValue,
                            newValue: value
                        }
                    });
                }
            },
            /**
             * Helper function to get value for specified property stored by this._set(),
             * i.e. for properties with custom setters.  Used mainly by custom getters.
             *  For example, CheckBox._getValueAttr() calls this._get("value").
             * @param name {string}
             * @returns {*}
             * @private
             */
            _get: function( name){
                // future: return name in this.props ? this.props[name] : this[name];
                return this[name];
            },
            /**
             *  Helper function for get() and set().
             *  Caches attribute name values so we don't do the string ops every time.
             * @param name
             * @returns {*}
             * @private
             */
            _getAttrNames: function(name){
                var apn = this._attrPairNames;
                if(apn[name]){
                    return apn[name];
                }
                var uc = name.replace(/^[a-z]|-[a-zA-Z]/g, function(c){
                    return c.charAt(c.length - 1).toUpperCase();
                });
                return (apn[name] = {
                    n: name + "Node",
                    s: "_set" + uc + "Attr", // converts dashes to camel case, ex: accept-charset --> _setAcceptCharsetAttr
                    g: "_get" + uc + "Attr",
                    l: uc.toLowerCase()        // lowercase name w/out dashes, ex: acceptcharset
                });
            },
            /**
             * Set a property on a widget
             * @description Sets named properties on a widget which may potentially be handled by a setter in the widget.
             *
             *		For example, if the widget has properties `foo` and `bar`
             *		and a method named `_setFooAttr()`, calling
             *		`myWidget.set("foo", "Howdy!")` would be equivalent to calling
             *		`widget._setFooAttr("Howdy!")` and `myWidget.set("bar", 3)`
             *		would be equivalent to the statement `widget.bar = 3;`
             *
             *      This is equivalent to calling `set(foo, "Howdy")` and `set(bar, 3)`
             *
             *		set() may also be called with a hash of name/value pairs, ex:
             *
             *	@example
             *	myWidget.set({
                    foo: "Howdy",
                    bar: 3
                    });
             * 
             * @param name {string} The property to set.
             * @param value {object|null}
             * @returns {*}
             */
            set: function(name, value){

                console.log('set ' + name + ' val = ' +value);
                if(typeof name === "object"){
                    for(var x in name){
                        this.set(x, name[x]);
                    }
                    return this;
                }
                var names = this._getAttrNames(name),
                    setter = this[names.s];

                if(_.isFunction(setter)){
                    // use the explicit setter
                    var result = setter.apply(this, Array.prototype.slice.call(arguments, 1));
                }else{


                    // Mapping from widget attribute to DOMNode/subwidget attribute/value/etc.
                    // Map according to:
                    //		1. attributeMap setting, if one exists (TODO: attributeMap deprecated, remove in 2.0)
                    //		2. _setFooAttr: {...} type attribute in the widget (if one exists)
                    //		3. apply to focusNode or domNode if standard attribute name, excluding funcs like onClick.
                    // Checks if an attribute is a "standard attribute" by whether the DOMNode JS object has a similar
                    // attribute name (ex: accept-charset attribute matches jsObject.acceptCharset).
                    // Note also that Tree.focusNode() is a function not a DOMNode, so test for that.
                    var defaultNode = this.focusNode && !lang.isFunction(this.focusNode) ? "focusNode" : "domNode",
                        tag = this[defaultNode] && this[defaultNode].tagName,
                        attrsForTag = tag && (tagAttrs[tag] || (tagAttrs[tag] = getAttrs(this[defaultNode]))),
                        map = name in this.attributeMap ? this.attributeMap[name] :
                            names.s in this ? this[names.s] :
                                ((attrsForTag && names.l in attrsForTag && typeof value != "function") ||
                                /^aria-|^data-|^role$/.test(name)) ? defaultNode : null;
                    if(map != null){
                        this._attrToDom(name, value, map);
                    }

                    this._set(name, value);

                }
                return this;
            },

            /*
            __buildRenderin:dcl.superCall(function(sup){
                debugger;
                console.log('_WidgetBase:: buildRendering');
                return function(){};
            }),
            */
            postCreate:function(){
                console.log('_WidgetBase:: postCreate');
            },
            postMixInProperties:function(){
                console.log('_WidgetBase:: postMixInProperties');
            },
            create: function(params, srcNodeRef){

                console.log('_WidgetBase:: create ');


                // summary:
                //		Kick off the life-cycle of a widget
                // description:
                //		Create calls a number of widget methods (postMixInProperties, buildRendering, postCreate,
                //		etc.), some of which of you'll want to override. See http://dojotoolkit.org/reference-guide/dijit/_WidgetBase.html
                //		for a discussion of the widget creation lifecycle.
                //
                //		Of course, adventurous developers could override create entirely, but this should
                //		only be done as a last resort.
                // params: Object|null
                //		Hash of initialization parameters for widget, including scalar values (like title, duration etc.)
                //		and functions, typically callbacks like onClick.
                //		The hash can contain any of the widget's properties, excluding read-only properties.
                // srcNodeRef: DOMNode|String?
                //		If a srcNodeRef (DOM node) is specified:
                //
                //		- use srcNodeRef.innerHTML as my contents
                //		- if this is a behavioral widget then apply behavior to that srcNodeRef
                //		- otherwise, replace srcNodeRef with my generated DOM tree
                // tags:
                //		private

                // First time widget is instantiated, scan prototype to figure out info about custom setters etc.
                //this._introspect();

                // store pointer to original DOM tree
                this.srcNodeRef = $(srcNodeRef)[0];

                // No longer used, remove for 2.0.
                this._connects = [];
                this._supportingWidgets = [];

                // this is here for back-compat, remove in 2.0 (but check NodeList-instantiate.html test)

                if(this.srcNodeRef && this.srcNodeRef.id){
                    this.id = this.srcNodeRef.id;
                }


                // mix in our passed parameters
                if(params){
                    this.params = params;
                    utils.mixin(this, params);
                }

                this.postMixInProperties();

                // Generate an id for the widget if one wasn't specified, or it was specified as id: undefined.
                // Do this before buildRendering() because it might expect the id to be there.
                if(!this.id){
                    this.id = registry.getUniqueId(this.declaredClass.replace(/\//g, "_"));
                    if(this.params){
                        // if params contains {id: undefined}, prevent _applyAttributes() from processing it
                        delete this.params.id;
                    }
                }


                // The document and <body> node this widget is associated with
                this.ownerDocument = this.ownerDocument || (this.srcNodeRef ? this.srcNodeRef.ownerDocument : document);
                this.ownerDocumentBody = $('body')[0];
                registry.add(this);

                this.buildRendering();

                var deleteSrcNodeRef,
                    preserveNodes;


                if(this.domNode){
                    // Copy attributes listed in attributeMap into the [newly created] DOM for the widget.
                    // Also calls custom setters for all attributes with custom setters.
                    //this._applyAttributes();

                    // If srcNodeRef was specified, then swap out original srcNode for this widget's DOM tree.
                    // For 2.0, move this after postCreate().  postCreate() shouldn't depend on the
                    // widget being attached to the DOM since it isn't when a widget is created programmatically like
                    // new MyWidget({}).	See #11635.
                    var source = this.srcNodeRef;
                    if(source && source.parentNode && this.domNode !== source){
                        source.parentNode.replaceChild(this.domNode, source);
                        deleteSrcNodeRef = true;
                    }

                    // Note: for 2.0 may want to rename widgetId to dojo._scopeName + "_widgetId",
                    // assuming that dojo._scopeName even exists in 2.0
                    //this.domNode.setAttribute("widgetId", this.id);
                    this.domNode.setAttribute("id", this.id);
                }
                this.postCreate();

                this._created = true;
            },
            constructor:function(params,container){
                console.log('_WidgetBase:: constructor');
                this.postscript(params,container);
            },
            postscript:function(params,srcNodeRef){
                console.log('_WidgetBase:: postscript');
                return this.create(params, srcNodeRef);
            },
            /**
             *		Returns all direct children of this widget, i.e. all widgets underneath this.containerNode whose parent
             *		is this widget.   Note that it does not return all descendants, but rather just direct children.
             *		Analogous to [Node.childNodes](https:*developer.mozilla.org/en-US/docs/DOM/Node.childNodes),
             *		except containing widgets rather than DOMNodes.
             *
             *		The result intentionally excludes internally created widgets (a.k.a. supporting widgets)
             *		outside of this.containerNode.
             *
             *		Note that the array returned is a simple array.  Application code should not assume
             *		existence of methods like forEach().
             *
             * @returns {*}
             */
            getChildren: function(){
                return this.containerNode ? registry.findWidgets(this.containerNode) : []; // dijit/_WidgetBase[]
            },
            getParent: function(){
                // summary:
                //		Returns the parent widget of this widget.
                return registry.getEnclosingWidget(this.domNode.parentNode);
            },
            //////////// DESTROY FUNCTIONS ////////////////////////////////
            /**
             * Destroy this widget and its descendants
             * @description If true, this method will leave the original DOM structure
             *		alone of descendant Widgets. Note: This will NOT work with
             *		dijit._TemplatedMixin widgets.             
             * @param preserveDom {boolean}
             */
            destroyRecursive: function(preserveDom){
                this._beingDestroyed = true;
                this.destroyDescendants(preserveDom);
                this.destroy(preserveDom);
            },

            destroy: function(/*Boolean*/ preserveDom){


                console.log('_destroy ' + this.id);

                // summary:
                //		Destroy this widget, but not its descendants.  Descendants means widgets inside of
                //		this.containerNode.   Will also destroy any resources (including widgets) registered via this.own().
                //
                //		This method will also destroy internal widgets such as those created from a template,
                //		assuming those widgets exist inside of this.domNode but outside of this.containerNode.
                //
                //		For 2.0 it's planned that this method will also destroy descendant widgets, so apps should not
                //		depend on the current ability to destroy a widget without destroying its descendants.   Generally
                //		they should use destroyRecursive() for widgets with children.
                // preserveDom: Boolean
                //		If true, this method will leave the original DOM structure alone.
                //		Note: This will not yet work with _TemplatedMixin widgets

                this._beingDestroyed = true;

                function destroy(w){
                    if(w.destroyRecursive){
                        w.destroyRecursive(preserveDom);
                    }else if(w.destroy){
                        w.destroy(preserveDom);
                    }
                }

                // Destroy supporting widgets, but not child widgets under this.containerNode (for 2.0, destroy child widgets
                // here too).   if() statement is to guard against exception if destroy() called multiple times (see #15815).
                if(this.domNode){
                    _.each(registry.findWidgets(this.domNode, this.containerNode), destroy);
                }

                this.destroyRendering(preserveDom);

                registry.remove(this.id);

                this._destroyed = true;
            },

            destroyRendering: function(/*Boolean?*/ preserveDom){
                // summary:
                //		Destroys the DOM nodes associated with this widget.
                // preserveDom:
                //		If true, this method will leave the original DOM structure alone
                //		during tear-down. Note: this will not work with _Templated
                //		widgets yet.
                // tags:
                //		protected

                if(this.domNode){
                    if(preserveDom){

                    }else{
                        domConstruct.destroy(this.domNode);
                    }
                    delete this.domNode;
                }

                if(this.srcNodeRef){
                    if(!preserveDom){
                        domConstruct.destroy(this.srcNodeRef);
                    }
                    delete this.srcNodeRef;
                }
            },


            destroyDescendants: function(/*Boolean?*/ preserveDom){
                // summary:
                //		Recursively destroy the children of this widget and their
                //		descendants.
                // preserveDom:
                //		If true, the preserveDom attribute is passed to all descendant
                //		widget's .destroy() method. Not for use with _Templated
                //		widgets.

                // get all direct descendants and destroy them recursively
                _.each(this.getChildren(), function(widget){                    
                    if(widget.destroyRecursive){
                        widget.destroyRecursive(preserveDom);
                    }
                });
            }
        }
        var Module = dcl(EventedMixin.dcl,Implementation);
        //dcl.chainBefore(Module, "buildRendering");
        return Module;

    }

    function createWidgetNormalClass(){

        var _class = createClass_WidgetBase();

        var _templatedClass = createClass_Templated();

        var _finalClass = dcl([_class,_templatedClass,_Widget.dcl],{});

        return _finalClass;
    }

    function doWidgetTests(tab){


        console.log('doWidgetTests');

        var _class = createClass_WidgetBase();

        var _templatedClass = createClass_Templated();

        //var _attachClass = createClass_AttachMixin();

        var _finalClass = dcl([_class,_templatedClass],{

        });



        var _baseWidget = new _finalClass({
            templateString:'<div>helo 2<button attachTo="button" class="btn btn-warning">warning</button></div>'
        },tab.containerNode);



        console.log('_base_widget',_baseWidget);




    }

    function createTabClass(baseClass){

        return dcl(baseClass,{
            declaredClass:'xide/widgets/TabContainerTab',
            iconClass:null,
            open:true,
            titleBar:null,
            titleNode:null,
            toggleNode:null,
            containerNode:null,
            height:'400px',
            padding:'0px',
            panelNode:null,
            templateString:'<div></div>',
            resize:function(){

                console.log('resize tab');
                this.inherited(arguments);
            },
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);


                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            _onShown:function(e){
                this.resize();
            },
            addChild:function(what,index,startup){

                //collect widget
                this.add(what,null);

                utils.addChild(this.containerNode,what.domNode);

                if(startup!==false && !what._started){
                    what.startup();
                }
            },
            buildRendering:function(){

                this.inherited(arguments);

                var self = this;
                var panel = this.panelNode;
                this.__addHandler(panel,'hidden.bs.collapse','_onHided');
                this.__addHandler(panel,'hide.bs.collapse','_onHide');
                this.__addHandler(panel,'shown.bs.collapse','_onShown');
                this.__addHandler(panel,'show.bs.collapse','_onShow');


            },
            postMixInProperties:function(){

                var closed = !this.open;
                this.ariaOpen = closed ? 'true' : 'false';
                this.containerClass = closed ? 'collapse' : 'collapse in';
                this.titleClass = closed ? 'collapsed' : '';

                var widgetStr = '<div style="float: :right" class="widget-controls">'+
                        '<a title="Options" href="#"><i class="fa-cogs"></i></a>'+
                        /*'<a data-widgster="expand" title="Expand" href="#"><i class="fa-chevron-up"></i></a>'+
                        '<a data-widgster="collapse" title="Collapse" href="#"><i class="fa-chevron-down"></i></a>'+*/
                        '<a data-widgster="close" title="Close" href="#"><i class="fa-remove"></i></a>'+
                    '</div>';

                widgetStr ='';

                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';
                var titleStr = '<span attachTo="titleNode" class=""> ${!title}</span>';
                var toggleNodeStr =
                    '<a attachTo="toggleNode" href="#${!id}-Collapse" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                        iconStr +   titleStr
                    '</a>';




                this.templateString = '<div class="panel widget" attachTo="panelNode">'+

                        '<div class="panel-heading" attachTo="titleBar">'+
                            toggleNodeStr +
                        '</div>'+

                        '<div attachTo="containerRoot" class="panel-collapse ${!containerClass}" id="${!id}-Collapse" aria-expanded="${!ariaOpen}">'+
                            '<div style="height: ${!height};padding:${!padding}" class="panel-body" attachTo="containerNode"></div>'+
                        '</div>'+

                    '</div>';


                this.inherited(arguments);
            }
        });
    };

    function _createTemplateBaseClass(){

        var templated = dcl(null,{

            templateUrl:'xide/tests/accordion_test.html',
            buildRenderering:function(){

                var _templateText = this._getTemplate();

                var root = $('div');
                var node = root.loadTemplate(require.toUrl(this.templateUrl));
                this.domNode = root;
            },
            _getTemplate:function(){
                return this.templateString || this._getText(this.templateUrl);
            },
            startup:function(){

                this.buildRenderering();

            },
            _getText:function(url){

                url = require.toUrl(url);
                var text = $.ajax({
                    url: url,
                    async:false
                });
                return text.responseText;
            }
        });

        return templated;

    }

    function doTests(tabContainer,tabClass){

        var pane = tabContainer.createTab('bla bla','fa-code',false,tabClass);
        var pane2 = tabContainer.createTab('Files','fa-cogs',true,tabClass);


        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane2);



        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane);



        /*
        setTimeout(function(){
            //pane.show();
        },1000);


        setTimeout(function(){
            pane.hide();
        },5000);
*/
    }

    function _createTabContainerClass(baseClass){

        var tabClass = dcl(baseClass,{
            declaredClass:'xide/widgets/TabContainer',
            templateString:'<div class="panel-group" attachTo="containerNode"/>',
            createTab:function(title,icon,open,tabClass){


                /*
                var _tab= new tabClass({
                    title:title,
                    iconClass:icon,
                    open:open
                },this.containerNode);
                */
/*
                var _tab= utils.addWidget(tabClass,{
                    title:title,
                    iconClass:icon,
                    open:open
                },this,this.containerNode,true);

                console.log('-------------add tab',_tab);


                return _tab;
                */
                return this.add(tabClass,{
                    title:title,
                    iconClass:icon,
                    open:open
                },this.containerNode,true);
            }

        });

        return tabClass;

    }

    function _createAccContainer(){

        var tabClass = declare(TemplatedWidgetBase,{
            templateString:'<div class="panel-group" data-dojo-attach-point="containerNode"/>',
            createTab:function(title,icon){

                return utils.addWidget(createTabClass(),{
                    title:title,
                    iconClass:icon
                },null,this.domNode,true);





/*
                '<div class="widget-controls">'+
                '<a data-widgster="load" title="Reload" href="#"><i class="glyphicon glyphicon-refresh"></i></a>'+
                '<a data-widgster="expand" title="Expand" href="#"><i class="fa fa-code"></i></a>'+
                '<a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-minus"></i></a>'+
                '<a data-widgster="fullscreen" title="Full Screen" href="#"><i class="glyphicon glyphicon-resize-full"></i></a>'+
                '<a data-widgster="restore" title="Restore" href="#"><i class="glyphicon glyphicon-resize-small"></i></a>'+
                '<a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>'+
                '</div>'+
                    */

                var closed = true;

                var ariaOpen = closed ? 'true' : 'false';
                var containerClass = closed ? 'collapse' : 'collapse in';
                var titleClass = closed ? 'collapsed' : '';

                var toggleNodeStr =
                    '<a data-dojo-attach-point="toggleNode" href="#collapseOneTwo" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                        '${!title}'+
                    '</a>';
                    //'<a data-widgster="expand" title="Expand" href="#"><i class="fa fa-arrow-down"></i></a>';

                var panelTemplate = '<div class="panel widget">'+

                    '<div class="panel-heading" data-dojo-attach-point="titleBar">'+

                        toggleNodeStr +

                    '</div>'+

                    '<div data-dojo-attach-point="containerRoot" class="panel-collapse ${!containerClass}" id="collapseOneTwo" aria-expanded="${!ariaOpen}" style="">'+
                        '<div class="panel-body" data-dojo-attach-point="containerNode">'+
                            'asdfasdf'+
                        '</div>'+
                    '</div>'+

                '</div>'


                var pane  = utils.templatify(null,panelTemplate, this.domNode , {
                    iconClass:'fa-play',
                    title:title,
                    ariaOpen : ariaOpen,
                    containerClass:containerClass,
                    titleClass:titleClass,
                    show:function(){

                        var container = $(this.containerRoot),
                            toggleNode= $(this.toggleNode);

                        toggleNode.removeClass('collapsed');
                        toggleNode.attr('aria-expanded',true);


                        container.removeClass('collapse');
                        container.addClass('collapse in');
                        container.attr('aria-expanded',true);

                    },
                    hide:function(){

                        var container = $(this.containerRoot),
                            toggleNode= $(this.toggleNode);

                        toggleNode.addClass('collapsed');
                        toggleNode.attr('aria-expanded',false);


                        container.removeClass('collapse in');
                        container.addClass('collapse');
                        container.attr('aria-expanded',false);
                    }
                });


                return pane;

                /*
                var panel = $(panelTemplate);

                $(this.domNode).append(panel);*/


            }
        });

        return tabClass;

    }

    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {


        var parent = TestUtils.createTab('Acc-Test',null,module.id);

        ////////////////////////////////////////////////////////
        //
        //  Test with creating a tab container basing on the new widget class
        //

        var widgetBaseClass = createWidgetNormalClass();
        var tabContainerClass = _createTabContainerClass(widgetBaseClass);
        var tabContainer = utils.addWidget(tabContainerClass,{},null,parent,true);
        var tabClass = createTabClass(widgetBaseClass);

        var pane = tabContainer.createTab('bla bla','fa-code',true,tabClass);

        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane);


        //doTests(tabContainer,widgetBaseClass);





        /*

        var tabContainer = _createTabContainer();

        var widget = utils.addWidget(tabContainer,{},null,parent,true);


        doTests(widget,tabContainer);*/

        //doWidgetTests(parent);

        return declare('a',null,{});

    }

    return Grid;

});