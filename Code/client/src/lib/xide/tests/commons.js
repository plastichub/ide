define([
    'dojo/_base/declare',
    'xide/utils',
    'xide/types',
    'xide/types/Types',
    'xide/utils/StringUtils',
    'xide/utils/HTMLUtils',
    'xide/utils/CIUtils',
    'xide/utils/StoreUtils',
    'xide/utils/WidgetUtils',
    'xide/utils/ObjectUtils',
    'xide/factory/Objects',
    'xide/factory/Events',
    'xide/factory/Widgets',
    'xide/factory/Views',
    'xfile/types',
    'xfile/config',
    'xfile/factory/Store'
],function(declare){

    Array.prototype.remove= function(){
        var what, a= arguments, L= a.length, ax;
        while(L && this.length){
            what= a[--L];
            if(this.indexOf==null){
                return;
            }
            while((ax= this.indexOf(what))!= -1){
                this.splice(ax, 1);
            }
        }
        return this;
    };
    return  declare("xide.tests.commons", null,{});
});