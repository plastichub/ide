/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "./TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "xide/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/cookie",
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    'xide/views/ConsoleView',
    'xide/views/ConsoleKeyboardDelegate',
    'xide/views/History',
    'xace/views/ACEEditor',
    'xace/views/Editor'

], function (declare, dcl, types,
             utils, Grid, TemplatedWidgetBase, EventedMixin,
             TestUtils, FTestUtils, _Widget, registry, module,
             cache, cookie, domConstruct, lang, string, _XWidget, ConsoleView,
             ConsoleKeyboardDelegate, History, ACEEditor, Editor) {
    console.clear();
    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    /***
     * Default editor persistence for peferences in cookies!
     **/
    Editor = declare('xace.views.Editor', [Editor, createPreferenceMixin()], {

        defaultPrefenceTheme: 'idle_fingers',
        defaultPrefenceFontSize: 14,
        saveValueInPreferences: true,
        getDefaultPreferences: function () {
            var self = this;
            return utils.mixin(
                {
                    theme: this.defaultPrefenceTheme,
                    fontSize: this.defaultPrefenceFontSize
                },
                this.saveValueInPreferences ? {value: this.get('value')} : null);
        },
        onAfterAction: function (action) {
            //console.log('onAfterAction ' + this.get('theme') + ' this ' + this.getEditor().getFontSize());
            this.savePreferences({
                theme: this.get('theme').replace('ace/theme/', ''),
                fontSize: this.getEditor().getFontSize()
            });
            return this.inherited(arguments);
        },
        /**
         * Override id for pref store:
         * know factors:
         *
         * - IDE theme
         * - per bean description and context
         * - by container class string
         * - app / plugins | product / package or whatever this got into
         * -
         **/
        toPreferenceId: function (prefix) {

            //console.log('p ' + prefix);
            prefix = prefix || ($('body').hasClass('xTheme-transparent') ? 'xTheme-transparent' : 'xTheme-white' );

            return (prefix || this.cookiePrefix || '') + '_xace';
        },
        getDefaultOptions: function () {

            //take our defaults, then mix with prefs from store,
            var _super = this.inherited(arguments),

                _prefs = this.loadPreferences(null);

            (_prefs && utils.mixin(_super, _prefs) ||
                //else store defaults
            this.savePreferences(this.getDefaultPreferences()));


            //this.savePreferences({});

            return _super;
        }
    });

    function createPreferenceMixin() {

        /**
         * @class module:xide/mixins/Persistence
         *
         **/
        var Implementation = {
            /***
             * soft override for getPreferencePersistence to allow using
             * another storage
             */
            persistenceFunction: null,
            /**
             * delete preferences in store
             */
            clearPreferences: function () {
                this.savePreferences(null, null, true);
            },
            /**
             * must be implemented
             */
            getDefaultPreferences: function () {
                return {};
            },
            /**
             * deserialize data with provider
             */
            deserializePreference: function (val) {
                return utils.getJson(val);
            },
            /**
             * serialize data with provider
             */
            serializePreference: function (val, safe) {
                try {
                    return safe === true ? utils.stringify(val) : JSON.stringify(val);
                } catch (e) {
                    logError(e, 'error serializing preference object');
                }
                return false;
            },
            /**
             * get a unique id for the store.
             *
             * @param prefix {string}
             **/
            toPreferenceId: function (prefix) {
                return (prefix || this.cookiePrefix || '') + '_ace';
            },
            /**
             * Return a function for persistence.apply(id,val)
             *
             * @param id {string} a unique id for the store.
             * @param val {string|null} the data. null when used for loading
             * @param provider {function|null} a default
             **/
            getPreferencePersistence: function (id, val, provider) {
                return this.persistenceFunction || provider || cookie;
            },
            /**
             * Load prefs by id (defaults to ::toPreferenceId)
             *
             * @param defaults {object|null} default data.
             * @param id {string|null} a unique id for the store.
             **/
            loadPreferences: function (defaults, id) {
                var _id = id || this.toPreferenceId(),
                    persistence = this.getPreferencePersistence(),
                    settings = null;
                try {
                    settings = this.deserializePreference(persistence.apply(this, [_id]));
                } catch (e) {
                    logError(e, 'error loading preferences, loading defaults');
                    return this.getDefaultPreferences() || defaults;
                }
                return settings || defaults;
            },
            /**
             * Save prefs per id (defaults to ::toPreferenceId)
             *
             * @param prefs {object|} the data.
             * @param object {object|null} provide another context for the persistence function
             * @param clear {Boolean} dont use defaults = means it clears the store
             **/
            savePreferences: function (_prefs, object, clear, id) {
                var _id = id || this.toPreferenceId(),
                    settings = _prefs || clear !== true ? _prefs : this.getDefaultPreferences(),
                    persistence = this.getPreferencePersistence();

                return persistence.apply(object || this, [_id, this.serializePreference(settings)]);
            }
        }


        var Module = declare('xide.mixins.PreferenceMixin', null, Implementation);
        Module.dcl = dcl(null, Implementation);

        return Module;
    }

    function createConsoleAceWidgetClassBak() {

        var PreferencePersistence = createPreferenceMixin();

        return declare("xide.views.Console", [TemplatedWidgetBase, ConsoleKeyboardDelegate, PreferencePersistence], {
            delegate: null,
            value: null,
            editNode: null,
            labelTextNode: null,
            labelNode: null,
            type: null,
            linkToggle: null,
            edit: null,
            consoleParent: null,
            isExpanded: false,
            theme: 'View/Themes/idle_fingers',
            consoleEditor: null,
            jsContext: null,
            destroy: function () {
                this.inherited(arguments);
                //this.consoleEditor && this.consoleEditor.destroy();
                this.aceEditor && this.aceEditor.destroy();

            },
            templateString: '<div class="consoleWidget form-group">' +

            '<div class="form-group" style="margin: 0">' +

            '<div class="input-group border-top-dark">' +
            '<div data-dojo-attach-point="consoleParent" class="form-control input-transparent" style="height: 2em;padding: 0;margin: 0;overflow-y: auto"></div>' +
            '<div class="input-group-btn btn-toolbar">' +
            '<button data-dojo-attach-point="clearButton" type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>' +
            '<button data-dojo-attach-point="expandButton" type="button" class="btn btn-warning"><i class="fa fa-expand"></i></button>' +
            '<button type="button" class="btn btn-success"><i class="fa fa-refresh"></i></button>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '</div>',
            isLinked: function () {
                if (this.linkToggle) {
                    return this.linkToggle.get('checked');
                }
                return false;
            },
            getEditor: function () {
                return this.consoleEditor;

            },
            resize: function () {
                this.inherited(arguments);
                if (this.isExpanded) {
                    var total = $(this.domNode.parentNode).height();
                    $(this.consoleParent).css({
                        height: total / 2 + 'px'
                    });
                }
                this.aceEditor && this.aceEditor.resize();
            },
            expandEditor: function () {

                var thiz = this,
                    editor = thiz.getEditor(),
                    aceEditor = this.aceEditor;

                if (thiz.isExpanded) {

                    $(thiz.consoleParent).css({
                        height: '2em'
                    });

                    thiz.isExpanded = false;
                    editor.renderer.$maxLines = 1;
                    editor.renderer.setShowGutter(false);
                    editor.renderer.setHighlightGutterLine(false);
                    aceEditor.showToolbar(false);
                } else {
                    $(thiz.consoleParent).css({
                        height: $(this.domNode.parentNode).height() / 2 + 'px'
                    });
                    editor.renderer.$maxLines = Infinity;
                    thiz.isExpanded = true;
                    editor.renderer.setShowGutter(true);
                    editor.renderer.setHighlightGutterLine(true);
                    utils.resizeTo(editor.renderer.container, thiz.consoleParent, true, true);
                    editor.resize();
                    aceEditor.resize();
                    aceEditor.showToolbar(true);
                    var toolbar = aceEditor.getToolbar();
                    toolbar && $(toolbar.domNode).css({
                        top: '80%',
                        position: "absolute"
                    });
                }

            },
            createEditor: function () {
                return createEditor(this.consoleParent, this.value, this, {
                    options: this.options
                });
            },
            createWidgets: function () {


                var aceEditor = this.createEditor();

                this.aceEditor = aceEditor;
                aceEditor.showToolbar(false);

                var editor = aceEditor.getEditor(),
                    self = this;

                this.aceEditorEditor = aceEditor;
                this.consoleEditor = editor;

                editor.renderer.$maxLines = 1;
                editor.renderer.setShowGutter(false);
                editor.renderer.setHighlightGutterLine(false);
                editor.$mouseHandler.$focusWaitTimout = 0;
                editor.setOptions({
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true
                });

                aceEditor.setMode(this.delegate.type);
                aceEditor.set('value', this.value);
                aceEditor.runAction(this.theme);
                aceEditor.set('value', this.value);


                $(this.expandButton).click(function (e) {
                    return self.expandEditor();
                });

                $(this.clearButton).on('click', function () {
                    if (self.delegate && self.delegate.onButton) {
                        self.delegate.onButton();
                    }
                });

                this.expandEditor();

                editor.commands.bindKeys({
                    "Shift-Return|Ctrl-Return|Alt-Return": function (cmdLine) {
                        if (self.isExpanded) {
                            editor.focus();
                            self.onEnter(editor.getValue());
                        } else {
                            editor.insert("\n");
                        }

                    },
                    "Esc|Shift-Esc": function (cmdLine) {
                        editor.focus();
                    },
                    "Return": function (cmdLine) {
                        var command = editor.getValue().split(/\s+/);
                        if (self.isExpanded) {
                            editor.insert("\n");
                        } else {
                            editor.focus();
                            self.onEnter(editor.getValue());
                        }
                    }
                });
                editor.commands.removeCommands(["find", "gotoline", "findall", "replace", "replaceall"]);
            },
            getValue: function () {
                return this.consoleEditor.getValue();
            },
            startup: function () {

                if (this._started) {
                    return;
                }
                this.history = new History();
                this.inherited(arguments);
                this.createWidgets();
            },
            onEnter: function (val) {
                this.delegate.onEnter(val, this.isExpanded == false);
                this.history.push(val);
            }
        });
    }
    function createConsoleAceWidgetClass() {

        var PreferencePersistence = createPreferenceMixin();
        return dcl([_XWidget,PreferencePersistence.dcl], {
            declaredClass:"xide.views.Console",
            delegate: null,
            value: null,
            editNode: null,
            labelTextNode: null,
            labelNode: null,
            type: null,
            linkToggle: null,
            edit: null,
            consoleParent: null,
            isExpanded: false,
            theme: 'View/Themes/idle_fingers',
            consoleEditor: null,
            jsContext: null,
            templateString: '<div class="consoleWidget">' +
            '<div class="" style="margin: 0">' +

            '<div class="input-group border-top-dark">' +
            '<div attachTo="consoleParent" class="form-control input-transparent" style="height: 2em;padding: 0;margin: 0;overflow-y: auto"></div>' +
                '<div class="input-group-btn btn-toolbar">' +
                    '<button attachTo="clearButton" type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>' +
                    '<button attachTo="expandButton" type="button" class="btn btn-warning"><i class="fa fa-expand"></i></button>' +
                    '<button type="button" class="btn btn-success"><i class="fa fa-refresh"></i></button>' +
                '</div>' +
            '</div>' +
            '</div>' +

            '</div>',
            isLinked: function () {
                if (this.linkToggle) {
                    return this.linkToggle.get('checked');
                }
                return false;
            },
            getEditor: function () {
                return this.consoleEditor;

            },
            resize: function () {
                this.inherited(arguments);
                if (this.isExpanded) {
                    var total = $(this.domNode.parentNode).height();
                    $(this.consoleParent).css({
                        height: total / 2 + 'px'
                    });
                }
                this.aceEditor && this.aceEditor.resize();
            },
            expandEditor: function () {


                var thiz = this,
                    editor = thiz.getEditor(),
                    aceEditor = this.aceEditor;

                if (thiz.isExpanded) {

                    $(thiz.consoleParent).css({
                        height: '2em'
                    });

                    thiz.isExpanded = false;
                    editor.renderer.$maxLines = 1;
                    editor.renderer.setShowGutter(false);
                    editor.renderer.setHighlightGutterLine(false);
                    aceEditor.showToolbar(false);
                } else {
                    $(thiz.consoleParent).css({
                        height: $(this.domNode.parentNode).height() / 2 + 'px'
                    });
                    editor.renderer.$maxLines = Infinity;
                    thiz.isExpanded = true;
                    editor.renderer.setShowGutter(true);
                    editor.renderer.setHighlightGutterLine(true);
                    utils.resizeTo(editor.renderer.container, thiz.consoleParent, true, true);
                    editor.resize();
                    aceEditor.resize();
                    aceEditor.showToolbar(true);
                    var toolbar = aceEditor.getToolbar();
                    toolbar && $(toolbar.domNode).css({
                        top: '80%',
                        position: "absolute"
                    });
                }

                if(this.delegate && this.delegate.onConsoleExpanded){
                    this.delegate.onConsoleExpanded();
                }
            },
            createEditor: function () {
                return createEditor(this.consoleParent, this.value, this, {
                    options: this.options
                });            },
            createWidgets: function () {

                var aceEditor = this.createEditor();
                this.add(aceEditor,null,false);
                this.aceEditor = aceEditor;
                aceEditor.showToolbar(false);

                var editor = aceEditor.getEditor(),
                    self = this;

                this.aceEditorEditor = aceEditor;
                this.consoleEditor = editor;

                editor.renderer.$maxLines = 1;
                editor.renderer.setShowGutter(false);
                editor.renderer.setHighlightGutterLine(false);
                editor.$mouseHandler.$focusWaitTimout = 0;
                editor.setOptions({
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true
                });

                aceEditor.setMode(this.delegate.type);
                aceEditor.set('value', this.value);
                aceEditor.runAction(this.theme);
                aceEditor.set('value', this.value);


                $(this.expandButton).click(function (e) {
                    return self.expandEditor();
                });

                $(this.clearButton).on('click', function () {
                    if (self.delegate && self.delegate.onButton) {
                        self.delegate.onButton();
                    }
                });

                this.expandEditor();

                editor.commands.bindKeys({
                    "Shift-Return|Ctrl-Return|Alt-Return": function (cmdLine) {
                        if (self.isExpanded) {
                            editor.focus();
                            self.onEnter(editor.getValue());
                        } else {
                            editor.insert("\n");
                        }

                    },
                    "Esc|Shift-Esc": function (cmdLine) {
                        editor.focus();
                    },
                    "Return": function (cmdLine) {
                        var command = editor.getValue().split(/\s+/);
                        if (self.isExpanded) {
                            editor.insert("\n");
                        } else {
                            editor.focus();
                            self.onEnter(editor.getValue());
                        }
                    }
                });
                editor.commands.removeCommands(["find", "gotoline", "findall", "replace", "replaceall"]);
            },
            getValue: function () {
                return this.consoleEditor.getValue();
            },
            startup: function () {
                if (this._started) {
                    return;
                }
                this.history = new History();
                this.inherited(arguments);
                this.createWidgets();
            },
            onEnter: function (val) {
                this.delegate.onEnter(val, this.isExpanded == false);
                this.history.push(val);
            }
        });
    }

    function createEditor(root, value, owner, mixin) {

        var item = {
            filePath: '',
            fileName: ''
        };
        var title = "No Title";

        var args = {
            _permissions: [],
            item: item,
            value: value,
            style: 'padding:0px;',
            iconClass: 'fa-code',
            options: utils.mixin(mixin, {
                filePath: item.path,
                fileName: item.name
            }),
            ctx: ctx,
            /***
             * Provide a text editor store delegate
             */
            storeDelegate: {},
            title: title
        };


        utils.mixin(args, mixin);

        editor = utils.addWidget(Editor, args, owner, root, true, null, null, false);

        editor.resize();

        return editor;
    }

    function createShellViewDelegate(){

        return dcl(null, {

            onServerResponse: function (theConsole, data, addTimes) {


                if (theConsole && data && theConsole.owner && theConsole.owner.onServerResponse) {
                    theConsole.owner.onServerResponse(data, addTimes);
                }
            },
            runBash: function (theConsole, value, cwd) {

                var thiz = this;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['sh', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole, response, false);
                });
            },

            runPHP: function (theConsole, value, cwd) {
                var thiz = this;
                var server = ctx.fileManager;
                var _value = server.serviceObject.base64_encode(value);
                server.runDeferred('XShell', 'run', ['php', _value, cwd]).then(function (response) {
                    thiz.onServerResponse(theConsole, response,false);
                });

            },
            runJavascript: function (theConsole, value, context, args) {

                var _function = new Function("{" + value + "; }");
                var response = _function.call(context, args);
                if (response != null) {
                    console.error('response : ' + response);
                    this.onServerResponse(theConsole, response);
                    return response;
                }
                return value;
            },
            onConsoleCommand: function (data, value) {

                var thiz = this,
                    theConsole = data.console;


                if (theConsole.type === 'sh') {

                    value = value.replace(/["'`]/g, "");

                    var dstPath = null
                    if (theConsole.isLinked()) {
                        dstPath = this.getCurrentPath();
                    }
                    return this.runBash(theConsole, value, dstPath);
                }

                if (theConsole.type === 'php') {

                    value = value.replace(/["'`]/g, "");

                    var dstPath = null
                    if (theConsole.isLinked()) {
                        dstPath = this.getCurrentPath();
                    }
                    return this.runPHP(theConsole, value, dstPath);
                }

                if (theConsole.type === 'javascript') {
                    return this.runJavascript(theConsole, value);
                }
            },
            onConsoleEnter: function (data, input) {
                return this.onConsoleCommand(data, input);
            }
        });
    }

    function createShellViewClass(){

        var Module = dcl([_XWidget],{
            templateString:'<div attachTo="containerNode" class="widget" style="height: 100%;width: 100%;">'+
                            '<div attachTo="logView" style="overflow: auto"></div></div>',
            value: "return 2;",
            resizeToParent:true,
            serverClass: 'XShell',
            consoleClass: createConsoleAceWidgetClass(),
            server: null,
            showProgress: false,
            jsContext: null,
            onConsoleExpanded:function(){
                this._resizeLogView();
            },
            _resizeLogView:function(){
                if(this.console) {
                    var total = this.$containerNode.height();
                    var consoleH = $(this.console.domNode).height();
                    $(this.logView).height(total - consoleH + 'px');
                }
            },
            _scrollToEnd: function () {

                var thiz = this;
                var container = this.getLoggingContainer();
                container.lastChild.scrollIntoViewIfNeeded();
                this._resizeLogView();
                return;


            },
            onServerResponse: function (data,addTimes) {

                var container = this.getLoggingContainer();
                container.children.length > 100 && dojo.empty(container);
                this.log(data,addTimes);
                this._scrollToEnd();

            },
            getLoggingContainer: function () {
                return this.logView;
            },
            onEnter: function (value,print) {

                console.log('on enter');
                if (this.showProgress) {
                    this.progressItem = this.createLogItem(value, this.getLoggingContainer());
                }


                var _resolved = '';

                if (this.delegate.onConsoleEnter) {
                    _resolved = this.delegate.onConsoleEnter({
                        view: this,
                        console: this.console
                    }, value,print);
                }


                if (this.showLastInput) {
                    var dst = this.getLoggingContainer();

                    print !==false && dst.appendChild(dojo.create("div", {
                        innerHTML: '# ' + (_resolved || value),
                        className: 'widget'
                    }));

                }
            },
            getServer: function () {
                return this.server || ctx.fileManager;
            },
            _toString: function (str, addTimes) {
                if (addTimes !== false) {
                    return this.addTime(str);
                } else {
                    return str;
                }
            },
            addTime: function (str) {
                return moment().format("HH:mm:SSS") + ' ::   ' + str + '';
            },
            log: function (msg, addTimes) {


                console.log('log');
                utils.destroy(this.progressItem);

                var out = '';
                if (_.isString(msg)) {
                    out += msg.replace(/\n/g, '<br/>');
                } else if (_.isObject(msg) || lang.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }
                ;

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var thiz = this;

                for (var i = 0; i < items.length; i++) {
                    var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');
                    last = dst.appendChild(dojo.create("div", {
                        className: _class,
                        innerHTML: this._toString(items[i], addTimes)

                    }));
                    this.lastIndex++;
                }

                if (last) {
                    last.scrollIntoViewIfNeeded();
                }

                setTimeout(function () {
                    thiz._scrollToEnd();
                }, 10);


            },
            startup:function(){
                this.createWidgets();
            },
            createWidgets: function () {

                this.console = utils.addWidget(this.consoleClass,{
                    style:'width:inherit',
                    delegate: this,
                    type: this.type,
                    owner: this,
                    className: 'consoleWidget',
                    value:this.value
                },this,this.containerNode,true);

                this.console.startup();
                this.add(this.console,null,false);
            }
        });

        return Module;

    }

    function createShellViewBak(tab, type) {

        var consoleClass = createShellViewClass();

        var view = utils.addWidget(ConsoleView, {
            type: type,
            value: "ls",
            serverClass: 'XShell',
            consoleClass: createConsoleAceWidgetClass(),
            server: null,
            showProgress: false,
            jsContext: null,
            getServer: function () {
                return this.server || ctx.fileManager;
            },
            delegate: {
                onServerResponse: function (theConsole, data, addTimes) {

                    if (theConsole && data && theConsole.owner && theConsole.owner.onServerResponse) {
                        theConsole.owner.onServerResponse(data, addTimes);
                    }
                },
                runBash: function (theConsole, value, cwd) {

                    var thiz = this;
                    var server = ctx.fileManager;
                    var _value = server.serviceObject.base64_encode(value);
                    server.runDeferred('XShell', 'run', ['sh', _value, cwd]).then(function (response) {
                        thiz.onServerResponse(theConsole, response, false);
                    });
                },
                runPHP: function (theConsole, value, cwd) {
                    var thiz = this;

                    var _cb = function (response) {
                        thiz.onServerResponse(theConsole, response, false);
                    };

                    console.log('send : ' + value);
                    console.log('cwd ' + cwd);

                    var server = ctx.fileManager;
                    var _value = server.serviceObject.base64_encode(value);
                    server.runDeferred('XShell', 'run', ['php', _value, cwd]).then(function (response) {
                        console.error('respo');
                    });

                },
                runJavascript: function (theConsole, value, context, args) {

                    var _function = new Function("{" + value + "; }");

                    var response = _function.call(context, args);
                    if (response != null) {
                        console.error('response : ' + response);
                        this.onServerResponse(theConsole, response);
                        return response;
                    }


                    return value;

                },
                onConsoleCommand: function (data, value) {

                    var thiz = this,
                        theConsole = data.console;


                    if (theConsole.type === 'sh') {

                        value = value.replace(/["'`]/g, "");

                        var dstPath = null
                        if (theConsole.isLinked()) {
                            dstPath = this.getCurrentPath();
                        }
                        return this.runBash(theConsole, value, dstPath);
                    }

                    if (theConsole.type === 'php') {

                        value = value.replace(/["'`]/g, "");

                        var dstPath = null
                        if (theConsole.isLinked()) {
                            dstPath = this.getCurrentPath();
                        }
                        return this.runPHP(theConsole, value, dstPath);
                    }

                    if (theConsole.type === 'javascript') {
                        return this.runJavascript(theConsole, value);
                    }
                },
                onConsoleEnter: function (data, input) {
                    return this.onConsoleCommand(data, input);
                }
            },

            _toString: function (str, addTimes) {

                if (addTimes !== false) {
                    return this.addTime(str);
                } else {
                    return str;
                }
            },
            addTime: function (str) {
                return moment().format("HH:mm:SSS") + ' ::   ' + str + '';
            },
            log: function (msg, addTimes) {


                utils.destroy(this.progressItem);

                var out = '';
                if (_.isString(msg)) {
                    out += msg.replace(/\n/g, '<br/>');
                } else if (_.isObject(msg) || lang.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }
                ;

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var thiz = this;

                for (var i = 0; i < items.length; i++) {
                    var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');
                    last = dst.appendChild(dojo.create("div", {
                        className: _class,
                        innerHTML: this._toString(items[i], addTimes)

                    }));
                    this.lastIndex++;
                }

                if (last) {
                    last.scrollIntoViewIfNeeded();
                }

                setTimeout(function () {
                    thiz._scrollToEnd();
                }, 10);


            }
        }, this, tab, true, null, null, null);

        return view;


    }

    function createShellView(tab, type) {

        var consoleViewClass = createShellViewClass();

        var handlerClass = createShellViewDelegate();

        var delegate = new handlerClass();

        return utils.addWidget(consoleViewClass, {
            type: type,
            value: "return 2;",
            serverClass: 'XShell',
            consoleClass: createConsoleAceWidgetClass(),
            server: null,
            showProgress: false,
            resizeToParent:true,
            jsContext: null,
            getServer: function () {
                return this.server || ctx.fileManager;
            },
            _toString: function (str, addTimes) {
                if (addTimes !== false) {
                    return this.addTime(str);
                } else {
                    return str;
                }
            },
            addTime: function (str) {
                return moment().format("HH:mm:SSS") + ' ::   ' + str + '';
            },
            log: function (msg, addTimes) {


                utils.destroy(this.progressItem);

                var out = '';
                if (_.isString(msg)) {
                    out += msg.replace(/\n/g, '<br/>');
                } else if (_.isObject(msg) || lang.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }
                ;

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var thiz = this;

                for (var i = 0; i < items.length; i++) {
                    var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');
                    last = dst.appendChild(dojo.create("div", {
                        className: _class,
                        innerHTML: this._toString(items[i], addTimes)

                    }));
                    this.lastIndex++;
                }

                if (last) {
                    last.scrollIntoViewIfNeeded();
                }

                setTimeout(function () {
                    thiz._scrollToEnd();
                }, 10);


            }


        },delegate,tab,true);
    }
    function addActions() {

        var result = [];
        var thiz = this;

        function createShell(action) {

            var parent = TestUtils.createTab(null, null, module.id);
            return createShellView(parent, action.shellType);
        }


        var _action = {
            label: "Javacript - Shell",
            command: "Window/JS-Shell",
            icon: 'fa-code',
            tab: "Home",
            group: 'View',
            handler: createShell,
            mixin: {
                addPermission: true,
                shellType: 'javascript'
            },
            owner: this
        }


        result.push(ctx.createAction(_action));

        _action = {
            label: "Bash - Shell",
            command: "Window/Bash-Shell",
            icon: 'fa-code',
            tab: "Home",
            group: 'View',
            handler: createShell,
            mixin: {
                addPermission: true,
                shellType: 'sh'
            },
            owner: this
        }
        result.push(ctx.createAction(_action));


        _action = {
            label: "PHP - Shell",
            command: "Window/PHP-Shell",
            icon: 'fa-code',
            tab: "Home",
            group: 'View',
            handler: createShell,
            mixin: {
                addPermission: true,
                shellType: 'php'
            },
            owner: this
        }
        result.push(ctx.createAction(_action));


        ctx.addActions(result);
        return result;

    }

    function doTests(tab, type) {

        addActions();
        return createShellView(tab, type);
    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {
        var tab = TestUtils.createTab('Test', null, 'test');
        doTests(tab, 'php');
        return declare('a', null, {});

    }

    return Grid;

});