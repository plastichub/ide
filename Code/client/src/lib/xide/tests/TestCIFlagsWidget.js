/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',
    'xide/views/CIActionDialog',
    'xide/widgets/WidgetBase',
    'xide/widgets/_Widget',
    'xide/widgets/_CacheMixin',
    'xide/widgets/ActionToolbar',
    'xide/action/ActionModel',
    'xaction/ActionProvider',
    "dstore/Memory",
    'xide/widgets/ExpressionsGridView',
    'xide/tests/TestUtils',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'module',


    'xide/json/JSONEditor',
    "xide/widgets/FlagsWidget"

], function (dcl,declare, types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,CIActionDialog,
             WidgetBase,_Widget,_CacheMixin,ActionToolbar,ActionModel,ActionProvider,
             Memory,ExpressionsGridView,
             TestUtils,ACEEditor,Editor,module,
             JSONEditor,
             FlagsWidget

    ) {

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    function createDriverCIS(driver,actionTarget){

        var CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID",
                    "name": "CF_DRIVER_ID",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 4,
                    "uid": "-1",
                    "value": {
                        test:2
                    },
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General21',
                    "id": "CF_DRIVER_ID",
                    "name": "CF_DRIVER_ID",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 13,
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true
                }
            ]
        };
        _.each(CIS.inputs,function(ci){

        });
        return CIS;
    }
    function onWidgetCreated(basicTab,condTab,varTab,logTab){}
    function doTests(){}



    function testWidget(){

        var toolbar = ctx.mainView.getToolbar();

        var docker = ctx.mainView.getDocker();

        var parent  = TestUtils.createTab(null,null,module.id);

        var cisRenderer = dcl(CIGroupedSettingsView,{
            typeMap:null,
            getTypeMap:function(){

                if (this.typeMap) {
                    return this.typeMap;
                }

                var self = this;
                var typeMap = {}

                typeMap['JSON'] = createJSON_EditorWidgetClass();


                this.typeMap = typeMap;


                return typeMap;
            }
        });

        //types.registerWidgetMapping('Arguments', createArgumentsWidget());

        //types.registerWidgetMapping('Script', createScriptWidget());

        //types.registerWidgetMapping('Expression', createExpressionWidget());

        //types.registerWidgetMapping(types.ECIType.EXPRESSION_EDITOR, createExpressionWidget());


        var ciView = utils.addWidget(cisRenderer,{
            cis:createDriverCIS().inputs
        },null,parent,true);


        docker.resize();
    }

    function testWidget2(){
        var thiz = this;















        var parent  = TestUtils.createTab(null,null,module.id);
        var flags = [
            {
                value: 2,
                label: 'Runs Server Side'
            },
            {
                value: 4,
                label: 'Show Debug Messages'
            },
            {
                value: 8,
                label: 'Allow Multiple Device Connections'
            }
        ];



        var flagsWidget = utils.addWidget(FlagsWidget, {
            value: 2,
            data: flags,
            lineBreak: false,
            title: '',
            single: false,
            userData:{
                value:0
            }
        }, thiz, parent, true, 'ui-widget-content');



    }

    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return blockManager.toScope(data);
    }

    if (ctx) {

        blockManager = ctx.getBlockManager();


        //driverManager = ctx.getDriverManager();
        //marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        //openDriverSettings(null);
        //openCISDialog();

        testWidget2();

        return declare('a',null,{});
    }


    return Editor;

});