/** @module xide/tests/TestMainMenu **/

define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xfile/tests/TestUtils',
    'module',
    'xide/widgets/ActionToolbar',
    'xide/action/ActionContext',
    'dijit/Toolbar',
    'dijit/ToolbarSeparator',
    'xide/widgets/ActionToolbarMixin',
    "xide/mixins/ActionMixin",
    "xide/mixins/EventedMixin",
    "xide/widgets/_MenuMixin",
    "xide/widgets/TemplatedWidgetBase",
    'xide/widgets/ActionToolbarButton',
    'dijit/Menu',
    'dijit/MenuItem',
    'xide/data/Reference',
    'dijit/form/DropDownButton',
    'dojo/Stateful',
    'dijit/PopupMenuItem',
    'dijit/MenuBar',
    'dojo/dom-construct'

], function (declare,types,utils,

             XFileTestUtils,module,ActionToolbar,ActionContext,

             Toolbar,ToolbarSeparator,ActionToolbarMixin, ActionMixin,EventedMixin,_MenuMixin,TemplatedWidgetBase,

             ActionToolbarButton,

             Menu, MenuItem, Reference,

             DropDownButton,

             Stateful,PopupMenuItem,MenuBar,

             domConstruct

) {




    
    /***
     *
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;


        
    

    function createToolbarClass(){

        var toolbar = declare("xide/widgets/ActionToolbar", [Toolbar,ActionContext, ActionMixin,EventedMixin,_MenuMixin], {

            /**
             *
             * @param action
             * @param where
             * @param widgetClass
             * @param showLabel
             * @returns {*}
             */
            renderAction: function (action, where, widgetClass, showLabel) {




                if(action.command.includes('File/New')) {
                    //console.log('render action ' + action.command);
                }


                //console.log('render action ' + action.command);



                /**
                 * Collect variables
                 */

                var thiz = this,


                    _items = action.getChildren ? action.getChildren() : action.items,

                    _hasItems = _items && _items.length,
                    isContainer = _hasItems,
                    _expand = thiz.getVisibilityField(action, 'expand') == true,
                    _forceSub = action.forceSubs,
                    widget = thiz.getVisibilityField(action, 'widget'),
                    parentAction = action.getParent ? action.getParent() : null,
                    parentWidget = parentAction ? thiz.getVisibilityField(parentAction, 'widget') : null,
                    actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};



                /**
                 * Cleanup variables, sanitize
                 */

                //case when visibility is not an object, upgrade visibility to an object
                if (!_.isObject(actionVisibility) && action.setVisibility) {
                    actionVisibility = {};
                    action.setVisibility(this.visibility, actionVisibility);
                }

                //further variables
                var label = '',// actionVisibility.label || action.label,
                    widgetArgs = {
                        iconClass: action.icon,
                        label: label,
                        item: action
                    };

                if (actionVisibility.widgetArgs) {
                    utils.mixin(widgetArgs, actionVisibility.widgetArgs);
                }




                /**
                 * Determine widget class to use, resolve
                 */
                widgetClass =

                        //override
                    actionVisibility.widgetClass ||
                        //argument
                    widgetClass ||
                        //visibility's default
                    thiz.widgetClass;


                // case one: isContainer = true
                if (!widget) {

                    if (isContainer) {

                        if (_expand) {
                            return null;
                        }
                        if (!parentWidget) {

                            var menu = utils.addWidget(DropDownButton, widgetArgs, this, where, true, null, [Reference]);

                            var pSubMenu = utils.addWidget(Menu, {
                                item: action
                            }, this, null, true);

                            menu.dropDown = pSubMenu;
                            actionVisibility.widget = pSubMenu;

                            thiz._publishActionWidget(menu,action,where);

                            return menu;

                        } else {

                            //parent widget there

                            var pSubMenu = new Menu({parentMenu: parentWidget});


                            var popup = new dijit.PopupMenuItem({
                                label: label,
                                popup: pSubMenu,
                                iconClass: action.icon
                            });

                            parentWidget.addChild(popup);

                            actionVisibility.widget = pSubMenu;
                            return pSubMenu;

                        }
                    }
                    if (parentWidget) {

                        where = parentWidget;
                        widgetClass = widgetClass || MenuItem;
                    }
                } else {
                    console.log('widget already rendered!');
                }

                if (!actionVisibility.widget && !action.domNode) {

                    if (!action.label) {
                        //huh, really? @TODO: get rid of label needs in action:render
                    } else {
                        actionVisibility.widget = thiz._addMenuItem(action, where, widgetClass, showLabel);
                        //actionVisibility.widget.visibility = thiz.visibility;
                    }
                } else {
                    console.log('have already rendered action', action);
                }

                return actionVisibility.widget;

            },
            /**
             * Renders a set of actions into is appropriate widgets. This utilizes this._renderAction and
             * puts the action's widget into its visibility store for global access.
             *
             * @param actions {xide/action/Action[]}
             * @param where {dijit/_Widget}
             * @param widgetClass {Module}
             * @private
             */
            _renderActions: function (actions, where, widgetClass, showLabel, separatorClass, force) {




                //track the last action group for adding a separator
                var _lastGroup = null,
                    thiz = this,
                    widgets = [],
                    _lastWidget;

                if (!actions) {
                    console.error('strange, have no actions!');
                    return;
                }

                for (var i = 0; i < actions.length; i++) {

                    var action = actions[i],
                        sAction = action._store ? action._store.getSync(action.command) : null;

                    if (sAction && sAction != action) {
                        console.log('weird!');
                    }
                    if (sAction) {
                        action = sAction;
                    }

                    if (!action) {
                        console.error('invalid action!');
                        continue;
                    }


                    //pick command group
                    if (action.group && _lastGroup !== action.group) {

                        console.log('action group : '+ _lastGroup);
                        if(_lastGroup) {
                            utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true);
                        }
                        _lastGroup = action.group;
                    }

                    //skip if action[visibility].show is false
                    if (thiz.getVisibilityField(action, 'show') === false) {
                        continue;
                    }

                    var _items = action.getChildren ? action.getChildren() : action.items,
                        _hasItems = _items && _items.length,
                        _expand = thiz.getVisibilityField(action, 'expand'),
                        _forceSub = action.forceSubs;


                    force = force != null ? force : this.forceRenderSubActions;


                    _lastWidget = thiz.renderAction(action, where, widgetClass, showLabel);

                }

                return widgets;
            },

            /**
             * Set action store
             * @param store {ActionStore}
             */
            setActionStore: function (store) {

                this.store = store;

                var self = this,
                    allActions = store.query(),

                    //return all actions with non-empty tab field
                    tabbedActions = allActions.filter(function (action) {
                        return action.tab != null;
                    }),

                    //group all tabbed actions : { Home[actions], View[actions] }
                    groupedTabs = _.groupBy(tabbedActions, function (action) {
                        return action.tab;
                    }),

                    //now flatten them
                    _actionsFlattened = [];


                _.each(groupedTabs,function(items,name){
                    _actionsFlattened = _actionsFlattened.concat(items);
                });


                this._renderActions(_actionsFlattened,this,null,null,null,null);

            },

            //
            //      NEW
            //
            /////////////////////////////////////////////////////////////////////////////////////
            /**
             * Set visibility filter
             */
            visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
            /**
             * The separator class for separating actions
             */
            separatorClass:ToolbarSeparator,
            /**
             * The class being used to render an action.
             *
             * @type {dijit/_Widget}
             * @member
             */
            widgetClass:ActionToolbarButton,
            clear:function(){
                //_destroyActions

                //this._destroyActions(this._actions);
            },
            /**
             *
             * @param visibility
             * @private
             */
            _destroyActions: function (visibility) {

                var actions = this.getActionStore().query();

                _.each(actions,function(action){

                    var actionVisibility = action.getVisibility!= null ? action.getVisibility(visibility) : null;
                    if(actionVisibility){

                        var widget = actionVisibility.widget;
                        if(widget){
                            //remove action reference widget
                            action.removeReference && action.removeReference(widget);
                            widget.destroy();
                            this.setVisibilityField(action, 'widget', null);
                        }
                    }
                },this);


            },
            destroy:function(){

                //_destroyActions
                this._destroyActions(this.visibility);

                this.inherited(arguments);
            },
            check:function(){
                if(!this.domNode || this._destroyed){
                    console.warn('@todo: ActionToolbar::check::orphan!');
                    this.destroy();
                    return false;
                }
                return true;
            },
            startup: function () {
                this.inherited(arguments);
            }
        });

        return toolbar;

        /*

        var toolbarClass = declare('xide.widgets.ActionToolbar',[ActionToolbar,ActionContext],{
            setItemActions: function (item, actions,owner) {
                //this should come from onContainerClick
                var _res = this.inherited(arguments);
                return _res;
            }
        });

        */


    }

    function createContextMenuClass(){


        return declare("xide/widgets/ContextMenu", [Stateful, ActionContext,ActionMixin,EventedMixin,_MenuMixin], {
            /**
             * The visibility filter. There an event "REGISTER_ACTION" this class is listening. Actions have a visibility
             * mask and this field will reject those actions which don't have this mask
             * @member visibility {module:xide/types/ACTION_VISIBILITY}
             */
            visibility: types.ACTION_VISIBILITY.CONTEXT_MENU,
            node: null,
            addJQueryClasses: true,
            forceRenderSubActions: true,
            /**
             * Implement action parent widget creation.
             * @param action
             * @param where
             * @returns {*}
             * @private
             */
            __createActionParentWidget:function(action,where){

                var menu = new Menu({parentMenu: where}),
                    self = this,
                    widgetArgs = {
                        label:action.label,
                        popup:menu,
                        iconClass:action.icon ||'fa-magic',
                        __accelKey: 'i'
                    },
                    _extra = self.getVisibilityField(action,'widgetArgs');



                self.setVisibilityField(action,'widget',menu);



                _extra && utils.mixin(widgetArgs,_extra);


                where.addChild(new PopupMenuItem(widgetArgs));

                self._publishActionWidget(menu,action,where);

                return menu;


            },
            /**
             *
             * @param action
             * @param where
             * @param widgetClass
             * @param showLabel
             * @returns {*}
             */
            renderAction: function (action, where, widgetClass, showLabel) {

                /**
                 * Collect variables
                 */

                var thiz = this,


                    _items = action.getChildren ? action.getChildren() : action.items,

                    _hasItems = _items && _items.length,
                    isContainer = _hasItems,
                    _expand = thiz.getVisibilityField(action, 'expand') == true,
                    _forceSub = action.forceSubs,
                    widget = thiz.getVisibilityField(action, 'widget'),
                    parentAction = action.getParent ? action.getParent() : null,
                    parentWidget = parentAction ? thiz.getVisibilityField(parentAction, 'widget') : null,
                    actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};



                /**
                 * Cleanup variables, sanitize
                 */

                //case when visibility is not an object, upgrade visibility to an object
                if (!_.isObject(actionVisibility) && action.setVisibility) {
                    actionVisibility = {};
                    action.setVisibility(this.visibility, actionVisibility);
                }

                //further variables
                var label = '',// actionVisibility.label || action.label,
                    widgetArgs = {
                        iconClass: action.icon,
                        label: label,
                        item: action
                    };

                if (actionVisibility.widgetArgs) {
                    utils.mixin(widgetArgs, actionVisibility.widgetArgs);
                }




                /**
                 * Determine widget class to use, resolve
                 */
                widgetClass =

                    //override
                    actionVisibility.widgetClass ||
                        //argument
                    widgetClass ||
                        //visibility's default
                    thiz.widgetClass;


                // case one: isContainer = true
                if (!widget) {

                    if (isContainer) {

                        if (_expand) {
                            return null;
                        }
                        if (!parentWidget) {
                            return thiz.__createActionParentWidget(action,where);
                        } else {

                            //parent widget there
                            var pSubMenu = new Menu({parentMenu: parentWidget}),
                                popup = new PopupMenuItem({
                                label: label,
                                popup: pSubMenu,
                                iconClass: action.icon
                            });
                            parentWidget.addChild(popup);
                            actionVisibility.widget = pSubMenu;
                            return pSubMenu;
                        }
                    }
                    if (parentWidget) {

                        where = parentWidget;
                        widgetClass = widgetClass || MenuItem;
                    }
                } else {
                    console.log('widget already rendered!',action);
                }

                if (!actionVisibility.widget && !action.domNode) {

                    if (!action.label) {
                        //huh, really? @TODO: get rid of label needs in action:render
                    } else {
                        //console.log('create widget = ' + widgetClass.prototype.declaredClass);
                        actionVisibility.widget = thiz._addMenuItem(action, where, widgetClass, showLabel);
                        //actionVisibility.widget.visibility = thiz.visibility;
                    }
                } else {
                    console.log('have already rendered action', action);
                }

                return actionVisibility.widget;

            },
            /**
             * Renders a set of actions into is appropriate widgets. This utilizes this._renderAction and
             * puts the action's widget into its visibility store for global access.
             *
             * @param actions {xide/action/Action[]}
             * @param where {dijit/_Widget}
             * @param widgetClass {Module}
             * @private
             */
            _renderActions: function (actions, where, widgetClass, showLabel, separatorClass, force) {




                //track the last action group for adding a separator
                var _lastGroup = null,
                    thiz = this,
                    widgets = [],
                    _lastWidget;

                if (!actions) {
                    console.error('strange, have no actions!');
                    return;
                }

                for (var i = 0; i < actions.length; i++) {

                    var action = actions[i],
                        sAction = action._store ? action._store.getSync(action.command) : null;

                    if (sAction && sAction != action) {
                        console.log('weird!');
                    }
                    if (sAction) {
                        action = sAction;
                    }

                    if (!action) {
                        console.error('invalid action!');
                        continue;
                    }


                    //pick command group
                    if (action.group && _lastGroup !== action.group) {

                        if(_lastGroup) {
                            utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true);
                        }
                        _lastGroup = action.group;
                    }

                    //skip if action[visibility].show is false
                    if (thiz.getVisibilityField(action, 'show') === false) {
                        continue;
                    }

                    var _items = action.getChildren ? action.getChildren() : action.items,
                        _hasItems = _items && _items.length,
                        _expand = thiz.getVisibilityField(action, 'expand'),
                        _forceSub = action.forceSubs;


                    force = force != null ? force : this.forceRenderSubActions;


                    _lastWidget = thiz.renderAction(action, where, widgetClass, showLabel);

                }

                return widgets;
            },
            /**
             * Set action store
             * @param store {ActionStore}
             */
            setActionStore: function (store) {

                this.store = store;
            },
            /**
             *
             * @param visibility
             * @private
             */
            _destroyActions: function (visibility) {

                var actions = this.getActionStore().query();

                _.each(actions,function(action){

                    var actionVisibility = action.getVisibility!= null ? action.getVisibility(visibility) : null;
                    if(actionVisibility){

                        var widget = actionVisibility.widget;
                        if(widget){
                            //remove action reference widget
                            action.removeReference && action.removeReference(widget);
                            widget.destroy();
                            this.setVisibilityField(action, 'widget', null);
                        }
                    }
                },this);


            },
            _onOpenMenu: function () {


                //prevent uneeded population
                if (this.rootMenu.dirty !== true) {
                    return;
                }
                this.rootMenu.dirty = false;


                this.empty(this.rootMenu);


                utils.empty(this.rootMenu);



                //_destroyActions
                this._destroyActions(this.visibility);



                var self = this,
                    allActions = this.getActionStore().query(),

                //return all actions with non-empty tab field
                    tabbedActions = allActions.filter(function (action) {
                        return action.tab != null;
                    }),

                //group all tabbed actions : { Home[actions], View[actions] }
                    groupedTabs = _.groupBy(tabbedActions, function (action) {
                        return action.tab;
                    }),

                //now flatten them
                    _actionsFlattened = [];


                _.each(groupedTabs,function(items,name){
                    _actionsFlattened = _actionsFlattened.concat(items);
                });


                this._renderActions(_actionsFlattened,this.rootMenu,null,null,null,null);

                //this._renderActions(flatten, this.rootMenu, null, null, null, true);

            },
            /**
             *
             * @param node {HTMLElement} fitted for dijit/Menu
             */
            createMenu: function (node) {
                this.node = node;//track
                this.rootMenu = this.createContextMenu(node, this._onOpenMenu.bind(this));
                this.rootMenu.dirty = true;
            },
            /**
             * Public main entry, creates the actual context menu for a given node
             *
             * @param node {HTMLElement}
             */
            initWithNode: function (node) {
                this.createMenu(node);
            },
            startup: function () {
                this.inherited(arguments);
            }

        });




    }

    function createContextMenu(){

        var thiz = this,
            _ctorArgs = {

            },
            mixin = {
                owner:this,
                delegate:this
            };


        utils.mixin(_ctorArgs,mixin);

        var clz = createContextMenuClass();

        var contextMenu = new clz(_ctorArgs);
        contextMenu.startup();
        contextMenu.initWithNode(thiz);

        contextMenu.addActionEmitter(this);
        contextMenu.setActionEmitter(this);

        thiz.contextMenu = contextMenu;
    }

    function createMainMenuClass(){

        return declare("xide/widgets/MainMenu", [TemplatedWidgetBase, ActionMixin, _MenuMixin], {
            /**
             * The visibility filter. There an event "REGISTER_ACTION" this class is listening. Actions have a visibility
             * mask and this field will reject those actions which don't have this mask
             * @member visibility {module:xide/types/ACTION_VISIBILITY}
             */
            visibility: types.ACTION_VISIBILITY.MAIN_MENU,
            /**
             *
             */
            templateString: "<div><div data-dojo-attach-point='rootMenu' data-dojo-type='dijit/MenuBar' class='ui-widget ui-widget-content ui-widget-header'></div></div>",

            /**
             * The class being used to render an action.
             *
             * @type {dijit/_Widget}
             * @member
             */
            widgetClass: MenuItem,
            /**
             * Root level action widget, this is the menubar in our case
             */
            rootMenu: null,

            /**
             * Reference to the last incoming & new item actions
             */
            _newActions: null,
            /**
             * Top level layout
             */
            _actions: [

            ],
            /**
             * Fixed menu reference
             */
            wThemesMenu: null,
            //////////////////////////////////////////////////////////////////////////////////////////
            //
            //  Public interface
            //
            //////////////////////////////////////////////////////////////////////////////////////////

            /**
             * 'setItemActions' does merges incoming actions in the menu tree. It won't render it. The
             * rendering takes place in a menu open callback ('this::_onOpenMenu')
             * @param item
             * @param actions
             */
            setItemActions: function (item, actions) {
                if (item == this.lastItem) {
                    return;
                }
                this.lastItem = item;
                this.rootMenu.dirty = true;

                this._newActions = actions;
                //this._computeList(actions);//mix items into this._actions
                /*console.log('new actions',this._actions);*/
            },
            _fixMenu: function (menu) {
                if (menu._popupWrapper && menu._popupWrapper) {
                    var dst = menu._popupWrapper;
                    domClass.add(dst, 'ui-menu ui-widget ui-widget-content');
                }
            },
            _patchMenu: function (widget) {

                var thiz = this;
                aspect.after(widget, 'onOpen', function () {
                    if (this._popupWrapper && this._popupWrapper) {
                        var dst = this._popupWrapper;
                        $(dst).addClass('ui-menu ui-widget ui-widget-content');
                    }
                });
            },
            /**
             * after user opens a menu, walk over top level menu items, and
             * create/disable menu items if necessary, using the last selected item
             * @private
             */
            _onOpenMenu: function () {

                var thiz = this;
                /**
                 * avoid re-creation of menu items
                 */
                if (this.rootMenu.dirty !== true) {
                    return;
                }
                this.rootMenu.dirty = false;

                this._fixMenu(this.rootMenu);

                /**
                 * clear the top menu items
                 * @param items
                 * @private
                 */
                var _clearTopMenu = function (items) {

                    _.each(items, function (level) {

                        if (level.menu && level.fixed !== true) {

                            utils.empty(level.menu);

                            thiz._clearItems(level.items);

                        }
                    });
                };

                var _attach = function (_items) {

                    var _lastGroup = null;
                    _.each(_items, function (level) {
                        var clear = true;

                        if (level.menu) {

                            if (level.items) {

                                for (var prop in level.items) {

                                    var item = level.items[prop];
                                    if (!item) {
                                        continue;
                                    }

                                    if (thiz.shouldShowAction(item) == false) {
                                        continue;
                                    }

                                    if (item.items && !thiz.getVisibilityField(item, 'menu')) {

                                        var menu = new Menu({parentMenu: level.menu});

                                        thiz.setVisibilityField(item, 'menu', menu);

                                        thiz._patchMenu(menu);

                                        thiz._renderActions(item.items, menu);

                                        level.menu.addChild(new PopupMenuItem({
                                            label: item.label,
                                            popup: menu,
                                            iconClass: item.icon || 'fa-magic'
                                        }));

                                        continue;
                                    }


                                    //the item has no menu yet and its not widget
                                    if (item && item.show !== false && !item.items) {
                                        /**
                                         * Insert menu seperator as soon there is a 'group' and the group actually changed
                                         */
                                        if (!_.isEmpty(item.group) && item.group !== _lastGroup) {

                                            var children = level.menu.getChildren();
                                            if (children.length > 0 && children[children.length - 1].prototype != MenuSeparator.prototype) {
                                                utils.addWidget(MenuSeparator, {}, null, level.menu, true);
                                            }

                                            _lastGroup = item.group;
                                        }
                                        //item.menu = null;
                                        thiz._renderAction(item, level.menu);


                                    }
                                }
                            }
                        }
                    });

                }.bind(this);


                //remove all items
                _clearTopMenu(this._actions);
                //_destroyActions

                this._destroyActions(this._actions);

                this._computeList(this._newActions);//mix items into this._actions

                this._computeList(this._permanentActions, true);

                //create new menu items
                _attach(this._actions);

            },
            /**
             * Prepare the top level menu
             * @param data
             * @private
             */
            _buildTopLevel: function (data) {
                _.each(data || this._actions, function (item) {
                    this._addLevel(item, this.rootMenu);
                }, this);
            },
            _createWidgets: function () {
                domConstruct.empty(this.rootMenu.containerNode);
                this._buildTopLevel();
            },
            onReloaded: function () {

                this._createWidgets();
            },
            startup: function () {
                this.inherited(arguments);
                this._createWidgets();
                this.rootMenu.dirty = true;
            },
            destroy: function () {

                this._destroyTop(this._actions);

                this._actions = null;
                this._lastItem = null;
                this.inherited(arguments);

            },

            /**
             * old
             */
            setThemes: function () {

                // availableThemes[] is just a list of 'official' jQuery themes, you can use ?theme=String
                // for 'un-supported' themes, too. (eg: yours)
                var availableThemes = [
                    {theme: "minimal"},
                    {theme: "bootstrap"},
                    {theme: "dflat"},
                    {theme: "metro"},
                    {theme: "dot-luv"},
                    {theme: "smoothness"},
                    {theme: "blitzer"},
                    {theme: "cupertino"},
                    {theme: "dark-hive"},
                    {theme: "eggplant"},
                    {theme: "excite-bike"},
                    {theme: "flick"},
                    {theme: "hot-sneaks"},
                    {theme: "humanity"},
                    {theme: "le-frog"},
                    {theme: "overcast"},
                    {theme: "pepper-grinder"},
                    {theme: "redmond"},
                    {theme: "south-street"},
                    {theme: "start"},
                    {theme: "sunny"},
                    {theme: "swanky-purse"},
                    {theme: "ui-darkness"},
                    {theme: "ui-lightness"},
                    {theme: "vader"}

                ];

                // Get current theme, a11y, and dir setting for page
                var curTheme = location.search.replace(/.*theme=([a-z...\-_/]+).*/, "$1") || "blitzer";
                var a11y = has("highcontrast") || /a11y=true/.test(location.search);
                var rtl = document.body.parentNode.dir == "rtl";

                function setUrl(theme, rtl, a11y) {
                    var aParams = utils.getUrlArgs(location.href);
                    // Function to reload page with specified theme, rtl, and a11y settings
                    var _newUrl = "?theme=" + theme + (rtl ? "&dir=rtl" : "") + (a11y ? "&a11y=true" : "");
                    if (aParams) {
                        _.each(aParams, function (value, key) {
                            if (key !== 'theme') {
                                _newUrl += '&' + key + '=' + value;
                            }
                        });
                    }
                    location.search = _newUrl

                }

                var thiz = this;

                availableThemes = _.sortBy(availableThemes, function (item) {
                    return item.theme;
                });


                // Create menu choices to test other themes
                _.each(availableThemes, function (theme) {
                    thiz.wThemesMenu.addChild(new MenuItem({
                        label: theme.theme,
                        group: "theme",
                        onClick: function () {
                            // Change theme, keep current a11y and rtl settings
                            setUrl(theme.theme, false, false);
                        }
                    }));
                });

            },
            fixButton: function (button) {

                if (button && button.iconNode) {
                    domClass.add(button.domNode, 'ui-menu-item');
                    domClass.remove(button.iconNode, 'dijitReset');
                    domClass.add(button.iconNode, 'actionToolbarButtonElusive');
                }
            }

        });
    }

    function createMainMenu(){


        utils.destroy(window['mainMenu']);


        var mainView = ctx.mainView;



        mainView.mainMenu = utils.addWidget(createMainMenuClass(), {}, this, mainView.layoutTop, true);

        domConstruct.place(mainView.mainMenu.domNode, mainView.layoutTop.containerNode, 'first');


        window['mainMenu'] = mainView.mainMenu;

    }


    function doTests(grid){

        createMainMenu();

        grid.deselectAll();

        grid.select([2],null,true,{
            focus:true,
            append:false
        });


    }



    function testMain(grid,panel){

        var ACTION_TYPE = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            ribbon,
            mainView = ctx.mainView;

        console.clear();


        grid.refresh().then(function(){

            setTimeout(function(e){
                doTests(grid);
            },1000);
        });


    }



    if (ctx) {


        var doTest = true;
        if (doTest) {

            var ACTION = types.ACTION;

            var grid = XFileTestUtils.createFileGrid('root',
                //args
                {

                    _columns: {
                        "Name": true,
                        "Path": false,
                        "Size": false,
                        "Modified": false,
                        "Owner":false
                    },
                    __permissions: [
                        ACTION.EDIT,
                        /*
                        ACTION.EDIT,
                        ACTION.RENAME,
                        ACTION.DOWNLOAD,
                        ACTION.COLUMNS,
                        ACTION.GO_UP,
                        ACTION.CLIPBOARD
                        */
                        //ACTION.COLUMNS,
                        ACTION.LAYOUT,
                        ACTION.COLUMNS,
                        ACTION.TOOLBAR
                        //ACTION.GO_UP,
                        //ACTION.NEW_FILE,
                        //ACTION.NEW_DIRECTORY
                    ],
                    tabOrder: {
                        'Home': 100,
                        'View': 50,
                        'Settings': 20
                    },
                    tabSettings: {
                        'Step': {
                            width:'190px'
                        },
                        'Show': {
                            width:'130px'
                        },
                        'Settings': {
                            width:'100%'
                        }
                    },
                    groupOrder: {
                        'Clipboard': 110,
                        'File': 100,
                        'Step': 80,
                        'Open': 70,
                        'Organize': 60,
                        'Insert': 10,
                        'Select': 5,
                        'Navigation':4
                    }
                },

                //overrides
                {

                    _createContextMenu:function(){
                        createContextMenu.apply(this);
                    },

                    /**
                     *
                     * @param show
                     * @param toolbarClass
                     */
                    showToolbar:function(show){

                        if(show==null){
                            show = this._toolbar==null;
                        }

                        if(show && !this._toolbar){

                            this._toolbar = utils.addWidget(createToolbarClass(),{
                                "class":"dijit dijitToolbar",
                                style:'min-height:30px;height:auto;width:100%'
                            },this,this.header,true);


                            this._toolbar.addActionEmitter(this);
                            this._toolbar.setActionEmitter(this);


                        }
                        if(!show && this._toolbar){
                            utils.destroy(this._toolbar,true,this);
                        }

                        this.resize();

                    }



            },'TestGrid',module.id,true);



            function test() {
                testMain(grid,grid._parent);
            }

            setTimeout(function () {
                test();
            }, 1000);
        }
    }

    return declare('maeh',null,{});

});
