/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "./TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "xide/registry",

    "module",

    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    // has("ie")
    "dojo/string", // string.substitute string.trim
    "xide/_base/_Widget"


], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget
) {



    console.clear();
    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;




    function createWidgetNormalClass(){
        return _XWidget;
    }
    function doWidgetTests(tab){

        console.log('doWidgetTests');

        var _class = createClass_WidgetBase();

        var _templatedClass = createClass_Templated();

        //var _attachClass = createClass_AttachMixin();

        var _finalClass = dcl([_class,_templatedClass],{
        });

        var _baseWidget = new _finalClass({
            templateString:'<div>helo 2<button attachTo="button" class="btn btn-warning">warning</button></div>'
        },tab.containerNode);



        console.log('_base_widget',_baseWidget);


    }

    function createTabClass(baseClass){

        return dcl(baseClass,{
            declaredClass:'xide/widgets/TabContainerTab',
            iconClass:null,
            open:true,
            titleBar:null,
            titleNode:null,
            toggleNode:null,
            containerNode:null,
            height:'400px',
            padding:'0px',
            panelNode:null,
            templateString:'<div></div>',
            resize:function(){

                console.log('resize tab');
                this.inherited(arguments);
            },
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);


                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            _onShown:function(e){
                this.resize();
            },
            addChild:function(what,index,startup){

                //collect widget
                this.add(what,null);

                utils.addChild(this.containerNode,what.domNode);

                if(startup!==false && !what._started){
                    what.startup();
                }
            },
            buildRendering:function(){

                this.inherited(arguments);

                var self = this;
                var panel = this.panelNode;
                this.__addHandler(panel,'hidden.bs.collapse','_onHided');
                this.__addHandler(panel,'hide.bs.collapse','_onHide');
                this.__addHandler(panel,'shown.bs.collapse','_onShown');
                this.__addHandler(panel,'show.bs.collapse','_onShow');


            },
            postMixInProperties:function(){

                var closed = !this.open;
                this.ariaOpen = closed ? 'true' : 'false';
                this.containerClass = closed ? 'collapse' : 'collapse in';
                this.titleClass = closed ? 'collapsed' : '';

                var widgetStr = '<div style="float: :right" class="widget-controls">'+
                        '<a title="Options" href="#"><i class="fa-cogs"></i></a>'+
                        /*'<a data-widgster="expand" title="Expand" href="#"><i class="fa-chevron-up"></i></a>'+
                        '<a data-widgster="collapse" title="Collapse" href="#"><i class="fa-chevron-down"></i></a>'+*/
                        '<a data-widgster="close" title="Close" href="#"><i class="fa-remove"></i></a>'+
                    '</div>';

                widgetStr ='';

                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';
                var titleStr = '<span attachTo="titleNode" class=""> ${!title}</span>';
                var toggleNodeStr =
                    '<a attachTo="toggleNode" href="#${!id}-Collapse" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                        iconStr +   titleStr
                    '</a>';




                this.templateString = '<div class="panel widget" attachTo="panelNode">'+

                        '<div class="panel-heading" attachTo="titleBar">'+
                            toggleNodeStr +
                        '</div>'+

                        '<div attachTo="containerRoot" class="panel-collapse ${!containerClass}" id="${!id}-Collapse" aria-expanded="${!ariaOpen}">'+
                            '<div style="height: ${!height};padding:${!padding}" class="panel-body" attachTo="containerNode"></div>'+
                        '</div>'+

                    '</div>';


                this.inherited(arguments);
            }
        });
    };

    function doTests(tabContainer,tabClass){

        var pane = tabContainer.createTab('bla bla','fa-code',false,tabClass);
        var pane2 = tabContainer.createTab('Files','fa-cogs',true,tabClass);


        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane2);



        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane);



        /*
        setTimeout(function(){
            //pane.show();
        },1000);


        setTimeout(function(){
            pane.hide();
        },5000);
*/
    }

    function _createTabContainerClass(baseClass){

        var tabClass = dcl(baseClass,{
            declaredClass:'xide/widgets/TabContainer',
            templateString:'<div class="panel-group" attachTo="containerNode"/>',
            createTab:function(title,icon,open,tabClass){


                /*
                var _tab= new tabClass({
                    title:title,
                    iconClass:icon,
                    open:open
                },this.containerNode);
                */
/*
                var _tab= utils.addWidget(tabClass,{
                    title:title,
                    iconClass:icon,
                    open:open
                },this,this.containerNode,true);

                console.log('-------------add tab',_tab);


                return _tab;
                */
                return this.add(tabClass,{
                    title:title,
                    iconClass:icon,
                    open:open
                },this.containerNode,true);
            }

        });

        return tabClass;

    }

    function _createAccContainer(){

        var tabClass = declare(TemplatedWidgetBase,{
            templateString:'<div class="panel-group" data-dojo-attach-point="containerNode"/>',
            createTab:function(title,icon){

                return utils.addWidget(createTabClass(),{
                    title:title,
                    iconClass:icon
                },null,this.domNode,true);





/*
                '<div class="widget-controls">'+
                '<a data-widgster="load" title="Reload" href="#"><i class="glyphicon glyphicon-refresh"></i></a>'+
                '<a data-widgster="expand" title="Expand" href="#"><i class="fa fa-code"></i></a>'+
                '<a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-minus"></i></a>'+
                '<a data-widgster="fullscreen" title="Full Screen" href="#"><i class="glyphicon glyphicon-resize-full"></i></a>'+
                '<a data-widgster="restore" title="Restore" href="#"><i class="glyphicon glyphicon-resize-small"></i></a>'+
                '<a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>'+
                '</div>'+
                    */

                var closed = true;

                var ariaOpen = closed ? 'true' : 'false';
                var containerClass = closed ? 'collapse' : 'collapse in';
                var titleClass = closed ? 'collapsed' : '';

                var toggleNodeStr =
                    '<a data-dojo-attach-point="toggleNode" href="#collapseOneTwo" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                        '${!title}'+
                    '</a>';
                    //'<a data-widgster="expand" title="Expand" href="#"><i class="fa fa-arrow-down"></i></a>';

                var panelTemplate = '<div class="panel widget">'+

                    '<div class="panel-heading" data-dojo-attach-point="titleBar">'+

                        toggleNodeStr +

                    '</div>'+

                    '<div data-dojo-attach-point="containerRoot" class="panel-collapse ${!containerClass}" id="collapseOneTwo" aria-expanded="${!ariaOpen}" style="">'+
                        '<div class="panel-body" data-dojo-attach-point="containerNode">'+
                            'asdfasdf'+
                        '</div>'+
                    '</div>'+

                '</div>'


                var pane  = utils.templatify(null,panelTemplate, this.domNode , {
                    iconClass:'fa-play',
                    title:title,
                    ariaOpen : ariaOpen,
                    containerClass:containerClass,
                    titleClass:titleClass,
                    show:function(){

                        var container = $(this.containerRoot),
                            toggleNode= $(this.toggleNode);

                        toggleNode.removeClass('collapsed');
                        toggleNode.attr('aria-expanded',true);


                        container.removeClass('collapse');
                        container.addClass('collapse in');
                        container.attr('aria-expanded',true);

                    },
                    hide:function(){

                        var container = $(this.containerRoot),
                            toggleNode= $(this.toggleNode);

                        toggleNode.addClass('collapsed');
                        toggleNode.attr('aria-expanded',false);


                        container.removeClass('collapse in');
                        container.addClass('collapse');
                        container.attr('aria-expanded',false);
                    }
                });


                return pane;

                /*
                var panel = $(panelTemplate);

                $(this.domNode).append(panel);*/


            }
        });

        return tabClass;

    }

    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;




    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {


        var parent = TestUtils.createTab('Acc-Test',null,module.id);

        ////////////////////////////////////////////////////////
        //
        //  Test with creating a tab container basing on the new widget class
        //

        var widgetBaseClass = createWidgetNormalClass();
        var tabContainerClass = _createTabContainerClass(widgetBaseClass);

        var tabContainer = utils.addWidget(tabContainerClass,{},null,parent,true);
        var tabClass = createTabClass(widgetBaseClass);

        var pane = tabContainer.createTab('bla bla','fa-code',true,tabClass);
        var grid = FTestUtils.createFileGrid('root',{},{},'TestGrid',module.id,true,pane);

        var pane2 = tabContainer.createTab('bla bla 2','fa-code',true,tabClass);
        var grid2 = FTestUtils.createFileGrid('root',{},{},'TestGrid',module.id,true,pane2);



        return declare('a',null,{});

    }

    return Grid;

});