define([
    "dcl/dcl",
    "xide/utils",
    "xdojo/declare",
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    "xide/mixins/EventedMixin",
    'xide/data/TreeMemory',
    'xide/data/Memory',
    'dstore/Trackable',

    "xide/data/Model",
    'xide/data/Source',
    "xide/types"
], function (dcl,utils,declare,TestUtils,_Widget,module,EventedMixin,TreeMemory,Memory,Trackable,Model,Source,types){
    var SOURCE_CLASS = declare('xgrid.data.Source', null,{
        _references: null,
        _originReference: null,
        onReferenceUpdate: function () {
        },
        onReferenceRemoved: function () {
        },
        onReferenceDelete: function () {
        },
        updateReference: function () {
        },
        destroy:function(){
            this._references=null;
        },
        getReferences: function () {
            return this._references ? utils.pluck(this._references, 'item') : [];
        },
        addReference: function (item, settings, addSource) {
            !this._references && (this._references = []);
            this._references.push({
                item: item,
                settings: settings
            });
            var thiz = this;
            if (settings && settings.onDelete) {
                if (item._store) {
                    item._store.on('delete', function (evt) {
                        if (evt.target == item) {
                            thiz._store.removeSync(thiz[thiz._store['idProperty']]);
                            thiz._references.remove(evt.target);
                        }
                    })
                }
            }

            if (addSource) {
                if (item.addSource) {
                    item.addSource(this, settings);
                } else {
                    _debug && console.log('empty: ', item.command);
                }
            }
        },
        removeReference: function (Reference) {
            _debug && console.log('remove reference ' + Reference.label, Reference);
            this._references && _.each(this._references, function (ref) {
                if (ref && ref.item == Reference) {
                    this._references && this._references.remove(ref);
                }
            }, this);
        },
        updateReferences: function (args) {
            var property = args.property,
                value = args.value;

            if (!this._references) {
                this._references = [];
            }
            for (var i = 0; i < this._references.length; i++) {
                var link = this._references[i],
                    item = link.item,
                    settings = link.settings,
                    store = item._store;

                if (this._originReference == item) {
                    continue;
                }
                if (args.property && settings.properties && settings.properties[args.property]) {
                    if (store) {
                        store._ignoreChangeEvents = true;
                    }
                    try {
                        if (item.onSourceChanged) {
                            item.onSourceChanged(property, value);
                        } else {
                            item.set(property, value);
                        }

                    } catch (e) {
                        _debug && console.error('error updating reference! ' + e, e);
                    }
                    if (store) {
                        store._ignoreChangeEvents = false;
                        store.emit('update', {target: item});
                    }
                }
            }
        },
        constructor: function (properties) {
            this._references = [];
            utils.mixin(this, properties);
        },
        onItemChanged: function (args) {
            this.updateReferences(args);
        }
    });
    var REFERENCE_CLASS = declare('xgrid.data.Source', null,{
        _sources: [],
        removeSource: function (source) {
        },
        updateSource: function (sources) {
        },
        onSourceUpdate: function (source) {
        },
        onSourceRemoved: function (source) {
        },
        onSourceDelete: function (source) {
        },
        onItemChanged: function (args) {
        },
        destroy: function () {
            if (this.item && !this.item.removeReference) {
                debug && console.error('item has no removeReference');
            } else {
                this.item && this.item.removeReference(this);
            }
            this.inherited && this.inherited(arguments);

            for (var i = 0; i < this._sources.length; i++) {
                var link = this._sources[i];
                if(link.item) {
                    link.item.removeReference && link.item.removeReference(this);
                }
            }
            this._sources = null;
        },
        addSource: function (item, settings) {
            this._sources.push({
                item: item,
                settings: settings
            });
            var thiz = this;
            if (settings && settings.onDelete) {
                item._store.on('delete', function (evt) {
                    if (evt.target == item) {
                        thiz._store.removeSync(thiz[thiz._store['idProperty']]);
                    }
                })
            }
        },
        updateSources: function (args) {
            for (var i = 0; i < this._sources.length; i++) {
                var link = this._sources[i];
                var item = link.item;
                var settings = link.settings;
                if (args.property && settings.properties &&
                    settings.properties[args.property]) {
                    item._store._ignoreChangeEvents = true;
                    item.set(args.property, args.value);
                    item._store._ignoreChangeEvents = false;
                    item._store.emit('update', {target: item});
                }
            }
        },
        constructor: function (properties) {
            this._sources = [];
            utils.mixin(this, properties);
        }
    });
    var OBSERVAVBLE_STORE_CLASS = declare('xide/data/Observable', EventedMixin, {
        _ignoreChangeEvents: true,
        observedProperties: [],
        mute:false,
        /**
         *
         * @param silent {boolean|null}
         */
        silent:function(silent){
            (silent===true || silent===false) && (this._ignoreChangeEvents = silent);
        },
        putSync: function (item,publish) {
            this.silent(true);
            var res = this.inherited(arguments);
            this.silent(true);
            publish!==false && this.emit('added', res);
            return res;
        },
        removeSync: function (id,silent) {
            this.silent(silent);
            var _item = this.getSync(id);
            _item && _item.onRemove && _item.onRemove();
            var res = this.inherited(arguments);
            this.silent(false);
            return res;
        },
        postscript: function () {
            var thiz = this;
            thiz.inherited(arguments);
            if (!thiz.on) {
                return;
            }
            thiz.on('add', function (evt) {
                var _item = evt.target;
                thiz._observe(_item);
                if (!_item._store) {
                    _item._store = thiz;
                }
                _item._onCreated && _item._onCreated();
                if(!_item._onCreated && _debug){
                    console.warn('item doesnt have _onCreated',_item);
                }
                _item.onAdd && _item.onAdd(_item);
            });
        },
        /**
         *
         * @param item
         * @param property
         * @param value
         * @param source
         * @private
         */
        _onItemChanged: function (item, property, value, source) {
            if (this._ignoreChangeEvents) {
                return;
            }
            var args = {
                target: item,
                property: property,
                value: value,
                source: source
            };
            this.emit('update', args);
            item.onItemChanged && item.onItemChanged(args);
        },
        _observe: function (item) {
            var thiz = this,
                props = thiz.observedProperties;

            if (item && item.observed) {
                props = props.concat(item.observed);
            }
            props && props.forEach(function (property) {
                item.property(property).observe(function (value) {
                    if (!thiz._ignoreChangeEvents) {
                        thiz._onItemChanged(item, property, value, thiz);
                    }
                });
            });
        },
        setData: function (data) {
            this.inherited(arguments);
            this.silent(true);
            data && _.each(data,this._observe, this);
            this.silent(false);
        }
    });

    var STORE_CLASS = declare('deviceStore', [TreeMemory, Trackable, OBSERVAVBLE_STORE_CLASS], {});
    var ItemModelClass = declare('xcf.model.Device',[Model,SOURCE_CLASS,EventedMixin],{
        _userStopped:false,
        /**
         * @type {module:xide/types~DEVICE_STATE}
         * @link module:xide/types/DEVICE_STATE
         * @see module:xide/types/DEVICE_STATE
         */
        state:types.DEVICE_STATE.DISCONNECTED,
        /**
         * The driver instance
         * @private
         */
        driverInstance:null,
        /**
         * The block scope of the driver instance (if the device is connected and ready)
         * @private
         */
        blockScope:null,
        getParent:function(){
            return this.getStore().getSync(this.parentId);
        },
        isServerSide:function(){
            var driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            return (1 << types.DRIVER_FLAGS.RUNS_ON_SERVER & driverOptions) ? true : false;
        },
        isServer:function(){
            var driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            return (1 << types.DRIVER_FLAGS.SERVER & driverOptions) ? true : false;
        },
        setServer:function(isServer){
            var driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            driverOptions.value = driverOptions.value | (1 << types.DRIVER_FLAGS.SERVER);
            this.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS,driverOptions.value);
        },
        setServerSide:function(isServer){
            var driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            driverOptions= driverOptions | (1 << types.DRIVER_FLAGS.RUNS_ON_SERVER);
            this.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS,driverOptions);
        },
        isDebug:function(){
            var driverOptions = this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS);
            return (1 << types.DRIVER_FLAGS.DEBUG & driverOptions) ? true : false;
        },
        check:function(){
            if(this._startDfd && this._userStopped===true){
                this.reset();
            }
        },
        getStore:function(){
            return this._store;
        },
        getScope:function(){
            var store = this.getStore();
            return store ? store.scope : this.scope;
        },
        isEnabled:function(){
            return this.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED) === true;
        },
        setEnabled:function(enabled){
            return this.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED,enabled);
        },
        shouldReconnect:function(){
            if (this._userStopped) {
                return false;
            }
            return this.isEnabled();
        },
        reset:function(){
            delete this._startDfd;
            this._startDfd = null;
            delete this['blockScope'];
            this['blockScope'] = null;
            delete this.serverVariables;
            this.serverVariables = null;
            delete this['driverInstance'];
            this['driverInstance'] = null;
            clearTimeout(this.reconnectTimer);
            delete this.lastReconnectTime;
            delete this.reconnectRetry;
            delete this.isReconnecting;
            this.setState(types.DEVICE_STATE.DISCONNECTED);
        },
        /**
         * @constructor
         * @alias module:xcf/model/Device
         */
        constructor:function(){},
        /**
         * Returns the block scope of the a driver's instance
         * @returns {module:xblox/model/Scope}
         */
        getBlockScope:function(){
            //return this.blockScope;
            return this.driverInstance && this.driverInstance.blockScope ? this.driverInstance.blockScope : this.blockScope;
        },
        /**
         * Returns the driver instance
         * @returns {model:xcf/driver/DriverBase}
         */
        getDriverInstance:function(){
            return this.driverInstance;
        },
        /**
         * Return the driver model item
         * @returns {module:xcf/model/Driver|null}
         */
        getDriver:function(){
            var scope = this.getBlockScope();
            if(scope){
                return scope.driver;
            }
            return null;
        },
        /**
         * Return a value by field from the meta database
         * @param title
         * @returns {string|int|boolean|null}
         */
        getMetaValue: function (title) {
            return utils.getCIInputValueByName(this.user,title);
        },
        /**
         * Set a value in the meta database
         * @param title {string} The name of the CI
         * @returns {void|null}
         */
        setMetaValue: function (what,value,publish) {
            var item = this;
            var meta = this.user;
            var ci = utils.getCIByChainAndName(meta, 0, what);
            if(!ci){
                return null;
            }
            var oldValue = this.getMetaValue(what);
            utils.setCIValueByField(ci, 'value', value);

            this[what] = value;
            if(publish!==false){
                var eventArgs = {
                    owner: this.owner,
                    ci: ci,
                    newValue: value,
                    oldValue: oldValue
                };
                return this.publish(types.EVENTS.ON_CI_UPDATE, eventArgs);
            }
        },
        /**
         * Return the internal state icon
         * @param state
         * @returns {string|null}
         */
        getStateIcon:function(state ){
            state = state || this.state;
            switch (state) {
                case types.DEVICE_STATE.DISCONNECTED:
                {
                    return 'fa-unlink iconStatusOff'
                }
                case types.DEVICE_STATE.READY:
                case types.DEVICE_STATE.CONNECTED:
                {
                    return 'fa-link iconStatusOn'
                }
                case types.DEVICE_STATE.SYNCHRONIZING:
                case types.DEVICE_STATE.CONNECTING:
                {
                    return 'fa-spinner fa-spin'
                }
                case types.DEVICE_STATE.LOST_DEVICE_SERVER:
                {
                    return 'fa-spinner fa-spin'
                }
            }
            return 'fa-unlink iconStatusOff';
        },
        /**
         * Set the state
         * @param state
         * @param silent
         */
        setState:function(state,silent){
            if(state===this.state){
                return;
            }
            var oldState = this.state,
                icon = this.getStateIcon(state);
            this.state = state;
            this.set('iconClass',icon);
            this.set('state',state);
            this._emit(types.EVENTS.ON_DEVICE_STATE_CHANGED,{
                old:oldState,
                state:state,
                icon:icon,
                "public":true
            });
            this.refresh();
        }
    });
    var store = null;
    function createStore(data) {
        new storeClass({
            data: data,
            idProperty: 'path',
            parentProperty: 'parentId',
            Model: ItemModelClass,
            id: utils.createUUID(),
            mayHaveChildren: function (parent) {
                return true;
            },
            observedProperties: [
                "name",
                "state",
                "iconClass",
                "enabled"
            ]
        });
        return store;
    }
    var ctx = window.sctx,
        root;

    if (ctx) {

        var parent = TestUtils.createTab(null,null,module.id);



        return declare('a',null,{});

    }
    return _Widget;

});

