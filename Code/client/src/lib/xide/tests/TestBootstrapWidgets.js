/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',
    "xide/widgets/TemplatedWidgetBase"

], function (declare,dcl,types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase) {



    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;




    var propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };




    function _createTemplateBaseClass(){

        var templated = dcl(null,{

            templateUrl:'xide/tests/accordion_test.html',
            buildRenderering:function(){

                var _templateText = this._getTemplate();

                var root = $('div');
                var node = root.loadTemplate(require.toUrl(this.templateUrl));
                this.domNode = node;
            },
            _getTemplate:function(){
                return this.templateString || this._getText(this.templateUrl);
            },
            startup:function(){

                this.buildRenderering();

            },
            _getText:function(url){

                url = require.toUrl(url);
                var text = $.ajax({
                    url: url,
                    async:false
                });
                return text.responseText;
            }
        });

        return templated;

    }


    function doTests(){}





    var createDelegate = function(){

        return {

        }


    }


/*
     * playground
     */
    var _lastGrid = window._lastBoot;
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;



    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {







        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();
        //marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        //scope = createScope();

        //openDriverSettings(marantz);

        var toolbar = ctx.mainView.getToolbar();
        var docker = ctx.mainView.getDocker();
        var parent  = window.bootTab;
        if(parent){
            docker.removePanel(parent);
        }

        parent = docker.addTab(null, {
            title: 'Marantz/Marantz',
            icon: 'fa-exchange'
        });

        window.bootTab = parent;




        var _accHTML = require.toUrl('xide/tests/accordion_test.html');

        //var _accHTML = require.toUrl('xide/tests/breadcrumb_test.html');

        //var _accHTML = require.toUrl('xide/tests/acc_test.html');



        //var _accHTML = require.toUrl('xide/tests/notification_test.html');


        function _getText(url) {
            var result;
            var def = dojo.xhrGet({
                url: url,
                sync: true,
                handleAs: 'text',
                load: function (text) {
                    result = text;
                }
            });
            return '' + result + '';
        }

        var template = _getText(_accHTML);





        var widget = declare('x',TemplatedWidgetBase,{
            templateString:template
        });





        utils.addWidget(widget,{},null,parent,true);



        return declare('a',null,{});

    }

    return Grid;

});