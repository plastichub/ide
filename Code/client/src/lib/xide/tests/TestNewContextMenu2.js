/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry',
    'xide/_base/_Widget',
    "dojo/text!./Navbar.html",
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    'xide/action/DefaultActions',
    "xaction/ActionProvider",
    "xgrid/Clipboard",
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    'xide/mixins/EventedMixin',
    "xide/widgets/_MenuMixin2"

], function (declare, dcl, types, utils, TestUtils, _Widget, module, registry, _XWidget, MenuBarTemplate, ActionMixin, ActionContext, ActionStore, DefaultActions, ActionProvider, Clipboard, Path, Action, FTestUtils, i18, EventedMixin,_MenuMixin2) {


    
    /**
     *
     * description
     */
    var ACTION = types.ACTION;
    var options = {
        fadeSpeed: 100,
        filter: function ($obj) {
            // Modify $obj, Do not return
        },
        above: 'auto',
        left: 'auto',
        preventDoubleContext: true,
        compress: false

    };
    var createCallback = function (func, menu, item) {

        return function (event) {
            func(event, menu, item);
        };
    }

    console.clear();

    var _ctx = {};
    var _debug = true;
    var grid = null;

    var MenuMixinClass = dcl(null, {
        constructor:function(options,node){
            this.target = node;
        },
        init: function(opts){

            var options = this.getDefaultOptions();

            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root,'click',null,function(){
                $('.dropdown-context').fadeOut(options.fadeSpeed, function(){
                    $('.dropdown-context').css({display:''}).find('.drop-left').removeClass('drop-left');
                });
            });

            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }


            this.__on(root,'mouseenter', '.dropdown-submenu', function(e){

                var _root = $(e.currentTarget);
                var $sub = _root.find('.dropdown-context-sub:first');
                var subWidth = $sub.width(),
                    subLeft = $sub.offset().left,
                    collision = (subWidth+subLeft) > window.innerWidth;
                if(collision){
                    $sub.addClass('drop-left');
                }
            });
        },
        getDefaultOptions:function(){
            return options = {
                fadeSpeed: 100,
                filter: function ($obj) {
                    // Modify $obj, Do not return
                },
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: true

            };
        },
        /**
         *
         * @param $menu
         * @param data
         * @param id
         * @param subMenu
         * @param addDynamicTag
         * @returns {*}
         */
        buildMenuItems: function ($menu, data, id, subMenu, addDynamicTag) {
            var linkTarget = '',
                self = this;

            for (var i = 0; i < data.length; i++) {
                var item = data[i],
                    $sub;
                if (typeof data[i].divider !== 'undefined') {
                    var divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    divider += '"></li>';
                    $menu.append(divider);
                } else if (typeof data[i].header !== 'undefined') {
                    var header = '<li class="nav-header';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + data[i].header + '</li>';
                    $menu.append(header);
                } else if (typeof data[i].menu_item_src !== 'undefined') {
                    var funcName;
                    if (typeof data[i].menu_item_src === 'function') {
                        if (data[i].menu_item_src.name === "") { // The function is declared like "foo = function() {}"
                            for (var globalVar in window) {
                                if (data[i].menu_item_src == window[globalVar]) {
                                    funcName = globalVar;
                                    break;
                                }
                            }
                        } else {
                            funcName = data[i].menu_item_src.name;
                        }
                    } else {
                        funcName = data[i].menu_item_src;
                    }
                    $menu.append('<li class="dynamic-menu-src" data-src="' + funcName + '"></li>');
                } else {

                    if (typeof data[i].href == 'undefined') {
                        data[i].href = '#';
                    }
                    if (typeof data[i].target !== 'undefined') {
                        linkTarget = ' target="' + data[i].target + '"';
                    }
                    if (typeof data[i].subMenu !== 'undefined') {
                        var sub_menu = '<li class="dropdown-submenu';
                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a tabindex="-1" href="' + data[i].href + '">';
                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }
                        sub_menu += data[i].text + '';
                        sub_menu += '</a></li>';

                        $sub = $(sub_menu);/*
                        console.error('build sub menu');
                        $sub.on('show.bs.dropdown', function () {
                            console.error('on open');
                        });
                        */
                    } else {

                        if (item.render) {

                            $sub = item.render(item, $menu);

                        } else {
                            var element = '<li class="" ';
                            element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                            element += '><a tabindex="-1" href="' + data[i].href + '"' + linkTarget + '>';
                            if (typeof data[i].icon !== 'undefined') {
                                element += '<span class="glyphicon ' + data[i].icon + '"></span> ';

                            }
                            element += data[i].text + '</a></li>';
                            $sub = $(element);
                            if (item.postRender) {
                                item.postRender($sub);
                            }
                        }
                    }
                    if (typeof data[i].action !== 'undefined') {

                        if (item.addClickHandler && item.addClickHandler() === false) {

                        } else {
                            var $action = data[i].action;
                            if ($sub && $sub.find) {
                                $sub.find('a')
                                    .addClass('context-event')
                                    .on('click', createCallback($action, item, $sub));
                            }
                        }
                    }

                    $menu.append($sub);
                    item.widget = $sub;

                    if (typeof data[i].subMenu != 'undefined') {
                        var subMenuData = self.buildMenu(data[i].subMenu, id, true);
                        $menu.find('li:last').append(subMenuData);
                    }
                }



                $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                    e.stopPropagation()
                });

            }
            return $menu;
        },
        /**
         *
         * @param data
         * @param id
         * @param subMenu
         * @returns {*}
         */
        buildMenu: function (data, id, subMenu) {

            var subClass = (subMenu) ? ' dropdown-context-sub' : '',
                $menu = $('<ul role="menu" class="dropdown-menu dropdown-context ' + subClass + '" id="dropdown-' + id + '"></ul>');





            /*
            if(!subMenu) {
                $(this.openTarget).attr('data-toggle', 'context');
                $(this.openTarget).attr('data-target', 'dropdown-' + id);
            }
            */
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        createNewAction:function(command){

            var segments = command.split('/');
            var lastSegment = segments[segments.length - 1];
            var action = new Action({
                command: command,
                label: lastSegment,
                group: lastSegment
            });
            return action;
        },
        getAction:function(command,store){

            store = store || this.store;

            var action = null;

            if(store) {
                action = store.getSync(command);
                if(!action){
                    action = this.createNewAction(command);
                }
            }
            return action;
        },
        toActions:function(commands,store){

            var result = [],
                self = this;
            _.each(commands, function (path) {
                var _action = self.getAction(path,store);
                _action && result.push(_action);
            });
            return result;
        },
        onRunAction:function(action,owner,e){

            _debug && console.log('run action ',action.command);
            return DefaultActions.defaultHandler.apply(owner,[action,e]);
        },
        getActionProperty:function(action,visibility,prop){

            var value = prop in action ? action[prop] : null;
            if(visibility && prop in visibility){
                value = visibility[prop];
            }
            return value;
        },
        toMenuItem:function(action, owner, label, icon, visibility) {

            var self = this,
                labelLocalized = self.localize(label),
                widgetArgs = visibility.widgetArgs,
                actionType = visibility.actionType || action.actionType;


            //console.log(action.command  + ' = ' + actionType);
            var item = {
                text: labelLocalized,
                icon: icon,
                data: action,
                owner:owner,
                addClickHandler: function (item) {

                    var action = this.data;
                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        return false;
                    }

                    return true;
                },
                render: function (data, $menu) {

                    var action = this.data;
                    var parentAction = action.getParent();




                    var closeOnClick = self.getActionProperty(action,visibility,'closeOnClick');
                    //console.log(action.command + '  = ' + closeOnClick);;

                    var keyComboString = ' \n';


                    if(action.command==ACTION.SOURCE) {
                        //console.log('action source',[action,parentAction]);
                        console.error('sdfsdf');
                    }


                    //console.error('render command ' + action.command);


                    if (action.keyboardMappings) {

                        var mappings = action.keyboardMappings;
                        var keyCombos = _.pluck(mappings, ['keys']);
                        if (keyCombos && keyCombos[0]) {
                            keyCombos = keyCombos[0];

                            if (keyCombos.join) {
                                var keycombo = [];
                                keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                            }
                        }
                    }

                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {

                        var element = '<li class="" >';
                        var id = action._store.id + '_' + action.command;
                        var checked = action.get('value');

                        //checkbox-circle
                        element += '<div class="checkbox checkbox-success ">';
                        if (typeof data.icon !== 'undefined') {
                            //element += '<span style="float: left" class="' + data.icon + '"></span>';
                        }
                        element += '<input id="' + id + '" type="checkbox" ' + (checked == true ? 'checked' : '') + '>';
                        element += '<label for="' + id + '">';
                        element += self.localize(data.text) + '</label>';
                        element += '</div>';

                        $menu.addClass('noclose');
                        var result = $(element);
                        var checkBox = result.find('INPUT');
                        checkBox.on('change', function (e) {
                            //console.warn('change ' + checkBox[0].checked);
                            action.set('value', checkBox[0].checked);
                        });

                        return result;
                    }



                    closeOnClick===false && $menu.addClass('noclose');


                    

                    /*
                    <div class="wcLoadingContainer"><div class="container-fluid center-block" style="opacity: 1;">

                        <div class="row-fluid">
                        <div class="offset3 span6 centering">
                        <div class="spinner-loader" style="">
                    </div>

                    </div></div></div>
                    </div>
                        */



                    if(parentAction) {
                        //console.log('single toggle ' + parentAction.command,parentAction);
                    }


                    if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {

                        if(action.command==ACTION.SOURCE) {
                            //console.log('single toggle',[action,parentAction]);
                        }


                        var value = action.value || action.get('value');

                        var parentValue = parentAction.get('value');

                        if(action.command.includes(ACTION.SOURCE)) {
                            //console.error('source ',[value,parentValue]);
                        }

                        if (value == parentValue) {
                            icon = 'fa-check';
                        }
                    }





                    //default:
                    var element = '<li ';
                    element += '';
                    element += '><a title="' + data.text + '\n' + keyComboString + '" href="' + data.href + '"' + '#' + '>';


                    //icon
                    if (typeof data.icon !== 'undefined') {
                        element += '<span class="icon ' + icon + '"></span> ';

                    }

                    element += data.text;

                    element += '<span style="max-width: 100px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';

                    element += '</a></li>';

                    return $(element);


                },
                get: function (key) {},
                set: function (key, value) {
                    var widget = this.widget;
                    if (widget) {
                        if (key === 'disabled') {
                            if (widget.toggleClass) {
                                widget.toggleClass('disabled', value);
                            }
                        }
                        if (key === 'icon') {
                            //console.log('set ' + key + ' ' + value + ' action ' + action.command);
                            var _iconNode = widget.find('.icon');
                            if(_iconNode) {
                                _iconNode.attr('class', 'icon');
                                this._lastIcon = this.icon;
                                this.icon = value;
                                _iconNode.addClass(value);
                            }else{
                                console.error('cant find icon node',widget);
                            }
                        }
                    }
                },
                action: function (e, data, menu) {
                    console.log('menu action', data);
                    return self.onRunAction(data.data,owner,e);
                    /*
                     console.log('menu action', data);
                     if (owner && owner.runAction && data.data) {
                     owner.runAction(data.data);
                     }
                     */
                },
                destroy: function () {
                    console.log('destroy');
                }
            }
            return item;
        },
        attach: function(selector,data){
            this.target = selector;
            this.menu = this.addContext(selector,data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;

            registry.add(this);

            //console.dir(this.menu);

            //this.menu.addClass('active');


            //this.menu.focus();
            return this.menu;
        },
        destroy: function(){
            try {
                if(this.domNode) {
                    registry.remove(this.domNode.id);
                }
                utils.destroy(this.domNode);
                this.inherited(arguments);
            }catch(e){
                logError(e);
            }
        },
        addReference:function(action,item){

            if (action.addReference) {
                action.addReference(item, {
                    properties: {
                        "value": true,
                        "disabled": true
                    }
                }, true);
            }
        }
    });
    //MenuMixinClass = _MenuMixin2;

    /**
     *
     * @class module:xide/widgets/MainMenuActionRenderer
     */
    var ActionRendererClass = dcl(null, {
        renderTopLevel: function (name, where) {

            where = where || $(this.getRootContainer());

            var item = $('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) + '<b class="caret"></b></a>' +
                '</li>');

            where.append(item);


            return item;

        },
        getRootContainer: function () {
            return this.navBar;
        },
        getActionData: function (action) {

            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label: actionVisibility.label != null ? actionVisibility.label : action.label,
                icon: actionVisibility.icon != null ? actionVisibility.icon : action.icon,
                visibility: actionVisibility
            }
        }
    });

    /**
     * @extends module:xide/widgets/MainMenuActionRenderer
     */
    var ContextMenu = dcl([_Widget.dcl, ActionContext.dcl, ActionMixin.dcl, ActionRendererClass,MenuMixinClass,_XWidget.StoreMixin], {
        target: null,
        openTarget:null,
        addContext: function (selector, data) {


            var id,
                $menu,
                self = this,
                target = this.openTarget ? (this.openTarget) : $(self.target);

            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = self.buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {

                var d = new Date();
                id = d.getTime();
                $menu = self.buildMenu(data, id);
                selector.append($menu);
            }

            //console.error('menu',$menu);




            /*
            $('.dropdown-menu a').bind('keydown', function (evt) {
                var $this = $(this);

                function select_previous () {
                    $this.parent('li').prev().find('a').focus();
                    evt.stopPropagation();
                }

                function select_next () {
                    $this.parent('li').next().find('a').focus();
                    evt.stopPropagation();
                }

                switch(evt.keyCode) {
                    case 13: // Enter key
                    case 32: // Space bar
                        $this.click();
                        $this.closest('.dropdown').removeClass('open');
                        evt.stopPropagation();
                        break;
                    case 9: // Tab key
                        if (evt.shiftKey) {
                            select_previous();
                        }
                        else {
                            select_next();
                        }
                        evt.preventDefault();
                        break;
                    case 38: // Up arrow
                        select_previous();
                        break;
                    case 40: // Down arrow
                        select_next();
                        break;
                }
            });
            */



            console.log('register context menu ',target);


            this.__on(target, 'contextmenu', null, function (e) {

                e.preventDefault();
                e.stopPropagation();
                var currentContextSelector = $(this);
                $('.dropdown-context:not(.dropdown-context-sub)').hide();

                var $dd = $('#dropdown-' + id);

                /*
                document.activeElement = null;

                setTimeout(function(){
                    console.error('asdfasfd');
                    self.openTarget.blur();
                    //99e.target.blur();
                    //99$dd.focus();
                },2000);

                */



                console.log('context menu',e);

                $dd.on('mouseenter',function(e){

                    //console.log('mouse enter ' + this.id, e.target);

                    //$(e.target).focus();
                    /*
                    var $this = $(this);
                    var first =$this.find('.dropdown-menu a:first');
                    first.focus();
                    console.log('mouse enter focus',first);
                    */

                    //var $parent  = getParent($this)
                    //var isActive = $parent.hasClass('open');

                })

                /*
                var first =$dd.find('li:first');
                console.log('focus: ',first);
                first.focus();*/


/*
                $('.dropdown-menu').bind('keydown', function (evt) {
                    var $this = $(this);
                    console.log('dropdown key');
                    switch(evt.keyCode) {
                        case 13: // Enter key
                        case 32: // Space bar
                        case 38: // Up arrow
                        case 40: // Down arrow
                            $this.addClass("open");
                            $this.find('.dropdown-menu a:first').focus();
                            break;
                        case 27: // Escape key
                            $this.removeClass("open");
                            $this.focus();
                            break;
                    }
                });

                $('.dropdown-menu a').bind('keydown', function (evt) {
                    console.log('key');
                    var $this = $(this);

                    function select_previous () {
                        $this.parent('li').prev().find('a').focus();
                        evt.stopPropagation();
                    }

                    function select_next () {
                        $this.parent('li').next().find('a').focus();
                        evt.stopPropagation();
                    }

                    switch(evt.keyCode) {
                        case 13: // Enter key
                        case 32: // Space bar
                            $this.click();
                            $this.closest('.dropdown').removeClass('open');
                            evt.stopPropagation();
                            break;
                        case 9: // Tab key
                            if (evt.shiftKey) {
                                select_previous();
                            }
                            else {
                                select_next();
                            }
                            evt.preventDefault();
                            break;
                        case 38: // Up arrow
                            select_previous();
                            break;
                        case 40: // Down arrow
                            select_next();
                            break;
                    }
                });

                var first =$dd.find('.dropdown-menu a:first');
                console.log('focus: ',first);
                first.focus();
*/

                $dd.find('.dynamic-menu-item').remove(); // Destroy any old dynamic menu items

                $dd.find('.dynamic-menu-src').each(function (idx, element) {
                    var menuItems = window[$(element).data('src')]($(selector));
                    $parentMenu = $(element).closest('.dropdown-menu.dropdown-context');
                    console.log('----');
                    $parentMenu = self.buildMenuItems($parentMenu, menuItems, id, undefined, true);
                });

                if (typeof options.above == 'boolean' && options.above) {
                    $dd.addClass('dropdown-context-up').css({
                        top: e.pageY - 20 - $('#dropdown-' + id).height(),
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                } else if (typeof options.above == 'string' && options.above == 'auto') {
                    $dd.removeClass('dropdown-context-up');
                    var autoH = $dd.height() + 0;
                    //console.log('autoh ')
                    var preferUp = true;
                    if ((e.pageY + autoH) > $('html').height() && preferUp) {

                        $dd.addClass('dropdown-context-up').css({
                            top: e.pageY - 20 - autoH,
                            left: e.pageX - 13
                        }).fadeIn(options.fadeSpeed);

                    } else {
                        $dd.css({
                            top: e.pageY + 10,
                            left: e.pageX - 13
                        }).fadeIn(options.fadeSpeed);
                    }
                }

                if (typeof options.left == 'boolean' && options.left) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width()
                    }).fadeIn(options.fadeSpeed);
                } else if (typeof options.left == 'string' && options.left == 'auto') {
                    $dd.removeClass('dropdown-context-left');
                    var autoL = $dd.width() - 12;
                    if ((e.pageX + autoL) > $('html').width()) {
                        $dd.addClass('dropdown-context-left').css({
                            left: e.pageX - $dd.width() + 13
                        });
                    }
                }
            });

            return $menu;
        },
        setActionStore: function (store, owner) {



            var self = this,
                allActions = store.query(),
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            this.store = store;



            //console.error(store.getSync(ACTION.SOURCE));


            self.wireStore(store,function(evt){
                if(evt.type==='update'){
                    var action = evt.target;
                    if(action.refreshReferences){
                        var refs = action.getReferences();
                        action.refreshReferences(evt.property,evt.value);
                    }
                }
            });


            //return all actions with non-empty tab field
            var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
            groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),
            //now flatten them
            _actionsFlattened = [];

            _.each(groupedTabs, function (items, name) {
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            var rootActions = [];
            _.each(tabbedActions, function (action) {
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });




            if (store.menuOrder) {
                rootActions = owner.sortGroups(rootActions, store.menuOrder);
            }

            // final menu data
            var data = [];

            //collect all existing actions as string array
            var allActionPaths = _.pluck(allActions, 'command');

            _.each(rootActions, function (level) {


                // collect all actions at level (File/View/...)
                var menuActions = owner.getItemsAtBranch(allActions, level);

                console.log('add level ' + level,menuActions);


                // convert action command strings to Action references
                var grouped = self.toActions(menuActions, store);

                // group all actions by group
                var groupedActions = _.groupBy(grouped, function (action) {
                    if (action && action.group) {
                        return action.group;
                    }
                });

                //temp array
                var _actions = [];
                _.each(groupedActions, function (items, level) {
                    if (level !== 'undefined') {
                        _actions = _actions.concat(items);
                    }
                });

                //flatten out again
                menuActions = _.pluck(_actions, 'command');

                var lastGroup = '';
                var lastHeader = {
                    header:''
                }



                _.each(menuActions, function (command) {

                    var action = self.getAction(command,store);

                    var isDynamicAction = false;

                    if (!action) {
                        isDynamicAction = true;
                        action = self.createAction(command);
                    }


                    if (action) {


                        var renderData = self.getActionData(action);
                        var icon = renderData.icon,
                            label = renderData.label,
                            visibility = renderData.visibility;


                        if (!isDynamicAction && action.group && groupedActions[action.group].length >= 2) {
                            if (lastGroup !== action.group) {
                                lastHeader = {header: i18.localize(action.group)};
                                data.push(lastHeader);
                                lastGroup = action.group;
                            }
                        }


                        var item = self.toMenuItem(action, owner, label, icon, visibility || {});



                        data.push(item);

                        visibility.widget = item;

                        self.addReference(action,item);

                        var childPaths = new Path(command).getChildren(allActionPaths, false),
                            isContainer = childPaths.length > 0,
                            childActions = isContainer ? self.toActions(childPaths,store) : null;

                        if (childActions) {
                            var subs = [];
                            _.each(childActions, function (child) {

                                var _renderData = self.getActionData(child);
                                var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility);
                                self.addReference(child,_item);
                                subs.push(_item);
                            });
                            item.subMenu = subs;
                        }
                    }
                });


            });

            var menu = self.attach($('body'), data);
            //console.dir(_.pluck(data,'text'));
        }
    });

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_ICON = types.ACTION_ICON,
        ribbon,
        CIS;




    function doTests(tab) {


        grid = FTestUtils.createFileGrid('root', {}, {}, 'TestGrid', module.id, true, tab);

        var menuNode = grid.header;
        var context = new ContextMenu({}, grid.domNode);
        context.openTarget = grid.domNode;
        context.init({preventDoubleContext: false});
        grid.resize();
        tab.add(context, null, false);
        //////////////////////////////////////////////////
        context._registerActionEmitter(grid);

        return;
        setTimeout(function(){
            var lAction = grid.getAction('View/Layout');
            var children = lAction.getChildren();
            var actionStore = grid.getActionStore();

            //var action = actionStore.getSync('View/Layout');
            //console.error('l',_.pluck(actionStore.query(),'command'));
            //console.error('l',lAction.getParent());
        },3000);
        return;
    }


    var ctx = window.sctx,
        root;

    if (ctx) {
        var parent = TestUtils.createTab(null, null, module.id);
        doTests(parent);
        return declare('a', null, {});

    }
    return _Widget;

});




