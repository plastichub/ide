define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/utils',
    "xide/tests/TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xfile/tests/TestUtils',
    "xide/lodash",
    "xide/widgets/TemplatedWidgetBase",
    'xace/views/Editor'
], function (declare, dcl, utils, TestUtils, _Widget, module, FTestUtils, _, TemplatedWidgetBase,Editor) {
    console.clear();

    var StartFile = "./_indexTest.md";
    var MarkdownFileGrid = {
        getColumns: function () {
            var thiz = this;
            this.columns = [];
            function createColumn(label, field, sortable, hidden) {
                if (thiz._columns[label] != null) {
                    hidden = !thiz._columns[label];
                }
                thiz.columns.push({
                    renderExpando: label === 'Name',
                    label: label,
                    field: field,
                    sortable: sortable,
                    formatter: function (value, obj) {
                        var parts = value.split('_');
                        //strip "01_" from path
                        value = parts.length ? parts[parts.length - 1] : value;
                        //string file extension
                        value = utils.basename(value, '.md');
                        value = utils.basename(value, '.MD');
                        return thiz.formatColumn(field, value, obj);
                    },
                    hidden: hidden
                });

            }

            createColumn('Name', 'name', true, false);
            return this.columns;
        }
    };


    var MarkdownView = dcl(TemplatedWidgetBase, {
        templateString: "<div class='widget container MarkdownView'>" +
            "<div class='Page' attachTo='markdown'></div>"+
        "</div>",
        resizeToParent: true
    });

    var FileItem = null;

    function resolveLink(match,url){}
    /**
     *
     * @param match
     * @param url
     * @param item
     * @returns {*}
     */
    function resolveImage(match,url,item){
        if(!url.toLowerCase().includes('http') && !url.includes('https')){
            return sctx.getFileManager().getImageUrl({
                mount:'docs',
                path:url
            });
        }
        return url;
    }

    /**
     * showdown extension to fix links since we're rendering markdown files from a VFS
     */
    function registerShowDown(){
        showdown.extension('xcf', function () {
            return [
                ///////////////////////////////////////////////////////////////
                //
                //      After parsed by showdown
                //
                {
                    type: 'output',
                    filter: function (text, converter, options) {
                        //var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
                        //links
                        var expression =/href="([^\'\"]+)/g;
                        var urlRegex = new RegExp(expression);
                        text.replace(urlRegex, function (match, url, rest) {
                            console.log('link : '+url);
                        });
                        //console.log('text: ',text);
                        return text;
                    }
                },
                ///////////////////////////////////////////////////////////////
                //
                //      Before parsed: fix image links
                //
                {
                    type: 'lang', //or output
                    filter: function (text, converter, options) {

                        //images
                        var inlineImageRegExp    = /!\[(.*?)]\s?\([ \t]*()<?(\S+?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(['"])(.*?)\6[ \t]*)?\)/g,
                            referenceImageRegExp = /!\[([^\]]*?)] ?(?:\n *)?\[(.*?)]()()()()()/g;

                        function writeImageTag (wholeMatch, altText, linkId, url, width, height, m5, title) {

                            var gUrls   = [],
                                gTitles = [],
                                gDims   = [];

                            linkId = linkId.toLowerCase();

                            if (!title) {
                                title = '';
                            }

                            if (url === '' || url === null) {
                                if (linkId === '' || linkId === null) {
                                    // lower-case and turn embedded newlines into spaces
                                    linkId = altText.toLowerCase().replace(/ ?\n/g, ' ');
                                }
                                url = '#' + linkId;

                                if (!showdown.helper.isUndefined(gUrls[linkId])) {
                                    url = gUrls[linkId];
                                    if (!showdown.helper.isUndefined(gTitles[linkId])) {
                                        title = gTitles[linkId];
                                    }
                                    if (!showdown.helper.isUndefined(gDims[linkId])) {
                                        width = gDims[linkId].width;
                                        height = gDims[linkId].height;
                                    }
                                } else {
                                    return wholeMatch;
                                }
                            }

                            altText = altText.replace(/"/g, '&quot;');
                            altText = showdown.helper.escapeCharacters(altText, '*_', false);
                            url = showdown.helper.escapeCharacters(url, '*_', false);
                            url = resolveImage(null,url);
                            var result = '<img src="' + url + '" alt="' + altText + '"';

                            if (title) {
                                title = title.replace(/"/g, '&quot;');
                                title = showdown.helper.escapeCharacters(title, '*_', false);
                                result += ' title="' + title + '"';
                            }

                            if (width && height) {
                                width  = (width === '*') ? 'auto' : width;
                                height = (height === '*') ? 'auto' : height;

                                result += ' width="' + width + '"';
                                result += ' height="' + height + '"';
                            }

                            result += ' />';

                            //console.log('result : ' + result);

                            return result;
                        }
                        // First, handle reference-style labeled images: ![alt text][id]
                        text = text.replace(inlineImageRegExp, writeImageTag);

                        // Next, handle inline images:  ![alt text](url =<width>x<height> "optional title")
                        text = text.replace(referenceImageRegExp, writeImageTag);

                        return text;
                    },
                    regex: /foo/g, // if filter is present, both regex and replace properties will be ignored
                    replace: 'bar'
                }
            ];
        });
    }

    /**
     * Render markdown page
     * @param fileItem {module:xfile/model/File}
     * @param where {module:xide/widgets/_Widget}
     * @param others {module:xfile/model/File[]|null} other items in the same folder. Used to render some TOC
     */
    function renderMarkdown(fileItem, where, others) {
        var ctx = this.ctx;
        var fileManager = ctx.getFileManager();
        var self = this;

        fileManager.getContent(fileItem.mount, fileItem.path, function (content) {
            FileItem = self.fileItem = fileItem;

            /////////////////////////////////////////////////////////////////////
            //
            //  Preview/Showdown update
            //
            if (typeof showdown !== 'undefined') {

                var converter = self.converter;
                if(!converter) {
                    self.converter = converter = new showdown.Converter({
                        github_flavouring: true,
                        extensions: ['xcf']
                    });
                    converter.setOption('tables', true);
                }
                var markdown = converter.makeHtml(content);
                if(!self.lastPreview) {
                    self.lastPreview = utils.addWidget(MarkdownView, null, self, where, true);
                    where.add(self.lastPreview);
                }
                self.lastPreview.$markdown.html(markdown);


                /////////////////////////////////////////////////////////////////////
                //
                //  apply code highlighting
                //
                if(typeof hljs!=='undefined') {
                    self.lastPreview.$markdown.find('code').each(function (i, block) {
                        hljs.highlightBlock(block);
                    });
                }else{
                    console.warn('have no highlight.js');
                }


                var aceEditor = self.editor;
                var isSettingValue = true;
                /////////////////////////////////////////////////////////////////////
                //
                //  Create/Update ACE editor
                //
                if(!self.__bottom) {

                    var bottom = self.__bottom = self.getBottomPanel(false, 0.5,'DefaultTab',null,self.__right);
                    bottom.getSplitter().pos(0.5);
                    var snippetManager = ace.require('ace/snippets').snippetManager;
                    var EditorClass = dcl(Editor,{
                        runAction:function(action){
                            var editor = this.editor;
                            var selectedText = editor.session.getTextRange(editor.getSelectionRange());
                            if(action.command==='Markdown/Bold'){
                                if (selectedText === '') {
                                    snippetManager.insertSnippet(editor, '**${1:text}**');
                                } else {
                                    snippetManager.insertSnippet(editor, '**' + selectedText + '**');
                                }
                            }
                            if(action.command==='Markdown/Italic'){
                                if (selectedText === '') {
                                    snippetManager.insertSnippet(editor, '*${1:text}*');
                                } else {
                                    snippetManager.insertSnippet(editor, '*' + selectedText + '*');
                                }
                            }
                            if(action.command==='Markdown/Link'){
                                if (selectedText === ''){
                                    snippetManager.insertSnippet(editor, '[${1:text}](http://$2)');
                                } else {
                                    snippetManager.insertSnippet(editor, '[' + selectedText + '](http://$1)');
                                }
                            }
                            editor.focus();
                            return this.inherited(arguments);
                        }
                    });

                    dcl.chainAfter('runAction',EditorClass);

                    self.editor = aceEditor = utils.addWidget(EditorClass,{
                        //value:content,
                        value:"",
                        item:fileItem,
                        options:{
                            mode:'markdown',
                            fileName:fileItem.path
                        },
                        storeDelegate:{
                            getContent:function(onSuccess,_item){
                                var file = _item || fileItem;
                                return self.ctx.getFileManager().getContent(file.mount,file.path,onSuccess);
                            },
                            saveContent:function(value,onSuccess,onError){
                                return self.ctx.getFileManager().setContent(fileItem.mount,fileItem.path,value,onSuccess);
                            }
                        }
                    },this,bottom,false,null,null);

                    aceEditor._on('onAddActions',function(evt){
                        var actions = evt.actions;
                        var mixin = {
                            addPermission:true
                        };
                        actions.push(aceEditor.createAction({
                            label: 'Bold',
                            command: 'Markdown/Bold',
                            icon: 'fa-bold',
                            group: 'Text',
                            tab:'Home',
                            mixin:mixin
                        }));

                        actions.push(aceEditor.createAction({
                            label: 'Italic',
                            command: 'Markdown/Italic',
                            icon: 'fa-italic',
                            group: 'Text',
                            tab:'Home',
                            mixin:mixin
                        }));

                        actions.push(aceEditor.createAction({
                            label: 'Link',
                            command: 'Markdown/Link',
                            icon: 'fa-link',
                            group: 'Text',
                            tab:'Home',
                            mixin:mixin
                        }));


                    },self);

                    aceEditor.startup();
                    aceEditor._on('change',function(val){
                        if(isSettingValue){
                            return;
                        }
                        if(val) {
                            var markdown = converter.makeHtml(val);
                            self.lastPreview.$markdown.html(markdown);
                        }
                    });
                    bottom.add(aceEditor);
                }
                isSettingValue = true;
                aceEditor && aceEditor.set('value',content);
                isSettingValue = false;
                where.resize();
            } else {
                console.warn('have no `showdown`, proceed without');
            }
        });
    }

    function gridReady() {

        registerShowDown();

        var right = this.getRightPanel(null, null, 'DefaultTab', {});
        right.closeable(false);
        right.getSplitter().pos(0.3);

        var self = this;
        var collection = self.collection;

        //  - auto render item
        //  - auto render a selected folder's _index.md
        this._on('selectionChanged', function (evt) {
            var selection = evt.selection;
            if (!selection || !selection[0]) {
                return;
            }

            var item = selection[0];
            if (item.isDir) {
                collection.open(item).then(function (items) {
                    //folder contains a standard _index.md, render it!
                    var _index = _.find(items, {
                        name: '_index.md'
                    });
                    if (_index) {
                        renderMarkdown.apply(self, [_index, right, items.filter(function (file) {
                            return file != _index;
                        })]);
                    }
                });
                return;
            }
            renderMarkdown.apply(self, [item, right]);
        });
        this._showHeader(false);

        //pre-select first index item;
        this.select([StartFile], null, true, {
            append: false,
            focus: true,
            delay: 1
        });
    }
    var excludes = ["internal","assets","modules","index"];



    function doTests(tab) {

        var grid = FTestUtils.createFileGrid('docs', {}, MarkdownFileGrid, 'TestGrid', module.id, false, tab, {}, {
            micromatch: "(*.md)|(*.MD)|!(*.*)",// Only folders and markdown files
            //recursive:true,
            _onAfterSort:function(data){
                data = data.filter(function(item){
                    return !excludes.includes(item.name);
                });
                return data;
            }
        });

        grid.refresh().then(function () {
            gridReady.apply(grid);
        });
    }
    var ctx = window.sctx;
    if (ctx) {
        var parent = TestUtils.createTab(null, null, module.id);
        doTests(parent);
        return declare('a', null, {});
    }
    return _Widget;
});

