/** @module xide/tests/TestMainMenu **/

define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xfile/tests/TestUtils',
    'module',
    'xide/widgets/ActionToolbar',
    'xide/action/ActionContext',
    'dijit/Toolbar',
    'dijit/ToolbarSeparator',
    'xide/widgets/ActionToolbarMixin',
    "xide/mixins/ActionMixin",
    "xide/mixins/EventedMixin",
    "xide/widgets/_MenuMixin",
    "xide/widgets/TemplatedWidgetBase",
    'xide/widgets/ActionToolbarButton',
    'dijit/Menu',
    'dijit/MenuItem',
    'xide/data/Reference',
    'dijit/form/DropDownButton',
    'dojo/Stateful',
    'dijit/PopupMenuItem',
    'dijit/MenuBar',
    'dojo/dom-construct',

    'xide/action/ActionStore',

    'xide/model/Path',
    'xide/action/Action'

], function (declare,types,utils,



             XFileTestUtils,module,ActionToolbar,ActionContext,

             Toolbar,ToolbarSeparator,ActionToolbarMixin, ActionMixin,EventedMixin,_MenuMixin,TemplatedWidgetBase,

             ActionToolbarButton,

             Menu, MenuItem, Reference,

             DropDownButton,

             Stateful,PopupMenuItem,MenuBar,

             domConstruct,

             ActionStore,

             Path,

             Action

) {


    console.clear();









    var _Path = declare("davinci.model.Path", null, {

        /**
         * @class davinci.model.Path
         * @constructor
         */
        constructor: function (path, hasLeading, hasTrailing) {
            path = path || '.';  // if empty string, use '.'
            hasLeading = false;
            hasTrailing = false;
            if (typeof path == 'string') {
                this.path = path;
                this.getSegments();
            } else {
                this.segments = path;
                this.hasLeading = hasLeading;
                this.hasTrailing = hasTrailing;
            }
        },

        endsWith: function (tail) {
            var segments = dojo.clone(this.segments);
            var tailSegments = (new Path(tail)).getSegments();
            while (tailSegments.length > 0 && segments.length > 0) {
                if (tailSegments.pop() != segments.pop()) {
                    return false;
                }
            }
            return true;
        },

        getExtension: function () {
            if (!this.extension) {
                this.extension = this.path.substr(this.path.lastIndexOf('.') + 1);
            }
            return this.extension;
        },

        segment: function (index) {
            var segs = this.getSegments();
            if (segs.length < index) return null;
            return segs[index];
        },

        getSegments: function () {
            if (!this.segments) {
                var path = this.path;
                this.segments = path.split('/');
                if (path.charAt(0) == '/') {
                    this.hasLeading = true;
                }
                if (path.charAt(path.length - 1) == '/') {
                    this.hasTrailing = true;
                    // If the path ends in '/', split() will create an array whose last element
                    // is an empty string. Remove that here.
                    this.segments.pop();
                }
                this._canonicalize();
            }
            return this.segments;
        },

        isAbsolute: function () {
            return this.hasLeading;
        },

        getParentPath: function () {
            if (!this._parentPath) {
                var parentSegments = dojo.clone(this.segments);
                parentSegments.pop();
                this._parentPath = new Path(parentSegments, this.hasLeading);
            }
            return dojo.clone(this._parentPath);
        },

        _clone: function () {
            return new Path(dojo.clone(this.segments), this.hasLeading, this.hasTrailing);
        },

        append: function (tail) {
            tail = tail || "";
            if (typeof tail == 'string') {
                tail = new Path(tail);
            }
            if (tail.isAbsolute()) {
                return tail;
            }
            var mySegments = this.segments;
            var tailSegments = tail.getSegments();
            var newSegments = mySegments.concat(tailSegments);
            var result = new Path(newSegments, this.hasLeading, tail.hasTrailing);
            if (tailSegments[0] == ".." || tailSegments[0] == ".") {
                result._canonicalize();
            }
            return result;
        },

        toString: function () {
            var result = [];
            if (this.hasLeading) {
                result.push('/');
            }
            for (var i = 0; i < this.segments.length; i++) {
                if (i > 0) {
                    result.push('/');
                }
                result.push(this.segments[i]);
            }
            if (this.hasTrailing) {
                result.push('/');
            }
            return result.join("");
        },

        removeRelative: function () {
            var segs = this.getSegments();
            if (segs.length > 0 && segs[1] == ".")
                return this.removeFirstSegments(1);
            return this;
        },

        relativeTo: function (base, ignoreFilename) {
            if (typeof base == 'string') {
                base = new Path(base);
            }
            var mySegments = this.segments;
            if (this.isAbsolute()) {
                return this;
            }
            var baseSegments = base.getSegments();
            var commonLength = this.matchingFirstSegments(base);
            var baseSegmentLength = baseSegments.length;
            if (ignoreFilename) {
                baseSegmentLength = baseSegmentLength - 1;
            }
            var differenceLength = baseSegmentLength - commonLength;
            var newSegmentLength = differenceLength + mySegments.length - commonLength;
            if (newSegmentLength == 0) {
                return davinci.model.Path.EMPTY;
            }
            var newSegments = [];
            for (var i = 0; i < differenceLength; i++) {
                newSegments.push('..');
            }
            for (var i = commonLength; i < mySegments.length; i++) {
                newSegments.push(mySegments[i]);
            }
            return new Path(newSegments, false, this.hasTrailing);
        },

        startsWith: function (anotherPath) {
            var count = this.matchingFirstSegments(anotherPath);
            return anotherPath._length() == count;
        },

        _length: function (anotherPath) {
            return this.segments.length;
        },

        matchingFirstSegments: function (anotherPath) {
            var mySegments = this.segments;
            var pathSegments = anotherPath.getSegments();
            var max = Math.min(mySegments.length, pathSegments.length);
            var count = 0;
            for (var i = 0; i < max; i++) {
                if (mySegments[i] != pathSegments[i]) {
                    return count;
                }
                count++;
            }
            return count;
        },

        removeFirstSegments: function (count) {
            return new Path(this.segments.slice(count, this.segments.length), this.hasLeading, this.hasTrailing);
        },

        removeMatchingLastSegments: function (anotherPath) {
            var match = this.matchingFirstSegments(anotherPath);
            return this.removeLastSegments(match);
        },

        removeMatchingFirstSegments: function (anotherPath) {
            var match = this.matchingFirstSegments(anotherPath);
            return this._clone().removeFirstSegments(match);
        },

        removeLastSegments: function (count) {
            if (!count) {
                count = 1;
            }
            return new Path(this.segments.slice(0, this.segments.length - count), this.hasLeading, this.hasTrailing);
        },

        lastSegment: function () {
            return this.segments[this.segments.length - 1];
        },

        firstSegment: function (length) {
            return this.segments[length || 0];
        },

        equals: function (anotherPath) {
            if (this.segments.length != anotherPath.segments.length) {
                return false;
            }
            for (var i = 0; i < this.segments.length; i++) {
                if (anotherPath.segments[i] != this.segments[i]) {
                    return false;
                }
                ;
            }
            return true;
        },

        _canonicalize: function () {

            var doIt;
            var segments = this.segments;
            for (var i = 0; i < segments.length; i++) {
                if (segments[i] == "." || segments[i] == "..") {
                    doIt = true;
                    break;
                }
            }
            if (doIt) {
                var stack = [];
                for (var i = 0; i < segments.length; i++) {
                    if (segments[i] == "..") {
                        if (stack.length == 0) {
                            // if the stack is empty we are going out of our scope
                            // so we need to accumulate segments.  But only if the original
                            // path is relative.  If it is absolute then we can't go any higher than
                            // root so simply toss the .. references.
                            if (!this.hasLeading) {
                                stack.push(segments[i]); //stack push
                            }
                        } else {
                            // if the top is '..' then we are accumulating segments so don't pop
                            if (".." == stack[stack.length - 1]) {
                                stack.push("..");
                            } else {
                                stack.pop();
                            }
                        }
                        //collapse current references
                    } else if (segments[i] != "." || this.segments.length == 1) {
                        stack.push(segments[i]); //stack push
                    }
                }
                //if the number of segments hasn't changed, then no modification needed
                if (stack.length == segments.length) {
                    return;
                }
                this.segments = stack;
            }
        }

    });

    var viewLayout = new Path('View/Layout');
    var viewLayoutList = new Path('View/Layout/List');
    var viewLayoutThumb = new Path('View/Layout/Thumb');
    var viewItems = [
        'View/Layout',
        'View/Layout/List',
        'View/Layout/Thumb',
        'View/Columns',
        'View/Columns/Show Name',
        'View/Columns/Show Media',
        'View/Source',
        'View/Show/Header',
        'View/Show/Toolbar',
        'View/Show/Statusbar'
    ];




    function getChildren(path,items,recursive){

        var result = [];

        var root = new Path(path);

        function addChild(child){

            var _path = typeof child !=='string' ? child.toString() : child;
            if(!result.includes(_path)){
                result.push(_path);
            }
        }

        _.each(items,function(item){

            var child = new Path(item);
            var diff = child.relativeTo(path);

            //root match
            if(child.startsWith(root)){

                if(recursive){
                    addChild(child.toString());
                }else{

                    var diffSegments = diff.getSegments();

                    //direct child
                    if(diffSegments.length==1){
                        addChild(child);
                    }else if(diffSegments.length>1){

                        //make sure that its parent has been added:
                        var parent = child.getParentPath();
                        var parentDiff = parent.relativeTo(path);
                        //check diff again
                        if(parentDiff.getSegments().length==1){
                            addChild(parent.toString());
                        }
                    }

                    //var relativePart = child.relativeTo(path);
                    //console.log('relative parth ' + relativePart.toString());

                    /*
                    //  that matches 'View/Layout" && "View/Columns" &&
                    if(child.getSegments().length==2){
                        result.push(child.toString());

                    }else if(child.getSegments().length>2){

                        //child 'View/Show/Toolbar'
                        console.log('   child segs > 2 ' + child.toString() + ' parent = ' +child.getParentPath());

                    }
                    */

                }

            }

        });


        return result;

    }

    function debugActions(items){
        _.each(items,function(action){
            console.log('\t' + action.command);
        });

    }


    /***
     *
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;


    function createMainMenuClass(){

        console.clear();


        return declare("xide/widgets/MainMenu", [TemplatedWidgetBase, ActionMixin, _MenuMixin,ActionContext], {

            _debug:false,
            _renderMap:null,
            _permanentActionStore:null,
            getPermanentActionStore:function(){

                if(!this._permanentActionStore){

                    this._permanentActionStore = new ActionStore({

                        data:[],
                        observedProperties:[
                            "value"
                        ],
                        tabOrder:this.tabOrder,
                        groupOrder:this.groupOrder,
                        tabSettings:this.tabSettings,
                        menuOrder:this.menuOrder
                    });

                }
                return this._permanentActionStore;
            },
            _clear:function(){
                this.clearActions();
                _.each(this._renderMap,function(what,name){
                    utils.destroy([what.menu,what.popup]);
                });
            },
            renderAction: function (action, where, widgetClass, showLabel,actions) {

                //console.log('render action ' + action.command + ' root = ' + action.getRoot());
                var thiz = this,

                    parentAction = action.getParent() /*|| new Action({command:action.getParentCommand()}))*/,
                    //parentActionItems = this.getItemsAtBranch(actions,parentAction.command),
                    _items = action.getChildren(),
                    _hasItems = _items && _items.length,
                    isContainer = _hasItems,
                    _expand = thiz.getVisibilityField(action, 'expand') == true,
                    widget = thiz.getVisibilityField(action, 'widget'),
                    parentWidget = parentAction ? thiz.getVisibilityField(parentAction, 'widget') : null,
                    actionVisibility = action.getVisibility(this.visibility),
                    customWidget  = actionVisibility.widgetClass;

                if(!widget && parentAction && parentWidget){
                    thiz._renderActions([action], parentWidget, null, null, null, true);
                }
            },
            getItemsAtBranch:function(items, path) {
                return new Path(path).getChildren(_.pluck(items,'command'),false);
            },
            /**
             * Implement action parent widget creation.
             * @param action
             * @param where
             * @returns {*}
             * @private
             */
            __createActionParentWidget:function(action,where){

                var menu = new Menu({parentMenu: where}),
                    self = this,
                    widgetArgs = {
                        label:action.label,
                        popup:menu,
                        iconClass:action.icon ||'fa-magic',
                        item:action
                    },
                    _extra = self.getVisibilityField(action,'widgetArgs');

                self.setVisibilityField(action,'widget',menu);

                _extra && utils.mixin(widgetArgs,_extra);

                var popup = new PopupMenuItem(widgetArgs);

                menu.dropDown = popup;

                where.addChild(popup);

                self._publishActionWidget(menu,action,where);

                return menu;


            },
            _onOpenMenu: function (menuItem) {

                if(menuItem.__rendered){
                    //console.log('already renderered');
                    //return;
                }

                console.log('on open');

                menuItem.__rendered = true;


                var path = menuItem.item.label,
                    thiz = this,

                    permanentActionStore = this.getPermanentActionStore(),
                    permanentActions = permanentActionStore.query(),

                    store = this.getActionStore(),
                    actions = store.query().concat(permanentActions),

                    //return all actions with non-empty tab field
                    allActions = actions.filter(function (action) {
                        return action.tab != null;
                    }),
                    //build array of action's command
                    allActionPaths = _.pluck(allActions,'command'),
                    //all actions at #path
                    menuActions = thiz.getItemsAtBranch(allActions,path);

                //return an action from both stores
                function getAction(command){
                    return store.getSync(command) || permanentActionStore.getSync(command);
                }

                //command strings to actions
                function toActions(paths){
                    var result = [];
                    _.each(paths,function(path){
                        result.push(getAction(path));
                    });
                    return result;
                }

                //find widget by action, needed since we use fake actions
                function findWidget(where,action){
                    var children = where.getChildren();
                    for (var i = 0; i < children.length; i++) {
                        var child = children[i];
                        if(child.item && child.item.command == action.command){
                            return child;
                        }
                    }
                    return null;
                }


                //render action
                function render(path) {


                    var action = getAction(path);
                    if (!action) {

                        var segments = path.split('/');
                        var lastSegment = segments[segments.length - 1];
                        action = new Action({
                            command: path,
                            label:lastSegment
                        });

                    }


                    var widget = thiz.getVisibilityField(action, 'widget'),
                        childPaths = new Path(path).getChildren(allActionPaths,false),
                        isContainer = childPaths.length> 0,
                        childActions = isContainer ? toActions(childPaths) : null ;



                    if(!widget){
                        widget = findWidget(menuItem,action);
                    }

                    if(!widget) {

                        if (isContainer) {
                            if (thiz._renderMap[path]) {
                                //console.error('already rendered ' + path);
                                //return;
                            }


                            var menu = thiz.__createActionParentWidget(action, menuItem);
                            thiz._renderActions(childActions, menu, null, null, null, true);
                            thiz._renderMap[path] = menu;

                        } else {

                            var _action = getAction(path);

                            if (_action) {
                                var _widget = thiz.getVisibilityField(_action, 'widget');
                                if (_widget) {
                                    console.warn('already renderer');
                                } else {

                                    thiz._debug && console.log('render action ' + _action.command);
                                    thiz._renderAction(_action, menuItem);
                                }

                            } else {
                                console.error('cant find action at ' + path);
                            }
                        }
                    }

                    return;
                }


                _.each(menuActions,render);

            },
            ///////////////////////////////////////////////////////////////////////
            //
            //  Implement action interface
            //
            ///////////////////////////////////////////////////////////////////////
            /**
             *
             * @param store module:xide/action/ActionStore
             */
            setActionStore:function(store){

                this._clear();


                this.store = store;
                this._renderMap = {};
                //console.log('main menu: set store');

                var self = this,
                    permanentActionStore = this.getPermanentActionStore(),
                    permanentActions = permanentActionStore.query(),
                    allActions = store.query().concat(permanentActions),

                //return all actions with non-empty tab field
                    tabbedActions = allActions.filter(function (action) {
                        return action.tab != null;
                    }),

                //group all tabbed actions : { Home[actions], View[actions] }
                    groupedTabs = _.groupBy(tabbedActions, function (action) {
                        return action.tab;
                    }),

                //now flatten them
                    _actionsFlattened = [];




                _.each(groupedTabs,function(items,name){
                    _actionsFlattened = _actionsFlattened.concat(items);
                    //console.log('group : ' + name,items);
                });

                //@type {string []} | example : ['File','Edit','View']
                var rootActions = [];
                _.each(tabbedActions,function(action){
                    var rootCommand = action.getRoot();
                    !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
                });

                if(self._debug){
                    _.each(tabbedActions,function(action) {
                        console.log('   action ' + action.command);
                    });
                }


                if (store.menuOrder) {
                    rootActions = this.sortGroups(rootActions, store.menuOrder);
                }

                var rootItems = [];


                _.each(rootActions,function(level){

                    //now add a root item
                    var item = self._addLevel({
                        label:level
                    }, self.rootMenu);

                    rootItems.push(item);

                    self._renderMap[level] = item;

                });

            },
            /**
             * The visibility filter. There an event "REGISTER_ACTION" this class is listening. Actions have a visibility
             * mask and this field will reject those actions which don't have this mask
             * @member visibility {module:xide/types/ACTION_VISIBILITY}
             */
            visibility: types.ACTION_VISIBILITY.MAIN_MENU,
            /**
             *
             */
            templateString: "<div><div data-dojo-attach-point='rootMenu' data-dojo-type='dijit/MenuBar' class='border-bottom-dark'></div></div>",

            /**
             * The class being used to render an action.
             *
             * @type {dijit/_Widget}
             * @member
             */
            widgetClass: MenuItem,
            /**
             * Root level action widget, this is the menubar in our case
             */
            rootMenu: null,
            sortGroups: function (groups, groupMap) {
                groups = groups.sort(function (a, b) {
                    if (groupMap[a] != null && groupMap[b] != null) {
                        var orderA = groupMap[a];
                        var orderB = groupMap[b];
                        return orderB - orderA;
                    }
                    return 100;
                });
                return groups;
            },
            sortGroups2: function (groups, groupMap) {
                groups = groups.sort(function (a, b) {
                    if (a.label && b.label && groupMap[a.label] != null && groupMap[b.label] != null) {
                        var orderA = groupMap[a.label];
                        var orderB = groupMap[b.label];
                        return orderB - orderA;
                    }
                    return 100;
                });
                return groups;
            },
            startup: function () {
                this.inherited(arguments);
                this.rootMenu.dirty = true;
            },
            destroy: function () {
                this._clear();
                this.store = null;
                delete this._renderMap;
                this._renderMap = null;
                this.inherited(arguments);
            }

        });

    }


    function createBootstrapMainMenuClass(){

        var breadCrumbClass = declare('breadcrumb',[TemplatedWidgetBase], {

            buildRendering: function () {
                this.inherited(arguments);
            },
            templateString: "<div style='display: block'>" +

            "<nav style='min-height: 30px;display: block' class='navbar navbar-inverse' role='navigation'>"+

            "<div class='collapse navbar-collapse'>"+
                "<ul class='nav navbar-nav'>"+
                "<li><a href='#'>Home</a></li>"+
            "<li><a href='#'>Profile</a></li>"+
            "<li class='dropdown'>"+
            "<a href='#' data-toggle='dropdown' class='dropdown-toggle'>Messages <b class='caret'></b></a>"+
                "<ul class='dropdown-menu bg-opaque'>"+
                    "<li><a href='#'>Inbox</a></li>"+
                    "<li><a href='#'>Drafts</a></li>"+
                    "<li><a href='#'>Sent Items</a></li>"+
                    "<li class='divider'></li>"+
                    "<li><a href='#'>Trash</a></li>"+
                "</ul>"+
            "</li>"+
            "</ul>"+
            "</nav>"+
            "</div>"+
            "</div>"
        });

        return breadCrumbClass;
    }


    function createBoostrapMenu(grid){


        utils.destroy(window['mainMenu']);


        var mainView = ctx.mainView;


        if(mainView.mainMenu){
            utils.destroy(mainView.mainMenu);
        }

        mainView.mainMenu = utils.addWidget(createBootstrapMainMenuClass(), {}, this, mainView.layoutTop, true);

        domConstruct.place(mainView.mainMenu.domNode, mainView.layoutTop.containerNode, 'first');

        window['mainMenu'] = mainView.mainMenu;


    }





    function openMenu(){

        var menu = window['mainMenu'] || ctx.mainMenu;
        if(menu){

            var _menu = menu.rootMenu;
            var children = _menu.getChildren();
            if(children){
                var _firstItem = children[0];


                _menu.onItemClick(_firstItem, {});

                var popup = _firstItem.popup;
                //popup.openDropDown();
                //_firstItem.openDropDown();
            }
            console.log('have menu2',[_menu,children]);
        }
    }





    function getPermanentActions(store){


        var result = [];



        result.push(this.createAction('Save2','File/Permanent','fa-save',['f9'],'Home','File',"global",
            //onCreate
            function(action){

                /*
                action.setVisibility(types.ACTION_VISIBILITY.ALL, {
                    show:false
                });
                */
            },
            //handler
            function(){

                console.log('run handler');

                openMenu();

            },
            {
                addPermission:true,
                show:true
            },null,null,[
                'File/Permanent'
            ],document,this
        ));



        return result;

    }

    function createMainMenu(grid){



        utils.destroy(window['mainMenu']);


        var mainView = ctx.mainView;

        if(mainView.mainMenu){
            utils.destroy(mainView.mainMenu);
        }

        var permanentActionStore = ctx.getActionStore();
        ctx.clearActions();

        var _actions = permanentActionStore.query();






        var permanentActions = getPermanentActions.apply(ctx,permanentActionStore);

        ctx.addActions(permanentActions);


        mainView.mainMenu = utils.addWidget(createMainMenuClass(), {

            _permanentActionStore:ctx.getActionStore()

        }, this, mainView.layoutTop, true);

        domConstruct.place(mainView.mainMenu.domNode, mainView.layoutTop.containerNode, 'first');

        window['mainMenu'] = mainView.mainMenu;


        mainView.mainMenu.addActionEmitter(grid);
        mainView.mainMenu.setActionEmitter(grid);


    }


    function doTests(grid){



        //createBoostrapMenu(grid);
        createMainMenu(grid);

        grid.deselectAll();

        grid.select([2],null,true,{
            focus:true,
            append:false
        });
    }

    function testMain(grid,panel){

        grid.refresh().then(function(){

            setTimeout(function(e){
                doTests(grid);
            },1000);
        });
    }





    if (ctx) {


        var doTest = true;

        if (doTest) {

            var ACTION = types.ACTION;

            var grid = XFileTestUtils.createFileGrid('root',
                //args
                {

                    _columns: {
                        "Name": true,
                        "Path": false,
                        "Size": false,
                        "Modified": false,
                        "Owner":false
                    },
                    __permissions: [
                        ACTION.EDIT,
                        /*
                        ACTION.EDIT,
                        ACTION.RENAME,
                        ACTION.DOWNLOAD,
                        ACTION.COLUMNS,
                        ACTION.GO_UP,
                        ACTION.CLIPBOARD
                        */
                        //ACTION.COLUMNS,
                        ACTION.LAYOUT,
                        ACTION.COLUMNS,
                        ACTION.TOOLBAR
                        //ACTION.GO_UP,
                        //ACTION.NEW_FILE,
                        //ACTION.NEW_DIRECTORY
                    ],
                    tabOrder: {
                        'Home': 100,
                        'View': 50,
                        'Settings': 20,
                        'Navigation': 10
                    },
                    menuOrder: {
                        'File': 110,
                        'Edit': 100,
                        'View': 50,
                        'Settings': 20,
                        'Navigation': 10
                    },
                    tabSettings: {
                        'Step': {
                            width:'190px'
                        },
                        'Show': {
                            width:'130px'
                        },
                        'Settings': {
                            width:'100%'
                        }
                    },
                    groupOrder: {
                        'Clipboard': 110,
                        'File': 100,
                        'Step': 80,
                        'Open': 70,
                        'Organize': 60,
                        'Insert': 10,
                        'Select': 5,
                        'Navigation':4
                    }
                },


                //overrides
                {

                },'TestGrid',module.id,true);



            function test() {
                testMain(grid,grid._parent);
            }

            setTimeout(function () {
                test();
            }, 1000);
        }
    }

    return declare('maeh',null,{});

});
