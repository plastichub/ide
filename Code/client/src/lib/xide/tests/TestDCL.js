/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/views/CIGroupedSettingsView',
    'xide/views/_CIDialog',
    'xide/widgets/WidgetBase',
    'xide/widgets/_Widget',
    'xide/widgets/_CacheMixin',
    'xide/widgets/ActionToolbar',
    'xide/action/ActionModel',
    'xaction/ActionProvider',
    "dstore/Memory",
    'xide/widgets/ExpressionsGridView',
    'xide/tests/TestUtils',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'xide/views/_LayoutMixin',
    'xide/action/ActionStore',
    'module'

], function (dcl,declare, types,
             utils, Grid, factory,CIViewMixin,
             CIGroupedSettingsView,CIActionDialog,
             WidgetBase,_Widget,_CacheMixin,ActionToolbar,ActionModel,
             ActionProvider,Memory,ExpressionsGridView,
             TestUtils,ACEEditor,Editor,_LayoutMixin,ActionStore,
             module) {

    console.clear();

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;

    if (ctx) {

        var D = dcl(null, {
            declaredClass: "D",
            method: function(x, y){
                console.log('run D method',arguments);
                return x + y;
            }
        });


        var E = dcl(D, {
            method: dcl.superCall(function(sup){
                return function(x, y){
                    console.log('run E method',arguments);
                    if(sup){
                        return sup.apply(this, arguments);
                    }
                    return 0;
                };
            })
        });

        var e = new E();
        e.method(2,1);


        var H = dcl(D, {
            method: dcl.after(function(){
                console.log("Called with arguments: ", arguments);
            })
        });

        console.log('---------with before');
        var h = new H();
        h.method(2,1);

        console.log('---------exp');
        var Top = dcl(null, {
            declaredClass: "A",
            method: dcl.after(function(args, result){
                console.log('run Top method');
                console.log("Returned result: ", result);
            })
        });

        var Child = dcl(Top, {
            method: function(){
                console.log('run child');
            }
        });

        var child = new Child();
        child.method(2,2);



        return declare('a',null,{});

    }
    return Grid;

});