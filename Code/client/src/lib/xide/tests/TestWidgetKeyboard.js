/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "./TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "xide/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    "xide/Keyboard",
    "xaction/ActionProvider"

], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,Keyboard,ActionProvider
) {

    console.clear();
    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    //var listeners = Keyboard.listeners;
    //var byNode = Keyboard.byNode;
    var listeners = [];
    var byNode = {};

    /**
     * Util to extend a keyboard mapping with control functions per listener. Keyboard mappings can
     * have multiple key sequences and this will take care about stop(), listen() and destroy().
     * @param mapping
     * @param listeners
     */
    var addListenerControls = function (mapping, listeners) {

        //console.log('addListenerControls ',listeners);

        mapping.stop = function () {
            _.invoke(listeners, 'stop_listening');
        };
        mapping.listen = function () {
            _.invoke(listeners, 'listen');
        };
        mapping.destroy = function () {
            console.error('destroy mapping');
            mapping.stop();
            _.each(listeners, function (listener) {
                if (listener && listener.destroy) {
                    listener.destroy();
                    listeners.remove(listener);
                }
            });
        };

        return mapping;
    };
    /**
     * Safe link to keypress prototype
     * @type {Listener|keypress.Listener}
     * @private
     */
    var keypressProto = window ? window['keypress'] ? window.keypress.Listener : null : null;
    if (!keypressProto) {
        console.error('you need keypress.min.js installed to use xide/Keyboard');
    }

    var Implementation = {
        /**
         * @member listener {Object[]} all keypress listener instances
         */
        _keyboardListeners: null,
        /**
         * The default setup for a listener, this is 'keypress' specific.
         *
         * @returns {{prevent_repeat: boolean, prevent_default: boolean, is_unordered: boolean, is_counting: boolean, is_exclusive: boolean, is_solitary: boolean, is_sequence: boolean}}
         */
        keyPressDefault: function () {
            return {
                prevent_repeat: false,
                prevent_default: true,
                is_unordered: false,
                is_counting: false,
                is_exclusive: false,
                is_solitary: false,
                is_sequence: true
            }
        },
        /**
         * Private listener creation method, accepts multiple key sequences for the same handler.
         *
         * @param keys {string|string[]} the key sequence (see below for the right codes ).
         * This option can be either an array of strings, or a single space separated string of key names that describe
         * the keys that make up the combo.
         *
         * @param params {Object|null} an additional parameter structure to override the default 'keypress' setup.
         * See this.keyPressDefault
         *
         * @param scope {Object|null} the scope in which the handler(s) are excecuted, defaults to 'this' as we are
         * a mixin.
         *
         *
         * @param type {string|null} the keypress combo type, can be:
         * simple_combo(keys, on_keydown_callback); // Registers a very basic combo;
         * counting_combo(keys, on_count_callback); // Registers a counting combo
         * sequence_combo(keys, callback); // Registers a sequence combo
         * register_combo(combo_dictionary); // Registers a combo from a dictionary
         *
         * @param handler {function|xide/types/KEYBOARD_EVENT} the callback for the key sequence. This can be one
         * function an structure per keyboard event. Usually its enough to leave this empty. You can also pass this
         * in the params

         * @param target {HTMLElement|null} the element on the listerner is bound to. Null means global!
         *
         * @public
         */
        addKeyboardListerner: function (keys, params, type, scope, handler, target, eventArgs) {

            // prepare keypress args
            var _defaults = lang.clone(this.keyPressDefault());
            //mixin override
            utils.mixin(_defaults, params);

            // defaults
            _defaults['this'] = _defaults['this'] || scope || this;

            // use simple_combo as default
            type = type || 'simple_combo';

            //normalize to array
            keys = !_.isArray(keys) ? [keys] : keys;

            var _keypress = window['keypress'];

            var _listeners = [],
                ignore = ['ctrl s', 'ctrl l', 'ctrl r', 'ctrl w', 'ctrl f4', 'shift f4', 'alt tab', 'ctrl tab'];


            _.each(keys, function (key_seq) {

                var listener = null,
                    targetId = target ? target.id : 'global',
                    wasCached = target ? !!byNode[target.id] : false;

                listener = byNode[targetId] || new keypressProto(target, _defaults);
                //listener = new keypressProto(target, _defaults);

                //console.log('was cached : ' + wasCached);
                console.error('--- ' + key_seq);

                listener[type](key_seq, function (e) {

                    if (e._did) {
                        return;
                    }
                    e._did = true;

                    if ((e.metaKey || e.altKey) && (key_seq !== 'alt enter')) {
                        //return;
                    }

                    //skip input fields
                    if (e.target.className.indexOf('input') == -1 || e.target.className.indexOf('ace_text-input') != -1) {

                        if (handler && handler.apply) {
                            handler.apply(_defaults['this'], eventArgs || e);
                            if (ignore.indexOf(key_seq) !== -1) {
                                e.preventDefault();
                                e.stopPropagation();
                            }
                        } else {
                            console.warn('keyboard: have no handler!');
                        }
                    }
                });

                byNode[targetId] = listener;

                if (!wasCached) {

                    !this._keyboardListeners && (this._keyboardListeners = []);

                    //store in local
                    this._keyboardListeners.push(listener);

                    //store in global
                    _listeners.push(listener);
                }

            }, this);

            return _listeners;
        },
        /**
         * Public interface to register a keyboard mapping
         * @param mapping {xide/types/KEYBOARD_MAPPING}
         * @returns {xide/types/KEYBOARD_MAPPING}
         */
        registerKeyboardMapping: function (mapping) {
            console.log('registerKeyboardMapping',mapping);
            try {
                var listeners = this.addKeyboardListerner(mapping.keys, mapping.params, null, mapping.scope, mapping.handler, mapping.target, mapping.eventArgs);
                mapping.listeners = listeners;
                return addListenerControls(mapping, listeners);
            } catch (e) {
                console.error('create keyboard mapping failed ', e);
            }
        },
        destroy:function(){

            this.inherited && this.inherited(arguments);
            var targetId = this.id;
            var listener = byNode[targetId];
            if(listener){
                listener.destroy();
                listener.element = null;
                delete byNode[targetId];
            }
        }
    };


    /**
     * Generic keyboard handler, using the external 'keypress' Javascript library
     * which handles keyboard events a bit more elegant and robust, it does allow
     * registration of keyboard - sequences as 'shift s':
     * @example
     *
     * listener.simple_combo("shift s", function() {
     *  console.log("You pressed shift and s");
     * });
     *
     * @link http://dmauro.github.io/Keypress/
     * @link https://github.com/dmauro/Keypress/blob/master/keypress.coffee#L728-864
     */
    var _Keyboard = declare("xide/Keyboard", null, Implementation);
    /**
     * Static mapping factory
     * @param keys
     * @param params
     * @param type
     * @param scope
     * @param handler
     * @param target
     * @param eventArgs
     * @memberOf module:xide/Keyboard
     * @returns {xide/types/KEYBOARD_MAPPING}
     */
    _Keyboard.createMapping = function (keys, params, type, scope, handler, target, eventArgs) {
        var mapping = utils.clone(types.KEYBOARD_MAPPING);//@TODO: bad copy, uses a ctr followed by a lang.mixin
        function getHandler(__handler) {
            return _.isString(__handler) ? lang.hitch(scope, __handler) : __handler;
        }

        mapping.keys = keys;
        mapping.params = params || {};
        mapping.type = type;
        mapping.scope = scope;
        mapping.handler = getHandler(handler);
        mapping.target = target;
        mapping.eventArgs = eventArgs;
        mapping.setHandler = function (event, handler) {
            mapping.params[event] = getHandler(handler);
            return mapping;
        };
        return mapping;

    };

    _Keyboard.defaultMapping = function (keys, handler, params, node, who, args) {
        return _Keyboard.createMapping(keys, params, null, who, handler, node, args);
    };

    _Keyboard.dcl = dcl(null, Implementation);
    _Keyboard.byNode = byNode;
    _Keyboard.listeners = listeners;







    function doTests(tab){
        var _widget = dcl([TemplatedWidgetBase,ActionProvider.dcl],{
        //var _widget = dcl([TemplatedWidgetBase,_Keyboard.dcl],{
            templateString:"<div tabindex='1' style='height: 100%;width: 100%'>Hello</div>",
            startup:function(){

                this.inherited(arguments);

                var thiz = this;
                setTimeout(function () {

                    /*

                    var keyCombo = 'ctrl 4';
                    var action = {};

                    function handler(){
                        console.error('---');
                    }

                    //var listeners = this.addKeyboardListerner(keyCombo, null, null, null, handler, this.domNode, null);

                    var _defaults = lang.clone(thiz.keyPressDefault());

                    var my_defaults = {
                        is_unordered    : true,
                        prevent_repeat  : true,
                        prevent_default  : true
                    };

                    var listener = new window.keypress.Listener(thiz.domNode.parentNode);

                    console.log(thiz.domNode.parentNode);


                    listener.register_combo({
                        "keys"              : 'shift s',
                        "on_keydown"        : function(){
                            console.error('down');
                        },
                        "on_keyup"          : function(){
                            console.error('up');
                        },
                        "on_release"        : function(){
                            console.error('release');
                        },
                        "this"              : thiz,
                        "prevent_default"   : true,
                        "prevent_repeat"    : false,
                        "is_unordered"      : false,
                        "is_counting"       : false,
                        "is_exclusive"      : false,
                        "is_solitary"       : false,
                        "is_sequence"       : false
                    });
                    */

                    //var listener = new keypressProto(this.domNode, _defaults);
                    /*
                    listener.simple_combo(keyCombo, function (e) {
                        console.error('-sdf');
                    });
                    */

                    //console.error(_defaults);
                },2000);

                var keyCombo = ['ctrl 4','ctrl 5'];
                var action = {};

                //debugger;

                var actions =[this.createAction({
                    label: 'Zoom In',
                    command: 'Window/Zoom In',
                    icon: 'fa-search-plus',
                    keycombo: keyCombo,
                    tab: 'Home',
                    group: 'Window',
                    owner: this,
                    mixin:{
                        addPermission:true
                    },
                    handler:function(){
                        console.error('--asdfsdf');
                    }
                })];
                this.addActions(actions);

                /*
                if (keyCombo) {
                    function handler(){
                        console.error('---');
                    }
                    var keyboardMappings;
                    if(this.keyboardMappings){
                        keyboardMappings = this.keyboardMappings;
                    }else{
                        action.keyboardMappings = keyboardMappings = [];
                    }
                    //keyboardMappings.push(Keyboard.defaultMapping(keyCombo, handler, keyProfile || types.KEYBOARD_PROFILE.DEFAULT, keyTarget, keyScope));
                    var mapping = Keyboard.defaultMapping(keyCombo, handler, types.KEYBOARD_PROFILE.DEFAULT, this.domNode, this,[]);
                    keyboardMappings.push(mapping);
                    //debugger;
                    var mappings = this.registerKeyboardMapping(mapping);
                    //console.dir(mappings);
                }
                */



            }
        });
        var widget = utils.addWidget(_widget,{

        },null,tab,true);

        setTimeout(function(){

            return;
            console.log('d ' , listeners);
            console.log('byId ' , byNode);
            widget.destroy();
            console.log('d after ' , listeners);
            console.log('byId ' , byNode);

        },2000);

    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {



        var parent = TestUtils.createTab(null,null,module.id);

        doTests(parent);

        return declare('a',null,{});

    }

    return Grid;

});