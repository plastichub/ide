/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "xdojo/has",
    'xide/types',
    'xide/utils',
    'xfile/tests/TestUtils',
    'module',
    'xide/widgets/ActionToolbar',
    'xide/action/ActionContext',
    'dijit/Toolbar',
    'dijit/ToolbarSeparator',
    'xide/widgets/ActionToolbarMixin',
    "xide/mixins/ActionMixin",
    "xide/mixins/EventedMixin",
    "xide/widgets/_MenuMixin",
    'xide/widgets/ActionToolbarButton',
    'dijit/Menu',
    'dijit/MenuItem',
    'xide/data/Reference',
    'dijit/form/DropDownButton',
    'xide/action/Action',
    'dijit/PopupMenuItem',
    'xide/model/Path'


], function (declare,has,types,utils,
             XFileTestUtils,module,ActionToolbar,ActionContext,

             Toolbar,ToolbarSeparator,ActionToolbarMixin, ActionMixin,EventedMixin,_MenuMixin,

             ActionToolbarButton,

             Menu, MenuItem, Reference,

             DropDownButton,
             Action,
             PopupMenuItem,
             Path

) {




    console.clear();

    var debugRegistry = window['xDebugRegistry'] = window['xDebugRegistry'] || {};

    debugRegistry['xide/widgets/ActionToolbar'] = {
        'renderAction':true
    }

    function registerHasDebugRegistry(registry){


        function register(node){

            /*
            has.add("mobile", function (global, document, anElement) {
                return (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase()));
            });
            */
        }

    }

    function createToolbarClass(){

        var debug = false;

        var toolbar = declare("xide/widgets/ActionToolbar", [Toolbar,ActionContext, ActionMixin,EventedMixin,_MenuMixin], {

            getItemsAtBranch:function(items, path) {
                return new Path(path).getChildren(_.pluck(items,'command'),false);
            },
            /**
             *
             * @param action
             * @param where
             * @param widgetClass
             * @param showLabel
             * @returns {*}
             */
            renderAction: function (action, where, widgetClass, showLabel,actions) {


                if(action.command==types.ACTION.TOOLBAR){
                    //debugger;
                }

                var collapse = [
                    'View/Show'
                ];








                var thiz = this,


                    _items = action.getChildren(),
                    _hasItems = _items && _items.length,
                    isContainer = _hasItems,
                    _expand = thiz.getVisibilityField(action, 'expand') == true,
                    _forceSub = action.forceSubs,
                    store = this.getActionStore(),
                    widget = thiz.getVisibilityField(action, 'widget'),
                    parentAction = action.getParent(),
                    parentWidget = parentAction ? thiz.getVisibilityField(parentAction, 'widget') : null,
                    actionVisibility = action.getVisibility(this.visibility),
                    customWidget  = actionVisibility.widgetClass;

                var parentCommand = action.getParentCommand();

                //command strings to actions
                function toActions(paths){
                    var result = [];
                    _.each(paths,function(path){
                        result.push(store.getSync(path));
                    });
                    return result;
                }

                if(action.command=='View/Show/Toolbar'){
                    //debugger;
                }
                /*
                 //if(action.command=='View/Show/Toolbar'){

                 //action is in collapse map
                 if(collapse.indexOf(parentCommand)!=-1){

                 //console.error('collapse');
                 if(!parentAction){
                 parentAction = new Action({
                 command:parentCommand,
                 _store:store,
                 getParentCommand: function () {
                 var segments = this.command.split('/');
                 if (segments.length > 1) {
                 var _s = segments.slice(0, segments.length-1);
                 var _c = _s.join('/');
                 return _c;
                 }
                 return null;
                 },
                 getChildren:function(){
                 var _items = thiz.getItemsAtBranch(actions,parentCommand);
                 return toActions(_items);
                 },
                 getParent:function(){
                 var segments = this.command.split('/');
                 if (segments.length > 1) {
                 var _s = segments.slice(0, segments.length-1);
                 var _c = _s.join('/');
                 return this._store.getSync(_c);

                 }
                 return null;
                 }
                 });
                 }

                 if(!parentWidget){
                 parentWidget = this.renderAction(parentAction,where,null,null,actions);
                 where = parentWidget;
                 }


                 }
                 }

                 */




                //further variables
                var label = '',// actionVisibility.label || action.label,
                    widgetArgs = {
                        iconClass: action.icon,
                        label: label,
                        item: action
                    };

                if (actionVisibility.widgetArgs) {
                    utils.mixin(widgetArgs, actionVisibility.widgetArgs);
                }






                /**
                 * Determine widget class to use, resolve
                 */
                widgetClass =

                    //override
                    actionVisibility.widgetClass ||
                        //argument
                    widgetClass ||
                        //visibility's default
                    thiz.widgetClass;





                debug && console.log('render action '+ action.command + ' with ' + widgetClass.prototype.declaredClass + ' render label ');

                // case one: isContainer = true
                if (!widget) {

                    if (isContainer) {

                        if (_expand) {
                            return null;
                        }
                        if (!parentWidget) {





                            var menu = utils.addWidget(DropDownButton, widgetArgs, this, where, true, null, [Reference]);

                            var pSubMenu = utils.addWidget(Menu, {
                                item: action
                            }, this, null, true);

                            menu.dropDown = pSubMenu;
                            actionVisibility.widget = pSubMenu;
                            actionVisibility.widget.visibility = thiz.visibility;

                            menu.visibility = thiz.visibility;


                            thiz._publishActionWidget(menu,action,where);




                            return menu;

                        } else {

                            //parent widget there

                            var pSubMenu = new Menu({parentMenu: parentWidget});


                            var popup = new dijit.PopupMenuItem({
                                label: label,
                                popup: pSubMenu,
                                iconClass: action.icon
                            });


                            parentWidget.addChild(popup);

                            actionVisibility.widget = pSubMenu;
                            actionVisibility.widget.visibility = thiz.visibility;
                            return pSubMenu;

                        }
                    }
                    if (parentWidget) {

                        where = parentWidget;
                        widgetClass = customWidget ? widgetClass : MenuItem;
                    }
                } else {
                    console.log('widget already rendered!');
                }


                function __showLabel(action,actions){

                    /**
                     *
                     * View/Show/Toolbar    =>false
                     *
                     * View/Source          =>false
                     *
                     *
                     * View/Columns
                     * View/Columns/Media   => true
                     *
                     */


                    var parentAction = action.getParent();

                    //console.log('render action ' + action.command + ' parent = ' + (parentAction ? parentAction.command : 'no parent'));

                    if(!parentAction){
                        return false;
                    }else{
                        return true;
                    }
                }
                if (!actionVisibility.widget && !action.domNode) {

                    if (!action.label) {
                        //huh, really? @TODO: get rid of label needs in action:render
                    } else {
                        //console.log('render action ' + action.command);
                        actionVisibility.widget = thiz._addMenuItem(action, where, widgetClass, __showLabel(action));
                        actionVisibility.widget.visibility = thiz.visibility;
                    }
                } else {

                    console.log('have already rendered action', action);
                }

                return actionVisibility.widget;

            },
            /**
             * Renders a set of actions into is appropriate widgets. This utilizes this._renderAction and
             * puts the action's widget into its visibility store for global access.
             *
             * @param actions {xide/action/Action[]}
             * @param where {dijit/_Widget}
             * @param widgetClass {Module}
             * @private
             */
            _renderActions: function (actions, where, widgetClass, showLabel, separatorClass, force) {




                //track the last action group for adding a separator
                var _lastGroup = null,
                    thiz = this,
                    widgets = [],
                    _lastWidget;

                if (!actions) {
                    console.error('strange, have no actions!');
                    return;
                }

                for (var i = 0; i < actions.length; i++) {

                    var action = actions[i],
                        sAction = action._store ? action._store.getSync(action.command) : null;

                    if (sAction && sAction != action) {
                        console.log('weird!');
                    }
                    if (sAction) {
                        action = sAction;
                    }

                    if (!action) {
                        console.error('invalid action!');
                        continue;
                    }




                    //pick command group
                    if (action.group && _lastGroup !== action.group) {

                        //console.log('action group : '+ _lastGroup);
                        if(_lastGroup) {
                            utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true);
                        }
                        _lastGroup = action.group;
                    }

                    //skip if action[visibility].show is false
                    if (thiz.getVisibilityField(action, 'show') === false) {
                        continue;
                    }

                    var _items = action.getChildren ? action.getChildren() : action.items,
                        _hasItems = _items && _items.length,
                        _expand = thiz.getVisibilityField(action, 'expand'),
                        _forceSub = action.forceSubs;


                    force = force != null ? force : this.forceRenderSubActions;


                    _lastWidget = thiz.renderAction(action, where, widgetClass, showLabel,actions);

                }

                return widgets;
            },
            __renderActions: function (actions, where, widgetClass, showLabel, separatorClass, force) {

                //track the last action group for adding a separator
                var _lastGroup = null,
                    thiz = this,
                    widgets = [],
                    _lastWidget;

                if (!actions) {
                    console.error('strange, have no actions!');
                    return;
                }

                for (var i = 0; i < actions.length; i++) {

                    var action = actions[i],
                        sAction = action._store ? action._store.getSync(action.command) : null;

                    if (sAction && sAction != action) {
                        console.log('weird!');
                    }
                    if (sAction) {
                        action = sAction;
                    }

                    if (!action) {
                        console.error('invalid action!');
                        continue;
                    }


                    //pick command group
                    if (action.group && _lastGroup !== action.group) {

                        console.log('action group : '+ _lastGroup);
                        if(_lastGroup) {
                            utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true);
                        }
                        _lastGroup = action.group;
                    }

                    //skip if action[visibility].show is false
                    if (thiz.getVisibilityField(action, 'show') === false) {
                        continue;
                    }

                    var _items = action.getChildren ? action.getChildren() : action.items,
                        _hasItems = _items && _items.length,
                        _expand = thiz.getVisibilityField(action, 'expand'),
                        _forceSub = action.forceSubs;


                    force = force != null ? force : this.forceRenderSubActions;


                    _lastWidget = thiz.renderAction(action, where, widgetClass, showLabel);

                }

                return widgets;
            },

            _renderRoot:function(path){


                var thiz = this,
                //permanentActionStore = this.getPermanentActionStore(),
                //permanentActions = permanentActionStore.query(),

                    store = this.getActionStore(),
                    actions = store.query()/*.concat(permanentActions)*/,

                //return all actions with non-empty tab field
                    allActions = actions.filter(function (action) {
                        return action.tab != null;
                    }),
                //build array of action's command
                    allActionPaths = _.pluck(allActions,'command'),
                //all actions at #path
                    menuActions = thiz.getItemsAtBranch(allActions,path);

                //return an action from both stores
                function getAction(command){
                    return store.getSync(command) /*|| permanentActionStore.getSync(command)*/;
                }

                //command strings to actions
                function toActions(paths){
                    var result = [];
                    _.each(paths,function(path){
                        result.push(getAction(path));
                    });
                    return result;
                }

                //render action
                function render(path) {

                    var action = getAction(path) || new Action({command: path}),
                        childPaths = new Path(path).getChildren(allActionPaths, false),
                        isContainer = childPaths.length > 0,
                        childActions = isContainer ? toActions(childPaths) : null;

                    //console.log('render path '+ path  + ' is container ' + isContainer, [ action , childActions] );

                    if (isContainer) {

                        if (thiz._renderMap[path]) {
                            console.error('already rendered ' + path);
                            return;
                        }

                        var menu = new Menu({parentMenu: menuItem});
                        thiz.setVisibilityField(action, 'widget', menu);
                        thiz._renderActions(childActions, menu, null, null, null, true);
                        var segments = path.split('/');
                        var lastSegment = segments[segments.length - 1];
                        var visibilityLabel = thiz.getVisibilityField(action, 'label');
                        var label = visibilityLabel || lastSegment;
                        var icon = thiz.getVisibilityField(action, 'icon', 'fa-magic') || 'fa-magic';


                        //!_.isEmpty(action) && thiz._publishActionWidget(menu,action);
                    } else {

                        var _action = getAction(path);

                        if (_action) {

                            var _widget = thiz.getVisibilityField(_action, 'widget');
                            if (_widget) {
                                console.warn('already renderer');
                            } else {

                                thiz._debug && console.log('render action ' + _action.command);

                                thiz._renderAction(_action, menuItem);

                            }

                        } else {
                            console.error('cant find action at ' + path);
                        }
                    }
                }


                _.each(menuActions,render);



            },
            /**
             * Set action store
             * @param store {ActionStore}
             */
            setActionStore: function (store) {

                this.store = store;

                var self = this,
                    allActions = store.query(),

                //return all actions with non-empty tab field
                    tabbedActions = allActions.filter(function (action) {
                        return action.tab != null;
                    }),

                //group all tabbed actions : { Home[actions], View[actions] }
                    groupedTabs = _.groupBy(tabbedActions, function (action) {
                        return action.tab;
                    }),

                //now flatten them
                    _actionsFlattened = [];


                _.each(groupedTabs, function (items, name) {
                    _actionsFlattened = _actionsFlattened.concat(items);
                });

                //console.log('actions : ');
                //debugActions(_actionsFlattened);



                this._renderActions(_actionsFlattened,this,null,null,null,null);


                /*
                 var rootActions = []; //['Edit','File','Navigation','View',....];
                 _.each(_actionsFlattened, function (action) {
                 var rootCommand = action.getRoot();
                 rootActions.indexOf(rootCommand) == -1 && rootActions.push(rootCommand);
                 });

                 console.clear();

                 _.each(rootActions, function (root) {
                 self._renderRoot(root);
                 });*/


                //console.clear();




            },

            //
            //      NEW
            //
            /////////////////////////////////////////////////////////////////////////////////////
            /**
             * Set visibility filter
             */
            visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
            /**
             * The separator class for separating actions
             */
            separatorClass:ToolbarSeparator,
            /**
             * The class being used to render an action.
             *
             * @type {dijit/_Widget}
             * @member
             */
            widgetClass:ActionToolbarButton,
            clear:function(){
                //_destroyActions

                //this._destroyActions(this._actions);
            },
            _destroyActions: function (visibility) {

                var actions = this.getActionStore().query();

                _.each(actions,function(action){

                    var actionVisibility = action.getVisibility!= null ? action.getVisibility(visibility) : null;
                    if(actionVisibility){

                        var widget = actionVisibility.widget;
                        if(widget){
                            //remove action reference widget
                            action.removeReference && action.removeReference(widget);
                            widget.destroy();
                            this.setVisibilityField(action, 'widget', null);
                        }
                    }
                },this);


            },
            destroy:function(){

                //_destroyActions
                this._destroyActions(this.visibility);

                this.inherited(arguments);
            },
            check:function(){
                if(!this.domNode || this._destroyed){
                    console.warn('@todo: ActionToolbar::check::orphan!');
                    this.destroy();
                    return false;
                }
                return true;
            }
        });

        return toolbar;

    }

    /***
     *
     * playground
     */
    var _lastFileGrid = window._lastFileGrid;
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        _lastRibbon = window._lastRibbon,
        ACTION = types.ACTION;

    /*****************************************************************
     * debug stuff
     * @param stackframes
     */
    var callback = function(stackframes) {
        var stringifiedStack = stackframes.map(function(sf) {
            return sf.toString();
        }).join('\n');
        console.log(stringifiedStack);
    };

    var errback = function(err) { console.log(err.message); };
    function debugActions(actions){

        var commands = _.pluck(actions,'command');
        console.log(commands.join('\n'));
    }

    //StackTrace.get().then(callback).catch(errback);

    /*****************************************************************
     * tests
     */


    var Implementation = {

        _onBlur:function(){

            console.log('   lost focus '  + this.id + ' for ');


        },
        _isActive:function(testNode){
            return utils.isDescendant(this.domNode.parentNode,testNode || document.activeElement);
        },
       _onFocusChanged:function(focused,type,newFocusedElement){

           console.log('focus changed ');
           return;


            var isActive = this.isActive(newFocusedElement);

            console.log('focuse changed ' + focused + ' type ' + type + ' is active = ' +isActive);

            if(this._focused && !focused){

                console.log('   lost focus '  + this.id + ' for ');
                /*
                 if(this.id==='xide_widgets_TemplatedWidgetBase_0'){
                 console.log('   left blur');
                 }
                 */
            }

            if(!this._focused && focused){
                this._emit(types.EVENTS.ON_VIEW_SHOW,this);
            }

            this._focused = focused;

            this.highlight  && this.highlight(focused);
        }

    }



    function doToolbarTests(grid){
        //grid.showToolbar(true,createToolbarClass());
        //grid.deselectAll();
        grid.openItem('./Tutorials').then(function(){
            grid.select([0],null,true,{
                focus:true,
                append:false
            });
        });


    }




    function testMain(grid,panel){
        ctx.getWindowManager().registerView(grid,true);
        grid.refresh().then(function(){
            setTimeout(function(e){
                doToolbarTests(grid);
            },1000);
        });
    }

    if (ctx) {

        var doTest = true;
        if (doTest) {

            var ACTION = types.ACTION;

            var grid = XFileTestUtils.createFileGrid('root',
                //args
                {

                    _columns: {
                        "Name": true,
                        "Path": false,
                        "Size": false,
                        "Modified": false,
                        "Owner":false
                    },
                    __permissions: [
                        ACTION.EDIT,
                        /*
                        ACTION.EDIT,
                        ACTION.RENAME,
                        ACTION.DOWNLOAD,
                        ACTION.COLUMNS,
                        ACTION.GO_UP,
                        ACTION.CLIPBOARD
                        */
                        //ACTION.COLUMNS,
                        ACTION.LAYOUT,
                        ACTION.COLUMNS,
                        ACTION.GO_UP,
                        ACTION.NEW_FILE,
                        ACTION.NEW_DIRECTORY
                    ],
                    tabOrder: {
                        'Home': 100,
                        'View': 50,
                        'Settings': 20
                    },
                    tabSettings: {
                        'Step': {
                            width:'190px'
                        },
                        'Show': {
                            width:'130px'
                        },
                        'Settings': {
                            width:'100%'
                        }
                    },
                    groupOrder: {
                        'Clipboard': 110,
                        'File': 100,
                        'Step': 80,
                        'Open': 70,
                        'Organize': 60,
                        'Insert': 10,
                        'Select': 5,
                        'Navigation':4
                    }
                },
                Implementation
                ,'TestGrid',module.id,true);



            function test() {
                testMain(grid,grid._parent);
            }

            setTimeout(function () {
                test();
            }, 1000);
        }
    }

    return declare('maeh',null,{});

});
