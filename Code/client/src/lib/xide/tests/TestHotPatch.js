/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/views/CIGroupedSettingsView',
    'xide/views/_CIDialog',
    'xide/widgets/WidgetBase',
    'xide/widgets/_Widget',
    'xide/widgets/_CacheMixin',
    'xide/widgets/ActionToolbar',
    'xide/action/ActionModel',
    'xaction/ActionProvider',
    "dstore/Memory",
    'xide/widgets/ExpressionsGridView',
    'xide/tests/TestUtils',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'xide/views/_LayoutMixin',
    'xide/action/ActionStore',
    'xide/mixins/Electron',
    'xdojo/has',
    'dojo/Deferred',
    'dojo/promise/all',
    'module',
    'xide/widgets/TemplatedWidgetBase'

], function (dcl,declare, types,
             utils, Grid, factory,CIViewMixin,
             CIGroupedSettingsView,CIActionDialog,
             WidgetBase,_Widget,_CacheMixin,ActionToolbar,ActionModel,
             ActionProvider,Memory,ExpressionsGridView,
             TestUtils,ACEEditor,Editor,_LayoutMixin,ActionStore,
             _Electron,has,Deferred,all,module,TemplatedWidgetBase

    ) {

    if(has('electronx')) {

        console.clear();
        var Electron = new _Electron();
        var jet = Electron.srequire('fs-jetpack');
        var app = Electron.srequire('app');
        var http = Electron.srequire('http');
        var fs = Electron.srequire('fs');
        var path = Electron.srequire('path');
        var request = Electron.srequire('request');
        var clientFile = "http://rawgit.com/net-commander/windows-dist/master/Code/client/src/xcf/dojo/xcf.js";
        clientFile = "https://github.com/net-commander/windows-dist/raw/master/Code/client/src/xcf/dojo/xcf.js";
        var appDir = app.getAppPath();
        var download = function(url, dest, cb) {
            var file = fs.createWriteStream(dest);
            var request = http.get(url, function(response) {
                response.pipe(file);
                console.log(response);
                file.on('finish', function(e) {
                    console.error('-finish',e);
                    file.close(cb);  // close() is async, call cb after close completes.
                });
            }).on('error', function(err) { // Handle errors
                fs.unlink(dest); // Delete the file async. (But we don't check the result)
                if (cb) cb(err.message);
            }).on('data', function(data) {
                console.log('data')
            }).on('progress', function(data) {
                console.log('data');
            });
        };
        var ROOT_URL = "https://github.com/net-commander/windows-dist/raw/master/";
        var APP_ROOT = jet.cwd(appDir);
        function downloadTo(_file,finish,progress){

            var dfd = new Deferred();
            var clientFile = ROOT_URL + '/' + _file + ('?time='+ new Date().getTime());

            //ensure
            APP_ROOT.file(_file,{
                content:""
            });

            var _dstFile = APP_ROOT.cwd() + '/' +_file;
            var dstFile = fs.createWriteStream(_dstFile);


            var file = request(clientFile).
            on('error',function(e){
                console.error('error',e);
            }).
            on('response',function(res){
                var len = parseInt(res.headers['content-length'], 10);
                var downloaded = 0;
                var total = 0;
                res.on('data', function(chunk) {
                    downloaded += chunk.length;
                    //console.log("Downloading " + (100.0 * downloaded / len).toFixed(2) + "% " + downloaded + " bytes");
                    progress((100.0 * downloaded / len).toFixed(2) + "%");
                }).on('end', function () {
                    console.log(' downloaded to: '+_dstFile);
                }).on('error', function (err) {
                    dfd.resolve(err);
                });
            }).
            on('data',function(e){}).

            pipe(dstFile);
            dstFile.on('finish',function(e){
                finish();
                dfd.resolve();
            });

            return dfd;
        }
        function update(file,progress){
            return downloadTo(file,function(){
                console.error('finished ' + file);
            },function(_progress){
                progress && progress(_progress);
            });
        }
    }

    console.clear();

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;



    function createArgumentsWidget(){

        var ArgumentsWidget = dcl(TemplatedWidgetBase, {
            declaredClass: "xide.widgets.ArgumentsWidget",
            templateString: "<div class='widget' style=''>" +
                "<div attachTo='inner' style='width:100%;'></div>" +
            "</div>",

            addDownload:function(file){
                var template = '<br/><label>' + file + '</label><br/>'+
                    '<div class="progress progress-striped active">'+
                    '<div class="progress-bar progress-bar-warning" style="width: 0%;"></div>'+
                '</div><hr/>';

                var $template = $(template);
                this.$inner.append($template);
                var progressItem =this.$inner.find('.progress-bar');
                var progressRoot =this.$inner.find('.progress-striped');
                var download = update(file,function(progress){
                    progressItem.css('width',progress);
                    progressItem.html(progress);
                }).then(function(){

                    //progressItem.removeClass('progress-bar-warning');
                    //progressRoot.removeClass('progress-striped');
                    //progressRoot.removeClass('active');
                    progressItem.addClass('progress-bar-info');
                    progressItem.html('Finish');
                });
                return download;
            },
            addDownloads:function(items){

                this.all = [];
                _.each(items,function(item){
                    this.all.push(this.addDownload(item));
                },this);

                return all(this.all);
            },
            startup: function () {

            }
        });

        return ArgumentsWidget;
    }


    function testWidget(){

        var toolbar = ctx.mainView.getToolbar();
        var docker = ctx.mainView.getDocker();
        var parent  = TestUtils.createTab(null,null,module.id);

        var view = utils.addWidget(createArgumentsWidget(),{

        },null,parent,true);


        view.addDownloads([
            'Code/client/src/xcf/dojo/xcf.js',
            'server/nodejs/nxappmain/serverbuild.js',
            'data/system/drivers/DriverBase.js',
            'Code/client/src/lib/xblox/build/xblox.js',
            'Code/client/src/lib/xblox/build/xbloxr.js'
        ]).then(function(){
            console.error('all done');
        });

        docker.resize();
    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;

    if (ctx) {

        testWidget();

        return declare('a',null,{});
    }

    return Grid;

});