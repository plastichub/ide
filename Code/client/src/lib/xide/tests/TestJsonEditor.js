/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    'xide/views/CIGroupedSettingsView',
    'xide/views/_CIDialog',
    'xide/widgets/WidgetBase',
    'xide/widgets/ActionToolbar',
    'xaction/ActionProvider',
    'xide/views/_LayoutMixin',
    'xide/layout/Container',
    'dojo/promise/all',
    'dojo/Deferred',
    'module',
    'xide/widgets/JSONDualEditorWidget',
    'xide/editor/Base',
    'xide/tests/TestUtils'

], function (dcl,declare, types,
             utils, Grid, factory,CIGroupedSettingsView,CIActionDialog,
             WidgetBase,ActionToolbar,ActionProvider,_LayoutMixin,Container,all,Deferred,
             module,JSONDualEditorWidget,EditorBase,
             TestUtils
    ) {

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;




    function createEditorBase(){
        return dcl([Container,ActionProvider.dcl],{
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Editor':9,
                'Step': 5,
                'New': 4,
                'Window': 3,
                'Help':2
            },
            declaredClass:'xide/views/Editor',
            options:null,
            /**
             * The icon class when doing any storage operation
             * @member loadingIcon {string}
             */
            loadingIcon:'fa-spinner fa-spin',
            /**
             * The original icon class
             * @member iconClassNormal {string}
             */
            iconClassNormal:'fa-code',
            //////////////////////////////////////////////////////////////////
            //
            //
            //
            getContent:function(item,onSuccess,onError){

                if(!this.storeDelegate){

                    onError && onError('Editor::getContent : Have no store delegate!');
                }else{
                    var _cb=function(content){
                        onSuccess(content);
                    };

                    this.storeDelegate.getContent(_cb,item||this.item);
                }
            },
            saveContent:function(value,onSuccess,onError){

                var thiz=this;

                this.set('iconClass', 'fa-spinner fa-spin');

                var _value = value || this.get('value');
                if(!_value){
                    console.log('Editor::saveContent : Have nothing to save, editor seems empty');
                }
                if(!this.storeDelegate){
                    if(onError){
                        onError('Editor::saveContent : Have no store delegate!');
                    }
                    return false;
                }else{
                    var _s = function(){

                        thiz.set('iconClass',thiz.iconClassNormal);
                        thiz.lastSavedContent=_value;
                        thiz.onContentChange && thiz.onContentChange(false);
                        var struct = {
                            //path:thiz.options.filePath,
                            item:thiz.item,
                            content:_value,
                            editor:thiz
                        };

                        thiz.publish(types.EVENTS.ON_FILE_CONTENT_CHANGED,struct,thiz);
                    };
                    return this.storeDelegate.saveContent(_value,_s,null,thiz.item);
                }
            },
            onLoaded: function () {
                this.set('iconClass', this.iconClassNormal);
            },
            startup: function () {


                //save icon class normal
                this.iconClassNormal = '' + this.iconClass;

                this.set('iconClass', 'fa-spinner fa-spin');



                var self = this,
                    options = this.options || {},
                    item = this.item,
                    ctx  = this.ctx,
                    result = new Deferred();

                this.storeDelegate = {
                    getContent:function(onSuccess){
                        ctx.getFileManager().getContent(item.mount,item.path,onSuccess);
                    },
                    saveContent:function(value,onSuccess,onError){
                        ctx.getFileManager().setContent(item.mount,item.path,value,onSuccess);
                    }
                };


                if (!this.item && this.value == null) {
                    return result;
                }


                function createEditor(options, value) {
                    self.createEditor(self.options || options, value);
                }

                if (this.value != null) {
                    createEditor(null, this.value);
                } else {

                    //we have no content yet, call in _TextEditor::getContent, this will be forwarded
                    //to our 'storeDelegate'
                    this.getContent(
                        this.item,
                        function (content) {//onSuccess
                            self.set('iconClass', thiz.iconClassNormal);
                            self.lastSavedContent = content;
                            createEditor(options, content);
                        },
                        function (e) {//onError

                            createEditor(null, '');
                            logError(e, 'error loading content from file');
                        }
                    );
                }
                return result;
            }
        });
    }


    function createDriverCIS(driver,actionTarget){

        var CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID",
                    "name": "CF_DRIVER_ID",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": "Arguments",
                    "uid": "-1",
                    "value": "{}",
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General9',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 13,
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true
                }
            ]
        };

        _.each(CIS.inputs,function(ci){

        });
        return CIS;
    }
    function createArgumentsWidget2(){

        var ArgumentsWidget = dcl([WidgetBase,_LayoutMixin.dcl,ActionProvider.dcl], {
            declaredClass: "xide.widgets.ArgumentsWidget",
            minHeight: "400px;",
            value: "",
            options: null,
            templateString: "<div attachTo='domNode' class='widgetContainer widgetBorder widgetTable' style=''></div>",
            jsonEditorWidget:null,
            editorWidget:null,
            setActiveEditor:function(editor){
                this._activeEditor = editor;
                this._emit('setActiveEditor',editor);
            },
            fillTemplate: function () {


                var thiz = this;

                var value = utils.toString(this.userData['value']) || '{}';


                var area = $('<textarea rows="3" class="form-control input-transparent" ></textarea>');
                area.val(value);
                this.editBox = area;
                $(this.previewNode).append(area);
                this.editBox.on("change", function (e) {

                    var value2 = e.target.value;

                    thiz.userData.changed = true;
                    thiz.userData.active = true;

                    utils.setCIValueByField(thiz.userData, "value", value2);

                    var _args = {
                        owner: thiz.delegate || thiz.owner,
                        ci: thiz.userData,
                        newValue: value2,
                        storeItem: thiz.storeItem
                    }
                    thiz.publish(types.EVENTS.ON_CI_UPDATE, _args);
                    thiz._emit('valueChanged', _args);
                });

                var btn = factory.createSimpleButton('', 'fa-magic', 'btn-default', {
                    style: ''
                });

                $(btn).click(function () {
                    thiz.onSelect();
                })

                $(this.button0).append(btn);
            },
            /**
             *
             * @param bottom
             * @param top
             * @param center
             * @returns {*}
             */
            createWidgets:function(bottom,top,center){

                var data = this.userData,
                    valueStr = JSON.stringify(data.value,null,2),
                    docker = this.getDocker(),
                    thiz = this;

                var CIS_TOP = [
                    utils.createCI('Arguments', types.ECIType.JSON_DATA, utils.getJson(data.value), {
                        options:{
                            readOnlyNodes:{
                                "commands":true,
                                "variables":true,
                                "meta":true
                            },
                            renderTemplates : [
                                {
                                    //
                                    //  This segment is used to replace something in the node's dom structure
                                    //

                                    /**
                                     * @type {string} the path within the dom structure
                                     */
                                    nodeValuePath: 'field.innerHTML',
                                    /**
                                     * @type {RegExp|string|function|RegExp[]|string[]|function[]}
                                     */
                                    match: [/^variables[\s]?\.(\d+)$/,/^commands[\s]?\.(\d+)$/],
                                    /**
                                     * @type {string} the new value for the field specified in nodeValuePath
                                     */
                                    replaceWith: '{nodeValue} - {name}',
                                    /**
                                     * @type {object} additional variables
                                     */
                                    variables: null,

                                    /**
                                     * @type {function} a function to transform the node's dom value into something else
                                     */
                                    nodeValueTransform:function(value){
                                        return utils.capitalize(value);
                                    },
                                    //
                                    //  This segment is about dom manipulation, todo!
                                    //

                                    /**
                                     * @type (object)
                                     */
                                    insertIfMatch:{}
                                }
                            ],
                            insertTemplates:[
                                {
                                    label: 'New Command',
                                    path: 'commands',
                                    value: '{title:"No Title",value:""}',
                                    newNodeTemplate: '[]',
                                    collapse:true,
                                    select:true
                                },
                                {
                                    label: 'New Variable',
                                    path: 'variables',
                                    value: '{title:"No Title",value:""}',
                                    newNodeTemplate: '[]',
                                    collapse:true,
                                    select:true
                                }
                            ]
                        },
                        widget:{
                            templateString:"<div class='widgetContainer widgetBorder widgetTable widget' style=''>" +
                            "<div class='widget jsonEditorWidget' attachTo='valueNode'></div>" +
                            "</div>"

                        }
                    })
                ];

                var renderTop =factory.renderCIS(CIS_TOP, top.containerNode, this);
                renderTop.then(function (widgets) {

                    thiz.jsonEditorWidget = widgets[0];
                    var toolbar = utils.addWidget(ActionToolbar,{
                        attachToGlobal:false,
                        resizeToParent:false
                    },null,center,true);

                    thiz.add(toolbar,null,true);
                    var actionStore = thiz.getActionStore();
                    var actions = thiz.jsonEditorWidget.editor.getItemActions();
                    _.each(actions,function(action){
                        action.mixin = {
                            addPermission:true
                        },
                        action.tab = "Home"
                    });
                    actions = thiz.addActions(actions);
                    toolbar.setActionStore(actionStore,thiz);
                    $(toolbar.domNode).css({
                        width:'auto',
                        'float':'left'
                    });
                    $(toolbar.domNode).removeClass('actionToolbar');

                    $(widgets[0].domNode).on('click',function(){
                        thiz.setActiveEditor(widgets[0]);
                    });
                    thiz.setActiveEditor(widgets[0]);

                });



                var CIS_BOTTOM = [

                    utils.createCI('Arguments', types.ECIType.SCRIPT,data.value,{
                        vertical:true,
                        widget:{
                            templateString: "<div class='' style='width: 100%;height:inherit'>",
                            height:'100%',
                            editorArgs:{
                                showGutter:true,
                                mode:'json',
                                options:{
                                    showGutter:true,
                                    mode:'json'
                                }
                            }
                            //resizeToParent:true
                        }
                    })
                ];

                var scriptWidget = null;
                var renderBottom = factory.renderCIS(CIS_BOTTOM, bottom.containerNode, this);
                renderBottom.then(function (widgets) {
                    thiz.editorWidget = widgets[0];
                    $(widgets[0].domNode).on('click',function(){
                        thiz.setActiveEditor(widgets[0]);
                    });
                });

                var btn = factory.createSimpleButton('', 'fa-caret-up', 'btn-default', {
                    style: 'margin-right:12px'
                });
                var btnDown = factory.createSimpleButton('', 'fa-caret-down', 'btn-default', {
                    style: ''
                });


                $(center.containerNode).append(btn);
                $(center.containerNode).append(btnDown);

                $(btn).click(function () {
                    thiz.onUp();
                })
                $(btnDown).click(function () {
                    thiz.onDown();
                });
                return all([renderBottom,renderTop]);
            },
            onUp:function(){
                var valueDown = this.editorWidget.getValue();
                var valueUp = this.jsonEditorWidget.getValue();
                try{
                    var newData = dojo.fromJson(valueDown);
                     this.jsonEditorWidget.setData(newData)
                }catch(e){
                    console.error('mal formed '+e);
                }
            },
            onDown:function(){
                var valueDown = this.editorWidget.getValue();
                var valueUp = this.jsonEditorWidget.getValue();
                this.editorWidget.aceEditor.set('value',valueUp);

            },
            startup: function () {

                var title = false,
                    docker = this.getDocker(),
                    toolbar = this._addPanel(utils.mixin({
                        h: '100',
                        title: title || '  '
                    },{}), types.DOCKER.DOCK.TOP,title,0.5,'DefaultFixed'),

                    bottom = this.getBottomPanel(false,null,null,null,null),
                    panels = docker.getPanels(),
                    _top = panels[1],
                    _center = panels[0],
                    _bottom = panels[2],
                    result = this.createWidgets(_bottom,_top,_center),
                    self = this;

                if(this.dfd){
                    result.then(function(w){
                        self.dfd.resolve(w);
                    })
                }

                _center.maxSize(null,58);
                _bottom.getSplitter().pos(0.5);
                $(_center.containerNode).css({
                    "textAlign":"center"
                });

                return result;
            },
            getValue:function(){
                return this._activeEditor.getValue();
            }
        });

        return ArgumentsWidget;
    }
    function testJsonWidget() {

        var toolbar = ctx.mainView.getToolbar();

        var docker = ctx.mainView.getDocker();

        var parent = TestUtils.createTab(null, null, module.id);


        var view = utils.addWidget(CIGroupedSettingsView, {
            title:  'title',
            typeMap:null,
            getTypeMap:function(){
                if (this.typeMap) {
                    return this.typeMap;
                }

                var self = this;
                var typeMap = {}
                typeMap['ArgumentsWidget2'] = createArgumentsWidget2();
                this.typeMap = typeMap;
                return typeMap;
            },
            cis: [
                utils.createCI('Arguments', 13, 'asd', {
                    group: 'Arguments2'
                }),
                utils.createCI('Arguments', 'ArgumentsWidget2', {a:2}, {
                    group: 'Arguments',
                    type:'ArgumentsWidget2'
                })
            ],
            storeDelegate: this,
            iconClass: 'fa-eye',
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'General'
            }
        }, null, parent, true);


    }


    var jsonEditor = dcl([EditorBase],{

        widget:null,
        createEditor:function(_options,value){
            var ci = {
                value:value
            }
            this.widget = utils.addWidget(JSONDualEditorWidget /*createArgumentsWidget2()*/,{
                userData:ci,
                dfd:this.dfd,
                item:this.item
            },null,this.domNode,false);

            this.add(this.widget,null,false);
            this.widget.startup();
        },
        getActionStore:function(){
            var editorWidget = this.widget.editorWidget;
            return editorWidget.aceEditor.getActionStore();
        },
        startup:function(){

            this.dfd = new Deferred();

            var self = this;

            this.dfd.then(function(){

                var editorWidget = self.widget.editorWidget;
                var oldAction = editorWidget.aceEditor.runAction;

                editorWidget.aceEditor.runAction=function(action){

                    if(action.command===types.ACTION.SAVE){
                        var value = self.widget.getValue();
                        self.saveContent(value);
                    }
                    return oldAction.apply(editorWidget.aceEditor,[action]);
                };

                if(self.ctx && self.registerView){
                    self.ctx.getWindowManager().registerView(self);
                }

            });

            return this.dfd;
        }
    });



    function testJsonEditor() {

        var toolbar = ctx.mainView.getToolbar();

        var docker = ctx.mainView.getDocker();

        var parent = TestUtils.createTab(null, null, module.id);

        var fileManager = ctx.getFileManager();
        var store = fileManager.getStore('workspace');
        var item = store.getItem('./test.json',true);


        var dfd = new Deferred();
        var view = utils.addWidget(jsonEditor, {
            title:  'title',
            item:item,
            ctx:ctx,
            registerView:true
        }, null, parent, true);

    }

    /*
     * playground
     */

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        marantz,
        doTest = true;


    if (ctx && doTest) {

        testJsonEditor();
        return declare('a',null,{});
    }





    return jsonEditor;


});