/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "./TestUtils",
    'xide/_base/_Widget',
    "module"
], function (dcl,declare,types,utils,
             Grid, TestUtils,_Widget,module) {


    console.clear();

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;



    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;

    

    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {



        var blockManager = ctx.getBlockManager();
        var driverManager = ctx.getDriverManager();
        var deviceManager = ctx.getDeviceManager();

        var parent = TestUtils.createTab(null,null,module.id);
        var widget = dcl(_Widget,{
            templateString:'<div>' +
            '<div attachTo="select"></div>'+
            '</div>',
            sources:null,
            options:null,
            startup:function(){


                var _t = $(this.select).selectize({

                    maxItems: 1,
                    valueField: 'id',
                    labelField: 'title',
                    searchField: 'title',
                    options: [
                        {id: 1, title: 'Spectrometer', url: 'http://en.wikipedia.org/wiki/Spectrometers'},
                        {id: 2, title: 'Star Chart', url: 'http://en.wikipedia.org/wiki/Star_chart'},
                        {id: 3, title: 'Electrical Tape', url: 'http://en.wikipedia.org/wiki/Electrical_tape'}
                    ],
                    create: true,
                    createOnBlur:true,
                    addPrecedence:true,
                    plugins: ['restore_on_backspace'],
                    persist: false
                });
                var _d  = _t.data();// 'selectize'
            }
        });

        var options = {

        }

        utils.addWidget(widget,{
            options:options
        },null,parent,true);


        return declare('a',null,{});

    }

    return Grid;

});