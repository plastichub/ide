/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    'xide/views/CIGroupedSettingsView',
    'xide/views/_CIDialog',
    'xide/widgets/WidgetBase',
    'xide/widgets/ActionToolbar',
    'xaction/ActionProvider',
    'xide/tests/TestUtils',
    'xide/views/_LayoutMixin',
    'module'

], function (dcl,declare, types,
             utils, Grid, factory,CIGroupedSettingsView,CIActionDialog,
             WidgetBase,ActionToolbar,ActionProvider,TestUtils,_LayoutMixin,module

    ) {

    console.clear();

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;



    function createDriverCIS(driver,actionTarget){

        var CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID",
                    "name": "CF_DRIVER_ID",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": "Arguments",
                    "uid": "-1",
                    "value": "{}",
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General9',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 13,
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true
                }
            ]
        };

        _.each(CIS.inputs,function(ci){

        });
        return CIS;
    }
    function createArgumentsWidget2(){


        var ArgumentsWidget = dcl([WidgetBase,_LayoutMixin.dcl,ActionProvider.dcl], {
            declaredClass: "xide.widgets.ArgumentsWidget",
            wContentSelectorButton: null,
            lastUploadedFileName: null,
            oriPath: null,
            minHeight: "400px;",
            value: "",
            options: null,
            templateString: "<div attachTo='domNode' class='widgetContainer widgetBorder widgetTable' style=''>" +

            "</div>",
            filePathValidator: function (value, constraints) {
                return true;
            },
            onSelect: function () {
                var thiz = this;
                var _defaultOptions = {};

                if (this.options) {
                    utils.mixin(_defaultOptions, this.options);
                }


                var value = utils.toString(this.userData['value']);
                value = {
                    a:3
                }
                var actionDialog = new CIActionDialog({
                    title: 'Arguments',
                    style: 'width:400px;height:400px;',
                    resizeable: true,
                    delegate: {
                        onOk: function (dlg, data) {
                            var expression = utils.getCIInputValueByName(data, 'Arguments');
                            thiz.userData.changed = true;
                            thiz.userData.active = true;
                            utils.setCIValueByField(thiz.userData, "value", expression);
                            thiz.editBox.set('value', expression);
                        }
                    },
                    cis: [
                        utils.createCI('Arguments', types.ECIType.JSON_DATA, value, {
                            group: 'Arguments'
                        })
                    ]
                });


                this.add(actionDialog,null,false);
                actionDialog.show();
            },

            fillTemplate: function () {


                var thiz = this;

                var value = utils.toString(this.userData['value']) || '{}';


                var area = $('<textarea rows="3" class="form-control input-transparent" ></textarea>');
                area.val(value);
                this.editBox = area;
                $(this.previewNode).append(area);
                this.editBox.on("change", function (e) {

                    var value2 = e.target.value;

                    thiz.userData.changed = true;
                    thiz.userData.active = true;

                    utils.setCIValueByField(thiz.userData, "value", value2);

                    var _args = {
                        owner: thiz.delegate || thiz.owner,
                        ci: thiz.userData,
                        newValue: value2,
                        storeItem: thiz.storeItem
                    }
                    thiz.publish(types.EVENTS.ON_CI_UPDATE, _args);
                    thiz._emit('valueChanged', _args);
                });

                var btn = factory.createSimpleButton('', 'fa-magic', 'btn-default', {
                    style: ''
                });

                $(btn).click(function () {
                    thiz.onSelect();
                })

                $(this.button0).append(btn);
            },
            jsonEditorWidget:null,
            editorWidget:null,
            createWidgets:function(bottom,top,center){

                var data = this.userData,
                    valueStr = JSON.stringify(data.value,null,2),
                    docker = this.getDocker(),
                    thiz = this;


                console.log('init with data',top);

                data.value = {
                    commands:[],
                    variables:[],
                    "xxas": {
                        "power_on": [140, 0, 0, 2, 1, 143],
                        "power_off": [140, 0, 0, 2, 0, 142],
                        "input_hdmi1": [140, 0, 2, 3, 4, 1, 150],
                        "input_hdmi2": [140, 0, 2, 3, 4, 2, 151],
                        "input_hdmi3": [140, 0, 2, 3, 4, 3, 152],
                        "input_hdmi4": [140, 0, 2, 3, 4, 4, 153],
                        "volume_up": [140, 0, 5, 3, 0, 0, 148],
                        "volume_down": [140, 0, 5, 3, 0, 1, 149],
                        "volume_set": [140, 0, 5, 3, 1, "n", 0],
                        "mute": [140, 0, 6, 3, 1, 1, 151],
                        "unmute": [140, 0, 6, 3, 1, 0, 150]
                    }
                };






                var CIS_TOP = [
                    utils.createCI('Arguments', types.ECIType.JSON_DATA, data.value, {
                        options:{
                            readOnlyNodes:{
                                "commands":true,
                                "variables":true,
                                "meta":true
                            },
                            renderTemplates : [
                                {
                                    //
                                    //  This segment is used to replace something in the node's dom structure
                                    //

                                    /**
                                     * @type {string} the path within the dom structure
                                     */
                                    nodeValuePath: 'field.innerHTML',
                                    /**
                                     * @type {RegExp|string|function|RegExp[]|string[]|function[]}
                                     */
                                    match: [/^variables[\s]?\.(\d+)$/,/^commands[\s]?\.(\d+)$/],
                                    /**
                                     * @type {string} the new value for the field specified in nodeValuePath
                                     */
                                    replaceWith: '{nodeValue} - {name}',
                                    /**
                                     * @type {object} additional variables
                                     */
                                    variables: null,

                                    /**
                                     * @type {function} a function to transform the node's dom value into something else
                                     */
                                    nodeValueTransform:function(value){
                                        return utils.capitalize(value);
                                    },
                                    //
                                    //  This segment is about dom manipulation, todo!
                                    //

                                    /**
                                     * @type (object)
                                     */
                                    insertIfMatch:{}
                                }
                            ],
                            insertTemplates:[
                                {
                                    label: 'New Command',
                                    path: 'commands',
                                    value: '{title:"No Title",value:""}',
                                    newNodeTemplate: '[]',
                                    collapse:true,
                                    select:true
                                },
                                {
                                    label: 'New Variable',
                                    path: 'variables',
                                    value: '{title:"No Title",value:""}',
                                    newNodeTemplate: '[]',
                                    collapse:true,
                                    select:true
                                }
                            ]
                        },
                        widget:{
                            templateString:"<div class='widgetContainer widgetBorder widgetTable widget' style=''>" +
                            "<div class='widget jsonEditorWidget' attachTo='valueNode'></div>" +
                            "</div>"

                        }
                    })
                ];





                factory.renderCIS(CIS_TOP, top.containerNode, this).then(function (widgets) {
                    thiz.jsonEditorWidget = widgets[0];
                    var toolbar = utils.addWidget(ActionToolbar,{
                        attachToGlobal:false,
                        resizeToParent:false
                    },null,center,true);

                    thiz.add(toolbar,null,true);
                    var actionStore = thiz.getActionStore();
                    var actions = thiz.jsonEditorWidget.editor.getItemActions();
                    _.each(actions,function(action){
                        action.mixin = {
                            addPermission:true
                        },
                        action.tab = "Home"
                    });
                    actions = thiz.addActions(actions);
                    console.dir(actions);
                    toolbar.setActionStore(actionStore,thiz);

                    $(toolbar.domNode).css({
                        width:'auto',
                        'float':'left'
                    });
                    $(toolbar.domNode).removeClass('actionToolbar');






                    //console.error(thiz.editorWidget);
                });

                var CIS_BOTTOM = [
                    utils.createCI('Arguments', types.ECIType.SCRIPT,valueStr,{
                        vertical:true,
                        widget:{
                            templateString: "<div class='' style='width: 100%;height:inherit'>",
                            height:'100%',
                            editorArgs:{
                                showGutter:true,
                                mode:'json',
                                options:{
                                    showGutter:true,
                                    mode:'json'
                                }
                            }
                            //resizeToParent:true
                        }
                    })
                ];

                var scriptWidget = null;
                factory.renderCIS(CIS_BOTTOM, bottom.containerNode, this).then(function (widgets) {
                    thiz.editorWidget = widgets[0];

                });



                var btn = factory.createSimpleButton('', 'fa-caret-up', 'btn-default', {
                    style: 'margin-right:12px'
                });
                var btnDown = factory.createSimpleButton('', 'fa-caret-down', 'btn-default', {
                    style: ''
                });

                $(center.containerNode).append(btn);
                $(center.containerNode).append(btnDown);

                $(btn).click(function () {
                    thiz.onUp();
                })
                $(btnDown).click(function () {
                    thiz.onDown();
                })

            },
            onUp:function(){


                var valueDown = this.editorWidget.getValue();
                var valueUp = this.jsonEditorWidget.getValue();
                try{
                    var newData = dojo.fromJson(valueDown);
                     this.jsonEditorWidget.setData(newData)
                }catch(e){
                    console.error('mal formed '+e);
                }
            },
            onDown:function(){
                var valueDown = this.editorWidget.getValue();
                var valueUp = this.jsonEditorWidget.getValue();
                this.editorWidget.aceEditor.set('value',valueUp);

            },
            startup: function () {

                var title = false;

                /*
                var top = this._addPanel(utils.mixin({
                    h: '100',
                    title: title || '  ',
                },{}), types.DOCKER.DOCK.TOP,title,0.5,'DefaultFixed',null);
                */

                var docker = this.getDocker();

                var toolbar = this._addPanel(utils.mixin({
                    h: '100',
                    title: title || '  '
                },{}), types.DOCKER.DOCK.TOP,title,0.5,'DefaultFixed');

                var bottom = this.getBottomPanel(false,null,null,null,null);

                var panels = docker.getPanels();

                var _top = panels[1],
                    _center = panels[0],
                    _bottom = panels[2];


                this.createWidgets(_bottom,_top,_center);
                _center.maxSize(null,58);
                _bottom.getSplitter().pos(0.5);
                $(_center.containerNode).css({
                    "textAlign":"center"
                })

                /*
                var bottom = this.getBottomPanel('Console'),
                    top = this.getTop();
                */

                return;

                try {
                    this.fillTemplate();
                    this.onReady();
                }catch(e){
                    logError(e);
                }
            }
        });

        return ArgumentsWidget;
    }
    function testJsonWidget() {

        var toolbar = ctx.mainView.getToolbar();

        var docker = ctx.mainView.getDocker();

        var parent = TestUtils.createTab(null, null, module.id);


        var view = utils.addWidget(CIGroupedSettingsView, {
            title:  'title',
            typeMap:null,
            getTypeMap:function(){
                if (this.typeMap) {
                    return this.typeMap;
                }

                var self = this;
                var typeMap = {}
                typeMap['ArgumentsWidget2'] = createArgumentsWidget2();
                this.typeMap = typeMap;
                return typeMap;
            },
            cis: [
                utils.createCI('Arguments', 13, 'asd', {
                    group: 'Arguments2'
                }),
                utils.createCI('Arguments', 'ArgumentsWidget2', {a:2}, {
                    group: 'Arguments',
                    type:'ArgumentsWidget2'
                })
            ],
            storeDelegate: this,
            iconClass: 'fa-eye',
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'General'
            }
        }, null, parent, true);


    }
/*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return blockManager.toScope(data);
    }

    if (ctx) {
        blockManager = ctx.getBlockManager();
        testJsonWidget();
        return declare('a',null,{});
    }
    return Grid;

});