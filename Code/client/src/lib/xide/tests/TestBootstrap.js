/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/dom-class',
    'dojo/dom-construct',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/ThumbRenderer',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xide/action/DefaultActions',
    'xide/action/Action',
    "xblox/widgets/BlockGridRowEditor",
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    'dijit/Menu',
    'xide/data/Reference',
    'dijit/form/DropDownButton',
    'dijit/MenuItem',
    'xdocker/Docker',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    "dojo/has!host-browser?xblox/views/BlockEditDialog",
    'xblox/views/BlockGrid',
    'xgrid/DnD',
    'xblox/views/BlocksGridDndSource',
    'xblox/widgets/DojoDndMixin',
    'xide/registry',
    'dojo/topic',

    'xide/views/CIGroupedSettingsView',
    'xide/widgets/WidgetBase',

    'xide/views/_LayoutMixin',

    'xcf/model/Command',
    'xcf/model/Variable',

    'xide/widgets/ToggleButton',
    'xide/widgets/_ActionValueWidgetMixin',

    'xide/layout/AccordionContainer',
    "dojo/store/Memory",

    "xide/widgets/TemplatedWidgetBase",


    "dijit/form/TextBox",
    "dijit/form/CheckBox",
    'dijit/form/ValidationTextBox'



], function (declare, domClass,domConstruct,types,
             utils, ListRenderer, TreeRenderer, ThumbRenderer,
             _ActionMixin,
             Grid, MultiRenderer, RadioButton, Ribbon, Registry, DefaultActions, Action,BlockGridRowEditor,
             Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Menu,Reference,DropDownButton,
             MenuItem,Docker,CIViewMixin,TabContainer,

             BlockEditDialog,
             BlockGrid,
             Dnd,BlocksGridDndSource,DojoDndMixin,registry,topic,
             CIGroupedSettingsView,
             WidgetBase,_LayoutMixin,Command,Variable,
             ToggleButton,_ActionValueWidgetMixin,AccordionContainer,Memory,
             TemplatedWidgetBase,

             TextBox, CheckBox, ValidationTextBox

    ) {




    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;




    var propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };




    function createCommandSettingsWidget(){


        var _class = declare("xcf.widgets.CommandSettings2", [WidgetBase,_LayoutMixin], {

            templateString:'<div style="width: inherit;height: 100%;"><div>',
            _docker:null,
            grids:null,
            getDocker:function(){
                if(!this._docker){
                    this._docker = Docker.createDefault(this.domNode,{
                        allowContextMenu:false
                    });
                    this._docker.$container.css('top',0);
                }
                return this._docker;
            },
            createWidgets:function(){




                var docker = this.getDocker(this.domNode),

                    ci = this.userData,

                    driver = ci.driver,

                    scope = driver.blockScope,

                    store = scope.blockStore,

                    //variableStore = scope.variableStore,

                    instance = driver.instance,

                    widget = this,


                    defaultTabArgs = {
                        icon:false,
                        closeable:false,
                        movable:false,
                        moveable:true,
                        tabOrientation:types.DOCKER.TAB.TOP,
                        location:types.DOCKER.DOCK.STACKED
                    },

                    // 'Basic' commands tab
                    basicCommandsTab = docker.addTab('DefaultFixed',
                        utils.mixin(defaultTabArgs,{
                            title: 'Basic Commands',
                            h:'90%'
                        })),

                    // 'Conditional' commands tab
                    condCommandsTab = docker.addTab('DefaultFixed',
                        utils.mixin(defaultTabArgs,{
                            title: 'Conditional Commands',
                            target:basicCommandsTab,
                            select:false,
                            h:'90%',
                            tabOrientation:types.DOCKER.TAB.TOP
                        }));


                    // 'Variables' tab
                    var variablesTab = docker.addTab(null,
                        utils.mixin(defaultTabArgs,{
                            title: 'Variables',
                            //target:condCommandsTab,
                            select:false,
                            h:100,
                            tabOrientation:types.DOCKER.TAB.BOTTOM,
                            location:types.DOCKER.TAB.BOTTOM
                        }));


                var group = 'basic';

                var logsTab = null;
/*
                var logsTab = docker.addTab(null,
                    utils.mixin(defaultTabArgs,{
                        title: 'Log',
                        target:variablesTab,
                        select:false,
                        tabOrientation:types.DOCKER.TAB.BOTTOM,
                        location:types.DOCKER.DOCK.STACKED
                    }));
                */

                //logsTab.initSize('100%','10%');

                //variablesTab.getFrame().showTitlebar(false);










                // prepare right property panel but leave it closed
                this.getRightPanel(null,1);

                // default grid args
                var gridArgs = {
                    _getRight:function(){
                        return widget.__right;
                    },
                    ctx:ctx,
                    blockScope: scope,
                    blockGroup: 'basic',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'basic'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    __right:this.__right,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition
                };


                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //basic commands

                grid = utils.addWidget(createGridClass(),gridArgs,null,basicCommandsTab,false);
                completeGrid(grid,'Command');

                docker._on(types.DOCKER.EVENT.SPLITTER_POS_CHANGED,function(evt){

                    var position = evt.position,
                        splitter = evt.splitter,
                        right = widget.__right;

                    if(right && splitter == right.getSplitter()){
                        if(position<1){
                            grid.showProperties(grid.getSelection()[0],true);
                        }
                    }
                });


                //run some actions
                var cmdAction = grid.getAction('New/Command'),
                    reloadAction = grid.getAction('File/Reload');

                grid.openRight(true);
                /*
                grid.select([0],null,true,{
                    focus:true
                });
                */
                this.grids.push(grid);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //conditional commands
                var gridArgs2 = {
                    ctx:ctx,
                    _getRight:function(){
                        return widget.__right;
                    },
                    blockScope: scope,
                    blockGroup: 'conditional',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'conditional'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    __right:this.__right,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition
                }

                var grid2 = utils.addWidget(createGridClass(),gridArgs2,null,condCommandsTab,false);

                completeGrid(grid2,'Command');

                this.grids.push(grid2);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //variables
                var group = 'basicVariables';

                var gridArgs3 = {
                    _getRight:function(){
                        return widget.__right;
                    },
                    ctx:ctx,
                    blockScope: scope,
                    blockGroup: 'basicVariables',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'basicVariables'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition,
                    showHeader:true,
                    columns:[
                        {
                            label: "Name",
                            field: "title",
                            sortable: true,
                            editorArgs: {
                                required: true,
                                promptMessage: "Enter a unique variable name",
                                //validator: thiz.variableNameValidator,
                                //delegate: thiz.delegate,
                                intermediateChanges: false
                            },
                            _editor: ValidationTextBox
                        },
                        {
                            label: "Initialize",
                            field: "initialize",
                            sortable: false,
                            editor: TextBox,
                            editOn:'click'
                        },
                        {
                            label: "Value",
                            field: "value",
                            sortable: false,
                            editor: TextBox,
                            editOn:'click',
                            editorArgs: {
                                autocomplete:'on',
                                templateString:'<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"'+
                                                '><div class="dijitReset dijitInputField dijitInputContainer"'+
                                                '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"'+
                                                '${!nameAttrSetting} type="${type}"'+
                                                '/></div'+
                                                '></div>'
                            }
                        }
                    ]
                };





                var grid3 = utils.addWidget(createGridClass(),gridArgs3,null,variablesTab,false);
                completeGrid(grid3,'Variable');


                domClass.add(grid3.domNode,'variableSettings');


                grid3.on("dgrid-datachange", function (evt) {

                    var cell = evt.cell;

                    //normalize data
                    var item = null;
                    if (cell && cell.row && cell.row.data) {
                        item = cell.row.data;
                    }
                    var id = evt.rowId;
                    var oldValue = evt.oldValue;
                    var newValue = evt.value;

                    var data = {
                        item: item,
                        id: id,
                        oldValue: oldValue,
                        newValue: newValue,
                        grid: grid3,
                        field: cell.column.field
                    };

                    if (item) {
                        item[data.field] = data.newValue;
                    }




                });

                this.grids.push(grid3);
                //variablesTab.select();
                docker.resize();
                grid3.refresh();
                basicCommandsTab.select();
                docker.resize();

                setTimeout(function(){

                    /*variablesTab.getFrame().showTitlebar(true);*/

                    function hide(who,show) {

                        var prop = show ? 'inherit' : 'none',
                            what = 'display';

                        if(who.$titleBar) {
                            who.$titleBar.css(what, prop);
                            who.$center.css('top', show ? 30 : 0);
                        }
                    }

                    hide(variablesTab.getFrame(),false);
                    variablesTab.getSplitter().pos(0.6);
                    variablesTab.select();
                    onWidgetCreated(basicCommandsTab,condCommandsTab,variablesTab,logsTab);
                },10);

            },
            startup:function(){

                this.inherited(arguments);
                var thiz = this;
                this.grids = [];
                /*setTimeout(function(){*/
                thiz.createWidgets();
                var ci = this.userData;
                if(ci.actionTarget){
                    _.each(this.grids,function(grid){
                        ci.actionTarget.addActionEmitter(grid);
                    });
                }
                /*},1000);*/
            }

        });


        dojo.setObject('xcf.widgets.CommandSettings2',_class);


        return _class;
    }


    function getFileActions(permissions) {



        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();


        return [];

        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            var action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        var who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }

        /*
         var rootAction = 'Block/Insert';
         permissions.push(rootAction);
         addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         dummy: true,
         onCreate: function (action) {
         action.setVisibility(VISIBILITY.CONTEXT_MENU, {
         label: 'Add'
         });

         }
         }, null, null);
         permissions.push('Block/Insert Variable');


         addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         }, null, null);
         */

        /*
         permissions.push('Clipboard/Paste/New');
         addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
         }, null, null);*/


        var newBlockActions = this.getAddActions();
        var addActions = [];
        var levelName = '';


        function addItems(commandPrefix, items) {

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                levelName = item.name;


                var path = commandPrefix + '/' + levelName;
                var isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }
        //console.clear();
        //addItems(rootAction, newBlockActions);
        //return result;


        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            var canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

            return !canMove;

        }


        function canParent(selection, reference, visibility) {

            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            if(!item){
                console.warn('bad item',selection);
                return false;
            }

            if(this.command === 'Step/Move Left'){
                return !item.getParent();
            }else{
                return item.getParent();
            }
            /*
             var canMove = item.canMove(item, this.command === 'Step/Move Left' ? -1 : 1);
             return !canMove;*/

            return true;

        }

        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */

        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    widgetArgs:{
                        label: ' ',
                        style:'font-size:25px!important;'
                    }
                });

            }
        }, null, isItem);
        permissions.push('Step/Run/From here');

        /**
         * run
         */

        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {

            onCreate: function (action) {

            }
        }, null, isItem);



        /**
         * move
         */

        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['alt up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);


        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['alt down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);
        /*


         permissions.push('Step/Edit');
         addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
         */
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        permissions.push('Step/Move Left');
        permissions.push('Step/Move Right');

        addAction('Move Left', 'Step/Move Left', 'fa-arrow-left', ['alt left'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canParent);

        addAction('Move Right', 'Step/Move Right', 'fa-arrow-right', ['alt right'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });


            }
        }, null, canParent);


        return result;

    }

    function doTests(){
    }


    var createDelegate = function(){

        return {

        }


    }


    function openDriverSettings(driver){


        //createCommandSettingsWidget();


        var toolbar = ctx.mainView.getToolbar();


        CIS = createDriverCIS(driver,toolbar);



        var docker = ctx.mainView.getDocker();
        var parent  = window.driverTab;
        if(parent){
            docker.removePanel(parent);
        }

        parent = docker.addTab(null, {
            title: 'Marantz/Marantz',
            icon: 'fa-exchange'
        });

        window.driverTab = parent;


        var cisRenderer = declare('cis',[CIGroupedSettingsView,CIViewMixin],{

            createGroupContainer: function () {

                if (this.tabContainer) {
                    return this.tabContainer;
                }
                var tabContainer = utils.addWidget(TabContainer,{
                    tabStrip: true,
                    tabPosition: "left-h",
                    splitter: true,
                    style: "min-width:450px;",
                    "className": "ui-widget-content"
                },null,this.containerNode,true);
                this.tabContainer = tabContainer;
                return tabContainer;
            },
            attachWidgets: function (data, dstNode,view) {

                var thiz = this;
                this.helpNodes = [];
                this.widgets = [];

                dstNode = dstNode || this.domNode;
                if(!dstNode && this.tabContainer){
                    dstNode = this.tabContainer.containerNode;
                }
                if (!dstNode) {
                    console.error('have no parent dstNode!');
                    return;
                }


                for (var i = 0; i < data.length; i++) {
                    var widget = data[i];
                    widget.delegate = this.owner || this;
                    dstNode.appendChild(widget.domNode);
                    widget.startup();

                    widget._on('valueChanged',function(evt){
                        thiz.onValueChanged(evt);
                    });

                    this.widgets.push(widget);
                    widget.userData.view=view;
                }
            },
            initWithCIS: function (data) {


                this.empty();

                data = utils.flattenCIS(data);

                this.data = data;

                var thiz = this,
                    groups = _.groupBy(data,function(obj){
                        return obj.group;
                    }),
                    groupOrder = this.options.groupOrder || {};

                groups = this.toArray(groups);

                var grouped = _.sortByOrder(groups, function(obj){
                    return groupOrder[obj.name] || 100;
                });

                if (grouped != null && grouped.length > 1) {
                    this.renderGroups(grouped);
                } else {
                    this.widgets = factory.createWidgetsFromArray(data, thiz, null, false);
                    if (this.widgets) {
                        this.attachWidgets(this.widgets);
                    }
                }
            }
        });


        var view = utils.addWidget(CIGroupedSettingsView, {
            title:  'title',
            cis: driver.user.inputs,
            storeItem: driver,
            delegate: createDelegate(),
            storeDelegate: this,
            iconClass: 'fa-eye',
            blockManager: ctx.getBlockManager(),
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'General'
            }
        }, null, parent, true);


        docker.resize();
        view.resize();
    }

/*
     * playground
     */
    var _lastGrid = window._lastBoot;
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;



    var _actions = [
        ACTION.RENAME
    ];

    function fixScope(scope){

        return scope;

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function(source,target,before,add){




            console.log('scope::move, add: ' +add,arguments);

            if(!add){
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if(add){

                //remove it from the source parent and re-parent the source
                if(target.canAdd && target.canAdd()){

                    var sourceParent = this.getBlockById(source.parentId);
                    if(sourceParent){
                        sourceParent.removeBlock(source,false);
                    }
                    target.add(source,null,null);
                    return;
                }else{
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if(!target.parentId && add==false){

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if(sourceParent && sourceParent.removeBlock){
                    sourceParent.removeBlock(source,false);
                    source.parentId=null;
                    source.group=target.group;
                }

                var itemsToBeMoved=[];
                var groupItems = this.getBlocks({
                    group:target.group
                });

                var rootLevelIndex=[];
                var store = this.getBlockStore();

                var sourceIndex = store.storage.index[source.id];
                var targetIndex = store.storage.index[target.id];
                for(var i = 0; i<groupItems.length;i++){

                    var item = groupItems[i];
                    //keep all root-level items

                    if( groupItems[i].parentId==null && //must be root
                        groupItems[i]!=source// cant be source
                    ){

                        var itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if(add){
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if(before){
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if(!before){
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            }else if( !source.parentId && target.parentId && add==false){
                source.group = target.group;
                if(target){

                }

                //we move from root to into root item
            }else if( !source.parentId && !target.parentId && add){

                console.error('we are adding an item into root root item');
                if(target.canAdd && target.canAdd()){
                    source.group=null;
                    target.add(source,null,null);
                }
                return true;

                // we move within the same parent
            }else if( source.parentId && target.parentId && add==false && source.parentId === target.parentId){
                console.error('we move within the same parents');
                var parent = this.getBlockById(source.parentId);
                if(!parent){
                    console.error('     couldnt find parent ');
                    return false;
                }

                var maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items,source);
                var cIndexTarget = source.indexOf(items,target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    parent.move(source,direction);
                }
                return true;

                // we move within the different parents
            }else if( source.parentId && target.parentId && add==false && source.parentId !== target.parentId){                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if(!sourceParent){
                    console.error('     couldnt find source parent ');
                    return false;
                }

                var targetParent = this.getBlockById(target.parentId);
                if(!targetParent){
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if(sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()){
                    sourceParent.removeBlock(source,false);
                    targetParent.add(source,null,null);
                }else{
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if(items==null){
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items,source);
                var cIndexTarget = targetParent.indexOf(items,target);
                if(!cIndexSource || !cIndexTarget){
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    targetParent.move(source,direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        var topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId:null
        });


        var grouped = _.groupBy(blocks,function(block){
            return block.group;
        });

        function createDummyBlock(id,scope){

            var block = {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": null,
                    "id": id,
                    "items": [

                    ],
                    "name": id,
                    "method": "----group block ----",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": false,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"

                };

            return scope.blockFromJson(block);

        }

        for(var group in grouped){

            var groupBlock = createDummyBlock(group,scope);
            var blocks = grouped[group];
            _.each(blocks,function(block){
                groupBlock['items'].push(block);



                if(!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }

        console.clear();
        var root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }


    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }

    if (ctx) {



        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();
        //marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        //scope = createScope();

        //openDriverSettings(marantz);

        var toolbar = ctx.mainView.getToolbar();
        var docker = ctx.mainView.getDocker();
        var parent  = window.bootTab;
        if(parent){
            docker.removePanel(parent);
        }

        parent = docker.addTab(null, {
            title: 'Marantz/Marantz',
            icon: 'fa-exchange'
        });

        window.bootTab = parent;

















        var _accHTML = require.toUrl('xide/tests/accordion_test.html');

        var _accHTML = require.toUrl('xide/tests/breadcrumb_test.html');

        var _accHTML = require.toUrl('xide/tests/acc_test.html');










        function _getText(url) {
            var result;
            var def = dojo.xhrGet({
                url: url,
                sync: true,
                handleAs: 'text',
                load: function (text) {
                    result = text;
                }
            });
            return '' + result + '';
        }
        var template = _getText(_accHTML);
        var widget = declare('x',TemplatedWidgetBase,{
            templateString:template
        });







        utils.addWidget(widget,{},null,parent,true);


        return declare('a',null,{});


        //return createCommandSettingsWidget();


        function createGridClass() {

            var renderers = [TreeRenderer];

            //, ThumbRenderer, TreeRenderer


            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);




            var _gridClass = Grid.createGridClass('driverTreeView',
                {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT:{
                        CLASS:declare('splitMixin',null,_layoutMixin)
                    },
                    DND:{
                        CLASS:Dnd//declare('splitMixin',Dnd,_dndMixin)
                    }

                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );



            return declare('gridFinal', BlockGrid, _gridBase);

            //return BlockGrid;
        }

        var blockScope = createScope('docs');


        var mainView = ctx.mainView;

        var docker = mainView.getDocker();

        if (mainView) {

            if (_lastGrid) {

                docker.removePanel(_lastGrid);
            }

            var parent = docker.addTab(null, {
                title: 'blox',
                icon: 'fa-folder'
            });
            window._lastGrid = parent;





            var store = blockScope.blockStore;


            var _gridClass = createGridClass();

            var gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                collection: store.filter({
                    group: "click"
                }),
                dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                dndParams: {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                }
            };


            grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);

            var blocks = blockScope.allBlocks();

            var root = blocks[0];


            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();

            var _defaultActions = [];

            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [grid.permissions]));

            grid.addActions(_defaultActions);



            if (!toolbar) {
                window._lastRibbon = ribbon = toolbar = utils.addWidget(Ribbon, {
                    store: actionStore,
                    flat: true,
                    currentActionEmitter: grid
                }, this, mainView.layoutTop, true);

            } else {
                toolbar.addActionEmitter(grid);
                toolbar.setActionEmitter(grid);
            }



            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {

                expand.apply(grid);
                return;
            }

            setTimeout(function () {
                test();
            }, 1000);
        }
    }

    return Grid;

});