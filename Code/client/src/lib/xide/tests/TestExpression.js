/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/Grid',
    "./TestUtils",
    'xide/_base/_Widget',
    "module",
    "xide/views/CIViewMixin",
    'xide/views/CIGroupedSettingsView',
    'xide/views/_CIDialog',
    'xide/widgets/WidgetBase',
    'xide/widgets/_CacheMixin',
    'xide/views/_Panel',
    'xide/views/CIView',
    "xcf/views/ExpressionConsole",
    'xide/registry',
    'xgrid/ListRenderer',
    'xgrid/Defaults',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'xide/views/ConsoleView',
    "xide/data/TreeMemory",
    'xide/views/_CIPanelDialog',
    'xide/widgets/TemplatedWidgetBase',
    'xblox/model/Expression',
    'xaction/ActionProvider'

], function (dcl,declare,types,utils,factory,
             Grid, TestUtils,_Widget,module,CIViewMixin,
             CIGroupedSettingsView,CIActionDialog,
             WidgetBase,_CacheMixin,_Panel,CIView,ExpressionConsole,registry,
             ListRenderer, Defaults, Focus,OnDemandGrid, EventedMixin, _LayoutMixin,Docker,ConsoleView,TreeMemory,_CIPanelDialog,TemplatedWidgetBase,Expression,ActionProvider) {


    console.clear();
    console.log('--do-tests');
    var EditorClass = dcl(ConsoleView.Editor,{
        didAddMCompleter:false,
        multiFileCompleter:null,
        blockScope:null,
        driverInstance:null,
        onACEReady: function (editor) {

            return;

            var add = true,
                scope = this.blockScope,
                driverInstance = this.driverInstance,
                driver = this.driver,
                device = this.device,
                variables = scope.getVariables({
                    group: 'basicVariables'
                }),
                _responseVariables = scope.getVariables({
                    group: 'processVariables'
                });


            if (_responseVariables && _responseVariables.length) {
                variables = variables.concat(_responseVariables);
            }
            var completors = [],
                path = driver.path;

            function createCompleter(text, value, help) {
                return {
                    word: text,
                    value: value,
                    meta: help || ''
                };
            }

            if (variables && variables.length > 0) {
                for (var i = 0; i < variables.length; i++) {
                    var obj = variables[i];

                    completors.push(createCompleter(obj.name, obj.value, 'Driver Variable'));

                }
            }
            for(var prop in driverInstance){
                var val = driverInstance[prop];
                if(_.isFunction(val)){
                    completors.push(createCompleter(prop, prop, 'Driver Method'));
                }else {

                    if (_.isString(val) || _.isObject(val)) {
                        completors.push(createCompleter(prop, prop, 'Driver Property'));
                    }
                }
                /*
                 if(_.isFunction(val) || _.isString(val) || _.isFunction(val)){
                 completors.push(createCompleter(prop, prop, 'Driver Method'));
                 }
                 */
            }
            return completors.length && this.addAutoCompleter(completors);

            var completer = this.addFileCompleter();
            var text = "var xxxTop = 2*3;";
            var path = driver.path;

            var _text = '';

            for(var prop in driverInstance){

                var val = driverInstance[prop];
                if(_.isFunction(val)){

                    _text +='\n';
                    _text +=prop;
                    _text +=':';
                    _text += val.toString();
                    _text +=',\n';
                }
            }
            //var _text = driverInstance.toString();
            completer.addDocument(path,_text);


        },
        onEditorCreated:function(editor,options){
            this.inherited(arguments);
            this.onACEReady(editor);
        }

    });
    var ConsoleClass = dcl(ConsoleView,{
        EditorClass:EditorClass,
        onAddEditorActions:dcl.superCall(function(sup){
            return function(evt){
                //grab the result from the handler
                var res = sup.call(this, evt),
                    thiz = this,
                    actions = evt.actions,
                    owner  = evt.owner;

                actions.remove(_.find(actions,{
                    command:'File/Save'
                }));

                actions.remove(_.find(actions,{
                    command:"File/Reload"
                }));
            };
        }),
        logTemplate:'<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>',
        _parse : function (scope, expression,errorCB) {
            var str = '' + expression;
            if (str.indexOf('{{') > 0 || str.indexOf('}}') > 0) {
                str = _parser.parse(types.EXPRESSION_PARSER.FILTREX,
                    str, this,
                    {
                        variables: scope.getVariablesAsObject(),
                        delimiters: {
                            begin: '{{',
                            end: '}}'
                        }
                    }
                );
            }else{
                var _text = scope.parseExpression(expression,null,null,null,errorCB);
                if(_text){
                    str = _text;
                }
            }
            return str;
        },
        parse:function(str,errorCB) {
            var driverInstance = this.driverInstance;
            if (driverInstance && driverInstance.blockScope) {
                return this._parse(driverInstance.blockScope, str,errorCB);
            }
            return str;
        }
    });
    var _ExpressionEditor = dcl([_Widget,_LayoutMixin.dcl,ConsoleView.HandlerClass,ActionProvider.dcl],{
        type:'javascript',
        reparent:false,
        consoleTabTitle:'Editor',
        consoleTabOptions:null,
        consoleTabType:'DefaultTab',
        templateString: '<div class="" style="height:inherit"/>',
        showHelp:false,
        showAPIBrowser:true,
        onAddEditorActions:function(evt){
            var actions  = evt.actions,
                owner  = evt.owner;
            actions.push(owner.createAction({
                label: 'Send',
                command: 'Console/Send',
                icon: 'fa-paper-plane',
                group: 'Console',
                tab:'Home',
                mixin:{
                    addPermission:true
                }
            }));
        },
        getConsoleClass:function(){
            return ConsoleView.ConsoleWidget;
        },
        showDocumentation: function (text) {
            if(!this.helpContainer){
                return;
            }
            dojo.empty(this.helpContainer);
            var help = dojo.create('div', {
                innerHTML: '',
                className: 'ui-widget'
            });
            this.helpContainer.appendChild(help);
            help.appendChild(dojo.create('span', {
                innerHTML: text,
                className: 'ui-content'
            }));
        },
        createTab:function(type,args){
            return this._docker.addTab(type || 'DefaultTab',utils.mixin({
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:types.DOCKER.TAB.TOP,
                location:types.DOCKER.DOCK.STACKED
            },args));
        },
        createBranch: function (name, where) {
            var branch = {
                name: name,
                group: 'top',
                children: [],
                value: null
            };
            where.push(branch);
            return branch;
        },
        apiItemToLeaf: function (item) {},
        addAutoCompleterWord: function (text, help) {
            if (!this.autoCompleterWords) {
                this.autoCompleterWords = [];
            }
            this.autoCompleterWords.push({
                word: text,
                value: text,
                meta: help || ''
            });
        },
        createLeaf: function (label, value, parent, help, isFunction, where) {
            var leaf = {
                name: label,
                group: 'leaf',
                value: value,
                parent: parent,
                help: help,
                isFunction: isFunction != null ? isFunction : true
            };
            this.addAutoCompleterWord(value, help);
            if (where) {
                where.children.push({
                    _reference: label
                })
            }
            return leaf;
        },
        getAPIField: function (item, title) {
            for (var i = 0; i < item.sectionHTMLs.length; i++) {
                var obj = item.sectionHTMLs[i];
                if (obj.includes("id = \"" + title + "\"")) {
                    return obj;
                }
            }
            return '';
        },
        insertApiItems: function (startString, dst, where) {
            var result = [];
            for (var i = 0; i < this.api.length; i++) {
                var item = this.api[i];
                if (item.title.includes(startString)) {
                    var leafData = this.apiItemToLeaf(item);
                    var title = item.title.replace(startString, '');
                    var description = this.getAPIField(item, 'Description');
                    var parameters = this.getAPIField(item, 'Parameters');
                    var examples = this.getAPIField(item, 'Examples');
                    var syntax = this.getAPIField(item, 'Syntax');
                    description = description + '</br>' + parameters + '<br/>' + examples;
                    syntax = utils.strip_tags(syntax);
                    syntax = syntax.replace('Syntax', '');
                    syntax = utils.replaceAll('\n', '', syntax);
                    syntax = utils.replaceAll('\r', '', syntax);
                    where.push(this.createLeaf(title, syntax, startString.replace('.', ''), description, true, dst));
                }
            }
            return result;
        },
        _createExpressionData: function () {

            var data = {
                items: [],
                identifier: 'name',
                label: 'name'
            };

            var _Array = this.createBranch('Array', data.items);
            this.insertApiItems('Array.', _Array, data.items);

            var _Date = this.createBranch('Date', data.items);
            this.insertApiItems('Date.', _Date, data.items);

            var _Function = this.createBranch('Function', data.items);
            this.insertApiItems('Function.', _Function, data.items);

            var _Math = this.createBranch('Math', data.items);
            this.insertApiItems('Math.', _Math, data.items);

            var _Number = this.createBranch('Number', data.items);
            this.insertApiItems('Number.', _Number, data.items);

            var _Object = this.createBranch('Object', data.items);
            this.insertApiItems('Object.', _Object, data.items);

            var _String = this.createBranch('String', data.items);
            this.insertApiItems('String.', _String, data.items);

            var _RegExp = this.createBranch('RegExp', data.items);
            this.insertApiItems('RegExp.', _RegExp, data.items);

            this.publish(types.EVENTS.ON_EXPRESSION_EDITOR_ADD_FUNCTIONS, {
                root: data,
                widget: this,
                user: this.userData
            });
            /*
             var _Variables = this.createBranch('Variables', data.items);
             data.items.push(this.createLeaf('getVariable', 'this.getVariable(\'variableName\')', 'Variables', 'gets a variable', true, _Variables));
             data.items.push(this.createLeaf('setVariable', 'this.setVariable(\'variableName\')', 'Variables', 'sets a variable by name', true, _Variables));
             */
            return data;
        },
        _createExpressionStore: function () {
            var store = declare('apiStore',[TreeMemory],{
                getChildren: function (parent, options) {
                    return this.query({parent: this.getIdentity(parent)});
                },
                mayHaveChildren: function (parent) {
                    return false;
                }
            });
            return new store({
                data: this._createExpressionData().items,
                idProperty:'name'
            });
        },
        insertSymbol: function (val, isFunction) {
            var editor = this.getConsoleEditor();
            if (editor) {
                var selection = editor.getSelectedText();
                if (selection && selection.length > 0) {
                    if (isFunction && !val.includes('(')) {
                        val = val + '(' + selection + ')';
                    } else {
                        val = ' ' + val + ' ';
                    }
                }
                editor.insert(val);
            }
        },
        getConsoleEditor:function(){
            if(this.console) {
                return this.console.console.consoleEditor;
            }
        },
        createLayout:function(){

            var DOCKER = types.DOCKER;
            this._docker = this._docker || Docker.createDefault(this.domNode,{}) || this.getDocker();
            var top = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false
            });

            var bottom,bottomRight

            this.add(this._docker,null,false);

            if(this.showAPIBrowser) {

                bottom = this.createTab(null, {
                    title: false,
                    icon: 'fa-long-arrow-right',
                    closeable: false,
                    tabOrientation: DOCKER.TAB.BOTTOM,
                    location: DOCKER.TAB.BOTTOM
                });


                bottom.maxSize(200);
                bottom.getSplitter().pos(0.8);
                bottomRight = this.createTab(null, {
                    title: false,
                    icon: 'fa-long-arrow-right',
                    closeable: false,
                    tabOrientation: DOCKER.TAB.RIGHT,
                    location: DOCKER.TAB.RIGHT,
                    target: bottom
                });
            }

            var center = this.showHelp ? this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM,
                h:'300px'
            }) : null;


            center && center.minSize(200, 100);


            return {
                top:top,
                bottom:bottom,
                bottomRight:bottomRight,
                center:center
            }
        },
        createGridClass:function(){
            return Grid.createGridClass('ExpressionAPIDocGrid',{
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {
                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD
                },
                {
                    //base flip
                },
                {
                    //args
                },
                {
                    GRID: OnDemandGrid,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );
        },
        /***
         *
         * @param where
         * @param item
         * @param mixin
         * @param value
         * @param startup
         * @returns {*}
         */
        createConsole:function(where,item,mixin,value,startup){
            var thiz = this,
                ctx = this.ctx;
            if(ctx) {
                if(item) {
                    var deviceManager = ctx.getDeviceManager();
                    return deviceManager.openConsole(item, where, {
                        value: value
                    });
                }else{

                    var args =  utils.mixin({
                        value:mixin ? mixin.value : ''
                    },mixin);
                    return utils.addWidget(ConsoleClass,utils.mixin({
                        ctx:ctx,
                        type:'javascript',
                        value:'return this.getVariable(\'Volume\');',
                        editorArgs:args,
                        owner:this,
                        log: function (msg) {
                            var printTemplate = '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>';
                            var out = '';
                            if (_.isString(msg)) {
                                out += msg.replace(/\n/g, '<br/>');
                            } else if (_.isObject(msg) || _.isArray(msg)) {
                                out += JSON.stringify(msg, null, true);
                            }
                            var items = out.split('<br/>');

                            for (var i = 0; i < items.length; i++) {
                                this.printCommand('',items[i],this.logTemplate,true);
                            }
                        },
                        onConsoleEnter: function(command){

                            var consoleWidget = this.getConsole();
                            var thiz = this,
                                failed = false;

                            function expressionError(message,error){
                                _resolved = '<span class="text-primary"><b>' + command + '</span></b><span class="text-info">'  +  '  failed: <span class="text-danger">' + error.message +  '</span>';
                                thiz.printCommand(_resolved,'');
                                failed = true;
                                return null;
                            }
                            var _resolved = this.parse(command,expressionError);
                            if(failed){
                                return null;
                            }
                            //this.owner.sendDeviceCommand(this.deviceInstance, _resolved);

                            if (_resolved == command) {
                                _resolved == '';
                            } else {
                                _resolved = '<span class="text-info"><b>' + command + '</span></b><span>'  +  '  evaluates to <span class="text-success">' + _resolved +  '</span>';
                            }

                            this.printCommand(_resolved,'');

                            return _resolved;
                        }
                    },args),null,where,startup);
                }
            }else{

            }
        },
        createWidgets:function(){

            var thiz = this,
                ctx = this.ctx;

            var DOCKER = types.DOCKER;
            ////////////////////////////////////////////////////////////////////////////////
            //
            //  Layout Parts
            //
            var layout = this.createLayout();
            var top = layout.top;
            var bottom = layout.bottom;
            var bottomRight = layout.bottomRight;
            var center = layout.center;
            if(center) {
                this.helpContainer = center.containerNode;
            }

            ////////////////////////////////////////////////////////////////////////////////
            //
            //  Console
            //

            this.console = this.createConsole(top,null,null,'test',true);

            ////////////////////////////////////////////////////////////////////////////////
            //
            //  API Doc Grids
            //
            var _gridClass = this.createGridClass();
            var store = this._createExpressionStore();
            var gridArgs = {
                ctx:ctx,
                attachDirect:true,
                collection: store.filter({
                    group:'top'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };

            //left:
            var grid = utils.addWidget(_gridClass,gridArgs,null,bottom,true);
            /**************************************************************/
            var gridArgsRight = {
                ctx:ctx,
                noDataMessage:'',
                attachDirect:true,
                collection: store.filter({
                    group:'nada'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };

            var gridRight = utils.addWidget(_gridClass,gridArgsRight,null,bottomRight,true);

            grid._on('selectionChanged',function(evt){
                var selection = evt.selection,
                    item = selection[0],
                    right = store.query({
                        parent:item.name
                    });

                gridRight.set("collection", store.filter({parent: 'xx'}));
                gridRight.renderArray([]);
                gridRight.renderArray(right);
            });

            gridRight._on('selectionChanged',function(evt){
                var selection = evt.selection;
                var item = selection[0];
                thiz.currentItem = item;
                if (item && item.help) {
                    thiz.showDocumentation(item.help || "");
                }
            });

            ////////////////////////////////////////////////////////////////////////////////
            //
            //  Post work
            //
            bottom && bottom.getSplitter().pos(0.5);

            center && center.getSplitter().pos(0.5);

            if(this.console) {
                var cBottom = this.console.__bottom;
                if (cBottom) {
                    cBottom.getSplitter().pos(0.5);
                    cBottom.title(false);
                    cBottom.title(false);
                }
            }


            if(center) {
                center.$container.find('.panelParent').css('overflow', 'auto');
                center.$container.parent().css({
                    overflow: 'auto'
                });
            }

            this.add(grid,null,false);
            this.add(gridRight,null,false);
            this.console && this.add(this.console,null,false);
            gridRight.on("dblclick", function (evt) {
                var selection = gridRight.getSelection();
                var item = selection[0];
                thiz.insertSymbol(item.value,item.isFunction);
            });
        },
        startup:function(){
            var thiz = this;
            //pull in ExpressionJavaScript syntax and documentation
            var _re = require;
            _re([
                'xide/widgets/ExpressionJavaScript'], function (ExpressionJavaScript) {
                thiz.api = ExpressionJavaScript.prototype.api;
                thiz.createWidgets();
            });
        }
    });

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON;


    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var __CIPanelDialog = dcl(_Panel,{
        containerClass:'CIDialog',
        getDefaultOptions:function(mixin){
            var self = this;
            var options = {
                "contentSize": {
                    width: '600px',
                    height: '500px'
                },
                footerToolbar:[
                    {
                        item:     "<button style='margin-left:5px;' type='button'><span class='...'></span></button>",
                        event:    "click",
                        btnclass: "btn btn-danger btn-sm",
                        btntext:  " Cancel",
                        callback: function( event ){
                            event.data.close();
                            self.onCancel();
                        }
                    },
                    {
                        item:     "<button style='margin-left:5px;' type='button'><span class='...'></span></button>",
                        event:    "click",
                        btnclass: "btn btn-primary btn-sm",
                        btntext:  " Ok",
                        callback: function( event ){
                            self.onOk(self.changedCIS);
                            event.data.close();
                        }
                    }
                ]
            };
            utils.mixin(options,mixin);
            return options;
        },
        CIViewClass:CIView,
        CIViewOptions:{},
        onShow:function(panel,contentNode,instance){

            this.changedCIS = [];
            var self = this;
            this.cisView = utils.addWidget(this.CIViewClass, utils.mixin({
                delegate: this,
                resizeToParent:true,
                ciSort:false,
                options: {
                    groupOrder: {
                        'General': 1,
                        'Send': 2,
                        'Advanced': 4,
                        'Description': 5
                    }
                },
                cis: this.cis
            },this.CIViewOptions), this, contentNode, false);
            self.add(this.cisView,null,false);
            this.cisView.startup();
            this.cisView.startDfd.then(function(){
                self.resize();
            });
            //collect changed CIs
            this.cisView._on('valueChanged', function (evt) {
                if(!_.find(self.changedCIS,{ci:evt.ci})){
                    self.changedCIS.push({
                        ci:evt.ci,
                        oldValue:evt.oldValue,
                        newValue:evt.newValue,
                        dst:evt.ci.dst,
                        props:evt.props
                    });
                }
            });

            return [this.cisView];
        }
    });

    var _actions = [
        ACTION.RENAME
    ];
    var CIS = [];


    CIS.push(utils.createCI('value','expression','1212',{
        group:'General',
        title:'Value',
        dst:'value',
        //widget:defaultArgs,
        widget:{
            ctx:ctx
        },
        delegate:{
            runExpression:function(val,run,error){
                debugger;

            }
        }
    }));
    /*
    CIS.push(utils.createCI('value',13,'1212',{
        group:'General2',
        title:'Value',
        dst:'value',
        //widget:defaultArgs,
        delegate:{
            runExpression:function(val,run,error){
            }
        }
    }))*/

    function createExpressionWidget(){
        return dcl(WidgetBase, {
            declaredClass: "xide.widgets.Expression",
            minHeight: "400px;",
            value: "",
            options: null,
            templateString: "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
            "<table border='0' cellpadding='5px' width='100%' >" +
            "<tbody>" +
            "<tr attachTo='extensionRoot'>" +
            "<td width='15%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
            "<td width='100px' class='widgetValue2' valign='middle' attachTo='previewNode'></td>" +
            "<td class='extension' width='25px' attachTo='button0'></td>" +
            "<td class='extension' width='25px' attachTo='button1'></td>" +
            "<td class='extension' width='25px' attachTo='button2'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
            postMixInProperties: function () {

                this.inherited(arguments);

                if (this.userData && this.userData.title) {
                    this.title = this.userData.title;
                }

                if ((this.userData && this.userData.vertical === true) || this.vertical === true) {

                    this.templateString = "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
                        "<table border='0' cellpadding='5px' width='100%' >" +
                        "<tbody>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='100%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
                        "</tr>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='100px' class='widgetValue2' valign='middle' attachTo='previewNode'></td>" +
                        "<td class='extension' width='25px' attachTo='button0'></td>" +
                        "<td class='extension' width='25px' attachTo='button1'></td>" +
                        "<td class='extension' width='25px' attachTo='button2'></td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
                        "<div attachTo='last'></div>" +
                        "</div>"
                }

            },
            onValueChanged:function(value,updateTextArea){
                if(this.userData.value===value){
                    return;
                }
                this.userData.changed = true;
                this.userData.active = true;
                utils.setCIValueByField(this.userData, "value", value);
                var _args = {
                    owner: this.delegate || this.owner,
                    ci: this.userData,
                    newValue: value
                }
                this.publish(types.EVENTS.ON_CI_UPDATE, _args);
                this._emit('valueChanged', _args);
                if(updateTextArea){
                    this.editBox.val(value);
                }

            },
            onSelect: function () {
                var thiz = this;
                var _defaultOptions = {};
                utils.mixin(_defaultOptions, this.options);
                var value = utils.toString(this.userData['value']);
                var actionDialog = new _CIPanelDialog({
                    title: 'Expression',
                    resizeable: true,
                    CIViewOptions:{
                        getTypeMap:function(){
                            //debugger;
                        }
                    },
                    onOk:function(changedCIS){
                        if(changedCIS && changedCIS[0]){
                            thiz.onValueChanged(changedCIS[0].ci.value,true);
                        }
                        this.headDfd.resolve(true);
                    },
                    cis: [
                        utils.createCI('Expression', types.ECIType.EXPRESSION_EDITOR, value, {
                            group: 'Expression',
                            delegate: this.userData.delegate
                        })
                    ]
                });
                this.add(actionDialog,null,false);
                var dfd = actionDialog.show();
            },
            startup: function () {
                var thiz = this;
                var value = utils.toString(this.userData['value']);
                var area = $('<textarea rows="3" class="form-control input-transparent" ></textarea>');
                area.val(value);
                this.editBox = area;
                $(this.previewNode).append(area);
                this.editBox.on("change", function (e) {
                    thiz.onValueChanged(e.target.value);
                });
                var btn = factory.createSimpleButton('', 'fa-magic', 'btn-default', {
                    style: ''
                },this.button0);
                $(btn).click(function () {
                    thiz.onSelect();
                })
                this.onReady();
            }
        });
    }

    if (ctx) {

        var parent = TestUtils.createTab(null,null,module.id);
        var cisRenderer = dcl(CIGroupedSettingsView,{
            typeMap:null,
            getTypeMap:function(){
                if (this.typeMap) {
                    return this.typeMap;
                }
                var self = this;
                var typeMap = {};
                typeMap['expression'] = _ExpressionEditor;// createExpressionWidget();
                this.typeMap = typeMap;
                return typeMap;
            }
        });
        var ciView = utils.addWidget(cisRenderer,{
            cis:CIS
        },null,parent,true);

        return declare('a',null,{});

    }

    return Grid;

});