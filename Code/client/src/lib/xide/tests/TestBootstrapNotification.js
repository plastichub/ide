/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',
    "xide/widgets/TemplatedWidgetBase"

], function (declare, types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase) {




    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;




    var propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };



    function doTests(){
    }





    var createDelegate = function(){

        return {

        }


    }







    function openDriverSettings(driver){


        //createCommandSettingsWidget();


        var toolbar = ctx.mainView.getToolbar();


        CIS = createDriverCIS(driver,toolbar);



        var docker = ctx.mainView.getDocker();
        var parent  = window.driverTab;
        if(parent){
            docker.removePanel(parent);
        }

        parent = docker.addTab(null, {
            title: 'Marantz/Marantz',
            icon: 'fa-exchange'
        });

        window.driverTab = parent;


        var cisRenderer = declare('cis',[CIGroupedSettingsView,CIViewMixin],{

            createGroupContainer: function () {

                if (this.tabContainer) {
                    return this.tabContainer;
                }
                var tabContainer = utils.addWidget(TabContainer,{
                    tabStrip: true,
                    tabPosition: "left-h",
                    splitter: true,
                    style: "min-width:450px;",
                    "className": "ui-widget-content"
                },null,this.containerNode,true);
                this.tabContainer = tabContainer;
                return tabContainer;
            },
            attachWidgets: function (data, dstNode,view) {

                var thiz = this;
                this.helpNodes = [];
                this.widgets = [];

                dstNode = dstNode || this.domNode;
                if(!dstNode && this.tabContainer){
                    dstNode = this.tabContainer.containerNode;
                }
                if (!dstNode) {
                    console.error('have no parent dstNode!');
                    return;
                }


                for (var i = 0; i < data.length; i++) {
                    var widget = data[i];
                    widget.delegate = this.owner || this;
                    dstNode.appendChild(widget.domNode);
                    widget.startup();

                    widget._on('valueChanged',function(evt){
                        thiz.onValueChanged(evt);
                    });

                    this.widgets.push(widget);
                    widget.userData.view=view;
                }
            },
            initWithCIS: function (data) {


                this.empty();

                data = utils.flattenCIS(data);

                this.data = data;

                var thiz = this,
                    groups = _.groupBy(data,function(obj){
                        return obj.group;
                    }),
                    groupOrder = this.options.groupOrder || {};

                groups = this.toArray(groups);

                var grouped = _.sortByOrder(groups, function(obj){
                    return groupOrder[obj.name] || 100;
                });

                if (grouped != null && grouped.length > 1) {
                    this.renderGroups(grouped);
                } else {
                    this.widgets = factory.createWidgetsFromArray(data, thiz, null, false);
                    if (this.widgets) {
                        this.attachWidgets(this.widgets);
                    }
                }
            }
        });


        var view = utils.addWidget(CIGroupedSettingsView, {
            title:  'title',
            cis: driver.user.inputs,
            storeItem: driver,
            delegate: createDelegate(),
            storeDelegate: this,
            iconClass: 'fa-eye',
            blockManager: ctx.getBlockManager(),
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'General'
            }
        }, null, parent, true);


        docker.resize();
        view.resize();
    }

/*
     * playground
     */
    var _lastGrid = window._lastBoot;
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;



    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {



        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();
        //marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        //scope = createScope();

        //openDriverSettings(marantz);

        var toolbar = ctx.mainView.getToolbar();
        var docker = ctx.mainView.getDocker();
        var parent  = window.bootTab;
        if(parent){
            docker.removePanel(parent);
        }

        parent = docker.addTab(null, {
            title: 'Marantz/Marantz',
            icon: 'fa-exchange'
        });

        window.bootTab = parent;













        var _accHTML = require.toUrl('xide/tests/accordion_test.html');

        var _accHTML = require.toUrl('xide/tests/breadcrumb_test.html');

        var _accHTML = require.toUrl('xide/tests/acc_test.html');


        var _accHTML = require.toUrl('xide/tests/notification_test.html');


        //$('.widget').widgster();


        /*
        Messenger().post("Thanks for checking out Messenger!");
        */


        var msg = Messenger().post({
            message: 'Launching thermonuclear war...',
            actions: {
                cancel: {
                    label: 'cancel launch',
                    action: function() {
                        return msg.update({
                            message: 'Thermonuclear war averted',
                            type: 'success',
                            actions: false
                        });
                    }
                }
            }
        });

        Messenger().post({
            message: 'Showing success message was successful!',
            type: 'success',
            showCloseButton: true
        });

        var i;

        i = 0;

        Messenger().run({
            errorMessage: 'Error destroying alien planet',
            successMessage: 'Alien planet destroyed!',
            action: function(opts) {
                if (++i < 3) {
                    return opts.error({
                        status: 500,
                        readyState: 0,
                        responseText: 0
                    });
                } else {
                    return opts.success();
                }
            }
        });


        function _getText(url) {

            var result;
            var def = dojo.xhrGet({
                url: url,
                sync: true,
                handleAs: 'text',
                load: function (text) {
                    result = text;
                }
            });
            return '' + result + '';
        }
        var template = _getText(_accHTML);
        var widget = declare('x',TemplatedWidgetBase,{
            templateString:template
        });




        utils.addWidget(widget,{},null,parent,true);


        return declare('a',null,{});

    }

    return Grid;

});