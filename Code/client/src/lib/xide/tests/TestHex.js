/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry',
    'xide/_base/_Widget',
    "dojo/text!./Navbar.html",
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    'xide/action/DefaultActions',
    "xaction/ActionProvider",
    "xgrid/Clipboard",
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    'xide/mixins/EventedMixin',
    "xide/widgets/_MenuMixin2",
    "xblox/tests/TestUtils",
    "xide/_Popup",
    "dojo/string",
    "dojo/_base/lang"

], function (declare,dcl,types,utils,TestUtils,_Widget,module,registry,_XWidget,MenuBarTemplate,ActionMixin,ActionContext,ActionStore,DefaultActions,ActionProvider,Clipboard,Path,Action,FTestUtils,i18,EventedMixin,_MenuMixin2,_TestBlockUtils,_Popup,string,lang){


    function displayResults(str, src) {
        // convert the string of characters into various formats and send to UI
        // str: string, the string of characters
        // src: string, the id of the UI location that originated the request
        var preserve = 'none';
        var pad = true;
        var showinvisibles;
        var bidimarkup;
        var cstyle = false;
        if (src != 'chars') { chars.value = str; }
        if (src != 'codePoints') {
            preserve = 'none';
            if (document.getElementById('hexcplatin1').checked) { preserve = 'latin1'; }
            else if (document.getElementById('hexcpascii').checked) { preserve = 'ascii'; }
            codePoints.value = convertCharStr2CP(str, preserve, true, 'hex');
        }
        if (src != 'decCodePoints') {
            preserve = 'none';
            if (document.getElementById('deccplatin1').checked) { preserve = 'latin1'; }
            else if (document.getElementById('deccpascii').checked) { preserve = 'ascii'; }
            decCodePoints.value = convertCharStr2CP(str, preserve, true, 'dec');
        }
        if (src != 'XML') {
            XML.value = convertCharStr2XML(str, getParameters(document.getElementById('xmlOptions')));
        }
        if (src != 'UTF8') {
            UTF8.value = convertCharStr2UTF8( str );
        }
        if (src != 'UTF16') {
            UTF16.value = convertCharStr2UTF16( str );
        }
        if (src != 'hexNCRs') {
            hexNCRs.value = convertCharStr2SelectiveCPs( str, getParameters(document.getElementById('hexNCROptions')), true, '&#x', ';', 'hex' );
        }
        if (src != 'decNCRs') {
            decNCRs.value = convertCharStr2SelectiveCPs( str, getParameters(document.getElementById('decNCROptions')), false, '&#', ';', 'dec' );
        }
        if (src != 'pEsc') {
            pEsc.value = convertCharStr2pEsc( str );
        }
        if (src != 'jEsc') {
            jEsc.value = convertCharStr2jEsc( str, getParameters(document.getElementById('jEscOptions')) );
        }
        if (src != 'Unicode') {
            preserve = 'none';
            if (document.getElementById('unicodelatin1').checked) { preserve = 'latin1'; }
            else if (document.getElementById('unicodeascii').checked) { preserve = 'ascii'; }
            Unicode.value = convertCharStr2CP(str, preserve, true, 'unicode');
            //Unicode.value = convertCharStr2Unicode( str, 'none', pad );
            //Unicode.value = convertCP2Unicode( convertCharStr2CP( str, preserve, pad ) ); 
        }
        if (src != 'zeroX') {
            preserve = 'none';
            if (document.getElementById('zeroXlatin1').checked) { preserve = 'latin1'; }
            else if (document.getElementById('zeroXascii').checked) { preserve = 'ascii'; }
            zeroX.value = convertCharStr2CP(str, preserve, false, 'zerox');
        }
        if (src != 'CSS') {
            CSS.value = convertCharStr2CSS( str );
        }
    }
    function getParameters (node) {
        var par = ''
        var checkboxes = node.querySelectorAll('input[type=checkbox]')
        for (var c=0;c<checkboxes.length;c++) {
            if (c>0) par += ';'
            if (checkboxes[c].checked) par += checkboxes[c].dataset.fn
            else par += ''
        }

        return par
    }
    function hex2char ( hex ) {
        // converts a single hex number to a character
        // note that no checking is performed to ensure that this is just a hex number, eg. no spaces etc
        // hex: string, the hex codepoint to be converted
        var result = '';
        var n = parseInt(hex, 16);
        if (n <= 0xFFFF) { result += String.fromCharCode(n); }
        else if (n <= 0x10FFFF) {
            n -= 0x10000
            result += String.fromCharCode(0xD800 | (n >> 10)) + String.fromCharCode(0xDC00 | (n & 0x3FF));
        }
        else { result += 'hex2Char error: Code point out of range: '+dec2hex(n); }
        return result;
    }
    function dec2char ( n ) {
        // converts a single string representing a decimal number to a character
        // note that no checking is performed to ensure that this is just a hex number, eg. no spaces etc
        // dec: string, the dec codepoint to be converted
        var result = '';
        if (n <= 0xFFFF) { result += String.fromCharCode(n); }
        else if (n <= 0x10FFFF) {
            n -= 0x10000
            result += String.fromCharCode(0xD800 | (n >> 10)) + String.fromCharCode(0xDC00 | (n & 0x3FF));
        }
        else { result += 'dec2char error: Code point out of range: '+dec2hex(n); }
        return result;
    }
    function dec2hex ( textString ) {
        return (textString+0).toString(16).toUpperCase();
    }
    function dec2hex2 ( textString ) {
        var hexequiv = new Array ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
        return hexequiv[(textString >> 4) & 0xF] + hexequiv[textString & 0xF];
    }
    function dec2hex4 ( textString ) {
        var hexequiv = new Array ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
        return hexequiv[(textString >> 12) & 0xF] + hexequiv[(textString >> 8) & 0xF]
            + hexequiv[(textString >> 4) & 0xF] + hexequiv[textString & 0xF];
    }
    function convertChar2CP ( textString ) {
        var haut = 0;
        var n = 0;
        var CPstring = '';
        for (var i = 0; i < textString.length; i++) {
            var b = textString.charCodeAt(i);
            if (b < 0 || b > 0xFFFF) {
                CPstring += 'Error in convertChar2CP: byte out of range ' + dec2hex(b) + '!';
            }
            if (haut != 0) {
                if (0xDC00 <= b && b <= 0xDFFF) {
                    CPstring += dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00)) + ' ';
                    haut = 0;
                    continue;
                }
                else {
                    CPstring += 'Error in convertChar2CP: surrogate out of range ' + dec2hex(haut) + '!';
                    haut = 0;
                }
            }
            if (0xD800 <= b && b <= 0xDBFF) {
                haut = b;
            }
            else {
                CPstring += dec2hex(b) + ' ';
            }
        }
        return CPstring.substring(0, CPstring.length-1);
    }
    

// ========================== Converting to characters ==============================================
    function convertAllEscapes (str, numbers) {
        // converts all escapes in the text str to characters, and can interpret numbers as escapes too
        // str: string, the text to be converted
        // numbers: string enum [none, hex, dec, utf8, utf16], what to treat numbers as

        var sle = false;
        str = convertUnicode2Char(str); //alert(str);
        str = convertZeroX2Char(str); //alert(str);
        str = convertHexNCR2Char(str); //alert(str);
        str = convertDecNCR2Char(str); //alert(str);
        if (sle) { str = convertjEsc2Char(str, true); }
        else { str = convertjEsc2Char(str, false); //alert(str);
            str = convertCSS2Char(str, false); } //alert(str);
        str = convertpEnc2Char(str);  //alert(str);
        str = convertEntities2Char(str); //alert(str);
        str = convertNumbers2Char(str, numbers); //alert(str);

        return str;
    }
    function convertUnicode2Char ( str ) {
        // converts a string containing U+... escapes to a string of characters
        // str: string, the input

        // first convert the 6 digit escapes to characters
        str = str.replace(/[Uu]\+10([A-Fa-f0-9]{4})/g,
            function(matchstr, parens) {
                return hex2char('10'+parens);
            }
        );
        // next convert up to 5 digit escapes to characters
        str = str.replace(/[Uu]\+([A-Fa-f0-9]{1,5})/g,
            function(matchstr, parens) {
                return hex2char(parens);
            }
        );
        return str;
    }

    function oldconvertUnicode2Char ( str ) {
        // converts a string containing U+... escapes to a string of characters
        // str: string, the input

        // first convert the 6 digit escapes to characters
        str = str.replace(/U\+10([A-Fa-f0-9]{4})/g,
            function(matchstr, parens) {
                return hex2char('10'+parens);
            }
        );
        // next convert up to 5 digit escapes to characters
        str = str.replace(/U\+([A-Fa-f0-9]{1,5})/g,
            function(matchstr, parens) {
                return hex2char(parens);
            }
        );
        return str;
    }
    function convertHexNCR2Char ( str ) {
        // converts a string containing &#x...; escapes to a string of characters
        // str: string, the input

        // convert up to 6 digit escapes to characters
        str = str.replace(/&#x([A-Fa-f0-9]{1,6});/g,
            function(matchstr, parens) {
                return hex2char(parens);
            }
        );
        return str;
    }
    function convertDecNCR2Char ( str ) {
        // converts a string containing &#...; escapes to a string of characters
        // str: string, the input

        // convert up to 6 digit escapes to characters
        str = str.replace(/&#([0-9]{1,7});/g,
            function(matchstr, parens) {
                return dec2char(parens);
            }
        );
        return str;
    }
    function convertZeroX2Char ( str ) {
        // converts a string containing 0x... escapes to a string of characters
        // str: string, the input

        // convert up to 6 digit escapes to characters
        str = str.replace(/0x([A-Fa-f0-9]{1,6})/g,
            function(matchstr, parens) {
                return hex2char(parens);
            }
        );
        return str;
    }
    function convertCSS2Char ( str, convertbackslash ) {
        // converts a string containing CSS escapes to a string of characters
        // str: string, the input
        // convertbackslash: boolean, true if you want \x etc to become x or \a to be treated as 0xA

        // convert up to 6 digit escapes to characters & throw away any following whitespace
        if (convertbackslash) {
            str = str.replace(/\\([A-Fa-f0-9]{1,6})(\s)?/g,
                function(matchstr, parens) {
                    return hex2char(parens);
                }
            );
            str = str.replace(/\\/g, '');
        }
        else {
            str = str.replace(/\\([A-Fa-f0-9]{2,6})(\s)?/g,
                function(matchstr, parens) {
                    return hex2char(parens);
                }
            );
        }
        return str;
    }
    function convertjEsc2Char ( str, shortEscapes ) {
        // converts a string containing JavaScript or Java escapes to a string of characters
        // str: string, the input
        // shortEscapes: boolean, if true the function will convert \b etc to characters

        // convert ES6 escapes to characters
        str = str.replace(/\\u\{([A-Fa-f0-9]{1,})\}/g,
            function(matchstr, parens) {
                return hex2char(parens);
            }
        );
        // convert \U and 6 digit escapes to characters
        str = str.replace(/\\U([A-Fa-f0-9]{8})/g,
            function(matchstr, parens) {
                return hex2char(parens);
            }
        );
        // convert \u and 6 digit escapes to characters
        str = str.replace(/\\u([A-Fa-f0-9]{4})/g,
            function(matchstr, parens) {
                return hex2char(parens);
            }
        );
        // convert \b etc to characters, if flag set
        if (shortEscapes) {
            //str = str.replace(/\\0/g, '\0'); 
            str = str.replace(/\\b/g, '\b');
            str = str.replace(/\\t/g, '\t');
            str = str.replace(/\\n/g, '\n');
            str = str.replace(/\\v/g, '\v');
            str = str.replace(/\\f/g, '\f');
            str = str.replace(/\\r/g, '\r');
            str = str.replace(/\\\'/g, '\'');
            str = str.replace(/\\\"/g, '\"');
            str = str.replace(/\\\\/g, '\\');
        }
        return str;
    }
    function convertpEnc2Char ( str ) {
        // converts a string containing precent encoded escapes to a string of characters
        // str: string, the input

        // find runs of hex numbers separated by % and send them for conversion
        str = str.replace(/((%[A-Fa-f0-9]{2})+)/g,
            function(matchstr, parens) {
                //return convertpEsc2Char(parens.replace(/%/g,' ')); 
                return convertpEsc2Char(parens);
            }
        );
        return str;
    }
    function convertEntities2Char ( str ) {
        // converts a string containing HTML/XML character entities to a string of characters
        // str: string, the input

        str = str.replace(/&([A-Za-z0-9]+);/g,
            function(matchstr, parens) { //alert(parens);
                if (parens in entities) { //alert(entities[parens]);
                    return entities[parens];
                }
                else { return matchstr; }						}
        );
        return str;
    }
    function convertNumbers2Char ( str, type ) {
        // converts a string containing HTML/XML character entities to a string of characters
        // str: string, the input
        // type: string enum [none, hex, dec, utf8, utf16], what to treat numbers as

        if (type == 'hex') {
            str = str.replace(/(\b[A-Fa-f0-9]{2,6}\b)/g,
                function(matchstr, parens) {
                    return hex2char(parens);
                }
            );
        }
        else if (type == 'dec') {
            str = str.replace(/(\b[0-9]+\b)/g,
                function(matchstr, parens) {
                    return dec2char(parens);
                }
            );
        }
        else if (type == 'utf8') {
            str = str.replace(/(( [A-Fa-f0-9]{2})+)/g,
                //str = str.replace(/((\b[A-Fa-f0-9]{2}\b)+)/g, 
                function(matchstr, parens) {
                    return convertUTF82Char(parens);
                }
            );
        }
        else if (type == 'utf16') {
            str = str.replace(/(( [A-Fa-f0-9]{1,6})+)/g,
                function(matchstr, parens) {
                    return convertUTF162Char(parens);
                }
            );
        }
        return str;
    }
    function convertUTF82Char ( str ) {
        // converts to characters a sequence of space-separated hex numbers representing bytes in utf8
        // str: string, the sequence to be converted
        var outputString = "";
        var counter = 0;
        var n = 0;

        // remove leading and trailing spaces
        str = str.replace(/^\s+/, '');
        str = str.replace(/\s+$/,'');
        if (str.length == 0) { return ""; }
        str = str.replace(/\s+/g, ' ');

        var listArray = str.split(' ');
        for ( var i = 0; i < listArray.length; i++ ) {
            var b = parseInt(listArray[i], 16);  // alert('b:'+dec2hex(b));
            switch (counter) {
                case 0:
                    if (0 <= b && b <= 0x7F) {  // 0xxxxxxx
                        outputString += dec2char(b); }
                    else if (0xC0 <= b && b <= 0xDF) {  // 110xxxxx
                        counter = 1;
                        n = b & 0x1F; }
                    else if (0xE0 <= b && b <= 0xEF) {  // 1110xxxx
                        counter = 2;
                        n = b & 0xF; }
                    else if (0xF0 <= b && b <= 0xF7) {  // 11110xxx
                        counter = 3;
                        n = b & 0x7; }
                    else {
                        outputString += 'convertUTF82Char: error1 ' + dec2hex(b) + '! ';
                    }
                    break;
                case 1:
                    if (b < 0x80 || b > 0xBF) {
                        outputString += 'convertUTF82Char: error2 ' + dec2hex(b) + '! ';
                    }
                    counter--;
                    outputString += dec2char((n << 6) | (b-0x80));
                    n = 0;
                    break;
                case 2: case 3:
                if (b < 0x80 || b > 0xBF) {
                    outputString += 'convertUTF82Char: error3 ' + dec2hex(b) + '! ';
                }
                n = (n << 6) | (b-0x80);
                counter--;
                break;
            }
        }
        return outputString.replace(/ $/, '');
    }
    function convertUTF162Char ( str ) {
        // Converts a string of UTF-16 code units to characters
        // str: sequence of UTF16 code units, separated by spaces
        var highsurrogate = 0;
        var suppCP;
        var n = 0;
        var outputString = '';

        // remove leading and multiple spaces
        str = str.replace(/^\s+/,'');
        str = str.replace(/\s+$/,'');
        if (str.length == 0){ return; }
        str = str.replace(/\s+/g,' ');

        var listArray = str.split(' ');
        for (var i = 0; i < listArray.length; i++) {
            var b = parseInt(listArray[i], 16); //alert(listArray[i]+'='+b);
            if (b < 0 || b > 0xFFFF) {
                outputString += '!Error in convertUTF162Char: unexpected value, b=' + dec2hex(b) + '!';
            }
            if (highsurrogate != 0) {
                if (0xDC00 <= b && b <= 0xDFFF) {
                    outputString += dec2char(0x10000 + ((highsurrogate - 0xD800) << 10) + (b - 0xDC00));
                    highsurrogate = 0;
                    continue;
                }
                else {
                    outputString += 'Error in convertUTF162Char: low surrogate expected, b=' + dec2hex(b) + '!';
                    highsurrogate = 0;
                }
            }
            if (0xD800 <= b && b <= 0xDBFF) { // start of supplementary character
                highsurrogate = b;
            }
            else {
                outputString += dec2char(b);
            }
        }
        return outputString;
    }
    function convertpEsc2Char ( str ) {
        // converts to characters a sequence of %-separated hex numbers representing bytes in utf8
        // str: string, the sequence to be converted

        var outputString = "";
        var counter = 0;
        var n = 0;

        var listArray = str.split('%');
        for ( var i = 1; i < listArray.length; i++ ) {
            var b = parseInt(listArray[i], 16);  // alert('b:'+dec2hex(b));
            switch (counter) {
                case 0:
                    if (0 <= b && b <= 0x7F) {  // 0xxxxxxx
                        outputString += dec2char(b); }
                    else if (0xC0 <= b && b <= 0xDF) {  // 110xxxxx
                        counter = 1;
                        n = b & 0x1F; }
                    else if (0xE0 <= b && b <= 0xEF) {  // 1110xxxx
                        counter = 2;
                        n = b & 0xF; }
                    else if (0xF0 <= b && b <= 0xF7) {  // 11110xxx
                        counter = 3;
                        n = b & 0x7; }
                    else {
                        outputString += 'convertpEsc2Char: error ' + dec2hex(b) + '! ';
                    }
                    break;
                case 1:
                    if (b < 0x80 || b > 0xBF) {
                        outputString += 'convertpEsc2Char: error ' + dec2hex(b) + '! ';
                    }
                    counter--;
                    outputString += dec2char((n << 6) | (b-0x80));
                    n = 0;
                    break;
                case 2: case 3:
                if (b < 0x80 || b > 0xBF) {
                    outputString += 'convertpEsc2Char: error ' + dec2hex(b) + '! ';
                }
                n = (n << 6) | (b-0x80);
                counter--;
                break;
            }
        }
        return outputString;
    }
    function convertXML2Char (str) {
        // converts XML or HTML text to characters by removing all character entities and ncrs
        // str: string, the sequence to be converted

        // remove various escaped forms
        str = convertHexNCR2Char(str);
        str = convertDecNCR2Char(str);
        str = convertEntities2Char(str);

        return str;
    }
// ============================== Convert to escapes ===============================================

    function convertCharStr2XML ( str, parameters ) {
        // replaces xml/html syntax-sensitive characters in a string with entities
        // also replaces invisible and ambiguous characters with escapes (list to be extended)
        // str: string, the input string
        // convertinvisibles: boolean, if true, invisible characters are converted to NCRs
        // bidimarkup: boolean, if true, bidi rle/lre/pdf/rli/lri/fsi/pdi characters are converted to markup
        str = str.replace(/&/g, '&amp;')
        str = str.replace(/"/g, '&quot;')
        str = str.replace(/</g, '&lt;')
        str = str.replace(/>/g, '&gt;')

        // replace invisible and ambiguous characters
        if (parameters.match(/convertinvisibles/)) {
            str = str.replace(/\u2066/g, '&#x2066;')  // lri
            str = str.replace(/\u2067/g, '&#x2067;')  // rli
            str = str.replace(/\u2068/g, '&#x2068;')  // fsi
            str = str.replace(/\u2069/g, '&#x2069;')  // pdi

            str = str.replace(/\u202A/g, '&#x202A;') // lre
            str = str.replace(/\u202B/g, '&#x202B;') // rle
            str = str.replace(/\u202D/g, '&#x202D;') // lro
            str = str.replace(/\u202E/g, '&#x202E;') // rlo
            str = str.replace(/\u202C/g, '&#x202C;') // pdf
            str = str.replace(/\u200E/g, '&#x200E;') // lrm
            str = str.replace(/\u200F/g, '&#x200F;') // rlm

            str = str.replace(/\u2000/g, '&#x2000;') // en quad
            str = str.replace(/\u2001/g, '&#x2001;') // em quad
            str = str.replace(/\u2002/g, '&#x2002;') // en space
            str = str.replace(/\u2003/g, '&#x2003;') // em space
            str = str.replace(/\u2004/g, '&#x2004;') // 3 per em space
            str = str.replace(/\u2005/g, '&#x2005;') // 4 per em space
            str = str.replace(/\u2006/g, '&#x2006;') // 6 per em space
            str = str.replace(/\u2007/g, '&#x2007;') // figure space
            str = str.replace(/\u2008/g, '&#x2008;') // punctuation space
            str = str.replace(/\u2009/g, '&#x2009;') // thin space
            str = str.replace(/\u200A/g, '&#x200A;') // hair space
            str = str.replace(/\u200B/g, '&#x200B;') // zwsp
            str = str.replace(/\u205F/g, '&#x205F;') // mmsp
            str = str.replace(/\uA0/g, '&#xA0;') // nbsp
            str = str.replace(/\u3000/g, '&#x3000;') // ideographic sp
            str = str.replace(/\u202F/g, '&#x202F;') // nnbsp

            str = str.replace(/\u180B/g, '&#x180B;') // mfvs1
            str = str.replace(/\u180C/g, '&#x180C;') // mfvs2
            str = str.replace(/\u180D/g, '&#x180D;') // mfvs3

            str = str.replace(/\u200C/g, '&#x200C;') // zwnj
            str = str.replace(/\u200D/g, '&#x200D;') // zwj
            str = str.replace(/\u2028/g, '&#x2028;') // line sep
            str = str.replace(/\u206A/g, '&#x206A;') // iss
            str = str.replace(/\u206B/g, '&#x206B;') // ass
            str = str.replace(/\u206C/g, '&#x206C;') // iafs
            str = str.replace(/\u206D/g, '&#x206D;') // aafs
            str = str.replace(/\u206E/g, '&#x206E;') // nads
            str = str.replace(/\u206F/g, '&#x206F;') // nods
        }

        // convert lre/rle/pdf/rli/lri/fsi/pdi to markup
        if (parameters.match(/bidimarkup/)) {
            str = str.replace(/\u2066/g, '&lt;span dir=&quot;ltr&quot;&gt;') // lri
            str = str.replace(/\u2067/g, '&lt;span dir=&quot;rtl&quot;&gt;') // rli
            str = str.replace(/\u2068/g, '&lt;span dir=&quot;auto&quot;&gt;') // fsi
            str = str.replace(/\u2069/g, '&lt;/span&gt;') // pdi

            str = str.replace(/\u202A/g, '&lt;span dir=&quot;ltr&quot;&gt;') // 
            str = str.replace(/\u202B/g, '&lt;span dir=&quot;rtl&quot;&gt;')
            str = str.replace(/\u202C/g, '&lt;/span&gt;')
            str = str.replace(/&#x202A;/g, '&lt;span dir=&quot;ltr&quot;&gt;')
            str = str.replace(/&#x202B;/g, '&lt;span dir=&quot;rtl&quot;&gt;')
            //str = str.replace(/\u202D/g, '&lt;bdo dir=&quot;ltr&quot;&gt;')
            //str = str.replace(/\u202E/g, '&lt;bdo dir=&quot;rtl&quot;&gt;')
            str = str.replace(/&#x202C;/g, '&lt;/span&gt;')
        }

        return str;
    }
    function convertCharStr2SelectiveCPs ( str, parameters, pad, before, after, base ) {
        // converts a string of characters to code points or code point based escapes
        // str: string, the string to convert
        // parameters: string enum [ascii, latin1], a set of characters to not convert
        // pad: boolean, if true, hex numbers lower than 1000 are padded with zeros
        // before: string, any characters to include before a code point (eg. &#x for NCRs)
        // after: string, any characters to include after (eg. ; for NCRs)
        // base: string enum [hex, dec], hex or decimal output
        var haut = 0;
        var n = 0; var cp;
        var CPstring = '';
        for (var i = 0; i < str.length; i++) {
            var b = str.charCodeAt(i);
            if (b < 0 || b > 0xFFFF) {
                CPstring += 'Error in convertCharStr2SelectiveCPs: byte out of range ' + dec2hex(b) + '!';
            }
            if (haut != 0) {
                if (0xDC00 <= b && b <= 0xDFFF) {
                    if (base == 'hex') {
                        CPstring += before + dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00)) + after;
                    }
                    else { cp = 0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00);
                        CPstring += before + cp + after;
                    }
                    haut = 0;
                    continue;
                }
                else {
                    CPstring += 'Error in convertCharStr2SelectiveCPs: surrogate out of range ' + dec2hex(haut) + '!';
                    haut = 0;
                }
            }
            if (0xD800 <= b && b <= 0xDBFF) {
                haut = b;
            }
            else {
                if (parameters.match(/ascii/) && b <= 127) { //  && b != 0x3E && b != 0x3C &&  b != 0x26) {
                    CPstring += str.charAt(i);
                }
                else if (b <= 255 && parameters.match(/latin1/)) { // && b != 0x3E && b != 0x3C &&  b != 0x26) {
                    CPstring += str.charAt(i);
                }
                else {
                    if (base == 'hex') {
                        cp = dec2hex(b);
                        if (pad) { while (cp.length < 4) { cp = '0'+cp; } }
                    }
                    else { cp = b; }
                    CPstring += before + cp + after;
                }
            }
        }
        return CPstring;
    }
    function convertCharStr2HexNCR ( textString ) {
        var outputString = "";
        textString = textString.replace(/^\s+/, '');
        if (textString.length == 0) { return ""; }
        textString = textString.replace(/\s+/g, ' ');
        var listArray = textString.split(' ');
        for ( var i = 0; i < listArray.length; i++ ) {
            var n = parseInt(listArray[i], 16);
            outputString += '&#x' + dec2hex(n) + ';';
        }
        return( outputString );
    }
    function convertCharStr2pEsc ( str ) {
        // str: sequence of Unicode characters
        var outputString = "";
        var CPstring = convertChar2CP(str);
        if (str.length == 0) { return ""; }
        // process each codepoint
        var listArray = CPstring.split(' ');
        for ( var i = 0; i < listArray.length; i++ ) {
            var n = parseInt(listArray[i], 16);
            //if (i > 0) { outputString += ' ';}
            if (n == 0x20) { outputString += '%20'; }
            else if (n >= 0x41 && n <= 0x5A) { outputString += String.fromCharCode(n); } // alpha
            else if (n >= 0x61 && n <= 0x7A) { outputString += String.fromCharCode(n); } // alpha
            else if (n >= 0x30 && n <= 0x39) { outputString += String.fromCharCode(n); } // digits
            else if (n == 0x2D || n == 0x2E || n == 0x5F || n == 0x7E) { outputString += String.fromCharCode(n); } // - . _ ~
            else if (n <= 0x7F) { outputString += '%'+dec2hex2(n); }
            else if (n <= 0x7FF) { outputString += '%'+dec2hex2(0xC0 | ((n>>6) & 0x1F)) + '%' + dec2hex2(0x80 | (n & 0x3F)); }
            else if (n <= 0xFFFF) { outputString += '%'+dec2hex2(0xE0 | ((n>>12) & 0x0F)) + '%' + dec2hex2(0x80 | ((n>>6) & 0x3F)) + '%' + dec2hex2(0x80 | (n & 0x3F)); }
            else if (n <= 0x10FFFF) {outputString += '%'+dec2hex2(0xF0 | ((n>>18) & 0x07)) + '%' + dec2hex2(0x80 | ((n>>12) & 0x3F)) + '%' + dec2hex2(0x80 | ((n>>6) & 0x3F)) + '%' + dec2hex2(0x80 | (n & 0x3F)); }
            else { outputString += '!Error ' + dec2hex(n) +'!'; }
        }
        return( outputString );
    }
    function convertCharStr2UTF8 ( str ) {
        // Converts a string of characters to UTF-8 byte codes, separated by spaces
        // str: sequence of Unicode characters
        var highsurrogate = 0;
        var suppCP; // decimal code point value for a supp char
        var n = 0;
        var outputString = '';
        for (var i = 0; i < str.length; i++) {
            var cc = str.charCodeAt(i);
            if (cc < 0 || cc > 0xFFFF) {
                outputString += '!Error in convertCharStr2UTF8: unexpected charCodeAt result, cc=' + cc + '!';
            }
            if (highsurrogate != 0) {
                if (0xDC00 <= cc && cc <= 0xDFFF) {
                    suppCP = 0x10000 + ((highsurrogate - 0xD800) << 10) + (cc - 0xDC00);
                    outputString += ' '+dec2hex2(0xF0 | ((suppCP>>18) & 0x07)) + ' ' + dec2hex2(0x80 | ((suppCP>>12) & 0x3F)) + ' ' + dec2hex2(0x80 | ((suppCP>>6) & 0x3F)) + ' ' + dec2hex2(0x80 | (suppCP & 0x3F));
                    highsurrogate = 0;
                    continue;
                }
                else {
                    outputString += 'Error in convertCharStr2UTF8: low surrogate expected, cc=' + cc + '!';
                    highsurrogate = 0;
                }
            }
            if (0xD800 <= cc && cc <= 0xDBFF) { // high surrogate
                highsurrogate = cc;
            }
            else {
                if (cc <= 0x7F) { outputString += ' '+dec2hex2(cc); }
                else if (cc <= 0x7FF) { outputString += ' '+dec2hex2(0xC0 | ((cc>>6) & 0x1F)) + ' ' + dec2hex2(0x80 | (cc & 0x3F)); }
                else if (cc <= 0xFFFF) { outputString += ' '+dec2hex2(0xE0 | ((cc>>12) & 0x0F)) + ' ' + dec2hex2(0x80 | ((cc>>6) & 0x3F)) + ' ' + dec2hex2(0x80 | (cc & 0x3F)); }
            }
        }
        return outputString.substring(1);
    }
    function convertCharStr2UTF16 ( str ) {
        // Converts a string of characters to UTF-16 code units, separated by spaces
        // str: sequence of Unicode characters
        var highsurrogate = 0;
        var suppCP;
        var n = 0;
        var outputString = '';
        for (var i = 0; i < str.length; i++) {
            var cc = str.charCodeAt(i);
            if (cc < 0 || cc > 0xFFFF) {
                outputString += '!Error in convertCharStr2UTF16: unexpected charCodeAt result, cc=' + cc + '!';
            }
            if (highsurrogate != 0) {
                if (0xDC00 <= cc && cc <= 0xDFFF) {
                    suppCP = 0x10000 + ((highsurrogate - 0xD800) << 10) + (cc - 0xDC00);
                    suppCP -= 0x10000; outputString += dec2hex4(0xD800 | (suppCP >> 10)) + ' ' + dec2hex4(0xDC00 | (suppCP & 0x3FF)) + ' ';
                    highsurrogate = 0;
                    continue;
                }
                else {
                    outputString += 'Error in convertCharStr2UTF16: low surrogate expected, cc=' + cc + '!';
                    highsurrogate = 0;
                }
            }
            if (0xD800 <= cc && cc <= 0xDBFF) { // start of supplementary character
                highsurrogate = cc;
            }
            else {
                result = dec2hex(cc)
                while (result.length < 4) result = '0' + result
                outputString += result + ' '
            }
        }
        return outputString.substring(0, outputString.length-1);
    }
    function convertCharStr2jEsc ( str, parameters ) {
        // Converts a string of characters to JavaScript escapes
        // str: sequence of Unicode characters
        // parameters: a semicolon separated string showing ids for checkboxes that are turned on
        var highsurrogate = 0;
        var suppCP;
        var pad;
        var n = 0;
        var pars = parameters.split(';')
        var outputString = '';
        for (var i = 0; i < str.length; i++) {
            var cc = str.charCodeAt(i);
            if (cc < 0 || cc > 0xFFFF) {
                outputString += '!Error in convertCharStr2UTF16: unexpected charCodeAt result, cc=' + cc + '!';
            }
            if (highsurrogate != 0) { // this is a supp char, and cc contains the low surrogate
                if (0xDC00 <= cc && cc <= 0xDFFF) {
                    suppCP = 0x10000 + ((highsurrogate - 0xD800) << 10) + (cc - 0xDC00);
                    if (parameters.match(/cstyleSC/)) {
                        pad = suppCP.toString(16);
                        while (pad.length < 8) { pad = '0'+pad; }
                        outputString += '\\U'+pad;
                    }
                    else if (parameters.match(/es6styleSC/)) {
                        pad = suppCP.toString(16)
                        outputString += '\\u{'+pad+'}'
                    }
                    else {
                        suppCP -= 0x10000;
                        outputString += '\\u'+ dec2hex4(0xD800 | (suppCP >> 10)) +'\\u'+ dec2hex4(0xDC00 | (suppCP & 0x3FF));
                    }
                    highsurrogate = 0;
                    continue;
                }
                else {
                    outputString += 'Error in convertCharStr2UTF16: low surrogate expected, cc=' + cc + '!';
                    highsurrogate = 0;
                }
            }
            if (0xD800 <= cc && cc <= 0xDBFF) { // start of supplementary character
                highsurrogate = cc;
            }
            else { // this is a BMP character
                //outputString += dec2hex(cc) + ' ';
                switch (cc) {
                    case 0: outputString += '\\0'; break;
                    case 8: outputString += '\\b'; break;
                    case 9: if (parameters.match(/noCR/)) {outputString += '\\t';} else {outputString += '\t'}; break;
                    case 10: if (parameters.match(/noCR/)) {outputString += '\\n';} else {outputString += '\n'}; break;
                    case 13: if (parameters.match(/noCR/)) {outputString += '\\r';} else {outputString += '\r'}; break;
                    case 11: outputString += '\\v'; break;
                    case 12: outputString += '\\f'; break;
                    case 34: if (parameters.match(/noCR/)) {outputString += '\\\"';} else {outputString += '"'}; break;
                    case 39: if (parameters.match(/noCR/)) {outputString += "\\\'";} else {outputString += '\''}; break;
                    case 92: outputString += '\\\\'; break;
                    default:
                        if (cc > 0x1f && cc < 0x7F) { outputString += String.fromCharCode(cc); }
                        else {
                            pad = cc.toString(16).toUpperCase();
                            while (pad.length < 4) { pad = '0'+pad; }
                            outputString += '\\u'+pad;
                        }
                }
            }
        }
        return outputString;
    }
    function convertCharStr2CSS ( str ) {
        // Converts a string of characters to CSS escapes
        // str: sequence of Unicode characters
        var highsurrogate = 0;
        var suppCP;
        var pad;
        var outputString = '';
        for (var i = 0; i < str.length; i++) {
            var cc = str.charCodeAt(i);
            if (cc < 0 || cc > 0xFFFF) {
                outputString += '!Error in convertCharStr2CSS: unexpected charCodeAt result, cc=' + cc + '!';
            }
            if (highsurrogate != 0) { // this is a supp char, and cc contains the low surrogate
                if (0xDC00 <= cc && cc <= 0xDFFF) {
                    suppCP = 0x10000 + ((highsurrogate - 0xD800) << 10) + (cc - 0xDC00);
                    pad = suppCP.toString(16).toUpperCase();
                    if (suppCP < 0x10000) { while (pad.length < 4) { pad = '0'+pad; } }
                    else { while (pad.length < 6) { pad = '0'+pad; } }
                    outputString += '\\'+pad+' ';
                    highsurrogate = 0;
                    continue;
                }
                else {
                    outputString += 'Error in convertCharStr2CSS: low surrogate expected, cc=' + cc + '!';
                    highsurrogate = 0;
                }
            }
            if (0xD800 <= cc && cc <= 0xDBFF) { // start of supplementary character
                highsurrogate = cc;
            }
            else { // this is a BMP character
                if (cc == 0x5C) { outputString += '\\\\'; }
                else if (cc > 0x1f && cc < 0x7F) { outputString += String.fromCharCode(cc); }
                else if (cc == 0x9 || cc == 0xA || cc == 0xD) { outputString += String.fromCharCode(cc); }
                else /* if (cc > 0x7E) */ {
                    pad = cc.toString(16).toUpperCase();
                    while (pad.length < 4) { pad = '0'+pad; }
                    outputString += '\\'+pad+' ';
                }
            }
        }
        return outputString;
    }
    function convertCharStr2CP ( textString, parameters, pad, type ) {
        // converts a string of characters to code points, separated by space
        // textString: string, the string to convert
        // parameters: string enum [ascii, latin1], a set of characters to not convert
        // pad: boolean, if true, hex numbers lower than 1000 are padded with zeros
        // type: string enum[hex, dec, unicode, zerox], whether output should be in hex or dec or unicode U+ form
        var haut = 0;
        var n = 0;
        var CPstring = '';
        var afterEscape = false;
        for (var i = 0; i < textString.length; i++) {
            var b = textString.charCodeAt(i);
            if (b < 0 || b > 0xFFFF) {
                CPstring += 'Error in convertChar2CP: byte out of range ' + dec2hex(b) + '!';
            }
            if (haut != 0) {
                if (0xDC00 <= b && b <= 0xDFFF) {
                    if (afterEscape) { CPstring += ' '; }
                    if (type == 'hex') {
                        CPstring += dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00));
                    }
                    else if (type == 'unicode') {
                        CPstring += 'U+'+dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00));
                    }
                    else if (type == 'zerox') {
                        CPstring += '0x'+dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00));
                    }
                    else {
                        CPstring += 0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00);
                    }
                    haut = 0;
                    continue;
                    afterEscape = true;
                }
                else {
                    CPstring += 'Error in convertChar2CP: surrogate out of range ' + dec2hex(haut) + '!';
                    haut = 0;
                }
            }
            if (0xD800 <= b && b <= 0xDBFF) {
                haut = b;
            }
            else {
                if (b <= 127 && parameters.match(/ascii/)) {
                    CPstring += textString.charAt(i);
                    afterEscape = false;
                }
                else if (b <= 255 && parameters.match(/latin1/)) {
                    CPstring += textString.charAt(i);
                    afterEscape = false;
                }
                else {
                    if (afterEscape) { CPstring += ' '; }
                    if (type == 'hex') {
                        cp = dec2hex(b);
                        if (pad) { while (cp.length < 4) { cp = '0'+cp; } }
                    }
                    else if (type == 'unicode') {
                        cp = dec2hex(b);
                        if (pad) { while (cp.length < 4) { cp = '0'+cp; } }
                        CPstring += 'U+';
                    }
                    else if (type == 'zerox') {
                        cp = dec2hex(b);
                        if (pad) { while (cp.length < 4) { cp = '0'+cp; } }
                        CPstring += '0x';
                    }
                    else {
                        cp = b;
                    }
                    CPstring += cp;
                    afterEscape = true;
                }
            }
        }
        return CPstring;
    }
    function convertCharStr2Unicode ( textString, preserve, pad ) { alert('here');
        // converts a string of characters to U+... notation, separated by space
        // textString: string, the string to convert
        // preserve: string enum [ascii, latin1], a set of characters to not convert
        // pad: boolean, if true, hex numbers lower than 1000 are padded with zeros
        var haut = 0;
        var n = 0;
        var CPstring = ''; pad=false;
        for (var i = 0; i < textString.length; i++) {
            var b = textString.charCodeAt(i);
            if (b < 0 || b > 0xFFFF) {
                CPstring += 'Error in convertChar2CP: byte out of range ' + dec2hex(b) + '!';
            }
            if (haut != 0) {
                if (0xDC00 <= b && b <= 0xDFFF) {
                    CPstring += 'U+' + dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00)) + ' ';
                    haut = 0;
                    continue;
                }
                else {
                    CPstring += 'Error in convertChar2CP: surrogate out of range ' + dec2hex(haut) + '!';
                    haut = 0;
                }
            }
            if (0xD800 <= b && b <= 0xDBFF) {
                haut = b;
            }
            else {
                if (b <= 127 && preserve == 'ascii') {
                    CPstring += textString.charAt(i)+' ';
                }
                else if (b <= 255 && preserve == 'latin1') {
                    CPstring += textString.charAt(i)+' ';
                }
                else {
                    cp = dec2hex(b);
                    if (pad) { while (cp.length < 4) { cp = '0'+cp; } }
                    CPstring += 'U+' + cp + ' ';
                }
            }
        }
        return CPstring.substring(0, CPstring.length-1);
    }


    var ACTION = types.ACTION;

    var _debug = false;
    var _debugWidgets = true;

    var OPEN_DELAY = 200;

    function correctPosition1(_root,$sub){
        //reset
        $sub.css({
            //top:0,
            left:0
        })

        var parent = _root.parent();
        var parentTopAbs = parent.offset().top;

        var rootOffset = _root.offset();
        var rootPos = _root.position();//top:0
        var rootHeight = _root.height();

        var deltaTop = rootOffset.top - rootPos.top;
        var parentDelta = parentTopAbs - rootOffset.top;

        var newTop = (rootPos.top + rootHeight) + parentDelta;
        $sub.css({
            top:newTop,
            left:0
        })
        var autoH = $sub.height() + 0;
        var totalH = $('html').height();
        var pos = $sub.offset();
        var overlapYDown = totalH - (pos.top + autoH);
        if((pos.top + autoH) > totalH){
            $sub.css({
                top: overlapYDown - 30
            }).fadeIn(options.fadeSpeed);
        }

        ////////////////////////////////////////////////////////////
        var subWidth = $sub.width(),
            subLeft = $sub.offset().left,
            collision = (subWidth+subLeft) > window.innerWidth;
        if(collision){
            $sub.addClass('drop-left');
        }
    }

    function correctPosition(_root,$sub){


        var level = 0;
        var _levelAttr = $sub.attr('level');
        if(!_levelAttr){
            _levelAttr = parseInt($sub.parent().attr('level'));
        }

        var _left = 0;
        if(_levelAttr==0){
            $sub.css({
                left:0
            })
        }else{

        }
        var parent = _root.parent();
        var parentTopAbs = parent.offset().top;

        var rootOffset = _root.offset();
        var rootPos = _root.position();//top:0
        var rootHeight = _root.height();

        var deltaTop = rootOffset.top - rootPos.top;
        var parentDelta = parentTopAbs - rootOffset.top;

        var newTop = (rootPos.top + rootHeight) + parentDelta;

        var offset = -20;
        if(_levelAttr!==0) {
            newTop += offset;
        }
        $sub.css({
            top:newTop
        })

        var autoH = $sub.height() + 0;
        var totalH = $('html').height();
        var pos = $sub.offset();
        var overlapYDown = totalH - (pos.top + autoH);
        if((pos.top + autoH) > totalH){
            $sub.css({
                top: overlapYDown - 30
            }).fadeIn(options.fadeSpeed);
        }

        ////////////////////////////////////////////////////////////
        var subWidth = $sub.width(),
            subLeft = $sub.offset().left,
            collision = (subWidth+subLeft) > window.innerWidth;

        if(collision){
            $sub.addClass('drop-left');
        }
    }

    var createCallback = function(func,menu,item) {

        return function(event) {
            func(event, menu,item);
        };
    }
    console.clear();

    var _ctx = {}

    var myString = "test \x7f asd \n \t  dsf \r";
    var pattern = /[^\x20-\x7E]/gim;
    var match = pattern.exec(myString);
    var matches = myString.match(pattern);
    var newString = "" + myString;

    myString.replace(pattern, function(a, p1, p2, p3, offset, string){
        //console.error(utils.hexEncode(a));
        var _raw = utils.hexEncode(a,'$');
        if(_raw.length==2){
            _raw = _raw.replace("$","$0")
        }
        //console.error(_raw);
        myString = myString.replace(a,_raw);
    });

    utils.hexEncode2 = function(str,prefix){
        var hex, i;
        prefix  = prefix !==null ? prefix : "000";

        var result = "";
        for (i=0; i<str.length; i++) {
            hex = str.charCodeAt(i).toString(16);
            result += (prefix+hex).slice(-4);
        }

        return result
    }

    var _inputString = "x21 x4A00";


    var inputString = "PW?x0A";

    



    function escapeRegExp(string){
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
    }
    var stringEscaped = escapeRegExp("\xfe");

    var stringEscaped2 = JSON.stringify("\xfe");
    //var stringUnEscaped = JSON.parse('\x20');
    //console.error('_' + stringEscaped2 + '_');
    //debugger;

    function test(what){
        return convertAllEscapes(what,"none");
    }

    window.xtest = test;




    return;


    String.prototype.decodeEscapeSequence = function() {
        return this.replace(/x([0-9A-Fa-f]{2})/gmi, function() {
            //console.log('match ',arguments);
            return String.fromCharCode(parseInt(arguments[1], 16));
        });
    };
    //console.log("http\\x3a\\x2f\\x2fwww.example.com".decodeEscapeSequence());


    var escaped = inputString.decodeEscapeSequence();

    //debugger;

    console.log(escaped + ' ' +utils.stringToHex(escaped) + ' escaped l ' + escaped.length);

    //console.log(inputString.decodeEscapeSequence());
    //console.log(inputString.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'));



    var _stringRepl = function(tmpl){
        // summary:
        //		Does substitution of ${foo} type properties in template string
        // tags:
        //		private
        var className = this.declaredClass, _this = this;
        // Cache contains a string because we need to do property replacement
        // do the property replacement
        return string.substitute(tmpl, this, function(value, key){
            if(key.charAt(0) == '!'){
                value = lang.getObject(key.substr(1), false, _this);
            }
            if(typeof value == "undefined"){
                throw new Error(className+" template:"+key);
            } // a debugging aide

            if(value == null){
                return "";
            }
            // Substitution keys beginning with ! will skip the transform step,
            // in case a user wishes to insert unescaped markup, e.g. ${!foo}
            return key.charAt(0) == "!" ? value : _escapeValue("" + value);
        }, this);
    }

    var _escapeValue = function( val){
        // summary:
        //		Escape a value to be inserted into the template, either into an attribute value
        //		(ex: foo="${bar}") or as inner text of an element (ex: <span>${foo}</span>)

        // Safer substitution, see heading "Attribute values" in
        // http://www.w3.org/TR/REC-html40/appendix/notes.html#h-B.3.2
        // and also https://www.owasp.org/index.php/XSS_%28Cross_Site_Scripting%29_Prevention_Cheat_Sheet#RULE_.231_-_HTML_Escape_Before_Inserting_Untrusted_Data_into_HTML_Element_Content
        return val.replace(/["'<>&]/g, function(val){
            return {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                "\"": "&quot;",
                "'": "&#x27;"
            }[val];
        });
    };

    var _message ="00500057005300540041004e004400420059000d0040005000570052003a0031000d";
    var _message ="PWSTANDBY\n";

    var zero = function (n, max) {
        n = n.toString(16).toUpperCase();
        while (n.length < max) {
            n = '0' + n;
        }
        return n;
    };

    function d2h(d) {
        return d.toString(16);
    }
    function h2d (h) {
        return parseInt(h, 16);
    }
    function stringToHex (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            var part = zero(d2h(c),2);
            var d = String.fromCharCode( h2d( part ) );
            console.log('-'+ d);
            str += utils.markHex(d) + ' ';
        }
        return str;
    }
    function hexToString (tmp) {
        var arr = tmp.split(' '),
            str = '',
            i = 0,
            arr_len = arr.length,
            c;

        for (; i < arr_len; i += 1) {
            c = String.fromCharCode( h2d( arr[i] ) );
            str += c;
        }

        return str;
    }
    function hex2a(hexx) {
        var hex = hexx.toString();//force conversion
        var str = '';
        for (var i = 0; i < hex.length; i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    }


    /*********************************************************/

    //console.error(pretty(_message));

    //_message = utils.hexEncode(_message);
    //_message = (_message);

    //_message = _message.replace(/(.{2})/g,"$1 ");
    //console.error(utils.markHex(stringToHex(_message)));
    //_message = utils.markHex(_message,"n","e");


    //PWSTANDBY 50575354414e444259 | 50575354414e444259
    //console.error(_message);


    return;

    /*********************************************************/

    //action renderer class
    var ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        templateString:'<div attachTo="navigation" class="actionToolbar">'+
            '<nav attachTo="navigationRoot" class="" role="navigation">'+
                '<ul attachTo="navBar" class="nav navbar-nav"/>'+
            '</nav>'+
        '</div>'
    });

    var MenuMixinClass =  dcl(null, {
        actionStores: null,
        correctSubMenu: false,
        _didInit: null,
        actionFilter:null,
        /**
         * Return a field from the object's given visibility store
         * @param action
         * @param field
         * @param _default
         * @returns {*}
         */
        getVisibilityField: function (action, field, _default) {
            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return actionVisibility[field] != null ? actionVisibility[field] : action[field] || _default;
        },
        /**
         * Sets a field in the object's given visibility store
         * @param action
         * @param field
         * @param value
         * @returns {*}
         */
        setVisibilityField: function (action, field, value) {
            var _default = {};
            if (action.getVisibility) {
                var actionVisibility = action.getVisibility(this.visibility) || _default;
                actionVisibility[field] = value;
            }
            return actionVisibility;
        },
        shouldShowAction: function (action) {
            if (this.getVisibilityField(action, 'show') == false) {
                return false;
            } else if (action.getVisibility && action.getVisibility(this.visibility) == null) {
                return false;
            }
            return true;
        },
        addActionStore: function (store) {
            if (!this.actionStores) {
                this.actionStores = [];
            }
            if (!this.actionStores.includes(store)) {
                this.actionStores.push(store);
            }
        },
        /**

         tree structure :

         {
            root: {
                Block:{
                    grouped:{
                        Step:[action,action]
                    }
                }
            },
            rootActions: string['File','Edit',...],

            allActionPaths: string[command],

            allActions:[action]
         }

         * @param store
         * @param owner
         * @returns {{root: {}, rootActions: Array, allActionPaths: *, allActions: *}}
         */
        buildActionTree: function (store, owner) {
            var self = this,
                allActions = self.getActions(),
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            if(!store){
                console.error('buildActionTree : invalid store');
                return null;
            }

            //if (_debug) { console.dir(_.pluck(allActions, 'command'));}
            self.wireStore(store, function (evt) {
                if (evt.type === 'update') {
                    var action = evt.target;
                    if (action.refreshReferences) {
                        var refs = action.getReferences();
                        action.refreshReferences(evt.property, evt.value);
                    }
                }
            });

            //return all actions with non-empty tab field
            var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
                groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),
            //now flatten them
                _actionsFlattened = [];

            _.each(groupedTabs, function (items, name) {
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            var rootActions = [];
            _.each(tabbedActions, function (action) {
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            if (store.menuOrder) {
                rootActions = owner.sortGroups(rootActions, store.menuOrder);
            }

            //collect all existing actions as string array
            var allActionPaths = _.pluck(allActions, 'command');
            var tree = {};

            _.each(rootActions, function (level) {
                // collect all actions at level (File/View/...)
                var menuActions = owner.getItemsAtBranch(allActions, level);
                // convert action command strings to Action references
                var grouped = self.toActions(menuActions, store);
                //apt-get install automake autopoint bison build-essential ccache cmake curl cvs default-jre fp-compiler gawk gdc gettext git-core gperf libasound2-dev libass-dev libavcodec-dev libavfilter-dev libavformat-dev libavutil-dev libbluetooth-dev libbluray-dev libbluray1 libboost-dev libboost-thread-dev libbz2-dev libcap-dev libcdio-dev libcrystalhd-dev libcrystalhd3 libcurl3 libcurl4-gnutls-dev libcwiid-dev libcwiid1 libdbus-1-dev libenca-dev libflac-dev libfontconfig-dev libfreetype6-dev libfribidi-dev libglew-dev libiso9660-dev libjasper-dev libjpeg-dev libltdl-dev liblzo2-dev libmad0-dev libmicrohttpd-dev libmodplug-dev libmp3lame-dev libmpeg2-4-dev libmpeg3-dev libmysqlclient-dev libnfs-dev libogg-dev libpcre3-dev libplist-dev libpng-dev libpostproc-dev libpulse-dev libsamplerate-dev libsdl-dev libsdl-gfx1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libshairport-dev libsmbclient-dev libsqlite3-dev libssh-dev libssl-dev libswscale-dev libtiff-dev libtinyxml-dev libtool libudev-dev libusb-dev libva-dev libva-egl1 libva-tpi1 libvdpau-dev libvorbisenc2 libxml2-dev libxmu-dev libxrandr-dev libxrender-dev libxslt1-dev libxt-dev libyajl-dev mesa-utils nasm pmount python-dev python-imaging python-sqlite swig unzip yasm zip zlib1g-dev
                // group all actions by group
                var groupedActions = _.groupBy(grouped, function (action) {
                    var _vis = (action.visibility_ || {})[visibility + '_val'] || {};
                    if (action) {
                        return _vis.group || action.group;
                    }
                });

                //temp array
                var _actions = [];
                _.each(groupedActions, function (items, level) {
                    if (level !== 'undefined') {
                        _actions = _actions.concat(items);
                    }
                });

                //flatten out again
                menuActions = _.pluck(_actions, 'command');
                menuActions.grouped = groupedActions;
                tree[level] = menuActions;
            });

            var result = {
                root: tree,
                rootActions: rootActions,
                allActionPaths: _.pluck(allActions, 'command'),
                allActions: allActions
            }

            this.lastTree = result;

            return result;
        },
        constructor: function (options, node) {
            this.target = node;
            utils.mixin(this, options);
        },
        onClose: function (e) {
            this._rootMenu && this._rootMenu.parent().removeClass('open');
        },
        onOpen: function () {
            this._rootMenu && this._rootMenu.parent().addClass('open');
        },
        init: function (opts) {
            if (this._didInit) {
                return;
            }
            this._didInit = true;
            var options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root, 'click', null, function (e) {
                if (!self.isOpen) {
                    return;
                }
                self.isOpen = false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display: ''
                }).find('.drop-left').removeClass('drop-left');
            });
            if (options.preventDoubleContext) {
                this.__on(root, 'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            this.__on(root, 'mouseenter', '.dropdown-submenu', function (e) {
                try {
                    var _root = $(e.currentTarget);
                    var $sub = _root.find('.dropdown-context-sub:first');
                    if ($sub.length == 0) {
                        return;
                    }
                    if (self.correctSubMenu == false) {
                        return;
                    }
                    if (self.menu) {
                        if (!$.contains(self.menu[0], _root[0])) {
                            console.log('not me');
                            return;
                        }
                    }
                    var _disabled = _root.hasClass('disabled');
                    if(_disabled){
                        $sub.css('display','none');
                        return;
                    }else{
                        $sub.css('display','');
                    }

                    //reset top
                    $sub.css({
                        top: 0
                    });

                    //console.log('-correct');
                    var autoH = $sub.height() + 0;
                    var totalH = $('html').height();
                    var pos = $sub.offset();
                    var overlapYDown = totalH - (pos.top + autoH);
                    if ((pos.top + autoH) > totalH) {
                        $sub.css({
                            top: overlapYDown - 30
                        }).fadeIn(options.fadeSpeed);
                    }
                    ////////////////////////////////////////////////////////////
                    var subWidth = $sub.width(),
                        subLeft = $sub.offset().left,
                        collision = (subWidth + subLeft) > window.innerWidth;

                    if (collision) {
                        $sub.addClass('drop-left');
                    }
                } catch (e) {
                    logError(e);
                }
            });
        },
        getDefaultOptions: function () {
            return options = {
                fadeSpeed: 0,
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: true
            };
        },
        buildMenuItems:function ($menu, data, id, subMenu, addDynamicTag) {
            //this._debugMenu && console.log('build - menu items ', arguments);
            var linkTarget = '',
                self = this,
                visibility = this.visibility;

            for (var i = 0; i < data.length; i++) {
                var item = data[i],
                    $sub,
                    widget = item.widget;

                if (typeof item.divider !== 'undefined' && !item.widget) {
                    var divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '' ;
                    divider += '"></li>';
                    item.widget = divider;
                    $menu.append(divider);
                } else if (typeof item.header !== 'undefined' && !item.widget) {
                    var header = '<li class="nav-header';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + item.header + '</li>';
                    item.widget = header;
                    $menu.append(header);
                } else if (typeof item.menu_item_src !== 'undefined') {

                } else {

                    if (!widget && typeof item.target !== 'undefined') {
                        linkTarget = ' target="' + item.target + '"';
                    }
                    if (typeof item.subMenu !== 'undefined' && !widget) {


                        var sub_menu = '<li class="dropdown-submenu';
                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a>';

                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }
                        sub_menu += item.text + '';
                        sub_menu += '</a></li>';
                        $sub = $(sub_menu);

                    } else {
                        if (!widget) {
                            if (item.render) {
                                $sub = item.render(item, $menu);
                            } else {
                                var element = '<li tabindex="2" class="" ';
                                element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                                element += '><a >';
                                if (typeof data[i].icon !== 'undefined') {
                                    element += '<span class="' + item.icon + '"></span> ';
                                }
                                element += item.text + '</a></li>';
                                $sub = $(element);
                                if (item.postRender) {
                                    item.postRender($sub);
                                }
                            }
                        }
                    }

                    if (typeof item.action !== 'undefined' && !item.widget) {
                        if (item.addClickHandler && item.addClickHandler() === false) {
                        } else {
                            var $action = item.action;
                            if ($sub && $sub.find) {
                                $sub.find('a')
                                    .addClass('context-event')
                                    .on('click', createCallback($action, item, $sub));
                            }
                        }
                    }

                    if ($sub && !widget) {
                        if (item.index == 0) {
                            $menu.prepend($sub);
                        } else {
                            $menu.append($sub);
                        }
                        item.widget = $sub;
                        $sub.menu = $menu;
                    }

                    if($sub){
                        $sub.attr('level',item.level);
                    }

                    if (typeof item.subMenu != 'undefined' && !item.subMenuData) {
                        var subMenuData = self.buildMenu(item.subMenu, id, true);
                        $menu.subMenuData = subMenuData;
                        item.subMenuData = subMenuData;
                        $menu.find('li:last').append(subMenuData);
                        subMenuData.attr('level',item.subMenu.level);
                        subMenuData.css('display','none');

                    }else{
                        if(item.subMenu && item.subMenuData) {
                            this.buildMenuItems(item.subMenuData, item.subMenu, id, true);
                        }
                    }
                }

                if (!$menu._didOnClick) {
                    $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                        e.stopPropagation()
                    });
                    $menu._didOnClick = true;
                }

            }
            return $menu;
        },
        buildMenu: function (data, id, subMenu) {
            var subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ',
                $menu = $('<ul tabindex="1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>');
            if (!subMenu) {
                this._rootMenu = $menu;
            }
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        createNewAction: function (command) {
            var segments = command.split('/');
            var lastSegment = segments[segments.length - 1];
            var action = new Action({
                command: command,
                label: lastSegment,
                group: lastSegment
            });
            return action;
        },
        findAction: function (command) {
            var stores = this.actionStores,
                action = null;
            _.each(stores, function (store) {
                var _action = store.getSync(command);
                if (_action) {
                    action = _action;
                }
            });

            return action;
        },
        getAction: function (command, store) {
            store = store || this.store;
            var action = null;
            if (store) {
                action = this.findAction(command);
                if (!action) {
                    action = this.createNewAction(command);
                }
            }
            return action;
        },
        getActions: function (query) {
            var result = [];
            var stores = this.actionStores,
                self = this,
                visibility = this.visibility;
            query = query || this.actionFilter;

            _.each(stores, function (store) {
                if(store) {//tmpFix
                    result = result.concat(store.query(query));
                }
            });
            result = result.filter(function (action) {
                var actionVisibility = action.getVisibility != null ? action.getVisibility(visibility) : {};
                if (action.show === false || actionVisibility === false || actionVisibility.show === false) {
                    return false;
                }
                return true;
            });
            return result;
        },
        toActions: function (commands, store) {
            var result = [],
                self = this;
            _.each(commands, function (path) {
                var _action = self.getAction(path, store);
                _action && result.push(_action);
            });
            return result;
        },
        onRunAction: function (action, owner, e) {
            var command = action.command;
            action = this.findAction(command);
            //_debug && console.log('run action ', action.command);
            return DefaultActions.defaultHandler.apply(action.owner || owner, [action, e]);
        },
        getActionProperty: function (action, visibility, prop) {
            var value = prop in action ? action[prop] : null;
            if (visibility && prop in visibility) {
                value = visibility[prop];
            }
            return value;
        },
        toMenuItem: function (action, owner, label, icon, visibility, showKeyCombo) {
            var self = this,
                labelLocalized = self.localize(label),
                widgetArgs = visibility.widgetArgs,
                actionType = visibility.actionType || action.actionType;

            var item = {
                text: labelLocalized,
                icon: icon,
                data: action,
                owner: owner,
                command: action.command,
                addClickHandler: function (item) {
                    var action = this.data;
                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        return false;
                    }
                    return true;
                },
                render: function (data, $menu) {
                    var action = this.data;
                    var parentAction = action.getParent ? action.getParent() : null;
                    var closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
                    var keyComboString = ' \n';
                    if (action.keyboardMappings && showKeyCombo !== false) {

                        var mappings = action.keyboardMappings;
                        var keyCombos = _.pluck(mappings, ['keys']);
                        if (keyCombos && keyCombos[0]) {
                            keyCombos = keyCombos[0];
                            if (keyCombos.join) {
                                var keycombo = [];
                                keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                            }
                        }
                    }

                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        var element = '<li class="" >';
                        var id = action._store.id + '_' + action.command + '_' + self.id;
                        var checked = action.get('value');
                        //checkbox-circle
                        element += '<div style="90%" class="checkbox checkbox-success ">';
                        element += '<input id="' + id + '" type="checkbox" ' + (checked == true ? 'checked' : '') + '>';
                        element += '<label for="' + id + '">';
                        element += self.localize(data.text);
                        element += '</label>';
                        element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                        element += '</div>';

                        $menu.addClass('noclose');
                        var result = $(element);
                        var checkBox = result.find('INPUT');
                        checkBox.on('change', function (e) {
                            action._originReference = data;
                            action._originEvent = e;
                            action.set('value', checkBox[0].checked);
                            action._originReference = null;
                        });
                        self.setVisibilityField(action, 'widget', data);
                        return result;
                    }
                    closeOnClick === false && $menu.addClass('noclose');
                    if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                        var value = action.value || action.get('value');
                        var parentValue = parentAction.get('value');
                        if (value == parentValue) {
                            icon = 'fa fa-check';
                        }
                    }
                    var title = data.text || labelLocalized || self.localize(action.title);

                    //default:
                    var element = '<li><a title="' + title + ' ' +keyComboString + '">';
                    var _icon = data.icon || icon;

                    //icon
                    if (typeof _icon !== 'undefined') {
                        //already html string
                        if (/<[a-z][\s\S]*>/i.test(_icon)) {
                            element += _icon;
                        } else {
                            element += '<span class="icon ' + _icon + '"/> ';
                        }
                    }
                    element += data.text;
                    element += '<span style="max-width:100px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span></a></li>';
                    self.setVisibilityField(action, 'widget', data);
                    return $(element);
                },
                get: function (key) {
                },
                set: function (key, value) {
                    //_debugWidgets && _.isString(value) && console.log('set ' + key + ' ' + value);
                    var widget = this.widget,
                        action = this.data;
                    function updateCheckbox(widget, checked) {
                        var _checkBoxNode = widget.find('INPUT');
                        var what = widget.find("input[type=checkbox]");
                        if (what) {
                            if (checked) {
                                what.prop("checked", true);
                            } else {
                                what.removeAttr('checked');
                            }
                        }
                    }
                    if (widget) {
                        if (key === 'disabled') {
                            if (widget.toggleClass) {
                                widget.toggleClass('disabled', value);
                            }
                        }
                        if (key === 'icon') {
                            var _iconNode = widget.find('.icon');
                            if (_iconNode) {
                                _iconNode.attr('class', 'icon');
                                this._lastIcon = this.icon;
                                this.icon = value;
                                _iconNode.addClass(value);
                            }
                        }
                        if (key === 'value') {
                            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE ||
                                actionType === types.ACTION_TYPE.SINGLE_TOGGLE) {
                                updateCheckbox(widget, value);
                            }
                        }
                    }
                },
                action: function (e, data, menu) {
                    _debug && console.log('menu action', data);
                    return self.onRunAction(data.data, owner, e);
                },
                destroy: function () {
                    if (this.widget) {
                        this.widget.remove();
                    }
                }
            }
            return item;
        },
        attach: function (selector, data) {
            this.target = selector;
            this.menu = this.addContext(selector, data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
            return this.menu;
        },
        addReference: function (action, item) {
            if (action.addReference) {
                action.addReference(item, {
                    properties: {
                        "value": true,
                        "disabled": true,
                        "enabled": true
                    }
                }, true);
            }
        },
        onDidRenderActions: function (store, owner) {
            if (owner && owner.refreshActions) {
                owner.refreshActions();
            }
        },
        getActionData: function (action) {
            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label: actionVisibility.label != null ? actionVisibility.label : action.label,
                icon: actionVisibility.icon != null ? actionVisibility.icon : action.icon,
                command: actionVisibility.command != null ? actionVisibility.command : action.command,
                visibility: actionVisibility,
                group: actionVisibility.group != null ? actionVisibility.group : action.group,
                widget: actionVisibility.widget
            }
        },
        _clearAction: function (action) {

        },
        _clear: function () {
            var actions = this.getActions();
            var store = this.store;
            if (store) {
                this.actionStores.remove(store);
            }
            var self = this;
            actions = actions.concat(this._tmpActions);
            _.each(actions, function (action) {
                if (action) {
                    var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        var widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                        delete actionVisibility.widget;
                        actionVisibility.widget = null;
                    }
                }
            });
            this.$navBar && this.$navBar.empty();
        }
    });

    var ActionRendererClass = dcl(null,{
        renderTopLevel:function(name,where){
            where = where || $(this.getRootContainer());
            var item =$('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) +'<b class="caret"></b></a>'+
                '</li>');

            where.append(item);
            return item;
        },
        getRootContainer:function(){
            return this.navBar;
        },
        getActionData:function(action){
            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label:actionVisibility.label !=null ? actionVisibility.label : action.label,
                icon:actionVisibility.icon !=null ? actionVisibility.icon : action.icon,
                visibility: actionVisibility
            }
        }
    });

    /**
     * @extends module:xide/widgets/MainMenuActionRenderer
     */
    var MainMenu = dcl([ContainerClass,MenuMixinClass,ActionRendererClass,_XWidget.StoreMixin],{
        declaredClass:'xide.widgets.Actiontoolbar',
        target:null,
        attachToGlobal:true,
        _isFollowing:false,
        _followTimer:null,
        _zIndex:1,
        visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
        init2: function(opts){
            var options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root,'click',null,function(e){
                if(!self.isOpen){
                    return;
                }
                self.isOpen=false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display:''
                }).find('.drop-left').removeClass('drop-left');
            });

            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }
            var _root2 = this.$navigationRoot;
            this.__on(_root2,'mouseleave', '.dropdown-submenu', function(e){
                var _root = $(e.currentTarget);
                var $sub = _root.find('.dropdown-context-sub:first');
                if (self.menu) {
                    if (!$.contains(self.menu[0], _root[0])) {
                        return;
                    }
                }
                $sub.css('display','none');
                $sub.data('left',true);
                clearTimeout($sub.data('openTimer'));
            });
            function onOpened($sub){
                /*
                 var desc = ' li:not(.divider):visible a'
                 //var $items = $sub.find('[role="menu"]' + desc + ', [role="listbox"]' + desc);
                 var $items = $sub.find(desc);
                 //console.log('items',$items);
                 if($items.length){
                 $($items[0]).focus();
                 }
                 */
            }

            this.__on(_root2,'mouseenter', '.dropdown-submenu', function(e){

                var _root = $(e.currentTarget);
                if (self.menu) {
                    if (!$.contains(self.menu[0], _root[0])) {
                        return;
                    }
                }
                var $sub = _root.find('.dropdown-context-sub:first');

                $sub.css('display', 'none');
                if(self.correctSubMenu!==false){
                    correctPosition(_root,$sub);
                }
                $sub.data('left',false);
                $sub.data('openTimer',setTimeout(function(){
                    if($sub.data('left')!==true) {
                        $sub.css('display', 'block');
                        if(self.correctSubMenu!==false){
                            correctPosition(_root,$sub);
                        }

                    }else{
                        $sub.css('display', 'none');
                    }
                },OPEN_DELAY));

            });
        },
        addContext:function(selector,data){
            var id,
                $menu,
                self = this;
            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = self.buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {
                var d = new Date();

                id = d.getTime();
                $menu = self.buildMenu(data, id);
                selector.append($menu);
            }
            return $menu;
        },
        resize:function(){
            utils.resizeTo(this.navigation,this.navBar,true,false);
        },
        destroy:function(){
            utils.destroy(this.$navBar[0]);
            utils.destroy(this.$navigation[0]);
            clearTimeout(this._followTimer);
        },
        setActionStore:function(store,owner){
            this._clear();

            if(!store){
                return;
            }
            this.store = store;

            this.addActionStore(store);
            var self = this,
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            var tree = self.buildActionTree(store,owner),
                allActions = tree.allActions,
                rootActions = tree.rootActions,
                allActionPaths = tree.allActionPaths;

            // final menu data
            var data = [];
            _.each(tree.root, function (menuActions,level) {
                var groupedActions = menuActions.grouped;

                //temp group string of the last rendered action's group
                var lastGroup = '';

                _.each(menuActions,function(command){
                    var action = self.getAction(command,store);
                    var isDynamicAction = false;
                    if (!action) {
                        isDynamicAction = true;
                        action = self.createAction(command);
                    }
                    if(action){
                        var renderData = self.getActionData(action),
                            icon = renderData.icon,
                            label = renderData.label,
                            visibility = renderData.visibility,
                            group = renderData.group,
                            item = self.toMenuItem(action, owner, '', icon, visibility || {},false);

                        data.push(item);
                        visibility.widget = item;
                        self.addReference(action,item);

                        var childPaths = new Path(command).getChildren(allActionPaths,false),
                            isContainer = childPaths.length> 0,
                            childActions = isContainer ? self.toActions(childPaths,store) : null ;


                        function parseChildren(command, parent) {

                            var childPaths = new Path(command).getChildren(allActionPaths, false),
                                isContainer = childPaths.length > 0,
                                childActions = isContainer ? self.toActions(childPaths, store) : null;

                            if (childActions) {

                                var subs = [];


                                _.each(childActions, function (child) {

                                    var _renderData = self.getActionData(child);
                                    var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility);

                                    var parentLevel = parent.level || 0;

                                    _item.level = parentLevel+1;

                                    self.addReference(child, _item);
                                    subs.push(_item);

                                    var _childPaths = new Path(child.command).getChildren(allActionPaths, false),
                                        _isContainer = _childPaths.length > 0,
                                        _childActions = _isContainer ? self.toActions(_childPaths, store) : null;

                                    if (_isContainer) {
                                        parseChildren(child.command, _item);
                                    }
                                });
                                parent.subMenu = subs;
                            }
                        }

                        parseChildren(command, item);

                        item.level = 0;
                        self.buildMenuItems(rootContainer, [item], "-"+new Date().getTime());

                        /*
                        if(childActions){
                            var subs = [];
                            console.log('render child actions '+command);
                            _.each(childActions,function(child){
                                var _renderData = self.getActionData(child);
                                console.log('add child action ',child.command);
                                var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility);
                                self.addReference(child,_item);
                                subs.push(_item);
                            });
                            item.subMenu = subs;
                        }
                        self.buildMenuItems(rootContainer, [item], 'noob');
                        */
                    }
                });
            });
            var parent = $(this.domNode).parent();
            if(parent[0] && parent[0].id!=='staticTopContainer') {
                this._height = this.$navigation.height();
                if (this._height) {
                    parent.css({
                        height: this._height
                    })
                }
            }
            this.resize();
        },
        _follow:function(){
            clearTimeout(this._followTimer);
            this._isFollowing=true;
            this._zIndex =_Popup.nextZ();
            this._zIndex +=2;
            var parent = $(this.domNode).parent();
            var node = $('#staticTopContainer');
            if (!node[0]) {
                var body = $('body');
                node = $('<div id="staticTopContainer" class=""></div>');
                body.prepend(node);
            }

            var what = this.$navigation,
                heightSource = this.$navBar,
                self = this;

            node.append(what);
            console.log('append ',what);


            function follow(){
                var offset = parent.offset(),
                    currentHeight = what.height();

                if (parent.offset().top == 0 || parent.offset().left == 0 && self._hide!==false) {
                    what.css({
                        display: 'none'
                    })
                    return;
                }
                what.css({
                    top: offset.top,
                    left: offset.left,
                    position: 'absolute',
                    zIndex: self._zIndex,
                    display: 'inherit'
                });

                if (currentHeight) {
                    parent.css({
                        height: currentHeight
                    })
                    utils.resizeTo(what[0], heightSource[0], true, null);
                    utils.resizeTo(what[0], parent[0], null, true);
                    self._height = currentHeight;
                }
            }

            follow();
            this._followTimer = setInterval(function () {
                follow();
            }, 500);
        },
        _unfollow:function(newParent){
            clearTimeout(this._followTimer);
            this._isFollowing=false;
            var what = this.$navigation;
            $(newParent || this.domNode).append(what);
            what.css({
                top: 0,
                left: 0,
                position: 'relative'
            });
        },
        startup:function(){

            var parent = $(this.domNode).parent();

            this.correctSubMenu = true;
            this.init2({
                preventDoubleContext: false
            });
            this.menu = this.$navigation;
            this._height = this.$navigation.height();

            if(this.attachToGlobal) {
                console.log('attachToGlobal');
                this._follow();
                this.correctSubMenu = true;
            }
        }
    });
    dcl.chainAfter(MainMenu,'destroy');
    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        CIS;



    function doTests(tab){
        

        var grid = FTestUtils.createFileGrid('root',{},{

        },'TestGrid',module.id,false,tab);

        var menuNode = grid.header;
        //var context = new MainMenu({}, container.domNode);
        var mainMenu = utils.addWidget(MainMenu,{

        },null,menuNode,true);
        mainMenu.init({preventDoubleContext: false});
        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);


        return;
    }
    function doBlockTests(tab){


        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            __permissions:[]
        });


        var menuNode = grid.header;
        //var context = new MainMenu({}, container.domNode);
        /*
        var mainMenu = utils.addWidget(MainMenu,{
        },null,menuNode,true);
        mainMenu.init({preventDoubleContext: false});
        */
        grid.resize();
        //////////////////////////////////////////////////
        /*
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);
        */
        grid.showToolbar(true,MainMenu,menuNode,true);

        setTimeout(function(){
            grid.select([0],null,true,{
                focus:true,
                delay:10
            });
        },1000);

        //grid.add(mainMenu);
        return;
    }
    function doGlobalTests(tab){


        grid = _TestBlockUtils.createBlockGrid(ctx,tab,{
            //permissions:[]
        });


        var menuNode = grid.header;
        var mainView = ctx.mainView;
        var where = mainView.layoutTop;
        var windowManager = ctx.getWindowManager();
        var application = ctx.getApplication();


        utils.destroy(mainView.toolbar);


        var id = '__' + module.id;
        if(window[id]){
            utils.destroy(window[id]);
        }

        var mainMenu = utils.addWidget(MainMenu,{
            id:id,
            attachToGlobal:false,
            actionFilter:{
                quick:true
            }
        },null,where,true);

        window[id] = mainMenu;


        windowManager.addActionReceiver(mainMenu);
        $(mainMenu.domNode).css({
            //'float':'right',
            'height':'40px',
            'width':'50%'
        });



        mainMenu.init({preventDoubleContext: false});

        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);

        grid.showToolbar(true);

        mainMenu._registerActionEmitter(application.fileGrid);

        setTimeout(function(){
            grid.select([0],null,true,{
                focus:true,
                delay:10
            });
        },1000);




        //grid.add(mainMenu,null,false);



        return;
    }

    var ctx = window.sctx,
        root;

    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);

        //doTests(parent);
        //doGlobalTests(parent);
        doBlockTests(parent);
        return declare('a',null,{});

    }
    return _Widget;

});

