define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/utils',
    "xide/tests/TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xfile/tests/TestUtils',
    "xide/lodash",
    "xide/widgets/TemplatedWidgetBase"
], function (declare, dcl, utils, TestUtils, _Widget, module, FTestUtils, _, TemplatedWidgetBase) {

    console.clear();

    var MarkdownFileGrid = {
        getColumns: function () {
            var thiz = this;
            this.columns = [];
            function createColumn(label, field, sortable, hidden) {
                if (thiz._columns[label] != null) {
                    hidden = !thiz._columns[label];
                }
                thiz.columns.push({
                    renderExpando: label === 'Name',
                    label: label,
                    field: field,
                    sortable: sortable,
                    formatter: function (value, obj) {
                        var parts = value.split('_');
                        //strip "01_" from path
                        value = parts.length ? parts[parts.length - 1] : value;
                        //string file extension
                        value = utils.basename(value, '.md');
                        value = utils.basename(value, '.MD');
                        return thiz.formatColumn(field, value, obj);
                    },
                    hidden: hidden
                });

            }

            createColumn('Name', 'name', true, false);
            return this.columns;
        }
    };

    var MarkdownView = dcl(TemplatedWidgetBase, {
        templateString: "<div class='widget container MarkdownView'>" +
            "<div class='Page' attachTo='markdown'></div>"+
        "</div>",
        resizeToParent: true
    });

    /**
     * showdown extension to fix links
     */
    function registerShowDown(){
        showdown.extension('xcf', function () {
            return [
                {
                    type: 'output',
                    filter: function (text, converter, options) {
                        //var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
                        var expression =/href="([^\'\"]+)/g;
                        var urlRegex = new RegExp(expression);
                        text.replace(urlRegex, function (match, url, rest) {
                            //console.log('link : '+url);
                        });
                        //console.log('text: ',text);
                        return text;
                    }
                },
                {
                    type: 'lang', //or output
                    filter: function (text, converter, options) {
                        //console.log('text',text);
                        // your code here
                        // ...
                        // text is the text being parsed
                        // converter is an instance of the converter
                        // ...
                        // don't forget to return the altered text. If you don't, nothing will appear in the output
                        return text;
                    },
                    regex: /foo/g, // if filter is present, both regex and replace properties will be ignored
                    replace: 'bar'
                }
            ];
        });
    }

    /**
     * Render markdown page
     * @param fileItem {module:xfile/model/File}
     * @param where {module:xide/widgets/_Widget}
     * @param others {module:xfile/model/File[]|null} other items in the same folder. Used to render some TOC
     */
    function renderMarkdown(fileItem, where, others) {
        var ctx = this.ctx;
        var fileManager = ctx.getFileManager();
        var self = this;


        fileManager.getContent(fileItem.mount, fileItem.path, function (content) {
            self.fileItem = fileItem;
            if (typeof showdown !== 'undefined') {
                var converter = new showdown.Converter({
                    github_flavouring:true,
                    extensions: ['xcf']
                });
                converter.setOption('tables', true);
                var markdown = converter.makeHtml(content);
                if(!self.lastPreview) {
                    self.lastPreview = utils.addWidget(MarkdownView, null, self, where, true);
                    where.add(self.lastPreview);
                }
                self.lastPreview.$markdown.html(markdown);
                if(typeof hljs!=='undefined') {
                    self.lastPreview.$markdown.find('code').each(function (i, block) {
                        hljs.highlightBlock(block);
                    });
                }else{
                    console.warn('have no highlight.js');
                }

                where.resize();
            } else {
                console.warn('have no `showdown`, proceed without');
            }
        });
    }

    function gridReady() {

        registerShowDown();

        var right = this.getRightPanel(null, null, 'DefaultTab', {});
        right.closeable(false);
        right.getSplitter().pos(0.3);

        var self = this;
        var collection = self.collection;

        //  - auto render item
        //  - auto render a selected folder's _index.md
        this._on('selectionChanged', function (evt) {
            var selection = evt.selection;
            if (!selection || !selection[0]) {
                return;
            }

            var item = selection[0];
            if (item.isDir) {
                collection.open(item).then(function (items) {
                    //folder contains a standard _index.md, render it!
                    var _index = _.find(items, {
                        name: '_index.md'
                    });
                    if (_index) {
                        renderMarkdown.apply(self, [_index, right, items.filter(function (file) {
                            return file != _index;
                        })]);
                    }
                });
                return;
            }
            renderMarkdown.apply(self, [item, right]);
        });

        this._showHeader(false);

        //pre-select first index item;
        this.select(['./_index.md'], null, true, {
            append: false,
            focus: true,
            delay: 1
        });
    }

    var excludes = ["internal","assets","modules","index"];


    function doTests(tab) {
        var grid = FTestUtils.createFileGrid('docs', {}, MarkdownFileGrid, 'TestGrid', module.id, false, tab, {}, {
            micromatch: "(*.md)|(*.MD)|!(*.*)",// Only folders and markdown files
            _onAfterSort:function(data){
                data = data.filter(function(item){
                    return !excludes.includes(item.name);
                });
                return data;
            }
        });

        grid.refresh().then(function () {
            gridReady.apply(grid);
        });
    }

    var ctx = window.sctx;
    if (ctx) {
        var parent = TestUtils.createTab(null, null, module.id);
        doTests(parent);
        return declare('a', null, {});
    }
    return _Widget;
});

