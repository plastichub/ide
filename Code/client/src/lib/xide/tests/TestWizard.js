/** @module xide/wizard/WizardBase **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/Grid',
    "./TestUtils",
    'xide/_base/_Widget',
    'xide/layout/_TabContainer',
    "module",
    "xfile/tests/TestUtils"
], function (dcl,declare,types,utils,factory,
             Grid, TestUtils,_Widget,_TabContainer,module,FTestUtils) {




    console.clear();

    

    console.log('--do-tests');
    var DefaultEvents = [
        'onInit',
        'onShow',
        'onNext',
        'onPrevious',
        'onFirst',
        'onLast',
        'onBack',
        'onFinish',
        'onTabChange',
        'onTabClick',
        'onTabShow'
    ];

    var WizardProto = dcl([_Widget],{
        containerClass:_TabContainer,
        containerArgs:{
            direction:'above',
            resizeContainer:false,
            navBarClass:'navbar-inverse'
        },
        options:null,
        paneClass:null,
        panes:[],
        buttons:[],
        wizard:null,
        $wizard:null,
        templateString:'<div attachTo="domNode" />',
        _templateString:'<div attachTo="domNode" >'+
            '<div class="navbar">'+
              '<div class="navbar-inner">'+
                '<div class="">'+
            '<ul>'+
                '<li><a href="#tab1" data-toggle="tab">First</a></li>'+
                '<li><a href="#tab2" data-toggle="tab">Second</a></li>'+
                '<li><a href="#tab3" data-toggle="tab">Third</a></li>'+
            '</ul>'+
             '</div>'+
              '</div>'+
            '</div>'+
            '<div class="tab-content">'+

                '<div class="tab-pane" id="tab1">1</div>'+
                '<div class="tab-pane" id="tab2">2</div>'+
                '<div class="tab-pane" id="tab3">3</div>'+

                '<div style="float:right">'+
                    '<input type="button" class="btn btn-default button-next" name="next" value="Next" />'+
                    '<input type="button" class="btn btn-default button-last" name="last" value="Last" />'+
                '</div>'+
                '<div style="float:left">'+
                    '<input type="button" class="btn btn-default button-first" name="first" value="First" />'+
                    '<input type="button" class="btn btn-default button-previous" name="previous" value="Previous" />'+
                '</div>'+

            '</div>'+
        '</div>',
        getDefaultOptions:function(){

            var events = this.events || DefaultEvents;

            //result
            var options = {};

            //handler, emits & calls instance method if aviable
            function handler(eventKey){
                var args = Array.prototype.slice.call(arguments, 1);
                this._emit(eventKey,args);
                _.isFunction(this[eventKey]) && this[eventKey](args);
            }

            var self = this;
            _.each(events,function(event){
                options[event]=function(args){
                    handler.apply(self,[event].concat(args));
                }
            });
            return options;
        },
        getPane:function(title){
            //var pane = _.find(this.)
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Stub functions, being overriden by the actual implementation of  https://github.com/VinceG/twitter-bootstrap-wizard
        //
        next:function(){},
        previous:function(){},
        first:function(){},
        last:function(){},
        finish:function(){},
        back:function(){},
        currentIndex:function(){},
        firstIndex:function(){},
        lastIndex:function(){},
        getIndex:function(){},
        nextIndex:function(){},
        previousIndex:function(){},
        navigationLength:function(){},
        activeTab:function(){},
        nextTab:function(){},
        previousTab:function(){},
        show:function(){},
        hide:function(){},
        display:function(){},
        enable:function(){},
        disable:function(){},
        remove:function(){},
        resetWizard:function(){},
        startup:function(){

            var _prog = true;

            if(_prog) {
                var container = utils.addWidget(this.containerClass, this.containerArgs, this, this.domNode, true);
                var panes = [
                    {
                        title: "test pane 1",
                        icon: "fa-code",
                        selected: true
                    },
                    {
                        title: "test pane 2",
                        icon: "fa-play"
                    },
                    {
                        title: "test pane 3",
                        icon: "fa-terminal"
                    }
                ];

                var self = this;
                _.each(panes, function (pane) {
                    pane.widget = container._createTab(this.paneClass || pane.paneClass, pane);
                }, this);



                var buttonBarParent = container.$tabContentNode[0];

                var buttons = [
                    {
                        title: 'First',
                        cls: 'btn btn-default button-first pull-left',
                        name: 'first',
                        value: 'First'
                    },
                    {
                        title: 'Previous',
                        cls: 'btn btn-default button-previous pull-left',
                        name: 'previous',
                        value: 'Previous'
                    },
                    {
                        title: 'Last',
                        cls: 'btn btn-default button-last pull-right',
                        name: 'last',
                        value: 'Last'
                    },
                    {
                        title: 'Next',
                        cls: 'btn btn-default button-next pull-right',
                        name: 'next',
                        value: 'Next'
                    }/*,
                    {
                        title: 'Finish',
                        cls: 'btn btn-default button-finish pull-right',
                        name: 'finish',
                        value: 'Finish'
                    },
                    {
                        title: 'Back',
                        cls: 'btn btn-default button-back pull-right',
                        name: 'back',
                        value: 'Back'
                    }*/



                ]

                _.each(buttons, function (button) {
                    //pane.tab = container._createTab(this.paneClass||pane.paneClass,pane);
                    button.widget = factory.createSimpleButton(button.title, button.icon, button.cls, button.mixin, buttonBarParent);
                    button.name && $(button.widget).attr('name', button.name);
                    button.value && $(button.widget).attr('value', button.value);
                }, this);


                var options = utils.mixin(this.getDefaultOptions(),this.options);

                utils.mixin(options,{
                    tabClass: 'nav',
                    'nextSelector': '.button-next',
                    'previousSelector': '.button-previous',
                    'firstSelector': '.button-first',
                    'lastSelector': '.button-last'
                    /*
                     'finishSelector': '.button-finish',
                     'backSelector': '.button-back'*/
                });




                var wizard = this.$domNode.bootstrapWizard(options);

                this.$wizard = wizard;
                this.wizard = wizard.data('bootstrapWizard');

                utils.mixin(this,this.wizard);

                /*
                this.$wizard.on('onShow',function(e){
                    console.error('on show');
                })
                */



                //this.next();



            }else {

                this.$domNode.bootstrapWizard({
                    'nextSelector': '.button-next',
                    'previousSelector': '.button-previous',
                    'firstSelector': '.button-first',
                    'lastSelector': '.button-last'
                });
            }
        }

    });

    /**
     * @class module:xide/wizard/WizardBase
     * @extends module:xide/mixins/EventedMixin
     * @augments module:xide/_base/_Widget
     */
    var WizardBase = dcl([_Widget],{
        containerClass:_TabContainer,
        containerArgs:{

            direction:'above',
            //resizeContainer:false,
            navBarClass:'',
            resizeToParent:true
        },
        options:null,
        paneClass:null,
        panes:[],
        buttons:[],
        wizard:null,
        $wizard:null,
        buttonBar:null,
        templateString:'<div attachTo="domNode" style="height: inherit;position: relative">' +
        '<div attachTo="containerNode" style="height: auto;min-height: 300px"></div>'+
        '<div style="min-height:initial;position: absolute" attachTo="buttonBar" class="navbar bg-opaque navbar-fixed-bottom"></div>'+
        '</div>',
        getDefaultOptions:function(){

            var events = this.events || DefaultEvents;

            //result
            var options = {};


            //handler, emits & calls instance method if aviable
            function handler(eventKey,args){
                //var args = Array.prototype.slice.call(arguments, 1);

                console.log('---'+eventKey,args);
                /*
                 if(eventKey =='onTabShow'){
                 //debugger;
                 }
                 */
                this._emit(eventKey,args);
                _.isFunction(this[eventKey]) && this[eventKey].apply(this,args);
            }

            var self = this;
            _.each(events,function(event){
                options[event]=function(){
                    //var _inner = [event].concat(arguments);
                    handler.apply(self,[event,arguments]);
                }
            });
            return options;
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Stub functions, being overriden by the actual implementation of  https://github.com/VinceG/twitter-bootstrap-wizard
        //
        next:function(){},
        previous:function(){},
        first:function(){},
        last:function(){},
        finish:function(){

        },
        back:function(){},
        currentIndex:function(){},
        firstIndex:function(){},
        lastIndex:function(){},
        getIndex:function(){},
        nextIndex:function(){},
        previousIndex:function(){},
        navigationLength:function(){},
        activeTab:function(){},
        nextTab:function(){},
        previousTab:function(){},
        show:function(){},
        hide:function(){},
        display:function(){},
        enable:function(){},
        disable:function(){},
        remove:function(){},
        resetWizard:function(){},
        onWidgetAdded:function(what){

            this.add(what,null,false);
            this.resize();
        },

        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Added API
        //
        disableButton:function(selector,disabled){

            var btn = $(this.domNode).find(selector);
            btn.toggleClass('disabled', disabled);

        },
        hideButton:function(selector,hidden){
            $(this.domNode).find(selector).toggleClass('hidden', hidden);
        },
        onTabShow:function(a,b,index){
            var pane = this.panes[index];

            if(pane){

                if(!pane.widget) {
                    var parent = pane.container;

                    if (pane.create) {

                        var what =pane.create.apply(this, [parent, pane]);
                        this.onWidgetAdded(what);
                    }


                }

                pane.onShow && pane.onShow.apply(this,[pane]);
            }

        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  STD API
        //
        startup:function(){



            var container = utils.addWidget(this.containerClass, this.containerArgs, this, this.$containerNode[0], true);
            this.add(container,null,false);
            var self = this;

            _.each(this.panes, function (pane) {
                pane.container = container._createTab(this.paneClass || pane.paneClass, pane);
            }, this);


            //var buttonParent = $('<div class="buttonBar"/>');


            //var buttonBarParent = container.$tabContentNode[0];


            var buttonBarParent = this.buttonParent || this.$buttonBar[0];

            this.buttons && _.each(this.buttons, function (button) {
                //pane.tab = container._createTab(this.paneClass||pane.paneClass,pane);
                if(!button.widget) {
                    button.widget = factory.createSimpleButton(button.title, button.icon, button.cls, button.mixin, buttonBarParent);
                    button.name && $(button.widget).attr('name', button.name);
                    button.value && $(button.widget).attr('value', button.value);
                }
            }, this);


            var options = utils.mixin(this.getDefaultOptions(),this.options);
            var wizard = this.$domNode.bootstrapWizard(options);
            this.$wizard = wizard;
            this.wizard = wizard.data('bootstrapWizard');

            this.options = options;
            utils.mixin(this,this.wizard);


            this.buttons && _.each(this.buttons, function (button) {

                $(button.widget).toggleClass('disabled', button.disabled);
            });

            this.resize();

        }

    });


    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        CIS;

    /*
     * playground
     */
    var ctx = window.sctx,
        root;

    if (ctx) {

        
        var parent = TestUtils.createTab(null,null,module.id);
        var args = {
            options:{
                tabClass: 'nav',
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                'firstSelector': '.button-first',
                'lastSelector': '.button-last',
                 'finishSelector': '.button-finish',
                 'backSelector': '.button-back'
            },
            panes:[
                {
                    title: "test pane 1",
                    icon: "fa-code",
                    selected: true,
                    create:function(parent,pane){

                        pane.widget = FTestUtils.createFileGrid(null,{},null,null,null,false,parent);

                    }
                },
                {
                    title: "test pane 2",
                    icon: "fa-play"
                },
                {
                    title: "test pane 3",
                    icon: "fa-terminal"
                }
            ],
            buttons:[
                {
                    title: 'First',
                    cls: 'btn btn-default button-first pull-left',
                    name: 'first',
                    value: 'First'
                },
                {
                    title: 'Previous',
                    cls: 'btn btn-default button-previous pull-left',
                    name: 'previous',
                    value: 'Previous'
                },
                {
                    title: 'Last',
                    cls: 'btn btn-default button-last pull-right',
                    name: 'last',
                    value: 'Last'
                },
                {
                    title: 'Next',
                    cls: 'btn btn-default button-next pull-right',
                    name: 'next',
                    value: 'Next'
                }
            ]
        };


        var wizard = utils.addWidget(WizardBase,args,null,parent,true);
        //wizard.disable(1);
        //wizard.disableButton('.button-next',true);
        //wizard.hideButton('.button-last',true);

        return declare('a',null,{});

    }


    return WizardBase;

});