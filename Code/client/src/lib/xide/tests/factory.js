define([
    'xdojo/declare',
    'xide/tests/commons',
    'xide/types',
    'xide/utils',
    '../../../external/jquery-1.9.1.min.js',
    "../../../external/messenger_latest/build/js/messenger.js",
    '../../../external/Keypress/keypress.js',
    '../../../external/lodash.min.js',
    'xide/manager/Context',
    'require',
    'xide/widgets/MainMenu'

],function(declare,commons,types,utils,_jQuery,messenger,_keypress,_lodash,Context,require,MainMenu){

    var Module = declare("xide.tests.factory", null,{});
    var context = null;
    var rootNode = null;
    /**
     *
     * @returns {*}
     */
    Module.getRootNode=function(){

        if(!rootNode){
            rootNode = document.createElement('div');//$("div");
            document.body.appendChild(rootNode);
        }

        return rootNode;
    };


    /**
     *
     * @param mixin
     * @returns {widgetProto}
     */
    Module.createMainMenu=function(mixin){


        var node = Module.getRootNode(),
            ctx = Module.getContext();

        var _args = {
            _permanentActionStore:ctx.getActionStore()
        };

        if (mixin) {
            lang.mixin(_args, mixin);
        }

        return utils.addWidget(MainMenu, _args, this, node, true);

    };
    Module.getContext = function(){

        if(!context){
            context = Module.createContext();
        }

        return context;
    };

    /**
     * Create a xide context
     * @returns module:xide/manager/Context
     */
    Module.createContext = function(args){
        return  new Context(args || {});
    };

    return Module;

});