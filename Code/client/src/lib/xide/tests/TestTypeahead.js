/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "./TestUtils",
    'xide/_base/_Widget',
    "module"
], function (dcl,declare,types,utils,
             Grid, TestUtils,_Widget,module) {


    console.clear();

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;






    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;

    

    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {


        var blockManager = ctx.getBlockManager();
        var driverManager = ctx.getDriverManager();
        var deviceManager = ctx.getDeviceManager();

        var parent = TestUtils.createTab('Acc-Test',null,module.id);

        var widget = dcl(_Widget,{
            templateString:'<div>' +
            '<input attachTo="input" data-provide="typeahead" class="typeahead form-control input-transparent" type="text" placeholder="States of USA">'+
            '</div>',
            sources:null,
            options:null,
            startup:function(){
                var substringMatcher = function(strs) {
                    return function findMatches(q, cb) {
                        var matches, substringRegex;

                        // an array that will be populated with substring matches
                        matches = [];

                        // regex used to determine if a string contains the substring `q`
                        substrRegex = new RegExp(q, 'i');

                        // iterate through the pool of strings and for any string that
                        // contains the substring `q`, add it to the `matches` array
                        $.each(strs, function(i, str) {
                            console.log('test ',str);
                            if (substrRegex.test(str.label)) {
                                matches.push(str);
                            }
                        });

                        cb(matches);
                    };
                };
                var states = this.sources || [{"label":"Test","id":"1"},{"label":"Tom","id":"2"}];
                var typehead = this.$input.typeahead(utils.mixin({
                    hint: true,
                    highlight: true,
                    minLength: 1,
                    name: 'states',
                    showHintOnFocus:true,
                    source: substringMatcher(states),
                    select: function () {
                        var val = this.$menu.find('.active').data('value');
                        this.$element.data('active', val);
                        if(this.autoSelect || val) {
                            var newVal = this.updater(val);
                            // Updater can be set to any random functions via "options" parameter in constructor above.
                            // Add null check for cases when updater returns void or undefined.
                            if (!newVal) {
                                newVal = "";
                            }
                            this.$element
                                .val(this.displayText(newVal,true) || newVal)
                                .change();

                            this.afterSelect(newVal);
                        }
                        return this.hide();
                    },
                    highlighter: function(item)
                    {
                        return item;
                    },
                    displayText: function (item,isValue) {
                        !isValue && console.log('---display text ' +item.label);
                        if(isValue){
                            return item.label;
                        }
                        return item.id + ' - ' + '<span class="text-info">'+item.label+'</span>';
                    }
                },this.options));
                console.log(typehead);

            }
        });

        var options = {

        }
        utils.addWidget(widget,{
            options:options
        },null,parent,true);


        return declare('a',null,{});

    }

    return Grid;

});