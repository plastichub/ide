/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "./TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "xide/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/cookie",
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    'xide/views/ConsoleView',
    'xide/views/ConsoleKeyboardDelegate',
    'xide/views/History',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'xide/mixins/PersistenceMixin',
    'xide/views/_Console',
    'xide/views/_ConsoleWidget'

], function (declare, dcl, types,
             utils, Grid, TemplatedWidgetBase, EventedMixin,
             TestUtils, FTestUtils, _Widget, registry, module,
             cache, cookie, domConstruct, lang, string, _XWidget, ConsoleView,
             ConsoleKeyboardDelegate, History, ACEEditor, Editor,PersistenceMixin,
             Console, ConsoleWidget) {
    
    console.clear();
    console.log('--do-tests');
    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;





    /***
     * Default editor persistence for peferences in cookies!
     **/
    Editor = dcl([Editor, PersistenceMixin.dcl], {
        declaredClass:'xace.views.Editor',
        defaultPrefenceTheme: 'idle_fingers',
        defaultPrefenceFontSize: 14,
        getDefaultPreferences: function () {
            return {theme: this.defaultPrefenceTheme, fontSize: this.defaultPrefenceFontSize};
        },
        onAfterAction: function (action) {
            //console.log('onAfterAction ' + this.get('theme') + ' this ' + this.getEditor().getFontSize());
            this.savePreferences({
                theme: this.get('theme').replace('ace/theme/', ''),
                fontSize: this.getEditor().getFontSize()
            });
            return this.inherited(arguments);
        },
        /**
         * Override id for pref store:
         * know factors:
         *
         * - IDE theme
         * - per bean description and context
         * - by container class string
         * - app / plugins | product / package or whatever this got into
         * -
         **/
        toPreferenceId: function (prefix) {

            prefix = prefix || ($('body').hasClass('xTheme-transparent') ? 'xTheme-transparent' : 'xTheme-white' );

            var res = (prefix || this.cookiePrefix || '') + '_xace';

            console.log('id ' + res);
            return res;

        },
        getDefaultOptions: function () {

            //take our defaults, then mix with prefs from store,
            var _super = this.inherited(arguments),

                _prefs = this.loadPreferences(null);

            (_prefs && utils.mixin(_super, _prefs) ||
                //else store defaults
            this.savePreferences(this.getDefaultPreferences()));

            return _super;
        }
    });



    function createConsoleAceWidgetClass() {

        return dcl([ConsoleWidget, PersistenceMixin.dcl], {
            createEditor: function (ctx) {
                var editor = createEditor(this.consoleParent, this.value, this, {
                    options: this.options,
                    ctx:ctx
                });
                return editor;
            }
        });
    }

    function createConsoleWidgetClass() {

        return dcl([_Widget, PersistenceMixin.dcl], {

            declaredClass: "xide.views._ConsoleWidget",
            delegate: null,
            value: null,
            editNode: null,
            labelTextNode: null,
            labelNode: null,
            type: null,
            linkToggle: null,
            edit: null,
            consoleParent: null,
            isExpanded: false,
            theme: 'View/Themes/idle_fingers',
            consoleEditor: null,
            jsContext: null,
            //resizeToParent:true,
            templateString: '<div class="consoleWidget">' +
            '<div class="" style="margin: 0">' +
            '<div class="input-group border-top-dark">' +
            '<div attachTo="consoleParent" class="form-control input-transparent" style="height: 2em;padding: 0;margin: 0;overflow-y: auto"></div>' +
            '<div class="input-group-btn btn-toolbar">' +
            '<button attachTo="clearButton" type="button" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></button>' +
            '<button attachTo="expandButton" type="button" class="btn btn-danger btn-sm"><i class="fa fa-expand"></i></button>' +
            '<button type="button" class="btn btn-danger btn-sm" style="bottom:0"><i class="fa fa-link"></i></button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>',
            isLinked: function () {

                if (this.linkToggle) {
                    return this.linkToggle.get('checked');
                }

                return false;
            },
            getEditor: function () {
                return this.consoleEditor;
            },
            resize: function () {
                this.inherited(arguments);

                if (this.isExpanded) {
                    var total = $(this.domNode.parentNode).height();

                    $(this.consoleParent).css({
                        height: total / 2 + 'px'
                    });
                }
                this.aceEditor && this.aceEditor.resize();
            },
            onClear:function(){
                this.delegate.onButton();
            },
            maximize:function(){
                if(this.delegate && this.delegate.maximize){
                    return this.delegate.maximize();
                }
            },
            expandEditor: function () {

                var thiz = this,
                    editor = thiz.getEditor(),
                    aceEditor = this.aceEditor;

                if (thiz.isExpanded) {

                    $(thiz.consoleParent).css({
                        height: '2em'
                    });

                    thiz.isExpanded = false;
                    editor.renderer.$maxLines = 1;
                    editor.renderer.setShowGutter(false);
                    editor.renderer.setHighlightGutterLine(false);
                    aceEditor.showToolbar(false);
                } else {
                    $(thiz.consoleParent).css({
                        height: $(this.domNode.parentNode).height() / 2 + 'px'
                    });
                    editor.renderer.$maxLines = Infinity;
                    thiz.isExpanded = true;
                    editor.renderer.setShowGutter(true);
                    editor.renderer.setHighlightGutterLine(true);
                    utils.resizeTo(editor.renderer.container, thiz.consoleParent, true, true);
                    editor.resize();
                    aceEditor.resize();
                    aceEditor.showToolbar(true);
                    var toolbar = aceEditor.getToolbar();
                    toolbar && $(toolbar.domNode).css({
                        top: '0%',
                        position: "absolute"
                    });
                }

                if (this.delegate && this.delegate.onConsoleExpanded) {
                    this.delegate.onConsoleExpanded();
                }

                this.resize();
            },
            createEditor: function () {
                var _thiz = this;
                return createEditor(this.consoleParent, this.value, this, {
                    options: this.options,
                    ctx:this.ctx
                });
            },
            createWidgets: function () {


                var aceEditor = this.createEditor(this.ctx);


                this.add(aceEditor, null, false);
                this.aceEditor = aceEditor;
                aceEditor.showToolbar(false);

                var editor = aceEditor.getEditor(),
                    self = this;


                aceEditor.maximize = function(){
                    return self.maximize();
                }


                this.aceEditorEditor = aceEditor;
                this.consoleEditor = editor;

                editor.renderer.$maxLines = 1;
                editor.renderer.setShowGutter(false);
                editor.renderer.setHighlightGutterLine(false);
                editor.$mouseHandler.$focusWaitTimout = 0;
                editor.setOptions({
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true
                });

                aceEditor.setMode(this.delegate.type);
                aceEditor.set('value', this.value);
                aceEditor.runAction(this.theme);
                aceEditor.set('value', this.value);


                $(this.expandButton).click(function (e) {
                    return self.expandEditor();
                });

                $(this.clearButton).on('click', function () {

                    if (self.delegate && self.delegate.onButton) {
                        self.delegate.onButton();
                    }
                });

                this.expandEditor();

                editor.commands.bindKeys({
                    "Ctrl-Return": function (cmdLine) {
                        if (self.isExpanded) {
                            editor.focus();
                            self.onEnter(editor.getValue());
                        } else {
                            //editor.insert("\n");
                            editor.focus();
                            self.onEnter(editor.getValue());
                        }

                    },
                    "Shift-Return": function (cmdLine) {
                        self.onClear();
                    },
                    "Esc|Shift-Esc": function (cmdLine) {
                        editor.focus();
                    },
                    "Return": function (cmdLine) {
                        var command = editor.getValue().split(/\s+/);
                        if (self.isExpanded) {
                            editor.insert("\n");
                        } else {
                            editor.focus();
                            self.onEnter(editor.getValue());
                        }
                    }
                });
                editor.commands.removeCommands(["find", "gotoline", "findall", "replace", "replaceall"]);
            },
            getValue: function () {
                return this.consoleEditor.getValue();
            },
            startup: function () {
                this.history = new History();
                this.inherited(arguments);
                this.createWidgets();
            },
            onEnter: function (val) {
                this.delegate.onEnter(val, this.isExpanded == false);
                this.history.push(val);
            }
        });

    }


    function createEditor(root, value, owner, mixin) {

        var item = {
            filePath: '',
            fileName: ''
        };
        var title = "No Title";

        var args = {
            _permissions: [],
            item: item,
            value: value,
            style: 'padding:0px;',
            iconClass: 'fa-code',
            options: utils.mixin(mixin, {
                filePath: item.path,
                fileName: item.name
            }),
            ctx: ctx,
            /***
             * Provide a text editor store delegate
             */
            storeDelegate: {
                getContent: function (onSuccess, _item) {
                    var file = _item || item;
                    return thiz.ctx.getFileManager().getContent(file.mount, file.path, onSuccess);
                },
                saveContent: function (value, onSuccess, onError) {
                    return thiz.ctx.getFileManager().setContent(item.mount, item.path, value, onSuccess);
                }
            },
            title: title
        };


        utils.mixin(args, mixin);

        editor = utils.addWidget(Editor, args, owner, root, true, null, null, false);

        editor.resize();

        return editor;
    }

    function createShellView(tab, type) {

        var view = utils.addWidget(Console, {
            type: type,
            value: "ls",
            serverClass: 'XShell',
            consoleClass: createConsoleAceWidgetClass(),
            server: null,
            showProgress: false,
            getServer: function () {
                return this.server || ctx.fileManager;
            },
            delegate: {
                onServerResponse: function (theConsole, data, addTimes) {

                    if (theConsole && data && theConsole.owner && theConsole.owner.onServerResponse) {
                        theConsole.owner.onServerResponse(data, addTimes);
                    }
                },
                runBash: function (theConsole, value, cwd) {

                    var thiz = this;
                    var server = ctx.fileManager;
                    var _value = server.serviceObject.base64_encode(value);
                    server.runDeferred('XShell', 'run', ['sh', _value, cwd]).then(function (response) {
                        thiz.onServerResponse(theConsole, response, false);
                    });
                },
                runPHP: function (theConsole, value, cwd) {
                    var thiz = this;

                    var _cb = function (response) {
                        thiz.onServerResponse(theConsole, response, false);
                    };

                    console.log('send : ' + value);
                    console.log('cwd ' + cwd);

                    var server = ctx.fileManager;
                    var _value = server.serviceObject.base64_encode(value);
                    server.runDeferred('XShell', 'run', ['php', _value, cwd]).then(function (response) {
                        console.error('respo');
                    });

                },
                runJavascript: function (theConsole, value, context, args) {

                    var _function = new Function("{" + value + "; }");

                    var response = _function.call(context, args);
                    if (response != null) {
                        console.error('response : ' + response);
                        this.onServerResponse(theConsole, response);
                        return response;
                    }


                    return value;

                },
                onConsoleCommand: function (data, value) {

                    var thiz = this,
                        theConsole = data.console;


                    if (theConsole.type === 'sh') {

                        value = value.replace(/["'`]/g, "");

                        var dstPath = null
                        if (theConsole.isLinked()) {
                            dstPath = this.getCurrentPath();
                        }
                        return this.runBash(theConsole, value, dstPath);
                    }

                    if (theConsole.type === 'php') {

                        value = value.replace(/["'`]/g, "");

                        var dstPath = null
                        if (theConsole.isLinked()) {
                            dstPath = this.getCurrentPath();
                        }
                        return this.runPHP(theConsole, value, dstPath);
                    }

                    if (theConsole.type === 'javascript') {
                        return this.runJavascript(theConsole, value);
                    }
                },
                onConsoleEnter: function (data, input) {
                    return this.onConsoleCommand(data, input);
                }
            },
            _toString: function (str, addTimes) {

                if (addTimes !== false) {
                    return this.addTime(str);
                } else {
                    return str;
                }
            },
            addTime: function (str) {
                return moment().format("HH:mm:SSS") + ' ::   ' + str + '';
            },
            log: function (msg, addTimes) {


                utils.destroy(this.progressItem);


                console.log('log');


                var out = '';
                if (_.isString(msg)) {
                    out += msg.replace(/\n/g, '<br/>');
                } else if (_.isObject(msg) || lang.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                } else if (_.isNumber(msg)) {
                    out += msg + '';
                }
                ;

                var dst = this.getLoggingContainer();
                var items = out.split('<br/>');
                var last = null;
                var thiz = this;

                for (var i = 0; i < items.length; i++) {
                    var _class = 'logEntry' + (this.lastIndex % 2 === 1 ? 'row-odd' : 'row-even');
                    last = dst.appendChild(dojo.create("div", {
                        className: _class,
                        innerHTML: this._toString(items[i], addTimes)

                    }));
                    this.lastIndex++;
                }

                if (last) {
                    last.scrollIntoViewIfNeeded();
                }

                setTimeout(function () {
                    thiz._scrollToEnd();
                }, 10);


            }
        }, this, tab, true, null, null, null);

        return view;


    }

    function addActions() {

        var result = [];
        var thiz = this;

        function createShell(action) {

            var parent = TestUtils.createTab(null, null, module.id);
            return createShellView(parent, action.shellType);
        }


        var _action = {
            label: "Javacript - Shell",
            command: "Window/JS-Shell",
            icon: 'fa-code',
            tab: "Home",
            group: 'View',
            handler: createShell,
            mixin: {
                addPermission: true,
                shellType: 'javascript'
            },
            owner: this
        }


        result.push(ctx.createAction(_action));

        _action = {
            label: "Bash - Shell",
            command: "Window/Bash-Shell",
            icon: 'fa-code',
            tab: "Home",
            group: 'View',
            handler: createShell,
            mixin: {
                addPermission: true,
                shellType: 'sh'
            },
            owner: this
        }
        result.push(ctx.createAction(_action));


        _action = {
            label: "PHP - Shell",
            command: "Window/PHP-Shell",
            icon: 'fa-code',
            tab: "Home",
            group: 'View',
            handler: createShell,
            mixin: {
                addPermission: true,
                shellType: 'php'
            },
            owner: this
        }
        result.push(ctx.createAction(_action));


        ctx.addActions(result);
        return result;

    }

    function doTests(tab, type) {

        addActions();
        return createShellView(tab, type);
    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {
        var tab = TestUtils.createTab('Test', null, 'test');
        doTests(tab, 'javascript');
        return declare('a', null, {});

    }

    return Grid;

});