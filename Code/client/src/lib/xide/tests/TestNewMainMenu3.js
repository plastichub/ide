/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry',
    'xide/_base/_Widget',
    "dojo/text!./Navbar.html",
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    'xide/action/DefaultActions',
    "xaction/ActionProvider",
    "xgrid/Clipboard",
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    'xide/mixins/EventedMixin',
    "xide/widgets/_MenuMixin2",
    "xblox/tests/TestUtils"

], function (declare,dcl,types,utils,TestUtils,_Widget,module,registry,_XWidget,MenuBarTemplate,ActionMixin,ActionContext,ActionStore,DefaultActions,ActionProvider,Clipboard,Path,Action,FTestUtils,i18,EventedMixin,MenuMixinClass,_TestBlockUtils){

    var ACTION = types.ACTION;

    var _debug = false;
    var _debugWidgets = false;

    var createCallback = function(func,menu,item) {

        return function(event) {
            func(event, menu,item);
        };
    }

    console.clear();
    var _ctx = {}
    //action renderer class
    var ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{

        templateString:'<div class="navbar mainMenu">'+
            '<nav attachTo="navigation" class="" role="navigation">'+
                    '<ul attachTo="navBar" class="nav navbar-nav"/>'+
            '</nav>'+
        '</div>'
    });


    MenuMixinClass = dcl(null, {
        actionStores:null,
        correctSubMenu:false,
        visibility:types.ACTION_VISIBILITY.CONTEXT_MENU,
        /**
         * Return a field from the object's given visibility store
         * @param action
         * @param field
         * @param _default
         * @returns {*}
         */
        getVisibilityField:function(action,field,_default){

            var actionVisibility = action.getVisibility !=null ? action.getVisibility(this.visibility) : {};

            return actionVisibility[field] !=null ? actionVisibility[field] : action[field] || _default;
        },
        /**
         * Sets a field in the object's given visibility store
         * @param action
         * @param field
         * @param value
         * @returns {*}
         */
        setVisibilityField:function(action,field,value){

            var _default = {};
            if(action.getVisibility) {
                var actionVisibility = action.getVisibility(this.visibility) || _default;
                actionVisibility[field] = value;
            }
            return actionVisibility;
        },
        shouldShowAction:function(action){

            if(this.getVisibilityField(action,'show')==false){
                return false;
            }else if(action.getVisibility && action.getVisibility(this.visibility)==null){
                return false;
            }

            return true;
        },
        addActionStore:function(store){

            if(!this.actionStores){
                this.actionStores = [];
            }

            if(!this.actionStores.includes(store)){
                this.actionStores.push(store);
            }


        },
        buildActionTree:function(store,owner){

            var self = this,
                allActions = self.getActions(),
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            self.wireStore(store,function(evt){
                if(evt.type==='update'){
                    var action = evt.target;
                    if(action.refreshReferences){
                        var refs = action.getReferences();
                        //action.refreshReferences(evt.property,evt.value);
                    }
                }
            });

            //return all actions with non-empty tab field
            var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
                }),

            //group all tabbed actions : { Home[actions], View[actions] }
                groupedTabs = _.groupBy(tabbedActions, function (action) {
                    return action.tab;
                }),
            //now flatten them
                _actionsFlattened = [];

            _.each(groupedTabs, function (items, name) {
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            var rootActions = [];
            _.each(tabbedActions, function (action) {
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            if (store.menuOrder) {
                rootActions = owner.sortGroups(rootActions, store.menuOrder);
            }

            //collect all existing actions as string array
            var allActionPaths = _.pluck(allActions, 'command');

            var tree = {};

            _.each(rootActions, function (level) {



                // collect all actions at level (File/View/...)
                var menuActions = owner.getItemsAtBranch(allActions, level);

                // convert action command strings to Action references
                var grouped = self.toActions(menuActions, store);
                //apt-get install automake autopoint bison build-essential ccache cmake curl cvs default-jre fp-compiler gawk gdc gettext git-core gperf libasound2-dev libass-dev libavcodec-dev libavfilter-dev libavformat-dev libavutil-dev libbluetooth-dev libbluray-dev libbluray1 libboost-dev libboost-thread-dev libbz2-dev libcap-dev libcdio-dev libcrystalhd-dev libcrystalhd3 libcurl3 libcurl4-gnutls-dev libcwiid-dev libcwiid1 libdbus-1-dev libenca-dev libflac-dev libfontconfig-dev libfreetype6-dev libfribidi-dev libglew-dev libiso9660-dev libjasper-dev libjpeg-dev libltdl-dev liblzo2-dev libmad0-dev libmicrohttpd-dev libmodplug-dev libmp3lame-dev libmpeg2-4-dev libmpeg3-dev libmysqlclient-dev libnfs-dev libogg-dev libpcre3-dev libplist-dev libpng-dev libpostproc-dev libpulse-dev libsamplerate-dev libsdl-dev libsdl-gfx1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libshairport-dev libsmbclient-dev libsqlite3-dev libssh-dev libssl-dev libswscale-dev libtiff-dev libtinyxml-dev libtool libudev-dev libusb-dev libva-dev libva-egl1 libva-tpi1 libvdpau-dev libvorbisenc2 libxml2-dev libxmu-dev libxrandr-dev libxrender-dev libxslt1-dev libxt-dev libyajl-dev mesa-utils nasm pmount python-dev python-imaging python-sqlite swig unzip yasm zip zlib1g-dev
                // group all actions by group
                var groupedActions = _.groupBy(grouped, function (action) {
                    if(action && action.command ==='Window/Navigation'){
                        //debugger;
                    }
                    var _vis = (action.visibility_ || {})[ visibility +'_val' ] || {};
                    if (action) {
                        return _vis.group || action.group;
                    }
                });

                //temp array
                var _actions = [];
                _.each(groupedActions, function (items, level) {
                    if (level !== 'undefined') {
                        _actions = _actions.concat(items);
                    }
                });

                //flatten out again
                menuActions = _.pluck(_actions, 'command');
                menuActions.grouped = groupedActions;
                tree[level] = menuActions;
            });

            return {
                root:tree,
                rootActions:rootActions,
                allActionPaths:_.pluck(allActions, 'command'),
                allActions:allActions
            }
        },
        constructor:function(options,node){
            this.target = node;
            utils.mixin(this,options);
        },
        onClose:function(e){
            this._rootMenu && this._rootMenu.parent().removeClass('open');
        },
        onOpen:function(){

            console.log('is open',this._rootMenu);
            var thiz=this;
            thiz._rootMenu && thiz._rootMenu.parent().addClass('open');
            setTimeout(function(){
                //thiz._rootMenu.focus();
            },1000);

        },
        init: function(opts){

            var options = this.getDefaultOptions();

            options = $.extend({}, options, opts);
            var self = this;
            var root = $(document);
            this.__on(root,'click',null,function(){


                $('.dropdown-context').fadeOut(options.fadeSpeed, function(e){
                    self.isOpen=false;
                    self.onClose(e)
                    $('.dropdown-context').css({
                        display:''
                    }).find('.drop-left').removeClass('drop-left');
                });

            });




            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }


            this.__on(root,'mouseenter', '.dropdown-submenu', function(e){

                var _root = $(e.currentTarget);

                var $sub = _root.find('.dropdown-context-sub:first');

                if(self.correctSubMenu==false){
                    return;
                }

                /*
                 console.log('mouse enter',{
                 target:$(e.target),
                 root:_root,
                 sub:$sub
                 })
                 */

                //reset top
                $sub.css({
                    top: 0
                });
                var autoH = $sub.height() + 0;
                var totalH = $('html').height();
                var pos = $sub.offset();
                var overlapYDown = totalH - (pos.top + autoH);
                if((pos.top + autoH) > totalH){
                    $sub.css({
                        top: overlapYDown - 30
                    }).fadeIn(options.fadeSpeed);
                }

                ////////////////////////////////////////////////////////////
                var subWidth = $sub.width(),
                    subLeft = $sub.offset().left,
                    collision = (subWidth+subLeft) > window.innerWidth;
                if(collision){
                    $sub.addClass('drop-left');
                }

            });
        },
        getDefaultOptions:function(){
            return options = {
                fadeSpeed: 100,
                filter: function ($obj) {
                    // Modify $obj, Do not return
                },
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: true

            };
        },
        /**
         *
         * @param $menu
         * @param data
         * @param id
         * @param subMenu
         * @param addDynamicTag
         * @returns {*}
         */
        buildMenuItems: function ($menu, data, id, subMenu, addDynamicTag) {

            var linkTarget = '',
                self = this,
                visibility = this.visibility;

            for (var i = 0; i < data.length; i++) {

                var item = data[i],
                    $sub;

                if (typeof data[i].divider !== 'undefined') {
                    var divider = '<li class="divider';
                    divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    divider += '"></li>';
                    $menu.append(divider);
                } else if (typeof data[i].header !== 'undefined') {
                    var header = '<li class="nav-header';
                    header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    header += '">' + data[i].header + '</li>';
                    $menu.append(header);
                } else if (typeof data[i].menu_item_src !== 'undefined') {
                    var funcName;
                    if (typeof data[i].menu_item_src === 'function') {
                        if (data[i].menu_item_src.name === "") { // The function is declared like "foo = function() {}"
                            for (var globalVar in window) {
                                if (data[i].menu_item_src == window[globalVar]) {
                                    funcName = globalVar;
                                    break;
                                }
                            }
                        } else {
                            funcName = data[i].menu_item_src.name;
                        }
                    } else {
                        funcName = data[i].menu_item_src;
                    }
                    $menu.append('<li class="dynamic-menu-src" data-src="' + funcName + '"></li>');
                } else {

                    if (typeof data[i].href == 'undefined') {
                        data[i].href = '#';
                    }
                    if (typeof data[i].target !== 'undefined') {
                        linkTarget = ' target="' + data[i].target + '"';
                    }
                    if (typeof data[i].subMenu !== 'undefined') {
                        var sub_menu = '<li class="dropdown-submenu';
                        sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                        sub_menu += '"><a tabindex="-1" href="' + data[i].href + '">';
                        if (typeof item.icon !== 'undefined') {
                            sub_menu += '<span class="icon ' + item.icon + '"></span> ';
                        }
                        sub_menu += data[i].text + '';
                        sub_menu += '</a></li>';
                        $sub = $(sub_menu);
                    } else {

                        if (item.render) {

                            $sub = item.render(item, $menu);

                        } else {
                            var element = '<li class="" ';
                            element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                            element += '><a tabindex="-1" href="' + data[i].href + '"' + linkTarget + '>';
                            if (typeof data[i].icon !== 'undefined') {
                                element += '<span class="glyphicon ' + data[i].icon + '"></span> ';

                            }
                            element += data[i].text + '</a></li>';
                            $sub = $(element);
                            if (item.postRender) {
                                item.postRender($sub);
                            }
                        }
                    }
                    if (typeof data[i].action !== 'undefined') {

                        if (item.addClickHandler && item.addClickHandler() === false) {

                        } else {
                            var $action = data[i].action;
                            if ($sub && $sub.find) {
                                $sub.find('a')
                                    .addClass('context-event')
                                    .on('click', createCallback($action, item, $sub));
                            }
                        }
                    }

                    $menu.append($sub);
                    item.widget = $sub;

                    if (typeof data[i].subMenu != 'undefined') {
                        var subMenuData = self.buildMenu(data[i].subMenu, id, true);
                        $menu.find('li:last').append(subMenuData);
                    }




                }



                $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                    e.stopPropagation()
                });



            }
            return $menu;
        },
        buildMenu: function (data, id, subMenu) {

            var subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ',
                $menu = $('<ul tabindex="1" aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>');

            if(!subMenu){
                this._rootMenu =$menu;
            }
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        createNewAction:function(command){

            var segments = command.split('/');
            var lastSegment = segments[segments.length - 1];
            var action = new Action({
                command: command,
                label: lastSegment,
                group: lastSegment
            });
            return action;
        },
        findAction:function(command){

            var stores = this.actionStores,
                action = null;



            _.each(stores,function(store){

                var _action = store.getSync(command);
                if(_action){
                    action = _action;
                }
            });

            return action;
        },
        getAction:function(command,store){

            store = store || this.store;

            var action = null;

            if(store) {
                action = this.findAction(command);
                if(!action){
                    action = this.createNewAction(command);
                }
            }

            return action;
        },
        getActions:function(query){
            var result = [];
            var stores = this.actionStores,
                self = this,
                visibility = this.visibility;

            _.each(stores,function(store){
                result = result.concat(store.query(query));
            });


            result = result.filter(function(action){
                var actionVisibility = action.getVisibility != null ? action.getVisibility(visibility) : {};
                if(action.show===false || actionVisibility === false || actionVisibility.show === false){
                    return false;
                }

                return true;
            });


            return result;
        },
        toActions:function(commands,store){

            var result = [],
                self = this;
            _.each(commands, function (path) {
                var _action = self.getAction(path,store);
                _action && result.push(_action);
            });
            return result;
        },
        onRunAction:function(action,owner,e){

            var command = action.command;
            action = this.findAction(command);
            _debug && console.log('run action ',action.command);
            return DefaultActions.defaultHandler.apply(action.owner||owner,[action,e]);
        },
        getActionProperty:function(action,visibility,prop){

            var value = prop in action ? action[prop] : null;
            if(visibility && prop in visibility){
                value = visibility[prop];
            }
            return value;
        },
        toMenuItem:function(action, owner, label, icon, visibility,showKeyCombo) {

            if(action.command=='Step/Move Up'){

            }

            //console.log('render ' + action.command);
            var self = this,
                labelLocalized = self.localize(label),
                widgetArgs = visibility.widgetArgs,
                actionType = visibility.actionType || action.actionType;

            var item = {
                text: labelLocalized,
                icon: icon,
                data: action,
                owner:owner,
                addClickHandler: function (item) {

                    var action = this.data;
                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                        return false;
                    }

                    return true;
                },
                render: function (data, $menu) {

                    var action = this.data;

                    var parentAction = action.getParent ? action.getParent() : null;
                    var closeOnClick = self.getActionProperty(action,visibility,'closeOnClick');

                    var keyComboString = ' \n';
                    if (action.keyboardMappings && showKeyCombo!==false) {

                        var mappings = action.keyboardMappings;
                        var keyCombos = _.pluck(mappings, ['keys']);
                        if (keyCombos && keyCombos[0]) {
                            keyCombos = keyCombos[0];

                            if (keyCombos.join) {
                                var keycombo = [];
                                keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                            }
                        }
                    }


                    if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {


                        var element = '<li class="" >';
                        var id = action._store.id + '_' + action.command + '_' + self.id;
                        var checked = action.get('value');

                        //checkbox-circle
                        element += '<div class="checkbox checkbox-success ">';
                        if (typeof data.icon !== 'undefined') {
                            //element += '<span style="float: left" class="' + data.icon + '"></span>';
                        }
                        element += '<input id="' + id + '" type="checkbox" ' + (checked == true ? 'checked' : '') + '>';
                        element += '<label for="' + id + '">';
                        element += self.localize(data.text) + '</label>';
                        element += '</div>';

                        $menu.addClass('noclose');
                        var result = $(element);
                        var checkBox = result.find('INPUT');
                        checkBox.on('change', function (e) {

                            console.warn('change ' + checkBox[0].checked);

                            action._originReference = data;
                            action._originEvent = e;

                            action.set('value', checkBox[0].checked);

                            action._originReference = null;

                        });

                        self.setVisibilityField(action,'widget',data);

                        return result;
                    }



                    closeOnClick===false && $menu.addClass('noclose');
                    if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                        var value = action.value || action.get('value');
                        var parentValue = parentAction.get('value');
                        if (value == parentValue) {
                            icon = 'fa-check';
                        }
                    }

                    //default:
                    var element = '<li ';
                    element += '';
                    element += '><a title="' + data.text + '\n' + keyComboString + '" href="' + data.href + '"' + '#' + '>';


                    //icon
                    if (typeof data.icon !== 'undefined') {
                        element += '<span class="icon ' + icon + '"></span> ';

                    }

                    element += data.text;

                    element += '<span style="max-width: 100px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';

                    element += '</a></li>';


                    self.setVisibilityField(action,'widget',data);


                    return $(element);


                },
                get: function (key) {},
                set: function (key, value) {

                    _debugWidgets && console.log('set ' + key + ' ' + value);

                    var widget = this.widget,
                        action = this.data;


                    if (widget) {
                        if (key === 'disabled') {
                            if (widget.toggleClass) {
                                widget.toggleClass('disabled', value);
                            }
                        }
                        if (key === 'icon') {
                            var _iconNode = widget.find('.icon');
                            if(_iconNode) {
                                _iconNode.attr('class', 'icon');
                                this._lastIcon = this.icon;
                                this.icon = value;
                                _iconNode.addClass(value);
                            }else{
                                console.error('cant find icon node',widget);
                            }
                        }
                        if (key === 'value') {

                            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                                var _checkBoxNode = widget.find('INPUT');
                                _checkBoxNode.attr('checked',value);
                            }

                        }
                    }
                },
                action: function (e, data, menu) {
                    _debug && console.log('menu action', data);
                    return self.onRunAction(data.data,owner,e);
                },
                destroy: function () {
                    if(this.widget){
                        this.widget.remove();
                    }
                }
            }
            return item;
        },
        attach: function(selector,data){
            this.target = selector;
            this.menu = this.addContext(selector,data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;

            registry.add(this);

            //console.dir(this.menu);

            //this.menu.addClass('active');


            //this.menu.focus();
            return this.menu;
        },
        addReference:function(action,item){

            if (action.addReference) {
                action.addReference(item, {
                    properties: {
                        "value": true,
                        "disabled": true
                    }
                }, true);
            }
        },
        onDidRenderActions:function(store,owner){

            if(owner && owner.refreshActions){
                owner.refreshActions();
            }

        },
        getActionData: function (action) {

            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label: actionVisibility.label != null ? actionVisibility.label : action.label,
                icon: actionVisibility.icon != null ? actionVisibility.icon : action.icon,
                command:actionVisibility.command != null ? actionVisibility.command : action.command,
                visibility: actionVisibility,
                group:actionVisibility.group != null ? actionVisibility.group : action.group
            }
        },
        _clear:function(){

            var actions = this.getActions();
            var store = this.store;
            if(store){
                this.actionStores.remove(store);
            }
            var self = this;

            actions = actions.concat(this._tmpActions);
            _.each(actions,function(action){
                if(action) {
                    var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        var widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        //console.log('remove reference ' + action.command,widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                    }
                }

            });

            this.$navBar && this.$navBar.empty();
        }
    });
    /**
     *
     * @class module:xide/widgets/MainMenuActionRenderer
     */
    var ActionRendererClass = dcl(null,{
        renderTopLevel:function(name,where){

            where = where || $(this.getRootContainer());

            var item =$('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) +'<b class="caret"></b></a>'+
                '</li>');

            where.append(item);

            return item;

        },
        getRootContainer:function(){
            return this.navBar;
        }
    });
    /**
     * @extends module:xide/widgets/MainMenuActionRenderer
     */
    var MainMenu = dcl([ContainerClass,ActionRendererClass,MenuMixinClass,_XWidget.StoreMixin],{
        target:null,
        visibility: types.ACTION_VISIBILITY.MAIN_MENU,
        attachToGlobal:false,
        _tmpActions:null,
        addContext:function(selector,data){
            var id,
                $menu,
                self = this;
            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = self.buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {
                var d = new Date();

                id = d.getTime();
                $menu = self.buildMenu(data, id);
                selector.append($menu);
            }

            return $menu;
        },
        onRootAction:function(level,container){
            return this.renderTopLevel(level,container);
        },
        setActionStore:function(store,owner){


            this._clear();

            this._tmpActions = [];

            this.store = store;

            this.addActionStore(store);

            var self = this,
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer()),
                tree = self.buildActionTree(store,owner),
                allActions = tree.allActions,
                rootActions = tree.rootActions,
                allActionPaths = tree.allActionPaths;

            _.each(tree.root, function (menuActions,level) {

                var root = self.onRootAction(level,rootContainer);

                // collect all actions at level (File/View/...)
                //var menuActions = owner.getItemsAtBranch(allActions,level);

                // final menu data
                var data = [];
                var groupedActions = menuActions.grouped;

                //temp group string of the last rendered action's group
                var lastGroup = '';

                _.each(menuActions,function(command){

                    var action = self.getAction(command,store);

                    var isDynamicAction = false;

                    if (!action) {
                        isDynamicAction = true;
                        action = self.createAction(command);
                        self._tmpActions.push(action);
                        console.error('add temp action');
                        self.setVisibilityField(action,'widget',{
                            widget:root,
                            destroy:function(){
                                this.widget.remove();
                            }
                        });

                    }


                    if(action){

                        var renderData = self.getActionData(action);
                        var icon = renderData.icon,
                            label = renderData.label,
                            visibility = renderData.visibility,
                            group = renderData.group;

                        if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 2) {
                            if(lastGroup!==group){
                                lastHeader = {header: i18.localize(group)},
                                lastGroup = group;
                            }
                        }
                        var item = self.toMenuItem(action, owner, label, icon, visibility || {});

                        data.push(item);

                        visibility.widget = item;

                        self.addReference(action,item);

                        var childPaths = new Path(command).getChildren(allActionPaths,false),
                            isContainer = childPaths.length> 0,
                            childActions = isContainer ? self.toActions(childPaths,store) : null ;

                        if(childActions){
                            var subs = [];
                            _.each(childActions,function(child){

                                var _renderData = self.getActionData(child);
                                //subs.push(toItem(child,owner,_renderData.label,_renderData.icon,_renderData.visibility));
                                var _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility);
                                self.addReference(child,_item);
                                subs.push(_item);
                            });
                            item.subMenu = subs;
                        }
                    }
                });

                console.error('attach  : root : ',root);

                var menu = self.attach(root,data);
                return menu;
            });

            self.onDidRenderActions(store,owner);

        },
        startup:function(){

            this.init({preventDoubleContext: false});

            if(this.attachToGlobal){

                var node = $('#staticTopContainer');
                if(!node[0]){
                    var body = $('body');
                    node = $('<div id="staticTopContainer" class=""></div>');
                    body.prepend(node);
                }
                node.append($(this.domNode));
            }
        }
    });

    var actions = [],
        thiz = this,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    var ctx = window.sctx,
        root;



    function doTests(tab){
        

        var grid = FTestUtils.createFileGrid('root',{},{

        },'TestGrid',module.id,false,tab);

        var menuNode = grid.header;


        var mainMenu = utils.addWidget(MainMenu,{
            //actionStores:[ctx.getActionStore()],
            attachToGlobal:false
        },null,menuNode,true);


        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        tab.add(mainMenu,null,false);

        mainMenu._clear = function(){

            var actions = this.getActions();

            var store = this.store;
            if(store){
                this.actionStores.remove(store);
            }
            var self = this;

            actions = actions.concat(this._tmpActions);
            //console.error('clear actions',this._tmpActions);
            _.each(actions,function(action){

                if(action) {

                    var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        var widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                    }
                }

            });

            this.$navBar.empty();


        }

        setTimeout(function(){


            //console.clear();

            console.error('destroy');

            mainMenu._clear();

            mainMenu.currentActionEmitter = null;

            mainMenu.setActionEmitter(grid);


        },2000);


        return;
    }
    function doBlockTests(tab){


        grid = _TestBlockUtils.createBlockGrid(ctx,tab);
        var menuNode = grid.header;
        var mainMenu = utils.addWidget(MainMenu,{
            attachToGlobal:false
        },null,menuNode,true);

        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        tab.add(mainMenu,null,false);

        mainMenu._clear = function(){

            var actions = this.getActions();

            var store = this.store;
            if(store){
                this.actionStores.remove(store);
            }
            var self = this;

            actions = actions.concat(this._tmpActions);
            //console.error('clear actions',this._tmpActions);
            _.each(actions,function(action){

                if(action) {

                    var actionVisibility = action.getVisibility != null ? action.getVisibility(self.visibility) : {};
                    if (actionVisibility) {
                        var widget = actionVisibility.widget;
                        action.removeReference && action.removeReference(widget);
                        if (widget && widget.destroy) {
                            widget.destroy();
                        }
                    }
                }

            });

            this.$navBar.empty();


        }

        setTimeout(function(){

            return;
            console.error('destroy');

            mainMenu._clear();

            mainMenu.currentActionEmitter = null;

            mainMenu.setActionEmitter(grid);


        },2000);


        return;
    }
    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);
        doTests(parent);
        //doBlockTests(parent);
        return declare('a',null,{});

    }
    return _Widget;

});

