/** @module xide/wizard/WizardBase **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/Grid',
    "./TestUtils",
    'xide/_base/_Widget',
    'xide/layout/_TabContainer',
    "module",
    "xfile/tests/TestUtils",
    "xide/form/Select",
    "xide/widgets/TemplatedWidgetBase",
    "xide/views/_Panel"
], function (dcl,declare,types,utils,factory,
             Grid, TestUtils,_Widget,_TabContainer,module,
             FTestUtils,Select,TemplatedWidgetBase,_Panel) {



    console.clear();
    console.log('--do-tests');
    var DefaultEvents = [
        'onInit',
        'onShow',
        'onNext',
        'onPrevious',
        'onFirst',
        'onLast',
        'onBack',
        'onFinish',
        'onTabChange',
        'onTabClick',
        'onTabShow'
    ];
    var WizardProto = dcl([_Widget],{
        containerClass:_TabContainer,
        containerArgs:{
            direction:'above',
            resizeContainer:false,
            navBarClass:'navbar-inverse'
        },
        options:null,
        paneClass:null,
        panes:[],
        buttons:[],
        wizard:null,
        $wizard:null,
        templateString:'<div attachTo="domNode" />',
        getDefaultOptions:function(){

            var events = this.events || DefaultEvents;

            //result
            var options = {};

            //handler, emits & calls instance method if aviable
            function handler(eventKey){
                var args = Array.prototype.slice.call(arguments, 1);
                this._emit(eventKey,args);
                _.isFunction(this[eventKey]) && this[eventKey](args);
            }

            var self = this;
            _.each(events,function(event){
                options[event]=function(args){
                    handler.apply(self,[event].concat(args));
                }
            });
            return options;
        },
        /**
         * Test
         * @param title {string} the title
         */
        getPane:function(title){
            //var pane = _.find(this.)
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Stub functions, being overriden by the actual implementation of  https://github.com/VinceG/twitter-bootstrap-wizard
        //
        next:function(){},
        previous:function(){},
        first:function(){},
        last:function(){},
        finish:function(){},
        back:function(){},
        currentIndex:function(){},
        firstIndex:function(){},
        lastIndex:function(){},
        getIndex:function(){},
        nextIndex:function(){},
        previousIndex:function(){},
        navigationLength:function(){},
        activeTab:function(){},
        nextTab:function(){},
        previousTab:function(){},
        show:function(){},
        hide:function(){},
        display:function(){},
        enable:function(){},
        disable:function(){},
        remove:function(){},
        resetWizard:function(){},
        startup:function(){
            var _prog = true;
            if(_prog) {
                var container = utils.addWidget(this.containerClass, this.containerArgs, this, this.domNode, true);
                var panes = [
                    {
                        title: "test pane 1",
                        icon: "fa-code",
                        selected: true
                    },
                    {
                        title: "test pane 2",
                        icon: "fa-play"
                    },
                    {
                        title: "test pane 3",
                        icon: "fa-terminal"
                    }
                ];

                var self = this;
                _.each(panes, function (pane) {
                    pane.widget = container._createTab(this.paneClass || pane.paneClass, pane);
                }, this);



                var buttonBarParent = container.$tabContentNode[0];

                var buttons = [
                    {
                        title: 'First',
                        cls: 'btn btn-default button-first pull-left',
                        name: 'first',
                        value: 'First'
                    },
                    {
                        title: 'Previous',
                        cls: 'btn btn-default button-previous pull-left',
                        name: 'previous',
                        value: 'Previous'
                    },
                    {
                        title: 'Last',
                        cls: 'btn btn-default button-last pull-right',
                        name: 'last',
                        value: 'Last'
                    },
                    {
                        title: 'Next',
                        cls: 'btn btn-default button-next pull-right',
                        name: 'next',
                        value: 'Next'
                    }/*,
                    {
                        title: 'Finish',
                        cls: 'btn btn-default button-finish pull-right',
                        name: 'finish',
                        value: 'Finish'
                    },
                    {
                        title: 'Back',
                        cls: 'btn btn-default button-back pull-right',
                        name: 'back',
                        value: 'Back'
                    }*/



                ]

                _.each(buttons, function (button) {
                    //pane.tab = container._createTab(this.paneClass||pane.paneClass,pane);
                    button.widget = factory.createSimpleButton(button.title, button.icon, button.cls, button.mixin, buttonBarParent);
                    button.name && $(button.widget).attr('name', button.name);
                    button.value && $(button.widget).attr('value', button.value);
                }, this);


                var options = utils.mixin(this.getDefaultOptions(),this.options);

                utils.mixin(options,{
                    tabClass: 'nav',
                    'nextSelector': '.button-next',
                    'previousSelector': '.button-previous',
                    'firstSelector': '.button-first',
                    'lastSelector': '.button-last'
                });




                var wizard = this.$domNode.bootstrapWizard(options);

                this.$wizard = wizard;
                this.wizard = wizard.data('bootstrapWizard');
                utils.mixin(this,this.wizard);
            }else {
                this.$domNode.bootstrapWizard({
                    'nextSelector': '.button-next',
                    'previousSelector': '.button-previous',
                    'firstSelector': '.button-first',
                    'lastSelector': '.button-last'
                });
            }
        }

    });
    /**
     * @class module:xide/wizard/WizardBase
     * @extends module:xide/mixins/EventedMixin
     * @augments module:xide/_base/_Widget
     */
    var WizardBase = dcl([_Widget],{

        containerClass:_TabContainer,
        containerArgs:{
            direction:'above',
            navBarClass:'',
            resizeToParent:true
        },
        options:null,
        paneClass:null,
        panes:[],
        buttons:[],
        wizard:null,
        $wizard:null,
        buttonBar:null,
        templateString:'<div attachTo="domNode" style="height: inherit;position: relative">' +
        '<div attachTo="containerNode" style="height: auto;min-height: 300px"></div>'+
        '<div style="min-height:initial;position: absolute" attachTo="buttonBar" class="navbar bg-opaque navbar-fixed-bottom"></div>'+
        '</div>',
        getDefaultOptions:function(){

            var events = this.events || DefaultEvents;

            //result
            var options = {};

            //handler, emits & calls instance method if aviable
            function handler(eventKey,args){
                this._emit(eventKey,args);
                _.isFunction(this[eventKey]) && this[eventKey].apply(this,args);
            }

            var self = this;
            _.each(events,function(event){
                options[event]=function(){
                    handler.apply(self,[event,arguments]);
                }
            });
            return options;
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Stub functions, being overriden by the actual implementation of  https://github.com/VinceG/twitter-bootstrap-wizard
        //
        next:function(){},
        previous:function(){},
        first:function(){},
        last:function(){},
        finish:function(){},
        back:function(){},
        currentIndex:function(){},
        firstIndex:function(){},
        lastIndex:function(){},
        getIndex:function(){},
        nextIndex:function(){},
        previousIndex:function(){},
        navigationLength:function(){},
        activeTab:function(){},
        nextTab:function(){},
        previousTab:function(){},
        show:function(){},
        hide:function(){},
        display:function(){},
        enable:function(){},
        disable:function(){},
        remove:function(){},
        resetWizard:function(){},
        onWidgetAdded:function(what){
            this.add(what,null,false);
            this.resize();
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Added API
        //
        disableButton:function(selector,disabled){
            var btn = $(this.domNode).find(selector);
            btn.toggleClass('disabled', disabled);
        },
        hideButton:function(selector,hidden){
            $(this.domNode).find(selector).toggleClass('hidden', hidden);
        },
        onTabShow:function(a,b,index){
            var pane = this.panes[index];
            if(pane){
                if(!pane.widget) {
                    var parent = pane.container;
                    if (pane.create) {

                        var what =pane.create.apply(this, [parent, pane]);
                        this.onWidgetAdded(what);
                    }
                }
                pane.onShow && pane.onShow.apply(this,[pane]);
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  STD API
        //
        startup:function(){

            var container = utils.addWidget(this.containerClass, this.containerArgs, this, this.$containerNode[0], true);

            this.add(container);

            var self = this;

            _.each(this.panes, function (pane) {
                pane.container = container._createTab(this.paneClass || pane.paneClass, pane);
            }, this);

            var buttonBarParent = this.buttonParent || this.$buttonBar[0];
            this.buttons && _.each(this.buttons, function (button) {
                if(!button.widget) {
                    button.widget = factory.createSimpleButton(button.title, button.icon, button.cls, button.mixin, buttonBarParent);
                    button.name && $(button.widget).attr('name', button.name);
                    button.value && $(button.widget).attr('value', button.value);
                }
            }, this);

            var options = utils.mixin(this.getDefaultOptions(),this.options);
            var wizard = this.$domNode.bootstrapWizard(options);
            this.$wizard = wizard;
            this.wizard = wizard.data('bootstrapWizard');

            this.options = options;
            utils.mixin(this,this.wizard);

            this.buttons && _.each(this.buttons, function (button) {
                $(button.widget).toggleClass('disabled', button.disabled);
            });

            this.resize();
        }

    });


    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        CIS;

    /*
     * playground
     */
    var ctx = window.sctx,
        root;


    if (ctx) {

        //var parent = TestUtils.createTab(null,null,module.id);


        function variableWizard(ready){

            var widgetProp = null;

            var value = "widgetProp";

            var panel = new _Panel({
                title: 'What you want with the variable ',
                containerClass:'CIDialog',
                options:{
                    "contentSize": {
                        width: '400px',
                        height: '300px'
                    },
                    footerToolbar:[
                        {
                            item:     "<button style='margin-left:5px;' type='button'><span class='...'></span></button>",
                            event:    "click",
                            btnclass: "btn btn-danger btn-sm",
                            btntext:  " Cancel",
                            callback: function( event ){
                                event.data.close();

                            }
                        },
                        {
                            item:     "<button style='margin-left:5px;' type='button'><span class='...'></span></button>",
                            event:    "click",
                            btnclass: "btn btn-primary btn-sm",
                            btntext:  " Ok",
                            callback: function( event ){
                                ready(widgetProp ? widgetProp.get('value') : null,value);
                                event.data.close();
                            }
                        }
                    ]
                },
                onShow:function(panel,contentNode){


                    var widget = dcl(TemplatedWidgetBase,{
                        templateString:'<div class="widget" style="padding: 16px">' + factory.radioButtonString(true,"thename","Update Widget Property","wRadioWidgetProp") + ' '  + factory.radioButtonString(false,"thename","Set State with Variable value","wRadioWidgetState") + '</div>'
                    });

                    var radios = utils.addWidget(widget,{},null,contentNode,true);

                    function textBox(){
                        utils.destroy(widgetProp);
                        widgetProp = factory.createValidationTextBox(contentNode,"","Widget Property","label");
                    }


                    function changed(radio){
                        value = radio;
                        if(radio==='widgetProp'){
                            //widgetProp = textBox();
                            widgetProp.set('value','label');
                        }else{
                            //utils.destroy(widgetProp);
                            //widgetProp = null;
                            widgetProp.set('value','state');
                        }
                    }

                    textBox();

                    radios.$wRadioWidgetProp.on('change',function(val){
                        changed('widgetProp');
                    })
                    radios.$wRadioWidgetState.on('change',function(val){
                        changed('widgetState');
                    })
                    var self = this;
                    return [radios];
                }
            });

            panel.show();
            return panel;

        }



        variableWizard(function(widgetProp,mode){
            
        });


        return;

        var args = {
            options:{
                tabClass: 'nav',
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                'firstSelector': '.button-first',
                'lastSelector': '.button-last',
                 'finishSelector': '.button-finish',
                 'backSelector': '.button-back'
            },
            panes:[
                {
                    title: "test pane 1",
                    icon: "fa-code",
                    selected: true,
                    create:function(parent,pane){

                        var value = "widgetProp";

                        function radio(checked,name,label,attachTo) {
                            var id = utils.createUUID();
                            var element = "";
                            element += '<div class="radio radio-info">';
                            element += '<input attachTo="' + attachTo + '" name="' + name + '" id="' + id + '" type="radio" ' + (checked == true ? 'checked' : '') + '/>';
                            element += '<label for="' + id + '">';
                            element += label;
                            element += '</label>';
                            element += '</div>';
                            return element;
                        }


                        //+ radio('true',"thename","Set State with Variable value")
                        var widget = dcl(TemplatedWidgetBase,{
                           templateString:'<div>' + radio(true,"thename","Update Widget Property","wRadioWidgetProp") + ' '  + radio(false,"thename","Set State with Variable value","wRadioWidgetState") + '</div>'
                        });

                        var radios = utils.addWidget(widget,{

                        },null,parent,true);

                        function changed(radio){
                            value = radio;
                        }

                        radios.$wRadioWidgetProp.on('change',function(val){
                            changed('widgetProp');
                        })

                        radios.$wRadioWidgetState.on('change',function(val){
                            changed('widgetState');
                        })



                        //pane.widget = FTestUtils.createFileGrid(null,{},null,null,null,false,parent);
/*
                        pane.widget = utils.addWidget(Select,{
                            options:[
                                {
                                    label:'Update widget field',
                                    value:1
                                }
                            ],
                            value:1,
                            title:''
                        },null,parent,true);
*/

                    }
                },
                {
                    title: "test pane 2",
                    icon: "fa-play"
                },
                {
                    title: "test pane 3",
                    icon: "fa-terminal"
                }
            ],
            buttons:[
                {
                    title: 'First',
                    cls: 'btn btn-default button-first pull-left',
                    name: 'first',
                    value: 'First'
                },
                {
                    title: 'Previous',
                    cls: 'btn btn-default button-previous pull-left',
                    name: 'previous',
                    value: 'Previous'
                },
                {
                    title: 'Last',
                    cls: 'btn btn-default button-last pull-right',
                    name: 'last',
                    value: 'Last'
                },
                {
                    title: 'Next',
                    cls: 'btn btn-default button-next pull-right',
                    name: 'next',
                    value: 'Next'
                }
            ]
        };


        var wizard = utils.addWidget(WizardBase,args,null,parent,true);
        //wizard.disable(1);
        //wizard.disableButton('.button-next',true);
        //wizard.hideButton('.button-last',true);

        return declare('a',null,{});

    }


    return WizardBase;

});