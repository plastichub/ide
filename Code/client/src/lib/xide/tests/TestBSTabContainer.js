/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',
    "xide/widgets/TemplatedWidgetBase",
    "./TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "module"

], function (declare,dcl,types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase,
             TestUtils,FTestUtils,module) {



    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;


    var propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };



    function createTabClass(){

        return declare(TemplatedWidgetBase,{
            iconClass:null,
            open:true,
            titleBar:null,
            titleNode:null,
            toggleNode:null,
            containerNode:null,
            height:'400px',
            padding:'0px',
            panelNode:null,
            resize:function(){

                console.log('resize tab');
                this.inherited(arguments);
            },
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);


                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            _onShown:function(e){
                this.resize();
            },
            addChild:function(what,index,startup){

                //collect widget
                this.add(what,null);

                utils.addChild(this.containerNode,what.domNode);

                if(startup!==false && !what._started){
                    what.startup();
                }
            },
            buildRendering:function(){

                this.inherited(arguments);

                var self = this;

                var panel = this.panelNode;

                this.__addHandler(panel,'hidden.bs.collapse','_onHided');
                this.__addHandler(panel,'hide.bs.collapse','_onHide');
                this.__addHandler(panel,'shown.bs.collapse','_onShown');
                this.__addHandler(panel,'show.bs.collapse','_onShow');

            },
            postMixInProperties:function(){

                var closed = !this.open;
                this.ariaOpen = closed ? 'true' : 'false';
                this.containerClass = closed ? 'collapse' : 'collapse in';
                this.titleClass = closed ? 'collapsed' : '';

                var widgetStr = '<div style="float: :right" class="widget-controls">'+
                        '<a title="Options" href="#"><i class="fa-cogs"></i></a>'+
                        /*'<a data-widgster="expand" title="Expand" href="#"><i class="fa-chevron-up"></i></a>'+
                        '<a data-widgster="collapse" title="Collapse" href="#"><i class="fa-chevron-down"></i></a>'+*/
                        '<a data-widgster="close" title="Close" href="#"><i class="fa-remove"></i></a>'+
                    '</div>';

                widgetStr ='';

                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';
                var titleStr = '<span data-dojo-attach-point="titleNode" class=""> ${!title}</span>';
                var toggleNodeStr =
                    '<a data-dojo-attach-point="toggleNode" href="#${!id}-Collapse" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                        iconStr +   titleStr + widgetStr
                    '</a>';

                this.templateString = '<div class="panel widget" data-dojo-attach-point="panelNode">'+

                        '<div class="panel-heading" data-dojo-attach-point="titleBar">'+
                            toggleNodeStr +
                        '</div>'+

                        '<div data-dojo-attach-point="containerRoot" class="panel-collapse ${!containerClass}" id="${!id}-Collapse" aria-expanded="${!ariaOpen}">'+
                            '<div style="height: ${!height};padding:${!padding}" class="panel-body" data-dojo-attach-point="containerNode"></div>'+
                        '</div>'+

                    '</div>';

                this.inherited(arguments);
            }
        });
    };

    function _createTemplateBaseClass(){

        var templated = dcl(null,{

            templateUrl:'xide/tests/accordion_test.html',
            buildRenderering:function(){

                var _templateText = this._getTemplate();

                var root = $('div');
                var node = root.loadTemplate(require.toUrl(this.templateUrl));
                this.domNode = root;
            },
            _getTemplate:function(){
                return this.templateString || this._getText(this.templateUrl);
            },
            startup:function(){

                this.buildRenderering();

            },
            _getText:function(url){

                url = require.toUrl(url);
                var text = $.ajax({
                    url: url,
                    async:false
                });
                return text.responseText;
            }
        });

        return templated;

    }

    function doTests(tabContainer){

        var pane = tabContainer.createTab('bla bla','fa-code',true);
        var pane2 = tabContainer.createTab('Files','fa-cogs',true);


        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane2);




        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane);



        /*
        setTimeout(function(){
            //pane.show();
        },1000);


        setTimeout(function(){
            pane.hide();
        },5000);
*/
    }
    function _createTabContainer(){

        var tabClass = declare(TemplatedWidgetBase,{
            templateString:'<div class="panel-group" data-dojo-attach-point="containerNode"/>',
            resize:function(){
                console.log('resize tab container');
                this.inherited(arguments);
            },
            createTab:function(title,icon,open){
                return this.add(createTabClass(),{
                    title:title,
                    iconClass:icon,
                    open:open
                },this.domNode,true);
            }

        });

        return tabClass;

    }

    function _createAccContainer(){

        var tabClass = declare(TemplatedWidgetBase,{
            templateString:'<div class="panel-group" data-dojo-attach-point="containerNode"/>',
            createTab:function(title,icon){

                return utils.addWidget(createTabClass(),{
                    title:title,
                    iconClass:icon
                },null,this.domNode,true);





/*
                '<div class="widget-controls">'+
                '<a data-widgster="load" title="Reload" href="#"><i class="glyphicon glyphicon-refresh"></i></a>'+
                '<a data-widgster="expand" title="Expand" href="#"><i class="fa fa-code"></i></a>'+
                '<a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-minus"></i></a>'+
                '<a data-widgster="fullscreen" title="Full Screen" href="#"><i class="glyphicon glyphicon-resize-full"></i></a>'+
                '<a data-widgster="restore" title="Restore" href="#"><i class="glyphicon glyphicon-resize-small"></i></a>'+
                '<a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>'+
                '</div>'+
                    */

                var closed = true;

                var ariaOpen = closed ? 'true' : 'false';
                var containerClass = closed ? 'collapse' : 'collapse in';
                var titleClass = closed ? 'collapsed' : '';

                var toggleNodeStr =
                    '<a data-dojo-attach-point="toggleNode" href="#collapseOneTwo" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                        '${!title}'+
                    '</a>';
                    //'<a data-widgster="expand" title="Expand" href="#"><i class="fa fa-arrow-down"></i></a>';

                var panelTemplate = '<div class="panel widget">'+

                    '<div class="panel-heading" data-dojo-attach-point="titleBar">'+

                        toggleNodeStr +

                    '</div>'+

                    '<div data-dojo-attach-point="containerRoot" class="panel-collapse ${!containerClass}" id="collapseOneTwo" aria-expanded="${!ariaOpen}" style="">'+
                        '<div class="panel-body" data-dojo-attach-point="containerNode">'+
                            'asdfasdf'+
                        '</div>'+
                    '</div>'+

                '</div>'


                var pane  = utils.templatify(null,panelTemplate, this.domNode , {
                    iconClass:'fa-play',
                    title:title,
                    ariaOpen : ariaOpen,
                    containerClass:containerClass,
                    titleClass:titleClass,
                    show:function(){

                        var container = $(this.containerRoot),
                            toggleNode= $(this.toggleNode);

                        toggleNode.removeClass('collapsed');
                        toggleNode.attr('aria-expanded',true);


                        container.removeClass('collapse');
                        container.addClass('collapse in');
                        container.attr('aria-expanded',true);

                    },
                    hide:function(){

                        var container = $(this.containerRoot),
                            toggleNode= $(this.toggleNode);

                        toggleNode.addClass('collapsed');
                        toggleNode.attr('aria-expanded',false);


                        container.removeClass('collapse in');
                        container.addClass('collapse');
                        container.attr('aria-expanded',false);
                    }
                });


                return pane;

                /*
                var panel = $(panelTemplate);

                $(this.domNode).append(panel);*/


            }
        });

        return tabClass;

    }

    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {


        var parent = TestUtils.createTab('Acc-Test',null,module.id);
        var _accHTML = require.toUrl('xide/tests/accordion_test.html');

        var tabContainer = _createTabContainer();

        var widget = utils.addWidget(tabContainer,{},null,parent,true);


        doTests(widget,tabContainer);


        return declare('a',null,{});

    }

    return Grid;

});