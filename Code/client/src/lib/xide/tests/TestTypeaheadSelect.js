/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "module",
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "xide/_base/_Widget",
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'xide/widgets/WidgetBase',
    'xide/$',
    'xide/lodash'
], function (declare,dcl,types,
             utils, TestUtils,module,
             domConstruct, _XWidget,
             TreeMemory,ObservableStore,Trackable,WidgetBase,$,_) {


    console.clear();
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches;
            // an array that will be populated with substring matches
            matches = [];
            // regex used to determine if a string contains the substring `q`
            //var substrRegex = new RegExp(q, 'i');
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                matches.push(str);
            });
            cb(matches);
        };
    };
    var _TypeAheadImplementation = {
        _items: function () {
            return _.map(this.store ? this.store.query(this.query) : this.options, function (item) {
                var _label = this.labelField ? item[this.labelField] : item.label;
                var _value = this.valueField ? item[this.valueField] : item.value;
                return {
                    name: _label,
                    value: _value,
                    run: item.run
                }
            }, this);
        },
        init: function (value) {
            var input = domConstruct.create('input', {
                "class": "form-control input-transparent",
                type: "text",
                value: value || "No Value"
            });
            this.$selectNode.parent().append(input);
            this.$selectNode.remove();
            this.$editBox = $(input);
            var options = {};
            var self = this;
            var typeahead = $(input).typeahead(utils.mixin({
                hint: true,
                highlight: true,
                minLength: 0,
                name: 'states',
                source: substringMatcher(self._items()),
                autoSelect: true,
                showHintOnFocus: 'all',
                appendTo: $("body"),
                //showHintOnFocus:true,
                select: function () {
                    var val = this.$menu.find('.active').data('value');
                    this.$element.data('active', val);
                    if (this.autoSelect || val) {
                        var newVal = this.updater(val);
                        // Updater can be set to any random functions via "options" parameter in constructor above.
                        // Add null check for cases when updater returns void or undefined.
                        if (!newVal) {
                            newVal = "";
                        }
                        self.typeahead.value = self.valueField ? newVal[self.valueField] : newVal.value;
                        this.$element.val(this.displayText(newVal, true) || newVal).change();
                        this.afterSelect(newVal);
                    }
                    return this.hide();
                },
                highlighter: function (item) {
                    return item;
                },
                displayText: function (item, isValue) {
                    if (isValue) {
                        return self.labelField ? item[self.labelField] : item.name;
                    }
                    return '<span class="text-info">' + item.value + '</span>';
                }
            }, options));

            this.typeahead = $(typeahead).data('typeahead');
            this.$editBox.on('click', function () {
                self.typeahead.lookup("");
            })

        },
        render: function () {
            this.empty();
            var _picker = this.getPicker();
            _picker.val(this.value, null, true);
        },
        empty: function () {
            //this.$selectNode.empty();
        },
        _destroy: function () {
            if (this._picker) {
                this.typeahead.destroy();
            }
            if (this.store && this.store.destroy && this.store.ownStore) {
                this.store.destroy();
            }
            delete this._picker;
            delete this.store;
        },
        getDefaultOptions: function () {
            return utils.mixin({
                valueField: this.valueField || 'value',
                labelField: this.labelField || 'label',
                searchField: this.labelField || 'label',
                options: [],
                create: true,
                createOnBlur: true,
                plugins: ['restore_on_backspace'],
                persist: false,
                dropdownParent: 'body'
            }, this.selectOptions);
        },
        getPicker: function () {
            if (!this._picker) {
                this._picker = this.$editBox;
            }
            return this._picker;
        },
        _createStore: function (options) {
            var storeClass = this.storeClass || declare('driverStore', [TreeMemory, Trackable, ObservableStore], {});
            var store = new storeClass({
                idProperty: 'value',
                ownStore: true
            });
            store.setData(options);
            return store;
        },
        set: function (what, value, label, silent) {
            var _picker = this.getPicker();
            if (what === 'disabled') {
                this.$editBox[value === true ? 'attr' : 'removeAttr'](what, value);
            }
            if (what === 'value') {
                this.value = value;
                this.typeahead.value = value;
                _picker.val(value, label, silent);
                return null;
            }
            return this.inherited(arguments);
        },
        get: function (what) {
            if (what === 'value') {
                return this.typeahead.value;
            }
            return this.inherited(arguments);
        },
        _renderItems: function (queryResults) {
            queryResults.forEach(this._renderItem, this);
        },
        onStoreChanged: function () {
            this.typeahead.source = substringMatcher(this._items());
        },
        _destroyItems: function () {
            var preserveDom = true;
            this.destroyDescendants(preserveDom);
            this.empty();
        },
        startupPost: function () {
            var thiz = this;
            this.getPicker().on('change', function () {
                thiz._emit('change', thiz.get('value'));
                thiz.setValue(thiz.get('value'));
            });
        }
    }

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    function createStore(){
        var storeClass = declare('driverStore',[TreeMemory,Trackable,ObservableStore],{});
        var store = new storeClass({
            idProperty:'value'
        });
        store.setData([
            {
                label:'test',
                value:'test-value',
                group:"Test"
            },
            {
                label:'Barbecue',
                value:'Barbecue-value',
                group:"Test"
            },
            {
                label:'Divider',
                value:utils.createUUID(),
                divider:true
            },
            {
                label:'Barbecue-B',
                value:'Barbecue-B value',
                group:"Test B"
            }
        ]);


        return store;

    }
    function createSelectClass(){

        var _SelectImplementation = {
            render: function () {
                this.empty();
                this._render();
                var _picker = this.getPicker();
                _picker.val(this.value);
                _picker.render();
            },
            empty: function () {
                this.$selectNode.empty();
            },
            _destroy: function () {
                if (this._picker) {
                    this._picker.destroy();
                }
                if (this.store && this.store.destroy && this.store.ownStore) {
                    this.store.destroy();
                }
                delete this._picker;
                delete this.store;
            },
            getPicker: function () {
                if (!this._picker) {
                    this._picker = this.$selectNode.selectpicker(this.selectOptions).data().selectpicker;
                }
                return this._picker;
            },
            _createStore: function (options) {
                var storeClass = declare('driverStore', [TreeMemory, Trackable, ObservableStore], {});
                var store = new storeClass({
                    idProperty: 'value',
                    ownStore: true
                });
                store.setData(options);
                return store;
            },
            set: function (what, value, label) {
                if (what === 'value') {
                    this.value = value;
                    var _picker = this.getPicker();
                    value !== null && _picker.refresh();
                    _picker.val(value, label);
                    _picker.render();
                    return null;
                }
                return this.inherited(arguments);
            },
            get: function (what) {
                if (what === 'value') {
                    var picker = this.getPicker();
                    var val = picker.val();
                    return val;
                }
                return this.inherited(arguments);
            },
            _render: function () {
                this._destroyItems();

                if (this.store) {
                    var items = [];
                    if (_.isArray(this.query)) {
                        var self = this;
                        _.each(this.query, function (query) {
                            items = items.concat(self.store.query(query));
                        });
                    } else {
                        items = this.store.query(this.query || {});
                    }
                    this._renderItems(items);
                } else if (this.options) {
                    this._items = [];
                    _.each(this.options, function (item) {
                        var renderedItem = this._renderItem(item);
                        // look for `placeAt` method to support items rendered as widgets
                        if ('placeAt' in renderedItem) {
                            renderedItem.placeAt(this);
                        }
                    }, this)

                }
            },
            _renderItems: function (queryResults) {
                this._items = [];
                var groups = _.groupBy(queryResults, function (obj) {
                    return obj.group;
                });
                groups = utils.toArray(groups);
                if (groups.length == 1 && groups[0].name === 'undefined') {
                    groups = [];
                }
                if (groups.length) {
                    _.each(groups, function (group) {
                        var groupNode = this.selectNode.appendChild($('<optgroup label="' + group.name + '">')[0]);
                        _.each(group.value, function (item) {
                            this._renderItem(item, groupNode);
                        }, this);
                    }, this);
                } else {
                    queryResults.forEach(function (item) {
                        var renderedItem = this._renderItem(item);
                        // look for `placeAt` method to support items rendered as widgets
                        if ('placeAt' in renderedItem) {
                            renderedItem.placeAt(this);
                        }
                    }, this);
                }
            },
            _renderItem: function (item, parent) {
                if (item.divider) {
                    return (parent || this.selectNode).appendChild($('<option data-divider="true"></option>')[0]);
                } else {
                    var _label = this.labelField ? item[this.labelField] : item.label;
                    var _value = this.valueField ? item[this.valueField] : item.value;
                    if (!_.find(this._items, {value: _value})) {
                        this._items.push({
                            label: _label,
                            value: _value
                        });
                    }
                    var option = $('<option value="' + _value + '" data-icon="' + item.icon + '">' + _label + '</option>');
                    (parent || this.selectNode).appendChild(option[0]);
                    option.data("item", item);
                    return option[0];
                }
            },
            _destroyItems: function () {
                var preserveDom = true;
                this.destroyDescendants(preserveDom);
                this.empty();
                this._items = [];
            },
            startupPost: function () {
                var thiz = this;
                this.$selectNode.on('change', function (args) {
                    var _value = thiz.get('value');
                    thiz.value = _value;
                    thiz._emit('change', _value);
                    thiz.setValue(_value);
                });
            }
        }

        var Module = dcl([WidgetBase, _XWidget.StoreMixin], {
            declaredClass: 'xide.form.Select',
            $selectNode: null,
            search: false,
            style: 'btn-info',
            store: null,
            query: null,
            title: 'Choose:',
            disabled: false,
            select: null,
            value: null,
            selectOptions: {
                container: 'body'
            },
            noStore: false,
            templateString: "<div class='widgetContainer widgetBorder widgetTable widget' style=''>" +
            "<table border='0' cellpadding='5px' width='100%'>" +
            "<tbody align='left'>" +
            "<tr attachTo='extensionRoot' valign='middle' style='height:90%'>" +
            "<td attachTo='titleColumn' width='15%' class='widgetTitle'><span attachTo='titleNode'>${!title}</span></td>" +
            "<td valign='middle' class='widgetValue' attachTo='valueNode' width='100px'>" +
                '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode">' +
            "</td>" +
            "<td class='extension' attachTo='previewNode'></td>" +
            "<td class='extension' attachTo='button0'></td>" +
            "<td class='extension' attachTo='button1'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
            init:function(value){},
            _createStore: function (options) {
                var storeClass = declare('driverStore', [TreeMemory, Trackable, ObservableStore], {});
                var store = new storeClass({
                    idProperty: 'value',
                    ownStore: true
                });
                store.setData(options);
                return store;
            },
            destroy: function () {
                this._destroy && this._destroy();
            },
            onStoreChanged:function(){},
            startup: function () {
                if (!this.title && this.$titleNode) {
                    this.$titleNode.remove();
                    this.$titleNode = null;
                    this.$titleColumn.remove();
                    this.$titleColumn = null;
                }
                if (!this.disabled) {
                    this.$selectNode.removeAttr('disabled');
                }
                var userData = this.userData || {};
                var value = userData.value || this.value;
                this.$titleNode && this.$titleNode.html(userData.title);

                utils.mixin(this, this.editable ? _TypeAheadImplementation : _SelectImplementation);


                if (this.noStore !== true && !this.store && this.options) {
                    this.store = this._createStore(this.options);
                    var _selected = _.find(this.options, {
                        selected: true
                    });
                    _selected && (this.value = _selected.value);
                }
                this.init(value);
                this.select = this.getPicker();
                var self = this;
                if (this.store) {
                    this.render();
                    this.wireStore(this.store, function (event) {
                        if (event.type === 'update' && event.value === self._lastUpdateValue) {
                            return;
                        }
                        self._lastUpdateValue = event.value;
                        self.onStoreChanged(event);
                    });

                    this.store._on('update',function(event){
                        if (event.type === 'update' && event.value === self._lastUpdateValue) {
                            return;
                        }
                        self._lastUpdateValue = event.value;
                        self.onStoreChanged(event);
                    });
                } else {
                    this.render();
                }
                if (value != null) {
                    this.set('value', value, null, true);
                }
                this.startupPost();
                this.onReady();
            }
        });
        return Module;


    }

    function doTests(tab){
        var _class = createSelectClass();
        var select = utils.addWidget(_class,{
            store:createStore(),
            userData:{
                value:'xas'
            },
            editable:true
        },null,tab,true);

        select._on('change',function(e){
            console.error('changed ' + e);
        });
        setTimeout(function(){
            select.store.addSync({
                label:'new',
                value:'new-value',
                icon:'fa-code'
            });
        },2000);

        setTimeout(function(){
            select.store.removeSync('test-value');
        },2000);

    }

    var ctx = window.sctx,
        ACTION = types.ACTION;



    var test = false;
    if (ctx && test) {
        var parent = TestUtils.createTab(null,null,module.id);
        doTests(parent);
        return declare('a',null,{});

    }

    return createSelectClass();

});