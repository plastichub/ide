/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "./TestUtils",
    'xide/widgets/WidgetBase',
    "module"
], function (dcl,declare,types,utils,
             Grid, TestUtils,_Widget,module) {


    console.clear();

    console.log('--do-tests');



    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;




    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;



    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {




        var parent = TestUtils.createTab(null,null,module.id);
        var widget = dcl(_Widget,{
            title:"Checkbox Label",
            checked:false,
            checkboxClass:"checkbox-success",
            checkbox:null,
            //userData:{},
            templateString:'<div>' +
                '<div class="checkbox ${!checkboxClass}">'+
                '<input attachTo="checkbox" id="${!id}_checkbox" type="checkbox">'+
                    '<label for="${!id}_checkbox">${!title}</label>'+
                '</div>'+
            '</div>',
            get:function(what){
                if(what == 'value'){
                    return this.$checkbox[0].checked;
                }
            },
            set:function(what,value){

                if(what == 'value'){
                    this.$checkbox.attr("checked",value)
                };
            },
            startup:function(){
                if(this.userData && 'value' in this.userData){
                    this.checked = this.userData.value;
                }
                this.$checkbox.attr("checked",this.checked);
                var self = this;
                this.$checkbox.on('change',function(val){
                    self.checked = self.$checkbox[0].checked;
                    this.userData && self.setValue(self.checked);
                });
            }
        });

        utils.addWidget(widget,null,null,parent,true);

        return declare('a',null,{});

    }

    return Grid;

});