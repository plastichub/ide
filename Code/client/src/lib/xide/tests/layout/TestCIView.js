/** @module xgrid/Base **/
define([
    "xdojo/declare",

    "dcl/dcl",
    "dcl/debug",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',

    "xide/widgets/TemplatedWidgetBase",


    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",

    "xide/_base/_Widget",
    "xide/layout/_TabContainer",
    "module",

    'dojo/dom-class',
    'dojo/_base/lang',
    'dojo/_base/connect',
    "dojo/dom-construct",
    'dojo/Deferred',
    "dojo/promise/all",
    'dojo/when',
    'require',
    'xide/widgets/WidgetBase'

], function (declare,dcl,debug,types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase,
             TestUtils,FTestUtils,_Widget,_TabContainer,module,
             domClass,lang,connect,domConstruct,Deferred,all,when,require,WidgetBase) {



    console.clear();



    var NativeWidget = dcl(WidgetBase, {
        declaredClass:'xide.widgets.NativeWidget',
        value: "unset",
        _lastValue: null,
        _nativeHandles:false,
        widgetChanged: function (value) {
            this.changed = true;

            if (this.userData) {
                this.userData.changed = true;
            }

            if (this.nativeWidget) {
                this.setValue(value);
            }
        },
        setupNativeWidgetHandler: function () {

            var thiz = this;

            if (this.nativeWidget && !this._nativeHandles) {

                this.nativeWidget._added = true;

                if(!this.nativeWidget._started){
                    //console.error('native widget not started : ' + this.nativeWidget.declaredClass);
                    this.valueNode.innerHTML = "";
                    this.valueNode.appendChild(this.nativeWidget.domNode);
                    this.nativeWidget.startup();
                }

                this._nativeHandles = true;


                if(this.nativeWidget._on){


                    //console.log('sub ', thiz.nativeWidget.id + '  ' + this.id);
                    this.nativeWidget._on("change", function (value) {
                        if (thiz._lastValue !== null && thiz._lastValue === value) {
                            return;
                        }
                        thiz._lastValue = value;
                        thiz.widgetChanged(this);

                    });

                }else {

                    this.addHandle("change", this.nativeWidget.on("change", function (value) {
                        if (thiz._lastValue !== null && thiz._lastValue === value) {
                            return;
                        }

                        thiz._lastValue = value;
                        thiz.widgetChanged(this);

                    }));
                }

                this.addHandle("blur", this.nativeWidget.on("blur", function () {
                    thiz.setActive(false);
                }));
                this.addHandle("focus", this.nativeWidget.on("focus", function () {
                    thiz.setActive(true);
                }));
            }
        },
        startup: function () {


            console.error('start native widget ',this);

            var data = this.userData;
            var type = this.type;
            var label = data.title;
            var value = this.value;
            var $root = $(this.valueNode);
            var thiz = this;

            if(type){

                this.valueNode.innerHTML = "";

                this.titleNode.innerHTML = "";


                if(type ==='CheckBox') {


                    $root = $(this.titleNode);


                    var id = this.id + '_Checkbox';

                    var element = '';
                    element += '<div class="checkbox checkbox-success ">';
                    if (typeof data.icon !== 'undefined') {
                        //element += '<span style="float: left" class="' + data.icon + '"></span>';
                    }
                    element += '<input id="' + id + '" type="checkbox" ' + (value == true ? 'checked' : '') + '>';
                    element += '<label for="' + id + '">';
                    element += this.localize(label) + '</label>';
                    element += '</div>';

                    var $widget = $(element);

                    $root.append($widget);

                    var $nativeWidget = $widget.find('INPUT');
                    this.__on($nativeWidget,'change',null,function(){

                        var _value = $nativeWidget[0].checked;

                        if (thiz._lastValue !== null && thiz._lastValue === _value) {
                            return;
                        }

                        thiz._lastValue = _value;
                        thiz.widgetChanged(_value);
                        console.log('widget changed!');

                    });
                }
            }

            /*
            if (this.nativeWidget && this.nativeWidget._added!==true) {
                this.valueNode.innerHTML = "";
                this.valueNode.appendChild(this.nativeWidget.domNode);
                this.setupNativeWidgetHandler();
            }
            */
            this.onReady();
        }
    });


    function addFileGrid(pane){

        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:true,
                //attachChild:true,
                title:'tes',
                icon:'fa-folder'
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane);


        pane.add(grid,null,false);


        return grid;

    }

    function createTabPaneClass(){

        return declare(_Widget,{
            templateString:'<div></div>',
            isContainer:true,
            declaredClass:'xide/layout/_TabPane',
            panelNode:null,
            selected:false,
            $toggleNode:null,
            $toggleButton:null,
            lazy:true,
            unselect:function(){
                this.$toggleButton && this.$toggleButton.removeClass('active');
                this.$selectorNode && this.$selectorNode.removeClass('active');
                this.$containerNode && this.$containerNode.removeClass('active');
                this.selected = false;
            },
            select:function(){
                //this.$toggleButton && this.$toggleButton.tab('show');


                //this.$toggleButton && this.$toggleButton.click();


                this.$toggleButton && this.$toggleButton.addClass('active');
                this.$selectorNode && this.$selectorNode.addClass('active');
                this.$containerNode && this.$containerNode.addClass('active');

                this._onShown();



            },

            _onShown:function(e){

                /*
                 if(this.onFirstTimeShown){
                 this.onFirstTimeShown();
                 delete this.onFirstTimeShown;
                 }*/



                this.selected = true;

                console.log('shown!');
                _.each(this._widgets,function(w){
                    console.log('w '+ w.declaredClass);
                    if(!w._started && w.startup){
                        w.startup();
                    }
                });

                this.onShow();
                this.resize();

            },
            _onShow:function(e){


                if(this.onFirstTimeShown){
                    this.onFirstTimeShown();
                    delete this.onFirstTimeShown;
                }

                console.log('show!');

                this.selected = true;
                this.resize();





            },
            shouldResizeWidgets:function(){
                return this.selected;
            },
            _onHide:function(){
                this.selected = false;

                this.onHide();
            },
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);

                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            __postMixInProperties:dcl.superCall(function(sup) {

                console.error('tabPane pm! in ');

                return function () {


                    var closed = !this.open;

                    var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';

                    var active = this.selected ? 'active' : '';

                    this.templateString = '<div class="tab-pane fade ' + active + '"></div>';


                    return sup ? sup.apply(this,arguments) : null;

                };

            }),
            postMixInProperties:function(){

                var closed = !this.open;
                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';
                var active = this.selected ? 'active' : '';
                this.templateString = '<div attachTo="containerNode" style="height:100%;width:100%" class="tab-pane ' + active + '"></div>';
                //this.inherited(arguments);
            },
            __init:function(){

                var self = this;

                var panel = this.$toggleNode;
                this.__addHandler(panel,'hidden.bs.tab','_onHided');
                this.__addHandler(panel,'hide.bs.tab','_onHide');
                this.__addHandler(panel,'shown.bs.tab','_onShown');
                this.__addHandler(panel,'show.bs.tab','_onShow');
            }
        });
    };

    function createTabContainerClass(baseClass,tabClass){

        return declare(baseClass || _Widget,{
            tabClass:tabClass || createTabPaneClass(_Widget),
            tabs:null,
            tabBar:null,
            tabContentNode:null,
            padding:'',
            containerCSSClass:'',
            direction:'top',
            templateString:'<div class="${!containerCSSClass} tabbable tabs-${!direction}" style="height: inherit;" attachTo="containerNode">' +

            '<ul attachTo="tabBar" class="nav nav-tabs" role="tablist" />' +
            '<div attachTo="tabContentNode" style="width: inherit; padding:${!padding}; height: 100%;" class="tab-content"/>' +

            '</div>',
            getTab:function(name){
                return _.find(this._widgets,{
                    title:name
                });
            },
            _unselectAll:function(){

                _.each(this._widgets,function(tab){
                    tab.unselect();
                });

            },
            selectChild:function(mixed){

                if(mixed) {

                    if (_.isString(mixed)) {

                        var tab = this.getTab(mixed);

                        if (tab && tab.select) {

                            this._unselectAll();

                            tab.select();
                        }
                    }else{
                        console.error('selectChild : not a string');
                    }
                }else{
                    console.error('selectChild : mixed = null');
                }

                //console.log('get tab ',tab);


                //console.log('select child ',arguments);
            },
            addWidget:function(widgetProto, ctrArgsIn, delegate, parent, startup, cssClass,baseClasses,select,classExtension){

                var target = parent;


                if(widgetProto.isContainer){

                }else{

                    console.log('addin tab',ctrArgsIn);

                    target = this._createTab(this.tabClass,{
                        title:ctrArgsIn.title,
                        icon:ctrArgsIn.icon,
                        selected:ctrArgsIn.selected,
                        ignoreAddChild:true
                    });
                }

                var w = target.add(widgetProto,ctrArgsIn,null,startup);

                return w;
            },
            resize:function(){

                if(this.tabBar){
                    switch (this.direction){

                        case 'left':
                        case 'right':{
                            this.$tabContentNode.css('width', '');
                            break;
                        }
                        case 'top':
                        case 'below':{

                            if(this.$containerNode) {

                                var _total = this.$containerNode.height();
                                var _toolbar = this.$tabBar.height();
                                this.$tabContentNode.css('height', _total - _toolbar);
                            }
                            break;
                        }
                    }
                }
                this.inherited(arguments);

            },
            _createTab:function(tabClass,options){


                !this.tabs && (this.tabs = []);

                var active = this.tabs.length == 0 ? 'active' : '',
                    icon = options.icon || '',
                    title = options.title || '',
                    selected = options.selected!=null ? options.selected : this.tabs.length ==0;


                if(this.tabs.length ==0){
                    //    selected=true;
                }

                var pane = utils.addWidget(tabClass || this.tabClass,{
                    title:title,
                    icon:icon,
                    selected:selected
                },null,this.tabContentNode,true);


                var tabId = pane.id,
                    iconStr = icon ? ' ' +icon : '',
                    toggleNodeStr =
                        '<li class="' +active + '">' +
                        '<a href="#'+tabId +'" data-toggle="tab" class="' +iconStr  +'"> ' + title +'</a></li>',
                    tabButton = $(toggleNodeStr);

                $(this.tabBar).append(tabButton);


                pane.$toggleNode  = tabButton.find('a[data-toggle="tab"]');
                pane.$selectorNode  = tabButton.find('li');
                pane.$toggleButton  = tabButton;

                pane.__init();

                this.tabs.push({
                    id:tabId,
                    pane: pane,
                    button:tabButton[0]
                });


                this.add(pane,null,false);

                return pane;
            },
            createTab:function(title,icon,selected,tabClass,mixin){
                return this._createTab(tabClass,utils.mixin({
                    icon:icon,
                    selected:selected,
                    title:title
                },mixin));
            }
        });

    }

    factory.checkForCustomTypes = function (cis) {

        var addedCIS = [];
        var removedCIs = [];
        for (var i = 0; i < cis.length; i++) {
            var ci = cis[i];

            var ciType = utils.toInt(ci.type);

            if (ciType > types.ECIType.END) {//type is higher than core types, try to resolve it
                var resolved = types.resolveType(ciType);
                if (resolved) {
                    lang.mixin(addedCIS, resolved);
                    removedCIs.push(ci);
                }
            }
        }
        if (addedCIS.length > 0) {
            cis = cis.concat(addedCIS);
        }
        if (removedCIs) {
            for (var i in removedCIs) {
                cis.remove(removedCIs[i]);
            }
        }
        return cis;
    };
    factory.createWidgetData = function (data, value) {

        var res ={
            type: "",
            nativeType: null,
            props: {
                options: data.options || []
            },
            title: "NoTitle",
            node: null,
            autocomplete: true,
            vertical: data.vertical != null ? data.vertical : false
        };
        res.type = utils.getWidgetType(data.type) || data.type;

        if (res.type) {
            var _type = types.resolveWidgetMapping(data.type);
            if (_type) {
                res.type = _type;
            }
        }

        var name = utils.toString(data.name);
        var _title = utils.toString(data.title);

        var namespace = 'xide.widgets.';

        //special sanity check regarding DS-CIs
        if (utils.isValidString(_title) && _title.length > 0 && _title[0].length > 0) {
            name = _title;
        }


        // is enum
        if (data.type == types.ECIType.ENUMERATION) {
            if (!data.options) {
                var options = types.resolveEnumeration(data.enumType);
                if (options && options.length > 0) {
                    res.props.options = options;
                }
                //utils.populateSelectWidgetOptionsForEnumType(data.enumType, res.props.options, value);
            }

            res.props.value = data.value;
            res.props.prevValue = data.value;
            res.type = "SelectWidget";
            res.nativeType = "xide.form.Select";
            res.autocomplete = true;
        }

        if(data.widget){
            if(data.widget['class']){
                res.nativeType = data.widget['class'];
            }
        }

        if (data.type == types.ECIType.STRING) {

            res.props.value = data.value;
            res.props.prevValue = data.value;
            //res.type = "TextWidget";
            res.type = namespace + "EditBox";
            res.props.title = name;

            /*
             res.autocomplete = true;
             */
        }



        if (data.type == types.ECIType.JSON_DATA) {
            res.props.valueStr = data.value;
            res.type = "JSONEditorWidget";

            res.props.newEntryTemplate =
            {
                isRoot: true,
                id: "root",
                schema: "{}"
            };


            res.props.initialTemplate = [];
            res.props.aceEditorOptions = {
                region: "center",
                value: '',
                style: "margin: 0; padding: 0; overflow: auto;position:relative;height:100%;width:100%;",
                theme: "twilight",
                mode: "json",
                readOnly: false,
                tabSize: 8,
                softTabs: true,
                wordWrap: false,
                printMargin: 120,
                showPrintMargin: true,
                highlightActiveLine: true,
                fontSize: '12px',
                showGutter: true
            }
        }

        if (data.type == types.ECIType.BOOL) {
            res.props['checked'] = utils.toBoolean(data.value);
            res.props.value = data.value;
            res.props.prevValue = data.value;
            res.type = NativeWidget;//"xide.widgets.NativeWidget";
            res.props.type = "CheckBox";
        }

        if (data.type == types.ECIType.IMAGE) {
            res.props.value = data.value;
            res.type = namespace + "ImageWidget";
            if (name == "BACKGROUND") {
                name = "Background";
            }
        }

        if (data.type == types.ECIType.FILE) {
            res.props.value = data.value;
            res.type =namespace +  "FileWidget";
        }

        if (data.type == types.ECIType.SCRIPT) {
            res.props.value = data.value;
            res.type = namespace + "ScriptWidget";
        }

        if (data.type == types.ECIType.RICHTEXT) {
            res.props.value = data.value;
            res.type = namespace + "RichTextWidget";
        }

        if (data.type == types.ECIType.ICON) {
            res.props.value = data.value;
            res.type = namespace + "IconWidget";
        }

        if (data.type == types.ECIType.REFERENCE) {
            res.props.disabled = !data.enabled;
            res.props.innerHTML = "Select";
        }

        if (res.type === null || res.type == '') {
            var _type = types.resolveWidgetMapping(data.type);
            if (_type) {
                res.type = _type;
            }
        }
        res.title = name;
        return res;
    };

    factory.createDijit = function (json, onChangeHander, storeItem, ci) {



        if (!json.type) {
            console.warn("type is missing for : " + json.title);
            return null;
        }

        var _ctrArgs = {
            delegate: onChangeHander,
            intermediateChanges: ci.intermediateChanges != null ? ci.intermediateChanges : true,
            vertical: ci.vertical != null ? ci.vertical : false,
            userData: ci
        };

        if (ci.widget != null) {
            lang.mixin(_ctrArgs, ci.widget);
        }
        /**
        if (json.type == "ImageWidget") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.title,
                storeItem: storeItem,
                href: "templates/ImageWidget.html"
            });

            return new xide.widgets.ImageWidget(_ctrArgs);
        }


        if (json.type == "FileWidget") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.props.title || 'File',
                value: json.props.value,
                oriValue: json.props.oriValue,
                storeItem: storeItem,
                href: "templates/DataWidget.html"
            });
            return new xide.widgets.FileWidget(_ctrArgs);
        }

        if (json.type == "FileEditor") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.props.title || 'File',
                value: json.props.value,
                oriValue: json.props.oriValue,
                storeItem: storeItem
            });
            return new xide.widgets.FileEditor(_ctrArgs);
        }

        if (json.type == "Expression") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.props.title || 'Expression',
                value: json.props.value,
                oriValue: json.props.oriValue,
                storeItem: storeItem
            });
            return new xide.widgets.Expression(_ctrArgs);
        }

        if (json.type == "WidgetReference") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.props.title || 'Widget Reference',
                value: json.props.value,
                oriValue: json.props.oriValue,
                storeItem: storeItem
            });
            return new xide.widgets.WidgetReference(_ctrArgs);
        }

        if (json.type == "DomStyleProperties") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.props.title || 'Style Properties',
                value: json.props.value,
                oriValue: json.props.oriValue,
                storeItem: storeItem
            });
            return new xide.widgets.DomStyleProperties(_ctrArgs);
        }

        if (json.type == "ExpressionEditor") {

            var _ExpressionEditor = require('xide/widgets/ExpressionEditor');
            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.props.title || 'ExpressionEditor',
                value: json.props.value,
                oriValue: json.props.oriValue,
                storeItem: storeItem
            });
            return new _ExpressionEditor(_ctrArgs);
        }
        if (json.type == "Arguments") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                preload: true,
                title: json.props.title || 'Arguments',
                value: json.props.value,
                oriValue: json.props.oriValue,
                storeItem: storeItem
            });
            return new xide.widgets.ArgumentsWidget(_ctrArgs);
        }

*/

        var type = json.type;
        if (json.nativeType) {
            json.props.intermediateChanges = true;
            type = json.nativeType;
        }

        var cls= json.result;

        /*
        var cls = _.isObject(type) ? type : dojo.getObject(type, false, dijit.form);
        if (!cls) {
            cls = dojo.getObject(type, false, dijit);
            if (!cls) {
                cls = dojo.getObject(type, false);
            }


        }
        if (!cls) {
            cls = dojo.getObject(type, false, xide.widgets);
        }


        if(!cls){
            //cls = utils.getObject(type);
            cls = json.result;
        }
        */


        //console.error('create class : ' + cls);

        if (!cls) {
            throw new Error("cannot find your widget type :" +type);
        }

        var myDijit = null;

        //ci provides additional widget properties, mix them into the constructor arguments
        if (ci.widget != null) {
            lang.mixin(json.props, ci.widget);
        }

        try {
            myDijit = new cls(json.props, json.node);
        } catch (e) {
            console.error('constructing dijit widget failed : ' + e.message + ' for ' + type ,json.props);
            return null;
        }
        var resultWidget = myDijit;


        /*
        if(json.type == "SelectWidget" && json.props && json.props['class']==null) {
            if (json.type == "SelectWidget") {
                _ctrArgs = lang.mixin(_ctrArgs, {
                    title: json.title,
                    nativeWidget: myDijit
                });
                resultWidget = new xide.widgets.SelectWidget(
                    _ctrArgs
                );
            }
        }
        if (json.type == "TextWidget") {

            _ctrArgs = lang.mixin(_ctrArgs, {
                title: json.title,
                nativeWidget: myDijit
            });
            resultWidget = new xide.widgets.SelectWidget(_ctrArgs);
        }
        */
        var changeWidget = myDijit;
        if (resultWidget.nativeWidget) {
            changeWidget = null;
        }

        return resultWidget;
    };

    var _defaultWidgetModule = WidgetBase;

    factory.createWidgetsFromArray = function (data, changeHandler, storeItem, showInvisible) {

        if (data == null)
            return null;

        var res = [],
            dfd = new Deferred();//head



        data = factory.checkForCustomTypes(data);

        var _widgetMeta = [];

        var options = {
            defaultModule:_defaultWidgetModule
        }

        for (var i = 0; i < data.length; i++) {
            var inDef = data[i];
            if (!inDef)
                continue;

            if (showInvisible == false && utils.toBoolean(inDef.visible) == false) {
                continue;
            }

            var wData = factory.createWidgetData(inDef, inDef.value);
            if (!wData) {
                continue;
            }

            if(!wData.type){
                continue;
            }

            wData.module = factory.resolveWidget(wData.type,options,wData);

            _widgetMeta.push(wData);
        }

        var _promises = _.pluck(_widgetMeta,'module');


        console.log('start all ',_promises);


        function complete(data){

            for (var i = 0; i < data.length; i++) {

                var wData = data[i];

                var widget = factory.createDijit(wData, changeHandler, storeItem, inDef);
                if (!widget) {
                    console.error('have no widget',wData);
                    continue;
                }

                widget["userData"] = inDef;
                widget["owner"] = changeHandler;
                widget["storeItem"] = storeItem;
                widget["order"] = i;
                inDef['_widget'] = widget;

                //tell everybody
                factory.publish(types.EVENTS.ON_CREATED_WIDGET, {
                    ci: inDef,
                    widget: widget,
                    storeItem: storeItem,
                    owner: changeHandler
                });


                if (widget.nativeWidget) {
                    widget.nativeWidget["userData"] = inDef;
                    widget.nativeWidget["storeItem"] = storeItem;
                }

                res.push(widget);
            }

            res.sort(function (a, b) {
                var orderA = utils.toInt(a.userData.order);
                var orderB = utils.toInt(b.userData.order);
                return orderA - orderB;
            });

            dfd.resolve(res);


        }


        all(_promises).then(function(args){
            console.error('all : args : ',args);
            console.dir(_widgetMeta);
            complete(_widgetMeta);

        },function(e){
            console.error('all : error',e);
        });



        return dfd;


        for (var i = 0; i < data.length; i++) {
            var inDef = data[i];
            if (!inDef)
                continue;

            if (showInvisible == false && utils.toBoolean(inDef.visible) == false) {
                continue;
            }
            var wData = factory.createWidgetData(inDef, inDef.value);
            if (!wData) {
                continue;
            }
            var widget = factory.createDijit(wData, changeHandler, storeItem, inDef);
            if (!widget) {
                continue;
            }

            widget["userData"] = inDef;
            widget["owner"] = changeHandler;
            widget["storeItem"] = storeItem;
            widget["order"] = i;
            inDef['_widget'] = widget;

            //tell everybody
            factory.publish(types.EVENTS.ON_CREATED_WIDGET, {
                ci: inDef,
                widget: widget,
                storeItem: storeItem,
                owner: changeHandler
            });


            if (widget.nativeWidget) {
                widget.nativeWidget["userData"] = inDef;
                widget.nativeWidget["storeItem"] = storeItem;
            }
            res.push(widget);

        }
        res.sort(function (a, b) {
            var orderA = utils.toInt(a.userData.order);
            var orderB = utils.toInt(b.userData.order);
            return orderA - orderB;
        });
        return res;
    };










    /*
    var what = 'xide.widgets.ActionToolbar';
    var _cls = factory.resolveWidget(what);


    when(_cls, function(value){
        // do something when resolved
        console.error('when resolved',value);
    }, function(err){
        // do something when rejected
        console.error('when error:',err);
    }, function(update){
        console.error('update');
    });
    */






    function doTabContainerTests_CITabView(tabContainerClass,parent){

        var CIS = TestUtils.createCIS();
        CIS.inputs = [];

        CIS.inputs.push({
            "chainType": 0,
            "class": "cmx.types.ConfigurableInformation",
            "dataRef": "",
            "dataSource": "",
            "description": null,
            "enabled": true,
            "enumType": "-1",
            "flags": -1,
            "group": 'AA',
            "id": "CF_DRIVER_CLASS3",
            "name": "CF_DRIVER_CLASS3",
            "order": 1,
            "params": null,
            "parentId": "myeventsapp108",
            "platform": null,
            "storeDestination": "metaDataStore",
            "title": "Driver Class",

            "_type": types.ECIType.EXPRESSION_EDITOR,
            "type": types.ECIType.BOOL,
            "uid": "-1",
            "value": true,
            "visible": true,
            "select":true,
            "widget":{
                showBrowser:true,
                icon:'fa-code'
            }
        });

        CIS.inputs.push({
            "chainType": 0,
            "class": "cmx.types.ConfigurableInformation",
            "dataRef": "",
            "dataSource": "",
            "description": null,
            "enabled": true,
            "enumType": "-1",
            "flags": -1,
            "group": 'Basics',
            "id": "CF_DRIVER_CLASS3",
            "name": "CF_DRIVER_CLASS3",
            "order": 1,
            "params": null,
            "parentId": "myeventsapp108",
            "platform": null,
            "storeDestination": "metaDataStore",
            "title": "Driver Class",

            "_type": types.ECIType.EXPRESSION_EDITOR,
            "type": 13,
            "uid": "-1",
            "value": "background:none",
            "visible": true,
            "select":true,
            "widget":{
                showBrowser:true,
                icon:'fa-code'
            }
        });
/*
        CIS.inputs.push({

            "chainType": 0,
            "class": "cmx.types.ConfigurableInformation",
            "dataRef": "",
            "dataSource": "",
            "description": null,
            "enabled": true,
            "enumType": "-1",
            "flags": -1,
            "group": 'AA',
            "id": "CF_DRIVER_CLASS3",
            "name": "CF_DRIVER_CLASS3",
            "order": 1,
            "params": null,
            "parentId": "myeventsapp108",
            "platform": null,
            "storeDestination": "metaDataStore",
            "title": "Driver Class",

            "_type": types.ECIType.EXPRESSION_EDITOR,
            "type": 'xide.widgets.Editbox6',
            "uid": "-1",
            "value": "background:none",
            "visible": true,
            "select":true,
            "widget":{
                showBrowser:true,
                icon:'fa-code'
            }
        });


        
*/








        var cisRenderer = dcl(CIGroupedSettingsView,{
            //tabContainerClass:tabContainerClass,
            options:{
                select:'AA'
            }

        });


        var cisView = utils.addWidget(cisRenderer,{
            cis:CIS.inputs,
            //tabContainerClass:tabContainerClass,
            __attachWidgets: function (data, dstNode,view) {
                console.error('attach widgets',data);
                var thiz = this;

                this.helpNodes = [];
                this.widgets = [];
                dstNode = dstNode || this.domNode;

                if(!dstNode && this.tabContainer){
                    dstNode = this.tabContainer.containerNode;
                }
                if (!dstNode) {
                    console.error('have no parent dstNode!');
                    return;
                }

                
                for (var i = 0; i < data.length; i++) {
                    var widget = data[i];

                    widget.delegate = this.owner || this;

                    dstNode.appendChild(widget.domNode);

                    if(view && view.lazy===true) {

                        widget._startOnShow = true;
                    }else{
                        widget.startup();
                    }

                    widget._on('valueChanged',function(evt){
                        thiz.onValueChanged(evt);
                    });

                    this.widgets.push(widget);

                    widget.userData.view=view;

                    if(view && view.add && view.add(widget,null,false)){

                    }else{
                        console.error('view has no add',view);
                    }



                }
            }
        },null,parent,true);


    }









    var _Tab = createTabPaneClass(_Widget);
    var _TabContainerClassInner = createTabContainerClass(_Widget,_Tab);

    _TabContainerClassInner.tabClass = _Tab;

/*
    dojo.setObject('xide.layout._TabContainer',_TabContainerClassInner);

    dojo.setObject('xide/layout/_TabContainer',_TabContainerClassInner);
*/




    var ctx = window.sctx,
        doTests = true;

    if (ctx && doTests) {

        var parent = TestUtils.createTab('Tab-Container-Tests',null,module.id);

        /*
        var tabContainer = utils.addWidget(_TabContainer,
            {
                padding:'10px',
                direction:'left'

            }
            ,null,parent,true);




        tabContainer.resize();
*/

        doTabContainerTests_CITabView(_TabContainerClassInner||_TabContainer,parent);

        return declare('a',null,{});

    }



    return Grid;

});