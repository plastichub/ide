/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    "dcl/debug",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',
    "xide/widgets/TemplatedWidgetBase",

    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/_base/_Widget",
    "module"

], function (declare,dcl,debug,types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase,
             TestUtils,FTestUtils,_Widget,module) {

    /**
     *
     * Remarks: this file served only as experiment
     */



    var MARKUP_CONTAINER = {

        "top":'<div class="${!containerCSSClass} tabbable tabs-above" style="height: inherit;width: inherit" attachTo="containerNode">' +

                '<ul attachTo="tabBar" class="nav nav-tabs" role="tablist" />' +

                '<div attachTo="tabContentNode" style="width: inherit; padding:${!padding}; height: 100%;" class="tab-content"/></div>',


        "left":'<div class="tabbable tabs-left" style="height: inherit;width: inherit" attachTo="containerNode">' +

                    '<ul attachTo="tabBar" class="nav nav-tabs" role="tablist"/>' +

                    '<div attachTo="tabContentNode" style="padding:${!padding};height: inherit;" class="tab-content"/>' +

                '</div>',

        "right":'<div class="tabbable tabs-right" style="height: inherit;width: inherit" attachTo="containerNode">' +

                    '<ul attachTo="tabBar" class="nav nav-tabs" role="tablist"/>' +

                    '<div attachTo="tabContentNode" style="padding:${!padding};height: inherit;" class="tab-content"/>' +

                '</div>',


        "bottom": '<div class="tabbable tabs-below" style="height: inherit;width: inherit" attachTo="containerNode">' +

                    '<div attachTo="tabContentNode" style="width: inherit; padding:${!padding}; height: 100%;" class="tab-content"/>'+

                    '<ul attachTo="tabBar" class="nav nav-tabs" role="tablist"/>'+

            '</div>'


    }





    console.clear();

    var thiz = this;
    function addFileGrid(pane){
        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                attachDirect:true,
                //attachChild:true,
                title:'tes',
                open:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane);


        pane.add(grid,null,false);





        return grid;

    }

    function createTabClass(){

        return declare(TemplatedWidgetBase,{
            iconClass:null,
            open:true,
            titleBar:null,
            toggleNode:null,
            containerNode:null,
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);


                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            postMixInProperties:function(){


                var closed = !this.open;
                this.ariaOpen = closed ? 'true' : 'false';
                this.containerClass = closed ? 'collapse' : 'collapse in';
                this.titleClass = closed ? 'collapsed' : '';

                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';


                var toggleNodeStr =
                    '<a data-dojo-attach-point="toggleNode" href="#${!id}-Collapse" data-toggle="collapse" class="accordion-toggle ${!titleClass}" aria-expanded="${!ariaOpen}">'+
                    iconStr +   ' ${!title}'+
                    '</a>';

                this.templateString = '<div class="panel widget">'+

                    '<div class="panel-heading" data-dojo-attach-point="titleBar">'+

                    toggleNodeStr +

                    '</div>'+

                    '<div data-dojo-attach-point="containerRoot" class="panel-collapse ${!containerClass}" id="${!id}-Collapse" aria-expanded="${!ariaOpen}" style="">'+
                    '<div class="panel-body" data-dojo-attach-point="containerNode">'+
                    '</div>'+
                    '</div>'+

                    '</div>';

                this.inherited(arguments);
            }
        });
    };

    function createTabPaneClass(){

        return declare(_Widget,{
            templateString:'<div></div>',
            isContainer:true,
            declaredClass:'xide/layout/_TabPane',
            panelNode:null,
            selected:false,
            $toggleNode:null,
            $toggleButton:null,
            _onShown:function(e){
                /*
                 if(this.onFirstTimeShown){
                 this.onFirstTimeShown();
                 delete this.onFirstTimeShown;
                 }*/

                this.selected = true;
                console.log('shown!');
                this.resize();
            },
            _onShow:function(e){

                if(this.onFirstTimeShown){
                    this.onFirstTimeShown();
                    delete this.onFirstTimeShown;
                }

                this.selected = true;

                this.resize();

            },
            shouldResizeWidgets:function(){
                return this.selected;
            },
            _onHide:function(){
                this.selected = false;
            },
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);

                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            __postMixInProperties:dcl.superCall(function(sup) {

                console.error('tabPane pm! in ');

                return function () {


                    var closed = !this.open;

                    var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';

                    var active = this.selected ? 'active' : '';

                    this.templateString = '<div class="tab-pane fade ' + active + '"></div>';


                    return sup ? sup.apply(this,arguments) : null;

                };

            }),

            postMixInProperties:function(){

                console.log('_TabPane:: postMixInProperties');

                var closed = !this.open;

                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';

                var active = this.selected ? 'active' : '';

                this.templateString = '<div attachTo="panelNode" style="height:100%;width:100%" class="tab-pane ' + active + '"></div>';

                //this.inherited(arguments);
            },
            __init:function(){

                var self = this;

                var panel = this.$toggleNode;

                this.__addHandler(panel,'hidden.bs.tab','_onHided');
                this.__addHandler(panel,'hide.bs.tab','_onHide');
                this.__addHandler(panel,'shown.bs.tab','_onShown');
                this.__addHandler(panel,'show.bs.tab','_onShow');
            }
        });
    };

    function doTabContainerTests(tabContainer){


        var pane = tabContainer.createTab('bla bla2','fa-cogs',true);
        addFileGrid(pane);

        var pane2 = tabContainer.createTab('bla bla 2','fa-cogs');

        addFileGrid(pane2);


        //var pane2 = tabContainer.createTab('bla bla2','fa-cogs');
    }


    function _createTabContainer(baseClass,direction){

        var tabClass = declare(baseClass || _Widget,{
            tabClass:createTabClass(),
            tabs:null,
            tabBar:null,
            tabContentNode:null,
            padding:'',
            containerCSSClass:'widget',
            resize:function(){



                if(this.tabBar){


                    switch (this.direction){

                        case 'left':
                        case 'right':{
                            this.$tabContentNode.css('width', '');
                            break;
                        }
                        case 'top':
                        case 'below':{

                            if(this.$containerNode) {

                                var _total = this.$containerNode.height();
                                var _toolbar = this.$tabBar.height();
                                this.$tabContentNode.css('height', _total - _toolbar);
                            }
                            break;
                        }
                    }
                }

                this.inherited(arguments);

            },
            direction:'top',
            __templateString:MARKUP_CONTAINER[direction || 'top'],
            templateString:'<div class="${!containerCSSClass} tabbable tabs-${!direction}" style="height: inherit;" attachTo="containerNode">' +

                '<ul attachTo="tabBar" class="nav nav-tabs" role="tablist" />' +
                '<div attachTo="tabContentNode" style="width: inherit; padding:${!padding}; height: 100%;" class="tab-content"/>' +

            '</div>',

            _templateString:'<div class="tabbable tabs-left" style="height: inherit;width: inherit" attachTo="containerNode">' +

                    '<ul attachTo="tabBar" class="nav nav-tabs"></ul>' +

                '<div attachTo="tabContentNode" style="padding:${!padding};height: inherit;" class="tab-content"></div>'+
            '</div>',

            postMixInProperties:dcl.superCall(function(sup) {
                return function () {
                    return sup ? sup.apply(this,arguments) : null;
                }
            }),
            _createTab:function(tabClass,options){
                return this.add(tabClass,options,this.containerNode,true);
            },
            createTab:function(title,icon,selected,tabClass){

                /*
                var options = arguments.length==1 ? arguments[0]:{
                    title:title,
                    iconClass:icon
                };
                return this._createTab(tabClass||this.tabClass,options);*/



                ////////////////////////////////////////////////////////////

                !this.tabs && (this.tabs = []);

                var active = this.tabs.length == 0 ? 'active' : '';

                var pane = utils.addWidget(createTabPaneClass(),{
                    title:title,
                    icon:icon,
                    selected:selected!=null ?selected : false
                },null,this.tabContentNode,true);


                var tabId = pane.id;

                var iconStr = icon ? ' ' +icon : '';

                var toggleNodeStr =
                    '<li class="' +active + '">' +
                        '<a href="#'+tabId +'" data-toggle="tab" class="' +iconStr  +'"> ' + title +'</a>'+
                    '</li>';

                var tabButton = $(toggleNodeStr);

                $(this.tabBar).append(tabButton);


                pane.$toggleNode  = tabButton.find('a[data-toggle="tab"]');
                pane.$toggleButton  = tabButton;

                pane.__init();
                this.tabs.push({
                    id:tabId,
                    pane: pane,
                    button:tabButton[0]
                });


                this.add(pane);

                return pane;
            }

        });

        return tabClass;

    }

    var ctx = window.sctx,
        doTests = true;

    if (ctx && doTests) {

        //dclWidgetTest();return declare('a',null,{});

        //dclTest();return declare('a',null,{});



        var parent = TestUtils.createTab('Tab-Container-Tests',null,module.id);



        var cls = _createTabContainer(null,'top');



        var tabContainer = utils.addWidget(cls,
            {
                padding:'0px',
                direction:'top'
            }
            ,null,parent,true);


        tabContainer.resize();



        doTabContainerTests(tabContainer);

        return declare('a',null,{});

    }

    return Grid;

});