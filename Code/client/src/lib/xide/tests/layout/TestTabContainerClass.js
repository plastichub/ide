/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    "dcl/debug",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',

    'xide/layout/_TabContainer',
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/_base/_Widget",
    "module"

], function (declare,dcl,debug,types,
             utils, Grid, factory,_TabContainer,
             TestUtils,FTestUtils,_Widget,module) {

    var thiz = this;

    function doTabContainerTests(tabContainer){

        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:true,
                //attachChild:true,
                title:'tes',
                iconClass:'fa-folder',
                selected:false
            },
            //overrides
            {


            },'TestGrid',module.id,true,tabContainer);

    }
    function _createTabContainer(baseClass){

        return _TabContainer;

    }

    var ctx = window.sctx,
        doTests = true;

    if (ctx && doTests) {


        var parent = TestUtils.createTab(null,null,module.id);

        var cls = _createTabContainer(null,'top');

        var tabContainer = utils.addWidget(cls,
            {
                padding:'0px',
                direction:'top'
            }
            ,null,parent,true);


        tabContainer.resize();



        doTabContainerTests(tabContainer);

        return declare('a',null,{});

    }

    return Grid;

});