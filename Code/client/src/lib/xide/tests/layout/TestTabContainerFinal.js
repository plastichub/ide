/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    "dcl/debug",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xide/views/CIGroupedSettingsView',

    "xide/widgets/TemplatedWidgetBase",

    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",

    "xide/_base/_Widget",
    "xide/layout/_TabContainer",
    "module"

], function (declare,dcl,debug,types,
             utils, Grid, factory,CIViewMixin,TabContainer,

             CIGroupedSettingsView,
             TemplatedWidgetBase,
             TestUtils,FTestUtils,_Widget,_TabContainer,module) {





    console.clear();

    function addFileGrid(pane){

        var grid = FTestUtils.createFileGrid('root',
            //args
            {
                //attachDirect:true,
                //attachChild:true,
                title:'tes',
                icon:'fa-folder'
            },
            //overrides
            {


            },'TestGrid',module.id,true,pane);


        pane.add(grid,null,false);


        return grid;

    }

    function createTabPaneClass(){

        return declare(_Widget,{
            templateString:'<div></div>',
            isContainer:true,
            declaredClass:'xide/layout/_TabPane',
            panelNode:null,
            selected:false,
            $toggleNode:null,
            $toggleButton:null,
            lazy:true,
            unselect:function(){
                this.$toggleButton && this.$toggleButton.removeClass('active');
                this.$selectorNode && this.$selectorNode.removeClass('active');
                this.$containerNode && this.$containerNode.removeClass('active');
                this.selected = false;
            },
            select:function(){
                //this.$toggleButton && this.$toggleButton.tab('show');


                //this.$toggleButton && this.$toggleButton.click();


                this.$toggleButton && this.$toggleButton.addClass('active');
                this.$selectorNode && this.$selectorNode.addClass('active');
                this.$containerNode && this.$containerNode.addClass('active');

                this._onShown();



            },

            _onShown:function(e){

                /*
                 if(this.onFirstTimeShown){
                 this.onFirstTimeShown();
                 delete this.onFirstTimeShown;
                 }*/



                this.selected = true;

                console.log('shown!');
                _.each(this._widgets,function(w){
                    console.log('w '+ w.declaredClass);
                    if(!w._started && w.startup){
                        w.startup();
                    }
                });

                this.onShow();
                this.resize();

            },
            _onShow:function(e){


                if(this.onFirstTimeShown){
                    this.onFirstTimeShown();
                    delete this.onFirstTimeShown;
                }

                console.log('show!');

                this.selected = true;
                this.resize();





            },
            shouldResizeWidgets:function(){
                return this.selected;
            },
            _onHide:function(){
                this.selected = false;

                this.onHide();
            },
            show:function(){

                var container = $(this.containerRoot),
                    toggleNode = $(this.toggleNode);

                toggleNode.removeClass('collapsed');
                toggleNode.attr('aria-expanded',true);


                container.removeClass('collapse');
                container.addClass('collapse in');
                container.attr('aria-expanded',true);

            },
            hide:function(){

                var container = $(this.containerRoot),
                    toggleNode= $(this.toggleNode);

                toggleNode.addClass('collapsed');
                toggleNode.attr('aria-expanded',false);

                container.removeClass('collapse in');
                container.addClass('collapse');
                container.attr('aria-expanded',false);
            },
            __postMixInProperties:dcl.superCall(function(sup) {

                console.error('tabPane pm! in ');

                return function () {


                    var closed = !this.open;

                    var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';

                    var active = this.selected ? 'active' : '';

                    this.templateString = '<div class="tab-pane fade ' + active + '"></div>';


                    return sup ? sup.apply(this,arguments) : null;

                };

            }),
            postMixInProperties:function(){

                var closed = !this.open;
                var iconStr = this.iconClass ? '<span class="${!iconClass}"/>' : '';
                var active = this.selected ? 'active' : '';
                this.templateString = '<div attachTo="containerNode" style="height:100%;width:100%" class="tab-pane ' + active + '"></div>';
                //this.inherited(arguments);
            },
            __init:function(){

                var self = this;

                var panel = this.$toggleNode;
                this.__addHandler(panel,'hidden.bs.tab','_onHided');
                this.__addHandler(panel,'hide.bs.tab','_onHide');
                this.__addHandler(panel,'shown.bs.tab','_onShown');
                this.__addHandler(panel,'show.bs.tab','_onShow');
            }
        });
    };

    function createTabContainerClass(baseClass,tabClass){

        return declare(baseClass || _Widget,{
            tabClass:tabClass || createTabPaneClass(_Widget),
            tabs:null,
            tabBar:null,
            tabContentNode:null,
            padding:'',
            containerCSSClass:'',
            direction:'top',
            templateString:'<div class="${!containerCSSClass} tabbable tabs-${!direction}" style="height: inherit;" attachTo="containerNode">' +

            '<ul attachTo="tabBar" class="nav nav-tabs" role="tablist" />' +
            '<div attachTo="tabContentNode" style="width: inherit; padding:${!padding}; height: 100%;" class="tab-content"/>' +

            '</div>',
            getTab:function(name){
                return _.find(this._widgets,{
                    title:name
                });
            },
            _unselectAll:function(){

                _.each(this._widgets,function(tab){
                    tab.unselect();
                });

            },
            selectChild:function(mixed){

                if(mixed) {

                    if (_.isString(mixed)) {

                        var tab = this.getTab(mixed);

                        if (tab && tab.select) {

                            this._unselectAll();

                            tab.select();
                        }
                    }else{
                        console.error('selectChild : not a string');
                    }
                }else{
                    console.error('selectChild : mixed = null');
                }

                //console.log('get tab ',tab);


                //console.log('select child ',arguments);
            },
            addWidget:function(widgetProto, ctrArgsIn, delegate, parent, startup, cssClass,baseClasses,select,classExtension){

                var target = parent;


                if(widgetProto.isContainer){

                }else{

                    console.log('addin tab',ctrArgsIn);

                    target = this._createTab(this.tabClass,{
                        title:ctrArgsIn.title,
                        icon:ctrArgsIn.icon,
                        selected:ctrArgsIn.selected,
                        ignoreAddChild:true
                    });
                }

                var w = target.add(widgetProto,ctrArgsIn,null,startup);

                return w;
            },
            resize:function(){

                if(this.tabBar){
                    switch (this.direction){

                        case 'left':
                        case 'right':{
                            this.$tabContentNode.css('width', '');
                            break;
                        }
                        case 'top':
                        case 'below':{

                            if(this.$containerNode) {

                                var _total = this.$containerNode.height();
                                var _toolbar = this.$tabBar.height();
                                this.$tabContentNode.css('height', _total - _toolbar);
                            }
                            break;
                        }
                    }
                }
                this.inherited(arguments);

            },
            _createTab:function(tabClass,options){


                !this.tabs && (this.tabs = []);

                var active = this.tabs.length == 0 ? 'active' : '',
                    icon = options.icon || '',
                    title = options.title || '',
                    selected = options.selected!=null ? options.selected : this.tabs.length ==0;


                if(this.tabs.length ==0){
                    //    selected=true;
                }

                var pane = utils.addWidget(tabClass || this.tabClass,{
                    title:title,
                    icon:icon,
                    selected:selected
                },null,this.tabContentNode,true);


                var tabId = pane.id,
                    iconStr = icon ? ' ' +icon : '',
                    toggleNodeStr =
                        '<li class="' +active + '">' +
                        '<a href="#'+tabId +'" data-toggle="tab" class="' +iconStr  +'"> ' + title +'</a></li>',
                    tabButton = $(toggleNodeStr);

                $(this.tabBar).append(tabButton);


                pane.$toggleNode  = tabButton.find('a[data-toggle="tab"]');
                pane.$selectorNode  = tabButton.find('li');
                pane.$toggleButton  = tabButton;

                pane.__init();

                this.tabs.push({
                    id:tabId,
                    pane: pane,
                    button:tabButton[0]
                });


                this.add(pane,null,false);

                return pane;
            },
            createTab:function(title,icon,selected,tabClass,mixin){
                return this._createTab(tabClass,utils.mixin({
                    icon:icon,
                    selected:selected,
                    title:title
                },mixin));
            }
        });

    }

    function doTabContainerTests_CITabView(tabContainerClass,parent){

        var CIS = TestUtils.createCIS();


        CIS.inputs.push({
            "chainType": 0,
            "class": "cmx.types.ConfigurableInformation",
            "dataRef": "",
            "dataSource": "",
            "description": null,
            "enabled": true,
            "enumType": "-1",
            "flags": -1,
            "group": 'AA',
            "id": "CF_DRIVER_CLASS3",
            "name": "CF_DRIVER_CLASS3",
            "order": 1,
            "params": null,
            "parentId": "myeventsapp108",
            "platform": null,
            "storeDestination": "metaDataStore",
            "title": "Driver Class",

            "type": types.ECIType.EXPRESSION_EDITOR,

            "uid": "-1",
            "value": "background:none",
            "visible": true,
            "select":true,
            "widget":{
                showBrowser:true,
                icon:'fa-code'
            }
        });




        var cisRenderer = declare('cis',[CIGroupedSettingsView],{
            tabContainerClass:tabContainerClass,
            options:{
                select:'General1'
            }

        });



        var cisView = utils.addWidget(cisRenderer,{
            cis:CIS.inputs,
            tabContainerClass:tabContainerClass,
            attachWidgets: function (data, dstNode,view) {
                //console.error('attach widgets',view);

                var thiz = this;

                this.helpNodes = [];
                this.widgets = [];

                dstNode = dstNode || this.domNode;

                if(!dstNode && this.tabContainer){
                    dstNode = this.tabContainer.containerNode;
                }
                if (!dstNode) {
                    console.error('have no parent dstNode!');
                    return;
                }

                for (var i = 0; i < data.length; i++) {
                    var widget = data[i];

                    widget.delegate = this.owner || this;

                    dstNode.appendChild(widget.domNode);

                    if(view && view.lazy===true) {

                        widget._startOnShow = true;
                    }else{
                        widget.startup();
                    }

                    widget._on('valueChanged',function(evt){
                        thiz.onValueChanged(evt);
                    });

                    this.widgets.push(widget);

                    widget.userData.view=view;

                    if(view && view.add && view.add(widget,null,false)){

                    }else{
                        console.error('view has no add',view);
                    }



                }
            }
        },null,parent,true);


    }






    var _Tab = createTabPaneClass(_Widget);
    var _TabContainerClassInner = createTabContainerClass(_Widget,_Tab);

    _TabContainerClassInner.tabClass = _Tab;

/*
    dojo.setObject('xide.layout._TabContainer',_TabContainerClassInner);

    dojo.setObject('xide/layout/_TabContainer',_TabContainerClassInner);
*/




    var ctx = window.sctx,
        doTests = true;

    if (ctx && doTests) {

        var parent = TestUtils.createTab('Tab-Container-Tests',null,module.id);

        /*
        var tabContainer = utils.addWidget(_TabContainer,
            {
                padding:'10px',
                direction:'left'

            }
            ,null,parent,true);




        tabContainer.resize();
*/

        doTabContainerTests_CITabView(_TabContainerClassInner||_TabContainer,parent);

        return declare('a',null,{});

    }



    return Grid;

});