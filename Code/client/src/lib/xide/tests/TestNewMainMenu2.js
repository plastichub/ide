/** @module xgrid/Base **/
define([
	
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "./TestUtils",
    "xide/widgets/_Widget",
    "module",
    'xide/registry',
    'xide/_base/_Widget',
    "dojo/text!./Navbar.html",
    "xide/mixins/ActionMixin",
    'xide/action/ActionContext',
    'xide/action/ActionStore',
    'xide/action/DefaultActions',
    "xaction/ActionProvider",
    "xgrid/Clipboard",
    "xide/model/Path",
    'xide/action/Action',
    'xfile/tests/TestUtils',
    'xlang/i18',
    'xide/mixins/EventedMixin'
    

], function (declare,dcl,types,utils,TestUtils,_Widget,module,registry,_XWidget,MenuBarTemplate,ActionMixin,ActionContext,ActionStore,DefaultActions,ActionProvider,Clipboard,Path,Action,FTestUtils,i18,EventedMixin){

    var ACTION = types.ACTION;
    var options = {
        fadeSpeed: 100,
        filter: function ($obj) {
            // Modify $obj, Do not return
        },
        above: 'auto',
        left: 'auto',
        preventDoubleContext: true,
        compress: false

        
    };
    function initialize(opts) {

        options = $.extend({}, options, opts);

        $(document).on('click', function () {
            $('.dropdown-context').fadeOut(options.fadeSpeed, function(){
                $('.dropdown-context').css({display:''}).find('.drop-left').removeClass('drop-left');
            });
        });
        if(options.preventDoubleContext){
            $(document).on('contextmenu', '.dropdown-context', function (e) {
                e.preventDefault();
            });
        }
        $(document).on('mouseenter', '.dropdown-submenu', function(){
            var t = $(this);

            console.error('this',this);

            var $sub = $(this).find('.dropdown-context-sub:first');

                var subWidth = $sub.width(),
                subLeft = $sub.offset().left,
                collision = (subWidth+subLeft) > window.innerWidth;
            if(collision){
                $sub.addClass('drop-left');
            }
        });

    }
    function updateOptions(opts){
        options = $.extend({}, options, opts);
    }
    function buildMenu(data, id, subMenu) {
        //console.log('build menu',arguments);
        var subClass = (subMenu) ? ' dropdown-context-sub' : '',
            compressed = options.compress ? ' compressed-context' : '',
            $menu = $('<ul role="menu" class="dropdown-menu dropdown-context' + subClass + compressed +'" id="dropdown-' + id + '"></ul>');

        var $this = $menu;
        //$menu.find('[data-toggle="tooltip"]').tooltip();
        //$('[data-toggle="tooltip"]').tooltip();
        /*
        $menu.keypress(function(e){


            // get the key that was pressed
            var key = String.fromCharCode(e.which);
            // look at all of the items to find a first char match
            $this.find("li").each(function(idx,item){
                $(item).removeClass("active"); // clear previous active item
                if ($(item).text().charAt(0).toLowerCase() == key) {
                    // set the item to selected (active)
                    $(item).addClass("active");
                }
            });

        });*/
        return buildMenuItems($menu, data, id, subMenu);
    }
    function buildMenuItems($menu, data, id, subMenu, addDynamicTag) {
        var linkTarget = '';
        for(var i = 0; i<data.length; i++) {

            var item = data[i],
                $sub;


            if (typeof data[i].divider !== 'undefined') {
                var divider = '<li class="divider';
                divider += (addDynamicTag) ? ' dynamic-menu-item' : '';
                divider += '"></li>';
                $menu.append(divider);
            } else if (typeof data[i].header !== 'undefined') {
                var header = '<li class="nav-header';
                header += (addDynamicTag) ? ' dynamic-menu-item' : '';
                header += '">' + data[i].header + '</li>';
                $menu.append(header);
            } else if (typeof data[i].menu_item_src !== 'undefined') {
                var funcName;
                if (typeof data[i].menu_item_src === 'function') {
                    if (data[i].menu_item_src.name === "") { // The function is declared like "foo = function() {}"
                        for (var globalVar in window) {
                            if (data[i].menu_item_src == window[globalVar]) {
                                funcName = globalVar;
                                break;
                            }
                        }
                    } else {
                        funcName = data[i].menu_item_src.name;
                    }
                } else {
                    funcName = data[i].menu_item_src;
                }
                $menu.append('<li class="dynamic-menu-src" data-src="' + funcName + '"></li>');
            } else {

                if (typeof data[i].href == 'undefined') {
                    data[i].href = '#';
                }
                if (typeof data[i].target !== 'undefined') {
                    linkTarget = ' target="'+data[i].target+'"';
                }
                if (typeof data[i].subMenu !== 'undefined') {
                    var sub_menu = '<li class="dropdown-submenu';
                    sub_menu += (addDynamicTag) ? ' dynamic-menu-item' : '';
                    sub_menu += '"><a tabindex="-1" href="' + data[i].href + '">';

                    if (typeof item.icon !== 'undefined') {
                        sub_menu+= '<span class=" ' + item.icon + '"></span> ';

                    }

                    sub_menu += data[i].text + '';
                    sub_menu+='</a></li>';
                    $sub = (sub_menu);
                } else {

                    if(item.render){

                        $sub = item.render(item,$menu);

                    }else {
                        var element = '<li class="" ';
                        element += (addDynamicTag) ? ' class="dynamic-menu-item"' : '';
                        element += '><a tabindex="-1" href="' + data[i].href + '"' + linkTarget + '>';
                        if (typeof data[i].icon !== 'undefined') {
                            element += '<span class="glyphicon ' + data[i].icon + '"></span> ';

                        }
                        element += data[i].text + '</a></li>';
                        $sub = $(element);
                        if (item.postRender) {
                            item.postRender($sub);
                        }
                    }
                }
                if (typeof data[i].action !== 'undefined') {

                    if(item.addClickHandler && item.addClickHandler()===false){
                    }else {
                        var $action = data[i].action;
                        if ($sub && $sub.find) {
                            $sub.find('a')
                                .addClass('context-event')
                                .on('click', createCallback($action, item, $sub));
                        }
                    }
                }

                $menu.append($sub);
                item.widget = $sub;

                if (typeof data[i].subMenu != 'undefined') {
                    var subMenuData = buildMenu(data[i].subMenu, id, true);
                    $menu.find('li:last').append(subMenuData);
                }
            }



            if (typeof options.filter == 'function') {
                options.filter($menu.find('li:last'));
            }



            //console.log('did menu',subMenu);
            if(subMenu) {

                /*
                subMenu.on('show.bs.dropdown', function (e) {
                    console.error('hide');
                });
                */
            }

            $menu.on('click', '.dropdown-menu > li > input[type="checkbox"] ~ label, .dropdown-menu > li > input[type="checkbox"], .dropdown-menu.noclose > li', function (e) {
                //console.error('abort');
                e.stopPropagation()
            });
            /*
            $sub.on('show.bs.dropdown', function (e) {
                console.error('hide');
                var target = $(e.target);
                if(target.hasClass("keepopen") || target.parents(".keepopen").length){
                    return false; // returning false should stop the dropdown from hiding.
                }else{
                    return true;
                }
            });
            */
        }
        return $menu;
    }
    function addContext(selector, data) {

        if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
            var id = data.id;
            $menu = $('body').find('#dropdown-' + id)[0];
            if (typeof $menu === 'undefined') {
                $menu = buildMenu(data.data, id);
                $('body').append($menu);
            }
        } else {
            var d = new Date(),
                id = d.getTime(),
                $menu = buildMenu(data, id);
            $('body').append($menu);
        }


        $(selector).on('contextmenu', function (e) {
            e.preventDefault();
            e.stopPropagation();

            currentContextSelector = $(this);


            $('.dropdown-context:not(.dropdown-context-sub)').hide();

            $dd = $('#dropdown-' + id);

            $dd.find('.dynamic-menu-item').remove(); // Destroy any old dynamic menu items
            $dd.find('.dynamic-menu-src').each(function(idx, element) {
                var menuItems = window[$(element).data('src')]($(selector));
                $parentMenu = $(element).closest('.dropdown-menu.dropdown-context');
                $parentMenu = buildMenuItems($parentMenu, menuItems, id, undefined, true);
            });

            if (typeof options.above == 'boolean' && options.above) {
                $dd.addClass('dropdown-context-up').css({
                    top: e.pageY - 20 - $('#dropdown-' + id).height(),
                    left: e.pageX - 13
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.above == 'string' && options.above == 'auto') {
                $dd.removeClass('dropdown-context-up');
                var autoH = $dd.height() + 12;
                if ((e.pageY + autoH) > $('html').height()) {
                    $dd.addClass('dropdown-context-up').css({
                        top: e.pageY - 20 - autoH,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                } else {
                    $dd.css({
                        top: e.pageY + 10,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                }
            }

            if (typeof options.left == 'boolean' && options.left) {
                $dd.addClass('dropdown-context-left').css({
                    left: e.pageX - $dd.width()
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.left == 'string' && options.left == 'auto') {
                $dd.removeClass('dropdown-context-left');
                var autoL = $dd.width() - 12;
                if ((e.pageX + autoL) > $('html').width()) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width() + 13
                    });
                }
            }
        });

        return $menu;
    }
    function destroyContext(selector) {
        $(document).off('contextmenu', selector).off('click', '.context-event');
    }

    var currentContextSelector = null;
    var createCallback = function(func,menu,item) {

        return function(event) {
            func(event, menu,item);
        };
    }




    
    function addContext2(selector, data) {

        var id,
            $menu;

        if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
            id = data.id;
            $menu = $('body').find('#dropdown-' + id)[0];
            if (typeof $menu === 'undefined') {
                $menu = buildMenu(data.data, id);
                selector.append($menu);
            }
        } else {
            var d = new Date();

            id = d.getTime();
            $menu = buildMenu(data, id);
            selector.append($menu);

        }

        return $menu;

        $(selector).on('contextmenu', function (e) {


            var currentContextSelector = $(this);


            $('.dropdown-context:not(.dropdown-context-sub)').hide();

            $dd = $('#dropdown-' + id);

            $dd.find('.dynamic-menu-item').remove(); // Destroy any old dynamic menu items
            $dd.find('.dynamic-menu-src').each(function(idx, element) {
                var menuItems = window[$(element).data('src')]($(selector));
                $parentMenu = $(element).closest('.dropdown-menu.dropdown-context');
                $parentMenu = buildMenuItems($parentMenu, menuItems, id, undefined, true);
            });

            if (typeof options.above == 'boolean' && options.above) {
                $dd.addClass('dropdown-context-up').css({
                    top: e.pageY - 20 - $('#dropdown-' + id).height(),
                    left: e.pageX - 13
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.above == 'string' && options.above == 'auto') {
                $dd.removeClass('dropdown-context-up');
                var autoH = $dd.height() + 12;
                if ((e.pageY + autoH) > $('html').height()) {
                    $dd.addClass('dropdown-context-up').css({
                        top: e.pageY - 20 - autoH,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                } else {
                    $dd.css({
                        top: e.pageY + 10,
                        left: e.pageX - 13
                    }).fadeIn(options.fadeSpeed);
                }
            }

            if (typeof options.left == 'boolean' && options.left) {
                $dd.addClass('dropdown-context-left').css({
                    left: e.pageX - $dd.width()
                }).fadeIn(options.fadeSpeed);
            } else if (typeof options.left == 'string' && options.left == 'auto') {
                $dd.removeClass('dropdown-context-left');
                var autoL = $dd.width() - 12;
                if ((e.pageX + autoL) > $('html').width()) {
                    $dd.addClass('dropdown-context-left').css({
                        left: e.pageX - $dd.width() + 13
                    });
                }
            }

        });

        return $menu;
    }

    console.clear();

    var _ctx = {}


    //action renderer class
    var ContainerClass = dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        visibility: types.ACTION_VISIBILITY.MAIN_MENU,
        templateString:'<div>'+
            '<nav attachTo="navigation" class="navbar navbar-inverse" role="navigation">'+
                    '<ul attachTo="navBar" class="nav navbar-nav"/>'+
            '</nav>'+
        '</div>'
    });







    /**
     *
     * @class module:xide/widgets/MainMenuActionRenderer
     */
    var ActionRendererClass = dcl(null,{
        renderTopLevel:function(name,where){

            where = where || $(this.getRootContainer());

            var item =$('<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) +'<b class="caret"></b></a>'+
                '</li>');

            where.append(item);

            return item;

        },
        getRootContainer:function(){
            return this.navBar;
        },
        getActionData:function(action){

            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};
            return {
                label:actionVisibility.label !=null ? actionVisibility.label : action.label,
                icon:actionVisibility.icon !=null ? actionVisibility.icon : action.icon,
                visibility: actionVisibility
            }
        }
    });

    /**
     * @extends module:xide/widgets/MainMenuActionRenderer
     */
    var MainMenu = dcl([ContainerClass,ActionRendererClass],{
        target:null,
        constructor:function(options,node){
            this.target = node;
        },
        _init:initialize,
        init: function(opts){

            var options = {
                fadeSpeed: 100,
                filter: function ($obj) {
                    // Modify $obj, Do not return
                },
                above: 'auto',
                left: 'auto',
                preventDoubleContext: false,
                compress: false

            };
            options = $.extend({}, options, opts);
            var self = this;

            var root = $(document);

            this.__on(root,'click',null,function(){
                $('.dropdown-context').fadeOut(options.fadeSpeed, function(){
                    $('.dropdown-context').css({display:''}).find('.drop-left').removeClass('drop-left');
                });
            });

            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }



            
            this.__on(root,'mouseenter', '.dropdown-submenu', function(e){

                var _root = $(e.currentTarget);
                var $sub = _root.find('.dropdown-context-sub:first');
                var subWidth = $sub.width(),
                    subLeft = $sub.offset().left,
                    collision = (subWidth+subLeft) > window.innerWidth;
                if(collision){
                    $sub.addClass('drop-left');
                }
            });


        },
        settings: function(){},
        addContext:function(selector,data){
            var id,
                $menu;

            if (typeof data.id !== 'undefined' && typeof data.data !== 'undefined') {
                id = data.id;
                $menu = $('body').find('#dropdown-' + id)[0];
                if (typeof $menu === 'undefined') {
                    $menu = buildMenu(data.data, id);
                    selector.append($menu);
                }
            } else {
                var d = new Date();

                id = d.getTime();
                $menu = buildMenu(data, id);
                selector.append($menu);

            }

            return $menu;
        },
        attach: function(selector,data){
            this.target = selector;
            this.menu = this.addContext(selector,data);
            this.domNode = this.menu[0];
            this.id = this.domNode.id;
            registry.add(this);
            return this.menu;
        },
        destroy: function(){
            try {
                if(this.domNode) {
                    registry.remove(this.domNode.id);
                }
                utils.destroy(this.domNode);
                this.inherited(arguments);
            }catch(e){
                logError(e);
            }
        },
        setActionStore:function(store,owner){


            var self = this,
                allActions = store.query(),
                visibility = self.visibility,
                rootContainer = $(self.getRootContainer());

            //return all actions with non-empty tab field
            var tabbedActions = allActions.filter(function (action) {
                    return action.tab != null;
            }),

            //group all tabbed actions : { Home[actions], View[actions] }
            groupedTabs = _.groupBy(tabbedActions, function (action) {
                return action.tab;
            }),
            //now flatten them
            _actionsFlattened = [];

            _.each(groupedTabs,function(items,name){
                _actionsFlattened = _actionsFlattened.concat(items);
            });

            var rootActions = [];
            _.each(tabbedActions,function(action){
                var rootCommand = action.getRoot();
                !rootActions.includes(rootCommand) && rootActions.push(rootCommand);
            });

            function getAction(command){
                var action = store.getSync(command);
                return action;
            }
            function toActions(paths){
                var result = [];
                _.each(paths,function(path){
                    result.push(getAction(path));
                });
                return result;
            }
            /**
             *
             * @param action {module:xide/action/Action}
             * @param owner
             * @returns {{text: *, icon: *, data: *, addClickHandler: item.addClickHandler, render: item.render, get: item.get, set: item.set, action: item.action, destroy: item.destroy}}
             */
            function toItem(action,owner,label,icon,visibility){

                var labelLocalized = i18.localize(label),
                    widgetArgs = visibility.widgetArgs,
                    actionType = visibility.actionType || action.actionType;

                var item = {
                    text:labelLocalized,
                    icon:icon,
                    data:action,
                    addClickHandler:function(item){

                        var action = this.data;
                        if(visibility.actionType==='multiToggle'){
                            return false;
                        }

                        return true;
                    },
                    render:function(data,$menu){

                        var action = this.data;
                        var parentAction = action.getParent();

                        var keyComboString =' \n';

                        if(action.keyboardMappings) {

                            var mappings = action.keyboardMappings;
                            var keyCombos = _.pluck(mappings,['keys']);
                            if(keyCombos && keyCombos[0]) {
                                keyCombos = keyCombos[0];

                                if(keyCombos.join) {
                                    var keycombo = [];
                                    keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                                }
                            }
                        }

                        if(actionType===types.ACTION_TYPE.MULTI_TOGGLE){

                            var element = '<li class="" >';
                            var id = action._store.id +'_' +action.command;
                            var checked = action.get('value');

                            //checkbox-circle
                            element+='<div class="checkbox checkbox-success ">';
                            if (typeof data.icon !== 'undefined') {
                                //element += '<span style="float: left" class="' + data.icon + '"></span>';
                            }
                            element+='<input id="' +  id + '" type="checkbox" ' + (checked == true ? 'checked' : '') +'>';
                            element+='<label for="' + id + '">';
                            element+= self.localize(data.text) +'</label>';
                            element+='</div>';

                            $menu.addClass('noclose');
                            var result = $(element);
                            var checkBox = result.find('INPUT');
                            checkBox.on('change',function(e){
                                console.warn('change ' + checkBox[0].checked);
                                action.set('value',checkBox[0].checked);
                            });

                            return result;
                        }


                        if(actionType===types.ACTION_TYPE.SINGLE_TOGGLE && parentAction){
                            var value = action.value;
                            var parentValue = parentAction.get('value');
                            if(value===parentValue){
                                icon = 'fa-check';
                            }
                        }


                        //default:
                        var element = '<li ';
                        element += '';
                        element += '><a data-toggle="tooltip" data-placement="right" title="' +data.text + '\n' + keyComboString +'" href="' + data.href + '"' + '#' + '>';


                        //icon
                        if (typeof data.icon !== 'undefined') {
                            element += '<span class="' + icon + '"></span> ';

                        }

                        element += data.text;

                        element += '<span style="max-width: 100px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString +'</span>';

                        element +='</a></li>';

                        return $(element);



                    },
                    get:function(key){},
                    set:function(key,value){

                        var widget = this.widget;
                        if(widget){
                            if(key==='disabled'){
                                if(widget.toggleClass){
                                    widget.toggleClass('disabled',value);
                                }
                            }
                        }
                    },
                    action: function(e, data,menu) {
                        console.log('menu action',data);
                        if(owner && owner.runAction && data.data){
                            owner.runAction(data.data);
                        }
                    },
                    destroy:function(){
                        console.log('destroy');
                    }
                }

                return item;

            }

            if (store.menuOrder) {
                rootActions = owner.sortGroups(rootActions, store.menuOrder);
            }

            _.each(rootActions,function(level){

                var dropdown = self.renderTopLevel(level,rootContainer);

                // collect all actions at level (File/View/...)
                var menuActions = owner.getItemsAtBranch(allActions,level);

                // final menu data
                var data = [];

                // convert action command strings to Action references
                var grouped = toActions(menuActions);

                // group all actions by group
                var groupedActions = _.groupBy(grouped, function (action) {
                    if(action && action.group) {
                        return action.group;
                    }
                });

                //temp array
                var _actions = [];
                _.each(groupedActions,function(items,level){
                    if(level!=='undefined') {
                        _actions = _actions.concat(items);
                    }
                });
                //flatten out again
                menuActions = _.pluck(_actions,'command');

                //collect all existing actions as string array
                var allActionPaths = _.pluck(allActions,'command');

                //temp group string of the last rendered action's group
                var lastGroup = '';

                _.each(menuActions,function(command){

                    var action = getAction(command);

                    var isDynamicAction = false;

                    if (!action) {
                        isDynamicAction = true;
                        var segments = command.split('/');
                        var lastSegment = segments[segments.length - 1];
                        action = new Action({
                            command: command,
                            label:lastSegment
                        });
                    }


                    if(action){

                        var renderData = self.getActionData(action);
                        var icon = renderData.icon,
                            label = renderData.label,
                            visibility = renderData.visibility;

                        if(!isDynamicAction && action.group && groupedActions[action.group].length>2){
                            if(lastGroup!==action.group){
                                data.push({header: i18.localize(action.group)});
                                lastGroup = action.group;
                            }
                        }

                        var item = toItem(action,owner,label,icon,visibility || {});


                        data.push(item);

                        visibility.widget = item;

                        if(action.addReference){
                            action.addReference(item, {
                                properties: {
                                    "value": true,
                                    "disabled":true
                                }
                            }, true);
                        }

                        var childPaths = new Path(command).getChildren(allActionPaths,false),
                            isContainer = childPaths.length> 0,
                            childActions = isContainer ? toActions(childPaths) : null ;

                        if(childActions){
                            var subs = [];
                            _.each(childActions,function(child){
                                var _renderData = self.getActionData(child);
                                subs.push(toItem(child,owner,_renderData.label,_renderData.icon,_renderData.visibility));
                            });
                            item.subMenu = subs;

                        }
                    }
                });

                var menu = self.attach(dropdown,data);
            });
        }
    });

    var MenuBar = dcl(_XWidget,{
        templateString:MenuBarTemplate
    });

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    //action source class
    var ActionProviderClass = dcl([ActionProvider.dcl],{
        permissions:[
            ACTION.EDIT,
            ACTION.COPY,
            ACTION.MOVE,
            ACTION.RENAME,
            ACTION.DOWNLOAD,
            ACTION.RELOAD,
            ACTION.DELETE,
            ACTION.NEW_FILE,
            ACTION.NEW_DIRECTORY,
            ACTION.CLIPBOARD,
            ACTION.CLIPBOARD_COPY,
            ACTION.CLIPBOARD_PASTE,
            ACTION.CLIPBOARD_CUT
        ]
    });

    //action source class instance
    var ActionProviderInstance = new ActionProviderClass({});



    function doTests(tab){
        

        var grid = FTestUtils.createFileGrid('root',{},{

        },'TestGrid',module.id,false,tab);

        var menuNode = grid.header;


        var _defaultActions = DefaultActions.getDefaultActions(grid.permissions, grid,grid);

        ActionProviderInstance.addActions(_defaultActions);

        //var context = new MainMenu({}, container.domNode);
        var mainMenu = utils.addWidget(MainMenu,{

        },null,menuNode,true);
        mainMenu.init({preventDoubleContext: false});
        grid.resize();
        //////////////////////////////////////////////////
        mainMenu._registerActionEmitter(grid);
        return;
    }



    var ctx = window.sctx,
        root;

    if (ctx) {
        var parent = TestUtils.createTab(null,null,module.id);
        doTests(parent);
        return declare('a',null,{});

    }
    return _Widget;

});

