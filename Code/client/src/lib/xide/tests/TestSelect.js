/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "./TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "xide/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'xide/widgets/WidgetBase'

], function (declare,dcl,types,
             utils, Grid, TemplatedWidgetBase,EventedMixin,
             TestUtils,FTestUtils,_Widget,registry,module,
             cache,domConstruct, lang, string,_XWidget,
             TreeMemory,ObservableStore,Trackable,WidgetBase
) {






    console.clear();

    console.log('--do-tests');

    var actions = [],
        thiz = this,
        ACTION_TYPE = types.ACTION,
        ACTION_ICON = types.ACTION_ICON,
        grid,
        ribbon,
        CIS;

    function _StoreMixin(){

        var Module = dcl(null,{

            wireStore:function(store,updateFn){

                store = store || this.store;


                store.on('update', function () {
                    console.warn(' store updated: ',arguments);
                    updateFn();
                });

                store.on('added', function () {
                    console.log(' added to store: ',arguments);
                    updateFn();
                });

                store.on('remove', function () {
                    console.log(' removed from store: ',arguments);
                    updateFn();
                });

                store.on('delete', function () {
                    console.log(' deleted from store: ',arguments);
                    updateFn();
                });
            }
        });

        return Module;
    }


    function createStore(){

        var storeClass = declare('driverStore',[TreeMemory,Trackable,ObservableStore],{});

        var store = new storeClass({
            idProperty:'value'
        });

        store.setData([
            {
                label:'test',
                value:'test-value'
            },
            {
                label:'Barbecue',
                value:'Barbecue'
            },
            {
                label:'Divider',
                value:utils.createUUID(),
                divider:true
            }
        ]);


        return store;

    }

    function createSelectClass(){
        var Module = dcl([WidgetBase,_StoreMixin()],{
            store: createStore(),
            declaredClass:'xide.form.Select',
            $selectNode:null,
            search:false,
            style:'btn-info',
            query: null,
            title:'Choose:',
            disabled:false,
            select:null,
            value:'Barbecue',
            selectOptions:{
                container:'body'
            },
            noStore:false,
            _picker:null,
            _items:null,
            userData:{
              value:"Barbecue"
            },
            templateString:"<div class='widgetContainer widgetBorder widgetTable widget' style=''>" +
            "<table border='0' cellpadding='5px' width='100%'>" +
            "<tbody align='left'>" +
            "<tr attachTo='extensionRoot' valign='middle' style='height:90%'>" +
            "<td attachTo='titleColumn' width='15%' class='widgetTitle'><b><span attachTo='titleNode'>${!title}</span></b></td>" +
            "<td valign='middle' class='widgetValue' attachTo='valueNode' width='100px'>" +
            '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode">'+
            "</td>" +
            "<td class='extension' attachTo='previewNode'></td>" +
            "<td class='extension' attachTo='button0'></td>" +
            "<td class='extension' attachTo='button1'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
            render:function(){
                this.empty();
                //if(this.store) {
                this._render();
                //this.$selectNode.selectpicker('refresh');
                var _picker = this.getPicker();
                //_picker.refresh();
                _picker.val(this.value);
                _picker.render();
                //}
            },
            empty:function(){
                this.$selectNode.empty();
            },

            destroy:function(){
                if(this._picker){
                    this._picker.destroy();
                }
                if(this.store && this.store.destroy && this.store.ownStore){
                    this.store.destroy();
                }
                delete this._picker;
                delete this.store;
            },
            getPicker:function(){
                if(!this._picker){
                    this._picker = this.$selectNode.selectpicker(this.selectOptions).data().selectpicker;

                }
                return this._picker;
            },
            _createStore:function(options){
                var storeClass = declare('driverStore',[TreeMemory,Trackable,ObservableStore],{});
                var store = new storeClass({
                    idProperty:'value',
                    ownStore:true
                });
                store.setData(options);
                return store;
            },
            set:function(what,value,label){
                if(what ==='value'){
                    this.value = value;
                    var _picker = this.getPicker();
                    value!==null && _picker.refresh();
                    _picker.val(value, label);
                    _picker.render();
                    return null;
                }
                return this.inherited(arguments);
            },
            get:function(what){
                if(what ==='value'){
                    return this.getPicker().val();
                }
                return this.inherited(arguments);
            },
            startup:function(){

                if(!this.title){
                    this.$titleNode.remove();
                    this.$titleNode = null;
                    this.$titleColumn.remove();
                    this.$titleColumn = null;
                }
                if(!this.disabled){
                    this.$selectNode.removeAttr('disabled');
                }

                var userData = this.userData || {};
                this.$titleNode && this.$titleNode.html(userData.title);
                if(!this.store && this.options && this.noStore!==true){
                    this.store = this._createStore(this.options);
                    var _selected = _.find(this.options,{
                        selected:true
                    });
                    _selected && (this.value = _selected.value);
                }

                this.select = this.getPicker();
                if(this.store) {
                    this.render();
                    this.wireStore(this.store,this.render.bind(this));
                }else{
                    this.render();
                }
                var thiz = this;
                if(this.value!==null) {
                    this.set('value',this.value);
                }
                this.$selectNode.on('change',function(){
                    thiz._emit('change',thiz.get('value'));
                    thiz.setValue(thiz.get('value'));
                });
                this.onReady();
            },
            _render: function () {
                this._destroyItems();

                if(this.store){
                    var items = [];

                    if(_.isArray(this.query)){
                        var self = this;
                        _.each(this.query,function(query){
                            items = items.concat(self.store.query(query));
                        });
                    }else{
                        items = this.store.query(this.query || {});
                    }
                    this._renderItems(items);
                }else if(this.options){
                    this._items = [];
                    _.each(this.options,function(item){
                        var renderedItem = this._renderItem(item);
                        // look for `placeAt` method to support items rendered as widgets
                        if ('placeAt' in renderedItem) {
                            renderedItem.placeAt(this);
                        }
                    },this)

                }
            },
            _renderItems: function (queryResults) {
                this._items = [];
                queryResults.forEach(function (item) {
                    var renderedItem = this._renderItem(item);
                    // look for `placeAt` method to support items rendered as widgets
                    if ('placeAt' in renderedItem) {
                        renderedItem.placeAt(this);
                    }
                }, this);
            },

            _renderItem: function (item) {
                if(item.divider){
                    return this.selectNode.appendChild($('<option data-divider="true"></option>')[0]);
                }else {

                    var _label = this.labelField ? item[this.labelField] :  item.label;
                    var _value = this.valueField ? item[this.valueField] :  item.value;

                    if(!_.find(this._items,{value:_value})) {
                        this._items.push({
                            label: _label,
                            value: _value
                        });
                    }
                    return this.selectNode.appendChild($('<option value="' + _value + '" data-icon="' + item.icon + '">' + _label + '</option>')[0]);
                }
            },
            _destroyItems: function () {
                var preserveDom = true;
                this.destroyDescendants(preserveDom);
                this.empty();
            }


        });


        return Module;


    }

    function doStoreTests(){

    }

    function doTests(tab){



        var _class = createSelectClass();

        var select = utils.addWidget(_class,{

        },null,tab,true);


        select._on('change',function(e){
            console.error('changed ' + e);
        });


        setTimeout(function(){
            select.store.addSync({
                label:'bla',
                value:'bla',
                icon:'fa-code'
            });
            select.set('value','bla');
        },2000);


        /*
        setTimeout(function(){
            select.store.removeSync('Barbecue');
        },2000);
        */

    }

    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    if (ctx) {



        var parent = TestUtils.createTab(null,null,module.id);

        doTests(parent);

        return declare('a',null,{});

    }

    return Grid;

});