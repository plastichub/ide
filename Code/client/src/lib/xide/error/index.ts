/**
*
 * Adds an error listener to window.onerror. If there already exists an error listener
 * a reference to the original existing error listener will be kept.
 * This error listener will then receive any js errors from the page and it will
 * report the information available about the javascript errors back to the server.
 * The error listener will also call the originally present error handler (if any).
 *
 * This script also adds an property change listener to window.onerror (using utility
 * function addPropertyListener) that should prevent that our error listener is overwritten
 * by a third party error listener.
 * However the property change listener does not work for all browsers so there is no
 * guarantuee that the error listener will never be overwritten.
 * Browsers that do not support the property change listener are:
 *  o IE < 9
 *  o Safari (TODO: test version)
 *  o Firefox < 11
 *
 * REFERENCES:
 *
 * Browser Support for window.onerror (http://www.w3.org/wiki/DOM/window.onerror)
 *    o Chrome 13+
 *    o Firefox 6.1+
 *    o Internet Explorer 5.5+
 *    o (Opera 12+)
 *    o Safari 5.1+
 *
 * Other sources for browser compatibility:
 *   o (http://pieisgood.org/test/script-link-events/)
 *   o Error Information available in onerror across browsers (https://bugsnag.com/blog/firefox-column-numbers)
 *   o Webkit Issue for passing error object to onerror (https://bugs.webkit.org/show_bug.cgi?id=55092)
 *   o Error Object compatibility table(http://blog.errorception.com/2013/02/error-object-compatibility-table.html)
 *
 * Other JS error tracking scripts:
 *   o Tracekit (https://github.com/occ/TraceKit), a slightly modified version is used by New Relic (see APM-18504)
 *   o BugSnag (https://github.com/bugsnag/bugsnag-js)
 *   o Track.js (see APM-20873)
 *   o Errorception.com
 *   o Cross domain script errors (/https://bugsnag.com/blog/script-error)
 *
 * Misc Resources:
 *   o JS Stacktraces: The good, the bad and the ugly (https://bugsnag.com/blog/js-stacktraces)
 *   o W3C ErrorEvent Spec (http://www.w3.org/html/wg/drafts/html/master/webappapis.html#the-errorevent-interface)
 *   o IE 10 i18n Error Strings (https://github.com/errorception/ie-error-languages/tree/master/error-strings)
 *   o http://stackoverflow.com/questions/1382107/whats-a-good-way-to-extend-error-in-javascript
 *   o The Javascript StackTrace blog post http://tobyho.com/2011/06/08/the-javascript-stacktrace-blog/
 *
 */

import { windowObj } from "../../shared/environment";
import { DtError } from "../../shared/types/extended-dom-types";
import { LogLevel } from "../../shared/types";
import { addEventListener } from '../../recorder/core/events';
import { removePropertyListener, addPropertyListener } from '../../recorder/core/utils';
import { getBrowserInfo } from '../../recorder/core/browserdetect';
import { reportError } from './sendError';
function q_log(msg: string, level: LogLevel) { // #NO_DEBUG
    console.log('log : ', msg, level);
}
/**
 * function name constant
 *
 * @const
 * @private
 */
const FUNCTION_NAME_ADDEVENTLISTENER = "addEventListener";

/**
 * function name constant
 *
 * @const
 * @private
 */
const FUNCTION_NAME_REMOVEEVENTLISTENER = "removeEventListener";

/**
 * reference to core dynatrace agent object
 * @name dt
 * @constant
 * @private
 */
let dt: any = {

};

interface DtEventListenerObject extends EventListenerObject {
    'srewrap'?: (evt: Event) => void;
}
interface DtEventListener extends EventListener {
    'srewrap'?: (evt: Event) => void;
}
type DtEventListenerFnType = (event: string, fn: DtEventListenerObject, capture?: boolean, secure?: boolean) => EventListener;
type DtErrorEventHandler = (message: string, filename?: string, lineno?: number, colno?: number, error?: DtError) => boolean;
type DtWrappedEventFunction = DtEventListener | DtEventListenerObject;
interface DtEventListenerOptions extends EventListenerOptions {
    eventHandler: true;
}

/**
 * Preserved original window.onerror function
 * We need to call this function with the unmodified parameters and also to
 * return its return value to not break anything.
 * @type {?function(...[?])}
 * @private
 */
let oldErrorHandler: DtErrorEventHandler | ErrorEventHandler;

/**
 * This flag indicates if the onErrorCallback is currently running. This avoids
 * possible infinite recursion if the errorhandler has wrapped itself or if the
 * errorhandler has wrapped.
 *
 * Although there are precautions to prevent this situation if possible it can
 * still happen e.g. if there is another js error detection script (Tracekit etc.)
 * and the javascript agent is loaded twice.
 *
 * @type {Boolean|boolean}
 * @private
 */
let errorCallbackActive = false;

/**
 * The JS error detection captures errors both in try/catch blocks wrapped around
 * functions and in onError. Errors detected and reported in try/catch blocks would
 * by default not show up in the browser console. This would make it difficult for
 * developers to reproduce and fix the problems if they get no error message in the
 * browser anymore.
 * Therefore it is necessary to rethrow the error after we caught and reported it
 * in the try/catch block. But in order to avoid reporting it again in the onError block
 * and causing duplicate we need to tell onError to skip reporting this error which is
 * what this variable is used for:
 *   - In the try/catch block we will increase this value by one for each error we rethrow.
 *   - In onError we check if the value is bigger than 0, if yes we skip reporting and
 *     reduce the error number by one.
 *
 * Due to the single threadedness of JavaScript we can be sure that the error processed
 * next by onerror was the one we have rethrown.
 *
 * @type {number}
 * @see ignoreNextOnError
 * @private
 */
let ignoreOnError = 0;

/**
 * Whether the error should be reported in the wrapped function (if possible)
 * or left to bubble up to windows.onerror and be reported there. This is necessary
 * because for some browsers catching and reporting in the wrapped function will
 * ruin the stacktraces.
 *
 * @type {boolean}
 * @private
 */
let shouldCatch = true;

const callbackFunctions = {
    'errorCallback': function (...rest) { // #NO_DEBUG
        return dt['aCF'].apply(this, arguments);
    }
};

/**
 * Event handler for "window.onerror".
 *
 * @function
 * @name onErrorCallback
 * @param {string|null} msg error message
 * @param {string} url file name of js/html file where error occured
 * @param {number=} line line where error occured
 * @param {number=} column column where error occured
 * @param {Object=} error the error object
 * @return {*}
 * @private
 */
function onErrorCallback(msg: string, url: string, line?: number, column?: number, error?: DtError): boolean {

    let originalReturnValue = false;

    if (!ignoreOnError && !errorCallbackActive) {
        errorCallbackActive = true;

        try {
            try {
                if (oldErrorHandler && oldErrorHandler !== onErrorCallback && typeof oldErrorHandler === 'function') {
                    originalReturnValue = (oldErrorHandler as DtErrorEventHandler)(msg, url, line, column, error);
                }
            } catch (e) {
                dt['rex'](e, null, true);
                q_log('## ERROR OCCURED DURING EXECUTION OF ORIGINAL ERROR CALLBACK! ' + e, LogLevel.ERROR); // #DEBUG
            }

            if (!originalReturnValue) {
                reportError(msg, url, line, column, error);
            }
        } catch (e) {
            q_log('## ERROR OCCURED IN ERROR CALLBACK! ' + e, LogLevel.ERROR); // #DEBUG
        }

        errorCallbackActive = false;
    } else {
        q_log('Ignoring error! Callback seems to be active already!', LogLevel.INFO); // #DEBUG
    }

    return originalReturnValue;
}

/**
 * Replaces a function of an object with a new function
 *
 * @name replaceFunction
 * @param {Object} obj the object of which the function should be replaced
 * @param {string} name the name of the function that should be replaced
 * @param {Function} replacementFnGenerator a function that generates the replacement function
 * @private
 */
function replaceFunction<T extends string, R>(obj: { [P in T]: R }, name: T, replacementFnGenerator: (originalFn: R) => R): void {
    let original = obj[name];
    obj[name] = replacementFnGenerator(original);
}

/**
 * This function returns a function that acts like the given functions but
 * captures and reports any js error to the server.
 *
 * If you call wrap twice on the same function, it'll give you back the
 * same wrapped function so double wrapping is impossible. This is required
 * so that removeEventListener does not fail to work.
 *
 * @name wrap
 * @param {Function} originalFn the original function to be wrapped
 * @param {Object=} options
 * @private
 */
function wrap(originalFn: DtWrappedEventFunction): DtWrappedEventFunction {
    try {
        // if the parameter isn't a function do nothing
        if (!isFunction(originalFn)) {
            return originalFn;
        }

        if (!originalFn.srewrap) {
            originalFn.srewrap = function (): void { // #NO_DEBUG

                // We set shouldCatch to false on IE < 10 because catching the error ruins the file/line as reported in window.onerror,
                // We set shouldCatch to false on Chrome/Safari because it interferes with "break on unhandled exception"
                // All other browsers need shouldCatch to be true, as they don't pass the exception object to window.onerror
                if (shouldCatch) {
                    try {
                        if (isFunction(originalFn)) {
                            return callbackFunctions['errorCallback'](originalFn, this, arguments);
                        }
                    } catch (e) {
                        if (e['number'] !== -2146823277) {      // = The error number for 5011, "Can't execute code from a freed script".
                            // We do this rather than stashing treating the error like lastEvent
                            // because in FF 26 onerror is not called for synthesized event handlers.
                            dt['rex'](e, null, true);

                            // as we throw the exception again we should not catch it again in window.onerror
                            ignoreNextOnError();
                            throw e;
                        }
                    }
                } else {
                    // Ignore error -2146823277 (a.k.a. 5011), "Can't execute code from a freed script", which occurs with removed iframes.
                    try {
                        if (isFunction(originalFn)) {
                            return callbackFunctions['errorCallback'](originalFn, this, arguments);
                        }
                    } catch (e) {
                        if (e['number'] !== -2146823277) {      // = The error number for 5011, "Can't execute code from a freed script".
                            throw e;
                        }
                    }
                }
            };
            // prevent wrapper function from being wrapped itself
            (originalFn.srewrap as DtWrappedEventFunction).srewrap = originalFn.srewrap;
        }
        return originalFn.srewrap;
    } catch (e) {
        // This can happen if the originalFn is not a normal javascript function.
        return originalFn;
    }
}

/**
 * If we've already reported an error in wrap, we don't want to
 * re-notify when it hits window.onerror after we re-throw it.
 */
function ignoreNextOnError(): void {
    ignoreOnError += 1;
    dt['st'](function () {
        ignoreOnError -= 1;
    }, 0);
}

/**
 * Utility function to test if an object is an executable function
 * @param x {Object}
 * @returns {boolean}
 */
function isFunction(x: object): boolean {
    return Object.prototype.toString.call(x) === '[object Function]';
}

/**
 * Initialisation function
 *
 * @name initialiseErrorHandler
 * @function
 * @private
 * @suppress {missingProperties}
 */
function initialiseErrorHandler(): void { // #NO_DEBUG

    // register onerror handler
    if (windowObj.onerror !== onErrorCallback) {
        // q_log('Registering error callback...', LogLevel.INFO); // #DEBUG
        dt['srehandle'] = onErrorCallback;
        if (windowObj.onerror) {
            oldErrorHandler = windowObj.onerror;
        }
        windowObj.onerror = dt['srehandle'];
    }

    // register property change listener to prevent overwriting of onerror handler
    try {
        // not working for safari and firefox < 11
        //  ==>  onerror is a non-configurable property
        addPropertyListener(windowObj, "onerror",
            function () { // #NO_DEBUG
                return onErrorCallback;
            },
            function (value) { // #NO_DEBUG
                oldErrorHandler = value;
            }
        );
    } catch (e) {
        console.error(" onerror is non-configurable" + e, LogLevel.ERROR); // #DEBUG
    }

    // register cleanup
    addEventListener(windowObj, 'unload', function () {
        removePropertyListener(windowObj, "onerror");
        windowObj.onerror = null;
    });

    // Disable catching on IE < 10 as it destroys stack-traces from generateStackTrace()
    if (!windowObj.atob) {
        shouldCatch = false;
    } else if (windowObj.ErrorEvent) {
        try {
            // Disable catching on browsers that support HTML5 ErrorEvents properly.
            // This lets debug on unhandled exceptions work.
            if (new windowObj.ErrorEvent("test").colno === 0) {
                shouldCatch = false;
            }
        } catch (e) { }
    }
    if (getBrowserInfo().msf) {
        shouldCatch = true;
    }
    // only wrap if we cannot get a stacktrace in the onerror handler
    // this will result in different stacktraces for different browsers, however
    // we will deal with that on the server side.
    if (shouldCatch) {
        /*
         * Initialisation of advanced stacktrace capturing
         *
         * Wrapping EventTargets addEventListener function is all that's required in modern chrome/opera
         * Wrapping EventTarget + Window + ModalWindow addEventListener function is all that's required in modern FF (ignoring Moz prefixed values)
         *
         * Wrapping of other Objects is required for other browsers but this is omitted for now as the majority of events will already be caught
         * and we want to keep things simple for the start.
         */

        // do not wrap for selenium, causes conflict with at least FirefoxDriver and some versions of Firefox
        let htmlTag = document.getElementsByTagName("html");
        if (htmlTag.length === 0 || (htmlTag.length > 0 && !htmlTag[0].hasAttribute) || (htmlTag.length > 0 && !htmlTag[0].hasAttribute("webdriver"))) {
            "EventTarget Window ModalWindow".replace(/\w+/g, function (global: string): string {
                let prototype = (windowObj as any)[global] && (windowObj as any)[global].prototype;
                if (prototype && prototype.hasOwnProperty && prototype.hasOwnProperty(FUNCTION_NAME_ADDEVENTLISTENER)) {
                    replaceFunction(prototype, FUNCTION_NAME_ADDEVENTLISTENER, function (originalFn: DtEventListenerFnType) {
                        return function (event: string, fn: DtEventListenerObject, capture?: boolean, secure?: boolean) { // #NO_DEBUG
                            // HTML lets event-handlers be objects with a handleEvent function,
                            // we need to change f.handleEvent here, as self.wrap will ignore f.
                            if (fn && fn.handleEvent) {
                                fn.handleEvent = wrap(fn.handleEvent) as EventListener;
                            }
                            return originalFn.call(this, event, wrap(fn), capture, secure);
                        };
                    });

                    // We also need to hack removeEventListener so that you can remove any event listeners.
                    replaceFunction(prototype, FUNCTION_NAME_REMOVEEVENTLISTENER, function (originalFn: DtEventListenerFnType) {
                        return function (event: string, fn: DtEventListenerObject, capture?: boolean, secure?: boolean) {  // #NO_DEBUG
                            originalFn.call(this, event, fn, capture, secure);
                            return originalFn.call(this, event, wrap(fn), capture, secure);
                        };
                    });
                }
                return null;
            });
        }
    }
}

// #DEBUG:BEGIN
/**
 * Helper function for creating consistent log statements
 *
 * @name f_log
 * @function
 * @private
 * @param {String} msg the message to log
 * @param {LogLevel} level the message to log
 */

// #DEBUG:END

export function initErrorHandlerModule(): void {

    initialiseErrorHandler();
}
