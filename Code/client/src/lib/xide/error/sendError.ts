
let docObj = document as DtDocument;
/**
 * Detects if the current browser is an IE version running in emulation mode and returns that information
 *
 * @function
 * @private
 * @returns {*}
 */
function getEmulationModeInfo(): string {
    let bi = getBrowserInfo();
    let ieVersion = bi['ie'];
    let documentMode = docObj.documentMode;
    if (bi['ie']) {
        let tridentVersion = (bi['trident'] || 0) + 4;
        if (ieVersion !== tridentVersion) {
            return tridentVersion + '_as_' + ieVersion;
        } else if (documentMode && documentMode !== ieVersion) {
            return tridentVersion + '_as_' + documentMode;
        }
    }
    return '';
}
/**
 * Separator character used to separate values in strings sent to the server
 *
 * @const
 * @private
 */
let PIPE = "|";

function buildDebugInformationForParameter(name: string, param: string | number | Error): string {
    let debugInformation = "";
    debugInformation += name + PIPE;
    let type = typeof param;
    debugInformation += type + PIPE;
    if (param === null) {
        debugInformation += "null" + PIPE;
    } else if (type === 'object') {
        let errorParam = param as Error;
        for (let prop in errorParam) {
            if (param.hasOwnProperty(prop) && prop !== 'stack' && prop !== 'error') {
                let key = prop as keyof Error;
                debugInformation += prop + PIPE;
                let propType = typeof errorParam[key];
                debugInformation += propType + PIPE;
                if (propType !== 'object' && propType !== 'function') {
                    debugInformation += errorParam[key] + PIPE;
                }
            }
        }
    } else {
        debugInformation += param + PIPE;
    }
    return debugInformation;
}

/**
 * builds debug information for errorhandler debugging
 * @param {string} msg
 * @param {string} file
 * @param {number} line
 * @param {number} column
 * @param {object} error
 * @returns {string}
 */
function buildDebugInformation(msg: string, file: string, line: number, column: number, error: Error): string {
    let debugInformation = buildDebugInformationForParameter("msg", msg);
    debugInformation += buildDebugInformationForParameter("file", file);
    debugInformation += buildDebugInformationForParameter("line", line);
    debugInformation += buildDebugInformationForParameter("column", column);
    debugInformation += buildDebugInformationForParameter("error", error);

    return debugInformation;
}

/**
 * Default error name if no other error name could be determined
 *
 * @const
 * @private
 */
let FALLBACK_ERROR_NAME = "Indeterminable error name";
/**
 * maximum number of frames in fake, generated stacktraces
 *
 * @const
 * @private
 */
let MAX_FAKE_STACK_SIZE = 10;

/**
 * maximum number of characters in the escaped stacktrace
 * @type {number}
 */
let MAX_STACK_STRING_LENGTH = 1200;

/**
 * Regex to parse a function name from a string
 *
 * @const
 * @private
 */
let FUNCTION_REGEX = /function\s*([\w\-$]+)?\s*\(/i;

/**
 * Placeholder for anonymous action names
 *
 * @const
 * @private
 */
let ANONYMOUS_FUNCTION_PLACEHOLDER = "[a]";

/**
 * Generate a browser stacktrace (or approximation) from the current stack.
 * This is used to add a stacktrace to reported javascript errors calls, and to add a
 * stacktrace approximation where we can't get one from an exception.
 *
 * The so generated stack traces currently have two extra lines, depending on
 * how they were created:
 *   - generateStacktrace() + window.onerror
 */
function generateStacktrace(): string {
    // Try to generate a real stacktrace (most browsers, except IE9 and below).
    try {
        //noinspection ExceptionCaughtLocallyJS - intended!
        throw new Error("");
    } catch (exception) {
        let stackTrace = getStacktraceFromException(exception);
        if (stackTrace && stackTrace.split(/\r\n|\r|\n/).length > 4) {
            return "<generated>\n" + stackTrace;
        }
    }

    // Otherwise, build a fake stacktrace from the list of method names by
    // looping through the list of functions that called this one (and skip
    // whoever called us).
    let ieVersion = getBrowserInfo()['ie'];
    if (ieVersion && ieVersion < 9) {
        let fnStack: string[] = [];
        let cnt = 0;
        try {
            /** @suppress {es5Strict} */
            // tslint:disable-next-line:no-arg
            let curr = arguments['callee']['caller']['caller'];
            while (curr && fnStack.length < MAX_FAKE_STACK_SIZE) {
                let regexMatch = FUNCTION_REGEX.exec(curr.toString());
                let fn = regexMatch ? (regexMatch[1] || ANONYMOUS_FUNCTION_PLACEHOLDER) : ANONYMOUS_FUNCTION_PLACEHOLDER;
                fnStack.push(fn);
                curr = curr.caller;
                cnt++;
            }
        } catch (e) {
            console.error(e, LogLevel.ERROR);
        }
        if (cnt > 3) {
            return "<generated-ie>\n" + fnStack.join("\n");
        }
    }
    return '';
}

/**
 * Retrieves stacktraces from an exception object, even if not
 * the default property names are used.
 *
 * @private
 * @function
 * @param exception
 * @returns
 */
function getStacktraceFromException(exception: DtError): string {
    if (!exception) {
        return '';
    }
    return exception.stack
        || exception.backtrace
        || exception.stacktrace
        || (exception.error && exception.error.stack)
        || '';
}

interface ErrorUserInput {
    name: string;
    type: string;
}
/**
 * Number of errors thrown in the time frame relevant. If this count is equal or
 * bigger than maxErrorPerPage then errors are no longer going to be reported to
 * the server.
 *
 * Starts with a value of 0. Every reported error will increase this counter
 * by one. Over time the counter will also gradually reduce itself whenever
 * a certain amount of time has passed. Currently this is set to reduce itself by
 * one for every 30 seconds passed.
 * @see search for "Reduce error count" to find the reset script
 * @type {number}
 * @private
 */
let errorsThrown = 0;
/**
 * Used for post error (ADK method), so post error can't spam error messages
 * @public
 * @function
 * @returns {number}
 */
export function getNumErrorsThrown(): number {
    return errorsThrown;
}
/**
 * Used by post error (ADK method) to increment the counter of already thrown
 * errors.
 * @function
 * @public
 *
 */
export function incrementErrorsThrown(): void {
    errorsThrown++;
}
/**
 * Makes the stack trace shorter
 * (for now simply removes the last frames but in the future this might be
 * replaced with a more advanced implementation.
 *
 * @function
 * @private
 * @param {String} stack the stacktrace of the error
 */
function shortenStack(stack: string): string {
    let lines = stack.split(/(\r\n|\n|\r)/gm);
    let result = '';
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i].trim();
        if (line.length > 0) {
            if (line.length > 250) {
                result += line.substring(0, 150) + '[...]' + line.substring(line.length - 100) + '\n';
            } else {
                result += line + '\n';
            }
        }
        if (result.length > MAX_STACK_STRING_LENGTH) {
            return result;
        }
    }
    return result;
}
/**
 * Reports an error to the server
 *
 * @name re
 * @param {String|string|null} msg error message
 * @param {string|null} file the file where the error occurred
 * @param {number|null=} line line where error occured
 * @param {number|null=} column column where error occured
 * @param {Error|string} [error] the error object
 * @param {number|null=} parentActionId parent action id
 * @param {boolean} [stackContainsWrapper] if a line in the stacktrace contains the wrapper function
 * @private
 */
export function reportError(msg: string, file: string, line: number, column: number, error: DtError | string, parentActionId?: number, stackContainsWrapper?: boolean): void {
    if ((msg || error) && getNumErrorsThrown() + 1 <= 30) {
        // increase number of reported errors for this page
        incrementErrorsThrown();
        let err: DtError = { message: '', name: '' };

        // normalize variables and create default values which will save us from doing a lot
        // of checks later on.
        if (typeof error === 'string') {
            msg = msg || error;
        } else {
            err = error || windowObj.event as any || err;
        }
        let userInput = getCurrentUserInput();
        let errorUserInput: ErrorUserInput | undefined;
        if (userInput) {
            errorUserInput = {
                name: userInput.getName(),
                type: userInput.type
            };
        }
        // compile all information about the error in a single object; this takes care
        // about different property names/used variables across different browsers
        let info: BeaconParamError = {
            message: err.message || err.name || err.msg || err.description || msg || err.errorMessage || err.Mb || err.data || err.bc || FALLBACK_ERROR_NAME,
            file: err.fileName || err.filename || err.sourceURL || err.errUrl || err.file || file || '',
            line: err.lineNumber || err.lineno || err.line || err.errorLine || line || -1,
            column: (err.columnNumber ? err.columnNumber + 1 : void 0) || err.errorCharacter || err.colno || err.column || column || -1, // #NOWARN
            stack: getStacktraceFromException(err) || generateStacktrace(),
            userInput: errorUserInput,
            code: err.number || err.code || err.errorCode || err.status!,
            timestamp: now() - getAgentStartTime(), // #NOWARN
            emulationMode: getEmulationModeInfo(),
            debugInfo: ""
        };

        if (info.message === FALLBACK_ERROR_NAME) {
            info.debugInfo = buildDebugInformation(msg, file, line, column, err);
            console.log('Sending debug information"' + info.debugInfo + '"'); // #DEBUG
        }

        if (info.stack && stackContainsWrapper) {
            info.stack = "<wrapper>" + info.stack;
        }

        if (info.stack) {
            if (info.stack.length > MAX_STACK_STRING_LENGTH) {
                info.stack = shortenStack(info.stack);
            }
            info.stack = info.stack.replace(PIPE, '^p').replace(/(\r\n|\n|\r)/gm, PIPE);
        }

        // error 5011 = "Can't execute code from a freed script", which seems to be caused when our wrapper holds
        // a reference to a no longer existing function. We should not report that problem ...
        if (info.code === 5011) {
            return;
        }

        // create main error node
        if (info.message.length > 300) {
            console.warn('Truncating message because full message is longer than 300 characters'); // #DEBUG
            info.message = info.message.substring(0, 300);
        }

        // let header = `${6},${now()},${now()},${now()}`;
        let ts = now();
        sendSpecificBeacon(createBeacon(`${JSON.stringify(events)}`), EVENTS_PAYLOAD_TYPE);
    }
}
