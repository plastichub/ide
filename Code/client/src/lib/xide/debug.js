define([
    'xdojo/declare',
    'dojo/has',
    'xdojo/has!debug?xide/serverDebug'
],function(declare,has,serverDebug){
	const Module = declare("xide.debug", null,{});

    if(has('debug')) {

        function displayServerDebugData(data){
            serverDebug && serverDebug.logError(data);
        }

        window.xappServerDebug = displayServerDebugData;

        const callback = function (stackframes) {
            const stringifiedStack = stackframes.map(function (sf) {
                return sf.toString();
            }).join('\n');
            console.log(stringifiedStack);
        };

        const errback = function (err) {
            console.log(err.message);
        };


        window.onerror = function (msg, file, line, col, error) {
            StackTrace.fromError(error).then(callback)['catch'](errback);
        };
        if (!window.logError) {
            window.logError = function (e, message) {
                console.error((message || '') + ' :: ' + e.message + ' stack:\n', e);
                if (typeof 'StackTracke' === 'undefined') {
                    let stack = e.stack;
                    stack = stack.split('\n').map(function (line) {
                        return line.trim();
                    });
                    stack.splice(stack[0] == 'Error' ? 2 : 1);
                    console.log(stack.join('\n'));
                } else {
                    StackTrace.fromError(e).then(callback)['catch'](errback);

                }
            }
        } else {
            console.log('logError already created');
        }
    }else{
        window.onerror = function (msg, file, line, col, error) {
            console.error(msg, error);
        };
        window.logError = function (e, message) {
            console.error((message || '') + ' :: ' + e.message + ' stack:\n', e);
        }
    }
    return Module;
});