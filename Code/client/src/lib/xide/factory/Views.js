define([
    'xide/factory',
    'xide/utils',
    'xide/types'
], function (factory, utils, types, ContentPane) {
    /**
     * Default pane factory
     * @TODO: trash
     * @param title
     * @param iconClass
     * @param target
     * @param mixins
     * @param cssClass
     * @returns {xide/layout/ContentPane}
     */
    factory.createPane = function(title,iconClass,target,mixins,cssClass,baseClasses,classExtension){};

    /**
     * @TODO: trash
     * @param show
     * @returns {null}
     */
    factory.showStandBy = function (show) {
        const splash = document.getElementById('loadingWrapper');
        if (splash) {
            if (show) {
                splash.style.display = 'inline-block';
            } else {
                splash.style.display = 'none';
            }
        }
        return null;
    };
    /**
     * @TODO: trash
     * @param CIS
     * @param parent
     * @param showAllTab
     * @returns {TabContainer}
     */
    factory.createTabbedSettingsView = function (CIS, parent, showAllTab) {

        // tab container :
        const tabContainer = new xide.layout.TabContainer({
            tabStrip: true,
            tabPosition: "left-h",
            splitter: true,
            style: "min-width:450px;",
            "className": "ui-widget-content"
        });

        parent.domNode.appendChild(tabContainer.domNode);
        tabContainer.startup();


        const widgetTabMap = {};
        const widgetGroups = [];
        if (showAllTab) {
            let dstTabContainerAll = widgetTabMap[types.WidgetGroup.All];
            if (dstTabContainerAll == null) {
                dstTabContainerAll = new ContentPane({
                    title: "All",
                    style: "padding:5px;",
                    closeable: false
                });
                widgetTabMap[types.WidgetGroup.All] = dstTabContainerAll;
                dstTabContainerAll.startup();
                tabContainer.addChild(dstTabContainerAll);
            }
        }


        let widgetGroup = null;
        for (let i = 0; i < CIS.length; i++) {
            const ci = CIS[i];

            let groupTitle = utils.toString(ci['group']) || null;
            if (groupTitle === '-1') {
                groupTitle = null;
            }

            if (groupTitle == null) {
                widgetGroup = utils.getWidgetGroup(ci);
                if (widgetGroup == types.WidgetGroup.Hidden)
                    continue;

                groupTitle = utils.getWidgetGroupTitle(widgetGroup);
            } else {
                widgetGroup = groupTitle;
            }
            let dstTabContainer = widgetTabMap[widgetGroup];
            if (dstTabContainer == null ) {
                widgetGroups.push(widgetGroup);
                dstTabContainer = new ContentPane({
                    title: groupTitle,
                    preload: false,
                    closeable: false,
                    adjustChildsToWidth: true
                });
                widgetTabMap[widgetGroup] = dstTabContainer;
            }
        }

        // sort by widget group
        widgetGroups.sort(function (a, b) {
            return a - b;
        });

        //attach widget containers
        for (let wgi = 0; wgi < widgetGroups.length; wgi++) {

            widgetGroup = widgetGroups[wgi];
            const container = widgetTabMap[widgetGroup];
            if(container) {
                container.startup();
                tabContainer.addChild(container);
            }
        }
        tabContainer["widgetTabMap"] = widgetTabMap;
        return tabContainer;
    };
});
