define([
    'xide/factory',
    'dojo/_base/connect',
    'dojo/_base/lang',
    "dojo/on",
    'dojo/has',
    'xide/lodash'
], function (factory, connect, lang, on, has) {
    const //print publish messages in console
        _debug = false;

    const //put publish in try/catch block
        _tryEvents = false;

    const //noop
        _foo = null;

    const _nativeEvents = {
        "click": _foo,
        "dblclick": _foo,
        "mousedown": _foo,
        "mouseup": _foo,
        "mouseover": _foo,
        "mousemove": _foo,
        "mouseout": _foo,
        "keypress": _foo,
        "keydown": _foo,
        "keyup": _foo,
        "focus": _foo,
        "blur": _foo,
        "change": _foo
    };

    let _debugGroup = false;


    /**
     * Returns true if it is a DOM element, might be not needed anymore
     * @param o
     * @returns {*}
     * @private
     */
    const _isElement = (o) => {
        return (
            typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
            o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName === "string"
        );
    }
    /**
     * Event debouncer/throttler
     * @param eventHandler
     * @param waitForMirror
     * @returns {Function}
     */
    function applyEventOnce(eventHandler, waitForMirror) {
        let timer;
        const mirror = this;
        return function () {
            const _arguments = arguments;
            if (timer)
                clearTimeout(timer);
            timer = setTimeout(function () {
                if (waitForMirror && mirror.isPending())
                    return setTimeout(function () {
                        applyEventOnce(eventHandler, true)
                    }, 0);
                eventHandler.apply(eventHandler, _arguments);
            }, 0);
        };
    }

    /**
     * asyncForEach does runs a chain of promises, needed this specialized for event callbacks
     * @param array
     * @param fn
     * @param test
     * @param callback
     */
    function asyncForEach(array, fn, test, callback) {
        if (!callback) {
            callback = test;
            test = null;
        }

        array = array.slice(); // copy before use

        let nested = false;
        let callNext = true;
        loop();

        function loop() {
            while (callNext && !nested) {
                callNext = false;
                while (array.length > 0 && test && !test(array[0]))
                    array.shift();

                const item = array.shift();
                // TODO: implement proper err argument?
                if (!item)
                    return callback && callback();

                nested = true;
                fn(item, loop);
                nested = false;
            }
            callNext = true;
        }
    }

    /***
     * @param keys
     * @param data
     * @param callee
     * @extends module:xide/factory
     * @memberOf xide/factory
     */
    factory.publish = function (keys, data, callee, filter) {
        const msgStruct = data ? _.isString(data) ? {
            message: data
        } : data : {}; //lookup cache
        let eventKeys = keys;
        const _publish = connect.publish;
        let result = [];

        //normalize to array
        if (!_.isArray(keys)) {
            eventKeys = [keys];
        }
        for (let i = 0, l = eventKeys.length; i < l; i++) {

            const eventKey = eventKeys[i];

            if (filter && !filter(eventKey)) {
                continue;
            }

            if (_debug) {
                //console.group("Events");
                _debugGroup = true;
                console.log('publish ' + eventKey + ' from : ' + (callee ? callee.id : ''), msgStruct);
            }

            if (_tryEvents) {
                try {
                    result = _publish(eventKey, msgStruct);
                } catch (e) {
                    logError(e, 'error whilst publishing event ' + eventKey);
                }
            } else {
                result = _publish(eventKey, msgStruct);
            }
        }

        return result;
    };

    /***
     *
     * Subscribes to multiple events
     * @param keys {String[]}
     * @param _cb {function|null} When null, it expects the owner having a function matching the event key!
     * @param owner {Object}
     * @extends module:xide/factory
     * @memberOf xide/factory
     * @returns {Object[]|null} Returns an array of regular Dojo-subscribe/on handles
     */
    factory.subscribe = function (keys, cb, owner, filter) {
        if (has('debug')) {
            if (!keys) {
                _debug && console.error('subscribe failed, event key is empty!');
                return null;
            }
        }

        //some vars
        let eventKeys = keys; //resulting subscribe handles
        //_isDom = _isElement(owner),       //dom element?

        const //cache
            _subscribe = connect.subscribe;

        const events = [];

        //normalize to array
        if (!_.isArray(keys)) {
            eventKeys = [keys];
        }

        for (let i = 0, l = eventKeys.length; i < l; i++) {
            if (!eventKeys[i] || filter && !filter(eventKey)) {
                continue;
            }

            const _item =
                //the raw item
                eventKeys[i];

            const //is string?
                _isString = _.isString(_item);

            var //if string: use it, otherwise assume struct
                eventKey = _isString ? _item : _item.key;

            const //pick handler from arguments or struct
                _handler = _isString ? cb : _item.handler;

            const //is native event?
                _isNative = eventKey in _nativeEvents;

            let //the final handle
                _handle;


            //owner specified, hitch the callback into owner's scope
            if (owner != null) {
                //try cb first, then owner.onEVENT_KEY, that enables similar effect as in Dojo2/Evented
                const _cb = _handler != null ? _handler : owner[eventKey];
                if (_isNative) {
                    _handle = on(owner, eventKey, lang.hitch(owner, _cb));
                } else {
                    _handle = _subscribe(eventKey, lang.hitch(owner, _cb));
                }
                _handle.handler = lang.hitch(owner, _cb);
            } else {
                _handle = connect.subscribe(eventKey, _handler);
                _handle.handler = _handler;
            }
            //track the actual event type
            _handle.type = eventKey;
            events.push(_handle);
        }
        return events;
    };
    return factory;
});