define([
    'dcl/dcl',
    'xide/utils',
    'xide/factory',
    'xdojo/declare'
], function (dcl,utils, factory, declare) {
    /***
     * Convinience object factory via dojo/declare
     * @param classNameOrPrototype
     * @param ctrArgs
     * @param baseClasses
     * @returns {*}
     */
    factory.createInstance = function (classNameOrPrototype, ctrArgs, baseClasses) {
        const ctrArgsFinal = {};
        utils.mixin(ctrArgsFinal, ctrArgs);
        //prepare object prototype, try dojo and then abort
        let objectProtoType = classNameOrPrototype;
        if (_.isString(classNameOrPrototype)) {
            const proto = dojo.getObject(objectProtoType) || dcl.getObject(objectProtoType);
            if (proto) {
                objectProtoType = proto;
            } else {
                console.error('no such class : ' + classNameOrPrototype);
                return null;
            }
        }

        baseClasses && ( objectProtoType = declare(baseClasses, objectProtoType.prototype));

        if(!ctrArgsFinal.id){
            const className = objectProtoType.declaredClass || 'no_class_';
            ctrArgsFinal.id = className.replace(/\//g, "_") + utils.createUUID();
        }

        const instance = new objectProtoType(ctrArgsFinal);

        //@TODO: trash
        instance && ( instance.ctrArgs = ctrArgsFinal);

        return instance;
    };
    return factory;
});