/** @module xide/Document **/
define([
    'xdojo/declare',
    "xide/utils",
    "dojo/window",
    "xide/Template",
    "xide/widgets/TemplatedWidgetBase"
], function (declare, utils, windowUtils, Template, TemplatedWidgetBase) {
    /**
     * Widget which wraps a templated document in a iFrame. It resolves variables into the final template.
     * have been loaded.
     *
     * @class module:xide/Document
     * @extends module:xide/widgets/TemplatedWidgetBase
     */
    return declare("xide/Document", [TemplatedWidgetBase], {
        document: null,
        templateString: "<div style='width: inherit; height: inherit'></div>",
        template: '',
        iframeattrs: {
            'class': 'silhouetteiframe_iframe'
        },
        resourceVariables: {},
        variableDelimiters: {
            begin: '{{',
            end: '}}'
        },
        templateArguments: {
            resourceDelegate: null,
            baseUrl: dojo.baseUrl,

            //this.ctx
            ctx: null,
            getDependencies: function () {
                return [];
            },
            resourceVariables: {
                requireUrl: dojo.baseUrl + 'ibm-js/requirejs/require.js'
            },
            bodyStyle: {
                width: "100%",
                height: "100%",
                visibility: "visible",
                margin: "0"
            },
            bootstrapModules: ""
        },
        getDocument: function () {
            return this.document;
        },
        getGlobal: function () {
            const doc = this.getDocument();
            return doc ? windowUtils.get(doc) : null;
        },
        startup: function () {
            const templateInstance = new Template(this.templateArguments);
            const templateContent = templateInstance.resolve(this.template, this.resourceVariables, this.variableDelimiters);
            const frame = utils.create("iframe", this.iframeattrs, this.domNode);

            try {
                const doc = frame.contentDocument || frame.contentWindow.document;
                const thiz = this;

                doc.open();
                doc.write(templateContent);
                doc.close();
                doc.onreadystatechange = function (state) {
                    try {
                        var state = doc.readyState;
                        if (state == 'interactive') {
                        } else if (state == 'complete') {
                            thiz._emit('ready', {
                                doc: doc,
                                global: thiz.getGlobal()
                            });
                        }
                    }catch(e){
                        console.error(e);
                    }

                };
                this.document = doc;
            } catch (e) {
                console.error('error creating document ' + e, e);
            }
        }
    });
});