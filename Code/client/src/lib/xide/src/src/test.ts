
import Evented from "./Evented";
import ContextBase from "./manager/ContextBase";
import * as compose from 'dojo-compose/compose';
import * as $ from 'jquery';
import { Handle, EventObject } from 'dojo-interfaces/core';
interface test{
    test:void;
}

class test2x{
    x:number;
    constructor(x: number, public y: number = 0) {
    this.x = x;
  }
}
const Evented2 = Evented.extend({});
function test(a:number){
    return Math.abs(a);
}

var isDone:boolean = false;
var lines:number = 42;
const name:string = "asd2f";
let list2 :number[] = [1,2,3];

enum Test {
    ONE=<any>'sd2'
};

var f1 = function(i: number): number { 
    return i * i; 
}
// Return type inferred
var f2 = function(i: number) { return i * i; }
// "Fat arrow" syntax
var f3 = (i: number): number => { return i * i; }
// "Fat arrow" syntax with return type inferred
var f4 = (i: number) => { return i * i; }
// "Fat arrow" syntax with return type inferred, braceless means no return
// keyword needed
var f5 = (i: number) =>  i * i;

f5(2);

////////////////////////////////////////////////////////


// Interfaces are structural, anything that has the properties is compliant with
// the interface
interface Person {
  name: string;
  // Optional properties, marked with a "?"
  age?: number;
  // And of course functions
  move(): void;
}

let propName: keyof Person;


// Object that implements the "Person" interface
// Can be treated as a Person since it has the name and move properties
var p: Person = { name: "Bobby", move: () => {} };

// Objects that have the optional property:
var validPerson: Person = { name: "Bobby", age: 42, move: () => {} };
// Is not a person because age is not a number
var invalidPerson: Person = { name: "Bobby", age: 20,move:function(){} };



// Interfaces can also describe a function type
interface SearchFunc {
  (source: string, subString: string): boolean;
}
// Only the parameters' types are important, names are not important.
var mySearch: SearchFunc;
mySearch = function(src: string, sub: string) {
  return src.search(sub) != -1;
}


// Generics
// Classes
class Tuple<T1, T2> {
  constructor(public item1: T1, public item2: T2) {
  }
}

// Interfaces
interface Pair<T> {
  item1: T;
  item2: T;
}

// And functions
var pairToTuple = function<T>(p: Pair<T>) {
  return new Tuple(p.item1, p.item2);
};


interface A { a: number }  
interface B { b: number }

var ab: A & B = { a: 1, b: 1 };  
var a: A = ab;  // A & B assignable to A  
var b: B = ab;  // A & B assignable to B

