define(["require", "exports", "./Evented"], function (require, exports, Evented_1) {
    "use strict";
    class test2x {
        constructor(x, y = 0) {
            this.y = y;
            this.x = x;
        }
    }
    const Evented2 = Evented_1.default.extend({});
    function test(a) {
        return Math.abs(a);
    }
    const isDone = false;
    const lines = 42;
    const name = "asd2f";
    let list2 = [1, 2, 3];
    let Test;
    (function (Test) {
        Test[Test["ONE"] = 'sd2'] = "ONE";
    })(Test || (Test = {}));
    ;
    const f1 = function (i) {
        return i * i;
    };
    // Return type inferred
    const f2 = function (i) { return i * i; };
    // "Fat arrow" syntax
    const f3 = (i) => { return i * i; };
    // "Fat arrow" syntax with return type inferred
    const f4 = (i) => { return i * i; };
    // "Fat arrow" syntax with return type inferred, braceless means no return
    // keyword needed
    const f5 = (i) => i * i;
    f5(2);
    let propName;
    // Object that implements the "Person" interface
    // Can be treated as a Person since it has the name and move properties
    const p = { name: "Bobby", move: () => { } };
    // Objects that have the optional property:
    const validPerson = { name: "Bobby", age: 42, move: () => { } };
    // Is not a person because age is not a number
    const invalidPerson = { name: "Bobby", age: 20, move: function () { } };
    // Only the parameters' types are important, names are not important.
    let mySearch;
    mySearch = function (src, sub) {
        return src.search(sub) != -1;
    };
    // Generics
    // Classes
    class Tuple {
        constructor(item1, item2) {
            this.item1 = item1;
            this.item2 = item2;
        }
    }
    // And functions
    const pairToTuple = function (p) {
        return new Tuple(p.item1, p.item2);
    };
    const ab = { a: 1, b: 1 };
    const a = ab; // A & B assignable to A  
    const b = ab; // A & B assignable to B
});
