import {SyncEvent} from 'ts-events/src/lib/sync-event';
const evtChange = new SyncEvent<string>();
evtChange.attach(function(s) {
    console.log(s);
});
evtChange.post('hi!');

