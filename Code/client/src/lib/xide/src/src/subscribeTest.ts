import { Handle, EventObject } from 'dojo-interfaces/core';
import { on } from 'dojo-core/aspect';
import { EventEmitter,EventCallback, emit } from 'dojo-core/on';
import * as Evt from 'dojo-core/Evented';



class hub implements EventEmitter {
    //full fill EventEmitter
    on(event: string, listener: EventCallback): EventEmitter { return this; }
    //full fill EventEmitter
    removeListener(event: string, listener: EventCallback): EventEmitter { return this; }

    // method for aspect/on
    test(): void { }

    subscribe(type: string, method: string): Handle {
        return on(this, method, function () { });
    }
    publish(type: string, args: EventObject) {
        return emit(this, args);
    }
    
}

interface EventHandlesPerType{    
    type:string;
    handlers:Array<EventCallback>
}
export interface Postable<T> {
    post(data: T): void;
}

interface IEventedMixin {
    /**
     * @param  {string} type Event type
     * @param  {EventCallback} handler The callback function
     * @param  {EventTarget} target? Override handler scope, defaults to this!
     * @returns Handle
     */
    subscribe(type: string, handler: EventCallback, target?: EventTarget): Handle;
    /**
     * @param  {string} type Event type
     * @param  {EventObject} args Event object
     * @returns Array All the listeners results
     */
    publish(type: string, args: EventObject): Array<any>;
    
    /////////////////////////////////////////////////////////////
    // lifecycle    
    
    /**
     * @param  {string} type?
     */
    _destroyHandles(type?:string);
    
    /////////////////////////////////////////////////////////////
    // sweats
    /**
     * Unregister from a global event.
     * @param  {} type
     * @param  {EventCallback|null} handle? If null: unsubscribe all handlers, otherwise only the specifc one
     * @returns boolean
     */
    off(type, handler: EventCallback | null): boolean;
    
    /**
     * Return listeners per event type
     * @param  {string} type
     * @returns Array<EventCallback>
     */
    listener(type: string): Array<EventCallback>;

    ////////////////////////////////////////////////////////////
    // properties
    /**
     * @TODO: 
     * - where did dojo/topic registered the event listeners? : looks like document
     * - figure out a way to use top level objects like "ApplicationContext"
     * - apply delegation
     */
    scope: HTMLElement | EventEmitter | EventTarget;

    //our collected handles per event type
    __handles:EventHandlesPerType;
}
class Evented{
    listeners: { [eventName: string]: Map<Function, number> } = {}
    on(event:string,callback: (data: T) => void){
        if (this.listeners[event] === undefined)
            this.listeners[event] = new Map<Function, number>()
        const callbacks = this.listeners[event]
        callbacks.set(callback, Infinity)
    }
}
