/** @module xide/types
 *  @description All the package's constants and enums in C style structures.
 */
define([
    'dojo/_base/lang',
    'xide/types',
    'dojo/_base/json',
    'dojo/_base/kernel',
    'xide/utils',
    'xide/utils/ObjectUtils'
], function (lang, types, json, dojo, utils) {
    /**
     * @TODO:
     * - apply xide/registry for types
     * - move mime handling to xfile
     * - remove ui types
     * - remove all other things which are part of ui or server only
     */
    /**
     * Custom CI Types, see ECITYPE enumeration. Each enum is mapped to a widget.
     */
    if (types['customTypes'] == null) {
        types['customTypes'] = {};
    }
    /**
     * ECTYPE_ENUM is mapped to and label-value option array
     */
    if (types['customEnumerations'] == null) {
        types['customEnumerations'] = {};
    }
    /**
     * The actual mapping of custom types to widget proto classes
     */
    if (types['widgetMappings'] == null) {
        types['widgetMappings'] = {};
    }
    /**
     * Mixes in new mime icons per ECITYPE & file extensions. Rendered by FontAwesome
     */
    if (types['customMimeIcons'] == null) {
        types['customMimeIcons'] = {};
    }
    /**
     * CI Render callbacks
     */
    if (types['CICallbacks'] == null) {
        types['CICallbacks'] = {};
    }
    /**
     * Public ECI_TYPE registry getter
     * @param type
     * @returns {*}
     */
    types.resolveType = function (type) {
        if (types['customTypes'][type]) {
            return types['customTypes'][type];
        }
        return null;
    };
    /**
     * Public ECI_TYPE registry setter
     * @param type
     * @param map
     */
    types.registerType = function (type, map) {
        types['customTypes'][type] = map;
    };
    /**
     * Public widget-type registry setter
     * @param type
     * @param map
     */
    types.registerWidgetMapping = function (type, map) {
        types['widgetMappings'][type] = map;
    };
    /**
     * Public custom enum registry setter
     * @param type
     * @param map
     */
    types.registerEnumeration = function (type, map) {
        types['customEnumerations'][type] = map;
    };
    /**
     * Public custom enumeration registry getter
     * @param type
     */
    types.resolveEnumeration = function (type) {
        if (types['customEnumerations'][type]) {
            return types['customEnumerations'][type];
        }
        return null;
    };
    /**
     * Public type-widget mapping registry setter
     * @param type
     */
    types.resolveWidgetMapping = function (type) {
        if (types['widgetMappings'][type]) {
            return types['widgetMappings'][type];
        }
        return null;
    };

    types.registerCICallbacks = function (type, callbacks) {
        if (!types['CICallbacks'][type]) {
            types['CICallbacks'][type] = {}
        }
        utils.mixin(types['CICallbacks'][type], callbacks);
        return null;
    };
    types.getCICallbacks = function (type) {
        if (types['CICallbacks'][type]) {
            return types['CICallbacks'][type];
        }
        return null;
    };

    types.GRID_FEATURE = {
        KEYBOARD_NAVIGATION: 'KEYBOARD_NAVIGATION',
        KEYBOARD_SELECT: 'KEYBOARD_SELECT',
        SELECTION: 'SELECTION',
        ACTIONS: 'ACTIONS',
        CONTEXT_MENU: 'CONTEXT_MENU'
    };

    types.VIEW_FEATURE = {
        KEYBOARD_NAVIGATION: 'KEYBOARD_NAVIGATION',
        KEYBOARD_SELECT: 'KEYBOARD_SELECT',
        SELECTION: 'SELECTION',
        ACTIONS: 'ACTIONS',
        CONTEXT_MENU: 'CONTEXT_MENU'
    };

    types.KEYBOARD_PROFILE = {
        DEFAULT: {
            prevent_default: true,
            prevent_repeat: false
        },
        PASS_THROUGH: {
            prevent_default: false,
            prevent_repeat: false
        },
        SEQUENCE: {
            prevent_default: true,
            is_sequence: true,
            prevent_repeat: true
        }
    };
    /////////////////////////////////////////////////////////////////////////////
    //
    // CORE TYPES
    //
    /////////////////////////////////////////////////////////////////////////////
    /**
     * A 'Configurable Information's ("CI") processing state during post or pre-processing.
     *
     * @enum {int} module:xide/types/CI_STATE
     * @memberOf module:xide/types
     */
    types.CI_STATE = {
        /**
         * Nothing done, could also mean there is nothing to do all
         * @constant
         * @type int
         */
        NONE: 0x00000000,
        /**
         * In pending state. At that time the compiler has accepted additional work and ci flag processing is queued
         * but not scheduled yet.
         * @constant
         * @type int
         */
        PENDING: 0x00000001,
        /**
         * The processing state.
         * @constant
         * @type int
         */
        PROCESSING: 0x00000002,
        /**
         * The CI has been processed but it failed.
         * @constant
         * @type int
         */
        FAILED: 0x00000004,
        /**
         * The CI was successfully processed.
         * @constant
         * @type int
         */
        SUCCESSED: 0x00000008,
        /**
         * The CI has been processed.
         * @constant
         * @type int
         */
        PROCESSED: 0x00000010,
        /**
         * The CI left the post/pre processor entirly but has not been accepted by the originating source.
         * This state can happen when the source became invalid and so its sort of orphan.
         * @constant
         * @type int
         */
        DEQUEUED: 0x00000020,
        /**
         * The CI fully resolved and no references except by the source are around.
         * @constant
         * @type int
         */
        SOLVED: 0x00000040,
        /**
         * Flag to mark the core's end of this bitmask, from here its user land
         * @constant
         * @type int
         */
        END: 0x00000080
    };
    /**
     * A 'Configurable Information's ("CI") type flags for post and pre-processing a value.
     * @enum {string} CIFLAGS
     * @global
     * @memberOf module:xide/types
     */
    types.CIFLAG = {
        /**
         * Instruct for no additional extra processing
         * @constant
         * @type int
         */
        NONE: 0x00000000,
        /**
         * Will instruct the pre/post processor to base-64 decode or encode
         * @constant
         * @type int
         */
        BASE_64: 0x00000001,
        /**
         * Post/Pre process the value with a user function
         * @constant
         * @type int
         */
        USE_FUNCTION: 0x00000002,
        /**
         * Replace variables with local scope's variables during the post/pre process
         * @constant
         * @type int
         */
        REPLACE_VARIABLES: 0x00000004,
        /**
         * Replace variables with local scope's variables during the post/pre process but evaluate the whole string
         * as Javascript
         * @constant
         * @type int
         */
        REPLACE_VARIABLES_EVALUATED: 0x00000008,
        /**
         * Will instruct the pre/post processor to escpape evaluated or replaced variables or expressions
         * @constant
         * @type int
         */
        ESCAPE: 0x00000010,
        /**
         * Will instruct the pre/post processor to replace block calls with oridinary vanilla script
         * @constant
         * @type int
         */
        REPLACE_BLOCK_CALLS: 0x00000020,
        /**
         * Will instruct the pre/post processor to remove variable delimitters/placeholders from the final string
         * @constant
         * @type int
         */
        REMOVE_DELIMTTERS: 0x00000040,
        /**
         * Will instruct the pre/post processor to remove   "[" ,"]" , "(" , ")" , "{", "}" , "*" , "+" , "."
         * @constant
         * @type int
         */
        ESCAPE_SPECIAL_CHARS: 0x00000080,
        /**
         * Will instruct the pre/post processor to use regular expressions over string substitution
         * @constant
         * @type int
         */
        USE_REGEX: 0x00000100,
        /**
         * Will instruct the pre/post processor to use Filtrex (custom bison parser, needs xexpression) over string substitution
         * @constant
         * @type int
         */
        USE_FILTREX: 0x00000200,
        /**
         * Cascade entry. There are cases where #USE_FUNCTION is not enough or we'd like to avoid further type checking.
         * @constant
         * @type int
         */
        CASCADE: 0x00000400,
        /**
         * Cascade entry. There are cases where #USE_FUNCTION is not enough or we'd like to avoid further type checking.
         * @constant
         * @type int
         */
        EXPRESSION: 0x00000800,
        /**
         * Dont parse anything
         * @constant
         * @type int
         */
        DONT_PARSE: 0x000001000,
        /**
         * Convert to hex
         * @constant
         * @type int
         */
        TO_HEX: 0x000002000,
        /**
         * Convert to hex
         * @constant
         * @type int
         */
        REPLACE_HEX: 0x000004000,
        /**
         * Wait for finish
         * @constant
         * @type int
         */
        WAIT: 0x000008000,
        /**
         * Wait for finish
         * @constant
         * @type int
         */
        DONT_ESCAPE: 0x000010000,
        /**
         * Flag to mark the maximum core bit mask, after here its user land
         * @constant
         * @type int
         */
        END: 0x000020000
    };
    /**
     * A CI's default post-pre processing order.
     *
     * @enum {string} module:xide/types/CI_ORDER
     * @memberOf module:xide/types
     */
    types.CI_CORDER = {};

    /**
     * A 'Configurable Information's ("CI") type information. Every CI has this information. You can
     * re-composite new types with ECIType.STRUCTURE. However all 'beans' (rich objects) in the system all displayed through a set of CIs,
     * also called the CIS (Configurable Information Set). There are many types already :
     *
     * Each ECIType has mapped widgets, BOOL : checkbox, STRING: Text-Areay and so forth.
     *
     * @enum {string} module:xide/types/ECIType
     * @memberOf module:xide/types
     */
    types.ECIType = {
        /**
         * @const
         * @type { int}
         */
        BOOL: 0,
        /**
         * @const
         * @type { int}
         */
        BOX: 1,
        /**
         * @const
         * @type { int}
         */
        COLOUR: 2,
        /**
         * @const
         * @type { int}
         */
        ENUMERATION: 3,
        /**
         * @const
         * @type { int}
         */
        FILE: 4,
        /**
         * @const
         * @type { int}
         */
        FLAGS: 5,
        /**
         * @const
         * @type { int}
         */
        FLOAT: 6,
        /**
         * @const
         * @type { int}
         */
        INTEGER: 7,
        /**
         * @const
         * @type { int}
         */
        MATRIX: 8,
        /**
         * @const
         * @type { int}
         */
        OBJECT: 9,
        /**
         * @const
         * @type { int}
         */
        REFERENCE: 10,
        /**
         * @const
         * @type { int}
         */
        QUATERNION: 11,
        /**
         * @const
         * @type { int}
         */
        RECTANGLE: 12,
        /**
         * @const
         * @type { int}
         */
        STRING: 13,
        /**
         * @const
         * @type { int}
         */
        VECTOR: 14,
        /**
         * @const
         * @type { int}
         */
        VECTOR2D: 15,
        /**
         * @const
         * @type { int}
         */
        VECTOR4D: 16,
        /**
         * @const
         * @type { int}
         */
        ICON: 17,
        /**
         * @const
         * @type { int}
         */
        IMAGE: 18,
        /**
         * @const
         * @type { int}
         */
        BANNER: 19,
        /**
         * @const
         * @type { int}
         */
        LOGO: 20,
        /**
         * @const
         * @type { int}
         */
        STRUCTURE: 21,
        /**
         * @const
         * @type { int}
         */
        BANNER2: 22,
        /**
         * @const
         * @type { int}
         */
        ICON_SET: 23,
        /**
         * @const
         * @type { int}
         */
        SCRIPT: 24,
        /**
         * @const
         * @type { int}
         */
        EXPRESSION: 25,
        /**
         * @const
         * @type { int}
         */
        RICHTEXT: 26,
        /**
         * @const
         * @type { int}
         */
        ARGUMENT: 27,
        /**
         * @const
         * @type { int}
         */
        JSON_DATA: 28,
        /**
         * @const
         * @type { int}
         */
        EXPRESSION_EDITOR: 29,
        /**
         * @const
         * @type { int}
         */
        WIDGET_REFERENCE: 30,
        /**
         * @const
         * @type { int}
         */
        DOM_PROPERTIES: 31,

        /**
         * @const
         * @type { int}
         */
        BLOCK_REFERENCE: 32,

        /**
         * @const
         * @type { int}
         */
        BLOCK_SETTINGS: 33,
        /**
         * @const
         * @type { int}
         */
        FILE_EDITOR: 34,
        /**
         * @const
         * @type { int}
         */
        END: 35,
        /**
         * @const
         * @type { int}
         */
        UNKNOWN: -1
    };
    /**
     * Dummy type for jsdoc
     * @typedef {Object} module:xide/types/ConfigurableInformation
     * @property {String} id
     * @property {String} name
     * @property {module:xide/types/ECIType} type
     */
    /**
     * Stub for registered bean types. This value is needed to let the UI switch between configurations per such type.
     * At the very root is the bean action context which may include more contexts.
     * @enum {string} module:xide/types/ITEM_TYPE
     * @memberOf module:xide/types
     */
    types.ITEM_TYPE = {
        /**
         * Bean type 'file' is handled by the xfile package
         * @constant
         */
        FILE: 'BTFILE', //file object
        /**
         * Bean type 'widget' is handled by the xide/ve and davinci package
         * @constant
         */
        WIDGET: 'WIDGET', //ui designer
        /**
         * Bean type 'block' is handled by the xblox package
         * @constant
         */
        BLOCK: 'BLOCK', //xblox
        /**
         * Bean type 'text' is used for text editors
         * @constant
         */
        TEXT: 'TEXT', //xace
        /**
         * Bean type 'xexpression' is used for user expressions
         * @constant
         */
        EXPRESSION: 'EXPRESSION' //xexpression
    };

    /**
     * Expression Parser is a map of currently existing parsers
     * and might be extended by additional modules. Thus, it acts as registry
     * and is here as stub.
     *
     * @enum module:xide/types/EXPRESSION_PARSER
     * @memberOf module:xide/types
     */
    if (!types.EXPRESSION_PARSER) {
        types.EXPRESSION_PARSER = {};
    }
    /**
     * Component names stub, might be extended by sub-classing applications
     * @constant xide.types.COMPONENT_NAMES
     */
    types.COMPONENT_NAMES = {
        XIDEVE: 'xideve',
        XNODE: 'xnode',
        XBLOX: 'xblox',
        XFILE: 'xfile',
        XACE: 'xace',
        XEXPRESSION: 'xexpression',
        XCONSOLE: 'xconsole',
        XTRACK: 'xtrack'
    };

    /**
     * WIDGET_REFERENCE_MODE enumerates possible modes to resolve a string expression
     * into instances. There are a few CI based widgets subclassed from xide/widgets/Referenced.
     * The reference structure consist out of this mode and that expression.
     *
     * @constant {Array.<module:xide/types~WidgetReferenceMode>}
     *     module:xide/types~WIDGET_REFERENCE_MODE
     */
    types.WIDGET_REFERENCE_MODE = {
        BY_ID: 'byid',
        BY_CLASS: 'byclass',
        BY_CSS: 'bycss',
        BY_EXPRESSION: 'expression'
    };
    /**
     * Possible split modes for rich editors with preview or live coding views.
     *
     * @constant {Array.<module:xide/types~ViewSplitMode>}
     *     module:xide/types~VIEW_SPLIT_MODE
     */
    types.VIEW_SPLIT_MODE = {
        DESIGN: 1,
        SOURCE: 2,
        SPLIT_VERTICAL: 6,
        SPLIT_HORIZONTAL: 7
    };
    /**
     * All client resources are through variables on the server side. Here the minimum variables for an xjs application.
     *
     * @constant {Array.<module:xide/types~RESOURCE_VARIABLES>}
     *     module:xide/types~RESOURCE_VARIABLES
     */
    types.RESOURCE_VARIABLES = {
        ACE: 'ACE',
        APP_URL: 'APP_URL',
        SITE_URL: 'SITE_URL'
    };
    /**
     * Events of xide.*
     * @enum {string} module:xide/types/EVENTS
     * @memberOf module:xide/types
     * @extends xide/types
     */
    types.EVENTS = {
        /**
         * generic
         */
        ERROR: 'onError', //xhr
        STATUS: 'onStatus', //xhr
        ON_CREATED_MANAGER: 'onCreatedManager', //context

        /**
         * item events, to be renoved
         */
        ON_ITEM_SELECTED: 'onItemSelected',
        ON_ITEM_REMOVED: 'onItemRemoved',
        ON_ITEM_CLOSED: 'onItemClosed',
        ON_ITEM_ADDED: 'onItemAdded',
        ON_ITEM_MODIFIED: 'onItemModified',
        ON_NODE_SERVICE_STORE_READY: 'onNodeServiceStoreReady',
        /**
         * old, to be removd
         */
        ON_FILE_STORE_READY: 'onFileStoreReady',
        ON_CONTEXT_MENU_OPEN: 'onContextMenuOpen',
        /**
         * CI events
         */
        ON_CI_UPDATE: 'onCIUpdate',

        /**
         * widgets
         */
        ON_WIDGET_READY: 'onWidgetReady',
        ON_CREATED_WIDGET: 'onWidgetCreated',
        RESIZE: 'onResize',
        /**
         * Event to notify classes about a reloaded module
         * @constant
         * @type string
         */
        ON_MODULE_RELOADED: 'onModuleReloaded',
        ON_MODULE_UPDATED: 'onModuleUpdated',


        ON_DID_OPEN_ITEM: 'onDidOpenItem', //remove
        ON_DID_RENDER_COLLECTION: 'onDidRenderCollection', //move

        ON_PLUGIN_LOADED: 'onPluginLoaded',
        ON_PLUGIN_READY: 'onPluginReady',
        ALL_PLUGINS_READY: 'onAllPluginsReady',

        /**
         * editors
         */
        ON_CREATE_EDITOR_BEGIN: 'onCreateEditorBegin', //move to xedit
        ON_CREATE_EDITOR_END: 'onCreateEditorEnd', //move to xedit
        REGISTER_EDITOR: 'registerEditor', //move to xedit
        ON_EXPRESSION_EDITOR_ADD_FUNCTIONS: 'onExpressionEditorAddFunctions', //move to xedit
        ON_ACE_READY: 'onACEReady', //remove

        /**
         * Files
         */
        ON_UNSAVED_CONTENT: 'onUnSavedContent',
        ON_FILE_CHANGED: 'fileChanged',
        ON_FILE_DELETED: 'fileDeleted',
        IMAGE_LOADED: 'imageLoaded',
        IMAGE_ERROR: 'imageError',
        UPLOAD_BEGIN: 'uploadBegin',
        UPLOAD_PROGRESS: 'uploadProgress',
        UPLOAD_FINISH: 'uploadFinish',
        UPLOAD_FAILED: 'uploadFailed',
        ON_FILE_CONTENT_CHANGED: 'onFileContentChanged',
        ON_COPY_BEGIN: 'onCopyBegin',
        ON_COPY_END: 'onCopyEnd',
        ON_DELETE_BEGIN: 'onDeleteBegin',
        ON_DELETE_END: 'onDeleteEnd',
        ON_MOVE_BEGIN: 'onMoveBegin',
        ON_MOVE_END: 'onMoveEnd',
        ON_CHANGED_CONTENT: 'onChangedContent',
        ON_COMPRESS_BEGIN: 'onCompressBegin',
        ON_COMPRESS_END: 'onCompressEnd',



        ON_COMPONENT_READY: 'onComponentReady',
        ON_ALL_COMPONENTS_LOADED: 'onAllComponentsLoaded',
        ON_APP_READY: 'onAppReady',
        /**
         * Store
         */
        ON_CREATE_STORE: 'onCreateStore',
        ON_STORE_CREATED: 'onStoreCreated',
        ON_STORE_CHANGED: 'onStoreChanged',
        ON_STATUS_MESSAGE: 'onStatusMessage',
        /**
         * layout
         */
        SAVE_LAYOUT: 'layoutSave',
        RESTORE_LAYOUT: 'layoutRestore',
        /**
         * views, panels and 'main view'
         */
        ON_SHOW_PANEL: 'onShowPanel',
        ON_PANEL_CLOSED: 'onPanelClosed',
        ON_PANEL_CREATED: 'onPanelCreated',

        ON_MAIN_VIEW_READY: 'onMainViewReady',
        ON_MAIN_MENU_READY: 'onMainMenuReady',
        ON_MAIN_MENU_OPEN: 'onMainMenuOpen',
        ON_VIEW_REMOVED: 'onViewRemoved',
        ON_VIEW_SHOW: 'onViewShow',
        ON_VIEW_HIDE: 'onViewHide',
        ON_VIEW_ADDED: 'onViewAdded',
        ON_OPEN_VIEW: 'onOpenView',
        ON_VIEW_MAXIMIZE_START: 'onViewMaximizeStart',
        ON_VIEW_MAXIMIZE_END: 'onViewMaximizeEnd',
        ON_CONTAINER_ADDED: 'onContainerAdded',
        ON_CONTAINER_REMOVED: 'onContainerRemoved',
        ON_REMOVE_CONTAINER: 'onRemoveContainer',
        ON_CONTAINER_REPLACED: 'onContainerReplaced',
        ON_CONTAINER_SPLIT: 'onContainerSplit',
        ON_RENDER_WELCOME_GRID_GROUP: 'onRenderWelcomeGridGroup',

        ON_DND_SOURCE_OVER: '/dnd/source/over',
        ON_DND_START: '/dnd/start',
        ON_DND_DROP_BEFORE: '/dnd/drop/before',
        ON_DND_DROP: '/dnd/drop',
        ON_DND_CANCEL: '/dnd/cancel'
    };
    /**
     * To be moved
     * @type {{SIZE_NORMAL: string, SIZE_SMALL: string, SIZE_WIDE: string, SIZE_LARGE: string}}
     */
    types.DIALOG_SIZE = {
        SIZE_NORMAL: 'size-normal',
        SIZE_SMALL: 'size-small',
        SIZE_WIDE: 'size-wide', // size-wide is equal to modal-lg
        SIZE_LARGE: 'size-large'
    };

    /**
     * To be moved
     * @type {{DEFAULT: string, INFO: string, PRIMARY: string, SUCCESS: string, WARNING: string, DANGER: string}}
     */
    types.DIALOG_TYPE = {
        DEFAULT: 'type-default',
        INFO: 'type-info',
        PRIMARY: 'type-primary',
        SUCCESS: 'type-success',
        WARNING: 'type-warning',
        DANGER: 'type-danger'
    };
    /**
     * @TODO: remove, defined in xideve
     */
    lang.mixin(types, {
        LAYOUT_RIGHT_CENTER_BOTTOM: 'LAYOUT_RIGHT_CENTER_BOTTOM',
        LAYOUT_CENTER_BOTTOM: 'LAYOUT_CENTER_BOTTOM',
        LAYOUT_CENTER_RIGHT: 'LAYOUT_CENTER_RIGHT',
        LAYOUT_LEFT_CENTER_RIGHT: 'LAYOUT_LEFT_CENTER_RIGHT',
        LAYOUT_LEFT_CENTER_RIGHT_BOTTOM: 'LAYOUT_LEFT_CENTER_RIGHT_BOTTOM'
    });

    /**
     * Hard Dojo override to catch malformed JSON.
     * @param js
     * @returns {*}
     */
    dojo.fromJson = function (js, debug) {
        let res = null;
        let didFail = false;
        debug = true;
        try {
            res = eval("(" + js + ")");
        } catch (e) {
            didFail = true;
        }

        if (didFail) {
            const js2 = js.substring(js.indexOf('{'), js.lastIndexOf('}') + 1);
            try {
                js2 && (res = eval("(" + js2 + ")"));
            } catch (e) {
                debug !== false && console.error('error in json parsing! ' + js);
                if (js.indexOf('error') !== -1) {
                    return {
                        "jsonrpc": "2.0",
                        "result": {
                            "error": {
                                "code": 1,
                                "message": js,
                                "data": null
                            }
                        },
                        "id": 0
                    };
                }
                throw new Error(js);
            }
        }
        return res;
    };
    return types;
});