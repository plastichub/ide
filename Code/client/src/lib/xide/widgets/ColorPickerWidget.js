/** @module xide/widgets/ColorPickerWidget **/
define([
    "dcl/dcl",
    'xide/widgets/EditBox',
    'xide/$',
    'xide/popup'
], function (dcl, EditBox, $, popup) {

    (function (Math) {
        const trimLeft = /^\s+/;
        const trimRight = /\s+$/;
        let tinyCounter = 0;
        const mathRound = Math.round;
        const mathMin = Math.min;
        const mathMax = Math.max;
        const mathRandom = Math.random;

        function tinycolor(color, opts) {

            color = (color) ? color : '';
            opts = opts || {};

            // If input is already a tinycolor, return itself
            if (color instanceof tinycolor) {
                return color;
            }
            // If we are called as a function, call using new instead
            if (!(this instanceof tinycolor)) {
                return new tinycolor(color, opts);
            }

            const rgb = inputToRGB(color);
            this._originalInput = color,
                this._r = rgb.r,
                this._g = rgb.g,
                this._b = rgb.b,
                this._a = rgb.a,
                this._roundA = mathRound(100 * this._a) / 100,
                this._format = opts.format || rgb.format;
            this._gradientType = opts.gradientType;

            // Don't let the range of [0,255] come back in [0,1].
            // Potentially lose a little bit of precision here, but will fix issues where
            // .5 gets interpreted as half of the total, instead of half of 1
            // If it was supposed to be 128, this was already taken care of by `inputToRgb`
            if (this._r < 1) {
                this._r = mathRound(this._r);
            }
            if (this._g < 1) {
                this._g = mathRound(this._g);
            }
            if (this._b < 1) {
                this._b = mathRound(this._b);
            }

            this._ok = rgb.ok;
            this._tc_id = tinyCounter++;
        }

        tinycolor.prototype = {
            isDark: function () {
                return this.getBrightness() < 128;
            },
            isLight: function () {
                return !this.isDark();
            },
            isValid: function () {
                return this._ok;
            },
            getOriginalInput: function () {
                return this._originalInput;
            },
            getFormat: function () {
                return this._format;
            },
            getAlpha: function () {
                return this._a;
            },
            getBrightness: function () {
                //http://www.w3.org/TR/AERT#color-contrast
                const rgb = this.toRgb();
                return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
            },
            getLuminance: function () {
                //http://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
                const rgb = this.toRgb();
                let RsRGB;
                let GsRGB;
                let BsRGB;
                let R;
                let G;
                let B;
                RsRGB = rgb.r / 255;
                GsRGB = rgb.g / 255;
                BsRGB = rgb.b / 255;

                if (RsRGB <= 0.03928) {
                    R = RsRGB / 12.92;
                } else {
                    R = Math.pow(((RsRGB + 0.055) / 1.055), 2.4);
                }
                if (GsRGB <= 0.03928) {
                    G = GsRGB / 12.92;
                } else {
                    G = Math.pow(((GsRGB + 0.055) / 1.055), 2.4);
                }
                if (BsRGB <= 0.03928) {
                    B = BsRGB / 12.92;
                } else {
                    B = Math.pow(((BsRGB + 0.055) / 1.055), 2.4);
                }
                return (0.2126 * R) + (0.7152 * G) + (0.0722 * B);
            },
            setAlpha: function (value) {
                this._a = boundAlpha(value);
                this._roundA = mathRound(100 * this._a) / 100;
                return this;
            },
            toHsv: function () {
                const hsv = rgbToHsv(this._r, this._g, this._b);
                return {h: hsv.h * 360, s: hsv.s, v: hsv.v, a: this._a};
            },
            toHsvString: function () {
                const hsv = rgbToHsv(this._r, this._g, this._b);
                const h = mathRound(hsv.h * 360);
                const s = mathRound(hsv.s * 100);
                const v = mathRound(hsv.v * 100);
                return (this._a == 1) ?
                    "hsv(" + h + ", " + s + "%, " + v + "%)" :
                    "hsva(" + h + ", " + s + "%, " + v + "%, " + this._roundA + ")";
            },
            toHsl: function () {
                const hsl = rgbToHsl(this._r, this._g, this._b);
                return {h: hsl.h * 360, s: hsl.s, l: hsl.l, a: this._a};
            },
            toHslString: function () {
                const hsl = rgbToHsl(this._r, this._g, this._b);
                const h = mathRound(hsl.h * 360);
                const s = mathRound(hsl.s * 100);
                const l = mathRound(hsl.l * 100);
                return (this._a == 1) ?
                    "hsl(" + h + ", " + s + "%, " + l + "%)" :
                    "hsla(" + h + ", " + s + "%, " + l + "%, " + this._roundA + ")";
            },
            toHex: function (allow3Char) {
                return rgbToHex(this._r, this._g, this._b, allow3Char);
            },
            toHexString: function (allow3Char) {
                return '#' + this.toHex(allow3Char);
            },
            toHex8: function (allow4Char) {
                return rgbaToHex(this._r, this._g, this._b, this._a, allow4Char);
            },
            toHex8String: function (allow4Char) {
                return '#' + this.toHex8(allow4Char);
            },
            toRgb: function () {
                return {r: mathRound(this._r), g: mathRound(this._g), b: mathRound(this._b), a: this._a};
            },
            toRgbString: function () {
                return (this._a == 1) ?
                    "rgb(" + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ")" :
                    "rgba(" + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ", " + this._roundA + ")";
            },
            toPercentageRgb: function () {
                return {
                    r: mathRound(bound01(this._r, 255) * 100) + "%",
                    g: mathRound(bound01(this._g, 255) * 100) + "%",
                    b: mathRound(bound01(this._b, 255) * 100) + "%",
                    a: this._a
                };
            },
            toPercentageRgbString: function () {
                return (this._a == 1) ?
                    "rgb(" + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%)" :
                    "rgba(" + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%, " + this._roundA + ")";
            },
            toName: function () {
                if (this._a === 0) {
                    return "transparent";
                }

                if (this._a < 1) {
                    return false;
                }

                return hexNames[rgbToHex(this._r, this._g, this._b, true)] || false;
            },
            toFilter: function (secondColor) {
                const hex8String = '#' + rgbaToArgbHex(this._r, this._g, this._b, this._a);
                let secondHex8String = hex8String;
                const gradientType = this._gradientType ? "GradientType = 1, " : "";

                if (secondColor) {
                    const s = tinycolor(secondColor);
                    secondHex8String = '#' + rgbaToArgbHex(s._r, s._g, s._b, s._a);
                }

                return "progid:DXImageTransform.Microsoft.gradient(" + gradientType + "startColorstr=" + hex8String + ",endColorstr=" + secondHex8String + ")";
            },
            toString: function (format) {
                const formatSet = !!format;
                format = format || this._format;

                let formattedString = false;
                const hasAlpha = this._a < 1 && this._a >= 0;
                const needsAlphaFormat = !formatSet && hasAlpha && (format === "hex" || format === "hex6" || format === "hex3" || format === "hex4" || format === "hex8" || format === "name");

                if (needsAlphaFormat) {
                    // Special case for "transparent", all other non-alpha formats
                    // will return rgba when there is transparency.
                    if (format === "name" && this._a === 0) {
                        return this.toName();
                    }
                    return this.toRgbString();
                }
                if (format === "rgb") {
                    formattedString = this.toRgbString();
                }
                if (format === "prgb") {
                    formattedString = this.toPercentageRgbString();
                }
                if (format === "hex" || format === "hex6") {
                    formattedString = this.toHexString();
                }
                if (format === "hex3") {
                    formattedString = this.toHexString(true);
                }
                if (format === "hex4") {
                    formattedString = this.toHex8String(true);
                }
                if (format === "hex8") {
                    formattedString = this.toHex8String();
                }
                if (format === "name") {
                    formattedString = this.toName();
                }
                if (format === "hsl") {
                    formattedString = this.toHslString();
                }
                if (format === "hsv") {
                    formattedString = this.toHsvString();
                }

                return formattedString || this.toHexString();
            },
            clone: function () {
                return tinycolor(this.toString());
            },

            _applyModification: function (fn, args) {
                const color = fn.apply(null, [this].concat([].slice.call(args)));
                this._r = color._r;
                this._g = color._g;
                this._b = color._b;
                this.setAlpha(color._a);
                return this;
            },
            lighten: function () {
                return this._applyModification(lighten, arguments);
            },
            brighten: function () {
                return this._applyModification(brighten, arguments);
            },
            darken: function () {
                return this._applyModification(darken, arguments);
            },
            desaturate: function () {
                return this._applyModification(desaturate, arguments);
            },
            saturate: function () {
                return this._applyModification(saturate, arguments);
            },
            greyscale: function () {
                return this._applyModification(greyscale, arguments);
            },
            spin: function () {
                return this._applyModification(spin, arguments);
            },

            _applyCombination: function (fn, args) {
                return fn.apply(null, [this].concat([].slice.call(args)));
            },
            analogous: function () {
                return this._applyCombination(analogous, arguments);
            },
            complement: function () {
                return this._applyCombination(complement, arguments);
            },
            monochromatic: function () {
                return this._applyCombination(monochromatic, arguments);
            },
            splitcomplement: function () {
                return this._applyCombination(splitcomplement, arguments);
            },
            triad: function () {
                return this._applyCombination(triad, arguments);
            },
            tetrad: function () {
                return this._applyCombination(tetrad, arguments);
            }
        };

        // If input is an object, force 1 into "1.0" to handle ratios properly
        // String input requires "1.0" as input, so 1 will be treated as 1
        tinycolor.fromRatio = function (color, opts) {
            if (typeof color == "object") {
                const newColor = {};
                for (const i in color) {
                    if (color.hasOwnProperty(i)) {
                        if (i === "a") {
                            newColor[i] = color[i];
                        }
                        else {
                            newColor[i] = convertToPercentage(color[i]);
                        }
                    }
                }
                color = newColor;
            }

            return tinycolor(color, opts);
        };

        // Given a string or object, convert that input to RGB
        // Possible string inputs:
        //
        //     "red"
        //     "#f00" or "f00"
        //     "#ff0000" or "ff0000"
        //     "#ff000000" or "ff000000"
        //     "rgb 255 0 0" or "rgb (255, 0, 0)"
        //     "rgb 1.0 0 0" or "rgb (1, 0, 0)"
        //     "rgba (255, 0, 0, 1)" or "rgba 255, 0, 0, 1"
        //     "rgba (1.0, 0, 0, 1)" or "rgba 1.0, 0, 0, 1"
        //     "hsl(0, 100%, 50%)" or "hsl 0 100% 50%"
        //     "hsla(0, 100%, 50%, 1)" or "hsla 0 100% 50%, 1"
        //     "hsv(0, 100%, 100%)" or "hsv 0 100% 100%"
        //
        function inputToRGB(color) {

            let rgb = {r: 0, g: 0, b: 0};
            let a = 1;
            let s = null;
            let v = null;
            let l = null;
            let ok = false;
            let format = false;

            if (typeof color == "string") {
                color = stringInputToObject(color);
            }

            if (typeof color == "object") {
                if (isValidCSSUnit(color.r) && isValidCSSUnit(color.g) && isValidCSSUnit(color.b)) {
                    rgb = rgbToRgb(color.r, color.g, color.b);
                    ok = true;
                    format = String(color.r).substr(-1) === "%" ? "prgb" : "rgb";
                }
                else if (isValidCSSUnit(color.h) && isValidCSSUnit(color.s) && isValidCSSUnit(color.v)) {
                    s = convertToPercentage(color.s);
                    v = convertToPercentage(color.v);
                    rgb = hsvToRgb(color.h, s, v);
                    ok = true;
                    format = "hsv";
                }
                else if (isValidCSSUnit(color.h) && isValidCSSUnit(color.s) && isValidCSSUnit(color.l)) {
                    s = convertToPercentage(color.s);
                    l = convertToPercentage(color.l);
                    rgb = hslToRgb(color.h, s, l);
                    ok = true;
                    format = "hsl";
                }

                if (color.hasOwnProperty("a")) {
                    a = color.a;
                }
            }

            a = boundAlpha(a);

            return {
                ok: ok,
                format: color.format || format,
                r: mathMin(255, mathMax(rgb.r, 0)),
                g: mathMin(255, mathMax(rgb.g, 0)),
                b: mathMin(255, mathMax(rgb.b, 0)),
                a: a
            };
        }


        // Conversion Functions
        // --------------------

        // `rgbToHsl`, `rgbToHsv`, `hslToRgb`, `hsvToRgb` modified from:
        // <http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript>

        // `rgbToRgb`
        // Handle bounds / percentage checking to conform to CSS color spec
        // <http://www.w3.org/TR/css3-color/>
        // *Assumes:* r, g, b in [0, 255] or [0, 1]
        // *Returns:* { r, g, b } in [0, 255]
        function rgbToRgb(r, g, b) {
            return {
                r: bound01(r, 255) * 255,
                g: bound01(g, 255) * 255,
                b: bound01(b, 255) * 255
            };
        }

        // `rgbToHsl`
        // Converts an RGB color value to HSL.
        // *Assumes:* r, g, and b are contained in [0, 255] or [0, 1]
        // *Returns:* { h, s, l } in [0,1]
        function rgbToHsl(r, g, b) {
            r = bound01(r, 255);
            g = bound01(g, 255);
            b = bound01(b, 255);

            const max = mathMax(r, g, b);
            const min = mathMin(r, g, b);
            let h;
            let s;
            const l = (max + min) / 2;

            if (max == min) {
                h = s = 0; // achromatic
            }
            else {
                const d = max - min;
                s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                switch (max) {
                    case r:
                        h = (g - b) / d + (g < b ? 6 : 0);
                        break;
                    case g:
                        h = (b - r) / d + 2;
                        break;
                    case b:
                        h = (r - g) / d + 4;
                        break;
                }

                h /= 6;
            }

            return {h: h, s: s, l: l};
        }

        // `hslToRgb`
        // Converts an HSL color value to RGB.
        // *Assumes:* h is contained in [0, 1] or [0, 360] and s and l are contained [0, 1] or [0, 100]
        // *Returns:* { r, g, b } in the set [0, 255]
        function hslToRgb(h, s, l) {
            let r;
            let g;
            let b;

            h = bound01(h, 360);
            s = bound01(s, 100);
            l = bound01(l, 100);

            function hue2rgb(p, q, t) {
                if (t < 0) t += 1;
                if (t > 1) t -= 1;
                if (t < 1 / 6) return p + (q - p) * 6 * t;
                if (t < 1 / 2) return q;
                if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
                return p;
            }

            if (s === 0) {
                r = g = b = l; // achromatic
            }
            else {
                const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                const p = 2 * l - q;
                r = hue2rgb(p, q, h + 1 / 3);
                g = hue2rgb(p, q, h);
                b = hue2rgb(p, q, h - 1 / 3);
            }

            return {r: r * 255, g: g * 255, b: b * 255};
        }

        // `rgbToHsv`
        // Converts an RGB color value to HSV
        // *Assumes:* r, g, and b are contained in the set [0, 255] or [0, 1]
        // *Returns:* { h, s, v } in [0,1]
        function rgbToHsv(r, g, b) {
            r = bound01(r, 255);
            g = bound01(g, 255);
            b = bound01(b, 255);

            const max = mathMax(r, g, b);
            const min = mathMin(r, g, b);
            let h;
            let s;
            const v = max;

            const d = max - min;
            s = max === 0 ? 0 : d / max;

            if (max == min) {
                h = 0; // achromatic
            }
            else {
                switch (max) {
                    case r:
                        h = (g - b) / d + (g < b ? 6 : 0);
                        break;
                    case g:
                        h = (b - r) / d + 2;
                        break;
                    case b:
                        h = (r - g) / d + 4;
                        break;
                }
                h /= 6;
            }
            return {h: h, s: s, v: v};
        }

        // `hsvToRgb`
        // Converts an HSV color value to RGB.
        // *Assumes:* h is contained in [0, 1] or [0, 360] and s and v are contained in [0, 1] or [0, 100]
        // *Returns:* { r, g, b } in the set [0, 255]
        function hsvToRgb(h, s, v) {
            h = bound01(h, 360) * 6;
            s = bound01(s, 100);
            v = bound01(v, 100);

            const i = Math.floor(h);
            const f = h - i;
            const p = v * (1 - s);
            const q = v * (1 - f * s);
            const t = v * (1 - (1 - f) * s);
            const mod = i % 6;
            const r = [v, q, p, p, t, v][mod];
            const g = [t, v, v, q, p, p][mod];
            const b = [p, p, t, v, v, q][mod];

            return {r: r * 255, g: g * 255, b: b * 255};
        }

        // `rgbToHex`
        // Converts an RGB color to hex
        // Assumes r, g, and b are contained in the set [0, 255]
        // Returns a 3 or 6 character hex
        function rgbToHex(r, g, b, allow3Char) {

            const hex = [
                pad2(mathRound(r).toString(16)),
                pad2(mathRound(g).toString(16)),
                pad2(mathRound(b).toString(16))
            ];

            // Return a 3 character hex if possible
            if (allow3Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1)) {
                return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0);
            }

            return hex.join("");
        }

        // `rgbaToHex`
        // Converts an RGBA color plus alpha transparency to hex
        // Assumes r, g, b are contained in the set [0, 255] and
        // a in [0, 1]. Returns a 4 or 8 character rgba hex
        function rgbaToHex(r, g, b, a, allow4Char) {

            const hex = [
                pad2(mathRound(r).toString(16)),
                pad2(mathRound(g).toString(16)),
                pad2(mathRound(b).toString(16)),
                pad2(convertDecimalToHex(a))
            ];

            // Return a 4 character hex if possible
            if (allow4Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1) && hex[3].charAt(0) == hex[3].charAt(1)) {
                return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0) + hex[3].charAt(0);
            }

            return hex.join("");
        }

        // `rgbaToArgbHex`
        // Converts an RGBA color to an ARGB Hex8 string
        // Rarely used, but required for "toFilter()"
        function rgbaToArgbHex(r, g, b, a) {

            const hex = [
                pad2(convertDecimalToHex(a)),
                pad2(mathRound(r).toString(16)),
                pad2(mathRound(g).toString(16)),
                pad2(mathRound(b).toString(16))
            ];

            return hex.join("");
        }

        // `equals`
        // Can be called with any tinycolor input
        tinycolor.equals = function (color1, color2) {
            if (!color1 || !color2) {
                return false;
            }
            return tinycolor(color1).toRgbString() == tinycolor(color2).toRgbString();
        };

        tinycolor.random = function () {
            return tinycolor.fromRatio({
                r: mathRandom(),
                g: mathRandom(),
                b: mathRandom()
            });
        };


        // Modification Functions
        // ----------------------
        // Thanks to less.js for some of the basics here
        // <https://github.com/cloudhead/less.js/blob/master/lib/less/functions.js>

        function desaturate(color, amount) {
            amount = (amount === 0) ? 0 : (amount || 10);
            const hsl = tinycolor(color).toHsl();
            hsl.s -= amount / 100;
            hsl.s = clamp01(hsl.s);
            return tinycolor(hsl);
        }

        function saturate(color, amount) {
            amount = (amount === 0) ? 0 : (amount || 10);
            const hsl = tinycolor(color).toHsl();
            hsl.s += amount / 100;
            hsl.s = clamp01(hsl.s);
            return tinycolor(hsl);
        }

        function greyscale(color) {
            return tinycolor(color).desaturate(100);
        }

        function lighten(color, amount) {
            amount = (amount === 0) ? 0 : (amount || 10);
            const hsl = tinycolor(color).toHsl();
            hsl.l += amount / 100;
            hsl.l = clamp01(hsl.l);
            return tinycolor(hsl);
        }

        function brighten(color, amount) {
            amount = (amount === 0) ? 0 : (amount || 10);
            const rgb = tinycolor(color).toRgb();
            rgb.r = mathMax(0, mathMin(255, rgb.r - mathRound(255 * -(amount / 100))));
            rgb.g = mathMax(0, mathMin(255, rgb.g - mathRound(255 * -(amount / 100))));
            rgb.b = mathMax(0, mathMin(255, rgb.b - mathRound(255 * -(amount / 100))));
            return tinycolor(rgb);
        }

        function darken(color, amount) {
            amount = (amount === 0) ? 0 : (amount || 10);
            const hsl = tinycolor(color).toHsl();
            hsl.l -= amount / 100;
            hsl.l = clamp01(hsl.l);
            return tinycolor(hsl);
        }

        // Spin takes a positive or negative amount within [-360, 360] indicating the change of hue.
        // Values outside of this range will be wrapped into this range.
        function spin(color, amount) {
            const hsl = tinycolor(color).toHsl();
            const hue = (hsl.h + amount) % 360;
            hsl.h = hue < 0 ? 360 + hue : hue;
            return tinycolor(hsl);
        }

        // Combination Functions
        // ---------------------
        // Thanks to jQuery xColor for some of the ideas behind these
        // <https://github.com/infusion/jQuery-xcolor/blob/master/jquery.xcolor.js>

        function complement(color) {
            const hsl = tinycolor(color).toHsl();
            hsl.h = (hsl.h + 180) % 360;
            return tinycolor(hsl);
        }

        function triad(color) {
            const hsl = tinycolor(color).toHsl();
            const h = hsl.h;
            return [
                tinycolor(color),
                tinycolor({h: (h + 120) % 360, s: hsl.s, l: hsl.l}),
                tinycolor({h: (h + 240) % 360, s: hsl.s, l: hsl.l})
            ];
        }

        function tetrad(color) {
            const hsl = tinycolor(color).toHsl();
            const h = hsl.h;
            return [
                tinycolor(color),
                tinycolor({h: (h + 90) % 360, s: hsl.s, l: hsl.l}),
                tinycolor({h: (h + 180) % 360, s: hsl.s, l: hsl.l}),
                tinycolor({h: (h + 270) % 360, s: hsl.s, l: hsl.l})
            ];
        }

        function splitcomplement(color) {
            const hsl = tinycolor(color).toHsl();
            const h = hsl.h;
            return [
                tinycolor(color),
                tinycolor({h: (h + 72) % 360, s: hsl.s, l: hsl.l}),
                tinycolor({h: (h + 216) % 360, s: hsl.s, l: hsl.l})
            ];
        }

        function analogous(color, results, slices) {
            results = results || 6;
            slices = slices || 30;

            const hsl = tinycolor(color).toHsl();
            const part = 360 / slices;
            const ret = [tinycolor(color)];

            for (hsl.h = ((hsl.h - (part * results >> 1)) + 720) % 360; --results;) {
                hsl.h = (hsl.h + part) % 360;
                ret.push(tinycolor(hsl));
            }
            return ret;
        }

        function monochromatic(color, results) {
            results = results || 6;
            const hsv = tinycolor(color).toHsv();
            const h = hsv.h;
            const s = hsv.s;
            let v = hsv.v;
            const ret = [];
            const modification = 1 / results;

            while (results--) {
                ret.push(tinycolor({h: h, s: s, v: v}));
                v = (v + modification) % 1;
            }

            return ret;
        }

        // Utility Functions
        // ---------------------

        tinycolor.mix = function (color1, color2, amount) {
            amount = (amount === 0) ? 0 : (amount || 50);

            const rgb1 = tinycolor(color1).toRgb();
            const rgb2 = tinycolor(color2).toRgb();

            const p = amount / 100;

            const rgba = {
                r: ((rgb2.r - rgb1.r) * p) + rgb1.r,
                g: ((rgb2.g - rgb1.g) * p) + rgb1.g,
                b: ((rgb2.b - rgb1.b) * p) + rgb1.b,
                a: ((rgb2.a - rgb1.a) * p) + rgb1.a
            };

            return tinycolor(rgba);
        };


        // Readability Functions
        // ---------------------
        // <http://www.w3.org/TR/2008/REC-WCAG20-20081211/#contrast-ratiodef (WCAG Version 2)

        // `contrast`
        // Analyze the 2 colors and returns the color contrast defined by (WCAG Version 2)
        tinycolor.readability = function (color1, color2) {
            const c1 = tinycolor(color1);
            const c2 = tinycolor(color2);
            return (Math.max(c1.getLuminance(), c2.getLuminance()) + 0.05) / (Math.min(c1.getLuminance(), c2.getLuminance()) + 0.05);
        };

        // `isReadable`
        // Ensure that foreground and background color combinations meet WCAG2 guidelines.
        // The third argument is an optional Object.
        //      the 'level' property states 'AA' or 'AAA' - if missing or invalid, it defaults to 'AA';
        //      the 'size' property states 'large' or 'small' - if missing or invalid, it defaults to 'small'.
        // If the entire object is absent, isReadable defaults to {level:"AA",size:"small"}.

        // *Example*
        //    tinycolor.isReadable("#000", "#111") => false
        //    tinycolor.isReadable("#000", "#111",{level:"AA",size:"large"}) => false
        tinycolor.isReadable = function (color1, color2, wcag2) {
            const readability = tinycolor.readability(color1, color2);
            let wcag2Parms;
            let out;

            out = false;

            wcag2Parms = validateWCAG2Parms(wcag2);
            switch (wcag2Parms.level + wcag2Parms.size) {
                case "AAsmall":
                case "AAAlarge":
                    out = readability >= 4.5;
                    break;
                case "AAlarge":
                    out = readability >= 3;
                    break;
                case "AAAsmall":
                    out = readability >= 7;
                    break;
            }
            return out;
        };

        // `mostReadable`
        // Given a base color and a list of possible foreground or background
        // colors for that base, returns the most readable color.
        // Optionally returns Black or White if the most readable color is unreadable.
        // *Example*
        //    tinycolor.mostReadable(tinycolor.mostReadable("#123", ["#124", "#125"],{includeFallbackColors:false}).toHexString(); // "#112255"
        //    tinycolor.mostReadable(tinycolor.mostReadable("#123", ["#124", "#125"],{includeFallbackColors:true}).toHexString();  // "#ffffff"
        //    tinycolor.mostReadable("#a8015a", ["#faf3f3"],{includeFallbackColors:true,level:"AAA",size:"large"}).toHexString(); // "#faf3f3"
        //    tinycolor.mostReadable("#a8015a", ["#faf3f3"],{includeFallbackColors:true,level:"AAA",size:"small"}).toHexString(); // "#ffffff"
        tinycolor.mostReadable = function (baseColor, colorList, args) {
            let bestColor = null;
            let bestScore = 0;
            let readability;
            let includeFallbackColors;
            let level;
            let size;
            args = args || {};
            includeFallbackColors = args.includeFallbackColors;
            level = args.level;
            size = args.size;

            for (let i = 0; i < colorList.length; i++) {
                readability = tinycolor.readability(baseColor, colorList[i]);
                if (readability > bestScore) {
                    bestScore = readability;
                    bestColor = tinycolor(colorList[i]);
                }
            }

            if (tinycolor.isReadable(baseColor, bestColor, {"level": level, "size": size}) || !includeFallbackColors) {
                return bestColor;
            }
            else {
                args.includeFallbackColors = false;
                return tinycolor.mostReadable(baseColor, ["#fff", "#000"], args);
            }
        };


        // Big List of Colors
        // ------------------
        // <http://www.w3.org/TR/css3-color/#svg-color>
        const names = tinycolor.names = {
            aliceblue: "f0f8ff",
            antiquewhite: "faebd7",
            aqua: "0ff",
            aquamarine: "7fffd4",
            azure: "f0ffff",
            beige: "f5f5dc",
            bisque: "ffe4c4",
            black: "000",
            blanchedalmond: "ffebcd",
            blue: "00f",
            blueviolet: "8a2be2",
            brown: "a52a2a",
            burlywood: "deb887",
            burntsienna: "ea7e5d",
            cadetblue: "5f9ea0",
            chartreuse: "7fff00",
            chocolate: "d2691e",
            coral: "ff7f50",
            cornflowerblue: "6495ed",
            cornsilk: "fff8dc",
            crimson: "dc143c",
            cyan: "0ff",
            darkblue: "00008b",
            darkcyan: "008b8b",
            darkgoldenrod: "b8860b",
            darkgray: "a9a9a9",
            darkgreen: "006400",
            darkgrey: "a9a9a9",
            darkkhaki: "bdb76b",
            darkmagenta: "8b008b",
            darkolivegreen: "556b2f",
            darkorange: "ff8c00",
            darkorchid: "9932cc",
            darkred: "8b0000",
            darksalmon: "e9967a",
            darkseagreen: "8fbc8f",
            darkslateblue: "483d8b",
            darkslategray: "2f4f4f",
            darkslategrey: "2f4f4f",
            darkturquoise: "00ced1",
            darkviolet: "9400d3",
            deeppink: "ff1493",
            deepskyblue: "00bfff",
            dimgray: "696969",
            dimgrey: "696969",
            dodgerblue: "1e90ff",
            firebrick: "b22222",
            floralwhite: "fffaf0",
            forestgreen: "228b22",
            fuchsia: "f0f",
            gainsboro: "dcdcdc",
            ghostwhite: "f8f8ff",
            gold: "ffd700",
            goldenrod: "daa520",
            gray: "808080",
            green: "008000",
            greenyellow: "adff2f",
            grey: "808080",
            honeydew: "f0fff0",
            hotpink: "ff69b4",
            indianred: "cd5c5c",
            indigo: "4b0082",
            ivory: "fffff0",
            khaki: "f0e68c",
            lavender: "e6e6fa",
            lavenderblush: "fff0f5",
            lawngreen: "7cfc00",
            lemonchiffon: "fffacd",
            lightblue: "add8e6",
            lightcoral: "f08080",
            lightcyan: "e0ffff",
            lightgoldenrodyellow: "fafad2",
            lightgray: "d3d3d3",
            lightgreen: "90ee90",
            lightgrey: "d3d3d3",
            lightpink: "ffb6c1",
            lightsalmon: "ffa07a",
            lightseagreen: "20b2aa",
            lightskyblue: "87cefa",
            lightslategray: "789",
            lightslategrey: "789",
            lightsteelblue: "b0c4de",
            lightyellow: "ffffe0",
            lime: "0f0",
            limegreen: "32cd32",
            linen: "faf0e6",
            magenta: "f0f",
            maroon: "800000",
            mediumaquamarine: "66cdaa",
            mediumblue: "0000cd",
            mediumorchid: "ba55d3",
            mediumpurple: "9370db",
            mediumseagreen: "3cb371",
            mediumslateblue: "7b68ee",
            mediumspringgreen: "00fa9a",
            mediumturquoise: "48d1cc",
            mediumvioletred: "c71585",
            midnightblue: "191970",
            mintcream: "f5fffa",
            mistyrose: "ffe4e1",
            moccasin: "ffe4b5",
            navajowhite: "ffdead",
            navy: "000080",
            oldlace: "fdf5e6",
            olive: "808000",
            olivedrab: "6b8e23",
            orange: "ffa500",
            orangered: "ff4500",
            orchid: "da70d6",
            palegoldenrod: "eee8aa",
            palegreen: "98fb98",
            paleturquoise: "afeeee",
            palevioletred: "db7093",
            papayawhip: "ffefd5",
            peachpuff: "ffdab9",
            peru: "cd853f",
            pink: "ffc0cb",
            plum: "dda0dd",
            powderblue: "b0e0e6",
            purple: "800080",
            rebeccapurple: "663399",
            red: "f00",
            rosybrown: "bc8f8f",
            royalblue: "4169e1",
            saddlebrown: "8b4513",
            salmon: "fa8072",
            sandybrown: "f4a460",
            seagreen: "2e8b57",
            seashell: "fff5ee",
            sienna: "a0522d",
            silver: "c0c0c0",
            skyblue: "87ceeb",
            slateblue: "6a5acd",
            slategray: "708090",
            slategrey: "708090",
            snow: "fffafa",
            springgreen: "00ff7f",
            steelblue: "4682b4",
            tan: "d2b48c",
            teal: "008080",
            thistle: "d8bfd8",
            tomato: "ff6347",
            turquoise: "40e0d0",
            violet: "ee82ee",
            wheat: "f5deb3",
            white: "fff",
            whitesmoke: "f5f5f5",
            yellow: "ff0",
            yellowgreen: "9acd32"
        };

        // Make it easy to access colors via `hexNames[hex]`
        var hexNames = tinycolor.hexNames = flip(names);


        // Utilities
        // ---------

        // `{ 'name1': 'val1' }` becomes `{ 'val1': 'name1' }`
        function flip(o) {
            const flipped = {};
            for (const i in o) {
                if (o.hasOwnProperty(i)) {
                    flipped[o[i]] = i;
                }
            }
            return flipped;
        }

        // Return a valid alpha value [0,1] with all invalid values being set to 1
        function boundAlpha(a) {
            a = parseFloat(a);

            if (isNaN(a) || a < 0 || a > 1) {
                a = 1;
            }

            return a;
        }

        // Take input from [0, n] and return it as [0, 1]
        function bound01(n, max) {
            if (isOnePointZero(n)) {
                n = "100%";
            }

            const processPercent = isPercentage(n);
            n = mathMin(max, mathMax(0, parseFloat(n)));

            // Automatically convert percentage into number
            if (processPercent) {
                n = parseInt(n * max, 10) / 100;
            }

            // Handle floating point rounding errors
            if ((Math.abs(n - max) < 0.000001)) {
                return 1;
            }

            // Convert into [0, 1] range if it isn't already
            return (n % max) / parseFloat(max);
        }

        // Force a number between 0 and 1
        function clamp01(val) {
            return mathMin(1, mathMax(0, val));
        }

        // Parse a base-16 hex value into a base-10 integer
        function parseIntFromHex(val) {
            return parseInt(val, 16);
        }

        // Need to handle 1.0 as 100%, since once it is a number, there is no difference between it and 1
        // <http://stackoverflow.com/questions/7422072/javascript-how-to-detect-number-as-a-decimal-including-1-0>
        function isOnePointZero(n) {
            return typeof n == "string" && n.indexOf('.') != -1 && parseFloat(n) === 1;
        }

        // Check to see if string passed in is a percentage
        function isPercentage(n) {
            return typeof n === "string" && n.indexOf('%') != -1;
        }

        // Force a hex value to have 2 characters
        function pad2(c) {
            return c.length == 1 ? '0' + c : '' + c;
        }

        // Replace a decimal with it's percentage value
        function convertToPercentage(n) {
            if (n <= 1) {
                n = (n * 100) + "%";
            }

            return n;
        }

        // Converts a decimal to a hex value
        function convertDecimalToHex(d) {
            return Math.round(parseFloat(d) * 255).toString(16);
        }

        // Converts a hex value to a decimal
        function convertHexToDecimal(h) {
            return (parseIntFromHex(h) / 255);
        }

        const matchers = (function () {

            // <http://www.w3.org/TR/css3-values/#integers>
            const CSS_INTEGER = "[-\\+]?\\d+%?";

            // <http://www.w3.org/TR/css3-values/#number-value>
            const CSS_NUMBER = "[-\\+]?\\d*\\.\\d+%?";

            // Allow positive/negative integer/number.  Don't capture the either/or, just the entire outcome.
            const CSS_UNIT = "(?:" + CSS_NUMBER + ")|(?:" + CSS_INTEGER + ")";

            // Actual matching.
            // Parentheses and commas are optional, but not required.
            // Whitespace can take the place of commas or opening paren
            const PERMISSIVE_MATCH3 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";
            const PERMISSIVE_MATCH4 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";

            return {
                CSS_UNIT: new RegExp(CSS_UNIT),
                rgb: new RegExp("rgb" + PERMISSIVE_MATCH3),
                rgba: new RegExp("rgba" + PERMISSIVE_MATCH4),
                hsl: new RegExp("hsl" + PERMISSIVE_MATCH3),
                hsla: new RegExp("hsla" + PERMISSIVE_MATCH4),
                hsv: new RegExp("hsv" + PERMISSIVE_MATCH3),
                hsva: new RegExp("hsva" + PERMISSIVE_MATCH4),
                hex3: /^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
                hex6: /^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/,
                hex4: /^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
                hex8: /^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/
            };
        })();

        // `isValidCSSUnit`
        // Take in a single string / number and check to see if it looks like a CSS unit
        // (see `matchers` above for definition).
        function isValidCSSUnit(color) {
            return !!matchers.CSS_UNIT.exec(color);
        }

        // `stringInputToObject`
        // Permissive string parsing.  Take in a number of formats, and output an object
        // based on detected format.  Returns `{ r, g, b }` or `{ h, s, l }` or `{ h, s, v}`
        function stringInputToObject(color) {

            color = color.replace(trimLeft, '').replace(trimRight, '').toLowerCase();
            let named = false;
            if (names[color]) {
                color = names[color];
                named = true;
            }
            else if (color == 'transparent') {
                return {r: 0, g: 0, b: 0, a: 0, format: "name"};
            }

            // Try to match string input using regular expressions.
            // Keep most of the number bounding out of this function - don't worry about [0,1] or [0,100] or [0,360]
            // Just return an object and let the conversion functions handle that.
            // This way the result will be the same whether the tinycolor is initialized with string or object.
            let match;
            if ((match = matchers.rgb.exec(color))) {
                return {r: match[1], g: match[2], b: match[3]};
            }
            if ((match = matchers.rgba.exec(color))) {
                return {r: match[1], g: match[2], b: match[3], a: match[4]};
            }
            if ((match = matchers.hsl.exec(color))) {
                return {h: match[1], s: match[2], l: match[3]};
            }
            if ((match = matchers.hsla.exec(color))) {
                return {h: match[1], s: match[2], l: match[3], a: match[4]};
            }
            if ((match = matchers.hsv.exec(color))) {
                return {h: match[1], s: match[2], v: match[3]};
            }
            if ((match = matchers.hsva.exec(color))) {
                return {h: match[1], s: match[2], v: match[3], a: match[4]};
            }
            if ((match = matchers.hex8.exec(color))) {
                return {
                    r: parseIntFromHex(match[1]),
                    g: parseIntFromHex(match[2]),
                    b: parseIntFromHex(match[3]),
                    a: convertHexToDecimal(match[4]),
                    format: named ? "name" : "hex8"
                };
            }
            if ((match = matchers.hex6.exec(color))) {
                return {
                    r: parseIntFromHex(match[1]),
                    g: parseIntFromHex(match[2]),
                    b: parseIntFromHex(match[3]),
                    format: named ? "name" : "hex"
                };
            }
            if ((match = matchers.hex4.exec(color))) {
                return {
                    r: parseIntFromHex(match[1] + '' + match[1]),
                    g: parseIntFromHex(match[2] + '' + match[2]),
                    b: parseIntFromHex(match[3] + '' + match[3]),
                    a: convertHexToDecimal(match[4] + '' + match[4]),
                    format: named ? "name" : "hex8"
                };
            }
            if ((match = matchers.hex3.exec(color))) {
                return {
                    r: parseIntFromHex(match[1] + '' + match[1]),
                    g: parseIntFromHex(match[2] + '' + match[2]),
                    b: parseIntFromHex(match[3] + '' + match[3]),
                    format: named ? "name" : "hex"
                };
            }

            return false;
        }

        function validateWCAG2Parms(parms) {
            // return valid WCAG2 parms for isReadable.
            // If input parms are invalid, return {"level":"AA", "size":"small"}
            let level;

            let size;
            parms = parms || {"level": "AA", "size": "small"};
            level = (parms.level || "AA").toUpperCase();
            size = (parms.size || "small").toLowerCase();
            if (level !== "AA" && level !== "AAA") {
                level = "AA";
            }
            if (size !== "small" && size !== "large") {
                size = "small";
            }
            return {"level": level, "size": size};
        }
        window.tinycolor = tinycolor;
    })(Math);

    $.fn.pickAColor = function (options) {
        // capabilities
        const supportsTouch = 'ontouchstart' in window;

        const // don't use LS if JSON is not available
        supportsLocalStorage = 'localStorage' in window && window.localStorage !== null &&
            typeof JSON === 'object';

        const // OH NOES!
        isIELT10 = document.all && !window.atob;

        const startEvent = supportsTouch ? "touchstart.pickAColor" : "mousedown.pickAColor";
        const moveEvent = supportsTouch ? "touchmove.pickAColor" : "mousemove.pickAColor";
        const endEvent = supportsTouch ? "touchend.pickAColor" : "mouseup.pickAColor";
        const clickEvent = supportsTouch ? "touchend.pickAColor" : "click.pickAColor";
        const dragEvent = "dragging.pickAColor";
        const endDragEvent = "endDrag.pickAColor";

        // settings
        const settings = $.extend({
            showSpectrum: true,
            showSavedColors: true,
            saveColorsPerElement: false,
            fadeMenuToggle: true,
            showAdvanced: true,
            showBasicColors: true,
            showHexInput: true,
            allowBlank: false,
            inlineDropdown: false,
            basicColors: {
                white: 'fff',
                red: 'f00',
                orange: 'f60',
                yellow: 'ff0',
                green: '008000',
                blue: '00f',
                purple: '800080',
                black: '000'
            }
        }, options);

        // override showBasicColors showAdvanced isn't shown
        if (!settings.showAdvanced && !settings.showBasicColors) {
            settings.showBasicColors = true;
        }
        const useTabs = (settings.showSavedColors && settings.showAdvanced) ||
            (settings.showBasicColors && settings.showSavedColors) ||
            (settings.showBasicColors && settings.showAdvanced);


        // so much markup
        const markupAfterInput = function () {
            const $markup = $("<div>").addClass("input-group-btn");
            const $dropdownButton = $("<button type='button'>").addClass("btn btn-default color-dropdown dropdown-toggle");
            const $dropdownColorPreview = $("<span>").addClass("color-preview current-color");
            const $dropdownCaret = $("<span>").addClass("caret");
            const $dropdownContainer = $("<div>").addClass("color-menu dropdown-menu");

            if (settings.inlineDropdown) {
                $dropdownContainer.addClass("color-menu--inline");
            }
            if (!settings.showHexInput) {
                $dropdownButton.addClass("no-hex");
                $dropdownContainer.addClass("no-hex");
            }
            $markup.append($dropdownButton.append($dropdownColorPreview).append($dropdownCaret));
            if (!useTabs && !settings.showSpectrum) {
                $dropdownContainer.addClass("small");
            }
            if (useTabs) {
                const $tabContainer = $("<div>").addClass("color-menu-tabs");
                const savedColorsClass = settings.showBasicColors ? "savedColors-tab tab" : "savedColors-tab tab tab-active";
                if (settings.showBasicColors) {
                    $tabContainer.append($("<span>").addClass("basicColors-tab tab tab-active").append($("<a>").text("Basic Colors")));
                }
                if (settings.showSavedColors) {
                    $tabContainer.append($("<span>").addClass(savedColorsClass).append($("<a>").text("Saved Colors")));
                }
                if (settings.showAdvanced) {
                    $tabContainer.append($("<span>").addClass("advanced-tab tab").append($("<a>").text("Advanced")));
                }
                $dropdownContainer.append($tabContainer);
            }

            if (settings.showBasicColors) {
                const $basicColors = $("<div>").addClass("basicColors-content active-content");
                if (settings.showSpectrum) {
                    $basicColors.append($("<h6>").addClass("color-menu-instructions").text("Tap spectrum or drag band to change color"));
                }
                const $listContainer = $("<ul>").addClass("basic-colors-list");
                $.each(settings.basicColors, function (index, value) {
                    const $thisColor = $("<li>").addClass("color-item");
                    const $thisLink = $("<a>").addClass(index + " color-link");
                    const $colorPreview = $("<span>").addClass("color-preview " + index);
                    const $colorLabel = $("<span>").addClass("color-label").text(index);

                    $thisLink.append($colorPreview, $colorLabel);
                    $colorPreview.append();
                    if (value[0] !== '#') {
                        value = '#' + value;
                    }
                    $colorPreview.css('background-color', value);

                    if (settings.showSpectrum) {
                        const $thisSpectrum = $("<span>").addClass("color-box spectrum-" + index);
                        if (isIELT10) {
                            $.each([0, 1], function (i) {
                                if (value !== "fff" && index !== "000") {
                                    $thisSpectrum.append($("<span>").addClass(index + "-spectrum-" + i +
                                        " ie-spectrum"));
                                }
                            });
                        }
                        const $thisHighlightBand = $("<span>").addClass("highlight-band");
                        $.each([0, 1, 2], function () {
                            $thisHighlightBand.append($("<span>").addClass("highlight-band-stripe"));
                        });
                        $thisLink.append($thisSpectrum.append($thisHighlightBand));
                    }
                    $listContainer.append($thisColor.append($thisLink));
                });
                $dropdownContainer.append($basicColors.append($listContainer));
            }

            if (settings.showSavedColors) {
                const savedColorsActiveClass = settings.showBasicColors ? 'inactive-content' : 'active-content';
                const $savedColors = $("<div>").addClass("savedColors-content").addClass(savedColorsActiveClass);
                $savedColors.append($("<p>").addClass("saved-colors-instructions").text("Type in a color or use the spectrums to lighten or darken an existing color."));
                $dropdownContainer.append($savedColors);
            }

            if (settings.showAdvanced) {
                const advancedColorsActiveClass = settings.showBasicColors || settings.showSavedColors ? 'inactive-content' : 'active-content';
                const $advanced = $("<div>").addClass("advanced-content").addClass(advancedColorsActiveClass).append($("<h6>").addClass("advanced-instructions").text("Tap spectrum or drag band to change color"));
                const $advancedList = $("<ul>").addClass("advanced-list");
                const $hueItem = $("<li>").addClass("hue-item");
                const $hueContent = $("<span>").addClass("hue-text").text("Hue: ").append($("<span>").addClass("hue-value").text("0"));
                const $hueSpectrum = $("<span>").addClass("color-box spectrum-hue");
                if (isIELT10) {
                    $.each([0, 1, 2, 3, 4, 5, 6], function (i) {
                        $hueSpectrum.append($("<span>").addClass("hue-spectrum-" + i +
                            " ie-spectrum hue"));
                    });
                }
                const $hueHighlightBand = $("<span>").addClass("highlight-band");
                $.each([0, 1, 2], function () {
                    $hueHighlightBand.append($("<span>").addClass("highlight-band-stripe"));
                });
                $advancedList.append($hueItem.append($hueContent).append($hueSpectrum.append($hueHighlightBand)));
                const $lightnessItem = $("<li>").addClass("lightness-item");
                const $lightnessSpectrum = $("<span>").addClass("color-box spectrum-lightness");
                const $lightnessContent = $("<span>").addClass("lightness-text").text("Lightness: ").append($("<span>").addClass("lightness-value").text("50%"));
                if (isIELT10) {
                    $.each([0, 1], function (i) {
                        $lightnessSpectrum.append($("<span>").addClass("lightness-spectrum-" + i +
                            " ie-spectrum"));
                    });
                }
                const $lightnessHighlightBand = $("<span>").addClass("highlight-band");
                $.each([0, 1, 2], function () {
                    $lightnessHighlightBand.append($("<span>").addClass("highlight-band-stripe"));
                });
                $advancedList.append($lightnessItem.append($lightnessContent).append($lightnessSpectrum.append($lightnessHighlightBand)));
                const $saturationItem = $("<li>").addClass("saturation-item");
                const $saturationSpectrum = $("<span>").addClass("color-box spectrum-saturation");
                if (isIELT10) {
                    $.each([0, 1], function (i) {
                        $saturationSpectrum.append($("<span>").addClass("saturation-spectrum-" + i +
                            " ie-spectrum"));
                    });
                }
                const $saturationHighlightBand = $("<span>").addClass("highlight-band");
                $.each([0, 1, 2], function () {
                    $saturationHighlightBand.append($("<span>").addClass("highlight-band-stripe"));
                });
                const $saturationContent = $("<span>").addClass("saturation-text").text("Saturation: ").append($("<span>").addClass("saturation-value").text("100%"));
                $advancedList.append($saturationItem.append($saturationContent).append($saturationSpectrum.append($saturationHighlightBand)));
                const $previewItem = $("<li>").addClass("preview-item").append($("<span>").addClass("preview-text").text("Preview"));
                const $preview = $("<span>").addClass("color-preview advanced").append("<button class='color-select btn btn-default advanced' type='button'>Select</button>");
                $advancedList.append($previewItem.append($preview));
                $dropdownContainer.append($advanced.append($advancedList));
            }
            $markup.append($dropdownContainer);
            return $markup;
        };
        const myColorVars = {};
        const myStyleVars = {
            rowsInDropdown: 8,
            maxColsInDropdown: 2
        };
        if (settings.showSavedColors) { // if we're saving colors...
            var allSavedColors = []; // make an array for all saved colors
            if (supportsLocalStorage && localStorage.allSavedColors) { // look for them in LS
                allSavedColors = JSON.parse(localStorage.allSavedColors);
                // if there's a saved_colors cookie...
            } else if (document.cookie.match("pickAColorSavedColors-allSavedColors=")) {
                const theseCookies = document.cookie.split(";"); // split cookies into an array...
                $.each(theseCookies, function (index) { // find the savedColors cookie!
                    if (theseCookies[index].match("pickAColorSavedColors-allSavedColors=")) {
                        allSavedColors = theseCookies[index].split("=")[1].split(",");
                    }
                });
            }
        }
        const methods = {
            initialize: function (index) {
                const $thisEl = $(this);
                let $thisParent;
                let myId;
                // if there's no name on the input field, create one, then use it as the myID
                if (!$thisEl.attr("name")) {
                    $thisEl.attr("name", "pick-a-color-" + index);
                }
                myId = $thisEl.attr("name");

                // enforce .pick-a-color class on input
                $thisEl.addClass("pick-a-color");
                // convert default color to valid hex value
                if (settings.allowBlank) {
                    // convert to Hex only if the field init value is not blank
                    if (!$thisEl.val().match(/^\s+$|^$/)) {
                        myColorVars.defaultColor = tinycolor($thisEl.val()).toHex();
                        myColorVars.typedColor = myColorVars.defaultColor;
                        $thisEl.val(myColorVars.defaultColor);
                    }
                } else {
                    myColorVars.defaultColor = tinycolor($thisEl.val()).toHex();
                    myColorVars.typedColor = myColorVars.defaultColor;
                    $thisEl.val(myColorVars.defaultColor);
                }

                // wrap initializing input field with unique div and add hex symbol and post-input markup
                $($thisEl).wrap('<div class="input-group pick-a-color-markup" id="' + myId + '">');
                $thisParent = $($thisEl.parent());
                if (settings.showHexInput) {
                    $thisParent.prepend('<span class="hex-pound input-group-addon">#</span>').append(markupAfterInput());
                } else {
                    $thisParent.append(markupAfterInput());
                }
                // hide input for no input option
                if (!settings.showHexInput) {
                    $thisEl.attr("type", "hidden");
                }
                $thisEl.data('preview', $thisEl.siblings(".input-group-btn").find(".current-color"));
            },
            updatePreview: function ($thisEl) {
                if (!settings.allowBlank) {
                    myColorVars.typedColor = tinycolor($thisEl.val()).toHex();
                    $thisEl.siblings(".input-group-btn").find(".current-color").css("background-color",
                        "#" + myColorVars.typedColor);
                } else {
                    myColorVars.typedColor = $thisEl.val().match(/^\s+$|^$/) ? '' : tinycolor($thisEl.val()).toHex();
                    if (myColorVars.typedColor === '') {
                        $thisEl.siblings(".input-group-btn").find(".current-color").css("background",
                            "none");
                    } else {
                        $thisEl.siblings(".input-group-btn").find(".current-color").css("background-color",
                            "#" + myColorVars.typedColor);
                    }
                }
            },
            // must be called with apply and an arguments array like [{thisEvent}]
            pressPreviewButton: function () {
                const thisEvent = arguments[0].thisEvent;
                thisEvent.stopPropagation();
                methods.toggleDropdown(thisEvent.target);
            },
            openDropdown: function (button, menu, container) {
                $(".color-menu").each(function () { // check all the other color menus...
                    const $thisEl = $(this);
                    if ($thisEl.css("display") === "block") { // if one is open,
                        // find its color preview button
                        const thisColorPreviewButton = $thisEl.parents(".input-group-btn");
                        methods.closeDropdown(thisColorPreviewButton, $thisEl); // close it
                    }
                });
                if (settings.fadeMenuToggle && !supportsTouch) { //fades look terrible in mobile
                    $(menu).fadeIn("fast");
                } else {
                    menu.addClass('pick-a-color-markup');
                    popup.open({
                        popup: menu[0],
                        around: container[0],
                        orient: ['below', 'above'],
                        maxHeight: -1,
                        owner: options.owner
                    });
                    $(menu).show();
                }
                $(button).addClass("open");
            },
            closeDropdown: function (button, menu) {
                if (settings.fadeMenuToggle && !supportsTouch) { //fades look terrible in mobile
                    $(menu).fadeOut("fast");
                } else {
                    $(menu).css("display", "none");
                }
                $(button).removeClass("open");
            },
            // can only be called with apply. requires an arguments array like:
            // [{button, menu}]
            closeDropdownIfOpen: function () {
                const button = arguments[0].button;
                const menu = arguments[0].menu;
                if (menu.css("display") === "block") {
                    methods.closeDropdown(button, menu);
                }
            },
            toggleDropdown: function (element) {
                const $container = $(element).parents(".pick-a-color-markup");
                const $input = $container.find("input");
                const $button = $container.find(".input-group-btn");

                let $menu = $container.find(".color-menu");
                if ($menu.length === 0) {
                    $menu = $container.data('_menu');
                }
                if (!$input.is(":disabled") && $menu.css("display") === "none") {
                    methods.openDropdown($button, $menu, $container);
                } else {
                    methods.closeDropdown($button, $menu);
                }
                $container.data('_menu', $menu);
            },
            tabbable: function () {
                const $this_el = $(this);
                const $myContainer = $this_el.parents(".pick-a-color-markup");

                $this_el.click(function () {
                    const $this_el = $(this);

                    const // interpret the associated content class from the tab class and get that content div
                    contentClass = $this_el.attr("class").split(" ")[0].split("-")[0] + "-content";

                    const myContent = $this_el.parents(".dropdown-menu").find("." + contentClass);

                    const parent = $this_el.parent().parent();
                    _.each([parent.find('.basicColors-content'), parent.find('.savedColors-content'), parent.find('.advanced-content')], function (tab) {
                        tab.removeClass("active-content");
                        tab.addClass("inactive-content");
                    })
                    parent.find('.' + contentClass).addClass('active-content').removeClass('inactive-content');
                    /*
                     if (!$this_el.hasClass("tab-active")) { // make all active tabs inactive
                     $myContainer.find(".tab-active").removeClass("tab-active");
                     // toggle visibility of active content
                     $myContainer.find(".active-content").removeClass("active-content").addClass("inactive-content");
                     $this_el.addClass("tab-active"); // make current tab and content active
                     $(myContent).addClass("active-content").removeClass("inactive-content");
                     }
                     */
                });
            },
            // takes a color and the current position of the color band,
            // returns the value by which the color should be multiplied to
            // get the color currently being highlighted by the band
            getColorMultiplier: function (spectrumType, position, tab) {
                // position of the color band as a percentage of the width of the color box
                let spectrumWidth = (tab === "basic") ? parseInt($(".color-box").first().width(), 10) :
                    parseInt($(".advanced-list").find(".color-box").first().width(), 10);
                if (spectrumWidth === 0) { // in case the width isn't set correctly
                    if (tab === "basic") {
                        spectrumWidth = supportsTouch ? 160 : 200;
                    } else {
                        spectrumWidth = supportsTouch ? 160 : 300;
                    }
                }
                const halfSpectrumWidth = spectrumWidth / 2;
                const percentOfBox = position / spectrumWidth;

                // for spectrums that lighten and darken, recalculate percent of box relative
                // to the half of spectrum the highlight band is currently in
                if (spectrumType === "bidirectional") {
                    return (percentOfBox <= 0.5) ?
                        (1 - (position / halfSpectrumWidth)) / 2 :
                        -((position - halfSpectrumWidth) / halfSpectrumWidth) / 2;
                    // now that we're treating each half as an individual spectrum, both are darkenRight
                } else {
                    return (spectrumType === "darkenRight") ? -(percentOfBox / 2) : (percentOfBox / 2);
                }
            },
            // modifyHSLLightness based on ligten/darken in LESS
            // https://github.com/cloudhead/less.js/blob/master/dist/less-1.3.3.js#L1763
            modifyHSLLightness: function (HSL, multiplier) {
                const hsl = HSL;
                hsl.l += multiplier;
                hsl.l = Math.min(Math.max(0, hsl.l), 1);
                return tinycolor(hsl).toHslString();
            },
            // defines the area within which an element can be moved
            getMoveableArea: function ($element) {
                const dimensions = {};
                const $elParent = $element.parent();
                const myWidth = $element.outerWidth();

                const // don't include borders for parent width
                parentWidth = $elParent.width();

                const parentLocation = $elParent.offset();
                dimensions.minX = parentLocation.left;
                dimensions.maxX = parentWidth - myWidth; //subtract myWidth to avoid pushing out of parent
                return dimensions;
            },
            moveHighlightBand: function ($highlightBand, moveableArea, e) {
                const hbWidth = $(".highlight-band").first().outerWidth();
                const threeFourthsHBWidth = hbWidth * 0.75;

                const // find the mouse!
                mouseX = supportsTouch ? e.originalEvent.pageX : e.pageX;

                let // mouse position relative to width of highlight-band
                newPosition = mouseX - moveableArea.minX - threeFourthsHBWidth;

                // don't move beyond moveable area
                newPosition = Math.max(0, (Math.min(newPosition, moveableArea.maxX)));
                $highlightBand.css("position", "absolute");
                $highlightBand.css("left", newPosition);
            },
            horizontallyDraggable: function () {
                $(this).on(startEvent, function (event) {
                    event.preventDefault();
                    const $this_el = $(event.delegateTarget);
                    $this_el.css("cursor", "-webkit-grabbing");
                    $this_el.css("cursor", "-moz-grabbing");
                    const dimensions = methods.getMoveableArea($this_el);

                    $(document).on(moveEvent, function (e) {
                        $this_el.trigger(dragEvent);
                        methods.moveHighlightBand($this_el, dimensions, e);
                    }).on(endEvent, function () {
                        $(document).off(moveEvent); // for desktop
                        $(document).off(dragEvent);
                        $this_el.css("cursor", "-webkit-grab");
                        $this_el.css("cursor", "-moz-grab");
                        $this_el.trigger(endDragEvent);
                        $(document).off(endEvent);
                    });
                }).on(endEvent, function (event) {
                    event.stopPropagation();
                    $(document).off(moveEvent); // for mobile
                    $(document).off(dragEvent);
                });
            },
            modifyHighlightBand: function ($highlightBand, colorMultiplier, spectrumType) {
                const darkGrayHSL = {h: 0, s: 0, l: 0.05};
                const bwMidHSL = {h: 0, s: 0, l: 0.5};

                const // change to color of band is opposite of change to color of spectrum
                hbColorMultiplier = -colorMultiplier;

                const // needs to be either black or white
                hbsColorMultiplier = hbColorMultiplier * 10;

                const $hbStripes = $highlightBand.find(".highlight-band-stripe");

                const newBandColor = (spectrumType === "lightenRight") ?
                    methods.modifyHSLLightness(bwMidHSL, hbColorMultiplier) :
                    methods.modifyHSLLightness(darkGrayHSL, hbColorMultiplier);

                $highlightBand.css("border-color", newBandColor);
                $hbStripes.css("background-color", newBandColor);
            },
            // must be called with apply and expects an arguments array like
            // [{type: "basic"}] or [{type: "advanced", hsl: {h,s,l}}]
            calculateHighlightedColor: function () {
                const $thisEl = $(this);
                const $thisParent = $thisEl.parent();
                const hbWidth = $(".highlight-band").first().outerWidth();
                const halfHBWidth = hbWidth / 2;
                const tab = arguments[0].type;
                let spectrumType;
                let colorHsl;
                let currentHue;
                let currentSaturation;
                let $advancedPreview;
                let $saturationSpectrum;
                let $hueSpectrum;
                let $lightnessValue;

                if (tab === "basic") {
                    // get the class of the parent color box and slice off "spectrum"
                    const colorName = $thisParent.attr("class").split("-")[2];

                    const colorHex = settings.basicColors[colorName];
                    colorHsl = tinycolor(colorHex).toHsl();
                    switch (colorHex) {
                        case "fff":
                            spectrumType = "darkenRight";
                            break;
                        case "000":
                            spectrumType = "lightenRight";
                            break;
                        default:
                            spectrumType = "bidirectional";
                    }
                } else {
                    // re-set current L value to 0.5 because the color multiplier ligtens
                    // and darkens against the baseline value
                    const $advancedContainer = $thisEl.parents(".advanced-list");
                    currentSaturation = arguments[0].hsl.s;
                    $hueSpectrum = $advancedContainer.find(".spectrum-hue");
                    currentHue = arguments[0].hsl.h;
                    $saturationSpectrum = $advancedContainer.find(".spectrum-saturation");
                    $lightnessValue = $advancedContainer.find(".lightness-value");
                    $advancedPreview = $advancedContainer.find(".color-preview");
                    colorHsl = {"h": arguments[0].hsl.h, "l": 0.5, "s": arguments[0].hsl.s};
                    spectrumType = "bidirectional";
                }


                // midpoint of the current left position of the color band
                const highlightBandLocation = parseInt($thisEl.css("left"), 10) + halfHBWidth;

                const colorMultiplier = methods.getColorMultiplier(spectrumType, highlightBandLocation, tab);
                const highlightedColor = methods.modifyHSLLightness(colorHsl, colorMultiplier);
                const highlightedHex = "#" + tinycolor(highlightedColor).toHex();
                const highlightedLightnessString = highlightedColor.split("(")[1].split(")")[0].split(",")[2];
                const highlightedLightness = (parseInt(highlightedLightnessString.split("%")[0], 10)) / 100;

                if (tab === "basic") {
                    $thisParent.siblings(".color-preview").css("background-color", highlightedHex);
                    // replace the color label with a 'select' button
                    $thisParent.prev('.color-label').replaceWith(
                        '<button class="color-select btn btn-default" type="button">Select</button>');
                    if (spectrumType !== "darkenRight") {
                        methods.modifyHighlightBand($thisEl, colorMultiplier, spectrumType);
                    }
                } else {
                    $advancedPreview.css("background-color", highlightedHex);
                    $lightnessValue.text(highlightedLightnessString);
                    methods.updateSaturationStyles($saturationSpectrum, currentHue, highlightedLightness);
                    methods.updateHueStyles($hueSpectrum, currentSaturation, highlightedLightness);
                    methods.modifyHighlightBand($(".advanced-content .highlight-band"), colorMultiplier, spectrumType);
                }
                return (tab === "basic") ? tinycolor(highlightedColor).toHex() : highlightedLightness;
            },
            updateSavedColorPreview: function (elements) {
                $.each(elements, function (index) {
                    const $this_el = $(elements[index]);
                    const thisColor = $this_el.attr("class");
                    $this_el.find(".color-preview").css("background-color", thisColor);
                });
            },
            updateSavedColorMarkup: function ($savedColorsContent, mySavedColors) {
                mySavedColors = mySavedColors ? mySavedColors : allSavedColors;
                if (settings.showSavedColors && mySavedColors.length > 0) {
                    if (!settings.saveColorsPerElement) {
                        $savedColorsContent = $(".savedColors-content");
                        mySavedColors = allSavedColors;
                    }

                    const maxSavedColors = myStyleVars.rowsInDropdown * myStyleVars.maxColsInDropdown;
                    mySavedColors = mySavedColors.slice(0, maxSavedColors);

                    const $col0 = $("<ul>").addClass("saved-color-col 0");
                    const $col1 = $("<ul>").addClass("saved-color-col 1");

                    $.each(mySavedColors, function (index, value) {
                        const $this_li = $("<li>").addClass("color-item");
                        const $this_link = $("<a>").addClass(value);
                        $this_link.append($("<span>").addClass("color-preview"));
                        $this_link.append($("<span>").addClass("color-label").text(value));
                        $this_li.append($this_link);
                        if (index % 2 === 0) {
                            $col0.append($this_li);
                        } else {
                            $col1.append($this_li);
                        }
                    });

                    $savedColorsContent.html($col0);
                    $savedColorsContent.append($col1);
                    const savedColorLinks = $($savedColorsContent).find("a");
                    methods.updateSavedColorPreview(savedColorLinks);
                }
            },
            setSavedColorsCookie: function (savedColors, savedColorsDataAttr) {
                const now = new Date();
                const tenYearsInMilliseconds = 315360000000;
                let expiresOn = new Date(now.getTime() + tenYearsInMilliseconds);
                expiresOn = expiresOn.toGMTString();

                if (typeof savedColorsDataAttr === "undefined") {
                    document.cookie = "pickAColorSavedColors-allSavedColors=" + savedColors +
                        ";expires=" + expiresOn;
                } else {
                    document.cookie = "pickAColorSavedColors-" + savedColorsDataAttr + "=" +
                        savedColors + "; expires=" + expiresOn;
                }
            },
            saveColorsToLocalStorage: function (savedColors, savedColorsDataAttr) {
                if (supportsLocalStorage) {
                    // if there is no data attribute, save to allSavedColors
                    if (typeof savedColorsDataAttr === "undefined") {
                        try {
                            localStorage.allSavedColors = JSON.stringify(savedColors);
                        }
                        catch (e) {
                            localStorage.clear();
                        }
                    } else { // otherwise save to a data attr-specific item
                        try {
                            localStorage["pickAColorSavedColors-" + savedColorsDataAttr] =
                                JSON.stringify(savedColors);
                        }
                        catch (e) {
                            localStorage.clear();
                        }
                    }
                } else {
                    methods.setSavedColorsCookie(savedColors, savedColorsDataAttr);
                }
            },
            removeFromArray: function (array, item) {
                if ($.inArray(item, array) !== -1) { // make sure it's in there
                    array.splice($.inArray(item, array), 1);
                }
            },
            updateSavedColors: function (color, savedColors, savedColorsDataAttr) {
                methods.removeFromArray(savedColors, color);
                savedColors.unshift(color);
                methods.saveColorsToLocalStorage(savedColors, savedColorsDataAttr);
            },
            // when settings.saveColorsPerElement, colors are saved to both mySavedColors and
            // allSavedColors so they will be avail to color pickers with no savedColorsDataAttr
            addToSavedColors: function (color, mySavedColorsInfo, $mySavedColorsContent) {
                if (settings.showSavedColors && color !== undefined) { // make sure we're saving colors
                    if (color[0] != "#") {
                        color = "#" + color;
                    }
                    methods.updateSavedColors(color, allSavedColors);
                    if (settings.saveColorsPerElement) {
                        // if we're saving colors per element...
                        const mySavedColors = mySavedColorsInfo.colors;

                        const dataAttr = mySavedColorsInfo.dataAttr;
                        methods.updateSavedColors(color, mySavedColors, dataAttr);
                        methods.updateSavedColorMarkup($mySavedColorsContent, mySavedColors);
                    } else { // if not saving per element, update markup with allSavedColors
                        methods.updateSavedColorMarkup($mySavedColorsContent, allSavedColors);
                    }
                }
            },
            // handles selecting a color from the basic menu of colors.
            // must be called with apply and relies on an arguments array like:
            // [{els, savedColorsInfo}]
            selectFromBasicColors: function () {
                let selectedColor = $(this).find("span:first").css("background-color");
                const myElements = arguments[0].els;
                const mySavedColorsInfo = arguments[0].savedColorsInfo;
                selectedColor = tinycolor(selectedColor).toHex();
                $(myElements.thisEl).val(selectedColor);
                $(myElements.thisEl).trigger("change");
                methods.updatePreview(myElements.thisEl);
                methods.addToSavedColors(selectedColor, mySavedColorsInfo, myElements.savedColorsContent);
                methods.closeDropdown(myElements.colorPreviewButton, myElements.colorMenu); // close the dropdown
            },
            // handles user clicking or tapping on spectrum to select a color.
            // must be called with apply and relies on an arguments array like:
            // [{thisEvent, savedColorsInfo, els, mostRecentClick}]
            tapSpectrum: function () {
                const thisEvent = arguments[0].thisEvent;
                const mySavedColorsInfo = arguments[0].savedColorsInfo;
                const myElements = arguments[0].els;
                const mostRecentClick = arguments[0].mostRecentClick;
                thisEvent.stopPropagation(); // stop this click from closing the dropdown
                const $highlightBand = $(this).find(".highlight-band");
                const dimensions = methods.getMoveableArea($highlightBand);
                if (supportsTouch) {
                    methods.moveHighlightBand($highlightBand, dimensions, mostRecentClick);
                } else {
                    methods.moveHighlightBand($highlightBand, dimensions, thisEvent);
                }
                const highlightedColor = methods.calculateHighlightedColor.apply($highlightBand, [{type: "basic"}]);
                methods.addToSavedColors(highlightedColor, mySavedColorsInfo, myElements.savedColorsContent);
                // update touch instructions
                myElements.touchInstructions.html("Press 'select' to choose this color");
            },
            // bind to mousedown/touchstart, execute provied function if the top of the
            // window has not moved when there is a mouseup/touchend
            // must be called with apply and an arguments array like:
            // [{thisFunction, theseArguments}]
            executeUnlessScrolled: function () {
                const thisFunction = arguments[0].thisFunction;
                const theseArguments = arguments[0].theseArguments;
                let windowTopPosition;
                let mostRecentClick;
                $(this).on(startEvent, function (e) {
                    windowTopPosition = $(window).scrollTop(); // save to see if user is scrolling in mobile
                    mostRecentClick = e;
                }).on(clickEvent, function (event) {
                    const distance = windowTopPosition - $(window).scrollTop();
                    if (supportsTouch && (Math.abs(distance) > 0)) {
                        return false;
                    } else {
                        theseArguments.thisEvent = event; //add the click event to the arguments object
                        theseArguments.mostRecentClick = mostRecentClick; //add start event to the arguments object
                        thisFunction.apply($(this), [theseArguments]);
                    }
                });
            },
            updateSaturationStyles: function (spectrum, hue, lightness) {
                const lightnessString = (lightness * 100).toString() + "%";
                const start = "#" + tinycolor("hsl(" + hue + ",0%," + lightnessString).toHex();
                const mid = "#" + tinycolor("hsl(" + hue + ",50%," + lightnessString).toHex();
                const end = "#" + tinycolor("hsl(" + hue + ",100%," + lightnessString).toHex();
                let fullSpectrumString = "";

                const standard = $.each(["-webkit-linear-gradient", "-o-linear-gradient"], function (index, value) {
                    fullSpectrumString += "background-image: " + value + "(left, " + start + " 0%, " + mid + " 50%, " + end + " 100%);";
                });

                const ieSpectrum0 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + start + "', endColorstr='" +
                    mid + "', GradientType=1)";

                const ieSpectrum1 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + mid + "', endColorstr='" +
                    end + "', GradientType=1)";

                fullSpectrumString =
                    "background-image: -moz-linear-gradient(left center, " + start + " 0%, " + mid + " 50%, " + end + " 100%);" +
                    "background-image: linear-gradient(to right, " + start + " 0%, " + mid + " 50%, " + end + " 100%); " +
                    "background-image: -webkit-gradient(linear, left top, right top," +
                    "color-stop(0, " + start + ")," + "color-stop(0.5, " + mid + ")," + "color-stop(1, " + end + "));" +
                    fullSpectrumString;
                if (isIELT10) {
                    const $spectrum0 = $(spectrum).find(".saturation-spectrum-0");
                    const $spectrum1 = $(spectrum).find(".saturation-spectrum-1");
                    $spectrum0.css("filter", ieSpectrum0);
                    $spectrum1.css("filter", ieSpectrum1);
                } else {
                    spectrum.attr("style", fullSpectrumString);
                }
            },
            updateLightnessStyles: function (spectrum, hue, saturation) {
                const saturationString = (saturation * 100).toString() + "%";
                const start = "#" + tinycolor("hsl(" + hue + "," + saturationString + ",100%)").toHex();
                const mid = "#" + tinycolor("hsl(" + hue + "," + saturationString + ",50%)").toHex();
                const end = "#" + tinycolor("hsl(" + hue + "," + saturationString + ",0%)").toHex();
                let fullSpectrumString = "";

                const standard = $.each(["-webkit-linear-gradient", "-o-linear-gradient"], function (index, value) {
                    fullSpectrumString += "background-image: " + value + "(left, " + start + " 0%, " + mid + " 50%, "
                        + end + " 100%);";
                });

                const ieSpectrum0 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + start + "', endColorstr='" +
                    mid + "', GradientType=1)";

                const ieSpectrum1 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + mid + "', endColorstr='" +
                    end + "', GradientType=1)";

                fullSpectrumString =
                    "background-image: -moz-linear-gradient(left center, " + start + " 0%, " + mid + " 50%, " + end + " 100%); " +
                    "background-image: linear-gradient(to right, " + start + " 0%, " + mid + " 50%, " + end + " 100%); " +
                    "background-image: -webkit-gradient(linear, left top, right top," +
                    " color-stop(0, " + start + ")," + " color-stop(0.5, " + mid + ")," + " color-stop(1, " + end + ")); " +
                    fullSpectrumString;
                if (isIELT10) {
                    const $spectrum0 = $(spectrum).find(".lightness-spectrum-0");
                    const $spectrum1 = $(spectrum).find(".lightness-spectrum-1");
                    $spectrum0.css("filter", ieSpectrum0);
                    $spectrum1.css("filter", ieSpectrum1);
                } else {
                    spectrum.attr("style", fullSpectrumString);
                }
            },
            updateHueStyles: function (spectrum, saturation, lightness) {
                const saturationString = (saturation * 100).toString() + "%";
                const lightnessString = (lightness * 100).toString() + "%";
                const color1 = "#" + tinycolor("hsl(0," + saturationString + "," + lightnessString + ")").toHex();
                const color2 = "#" + tinycolor("hsl(60," + saturationString + "," + lightnessString + ")").toHex();
                const color3 = "#" + tinycolor("hsl(120," + saturationString + "," + lightnessString + ")").toHex();
                const color4 = "#" + tinycolor("hsl(180," + saturationString + "," + lightnessString + ")").toHex();
                const color5 = "#" + tinycolor("hsl(240," + saturationString + "," + lightnessString + ")").toHex();
                const color6 = "#" + tinycolor("hsl(300," + saturationString + "," + lightnessString + ")").toHex();
                const color7 = "#" + tinycolor("hsl(0," + saturationString + "," + lightnessString + ")").toHex();

                const ieSpectrum0 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + color1 + "', endColorstr='" +
                    color2 + "', GradientType=1)";

                const ieSpectrum1 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + color2 + "', endColorstr='" +
                    color3 + "', GradientType=1)";

                const ieSpectrum2 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + color3 + "', endColorstr='" +
                    color4 + "', GradientType=1)";

                const ieSpectrum3 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + color4 + "', endColorstr='" +
                    color5 + "', GradientType=1)";

                const ieSpectrum4 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + color5 + "', endColorstr='" +
                    color6 + "', GradientType=1)";

                const ieSpectrum5 = "progid:DXImageTransform.Microsoft.gradient(startColorstr='" + color6 + "', endColorstr='" +
                    color7 + "', GradientType=1)";

                let fullSpectrumString = "";
                $.each(["-webkit-linear-gradient", "-o-linear-gradient"], function (index, value) {
                    fullSpectrumString += "background-image: " + value + "(left, " + color1 + " 0%, " + color2 + " 17%, " +
                        color3 + " 24%, " + color4 + " 51%, " + color5 + " 68%, " + color6 + " 85%, " + color7 + " 100%);";
                });
                fullSpectrumString += "background-image: -webkit-gradient(linear, left top, right top," +
                    "color-stop(0%, " + color1 + ")," + "color-stop(17%, " + color2 + ")," + "color-stop(34%, " + color3 + ")," +
                    "color-stop(51%, " + color4 + ")," + "color-stop(68%, " + color5 + ")," + "color-stop(85%, " + color6 + ")," +
                    "color-stop(100%, " + color7 + "));" +
                    "background-image: linear-gradient(to right, " + color1 + " 0%, " + color2 + " 17%, " + color3 + " 24%," +
                    color4 + " 51%," + color5 + " 68%," + color6 + " 85%," + color7 + " 100%); " +
                    "background-image: -moz-linear-gradient(left center, " +
                    color1 + " 0%, " + color2 + " 17%, " + color3 + " 24%, " + color4 + " 51%, " + color5 + " 68%, " +
                    color6 + " 85%, " + color7 + " 100%);";
                if (isIELT10) {
                    const $spectrum0 = $(spectrum).find(".hue-spectrum-0");
                    const $spectrum1 = $(spectrum).find(".hue-spectrum-1");
                    const $spectrum2 = $(spectrum).find(".hue-spectrum-2");
                    const $spectrum3 = $(spectrum).find(".hue-spectrum-3");
                    const $spectrum4 = $(spectrum).find(".hue-spectrum-4");
                    const $spectrum5 = $(spectrum).find(".hue-spectrum-5");
                    $spectrum0.css("filter", ieSpectrum0);
                    $spectrum1.css("filter", ieSpectrum1);
                    $spectrum2.css("filter", ieSpectrum2);
                    $spectrum3.css("filter", ieSpectrum3);
                    $spectrum4.css("filter", ieSpectrum4);
                    $spectrum5.css("filter", ieSpectrum5);
                } else {
                    spectrum.attr("style", fullSpectrumString);

                }
            },
            // takes the position of a highlight band on the hue spectrum and finds highlighted hue
            // and updates the background of the lightness and saturation spectrums
            // relies on apply and an arguments array like [{h, s, l}]
            getHighlightedHue: function () {
                const $thisEl = $(this);
                const hbWidth = $thisEl.outerWidth();
                const halfHBWidth = hbWidth / 2;
                const position = parseInt($thisEl.css("left"), 10) + halfHBWidth;
                const $advancedContainer = $thisEl.parents(".advanced-list");
                const $advancedPreview = $advancedContainer.find(".color-preview");
                const $lightnessSpectrum = $advancedContainer.find(".spectrum-lightness");
                const $saturationSpectrum = $advancedContainer.find(".spectrum-saturation");
                let spectrumWidth = parseInt($advancedContainer.find(".color-box").first().width(), 10);
                const $hueValue = $advancedContainer.find(".hue-value");
                const currentLightness = arguments[0].l;
                const currentSaturation = arguments[0].s;
                const saturationString = (currentSaturation * 100).toString() + "%";
                const lightnessString = (currentLightness * 100).toString() + "%";

                if (spectrumWidth === 0) { // in case the width isn't set correctly
                    spectrumWidth = supportsTouch ? 160 : 300;
                }

                const hue = Math.floor((position / spectrumWidth) * 360);
                let color = "hsl(" + hue + "," + saturationString + "," + lightnessString + ")";
                color = "#" + tinycolor(color).toHex();

                $advancedPreview.css("background-color", color);
                $hueValue.text(hue);
                methods.updateLightnessStyles($lightnessSpectrum, hue, currentSaturation);
                methods.updateSaturationStyles($saturationSpectrum, hue, currentLightness);
                return hue;
            },

            // relies on apply and an arguments array like [{h, s, l}]
            getHighlightedSaturation: function () {
                const $thisEl = $(this);
                const hbWidth = $thisEl.outerWidth();
                const halfHBWidth = hbWidth / 2;
                const position = parseInt($thisEl.css("left"), 10) + halfHBWidth;
                const $advancedContainer = $thisEl.parents(".advanced-list");
                const $advancedPreview = $advancedContainer.find(".color-preview");
                const $lightnessSpectrum = $advancedContainer.find(".spectrum-lightness");
                const $hueSpectrum = $advancedContainer.find(".spectrum-hue");
                const $saturationValue = $advancedContainer.find(".saturation-value");
                let spectrumWidth = parseInt($advancedContainer.find(".color-box").first().width(), 10);
                const currentLightness = arguments[0].l;
                const lightnessString = (currentLightness * 100).toString() + "%";
                const currentHue = arguments[0].h;

                if (spectrumWidth === 0) { // in case the width isn't set correctly
                    spectrumWidth = supportsTouch ? 160 : 300;
                }

                const saturation = position / spectrumWidth;
                const saturationString = Math.round((saturation * 100)).toString() + "%";
                let color = "hsl(" + currentHue + "," + saturationString + "," + lightnessString + ")";
                color = "#" + tinycolor(color).toHex();

                $advancedPreview.css("background-color", color);
                $saturationValue.text(saturationString);
                methods.updateLightnessStyles($lightnessSpectrum, currentHue, saturation);
                methods.updateHueStyles($hueSpectrum, saturation, currentLightness);
                return saturation;
            },

            updateAdvancedInstructions: function (instructionsEl) {
                instructionsEl.html("Press the color preview to choose this color");
            }

        };

        return this.each(function (index) {
            methods.initialize.apply(this, [index]);
            // commonly used DOM elements for each color picker
            const myElements = {
                thisEl: $(this),
                thisWrapper: $(this).parent(),
                colorTextInput: $(this).find("input"),
                colorMenuLinks: $(this).parent().find(".color-menu li a"),
                colorPreviewButton: $(this).parent().find(".input-group-btn"),
                colorMenu: $(this).parent().find(".color-menu"),
                colorSpectrums: $(this).parent().find(".color-box"),
                basicSpectrums: $(this).parent().find(".basicColors-content .color-box"),
                touchInstructions: $(this).parent().find(".color-menu-instructions"),
                advancedInstructions: $(this).parent().find(".advanced-instructions"),
                highlightBands: $(this).parent().find(".highlight-band"),
                basicHighlightBands: $(this).parent().find(".basicColors-content .highlight-band")
            };

            let // for storing click events when needed
            mostRecentClick;

            let // for storing the position of the top of the window when needed
            windowTopPosition;

            let advancedStatus;
            let mySavedColorsInfo;

            if (useTabs) {
                myElements.tabs = myElements.thisWrapper.find(".tab");
            }

            if (settings.showSavedColors) {
                myElements.savedColorsContent = myElements.thisWrapper.find(".savedColors-content");
                if (settings.saveColorsPerElement) { // when saving colors for each color picker...
                    mySavedColorsInfo = {
                        colors: [],
                        dataObj: $(this).data()
                    };
                    $.each(mySavedColorsInfo.dataObj, function (key) {
                        mySavedColorsInfo.dataAttr = key;
                    });

                    // get this picker's colors from local storage if possible
                    if (supportsLocalStorage && localStorage["pickAColorSavedColors-" +
                        mySavedColorsInfo.dataAttr]) {
                        mySavedColorsInfo.colors = JSON.parse(localStorage["pickAColorSavedColors-" +
                        mySavedColorsInfo.dataAttr]);

                        // otherwise, get them from cookies
                    } else if (document.cookie.match("pickAColorSavedColors-" +
                            mySavedColorsInfo.dataAttr)) {
                        const theseCookies = document.cookie.split(";"); // an array of cookies...
                        for (let i = 0; i < theseCookies.length; i++) {
                            if (theseCookies[i].match(mySavedColorsInfo.dataAttr)) {
                                mySavedColorsInfo.colors = theseCookies[i].split("=")[1].split(",");
                            }
                        }

                    } else { // if no data-attr specific colors are in local storage OR cookies...
                        mySavedColorsInfo.colors = allSavedColors; // use mySavedColors
                    }
                }
            }
            if (settings.showAdvanced) {
                advancedStatus = {
                    h: 0,
                    s: 1,
                    l: 0.5
                };

                myElements.advancedSpectrums = myElements.thisWrapper.find(".advanced-list").find(".color-box");
                myElements.advancedHighlightBands = myElements.thisWrapper.find(".advanced-list").find(".highlight-band");
                myElements.hueSpectrum = myElements.thisWrapper.find(".spectrum-hue");
                myElements.lightnessSpectrum = myElements.thisWrapper.find(".spectrum-lightness");
                myElements.saturationSpectrum = myElements.thisWrapper.find(".spectrum-saturation");
                myElements.hueHighlightBand = myElements.thisWrapper.find(".spectrum-hue .highlight-band");
                myElements.lightnessHighlightBand = myElements.thisWrapper.find(".spectrum-lightness .highlight-band");
                myElements.saturationHighlightBand = myElements.thisWrapper.find(".spectrum-saturation .highlight-band");
                myElements.advancedPreview = myElements.thisWrapper.find(".advanced-content .color-preview");
            }

            // add the default color to saved colors
            methods.addToSavedColors(myColorVars.defaultColor, mySavedColorsInfo, myElements.savedColorsContent);
            methods.updatePreview(myElements.thisEl);

            //input field focus: clear content
            // input field blur: update preview, restore previous content if no value entered

            myElements.thisEl.focus(function () {
                const $thisEl = $(this);
                myColorVars.typedColor = $thisEl.val(); // update with the current
                if (!settings.allowBlank) {
                    $thisEl.val(""); //clear the field on focus
                }
                methods.toggleDropdown(myElements.colorPreviewButton, myElements.ColorMenu);
            }).blur(function () {
                const $thisEl = $(this);
                myColorVars.newValue = $thisEl.val(); // on blur, check the field's value
                // if the field is empty, put the original value back in the field
                if (myColorVars.newValue.match(/^\s+$|^$/)) {
                    if (!settings.allowBlank) {
                        $thisEl.val(myColorVars.typedColor);
                    }
                } else { // otherwise...
                    myColorVars.newValue = tinycolor(myColorVars.newValue).toHex(); // convert to hex
                    $thisEl.val(myColorVars.newValue); // put the new value in the field
                    // save to saved colors
                    methods.addToSavedColors(myColorVars.newValue, mySavedColorsInfo, myElements.savedColorsContent);
                }
                methods.toggleDropdown(myElements.colorPreviewButton, myElements.ColorMenu);
                methods.updatePreview($thisEl); // update preview
            });

            // toggle visibility of dropdown menu when you click or press the preview button
            methods.executeUnlessScrolled.apply(myElements.colorPreviewButton,
                [{"thisFunction": methods.pressPreviewButton, "theseArguments": {}}]);

            // any touch or click outside of a dropdown should close all dropdowns
            methods.executeUnlessScrolled.apply($(document), [{
                "thisFunction": methods.closeDropdownIfOpen,
                "theseArguments": {"button": myElements.colorPreviewButton, "menu": myElements.colorMenu}
            }]);

            // prevent click/touchend to color-menu or color-text input from closing dropdown

            myElements.colorMenu.on(clickEvent, function (e) {
                e.stopPropagation();
            });
            myElements.thisEl.on(clickEvent, function (e) {
                e.stopPropagation();
            });

            // update field and close menu when selecting from basic dropdown
            methods.executeUnlessScrolled.apply(myElements.colorMenuLinks, [{
                "thisFunction": methods.selectFromBasicColors,
                "theseArguments": {"els": myElements, "savedColorsInfo": mySavedColorsInfo}
            }]);

            if (useTabs) { // make tabs tabable
                methods.tabbable.apply(myElements.tabs);
            }

            if (settings.showSpectrum || settings.showAdvanced) {
                methods.horizontallyDraggable.apply(myElements.highlightBands);
            }

            // for using the light/dark spectrums
            if (settings.showSpectrum) {
                // move the highlight band when you click on a spectrum
                methods.executeUnlessScrolled.apply(myElements.basicSpectrums, [{
                    "thisFunction": methods.tapSpectrum,
                    "theseArguments": {"savedColorsInfo": mySavedColorsInfo, "els": myElements}
                }]);

                $(myElements.basicHighlightBands).on(dragEvent, function (event) {
                    methods.calculateHighlightedColor.apply(this, [{type: "basic"}]);
                }).on(endDragEvent, function (event) {
                    const $thisEl = event.delegateTarget;
                    const finalColor = methods.calculateHighlightedColor.apply($thisEl, [{type: "basic"}]);
                    methods.addToSavedColors(finalColor, mySavedColorsInfo, myElements.savedColorsContent);
                });

            }

            if (settings.showAdvanced) {
                // for dragging advanced sliders
                $(myElements.hueHighlightBand).on(dragEvent, function (event) {
                    advancedStatus.h = methods.getHighlightedHue.apply(this, [advancedStatus]);
                });

                $(myElements.lightnessHighlightBand).on(dragEvent, function () {
                    methods.calculateHighlightedColor.apply(this, [{"type": "advanced", "hsl": advancedStatus}]);
                }).on(endEvent, function () {
                    advancedStatus.l = methods.calculateHighlightedColor.apply(this, [{
                        "type": "advanced",
                        "hsl": advancedStatus
                    }]);
                });

                $(myElements.saturationHighlightBand).on(dragEvent, function () {
                    methods.getHighlightedSaturation.apply(this, [advancedStatus]);
                }).on(endDragEvent, function () {
                    advancedStatus.s = methods.getHighlightedSaturation.apply(this, [advancedStatus]);
                });

                $(myElements.advancedHighlightBand).on(endDragEvent, function () {
                    methods.updateAdvancedInstructions(myElements.advancedInstructions);
                });

                // for clicking/tapping advanced sliders
                $(myElements.lightnessSpectrum).click(function (event) {
                    event.stopPropagation(); // stop this click from closing the dropdown
                    const $highlightBand = $(this).find(".highlight-band");
                    const dimensions = methods.getMoveableArea($highlightBand);
                    methods.moveHighlightBand($highlightBand, dimensions, event);
                    advancedStatus.l = methods.calculateHighlightedColor.apply($highlightBand, [{
                        "type": "advanced",
                        "hsl": advancedStatus
                    }]);
                });

                $(myElements.hueSpectrum).click(function (event) {
                    event.stopPropagation(); // stop this click from closing the dropdown
                    const $highlightBand = $(this).find(".highlight-band");
                    const dimensions = methods.getMoveableArea($highlightBand);
                    methods.moveHighlightBand($highlightBand, dimensions, event);
                    advancedStatus.h = methods.getHighlightedHue.apply($highlightBand, [advancedStatus]);
                });

                $(myElements.saturationSpectrum).click(function (event) {
                    event.stopPropagation(); // stop this click from closing the dropdown
                    const $highlightBand = $(this).find(".highlight-band");
                    const dimensions = methods.getMoveableArea($highlightBand);
                    methods.moveHighlightBand($highlightBand, dimensions, event);
                    advancedStatus.s = methods.getHighlightedSaturation.apply($highlightBand, [advancedStatus]);
                });

                $(myElements.advancedSpectrums).click(function () {
                    methods.updateAdvancedInstructions(myElements.advancedInstructions);
                });

                //for clicking advanced color preview
                $(myElements.advancedPreview).click(function () {
                    const selectedColor = tinycolor($(this).css("background-color")).toHex();
                    $(myElements.thisEl).val(selectedColor);
                    $(myElements.thisEl).trigger("change");
                    methods.updatePreview(myElements.thisEl);
                    methods.addToSavedColors(selectedColor, mySavedColorsInfo, myElements.savedColorsContent);
                    methods.closeDropdown(myElements.colorPreviewButton, myElements.colorMenu); // close the dropdown
                });
            }

            // for using saved colors
            if (settings.showSavedColors) {
                // make the links in saved colors work
                $(myElements.savedColorsContent).click(function (event) {
                    const $thisEl = $(event.target);

                    // make sure click happened on a link or span
                    if ($thisEl.is("SPAN") || $thisEl.is("A")) {
                        //grab the color the link's class or span's parent link's class
                        const selectedColor = $thisEl.is("SPAN") ?
                            $thisEl.parent().attr("class").split("#")[1] :
                            $thisEl.attr("class").split("#")[1];
                        $(myElements.thisEl).val(selectedColor);
                        $(myElements.thisEl).trigger("change");
                        methods.updatePreview(myElements.thisEl);
                        methods.closeDropdown(myElements.colorPreviewButton, myElements.colorMenu);
                        methods.addToSavedColors(selectedColor, mySavedColorsInfo, myElements.savedColorsContent);
                    }
                });
                // update saved color markup with content from localStorage or cookies, if available
                if (!settings.saveColorsPerElement) {
                    methods.updateSavedColorMarkup(myElements.savedColorsContent, allSavedColors);
                } else if (settings.saveColorsPerElement) {
                    methods.updateSavedColorMarkup(myElements.savedColorsContent, mySavedColorsInfo.colors);
                }
            }
        });
    };
    return dcl(EditBox, {
        declaredClass: "xide.widgets.ColorWidget",
        get: function (what) {
            const widget = this.nativeWidget;
            if (what === 'value' && widget) {
                const value = widget.val();
                if (value.length > 0 && value.indexOf('#') === -1) {
                    return '#' + value;
                } else {
                    return value;
                }
            }
        },
        set: function (what, value, updateOnly) {
            if (updateOnly === true) {
                this.updateOnly = true;
            }
            const widget = this.nativeWidget;
            if (widget) {
                if (what === 'value') {
                    widget.val(value);
                    this._lastValue = value;
                    this.nativeWidget && this.nativeWidget.data('preview') && this.nativeWidget.data('preview').css('background-color', '#' + value);
                    return true;
                }
                if (what === 'disabled') {
                    widget[value === true ? 'attr' : 'removeAttr'](what, value);
                    return true;
                }
            }
            return this.inherited(arguments);
        },
        startup: function () {
            this.nativeWidget.pickAColor({
                showSpectrum: true,
                showSavedColors: true,
                saveColorsPerElement: true,
                fadeMenuToggle: false,
                showAdvanced: true,
                showBasicColors: true,
                showHexInput: true,
                allowBlank: true,
                inlineDropdown: false,
                owner: this
            });
        }
    });
});