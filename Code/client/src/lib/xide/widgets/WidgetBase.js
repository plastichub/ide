/** @module xide/widgets/WidgetBase **/
define([
    'dcl/dcl',
    'dojo/_base/connect',
    "dojo/dom-class",
    "xide/widgets/TemplatedWidgetBase",
    "xide/utils",
    "xide/factory",
    "xide/types",
    "dojo/dom-construct",
    "xide/lodash",
    "xide/$"
], function (dcl, connect, domClass, TemplatedWidgetBase, utils, factory, types, domConstruct, _, $) {
    /**
     * @class module:xide/widgets/WidgetBase
     * @extends module:xide/widgets/TemplatedWidgetBase
     */
    const Module = dcl([TemplatedWidgetBase], {
        declaredClass: "xide.widgets.WidgetBase",
        data: null,
        model: null,
        widgets: null,
        delegate: null,
        helpNodes: null,
        currentWidget: null,
        currentDataItem: null,
        didLoad: false,
        isSubWidget: false,
        title: "NoTitle",
        value: "Unset",
        minHeight: "60px;",
        previewNode: null,
        button0: null,
        valueNode: null,
        button1: null,
        button2: null,
        button3: null,
        wButton2: null,
        wButton3: null,
        nativeWidget: null,
        isInherited: false,
        lastHeight: null,
        titleNode: null,
        extensionRoot: null,
        extensionTag: 'td',
        extensionClass: 'extension',
        _didSendReady: false,
        styles: null,
        active: true,
        vertical: false,
        changed: false,
        titleColumn: null,
        widgetClass:'widgetContainer widgetBorder widgetTable widget',
        templateString: "<div class='${!widgetClass}' style=''>" +
        "<table border='0' cellpadding='5px' width='100%'>" +
        "<tbody align='left'>" +
            "<tr attachTo='extensionRoot' valign='middle' style='height:90%'>" +
                "<td attachTo='titleColumn' width='20%' class='widgetTitle'><span attachTo='titleNode'>${!title}</span></td>" +
                "<td valign='middle' class='widgetValue' attachTo='valueNode'>${!value}</td>" +
                "<td class='extension' attachTo='previewNode'></td>" +
                "<td class='extension' attachTo='button0'></td>" +
                "<td class='extension' attachTo='button1'></td>" +
            "</tr>" +
        "</tbody>" +
        "</table>" +
        "<div attachTo='expander' style='width:100%;'></div>" +
        "<div attachTo='last'></div>" +
        "</div>",
        postMixInProperties: function () {
            //treat camel case
            this.title = (this.title || "").replace(/([a-z](?=[A-Z]))/g, '$1 ');
        },
        setActive: function (active) {
            this.active = false;
            if (this.userData) {
                this.userData._active = active;
            }
        },
        setStoreDelegate: function (delegate) {
            this.storeDelegate = delegate;
        },
        getExtensionRoot: function () {
            return this.extensionRoot;
        },
        getFreeExtensionSlot: function (startNode) {
            const root = startNode || this.getExtensionRoot();
            if (!root) {
                return null;
            }
            let node = utils.findEmptyNode(root, '.extension');
            if (!node) {
                node = domConstruct.create(this.extensionTag, {
                    className: this.extensionClass
                }, root);
            }
            return node;
        },
        getCurrentPlatform: function () {
            return this._currentPlatform;
        },
        getRootId: function () {
            return this.id;
        },
        save: function () {
            this.publish(types.EVENTS.ON_CI_UPDATE, {
                owner: this.delegate || this.owner,
                ci: this.userData,
                newValue: this.getValue(),
                storeItem: this.storeItem
            });
        },
        /***
         * Widget base protocol to be implemented by each subclass
         * @param value
         */
        setValue: function (value) {
            const user = this.userData || {};
            const oldValue = utils.getCIValueByField(user, "value");
            utils.setCIValueByField(user, "value", value);
            if (this.skipSave == true) {
                this.skipSave = false;
                return;
            }
            const eventArgs = {
                owner: this.delegate || this.owner,
                ci: user,
                newValue: value,
                oldValue: oldValue,
                storeItem: this.storeItem
            };
            this.publish(types.EVENTS.ON_CI_UPDATE, eventArgs);
            this._emit('valueChanged', eventArgs);
        },
        /**
         *
         * @param platform
         * @param field
         * @returns {*}
         */
        getValue: function (platform, field) {
            return utils.getCIValueByField(this.userData, field || "value");
        },
        clearValue: function () {

        },
        empty: function () {
        },
        fillTemplate: function () {
            if (this.nativeWidget) {
                this.valueNode.innerHTML = "";
                this.valueNode.appendChild(this.nativeWidget.domNode);
            }
        },
        startup: function () {
            if (this.value == null) {
                this.value = "Unset";
            }
            if (this._started) {
                return;
            }
            const res = this.inherited(arguments);
            if (this.userData && this.userData.inherited && this.userData.inherited == true) {
                this.isInherited = true;
                domClass.remove(this.domNode, "widgetContainer");
                this.buildToolbar();
                domClass.add(this.domNode, "widgetContainerAlternate");

            }
            if (this.userData.toolTip) {
                this.initToolTip(this.userData.toolTip);
            }

            if (this.nativeWidget && this.setupNativeWidgetHandler) {
                this.setupNativeWidgetHandler();
            }
            this.updateTitleNode(this.title);

            if (this.showLabel == false) {
                utils.destroy(this.titleColumn);
            }
            return res;
        },
        connectEditBox: function (editBox, onChanged) {
            const thiz = this;
            function cb(value) {
                if (onChanged) {
                    onChanged(value);
                }
                const _valueNow = utils.toString(thiz.userData['value']);

                if (value === _valueNow) {
                    return;
                }
                thiz.userData.changed = true;
                thiz.userData.active = true;
                if (thiz.serialize) {
                    value = thiz.serialize();
                }
                utils.setCIValueByField(thiz.userData, "value", value);
                factory.publish(types.EVENTS.ON_CI_UPDATE, {
                    owner: thiz.delegate || thiz.owner,
                    ci: thiz.userData,
                    newValue: value,
                    storeItem: thiz.storeItem
                }, thiz);
            }

            if (editBox._on) {
                editBox._on('change', cb);
            } else {
                connect.connect(editBox, "onChange", cb);
            }
        },
        onReady: function () {
            if (!this._didSendReady) {
                factory.publish(types.EVENTS.ON_WIDGET_READY, {
                    ci: this.userData,
                    widget: this,
                    storeItem: this.storeItem,
                    owner: this.owner
                });
                this._didSendReady = true;
            }
            const $node = $(this.domNode);
            if (this.userData && this.userData.vertical === true) {
                $node.addClass('vertical');
            }
            const _class = _.last(this.declaredClass.split('.'));
            $node.addClass(_class);
            if (this.userData) {
                const ci = this.userData;
                const callbacks = types.getCICallbacks(ci.id);
                if (callbacks && callbacks.on) {
                    if (callbacks.on.render) {
                        callbacks.on.render(this.owner, this, ci);
                    }
                }
                const description = ci.description;
                if (!this.tooltip && description && description.length > 5 && description !== 'Options') {
                    const node = this.nativeWidget ? $(this.nativeWidget) : $(this.domNode);
                    const tooltip = node.tooltip({
                        title: description,
                        placement: 'bottom',
                        container: 'body'
                    });
                    this.tooltip = tooltip.data()['bs.tooltip'];
                    this.add(tooltip);
                }
            }
        },
        onDidRenderWidgets: function (view, widgets) {
        },
        onAttached: function (view) {
        },
        set: function (what, value, updateOnly) {
            if (what === 'disabled') {
                $(this.domNode)[value === true ? 'attr':'removeAttr'](what,value);
            }
        }
    });
    dcl.chainAfter(Module, "destroy");
    return Module;
});
