define([
    "dojo/_base/declare",
    'xide/mixins/EventedMixin'
], function (declare, EventedMixin) {
    // Deprecated / Unused
    return declare("xide.widgets.ActionTogglePane", [EventedMixin], {});
});