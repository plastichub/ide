/** @module xide/widgets/ActionToolbar4 **/
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xide/_base/_Widget',
    "xide/mixins/ActionMixin",
    'xaction/ActionContext',
    "xide/model/Path",
    'xlang/i18',
    "xide/widgets/_MenuMixin4",
    "xide/popup"
], function (dcl,types,utils,_XWidget,
             ActionMixin,ActionContext,Path,i18,
             _MenuMixin4,popup){

    const OPEN_DELAY = 200;
    const ContainerClass =  dcl([_XWidget,ActionContext.dcl,ActionMixin.dcl],{
        templateString:'<div attachTo="navigation" class="actionToolbar">'+
        '<nav attachTo="navigationRoot" class="" role="navigation">'+
        '<ul attachTo="navBar" class="nav navbar-nav"/>'+
        '</nav>'+
        '</div>'
    });
    /**
     * @class module:xide/widgets/ActionToolbar4
     */
    const ActionRendererClass = dcl(null,{
        ITEM_TAG : "div",
        CONTAINER_TAG : "div",
        ITEM_CLASS : "actionItem",
        renderTopLevel:function(name,where){
            where = where || $(this.getRootContainer());
            const item =$('<li tabindex="-1" class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' + i18.localize(name) +'<b class="caret"></b></a>'+
                '</li>');

            where.append(item);
            return item;
        },
        getRootContainer:function(){
            return this.navBar;
        }
    });
    const Module = dcl([ContainerClass,_MenuMixin4,ActionRendererClass,_XWidget.StoreMixin],{
        declaredClass:'xide.widgets.Actiontoolbar',
        target:null,
        hideSubsFirst:true,
        visibility: types.ACTION_VISIBILITY.ACTION_TOOLBAR,
        actionFilter:{
            quick:true
        },
        _renderItem:function(item,data,$menu,action, owner, label, icon, visibility, showKeyCombo, lazy){
            const self = this;
            const labelLocalized = self.localize(label);
            const actionType = visibility.actionType || action.actionType;
            const parentAction = action.getParent ? action.getParent() : null;
            const closeOnClick = self.getActionProperty(action, visibility, 'closeOnClick');
            let keyComboString = ' \n';
            let element = null;
            if (action.keyboardMappings && showKeyCombo !== false) {
                const mappings = action.keyboardMappings;
                const keyCombos = mappings[0].keys;
                if (keyCombos && keyCombos.length) {
                    keyComboString += '' + keyCombos.join(' | ').toUpperCase() + '';
                }
            }

            if (actionType === types.ACTION_TYPE.MULTI_TOGGLE) {
                element = '<li class="" >';
                const id = action._store.id + '_' + action.command + '_' + self.id;
                const checked = action.get('value');
                //checkbox-circle
                element += '<div class="action-checkbox checkbox checkbox-success ">';
                element += '<input id="' + id + '" type="checkbox" ' + (checked === true ? 'checked' : '') + '>';
                element += '<label for="' + id + '">';
                element += self.localize(data.text);
                element += '</label>';
                element += '<span style="max-width:100px;margin-right:20px" class="text-muted pull-right ellipsis keyboardShortCut">' + keyComboString + '</span>';
                element += '</div>';

                $menu.addClass('noclose');
                const result = $(element);
                const checkBox = result.find('INPUT');
                checkBox.on('change', function (e) {
                    action._originReference = data;
                    action._originEvent = e;
                    action.set('value', checkBox[0].checked);
                    action._originReference = null;
                });
                self.setVisibilityField(action, 'widget', data);
                return result;
            }
            closeOnClick === false && $menu.addClass('noclose');
            if (actionType === types.ACTION_TYPE.SINGLE_TOGGLE && parentAction) {
                const value = action.value || action.get('value');
                const parentValue = parentAction.get('value');
                if (value == parentValue) {
                    icon = 'fa fa-check';
                }
            }
            const title = data.text || labelLocalized || self.localize(action.title);

            //default:
            element = '<li class="" title="' + title + ' ' + keyComboString + '">';
            const _icon = data.icon || icon;
            //icon
            if (typeof _icon !== 'undefined') {
                //already html string
                if (/<[a-z][\s\S]*>/i.test(_icon)) {
                    element += _icon;
                } else {
                    element += '<span style="" class="icon ' + _icon + '"/> ';
                }
            }
            element +='<a class="">';
            element += data.text;
            element += '</a></li>';
            self.setVisibilityField(action, 'widget', data);
            return $(element);
        },
        init2: function(opts){
            let options = this.getDefaultOptions();
            options = $.extend({}, options, opts);
            const self = this;
            const root = $(document);
            this.__on(root,'click',null,function(e){
                if(!self.isOpen){
                    return;
                }
                self.isOpen=false;
                self.onClose(e)
                $('.dropdown-context').css({
                    display:''
                }).find('.drop-left').removeClass('drop-left');
            });
            if(options.preventDoubleContext){
                this.__on(root,'contextmenu', '.dropdown-context', function (e) {
                    e.preventDefault();
                });
            }

            function correctPosition(_root,$sub){
                const level = 0;
                let _levelAttr = $sub.attr('level');
                if(!_levelAttr){
                    _levelAttr = parseInt($sub.parent().attr('level'),10) || 0;
                }

                if(_levelAttr===0){
                    $sub.css({
                        left:0
                    });
                }
                const parent = _root.parent();
                const parentTopAbs = parent.offset().top;
                const rootOffset = _root.offset();
                const rootPos = _root.position();
                const rootHeight = _root.height();
                const parentDelta = parentTopAbs - rootOffset.top;
                let newTop = (rootPos.top + rootHeight) + parentDelta;
                const offset = 0;
                if(_levelAttr!==0) {
                    newTop += offset;
                }
                $sub.css({
                    top:-2
                });
                const subHeight = $sub.height() + 0;
                const totalH = $('html').height();
                const pos = $sub.offset();
                const posWrapper = $sub.offset();
                const overlapYDown = totalH - (pos.top + subHeight);
                if((pos.top + subHeight) > totalH){
                    $sub.css({
                        top: overlapYDown - 30
                    });
                }

                ////////////////////////////////////////////////////////////
                const subWidth = $sub.width();

                const subLeft = $sub.offset().left;
                const collision = (subWidth+subLeft) > window.innerWidth;

                if(collision){
                    $sub.addClass('drop-left');
                }
            }

            function mouseEnterHandler(e){
                const _root = $(e.currentTarget);
                let $sub = _root.find('.dropdown-context-sub:first');
                let didPopup = false;
                if ($sub.length === 0) {
                    $sub = _root.data('sub');
                    if($sub){
                        didPopup = true;
                    }else {
                        return;
                    }
                }

                const data = $sub.data('data');
                const level = data ? data[0].level : 0;
                const isFirst = level ===1;

                $sub.css('display', 'none');
                $sub.data('left',false);
                _root.data('left',false);
                if(isFirst) {
                    $sub.css('display', 'initial');
                    $sub.css('position', 'initial');
                    function close() {
                        $sub.data('open',false);
                        popup.close($sub[0]);
                    }
                    if(_root.data('didSetup')!==true){
                        _root.data('didSetup',true);
                        _root.data('sub',$sub);
                        _root.on('mouseleave', function (e) {
                            const _thisSub = _root.data('sub');
                            const next = e.toElement;
                            if(next==_thisSub[0] || $.contains(_thisSub[0],next)){
                                return;
                            }
                            close();
                        });
                    }
                    if (!didPopup) {
                        _root.data('sub', $sub);
                        $sub.data('owner', self);
                        if(!$sub.data('didSetup')){
                            $sub.data('root',_root);
                            $sub.on('mouseleave', function () {
                                $sub.data('left',true);
                                close();

                            });
                            $sub.data('didSetup',true);
                            $sub.on('mouseenter', function(e){
                                $sub.data('entered',true);
                            });
                        }
                    }
                    function open() {
                        if($sub.data('open')){
                            return;
                        }
                        $sub.data('open',true);
                        const wrapper = popup.open({
                            popup: $sub[0],
                            around: _root[0],
                            orient: ['below', 'above'],
                            maxHeight: -1,
                            owner: self,
                            extraClass: 'ActionToolbar',
                            onExecute: function () {
                                self.closeDropDown(true);
                            },
                            onCancel: function () {
                                close();
                            },
                            onClose: function () {
                                _root.data('left',true);
                                close();
                            }
                        });
                        correctPosition(_root,$sub,$(wrapper));
                    }
                    open();
                    /*
                     //clearTimeout($sub.data('openTimer'));
                     return;
                     $sub.data('openTimer',setTimeout(function(){
                     if($sub.data('left')!==true) {

                     }else{
                     console.log('left');
                     $sub.css('display', 'none');
                     }
                     },OPEN_DELAY));
                     return;
                     */
                }else{
                    $sub.css('display','block');
                    if(!$sub.data('didSetup')){
                        $sub.data('didSetup',true);
                        $sub.on('mouseleave',function(){
                            $sub.css('display','');
                            $sub.data('left',true);

                        });
                    }
                    if(!_root.data('didSetup')){
                        _root.data('didSetup',true);
                        _root.on('mouseleave',function(){
                            _root.data('left',true);
                            $sub.css('display','');

                        });
                    }
                }
                const autoH = $sub.height() + 0;
                const totalH = $('html').height();
                const pos = $sub.offset();
                const overlapYDown = totalH - (pos.top + autoH);
                if ((pos.top + autoH) > totalH) {
                    $sub.css({
                        top: overlapYDown - 30
                    }).fadeIn(options.fadeSpeed);
                }

                ////////////////////////////////////////////////////////////
                const subWidth = $sub.width();

                const subLeft = $sub.offset().left;
                const collision = (subWidth + subLeft) > window.innerWidth;

                if (collision) {
                    $sub.addClass('drop-left');
                }
            }
            this.__on(root,'mouseenter', '.dropdown-submenu', mouseEnterHandler.bind(this));
        },
        resize:function(){
            this._height = this.$navBar.height();
            if (this._height > 0){
                this.$navBar.css('width',this.$navigation.width());
                this.$navigationRoot.css('width',this.$navigation.width());
                this.$navigation.css('height',this._height);
                this.$navigationRoot.css('height', this._height);
            }
        },
        destroy:function(){
            utils.destroy(this.$navBar[0]);
            utils.destroy(this.$navigation[0]);
        },
        buildMenu:function (data, id, subMenu,update) {
            const subClass = (subMenu) ? ' dropdown-context-sub' : ' scrollable-menu ';
            const menuString = '<ul aria-expanded="true" role="menu" class="dropdown-menu dropdown-context' + subClass + '" id="dropdown-' + id + '"></ul>';
            const $menu = update ? (this._rootMenu || this.$navBar || $(menuString)) : $(menuString);

            if(!subMenu){
                this._rootMenu = $menu;
            }
            $menu.data('data',data);
            return this.buildMenuItems($menu, data, id, subMenu);
        },
        setActionStore: function (store, owner,subscribe,update,itemActions) {
            if(!update && store && this.store && store!=this.store){
                this.removeCustomActions();
            }
            if(!update){
                this._clear();
                this.addActionStore(store);
            }
            if(!store){
                return;
            }
            this.store = store;
            const self = this;
            const visibility = self.visibility;
            const rootContainer = $(self.getRootContainer());

            const tree = update ? self.lastTree : self.buildActionTree(store,owner);

            const allActions = tree.allActions;
            const rootActions = tree.rootActions;
            const allActionPaths = tree.allActionPaths;
            const oldMenuData = self.menuData;

            if(subscribe!==false) {
                if(!this['_handleAdded_' + store.id]) {
                    this.addHandle('added', store._on('onActionsAdded', function (actions) {
                        self.onActionAdded(actions);
                    }));

                    this.addHandle('delete', store.on('delete', function (evt) {
                        self.onActionRemoved(evt);
                    }));
                    this['_handleAdded_' + store.id]=true;
                }
            }

            // final menu data
            const data = [];
            if(!update) {
                _.each(tree.root, function (menuActions, level) {
                    const lastGroup = '';
                    _.each(menuActions, function (command) {
                        let action = self.getAction(command, store);
                        let isDynamicAction = false;
                        if (!action) {
                            isDynamicAction = true;
                            action = self.createAction(command);
                        }
                        if (action) {
                            const renderData = self.getActionData(action);
                            const icon = renderData.icon;
                            const label = renderData.label;
                            const visibility = renderData.visibility;
                            const group = renderData.group;

                            const lastHeader = {
                                header: ''
                            };


                            if (visibility === false) {
                                return;
                            }

                            /*
                             if (!isDynamicAction && group && groupedActions[group] && groupedActions[group].length >= 1) {
                             //if (lastGroup !== group) {
                             var name = groupedActions[group].length >= 2 ? i18.localize(group) : "";
                             lastHeader = {divider: name,vertial:true};
                             data.push(lastHeader);
                             lastGroup = group;
                             //}
                             }
                             */

                            const item = self.toMenuItem(action, owner, '', icon, visibility || {}, false);
                            data.push(item);
                            item.level = 0;
                            visibility.widget = item;

                            self.addReference(action, item);

                            const childPaths = new Path(command).getChildren(allActionPaths, false);
                            const isContainer = childPaths.length > 0;

                            function parseChildren(command, parent) {
                                const childPaths = new Path(command).getChildren(allActionPaths, false);
                                const isContainer = childPaths.length > 0;
                                const childActions = isContainer ? self.toActions(childPaths, store) : null;
                                if (childActions) {
                                    const subs = [];
                                    _.each(childActions, function (child) {
                                        const _renderData = self.getActionData(child);
                                        const _item = self.toMenuItem(child, owner, _renderData.label, _renderData.icon, _renderData.visibility,false);
                                        const parentLevel = parent.level || 0;
                                        _item.level = parentLevel + 1;
                                        self.addReference(child, _item);
                                        subs.push(_item);
                                        const _childPaths = new Path(child.command).getChildren(allActionPaths, false);
                                        const _isContainer = _childPaths.length > 0;
                                        if (_isContainer) {
                                            parseChildren(child.command, _item);
                                        }
                                    });
                                    parent.subMenu = subs;
                                }
                            }
                            parseChildren(command, item);
                            self.buildMenuItems(rootContainer, [item], "-" + new Date().getTime());
                        }
                    });
                });
                self.onDidRenderActions(store, owner);
                this.menuData = data;
            }else{
                if(itemActions || !_.isEmpty(itemActions)) {

                    _.each(itemActions, function (newAction) {
                        if (newAction) {
                            const action = self.getAction(newAction.command);
                            if (action){
                                const renderData = self.getActionData(action);
                                const icon = renderData.icon;
                                const label = renderData.label;
                                const aVisibility = renderData.visibility;
                                const group = renderData.group;
                                const item = self.toMenuItem(action, owner, label, icon, aVisibility || {},null,false);

                                if(aVisibility.widget){
                                    return;
                                }

                                aVisibility.widget = item;

                                self.addReference(newAction, item);

                                if(!action.getParentCommand){
                                    return;
                                }
                                const parentCommand = action.getParentCommand();
                                const parent = self._findParentData(oldMenuData,parentCommand);
                                if(parent && parent.subMenu){
                                    parent.lazy = true;
                                    parent.subMenu.push(item);
                                }else{
                                    oldMenuData.splice(0, 0, item);
                                }
                            } else {
                                console.error('cant find action ' + newAction.command);
                            }
                        }
                    });
                    self.buildMenu(oldMenuData, self.id,null,update);
                }
            }
            this.resize();
        },
        startup:function(){
            this.correctSubMenu = true;
            this.init2({
                preventDoubleContext: false
            });
            this.menu = this.$navigation;
        }
    });
    dcl.chainAfter(Module,'destroy');
    return Module;
});

