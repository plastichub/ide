define([
    'dcl/dcl',
    'dojo/on',
    'xide/widgets/WidgetBase',
    "dojo/keys",
    "dojo/dom-style",
    "xide/utils"
], function (dcl, on, WidgetBase, keys, domStyle, utils) {
    return dcl(WidgetBase, {
        declaredClass: "xide.widgets.RichTextWidget",
        didLoad: false,
        minHeight: "300px",
        editorHeight: "470px",
        jsonEditorHeight: "270px",
        aceEditorHeight: "200px",
        aceEditorOptions: null,
        aceEditor: null,
        aceNode: null,
        useACE: false,
        templateString: "<div class='jsonEditorWidgetContainer' style='width: 100%;height:300px'>" +
            "<div attachTo='textArea' class='textarea' placeholder='Enter text ...' style='width: 100%; min-height: 100px; font-size: 14px; line-height: 18px;'></div><div attachTo='toolbar' style='display: none;'></div></div>",
        editor: null,
        getValue: function () {
            if (this.aceEditor) {
                return this.aceEditor.get('value');
            }
            return this.inherited(arguments);
        },
        resize: function () {
            this.inherited(arguments);
            if (this.aceEditor) {
                this.aceEditor.resize();
            }
        },
        ctlrKeyDown: false,
        onKeyUp: function (evt) {
            switch (evt.keyCode) {
                case keys.ALT:
                case keys.CTRL:
                    {
                        this.ctlrKeyDown = false;
                        break;
                    }
            }
        },
        onKey: function (evt) {
            switch (evt.keyCode) {
                case keys.META:
                case keys.ALT:
                case keys.SHIFT:
                case keys.CTRL:
                    {
                        this.ctlrKeyDown = true;
                        setTimeout(function () {
                            thiz.ctlrKeyDown = false
                        }, 2000);
                        break;
                    }
            }
            var thiz = this;
            if (evt.type && evt.type == 'keyup') {
                return this.onKeyUp(evt);
            }
            const charOrCode = evt.charCode || evt.keyCode;
            if (this.ctlrKeyDown && charOrCode === 83) {
                evt.preventDefault();

                if (this.delegate && this.delegate.onSave) {
                    this.delegate.onSave(this.userData, this.getValue());
                }
            }
        },
        _lastContent: null,
        startup: function () {
            const value = this.userData ? this.userData['value'] : '';
            const thiz = this;
            try {
                const wysihtml5ParserRules = {
                    tags: {
                        strong: {},
                        b: {},
                        i: {},
                        em: {},
                        br: {},
                        p: {},
                        div: {},
                        span: {},
                        ul: {},
                        ol: {},
                        li: {},
                        a: {
                            set_attributes: {
                                target: "_blank",
                                rel: "nofollow"
                            },
                            check_attributes: {
                                href: "url" // important to avoid XSS
                            }
                        }
                    }
                };
                const text = this.$textArea;
                this.$textArea.html(value);
                const editor = new wysihtml5.Editor(this.$textArea[0], {
                    toolbar: this.$toolbar[0],
                    parserRules: wysihtml5ParserRules
                });
                editor.on('change', ()=> {
                    this.setValue(text.html());
                });
                this.add(editor);
            } catch (e) {
                console.error('constructing json editor widget failed : ' + e.message);
                logError(e);
            }
        }
    });
});