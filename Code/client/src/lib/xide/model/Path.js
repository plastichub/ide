/** @module xide/model/Path */
define([
    "xide/utils",
    "dcl/dcl"
], function (utils, dcl) {
    const Path = dcl(null, {
        declaredClass: "xide.model.Path",
        /**
         * @class xide.model.Path
         * @constructor
         */
        constructor: function (path, hasLeading, hasTrailing) {
            path = path || '.';  // if empty string, use '.'
            if (typeof path == 'string') {
                this.path = path;
                this.getSegments();
            } else {
                this.segments = path;
                this.hasLeading = hasLeading !== null ? hasLeading : false;
                this.hasTrailing = hasTrailing !== null ? hasLeading : false;
            }
        },

        endsWith: function (tail) {
            const segments = utils.clone(this.segments);
            const tailSegments = (new Path(tail)).getSegments();
            while (tailSegments.length > 0 && segments.length > 0) {
                if (tailSegments.pop() != segments.pop()) {
                    return false;
                }
            }
            return true;
        },
        getExtension: function () {
            if (!this.extension) {
                this.extension = this.path.substr(this.path.lastIndexOf('.') + 1);
            }
            return this.extension;
        },
        segment: function (index) {
            const segs = this.getSegments();
            if (segs.length < index) {
                return null;
            }
            return segs[index];
        },
        /**
         * Return all items under this path
         * @param items {String[]}
         * @param recursive {boolean}
         * @returns {String[]}
         */
        getChildren: function (items, recursive) {
            const result = [];
            const root = this;
            const path = this.toString();

            function addChild(child) {
                const _path = typeof child !== 'string' ? child.toString() : child;
                if (_path !== path && !result.includes(_path)) {
                    result.push(_path);
                }
            }

            _.each(items, function (item) {
                const child = new Path(item);
                //root match
                if (child.startsWith(root)) {
                    if (recursive) {
                        addChild(child.toString());
                    } else {

                        const diff = child.relativeTo(path);
                        if (diff) {
                            const diffSegments = diff.getSegments();
                            //direct child
                            if (diffSegments.length == 1) {
                                addChild(child);
                            } else if (diffSegments.length > 1) {

                                //make sure that its parent has been added:
                                const parent = child.getParentPath();
                                const parentDiff = parent.relativeTo(path);

                                //check diff again
                                if (parentDiff.getSegments().length == 1) {
                                    addChild(parent.toString());
                                }
                            }
                        }
                    }

                }
            });
            return result;
        },
        getSegments: function () {
            if (!this.segments) {
                const path = this.path;
                this.segments = path.split('/');
                if (path.charAt(0) == '/') {
                    this.hasLeading = true;
                }
                if (path.charAt(path.length - 1) == '/') {
                    this.hasTrailing = true;
                    // If the path ends in '/', split() will create an array whose last element
                    // is an empty string. Remove that here.
                    this.segments.pop();
                }
                this._canonicalize();
            }
            return this.segments;
        },
        isAbsolute: function () {
            return this.hasLeading;
        },
        getParentPath: function () {
            if (!this._parentPath) {
                const parentSegments = utils.clone(this.segments);
                parentSegments.pop();
                this._parentPath = new Path(parentSegments, this.hasLeading);
            }
            return utils.clone(this._parentPath);
        },
        _clone: function () {
            return new Path(utils.clone(this.segments), this.hasLeading, this.hasTrailing);
        },
        append: function (tail) {
            tail = tail || "";
            if (typeof tail == 'string') {
                tail = new Path(tail);
            }
            if (tail.isAbsolute()) {
                return tail;
            }
            const mySegments = this.segments;
            const tailSegments = tail.getSegments();
            const newSegments = mySegments.concat(tailSegments);
            const result = new Path(newSegments, this.hasLeading, tail.hasTrailing);
            if (tailSegments[0] == ".." || tailSegments[0] == ".") {
                result._canonicalize();
            }
            return result;
        },
        toString: function () {
            const result = [];
            if (this.hasLeading) {
                result.push('/');
            }
            for (let i = 0; i < this.segments.length; i++) {
                if (i > 0) {
                    result.push('/');
                }
                result.push(this.segments[i]);
            }
            if (this.hasTrailing) {
                result.push('/');
            }
            return result.join("");
        },
        removeRelative: function () {
            const segs = this.getSegments();
            if (segs.length > 0 && segs[1] == ".") {
                return this.removeFirstSegments(1);
            }
            return this;
        },
        relativeTo: function (base, ignoreFilename) {
            if (typeof base == 'string') {
                base = new Path(base);
            }
            const mySegments = this.segments;
            if (this.isAbsolute()) {
                return this;
            }
            const baseSegments = base.getSegments();
            const commonLength = this.matchingFirstSegments(base);
            let baseSegmentLength = baseSegments.length;
            if (ignoreFilename) {
                baseSegmentLength = baseSegmentLength - 1;
            }
            const differenceLength = baseSegmentLength - commonLength;
            const newSegmentLength = differenceLength + mySegments.length - commonLength;
            if (newSegmentLength == 0) {
                return Path.EMPTY;
            }
            const newSegments = [];
            for (var i = 0; i < differenceLength; i++) {
                newSegments.push('..');
            }
            for (var i = commonLength; i < mySegments.length; i++) {
                newSegments.push(mySegments[i]);
            }
            return new Path(newSegments, false, this.hasTrailing);
        },
        startsWith: function (anotherPath) {
            const count = this.matchingFirstSegments(anotherPath);
            return anotherPath._length() == count;
        },
        _length: function () {
            return this.segments.length;
        },
        matchingFirstSegments: function (anotherPath) {
            const mySegments = this.segments;
            const pathSegments = anotherPath.getSegments();
            const max = Math.min(mySegments.length, pathSegments.length);
            let count = 0;
            for (let i = 0; i < max; i++) {
                if (mySegments[i] != pathSegments[i]) {
                    return count;
                }
                count++;
            }
            return count;
        },
        removeFirstSegments: function (count) {
            return new Path(this.segments.slice(count, this.segments.length), this.hasLeading, this.hasTrailing);
        },
        removeMatchingLastSegments: function (anotherPath) {
            const match = this.matchingFirstSegments(anotherPath);
            return this.removeLastSegments(match);
        },
        removeMatchingFirstSegments: function (anotherPath) {
            const match = this.matchingFirstSegments(anotherPath);
            return this._clone().removeFirstSegments(match);
        },
        removeLastSegments: function (count) {
            if (!count) {
                count = 1;
            }
            return new Path(this.segments.slice(0, this.segments.length - count), this.hasLeading, this.hasTrailing);
        },
        lastSegment: function () {
            return this.segments[this.segments.length - 1];
        },
        firstSegment: function (length) {
            return this.segments[length || 0];
        },
        equals: function (anotherPath) {
            if (this.segments.length != anotherPath.segments.length) {
                return false;
            }
            for (let i = 0; i < this.segments.length; i++) {
                if (anotherPath.segments[i] != this.segments[i]) {
                    return false;
                }
            }
            return true;
        },
        _canonicalize: function () {
            let doIt;
            const segments = this.segments;
            for (var i = 0; i < segments.length; i++) {
                if (segments[i] == "." || segments[i] == "..") {
                    doIt = true;
                    break;
                }
            }
            if (doIt) {
                const stack = [];
                for (var i = 0; i < segments.length; i++) {
                    if (segments[i] == "..") {
                        if (stack.length == 0) {
                            // if the stack is empty we are going out of our scope
                            // so we need to accumulate segments.  But only if the original
                            // path is relative.  If it is absolute then we can't go any higher than
                            // root so simply toss the .. references.
                            if (!this.hasLeading) {
                                stack.push(segments[i]); //stack push
                            }
                        } else {
                            // if the top is '..' then we are accumulating segments so don't pop
                            if (".." == stack[stack.length - 1]) {
                                stack.push("..");
                            } else {
                                stack.pop();
                            }
                        }
                        //collapse current references
                    } else if (segments[i] != "." || this.segments.length == 1) {
                        stack.push(segments[i]); //stack push
                    }
                }
                //if the number of segments hasn't changed, then no modification needed
                if (stack.length == segments.length) {
                    return;
                }
                this.segments = stack;
            }
        }

    });
    Path.EMPTY = new Path("");
    return Path;
});