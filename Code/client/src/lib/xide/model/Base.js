/** @module xide/model/Base **/
define([
    'dcl/dcl',
    "dojo/_base/declare",
    "xide/utils"
], function (dcl,declare,utils) {
    
    const Implementation = {
        declaredClass:"xide/model/Base",
        /**
         * Mixin constructor arguments into this.
         * This could have been done in another base class but performance matters
         * @todo use a mixin from lodash
         * @constructor
         */
        constructor: function (args) {
            utils.mixin(this, args);
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Public interface, keep it small and easy
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * Return a human friendly name
         * @abstract
         * @returns {string|null}
         */
        getLabel: function () {
            return null;
        },
        /**
         * Return a unique ID.
         * @abstract
         * @returns {string|null}
         */
        getID: function () {
            return null;
        }
    };

    const Module = declare("xide/model/Base",null,Implementation);
    Module.dcl = dcl(null,Implementation);
    return Module;
});

