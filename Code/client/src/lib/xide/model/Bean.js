/** @interface xide/model/Bean **/
define([
    "dojo/_base/declare",
    "xide/types",
    "xide/model/Base",
    "xide/utils"
], function (declare, types, Base, utils) {
    utils.mixin(types, {
        /**
         * ActionVisibility
         * @enum module:xide/types/ACTION_VISIBILITY
         * @memberOf module:xide/types
         */
        BEAN_FLAGS: {}
    });
    /**
     * @namespace xide/model
     * @class module:xide/model/Base
     * @interface
     * @constructor
     */
    return declare("xide/model/Bean", [Base], {
        /**
         * Mixin constructor arguments into this.
         * This could have been done in another base class but performance matters
         * @constructor
         */
        constructor: function (args) {
            utils.mixin(this, args);
        },
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Public interface, keep it small and easy
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * Return a human friendly name
         * @abstract
         * @returns {string|null}
         */
        getLabel: function () {
            return null;
        },
        /**
         * Return a unique ID.
         * @abstract
         * @returns {string|null}
         */
        getID: function () {
            return null;
        }
    });
});

