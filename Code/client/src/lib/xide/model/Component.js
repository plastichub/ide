/** @module xide/model/Component **/
define([
    "dcl/dcl",
    "dojo/Deferred",
    "dojo/has",
    "require",
    "xide/model/Base",
    "xide/types",
    "xide/mixins/EventedMixin"
], function (dcl, Deferred, has, require, Base, types, EventedMixin) {
    /**
     * COMPONENT_FLAGS is a number of flags being used for the component's instance creation. Use an object instead of
     * an integer, never know how big this becomes and messing with 64bit integers doesn't worth the effort.
     *
     * @enum module:xide/types/COMPONENT_FLAGS
     * @memberOf module:xide/types
     * @extends xide/types
     */
    types.COMPONENT_FLAGS = {
        /**
         * Due the object instantiation, instruct the component loader to call ::load()
         * @constant
         */
        LOAD: 1,
        /**
         * Due the object instantiation, instruct the component loader to call ::run()
         * @constant
         */
        RUN: 1
    };

    /**
     * @class xide/model/Component
     * @extends module:xide/mixins/EventedMixin
     * @extends module:xide/model/Base
     */
    return dcl([Base.dcl, EventedMixin.dcl], {
        declaredClass:"xide/model/Component",
        /**
         * Flag indicating that all dependencies are fully loaded
         * @type {boolean}
         * @default false
         */
        _loaded: false,
        /**
         * Pointer to a context, filled by the component loader
         * @member module:xide/manager/Context
         */
        ctx: null,
        /**
         * Pointer to an optional owner
         * @member {Object|null}
         */
        owner: {
            reloadComponent: function () {
            }
        },
        /**
         * Usually a component is about a new beantype.
         * @TODO handle many
         */
        beanType: null,
        /**
         * Array of typical JS packages
         * @member {Array|null} packages
         */
        packages: null,

        /**
         * Array of resources. A components has typically a bunch of resources like CSS.
         * @member {Array} resources
         */
        resources: [],
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        getDependencies: function () {
            return [];
        },
        hasEditors: function () {
            return [];
        },
        /**
         * Return a human readable string
         * @returns {string}
         */
        getLabel: function () {
            return "Have no label";
        },
        /**
         * If this component has additional dependencies, load them here!
         * @returns {dojo.Deferred}
         */
        load: function (hasName) {
            const _defered = new Deferred();
            const thiz = this;
            const _re = require;

            hasName = hasName || this.getLabel();

            _re(this.getDependencies(), function () {
                thiz._loaded = true;
                if (hasName) {
                    has.add(hasName, function () {
                        return true;
                    });
                }
                _defered.resolve();
            });
            return _defered.promise;
        },
        run: function () {
            return false;
        },
        onModuleReloaded: function () {
            this.owner.reloadComponent(this);
        },
        isLoaded: function () {
            return this._loaded;
        }
    });
});

