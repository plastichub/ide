/** @module xide/data/ObservableStore **/
define([
    "dojo/_base/declare",
    "xide/mixins/EventedMixin",
    "xide/lodash"
], function (declare, EventedMixin, _) {
    /**
     * Mixin to deal with dmodel
     * @class module:xide/data/ObservableStore
     * @lends module:xide/data/_Base
     * @lends module:dstore/Store
     * @lends module:xide/data/Memory
     */
    return declare('xide/data/Observable', EventedMixin, {
        /**
         * @type {boolean} Toggle to mute notifications during batch operations.
         */
        _ignoreChangeEvents: true,
        /**
         * @type {Array<String>} List of default properties to be observed by dmodel.property.observe.
         */
        observedProperties: [],
        /**
         * XIDE Override and extend putSync for adding the _store property and observe a new item's properties.
         * @param item
         * @param publish
         * @returns {*}
         */
        putSync: function (item, publish) {
            this.silent(!publish);
            const res = this.inherited(arguments);
            const self = this;
            publish !== false && this.emit('added', res);
            res && !res._store && Object.defineProperty(res, '_store', {
                get: function () {
                    return self;
                }
            });
            this._observe(res);
            this.silent(false);
            return res;
        },
        /**
         * Extend and override removeSync to silence notifications during batch operations.
         * @param id {string}
         * @param silent {boolean|null}
         * @returns {*}
         */
        removeSync: function (id, silent) {
            this.silent(silent);
            const _item = this.getSync(id);
            _item && _item.onRemove && _item.onRemove();
            const res = this.inherited(arguments);
            this.silent(false);
            return res;
        },
        /**
         *
         * @param item
         * @param property
         * @param value
         * @param source
         * @private
         */
        _onItemChanged: function (item, property, value, source) {
            if (this._ignoreChangeEvents) {
                return;
            }
            const args = {
                target: item,
                property: property,
                value: value,
                source: source
            };
            this.emit('update', args);
            item.onItemChanged && item.onItemChanged(args);
        },
        /**
         * Observe an item's properties specified in this.observedProperties and item.observed, called upon putSync.
         * @param item {module:xide/data/Model}
         * @private
         */
        _observe: function (item) {
            let props = this.observedProperties;
            item.observed && (props = props.concat(item.observed));
            props.forEach((property) => {
                item.property && item.property(property).observe(function (value) {
                    this._onItemChanged(item, property, value, this);
                }.bind(this));
            });
        },
        /**
         * Override setData to bring in dmodel's observe for new items.
         * @param data {object[]}
         * @returns {*}
         */
        setData: function (data) {
            const res = this.inherited(arguments);
            this.silent(true);
            data && data.forEach((e) => this._observe);
            this.silent(false);
            return res;
        }
    });
});