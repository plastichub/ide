define([
    'dcl/dcl',
    "dojo/_base/array",
    "dojo/dom-style",
    "dojo/dom-geometry", // domGeometry.position
    'xide/types',
    'xide/mixins/EventedMixin',
    "xide/_base/_Widget"
], function (dcl, array, domStyle, registry, domGeometry, types, _Widget) {
    return dcl(_Widget, {});
});