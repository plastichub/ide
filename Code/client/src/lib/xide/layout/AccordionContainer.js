define([
    'xdojo/declare',
    "xide/layout/LayoutContainer",
    "xide/mixins/EventedMixin"
], function (declare, LayoutContainer, EventedMixin) {
    return declare("xide/layout/AccordionContainer", [LayoutContainer, EventedMixin], {});
});