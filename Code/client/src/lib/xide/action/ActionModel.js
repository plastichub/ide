define([
    "xdojo/declare",
    'xide/action/Action',
    'xide/data/Model',
    "xide/data/Source",
    'xide/model/Path',
    'xide/utils'

], function (declare, Action, Model, Source, Path, utils) {

    const debug = false;
    /**
     * @mixin module:xide/action/ActionModel
     * @extends module:xide/data/Source
     * @extends module:xide/action/Action
     * @extends module:xide/mixins/EventedMixin
     */
    return declare("xide/action/ActionModel", [Action, Model, Source], {

        filterGroup: "item|view",
        keyboardMappings: null,
        bindWidgetProperties: [
            //2-way bindings for these props:
            'value',
            'icon',
            'disabled'
        ],
        items: null,
        onRemove: function () {
            _.invoke(this.getReferences(), 'destroy');
            this.keyboardMappings && _.invoke(this.keyboardMappings, "destroy");
            this.destroy();
        },
        shouldShow: function () {
            return true;
        },
        shouldDisable: function () {
            return false;
        },
        updateReference: function (selection, reference, visibility) {
            reference.set('disabled', this.shouldDisable(selection, reference, visibility));

            if (this.icon !== null && reference.icon !== null && this.icon !== reference.icon) {
                reference.set('icon', this.icon);
            }
            if (this.value !== null && reference.value !== null && this.value !== reference.value) {
                reference.set('value', this.value);
            }
        },
        refreshReferences: function (property, value) {
            this.getReferences().forEach((ref) => {
                ref.set(property, value);
            });
        },
        refresh: function (selection) {
            this._emit('refresh', {
                action: this,
                selection: selection
            });
            this.getReferences().forEach((ref) => {
                this.updateReference(selection, ref, ref.visibility);
            });
        },
        setProperty: function (key, value, updateReferences) {
            return this.set(key, value);
        },
        complete: function () {
            this.items = this.getChildren();
        },
        getParent: function () {
            const segments = this.command.split('/');
            if (segments.length > 1) {
                return this._store.getSync(segments.slice(0, segments.length - 1).join('/'));
            }
        },
        getParentCommand: function () {
            const segments = this.command.split('/');
            if (segments.length > 1) {
                return segments.slice(0, segments.length - 1).join('/');
            }
        },
        getSegments: function (command) {
            return command.split('/');
        },
        getRoot: function () {
            return this.command.split('/')[0];
        },
        getItemsAtBranch: function (items, path) {
            return new Path(path).getChildren(utils.pluck(items, 'command'), false);
        },
        getChildren: function () {
            const children = this.getItemsAtBranch(this._store.getAll(), this.command);
            //return an action from both stores
            const getAction = (command) => {
                return this._store.getSync(command);
            }

            //command strings to actions
            const toActions = (paths) => {
                const result = [];
                paths.forEach((path) => {
                    result.push(getAction(path));
                });
                return result;
            }
            return toActions(children);
        },
        /**
         * @param evt {object}
         * @param evt.parent {widget}
         * @param evt.widget {widget}
         * @param evt.visibility {string}
         * @private
         */
        _onWidgetCreated: function (evt) {
            if (evt.widget.addSource) {
                this.addReference(evt.widget, {
                    properties: {
                        "value": true
                    }
                }, true);
            } else {
                debug && console.warn('widget is not a reference! ', evt);
            }
        },
        _onCreated: function () {}
    });
});