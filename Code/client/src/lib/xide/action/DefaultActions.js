/** @module xide/action/DefaultActions **/
define([
    'xaction/DefaultActions'
], function (DefaultActions) {
    return DefaultActions;
});