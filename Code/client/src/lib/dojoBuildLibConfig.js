var dojoConfig = {
    __hasCache: {
        'host-browser':1,
        "host-node": 1,
        "dom":1,
        "dojo-amd-factory-scan":0,
        "dojo-has-api":1,
        "dojo-inject-api":0,
        "dojo-timeout-api":0,
        "dojo-trace-api":0,
        "dojo-log-api":0,
        "config-dojo-loader-catches":1,
        "dojo-dom-ready-api":0,
        "dojo-publish-privates":1,
        "dojo-config-api":1,
        "dojo-sniff":1,
        "dojo-sync-loader":0,
        "dojo-test-sniff":0,
        "config-deferredInstrumentation":0,
        "config-useDeferredInstrumentation":"report-unhandled-rejections",
        "config-tlmSiblingOfDojo":1,
        'xblox':true,
        'xlog':true,
        'xblox-ui':true,
        'xreload':true,
        'dojo-undef-api': true,
        'dstore-legacy':true
    },
    trace: 0,
    async: 1,
    packages: [
        {
            name:'xace',
            location:'xace/src'
        }
    ]
};
