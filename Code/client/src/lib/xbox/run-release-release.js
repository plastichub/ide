/**
 * This file is used to reconfigure parts of the loader at runtime for this application. We’ve put this extra
 * configuration in a separate file, instead of adding it directly to index.html, because it contains options that
 * can be shared if the application is run on both the client and the server.
 *
 * If you aren’t planning on running your app on both the client and the server, you could easily move this
 * configuration into index.html (as a dojoConfig object) if it makes your life easier.
 */
require({
    // The base path for all packages and modules. If you don't provide this, baseUrl defaults to the directory
    // that contains dojo.js. Since all packages are in the root, we just leave it blank. (If you change this, you
    // will also need to update app.profile.js).

    // A list of packages to register. Strictly speaking, you do not need to register any packages,
    // but you can't require "xbox" and get xbox/main.js if you do not register the "xbox" package (the loader will look
    // for a module at <baseUrl>/xbox.js instead). Unregistered packages also cannot use the packageMap feature, which
    // might be important to you if you need to relocate dependencies. TL;DR, register all your packages all the time:
    // it will make your life easier.
    packages: [
        // If you are registering a package that has an identical name and location, you can just pass a string
        // instead, and it will configure it using that string for both the "name" and "location" properties. Handy!
        'dojo',
        {
            name: 'dijit',
            location: 'build/dijit-release/dijit',
            packageMap: {}
        },
        {
            name: 'xgrid',
            location: 'xgrid',
            packageMap: {}
        },

        {
            name: 'dojox',
            location: 'build/dojox-release/dojox',
            packageMap: {}
        },
        {
            name: 'xapp',
            location: 'xapp',
            packageMap: {}
        },
        {
            name: 'xas',
            location: 'xas',
            packageMap: {}
        },
        {
            name: 'xbox',
            location: 'xbox',
            packageMap: {}
        },
        {
            name: 'xide',
            location: 'xide',
            packageMap: {}
        },
        {
            name: 'dgrid',
            location: 'dgrid',
            packageMap: {}
        },
        {
            name: 'xfile',
            location: 'build/xfile-release/xfile',
            packageMap: {}
        },
        {
            name: 'xblox',
            location: 'xblox',
            packageMap: {}
        },
        {
            name: 'dcl',
            location: 'dcl',
            packageMap: {}
        },
        {
            name: 'xlog',
            location: 'xlog',
            packageMap: {}
        },
        {
            name: 'davinci',
            location: 'davinci',
            packageMap: {}
        },
        {
            name: 'system',
            location: 'system',
            packageMap: {}
        },
        {
            name: 'orion',
            location: 'build/orion-release/orion',
            packageMap: {}
        },
        {
            name: 'preview',
            location: 'preview',
            packageMap: {}
        },
        {
            name: 'delite',
            location: 'delite',
            packageMap: {}
        },
        {
            name: 'xideve',
            location: 'xideve',
            packageMap: {}
        },
        {
            name: 'dstore',
            location: 'dstore',
            packageMap: {}
        },
        {
            name: 'xdojo',
            location: 'xdojo',
            packageMap: {}
        },
        {
            name: 'xdocker',
            location: 'xdocker',
            packageMap: {}
        },
        {
            name: 'xnode',
            location: 'xnode',
            packageMap: {}
        },
        {
            name: 'xaction',
            location: 'xaction/src',
            packageMap: {}
        },
        {
            name: 'xconsole',
            location: 'xconsole',
            packageMap: {}
        },
        'xbox'
    ],
    cache: {}
// Require 'xbox'. This loads the main application file, xbox/main.js.
}, ['xbox']);
