var debug=true;
var sctx=null;
var rtConfig="debug";
function JSCOMPILER_PRESERVE(){}


/*var xFileConfigMixin =%XFILE_CONFIG_MIXIN%;*/
var xFileConfigMixin ={"LAYOUT_PRESET":1,"PANEL_OPTIONS":{
    "ALLOW_COLUMN_RESIZE":true,
    "ALLOW_COLUMN_REORDER":true,
    "ALLOW_COLUMN_HIDE":true,
    "ALLOW_NEW_TABS":true,
    "ALLOW_MULTI_TAB":false,
    "ALLOW_INFO_VIEW":true,
    "ALLOW_LOG_VIEW":true,
    "ALLOW_BREADCRUMBS":false,
    "ALLOW_CONTEXT_MENU":true,
    "ALLOW_LAYOUT_SELECTOR":true,
    "ALLOW_SOURCE_SELECTOR":true
},"ALLOWED_ACTIONS":[0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],"FILE_PANEL_OPTIONS_LEFT":{"LAYOUT":2,"AUTO_OPEN":"true"},"FILE_PANEL_OPTIONS_MAIN":{"LAYOUT":2,"AUTO_OPEN":"true"},"FILE_PANEL_OPTIONS_RIGHT":{"LAYOUT":2,"AUTO_OPEN":"true"}};

var xFileConfig={

    //permissions:%permissions%,
    serviceUrl:'%RPC_URL%',
    permissions:[],
    mixins:[
        {
            declaredClass:'davinci.model.resource.Resource',
            mixin:{
                serviceUrl:'%RPC_URL%',
                singleton:true
            }
        },
        {
            declaredClass:'xide.manager.ServerActionBase',
            mixin:{
                serviceUrl:'%RPC_URL%',
                singleton:true
            }
        },
        {
            declaredClass:'xfile.manager.FileManager',
            mixin:{
                serviceUrl:'%RPC_URL%',
                singleton:true
            }
        },
        {
            declaredClass:'xide.manager.SettingsManager',
            mixin:{
                serviceUrl:'%RPC_URL%',
                singleton:true
            }
        },
        {
            declaredClass:'xide.manager.ResourceManager',
            mixin: {
                serviceUrl: '%RPC_URL%',
                singleton: true,
                resourceVariables: %RESOURCE_VARIABLES%
            }
        }


    ],
    WEB_ROOT:'%APP_URL%',
    FILES_STORE_SERVICE_CLASS:'XCOM_Directory_Service',
    RPC_PARAMS:{
        rpcUserField:'user',
        rpcUserValue:'%RPC_USER_VALUE%',
        rpcSignatureField:'sig',
        rpcSignatureToken:'%RPC_SIGNATURE_TOKEN%',
        rpcFixedParams:{}
    }

};
var xappPluginResources=[];