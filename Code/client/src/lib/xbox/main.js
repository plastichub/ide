define([
    'require',
    'xide/Features'
], function (require, declare, Features) {
    require([
        'dijit/dijitb',
        'dgrid/dgrid',
        'xfile/xfile',
        'xide/xide',
        'xide/utils',
        'xide/utils/StringUtils',
        'xide/utils/ObjectUtils',
        'xlang/i18',
        'xide/Lang'
    ], function (_dijit, _dgrid, _xfile, _xide, utils, StringUtils, ObjectUtils, i18, Lang) {

        function loadMain() {
            require([
                'xide/debug',
                'xbox/XBoxCommons',
                'xbox/Managers',                 //  App Manager
                'xfile/config',                  //  XFile config,
                'xbox/Views',
                'xbox/Widgets',
                'xbox/manager/Context',
                'xbox/GUIALL',
                'dojo/_base/window',
                'dojo/domReady!'
            ], function (debug, XBoxCommons, Managers, XFileConfig, Views, Widgets, Context, GUIALL, win) {

                //pick config from global
                var xFileConfiguration = typeof (xFileConfig) !== 'undefined' ? xFileConfig : null;

                //context
                var xasContext = new Context(xFileConfiguration);

                window.sctx = xasContext;

                //main, wrap in try
                var mainFn = function () {
                    try {

                        xasContext.prepare();

                        //construct managers
                        xasContext.constructManagers();
                        //init managers
                        xasContext.initManagers();

                        //init locals!
                        //xasContext.getLocals("dijit", "common");

                        //punch it
                        xasContext.getApplication().start(xFileConfiguration);

                        utils.destroy('#loadingWrapper');


                    } catch (e) {
                        console.error('main crash : ' + e, e);
                        console.dir(e.stack);
                        //debugger;
                    }
                };
                mainFn();
            });
        }
        try {
            Lang.loadLanguage().then(function () {
                loadMain();
            });
        } catch (e) {
            console.error('error loading main:', e);
        }
    });

});
