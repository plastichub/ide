var dojoConfig = {
    staticHasFeatures: {
        // The trace & log APIs are used for debugging the loader, so we don’t need them in the build
        'dojo-trace-api':0,
        'dojo-log-api':0,
        'dojo-bidi':0,
        // This causes normally private loader data to be exposed for debugging, so we don’t need that either
        'dojo-publish-privates':0,
        // We’re fully async, so get rid of the legacy loader
        'dojo-sync-loader':0,
        // dojo-xhr-factory relies on dojo-sync-loader
        'dojo-xhr-factory':0,
        // We aren’t loading tests in production
        'dojo-test-sniff':0,
        'dojo-firebug': false,
        'tab-split':true,
        'dojo-undef-api': true,
        'xblox-ui':false,
        'xlog':false,
        'xblox':false,
        'xideve':false,
        'xreload':false,
        'xidebeans':true,
        'delite':false,
        'xexpression':false,
        'filtrex':false,
        'ace-formatters':true,
        'xnode-ui':false,
        'xfile':true,
        'xcf-ui':false,
        'host-node':false,
        'xace':true,
        'drivers':false,
        'files':true,
        'devices':false,
        'debug':false,
        'FileConsole':true
    }
};