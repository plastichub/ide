define([
    "dcl/dcl",
    'xfile/manager/Context',
    'xbox/manager/Application',
    'xide/manager/ResourceManager'
], function (dcl,Context,Application,ResourceManager) {

    return dcl(Context,{
        declaredClass:"xfile.manager.Context",
        constructManagers:function() {
            this.application = this.createManager(Application,null);
            this.resourceManager = this.createManager(ResourceManager,null);
        },
        initManagers:function() {
            this.resourceManager.init();
        }
    });
});