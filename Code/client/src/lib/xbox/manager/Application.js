define([
    "dcl/dcl",
    'xdojo/has',
    'xide/manager/Application',
    'require',
    'xide/types',
    'xide/utils',
    'xbox/views/MainView',
    'xide/editor/Default'
], function (dcl,has,Application,require,types,utils,MainView,Default) {
    /**
     * @class xbox.manager.Application
     * @extends module:xide/manager/Application
     */
    return dcl(Application, {
        declaredClass:"xbox.manager.Application",
        onXFileReady:function(config,gridClass){
            Default.Implementation.ctx=this.ctx;
            var _require = require,
                thiz = this,
                mainView = thiz.mainView,
                container = mainView.layoutLeft,
                windowManager = this.ctx.getWindowManager(),
                breadcrumb = mainView && mainView.getBreadcrumb ? mainView.getBreadcrumb() : null,
                ctx = thiz.ctx;

            var newTarget= mainView.layoutCenter;
            if(mainView.getNewDefaultTab){
                newTarget = function(args){
                    return mainView.getNewDefaultTab(args);
                };
            }
            if(breadcrumb) {
                this.subscribe(types.EVENTS.ON_OPEN_VIEW, function (e) {
                    var view = e.view;
                    if(view instanceof gridClass) {
                        view.addHandle('click',view.on('click',function(){
                            breadcrumb.setSource(view);
                        }));
                        breadcrumb.setSource(view);
                        var srcStore = view.collection;
                        function _onChangeFolder(store,item,grid){
                            if(breadcrumb.grid!=grid){
                                breadcrumb.setSource(grid);
                            }
                            breadcrumb.clear();
                            breadcrumb.setPath('.',srcStore.getRootItem(),item.getPath(),store);
                        }

                        view._on('openFolder', function (evt) {
                            _onChangeFolder(srcStore,evt.item,view);
                        });
                    }
                });
            }



            _require(['xfile/factory/Store','xfile/types',
                'xfile/views/Grid',
                'xfile/views/FileGrid',
                'xfile/views/FilePreview',
                'xfile/ThumbRenderer'
            ],function(factory,types,Grid,FileGrid,FilePreview,ThumbRenderer){

                ctx.registerEditorExtension('Preview', 'mp4|ma4|mov|html|pdf|avi|mp3|mkv|ogg|png|jpg', 'fa-play', this, true, null, FilePreview.EditorClass, {
                    updateOnSelection: false,
                    leftLayoutContainer: newTarget,
                    ctx: ctx
                });

                var store = factory.createFileStore('root',null,config);
                try {

                    var grid = new FileGrid({
                        //selectedRenderer:ThumbRenderer,
                        newTabArgs:{
                            showHeader:true
                        },
                        style:'height:100%',
                        newTarget: newTarget,
                        collection: store.getDefaultCollection("./"),
                        showHeader: false,
                        ctx:thiz.ctx,
                        _parent:container,
                        _columns: {
                            "Name": true,
                            "Path": false,
                            "Size": false,
                            "Modified": false,
                            "Owner":false
                        }

                    },container.containerNode);

                    grid.startup();

                    grid.showStatusbar(false);

                    windowManager.registerView(grid,true);

                    if(breadcrumb){

                        breadcrumb.setSource(grid);

                        var srcStore = grid.collection;


                        function _onChangeFolder(store, item, grid) {
                            if (breadcrumb.grid != grid) {
                                breadcrumb.setSource(grid);
                            }
                            breadcrumb.clear();
                            breadcrumb.setPath('.', srcStore.getRootItem(), item.getPath(), store);
                        }

                        grid._on('openedFolder', function (evt) {
                            console.error('opened folder!!');
                            _onChangeFolder(srcStore, evt.item, grid);
                        });
                    }

                    if(has('phone')){
                        setTimeout(function(){
                            grid.showToolbar(true);
                            //grid.resize();
                        },1500);

                    };

                }catch(e){
                    logError(e,'onXFileReady');
                }
            });

        },
        createMainView:function(rootSelector){
            this.container = $(rootSelector  || '#root')[0];
            this.mainView = utils.addWidget(MainView,{
                ctx: this.ctx,
                config: this.config,
                persistent: false,
                windowManager:this.ctx.getWindowManager(),
                container:this.container
            },null,this.container,true);

            this.ctx.mainView = this.mainView;
        },
        start:function(showGUI,rootSelector){
            this.createMainView(rootSelector);
            this.doComponents();
        }
    });
});