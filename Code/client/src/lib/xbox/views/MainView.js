define([
    "dcl/dcl",
    'xide/views/_MainView',
    'xdocker/types'
], function (dcl,_MainView) {
    var RootView = dcl(null,{});
    return dcl([_MainView,RootView],{});
});
