define([
    "dojo/_base/declare",
    'xide/views/MainView',
    'xide/types',
    'xide/factory',
    'xide/utils',
    'xide/views/_MainViewMixin',
    'dojo/dom-construct',
    'dojo/has',

    'xide/widgets/Ribbon',
    'xaction/ActionStore',

    'xdocker/types',
    'xdocker/Docker',
    'xdocker/Docker2',
    'xdocker/Panel2',
    'xdocker/Frame2'

], function (declare, MainView, types, factory,utils,_MainViewMixin, domConstruct,has,Ribbon,ActionStore,dTypes,Docker,Docker2,Panel2,Frame2) {
    return declare("xcf.views.MainView", [MainView, _MainViewMixin], {
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Variables
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /***
         * var mainMenu, instance to our main menu
         */
        beanContextName: "main",
        mainMenu: null,
        docker:null,
        getNewAlternateTarget:function(){
            return this.layoutCenter;
        },
        resize:function(){


            this.inherited(arguments);

            if(this.docker){
                this.docker.resize();
            }
            if(this.layoutLeft){
                this.layoutLeft.resize();
            }
            if(this.layoutCenter){
                this.layoutCenter.resize();

            }

        },
        getDocker:function(){

            return this.docker;

        },
        _createDocker:function(where){

            var themeRoot = require.toUrl('xdocker');

            var docker = new Docker2($(this.layoutCenter.containerNode), {
                allowCollapse: true,
                responseRate: 10,
                allowContextMenu: false,
                themePath: themeRoot + '/Themes/',
                theme: 'white',
                wcPanelClass:Panel2,
                wcFrameClass:Frame2
            });

            this.docker = docker;

            docker.registerPanelType('DefaultFixed',Docker.defaultPaneType('','',false,false,true,null,true));

            docker.registerPanelType('DefaultTab',Docker.defaultPaneType('','',true,false,true,null,true));


            return docker;
        },
        prepareLayout: function (config) {


            //remove splitter
            if (this.layoutTop._splitterWidget) {
                utils.destroy(this.layoutTop._splitterWidget);
                this.layoutTop._splitterWidget = null;
            }

            this.resize();


            //create docker
            var docker = this._createDocker();
            this.docker = docker;

            var layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                w: '30%',
                h:'90%',
                title:'&nbsp;&nbsp;'
            });


            /*
            var layoutBottom = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.BOTTOM, null , {
                h: 100,
                title:'&nbsp;&nbsp;'
            });*/






            //addPanel: function (typeName, location, targetPanel, options) {
            var layoutCenter = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.RIGHT, layoutTop, {
                w: '80%',
                title:'Welcome',
                mixin: {
                    isDefault: true
                }
            });
            layoutCenter.title('Welcome');



            /*
            var layoutRight = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.RIGHT, layoutCenter, {
                w: '20%',
                title:'&nbsp;&nbsp;'
            });*/

            //this.resize();


            //layoutBottom.initSize('100%',100);

            docker.__update();
            //layoutBottom.__update();

            //docker.removePanel(layoutTop);



            /*layoutTop.initSize('90%','100%');*/

            /*
            return;

            var layoutLeft = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                w: '200px',
                title:'Left'
            });

            var layoutBottom = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.BOTTOM, null , {
                h: 100,
                title:'Bottom'
            });

            var layoutCenter = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.RIGHT, layoutLeft, {
                w: '80%',
                title:'Welcome',
                temp:true
            });



            var layoutRight = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.RIGHT, layoutCenter, {
                w: '20%',
                title:'Right'
            });
            */



            var frame = layoutTop.getFrame();
            frame.showTitlebar(false);
            this.layoutLeft = layoutTop;
            this._layoutCenter = this.layoutCenter;
            this.layoutCenter = layoutCenter;

            //this.layoutBottom = layoutBottom;
            //this.layoutRight = layoutRight;


            //this.resize();

            /*
            this.layoutLeft = docker.addPanel('Default', types.DOCKER.DOCK.LEFT, null, {
                w: '20%'
            });*/
        },
        initMainMenu: function () {

            return;

        },
        initToolbar: function () {
            this._createToolbar(null, null, this.layoutTop);
        },
        getActionStore:function(){

        },
        getRibbon:function(){
            return this.toolbar;
        },
        getToolbar:function(){
            return this.toolbar;
        },
        createWidgets: function () {

            this.initMainMenu();
            /*
            if(!has('ribbons')) {
                this.initToolbar();
            }else{
                */
                this.toolbar =  utils.addWidget(Ribbon,{
                    store:new ActionStore({}),
                    flat:false
                },this,this.layoutTop,true);
            //}
        },
        onReady: function () {

            var thiz = this;
            thiz.resize();
            setTimeout(function () {
                thiz.resize();
                factory.showStandBy(false);
            }, 1000);

            this.registerFileOperationEvents();//xfile/views/_MainViewMixin

            this.registerBeanEvents();//xfile/views/_MainViewMixin



            setTimeout(function () {
                thiz.layoutMain.resize();
                thiz.ctx.application.onMainViewReady(thiz);
                factory.publish(types.EVENTS.ON_MAIN_VIEW_READY, {
                    view: thiz
                });
            }, 8000);

            console.error('on main view ready');
        },
        ////////////////////////////////////////////////////////////////////////////
        //
        //  Action toolbar & view lifecycle handling
        //
        ////////////////////////////////////////////////////////////////////////////
        onViewAdded: function () {},
        onViewShow: function (evt) {

            //take care about the action toolbar
            var view = evt['view'];
            if (view &&                         // must have a valid view
                view['hasItemActions'] != null && // must comply with the bean protocol
                view.hasItemActions() === true) {
                evt['owner'] = view;

                if (view.beanContextName) {
                    evt['beanContextName'] = view.beanContextName;
                }

                this.onItemSelected(evt);
            }
        },
        onItemSelected: function (evt) {

            if (evt[this.id]) {
                return;
            }

            evt[this.id] = true;

            var _debug = false;

            var owner = evt['owner'] || evt['view'];
            if (owner &&                         // must have a valid view
                owner['hasItemActions'] != null && // must comply with the bean protocol
                owner.hasItemActions() === true) {

                if (!evt.beanContextName && evt.view && evt.view.beanContextName) {
                    evt.beanContextName = evt.view.beanContextName;
                }

                if (!evt.beanContextName && evt.owner && evt.owner.beanContextName) {
                    evt.beanContextName = evt.owner.beanContextName;
                }
                if (!evt.beanContextName) {
                    if (_debug) {
                        console.log('have no beancontext');
                    }
                    //console.warn('ignore itemSelection: have no beanContextName');
                    //return;
                }

                if (!this.isMyBeanContext(evt)) {
                    console.warn('not my bean context', evt);
                    if (_debug) {
                        console.log('not my beancontext');
                    }
                    return;
                }

                var item = evt.item || owner.item || owner.getItem();

                if (!item || _.isEmpty(item)) {
                    console.log('is empty');
                    return;
                }

                if (this._lastItem && this._lastOwner && item !== null && this._lastItem == item && owner && this._lastOwner == owner) {
                    if (_debug) {
                        console.log('same item', item);
                        console.log('   items view', owner);
                    }
                    return;
                }


                this._lastItem = item;
                this._lastOwner = owner;

                var actions = owner.getItemActions();

                if (_debug) {
                    console.log('update !!! ', item);
                }
                //console.log('update item actions for bcName '  + evt.beanContextName,evt);

                this.addToHistory({
                    item: item,
                    context: owner,
                    beanContextName: evt.beanContextName,
                    event: evt
                });

                this.updateItemActions(owner.getItem(), owner, actions);
            }
        },
        onViewRemoved: function (evt) {

            this.removeFromHistory({
                event: types.EVENTS.ON_VIEW_REMOVED,
                item: evt.view.item,
                context: evt.view
            });

            //console.log('bean history:',this._beanHistory);

            //console.log('last item',this.getLastHistoryItem());

            var lastItem = this.getLastHistoryItem();

            if (lastItem && lastItem.item && lastItem.context && lastItem.event) {

                //console.log('on view removed,switch to ', lastItem);
                lastItem.event[this.id] = null;
                //this.onItemSelected(lastItem.event);

            }



        },
        _onResize:function(){

            this.layoutMain.resize();
            this.layoutTop.resize();
            if(this.layoutLeft) {
                this.layoutLeft.resize();
            }
        },
        startup: function () {

            var thiz = this;

            this.inherited(arguments);

            this.createWidgets();

            this.onReady();
            this.initHistory();

            //done in driver handler
            //this.subscribe(types.EVENTS.ON_DRIVER_SELECTED, this.onDriverSelected);
            /*
            this.subscribe(types.EVENTS.ON_VIEW_SHOW, this.onViewShow);
            this.subscribe(types.EVENTS.ON_VIEW_REMOVED, this.onViewRemoved);
            this.subscribe(types.EVENTS.ON_ITEM_SELECTED, this.onItemSelected);
            */
            this.subscribe(types.EVENTS.RESIZE, this._onResize);
        }
    });
});
