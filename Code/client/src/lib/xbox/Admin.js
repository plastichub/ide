var dojoConfig = {
    async: true,
    parseOnLoad: false,
    isDebug: 0,
    baseUrl: '%APP_URL%/lib/',
    tlmSiblingOfDojo: 0,
    useCustomLogger:false,
    packages: [
        { name: "dojo", location: "dojo" },
        { name: "dojox", location: "dojox" },
        { name: "dijit", location: "dijit" },
        { name: "cbtree",location: "cbtree" },
        { name: "xbox",location: "xbox" },
        { name: "xfile",location: "xfile" }
    ],
    has:{
        'dojo-undef-api': true,
        'dojo-firebug': false
    },
    locale:'en'
};

var isMaster = true;
var debug=true;
var device=null;
var sctx=null;
var ctx=null;
var cctx=null;
var mctx=null;
var rtConfig="debug";
var returnUrl= "";
var dataHost ="%APP_URL%/../server/service/";