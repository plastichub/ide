define(["xide/types","xide/utils"], function (types,utils) {

    //stub
    var wcDocker = {};

    /**
     * Enumerated Docking positions.
     * @version 3.0.0
     * @memberOf module:wcDocker
     * @enum {String} module:wcDocker.DOCK
     */
    wcDocker.DOCK = {
        /** A floating panel that blocks input until closed */
        MODAL: 'modal',
        /** A floating panel */
        FLOAT: 'float',
        /** Docks to the top of a target or window */
        TOP: 'top',
        /** Docks to the left of a target or window */
        LEFT: 'left',
        /** Docks to the right of a target or window */
        RIGHT: 'right',
        /** Docks to the bottom of a target or window */
        BOTTOM: 'bottom',
        /** Docks as another tabbed item along with the target */
        STACKED: 'stacked'
    };

    /**
     * Enumerated Layout wcDocker.
     * @memberOf module:wcDocker
     * @version 3.0.0
     * @enum {String} module:wcDocker.LAYOUT
     */
    wcDocker.LAYOUT = {
        /** Contains a single div item without management using a {@link module:wcLayoutSimple}, it is up to you to populate it however you wish. */
        SIMPLE: 'wcLayoutSimple',
        /** Manages a table grid layout using {@link module:wcLayoutTable}, this is the default layout used if none is specified. **/
        TABLE: 'wcLayoutTable'
    };

    /**
     * Enumerated Internal events
     * @version 3.0.0
     * @memberOf module:wcDocker
     * @enum {String} module:wcDocker.EVENT
     */
    wcDocker.EVENT = {
        INIT: 'panelInit',
        LOADED: 'dockerLoaded',
        UPDATED: 'panelUpdated',
        VISIBILITY_CHANGED: 'panelVisibilityChanged',
        BEGIN_DOCK: 'panelBeginDock',
        END_DOCK: 'panelEndDock',
        GAIN_FOCUS: 'panelGainFocus',
        LOST_FOCUS: 'panelLostFocus',
        CLOSING: 'panelClosing',
        CLOSED: 'panelClosed',
        PERSISTENT_CLOSED: 'panelPersistentClosed',
        PERSISTENT_OPENED: 'panelPersistentOpened',
        BUTTON: 'panelButton',
        ATTACHED: 'panelAttached',
        DETACHED: 'panelDetached',
        MOVE_STARTED: 'panelMoveStarted',
        MOVE_ENDED: 'panelMoveEnded',
        MOVED: 'panelMoved',
        RESIZE_STARTED: 'panelResizeStarted',
        RESIZE_ENDED: 'panelResizeEnded',
        RESIZED: 'panelResized',
        ORDER_CHANGED: 'panelOrderChanged',
        SCROLLED: 'panelScrolled',
        SAVE_LAYOUT: 'layoutSave',
        RESTORE_LAYOUT: 'layoutRestore',
        CUSTOM_TAB_CHANGED: 'customTabChanged',
        CUSTOM_TAB_CLOSED: 'customTabClosed',
        BEGIN_FLOAT_RESIZE: 'beginFloatResize',
        END_FLOAT_RESIZE: 'endFloatResize',
        BEGIN_RESIZE:"beginResize",
        END_RESIZE:"endResize",CUSTOM_TAB_CLOSED: 'customTabClosed'
    };

    /**
     * The name of the placeholder panel.
     * @private
     * @memberOf module:wcDocker
     * @constant {String} module:wcDocker.PANEL_PLACEHOLDER
     */
    wcDocker.PANEL_PLACEHOLDER = '__wcDockerPlaceholderPanel';

    /**
     * Used when [adding]{@link module:wcDocker#addPanel} or [moving]{@link module:wcDocker#movePanel} a panel to designate the target location as collapsed.<br>
     * Must be used with [docking]{@link module:wcDocker.DOCK} positions LEFT, RIGHT, or BOTTOM only.
     * @memberOf module:wcDocker
     * @constant {String} module:wcDocker.COLLAPSED
     */
    wcDocker.COLLAPSED = '__wcDockerCollapsedPanel';

    /**
     * Used for the splitter bar orientation.
     * @version 3.0.0
     * @memberOf module:wcDocker
     * @enum {Boolean} module:wcDocker.ORIENTATION
     */
    wcDocker.ORIENTATION = {
        /** Top and Bottom panes */
        VERTICAL: false,
        /** Left and Right panes */
        HORIZONTAL: true
    };
    /**
     * Used to determine the position of tabbed widgets for stacked panels.<br>
     * <b>Note:</b> Not supported on IE8 or below.
     * @version 3.0.0
     * @enum {String} module:wcDocker.TAB
     * @memberOf module:wcDocker
     */
    wcDocker.TAB = {
        /** The default, puts tabs at the top of the frame */
        TOP: 'top',
        /** Puts tabs on the left side of the frame */
        LEFT: 'left',
        /** Puts tabs on the right side of the frame */
        RIGHT: 'right',
        /** Puts tabs on the bottom of the frame */
        BOTTOM: 'bottom'
    };

    if(!types.DOCKER){
        types.DOCKER=wcDocker;
    }

    return wcDocker;

});