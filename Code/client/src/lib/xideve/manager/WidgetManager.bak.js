/** @module xideve/manager/WidgetManager **/
define([
    'dcl/dcl',
    "xdojo/declare",
    "dojo/_base/lang",
    'dojo/dom-construct',
    "xide/manager/ManagerBase",
    "xide/mixins/ReloadMixin",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xide/layout/ContentPane',
    'xide/widgets/_MenuMixin2',
    'xide/mixins/ActionMixin',
    'xide/action/Action',
    './WidgetManagerBlox',
    './WidgetManagerJS',
    'xide/mixins/VariableMixin',
    "davinci/ve/metadata"
], function (dcl,declare, lang, domConstruct, ManagerBase,ReloadMixin,types, utils, factory, ContentPane,_MenuMixin,ActionMixin,Action,WidgetManagerBlox,WidgetManagerJS,VariableMixin,Metadata) {

    const contextMenu=false;
    /**
     * Extend standard action visibility for widget properties
     */
    utils.mixin(types.ACTION_VISIBILITY, {
        /**
         * Action visibility 'WIDGET_PROPERTY'
         * @memberOf module:xide/types/ACTION_VISIBILITY
         */
        WIDGET_PROPERTY: 'WIDGET_PROPERTY'
    });

    const _foo = null,
          _nativeEvents = {
          "onclick": _foo,
          "ondblclick":_foo,
          "onmousedown":_foo,
          "onmouseup":_foo,
          "onmouseover":_foo,
          "onmousemove":_foo,
          "onmouseout":_foo,
          "onkeypress":_foo,
          "onkeydown":_foo,
          "onkeyup":_foo,
          "onfocus":_foo,
          "onblur":_foo,
          "onchange":_foo
      };
    /**
     * Handler for davinci context and visual editor events. Currently just a stub. Concrete impl. happens in app subclasses.
     *
     * @class module:xide/manager/WidgetManager
     *
     * @augments  module:xide/manager/ManagerBase
     * @augments  module:xide/widgets/_MenuMixin
     * @augments  module:xide/mixins/ActionMixin
     */
    //ActionMixin
    return dcl([ManagerBase,ReloadMixin.dcl,WidgetManagerBlox,WidgetManagerJS,_MenuMixin,VariableMixin.dcl],{
            declaredClass:"xideve/manager/WidgetManager",
            /**
             * currentContext holds the current davinci.ve.Context instance
             */
            currentContext: null,

            _currentWidget:null,
            /**
             * This class has actions for this visibility:
             */
            visibility:types.ACTION_VISIBILITY.WIDGET_PROPERTY,
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Utils
            //
            /////////////////////////////////////////////////////////////////////////////////////
            onReloaded:function(){

                console.log('reloaded');


                /**
                var rm = this.ctx.resourceManager;

                var str = "%%XIDEVE_LIB_ROOT%%/%%test%%";

                rm.setVariable('test','hola');

                //console.log(_.compact(rm.resourceVariables));

                var variableDelimiters={
                    begin: "%%",
                    end: "%%"
                };
                var ocurr = this._findOcurrences(str, variableDelimiters);

                if (ocurr) {
                    for (var j = 0; j < ocurr.length; j++) {
                        var el = ocurr[j];
                        var _variableName = utils.replaceAll(variableDelimiters.begin, '', el);
                        _variableName = utils.replaceAll(variableDelimiters.end, '', el);
                        str = utils.replaceAll(el, rm.getVariable(_variableName), str);
                    }
                }
                //console.log('resolved ' + str);

                var hash = {
                    'XIDEVE_LIB_ROOT':'test',
                    'test':'aaa'
                };



                //console.profile('2');
                var a= utils.multipleReplace('XIDEVE_LIB_ROOT/test/test',rm.resourceVariables);
                console.log('resolved b ' +a);


                var b1= utils.replace('%%XIDEVE_LIB_ROOT%%/test/%%hola%%',null,rm.resourceVariables,variableDelimiters);
                console.log('replaced a ' + b1);

                var b= utils.replace('%%XIDEVE_LIB_ROOT%%/test/%%JQUERY_THEME%%',null,rm.resourceVariables,variableDelimiters);

                console.log('replaced c ' + b,rm.resourceVariables);

                //console.profileEnd('2');

                */



            },

            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Event callbacks
            //
            /////////////////////////////////////////////////////////////////////////////////////

            /**
             * @TODO: Currently wrong place to dance to create block palettes
             *
             *
             * Event when an editor create a 'library container' for palette, outline etc...
             *
             * This will add shared blocks 'xblox' into an additional palette, so a user
             * just drop in his 'scripts' onto widgets
             *
             * The current procedure is:
             *
             * 1. let ContextManager load all the scene's blocks
             * 2. publish 'createBlockPalette', wait for consumers to fill it up with items
             * 3. when any consumer has blocks for this editor, then actually create the palette.
             *
             *
             *
             * @param evt {Object} event data
             * @param evt.item {File} the file object of the editor
             * @param evt.container {LayoutContainer} the container created (Accordion, Tab,..)
             * @param evt.editor {VisualEditor} the editor
             * @param evt.context {xide/manager/Context} the root context of the editor
             */
            onLibraryContainerCreated:function(evt){


            },
            /**
             * Event when an editor context and its containing application has been fully
             * loaded.
             *
             * @param evt {Object} event data
             * @param evt.context {davinci/ve/Context} the davinci context
             * @param evt.appContext {xapp/manager/Context} the xapp context
             * @param evt.settings {Object} the application's settings
             * @param evt.ctx {xide/manager/Context} the root context (IDE context)
             * @param evt.global {Object} the loaded document's global
             * @param evt.blockScopes {xblox/model/Scope[]} the loaded xblox scopes
             *
             */
            onContextReady:function(evt){

            },
            _completeEventInfo:function(params){


                const thiz = this;
                //set to context to the selected widget
                const expressionContext = params.target || null;

                //get davinci context
                const widgetContext = params.widget ? params.target.getContext() : null;



                if(!widgetContext.editor){
                    params.editor = widgetContext._edit_context.editor;
                }else{
                    params.editor = widgetContext.editor;
                }

                //get global
                const global = widgetContext ? widgetContext.getGlobal() : null;

                //get app context
                const appContext = widgetContext.appContext;

                //the actual application
                const app  = appContext.application;

                let node  = expressionContext.domNode || expressionContext;
                if(expressionContext.isDijitWidget) {
                    node = expressionContext.dijitWidget.domNode;
                }



                params.appSettings = app.settings;//the applications settings
                params.appContext = appContext;
                params.app = app;
                params.global = global;
                params.node = node;
                params.widgetContext = expressionContext;
                params.dijitWidget = expressionContext.isDijitWidget ? expressionContext.dijitWidget : null;

                return params;


            },
            onWidgetPropertyChanged:function(evt){

                const params = this._completeEventInfo(evt);

                console.log('widget changed ' , params);

                const prop = params.widgetProperty, node = params.node, widget = params.dijitWidget, _isNative = prop in _nativeEvents, global = params.global;


                if(prop && node){


                    if(_isNative &&  widget && widget.on){

                        if(node[prop]){
                            console.log('asdfasdf');

                        }

                        node.removeAttribute(prop);

                        if(node[prop]){
                            node.removeEventListener(prop,node[prop]);
                        }
                        node[prop] = null;//

                        global.dojo.setAttr(params.node,params.widgetProperty,'');

                        global.dojo.setAttr(params.node,params.widgetProperty,params.value);

                        /*
                        if(widget['__' + prop]){
                            widget['__' + prop].remove();
                        }

                        var _evt = '' + prop.replace('on','');
                        widget['__' + prop] = widget.on(_evt,function(){
                            //console.log('asdfasdf');
                            global.eval(params.value,widget);

                        });
                        */
                    }


                    //params.node.removeEventListener(prop);

                    /*
                    node[params.widgetProperty] = null;

                    params.node[prop] = function(){
                        console.log('noob');
                    };
                    params.global.dojo.setAttr(params.node,params.widgetProperty,params.value);
                    */
                }

            },
            /**
             * Callback when a widget property has been rendered. This is common entry only. It
             * @param evt
             */
            onWidgetPropertyRendered: function (evt) {

                //avoid multiple handling
                if (evt.wmHandled) {
                    return;
                }

                evt.wmHandled = true;

                const prop = evt['row'];

                //react per widget property type
                switch (prop.type) {

                    case 'file':
                    {
                        this.renderFileWidget(prop, evt['node'], evt['view']);
                        break;
                    }

                    case 'block':
                    {
                        this.renderBlockWidget(prop, evt['node'], evt['view']);
                        break;
                    }

                    case 'number':
                    {
                        this.onNumberPropertyRendered(prop, evt['node'], evt['view'],evt['row']);
                        break;
                    }
                    default:
                    {
                    }
                }
            },
            /**
             *
             * Default actions for widget/node property. This should
             * support :
             * - 'Bind to Javascript'  and a few variants:
             *  - 'Expression'
             *  - 'Bind to data or store' (bi-direction double xwire with possibly in/outbound filters)
             *  - 'XBlox'
             */
            defaultNumberActions:function(context){

                const thiz=this;

                const _handlerJS = function(command,menu,owner,menuItem,action){
                    thiz.onJavascript(action.context);
                };
                
                const _js = Action.createDefault('Javascript','fa-code','Widget/Bind to Javascript','widgetPropertyAction',_handlerJS);
                _js.setVisibility(this.visibility,{});
                _js.context = context;



                const _handlerBlox = function(command,menu,owner,menuItem,action){
                    thiz.onXBlox(action.context);
                };

                const _blox = Action.createDefault('XBlox','fa-play-circle-o','Widget/Bind to XBlox','widgetPropertyAction',_handlerBlox);
                _blox.setVisibility(types.ACTION_VISIBILITY.WIDGET_PROPERTY,{});
                _blox.context = context;


                const _actions = [
                    _js,
                    _blox
                ];

                return _actions;
            },
            /**
             * 
             * @param menu
             * @param prop
             * @param view
             * @param row
             * @private
             */
            _showContextActions:function(menu,prop,view,row){


                if(!menu.actions) {

                    const actions = this.defaultNumberActions({
                        prop:prop,
                        view:view,
                        widget:view._widget, //current selection
                        row:row
                    });

                    //tell every body, xblox and others might add some more actions
                    this.publish(types.EVENTS.ON_SET_WIDGET_PROPERTY_ACTIONS,{
                        actions:actions,
                        property:prop,
                        view:view,
                        menu:menu
                    });

                    this._renderActions(actions, menu,null);

                    menu.actions = actions;
                }
            },
            /**
             * Create context menu on the widget property wizard button. Render
             * the actions onOpen.

             * @param widget
             * @param prop
             * @param view
             * @private
             */
            _createContextMenu:function(widget,prop,view,row){

                //skip if we've got a menu already
                if(widget._ctxMenu){
                    return;
                }
                //custom open, forward
                const _openFn = function(menu){
                    this._showContextActions(menu,prop,view,row);
                }.bind(this);

                //done in _MenuMixin
                widget._ctxMenu = this.createContextMenu(widget,_openFn,{
                    leftClickToOpen:true
                });

                widget.row = row;


                return widget._ctxMenu;

            },
            /**
             *
             * @param node
             * @param prop
             * @param view
             * @param _button
             */
            doWidgetPropWizard:function(node,prop,view,_button,row){
                this._createContextMenu(_button,prop,view,row);
            },
            /**
             *
             * @param prop
             * @param node
             * @param view
             */
            renderBlockWidget:function(prop, node, view){

                //runBlox('workspace://min.xblox','e813a85f-7511-723f-bc1f-c13698456a5b',this)
                console.log('block rendered',arguments);

                //x-script : view._widget.domNode



            },
            /**
             * Callback when a 'number' property has been rendered. This adds a 'wizard' button
             * right side of the widget property value field.
             *
             * @param prop
             * @param node
             * @param view
             */
            onNumberPropertyRendered: function (prop, node, view,row) {

                let where = node;

                if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                    where = node.parentNode.nextSibling;
                }

                //var _button = factory.createButton(where, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);

                const _button = factory.createSimpleButton('','fa-magic',null,{
                    style:"margin-left:11px !important; border-radius:2px;"
                },where);

                this.doWidgetPropWizard(node, prop, view,_button,row);
            },
            /**
             * Callback when a ui-designer's context has been fully loaded. The context is tracked.
             * This is currently loading additional modules!
             * @param context
             */
            onContextLoaded:function (context) {

                this.currentContext = context;
            },
            /**
             * Callback when a 'number' property has been rendered. This adds a 'wizard' button
             * right side of the widget property value field.
             *
             * @param prop
             * @param node
             * @param view
             */
            onEventPropertyRendered: function (prop, node, view) {

                let where = node;

                if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                    where = node.parentNode.nextSibling;
                }

                //var _button = factory.createButton(where, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);
                const _button = factory.createSimpleButton('','fa-magic',null,{
                    style:"margin-left:11px !important; border-radius:2px;"
                },where);

                this.doWidgetPropWizard(node, prop, view,_button);
            },
            /**
             * Event when the event property section is being rendered,
             * fish out
             * @param evt
             */

            onEventSelectionRendered:function(evt){

                console.log('onEventSelectionRendered : ' , evt);
                const pageTemplate = evt.pageTemplate;

                for (let i = 0; i < pageTemplate.length; i++) {

                    const page = pageTemplate[i];
                    const propNode = utils.find('.propertyExtra',dojo.byId(page.rowId) , true);

                    //var _button = factory.createButton(propNode, "el-icon-magic", "elusiveButton", "margin-left:11px !important; border-radius:2px;", "", null, this);
                    const _button = factory.createSimpleButton('','fa-magic',null,{
                        style:"margin-left:11px !important; border-radius:2px;",
                        id:'btn_sel' + utils.createUUID()
                    },propNode);


                    this.doWidgetPropWizard(null, null, evt.eventSelection,_button,page);

                }

            },

            /**
             * Callback when user selected another widget.
             * @param evt {Array} the current ui-designer selection.
             */
            onSelectionChanged: function (evt) {

                console.log('selection changed');
            },

            /**
             * Callback when user selected a widget
             * @param evt {Array}
             */
            onWidgetSelected: function (evt) {

                console.log('widget selected', evt);

                if(evt.isFake === true){
                    return;
                }

                this._currentWidget = evt.selection;

                const mainView = this.ctx.getApplication().mainView;

                const selection = evt.selection;



                //update description in main view
                // when a widget is selected, pick the description from meta data and place it the right - bottom panel
                const layoutRightMain = mainView.getLayoutRightMain ? mainView.getLayoutRightMain() : null;
                if (evt && lang.isArray(selection) && selection.length == 1) {

                    const widgetProps = selection[0];

                    let _description = Metadata.getOamDescriptivePropertyForType(widgetProps.type, 'description');
                    if(!_description){
                        //Metadata.getWidgetDescriptorForType(widgetProps.type) ||

                        _description  = Metadata.getOamDescriptivePropertyForType(widgetProps.type, 'title');
                        if(_description && _description.value){
                            _description = _description.value;
                        }
                    }else{

                        _description = _description.value;

                    }

                    if(!_description && widgetProps.metadata && widgetProps.metadata.description && widgetProps.metadata.description.value){
                        _description = widgetProps.metadata.description.value;
                    }

                    //render description on info panel
                    if (_description) {
                        if (mainView) {
                            //this should return a tab-container
                            if(mainView.getRightBottomTarget) {
                                const parent = mainView.getRightBottomTarget(true, this._doOpenRight);
                                if (parent) {
                                    if (this._doOpenRight) {
                                        this._doOpenRight = false;
                                    }
                                    const _pane = utils.addWidget(ContentPane, {
                                        delegate: this,
                                        style: "width:100%;height:100%;padding:0px",
                                        parentContainer: parent,
                                        title: 'Description',
                                        closable: true,
                                        splitter: true
                                    }, this, parent, true);
                                    _pane.containerNode.innerHTML = _description;
                                }
                            }
                        }
                    }

                    //now publish on event bus
                    selection[0].beanType = 'widget';

                    this.publish(types.EVENTS.ON_ITEM_SELECTED, {
                        item: selection[0],
                        beanType: 'widget',
                        view: evt.context.editor
                    }, evt.context.editor);//re-root owner of the publisher to the current editor




                } else {
                    mainView.getRightBottomTarget && mainView.getRightBottomTarget(true, false);
                }

                const editor = evt.context.getVisualEditor();
                editor._emit('selectionChanged',{
                    selection:selection
                });

                layoutRightMain && layoutRightMain.resize();


            },
            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  UX factory and utils
            //
            /////////////////////////////////////////////////////////////////////////////////////
            /**
             * Renders a file picker button at a given node
             *
             * @param prop {Array} : property data
             * @param node {HTMLElement}
             * @param view {davinci/ve/views/SwitchingStyleView}
             */
            renderFileWidget: function (prop, node, view) {
                let where = node;
                if (node.parentNode && node.parentNode.nextSibling && node.parentNode.nextSibling.className.indexOf('propertyExtra') != -1) {
                    where = node.parentNode.nextSibling;//should be the
                }

                const selectButton = factory.createButton(domConstruct.create('div'), "el-icon-folder-open", "elusiveButton", "", "", function () {
                    this.onSelectFile(node, prop, view);
                }.bind(this), this);

                dojo.place(selectButton.domNode, where, 'after');

            },

            /////////////////////////////////////////////////////////////////////////////////////
            //
            //  Standard XIDE context calls
            //
            /////////////////////////////////////////////////////////////////////////////////////
            /**
             * Init is being called upon {link:xide/manager/Context}::initManagers()
             *
             */
            init: function () {

                this.inherited(arguments);

                this.initReload();
                const thiz= this;


                this.subscribe("/davinci/ui/onSelectionChanged", this.onSelectionChanged);
                this.subscribe("/davinci/ui/widgetSelected", this.onWidgetSelected);
                this.subscribe("/davinci/ui/context/loaded", this.onContextLoaded);

                this.subscribe([

                    types.EVENTS.WIDGET_PROPERTY_RENDERED,  //extend style view additional actions per widget/node properties
                    types.EVENTS.ON_EVENT_SELECTION_RENDERED,
                    types.EVENTS.ON_WIDGET_PROPERTY_CHANGED,
                    types.EVENTS.ON_LIBRARY_CONTAINER_CREATED,
                    types.EVENTS.ON_CONTEXT_READY

                ]);



            }
        });
});
