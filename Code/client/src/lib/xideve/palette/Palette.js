define([
    'xdojo/declare',
    'xide/utils',
    'davinci/Workbench',
    'davinci/library',
    'davinci/Runtime',
    'xide/lodash',
    'xide/$',
    'dstore/Trackable',
    'xide/data/TreeMemory',
    'davinci/ve/metadata',
    'dojo/promise/all',
    'dojo/Deferred',
    './Grid',
    './Model'
], function (declare, utils, Workbench, Library, Runtime, _,
    $, Trackable, TreeMemory,
    Metadata, all, Deferred, GridCSourcelass, Model
) {

    console.clear();
    const test = false;

    const _context = () => {
        try {
            return Runtime.currentEditor.getContext();
        } catch (e) {

        }
    }

    var ctx = window.sctx,
        root;

    function createContainer(ctx, title) {
        var leftContainer = ctx.mainView.leftLayoutContainer;
        return leftContainer.createTab(title, 'fa-cube', true, null, {
            parentContainer: leftContainer,
            open: true,
            icon: 'fa-folder'
        });
    }

    ///////////////////////////////////////////////////////////////
    //
    //
    const Module = declare('m', null, []);
    class Palette {
        // Whether the given preset has created its PaletteFolder and PaletteItems
        constructor(context) {
            this.ctx = context;
            this._presetCreated = {};
            this._presetSections = {}; // Assoc array of all paletteItem objects, indexed by [preset][section]
            this._userWidgetSection = {
                "id": "$$UserWidgets$$",
                "name": "User Widgets",
                "includes": []
            }
        }
        _prepareSectionItem(item, section, paletteItemGroup) {
            var $wm = Metadata.getLibraryMetadataForType(item.type);
            item.$library = $wm;
            item.section = section;
            item._paletteItemGroup = paletteItemGroup;
        }
        _createSectionItems(section, preset, widgetList) {
            section.items = [];
            var collections = preset.collections;
            var includes = section.includes || [];
            for (var inc = 0; inc < includes.length; inc++) {
                var includeValue = includes[inc];
                // Each item in "includes" property can be an array of strings or string
                var includeArray = _.isArray(includeValue) ? includeValue : [includeValue];
                var sectionItems = [];
                for (var ii = 0; ii < includeArray.length; ii++) {
                    var includeItem = includeArray[ii];
                    var items = [];
                    if (includeItem.substr(0, 5) === 'type:') {
                        // explicit widget type
                        var item = Metadata.getWidgetDescriptorForType(includeItem.substr(5));
                        if (item) {
                            items.push(item);
                        }
                    } else {
                        items = Metadata.getWidgetsWithTag(includeItem);
                    }
                    for (var itemindex = 0; itemindex < items.length; itemindex++) {
                        var item = items[itemindex];
                        var newItem = dojo.clone(item);
                        this._prepareSectionItem(newItem, section, this._paletteItemGroupCount);
                        sectionItems.push(newItem);
                    }
                }
                // Sort sectionItems based on order in "collections" property
                var sortedItems = [];
                // Created a sorted list of items, using preset.collections to define the order
                // of widgets within this group.
                if (collections && collections.length) {
                    for (var co = 0; co < collections.length; co++) {
                        var collection = collections[co];
                        var si = 0;
                        while (si < sectionItems.length) {
                            var sectionItem = sectionItems[si];
                            if (sectionItem.collection == collection.id) {
                                sortedItems.push(sectionItem);
                                sectionItems.splice(si, 1);
                            } else {
                                si++;
                            }
                        }
                    }
                    // Add any remaining section items to end of sortedItems
                    for (var si = 0; si < sectionItems.length; si++) {
                        sortedItems.push(sectionItems[si]);
                    }
                } else {
                    sortedItems = sectionItems;
                }
                for (var si = 0; si < sortedItems.length; si++) {
                    var sortedItem = sortedItems[si];
                    var idx = widgetList.indexOf(sortedItem.type);
                    if (idx >= 0) {
                        // Remove the current widget type from widgetList array
                        // to indicate that the given widget has been added to palette
                        widgetList.splice(idx, 1);
                    }
                    section.items.push(sortedItem);
                }
                this._paletteItemGroupCount++;
            }
        }
        initMeta(ctx) {
            var metaRoot = require.toUrl('xideve/metadata/');
            return Metadata.init(ctx || window.sctx, metaRoot);
        }
        init() {
            var metaRoot = require.toUrl('xideve/metadata/');
            var c = Metadata.init(sctx, metaRoot);
            var allLibraries = Metadata.getLibrary();
            var userLibs = Library.getUserLibs(Workbench.getProject()) || {};
            var libraries = {};
            const subs = [];

            function findInAll(name, version) {
                for (var n in allLibraries) {
                    if (allLibraries.hasOwnProperty(n)) {
                        var lib = allLibraries[n];
                        if (lib.name === name && lib.version === version) {
                            var ro = {};
                            ro[name] = allLibraries[n];
                            return ro;
                        }
                    }
                }
                return null;
            }

            userLibs.forEach((ulib) => {
                const library = findInAll(ulib.id, ulib.version);
                dojo.mixin(libraries, library);
            });
            var customWidgetDescriptors = {};

            const customs = new Deferred();
            subs.push(customs);
            const pWidgets = Library.getCustomWidgets();
            if (pWidgets) {
                pWidgets.then((args) => {
                    var customWidgets = args;
                    var customWidgetDescriptors = Library.getCustomWidgetDescriptors();
                    customs.resolve();
                    //if (customWidgets) {
                    //    dojo.mixin(libraries, customWidgets);
                    //}
                    //console.log('libs', libraries);
                    //debugger;
                });
            }
            /*
            var customWidgetDescriptors = Library.getCustomWidgetDescriptors();
            if (customWidgets) {
                dojo.mixin(libraries, customWidgets);
            }
            */
            var widgetTypeList = [];
            for (var name in libraries) {
                if (libraries.hasOwnProperty(name)) {
                    var lib = libraries[name].$wm;
                    if (!lib) {
                        continue;
                    }
                    lib && lib.widgets && lib.widgets.forEach((item) => {
                        // skip untested widgets
                        if (item.category == "untested" || item.hidden) {
                            return;
                        }
                        widgetTypeList.push(item.type);
                    });
                }
            }
            // groups
            this._paletteItemGroupCount = 0;
            this._widgetPalette = Runtime.getSiteConfigData('widgetPalette');
            const work = new Deferred();
            subs.push(work);
            if (!this._widgetPalette) {
                console.error('widgetPalette.json not defined (in siteConfig folder)');
            } else {
                // There should be a preset for each of built-in composition types (desktop, mobile, sketchhifi, sketchlofi)
                // In future, we might allow users to create custom presets
                var presets = this._widgetPalette.presets || {};

                for (var p in presets) {
                    var widgetList = widgetTypeList.concat(); // clone the array
                    this._presetSections[p] = [];
                    var preset = presets[p];
                    var catchAllSection;
                    // For each preset, the 'sections' property can either be a string or an array of section objects.
                    // If a string, then that string is an reference to a sub-property of the top-level 'defs' object 
                    var sections = (typeof preset.sections == 'string') ?
                        (this._widgetPalette.defs ? this._widgetPalette.defs[preset.sections] : undefined) :
                        preset.sections;

                    if (sections && sections.length) {
                        // console.warning('No sections defined for preset ' + p + ' in widgetPalette.json (in siteConfig folder)');
                        var arr = [];
                        for (var cw in customWidgetDescriptors) {
                            var obj = customWidgetDescriptors[cw];
                            if (obj.descriptor) {
                                arr.push({
                                    name: cw,
                                    value: obj
                                });
                            }
                        }
                        arr.sort(function (a, b) {
                            var aa = a.name.split('/').pop().toLowerCase();
                            var bb = b.name.split('/').pop().toLowerCase();
                            return aa < bb ? -1 : (aa > bb ? 1 : 0);
                        });
                        var UserWidgetSectionAdded = false;
                        var userWidgetSection, customIncludes;
                        for (var j = 0; j < arr.length; j++) {
                            if (!UserWidgetSectionAdded) {
                                userWidgetSection = dojo.clone(this._userWidgetSection);
                                sections = sections.concat(userWidgetSection);
                                UserWidgetSectionAdded = true;
                                customIncludes = userWidgetSection.includes;
                            }
                            var custWidgets = arr[j].value.descriptor.widgets;
                            for (var cw = 0; cw < custWidgets.length; cw++) {
                                var custWidget = custWidgets[cw];
                                customIncludes.push('type:' + custWidget.type);
                            }
                        }
                        for (var s = 0; s < sections.length; s++) {
                            // For each sections, the value can either be a string or a section objects.
                            // If a string, then that string is an reference to a sub-property of the top-level 'defs' object 
                            var sectionObj = (typeof sections[s] == 'string') ?
                                (this._widgetPalette.defs ? this._widgetPalette.defs[sections[s]] : undefined) :
                                sections[s];
                            var section = dojo.clone(sectionObj);
                            // Add preset name to object so downstream logic can add an appropriate CSS class
                            // to the paletteFolder and paletteItem DOM nodes
                            section.preset = preset;
                            section.presetId = p;
                            if (section.subsections && section.subsections.length) {
                                var subsections = section.subsections;
                                for (var sub = 0; sub < subsections.length; sub++) {
                                    if (typeof subsections[sub] == 'string') {
                                        var subsectionObj = this._widgetPalette.defs ? this._widgetPalette.defs[subsections[sub]] : undefined;
                                        if (subsectionObj) {
                                            subsections[sub] = dojo.clone(subsectionObj);
                                        }
                                    }
                                    var subsection = subsections[sub];
                                    if (subsection.includes && subsection.includes.indexOf("$$AllOthers$$") >= 0) {
                                        catchAllSection = subsection;
                                    }
                                    subsection.preset = preset;
                                    subsection.presetId = p;
                                    // Stuffs in value for section.items
                                    this._createSectionItems(subsection, preset, widgetList);
                                }
                            } else {
                                // Stuffs in value for section.items
                                this._createSectionItems(section, preset, widgetList);
                                if (section.includes && section.includes.indexOf("$$AllOthers$$") >= 0) {
                                    catchAllSection = section;
                                }
                            }
                            this._presetSections[p].push(section);
                        }
                    }
                    for (var wi = 0; wi < widgetList.length; wi++) {
                        var widgetType = widgetList[wi];
                        if (catchAllSection) {
                            var item = Metadata.getWidgetDescriptorForType(widgetType);
                            var newItem = dojo.clone(item);
                            this._prepareSectionItem(newItem, catchAllSection, this._paletteItemGroupCount);
                            catchAllSection.items.push(newItem);
                            this._paletteItemGroupCount++;
                        } else {
                            console.log('For preset ' + p + ' Not in widget palette: ' + widgetList[wi]);
                        }
                    }
                }
                work.resolve();
            }
            return all(subs);
        }
        createRootItem(section) {
            const store = this.store;
            section.group = section.subsections.length || section.items.length;
            section.title = section.name;
            section.parentId = section.parentId || '.';
            // console.log('create root item : ' + section.name, section);
            section = store.putSync(section);
            if (section.subsections && section.subsections.length) {

                for (var i = 0; i < section.subsections.length; i++) {
                    var subsection = section.subsections[i];
                    subsection._id = subsection.id;
                    subsection.id = section.id + '_' + subsection.name;
                    subsection.parentId = section.id;
                    subsection.section = section;
                    subsection.subsections = subsection.subsections || [];
                    subsection.items = subsection.items || [];
                    subsection.items.forEach((item) => {
                        this.createItem(item, subsection);
                    });
                    this.createRootItem(subsection);
                }
            }
            return section;
        }
        createItem(item, parent, id) {
            item.id = id || (parent.id + '_' + item.type);
            item.parentId = parent.id;
            var name = item.name;
            if (item.$library) {
                name = item.$library._maqGetString(item.type) ||
                    item.$library._maqGetString(item.name) || item.name;
            }

            name = name.replace('<', '');
            name = name.replace('>', '');
            item.title = name;
            item.name = name;
            this.store.putSync(item);
        }
        createStylesSection() {
            const section = {
                "id": "Styles",
                "name": "States",
                "includes": [],
                "subsections": [],
                "items": [],
                "group": true
            }
            var styles = this.ctx.getLibraryManager().getSetting('styles');
            var store = this.ctx.getLibraryManager().getStore();
            //console.log('add states',store.data);

            store.data.forEach((style) => {
                section.items.push({
                    id: 'Styles_' + style.id,
                    name: style.id,
                    type: style.value.type,
                    group: false,
                    properties: utils.mixin({}, style.value.properties, {
                        name: style.id
                    })
                })
            });

            const root = this.createRootItem(section);
            root.items.forEach((style) => {
                this.createItem(style, root, style.id);
            });


            return root;


        }
        createExtras() {
            const root = this.createStylesSection();
            console.log('create palette extras! ');
            const store = this.ctx.getLibraryManager().getStore();
            store.on('update', (evt) => {
                const _style = evt.target;
                const style = {
                    id: 'Styles_' + _style.id,
                    name: _style.id,
                    type: _style.value.type,
                    group: false,
                    properties: utils.mixin({}, _style.value.properties, {
                        name: _style.id
                    })
                }
                this.createItem(style, root, _style.id);
            });
        }
        createStore() {
            const comptype = 'delite';
            const storeClass = declare('paletteStore', [TreeMemory, Trackable], {});
            const data = [];
            const store = new storeClass({
                data: data,
                idProperty: 'id',
                parentProperty: 'parentId',
                id: utils.createUUID(),
                ctx: sctx,
                Model: Model,
                mayHaveChildren: function () {
                    return true;
                },
                /**
                 * Return the root item, is actually private
                 * @TODO: root item unclear
                 * @returns {{path: string, name: string, mount: *, directory: boolean, virtual: boolean, _S: (xfile|data|FileStore), getPath: Function}}
                 */
                getRootItem: function () {
                    return {
                        _EX: true,
                        id: '.',
                        name: '.',
                        group: true,
                        virtual: true,
                        _S: this,
                        getPath: function () {
                            return this.id;
                        }
                    };
                },
                getParent: function (mixed) {
                    if (!mixed) {
                        return null;
                    }
                    var item = mixed,
                        result = null;

                    if (_.isString(item)) {
                        item = this.getSync(mixed);
                    }

                    if (item && item.parentId) {
                        result = this.getSync(item.parentId);
                    }
                    return result || this.getRootItem();
                }
            });

            this.store = store;
            this._presetCreated[comptype] = true;
            var presetClassName = this._presetClassNamePrefix + comptype; // Only used for debugging purposes
            var editorPrefs = {
                widgetPaletteLayout: 'icons',
                snap: true
            };
            //FIXME: current preset might be different than comptype once various new UI options become available
            var orderedDescriptions = [];
            var sections = this._presetSections[comptype];
            if (sections) {
                for (var s = 0; s < sections.length; s++) {
                    var section = sections[s];
                    if (!section._created) {
                        var orderedDescriptors = [section];
                        this.createRootItem(section);
                        section._created = true;
                    }
                }
            }
            this.createExtras()
            return store;
        }
    }
    Module.create = function () {
        const p = new Palette();
        p.initMeta();
        setTimeout(() => {
            p.init();
            const store = p.createStore();
        }, 2000);
    }
    if (window.sctx && test) {
        const p = new Palette(window.sctx);
        p.initMeta(window.sctx);
        setTimeout(() => {
            p.init();
            const store = p.createStore();
            if (window['targetContainer']) {
                window['targetContainer'].destroy();
            }
            const target = createContainer(ctx, 'Palette2');
            window['targetContainer'] = target;
            const grid2 = utils.addWidget(GridCSourcelass, {
                ctx: ctx,
                attaddCustachDirect: true,
                collection: store,
                open: true,
                expandOnClick: true,
                sources: [],
                palette: p
            }, null, target, true);
            target.add(grid2);
            $(grid2.bodyNode).addClass('dojoyPalette');
            $(grid2.bodyNode).addClass('paletteLayoutIcons');
            // const windowManager = ctx.getWindowManager();
            // windowManager.registerView(grid2, true);
        }, 2000);
    }
    Module.Palette = Palette;
    Module.Grid = GridCSourcelass;
    return Module;
});