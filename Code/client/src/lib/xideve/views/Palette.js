define([
    "dojo/_base/declare"
], function (declare) {
    return declare("xideve/views/Palette", [], {
        _init: function () {
            delete this._loaded;
            this._presetSections = {};
            this._createFolderTemplate();
        }
    });
});