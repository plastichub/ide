/** @module xideve/views/VisualEditorLayout **/
define([
    "dcl/dcl",
    'xaction/Action',
    'xide/types',
    "davinci/Workbench",
    "dojo/dom-style"
], function (dcl,Action,types, Workbench,domStyle) {
    /**
     *
     * @mixin module:xideve/views/VisualEditorLayout
     * @lends module:xideve/views/VisualEditor
     */
    return dcl(null, {
        declaredClass:"xideve/views/VisualEditorLayout",
        changeGlobalLayout:function(type){
            var pageEditor = this.getPageEditor();
            var mainView = this.getMainView();
            var left = mainView.layoutLeft;
            var right = mainView.layoutRight;
            var bottom = mainView.layoutBottom;

            var thiz = this;

            /**
             *      case types.VIEW_SPLIT_MODE.DESIGN:
             {
                 newMode = 'design';
                 break;
             }
             case types.VIEW_SPLIT_MODE.SOURCE:
             {
                 newMode = 'source';
                 break;
             }
             case types.VIEW_SPLIT_MODE.SPLIT_VERTICAL:
             {
                 newMode = 'splitVertical';
                 break;
             }
             case types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL:
             {
                 newMode = 'splitHorizontal';
                 break;
             }

             */

            switch(type){

                case types.VE_LAYOUT_RIGHT_CENTER_BOTTOM:{

                    //close left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'closed');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);


                    break;
                }
                case types.VE_LAYOUT_CENTER_BOTTOM:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);

                    //close left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'closed');
                    }

                    //close right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'closed');
                    }
                    break;
                }
                case types.VE_LAYOUT_CENTER_RIGHT:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.DESIGN);

                    //close left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'closed');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }

                    break;
                }
                case types.VE_LAYOUT_LEFT_CENTER_RIGHT:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.DESIGN);

                    //open left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'full');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }

                    break;
                }
                case types.VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM:{

                    //switch to horizontal
                    this.doSplit(types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL);
                    domStyle.set(left.domNode,{
                        width:'200px'
                    });
                    //open left
                    if (left._splitterWidget) {
                        left._splitterWidget.set('state', 'full');
                    }

                    //open right
                    if (right._splitterWidget) {
                        right._splitterWidget.set('state', 'full');
                    }


                    break;
                }
            }


            this.publish(types.EVENTS.RESIZE);

            /*
             pageEditor._bc.resize();
             var tab = pageEditor.bottomTabContainer;
             tab.selectChild(tab.getChildren()[tab.getChildren().length-1]);
             tab.selectChild(tab.getChildren()[0]);
             */

            setTimeout(function(){
                thiz.publish(types.EVENTS.RESIZE);
            },1000)


        },
        /**
         *
         */
        getLayoutActions:function(){

            if(this._layoutActions){
                return this._layoutActions;
            }
            var thiz = this;
            var idx = 0;
            function _createLayoutAction(label,command,icon,handler) {
                var _default = Action.create(label, icon, 'View/' +command + '_' + idx, false, null, types.ITEM_TYPE.WIDGET, 'viewActions', null, false,function(){
                    thiz.changeGlobalLayout(command);
                });

                _default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                    label: '',
                    widgetArgs:{
                        style:'text-align:right;float:right;font-size:120%;'
                    },
                    permanent:function(){
                        return thiz.destroyed==false;
                    }
                });
                idx++;
                return _default;
            }


            var VE_LAYOUT_RIGHT_CENTER_BOTTOM = _createLayoutAction('Center Bottom Right',
                types.VE_LAYOUT_RIGHT_CENTER_BOTTOM,'layoutIcon-rightCenterBottom');
            var VE_LAYOUT_CENTER_BOTTOM = _createLayoutAction('Center Bottom',
                types.VE_LAYOUT_CENTER_BOTTOM,'layoutIcon-centerBottom');
            var VE_LAYOUT_CENTER_RIGHT = _createLayoutAction('Center Right',
                types.VE_LAYOUT_CENTER_RIGHT,'layoutIcon-centerRight');

            var VE_LAYOUT_LEFT_CENTER_RIGHT = _createLayoutAction('Left Center Right',
                types.VE_LAYOUT_LEFT_CENTER_RIGHT,'layoutIcon-leftCenterRight');

            var VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM = _createLayoutAction('Left Center Right Bottom',
                types.VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM,'layoutIcon-leftCenterRightBottom');

            var out = [
                VE_LAYOUT_RIGHT_CENTER_BOTTOM,// trouble!
                VE_LAYOUT_CENTER_BOTTOM,//ok
                VE_LAYOUT_CENTER_RIGHT,//ok

                VE_LAYOUT_LEFT_CENTER_RIGHT,
                VE_LAYOUT_LEFT_CENTER_RIGHT_BOTTOM];


            this._layoutActions = out;

            return out;
        },
        doSplit: function (mode) {

            console.log('-do split ' + mode);

            return;

            if (this.editor) {

                var newMode = 'design';
                switch (mode) {

                    case types.VIEW_SPLIT_MODE.DESIGN:
                    {
                        newMode = 'design';
                        break;
                    }
                    case types.VIEW_SPLIT_MODE.SOURCE:
                    {
                        newMode = 'source';
                        break;
                    }
                    case types.VIEW_SPLIT_MODE.SPLIT_VERTICAL:
                    {
                        newMode = 'splitVertical';
                        break;
                    }
                    case types.VIEW_SPLIT_MODE.SPLIT_HORIZONTAL:
                    {
                        newMode = 'splitHorizontal';
                        break;
                    }
                }


                //this.editor.switchDisplayMode(newMode);
                var thiz = this;
                var pe = thiz.getPageEditor();
                pe.switchDisplayMode(newMode);

                var tab = pe.bottomTabContainer;
                /*if(!tab._didSelectFirstPane){
                 pe._bc.resize();
                 return;
                 }else {*/
                tab._didSelectFirstPane=true;
                setTimeout(function () {

                    if (tab) {
                        tab.resize();
                        var fCP = tab.getChildren()[0];
                        if(fCP) {
                            tab.selectChild(fCP);
                            fCP.resize();
                        }

                        thiz.publish(types.EVENTS.RESIZE);
                        tab.resize();
                        pe._bc.resize();
                    }
                    thiz.publish(types.EVENTS.RESIZE);
                    pe._bc.resize();
                }, 1500);
                pe._bc.resize();
            }
        },
        onSplit: function () {

            if (this.editor) {
                Workbench._switchEditor(null);
                this.publish("/davinci/ui/editorSelected", [{
                    editor: null,
                    oldEditor: this.editor
                }]);
            }
        },
        getSplitTarget: function () {
            return this.containerNode;
        },
        getSplitContent: function () {
            return this.editor.domNode;
        }
    });
});