import {addEventProcessor, removeEventProcessor} from "../platform/events";
import {throttleOncePerAnimationFrame} from "../utils/rateLimiters";
import {getViewSize} from "../utils/view";
import {Event} from "./entities/Event";

async function process() {
    const viewSize = getViewSize();
    return new Event("resize", {
        width: viewSize.width,
        height: viewSize.height
    });
}

export function observeResize(callback) {
    process().then(callback);
    addEventProcessor(window, "resize", process, {
        callback,
        throttle: throttleOncePerAnimationFrame
    });
}

export function stopObservingResize() {
    removeEventProcessor(window, "resize", process);
}
