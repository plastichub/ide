import {addEventProcessor, removeEventProcessor} from "../platform/events";
import {throttleOncePerAnimationFrame} from "../utils/rateLimiters";
import {getMapping} from "../processor/mapping";
import {Event} from "./entities/Event";
import {shouldIgnoreInteraction} from "../masking/interaction";

async function process(nativeEvent) {
    if (shouldIgnoreInteraction(nativeEvent.target)) {
        return null;
    }
    return new Event("move", {
        targetId: getMapping(nativeEvent.target),
        x: nativeEvent.clientX,
        y: nativeEvent.clientY
    });
}

export function observeMove(callback) {
    addEventProcessor(document, "mousemove", process, {
        callback,
        throttle: throttleOncePerAnimationFrame
    });
}

export function stopObservingMove() {
    removeEventProcessor(document, "mousemove", process);
}
