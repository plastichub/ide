import { now } from "../../platform/core";
import { config } from "../../config";

interface EventParams {
    targetId?;
    url?;
    isFrame?;
    tabId?;
    x?;
    y?;
    width?;
    height?;
    top?;
    left?;
    name?;
    value?;
    duration?;
    caret?;
    removals?;
    sel?;
}

/** @constructor */
export class Event {

    static decode(data) {
        try {
            return new Event(data["type"], {
                url: data["url"],
                isFrame: data["isIframe"],
                tabId: data["tabId"],
                targetId: data["targetId"],
                x: data["x"],
                y: data["y"],
                width: data["width"],
                height: data["height"],
                top: data["top"],
                left: data["left"],
                name: data["name"],
                value: data["value"],
                duration: data["duration"],
                sel: data["sel"]
            }, config.viewId, data["time"]);
        } catch (e) { }
    }

    targetId;
    url;
    isFrame;
    tabId;
    x;
    y;
    width;
    height;
    top;
    left;
    name;
    value;
    duration;
    sel;
    formalizedTime;

    constructor(public type,
        data: EventParams = {},
        public viewId: any = config.viewId,
        public time = now(), extra?: any) {
        this.targetId = data.targetId;

        // view
        this.url = data.url;
        this.isFrame = data.isFrame;
        this.tabId = data.tabId;

        // click, move
        this.x = data.x;
        this.y = data.y;

        // resolution
        this.width = data.width;
        this.height = data.height;

        // scroll
        this.top = data.top;
        this.left = data.left;

        // input events
        this.name = data.name;
        this.value = data.value;
        this.duration = data.duration;
        this.sel = data.sel;
    }

    formalize(start = 0) {
        return {
            "sel": this.sel,
            "viewId": this.viewId,
            "time": this.time - start,
            "type": this.type,
            "url": this.url,
            "isIframe": this.isFrame,
            "tabId": this.tabId,
            "targetId": this.targetId,
            "x": this.x,
            "y": this.y,
            "width": this.width,
            "height": this.height,
            "top": this.top,
            "left": this.left,
            "name": this.name,
            "value": this.value,
            "duration": this.duration
        };
    }

    // #DEBUG:BEGIN
    toJSON() {
        return "";
    }

    toString() {
        return "";
    }
    // #DEBUG:END
}
