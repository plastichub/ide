import { now, shouldUseCompactEventRepresentation } from "../../platform/core";
import { serializeEvents } from "../serializer";

// todo: review tslint errors
// tslint:disable:no-underscore
/** @constructor */
export class EventList {
    start;
    end;
    _set;
    size;

    constructor() {
        this.start = now();
        this.end = now();
        this._set = [];
        this.size = this._set.length;
    }

    add(event) {
        this.end = now();
        this._set.push(event);
        this.size = this._set.length;
    }

    forEach(callback) {
        this._set.forEach(entry => callback(entry));
    }

    serialize() {
        this.end = now();
        this.forEach(event => event.formalizedTime = event.time - this.start);
        // if (shouldUseCompactEventRepresentation()) {
        // return serializeEvents(this._set);
        // } else {
        return `[${this._set.map(event => JSON.stringify(event.formalize(this.start))).join(",")}]`;
        // }
    }

    // #DEBUG:BEGIN
    toJSON() {
        return "";
    }

    toString() {
        return "";
    }
    // #DEBUG:END
}
