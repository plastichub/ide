import {addEventProcessor, removeEventProcessor} from "../platform/events";
import {throttleOncePerAnimationFrame} from "../utils/rateLimiters";
import {getMapping} from "../processor/mapping";
import {Event} from "./entities/Event";

async function process(nativeEvent) {
    const target = nativeEvent.target === document ? document.documentElement : nativeEvent.target;
    return new Event("scroll", {
        targetId: getMapping(target),
        top: target.scrollTop,
        left: target.scrollLeft
    });
}

export function observeScroll(callback) {
    addEventProcessor(document, "scroll", process, {
        callback,
        throttle: throttleOncePerAnimationFrame,
        useCapture: true
    });
}

export function stopObservingScroll() {
    removeEventProcessor(document, "scroll", process);
}
