import {warn} from "../../../shared/sessionrecorder/platform/logger";

const VIEW_DELIMITER = "|";
const TYPE_DELIMITER = "^";
const EVENT_DELIMITER = ">";
const PROPERTY_DELIMITER = " ";
const REPEAT_VALUE = "~";
const EVENT_TYPE_IDS = {
    "view": "v",
    "move": "m",
    "click": "c",
    "resize": "r",
    "scroll": "s",
    "input": "i"
};

/**
 * Serializes an array of Session Replay events object representation into a
 * more compact string format as specified by https://dev-wiki.dynatrace.org/x/qaXFC
 *
 * @param  {Array<Event>} events Array of events to serialize
 * @return {string}
 */
export function serializeEvents(events) {
    // Group events by 'viewId', then by 'type'
    const views = eventsByViewAndType(events);
    let result = "";
    let viewCount = 0;
    // Serialization begins
    views.forEach((view, viewId) => {
        result += viewId + TYPE_DELIMITER;
        let typeCount = 0;
        view.forEach((type, typeName) => {
            const typeId = EVENT_TYPE_IDS[typeName];
            type.sort(sortEventsByTime);
            let eventCount = 0;
            let timePreceding = 0;
            let precedingEvent = {};
            type.forEach(evnt => {
                // Imprint type identifier just once
                if (eventCount === 0) {
                    result += typeId + PROPERTY_DELIMITER;
                }
                // Time
                result += (evnt.time - timePreceding) + PROPERTY_DELIMITER;
                // Avoid repeating property values
                result += serializeValuesAvoidingRepetitions(evnt, precedingEvent);
                // tabId is last value, and optional if same as viewId
                if (evnt.tabId && evnt.tabId !== viewId) {
                    result += PROPERTY_DELIMITER + evnt.tabId;
                }
                // On collated events, time source is first event
                if (eventCount === 0) {
                    timePreceding = evnt.time;
                }
                if (++eventCount < type.length) {
                    result += EVENT_DELIMITER;
                    precedingEvent = evnt;
                }
            });
            if (++typeCount < view.size) {
                result += TYPE_DELIMITER;
            }
        });
        if (++viewCount < views.size) {
            result += VIEW_DELIMITER;
        }
    });

    return result;
}

/**
 * Groups events by viewId, then by event 'type'
 *
 * @param  {Array<Event>} events
 * @return {Map<number, Map<string, Array<Event>>>} viewId to types to events
 */
function eventsByViewAndType(events) {
    const views = new Map();
    events.forEach(evnt => {
        if (!EVENT_TYPE_IDS[evnt.type]) {
            return warn("Event Serializer: Unrecognized event type"); // #DEBUG
        }
        const view = retrieveMapping(views, Number(evnt.viewId), Map);
        const type = retrieveMapping(view, evnt.type, Array);
        type.push(evnt);
    });
    return views;
}

/**
 * Retrieves or creates a value of the given type inside a Map, given its key
 *
 * @param  {Map<any, any>} container
 * @param  {any} key
 * @param  {any} valueType
 */
function retrieveMapping(container, key, valueType) {
    if (!container.has(key)) {
        container.set(key, new valueType());
    }
    return container.get(key);
}

/**
 * Sorting function for events by 'time'
 *
 * @param  {Event} eventA
 * @param  {Event} eventB
 */
function sortEventsByTime(eventA, eventB) {
    return eventA.time > eventB.time;
}

/**
 * Extract and serialize values from a given event, taking into account type
 * specificities and required property order, and not repeating values from
 * its preceding event
 *
 * @param  {Event} evnt
 * @param  {Event} preceding
 * @return {string} serialized result
 */
function serializeValuesAvoidingRepetitions(evnt, preceding) {
    let result = "";
    switch (evnt.type) {
        case "view": {
            result += valueOrRepeatingCharacter(evnt.url, preceding.url);
            break;
        }
        case "click":
        case "move": {
            result += valueOrRepeatingCharacter(evnt.targetId, preceding.targetId);
            result += PROPERTY_DELIMITER;
            result += valueOrRepeatingCharacter(evnt.x, preceding.x);
            result += PROPERTY_DELIMITER;
            result += valueOrRepeatingCharacter(evnt.y, preceding.y);
            break;
        }
        case "resize": {
            result += valueOrRepeatingCharacter(evnt.width, preceding.width);
            result += PROPERTY_DELIMITER;
            result += valueOrRepeatingCharacter(evnt.height, preceding.height);
            break;
        }
        case "scroll": {
            result += valueOrRepeatingCharacter(evnt.targetId, preceding.targetId);
            result += PROPERTY_DELIMITER;
            result += valueOrRepeatingCharacter(evnt.top, preceding.top);
            result += PROPERTY_DELIMITER;
            result += valueOrRepeatingCharacter(evnt.left, preceding.left);
            break;
        }
        case "input": {
            result += valueOrRepeatingCharacter(evnt.targetId, preceding.targetId);
            result += PROPERTY_DELIMITER;
            result += valueOrRepeatingCharacter(evnt.duration, preceding.duration);
            result += PROPERTY_DELIMITER;
            result += encodeURIComponent(valueOrRepeatingCharacter(evnt.name, preceding.name));
            result += PROPERTY_DELIMITER;
            result += encodeURIComponent(valueOrRepeatingCharacter(evnt.value, preceding.value));
            break;
        }
        default: {
            break;
        }
    }
    return result;
}

/**
 * Use a value only if not repeating its preceding event's value.
 * Else return the repeating value wildcard.
 *
 * @param  {} value
 * @param  {} precedingValue
 */
function valueOrRepeatingCharacter(value, precedingValue) {
    return value !== precedingValue ? value : REPEAT_VALUE;
}
