import {addEventProcessor, removeEventProcessor} from "../platform/events";
import {getMapping} from "../processor/mapping";
import {Event} from "./entities/Event";
import {now, apiNamespace} from "../platform/core";
import {includes} from "../../shared/sessionrecorder/utils/array";
import {maskText} from "../../shared/sessionrecorder/utils/masking";
import {shouldMaskInput} from "../masking/input";
import {shouldIgnoreInteraction} from "../masking/interaction";

async function process(nativeEvent, start = now()) {
    if (shouldIgnoreInteraction(nativeEvent.target)) {
        // Checkbox, radio, and other field/input types will return here as per masking spec
        return null;
    }
    const isValueLessInput = includes(["radio", "checkbox"], nativeEvent.target.type.toLowerCase());
    let value;
    if (shouldMaskInput(nativeEvent.target)) {
        value = maskText(nativeEvent.target.value);
    } else {
        value = isValueLessInput ? nativeEvent.target.checked : nativeEvent.target.value;
    }
    const sel = apiNamespace.finder(nativeEvent.target);
    return new Event("input", {
        targetId: getMapping(nativeEvent.target),
        name: nativeEvent.target.name,
        value: value,
        duration: now() - start,
        sel: sel
    });
}

export function observeInput(callback) {
    addEventProcessor(document, "focus change", process, {
        callback,
        throttle: func => {
            let startTime;
            return event => {
                if (event.type === "change") {
                    func(event, startTime);
                }
                startTime = now();
            };
        },
        useCapture: true
    });
}

export function stopObservingInput() {
    removeEventProcessor(document, "focus change", process);
}
