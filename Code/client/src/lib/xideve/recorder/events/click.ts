import { addEventProcessor, removeEventProcessor } from "../platform/events";
import { getMapping } from "../processor/mapping";
import { Event } from "./entities/Event";
import { shouldIgnoreInteraction } from "../masking/interaction";
import { apiNamespace } from '../platform/core';

async function process(nativeEvent) {
    if (shouldIgnoreInteraction(nativeEvent.target)) {
        return null;
    }
    const sel = apiNamespace.finder(nativeEvent.target);
    return new Event("click", {
        targetId: getMapping(nativeEvent.target),
        x: nativeEvent.clientX,
        y: nativeEvent.clientY,
        sel: sel,
        name: nativeEvent.target.innerText,
        value: nativeEvent.target.id
    });
}

export function observeClick(callback) {
    addEventProcessor(document, "click", process, { callback });
}

export function stopObservingClick() {
    removeEventProcessor(document, "click", process);
}
