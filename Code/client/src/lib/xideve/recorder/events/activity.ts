import {addEventProcessor, removeEventProcessor} from "../platform/events";
import {debounce, throttle} from "../utils/rateLimiters";
import {
    INACTIVITY_THRESHOLD_MS,
    INTERACTION_HANDLER,
    INTERACTION_UPDATE_THRESHOLD_MS
} from "../../shared/sessionrecorder/constants";
import {addQueryHandler} from "../../shared/sessionrecorder/bridge";
import {wait} from "../../shared/sessionrecorder/utils/time";

const INTERACTION_EVENTS = [
    "focus", "blur", "resize", "scroll", "keydown", "keyup", "mousemove",
    "mousedown", "mouseup", "click", "touchstart", "touchend", "touchmove"
];
const FOCUS_CONFIRMATION_WAIT = 100;

let active = false;
interface OnFocusFunction {
    (event): Promise<{}>;
    pending?: any;
}
const onFocus: OnFocusFunction = function (event) {
    return new Promise(async resolve => {
        if (!document.hasFocus() && event.type === "mouseenter") {
            onFocus.pending = {resolve};
            await wait(FOCUS_CONFIRMATION_WAIT);
            if (!onFocus.pending) {
                return;
            }
        }
        if (active) {
            resolve(null);
            return;
        }
        active = true;
        resolve(active);
    });
};

function onBlur() {
    if (document.hasFocus()) {
        return null;
    }
    if (onFocus.pending) {
        onFocus.pending.resolve(null);
        onFocus.pending = null;
    }
    if (!active) {
        return null;
    }
    active = false;
    return active;
}

function onInteraction(event, {leading, trailing} = {leading: true, trailing: void 0}) {
    if (leading && !active) {
        active = true;
        return active;
    } else if (trailing && active) {
        active = false;
        return active;
    } else {
        return null;
    }
}

export function observeInteraction(listener) {
    addEventProcessor(document, INTERACTION_EVENTS.join(" "), e => e, {
        callback: listener,
        throttle: callback => throttle(callback, INTERACTION_UPDATE_THRESHOLD_MS)
    });
    addQueryHandler(INTERACTION_HANDLER, () => listener());
}

export function observeActivity(listener) {
    if (document.hasFocus()) {
        active = true;
        listener(active);
    }

    addEventProcessor(document, "mouseenter", onFocus, {callback: listener});
    addEventProcessor(document, "mouseleave", onBlur, {callback: listener});
    addEventProcessor(window, "focus", onFocus, {callback: listener});
    addEventProcessor(window, "blur", onBlur, {callback: listener});
    const interactionListener = addEventProcessor(
        document, INTERACTION_EVENTS.join(" "), onInteraction,
        {
            callback: listener,
            throttle: callback => debounce(callback, INACTIVITY_THRESHOLD_MS, {
                leading: true,
                trailing: true
            })
        }
    );
    addQueryHandler(INTERACTION_HANDLER, () => interactionListener(void 0));
}

export function stopObservingActivity() {
    removeEventProcessor(document, "mouseenter", onFocus);
    removeEventProcessor(document, "mouseleave", onBlur);
    removeEventProcessor(window, "focus", onFocus);
    removeEventProcessor(window, "blur", onBlur);
    removeEventProcessor(document, INTERACTION_EVENTS.join(" "), onInteraction);
}
