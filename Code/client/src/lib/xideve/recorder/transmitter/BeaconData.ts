/**
 * Class to force closure compiler to keep track of object names
 */
export class BeaconData {
    static decode(encoded) {
        return new BeaconData({
            type: encoded["type"],
            data: encoded["data"],
            start: encoded["start"],
            end: encoded["end"],
            state: encoded["state"],
            status: encoded["status"],
            retries: encoded["retries"],
            sequenceNumber: encoded["sequenceNumber"]
        });
    }

    type;
    data;
    start;
    end;
    state;
    status;
    retries;
    sequenceNumber: number;

    constructor ({type, data, start, end, state = void 0, status = void 0, retries = void 0, sequenceNumber}) {
        this.type = type;
        this.data = data;
        this.start = start;
        this.end = end;
        this.state = state;
        this.status = status;
        this.retries = retries;
        this.sequenceNumber = sequenceNumber;
    }
    formalize() {
        return {
            "type": this.type,
            "data": this.data.toString(),
            "start": this.start,
            "end": this.end,
            "state": this.state,
            "status": this.status,
            "retries": this.retries,
            "index": this.sequenceNumber
        };
    }
}
