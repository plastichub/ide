import {
    getCurrentVisitId,
    storeValueToLocalStorage,
    loadValueFromLocalStorage,
    removeLocalStorageValue,
    addBeaconProtocolStateChangedCallback,
    now
} from "../platform/core";
import { transmitBeacon } from "./index";
import { BeaconData } from "./BeaconData";
import {
    MUTATIONS_PAYLOAD_TYPE,
    STATE_SUCCESS,
    STATE_FAIL,
    STATUS_OK,
    STATUS_FAIL,
    RETRY_STORAGE_KEY,
    RETRY_STORAGE_VERSION
} from "../../shared/sessionrecorder/constants";

const expirationPeriod = 900000; // 15 minutes
const retryTimes = [5, 25, 50]; // in seconds
// like retryTimes but relative to previous retryTime
const retryIntervals = retryTimes.map((time, index) => {
    const relativeTime = index ? time - retryTimes[index - 1] : time;
    return relativeTime * 1000;
});

/** @type {Map<number, BeaconData>} */
const pendingBeacons = new Map();
/** @type {Map<number, BeaconData>} */
const failedBeacons = new Map();
const beaconIds = {};

let lastRetryId = 0;

export function initBeaconRetryHandling() {
    const abort = true;
    if (abort) {
        return;
    }
    addBeaconProtocolStateChangedCallback((beacon, state, status) => {
        if (!beaconIds.hasOwnProperty(beacon.id)) {
            return;
        }
        const sendBeaconFailed = !(typeof status === "string" && status.indexOf(STATUS_OK) === 0 && state === STATE_SUCCESS);
        const retryId = beaconIds[beacon.id];
        if (failedBeacons.has(retryId)) {
            if (sendBeaconFailed) {
                // tslint:disable-next-line:no-console
                console.log('failed : ', beacon, status);
                // tslint:disable-next-line:no-debugger
                retryBeacon(retryId);
            } else {
                removeFailedBeacon(retryId);
            }
        }
        if (pendingBeacons.has(retryId)) {
            if (sendBeaconFailed) {
                // tslint:disable-next-line:no-console
                console.log('pending : ', beacon, status);
                addFailedBeacon(retryId, state, status);
                retryBeacon(retryId);
            }
            pendingBeacons.delete(retryId);
            if (!sendBeaconFailed) {
                delete beaconIds[retryId];
            }
        }
    });
    listenPageLeave();
    getPreviousBeaconRetry();
}

export function handleRetry(beaconId, retryId, beaconData) {
    if (retryId === null) {
        retryId = lastRetryId++;
        pendingBeacons.set(retryId, beaconData);
    } else {
        const failedBeaconData = failedBeacons.get(retryId);
        failedBeaconData.retries++;
        if (failedBeaconData.retries > 2) {
            failedBeacons.delete(retryId);
        }
    }
    beaconIds[beaconId] = retryId;
}

// #DEBUG:BEGIN
/**
 * Only used in tests.
 * Returns pending and failed beacons
 */
export function getRetryBeacons() {
    return {
        pendingBeacons,
        failedBeacons
    };
}
// #DEBUG:END

function addFailedBeacon(retryId, state, status) {
    // tslint:disable-next-line:no-debugger
    // debugger;
    // tslint:disable-next-line:no-console
    // console.error('failed beacon');
    const beaconData = pendingBeacons.get(retryId);
    beaconData.state = state;
    beaconData.status = status;
    beaconData.retries = 0;
    failedBeacons.set(retryId, beaconData);
}

function removeFailedBeacon(retryId) {
    failedBeacons.delete(retryId);
    delete beaconIds[retryId];
}

function retryBeacon(retryId) {
    const failedBeaconData = failedBeacons.get(retryId);
    if (!failedBeaconData) {
        return;
    }
    const retryCount = failedBeaconData.retries;
    if (retryCount < retryIntervals.length) {
        setTimeout(() => {
            transmitBeacon(failedBeaconData.type, failedBeaconData.data, {
                start: failedBeaconData.start,
                end: failedBeaconData.end,
                sequenceNumber: failedBeaconData.sequenceNumber,
                mode: (failedBeaconData.status === STATUS_FAIL) ? "localStorage" : "retry"
            }, retryId);
        }, retryIntervals[retryCount]);
    }
}

function listenPageLeave() {
    document.addEventListener("visibilitychange", () => {
        if (document.visibilityState === "hidden") {
            onPageLeave();
        }
        if (document.visibilityState === "visible") {
            removeLocalStorageValue(RETRY_STORAGE_KEY);
        }
    });
    window.addEventListener("beforeunload", onPageLeave);
}

function onPageLeave() {
    const storedBeaconRetry = {
        "pendingBeacons": {},
        "failedBeacons": {}
    };
    let somethingToSave = false;
    if (pendingBeacons.size) {
        pendingBeacons.forEach((beaconData, key) => storedBeaconRetry["pendingBeacons"][parseInt(key, 10)] = beaconData.formalize());
        somethingToSave = true;
    }
    if (failedBeacons.size) {
        failedBeacons.forEach((beaconData, key) => storedBeaconRetry["failedBeacons"][parseInt(key, 10)] = beaconData.formalize());
        somethingToSave = true;
    }
    if (somethingToSave) {
        storedBeaconRetry["visitId"] = getCurrentVisitId();
        storedBeaconRetry["version"] = RETRY_STORAGE_VERSION;
        storeValueToLocalStorage(RETRY_STORAGE_KEY, JSON.stringify(storedBeaconRetry));
    }
}

function isBeaconExpired(beacon) {
    return now() - beacon.end > expirationPeriod;
}

function getPreviousBeaconRetry() {
    const storedJSON = loadValueFromLocalStorage(RETRY_STORAGE_KEY);
    removeLocalStorageValue(RETRY_STORAGE_KEY);
    if (!storedJSON) {
        return;
    }
    let parsed;
    try {
        parsed = JSON.parse(storedJSON);
    } catch (error) {
        return;
    }
    if (parsed["visitId"] !== getCurrentVisitId()) {
        return; // Beacons from a previous visit
    }
    if (parsed["version"] !== RETRY_STORAGE_VERSION) {
        return; // Beacons from a different version
    }
    /** @type {Map<number, BeaconData>} */
    const pendingBeaconsMap = new Map();
    Object.keys(parsed["pendingBeacons"]).forEach(
        key => pendingBeaconsMap.set(parseInt(key, 10), BeaconData.decode(parsed["pendingBeacons"][key]))
    );
    /** @type {Map<number, BeaconData>} */
    const failedBeaconsMap = new Map();
    Object.keys(parsed["failedBeacons"]).forEach(
        key => failedBeaconsMap.set(parseInt(key, 10), BeaconData.decode(parsed["failedBeacons"][key]))
    );
    const storedBeaconRetry = {
        /** @type {Map<number, BeaconData>} */
        pendingBeacons: pendingBeaconsMap,
        /** @type {Map<number, BeaconData>} */
        failedBeacons: failedBeaconsMap
    };
    storedBeaconRetry.pendingBeacons.forEach(pendingBeacon => {
        if (pendingBeacon && !isBeaconExpired(pendingBeacon)) {
            if (pendingBeacon.type === MUTATIONS_PAYLOAD_TYPE) {
                pendingBeacon.data = new Uint8Array(pendingBeacon.data.split(","));
            }
            const newRetryId = lastRetryId++;
            pendingBeacons.set(newRetryId, pendingBeacon);
            addFailedBeacon(newRetryId, STATE_FAIL, STATUS_FAIL);
            pendingBeacons.delete(newRetryId);
        }
    });
    storedBeaconRetry.failedBeacons.forEach(failedBeacon => {
        if (failedBeacon && !isBeaconExpired(failedBeacon)) {
            failedBeacon.state = STATE_FAIL;
            failedBeacon.status = STATUS_FAIL;
            failedBeacon.retries = 0;
            if (failedBeacon.type === MUTATIONS_PAYLOAD_TYPE) {
                failedBeacon.data = new Uint8Array(failedBeacon.data.split(","));
            }
            const newRetryId = lastRetryId++;
            failedBeacons.set(newRetryId, failedBeacon);
            retryBeacon(newRetryId);
        }
    });
}
