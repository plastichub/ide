import { now } from "../platform/core";
import { createBeacon, sendBeacon } from "../platform/beacon";
import {
    SR_BEACON_PAYLOAD_VERSION, EVENTS_PAYLOAD_TYPE, MUTATIONS_PAYLOAD_TYPE
} from "../../shared/sessionrecorder/constants";
// import { stringToUtf8ByteArray } from "../../shared/sessionrecorder/utils/encode";
import { handleRetry } from "./retry";
import { BeaconData } from "./BeaconData";
// TODO: resolve linter issues
// tslint:disable:no-console
function generateBeaconPayload({ type, data, start, end, sequenceNumber, mode }) {
    /*let payload_version;
    if (shouldUseCompactEventRepresentation()) {
        payload_version = SR_BEACON_PAYLOAD_VERSION_COMPACTEVENTS;
    } else {*/
    let payload_version = SR_BEACON_PAYLOAD_VERSION;
    // }
    // let header = `${payload_version},${now()},${start},${end}`;
    switch (type) {
        case MUTATIONS_PAYLOAD_TYPE: {
            /// header = `${header},${sequenceNumber},`;
            // const encodedHeader = stringToUtf8ByteArray(header);
            // const payload = new Uint8Array(encodedHeader.length + data.length);
            // payload.set(encodedHeader, 0);
            // payload.set(data, encodedHeader.length);
            const payload = new Uint8Array(data.length);
            payload.set(data, 0);
            if (!sequenceNumber) {
                console.log(`SR: binary beacon without sequenceNumber detected, mode: ${mode}`); // #DEBUG
            }
            // console.log('binary beacon ', (payload as any).join());
            // console.log('bin');
            return payload.buffer;
        }
        case EVENTS_PAYLOAD_TYPE: {
            // const ret = `${header},${data}`;
            return JSON.stringify({
                'v': SR_BEACON_PAYLOAD_VERSION,
                'n': now(),
                's': start,
                'e': end,
                'evts': JSON.parse(data)
            });
        }
        default: {
            return null;
        }
    }
}

interface TransmitBeaconParams {
    start;
    end;
    sequenceNumber?;
    mode?;
}

export function transmitBeacon(type, data, { start, end, sequenceNumber, mode = 'regular' }: TransmitBeaconParams, retryId = null) {
    let payload = generateBeaconPayload({ type, data, start, end, sequenceNumber, mode });
    if (!payload) {
        return;
    }

    const beacon = createBeacon(payload);
    const params = payload instanceof ArrayBuffer ? { 'ms': start, 'me': end, 'sn': sequenceNumber } : {};
    handleRetry(beacon.id, retryId, new BeaconData({ type, data, start, end, sequenceNumber }));
    sendBeacon(beacon, {
        // sendAsStream: payload instanceof ArrayBuffer,
        contentType: type,
        params
    });
}
