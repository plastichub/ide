import { sendQueryToWorker } from "../../shared/sessionrecorder/bridge";
import { now } from "../platform/core";
import { getNumericConfigValue, getBooleanConfigValue } from '../platform/config';
import { addEventListener } from '../core/events'
let isLeavingPage = false;

function listenPageLeave() {
    addEventListener(document, "visibilitychange", () => {
        if (document.visibilityState === "hidden") {
            isLeavingPage = true;
            collect();
        }
        if (document.visibilityState === "visible") {
            isLeavingPage = false;
        }
    });
    addEventListener(window, "beforeunload", () => {
        isLeavingPage = true;
        collect();
    });
}

export async function initializeCollector() {
    const config = {
        "sl": getNumericConfigValue('sl'),
        "td": getNumericConfigValue('td'),
        "co": false
    };
    await sendQueryToWorker("cconf", config);
    listenPageLeave();
    return null;
}

export async function collect() {
    const collectionResponse = await sendQueryToWorker("collect", now(), isLeavingPage);

    if (!collectionResponse) {
        return [];
    }

    return collectionResponse.map(chunk => ({
        data: chunk["data"] !== null ? new Uint8Array(chunk["data"]) : new Uint8Array(0),
        start: chunk["from"],
        end: chunk["to"]
    }));
}
