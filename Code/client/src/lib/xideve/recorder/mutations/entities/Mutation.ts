import {
    MUTATION_UPDATE_ATTRIBUTE
} from "../../../shared/sessionrecorder/constants";
import { warn } from "../../../shared/sessionrecorder/platform/logger";
import { getNodeReferenceIds, transformToSerializableParams } from "./serializableParams";
import { MutationBase } from "../../../shared/sessionrecorder/entities/MutationBase";

function isValidParameters(type, node, attr, params) {
    if (!type) {
        return false;
    }
    return (
        params ||
        (node && (type !== MUTATION_UPDATE_ATTRIBUTE || attr))
    );
}

export class Mutation extends MutationBase {
    node;
    constructor(type, { node, attr = void 0 } = { node: void 0, attr: void 0 }, params = null) {
        if (!isValidParameters(type, node, attr, params)) {
            warn("Invalid parameters for mutation construction", { type, node, attr });
            throw new Error("Invalid parameters");
        }

        const serializableParams = params || transformToSerializableParams(type, node, attr);
        if (!serializableParams) {
            warn("Invalid parameters for mutation construction", { type, node, attr });
            throw new Error("Invalid parameters");
        }

        super(type, serializableParams);
        this.node = node;
    }

    updateMappings() {
        if (!this.node) {
            return;
        }
        const serializableParams = getNodeReferenceIds(this.type, this.node);
        this.nodeId = serializableParams.nodeId;
        this.parentId = this.parentId || serializableParams.parentId;
        this.previousId = this.previousId || serializableParams.previousId;
        this.nextId = this.nextId || serializableParams.nextId;
    }
}
