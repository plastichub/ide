import {ensureMapping, getMapping} from "../../processor/mapping";
import {
    MUTATION_ADD_NODE, MUTATION_REMOVE_NODE, MUTATION_UPDATE_ATTRIBUTE, MUTATION_UPDATE_NODE, MUTATION_UPDATE_NODE_VALUE
} from "../../../shared/sessionrecorder/constants";
import {isAllowedNode} from "../dom/filter";
import {handleMaskingSettingsForMutation} from "../../masking";

interface ReferenceIds {
    nodeId;
    parentId?;
    previousId?;
    nextId?;
    masking?;
    nodeName?;
    nodeValue?;
    namespaceURI?;
    attributeName?;
    attributeValue?;
}

function getFirstValidNode(getNextPossibleNode, node) {
    let possibleNode = getNextPossibleNode(node);
    while (possibleNode) {
        if (isAllowedNode(possibleNode)) {
            return possibleNode;
        }
        possibleNode = getNextPossibleNode(possibleNode);
    }
    return null;
}

export function getNodeReferenceIds(type, node) {
    if (type === MUTATION_ADD_NODE) {
        ensureMapping(node);
    }
    const referenceIds: ReferenceIds = {
        nodeId: getMapping(node)
    };
    if (type === MUTATION_ADD_NODE || type === MUTATION_UPDATE_NODE) {
        referenceIds.parentId = node.parentNode ? getMapping(node.parentNode) : null;
        const previousNode = getFirstValidNode(n => n.previousSibling, node);
        const nextNode = getFirstValidNode(n => n.nextSibling, node);
        referenceIds.previousId = previousNode ? getMapping(previousNode) : null;
        referenceIds.nextId = nextNode ? getMapping(nextNode) : null;
    }
    return referenceIds;
}

export function transformToSerializableParams(type, node, attr) {
    const params = getNodeReferenceIds(type, node);
    params.masking = handleMaskingSettingsForMutation(type, node, attr ? attr.name : null);
    switch (type) {
        case MUTATION_ADD_NODE: {
            params.nodeName = node.nodeName.toLowerCase();
            params.nodeValue = node.nodeValue;
            params.namespaceURI = node.namespaceURI;
            return params;
        }
        case MUTATION_UPDATE_NODE:
        case MUTATION_REMOVE_NODE: {
            return params;
        }
        case MUTATION_UPDATE_NODE_VALUE: {
            params.nodeValue = node.nodeValue;
            return params;
        }
        case MUTATION_UPDATE_ATTRIBUTE: {
            params.attributeName = attr.name;
            params.attributeValue = attr.value;
            return params;
        }
        default: {
            return null;
        }
    }
}
