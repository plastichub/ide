import {includes} from "../../../shared/sessionrecorder/utils/array";
import {isAllWhiteSpace} from "../../utils/isAllWhitespace";
import {selfObj} from "../../../shared/environment";

const allowedNodeTypes = selfObj.Node ? [
    Node.DOCUMENT_NODE,
    Node.DOCUMENT_FRAGMENT_NODE,
    Node.DOCUMENT_TYPE_NODE,
    Node.ELEMENT_NODE,
    Node.TEXT_NODE
] : [];

const eventAttributes = [
    "onabort", "onauxclick", "onafterprint", "onbeforeprint", "onbeforeunload", "onblur", "oncancel",
    "oncanplay", "oncanplaythrough", "onchange", "onclick", "onclose", "oncontextmenu", "oncopy", "oncuechange",
    "oncut", "ondblclick", "ondrag", "ondragend", "ondragenter", "ondragexit", "ondragleave", "ondragover",
    "ondragstart", "ondrop", "ondurationchange", "onemptied", "onended", "onerror", "onfocus", "onhashchange",
    "oninput", "oninvalid", "onkeydown", "onkeypress", "onkeyup", "onlanguagechange", "onload", "onloadeddata",
    "onloadedmetadata", "onloadend", "onloadstart", "onmessage", "onmessageerror", "onmousedown", "onmouseenter",
    "onmouseleave", "onmousemove", "onmouseout", "onmouseover", "onmouseup", "onwheel", "onoffline", "ononline",
    "onpagehide", "onpageshow", "onpaste", "onpause", "onplay", "onplaying", "onpopstate", "onprogress",
    "onratechange", "onreset", "onresize", "onrejectionhandled", "onscroll", "onsecuritypolicyviolation",
    "onseeked", "onseeking", "onselect", "onstalled", "onstorage", "onsubmit", "onsuspend", "ontimeupdate",
    "ontoggle", "onunhandledrejection", "onunload", "onvolumechange", "onwaiting"
];

const nonValueInputTypes = ["radio", "checkbox"];

export function isAllowedNode(node) {
    if (!node) {
        return false;
    }
    if (node.name && node.name.toLowerCase() === "html") {
        return true;
    }
    return includes(allowedNodeTypes, node.nodeType) &&
        !isAllWhiteSpace(node) &&
        !(node instanceof HTMLScriptElement) &&
        !(node instanceof HTMLMetaElement) &&
        !(node instanceof HTMLIFrameElement && node.parentNode instanceof HTMLHeadElement);
}

export function isFileInputNode(node) {
    return node instanceof HTMLInputElement && node.type.toLowerCase() === "file";
}

export function isPasswordInputNode(node) {
    return node instanceof HTMLInputElement && node.type.toLowerCase() === "password";
}

export function isInputNodeWithValue(node) {
    return (
        (node instanceof HTMLTextAreaElement) ||
        (node instanceof HTMLInputElement && !includes(nonValueInputTypes, node.type.toLowerCase()))
    );
}

export function isInputNodeWithoutValue(node) {
    return node instanceof HTMLInputElement && includes(nonValueInputTypes, node.type.toLowerCase());
}

export function isSelectNode(node) {
    return node instanceof HTMLSelectElement;
}

export function isOptionNode(node) {
    return node instanceof HTMLOptionElement;
}

export function isHiddenInputNode(node) {
    return node instanceof HTMLInputElement && node.type.toLowerCase() === "hidden";
}

export function isAllowedAttribute(node, attributeName) {
    return (
        !includes(eventAttributes, attributeName) &&
        !(isFileInputNode(node) && attributeName === "value") &&
        !(isInputNodeWithValue(node) && attributeName === "value") &&
        !(isSelectNode(node) && attributeName === "value") &&
        !(isInputNodeWithoutValue(node) && attributeName === "checked") &&
        !(isOptionNode(node) && attributeName === "selected") &&
        !(isHiddenInputNode(node) && attributeName === "value")
    );
}
