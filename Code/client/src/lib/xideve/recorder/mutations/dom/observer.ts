import {snapshotDom} from "./snapshotter";
import {isAllowedNode, isAllowedAttribute} from "./filter";
import {Mutation} from "../entities/Mutation";
import {MutationList} from "../entities/MutationList";
import {
    MUTATION_REMOVE_NODE, MUTATION_UPDATE_ATTRIBUTE, MUTATION_UPDATE_NODE_VALUE
} from "../../../shared/sessionrecorder/constants";
import {warn} from "../../../shared/sessionrecorder/platform/logger";
import {now} from "../../platform/core";
import {hasMapping} from "../../processor/mapping";
import {forEach} from "../../../shared/sessionrecorder/utils/array";
import {observeUserInput, stopObservingUserInput} from "./userInput";
import {windowObj} from "../../../shared/environment";

let observerCallback;

const observer = new windowObj.MutationObserver(function (mutations) {
    const mutationList = createEntriesFromMutations(mutations);
    if (mutationList.size > 0) {
        observerCallback(mutationList);
    }
});

function createEntriesFromMutations(mutations) {
    const mutationList = new MutationList(now());
    const addedNodes = [];
    const removedNodes = [];

    mutations
        .filter(mutation => mutation.type === "childList")
        .forEach(mutation => {
            forEach(mutation.removedNodes, node => removedNodes.push(node));
            forEach(mutation.addedNodes, node => addedNodes.push(node));
        });

    removedNodes
        .filter(node => hasMapping(node))
        .forEach(node => mutationList.add(new Mutation(MUTATION_REMOVE_NODE, {node, attr: void 0})));

    addedNodes
        .filter(node => isAllowedNode(node) && hasMapping(node.parentNode))
        .forEach(node => snapshotDom(node, snapshotMutationList => {
            snapshotMutationList.forEach(mutation => mutationList.add(mutation));
        }));

    mutations
        .filter(mutation => mutation.type !== "childList" && hasMapping(mutation.target))
        .forEach(mutation => {
            switch (mutation.type) {
                case "characterData": {
                    mutationList.add(new Mutation(MUTATION_UPDATE_NODE_VALUE, {node: mutation.target, attr: void 0}));
                    break;
                }
                case "attributes": {
                    if (!isAllowedAttribute(mutation.target, mutation.attributeName)) {
                        return;
                    }
                    const attrValue =  mutation.target.getAttribute(mutation.attributeName);
                    mutationList.add(new Mutation(MUTATION_UPDATE_ATTRIBUTE, {
                        node: mutation.target,
                        attr: {
                            name: mutation.attributeName,
                            value: attrValue
                        }
                    }));
                    break;
                }
                default: {
                    warn("Unexpected mutation type", { type: mutation.type }); // #DEBUG
                    break;
                }
            }
        });

    return mutationList;
}

/**
 * Observes the dom node and its children for mutations
 * @param {Node} documentNode dom node to observe
 * @param {Function} callback function that's called with mutation entries as an argument
 */
export function observeDom(documentNode, callback) {
    if (typeof callback !== "function") {
        return;
    }
    observerCallback = callback;
    observer.observe(documentNode, {
        childList: true,
        attributes: true,
        characterData: true,
        subtree: true
    });
    observeUserInput(documentNode, callback);
}

export function processPendingDomChanges() {
    const mutations = observer.takeRecords();
    if (mutations) {
        const mutationList = createEntriesFromMutations(mutations);
        if (mutationList.size > 0) {
            observerCallback(mutationList);
        }
    }
}

/**
 * Stops MutationObserver instance from receiving mutation notifications
 */
export function stopObservingDom({processPending = true} = { processPending: void 0}) {
    if (processPending) {
        processPendingDomChanges();
    }
    observer.disconnect();
    stopObservingUserInput();
    observerCallback = null;
}
