import { isAllowedNode, isAllowedAttribute } from "./filter";
import { Mutation } from "../entities/Mutation";
import { MutationList } from "../entities/MutationList";
import { now } from "../../platform/core";
import { hasMapping } from "../../processor/mapping";
import { snapshotUserInput } from "./userInput";
import { MUTATION_UPDATE_NODE, MUTATION_ADD_NODE, MUTATION_UPDATE_ATTRIBUTE } from "../../../shared/sessionrecorder/constants";
import { forEach } from "../../../shared/sessionrecorder/utils/array";

function snapshotAttributeNodes(documentNode, mutationList) {
    if (!documentNode.attributes) {
        return;
    }
    forEach(documentNode.attributes, attr => {
        if (!isAllowedAttribute(documentNode, attr.name)) {
            return;
        }
        const mutation = new Mutation(MUTATION_UPDATE_ATTRIBUTE, {
            node: documentNode,
            attr
        });

        mutationList.add(mutation);
    });
    snapshotUserInput(documentNode, mutation => mutationList.add(mutation));
}

function snapshotNodes(documentNode, mutationList) {
    if (!isAllowedNode(documentNode) || !isAllowedNode(documentNode.parentNode)) {
        return;
    }
    const mutation = new Mutation(hasMapping(documentNode) ? MUTATION_UPDATE_NODE : MUTATION_ADD_NODE, {
        node: documentNode
    });

    mutationList.add(mutation);

    snapshotAttributeNodes(documentNode, mutationList);

    let childNode = documentNode.firstChild;

    while (childNode) {
        snapshotNodes(childNode, mutationList);
        childNode = childNode.nextSibling;
    }
}
/**
 * Creates a snapshot of the dom element it receives with all it's nested elements.
 * @param { Node } documentNode
 * @param { Function } callback that receives an array of entries
 */

export function snapshotDom(documentNode, callback) {
    if (!documentNode.parentNode) {
        return;
    }
    const mutationList = new MutationList(now());
    snapshotNodes(documentNode, mutationList);
    callback(mutationList);
}
