import {isInputNodeWithoutValue, isInputNodeWithValue, isOptionNode, isSelectNode} from "./filter";
import {Mutation} from "../entities/Mutation";
import {addEventProcessor, removeEventProcessor} from "../../platform/events";
import {MutationList} from "../entities/MutationList";
import {now} from "../../platform/core";
import {forEach} from "../../../shared/sessionrecorder/utils/array";
import { MUTATION_UPDATE_ATTRIBUTE } from '../../../common/constants';

function extractInputRelatedMutation(documentNode) {
    let attr;
    if (isInputNodeWithValue(documentNode)) {
        attr = {
            name: "value",
            value: documentNode.value
        };
    } else if (isInputNodeWithoutValue(documentNode)) {
        attr = {
            name: "checked",
            value: documentNode.checked ? "" : void 0
        };
    } else if (isOptionNode(documentNode)) {
        attr = {
            name: "selected",
            value: documentNode.selected ? "" : void 0
        };
    } else {
        return void 0;
    }

    return new Mutation(MUTATION_UPDATE_ATTRIBUTE, {
        node: documentNode,
        attr
    });
}

function processInputEvent(event) {
    const mutationList = new MutationList(now());
    if (isSelectNode(event.target)) {
        forEach(event.target.querySelectorAll("option"), node => {
            mutationList.add(extractInputRelatedMutation(node));
        });
    } else {
        mutationList.add(extractInputRelatedMutation(event.target));
    }
    return mutationList;
}

export function snapshotUserInput(documentNode, callback) {
    const mutation = extractInputRelatedMutation(documentNode);
    if (mutation) {
        callback(mutation);
    }
}

export function observeUserInput(documentNode, callback) {
    addEventProcessor(documentNode, "input change", processInputEvent, { callback });
}

export function stopObservingUserInput() {
    removeEventProcessor(document, "input", processInputEvent);
}
