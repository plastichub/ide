/** @type {WeakMap<Node, number>} */
let map = new WeakMap();

let idCounter = 0;

function addMapping(node, id) {
    map.set(node, id);
}

export function hasMapping(node) {
    return map.has(node);
}

export function getMapping(node) {
    return hasMapping(node) ? map.get(node) : null;
}

export function ensureMapping(node) {
    if (hasMapping(node)) {
        return getMapping(node);
    }
    ++idCounter;
    addMapping(node, `${idCounter}`);
    return `${idCounter}`;
}

export function resetMappings() {
    idCounter = 0;
    map = new WeakMap();
}
