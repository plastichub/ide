import { sendQueryToParent, sendQueryToWorker } from "../../shared/sessionrecorder/bridge";
import { EVENT_HANDLER, MUTATION_HANDLER } from "../../shared/sessionrecorder/constants";
import { isIFrameEnvironment } from "../../shared/sessionrecorder/utils/environment";

export function dispatchMutations(mutationList, html) {
    const query = [
        mutationList.serialize(),
        html
    ];
    isIFrameEnvironment() ? sendQueryToParent(MUTATION_HANDLER, ...query) : sendQueryToWorker(MUTATION_HANDLER, ...query);
}

export function dispatchEvent(event) {
    if (isIFrameEnvironment()) {
        // todo: review returned promise not handled
        sendQueryToParent(EVENT_HANDLER, event.formalize());
        return;
    }
    onEvent.listener(event);
}

interface EventFn {
    (listener: (evt) => void): void;
    listener?: (evt) => void;
}

export const onEvent: EventFn = function (listener) {
    onEvent.listener = listener;
};
