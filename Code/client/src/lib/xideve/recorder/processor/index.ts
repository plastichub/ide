import { dispatchEvent, dispatchMutations } from "./dispatcher";
import { getMapping } from "./mapping";
import { includes } from "../../shared/sessionrecorder/utils/array";

const contexts = new WeakMap();

function getContainerNodeId(context) {
    if (!context) {
        return void 0;
    }
    if (contexts.has(context)) {
        return contexts.get(context).nodeId;
    }

    const iFrames = document.querySelectorAll('iframe');
    for (let i = 0; i < iFrames.length; i++) {
        if (iFrames[i].contentWindow === context) {
            const nodeId = getMapping(iFrames[i]);
            contexts.set(context, { nodeId, frame: iFrames[i] });
            return nodeId;
        }
    }
    return null;
}

function getFrameOffset(context) {
    const rect = contexts.get(context).frame.getBoundingClientRect();
    return {
        top: rect.top,
        left: rect.left
    };
}

export function processDomMutations(mutationList, { context, html } = { context: void 0, html: void 0 }) {
    html = html || document.documentElement.outerHTML; // #DEBUG
    let contextId = getContainerNodeId(context);
    mutationList.clean(mutation => {
        if (mutation.node) {
            mutation.updateMappings();
        }
        if (typeof mutation.nodeId === "undefined") {
            return true;
        }
        if (!context) {
            return false;
        }
        if (!contextId) {
            return true;
        }
        mutation.nodeId = `${contextId}.${mutation.nodeId}`;
        if (mutation.parentId) {
            mutation.parentId = `${contextId}.${mutation.parentId}`;
        } else {
            mutation.parentId = contextId;
        }
        if (mutation.nextId) {
            mutation.nextId = `${contextId}.${mutation.nextId}`;
        }
        if (mutation.previousId) {
            mutation.previousId = `${contextId}.${mutation.previousId}`;
        }
        return false;
    });
    dispatchMutations(mutationList, html);
}

export function processDomEvent(event, { context } = { context: void 0 }) {
    let contextId = getContainerNodeId(context);
    if (context && !contextId) {
        return;
    }

    if (contextId) {
        event.targetId = `${contextId}.${event.targetId}`;

        if (includes(["click", "move"], event.type)) {
            const offset = getFrameOffset(context);
            event.x += offset.left;
            event.y += offset.top;
        }
    }
    dispatchEvent(event);
}
