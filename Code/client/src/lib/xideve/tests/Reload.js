define([
    'xdojo/declare',
    'dojo/Deferred',
    'xide/editor/Registry',
    'xideve/views/VisualEditor'
], function (declare,Deferred,Registry,VisualEditor) {

    var ctx = window.sctx;

    function testMain(){

        var component = VisualEditor.component;

        var editorStore = Registry.getStore();
        Registry.unregisterEditor('Visual Editor');
        var editors = Registry.getEditors({
            path:'./index.dhtml'
        });
        var _r= Registry;
        var dfd = ctx.reloadModules([
            "davinci/ve/PageEditor",
            'davinci/ve/views/SwitchingStyleView',
            'xideve/views/VisualEditorPalette',
            'xideve/views/VisualEditorSourceEditor',
            'xideve/views/VisualEditorLayout',
            'xideve/views/VisualEditorAction',
            'xideve/views/VisualEditorTools'
        ]);
        var head = new Deferred();

        dfd.then(function(){

            setTimeout(function(){

                ctx.reloadModules(['xideve/views/VisualEditor']).then(function(modules){

                    var components = ctx.getApplication().getComponents(),
                        _xideve = components['xideve'],
                        _ve = modules[0];

                    _ve.component = _xideve;
                    head.resolve({
                        ve:modules[0],
                        component:_xideve
                    });
                });

            },100);
        });
        return head;
    }
    var Module =declare('m',null,[]);
    Module.reload=testMain;

    return Module;
});