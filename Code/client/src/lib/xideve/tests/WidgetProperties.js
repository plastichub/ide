/** @module xgrid/Base **/
define([
    'xdojo/declare',
    'xide/types',
    'xide/utils',
    'module',
    './Basics',
    "xideve/tests/Reload"

], function (declare,types,utils,module,Basics,Reload) {


    console.clear();

    /***
     *
     * playground
     */
    var leftGrid = window[module.id + 'leftGrid'];
    var ctx = window.sctx,
        parent,
        _lastEditor = window[module.id + 'lastVE'];

    ///////////////////////////////////////////////////
    //
    //
    //
    //  Switches
    //
    //
    var addOutline = false;
    var showLESS = true;
    var showPalette = true;
    var showBehaviours = false;

    function showProperties(){


        //console.clear();

        var pe = this.getPageEditor();
        console.log('showProperties 2',pe);
        //pe.getPropertyPane();
        //this.addStyleView();

        var self = this;
        setTimeout(function(){
            self.select('btnMediaPlayer');
        },2000);

    }

    function createEditorOverrideClass(){

        var ACTION = types.ACTION;




        var _class = {

            runAction:function(action){

                action = _.isString(action) ? this.getAction(action) : action;

                //console.log('r un ' + action.command);

                if(action.command==='View/Show/Properties'){
                    showProperties.apply(this,[]);
                    return true;
                }


                return this.inherited(arguments);
            },
            addOutline:addOutline,
            showPalette:showPalette,
            showLESS:showLESS,
            showBehaviours:showBehaviours,
            permissions: [
                //ACTION.DOWNLOAD,
                //ACTION.RELOAD,
                //ACTION.DELETE,
                //ACTION.SAVE,
                //ACTION.CLIPBOARD,
                //ACTION.LAYOUT,
                ACTION.SELECTION
                //ACTION.PREVIEW
            ],
            onUseActionStore:function(){}


        };
        return _class;
    }


    function doEditorTests(editor){
        //var d = editor.getActionStore();
        //editor.selectAll();
        //editor.runAction('View/Split/7');
        //editor.select('btnDVD');
        //editor.select('btnTV',true);
    }
    function onEditorReady(editor){

        window[module.id + 'lastVE'] = editor;
        setTimeout(function(){
            ctx.mainView.getToolbar().setActionEmitter(null);
            ctx.mainView.getToolbar().setActionEmitter(editor);
            doEditorTests(editor);
            onReloaded.apply(editor,[editor]);
        },4000);
    }
    function onReloaded(editor){
        console.log('reloaded ve');
        //editor.select('btnDVD');
        showProperties.apply(editor,[]);
        /*
        editor.runAction('View/Show/Properties');
        */

    }

    function testMainUpdate(){

        /*
        if(_lastEditor){
            utils.destroy(_lastEditor);
            if(_lastEditor._parent && _lastEditor._parent.docker()) {
                _lastEditor._parent.docker().removePanel(_lastEditor._parent);
            }
        }
        */
        if(!_lastEditor) {
            Basics.createVisualEditor('workspace', './index.dhtml', createEditorOverrideClass(), onEditorReady);
        }else{
            Reload.reload().then(function(data){
                var component = data.component;
                component.registerEditors();
                onReloaded.apply(_lastEditor,[_lastEditor]);
            });
        }
    }
    function testMain(){


         if(_lastEditor){

             utils.destroy(_lastEditor);

             if(_lastEditor._parent && _lastEditor._parent.docker()) {

                 _lastEditor._parent.docker().removePanel(_lastEditor._parent);
             }
         }

        Reload.reload().then(function(data){
            var component = data.component;
            component.registerEditors();
            Basics.createVisualEditor('workspace', './index.dhtml', createEditorOverrideClass(), onEditorReady);
            //onReloaded.apply(_lastEditor,[_lastEditor]);

        });
    }



    if (ctx) {
        testMain();
    }


    var Module =declare('m',null,[]);


    return Module;
});