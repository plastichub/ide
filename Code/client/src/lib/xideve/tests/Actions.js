/** @module xgrid/Base **/
define([
    'xdojo/declare',
    'xide/types',
    'xdocker/Docker2',
    'xide/utils',
    'module',
    './Basics',
    'xide/action/DefaultActions',
    "davinci/Workbench"

], function (declare,types,Docker,utils,module,Basics,DefaultActions,Workbench) {

    console.clear();

    /***
     *
     * playground
     */
    var leftGrid = window[module.id + 'leftGrid'];
    var ctx = window.sctx,
        parent,
        _lastEditor = window[module.id + 'lastVE'];


    ///////////////////////////////////////////////////
    //

    //
    //
    //  Switches
    //
    //
    function createSubDocker(tab){
        var myDocker = Docker.createDefault(tab.containerNode, {
            extension:createDockerClass()
        });
        myDocker.uuid = 'sub docker';
        return myDocker;
    }



    function createEditorOverrideClass(){

        var ACTION = types.ACTION;



        var _class = {
            _detectFocus:null,
            _detectBlur:null,
            _onBlur:function(){

            },
            setFocused:function(focused){
                ///console.log('focused ' + focused);
            },
            _onFocusChanged:function(focused,type){


                //console.log('on focus changed'  + focused);


                if(this._focused && !focused){
                    this._onBlur();

                    //console.log('lost focus '  + this.id);
                    /*
                     if(this.id==='xide_widgets_TemplatedWidgetBase_0'){
                     console.log('   left blur');
                     }
                     */
                }

                if(!this._focused && focused){
                    //this._emit(types.EVENTS.ON_VIEW_SHOW,this);
                }

                this._focused = focused;

                this.highlight  && this.highlight(focused);
            },
            destroy:function(){
                this.inherited(arguments);
                window.removeEventListener('focus', this._detectFocus, true);
                window.removeEventListener('blur', this._detectBlur, true);
            },
            onHide:function(){
                console.log('on hide ');
                this.inherited(arguments);
                var ctx = this.getEditorContext();
                if(ctx){
                    ctx.hideFocusAll();
                }
            },
            runAction:function(action){

                console.log('run ve - action ' + action.command,action);

                action = _.isString(action) ? this.getAction(action) : action;

                if(!action){
                    return;
                }

                var self = this,
                    command = action.command,
                    parts = command.split('/'),
                    last = parts[parts.length-1],
                    ctx = this.getEditorContext();


                if(command.indexOf('View/Split/')!==-1){
                    this.setSplitMode(parseInt(last), null);
                    this.updateFrame();
                    return true;
                }

                if(command.indexOf('Widget/Align/')!==-1){
                    switch (command){
                        case 'Widget/Align/Align Left':
                            return self._alignLeftVertical();

                        case 'Widget/Align/Align Center':
                            return self._alignCenterVertical();
                        case 'Widget/Align/Align Right':
                            return self._alignRightVertical();
                        case 'Widget/Align/Align Top':
                            return self._alignTopHorizontal();
                        case 'Widget/Align/Same Size':
                            return self._sameSize();
                        case 'Widget/Align/Same Width':
                            return self._sameWidth();
                        case 'Widget/Align/Same Height':
                            return self._sameHeight();
                    }
                    return true;
                }



                if(action.action){
                    var result = Workbench._runAction(action.action, ctx);
                    return result;
                }


                return this.inherited(arguments);

            },
            permissions: [
                //ACTION.DOWNLOAD,
                //ACTION.RELOAD,
                //ACTION.DELETE,
                //ACTION.SAVE,
                //ACTION.CLIPBOARD,
                //ACTION.LAYOUT,
                ACTION.SELECTION
                //ACTION.PREVIEW

            ],
            isActive:function(testNode){
                return utils.isDescendant(this.domNode,testNode || document.activeElement);
            },
            startup:function(){

                this.inherited(arguments);
                var self = this;

                if (this.permissions) {
                    var _defaultActions = DefaultActions.getDefaultActions(this.permissions, this,this);
                    _defaultActions = _defaultActions.concat(this.getItemActions());
                    this.addActions(_defaultActions);
                }



                this._on('selectionChanged',function(e){
                    //console.log('selectionChanged: ',e);
                    self.refreshActions();
                });

                var thiz = this,
                    node = thiz.domNode.parentNode;

                this._focused  = this.isActive();

                function _detectBlur(element) {
                    //console.log('_detectBlur '+element.target);
                    var testNode = element.target;
                    /*
                    if(utils.isDescendant(node,testNode)) {
                        var active = thiz.isActive();
                        thiz._onFocusChanged(active, ' blur',testNode);
                    }
                    */
                    thiz._onFocusChanged(thiz.isActive(testNode), ' blur',testNode);
                }

                function _detectFocus(element) {
                    //console.log('_detectFocs');
                    //console.log('_detectFocus ',element.target);
                    var testNode = element.target;
                    thiz._onFocusChanged(thiz.isActive(testNode), ' focus',testNode);
                    /*
                    if(utils.isDescendant(node,testNode)){
                        thiz._onFocusChanged(thiz.isActive(testNode),'focus',testNode);
                    }
                    */

                }

                this._detectFocus = _detectFocus;
                this._detectBlur = _detectBlur;

                function attachEvents() {
                    window.addEventListener ? window.addEventListener('focus', _detectFocus, true) : window.attachEvent('onfocusout', _detectFocus);
                    window.addEventListener ? window.addEventListener('blur', _detectBlur, true) : window.attachEvent('onblur', _detectBlur);
                }
                attachEvents();
            },
            getSplitViewActions:function(){

                var result = [],
                    self = this,

                    root = 'View/Split',

                    VIEW_SPLIT_MODE = types.VIEW_SPLIT_MODE;/*,

                    head = this.createAction(
                        {
                            label:'Split',
                            command:root,
                            icon:'fa-eye',
                            tab:'Home',
                            group:'View',
                            mixin:{
                                addPermission:true
                            }

                        }
                    );
                */





                var modes = [
                    {
                        mode:VIEW_SPLIT_MODE.DESIGN,
                        label:'Design',
                        icon:'fa-eye'
                    },
                    {
                        mode:VIEW_SPLIT_MODE.SOURCE,
                        label:'Code',
                        icon:'fa-code'
                    },
                    {
                        mode:VIEW_SPLIT_MODE.SPLIT_HORIZONTAL,
                        label:'Horizontal',
                        icon:'layoutIcon-horizontalSplit'
                    },
                    {
                        mode:VIEW_SPLIT_MODE.SPLIT_VERTICAL,
                        label:'Vertical',
                        icon:'layoutIcon-layout293'
                    }
                ];

                //result.push(head);

                _.each(modes,function(mode){
                    result.push(self.createAction({
                        label:mode.label,
                        command:root + '/' + mode.mode,
                        icon:mode.icon,
                        tab:'Home',
                        group:'View',
                        keycombo:['ctrl '+mode.mode],
                        mixin:{
                            addPermission:true
                        }
                    }));
                });

                return result;

            },
            getWidgetLayoutActions:function(actions){

                var thiz = this;
                var result = [];
                function add(label,icon,command,handler){

                    result.push(thiz.createAction({
                        label:label,
                        icon:icon,
                        group:'Widget',
                        tab:'Home',
                        command:'Widget/Align/'+command,
                        mixin:{
                            addPermission:true
                        }

                    }));
                }


                result.push(this.createAction({
                    label:"Align",
                    command:"Widget/Align",
                    icon:'fa-arrows',
                    group:'Widget',
                    tab:'Home',
                    mixin:{
                        addPermission:true
                    },
                    shouldDisable:function(){

                        var ctx = thiz.getEditorContext();
                        if(!ctx){
                            return true;
                        }
                        var selection = thiz.getSelection();
                        return selection.length == 0 || selection.length < 2;
                    }
                }));

                add('Align Left', 'layoutIcon-alignLeftVertical', 'Align Left',thiz._alignLeftVertical);
                add('Align Center', 'layoutIcon-alignCenterVertical', 'Align Center', thiz._alignCenterVertical);
                add('Align Right', 'layoutIcon-alignRightVertical', 'Align Right',thiz._alignRightVertical);
                add('Align Top', 'layoutIcon-alignTopHorizontal', 'Align Top', thiz._alignTopHorizontal);
                add('Align Bottom', 'layoutIcon-alignBottomHorizontal', 'Align Bottom',thiz._alignBottomHorizontal);
                add('Same Size', 'layoutIcon-sameSize', 'Same Size',thiz._sameSize);
                add('Same Width', 'layoutIcon-sameWidth', 'Same Width',thiz._sameWidth);
                add('Same Height', 'layoutIcon-sameHeight', 'Same Height', thiz._sameHeight);
                return result;
            },
            _toAction: function (action) {

                //var _default = Action.create(action.label, this._getIconClass(action.iconClass), this._toCommand(action.label), false, null, types.ITEM_TYPE.WIDGET, 'editActions', null, false);


                var cmd = action.command || this._toCommand(action.label) || action.id;
                if(!cmd){
                    return null;
                }


                var self = this,
                    _default = this.createAction({
                    label:action.label,
                    group:action.group || 'Ungrouped',
                    icon: this._getIconClass(action.iconClass) || action.iconClass,
                    command:cmd,
                    tab: action.tab || 'Home',
                    mixin:{
                        action:action,
                        addPermission:true
                    },
                    shouldDisable:function(){

                        var ctx = self.getEditorContext();
                        if(ctx && action.action && action.action.isEnabled){
                            return !action.action.isEnabled(ctx);
                        }

                        return false;
                    }
                });





                switch (action.id) {

                    case "documentSettings":
                    {
                        return null;
                    }
                    case 'undo':
                    case 'redo':
                    {
                        _default.group = 'Edit';
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''});
                        return _default;
                    }
                    case 'cut':
                    case 'copy':
                    case 'paste':
                    {
                        _default.group = 'Edit';
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {show: false});
                        return _default;
                    }
                    case 'delete':
                    {
                        //_default.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {label: ''});
                        _default.group = 'Organize';
                        return _default;

                    }
                    case 'theme':
                    case 'rotateDevice':
                    case 'chooseDevice':
                    case 'stickynote':
                    case 'savecombo':
                    case 'showWidgetsPalette':
                    case 'layout':
                    case 'sourcecombo':
                    case 'design':
                    case 'closeactiveeditor':
                    case 'tableCommands':{
                        return null;
                    }




                }

                //console.log('unknown action : ',action);
                //_default.command=action.id;
                return _default;
            },
            getEditToggleActions: function () {
                var thiz = this;

                var result = [],
                    self = this,

                    root = 'View/Mode';

                result.push(this.createAction(
                    {
                        label:'Mode',
                         command:root,
                         icon:'fa-eye',
                         tab:'Home',
                         group:'View',
                         mixin:{
                            addPermission:true
                         }
                    }));

                result.push(this.createAction({
                    label:'Edit',
                    command:root  + '/Design',
                    icon:'fa-edit',
                    tab:'Home',
                    group:'View',
                    mixin:{
                        addPermission:true
                    }
                }));

                result.push(this.createAction({
                    label:'Preview',
                    command:root  + '/Preview',
                    icon:'fa-play',
                    tab:'Home',
                    group:'View',
                    mixin:{
                        addPermission:true
                    }
                }));





                /*

                var _toggle = Action.createDefault('Design', 'fa-toggle-on', 'View/Design Mode', 'viewActions', null, {
                    widgetClass: 'xide/widgets/ToggleButton',
                    widgetArgs: {
                        icon1: 'fa-toggle-on',
                        icon2: 'fa-toggle-off',
                        delegate: this,
                        checked: thiz._designMode
                    },
                    handler: function (command, item, owner, button) {
                        thiz._designMode = !thiz._designMode;
                        button.set('checked', thiz._designMode);
                        thiz.setDesignMode(thiz._designMode);

                    }
                }).setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null).
                    setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                        label: '',
                        permanent:function(){
                            return thiz.destroyed==false;
                        },
                        widgetArgs:{
                            style:'text-align:right;float:right;font-size:120%;'
                        }
                    }).
                    setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {show: false});

                this._designToggleAction = _toggle;
                */

                return result;
            },
            getFlowActions: function () {

                var result = [],
                    self = this,

                    root = 'View/Layout Mode';

                result.push(this.createAction(
                    {
                        label:'Flow',
                        command:root,
                        icon:'fa-crosshairs',
                        tab:'Home',
                        group:'View',
                        mixin:{
                            addPermission:true
                        }
                    }));

                result.push(this.createAction({
                    label:'Relative',
                    command:root  + '/Relative',
                    tab:'Home',
                    group:'View',
                    mixin:{
                        addPermission:true
                    }
                }));

                result.push(this.createAction({
                    label:'Absolute',
                    command:root  + '/Absolute',
                    tab:'Home',
                    group:'View',
                    mixin:{
                        addPermission:true
                    }
                }));
                return result;
            },
            getItemActions: function () {

                //console.log('ve-------------    get item actions2');

                //console.clear();



                var result = [],
                    self = this;

                result = result.concat(this.getSplitViewActions());

                result.push(this.createAction({
                    label:"Save",
                    command:"File/Save",
                    icon:'fa-save',
                    group:'File',
                    tab:'Home',
                    keycombo:['ctrl s'],
                    mixin:{
                        addPermission:true
                    }
                }));

                result.push(this.createAction({
                    label:"Select",
                    command:"Widget/Select",
                    group:'Widget',
                    tab:'Home',
                    icon:'fa-hand-o-up',
                    mixin:{
                        addPermission:true
                    }
                }));

                result.push(this.createAction({
                    label:"Move",
                    command:"Widget/Move",
                    group:'Widget',
                    tab:'Home',
                    icon:'fa-crosshairs',
                    mixin:{
                        addPermission:true
                    }
                }));
                result = result.concat(this.getWidgetLayoutActions());
                result = result.concat(this.getEditToggleActions());
                result = result.concat(this.getFlowActions());

                ///////////////////////////////////////////////////////////
                //
                //  Normal view actions
                //
                //
                var _viewActions = this._getViewActions();
                if (_viewActions && _viewActions[0]) {
                    _viewActions = _viewActions[0].actions;
                }

                var context = this.getEditorContext();
                if (this.editor && this.editor.currentEditor && this.editor.currentEditor.context) {
                    context = this.editor.currentEditor.context;
                }

                if(!context){
                    //return result;
                }
                function completeAndAddAction(action){
                    var _a = self._toAction(action);

                    if (!_a || !_a.command) {
                        return;
                    }

                    _a.tab = 'Home';
                    _a.action = action;
                    //_wireAction(_a);
                    //actions.push(_a);
                    result.push(_a);
                }
                for (var i = 0; i < _viewActions.length; i++) {

                    var action = _viewActions[i];



                    //if (_.isString(action.action)) {
                        Workbench._loadActionClass(action);
                    //}

                    var parms = {showLabel:false/*, id:(id + "_toolbar")*/};
                    ['label','showLabel','iconClass'].forEach(function(prop){
                        if(action.hasOwnProperty(prop)){
                            parms[prop] = action[prop];
                        }
                    });
                    if (action.className) {
                        parms['class'] = action.className;
                    }



                    if(action.menu && (action.type == 'DropDownButton' || action.type == 'ComboButton')){

                        for(var ddIndex=0; ddIndex< action.menu.length; ddIndex++){

                            var menuItemObj = action.menu[ddIndex];

                            Workbench._loadActionClass(menuItemObj);

                            var menuItemParms = {
                                //onClick: dojo.hitch(this, "_runAction", menuItemObj, context)
                            };

                            var props = ['label','iconClass'];

                            props.forEach(function(prop){
                                if(menuItemObj[prop]){
                                    menuItemParms[prop] = menuItemObj[prop];
                                }
                            });






                            //console.log('sub action : ' + menuItemObj.id , menuItemObj);
                            completeAndAddAction(menuItemObj);
                            //var menuItem = new MenuItem(menuItemParms);
                            //menuItem._maqAction = menuItemObj;
                            //menu.addChild(menuItem);
                        }

                        /*
                        if(action.type == 'DropDownButton'){
                            dojoAction = new DropDownButton(parms);
                        }else{
                            dojoAction = new ComboButton(parms);
                        }
                        dojoAction.onClick = dojo.hitch(this, "_runAction", action, context);
                        dojoAction._maqAction = action;
                        dojoActionDeferred.resolve();
                        */
                    }else if (action.toggle || action.radioGroup) {



                        /*
                        dojoAction = new ToggleButton(parms);
                        dojoAction.item = action;
                        dojoAction.set('checked', action.initialValue);

                        if (action.radioGroup) {
                            var group = radioGroups[action.radioGroup];
                            if (!group) {
                                group = radioGroups[action.radioGroup]=[];
                            }
                            group.push(dojoAction);
                            dojoAction.onChange = dojo.hitch(this, "_toggleButton", dojoAction, context, group);
                        } else {
                            dojoAction.onChange = dojo.hitch(this,"_runAction", action, context);
                        }

                        dojoAction._maqAction = action;
                        dojoActionDeferred.resolve();
                        */
                    }


                    if(action.id ==='documentSettings'){
                        console.log('   maq action : ' + action.id,action);
                    }


                    var disabled = false;

                    /*
                    if (action.action && action.action.isEnabled) {
                        //disabled = !action.action.isEnabled(context);
                    }
                    */


                    if(action.id==='layout'){
                        layoutAction=action;
                    }

                    completeAndAddAction(action);

                }
                return result;
            },
            onUseActionStore:function(){}

        };
        return _class;
    }

    function doEditorTests(editor){

        //var d = editor.getActionStore();
        editor.selectAll();
        //editor.runAction('View/Split/7');
        // editor.select('btnDVD');
        //editor.select('btnTV',true);
    }


    function onEditorReady(editor){
        window[module.id + 'lastVE'] = editor;
        setTimeout(function(){
            ctx.mainView.getToolbar().setActionEmitter(null);
            ctx.mainView.getToolbar().setActionEmitter(editor);
            doEditorTests(editor);

        },5000);
    }




    function testMain(){
        if(_lastEditor){
            utils.destroy(_lastEditor);
            if(_lastEditor._parent && _lastEditor._parent.docker()) {
                _lastEditor._parent.docker().removePanel(_lastEditor._parent);
            }
        }
        Basics.createVisualEditor('workspace_user','./A-MediaPlayer.dhtml',createEditorOverrideClass(),onEditorReady);
    }

    if (ctx) {

        testMain();
    }

    var Module =declare('m',null,[]);


    return Module;
});