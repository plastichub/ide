/** @module xgrid/Base **/
define([
    'xdojo/declare',
    'xide/types',
    'xide/utils',
    'module',
    './Basics',
    "xideve/tests/Reload",
    "xideve/views/VisualEditor",
    "xideve/views/VisualEditorAction"

], function (declare, types, utils, module, Basics, Reload, VisualEditor, VisualEditorAction) {

    console.clear();
    /***
     *
     * playground
     */
    var leftGrid = window[module.id + 'leftGrid'];
    var ctx = window.sctx,
        parent,
        _lastEditor = window[module.id + 'lastVE'];

    ///////////////////////////////////////////////////
    //
    //
    //
    //  Switches
    //
    //

    var addOutline = false;
    var showLESS = true;
    var showPalette = false;
    var showBehaviours = false;
    var showStyleView = false;
    var showBlocks = false;

    function createEditorOverrideClass() {

        var ACTION = types.ACTION;

        var _class = {
            handleActionClick: function (e) {
                //console.log('handle action clicke ', e.target);
                return false;

            },
            dcl: true,
            addOutline: addOutline,
            showPalette: showPalette,
            showLESS: showLESS,
            showBehaviours: showBehaviours,
            showStyleView: showStyleView,
            showBlocks: showBlocks,
            permissions: [
                //ACTION.DOWNLOAD,
                //ACTION.RELOAD,
                //ACTION.DELETE,
                //ACTION.SAVE,
                //ACTION.CLIPBOARD,
                //ACTION.LAYOUT,
                ACTION.SELECTION
                //ACTION.PREVIEW
            ],
            onUseActionStore: function () {}
        };
        return _class;
    }

    function doEditorTests(editor) {
        //var d = editor.getActionStore();
        //editor.selectAll();
        //editor.runAction('View/Split/7');
        //editor.select('btnDVD');
        //editor.select('btnTV',true);
    }

    function onEditorReady(editor) {

        window[module.id + 'lastVE'] = editor;
        setTimeout(function () {

            ctx.getWindowManager().registerView(editor, true);

            //ctx.mainView.getToolbar().setActionEmitter(null);
            //ctx.mainView.getToolbar().setActionEmitter(editor);
            doEditorTests(editor);

        }, 5000);

    }

    function testMain() {

        if (_lastEditor) {
            utils.destroy(_lastEditor);
            if (_lastEditor._parent && _lastEditor._parent.docker()) {
                _lastEditor._parent.docker().removePanel(_lastEditor._parent);
            }
        }
        Basics.createVisualEditor('workspace_user', './marantz.dhtml', createEditorOverrideClass(), onEditorReady);
    }

    if (ctx) {
        function findMethod(_module, method) {


            function test(proto) {

                if (proto[method]) {

                    console.log('found method');

                    return proto[method];
                }

                debugger;

                console.dir(_module);

                var meta = proto['_meta'];
                if (meta) {
                    var _parents = _.isArray(meta.parents) ? meta.parents : meta.parents();
                    console.log('check parents', _parents);
                }
            }


            var fn = test(_module, method);

        }
        //var method = findMethod(VisualEditorAction,'reloadModule');

        //return ;
        Reload.reload().then(function (data) {
            var component = data.component;
            component.registerEditors();
            testMain();
        });
    }

    var Module = declare('m', null, []);
    return Module;
});