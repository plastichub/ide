/** @module xgrid/Base **/
define([
    'xdojo/declare',
    'xdocker/Docker2',
    'xfile/tests/TestUtils',
    'dcl/dcl',
    'xide/utils',
    'xide/editor/Registry',
    'module'

], function (declare,Docker,FTestUtils,dcl,utils,Registry,module) {


    /***
     *
     * playground
     */
    var leftGrid = window[module.id + 'leftGrid'];
    var ctx = window.sctx,
        parent,
        _lastEditor = window[module.id + 'lastVE'];

    ///////////////////////////////////////////////////
    //
    //
    //
    //  Switched
    //
    //

    var CREATE_LEFT_FILE_GRID = false;

    function createDockerClass(){
        var _class = dcl(null,{
        });
        return _class;
    }
    function createSubDocker(tab){
        var myDocker = Docker.createDefault(tab.containerNode, {
            extension:createDockerClass()
        });
        myDocker.uuid = 'sub docker';
        return myDocker;
    }
    function addFileGridToLeft(){

        utils.destroy(leftGrid);

        var mainView = ctx.mainView,
            application = ctx.application,
            toolbar = mainView.getToolbar(),
            docker = mainView.getDocker(),
            left = mainView.layoutLeft,
            container = application.leftLayoutContainer;


        var grid = FTestUtils.createFileGrid(null,{},null,'Files',module.id,true,container);

        window[module.id + 'leftGrid'] = grid;

        return grid;


    }

    function createEditorOverrideClass(){

        var _class = {
            startup:function(){
            }
        };
        //"TypeError: Cannot read property 'frameNode' of null at declare.destroy (http://127.0.0.1/projects/x4mm/Code/client/src/lib/xideve/views/VisualEditor.js:352:50) at Object.dispatcher.around.advice (http://127.0.0.1/projects/x4mm/Code/client/src/lib/dojo/aspect.js:110:23) at target.(anonymous function).dispatcher [as destroy] (http://127.0.0.1/projects/x4mm/Code/client/src/lib/dojo/aspect.js:92:39) at Function.utils._destroyWidget (http://127.0.0.1/projects/x4mm/Code/client/src/lib/xide/utils/HTMLUtils.js:480:26) at Function.utils.destroy (http://127.0.0.1/projects/x4mm/Code/client/src/lib/xide/utils/HTMLUtils.js:534:23) at Function.utils.destroy (http://127.0.0.1/projects/x4mm/Code/client/src/lib/xide/utils/HTMLUtils.js:547:22) at createVisualEditor (http://127.0.0.1/projects/x4mm/Code/client/src/lib/xideve/tests/Basics.js?time=Thu%20Oct%2029%202015%2013:07:32%20GMT+0100%20(CET):98:19) at http://127.0.0.1/projects/x4mm/Code/client/src/lib/xideve/tests/Basics.js?time=Thu%20Oct%2029%202015%2013:07:32%20GMT+0100%20(CET):123:17"
        return _class;
    }

    function createVisualEditor(item,override,readyCB){

        var editors = Registry.getEditors(item);
        var ve = _.find(editors,function(e){
            return e.name ==='Visual Editor' && e.extensions==='dhtml';
        });

        var editor = ve.onEdit(item,override,null/*|| createEditorOverrideClass()*/);
        editor && editor.then && editor.then(function(instance){
            if(readyCB){
                readyCB(instance);
            }
        });
    }
    function testMain(tab){
        var fileStore = FTestUtils.createStore('workspace_user');
        fileStore.fetchRange().then(function(e){
            setTimeout(function(){
                var item = fileStore.getItem('./marantz.dhtml',true);
                debugger;
                createVisualEditor(fileStore.getItem('./marantz.dhtml',true));
            },1000);
        });
    }
    var Module =declare('m',null,[]);
    Module.createVisualEditor=function(mount,file,override,readyCB){

        var fileStore = FTestUtils.createStore(mount || 'workspace_user');
        fileStore.fetchRange().then(function(e){
            setTimeout(function(){
                createVisualEditor(fileStore._getItem( file || './marantz.dhtml'),override,readyCB);
            },500);

        });
    }



    return Module;
});