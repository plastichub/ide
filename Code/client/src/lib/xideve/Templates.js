/** @module xideve/Templates **/
define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "xide/utils",
    "dojo/has",
    './stub',
    "require"
], function (declare,lang,utils,has,stub,require) {



    /**
     * Simple and static registry for 'Document' templates.
     * The original IDE only supported Dojo applications and this
     * registry acts database for more document types in regard of
     * jQuery/Angular/Deliteful apps.
     *
     *
     * Attention: this is currently a temporary solution only and implemented as
     * singleton per IDE context.
     *
     * @class module:xideve/Templates
     */
    var Templates = declare('xideve/Templates',null,{
        /**
         * Public registry of document types: 'Dojo' | 'Delite'
         * @type {Object}
         */
        registry:{},

        /**
         *
         *
         * @param type {string}
         * @param data {object}
         */
        register:function(type,data){

            if(type in this.registry == false){
                this.registry[type]=data;
            }
        }
    });

    var instance = new Templates();

    lang.mixin(Templates,instance);//enable static access

    return Templates;

});

