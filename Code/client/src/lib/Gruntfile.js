/*global module */
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        jshint: {
            src: [
                "**/*.js",
                "!./app.profile.js",
                "!./MaqettaPatch.js",
                "!{node_modules,dijit,form,layout,mobile}/**/*.js",

                // Note: skip this file since it gives a JSHint error about a character being silently deleted.
                // It will have to be fixed by the translators.
                "!nls/he/loading.js"
            ],
            options: {
                jshintrc: ".jshintrc"
            }
        },
        "jsdoc-amddcl": {

            "plugins": [
                "plugins/markdown"
            ],
            docs: {
                files: [
                    {
                        src: [
                            "xcf/constants.jsdoc",
                            "xcf/consumer.js",
                            "xcf/ReadmeJSDOC.md",
                            "xide/types.js",
                            "xide/types/Types.js",
                            "xcf/types/Types.js",
                            "xcf/manager/DriverManager.js",
                            "xcf/manager/DeviceManager.js",
                            "xcf/model/Device.js",
                            "xcf/model/Driver.js",
                            "xcf/model/Command.js",
                            "xcf/model/Variable.js",
                            "xcf/driver/*.js",
                            "xide/utils/StringUtils.js",
                            "xblox/types/Types.js",
                            "!./node_modules"
                        ]


                    }
                ]
            },
            'export': {
                files: [
                    {
                        args: [
                            "-X"
                        ],
                        src: [
                            "./"

                        ],
                        dest: "/tmp/doclets_xcf.json"
                    }
                ]
            }
        }
    });

    // Load plugins
    grunt.loadNpmTasks("intern");
    grunt.loadNpmTasks("grunt-contrib-jshint");

    grunt.loadNpmTasks("jsdoc-amddcl");
    //grunt.loadTasks("themes/tasks");// Custom cssToJs task to convert CSS to JS

    // Aliases
    //grunt.registerTask("css", ["less", "cssToJs"]);
    grunt.registerTask("jsdoc", "jsdoc-amddcl");
};