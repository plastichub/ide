/** @module xblox/views/Grid **/
define([
    "xdojo/declare",
    'xide/types',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xblox/views/ThumbRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xide/views/_LayoutMixin',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'xgrid/KeyboardNavigation',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/utils',
    'xblox/BlockActions',
    'xgrid/Search',
    "xide/widgets/_Widget",
    "xblox/views/DnD"

], function (declare, types,
             ListRenderer, TreeRenderer, ThumbRenderer,
             Grid, MultiRenderer, _LayoutMixin,
             Defaults, Layout, Focus, KeyboardNavigation,
             OnDemandGrid, EventedMixin, utils, BlockActions, Search, _Widget,DnD) {

    var renderers = [TreeRenderer, ThumbRenderer],
        multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);


    /**
     * Block grid base class.
     * @class module:xblox/views/Grid
     */
    var GridClass = Grid.createGridClass('xblox.views.Grid',
        {
            options: utils.clone(types.DEFAULT_GRID_OPTIONS)
        },
        //features
        {

            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            COLUMN_RESIZER: types.GRID_FEATURES.COLUMN_RESIZER,
            SPLIT: {
                CLASS: _LayoutMixin
            },
            BLOCK_ACTIONS: {
                CLASS: BlockActions
            },
            DND:{
                CLASS:DnD
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },

            SEARCH: {
                CLASS: Search
            },
            WIDGET: {
                CLASS: _Widget
            }

        },
        {
            //base flip
            RENDERER: multiRenderer

        },
        {
            //args
            renderers: renderers,
            selectedRenderer: TreeRenderer
        },
        {
            GRID: OnDemandGrid,
            //EDITOR: Editor,
            LAYOUT: Layout,
            DEFAULTS: Defaults,
            RENDERER: ListRenderer,
            EVENTED: EventedMixin,
            FOCUS: Focus
        }
    );

    //static exports
    GridClass.DEFAULT_RENDERERS = renderers;
    GridClass.DEFAULT_MULTI_RENDERER = multiRenderer;
    return GridClass;
});