define([
    "dcl/dcl",
    'xide/views/_CIDialog',
    'xide/utils',
    'xblox/views/BlockEditView'
], function (dcl, _CIDialog, utils, BlockEditView) {
    return dcl(_CIDialog, {
        bodyCSS: {
            'height': 'auto',
            'min-height': '400px'
        },
        declaredClass: "xblock.views.BlockEditDialog",
        onSave: function (ci, value) {
            if (ci.dst && this.item[ci.dst]) {
                this.item[ci.dst] = value;
            }
        },
        __initWithCIS: function (cis) {
            this.cisView = utils.addWidget(BlockEditView, {
                delegate: this,
                options: {
                    groupOrder: {
                        'General': 0,
                        'Advanced': 1,
                        'Description': 2
                    }
                },
                cis: cis
            }, this, this.containerNode, true);
        },
        __onOk: function () {
            var cis = this.cisView.getCIS();
            var options = utils.toOptions(cis);
            //now convert back to block fields
            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                var field = option.dst;
                if (field != null && this.item[field] != null) {
                    if (option.active != null && option.active === false && option.changed === false) {
                        continue;
                    }
                    if (this.item[option.dst] != option.value ||
                        this.item[option.dst] !== option.value) {
                        if (this.item.onChangeField) {
                            this.item.onChangeField(option.dst, option.value);
                        }
                        this.item[option.dst] = option.value;
                    }
                }
            }
            if (this.delegate && this.delegate.onOk) {
                this.delegate.onOk(this, this.item);
            }
            this.hide();
            return true;
        },
        initWithBlock: function (item) {
            var cis = item.getFields();
            if (cis) {
                this.initWithCIS(cis);
            }
        },
        startup: function () {
            if (this._started) {
                return;
            }
            this.inherited(arguments);
            if (this.item) {
                this.initWithBlock(this.item);
            }
            this.addActionButtons();
            this.inherited(arguments);
        }
    });
});