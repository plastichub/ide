/** @module xblox/views/ThumbRenderer **/
define([
    "xdojo/declare",
    'xide/utils',
    'dojo/dom-construct',
    'xgrid/ThumbRenderer',
    "xide/$"
], function (declare, utils, domConstruct, ThumbRenderer, $) {
    /**
     * The list renderer does nothing since the xgrid/Base is already inherited from
     * dgrid/OnDemandList and its rendering as list already.
     *
     * @class module:xfile/ThumbRenderer
     * @extends module:xgrid/Renderer
     */
    return declare('xblox.views.ThumbRenderer', [ThumbRenderer], {
        thumbSize: "400",
        resizeThumb: true,
        deactivateRenderer: function () {
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding', '');
            this.isThumbGrid = false;

        },
        activateRenderer: function () {
            $(this.domNode.parentNode).addClass('metro');
            $(this.contentNode).css('padding', '8px');
            this.isThumbGrid = true;
            this.refresh();
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            if (obj.renderRow) {
                var _res = obj.renderRow.apply(this, [obj]);
                if (_res) {
                    return _res;
                }
            }
            var thiz = this,
                div = domConstruct.create('div', {
                    className: "tile widget"
                }),
                icon = obj.icon,
                no_access = obj.read === false && obj.write === false,
                isBack = obj.name == '..',
                directory = true,
                useCSS = false,
                label = '',
                imageClass = obj.icon ? (obj.icon + ' fa-5x' ) : 'fa fa-folder fa-5x',
                isImage = false;


            this._doubleWidthThumbs = true;
            var iconStyle = 'text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;opacity: 0.7';
            var contentClass = 'icon';
            if (directory) {

                if (isBack) {
                    imageClass = 'fa fa-level-up fa-5x itemFolder';
                    useCSS = true;
                } else if (!no_access) {
                    imageClass = 'fa fa-folder fa-5x itemFolder';
                    useCSS = true;
                } else {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                }

            } else {
                imageClass = 'itemIcon';
                if (no_access) {
                    imageClass = 'fa fa-lock fa-5x itemFolder';
                    useCSS = true;
                } else {


                    imageClass = 'fa fa-5x ' + 'fa-play';
                    useCSS = true;
                }

            }

            label = obj.name;

            imageClass = obj.icon ? (obj.icon + ' fa-5x' ) : 'fa fa-folder fa-5x';

            var folderContent = '<span style="' + iconStyle + '" class="fa fa-6x ' + imageClass + '"></span>';

            var _image = false;
            if (_image) {

                var url = this.getImageUrl(obj);
                if (url) {
                    obj.icon = url;
                } else {
                    obj.icon = thiz.config.REPO_URL + '/' + obj.path;
                }

                imageClass = '';
                contentClass = 'image';
                folderContent = '<div style="" class="tile-content image">' +
                    '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                    '</div>';

                useCSS = true;
                isImage = true;

            }
            var label2 = label + '\n' + obj.modified;
            var html = '<div title="' + label2 + '" class="tile-content ' + contentClass + '">' +
                folderContent +
                '</div>' +

                '<div class="brand opacity">' +
                '<span class="thumbText text opacity ellipsis" style="">' +
                label +
                '</span>' +
                '</div>';
            if (isImage || this._doubleWidthThumbs) {
                $(div).addClass('double');
            }

            if (useCSS) {
                div.innerHTML = html;
                return div;
            }
            if (directory) {
                div.innerHTML = html;
            } else {
                div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
            }
            return div;
        }
    });
});