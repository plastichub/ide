define([
    'dcl/dcl',
    'dojo/_base/declare',
    'xide/factory',
    'xide/utils',
    'xblox/model/variables/Variable',
    'dojo/Deferred',
    'xide/types',
    'xide/views/_LayoutMixin',
    'xblox/views/BlockGrid',
    'xaction/ActionProvider',
    'xide/layout/Container',
    'xdocker/Docker2',
    "xide/views/_CIPanelDialog",
    "xide/lodash"
], function (dcl, declare, factory, utils, Variable, Deferred, types, _LayoutMixin, BlockGrid, ActionProvider, Container, Docker, _CIPanelDialog, _) {
    var LayoutClass = dcl(_LayoutMixin.dcl, {
        rootPanel: null,
        _lastTab: null,
        getDocker: function () {
            if (!this._docker) {
                this._docker = Docker.createDefault(this.containerNode);
                this.add(this._docker);
            }
            return this._docker;
        },
        getRootContainer: function (args) {
            if (this.rootPanel) {
                return this.rootPanel;
            }
            this.reparent = true;
            var docker = this.getDocker();
            var DOCKER = types.DOCKER;
            var defaultTabArgs = {
                icon: false,
                closeable: true,
                moveable: true,
                tabOrientation: DOCKER.TAB.TOP,
                location: DOCKER.DOCK.STACKED

            };
            this.rootPanel = docker.addTab(null, utils.mixin(defaultTabArgs, args));
            return this.rootPanel;
        },
        createLayout: function () {
            if (!this.rootPanel) {
                this.rootPanel = this.getRootContainer({
                    title: 'test'
                });
            }
            return this.rootPanel;
        },
        startup: function () {

        },
        getPropertyStruct: function () {
            return this.propertyStruct;
        },
        setPropertyStruct: function (struct) {
            this.propertyStruct = struct;
        },
        getGroupContainer: function () {
            return this.rootPanel;
        },
        createGroupView: function (groupContainer, group, scope, closable, iconClass, selected, args) {
            var DOCKER = types.DOCKER;
            var defaultTabArgs = {
                icon: iconClass,
                closeable: true,
                moveable: true,
                title: group,
                target: this._lastTab || this.rootPanel,
                tabOrientation: DOCKER.TAB.TOP,
                location: DOCKER.DOCK.STACKED
            };
            var tab = this.getDocker().addTab(null, utils.mixin(defaultTabArgs, args));
            this._lastTab = tab;
            this.tabs.push(tab);
            return tab;
        }
    });

    var GridClass = declare('BlockGrid', BlockGrid, {
        /**
         * Step/Move Down & Step/Move Up action
         * @param dir
         */
        move: function (dir) {
            var items = this.getSelection();
            if (!items || !items.length) {
                console.log('cant move, no selection or parentId', items);
                return;
            }
            var thiz = this;
            _.each(items, function (item) {
                item.move(item, dir);
            });
            thiz.refreshRoot();
            thiz.refreshCurrent();
            this.select(items, null, true, {
                focus: true,
                delay: 10
            }).then(function () {
                thiz.refreshActions();
            });
        },
        reParentBlock: function (dir) {
            var item = this.getSelection()[0];
            if (!item) {
                console.log('cant move, no selection or parentId', item);
                return false;
            }
            var thiz = this;
            if (dir == -1) {
                item.unparent(thiz.blockGroup);
            } else {
                item.reparent();
            }
            thiz.deselectAll();
            thiz.refreshRoot();
            this.refreshCurrent();
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 100,
                select: item,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        defaultActionResult: function (items) {
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 0,
                select: items,
                expand: true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        }
    });

    var Module = dcl([Container, LayoutClass, ActionProvider.dcl], {
        declaredClass: "xblox.views.BlocksFileEditor",
        registerView: false,
        _item: null,
        cssClass: 'bloxEditor',
        blockManager: null,
        blockManagerClass: 'xblox.manager.BlockManager',
        model: null,
        store: null,
        tree: null,
        currentItem: null,
        didLoad: false,
        selectable: false,
        beanType: 'BLOCK',
        newGroupPrefix: '',
        _debug: false,
        blockScope: null,
        groupContainer: null,
        canAddGroups: true,
        gridClass: GridClass,
        activeGrid: null,
        activeTab: null,
        registerGrids: true,
        visibileTab: null,
        setVisibleTab: function (tab) {
            this.visibileTab = tab;
        },
        getVisibleTab: function () {
            return this.visibileTab;
        },
        constructor: function (options, container) {
            utils.mixin(this, options);
        },
        onGridAction: function (evt) {
            var action = evt.action ? evt.action : evt,
                command = action.command,
                ACTION = types.ACTION;

            switch (command) {
                case ACTION.SAVE: {
                    return this.save();
                }
            }
        },
        clearGroupViews: function (all) {
            _.each(this.tabs, function (tab) {
                tab.destroy();
            });
            delete this.tabs;
            this.destroyWidgets();
        },
        getContainerLabel: function (group) {
            var title = '' + group;
            if (utils.isNativeEvent(group)) {
                title = title.replace('on', '');
            }

            if (group.indexOf(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED) !== -1) {
                var deviceManager = this.ctx.getDeviceManager();
                var driverManager = this.ctx.getDriverManager();
                var parts = group.split('__');
                var blockUrl = parts[1];
                var device = deviceManager.getDeviceByUrl(blockUrl);
                var deviceTitle = device ? deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE) : 'Invalid Device';
                var variable = driverManager.getBlock(group);
                title = '(' + deviceTitle + '/' + (variable ? variable.name : 'Unknown Variable') + ')';
                return title;
            }
            if(title==='basicVariables'){
                title = 'Action Variables';
            }
            return title;
        },
        addNewBlockGroup: function (group) {
            if (!group) {
                return;
            }
            var blockScope = this.blockScope;
            var title = this.getContainerLabel(group);
            var contentPane = this.createGroupView(null, title, blockScope, true, 'fa-bell');
            var newGroup = this.newGroupPrefix + group;
            var gridViewConstructurArgs = {
                newRootItemGroup: newGroup
            };
            var view = this.createGroupedBlockView(contentPane, newGroup, blockScope, gridViewConstructurArgs);
            contentPane.grid = view;
            this.activeGrid = view;
            contentPane.select();
        },
        createGroupedBlockView: function (container, group, scope, extra, gridBaseClass) {
            var thiz = this;
            gridBaseClass = gridBaseClass || this.gridClass;
            var store = scope.blockStore;
            if (this._lastTab && !this.__right) {
                this.__right = this.getRightPanel(null, null, 'DefaultFixed', {
                    target: this._lastTab
                });
            }
            var gridArgs = {
                __right: this.__right,
                ctx: this.ctx,
                blockScope: scope,
                blockGroup: group,
                attachDirect: true,
                resizeToParent: true,
                collection: store.filter({
                    group: group
                }),
                _parent: container,
                getPropertyStruct: this.getPropertyStruct,
                setPropertyStruct: this.setPropertyStruct
            };

            extra && utils.mixin(gridArgs, extra);
            var view = utils.addWidget(gridBaseClass, gridArgs, null, container, false);

            if (!view.__editorActions) {
                view.__editorActions = true;
                this.addGridActions(view, view);
            }
            container.grid = view;
            view._on('selectionChanged', function (evt) {
                thiz._emit('selectionChanged', evt);
            });
            view._on('onAfterAction', function (e) {
                thiz.onGridAction(e);
            });
            container.on(types.DOCKER.EVENT.VISIBILITY_CHANGED, function (visible) {
                if (visible) {
                    thiz.setVisibleTab(container);
                    view.onShow();
                    var right = thiz.getRightPanel();
                    var splitter = right.getSplitter();
                    if (!splitter.isCollapsed()) {
                        view.showProperties(view.getSelection()[0]);
                    }
                    if (view.__last) {
                        view._restoreSelection();
                        view.focus(view.row(view.__last.focused));
                    }
                } else {
                    view.__last = view._preserveSelection();
                }
                if (visible && !this.grid._started) {
                }
            });
            this.registerGrids && this.ctx.getWindowManager().registerView(view, false);
            if (!container._widgets) {
                container._widgets = [];
            }
            container.add(view);
            return view;
        },
        getDeviceVariablesAsEventOptions: function (startIntend) {
            var options = [];
            var _item = function (label, value, intend, selected, displayValue) {
                var string = "<span style=''>" + label + "</span>";
                var pre = "";
                if (intend > 0) {
                    for (var i = 0; i < intend; i++) {
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label: _final,
                    label2: displayValue,
                    value: value
                };
            };
            var deviceManager = this.ctx.getDeviceManager();
            var items = deviceManager.getDevices(false, true);

            for (var i = 0; i < items.length; i++) {
                var device = items[i];
                var driver = device.driver;
                if (!driver) {
                    continue;
                }

                var title = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                var id = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_ID);
                options.push(_item(title, driver.id + '/' + driver.id, startIntend, false));
                var blockScope = driver.blockScope;
                var variables = blockScope.getVariables();
                for (var j = 0; j < variables.length; j++) {
                    var variable = variables[j];
                    var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED + '__' + id + '__' + driver.id + '__' + variable.id;
                    var selected = false;
                    options.push(_item(variable.name, value, startIntend + 1, selected, title + '/' + variable.name));
                }
            }

            return options;
        },
        openNewGroupDialog: function () {
            var options = [];
            var _item = function (label, value, intend, isHTML) {
                var string = isHTML !== true ? "<span style=''>" + label + "</span>" : label;
                var pre = "";
                if (intend > 0) {
                    for (var i = 0; i < intend; i++) {
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                        pre += "&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label: _final,
                    value: value,
                    label2: label
                };
            };

            options.push(_item('HTML', '', 0));
            options = options.concat([
                _item('onclick', 'click', 1),
                _item('ondblclick', 'dblclick', 1),
                _item('onmousedown', 'mousedown', 1),
                _item('onmouseup', 'mouseup', 1),
                _item('onmouseover', 'mouseover', 1),
                _item('onmousemove', 'mousemove', 1),
                _item('onmouseout', 'mouseout', 1),
                _item('onkeypress', 'keypress', 1),
                _item('onkeydown', 'keydown', 1),
                _item('onkeyup', 'keyup', 1),
                _item('onfocus', 'focus', 1),
                _item('onblur', 'blur', 1),
                _item('onchange', 'change', 1),
                _item('input', 'input', 1),
                _item('load', 'load', 1)
            ]);

            var inVariableStr = "";

            var thiz = this;
            var actionDialog = new _CIPanelDialog({
                title: 'Create a new block group',
                style: 'width:500px;min-height:200px;',
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                resizeable: true,
                onOk: function (changedCIS) {
                    //command picker
                    if (changedCIS && changedCIS[0] && changedCIS[0].dst === 'variable') {
                        var url = changedCIS[0].newValue;
                        var parts = utils.parse_url(url);//strip scheme
                        parts = utils.urlArgs(parts.host);//go on with query string
                        //var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED+ '__' + parts.device.value +'__'+parts.driver.value + '__'+ parts.block.value;
                        var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED + '__' + url;
                        thiz.addNewBlockGroup(value);
                        return;
                    }
                    if (changedCIS && changedCIS[0]) {
                        thiz.addNewBlockGroup(changedCIS[0].newValue);
                    }
                },
                cis: [
                    utils.createCI('Event', types.ECIType.ENUMERATION, '',
                        {
                            group: 'Event',
                            delegate: null,
                            options: options,
                            value: 'HTML',
                            title: 'Select an event &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                            widget: {
                                search: true,
                                "class": "xide.form.FilterSelect"
                            }
                        }
                    ),
                    utils.createCI('value', 'xcf.widgets.CommandPicker', inVariableStr, {
                        group: 'Event',
                        title: ' ',
                        dst: 'variable',
                        pickerType: 'variable',
                        value: ' Select Driver Variable ',
                        block: {
                            scope: {
                                ctx: thiz.ctx
                            }
                        },
                        widget: {
                            //store:this.scope.blockStore,
                            labelField: 'name',
                            valueField: 'id',
                            value: 'Select Driver Variable',
                            query: [
                                {
                                    group: 'basicVariables'
                                },
                                {
                                    group: 'processVariables'
                                }
                            ]

                        }
                    })
                ]
            });

            this.add(actionDialog);
            return actionDialog.show();
        },
        onGroupsCreated: function () {
            //this.ctx.getWindowManager().registerView(this,true);
        },
        getActionStore: function () {
            if (this.activeGrid) {
                return this.activeGrid.getActionStore();
            }
            return null;
        },
        getTabContainer: function () {
            return this.groupContainer;
        },
        onReloaded: function () {
            this.destroyWidgets();
            this.openItem(this._item);
        },
        /**
         * default entry when opening a file item through xfile
         * @param item
         */
        openItem: function (item) {

            var dfd = new Deferred();
            this._item = item;
            var thiz = this;

            var blockManager = this.getBlockManager();

            blockManager.load(item.mount, item.path).then(function (scope) {
                thiz.initWithScope(scope);
                dfd.resolve(thiz);
            });

            return dfd;

        },
        /**
         * Init with serialized string, forward to
         * @param content
         */
        initWithContent: function (content) {
            var data = null;
            try {
                data = utils.getJson(content);
            } catch (e) {
                console.error('invalid block data');
            }

            if (data) {
                this.initWithData(data);
            }
        },
        /**
         * Entry point when a blox scope is fully parsed
         * @param blockScope
         */
        initWithScope: function (blockScope) {
            this.blockScope = blockScope;
            var allBlockGroups = blockScope.allGroups(),
                thiz = this;
            if (allBlockGroups.indexOf('Variables') == -1) {
                allBlockGroups.push('Variables');
            }
            if (allBlockGroups.indexOf('Events') == -1) {
                allBlockGroups.push('Events');
            }
            if (allBlockGroups.indexOf('On Load') == -1) {
                allBlockGroups.push('On Load');
            }
            thiz.renderGroups(allBlockGroups, blockScope);

        },
        getScopeUserData: function () {
            return {
                owner: this
            };
        },
        initWithData: function (data) {
            if (this._debug) {
                console.log('init with data', data);
            }

            this.onLoaded();

            var scopeId = utils.createUUID(),
                blockInData = data,
                variableInData = data;

            //check structure
            if (_.isArray(data)) {// a flat list of blocks

            } else if (_.isObject(data)) {
                scopeId = data.scopeId || scopeId;
                blockInData = data.blocks || [];
            }
            var blockManager = this.getBlockManager();
            this.blockManager = blockManager;
            var scopeUserData = this.getScopeUserData();
            var blockScope = blockManager.getScope(scopeId, scopeUserData, true);
            var allBlocks = blockScope.blocksFromJson(blockInData);
            for (var i = 0; i < allBlocks.length; i++) {
                var obj = allBlocks[i];

                obj._lastRunSettings = {
                    force: false,
                    highlight: true
                };
            }
            blockManager.onBlocksReady(blockScope);
            if (allBlocks) {
                return this.initWithScope(blockScope);
            }
        },
        destroyWidgets: function () {
            if (this.blockManager && this.blockScope) {
                this.blockManager.removeScope(this.blockScope.id);
            }
            this.blockManager = null;
        },
        destroy: function () {
            this.destroyWidgets();
            delete this.tabs;
            delete this.propertyStruct;
            delete this.visualEditor;
            delete this.widget;
            delete this.params;
            delete this.pageEditor;
            delete this.editorContext;
            delete this.activeGrid;

            this.item = null;
            this._parent = null;
            this.ctx = null;
            this.docker = null;
            this.__right = null;
            this.blockScope = null;
            this.delegate = null;


        },
        getBlockManager: function () {
            if (!this.blockManager) {
                if (this.ctx.blockManager) {
                    return this.ctx.blockManager;
                }
                this.blockManager = factory.createInstance(this.blockManagerClass, {
                    ctx: this.ctx
                });
                if (this._debug) {
                    console.log('_createBlockManager ', this.blockManager);
                }
            }
            return this.blockManager;
        },
        getActiveGrid: function () {
            return this.activeGrid;
        },
        runAction: function (action) {
            if (action.command == 'File/New Group') {
                this.openNewGroupDialog();
                return null;
            }
            return this.getActiveGrid().runAction(arguments);
        },
        addGridActions: function (grid, who) {
            var result = [];
            var thiz = this;
            var defaultMixin = {addPermission: true};
            result.push(thiz.createAction({
                label: 'New Group',
                command: 'File/New Group',
                icon: 'fa-magic',
                tab: 'Home',
                group: 'File',
                keycombo: ['f7'],
                mixin: utils.mixin({quick: true}, defaultMixin)
            }));
            result.push(thiz.createAction({
                label: 'Delete Group',
                command: 'File/Delete Group',
                icon: 'fa-remove',
                tab: 'Home',
                group: 'File',
                keycombo: ['ctrl f7'],
                mixin: utils.mixin({quick: true}, defaultMixin)
            }));
            (who || this).addActions(result);
        },
        onShowGrid: function (grid) {
        },
        renderGroup: function (group, blockScope, gridBaseClass) {
            blockScope = blockScope || this.blockScope;
            var thiz = this;

            var title = group.replace(this.newGroupPrefix, '');
            if (utils.isNativeEvent(group)) {
                title = title.replace('on', '');
            }
            title = this.getContainerLabel(group.replace(this.newGroupPrefix, ''));
            var isVariableView = group === 'Variables';
            var contentPane = this.createGroupView(null, title, blockScope, !isVariableView, isVariableView ? ' fa-info-circle' : 'fa-bell', !isVariableView);
            var gridViewConstructurArgs = {};
            if (group === 'Variables') {
                gridViewConstructurArgs.newRootItemFunction = function () {
                    new Variable({
                        title: 'No-Title-Yet',
                        type: 13,
                        value: 'No Value',
                        enumType: 'VariableType',
                        save: false,
                        initialize: '',
                        group: 'Variables',
                        id: utils.createUUID(),
                        scope: blockScope
                    });

                };

                gridViewConstructurArgs.onGridDataChanged = function (evt) {

                    var item = evt.item;
                    if (item) {
                        item[evt.field] = evt.newValue;
                    }
                    thiz.save();
                };
                gridViewConstructurArgs.showAllBlocks = false;
                gridViewConstructurArgs.newRootItemLabel = 'New Variable';
                gridViewConstructurArgs.newRootItemIcon = 'fa-code';
                gridViewConstructurArgs.gridParams = {
                    cssClass: 'bloxGridView',
                    getColumns: function () {
                        return [
                            {
                                label: "Name",
                                field: "title",
                                sortable: true

                            },
                            {
                                label: "Value",
                                field: "value",
                                sortable: false
                            }
                        ];
                    }
                };
            }


            gridViewConstructurArgs.newRootItemGroup = group;

            var view = this.createGroupedBlockView(contentPane, group, blockScope, gridViewConstructurArgs, gridBaseClass);
            contentPane.blockView = view;
            !this.activeGrid && (this.activeGrid = view);
            return view;
        },
        renderGroups: function (_array, blockScope) {
            this._isCreating = true;
            this.activeGrid = null;
            this.tabs = [];
            this._lastTab = null;
            this.setPropertyStruct({
                currentCIView: null,
                targetTop: null,
                _lastItem: null,
                id: utils.createUUID()
            });
            for (var i = 0; i < _array.length; i++) {
                var group = _array[i];
                if (this.newGroupPrefix == '' && group.indexOf('__') !== -1) {
                    continue;
                }
                try {
                    var groupBlocks = blockScope.getBlocks({
                        group: group
                    });

                    if (group !== 'Variables' && (!groupBlocks || !groupBlocks.length)) {//skip empty
                        continue;
                    }
                    this.renderGroup(group, blockScope);
                } catch (e) {
                    logError(e);
                }
            }

            if (this.tabs && this.tabs[0]) {
                this.tabs[0].silent = true;
                this.tabs[0].select();
                this.tabs[0].silent = false;
            }
        },
        onSave: function (groupedBlockView) {
            this.save();
        },
        save: function () {
            if (this.blockScope) {
                var fileManager = this.ctx.getFileManager(),
                    item = this.item;
                fileManager.setContent(item.mount, item.path, this.blockScope.toString(), function () {
                    console.log('saved blocks! to ' + item.path);
                });
            } else {
                console.warn('BlocksFileEditor::save : have no block scope');
            }
        }
    });
    return Module;
});