/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dojo/_base/lang",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/ThumbRenderer',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    "xblox/widgets/BlockGridRowEditor",

    'dgrid/Editor',


    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',

    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',

    'xide/factory',


    'dijit/Menu',

    'xide/data/Reference',

    'dijit/form/DropDownButton',
    'dijit/MenuItem',

    'xide/docker/Docker',


    "xide/views/CIViewMixin",

    'xide/layout/TabContainer',


    "dojo/has!host-browser?xblox/views/BlockEditDialog",

    'xblox/views/BlockGrid',

    'xide/layout/ContentPane',

    'xide/views/_LayoutMixin'


], function (declare,lang, types,
             utils, ListRenderer, TreeRenderer, ThumbRenderer,
             _ActionMixin,
             Grid, MultiRenderer, RadioButton, Ribbon, Registry, DefaultActions, Action, BlockGridRowEditor,
             Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Menu,Reference,DropDownButton,
             MenuItem,Docker,CIViewMixin,TabContainer,

             BlockEditDialog,
             BlockGrid,
             ContentPane,
             _LayoutMixin
    ) {


    var editorClass = declare("xblox.views.BlocksFileEditor", [ContentPane,_LayoutMixin],{
        getActionStore:function(){



            var tContainer = this.getGroupContainer();

            if(tContainer.selectedChildWidget){
                return tContainer.selectedChildWidget.grid.getActionStore();
            }

            return null;

        },
        //////////////////////////////////////////////////////////
        //
        //  object instances
        //

        /**
         * xFile item, tracked in ::openItem
         */
        _item: null,
        cssClass: 'bloxEditor',
        blockManager: null,
        blockManagerClass: 'xblox.manager.BlockManager',
        model: null,
        store: null,
        tree: null,
        currentItem: null,
        didLoad: false,
        selectable: false,
        beanType: 'BLOCK',
        newGroupPrefix:'',
        _debug:false,
        /**
         * Current Blox Scope {xblox.model.Scope}
         */
        blockScope: null,
        groupContainer: null,
        canAddGroups:true,
        gridClass:BlockGrid,
        clearGroupViews:function(all){



            this.destroyWidgets();
            return;

            var container = this.getGroupContainer(),
                thiz = this;

            var panes = container.getChildren();
            for (var i = 0; i < panes.length; i++) {
                if(panes[i].isNewTab){
                    container.removeChild(panes[i]);
                }
            }
            for (var i = 0; i < panes.length; i++) {

                var pane = panes[i];
                /*
                 if(pane.title=='Variables' && all!==true){
                 continue;
                 }*/
                container.removeChild(pane);


            }

            this.createNewTab();



        },
        getContainerLabel:function(group){


            var title = '' + group;
            console.log('add new block group ' + group);
            if(utils.isNativeEvent(group)){
                title = title.replace('on','');
            }


            //device variable changed: onDriverVariableChanged__deviceId__dd2985b9-9071-1682-226c-70b84b481117/9ab3eabe-ef9a-7613-c3c8-099cde54ef39
            if(group.indexOf(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED)!==-1){

                var deviceManager = this.ctx.getDeviceManager();
                var parts = group.split('__');
                var event = parts[0];
                var deviceId = parts[1];
                var driverId = parts[2];
                var variableId = parts[3];
                var device = deviceManager.getDeviceById(deviceId);

                var driverScope = device ? device.driver : null;

                //not initiated driver!
                if(driverScope && driverScope.blockScope){
                    driverScope=driverScope.blockScope;
                }

                if(!driverScope){
                    console.error('have no driver, use driver from DB',group);
                    if(device) {
                        var driverId = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                        //var driverManager = this.ctx.getDriverManager();
                        driverScope = this.ctx.getBlockManager().getScope(driverId);

                    }
                }

                var deviceTitle = device ? deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE) : 'Invalid Device';
                var variable=driverScope ? driverScope.getVariableById(driverId + '/' + variableId) : 'Invalid Variable(Have no driver)';
                title = 'Variable Changed('+deviceTitle+'/'+ (variable ? variable.title : 'Unknown Variable') +')';


            }
            return title;
        },
        addNewBlockGroup:function(group){


            if(!group){
                return;
            }

            var blockScope = this.blockScope;
            var container = this.getGroupContainer();

            var title = this.getContainerLabel(group);
            var contentPane = this.createGroupView(container, title, blockScope,true,'fa-bell');

            var gridViewConstructurArgs = {};


            var newGroup = this.newGroupPrefix + group;
            gridViewConstructurArgs.newRootItemGroup = newGroup;

            if(this._debug) {
                console.log('add new group:' + newGroup);
            }

            var view = this.createGroupedBlockView(contentPane.containerNode, newGroup, blockScope, gridViewConstructurArgs);

            container.selectChild(contentPane);
        },
        createGroupedBlockView: function (container, group, scope, extra,gridBaseClass) {

            var thiz = this;

            gridBaseClass = gridBaseClass || this.gridClass;


            var store = group ==='Variables' ?  scope.variableStore : scope.blockStore;


            var gridArgs = {
                _docker:this.getDocker(),
                __right:this.getRightPanel(),
                ctx:this.ctx,
                blockScope: scope,
                blockGroup: group,
                attachDirect:true,
                shouldShowAction: function (action) {
                    return true;
                },
                collection: store.filter({
                    group: group
                })
            };

            var args = {
                blockGroup: group,
                title: group,
                blockScope: scope,
                ctx: this.ctx,
                delegate: this,
                showAllBlocks: true,
                open: true,
                lazy: true,
                titlePane: false,
                canToggle: false,
                gridParams: {
                    cssClass: 'bloxGridView'
                },
                gridBaseClass:gridBaseClass
            };

            if (extra) {
                lang.mixin(gridArgs, extra);
            }

            var view = utils.addWidget(gridBaseClass,gridArgs,null,container,true);

            container.grid = view;

            view._on('selectionChanged',function(evt){
                thiz._emit('selectionChanged',evt);
            });

            return view;
        },
        getDeviceVariablesAsEventOptions:function(startIntend){

            var options = [];
            var _item = function(label,value,intend,selected,displayValue){


                var string="<span style=''>" +label + "</span>";
                var pre = "";
                if(intend>0){
                    for (var i = 0; i < intend; i++) {
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label:_final,
                    label2:displayValue,
                    value:value
                };
            };

            var deviceManager = this.ctx.getDeviceManager();
            var items = deviceManager.getDevices(false,true);

            for (var i = 0; i < items.length; i++) {
                var device = items[i];
                var driver = device.driver;
                if(!driver){
                    continue;
                }

                var title = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                var id = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_ID);
                options.push(_item(title,driver.id+'/' +driver.id,startIntend,false));


                var blockScope = driver.blockScope;
                var variables = blockScope.getVariables();

                console.log(device);

                for (var j = 0; j < variables.length; j++) {
                    var variable = variables[j];
                    var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED+ '__' + id+'__'+driver.id + '__'+ variable.id;
                    var selected = false;
                    options.push(_item(variable.title,value,startIntend + 1,selected,title + '/' + variable.title));
                }
            }

            return options;
        },
        openNewGroupDialog:function(){

            //var options = utils.getEventsAsOptions();

            var options = [];

            var _item = function(label,value,intend){


                var string="<span style=''>" +label + "</span>";
                var pre = "";

                if(intend>0){
                    for (var i = 0; i < intend; i++) {
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label:_final,
                    value:value,
                    label2:label
                };
            };

            options.push( _item('HTML','',0));
            options = options.concat([
                _item('onclick','click',1),
                _item('ondblclick','dblclick',1),
                _item('onmousedown','mousedown',1),
                _item('onmouseup','mouseup',1),
                _item('onmouseover','mouseover',1),
                _item('onmousemove','mousemove',1),
                _item('onmouseout','mouseout',1),
                _item('onkeypress','keypress',1),
                _item('onkeydown','keydown',1),
                _item('onkeyup','keyup',1),
                _item('onfocus','focus',1),
                _item('onblur','blur',1),
                _item('load','load',1)
            ]);

            options.push( _item('Device Variable Changed','',0));
            options = options.concat(this.getDeviceVariablesAsEventOptions(1));



            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: 'Create a new block group',
                style: 'width:500px;min-height:200px;',
                resizeable: true,
                delegate: {
                    onOk: function (dlg, data) {

                        var event = data[0].value;
                        thiz.addNewBlockGroup(event);
                    }
                },
                cis: [
                    utils.createCI('Event', types.ECIType.ENUMERATION, '',
                        {
                            group: 'Event',
                            delegate: null,
                            options:options,
                            value:'HTML',
                            widget:{
                                "class":"xide.form.FilterSelect"
                            }
                        }

                    )
                ],
                viewArgs:{
                    inserts: [{
                        query: '.dijitDialogPaneContent',
                        insert: '<div><span class="simpleText">Select the event for the new group:</span></div>',
                        place: 'first'
                    }]
                }
            });
            actionDialog.show();
        },
        createNewTab:function(){

            var thiz=this;
            utils.addWidget(ContentPane,{
                iconClass:'fa-magic',
                canSelect:false,
                isNewTab:true,
                onSelect:function(){
                    thiz.openNewGroupDialog();
                }
            },this,this.getGroupContainer(),true,'',null,false);

        },
        onGroupsCreated:function(){
            if(this.canAddGroups){
                this.createNewTab();
            }
        },
        getTabContainer:function(){
            return this.groupContainer;
        },
        //////////////////////////////////////////////////////////
        //
        //  Public API
        //
        onReloaded: function () {
            this.destroyWidgets();
            this.openItem(this._item);
        },
        /**
         * default entry when opening a file item through xfile
         * @param item
         */
        openItem: function (item) {

            var dfd = new Deferred();
            this._item = item;
            var thiz = this;

            var blockManager = this.getBlockManager();

            blockManager.load(item.mount, item.path).then(function (scope) {
                thiz.initWithScope(scope);
                dfd.resolve(thiz);
            });

            return dfd;

        },
        /**
         * Init with serialized string, forward to
         * @param content
         */
        initWithContent: function (content) {

            var data = null;
            try {
                data = utils.getJson(content);
            } catch (e) {
                console.error('invalid block data');
            }

            if (data) {
                this.initWithData(data);
            }

        },
        /**
         * Entry point when a blox scope is fully parsed
         * @param blockScope
         */
        initWithScope: function (blockScope) {

            this.blockScope = blockScope;


            var allBlockGroups = blockScope.allGroups(),
                thiz = this;

            if (allBlockGroups.indexOf('Variables') == -1) {
                allBlockGroups.push('Variables');
            }
            if (allBlockGroups.indexOf('Events') == -1) {
                allBlockGroups.push('Events');
            }
            if (allBlockGroups.indexOf('On Load') == -1) {
                allBlockGroups.push('On Load');
            }

            thiz.renderGroups(allBlockGroups, blockScope);

        },
        getScopeUserData: function () {
            return {
                owner: this
            };
        },
        initWithData: function (data) {

            if(this._debug) {
                console.log('init with data', data);
            }

            this.onLoaded();

            var scopeId = utils.createUUID(),
                blockInData = data,
                variableInData = data;

            //check structure
            if (lang.isArray(data)) {// a flat list of blocks

            } else if (lang.isObject(data)) {
                scopeId = data.scopeId || scopeId;
                blockInData = data.blocks || [];
                variableInData = data.variables || [];
            }

            var blockManager = this.getBlockManager();
            this.blockManager = blockManager;
            var scopeUserData = this.getScopeUserData();

            var blockScope = blockManager.getScope(scopeId, scopeUserData, true);
            var allBlocks = blockScope.blocksFromJson(blockInData);

            for (var i = 0; i < allBlocks.length; i++) {
                var obj = allBlocks[i];

                obj._lastRunSettings = {
                    force: false,
                    highlight: true
                }
            }

            var allVariables = blockScope.variablesFromJson(variableInData);
            blockManager.onBlocksReady(blockScope);
            if(this._debug) {
                console.log('   got blocks', allBlocks);
                console.log('   got variables', allVariables);
            }
            if (allBlocks) {
                return this.initWithScope(blockScope);
            }
            /**
             * a blocks file must be in that structure :
             */
        },
        //////////////////////////////////////////////////////////
        //
        //  utils
        //
        destroyWidgets: function () {
            utils.destroy(this.groupContainer);
            if (this.blockManager && this.blockScope) {
                this.blockManager.removeScope(this.blockScope.id);
            }
            this.groupContainer = null;
            this.blockManager = null;
        },
        destroy: function () {
            this.destroyWidgets();
            this.inherited(arguments);
        },
        /**
         * Get/Create a block manager implicit
         * @returns {xblox.manager.BlockManager}
         */
        getBlockManager: function () {

            if (!this.blockManager) {

                if (this.ctx.blockManager) {
                    return this.ctx.blockManager;
                }

                this.blockManager = factory.createInstance(this.blockManagerClass, {
                    ctx: this.ctx
                });
                if(this._debug) {
                    console.log('_createBlockManager ', this.blockManager);
                }
            }
            return this.blockManager;
        },
        //////////////////////////////////////////////////////////
        //
        //  UI - Factory
        //
        createGroupContainer: function () {

            var tabContainer = utils.addWidget(TabContainer, {
                tabStrip: true,
                tabPosition: "top",
                /*splitter: true,*/
                style: "min-width:450px;min-height:400px;height:inherit;padding:0px;"
            }, this, this.containerNode, true,'ui-widget-content');
            return tabContainer;
        },
        getGroupContainer: function () {
            if (this.groupContainer) {
                return this.groupContainer;
            }
            this.groupContainer = this.createGroupContainer();
            return this.groupContainer;
        },
        createGroupView: function (groupContainer, group,scope,closable,iconClass,select) {

            return utils.addWidget(ContentPane, {
                delegate: this,
                iconClass:iconClass || '',
                title: group,
                allowSplit:true,
                closable:closable,
                style: 'padding:0px',
                cssClass: 'blocksEditorPane',
                blockView: null,
                onSelect: function () {
                    if (this.blockView) {
                        //this.blockView.onShow();
                        this.blockView.resize();
                    }
                }
            }, this, groupContainer, true,null,null,select,null);
        },
        renderGroup:function(group,blockScope,gridBaseClass){

            blockScope = blockScope || this.blockScope;


            var groupContainer = this.getGroupContainer(),
                thiz = this;






            var title = group.replace(this.newGroupPrefix,'');
            if(utils.isNativeEvent(group)){
                title = title.replace('on','');
            }


            title = this.getContainerLabel(group.replace(this.newGroupPrefix,''));
            if(this._debug) {
                console.log('render group : ' + title);
            }

            var isVariableView = group === 'Variables';

            var contentPane = this.createGroupView(groupContainer, title, blockScope,!isVariableView,isVariableView ? ' fa-info-circle' : 'fa-bell',!isVariableView);

            var gridViewConstructurArgs = {};

            if (group === 'Variables') {

                gridViewConstructurArgs.newRootItemFunction = function () {
                    try {
                        var newItem = new Variable({
                            title: 'No-Title-Yet',
                            type: 13,
                            value: 'No Value',
                            enumType: 'VariableType',
                            save: false,
                            initialize: '',
                            group: 'Variables',
                            id: utils.createUUID(),
                            scope: blockScope
                        });
                    } catch (e) {
                        debugger;
                    }
                };

                gridViewConstructurArgs.onGridDataChanged = function (evt) {

                    var item = evt.item;
                    if (item) {
                        item[evt.field] = evt.newValue;
                    }
                    thiz.save();
                };
                gridViewConstructurArgs.showAllBlocks = false;
                gridViewConstructurArgs.newRootItemLabel = 'New Variable';
                gridViewConstructurArgs.newRootItemIcon = 'fa-code';
                gridViewConstructurArgs.storeField = 'variableStore';
                gridViewConstructurArgs.gridParams ={
                    cssClass: 'bloxGridView',
                    getColumns:function(){
                        return [
                            {
                                label: "Name",
                                field: "title",
                                sortable: true

                            },
                            {
                                label: "Value",
                                field: "value",
                                sortable: false
                            }
                        ]
                    }
                };
            }



            gridViewConstructurArgs.newRootItemGroup = group;

            var view = this.createGroupedBlockView(contentPane, group, blockScope, gridViewConstructurArgs,gridBaseClass);
            contentPane.blockView = view;
            view.parentContainer = contentPane;

            return view;


        },
        renderGroups: function (_array, blockScope) {

            var groupContainer = this.getGroupContainer();
            var lastChild = null, thiz = this;

            for (var i = 0; i < _array.length; i++) {

                var group =  _array[i];

                if(this.newGroupPrefix=='' && group.indexOf('__')!==-1){
                    continue;
                }

                try {

                    var title = group.replace(this.newGroupPrefix,'');

                    var groupBlocks = blockScope.getBlocks({
                        group: group
                    });

                    if (group !== 'Variables' && (!groupBlocks || !groupBlocks.length)) {//skip empty
                        continue;
                    }

                    this.renderGroup(group,blockScope);


                } catch (e) {
                    debugger;
                }
            }


            groupContainer.resize();
            setTimeout(function () {
                if (thiz.parentContainer) {
                    thiz.parentContainer.resize();
                }
            }, 500);

            this.onGroupsCreated();

        },
        onSave: function (groupedBlockView) {
            this.save();
        },
        //////////////////////////////////////////////////////////
        //
        //  Editor related
        //
        save: function () {

            if (this.blockScope) {

                var all = {
                    blocks: null,
                    variables: null
                };
                var blocks = this.blockScope.blocksToJson();
                try {
                    //test integrity
                    dojo.fromJson(JSON.stringify(blocks));
                } catch (e) {
                    console.error('invalid data');
                    return;
                }

                var _onSaved = function () {};

                var variables = this.blockScope.variablesToJson();
                try {
                    //test integrity
                    dojo.fromJson(JSON.stringify(variables));
                } catch (e) {
                    console.error('invalid data');
                    return;
                }
                all.blocks = blocks;
                all.variables = variables;
                this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
            }
        }

    });


    var _gridBase = {
        currentCIView:null,
        targetTop:null,
        lastSelectedTopTabTitle:'General',
        editBlock:function(item,changedCB,select){


            var selection = this.getSelection(),
                item=null;
            if(selection.length == 1){
                item = selection[0];
            }

            if(!item){
                return;
            }

            if(!item.canEdit()){
                return;
            }
            var title='Edit ',
                thiz=this;
            if(item.title){
                title+=item.title;
            }else if(item.name){
                title+=item.name;
            }
            try {
                var actionDialog = new BlockEditDialog({
                    item: item,
                    title: title,
                    style: 'width:400px',
                    resizeable: true,
                    delegate: {
                        onOk: function (dlg, data) {

                            //item.scope.expression.

                            if (changedCB) {
                                changedCB(item);
                            }

                            /**
                             * triggers to refresh block grid views
                             */
                            thiz.publish(types.EVENTS.ON_BLOCK_PROPERTY_CHANGED,{
                                item:item
                            });

                            /**
                             * update block tree view!
                             */

                            if(select!==false) {

                                thiz.onBlockSelected({
                                    item: item,
                                    owner: this.currentCIView
                                });
                            }

                        }
                    }
                });
            }catch(e){
                //debugger;
            }
            actionDialog.show();
            actionDialog.resize();
        },
        execute: function (block) {

            if (!block || !block.scope) {
                console.error('have no scope');
                return;
            }
            try {
                var result = block.scope.solveBlock(block, {
                    highlight: true,
                    force: true
                });
            } catch (e) {
                console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                console.error(printStackTrace().join('\n\n'));
            }
            return true;
        },
        move: function (dir) {

            var item = this.getSelection()[0];

            if (!item || !item.parentId) {

                console.log('cant move, no selection or parentId', item);

                return;

            }

            //parent.items = items.swap(item,upperItem);

            console.log('move ', item);


            try {
                item.move(item, dir);
                //this.onItemAction(true);
                console.log('move up to' + item.name);
            } catch (e) {
                debugger;
            }



        },
        runAction: function (action) {

            switch (action.command) {
                case 'Step/Run':
                {
                    return this.execute(this.getSelection()[0]);
                }
                case 'Step/Move Up':
                {
                    return this.move(-1);
                }
                case 'Step/Move Down':
                {
                    return this.move(1);
                }
                case 'Step/Edit':
                {
                    return this.editBlock();
                }
            }


        },
        getAddActions: function (item) {

            var thiz = this;


            var items = [];
            this.showAllBlocks = true;
            if (this.showAllBlocks || item) {

                var variables = this.blockScope.getVariables();
                var variableItems = [];
                /*
                 for (var i = 0; i < variables.length; i++) {
                 variableItems.push({
                 name: variables[i].title,
                 target: item,
                 iconClass: 'el-icon-compass',
                 proto: VariableAssignmentBlock,
                 item: variables[i],
                 ctrArgs: {
                 variable: variables[i].title,
                 scope: this.blockScope,
                 value: '0'
                 }
                 });
                 }*/
                items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
                items.push({
                    name: 'Set Variable',
                    target: item,
                    iconClass: 'el-icon-pencil-alt',
                    items: variableItems
                });

                //tell everyone
                factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                    items: items
                });
                return items;
            }


            /*
             if (!item) {

             if (this.newRootItemFunction) {
             items.push({
             name: this.newRootItemLabel,
             iconClass: this.newRootItemIcon,
             handler: function () {
             thiz.newRootItemFunction();
             thiz.onItemAction();
             }
             });
             return items;
             }
             }
             */

        },
        showProperties:function(item){


            var block = item,
                thiz = this;

            var right = this.getRightPanel();

            if(item == this._lastItem){
                return;
            }

            this._lastItem = item;

            var tabContainer = this.targetTop;


            if(!tabContainer) {

                tabContainer = utils.addWidget(TabContainer, {
                    delegate: this,
                    tabStrip: true,
                    tabPosition: "top",
                    attachParent: true,
                    style: "width:100%;height:100%;overflow-x:hidden;",
                    allowSplit: false
                }, null,right.containerNode, true);
                this.targetTop =tabContainer;
            }

            if (tabContainer.selectedChildWidget) {
                this.lastSelectedTopTabTitle = tabContainer.selectedChildWidget.title;
            }else{
                this.lastSelectedTopTabTitle = 'General';
            }



            _.each(tabContainer.getChildren(),function(tab){
                tabContainer.removeChild(tab);
            });

            if (this.currentCIView){
                this.currentCIView.empty();
            }




            // clear tab - container
            /*
            if (this.currentCIView && this.currentCIView.tabs) {

                if(this.currentCIView.tabContainer._destroyed){
                    this.currentCIView=null;
                }else {
                    this.currentCIView.empty();

                    try {
                        for (var i = 0; i < this.currentCIView.tabs.length; i++) {
                            this.targetTop.removeChild(this.currentCIView.tabs[i]);
                        }
                    } catch (e) {
                        //should not happen!
                        console.error('clear failed: ' + e, e);
                        console.trace();
                    }
                }
            }
            */

            if(!block.getFields){
                console.log('have no fields',block);
                return;
            }

            var cis = block.getFields();
            for (var i = 0; i < cis.length; i++) {
                cis[i].vertical = true;
            }







            var ciView = new CIViewMixin({
                tabContainer: this.targetTop,
                delegate: this,
                viewStyle: 'padding:0px;',
                autoSelectLast: true,
                item: block,
                source: this.callee,
                options: {
                    groupOrder: {
                        'General': 0,
                        'Advanced': 1,
                        'Description': 2
                    }
                },
                cis: cis
            });
            ciView.initWithCIS(cis);

            this.currentCIView = ciView;

            if(block.onFieldsRendered){
                block.onFieldsRendered(block,cis);
            }

            ciView._on('valueChanged',function(evt){
                console.log('ci value changed ',evt);
            });


            var containers = this.targetTop.getChildren();
            var descriptionView = null;
            for (var i = 0; i < containers.length; i++) {

                // @TODO : why is that not set?
                containers[i].parentContainer = this.targetTop;

                // track description container for re-rooting below
                if (containers[i].title === 'Description') {
                    descriptionView = containers[i];
                }

                if(this.targetTop.selectedChildWidget.title!==this.lastSelectedTopTabTitle) {
                    if (containers[i].title === this.lastSelectedTopTabTitle) {
                        this.targetTop.selectChild(containers[i]);
                    }
                }
            }


            this._docker.resize();



            /*
            if(this.targetBottom && this.targetBottom._destroyed){
                utils.destroy(this.targetBottom);
                this.targetBottom=null;
            }


            if(!this.targetBottom) {
                //  Re root description view into right bottom panel
                this.targetBottom = this.getRightBottomTarget(true, false);
            }
            */



/*
            for (var i = 0; i < cis.length; i++) {
                if(cis[i].select === true && cis[i].view){
                    this.targetTop.selectChild(cis[i].view);
                }
            }
            */



            /*
            if (descriptionView && this.targetBottom) {
                this.targetBottom.destroyDescendants(false);
                this.currentCIView.tabs.remove(descriptionView);
                descriptionView.parentContainer.removeChild(descriptionView);
                this.targetBottom.addChild(descriptionView);
                var main = this.getLayoutRightMain(false, false);
                main.resize();
            }
            */


            if (this.blockTreeView) {

            } else {
                /*
                if(this.renderNewTab) {

                    var main = this.ctx.getApplication().mainView;
                    if (main) {
                        main._openRight();
                    }
                    this.blockTreeView = this.createBlockTreeView(this.targetTop, 'New');
                }
                */
            }



        },
        startup:function(){

            this.inherited(arguments);

            this._on('selectionChanged',function(evt){

                var selection = evt.selection,
                    thiz = this;

                if(selection && selection[0]){
                    thiz.showProperties(selection[0]);
                }



            });

        }
    };

    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        ACTION = types.ACTION,
        root;


    function getFileActions(permissions) {


        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();


        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            var action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        var who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }


        var rootAction = 'Block/Insert';
        permissions.push(rootAction);
        addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
            dummy: true,
            onCreate: function (action) {
                 action.setVisibility(VISIBILITY.CONTEXT_MENU, {
                    label: 'Add'
                 });

            }
        }, null, null);

        permissions.push('Block/Insert Variable');


        addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
        }, null, null);
        /*
        permissions.push('Clipboard/Paste/New');
        addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
        }, null, null);*/


        var newBlockActions = this.getAddActions();
        var addActions = [];
        var levelName = '';



        function addItems(commandPrefix, items) {

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                levelName = item.name;


                var path = commandPrefix + '/' + levelName;
                var isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }



        //console.clear();

        addItems(rootAction, newBlockActions);


        //console.dir(newBlockActions);


        /*
         for (var i = 0; i < newBlockActions.length; i++) {


         //top level items
         var item = newBlockActions[i];

         if(_.isEmpty(item.items)){
         continue;
         }

         levelName = item.name;

         var path = rootAction + '/' + levelName;

         permissions.push(path);

         addAction(levelName,path,item.iconClass,null,'Home','Insert','item|view',null,null,{
         dummy:true
         },null,null);

         //now the items



         addItems(path,item.items);


         }*/


        console.log('new items', newBlockActions);

        //return result;

        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            var canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);
            return !canMove;

        }


        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */
        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                /*
                action.setVisibility(types.ACTION_VISIBILITY_ALL, {
                    label: ''
                });*/
            }
        }, null, isItem);

        permissions.push('Step/Run/From here');

        /**
         * run
         */
        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                /*
                action.setVisibility(types.ACTION_VISIBILITY_ALL, {
                    label: ''
                });
                */
            }
        }, null, isItem);


        /**
         * move
         */
        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['ctrl up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);

        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['ctrl down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);




        permissions.push('Step/Edit');

        addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        return result;

    }

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [],
                    "id": "root2",
                    "name": "On Key",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "group": "My Group"
                },
                {

                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Run Script",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "order": 0

                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,

                    "order": 0
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "order": 0
                }
            ],
            "variables": []
        };

        return blockManager.toScope(data);
    }
    if (ctx) {




        var blockManager = ctx.getBlockManager();
        var doTest = true;

        var ribbon = ctx.mainView.getToolbar();

        function createGridClass() {
            return BlockGrid;
        }



        var blockScope = createScope('docs');


        var mainView = ctx.mainView;

        var docker = mainView.getDocker();


        if (mainView) {


            if (_lastGrid) {

                docker.removePanel(_lastGrid);
            }


            var parent = docker.addTab(null, {
                title: 'blocks - editor',
                icon: 'fa-folder'
            });

            window._lastGrid = parent;


            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon;


            var store = blockScope.blockStore;
            var _gridClass = createGridClass();



            var editor = utils.addWidget(editorClass,{
                gridClass:_gridClass,
                ctx:ctx
            },null,parent,true);



            editor.initWithScope(blockScope);





            /*
            grid = new _gridClass({

                blockScope: blockScope,
                blockGroup: 'click',
                shouldShowAction: function (action) {
                    return true;
                },
                collection: store.filter({
                    group: "click"
                }),
                tabOrder: {
                    'Home': 100,
                    'View': 50
                },
                groupOrder: {


                    'Clipboard': 110,
                    'File': 100,
                    'Step': 80,
                    'Open': 70,
                    'Organize': 60,
                    'Insert': 10,
                    'Select': 0
                },
                showHeader: true,
                options: utils.clone(types.DEFAULT_GRID_OPTIONS),
                _parent: parent,
                columns: [
                    {
                        renderExpando: true, label: "Name", field: "name", sortable: false, editor: BlockGridRowEditor
                    }
                ]
            }, parent.containerNode);
            */

            var gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                shouldShowAction: function (action) {
                    return true;
                },
                collection: store.filter({
                    group: "click"
                }),
                tabOrder: {
                    'Home': 100,
                    'View': 50
                },
                groupOrder: {


                    'Clipboard': 110,
                    'File': 100,
                    'Step': 80,
                    'Open': 70,
                    'Organize': 60,
                    'Insert': 10,
                    'Select': 0
                },
                options: utils.clone(types.DEFAULT_GRID_OPTIONS),
                columns: [
                    {
                        renderExpando: true, label: "Name", field: "name", sortable: false, editor: BlockGridRowEditor
                    }
                ]
            };

            //grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);

            //var grid2 = utils.addWidget(_gridClass,gridArgs,null,right,true);

/*

            grid.startup();
           // grid2.startup();

            grid.select(['root'], null, true, {
                focus: true
            });
            */
            /*
            var _actions = [
                //ACTION.EDIT,
                ACTION.RENAME,
                ACTION.RELOAD,
                ACTION.DELETE,
                //ACTION.NEW_FILE,
                //ACTION.NEW_DIRECTORY,
                ACTION.CLIPBOARD,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.PREVIEW,
                'Step/Run',
                'Step/Move Up',
                'Step/Move Down'

            ];

            var blocks = blockScope.allBlocks();
            var root = blocks[0];*/


            /*
            var _defaultActions = DefaultActions.getDefaultActions(_actions, grid);
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [_actions]));
            grid.addActions(_defaultActions);*/

            var grid = editor;


            //var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();




            if (!toolbar) {
                window._lastRibbon = ribbon = toolbar = utils.addWidget(Ribbon, {
                    store: actionStore,
                    flat: true,
                    currentActionEmitter: grid
                }, this, mainView.layoutTop, true);

            } else {
                toolbar.addActionEmitter(grid);

                toolbar.setActionEmitter(grid);
            }


            setTimeout(function () {
                mainView.resize();
                //grid.resize();

            }, 1000);


            function test() {

                return;

            }

            function test2() {

            }

            setTimeout(function () {
                test();
            }, 4000);
            setTimeout(function () {
                test2();
            }, 2000);

        }
    }

    return Grid;

});