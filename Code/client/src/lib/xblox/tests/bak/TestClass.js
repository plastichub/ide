/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/dom-class',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/ThumbRenderer',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    "xblox/widgets/BlockGridRowEditor",

    'dgrid/Editor',


    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',

    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',

    'xide/factory',


    'dijit/Menu',

    'xide/data/Reference',

    'dijit/form/DropDownButton',
    'dijit/MenuItem',

    'xide/docker/Docker',


    "xide/views/CIViewMixin",

    'xide/layout/TabContainer',


    "dojo/has!host-browser?xblox/views/BlockEditDialog",

    'xblox/views/BlockGrid',

    'xgrid/DnD',
    'xblox/views/BlocksGridDndSource',
    'xblox/widgets/DojoDndMixin',


    'xide/registry',

    'dojo/topic'


], function (declare, domClass,types,
             utils, ListRenderer, TreeRenderer, ThumbRenderer,
             _ActionMixin,
             Grid, MultiRenderer, RadioButton, Ribbon, Registry, DefaultActions, Action, BlockGridRowEditor,
             Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Menu,Reference,DropDownButton,
             MenuItem,Docker,CIViewMixin,TabContainer,

             BlockEditDialog,
             BlockGrid,
             Dnd,BlocksGridDndSource,DojoDndMixin,

             registry,topic
    ) {


    function patchToolbar(ribbon) {

        var _mixin = {

        }

        utils.mixin(ribbon,_mixin);
    }
    /**
     *
     */
    var _layoutMixin = {
        _docker:null,
        _parent:null,
        __right:null,
        getDocker:function(container){

            if(!this._docker){

                var _dst = container || this._domNode.parentNode,
                    thiz = this;

                thiz._docker = Docker.createDefault(_dst);
                thiz._oldParent = thiz._parent;

                var parent = thiz._docker.addPanel('DefaultFixed', types.DOCKER.TOP, null, {
                    w: '100%',
                    title:'  '
                });

                dojo.place(thiz._domNode,parent.containerNode);

                thiz._docker.$container.css('top',0);
                thiz._parent = parent;

                parent._parent.showTitlebar(false);


/*
                var right = this._docker.addPanel('Collapsible', types.DOCKER.RIGHT, parent, {
                    w: '20%',
                    title:'Properties'
                });

                parent._parent.showTitlebar(false);*/





                //parent._parent.$center.css('top',0);

            }

            return thiz._docker;
        },
        getRightPanel:function(){

            if(this.__right){
                return this.__right;
            }

            var docker = this.getDocker();


            var right = docker.addPanel('Collapsible', types.DOCKER.RIGHT, this._parent, {
                w: '300px',
                title:'  '
            });



            right._parent.showTitlebar(false);

            var splitter = right.getSplitter();

            splitter.pos(0.6);


            this.__right = right;

            return right;
        }
    }


    var _statusMixin = {
    }

    /**
     * Dnd impl.
     */
    var _dndMixin = {

    }




    var DojoDndMixin  = declare("xblox.widgets.DojoDndMixin",null,{
        dropEvent:"/dnd/drop",
        dragEvent:"/dnd/start",
        overEvent:"/dnd/source/over",
        _eDrop:false,
        isDragging:false,
        didInit:false,
        /***
         * Pre-Process DND Events
         * @param node
         * @param targetArea
         * @param indexChild
         */
        _calcNewItemList:function(items){
            var res = [];
            if(items){

                for(var i=0 ; i<items.length ; i++){
                    var widget =registry.getEnclosingWidget(items[i].item.node);
                    if(widget){
                        widget.item.dndCurrentIndex=i;
                        res.push(widget);
                    }
                }
            }
            return res;
        },
        _onOver:function(node, source){

            if(this.isDragging /*this.onOver && node*/){
                var widget=null;
                if(source){
                    widget = registry.getEnclosingWidget(source[0]);
                }
                console.log('on over : ' );
                /*this.onOver(widget,node);*/
            }

        },
        _onDrag:function(node, source){

            this.isDragging=true;
            /*
             if(this.onDrag){
             if(source && source[0]){
             var widget = registry.getEnclosingWidget(source[0]);
             this.onDrag(widget,node);
             }
             }
             */
        },
        checkAcceptance:function(sources,nodes){

            return false;
            if(!sources||!nodes){
                return;
            }
            this.setupDND();

            var node = nodes[0];

            var row = this.grid.row(node);
            if(!row || !row.data){
                return;
            }
            var item = row.data;

            //console.log('acce : ' + item.name);

            return true;
        },
        onDrop: function(source, nodes, copy,target){

            if(!source||!nodes){
                return;
            }
            this.isDragging=false;
            var node = nodes[0];
            var item = null;
            var dstItem = null;
            var isTree = false;
            var didNonTree=false;
            var didTree=false;
            var grid = this.grid;
            var itemToFocus;

            if(source.tree && source.anchor && source.anchor.item && target){

                item    = source.anchor.item;
                isTree  = true;

                var dstNode = target.current;
                if(!dstNode){

                    console.warn('have dstNode in target.current');

                    if (grid.onDrop) {
                        console.log('drop on nothing');
                        grid.onDrop(item, {
                            parentId:null
                        }, false, grid, source.targetState, false);
                    }

                    return;
                }
                var dstRow = grid.row(dstNode);
                if(!dstRow){
                    console.warn('have now row');
                    return;
                }
                dstItem = dstRow.data;
                if(!dstItem){
                    console.warn('have no dstItem');
                    return;
                }

                try {

                    if (grid.onDrop) {
                        itemToFocus = item;
                        grid.onDrop(item, dstItem, target.before, grid, source.targetState, target.hover);
                        didTree = true;
                    }
                }catch(e){
                    debugger;
                }



            }else{






                var row=grid.row(node);

                if(!row || !row.data){
                    return;
                }
                item = row.data;



                var dstNode = source.current;
                if(!dstNode){
                    console.warn('have no dstNode in source.current, unparent');
                    var parent = item.getParent();

                    if(parent) {
                        item.unparent(grid.blockGroup, false);
                    }
                    grid.refreshAll([item]);

                    return;
                }
                var dstRow = this.grid.row(dstNode);
                if(!dstRow){
                    console.warn('have now row');
                    return;
                }
                dstItem = dstRow.data;
                if(!dstItem){
                    console.warn('have no dstItem');
                    return;
                }

                try {
                    if (grid.onDrop) {
                        itemToFocus = item;
                        grid.onDrop(item, dstItem, source.before, grid, source.sourceState, source.hover);
                        didNonTree = true;
                    }
                }catch(e){
                    console.error('error grid:onDrop:',e);
                    console.trace();
                }
            }

            if(didNonTree==false && didTree==false){
                console.dir(arguments);
                console.log('something wrong')
            }else{
                console.dir(arguments);
                console.log('dropping : ' + item.name  + ' into ' + dstItem.name + ' before = ' + source.before);
                console.log('didNonTree ' + didNonTree + ' didTree ' + didTree);
                grid.refreshAll([itemToFocus]);

            }
        },
        setupDND:function(){


            var thiz = this;

            if(this.didInit){
                return;
            }
            this.didInit=true;

            topic.subscribe(this.dragEvent, function(node, targetArea, indexChild){
                thiz._onDrag(node, targetArea, indexChild);
            });
            if(this.overEvent){
                topic.subscribe(this.overEvent, function(node, targetArea, indexChild){
                    thiz._onOver(node, targetArea, indexChild);
                });
            }
            topic.subscribe(this.dropEvent, function(source, nodes, copy, target){
                thiz._onDrop(source, nodes, copy, target);
            });
        }
    });

    var SharedDndGridSource = declare([BlocksGridDndSource, DojoDndMixin], {
        /*
        blockScope: this.blockScope,
        delegate: this.delegate,*/
        onDndDrop: function (source, nodes, copy, target) {
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, "Hover");
            }
            if (this == target) {
                // this one is for us => move nodes!
                this.onDrop(source, nodes, copy, target);
            }
            this.onDndCancel();
        }
    });


    function getFileActions(permissions) {



        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();


        return [];

        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            var action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        var who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }

        /*
         var rootAction = 'Block/Insert';
         permissions.push(rootAction);
         addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         dummy: true,
         onCreate: function (action) {
         action.setVisibility(VISIBILITY.CONTEXT_MENU, {
         label: 'Add'
         });

         }
         }, null, null);
         permissions.push('Block/Insert Variable');


         addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         }, null, null);
         */

        /*
         permissions.push('Clipboard/Paste/New');
         addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
         }, null, null);*/


        var newBlockActions = this.getAddActions();
        var addActions = [];
        var levelName = '';


        function addItems(commandPrefix, items) {

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                levelName = item.name;


                var path = commandPrefix + '/' + levelName;
                var isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }
        //console.clear();
        //addItems(rootAction, newBlockActions);
        //return result;


        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            var canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

            return !canMove;

        }


        function canParent(selection, reference, visibility) {

            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            if(!item){
                console.warn('bad item',selection);
                return false;
            }

            if(this.command === 'Step/Move Left'){
                return !item.getParent();
            }else{
                return item.getParent();
            }
            /*
             var canMove = item.canMove(item, this.command === 'Step/Move Left' ? -1 : 1);
             return !canMove;*/

            return true;

        }

        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */

        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    widgetArgs:{
                        label: ' ',
                        style:'font-size:25px!important;'
                    }
                });

            }
        }, null, isItem);
        permissions.push('Step/Run/From here');

        /**
         * run
         */

        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {

            onCreate: function (action) {

            }
        }, null, isItem);



        /**
         * move
         */

        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['alt up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);


        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['alt down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);
        /*


         permissions.push('Step/Edit');
         addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
         */
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        permissions.push('Step/Move Left');
        permissions.push('Step/Move Right');

        addAction('Move Left', 'Step/Move Left', 'fa-arrow-left', ['alt left'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canParent);

        addAction('Move Right', 'Step/Move Right', 'fa-arrow-right', ['alt right'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });


            }
        }, null, canParent);


        return result;

    }

    function doPost(view){

        var right  = view.getRightPanel();
        var splitter = right.getSplitter();
        //var ori = splitter.orientation();
        //splitter.orientation(false);
        //spl

        //splitter.collapse();
        //splitter.expand();
        splitter.minimize = function(){

            this._isToggledMin = true;

            //save pos
            if(this._isToggledMin){
                this._savedPosMin = this.pos();
            }

            var togglePosValue = this._isToggledMin ? 1 : 0;

            if(togglePosValue==1){
                //togglePosValue = this._savedPosMin;
            }


            //console.log('toggle ' + togglePosValue + ' cvalue ' + this.pos());


            this.pos(togglePosValue);

        }
        //splitter.minimize();

    }

    function clipboardTest(){

        this.select(['root2'],null,true,{
            focus:true
        });

        var actionStore = this.getActionStore();

        var copy = actionStore.getSync('Clipboard/Copy');
        var paste = actionStore.getSync('Clipboard/Paste');

        console.clear();


        this.runAction(copy);
        this.runAction(paste);

        //this.refresh();


    }

    function unparent(){

        this.select(['sub0'],null,true,{
            focus:true
        });
        this.runAction('Step/Move Left');
        this.refresh();
    }

    function reparent(){

        this.select(['root3'],null,true,{
            focus:true
        });
        this.runAction('Step/Move Right');
        this.refresh();
    }


    function expand(){


        this.select(['root3'],null,true,{
            focus:true
        });

        //clipboardTest.apply(this);

        //reparent.apply(this);


        return;



        //var row = grid.row('root');
        //var _t = this._normalize('root');
        //this.expand(_t);
        //var _expanded = this.isExpanded(_t);
        //debugger;
        //this.isRendered('root');
        var store = this.collection;
        //var item = store.getSync('sub0');
        //this._expandTo(item);

        var root = this.collection.getSync('click');


        this.select(['root2','root3'],null,true,{
            focus:true
        });


        var actionStore = this.getActionStore();

        var moveLeft = actionStore.getSync('Step/Move Left');


        var moveUp = actionStore.getSync('Step/Move Up');

        var moveDown = actionStore.getSync('Step/Move Down');

        var items = this.collection.storage.fullData;
        //console.log('before move',items);
        //this.printRootOrder();
        this.runAction(moveUp);
        //console.log('after move',this.collection.storage.fullData);
        //this._place('root','root2','below');
        //this.printRootOrder();


        var thiz = this;

        setTimeout(function(){

            //thiz.refresh();
            //this.runAction(moveDown);
            //thiz.refreshRoot();
            thiz.printRootOrder();
        },1500);

        //this.runAction(moveLeft);
    }


    var _gridBase = {
        _place:function(dst,ref,direction,items){


            var store = this.collection;

            ref = _.isString(ref) ? store.getSync(ref) : ref;
            dst = _.isString(dst) ? store.getSync(dst) : dst;

            items = items || store.storage.fullData;

            direction = direction == -1 ? 0 : 1;

            function index(what){
                for (var j = 0; j < items.length; j++) {
                    if(what.id===items[j].id){
                        return j;
                    }
                }
            };

            function compare(left,right){
                return index(left) - index(right);
            }

            items.remove(dst);
            if(direction==-1){
                direction=0;
            }
            items.splice( Math.max(index(ref) + direction,0), 0, dst);

            store._reindex();
        },
        __reParentBlock:function(dir){


            var item = this.getSelection()[0];

            if (!item /*|| !item.parentId*/) {
                console.log('cant move, no selection or parentId', item);
                return;
            }
            //console.log('reparenting block',item);

            var thiz = this;
            //thiz._preserveSelection();

            var store = item._store;

            item._next = function(dir){

                var _dstIndex = 0;
                var step = 1;

                var items = this._store.storage.fullData;

                //find item
                function _next(item,items,dir){

                    var cIndex = item.indexOf(items, item);
                    var upperItem = items[cIndex + (dir * step)];
                    if(upperItem){
                        if(!upperItem.parentId && upperItem.group && upperItem.group===item.group){

                            _dstIndex = cIndex + (dir * step);
                            return upperItem;
                        }else{
                            step++;
                            return _next(item,items,dir);
                        }
                    }
                    return null;
                }



                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var next = _next(item,items,dir);
                if (!next) {
                    return false;
                }
                items[_dstIndex]=item;
                items[cIndex] = next;
                //store._reindex();
            };

            item.__unparent = function () {

                var item = this;

                if (!item) {
                    return false;
                }
                var parent = item.getParent();

                var items = null;

                var newParent = null;

                if(parent){
                    newParent = parent.getParent();
                }



                if(newParent){

                    item.group = newParent.group;

                    if(newParent.id == thiz.blockGroup){
                        item.group = newParent.id;
                    }
                    item.parentId = newParent.id;
                }else{

                    //var items = store.storage.fullData;

                    //console.log('next down - parent ',parent.next(items,1));

                    if (parent && parent.removeBlock) {
                        parent.removeBlock(item,false);
                    }
                    item.group = thiz.blockGroup;
                    item.parentId = null;
                    item.parent = null;
                }

                /*
                var scope = thiz.blockScope;
                var t = item._store.getSync('sub0');*/

                item._place(null,-1,null);
                item._place(null,-1,null);


                //item._place(null,-1,null);

                //console.log('new parent : ' + thiz.blockGroup ,scope);
            };

            item.reparent = function () {

                var item = this;

                if (!item) {
                    return false;
                }
                var parent = item.getParent();

                if(parent){
                }else{

                    var _next = item.next(null,1) || item.next(null,-1);
                    if(_next){
                        item.group=null;
                        _next._add(item);
                        thiz.refreshRoot();
                    }
                }
            }

            if(dir==-1) {
                item.unparent(thiz.blockGroup);
            }else{
                item.reparent();
            }
            thiz.deselectAll();
            this.refreshRoot();
            this.refresh();
            setTimeout(function(){
                thiz.select(item,null,true,{
                    focus:true
                });
            },20);


        },
        /**
         *
         * @param mixed
         * @param toRow {object} preserve super
         * @param select {boolean} preserve super
         * @param options {object}
         * @param options.focus {boolean}
         * @param options.silent {boolean}
         * @param options.append {boolean}
         */
        /**
         * @param menuItem
         */
        addItem: function (item,action) {


            if (!item) {
                return;
            }
            var target = this.getSelection()[0],
                thiz = this;
            var proto = item.proto;
            var ctorArgs = item.ctrArgs;
            var where = null;
            var newBlock = null;


            //cache problem:
            if(!target){
                var _item = this.getSelection()[0];
                if(_item){
                    target=_item;
                }
            }


            if(ctorArgs) {
                if (ctorArgs.id) {
                    console.error('addItem:: new block constructor args had id!!!');
                    ctorArgs.id = utils.createUUID();
                }
                if (ctorArgs.parent) {
                    ctorArgs.parent = null;
                }
                if (ctorArgs.parentId) {
                    ctorArgs.parentId = null;
                }
                if (ctorArgs.items) {
                    console.error('addItem:: new block constructor args had items!!!');
                    ctorArgs.items = [];
                }
            }

            if (target && proto && ctorArgs) {

                if (target.owner && target.dstField) {
                    where = target.dstField;
                    target = target.owner;
                }

                ctorArgs['parentId'] = target.id;
                ctorArgs['group'] = null;
                ctorArgs['parent'] = target;//should be obselete

                newBlock = target.add(proto, ctorArgs, where);
                newBlock.parent = target;

            } else if (!target && proto && ctorArgs) {

                if(ctorArgs.group==="No Group" && this.newRootItemGroup){
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.blockGroup) {
                    ctorArgs.group = this.blockGroup;
                }

                if (ctorArgs.group && this.newRootItemGroup && ctorArgs.group !==this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }

                newBlock = factory.createBlock(proto, ctorArgs);//root block
            }

            if (newBlock && newBlock.postCreate) {
                newBlock.postCreate();
            }
            //this.onItemAction();

            if(!newBlock){
                console.error('didnt create block');
                return;
            }


            var store = this.getStore(target);

            var storeItem = store.getSync(newBlock[store.idProperty]);
            if(!storeItem){
                console.error('new block not in store');
                store.putSync(newBlock);
            }

            try {
                scope._emit(types.EVENTS.ON_ITEM_ADDED, {
                    item: newBlock,
                    owner: scope
                });
            }catch(e){

                console.error('error emitting',e);
                debugger;
            }

            this._itemChanged('added',newBlock,store);

        },
        refreshItem:function(item){},
        _itemChanged:function(type,item,store){

            store = store || this.getStore(item);

            var thiz = this;

            function _refreshParent(item,silent){

                var parent  = item.getParent();
                if(parent) {
                    var args = {
                        target: parent
                    };
                    if(silent){
                        this._muteSelectionEvents=true;
                    }
                    store.emit('update', args);
                    if(silent){
                        this._muteSelectionEvents=false;
                    }
                }else{
                    thiz.refresh();
                }
            }




            function select(item){
                thiz.select(item,null,true,{
                    focus:true
                });
            }


            switch (type){

                case 'added':{
                    //_refreshParent(item);
                    this.refresh();
                    select(item);
                    break;
                }


                case 'moved':{
                    //_refreshParent(item,true);
                    //this.refresh();
                    //select(item);
                    break;
                }



                case 'deleted':{

                    var parent  = item.getParent();
                    if(parent) {

                        var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;


                        var _container = parent.getContainer();
                        if(_container){
                            _.each(_container,function(child){
                                if(child.id == item.id) {
                                    _container.remove(child);
                                }
                            });
                        }
                        this.refresh();

                        setTimeout(function(){
                            if(_prev) {
                                select(_prev);
                            }
                        },1);


                    }

                    break;
                }
            }

        },
        startup:function(){

            this.inherited(arguments);


            this._on('selectionChanged',function(evt){

                var selection = evt.selection;

                //console.log('selection changed',selection);

                return;

                if(selection[0]){

                    selection[0].canParent = function (item, dir) {

                        try {
                            item = item || this;

                            if (!item) {
                                return false;
                            }
                            var parent = item.getParent();
                            var items = null;

                            if (parent) {
                                items = parent[parent._getContainer(item)];
                                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                                    return false;
                                }
                                var cIndex = this.indexOf(items, item);
                                if (cIndex + (dir) < 0) {
                                    return false;
                                }
                                var upperItem = items[cIndex + (dir)];
                                if (!upperItem) {
                                    return false;
                                }
                            } else {
                                var store = this._store;
                                items = store.storage.fullData;
                                var _next = this.next(items, dir);
                                return _next != null;
                            }

                        } catch (e) {
                            debugger;
                        }
                        return true;
                    }


                    


                    return;
                    selection[0].canMove = function (item, dir) {

                        try {
                            item = item || this;

                            if (!item) {
                                return false;
                            }
                            var parent = item.getParent();
                            var items = null;
                            if (parent) {
                                items = parent[parent._getContainer(item)];
                                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                                    return false;
                                }
                                var cIndex = this.indexOf(items, item);
                                if (cIndex + (dir) < 0) {
                                    return false;
                                }
                                var upperItem = items[cIndex + (dir)];
                                if (!upperItem) {
                                    return false;
                                }
                            } else {

                                var store = this._store;
                                items = store.storage.fullData;
                                var _next = this.next(items, dir);
                                return _next != null;
                            }

                        } catch (e) {
                            debugger;
                        }
                        return true;
                    }

                }

            })
        }
    };

    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    function fixScope(scope){

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function(source,target,before,add){




            console.log('scope::move, add: ' +add,arguments);

            if(!add){
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if(add){

                //remove it from the source parent and re-parent the source
                if(target.canAdd && target.canAdd()){

                    var sourceParent = this.getBlockById(source.parentId);
                    if(sourceParent){
                        sourceParent.removeBlock(source,false);
                    }
                    target.add(source,null,null);
                    return;
                }else{
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if(!target.parentId && add==false){

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if(sourceParent && sourceParent.removeBlock){
                    sourceParent.removeBlock(source,false);
                    source.parentId=null;
                    source.group=target.group;
                }

                var itemsToBeMoved=[];
                var groupItems = this.getBlocks({
                    group:target.group
                });

                var rootLevelIndex=[];
                var store = this.getBlockStore();

                var sourceIndex = store.storage.index[source.id];
                var targetIndex = store.storage.index[target.id];
                for(var i = 0; i<groupItems.length;i++){

                    var item = groupItems[i];
                    //keep all root-level items

                    if( groupItems[i].parentId==null && //must be root
                        groupItems[i]!=source// cant be source
                    ){

                        var itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if(add){
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if(before){
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if(!before){
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            }else if( !source.parentId && target.parentId && add==false){
                source.group = target.group;
                if(target){

                }

                //we move from root to into root item
            }else if( !source.parentId && !target.parentId && add){

                console.error('we are adding an item into root root item');
                if(target.canAdd && target.canAdd()){
                    source.group=null;
                    target.add(source,null,null);
                }
                return true;

                // we move within the same parent
            }else if( source.parentId && target.parentId && add==false && source.parentId === target.parentId){
                console.error('we move within the same parents');
                var parent = this.getBlockById(source.parentId);
                if(!parent){
                    console.error('     couldnt find parent ');
                    return false;
                }

                var maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items,source);
                var cIndexTarget = source.indexOf(items,target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    parent.move(source,direction);
                }
                return true;

                // we move within the different parents
            }else if( source.parentId && target.parentId && add==false && source.parentId !== target.parentId){                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if(!sourceParent){
                    console.error('     couldnt find source parent ');
                    return false;
                }

                var targetParent = this.getBlockById(target.parentId);
                if(!targetParent){
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if(sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()){
                    sourceParent.removeBlock(source,false);
                    targetParent.add(source,null,null);
                }else{
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if(items==null){
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items,source);
                var cIndexTarget = targetParent.indexOf(items,target);
                if(!cIndexSource || !cIndexTarget){
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    targetParent.move(source,direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        var topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId:null
        });


        var grouped = _.groupBy(blocks,function(block){
            return block.group;
        });

        function createDummyBlock(id,scope){

            var block = {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": null,
                    "id": id,
                    "items": [

                    ],
                    "name": id,
                    "method": "----group block ----",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": false,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"

                };

            return scope.blockFromJson(block);

        }

        for(var group in grouped){

            var groupBlock = createDummyBlock(group,scope);
            var blocks = grouped[group];
            _.each(blocks,function(block){
                groupBlock['items'].push(block);



                if(!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }

        console.clear();
        var root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }



    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }

    if (ctx) {


        var blockManager = ctx.getBlockManager();


        function createGridClass() {

            var renderers = [TreeRenderer];
            //, ThumbRenderer, TreeRenderer

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);


            var _gridClass = Grid.createGridClass('driverTreeView',
                {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT:{
                        CLASS:declare('splitMixin',null,_layoutMixin)
                    },
                    DND:{
                        CLASS:Dnd//declare('splitMixin',Dnd,_dndMixin)
                    }

                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );



            return declare('gridFinal', BlockGrid, _gridBase);

            //return BlockGrid;
        }

        var blockScope = createScope('docs');


        var mainView = ctx.mainView;

        var docker = mainView.getDocker();


        if (mainView) {


            if (_lastGrid) {

                docker.removePanel(_lastGrid);
            }

            var parent = docker.addTab(null, {
                title: 'blox',
                icon: 'fa-folder'
            });
            window._lastGrid = parent;




            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon;


            var store = blockScope.blockStore;


            var _gridClass = createGridClass();




            var gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                collection: store.filter({
                    group: "click"
                }),
                dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                dndParams: {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                }
            };



            grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);


            var blocks = blockScope.allBlocks();

            var root = blocks[0];


            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();

            var _defaultActions = [];

            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [grid.permissions]));

            grid.addActions(_defaultActions);



            if (!toolbar) {
                window._lastRibbon = ribbon = toolbar = utils.addWidget(Ribbon, {
                    store: actionStore,
                    flat: true,
                    currentActionEmitter: grid
                }, this, mainView.layoutTop, true);

            } else {
                toolbar.addActionEmitter(grid);
                toolbar.setActionEmitter(grid);
            }

            doPost(grid);


            setTimeout(function () {
                mainView.resize();
                grid.resize();

            }, 1000);


            function test() {

                expand.apply(grid);

                return;

            }

            function test2() {

                return;

                /*var isToolbared = grid.hasFeature('TOOLBAR');
                 console.warn('has Toolbar ');*/


                /*
                 var nameProperty = item.property('label');

                 var urlProperty = item.property('url');


                 nameProperty.observe(function () {
                 console.log('horray!', arguments);
                 });

                 urlProperty.observe(function () {
                 console.log('horray url', arguments);
                 });

                 item.set('label', 'new label');
                 item.set('url', null);

                 //store.notify(item,'id3');
                 store.emit('update', {target: item});
                 */


                /*grid.refresh();*/
                //console.log('item ', item);

                /*
                 grid.select(store.getSync('id99'), null, true, {
                 append: true,
                 focus: true,
                 silent:true
                 });*/

                var item99 = store.getSync('id99');
                var item98 = store.getSync('id98');

                grid.select([item99, item98], null, true, {
                    append: true,
                    focus: true,
                    silent: false
                });


                //console.log('is selected ' + grid.isSelected(item98));

                //console.dir(grid.getRows(true));

                /*console.dir(grid.getPrevious(item99, false, true));*/
                /*
                 console.dir(grid.getSelection(function(item){
                 return item.id!='id99';
                 }));*/


                //console.log(grid.getFocused());

                /*store.removeSync('id3');*/
            }

            setTimeout(function () {
                test();
            }, 1000);


            setTimeout(function () {
                test2();
            }, 2000);

        }
    }

    return Grid;

});