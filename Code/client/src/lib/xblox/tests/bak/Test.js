/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xide/widgets/Ribbon',
    'xaction/DefaultActions',
    "xblox/widgets/BlockGridRowEditor",

    'dgrid/Editor',


    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',

    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',

    'xide/factory',


    'dijit/Menu',

    'xide/data/Reference',




    'xide/docker/Docker',


    "xide/views/CIViewMixin",

    'xide/layout/TabContainer',


    "dojo/has!host-browser?xblox/views/BlockEditDialog"


], function (declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, Ribbon, DefaultActions, BlockGridRowEditor,
             Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Menu,Reference,
             Docker,CIViewMixin,TabContainer,

             BlockEditDialog) {

    function patchToolbar(ribbon) {

        return ;

        ribbon._renderAction = function (action, where, widgetClass, showLabel) {

            var thiz = this;


            //if (action && action.show !== false) {

            var actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};

            //case when visibility is not an object, upgrade visibility to an object
            if (!_.isObject(actionVisibility) && action.setVisibility) {
                actionVisibility = {};

                action.setVisibility(this.visibility, actionVisibility);

            }

            //pre-rendered widget
            if (actionVisibility._widget) {

                utils.addChild(where, actionVisibility._widget);

                domClass.add(actionVisibility._widget.domNode, utils.replaceAll('/', '-', action.command));

                actionVisibility.widget = actionVisibility._widget;//for clean up

                thiz._onDidCreateWidget(action, actionVisibility, actionVisibility._widget);

                return;
            }

            //render only a widget for this action if not already done and the actions isn't a widget itself=dirty yet
            if (!actionVisibility.widget && !action.domNode) {

                if (!action.label) {//huh, really? @TODO: get rid of label needs in action:render
                } else {
                    actionVisibility.widget = thiz._addMenuItem(action, where, actionVisibility.widgetClass || widgetClass || thiz.widgetClass, showLabel);
                    actionVisibility.widget.visibility = thiz.visibility;
                }
            } else {
                console.log('have already rendered action',action);
            }

            return actionVisibility.widget;


        }


        /**
         *
         * @param action
         * @param where
         * @param widgetClass
         * @param showLabel
         * @returns {*}
         */
        ribbon.renderAction = function (action, where, widgetClass, showLabel) {


            /**
             * Collect variables
             */

            var thiz = this,
                _items = action.getChildren ? action.getChildren() : action.items,
                _hasItems = _items && _items.length,
                isContainer = _hasItems,
                _expand = thiz.getVisibilityField(action, 'expand') == true,
                _forceSub = action.forceSubs,

                widget = thiz.getVisibilityField(action, 'widget'),
                parentAction = action.getParent ? action.getParent() : null,
                parentWidget = parentAction ? thiz.getVisibilityField(parentAction, 'widget') : null,
                actionVisibility = action.getVisibility != null ? action.getVisibility(this.visibility) : {};




            /**
             * Cleanup variables, sanitize
             */

            //case when visibility is not an object, upgrade visibility to an object
            if (!_.isObject(actionVisibility) && action.setVisibility) {
                actionVisibility = {};
                action.setVisibility(this.visibility, actionVisibility);
            }

            //further variables
            var label = actionVisibility.label || action.label,

                widgetArgs = {
                    iconClass: action.icon,
                    label:label,
                    item:action
                };

            if(actionVisibility.widgetArgs){
                utils.mixin(widgetArgs,actionVisibility.widgetArgs);
            }



            /**
             * Determine widget class to use, resolve
             */
            widgetClass  =
                //override
                actionVisibility.widgetClass ||
                //argument
                widgetClass ||
                //visibility's default
                thiz.widgetClass;


            // case one: isContainer = true
            if(!widget) {

                if (isContainer) {

                    if(_expand){
                        return null;
                    }


                    console.log('render container ' + action.command);

                    if(!parentWidget) {

                        var menu = utils.addWidget(DropDownButton, widgetArgs, this, where, true, null, [Reference]);

                        var pSubMenu = utils.addWidget(Menu, {
                            item: action
                        }, this, null, true);

                        menu.dropDown = pSubMenu;
                        actionVisibility.widget = pSubMenu;
                        return menu;

                    }else{

                        //parent widget there

                        var pSubMenu = new Menu({parentMenu: parentWidget});

                        var popup = new dijit.PopupMenuItem({
                            label: label,
                            popup: pSubMenu,
                            iconClass: action.icon
                        });

                        parentWidget.addChild(popup);

                        actionVisibility.widget = pSubMenu;
                        return pSubMenu;

                    }
                }
                if(parentWidget){

                    where = parentWidget;
                    widgetClass = MenuItem;
                }
            }else{
                console.log('widget already rendered!');
            }


            console.log('render action ' + action.command, {
                widgetClass: widgetClass ? widgetClass.prototype.declaredClass : 'no class',
                hasItems: _hasItems,
                expand: _expand,
                widget: widget,
                parentAction: parentAction,
                parentWidget: parentWidget,
                where: where
            });


            if (!actionVisibility.widget && !action.domNode) {

                if (!action.label) {//huh, really? @TODO: get rid of label needs in action:render
                } else {
                    actionVisibility.widget = thiz._addMenuItem(action, where, widgetClass, showLabel);
                    actionVisibility.widget.visibility = thiz.visibility;
                }
            } else {
                console.log('have already rendered action',action);
            }


            /*var _lastWidget = thiz._renderAction(action, where, widgetClass, showLabel);*/


            return actionVisibility.widget;

        }

        /**
         * Renders a set of actions into is appropriate widgets. This utilizes this._renderAction and
         * puts the action's widget into its visibility store for global access.
         *
         * @param actions {xaction/Action[]}
         * @param where {dijit/_Widget}
         * @param widgetClass {Module}
         * @private
         */
        ribbon._renderActions = function (actions, where, widgetClass, showLabel, separatorClass, force) {



            //track the last action group for adding a separator
            var _lastGroup = null,
                thiz = this,
                widgets = [],
                _lastWidget;

            if (!actions) {
                console.error('strange, have no actions!');
                return;
            }

            for (var i = 0; i < actions.length; i++) {

                var action = actions[i],
                    sAction = action._store ? action._store.getSync(action.command) : null;

                if (sAction && sAction != action) {
                    console.log('weird!');
                }
                if (sAction) {
                    action = sAction;
                }

                if (!action) {
                    console.error('invalid action!');
                    continue;
                }




                //pick command group
                if (i == 0 && !_lastGroup && action.group) {
                    _lastGroup = action.group;
                }

                //skip if action[visibility].show is false
                if (thiz.getVisibilityField(action, 'show') === false) {
                    continue;
                }

                var _items = action.getChildren ? action.getChildren() : action.items,
                    _hasItems = _items && _items.length,
                    _expand = thiz.getVisibilityField(action, 'expand'),
                    _forceSub = action.forceSubs;




                //action has its own set of actions

                /*
                 if ((_hasItems || _forceSub ) && !thiz.getVisibilityField(action, 'widget')) {
                 if (_expand == true) {

                 } else {
                 thiz.renderSubActions(action, where);
                 }
                 continue;
                 }
                 */


                force = force != null ? force : this.forceRenderSubActions;


                /*
                 //check action for a parent widget
                 var parent = action.getParent ? action.getParent() : null;
                 if (parent) {

                 var parentWidget = thiz.getVisibilityField(parent, 'widget');

                 if (parentWidget && force !== true && _expand !== true) {
                 console.log('skip action, has parent and force not TRUE');
                 continue;
                 }
                 }
                 */


                //console.log('render action: '+action.command,[actions,action]);

                /*if (action && action.show !== false) {*/


                //  Insert separator as soon there is a 'group' and the group actually changed
                /*
                 if (!_.isEmpty(action.group) && action.group !== _lastGroup) {

                 var children = where.getChildren();

                 if (children.length > 0 && children[children.length - 1].prototype != thiz.separatorClass.prototype) {

                 var separator = utils.addWidget(separatorClass || thiz.separatorClass, {}, null, where, true);

                 var widgetArgs = thiz.getVisibilityField(action, 'widgetArgs');
                 if (widgetArgs && widgetArgs.style && widgetArgs.style.indexOf('float:right') != -1) {
                 domStyle.set(separator.domNode, {
                 float: 'right'
                 });
                 }

                 }
                 _lastGroup = action.group;
                 }
                 */


                _lastWidget = thiz.renderAction(action, where, widgetClass, showLabel);


                /*
                 _lastWidget = thiz._renderAction(action, where, widgetClass, showLabel);
                 if (_lastWidget) {
                 widgets.push(_lastWidget);
                 }
                 */

                /*}*/
            }

            return widgets;
        }

    }

    /**
     *
     */
    var _layoutMixin = {
        _docker:null,
        _parent:null,
        __right:null,
        /**
         * @param container
         * @returns {module:xdocker/Docker2}
         */
        getDocker:function(container){

            if(!this._docker){

                var _dst = container || this._domNode.parentNode,
                    thiz = this;

                thiz._docker = Docker.createDefault(_dst);
                thiz._oldParent = thiz._parent;

                var parent = thiz._docker.addPanel('DefaultFixed', types.DOCKER.TOP, null, {
                    w: '100%',
                    title:'  '
                });

                dojo.place(thiz._domNode,parent.containerNode);

                thiz._docker.$container.css('top',0);
                thiz._parent = parent;

                parent._parent.showTitlebar(false);


/*
                var right = this._docker.addPanel('Collapsible', types.DOCKER.RIGHT, parent, {
                    w: '20%',
                    title:'Properties'
                });

                parent._parent.showTitlebar(false);*/





                //parent._parent.$center.css('top',0);

            }

            return thiz._docker;
        },
        getRightPanel:function(){

            if(this.__right){
                return this.__right;
            }

            var docker = this.getDocker();


            var right = docker.addPanel('Collapsible', types.DOCKER.RIGHT, this._parent, {
                w: '300px',
                title:'  '
            });



            right._parent.showTitlebar(false);

            var splitter = right.getSplitter();

            splitter.pos(0.6);


            this.__right = right;

            return right;
        }
    }

    var _gridBase = {
        currentCIView:null,
        targetTop:null,
        lastSelectedTopTabTitle:'General',
        editBlock:function(item,changedCB,select){


            var selection = this.getSelection(),
                item=null;
            if(selection.length == 1){
                item = selection[0];
            }

            if(!item){
                return;
            }

            if(!item.canEdit()){
                return;
            }
            var title='Edit ',
                thiz=this;
            if(item.title){
                title+=item.title;
            }else if(item.name){
                title+=item.name;
            }
            try {
                var actionDialog = new BlockEditDialog({
                    item: item,
                    title: title,
                    style: 'width:400px',
                    resizeable: true,
                    delegate: {
                        onOk: function (dlg, data) {

                            //item.scope.expression.

                            if (changedCB) {
                                changedCB(item);
                            }

                            /**
                             * triggers to refresh block grid views
                             */
                            thiz.publish(types.EVENTS.ON_BLOCK_PROPERTY_CHANGED,{
                                item:item
                            });

                            /**
                             * update block tree view!
                             */

                            if(select!==false) {

                                thiz.onBlockSelected({
                                    item: item,
                                    owner: this.currentCIView
                                });
                            }

                        }
                    }
                });
            }catch(e){
                //debugger;
            }
            actionDialog.show();
            actionDialog.resize();
        },
        execute: function (block) {

            if (!block || !block.scope) {
                console.error('have no scope');
                return;
            }
            try {
                var result = block.scope.solveBlock(block, {
                    highlight: true,
                    force: true
                });
            } catch (e) {
                console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                console.error(printStackTrace().join('\n\n'));
            }
            return true;
        },
        move: function (dir) {

            var item = this.getSelection()[0];

            if (!item || !item.parentId) {

                console.log('cant move, no selection or parentId', item);

                return;

            }

            //parent.items = items.swap(item,upperItem);

            console.log('move ', item);


            try {
                item.move(item, dir);
                //this.onItemAction(true);
                console.log('move up to' + item.name);
            } catch (e) {
                debugger;
            }



        },
        runAction: function (action) {

            switch (action.command) {
                case 'Step/Run':
                {
                    return this.execute(this.getSelection()[0]);
                }
                case 'Step/Move Up':
                {
                    return this.move(-1);
                }
                case 'Step/Move Down':
                {
                    return this.move(1);
                }
                case 'Step/Edit':
                {
                    return this.editBlock();
                }
            }


        },
        getAddActions: function (item) {

            var thiz = this;


            var items = [];
            this.showAllBlocks = true;
            if (this.showAllBlocks || item) {

                var variables = this.blockScope.getVariables();
                var variableItems = [];
                /*
                 for (var i = 0; i < variables.length; i++) {
                 variableItems.push({
                 name: variables[i].title,
                 target: item,
                 iconClass: 'el-icon-compass',
                 proto: VariableAssignmentBlock,
                 item: variables[i],
                 ctrArgs: {
                 variable: variables[i].title,
                 scope: this.blockScope,
                 value: '0'
                 }
                 });
                 }*/
                items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
                items.push({
                    name: 'Set Variable',
                    target: item,
                    iconClass: 'el-icon-pencil-alt',
                    items: variableItems
                });

                //tell everyone
                factory.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                    items: items
                });
                return items;
            }


            /*
             if (!item) {

             if (this.newRootItemFunction) {
             items.push({
             name: this.newRootItemLabel,
             iconClass: this.newRootItemIcon,
             handler: function () {
             thiz.newRootItemFunction();
             thiz.onItemAction();
             }
             });
             return items;
             }
             }
             */

        },
        showProperties:function(item){


            var block = item,
                thiz = this;

            var right = this.getRightPanel();

            if(item == this._lastItem){
                return;
            }

            this._lastItem = item;

            var tabContainer = this.targetTop;


            if(!tabContainer) {

                tabContainer = utils.addWidget(TabContainer, {
                    delegate: this,
                    tabStrip: true,
                    tabPosition: "top",
                    attachParent: true,
                    style: "width:100%;height:100%;overflow-x:hidden;",
                    allowSplit: false
                }, null,right.containerNode, true);
                this.targetTop =tabContainer;
            }

            if (tabContainer.selectedChildWidget) {
                this.lastSelectedTopTabTitle = tabContainer.selectedChildWidget.title;
            }else{
                this.lastSelectedTopTabTitle = 'General';
            }



            _.each(tabContainer.getChildren(),function(tab){
                tabContainer.removeChild(tab);
            });

            if (this.currentCIView){
                this.currentCIView.empty();
            }




            // clear tab - container
            /*
            if (this.currentCIView && this.currentCIView.tabs) {

                if(this.currentCIView.tabContainer._destroyed){
                    this.currentCIView=null;
                }else {
                    this.currentCIView.empty();

                    try {
                        for (var i = 0; i < this.currentCIView.tabs.length; i++) {
                            this.targetTop.removeChild(this.currentCIView.tabs[i]);
                        }
                    } catch (e) {
                        //should not happen!
                        console.error('clear failed: ' + e, e);
                        console.trace();
                    }
                }
            }
            */

            if(!block.getFields){
                console.log('have no fields',block);
                return;
            }

            var cis = block.getFields();
            for (var i = 0; i < cis.length; i++) {
                cis[i].vertical = true;
            }







            var ciView = new CIViewMixin({
                tabContainer: this.targetTop,
                delegate: this,
                viewStyle: 'padding:0px;',
                autoSelectLast: true,
                item: block,
                source: this.callee,
                options: {
                    groupOrder: {
                        'General': 0,
                        'Advanced': 1,
                        'Description': 2
                    }
                },
                cis: cis
            });
            ciView.initWithCIS(cis);

            this.currentCIView = ciView;

            if(block.onFieldsRendered){
                block.onFieldsRendered(block,cis);
            }

            ciView._on('valueChanged',function(evt){
                console.log('ci value changed ',evt);
            });


            var containers = this.targetTop.getChildren();
            var descriptionView = null;
            for (var i = 0; i < containers.length; i++) {

                // @TODO : why is that not set?
                containers[i].parentContainer = this.targetTop;

                // track description container for re-rooting below
                if (containers[i].title === 'Description') {
                    descriptionView = containers[i];
                }

                if(this.targetTop.selectedChildWidget.title!==this.lastSelectedTopTabTitle) {
                    if (containers[i].title === this.lastSelectedTopTabTitle) {
                        this.targetTop.selectChild(containers[i]);
                    }
                }
            }


            this._docker.resize();



            /*
            if(this.targetBottom && this.targetBottom._destroyed){
                utils.destroy(this.targetBottom);
                this.targetBottom=null;
            }


            if(!this.targetBottom) {
                //  Re root description view into right bottom panel
                this.targetBottom = this.getRightBottomTarget(true, false);
            }
            */



/*
            for (var i = 0; i < cis.length; i++) {
                if(cis[i].select === true && cis[i].view){
                    this.targetTop.selectChild(cis[i].view);
                }
            }
            */



            /*
            if (descriptionView && this.targetBottom) {
                this.targetBottom.destroyDescendants(false);
                this.currentCIView.tabs.remove(descriptionView);
                descriptionView.parentContainer.removeChild(descriptionView);
                this.targetBottom.addChild(descriptionView);
                var main = this.getLayoutRightMain(false, false);
                main.resize();
            }
            */


            if (this.blockTreeView) {

            } else {
                /*
                if(this.renderNewTab) {

                    var main = this.ctx.getApplication().mainView;
                    if (main) {
                        main._openRight();
                    }
                    this.blockTreeView = this.createBlockTreeView(this.targetTop, 'New');
                }
                */
            }



        },
        startup:function(){

            this.inherited(arguments);

            this._on('selectionChanged',function(evt){

                var selection = evt.selection,
                    thiz = this;

                if(selection && selection[0]){
                    thiz.showProperties(selection[0]);
                }
            });

        }
    };

    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent,
        ACTION = types.ACTION,
        root;


    var _actions = [
        ACTION.RENAME
    ];

    /**
     *
     */
    function getFileActions(permissions) {


        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();


        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            var action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        var who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }


        var rootAction = 'Block/Insert';
        permissions.push(rootAction);
        addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
            dummy: true,
            onCreate: function (action) {
                 action.setVisibility(VISIBILITY.CONTEXT_MENU, {
                    label: 'Add'
                 });

            }
        }, null, null);

        permissions.push('Block/Insert Variable');


        addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
        }, null, null);
        /*
        permissions.push('Clipboard/Paste/New');
        addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
        }, null, null);*/


        var newBlockActions = this.getAddActions();
        var addActions = [];
        var levelName = '';


        function addItems(commandPrefix, items) {

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                levelName = item.name;


                var path = commandPrefix + '/' + levelName;
                var isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }



        //console.clear();

        addItems(rootAction, newBlockActions);


        //console.dir(newBlockActions);


        /*
         for (var i = 0; i < newBlockActions.length; i++) {


         //top level items
         var item = newBlockActions[i];

         if(_.isEmpty(item.items)){
         continue;
         }

         levelName = item.name;

         var path = rootAction + '/' + levelName;

         permissions.push(path);

         addAction(levelName,path,item.iconClass,null,'Home','Insert','item|view',null,null,{
         dummy:true
         },null,null);

         //now the items



         addItems(path,item.items);


         }*/


        console.log('new items', newBlockActions);

        //return result;

        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            var canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);
            return !canMove;

        }


        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */
        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                /*
                action.setVisibility(types.ACTION_VISIBILITY_ALL, {
                    label: ''
                });*/
            }
        }, null, isItem);

        permissions.push('Step/Run/From here');

        /**
         * run
         */
        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                /*
                action.setVisibility(types.ACTION_VISIBILITY_ALL, {
                    label: ''
                });
                */
            }
        }, null, isItem);


        /**
         * move
         */
        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['ctrl up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);

        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['ctrl down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);



        permissions.push('Step/Edit');

        addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        return result;

    }

    if (ctx) {


        var blockManager = ctx.getBlockManager();


        var doTest = true;


        //var xfileCtx = ctx.getXFileContext();
        function createScope(mount) {

            var data = {
                "blocks": [
                    {
                        "_containsChildrenIds": [
                            "items"
                        ],
                        "group": "click",
                        "id": "root",
                        "items": [
                            "sub0",
                            "sub1"
                        ],
                        "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                        "name": "Run Script",
                        "method": "console.log('asd',this);",
                        "args": "",
                        "deferred": false,
                        "declaredClass": "xblox.model.code.RunScript",
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "_scenario": "update"
                    },
                    {
                        "_containsChildrenIds": [],
                        "parentId": "root",
                        "id": "sub0",
                        "name": "On Event",
                        "event": "",
                        "reference": "",
                        "declaredClass": "xblox.model.events.OnEvent",
                        "_didRegisterSubscribers": false,
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "_scenario": "update"
                    },
                    {
                        "_containsChildrenIds": [],
                        "parentId": "root",
                        "id": "sub1",
                        "name": "On Event",
                        "event": "",
                        "reference": "",
                        "declaredClass": "xblox.model.events.OnEvent",
                        "_didRegisterSubscribers": false,
                        "enabled": true,
                        "serializeMe": true,
                        "shareTitle": "",
                        "description": "No Description",
                        "canDelete": true,
                        "renderBlockIcon": true,
                        "order": 0,
                        "additionalProperties": true,
                        "_scenario": "update"
                    }
                ],
                "variables": []
            };

            return blockManager.toScope(data);
        }


        var ribbon = ctx.mainView.getToolbar();
        patchToolbar(ribbon);


        function createGridClass() {

            var renderers = [TreeRenderer];

            //, ThumbRenderer, TreeRenderer


            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);






            var _gridClass = Grid.createGridClass('driverTreeView',
                {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT:{
                        CLASS:declare('splitMixin',null,_layoutMixin)
                    }

                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );


            return declare('gridFinal', _gridClass, _gridBase);

        }

        var blockScope = createScope('docs');


        var mainView = ctx.mainView;

        var docker = mainView.getDocker();


        if (mainView) {


            if (_lastGrid) {

                docker.removePanel(_lastGrid);
            }

            var parent = docker.addTab(null, {
                title: 'blox',
                icon: 'fa-folder'
            });


            //var themeRoot = require.toUrl('xide/docker');

            //var docker = Docker.createDefault(master.containerNode);


            window._lastGrid = parent;



/*
            parent = docker.addPanel('DefaultFixed', types.DOCKER.TOP, null, {
                w: '80%',
                title:''
            });


            var right = docker.addPanel('Collapsible', types.DOCKER.LEFT, parent, {
                w: '20%',
                title:'Properties'
            });
            */


            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon;


            var store = blockScope.blockStore;


            var _gridClass = createGridClass();

            /*
            grid = new _gridClass({

                blockScope: blockScope,
                blockGroup: 'click',
                shouldShowAction: function (action) {
                    return true;
                },
                collection: store.filter({
                    group: "click"
                }),
                tabOrder: {
                    'Home': 100,
                    'View': 50
                },
                groupOrder: {


                    'Clipboard': 110,
                    'File': 100,
                    'Step': 80,
                    'Open': 70,
                    'Organize': 60,
                    'Insert': 10,
                    'Select': 0
                },
                showHeader: true,
                options: utils.clone(types.DEFAULT_GRID_OPTIONS),
                _parent: parent,
                columns: [
                    {
                        renderExpando: true, label: "Name", field: "name", sortable: false, editor: BlockGridRowEditor
                    }
                ]
            }, parent.containerNode);
            */

            var gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                shouldShowAction: function (action) {
                    return true;
                },
                collection: store.filter({
                    group: "click"
                }),
                tabOrder: {
                    'Home': 100,
                    'View': 50
                },
                groupOrder: {


                    'Clipboard': 110,
                    'File': 100,
                    'Step': 80,
                    'Open': 70,
                    'Organize': 60,
                    'Insert': 10,
                    'Select': 0
                },
                showHeader: true,
                options: utils.clone(types.DEFAULT_GRID_OPTIONS),
                columns: [
                    {
                        renderExpando: true, label: "Name", field: "name", sortable: false, editor: BlockGridRowEditor
                    }
                ]
            };

            grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);
            //var grid2 = utils.addWidget(_gridClass,gridArgs,null,right,true);







            grid.startup();
           // grid2.startup();

            grid.select(['root'], null, true, {
                focus: true
            });
            var _actions = [
                //ACTION.EDIT,
                ACTION.RENAME,
                /*ACTION.DOWNLOAD,*/
                ACTION.RELOAD,
                ACTION.DELETE,
                /*ACTION.NEW_FILE,
                 ACTION.NEW_DIRECTORY,*/
                ACTION.CLIPBOARD,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.PREVIEW,
                'Step/Run',
                'Step/Move Up',
                'Step/Move Down'

            ];
            /**
             * struct
             *

             -  if ....
             - sub0
             - Run-Script
             - subsub0
             - else
             - sub1

             - else-if
             - sub2




             */

            var blocks = blockScope.allBlocks();
            var root = blocks[0];


            grid._on('selectionChanged', function (evt) {

                var selection = evt.selection;
                var r0 = selection[0];
                var same = root == r0;


            });


            var _defaultActions = DefaultActions.getDefaultActions(_actions, grid);
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [_actions]));
            grid.addActions(_defaultActions);


            /*
             _defaultActions = _defaultActions.concat(grid.getFileActions(_actions));
             grid.addActions(_defaultActions);

             */


            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();

            if (!toolbar) {
                window._lastRibbon = ribbon = toolbar = utils.addWidget(Ribbon, {
                    store: actionStore,
                    flat: true,
                    currentActionEmitter: grid
                }, this, mainView.layoutTop, true);

            } else {
                toolbar.addActionEmitter(grid);
                toolbar.setActionEmitter(grid);
            }

            setTimeout(function () {
                mainView.resize();
                grid.resize();

            }, 1000);


            function test() {



                //grid.getRightPanel();




                //console.clear();
                //c1.set('value', true);
                try {
                    //     copy.set('value', true, true);
                } catch (e) {
                    //debugger;

                }


                grid.refresh();

                return;

            }

            function test2() {

                return;


                /*
                 var c2 = actionStore.getSync('View/Columns/Show Size');
                 c2._originEvent='change';
                 var _c = c1.get('value');
                 */


                //console.log('c2 value : ' + _c);


                try {
                    //copy.set('value', false, true);
                    c2.set('value', false);


                } catch (e) {
                    //debugger;
                }

                return;

                console.log('-------------------------------------------------------------------------');

                var item = store.getSync('id3');
                var item1 = store.getSync('id1');
                var item2 = store.getSync('id2');


                //console.dir(grid._rows);


                /*
                 grid.select(item);
                 grid.focus(item);*/


                //grid.select([item,item1,item2]);
                //grid.select(item);
                grid.select([item1, item2], null, true, {
                    append: true,
                    focus: false,
                    silent: true

                });


                /*var isToolbared = grid.hasFeature('TOOLBAR');
                 console.warn('has Toolbar ');*/


                /*
                 var nameProperty = item.property('label');

                 var urlProperty = item.property('url');


                 nameProperty.observe(function () {
                 console.log('horray!', arguments);
                 });

                 urlProperty.observe(function () {
                 console.log('horray url', arguments);
                 });

                 item.set('label', 'new label');
                 item.set('url', null);

                 //store.notify(item,'id3');
                 store.emit('update', {target: item});
                 */


                /*grid.refresh();*/
                //console.log('item ', item);

                /*
                 grid.select(store.getSync('id99'), null, true, {
                 append: true,
                 focus: true,
                 silent:true
                 });*/

                var item99 = store.getSync('id99');
                var item98 = store.getSync('id98');

                grid.select([item99, item98], null, true, {
                    append: true,
                    focus: true,
                    silent: false
                });


                //console.log('is selected ' + grid.isSelected(item98));

                //console.dir(grid.getRows(true));

                /*console.dir(grid.getPrevious(item99, false, true));*/
                /*
                 console.dir(grid.getSelection(function(item){
                 return item.id!='id99';
                 }));*/


                //console.log(grid.getFocused());

                /*store.removeSync('id3');*/
            }

            setTimeout(function () {
                test();
            }, 4000);
            setTimeout(function () {
                test2();
            }, 2000);

        }
    }

    return Grid;

});