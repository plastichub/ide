define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xdocker/Docker2',
    'xblox/views/BlockGrid',
    'xide/tests/TestUtils',
    'xblox/BlockActions',
    'xide/views/_LayoutMixin',
    "xide/widgets/_Widget",

    'dojo/dnd/Source',
    'dojo/dnd/Manager',
    'dojo/_base/NodeList',
    'module',
    'dojo/when',
    'dojo/has!touch?dgrid/util/touch',
    'dojo/topic',
    'xdojo/has',
    'dojo/_base/array',
    'dojo/aspect',
    'dojo/dom-class',
    'xide/factory',
    'xgrid/DnD',
    'dojo/Deferred',
    'xide/registry',
    'xblox/views/ThumbRenderer',
    'dojo/dnd/Manager',
    'dojo/dom-geometry',
    "dojo/dnd/common",
    "xblox/views/DnD"

], function (declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, Editor, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, Docker, BlockGrid,
             TestUtils, BlockActions, _LayoutMixin, _Widget, DnDSource,
             DnDManager, NodeList, module, when, touchUtil, topic, has, arrayUtil,
             aspect, domClass, factory, DnD, Deferred, registry, ThumbRenderer, Manager, domGeom, dnd,BlocksDnD) {




    //console.clear();
    /**
     * flow  - drop
     * 1. SharedDndGridSource:onDrop ->
     * 1.1. SharedDndGridSource:onDropInternal or SharedDndGridSource:onDropExternal
     * 1.2 SharedDndGridSource:onDropInternal
     * 1.3 DojoDndMixin#onDrop
     * 1.4 Grid#onDrop
     */
    //var Source = DnD.GridSource;

    /**
     *
     * @class module:xblox/views/BlockGridDnDSource
     * @extends module:xgrid/DnD/GridDnDSource
     */
    var BlockGridDnDSource = declare(DnD.GridSource, {
        declaredClass: 'xblox/views/BlockGridDnDSource',
        _lastTargetId: null,
        _lastTargetCrossCounter: 0,
        /**
         * Assigns a class to the current target anchor based on "before" status
         * @param before {boolean} insert before, if true, after otherwise
         * @param center {boolean}
         * @param canDrop {boolean}
         * @private
         */
        _markTargetAnchor: function (before, center, canDrop) {
            if (this.current == this.targetAnchor && this.before === before && this.center === center) {
                return;
            }
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, this.before ? "Before" : "After");
                this._removeItemClass(this.targetAnchor, "Disallow");
                this._removeItemClass(this.targetAnchor, "Center");
            }
            this.targetAnchor = this.current;
            this.targetBox = null;
            this.before = before;
            this.center = center;
            this._before = before;
            this._center = center;

            if (this.targetAnchor) {
                if (center) {
                    this._removeItemClass(this.targetAnchor, "Before");
                    this._removeItemClass(this.targetAnchor, "After");
                    this._addItemClass(this.targetAnchor, 'Center');
                }
                !center && this._addItemClass(this.targetAnchor, this.before ? "Before" : "After");
                center && canDrop === false && this._addItemClass(this.targetAnchor, "Disallow");
            }
        },
        /**
         * @param sourceSource {module:dojo/dnd/Source}
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:dojo/dnd/Target}
         */
        onDropExternal: function (sourceSource, nodes, copy, targetItem) {
            // Note: this default implementation expects that two grids do not
            // share the same store.  There may be more ideal implementations in the
            // case of two grids using the same store (perhaps differentiated by
            // query), dragging to each other.
            var grid = this.grid,
                store = this.grid.collection,
                sourceGrid = sourceSource.grid;

            // TODO: bail out if sourceSource.getObject isn't defined?
            nodes.forEach(function (node, i) {
                when(sourceSource.getObject(node), function (object) {
                    // Copy object, if supported by store; otherwise settle for put
                    // (put will relocate an existing item with the same id).
                    // Note that we use store.copy if available even for non-copy dnd:
                    // since this coming from another dnd source, always behave as if
                    // it is a new store item if possible, rather than replacing existing.
                    grid._trackError(function () {
                        return store[store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {
                            if (!copy) {
                                if (sourceGrid) {
                                    // Remove original in the case of inter-grid move.
                                    // (Also ensure dnd source is cleaned up properly)
                                    var id = sourceGrid.collection.getIdentity(object);
                                    !i && sourceSource.selectNone(); // Deselect all, one time
                                    sourceSource.delItem(node.id);
                                    return sourceGrid.collection.remove(id);
                                }
                                else {
                                    sourceSource.deleteSelectedNodes();
                                }
                            }
                        });
                    });
                });
            });
        }
    });

    function debugStoreItem(item) {
        if (item) {
            var parent = item.getParent() || {};
            console.log("Item : " + item.name + ' | ' + parent.name);
        }
    }

    /**
     *
     * @enum {int} module:xblox/types/DROP_TYPE
     * @memberOf module:xide/types
     */
    var DROP_TYPE = {
        BEFORE: 1,
        CENTER: 2
    };
    /**
     * @class module:xblox/views/DojoDndMixin
     */
    var DojoDndMixin = declare("xblox.widgets.DojoDndMixin", null, {
        declaredClass: 'xblox.widgets.DojoDndMixin',
        dropEvent: "/dnd/drop",
        dragEvent: "/dnd/start",
        overEvent: "/dnd/source/over",
        /**
         * Final
         * @param sources {module:xblox/model/Block[]}
         * @param target {module:xblox/model/Block}
         * @param sourceGrid {module:xgrid/Base| module:dgrid/List}
         * @param params {object}
         * @param params.SAME_STORE {object}
         * @param copy {boolean}
         * @private
         */
        __onDrop: function (sources, target, sourceGrid, params, copy) {
            var into = params.center === true;
            _.each(sources, function (source) {
                //happens when dragged on nothing
                if (source == target) {
                    return;
                }
                if (into) {
                    if (target.canAdd(source) !== false) {
                        target.scope.moveTo(source, target, params.before, into);
                    }
                } else {
                    var sourceParent = source.getParent();
                    if (sourceParent) {
                        var targetParent = target.getParent();
                        if (!targetParent) {
                            //move to root - end
                            source.group = this.blockGroup;
                            sourceParent.removeBlock(source, false);
                            source._store.putSync(source);
                        } else {

                            //we dropped at the end within the same tree
                            if (targetParent == source.getParent()) {
                                target.scope.moveTo(source, target, params.before, into);
                            }
                        }
                    }
                }
                source.refresh();
            }, this);
            
            this.set('collection', this.collection.filter({
                group: "click"
            }))

            this.select(sources[0], null, true, {
                focus: true,
                append: false,
                expand: true,
                delay: 2
            });
        },
        /**
         *
         * @param source {module:dojo/dnd/Source}
         * @returns {module:xblox/model/Block}
         * @returns {module:xgrid/Base}
         * @private
         */
        _sourceToModel: function (source, grid) {
            var result = null;
            if (source) {

                var anchor = source._targetAnchor || source.anchor || source;
                grid = grid || source.grid || this;
                if (!anchor || !anchor.ownerDocument) {
                    return null;
                }
                var row = grid.row(anchor);
                if (row) {
                    return row.data;
                }
                if (anchor) {
                    result = grid.collection.getSync(anchor.id.slice(grid.id.length + 5));
                    if (!result) {
                        result = grid.row(anchor);
                    }
                }
            }
            return result;
        },
        __dndNodesToModel: function (nodes) {
            return _.map(nodes, function (n) {
                return (this.row(n) || {}).data;
            }, this);
        },
        startup:function(){
            if(this._started){
                return;
            }
            this.subscribe(this.dropEvent, function (source, nodes, copy, target) {
                if (source.grid == this) {
                    var rows = this.__dndNodesToModel(nodes);
                    target = target || {};
                    var _target = this._sourceToModel(target, target.grid);

                    if (!_target || _.isEmpty(rows)) {
                        return;
                    }
                    var DND_PARAMS = {
                        SAME_STORE: rows[0]._store == _target._store,
                        before: target._before,
                        center: target._center
                    }
                    if (rows && _target) {
                        this.__onDrop(rows, _target, source.grid, DND_PARAMS, copy);
                    }
                }
            });

            return this.inherited(arguments);

        }
    });

    // Mix in Selection for more resilient dnd handling, particularly when part
    // of the selection is scrolled out of view and unrendered (which we
    // handle below).
    DnD = declare('xblox.dnd', [DojoDndMixin], {
        // dndSourceType: String
        //		Specifies the type which will be set for DnD items in the grid,
        //		as well as what will be accepted by it by default.
        dndSourceType: 'dgrid-row',

        // dndParams: Object
        //		Object containing params to be passed to the DnD Source constructor.
        dndParams: null,

        // dndConstructor: Function
        //		Constructor from which to instantiate the DnD Source.
        //		Defaults to the GridSource constructor defined/exposed by this module.
        dndConstructor: BlockGridDnDSource,
        postMixInProperties: function () {
            this.inherited(arguments);
            // ensure dndParams is initialized
            this.dndParams = utils.mixin({accept: [this.dndSourceType]}, this.dndParams);
        },
        postCreate: function () {
            this.inherited(arguments);
            // Make the grid's content a DnD source/target.
            var Source = this.dndConstructor || BlockGridDnDSource;
            var dndParams = utils.mixin(this.dndParams, {
                // add cross-reference to grid for potential use in inter-grid drop logic
                grid: this,
                dropParent: this.contentNode
            });
            if (typeof this.expand === 'function') {
                // If the Tree mixin is being used, allowNested needs to be set to true for DnD to work properly
                // with the child rows.  Without it, child rows will always move to the last child position.
                dndParams.allowNested = true;
            }
            this.dndSource = new Source(this.bodyNode, dndParams);

            // Set up select/deselect handlers to maintain references, in case selected
            // rows are scrolled out of view and unrendered, but then dragged.
            var selectedNodes = this.dndSource._selectedNodes = {};

            function selectRow(row) {
                selectedNodes[row.id] = row.element;
            }

            function deselectRow(row) {
                delete selectedNodes[row.id];
                // Re-sync dojo/dnd UI classes based on deselection
                // (unfortunately there is no good programmatic hook for this)
                domClass.remove(row.element, 'dojoDndItemSelected dojoDndItemAnchor');
            }

            this.on('dgrid-select', function (event) {
                arrayUtil.forEach(event.rows, selectRow);
            });
            this.on('dgrid-deselect', function (event) {
                arrayUtil.forEach(event.rows, deselectRow);
            });

            aspect.after(this, 'destroy', function () {
                delete this.dndSource._selectedNodes;
                selectedNodes = null;
                this.dndSource.destroy();
            }, true);
        },
        insertRow: function (object) {
            // override to add dojoDndItem class to make the rows draggable
            var row = this.inherited(arguments),
                type = typeof this.getObjectDndType === 'function' ?
                    this.getObjectDndType(object) : [this.dndSourceType];

            domClass.add(row, 'dojoDndItem');
            this.dndSource.setItem(row.id, {
                data: object,
                type: type instanceof Array ? type : [type]
            });
            return row;
        },
        removeRow: function (rowElement) {
            this.dndSource.delItem(this.row(rowElement));
            this.inherited(arguments);
        }
    });

    DnD.GridSource = BlockGridDnDSource;

    function debugSource(source) {
        console.log('\t Source : ' + source.declaredClass + ' | before: ' + source._before + ' | ContainerState = ' + source.containerState + ' | TargetState =' + source.targetState + ' | SourceState ' + source.sourceState + ' | center = ' + source._center, source);
    }
    function debugTarget(source) {
        console.log('\t Target : ' + source.declaredClass + ' | before: ' + source._before + ' | ContainerState = ' + source.containerState + ' | TargetState =' + source.targetState + ' | SourceState ' + source.sourceState + ' | center = ' + source._center, source);
    }





    /**
     * @class module:xblox/views/SharedDndGridSource
     * @extends module:xblox/views/BlockGridDnDSource
     * @extends module:xblox/views/DojoDndMixin
     */
    var SharedDndGridSource = declare([DnD.GridSource], {
        autoSync: true,
        skipForm: true,
        selfAccept: false,
        onDndDrop: function (source, nodes, copy, target) {
            console.error('ondrop');
            if (this == target) {
                // this one is for us => move nodes!
                this.onDrop(source, nodes, copy, target);
                this.grid.refresh();
            }
            this.onDndCancel();
        }
    });


    /***
     * playground
     */
    var ctx = window.sctx,
        root;

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "icon": 'fa-bell text-fatal',
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0
                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    'icon': 'fa-play'

                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "group": "click",
                    "id": "6a12d54a-f5af-aaf8-0abb-bff866211fc4",
                    "declaredClass": "xblox.model.logic.SwitchBlock",
                    "name": "Switch",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "order": 0,
                    "type": "added"
                },
                {
                    "_containsChildrenIds": [],
                    "group": "click",
                    "id": "8e1c9d2a-d2fe-356f-d484-82e27c793dc9",
                    "declaredClass": "xblox.model.variables.VariableSwitch",
                    "name": "Switch on Variable",
                    "icon": "",
                    "variable": "PowerState",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "order": 0,
                    "type": "added"
                }
            ],
            "variables": []
        };
        return blockManager.toScope(data);
    }

    if (ctx) {

        var blockManager = ctx.getBlockManager();


        function createGridClass() {

            var renderers = [TreeRenderer, ThumbRenderer];

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var _gridClass = Grid.createGridClass('driverTreeView', {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT: {
                        CLASS: _LayoutMixin
                    },
                    Dnd: {
                        CLASS: DnD
                    },
                    Base: {
                        CLASS: declare('Base', null, BlockGrid.IMPLEMENTATION)
                    },
                    BLOCK_ACTIONS: {
                        CLASS: BlockActions
                    },
                    WIDGET: {
                        CLASS: _Widget
                    }
                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );
            //return declare('gridFinal', BlockGrid, _gridClass);
            return _gridClass;
        }

        var blockScope = createScope('docs');
        var mainView = ctx.mainView;
        if (mainView) {


            var parent = TestUtils.createTab('BlockGrid-Test-Source', null, module.id);
            var target = TestUtils.createTab('BlockGrid-Test-Target', null, module.id + '_target', parent, types.DOCKER.TAB.BOTTOM, types.DOCKER.DOCK.BOTTOM);

            var thiz = this,
                grid, grid2;

            var store = blockScope.blockStore;

            var _gridClass = createGridClass();

            var dndParams = {
                allowNested: true, // also pick up indirect children w/ dojoDndItem class
                checkAcceptance: function (source, nodes) {
                    return true;//source !== this; // Don't self-accept.
                },
                isSource: true
            };


            var gridArgs = {
                ctx: ctx,
                /**
                 * @type {module:xblox/views/SharedDndGridSource}
                 */
                dndConstructor: SharedDndGridSource, // use extension defined above
                blockScope: blockScope,
                blockGroup: 'click',
                dndParams: dndParams,
                name: "Grid - 1",
                collection: store.filter({
                    group: "click"
                })
            };


            var gridArgs2 = {
                ctx: ctx,
                /**
                 * @type {module:xblox/views/SharedDndGridSource}
                 */
                dndConstructor: SharedDndGridSource, // use extension defined above
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect: true,
                name: "Grid - 2",
                collection: store.filter({
                    group: "click"
                })
            };

            grid = utils.addWidget(_gridClass, gridArgs, null, parent, true);

            grid2 = utils.addWidget(_gridClass, gridArgs2, null, target, true);



            ctx.getWindowManager().registerView(grid);
            grid2 && ctx.getWindowManager().registerView(grid2);


            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {
                return;
            }

        }
    }

    return Grid;

});