define([
    'require',
    'xdojo/has',
    'dcl/dcl',
    'dojo/_base/lang',
    'dojo/_base/declare',
    'xide/factory',
    'xide/utils',
    'xblox/model/variables/Variable',
    'dojo/Deferred',
    'xide/views/_CIDialog',
    'xide/types',
    'xide/views/_LayoutMixin',
    'xblox/views/BlockGrid',
    'xaction/ActionProvider',
    'xide/layout/_TabContainer',
    'xide/layout/Container',
    'xdocker/Docker2',
    'xide/tests/TestUtils',
    'xblox/tests/TestUtils',
    'module'
], function (require,has,dcl,lang,declare, factory, utils, Variable, Deferred,_CIDialog,types,_LayoutMixin,BlockGrid,ActionProvider,_TabContainer,Container,Docker,
             TestUtils,BTestUtils,module) {

    var ACTION = types.ACTION;
    console.clear();


    /**
     * the layouter
     */
    var LayoutClass = dcl(_LayoutMixin.dcl,{
        rootPanel:null,
        _lastTab:null,
        getDocker:function(){
            if(!this._docker){
                this._docker = Docker.createDefault(this.containerNode);
                this.add(this._docker);
            }
            return this._docker;
        },
        getRootContainer:function(args){

            if(this.rootPanel){
                return this.rootPanel;
            }

            this.reparent = true;
            var docker = this.getDocker();
            var DOCKER = types.DOCKER;
            var defaultTabArgs = {
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:DOCKER.TAB.TOP,
                location:DOCKER.DOCK.STACKED

            };
            this.rootPanel = docker.addTab(null,utils.mixin(defaultTabArgs,args));
            return this.rootPanel;
        },
        createLayout:function(){
            if(!this.rootPanel) {
                this.rootPanel = this.getRootContainer({
                    title: 'test'
                });
            }
            return this.rootPanel;
        },
        startup:function(){

        },
        getPropertyStruct:function(){
            return this.propertyStruct;
        },
        setPropertyStruct:function(struct){
            this.propertyStruct = struct;
        },
        getGroupContainer: function () {
            return this.rootPanel;
        },
        createGroupView: function (groupContainer, group,scope,closable,iconClass,selected,args) {
            var DOCKER = types.DOCKER;
            var defaultTabArgs = {
                icon:iconClass,
                closeable:true,
                moveable:true,
                title:group,
                target:this._lastTab||this.rootPanel,
                tabOrientation:DOCKER.TAB.TOP,
                location:DOCKER.DOCK.STACKED

            };

            var  tab = this.getDocker().addTab(null,utils.mixin(defaultTabArgs,args));

            this._lastTab = tab;

            /*
            var tab = groupContainer.createTab(group,iconClass,selected,_TabContainer.tabClass,{
                delegate: this,
                iconClass:iconClass || '',
                title: group,
                allowSplit:true,
                closable:closable,
                style: 'padding:0px',
                cssClass: 'blocksEditorPane',
                blockView: null
            });

            tab._on('show',function(tab){
                if(tab.grid) {
                    this.activeTab = tab;
                    this.activeGrid = tab.grid;
                    this.onShowGrid(tab.grid);                }
            },this);
            */

            return tab;
        }
    });
    var GridClass = declare('BlockGrid',BlockGrid,{
        _refresh:function(args){
            //var res = this.inherited(arguments);
            var history = this.getHistory();
            var now = history.getNow();
            if(now){
                //debugger;
                this.setParentBlock(now);
            }
            return res;
        },
        editBlock:function(_item,changedCB,select) {

            var selection = this.getSelection(),
                item = selection[0] || _item;

            if (!item) {
                return;
            }
            var head = new Deferred(),
                children = item.getChildren();

            if(children && children.length){

                console.log('--p');

                var history = this.getHistory(),
                    self = this,
                    select = true;

                this.setParentBlock(item);
                var isBack = false;
                head.resolve({
                    select: select !== false ? ( isBack ? item : self.getRows()[0]) : null,
                    focus: true,
                    append: false,
                    delay: 200
                });

                return head;
            }
        },
        __select:function(items){
            var item = items[0] || {};
            return this.inherited(arguments);
        },
        /**
         * Step/Move Down & Step/Move Up action
         * @param dir
         */
        move: function (dir) {

            var items =this.getSelection();

            console.log('move ' + dir,_.pluck(items,'id'));

            if (!items || !items.length /*|| !item.parentId*/) {
                console.log('cant move, no selection or parentId', items);
                return;
            }
            var thiz = this;

            if(dir===1){
                //items.reverse();
            }

            _.each(items,function(item){
                item.move(item, dir);
            });

            thiz.refreshRoot();
            thiz.refreshCurrent();


            this.select(items,null,true,{
                focus:true,
                delay:10
            }).then(function(){
                thiz.refreshActions();
            });


        },
        reParentBlock:function(dir){

            var item = this.getSelection()[0];
            if (!item) {
                console.log('cant move, no selection or parentId', item);
                return false;
            }
            var thiz = this;
            if(dir==-1) {
                item.unparent(thiz.blockGroup);
            }else{
                item.reparent();
            }


            thiz.deselectAll();
            thiz.refreshRoot();
            this.refreshCurrent();
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 100,
                select:item,
                expand:true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        defaultActionResult:function(items){
            var dfd = new Deferred();
            var defaultSelectArgs = {
                focus: true,
                append: false,
                delay: 0,
                select:items,
                expand:true
            };
            dfd.resolve(defaultSelectArgs);
            return dfd;
        },
        runAction: function (action) {
            var thiz = this;
            var sel = this.getSelection();

            var selection = this.getSelection(),
                item = selection[0];
            switch (action.command){
                /*
                 case 'Step/Move Left':
                 {
                 return this.reParentBlock(-1);
                 }
                 case 'Step/Move Right':
                 {
                 return this.reParentBlock(1);
                 }
                 */
            }

            if(action) {
                //console.error('run action ' + action.command);
            }

            return this.inherited(arguments);
        }
    });
    var Module =  dcl([Container,LayoutClass,ActionProvider.dcl],{
        declaredClass:"xblox.views.BlocksFileEditor",
        registerView:false,
        _item: null,
        cssClass: 'bloxEditor',
        blockManager: null,
        blockManagerClass: 'xblox.manager.BlockManager',
        model: null,
        store: null,
        tree: null,
        currentItem: null,
        didLoad: false,
        selectable: false,
        beanType: 'BLOCK',
        newGroupPrefix:'',
        _debug:false,
        blockScope: null,
        groupContainer: null,
        canAddGroups:true,
        gridClass:GridClass,
        activeGrid:null,
        activeTab:null,
        registerGrids:true,
        visibileTab:null,
        setVisibleTab:function(tab){
            this.visibileTab = tab;
        },
        getVisibleTab:function(){
            return this.visibileTab;
        },
        constructor:function(options,container){
            utils.mixin(this,options);
        },
        onGridAction:function(evt){
            var action = evt.action,
                command = action.command,
                result = evt.result,
                ACTION = types.ACTION;

            switch (command){
                case ACTION.SAVE:{
                    return this.save();
                }
            }
            //console.log('on after action '+evt.action.command);

        },
        clearGroupViews:function(all){

            var container = this.getGroupContainer(),
                thiz = this;


            container.empty();


            this.destroyWidgets();
            return;

            var container = this.getGroupContainer(),
                thiz = this;

            var panes = container.getChildren();
            for (var i = 0; i < panes.length; i++) {
                if(panes[i].isNewTab){
                    container.removeChild(panes[i]);
                }
            }
            for (var i = 0; i < panes.length; i++) {

                var pane = panes[i];
                /*
                 if(pane.title=='Variables' && all!==true){
                 continue;
                 }*/
                container.removeChild(pane);


            }

            this.createNewTab();



        },
        getContainerLabel:function(group){

            var title = '' + group;
            if(utils.isNativeEvent(group)){
                title = title.replace('on','');
            }

            //device variable changed: onDriverVariableChanged__deviceId__dd2985b9-9071-1682-226c-70b84b481117/9ab3eabe-ef9a-7613-c3c8-099cde54ef39
            if(group.indexOf(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED)!==-1){

                var deviceManager = this.ctx.getDeviceManager();
                var parts = group.split('__');
                var deviceId = parts[1];
                var driverId = parts[2];
                var variableId = parts[3];
                var device = deviceManager.getDeviceById(deviceId);

                var driverScope = device ? device.driver : null;

                //not initiated driver!
                if(driverScope && driverScope.blockScope){
                    driverScope=driverScope.blockScope;
                }

                if(!driverScope){
                    console.error('have no driver, use driver from DB',group);
                    if(device) {
                        var driverId = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                        //var driverManager = this.ctx.getDriverManager();
                        driverScope = this.ctx.getBlockManager().getScope(driverId);

                    }
                }
                var deviceTitle = device ? deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE) : 'Invalid Device';
                var variable=driverScope ? driverScope.getVariableById(driverId + '/' + variableId) : 'Invalid Variable(Have no driver)';
                title = 'Variable Changed('+deviceTitle+'/'+ (variable ? variable.title : 'Unknown Variable') +')';

            }
            return title;
        },
        addNewBlockGroup:function(group){

            if(!group){
                return;
            }
            var blockScope = this.blockScope;
            var container = this.getGroupContainer();
            var title = this.getContainerLabel(group);
            var contentPane = this.createGroupView(container, title, blockScope,true,'fa-bell');
            console.log('add group',contentPane);
            /*
            var gridViewConstructurArgs = {};
            var newGroup = this.newGroupPrefix + group;
            gridViewConstructurArgs.newRootItemGroup = newGroup;
            var view = this.createGroupedBlockView(contentPane.containerNode, newGroup, blockScope, gridViewConstructurArgs);
            contentPane.grid=view;
            container.selectChild(contentPane);
            view.resize();*/
        },

        createGroupedBlockView: function (container, group, scope, extra,gridBaseClass){

            var thiz = this;
            gridBaseClass = gridBaseClass || this.gridClass;
            var store = scope.blockStore;

            if(this._lastTab && !this.__right){
                this.__right = this.getRightPanel(null,null,'DefaultFixed',{
                    target:this._lastTab
                });
            }
            var gridArgs = {
                __right:this.__right,
                ctx:this.ctx,
                blockScope: scope,
                blockGroup: group,
                attachDirect:true,
                resizeToParent:true,
                collection: store.filter({
                    group: group
                }),
                _parent:container,
                getPropertyStruct:this.getPropertyStruct,
                setPropertyStruct:this.setPropertyStruct
            };

            extra && lang.mixin(gridArgs, extra);
            var view = utils.addWidget(gridBaseClass,gridArgs,null,container,false);

            if(!view.__editorActions){
                view.__editorActions=true;
                this.addGridActions(view,view);
            }
            container.grid = view;
            view._on('selectionChanged',function(evt){
                thiz._emit('selectionChanged',evt);
            });

            view._on('onAfterAction',function(e){
                thiz.onGridAction(e);
            });
            container.on(types.DOCKER.EVENT.VISIBILITY_CHANGED, function (visible) {
                if(visible){
                    thiz.setVisibleTab(container);
                    view.onShow();
                    var right = thiz.getRightPanel();
                    var splitter = right.getSplitter();
                    if (!splitter.isCollapsed()) {
                        view.showProperties(view.getSelection()[0]);
                    }
                    if(view.__last){
                        view._restoreSelection();
                        view.focus(view.row(view.__last.focused));
                    }
                }else{
                    view.__last = view._preserveSelection();
                }
                if(visible && !this.grid._started){}
            });
            this.registerGrids && this.ctx.getWindowManager().registerView(view,true);
            if(!container._widgets){
                container._widgets=[];
            }
            container.add(view);
            return view;
        },
        getDeviceVariablesAsEventOptions:function(startIntend){
            var options = [];
            var _item = function(label,value,intend,selected,displayValue){

                var string="<span style=''>" +label + "</span>";
                var pre = "";
                if(intend>0){
                    for (var i = 0; i < intend; i++) {
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label:_final,
                    label2:displayValue,
                    value:value
                };
            };

            var deviceManager = this.ctx.getDeviceManager();
            var items = deviceManager.getDevices(false,true);

            for (var i = 0; i < items.length; i++) {
                var device = items[i];
                var driver = device.driver;
                if(!driver){
                    continue;
                }

                var title = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                var id = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_ID);
                options.push(_item(title,driver.id+'/' +driver.id,startIntend,false));


                var blockScope = driver.blockScope;
                var variables = blockScope.getVariables();
                for (var j = 0; j < variables.length; j++) {
                    var variable = variables[j];
                    var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED+ '__' + id+'__'+driver.id + '__'+ variable.id;
                    var selected = false;
                    options.push(_item(variable.name,value,startIntend + 1,selected,title + '/' + variable.name));
                }
            }

            return options;
        },
        openNewGroupDialog:function(){
            var options = [];
            var _item = function(label,value,intend,isHTML){
                var string= isHTML !==true ? "<span style=''>" +label + "</span>" : label;
                var pre = "";
                if(intend>0){
                    for (var i = 0; i < intend; i++) {
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label:_final,
                    value:value,
                    label2:label
                };
            };

            options.push( _item('HTML','',0));
            options = options.concat([
                _item('onclick','click',1),
                _item('ondblclick','dblclick',1),
                _item('onmousedown','mousedown',1),
                _item('onmouseup','mouseup',1),
                _item('onmouseover','mouseover',1),
                _item('onmousemove','mousemove',1),
                _item('onmouseout','mouseout',1),
                _item('onkeypress','keypress',1),
                _item('onkeydown','keydown',1),
                _item('onkeyup','keyup',1),
                _item('onfocus','focus',1),
                _item('onblur','blur',1),
                _item('load','load',1)
            ]);

            options.push( _item('<span class="text-default">Device Variable Changed</span>','',0,true));
            options = options.concat(this.getDeviceVariablesAsEventOptions(1));

            var thiz = this;
            var actionDialog = new _CIDialog({
                title: 'Create a new block group',
                style: 'width:500px;min-height:200px;',
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                resizeable: true,
                onOk: function () {
                    thiz.addNewBlockGroup(this.getField('Event'));
                },
                cis: [
                    utils.createCI('Event', types.ECIType.ENUMERATION, '',                        {
                            group: 'Event',
                            delegate: null,
                            options:options,
                            value:'HTML',
                            title:'Select an event &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                            widget:{
                                search:true,
                                "class":"xide.form.FilterSelect"
                            }
                        }
                    )
                ]
            });
            this.add(actionDialog,null,false);

            return actionDialog.show();
        },
        onGroupsCreated:function(){
            //this.ctx.getWindowManager().registerView(this,true);
        },
        getActionStore:function(){
            if(this.activeGrid){
                return this.activeGrid.getActionStore();
            }
            return null;
        },
        getTabContainer:function(){
            return this.groupContainer;
        },
        onReloaded: function () {
            this.destroyWidgets();
            this.openItem(this._item);
        },
        /**
         * default entry when opening a file item through xfile
         * @param item
         */
        openItem: function (item) {

            var dfd = new Deferred();
            this._item = item;
            var thiz = this;

            var blockManager = this.getBlockManager();

            blockManager.load(item.mount, item.path).then(function (scope) {
                thiz.initWithScope(scope);
                dfd.resolve(thiz);
            });

            return dfd;

        },
        /**
         * Init with serialized string, forward to
         * @param content
         */
        initWithContent: function (content) {

            var data = null;
            try {
                data = utils.getJson(content);
            } catch (e) {
                console.error('invalid block data');
            }

            if (data) {
                this.initWithData(data);
            }

        },
        /**
         * Entry point when a blox scope is fully parsed
         * @param blockScope
         */
        initWithScope: function (blockScope) {

            this.blockScope = blockScope;


            var allBlockGroups = blockScope.allGroups(),
                thiz = this;

            if (allBlockGroups.indexOf('Variables') == -1) {
                allBlockGroups.push('Variables');
            }
            if (allBlockGroups.indexOf('Events') == -1) {
                allBlockGroups.push('Events');
            }
            if (allBlockGroups.indexOf('On Load') == -1) {
                allBlockGroups.push('On Load');
            }

            thiz.renderGroups(allBlockGroups, blockScope);

        },
        getScopeUserData: function () {
            return {
                owner: this
            };
        },
        initWithData: function (data) {

            if(this._debug) {
                console.log('init with data', data);
            }

            this.onLoaded();

            var scopeId = utils.createUUID(),
                blockInData = data,
                variableInData = data;

            //check structure
            if (lang.isArray(data)) {// a flat list of blocks

            } else if (lang.isObject(data)) {
                scopeId = data.scopeId || scopeId;
                blockInData = data.blocks || [];
                variableInData = data.variables || [];
            }

            var blockManager = this.getBlockManager();
            this.blockManager = blockManager;
            var scopeUserData = this.getScopeUserData();

            var blockScope = blockManager.getScope(scopeId, scopeUserData, true);
            var allBlocks = blockScope.blocksFromJson(blockInData);

            for (var i = 0; i < allBlocks.length; i++) {
                var obj = allBlocks[i];

                obj._lastRunSettings = {
                    force: false,
                    highlight: true
                }
            }

            var allVariables = blockScope.variablesFromJson(variableInData);
            blockManager.onBlocksReady(blockScope);
            if(this._debug) {
                console.log('   got blocks', allBlocks);
                console.log('   got variables', allVariables);
            }
            if (allBlocks) {
                return this.initWithScope(blockScope);
            }
            /**
             * a blocks file must be in that structure :
             */
        },
        destroyWidgets: function () {
            if (this.blockManager && this.blockScope) {
                this.blockManager.removeScope(this.blockScope.id);
            }
            //this.groupContainer = null;
            this.blockManager = null;
        },
        destroy: function () {
            this.destroyWidgets();
        },
        getBlockManager: function () {
            if (!this.blockManager) {
                if (this.ctx.blockManager) {
                    return this.ctx.blockManager;
                }
                this.blockManager = factory.createInstance(this.blockManagerClass, {
                    ctx: this.ctx
                });
                if(this._debug) {
                    console.log('_createBlockManager ', this.blockManager);
                }
            }
            return this.blockManager;
        },
        getActiveGrid:function(){
            return this.activeGrid;
        },
        runAction:function(action){
            if(action.command=='File/New Group'){
                this.openNewGroupDialog();
                return null;
            }
            return this.getActiveGrid().runAction(arguments);
        },
        addGridActions:function(grid,who){
            var result = [];
            var thiz = this;
            var defaultMixin = { addPermission: true };
            result.push(thiz.createAction({
                label: 'New Group',
                command: 'File/New Group',
                icon: 'fa-magic',
                tab: 'Home',
                group: 'File',
                keycombo: ['f7'],
                mixin: utils.mixin({quick:true},defaultMixin)
            }));
            result.push(thiz.createAction({
                label: 'Delete Group',
                command: 'File/Delete Group',
                icon: 'fa-remove',
                tab: 'Home',
                group: 'File',
                keycombo: ['ctrl f7'],
                mixin: utils.mixin({quick:true},defaultMixin)
            }));
            (who || this).addActions(result);
        },
        onShowGrid:function(grid){

            /*
            if(this._isCreating){
                return;
            }
            this.ctx.getWindowManager().clearView(null);
            if(!grid.__editorActions){
                grid.__editorActions=true;
                this.addGridActions(grid);
            }

            //this.groupContainer.resize();
            //grid.resize();
            setTimeout(function(){
                grid.resize();
            },100);
            this.ctx.getWindowManager().registerView(grid);
            */
        },
        renderGroup:function(group,blockScope,gridBaseClass){
            blockScope = blockScope || this.blockScope;
            var thiz = this;

            var title = group.replace(this.newGroupPrefix,'');
            if(utils.isNativeEvent(group)){
                title = title.replace('on','');
            }

            title = this.getContainerLabel(group.replace(this.newGroupPrefix,''));
            var isVariableView = group === 'Variables';
            var contentPane = this.createGroupView(null, title, blockScope,!isVariableView,isVariableView ? ' fa-info-circle' : 'fa-bell',!isVariableView);
            var gridViewConstructurArgs = {};
            if (group === 'Variables') {

                gridViewConstructurArgs.newRootItemFunction = function () {
                    try {
                        var newItem = new Variable({
                            title: 'No-Title-Yet',
                            type: 13,
                            value: 'No Value',
                            enumType: 'VariableType',
                            save: false,
                            initialize: '',
                            group: 'Variables',
                            id: utils.createUUID(),
                            scope: blockScope
                        });
                    } catch (e) {
                        debugger;
                    }
                };

                gridViewConstructurArgs.onGridDataChanged = function (evt) {

                    var item = evt.item;
                    if (item) {
                        item[evt.field] = evt.newValue;
                    }
                    thiz.save();
                };
                gridViewConstructurArgs.showAllBlocks = false;
                gridViewConstructurArgs.newRootItemLabel = 'New Variable';
                gridViewConstructurArgs.newRootItemIcon = 'fa-code';
                gridViewConstructurArgs.storeField = 'variableStore';
                gridViewConstructurArgs.gridParams ={
                    cssClass: 'bloxGridView',
                    getColumns:function(){
                        return [
                            {
                                label: "Name",
                                field: "title",
                                sortable: true

                            },
                            {
                                label: "Value",
                                field: "value",
                                sortable: false
                            }
                        ]
                    }
                };
            }


            gridViewConstructurArgs.newRootItemGroup = group;

            var view = this.createGroupedBlockView(contentPane, group, blockScope, gridViewConstructurArgs,gridBaseClass);
            contentPane.blockView = view;
            !this.activeGrid && (this.activeGrid=view);
            return view;
        },
        renderGroups: function (_array, blockScope) {
            this._isCreating = true;
            this.activeGrid = null;

            this.setPropertyStruct({
                currentCIView:null,
                targetTop:null,
                _lastItem:null,
                id:utils.createUUID()
            });
            for (var i = 0; i < _array.length; i++) {
                var group =  _array[i];
                if(this.newGroupPrefix=='' && group.indexOf('__')!==-1){
                    continue;
                }
                try {
                    var groupBlocks = blockScope.getBlocks({
                        group: group
                    });

                    if (group !== 'Variables' && (!groupBlocks || !groupBlocks.length)) {//skip empty
                        continue;
                    }
                    this.renderGroup(group,blockScope);
                } catch (e) {
                    logError(e);
                }
            }

            if(this._lastTab) {
                this._lastTab.select();
            }
            //this.onShowGrid()
        },
        onSave: function (groupedBlockView) {
            this.save();
        },
        save: function () {
            if (this.blockScope) {
                var fileManager = this.ctx.getFileManager(),
                    item = this.item;

                fileManager.setContent(item.mount,item.path,this.blockScope.toString(),function(){
                    console.log('saved blocks! to ' + item.path);
                });


            }else{
                console.warn('BlocksFileEditor::save : have no block scope');
            }
        }
    });

    var ctx = window.sctx;
    var mainView = ctx.mainView;


    if (mainView) {
        var blockManager = ctx.getBlockManager();
        var blockScope = BTestUtils.createScope(blockManager,null);
        var parent = TestUtils.createTab('BlockGrid-Test', null, module.id);
        var editor = utils.addWidget(Module,{
            blockManager:blockManager,
            blockScope:blockScope,
            gridClass:GridClass,
            ctx:ctx

        },null,parent,true);
        editor.initWithScope(blockScope);

    }


    return Module;

});