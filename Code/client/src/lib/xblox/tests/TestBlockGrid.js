/** @module xgrid/Base **/
define([
    'xide/utils',
    'xide/types',
    'xgrid/Grid',
    'xblox/views/BlockGrid',
    'xblox/views/ThumbRenderer',
    'xide/tests/TestUtils',
    'dojo/dom-construct',
    'xaction/DefaultActions',
    './TestUtils',
    'module'
], function (utils, types, Grid, BlockGrid, ThumbRenderer, TestUtils, domConstruct, DefaultActions, BlockUtils, module) {

    var ACTION = types.ACTION;
    console.clear();
    /***
     *
     * playground
     */
    var ctx = window.sctx;

    function doGridTest(grid, scope) {


        /*
        grid.refresh().then(function () {
            //grid.runAction('View/Layout/Thumb');
            grid.runAction('View/Show/Toolbar');

            grid.select([0]).then(function (e) {
            });

        });
        */

    }




    ThumbRenderer.prototype.runAction = function (action) {
        if (action.command === 'Step/Edit') {
            var collection = this.collection,
                item = this.getSelectedItem();

            if (item) {

                this.set('collection', collection.filter({
                    parentId: item.id
                }));
            }
        }

        return true;
    }
    ThumbRenderer.prototype.renderRow = function (obj) {
        if (obj.renderRow) {
            var _res = obj.renderRow.apply(this, [obj]);
            if (_res) {
                return _res;
            }
        }

        var thiz = this,
            div = domConstruct.create('div', {
                className: "tile widget"
            }),
            icon = obj.icon,
            no_access = obj.read === false && obj.write === false,
            isBack = obj.name == '..',
            directory = obj.getChildren().length > 0,
            useCSS = false,
            imageClass = 'fa fa-folder fa-5x',
            isImage = false,
            blockIcon = obj.icon;

        this._doubleWidthThumbs = true;


        var _d = obj.getChildren();



        console.log('render row ' + obj.name + ' = ' + directory + ' blockIcon ' + blockIcon);


        var iconStyle = 'text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: 72px;opacity: 0.7';
        var contentClass = 'icon';


        if (directory) {
            imageClass = 'fa fa-5x fa-folder';
            useCSS = true;

        } else {

            imageClass = 'fa fa-5x ' + ( blockIcon || 'fa-play');

            useCSS = true;
        }

        var label = obj.name;

        var folderContent = '<span style="' + iconStyle + '" class="fa fa-6x ' + imageClass + '"></span>';

        var _image = false;
        if (_image) {

            var url = this.getImageUrl(obj);
            if (url) {
                obj.icon = url;
            } else {
                obj.icon = thiz.config.REPO_URL + '/' + obj.path;
            }

            imageClass = '';
            contentClass = 'image';
            //folderContent =  '<span style="' + iconStyle + '" class="fa fa-6x '+imageClass +'"></span>';
            folderContent = '<div style="" class="tile-content image">' +
                '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>' +
                '</div>';

            useCSS = true;
            isImage = true;

        }


        //var iconStyle='text-shadow: 2px 2px 5px rgba(0,0,0,0.3);left:40px;text-align:left;font-size: 72px;margin-top:-45px;opacity: 0.7';


        var label2 = label + '\n' + obj.modified;

        var html = '<div title="' + label2 + '" class="tile-content ' + contentClass + '">' +
            folderContent +
            '</div>' +

            '<div class="brand opacity">' +
            '<span class="thumbText text opacity ellipsis" style="">' +
            label +
            '</span>' +
            '</div>';


        if (isImage || this._doubleWidthThumbs) {
            $(div).addClass('double');
        }

        if (useCSS) {
            div.innerHTML = html;
            return div;
        }


        if (directory) {
            div.innerHTML = html;
        } else {
            div.innerHTML = '<img class=\"' + imageClass + '\" src="' + obj.icon + '"/>&nbsp;<div class="name">' + obj.name + '</div>';
        }
        return div;
    }

    if (ctx) {
        var blockManager = ctx.getBlockManager();
        var blockScope = BlockUtils.createScope(blockManager, [
            {
                "_containsChildrenIds": [],
                "id": "Shell-Block",
                "description": "Runs a JSON-RPC-2.0 method on the server",
                "name": "Run Server Method",
                "method": "XShell::run",
                "args": "ls",
                "deferred": true,
                "defaultServiceClass": "XShell",
                "defaultServiceMethod": "run",
                "declaredClass": "xblox.model.server.RunServerMethod",
                "enabled": true,
                "shareTitle": "",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "group": "click"
            }
        ]);
        var mainView = ctx.mainView;

        if (mainView) {

            var parent = TestUtils.createTab(null, null, module.id);

            var grid,
                store = blockScope.blockStore,
                gridArgs = {
                    ctx: ctx,
                    blockScope: blockScope,
                    blockGroup: 'click',
                    attachDirect: true,
                    collection: store.filter({
                        group: "click"
                    }),
                    permissions: [
                        //ACTION.EDIT,
                        ACTION.RENAME,
                        ACTION.RELOAD,
                        ACTION.DELETE,
                        ACTION.CLIPBOARD,
                        ACTION.LAYOUT,
                        ACTION.COLUMNS,
                        ACTION.SELECTION,
                        //ACTION.PREVIEW,
                        ACTION.SAVE,
                        ACTION.SEARCH,
                        ACTION.DOWNLOAD,
                        'Step/Run',
                        'Step/Move Up',
                        'Step/Move Down',
                        'Step/Edit',
                        ACTION.TOOLBAR

                    ],
                    getBlockActions: function (permissions) {

                        var result = [],
                            ACTION = types.ACTION,
                            ACTION_ICON = types.ACTION_ICON,
                            VISIBILITY = types.ACTION_VISIBILITY,
                            thiz = this,
                            container = thiz.domNode,
                            actionStore = thiz.getActionStore();

                        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

                            var action = null;
                            mixin = mixin || {};
                            utils.mixin(mixin, {owner: thiz});

                            if (mixin.addPermission || DefaultActions.hasAction(permissions, command)) {

                                if (!handler) {
                                    handler = function (action) {
                                        console.log('log run action', arguments);
                                        var who = this;
                                        if (who.runAction) {
                                            who.runAction.apply(who, [action]);
                                        }
                                    }
                                }

                                if (!mixin.tooltip && keycombo) {

                                    if (_.isString(keycombo)) {
                                        keycombo = [keycombo];
                                    }


                                    mixin.tooltip = keycombo.join('<br/>').toUpperCase();

                                }


                                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);
                                result.push(action);
                                return action;
                            }
                        }




                        var rootAction = 'Block/Insert';
                        var defaultMixin = { addPermission: true };

                        result.push(thiz.createAction({
                            label: 'New Block',
                            command: rootAction,
                            icon: 'fa-plus',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['alt up'],
                            mixin: defaultMixin
                        }));
                        result.push(thiz.createAction({
                            label: 'Save',
                            command: 'File/Save',
                            icon: 'fa-save',
                            tab: 'Home',
                            group: 'File',
                            keycombo: ['ctrl '],
                            mixin: defaultMixin
                        }));
                        result.push(thiz.createAction({
                            label: 'Save As',
                            command: 'File/Save As',
                            icon: 'fa-save',
                            tab: 'Home',
                            group: 'File',
                            mixin: defaultMixin
                        }));

                        var newBlockActions = this.getAddActions();
                        var levelName = '';

                        function addItems(commandPrefix, items) {

                            for (var i = 0; i < items.length; i++) {
                                var item = items[i];

                                levelName = item.name;

                                var path = commandPrefix + '/' + levelName;
                                var isContainer = !_.isEmpty(item.items);

                                result.push(thiz.createAction({
                                    label: levelName,
                                    command: path,
                                    icon: item.iconClass,
                                    tab: 'Home',
                                    group: 'Step',
                                    mixin:defaultMixin
                                }));

                                /*
                                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {
                                    item: item,
                                    addPermission: true
                                }, null, null);
                                */


                                if (isContainer) {
                                    addItems(path, item.items);
                                }
                            }

                        }

                        addItems(rootAction, newBlockActions);



                        function _selection() {
                            var selection = thiz.getSelection();
                            if (!selection || !selection.length) {
                                return null;
                            }
                            var item = selection[0];
                            if (!item) {
                                console.error('have no item');
                                return null;
                            }
                            return selection;
                        }

                        function _canMove() {

                            var selection = _selection();
                            if (!selection) {
                                return true;
                            }
                            return selection[0].canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

                        }


                        function canParent() {

                            var selection = thiz.getSelection();
                            if (!selection) {
                                return true;
                            }
                            var item = selection[0];
                            if (!item) {
                                //console.error('canParent:have no item!!!',this);
                                return true;
                            }
                            if (this.command === 'Step/Move Left') {
                                return !item.getParent();
                            } else {
                                return item.getParent();
                            }
                        }

                        function isItem() {
                            var selection = _selection();
                            if (!selection) {
                                return true;
                            }
                            return false;
                        }

                        function canMove() {
                            var selection = _selection();

                            if (!selection) {
                                return true;
                            }

                            var item = selection[0];

                            if (this.command === 'Step/Move Up') {
                                return !item.canMove(null, -1);
                            } else {
                                return !item.canMove(null, 1);
                            }

                            return false;
                        }

                        result.push(thiz.createAction({
                            label: 'Run',
                            command: 'Step/Run',
                            icon: 'fa-play',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['r'],
                            shouldDisable: isItem
                        }));

                        //////////////////////////////////////////////////////////////////
                        //
                        //  Step - Move
                        //

                        result.push(thiz.createAction({
                            label: 'MoveUp',
                            command: 'Step/Move Up',
                            icon: 'fa-arrow-up',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['alt up'],
                            shouldDisable: canMove
                        }));

                        result.push(thiz.createAction({
                            label: 'MoveDown',
                            command: 'Step/Move Down',
                            icon: 'fa-arrow-down',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['alt down'],
                            shouldDisable: canMove,
                            mixin: defaultMixin
                        }));

                        result.push(thiz.createAction({
                            label: 'MoveLeft',
                            command: 'Step/Move Left',
                            icon: 'fa-arrow-left',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['alt left'],
                            shouldDisable: canMove,
                            mixin: defaultMixin
                        }));

                        result.push(thiz.createAction({
                            label: 'MoveRight',
                            command: 'Step/Move Right',
                            icon: 'fa-arrow-right',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['alt right'],
                            shouldDisable: canParent,
                            mixin: defaultMixin
                        }));


                        //Step enable/disable
                        /*
                         addAction('On', 'Step/Enable', 'fa-toggle-off', ['alt d'], 'Home', 'Step', 'item|view', null, null,
                         {
                         addPermission:true,
                         onCreate: function (action) {
                         action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                         widgetClass: declare.classFactory('_Checked', [ToggleButton,_ActionValueWidgetMixin], null, {} ,null),
                         widgetArgs: {
                         icon1: 'fa-toggle-on',
                         icon2: 'fa-toggle-off',
                         delegate: thiz,
                         checked:true,
                         iconClass:'fa-toggle-on'
                         }
                         });
                         }
                         },null, function(){
                         return thiz.getSelection().length==0;
                         });
                         */

                        result.push(thiz.createAction({
                            label: 'On',
                            command: 'Step/Enable',
                            icon: 'fa-toggle-off',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['alt d'],
                            shouldDisable: isItem,
                            mixin: defaultMixin
                        }));


                        result.push(this.createAction({
                            label: 'Edit',
                            command: 'Step/Edit',
                            icon: ACTION_ICON.EDIT,
                            keycombo: ['f4', 'enter', 'dblclick'],
                            tab: 'Home',
                            group: 'Step',
                            shouldDisable: isItem
                        }));

                        /*
                         addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                         {
                         addPermission: true,
                         onCreate: function (action) {
                         action.handle=false;
                         action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                         widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                         widgetArgs: {
                         icon1: 'fa-toggle-on',
                         icon2: 'fa-toggle-off',
                         delegate: thiz,
                         checked: false,
                         iconClass: 'fa-toggle-off'
                         }
                         });
                         }
                         }, null, function () {
                         return thiz.getSelection().length == 0;
                         });
                         */
                        result.push(thiz.createAction({
                            label: 'Properties',
                            command: 'Step/Properties',
                            icon: 'fa-arrow-up',
                            tab: 'Home',
                            group: 'Step',
                            keycombo: ['alt enter'],
                            shouldDisable: isItem,
                            mixin: defaultMixin
                        }));

                        this._emit('onAddActions', {
                            actions: result,
                            addAction: addAction,
                            permissions: permissions,
                            store: actionStore
                        });

                        //console.dir(_.pluck(result,'command'));


                        return result;
                    }
                };

            grid = utils.addWidget(BlockGrid, gridArgs, null, parent, true);
            ctx.getWindowManager().registerView(grid, true);

            var blocks = blockScope.allBlocks();



            setTimeout(function () {
                mainView.resize();
                grid.resize();
                doGridTest(grid, blockScope);
            }, 1000);


        }
    }

    return Grid;

});