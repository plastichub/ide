/** @module xgrid/Base **/
define([
    'xdojo/declare',
    'xide/utils',
    'xgrid/Grid',
    'xblox/views/BlockGrid',
    'xide/tests/TestUtils',
    './TestUtils',
    'module'



], function (declare,utils, Grid, BlockGrid,
             TestUtils,BlockUtils,module
    ) {


    /***
     * playground
     */
    var ctx = window.sctx;

    console.clear();
    console.log('do grid test2');

    function doGridTest(grid,scope){





        grid.refresh().then(function(){
            grid.select([0]).then(function(e){
                //grid.runAction('Step/Run');
            });
        });

    }

    var did = false;


    function format(value,block,who){


        if(!did){
            var items = block._store.storage.fullData;
            items = items.filter(function(item){
                return item.group ==='click';
            });
            console.dir(_.pluck(items,'name'));
            console.dir(_.pluck(items,'group'));
            did=true;
        }

        function index(item,dir){

            var item = this,
                parent = item.getParent(),
                items = null,
                group = item.group,
                store = this._store;

            if (parent) {
                items = parent[parent._getContainer(item)];
                items = items.filter(function(item){
                    return item.group ===group;
                });
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }
                return this.indexOf(items, item);

            }else{
                items = store.storage.fullData;
                items = items.filter(function(item){
                    return item.group ===group;
                });
                return this.indexOf(items, item);
            }
        }

        var parent = block.getParent();

        var _index = index.apply(block,[block,1]);
        if(parent){
            var parents =block.numberOfParents();
            var _out='&nbsp;';
            for (var i = 0; i < parents; i++) {
                _out+='&nbsp;'
            }
            _out += '';

            for (var i = 0; i < parents; i++) {
                _out+='-'
            }
            _out += '&nbsp;';
            _index=_out + _index;
        }

        console.log('index ' + block.name  + ' :  ' + _index);

        return _index;
    }
    xide.utils.fff= format;




    if (ctx) {

        var blockManager = ctx.getBlockManager();


        var blockScope = BlockUtils.createScope(blockManager,[],[
            {
                "_containsChildrenIds": [],
                "id": "Shell-Block",
                "description": "Runs a JSON-RPC-2.0 method on the server",
                "name": "Run Server Method",
                "method": "XShell::run",
                "args": "ls",
                "deferred": true,
                "defaultServiceClass": "XShell",
                "defaultServiceMethod": "run",
                "declaredClass": "xblox.model.server.RunServerMethod",
                "enabled": true,
                "shareTitle": "",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "group": "click"
            }
        ]);

        var mainView = ctx.mainView;





        if (mainView) {

            var parent = TestUtils.createTab(null,null,module.id);
            var grid,
                store = blockScope.blockStore,

                gridArgs = {
                    ctx:ctx,
                    blockScope: blockScope,
                    blockGroup: 'click',
                    attachDirect:true,
                    collection: store.filter({
                        group: "click"
                    })

                };
            var gridClass = declare('sdfdf ' + new Date().getTime(),BlockGrid,{
                postMixInProperties: function() {


                    var _has = false;

                    var self = this;
                    _.each(this.columns, function (col) {
                        if (col.field == 'order') {
                            _has = true;
                        }
                    });

                    if (!_has && this.blockGroup !== 'basicVariables') {

                        this.columns.unshift({
                            //renderExpando: true,
                            label: "Line",
                            field: "order",
                            sortable: false,
                            formatter: function(val,b){
                                return xide.utils.fff(val,b,self);
                            }
                        });
                    }
                    this.inherited(arguments);
                },
                formatOrder:function(value,block){



                    /*
                    if(block) {
                        var row = this.row(block);
                        if (row && row.element) {
                            return 'rendered';
                        } else {
                            console.log('not rendered yet2');
                            return 'asdfsd';
                        }



                    }else{
                        console.log('-have no block');
                    }

                    if(this._map) {
                        if (this._map[block.id]) {
                            return this._map[block.id];
                        }
                    }else{
                        //console.log('have no map');
                    }
                    */
                    return '0';

                }
            });


            grid = utils.addWidget(gridClass,gridArgs,null,parent,true);


            ctx.getWindowManager().registerView(grid,true);




            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {
                doGridTest(grid,blockScope);
                return;

            }

            function test2() {
                return;
            }

            setTimeout(function () {
                test();
            }, 1000);


            setTimeout(function () {
                test2();
            }, 2000);

        }
    }

    return Grid;

});