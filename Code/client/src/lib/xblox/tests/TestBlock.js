/**
 * @param Block {module:xblox/model/Block}
 * @param types {module:xide/types/Types}
 * @param utils {module:xide/utils}
 * @param _ {module:xide/lodash}
 * @param BlockFactory {module:xblox/factory/Blocks}
 * @param Command {module:xcf/model/Command}
 */
function loaded(Block, types, utils, _, BlockFactory, Command) {
    /*
     * playground
     */
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root,
        scope,
        blockManager,
        driverManager,
        marantz;


    if (ctx) {

        console.clear();


        var blockManager = ctx.getBlockManager();
        var scope = blockManager.toScope([]);

        /**
         * @type {module:xide/types/BLOCK_CAPABILITIES}
         */
        var BLOCK_CAPABILITIES = types.BLOCK_CAPABILITIES;
        var TopMost = BlockFactory.createBlock(Block, {
            scope: scope,
            /**
             * @type {module:xide/types/BLOCK_CAPABILITIES}
             */
            capabilities:(BLOCK_CAPABILITIES.CHILDREN)
        });

        var canChildren = (TopMost.capabilities & BLOCK_CAPABILITIES.CHILDREN);

        console.log('can children ',canChildren);

    }
}

/** @module xblox/types
 *  @description All the package's constants and enums in C style structures.
 */
define([
    'xblox/model/Block',
    'xide/types',
    'xide/utils',
    'xide/lodash',
    'xblox/factory/Blocks',
    'xcf/model/Command'
],loaded);