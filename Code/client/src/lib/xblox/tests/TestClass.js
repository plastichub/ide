/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/dom-class',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/ThumbRenderer',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    "xblox/widgets/BlockGridRowEditor",

    'dgrid/Editor',


    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',

    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',

    'xide/factory',


    'dijit/Menu',

    'xide/data/Reference',

    'dijit/form/DropDownButton',
    'dijit/MenuItem',

    'xdocker/Docker2',

    "xide/views/CIViewMixin",

    'xide/layout/TabContainer',


    "dojo/has!host-browser?xblox/views/BlockEditDialog",

    'xblox/views/BlockGrid',

    'xgrid/DnD',
    'xblox/views/BlocksGridDndSource',
    'xblox/widgets/DojoDndMixin',


    'xide/registry',

    'dojo/topic',
    'xide/tests/TestUtils',
    './TestUtils',
    'module'


], function (declare, domClass,types,
             utils, ListRenderer, TreeRenderer, ThumbRenderer,
             _ActionMixin,
             Grid, MultiRenderer, RadioButton, Ribbon, Registry, DefaultActions, Action, BlockGridRowEditor,
             Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Menu,Reference,DropDownButton,
             MenuItem,Docker,CIViewMixin,TabContainer,

             BlockEditDialog,
             BlockGrid,
             Dnd,BlocksGridDndSource,DojoDndMixin,
             registry,topic,TestUtils,BlockUtils,module
    ) {



    /**
     *
     */
    var _layoutMixin = {
        _docker:null,
        _parent:null,
        __right:null,
        getDocker:function(container){

            if(!this._docker){

                var _dst = container || this._domNode.parentNode,
                    thiz = this;

                thiz._docker = Docker.createDefault(_dst);
                thiz._oldParent = thiz._parent;

                var parent = thiz._docker.addPanel('DefaultFixed', types.DOCKER.TOP, null, {
                    w: '100%',
                    title:'  '
                });

                dojo.place(thiz._domNode,parent.containerNode);

                thiz._docker.$container.css('top',0);
                thiz._parent = parent;

                parent._parent.showTitlebar(false);


/*
                var right = this._docker.addPanel('Collapsible', types.DOCKER.RIGHT, parent, {
                    w: '20%',
                    title:'Properties'
                });

                parent._parent.showTitlebar(false);*/





                //parent._parent.$center.css('top',0);

            }

            return thiz._docker;
        },
        getRightPanel:function(){

            if(this.__right){
                return this.__right;
            }

            var docker = this.getDocker();


            var right = docker.addPanel('Collapsible', types.DOCKER.RIGHT, this._parent, {
                w: '300px',
                title:'  '
            });



            right._parent.showTitlebar(false);

            var splitter = right.getSplitter();

            splitter.pos(0.6);


            this.__right = right;

            return right;
        }
    };

    var DojoDndMixin  = declare("xblox.widgets.DojoDndMixin",null,{
        dropEvent:"/dnd/drop",
        dragEvent:"/dnd/start",
        overEvent:"/dnd/source/over",
        _eDrop:false,
        isDragging:false,
        didInit:false,
        /***
         * Pre-Process DND Events
         * @param node
         * @param targetArea
         * @param indexChild
         */
        _calcNewItemList:function(items){
            var res = [];
            if(items){

                for(var i=0 ; i<items.length ; i++){
                    var widget =registry.getEnclosingWidget(items[i].item.node);
                    if(widget){
                        widget.item.dndCurrentIndex=i;
                        res.push(widget);
                    }
                }
            }
            return res;
        },
        _onOver:function(node, source){

            if(this.isDragging /*this.onOver && node*/){
                var widget=null;
                if(source){
                    widget = registry.getEnclosingWidget(source[0]);
                }
                console.log('on over : ' );
                /*this.onOver(widget,node);*/
            }

        },
        _onDrag:function(node, source){

            this.isDragging=true;
            /*
             if(this.onDrag){
             if(source && source[0]){
             var widget = registry.getEnclosingWidget(source[0]);
             this.onDrag(widget,node);
             }
             }
             */
        },
        checkAcceptance:function(sources,nodes){

            return false;
            if(!sources||!nodes){
                return;
            }
            this.setupDND();

            var node = nodes[0];

            var row = this.grid.row(node);
            if(!row || !row.data){
                return;
            }
            var item = row.data;

            //console.log('acce : ' + item.name);

            return true;
        },
        onDrop: function(source, nodes, copy,target){

            if(!source||!nodes){
                return;
            }
            this.isDragging=false;
            var node = nodes[0];
            var item = null;
            var dstItem = null;
            var isTree = false;
            var didNonTree=false;
            var didTree=false;
            var grid = this.grid;
            var itemToFocus;

            if(source.tree && source.anchor && source.anchor.item && target){

                item    = source.anchor.item;
                isTree  = true;

                var dstNode = target.current;
                if(!dstNode){

                    console.warn('have dstNode in target.current');

                    if (grid.onDrop) {
                        console.log('drop on nothing');
                        grid.onDrop(item, {
                            parentId:null
                        }, false, grid, source.targetState, false);
                    }

                    return;
                }
                var dstRow = grid.row(dstNode);
                if(!dstRow){
                    console.warn('have now row');
                    return;
                }
                dstItem = dstRow.data;
                if(!dstItem){
                    console.warn('have no dstItem');
                    return;
                }

                try {

                    if (grid.onDrop) {
                        itemToFocus = item;
                        grid.onDrop(item, dstItem, target.before, grid, source.targetState, target.hover);
                        didTree = true;
                    }
                }catch(e){
                    debugger;
                }



            }else{






                var row=grid.row(node);

                if(!row || !row.data){
                    return;
                }
                item = row.data;



                var dstNode = source.current;
                if(!dstNode){
                    console.warn('have no dstNode in source.current, unparent');
                    var parent = item.getParent();

                    if(parent) {
                        item.unparent(grid.blockGroup, false);
                    }
                    grid.refreshAll([item]);

                    return;
                }
                var dstRow = this.grid.row(dstNode);
                if(!dstRow){
                    console.warn('have now row');
                    return;
                }
                dstItem = dstRow.data;
                if(!dstItem){
                    console.warn('have no dstItem');
                    return;
                }

                try {
                    if (grid.onDrop) {
                        itemToFocus = item;
                        grid.onDrop(item, dstItem, source.before, grid, source.sourceState, source.hover);
                        didNonTree = true;
                    }
                }catch(e){
                    console.error('error grid:onDrop:',e);
                    console.trace();
                }
            }

            if(didNonTree==false && didTree==false){
                console.dir(arguments);
                console.log('something wrong')
            }else{
                console.dir(arguments);
                console.log('dropping : ' + item.name  + ' into ' + dstItem.name + ' before = ' + source.before);
                console.log('didNonTree ' + didNonTree + ' didTree ' + didTree);
                grid.refreshAll([itemToFocus]);

            }
        },
        setupDND:function(){


            var thiz = this;

            if(this.didInit){
                return;
            }
            this.didInit=true;

            topic.subscribe(this.dragEvent, function(node, targetArea, indexChild){
                thiz._onDrag(node, targetArea, indexChild);
            });
            if(this.overEvent){
                topic.subscribe(this.overEvent, function(node, targetArea, indexChild){
                    thiz._onOver(node, targetArea, indexChild);
                });
            }
            topic.subscribe(this.dropEvent, function(source, nodes, copy, target){
                thiz._onDrop(source, nodes, copy, target);
            });
        }
    });

    var SharedDndGridSource = declare([BlocksGridDndSource, DojoDndMixin], {
        /*
        blockScope: this.blockScope,
        delegate: this.delegate,*/
        onDndDrop: function (source, nodes, copy, target) {
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, "Hover");
            }
            if (this == target) {
                // this one is for us => move nodes!
                this.onDrop(source, nodes, copy, target);
            }
            this.onDndCancel();
        }
    });

    function getFileActions(permissions) {



        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();


        return [];

        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            var action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        var who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }

        /*
         var rootAction = 'Block/Insert';
         permissions.push(rootAction);
         addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         dummy: true,
         onCreate: function (action) {
         action.setVisibility(VISIBILITY.CONTEXT_MENU, {
         label: 'Add'
         });

         }
         }, null, null);
         permissions.push('Block/Insert Variable');


         addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         }, null, null);
         */

        /*
         permissions.push('Clipboard/Paste/New');
         addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
         }, null, null);*/


        var newBlockActions = this.getAddActions();
        var addActions = [];
        var levelName = '';


        function addItems(commandPrefix, items) {

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                levelName = item.name;


                var path = commandPrefix + '/' + levelName;
                var isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }
        //console.clear();
        //addItems(rootAction, newBlockActions);
        //return result;


        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            var canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

            return !canMove;

        }


        function canParent(selection, reference, visibility) {

            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            if(!item){
                console.warn('bad item',selection);
                return false;
            }

            if(this.command === 'Step/Move Left'){
                return !item.getParent();
            }else{
                return item.getParent();
            }
            /*
             var canMove = item.canMove(item, this.command === 'Step/Move Left' ? -1 : 1);
             return !canMove;*/

            return true;

        }

        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */

        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    widgetArgs:{
                        label: ' ',
                        style:'font-size:25px!important;'
                    }
                });

            }
        }, null, isItem);
        permissions.push('Step/Run/From here');

        /**
         * run
         */

        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {

            onCreate: function (action) {

            }
        }, null, isItem);



        /**
         * move
         */

        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['alt up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);


        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['alt down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);
        /*


         permissions.push('Step/Edit');
         addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
         */
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        permissions.push('Step/Move Left');
        permissions.push('Step/Move Right');

        addAction('Move Left', 'Step/Move Left', 'fa-arrow-left', ['alt left'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canParent);

        addAction('Move Right', 'Step/Move Right', 'fa-arrow-right', ['alt right'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });


            }
        }, null, canParent);


        return result;

    }

    function doPost(view){
        return;

        var right  = view.getRightPanel();
        var splitter = right.getSplitter();

        //var ori = splitter.orientation();
        //splitter.orientation(false);
        //spl

        //splitter.collapse();
        //splitter.expand();
        splitter.minimize = function(){

            this._isToggledMin = true;

            //save pos
            if(this._isToggledMin){
                this._savedPosMin = this.pos();
            }

            var togglePosValue = this._isToggledMin ? 1 : 0;

            if(togglePosValue==1){
                //togglePosValue = this._savedPosMin;
            }


            //console.log('toggle ' + togglePosValue + ' cvalue ' + this.pos());


            this.pos(togglePosValue);

        }
        //splitter.minimize();

    }

    function clipboardTest(){

        this.select(['root2'],null,true,{
            focus:true
        });

        var actionStore = this.getActionStore();

        var copy = actionStore.getSync('Clipboard/Copy');
        var paste = actionStore.getSync('Clipboard/Paste');

        console.clear();


        this.runAction(copy);
        this.runAction(paste);



        //this.refresh();


    }

    function unparent(){

        this.select(['sub0'],null,true,{
            focus:true
        });
        this.runAction('Step/Move Left');
        this.refresh();
    }

    function reparent(){

        this.select(['root3'],null,true,{
            focus:true
        });
        this.runAction('Step/Move Right');
        this.refresh();
    }

    function expand(){


        this.select(['root3'],null,true,{
            focus:true
        });

        //clipboardTest.apply(this);

        //reparent.apply(this);


        return;



        //var row = grid.row('root');
        //var _t = this._normalize('root');
        //this.expand(_t);
        //var _expanded = this.isExpanded(_t);
        //debugger;
        //this.isRendered('root');
        var store = this.collection;
        //var item = store.getSync('sub0');
        //this._expandTo(item);

        var root = this.collection.getSync('click');


        this.select(['root2','root3'],null,true,{
            focus:true
        });


        var actionStore = this.getActionStore();

        var moveLeft = actionStore.getSync('Step/Move Left');


        var moveUp = actionStore.getSync('Step/Move Up');

        var moveDown = actionStore.getSync('Step/Move Down');

        var items = this.collection.storage.fullData;
        //console.log('before move',items);
        //this.printRootOrder();
        this.runAction(moveUp);
        //console.log('after move',this.collection.storage.fullData);
        //this._place('root','root2','below');
        //this.printRootOrder();


        var thiz = this;

        setTimeout(function(){

            //thiz.refresh();
            //this.runAction(moveDown);
            //thiz.refreshRoot();
            thiz.printRootOrder();
        },1500);

        //this.runAction(moveLeft);
    }



    var _gridBase = {
        startup:function(){

            this.inherited(arguments);

            this._on('selectionChanged',function(evt){

                var selection = evt.selection;
                return;
            })
        }
    };

    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;

    function fixScope(scope){

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function(source,target,before,add){

            console.log('scope::move, add: ' +add,arguments);

            if(!add){
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if(add){

                //remove it from the source parent and re-parent the source
                if(target.canAdd && target.canAdd()){

                    var sourceParent = this.getBlockById(source.parentId);
                    if(sourceParent){
                        sourceParent.removeBlock(source,false);
                    }
                    target.add(source,null,null);
                    return;
                }else{
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if(!target.parentId && add==false){

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if(sourceParent && sourceParent.removeBlock){
                    sourceParent.removeBlock(source,false);
                    source.parentId=null;
                    source.group=target.group;
                }

                var itemsToBeMoved=[];
                var groupItems = this.getBlocks({
                    group:target.group
                });

                var rootLevelIndex=[];
                var store = this.getBlockStore();

                var sourceIndex = store.storage.index[source.id];
                var targetIndex = store.storage.index[target.id];
                for(var i = 0; i<groupItems.length;i++){

                    var item = groupItems[i];
                    //keep all root-level items

                    if( groupItems[i].parentId==null && //must be root
                        groupItems[i]!=source// cant be source
                    ){

                        var itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if(add){
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if(before){
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if(!before){
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            }else if( !source.parentId && target.parentId && add==false){
                source.group = target.group;
                if(target){

                }

                //we move from root to into root item
            }else if( !source.parentId && !target.parentId && add){

                console.error('we are adding an item into root root item');
                if(target.canAdd && target.canAdd()){
                    source.group=null;
                    target.add(source,null,null);
                }
                return true;

                // we move within the same parent
            }else if( source.parentId && target.parentId && add==false && source.parentId === target.parentId){
                console.error('we move within the same parents');
                var parent = this.getBlockById(source.parentId);
                if(!parent){
                    console.error('     couldnt find parent ');
                    return false;
                }

                var maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items,source);
                var cIndexTarget = source.indexOf(items,target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    parent.move(source,direction);
                }
                return true;

                // we move within the different parents
            }else if( source.parentId && target.parentId && add==false && source.parentId !== target.parentId){                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if(!sourceParent){
                    console.error('     couldnt find source parent ');
                    return false;
                }

                var targetParent = this.getBlockById(target.parentId);
                if(!targetParent){
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if(sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()){
                    sourceParent.removeBlock(source,false);
                    targetParent.add(source,null,null);
                }else{
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if(items==null){
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items,source);
                var cIndexTarget = targetParent.indexOf(items,target);
                if(!cIndexSource || !cIndexTarget){
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    targetParent.move(source,direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        var topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId:null
        });


        var grouped = _.groupBy(blocks,function(block){
            return block.group;
        });

        function createDummyBlock(id,scope){

            var block = {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": null,
                    "id": id,
                    "items": [

                    ],
                    "name": id,
                    "method": "----group block ----",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": false,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"

                };

            return scope.blockFromJson(block);

        }

        for(var group in grouped){

            var groupBlock = createDummyBlock(group,scope);
            var blocks = grouped[group];
            _.each(blocks,function(block){
                groupBlock['items'].push(block);



                if(!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }

        console.clear();
        var root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }

    if (ctx) {


        var blockManager = ctx.getBlockManager();

        function createGridClass() {

            var renderers = [TreeRenderer];
            //, ThumbRenderer, TreeRenderer

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);


            var _gridClass = Grid.createGridClass('driverTreeView',
                {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT:{
                        CLASS:declare('splitMixin',null,_layoutMixin)
                    },
                    DND:{
                        CLASS:Dnd//declare('splitMixin',Dnd,_dndMixin)
                    }

                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );



            return declare('gridFinal', BlockGrid, _gridBase);

            //return BlockGrid;
        }
        var blockScope = createScope('docs');
        var mainView = ctx.mainView;
        if (mainView) {
            var parent = TestUtils.createTab('BlockGrid-Test',null,module.id);


            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon;

            var store = blockScope.blockStore;


            var _gridClass = createGridClass();


            var gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                collection: store.filter({
                    group: "click"
                }),
                //dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                dndParams: {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                }
            };


            grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);

            var blocks = blockScope.allBlocks();

            var root = blocks[0];

            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();
            var _defaultActions = [];
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [grid.permissions]));
            grid.addActions(_defaultActions);



            if (!toolbar) {


            } else {
                toolbar.addActionEmitter(grid);
                toolbar.setActionEmitter(grid);
            }

            doPost(grid);

            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {
                return;

            }

            function test2() {
                return;
            }

            setTimeout(function () {
                test();
            }, 1000);


            setTimeout(function () {
                test2();
            }, 2000);

        }
    }

    return Grid;

});