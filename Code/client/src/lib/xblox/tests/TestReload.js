/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/dom-class',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/ThumbRenderer',
    'xide/views/_ActionMixin',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dijit/form/RadioButton',
    'xide/widgets/Ribbon',
    'xide/editor/Registry',
    'xaction/DefaultActions',
    'xaction/Action',
    "xblox/widgets/BlockGridRowEditor",

    'dgrid/Editor',


    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',

    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',

    'xide/factory',


    'dijit/Menu',

    'xide/data/Reference',

    'dijit/form/DropDownButton',
    'dijit/MenuItem',

    'xide/docker/Docker',


    "xide/views/CIViewMixin",

    'xide/layout/TabContainer',


    "dojo/has!host-browser?xblox/views/BlockEditDialog",

    'xblox/views/BlockGrid',

    'xgrid/DnD',
    'xblox/views/BlocksGridDndSource',
    'xblox/widgets/DojoDndMixin',


    'xide/registry',

    'dojo/topic',
    'xide/tests/TestUtils',
    './TestUtils',
    'module'


], function (declare, domClass,types,
             utils, ListRenderer, TreeRenderer, ThumbRenderer,
             _ActionMixin,
             Grid, MultiRenderer, RadioButton, Ribbon, Registry, DefaultActions, Action, BlockGridRowEditor,
             Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Menu,Reference,DropDownButton,
             MenuItem,Docker,CIViewMixin,TabContainer,

             BlockEditDialog,
             BlockGrid,
             Dnd,BlocksGridDndSource,DojoDndMixin,
             registry,topic,TestUtils,BlockUtils,module
    ) {


    var ctx = window.sctx;

    if(ctx){

        var blockManager = ctx.getBlockManager();



        var scope = BlockUtils.createScope(blockManager);





    }



    var Module = declare('xblox.tests.TestUtils',null,{});


    return Module;

});