define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xdocker/Docker2',
    'xblox/views/BlockGrid',
    'xide/tests/TestUtils',
    'xblox/BlockActions',
    'xide/views/_LayoutMixin',
    "xide/widgets/_Widget",
    'dojo/dnd/Source',
    'dojo/dnd/Manager',
    'dojo/_base/NodeList',
    'module',
    'dojo/when',
    'dojo/has!touch?dgrid/util/touch',
    'dojo/topic',
    'xdojo/has',
    'dojo/_base/array',
    'dojo/aspect',
    'dojo/dom-class',
    'xide/factory',
    'xgrid/DnD',
    'dojo/Deferred',
    'xide/registry',
    'xblox/views/ThumbRenderer',
    'dojo/dnd/Manager',
    'dojo/dom-geometry',
    "dojo/dnd/common",
    "xblox/views/DnD",
    'dojo/dom-construct',

    'xgrid/Selection',
    "xide/widgets/_Widget",
    'xgrid/KeyboardNavigation',
    'xaction/ActionStore',
    'xide/factory',
    'xlang/i18'

], function (declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, Editor, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, Docker, BlockGrid,
             TestUtils, BlockActions, _LayoutMixin, _Widget, DnDSource,
             DnDManager, NodeList, module, when, touchUtil, topic, has, arrayUtil,
             aspect, domClass, factory, DnD, Deferred, registry, ThumbRenderer, Manager, domGeom, dnd,BlocksDnD,
             domConstruct,Selection, _XWidget, KeyboardNavigation, ActionStore, factory, i18) {


    var ThumbClass = declare('xfile.ThumbRenderer2', [ThumbRenderer], {
        thumbSize: "400",
        resizeThumb: true,
        __type: 'thumb',
        deactivateRenderer: function () {
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding', '');
            this.isThumbGrid = false;

        },
        activateRenderer: function () {
            $(this.contentNode).css('padding', '8px');
            this.isThumbGrid = true;
            this.refresh();
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            var div = domConstruct.create('div', {
                    className: "tile widget form-control",
                    style: 'margin-left:16px;margin-top:8px;margin-right:8px;width:150px;height:initial;max-width:200px;float:left;padding:8px'
                }),
                icon = obj.icon,
                label = obj.title,
                imageClass = obj.icon || 'fa fa-folder fa-2x';

            var iconStyle = 'float:left; margin-left:3px;margin-top:3px;margin-right:3px;text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: inherit;opacity: 0.4';
            var folderContent = '<span style="' + iconStyle + '" class="' + imageClass + '"></span>';

            div.innerHTML =
                '<div class="opacity">' +
                folderContent +
                '<span class="thumbText text opacity ellipsis" style="text-align:center">' + label + '</span>' +
                '</div>';
            return div;
        }
    });
    var GroupRenderer = declare('xfile.GroupRenderer', TreeRenderer, {
        itemClass: ThumbClass,
        groupClass: TreeRenderer,
        renderRow: function (obj) {
            return (!obj.group ? this.itemClass : this.groupClass).prototype.renderRow.apply(this, arguments);
        }
    });
    var Implementation = {},
        renderers = [GroupRenderer, ThumbClass, TreeRenderer],

        multiRenderer = declare.classFactory('xgrid/MultiRendererGroups', {}, renderers, MultiRenderer.Implementation);





    




    console.clear();
    /**
     * flow  - drop
     * 1. SharedDndGridSource:onDrop ->
     * 1.1. SharedDndGridSource:onDropInternal or SharedDndGridSource:onDropExternal
     * 1.2 SharedDndGridSource:onDropInternal
     * 1.3 DojoDndMixin#onDrop
     * 1.4 Grid#onDrop
     */
    /**
     *
     * @class module:xblox/views/BlockGridDnDSource
     * @extends module:xgrid/DnD/GridDnDSource
     */
    var BlockGridDnDSource = declare(DnD.GridSource, {
        declaredClass: 'xblox/views/BlockGridDnDSource',
        _lastTargetId: null,
        _lastTargetCrossCounter: 0,
        /**
         * Assigns a class to the current target anchor based on "before" status
         * @param before {boolean} insert before, if true, after otherwise
         * @param center {boolean}
         * @param canDrop {boolean}
         * @private
         */
        _markTargetAnchor: function (before, center, canDrop) {
            if (this.current == this.targetAnchor && this.before === before && this.center === center) {
                return;
            }
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, this.before ? "Before" : "After");
                this._removeItemClass(this.targetAnchor, "Disallow");
                this._removeItemClass(this.targetAnchor, "Center");
            }
            this.targetAnchor = this.current;
            this.targetBox = null;
            this.before = before;
            this.center = center;
            this._before = before;
            this._center = center;

            if (this.targetAnchor) {
                if (center) {
                    this._removeItemClass(this.targetAnchor, "Before");
                    this._removeItemClass(this.targetAnchor, "After");
                    this._addItemClass(this.targetAnchor, 'Center');
                }
                !center && this._addItemClass(this.targetAnchor, this.before ? "Before" : "After");
                center && canDrop === false && this._addItemClass(this.targetAnchor, "Disallow");
            }
        },
        /**
         *
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:xide/data/Model}
         */
        onDropInternal: function (nodes, copy, targetItem) {
            var grid = this.grid,
                store = grid.collection,
                targetSource = this,
                anchor = targetSource._targetAnchor,
                targetRow,
                nodeRow;

            if (anchor) { // (falsy if drop occurred in empty space after rows)
                targetRow = this._center ? anchor : this.before ? anchor.previousSibling : anchor.nextSibling;
            }
            // Don't bother continuing if the drop is really not moving anything.
            // (Don't need to worry about edge first/last cases since dropping
            // directly on self doesn't fire onDrop, but we do have to worry about
            // dropping last node into empty space beyond rendered rows.)
            nodeRow = grid.row(nodes[0]);
            if (!copy && (targetRow === nodes[0] || (!targetItem && nodeRow && grid.down(nodeRow).element === nodes[0]))) {
                return;
            }
            var rows = grid.__dndNodesToModel(nodes);
            var _target = grid._sourceToModel(targetSource, grid);
            var DND_PARAMS = {
                before: this._before,
                center: this._center,
                targetGrid:grid
            };

            if(this._center){
                grid.__onDrop(rows, _target, grid, DND_PARAMS, copy);
            }
            nodes.forEach(function (node) {
                when(targetSource.getObject(node), function (object) {
                    var id = store.getIdentity(object);
                    // For copy DnD operations, copy object, if supported by store;
                    // otherwise settle for put anyway.
                    // (put will relocate an existing item with the same id, i.e. move).
                    grid._trackError(function () {
                        return store[copy && store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {

                            // Self-drops won't cause the dgrid-select handler to re-fire,
                            // so update the cached node manually
                            if (targetSource._selectedNodes[id]) {
                                targetSource._selectedNodes[id] = grid.row(id).element;
                            }
                            !this.center && grid.__onDrop(rows, _target, grid, DND_PARAMS, copy);
                        });
                    });
                });
            });
        },
        /**
         * @param sourceSource {module:dojo/dnd/Source}
         * @param nodes {HTMLElement[]}
         * @param copy {boolean}
         * @param targetItem {module:dojo/dnd/Target}
         */
        onDropExternal: function (sourceSource, nodes, copy, targetItem) {
            // Note: this default implementation expects that two grids do not
            // share the same store.  There may be more ideal implementations in the
            // case of two grids using the same store (perhaps differentiated by
            // query), dragging to each other.
            var grid = this.grid,
                store = this.grid.collection,
                sourceGrid = sourceSource.grid,
                targetSource = this;

            function done(grid,object){
                grid.refresh();
                return grid.select(object, null, true, {
                    focus: true,
                    append: false,
                    expand: true,
                    delay: 2
                },'mouse');
            }

            nodes.forEach(function (node, i) {
                when(sourceSource.getObject(node), function (object) {
                    var DND_PARAMS = {
                        before: targetSource._before,
                        center: targetSource._center,
                        targetGrid:grid
                    };
                    if(object.toDropObject){
                        object  = object.toDropObject(object,DND_PARAMS.center ? sourceGrid._sourceToModel(targetSource) : targetItem,DND_PARAMS,copy);
                        if(DND_PARAMS.center){
                            return done(grid,object);
                        }
                    }
                    // Copy object, if supported by store; otherwise settle for put
                    // (put will relocate an existing item with the same id).
                    // Note that we use store.copy if available even for non-copy dnd:
                    // since this coming from another dnd source, always behave as if
                    // it is a new store item if possible, rather than replacing existing.
                    grid._trackError(function () {
                        return store[store.copy ? 'copy' : 'put'](object, {
                            beforeId: targetItem ? store.getIdentity(targetItem) : null
                        }).then(function () {
                            if (!copy) {
                                if (sourceGrid) {
                                    // Remove original in the case of inter-grid move.
                                    // (Also ensure dnd source is cleaned up properly)
                                    var id = sourceGrid.collection.getIdentity(object);
                                    !i && sourceSource.selectNone(); // Deselect all, one time
                                    sourceSource.delItem(node.id);
                                    done(grid,object);
                                    return sourceGrid.collection.remove(id);
                                }
                                else {
                                    sourceSource.deleteSelectedNodes();
                                }
                            }
                        });
                    });
                });
            });
        }
    });

    function debugStoreItem(item) {
        if (item) {
            var parent = item.getParent() || {};
            console.log("Item : " + item.name + ' | ' + parent.name);
        }
    }
    /**
     * @class module:xblox/views/DojoDndMixin
     */
    var DojoDndMixin = declare("xblox.widgets.DojoDndMixin", null, {
        declaredClass: 'xblox.widgets.DojoDndMixin',
        dropEvent: "/dnd/drop",
        dragEvent: "/dnd/start",
        overEvent: "/dnd/source/over",
        /**
         * Final
         * @param sources {module:xblox/model/Block[]}
         * @param target {module:xblox/model/Block}
         * @param sourceGrid {module:xgrid/Base| module:dgrid/List}
         * @param params {object}
         * @param params.SAME_STORE {object}
         * @param copy {boolean}
         * @private
         */
        __onDrop: function (sources, target, sourceGrid, params, copy) {
            var into = params.center === true;
            _.each(sources, function (source) {
                if(source.toDropObject){
                    source  = source.toDropObject(source,target,params,copy);
                }
                //happens when dragged on nothing
                if (source == target) {
                    return;
                }
                if (into) {
                    if (target.canAdd(source) !== false) {
                        target.scope.moveTo(source, target, params.before, into);
                    }
                } else {
                    var sourceParent = source.getParent();
                    if (sourceParent) {
                        var targetParent = target.getParent();
                        if (!targetParent) {
                            //move to root - end
                            source.group = this.blockGroup;
                            sourceParent.removeBlock(source, false);
                            source._store.putSync(source);
                        } else {
                            //we dropped at the end within the same tree
                            if (targetParent == source.getParent()) {
                                target.scope.moveTo(source, target, params.before, into);
                            }
                        }
                    }
                }
                source.refresh();
            }, this);
            
            this.set('collection', this.collection.filter({
                group: "click"
            }));

            this.select(sources[0], null, true, {
                focus: true,
                append: false,
                expand: true,
                delay: 2
            },'mouse');

        },
        /**
         *
         * @param source {module:dojo/dnd/Source}
         * @returns {module:xblox/model/Block}
         * @returns {module:xgrid/Base}
         * @private
         */
        _sourceToModel: function (source, grid) {
            var result = null;
            if (source) {

                var anchor = source._targetAnchor || source.anchor || source;
                grid = grid || source.grid || this;
                if (!anchor || !anchor.ownerDocument) {
                    return null;
                }
                var row = grid.row(anchor);
                if (row) {
                    return row.data;
                }
                if (anchor) {
                    result = grid.collection.getSync(anchor.id.slice(grid.id.length + 5));
                    if (!result) {
                        result = grid.row(anchor);
                    }
                }
            }
            return result;
        },
        __dndNodesToModel: function (nodes) {
            return _.map(nodes, function (n) {
                return (this.row(n) || {}).data;
            }, this);
        },
        startup:function(){
            if(this._started){
                return;
            }
            //this.dropEvent = null;
            /*
            this.dropEvent && this.subscribe(this.dropEvent, function (source, nodes, copy, target) {
                target = target || {};
                var sourceGrid = source.grid;
                var targetGrid = target.grid;
                if(targetGrid==this || sourceGrid==this){
                    var rows = sourceGrid.__dndNodesToModel(nodes);
                    var _target = targetGrid._sourceToModel(target, target.grid);
                    if (!_target || _.isEmpty(rows)) {
                            return;
                        }
                        var DND_PARAMS = {
                            SAME_STORE: rows[0]._store == _target._store,
                            before: target._before,
                            center: target._center,
                            targetGrid:targetGrid
                        };
                        if (rows && _target) {
                            targetGrid.__onDrop(rows, _target, sourceGrid, DND_PARAMS, copy);
                        }
                    }
            });
            */
            return this.inherited(arguments);
        }
    });
    // Mix in Selection for more resilient dnd handling, particularly when part
    // of the selection is scrolled out of view and unrendered (which we
    // handle below).
    DnD = declare('xblox.dnd', [DojoDndMixin], {
        // dndSourceType: String
        //		Specifies the type which will be set for DnD items in the grid,
        //		as well as what will be accepted by it by default.
        dndSourceType: 'dgrid-row',
        // dndParams: Object
        //		Object containing params to be passed to the DnD Source constructor.
        dndParams: null,
        // dndConstructor: Function
        //		Constructor from which to instantiate the DnD Source.
        //		Defaults to the GridSource constructor defined/exposed by this module.
        dndConstructor: BlockGridDnDSource,
        postMixInProperties: function () {
            this.inherited(arguments);
            // ensure dndParams is initialized
            this.dndParams = utils.mixin({accept: [this.dndSourceType]}, this.dndParams);
        },
        postCreate: function () {
            this.inherited(arguments);
            // Make the grid's content a DnD source/target.
            var Source = this.dndConstructor || BlockGridDnDSource;
            var dndParams = utils.mixin(this.dndParams, {
                // add cross-reference to grid for potential use in inter-grid drop logic
                grid: this,
                dropParent: this.contentNode
            });
            if (typeof this.expand === 'function') {
                // If the Tree mixin is being used, allowNested needs to be set to true for DnD to work properly
                // with the child rows.  Without it, child rows will always move to the last child position.
                dndParams.allowNested = true;
            }
            this.dndSource = new Source(this.bodyNode, dndParams);

            // Set up select/deselect handlers to maintain references, in case selected
            // rows are scrolled out of view and unrendered, but then dragged.
            var selectedNodes = this.dndSource._selectedNodes = {};

            function selectRow(row) {
                selectedNodes[row.id] = row.element;
            }

            function deselectRow(row) {
                delete selectedNodes[row.id];
                // Re-sync dojo/dnd UI classes based on deselection
                // (unfortunately there is no good programmatic hook for this)
                domClass.remove(row.element, 'dojoDndItemSelected dojoDndItemAnchor');
            }

            this.on('dgrid-select', function (event) {
                arrayUtil.forEach(event.rows, selectRow);
            });
            this.on('dgrid-deselect', function (event) {
                arrayUtil.forEach(event.rows, deselectRow);
            });

            aspect.after(this, 'destroy', function () {
                delete this.dndSource._selectedNodes;
                selectedNodes = null;
                this.dndSource.destroy();
            }, true);
        },
        insertRow: function (object) {
            // override to add dojoDndItem class to make the rows draggable
            var row = this.inherited(arguments),
                type = typeof this.getObjectDndType === 'function' ?
                    this.getObjectDndType(object) : [this.dndSourceType];

            domClass.add(row, 'dojoDndItem');
            this.dndSource.setItem(row.id, {
                data: object,
                type: type instanceof Array ? type : [type]
            });
            return row;
        },
        removeRow: function (rowElement) {
            this.dndSource.delItem(this.row(rowElement));
            this.inherited(arguments);
        }
    });

    DnD.GridSource = BlockGridDnDSource;

    function debugSource(source) {
        console.log('\t Source : ' + source.declaredClass + ' | before: ' + source._before + ' | ContainerState = ' + source.containerState + ' | TargetState =' + source.targetState + ' | SourceState ' + source.sourceState + ' | center = ' + source._center, source);
    }
    function debugTarget(source) {
        console.log('\t Target : ' + source.declaredClass + ' | before: ' + source._before + ' | ContainerState = ' + source.containerState + ' | TargetState =' + source.targetState + ' | SourceState ' + source.sourceState + ' | center = ' + source._center, source);
    }


    /**
     * @class module:xblox/views/SharedDndGridSource
     * @extends module:xblox/views/BlockGridDnDSource
     * @extends module:xblox/views/DojoDndMixin
     */
    var SharedDndGridSource = declare([DnD.GridSource], {
        autoSync: true,
        skipForm: true,
        selfAccept: false,
        onDndDrop: function (source, nodes, copy, target) {
            if (this == target) {
                // this one is for us => move nodes!
                this.onDrop(source, nodes, copy, target);
                this.grid.refresh();
            }
            this.onDndCancel();
        }
    });



    var GridClass = Grid.createGridClass('xfile.views.Grid', Implementation, {
            SELECTION: {
                CLASS: Selection
            },
            KEYBOARD_SELECTION: true,
            CONTEXT_MENU: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            WIDGET: {
                CLASS: _XWidget
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            Dnd: {
                CLASS: DnD
            }
        },
        {
            RENDERER: multiRenderer
        },
        {
            renderers: renderers,
            selectedRenderer: GroupRenderer
        }
    );
    var GridCSourcelass = declare('ActionGrid', GridClass, {
        formatColumn: function (field, value, obj) {
            var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
            if (renderer.formatColumn_) {
                var result = renderer.formatColumn.apply(arguments);
                if (result) {
                    return result;
                }
            }
            if (obj.renderColumn_) {
                var rendered = obj.renderColumn.apply(this, arguments);
                if (rendered) {
                    return rendered;
                }
            }
            switch (field) {
                case "title": {
                    value = obj.group ? ('<span style="float:left;margin-right: 10px">' + value + '</span><hr style="margin-top: 13px;margin-left: 4px"/>') : value;
                }
            }
            return value;
        },
        _columns: {},
        postMixInProperties: function () {
            var state = this.state;
            if (state) {
                if (state._columns) {
                    this._columns = state._columns;
                }
            }
            this.columns = this.getColumns();
            var newBlockActions = this.getAddActions();
            var levelName = '';
            var result = [];
            var BLOCK_INSERT_ROOT_COMMAND = '';
            var defaultMixin = {addPermission: true};
            var rootAction = BLOCK_INSERT_ROOT_COMMAND;
            var thiz = this;

            function addItems(commandPrefix, items) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    levelName = item.name;
                    var path = commandPrefix ? commandPrefix + '/' + levelName : levelName;
                    var isContainer = !_.isEmpty(item.items);
                    result.push(thiz.createAction({
                        label: levelName,
                        command: path,
                        icon: item.iconClass,
                        tab: 'Home',
                        mixin: utils.mixin({
                            item: item,
                            parent: isContainer ? "root" : commandPrefix,
                            quick: true,
                            title: i18.localize(levelName),
                            group: isContainer,
                            icon: item.iconClass,
                            toDropObject:function(item,targetItem,params){
                                var blockArgs = item.item;
                                var ctrArgs = blockArgs.ctrArgs;
                                ctrArgs.id = null;
                                ctrArgs.items = null;
                                var proto = blockArgs.proto;
                                var parent = params.center === true && targetItem ? targetItem : null;
                                if(parent) {
                                    var block = factory.createBlock(proto, ctrArgs);//root block
                                    return parent.add(block, null, null);

                                }else{
                                    ctrArgs.group = params.targetGrid.blockGroup;
                                }
                                return factory.createBlock(proto, ctrArgs);//root block
                            }
                        }, defaultMixin)

                    }));

                    if (isContainer) {
                        addItems(path, item.items);
                    }
                }
            }

            addItems(rootAction, newBlockActions);
            var actionStore = new ActionStore({
                parentProperty: 'parent',
                parentField: 'parent',
                idProperty: 'command',
                mayHaveChildren: function (parent) {
                    if (parent._mayHaveChildren === false) {
                        return false;
                    }
                    return true;
                }
            });
            actionStore.setData(result);
            var all = actionStore.query(null);
            _.each(all, function (item) {
                item._store = actionStore;
            });
            var $node = $(this.domNode);
            $node.addClass('xFileGrid metro welcomeGrid');
            $node.css('height', '30px');
            this.collection = actionStore.filter({
                parent: "root"
            });

            return this.inherited(arguments);
        },
        getColumns: function (formatters) {
            var self = this;
            this.columns = [
                {
                    renderExpando: true,
                    label: 'Name',
                    field: 'title',
                    sortable: true,
                    formatter: function (value, obj) {
                        return self.formatColumn('title', value, obj);
                    },
                    hidden: false
                }
            ];
            return this.columns;
        },
        /**
         *
         * @param item
         * @returns {Array}
         */
        getAddActions: function (item) {
            var thiz = this;
            item = item || {};
            var items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
            //tell everyone
            thiz.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this,
                item: item,
                group: this.blockGroup,
                scope: this.blockScope
            });

            thiz._emit(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                items: items,
                view: this,
                owner: this
            });
            return items;


        },
        dropEvent:null,
        startup: function () {
            //this.collection = actionStore;
            this._showHeader(false);
            var res = this.inherited(arguments);
            return res;
        }
    });

    /***
     * playground
     */
    var ctx = window.sctx,
        root;

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "icon": 'fa-bell text-fatal',
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0
                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    'icon': 'fa-play'

                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "group": "click",
                    "id": "6a12d54a-f5af-aaf8-0abb-bff866211fc4",
                    "declaredClass": "xblox.model.logic.SwitchBlock",
                    "name": "Switch",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "order": 0,
                    "type": "added"
                },
                {
                    "_containsChildrenIds": [],
                    "group": "click",
                    "id": "8e1c9d2a-d2fe-356f-d484-82e27c793dc9",
                    "declaredClass": "xblox.model.variables.VariableSwitch",
                    "name": "Switch on Variable",
                    "icon": "",
                    "variable": "PowerState",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "order": 0,
                    "type": "added"
                }
            ],
            "variables": []
        };
        return blockManager.toScope(data);
    }

    if (ctx) {

        var blockManager = ctx.getBlockManager();


        function createGridClass() {

            var renderers = [TreeRenderer, ThumbRenderer];

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var _gridClass = Grid.createGridClass('driverTreeView', {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT: {
                        CLASS: _LayoutMixin
                    },
                    Dnd: {
                        CLASS: DnD
                    },
                    Base: {
                        CLASS: declare('Base', null, BlockGrid.IMPLEMENTATION)
                    },
                    BLOCK_ACTIONS: {
                        CLASS: BlockActions
                    },
                    WIDGET: {
                        CLASS: _Widget
                    }
                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );
            //return declare('gridFinal', BlockGrid, _gridClass);
            return _gridClass;
        }

        var blockScope = createScope('docs');
        var mainView = ctx.mainView;
        if (mainView) {
            var parent = TestUtils.createTab('BlockGrid-Test-Source', null, module.id);
            var target = TestUtils.createTab('BlockGrid-Test-Target', null, module.id + '_target', parent, types.DOCKER.TAB.BOTTOM, types.DOCKER.DOCK.BOTTOM);
            var thiz = this,
                grid, grid2;

            var store = blockScope.blockStore;

            var _gridClass = createGridClass();
            var dndParams = {
                allowNested: true, // also pick up indirect children w/ dojoDndItem class
                checkAcceptance: function (source, nodes) {
                    return true;//source !== this; // Don't self-accept.
                },
                isSource: true
            };
            var gridArgs = {
                ctx: ctx,
                /**
                 * @type {module:xblox/views/SharedDndGridSource}
                 */
                dndConstructor: SharedDndGridSource, // use extension defined above
                blockScope: blockScope,
                blockGroup: 'click',
                dndParams: dndParams,
                name: "Grid - 1",
                collection: store.filter({
                    group: "click"
                })
            };
            var gridArgs2 = {
                options: utils.clone(types.DEFAULT_GRID_OPTIONS),
                ctx: ctx
            };

            var gridArgs2 = {
                ctx: ctx,
                /**
                 * @type {module:xblox/views/SharedDndGridSource}
                 */
                dndConstructor: SharedDndGridSource, // use extension defined above
                blockScope: blockScope,
                attachDirect: true,
                name: "Grid - 2",
                dndParams: {
                    allowNested: false, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return false;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                }
            };

            grid = utils.addWidget(_gridClass, gridArgs, null, parent, true);

            //grid2 = utils.addWidget(_gridClass, gridArgs2, null, target, true);

            grid2 = utils.addWidget(GridCSourcelass, gridArgs2, null, target, true);


            ctx.getWindowManager().registerView(grid);
            //grid2 && ctx.getWindowManager().registerView(grid2);


            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {
                return;
            }

        }
    }

    return Grid;

});