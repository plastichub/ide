/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xaction/DefaultActions',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xdocker/Docker2',
    'xblox/views/BlockGrid',
    'xgrid/DnD',
    'xblox/views/BlocksGridDndSource',
    'xblox/widgets/DojoDndMixin',
    'xide/registry',
    'dojo/topic',
    'xide/tests/TestUtils',
    'module'

], function (declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, DefaultActions, Editor, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, Docker, BlockGrid,
             Dnd, BlocksGridDndSource, DojoDndMixin,
             registry, topic, TestUtils, module) {


    /**
     *
     */
    var _layoutMixin = {
        _docker: null,
        _parent: null,
        __right: null,
        getDocker: function (container) {

            if (!this._docker) {

                var _dst = container || this._domNode.parentNode,
                    thiz = this;

                thiz._docker = Docker.createDefault(_dst);
                thiz._oldParent = thiz._parent;

                var parent = thiz._docker.addPanel('DefaultFixed', types.DOCKER.TOP, null, {
                    w: '100%',
                    title: '  '
                });

                dojo.place(thiz._domNode, parent.containerNode);

                thiz._docker.$container.css('top', 0);
                thiz._parent = parent;

                parent._parent.showTitlebar(false);


                /*
                 var right = this._docker.addPanel('Collapsible', types.DOCKER.RIGHT, parent, {
                 w: '20%',
                 title:'Properties'
                 });

                 parent._parent.showTitlebar(false);*/


                //parent._parent.$center.css('top',0);

            }

            return thiz._docker;
        },
        getRightPanel: function () {

            if (this.__right) {
                return this.__right;
            }

            var docker = this.getDocker();


            var right = docker.addPanel('Collapsible', types.DOCKER.RIGHT, this._parent, {
                w: '300px',
                title: '  '
            });


            right._parent.showTitlebar(false);

            var splitter = right.getSplitter();

            splitter.pos(0.6);


            this.__right = right;

            return right;
        }
    };

    function getFileActions(permissions) {


        var result = [],
            ACTION = types.ACTION,
            ACTION_ICON = types.ACTION_ICON,
            VISIBILITY = types.ACTION_VISIBILITY,
            thiz = this,
            actionStore = thiz.getActionStore();


        return [];

        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            var action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        var who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }

        /*
         var rootAction = 'Block/Insert';
         permissions.push(rootAction);
         addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         dummy: true,
         onCreate: function (action) {
         action.setVisibility(VISIBILITY.CONTEXT_MENU, {
         label: 'Add'
         });

         }
         }, null, null);
         permissions.push('Block/Insert Variable');


         addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         }, null, null);
         */

        /*
         permissions.push('Clipboard/Paste/New');
         addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
         }, null, null);*/


        var newBlockActions = this.getAddActions();
        var addActions = [];
        var levelName = '';


        function addItems(commandPrefix, items) {

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                levelName = item.name;


                var path = commandPrefix + '/' + levelName;
                var isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }

        //console.clear();
        //addItems(rootAction, newBlockActions);
        //return result;


        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            var canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

            return !canMove;

        }


        function canParent(selection, reference, visibility) {

            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            var item = selection[0];
            if (!item) {
                console.warn('bad item', selection);
                return false;
            }

            if (this.command === 'Step/Move Left') {
                return !item.getParent();
            } else {
                return item.getParent();
            }
            /*
             var canMove = item.canMove(item, this.command === 'Step/Move Left' ? -1 : 1);
             return !canMove;*/

            return true;

        }

        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */

        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    widgetArgs: {
                        label: ' ',
                        style: 'font-size:25px!important;'
                    }
                });

            }
        }, null, isItem);
        permissions.push('Step/Run/From here');

        /**
         * run
         */

        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {

            onCreate: function (action) {

            }
        }, null, isItem);


        /**
         * move
         */

        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['alt up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);


        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['alt down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);
        /*


         permissions.push('Step/Edit');
         addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
         */
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        permissions.push('Step/Move Left');
        permissions.push('Step/Move Right');

        addAction('Move Left', 'Step/Move Left', 'fa-arrow-left', ['alt left'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canParent);

        addAction('Move Right', 'Step/Move Right', 'fa-arrow-right', ['alt right'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });


            }
        }, null, canParent);


        return result;

    }

    function clipboardTest() {


        /*
         this.select(['root2'], null, true, {
         focus: true,
         expand:false
         });
         */

        this._on('selectionChanged',function(e){


            var selection = e.selection;
            if(selection && selection[0]) {


                var item = selection[0];
                var scope = item.scope;

                var targetParent = scope.getBlockById(item.parentId) || {};

//                console.log('selected '+item.id,item);
                console.log('selected ' + item.name + ' belongs to ' + targetParent.name + ' | ' + item.id +'  ==  ' + targetParent.id);
                var items = item.items;
                //console.log('items', items);


            }

        });

        this.refresh();

        var actionStore = this.getActionStore();
        var copy = actionStore.getSync(types.ACTION.CLIPBOARD_COPY);
        var paste = actionStore.getSync(types.ACTION.CLIPBOARD_PASTE);

        //console.clear();

        return;

        this.runAction(copy);
        this.runAction(paste);

    }

    function unparent() {

        this.select(['sub0'], null, true, {
            focus: true
        });
        this.runAction('Step/Move Left');
        this.refresh();
    }

    function reparent() {

        this.select(['root3'], null, true, {
            focus: true
        });
        this.runAction('Step/Move Right');
        this.refresh();
    }

    function expand() {


        this.select(['root3'], null, true, {
            focus: true
        });

        //clipboardTest.apply(this);

        //reparent.apply(this);


        return;


        //var row = grid.row('root');
        //var _t = this._normalize('root');
        //this.expand(_t);
        //var _expanded = this.isExpanded(_t);
        //debugger;
        //this.isRendered('root');
        var store = this.collection;
        //var item = store.getSync('sub0');
        //this._expandTo(item);

        var root = this.collection.getSync('click');


        this.select(['root2', 'root3'], null, true, {
            focus: true
        });


        var actionStore = this.getActionStore();

        var moveLeft = actionStore.getSync('Step/Move Left');


        var moveUp = actionStore.getSync('Step/Move Up');

        var moveDown = actionStore.getSync('Step/Move Down');

        var items = this.collection.storage.fullData;
        //console.log('before move',items);
        //this.printRootOrder();
        this.runAction(moveUp);
        //console.log('after move',this.collection.storage.fullData);
        //this._place('root','root2','below');
        //this.printRootOrder();


        var thiz = this;

        setTimeout(function () {

            //thiz.refresh();
            //this.runAction(moveDown);
            //thiz.refreshRoot();
            thiz.printRootOrder();
        }, 1500);

        //this.runAction(moveLeft);
    }


    var blocks = [{"_containsChildrenIds":[],"group":"click4","id":"root4","description":"Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>","name":"Root - 4","method":"console.log(this);","args":"","deferred":false,"declaredClass":"xblox.model.code.RunScript","enabled":true,"shareTitle":"","order":0,"icon":"fa-code","outlet":0,"type":"added"},{"_containsChildrenIds":["elseIfBlocks"],"group":"click","condition":"[value]=='PW'","id":"bbb6f653-3c92-0b0d-2ed2-e952103680a7","elseIfBlocks":["ac0e58b2-5016-a150-f228-af93590c2700"],"declaredClass":"xblox.model.logic.IfBlock","name":"if","icon":"","outlet":0,"enabled":true,"shareTitle":"","description":"No Description","order":0,"type":"added"},{"_containsChildrenIds":["items"],"name":"else if","items":["0db30720-1889-81c4-3a88-2b3b3df14a3a"],"dstField":"elseIfBlocks","parentId":"bbb6f653-3c92-0b0d-2ed2-e952103680a7","id":"ac0e58b2-5016-a150-f228-af93590c2700","declaredClass":"xblox.model.logic.ElseIfBlock","condition":"","icon":"","outlet":0,"enabled":true,"shareTitle":"","description":"No Description","order":0,"type":"added"},{"_containsChildrenIds":[],"parentId":"ac0e58b2-5016-a150-f228-af93590c2700","id":"0db30720-1889-81c4-3a88-2b3b3df14a3a","declaredClass":"xblox.model.code.RunScript","name":"Run Script","method":"","args":"","deferred":false,"icon":"fa-code","outlet":0,"enabled":true,"shareTitle":"","order":0,"type":"added","description":"No Description"},{"_containsChildrenIds":[],"group":"click","id":"2879e1f2-d973-ebdf-00ea-a8c3b9fe05eb","declaredClass":"xblox.model.code.RunScript","name":"Run Script","method":"","args":"","deferred":false,"icon":"fa-code","outlet":0,"enabled":true,"shareTitle":"","order":0,"type":"added"}];

    var _gridBase = {
        onDrop: function (source, target, before, grid, targetState, insert) {


            //debugger;
            var ctrArgs = source.ctrArgs || {};
            var proto = source.proto;

            var add = !before == true && target.parentId == null;
            var newBlock = null;
            var isNewItem = false;

            console.log('grid::onDrop2',arguments);

            //prepare args
            if (source.ctrArgs) {//comes from factory
                ctrArgs.scope = ctrArgs.scope || target.scope;
                ctrArgs.group = ctrArgs.group || target.group;
                ctrArgs.parentId = ctrArgs.parentId || target.id;
                isNewItem = true;
            }

            if (isNewItem) {
                //new item at root level
                if (target.parentId == null && !insert) {
                    ctrArgs.parentId = null;

                    newBlock = factory.createBlock(proto, ctrArgs);//root block

                } else if (insert && target.canAdd && target.canAdd() != null) {//new item at item level
                    newBlock = target.add(proto, ctrArgs, null);
                }
            } else {



                //real item move, before or after
                if (targetState === 'Moved') {

                    if (source.scope && target.scope && source.scope == target.scope) {

                        source.group = null;
                        var moved = target.scope.moveTo(source, target, before, insert);
                        return source;
                    }
                }
            }
        },
        paste: function (items, owner, cut) {


            if (!items) {
                return;
            }
            var target = this.getSelection()[0],
                _flatten,
                source,
                thiz = this,
                scope = thiz.blockScope;

            if (!owner) {
                return;//@TODO : support
            }


            scope.__flatten = function(blocks){
                var result = [];
                for(var b in blocks){

                    var block = blocks[b];

                    if(block.keys==null){
                        continue;
                    }

                    var found = _.find(result,{
                        id:block.id
                    })

                    if(found){
                        //console.error('already in array  : ' +found.name);
                    }else {
                        result.push(block);
                    }

                    for(var prop in block){
                        if (prop == 'ctrArgs') {
                            continue;
                        }
                        //flatten children to ids. Skip "parent" field
                        if (prop !== 'parent') {

                            var value = block[prop];
                            if (this.isBlock(value)){
                                // if the field is a single block container, store the child block's id
                                //result.push(value);
                                found = _.find(result,{
                                    id:value.id
                                })
                                if(found){
                                    //console.error('already in array  : ' +value.name);
                                }else {
                                    result.push(value);
                                }
                            } else if (this.areBlocks(value)){

                                //console.log('found sub blocks in ' + prop + ' ' + block.name);

                                for(var i = 0; i < value.length ; i++){
                                    var sBlock = value[i];

                                    //result.push(sBlock);
                                    found = _.find(result,{
                                        id:sBlock.id
                                    })

                                    if(found){
                                        //console.error('already in array  : ' +sBlock.name);
                                    }else {
                                        result.push(sBlock);
                                    }
                                    result = result.concat(this.flatten([sBlock]));
                                }
                            }
                        }
                    }
                }

                //console.log('un : ' , _.uniqBy(result, 'id'));

                //console.log('found in total '+result.length + ' blocks ',result);

                result = _.uniq(result,false,function(item){
                    return item.id;
                });

                return result;
            }



            

            //special case when paste on nothing
            if (!target) {
                //save parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj.parentId) {
                        obj._parentOri = '' + obj.parentId;
                        obj.parentId = null;
                    }
                }
                //flatten them
                _flatten = scope.flatten(items);

                var _flat2 = _.uniq(_flatten,false,function(item){
                    return item.id
                });
                //clone them
                var _clones = scope.cloneBlocks2(_flat2, this.newRootItemGroup);

                var _flattenClones = scope.flatten(_clones);
                var firstItem = null;
                for (var i = 0; i < _clones.length; i++) {
                    var clone = _clones[i];
                    if (!firstItem) {
                        firstItem = clone;
                    }
                }
                //restore parentIds
                for (var i = 0; i < items.length; i++) {
                    var obj = items[i];
                    if (obj._parentOri) {//restore
                        obj.parentId = obj._parentOri;
                        delete obj['_parentOri'];
                    }
                }
                return _clones;
            }

            var grid = owner;

            var srcScope = items[0].scope;
            var dstScope = target.scope;
            if (srcScope != dstScope) {
                return;
            }
            var insert = target.canAdd() || false;
            var parent = srcScope.getBlockById(target.parentId);
            if (!parent) {
                parent = target;
            }
            var targetState = 'Moved';
            var before = false;

            _flatten = scope.flatten(items);

            _flatten = _.uniq(_flatten,false,function(item){
                return item.id
            });

            items = srcScope.cloneBlocks2(_flatten);
            var newItems = [];




            for (var i = 0; i < items.length; i++) {

                source = items[i];
                //
                //Case source=target
                //
                if (source == target) {
                    console.log('source=target!');
                }

                if(source.parentId){
                    newItems.push(source);
                }else{
                    source.parentId = parent.id;
                    parent.add(source);
                    var nItem = this.onDrop(source, target, before, grid, targetState, insert);
                    newItems.push(nItem);
                }
            }
            return newItems;
        }
    };

    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        ACTION = types.ACTION,
        root;

    function fixScope(scope) {

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function (source, target, before, add) {

            console.log('scope::move, add: ' + add, arguments);

            if (!add) {
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if (add) {

                //remove it from the source parent and re-parent the source
                if (target.canAdd && target.canAdd()) {

                    var sourceParent = this.getBlockById(source.parentId);
                    if (sourceParent) {
                        sourceParent.removeBlock(source, false);
                    }
                    target.add(source, null, null);
                    return;
                } else {
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if (!target.parentId && add == false) {

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if (sourceParent && sourceParent.removeBlock) {
                    sourceParent.removeBlock(source, false);
                    source.parentId = null;
                    source.group = target.group;
                }

                var itemsToBeMoved = [];
                var groupItems = this.getBlocks({
                    group: target.group
                });

                var rootLevelIndex = [];
                var store = this.getBlockStore();

                var sourceIndex = store.storage.index[source.id];
                var targetIndex = store.storage.index[target.id];
                for (var i = 0; i < groupItems.length; i++) {

                    var item = groupItems[i];
                    //keep all root-level items

                    if (groupItems[i].parentId == null && //must be root
                        groupItems[i] != source// cant be source
                    ) {

                        var itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if (add) {
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for (var j = 0; j < itemsToBeMoved.length; j++) {
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if (before) {
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for (var j = 0; j < itemsToBeMoved.length; j++) {
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if (!before) {
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            } else if (!source.parentId && target.parentId && add == false) {
                source.group = target.group;
                if (target) {

                }

                //we move from root to into root item
            } else if (!source.parentId && !target.parentId && add) {

                console.error('we are adding an item into root root item');
                if (target.canAdd && target.canAdd()) {
                    source.group = null;
                    target.add(source, null, null);
                }
                return true;

                // we move within the same parent
            } else if (source.parentId && target.parentId && add == false && source.parentId === target.parentId) {
                console.error('we move within the same parents');
                var parent = this.getBlockById(source.parentId);
                if (!parent) {
                    console.error('     couldnt find parent ');
                    return false;
                }

                var maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items, source);
                var cIndexTarget = source.indexOf(items, target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before == true ? -1 : 1)));
                for (var i = 0; i < distance - 1; i++) {
                    parent.move(source, direction);
                }
                return true;

                // we move within the different parents
            } else if (source.parentId && target.parentId && add == false && source.parentId !== target.parentId) {
                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if (!sourceParent) {
                    console.error('     couldnt find source parent ');
                    return false;
                }

                var targetParent = this.getBlockById(target.parentId);
                if (!targetParent) {
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if (sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()) {
                    sourceParent.removeBlock(source, false);
                    targetParent.add(source, null, null);
                } else {
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if (items == null) {
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items, source);
                var cIndexTarget = targetParent.indexOf(items, target);
                if (!cIndexSource || !cIndexTarget) {
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before == true ? -1 : 1)));
                for (var i = 0; i < distance - 1; i++) {
                    targetParent.move(source, direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        var topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId: null
        });


        var grouped = _.groupBy(blocks, function (block) {
            return block.group;
        });

        function createDummyBlock(id, scope) {

            var block = {
                "_containsChildrenIds": [
                    "items"
                ],
                "group": null,
                "id": id,
                "items": [],
                "name": id,
                "method": "----group block ----",
                "args": "",
                "deferred": false,
                "declaredClass": "xblox.model.code.RunScript",
                "enabled": true,
                "serializeMe": false,
                "shareTitle": "",
                "canDelete": true,
                "renderBlockIcon": true,
                "order": 0,
                "additionalProperties": true,
                "_scenario": "update"

            };

            return scope.blockFromJson(block);

        }

        for (var group in grouped) {

            var groupBlock = createDummyBlock(group, scope);
            var blocks = grouped[group];
            _.each(blocks, function (block) {
                groupBlock['items'].push(block);


                if (!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }

        console.clear();
        var root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }

    function createScope() {

        var data = {
            "___blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "blocksz": [
                {
                    "_containsChildrenIds": [],
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "shareTitle": "",
                    "order": 0,
                    "icon": "fa-code",
                    "outlet": 0,
                    "type": "added"
                }, {
                    "_containsChildrenIds": ["items", "consequent", "elseIfBlocks"],
                    "group": "click",
                    "condition": "[value]=='PW'",
                    "id": "29986b6f-fc18-d2b5-0efa-c4aed858b4df",
                    "items": ["a2077337-2d29-60a9-d222-821f6a59463b"],
                    "consequent": ["2fed8943-f1bb-9129-ef89-9064832ea6eb"],
                    "elseIfBlocks": ["a2077337-2d29-60a9-d222-821f6a59463b"],
                    "declaredClass": "xblox.model.logic.IfBlock",
                    "autoCreateElse": true,
                    "name": "if",
                    "icon": "",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "order": 0,
                    "type": "added"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "29986b6f-fc18-d2b5-0efa-c4aed858b4df",
                    "id": "2fed8943-f1bb-9129-ef89-9064832ea6eb",
                    "declaredClass": "xblox.model.code.RunScript",
                    "name": "Run Script",
                    "method": "ELSE-OK",
                    "args": "",
                    "deferred": false,
                    "icon": "fa-code",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "order": 0
                }, {
                    "_containsChildrenIds": ["items"],
                    "name": "else if",
                    "items": ["66b32440-2809-4c30-8552-36508b6805da"],
                    "dstField": "elseIfBlocks",
                    "parentId": "29986b6f-fc18-d2b5-0efa-c4aed858b4df",
                    "id": "a2077337-2d29-60a9-d222-821f6a59463b",
                    "declaredClass": "xblox.model.logic.ElseIfBlock",
                    "condition": "",
                    "icon": "",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "order": 0
                }, {
                    "_containsChildrenIds": [],
                    "parentId": "a2077337-2d29-60a9-d222-821f6a59463b",
                    "id": "66b32440-2809-4c30-8552-36508b6805da",
                    "declaredClass": "xblox.model.code.RunScript",
                    "name": "Run Script",
                    "method": "Else-IF",
                    "args": "",
                    "deferred": false,
                    "icon": "fa-code",
                    "outlet": 0,
                    "enabled": true,
                    "shareTitle": "",
                    "order": 0,
                    "type": "added"
                }],
            blocks: blocks,
            "variables": []

        };


        return fixScope(blockManager.toScope(data));
    }

    function createGridClass() {

        var renderers = [TreeRenderer];
        var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
        var _gridClass = Grid.createGridClass('driverTreeView',
            {
                options: utils.clone(types.DEFAULT_GRID_OPTIONS)
            },
            //features
            {

                SELECTION: true,
                KEYBOARD_SELECTION: true,
                PAGINATION: false,
                ACTIONS: types.GRID_FEATURES.ACTIONS,
                //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                SPLIT: {
                    CLASS: declare('splitMixin', null, _layoutMixin)
                }
            },
            {
                //base flip
                RENDERER: multiRenderer

            },
            {
                //args
                renderers: renderers,
                selectedRenderer: TreeRenderer
            },
            {
                GRID: OnDemandGrid,
                LAYOUT: Layout,
                DEFAULTS: Defaults,
                RENDERER: ListRenderer,
                EVENTED: EventedMixin,
                FOCUS: Focus
            }
        );
        return declare('gridFinal', BlockGrid, _gridBase);
    }

    if (ctx) {


        var blockManager = ctx.getBlockManager();


        var blockScope = createScope('docs');
        var mainView = ctx.mainView;
        if (mainView) {
            var parent = TestUtils.createTab('BlockGrid-Test', null, module.id);
            var actions = [],
                thiz = this,
                ACTION_TYPE = types.ACTION,
                ACTION_ICON = types.ACTION_ICON,
                grid,
                ribbon;

            var store = blockScope.blockStore;
            var _gridClass = createGridClass();
            var gridArgs = {
                ctx: ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect: true,
                collection: store.filter({
                    group: "click"
                }),
                dndParams: {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                }
            };


            grid = utils.addWidget(_gridClass, gridArgs, null, parent, true);

            var blocks = blockScope.allBlocks();
            var root = blocks[0];
            var actionStore = grid.getActionStore();
            var toolbar = mainView.getToolbar();
            var _defaultActions = [];
            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [grid.permissions]));
            grid.addActions(_defaultActions);
            if (!toolbar) {
            } else {
                toolbar.addActionEmitter(grid);
                toolbar.setActionEmitter(grid);
            }
            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);

            setTimeout(function () {
                clipboardTest.apply(grid);
            }, 1000);
        }
    }

    return Grid;

});