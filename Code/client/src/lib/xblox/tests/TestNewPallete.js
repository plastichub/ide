/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xdocker/Docker2',
    'xblox/views/BlockGrid',
    'xide/tests/TestUtils',
    'module',
    'xgrid/ThumbRenderer',
    'dojo/dom-construct',
    'xgrid/Selection',
    "xide/widgets/_Widget",
    'xgrid/KeyboardNavigation',
    'xaction/ActionStore',
    'xide/factory',
    'xlang/i18'
], function (declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, Editor, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, Docker, BlockGrid,
             TestUtils, module, ThumbRenderer, domConstruct,
             Selection, _XWidget, KeyboardNavigation, ActionStore, factory, i18) {


    console.clear();

    var ThumbClass = declare('xfile.ThumbRenderer2', [ThumbRenderer], {
        thumbSize: "400",
        resizeThumb: true,
        __type: 'thumb',
        deactivateRenderer: function () {
            $(this.domNode.parentNode).removeClass('metro');
            $(this.domNode).css('padding', '');
            this.isThumbGrid = false;

        },
        activateRenderer: function () {
            $(this.contentNode).css('padding', '8px');
            this.isThumbGrid = true;
            this.refresh();
        },
        /**
         * Override renderRow
         * @param obj
         * @returns {*}
         */
        renderRow: function (obj) {
            var div = domConstruct.create('div', {
                    className: "tile widget form-control",
                    style: 'margin-left:16px;margin-top:8px;margin-right:8px;width:150px;height:initial;max-width:200px;float:left;padding:8px'
                }),
                icon = obj.icon,
                label = obj.title,
                imageClass = obj.icon || 'fa fa-folder fa-2x';

            var iconStyle = 'float:left; margin-left:3px;margin-top:3px;margin-right:3px;text-shadow: 2px 2px 5px rgba(0,0,0,0.3);font-size: inherit;opacity: 0.4';
            var folderContent = '<span style="' + iconStyle + '" class="' + imageClass + '"></span>';

            div.innerHTML =
                '<div class="opacity">' +
                    folderContent +
                    '<span class="thumbText text opacity ellipsis" style="text-align:center">' + label + '</span>' +
                '</div>';
            return div;
        }
    });
    var GroupRenderer = declare('xfile.GroupRenderer', TreeRenderer, {
        itemClass: ThumbClass,
        groupClass: TreeRenderer,
        renderRow: function (obj) {
            return (!obj.group ? this.itemClass : this.groupClass).prototype.renderRow.apply(this, arguments);
        }
    });
    var Implementation = {},
        renderers = [GroupRenderer, ThumbClass, TreeRenderer],
        multiRenderer = declare.classFactory('xgrid/MultiRendererGroups', {}, renderers, MultiRenderer.Implementation);

    var GridClass = Grid.createGridClass('xfile.views.Grid', Implementation, {
            SELECTION: {
                CLASS: Selection
            },
            KEYBOARD_SELECTION: true,
            CONTEXT_MENU: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            WIDGET: {
                CLASS: _XWidget
            },
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            }
        },
        {
            RENDERER: multiRenderer
        },
        {
            renderers: renderers,
            selectedRenderer: GroupRenderer
        }
    );
    /***
     * playground
     */
    var ctx = window.sctx,
        root;


    function fixScope(scope) {
        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */


        return scope;
    }

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }

    if (ctx) {
        var blockManager = ctx.getBlockManager();


        var blockScope = createScope('docs');

        var mainView = ctx.mainView;

        if (mainView) {

            var parent = TestUtils.createTab('BlockGrid-Test', null, module.id);
            var thiz = this,
                grid;

            GridClass = declare('ActionGrid', GridClass, {
                formatColumn: function (field, value, obj) {
                    var renderer = this.selectedRenderer ? this.selectedRenderer.prototype : this;
                    if (renderer.formatColumn_) {
                        var result = renderer.formatColumn.apply(arguments);
                        if (result) {
                            return result;
                        }
                    }
                    if (obj.renderColumn_) {
                        var rendered = obj.renderColumn.apply(this, arguments);
                        if (rendered) {
                            return rendered;
                        }
                    }
                    switch (field) {
                        case "title": {
                            value = obj.group ? ('<span style="float:left;margin-right: 10px">' + value + '</span><hr style="margin-top: 13px;margin-left: 4px"/>') : value;
                        }
                    }
                    return value;
                },
                _columns: {},
                postMixInProperties: function () {
                    var state = this.state;
                    if (state) {
                        if (state._columns) {
                            this._columns = state._columns;
                        }
                    }
                    this.columns = this.getColumns();
                    var newBlockActions = this.getAddActions();
                    var levelName = '';
                    var result = [];
                    var BLOCK_INSERT_ROOT_COMMAND = '';
                    var defaultMixin = {addPermission: true};
                    var rootAction = BLOCK_INSERT_ROOT_COMMAND;
                    var thiz = this;

                    function addItems(commandPrefix, items) {
                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];
                            levelName = item.name;
                            var path = commandPrefix ? commandPrefix + '/' + levelName : levelName;
                            var isContainer = !_.isEmpty(item.items);
                            result.push(thiz.createAction({
                                label: levelName,
                                command: path,
                                icon: item.iconClass,
                                tab: 'Home',
                                mixin: utils.mixin({
                                    item: item,
                                    parent: isContainer ? "root" : commandPrefix,
                                    quick: true,
                                    title: i18.localize(levelName),
                                    group: isContainer,
                                    icon: item.iconClass
                                }, defaultMixin)
                            }));

                            if (isContainer) {
                                addItems(path, item.items);
                            }
                        }
                    }

                    addItems(rootAction, newBlockActions);
                    var actionStore = new ActionStore({
                        parentProperty: 'parent',
                        parentField: 'parent',
                        idProperty: 'command',
                        mayHaveChildren: function (parent) {
                            if (parent._mayHaveChildren === false) {
                                return false;
                            }
                            return true;
                        }
                    });


                    actionStore.setData(result);
                    var all = actionStore.query(null);
                    _.each(all, function (item) {
                        item._store = actionStore;
                    });
                    var $node = $(this.domNode);
                    $node.addClass('xFileGrid metro welcomeGrid');
                    //$node.css('padding', '16px');
                    $node.css('height', '30px');
                    this.collection = actionStore.filter({
                        parent: "root"
                    });

                    return this.inherited(arguments);
                },
                getColumns: function (formatters) {
                    var self = this;
                    this.columns = [
                        {
                            renderExpando: true,
                            label: 'Name',
                            field: 'title',
                            sortable: true,
                            formatter: function (value, obj) {
                                return self.formatColumn('title', value, obj);
                            },
                            hidden: false
                        }
                    ];
                    return this.columns;
                },
                /**
                 *
                 * @param item
                 * @returns {Array}
                 */
                getAddActions: function (item) {
                    var thiz = this;
                    item = item || {};
                    var items = factory.getAllBlocks(this.blockScope, this, item, this.blockGroup, false);
                    //tell everyone
                    thiz.publish(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                        items: items,
                        view: this,
                        owner: this,
                        item: item,
                        group: this.blockGroup,
                        scope: this.blockScope
                    });

                    thiz._emit(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, {
                        items: items,
                        view: this,
                        owner: this
                    });
                    return items;

                },
                startup: function () {
                    //this.collection = actionStore;
                    this._showHeader(false);
                    var res = this.inherited(arguments);
                    return res;
                }
            });

            var _gridClass = GridClass;
            var gridArgs = {
                options: utils.clone(types.DEFAULT_GRID_OPTIONS),
                ctx: ctx
            };

            grid = utils.addWidget(_gridClass, gridArgs, null, parent, true);
            var blocks = blockScope.allBlocks();
            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);
        }
    }

    return Grid;

});