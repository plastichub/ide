/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xdocker/Docker2',
    'xblox/views/BlockGrid',
    'xide/tests/TestUtils',
    'module'

], function (declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, Docker,BlockGrid,
             TestUtils,module
    ) {
    var _layoutMixin = {
        _docker:null,
        _parent:null,
        __right:null,
        getDocker:function(container){

            if(!this._docker){

                var _dst = container || this._domNode.parentNode,
                    thiz = this;

                thiz._docker = Docker.createDefault(_dst);
                thiz._oldParent = thiz._parent;

                var parent = thiz._docker.addPanel('DefaultFixed', types.DOCKER.TOP, null, {
                    w: '100%',
                    title:'  '
                });

                dojo.place(thiz._domNode,parent.containerNode);

                thiz._docker.$container.css('top',0);
                thiz._parent = parent;

                parent._parent.showTitlebar(false);


/*
                var right = this._docker.addPanel('Collapsible', types.DOCKER.RIGHT, parent, {
                    w: '20%',
                    title:'Properties'
                });

                parent._parent.showTitlebar(false);*/





                //parent._parent.$center.css('top',0);

            }

            return thiz._docker;
        },
        getRightPanel:function(){

            if(this.__right){
                return this.__right;
            }

            var docker = this.getDocker();


            var right = docker.addPanel('Collapsible', types.DOCKER.RIGHT, this._parent, {
                w: '300px',
                title:'  '
            });



            right._parent.showTitlebar(false);

            var splitter = right.getSplitter();

            splitter.pos(0.6);


            this.__right = right;

            return right;
        }
    };

    var _gridBase = {

        getTypeMap:function(){

        },
        startup:function(){
            this.inherited(arguments);
            this._on('selectionChanged',function(evt){

                var selection = evt.selection;
                return;
            })
        }
    };

    /***
     * playground
     */
    var ctx = window.sctx,
        root;

    function fixScope(scope){
        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */


        return scope;
    }

    function createScope() {

        var data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }

    if (ctx) {


        var blockManager = ctx.getBlockManager();

        function createGridClass() {

            var renderers = [TreeRenderer];
            //, ThumbRenderer, TreeRenderer

            var multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);
            var _gridClass = Grid.createGridClass('driverTreeView',{
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT:{
                        CLASS:declare('splitMixin',null,_layoutMixin)
                    }

                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );
            return declare('gridFinal', BlockGrid, _gridBase);
        }


        var blockScope = createScope('docs');

        var mainView = ctx.mainView;

        if (mainView) {

            var parent = TestUtils.createTab('BlockGrid-Test',null,module.id);


            var thiz = this,
                grid;
            var store = blockScope.blockStore;
            var _gridClass = createGridClass();
            var gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                collection: store.filter({
                    group: "click"
                })
            };


            grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);

            var blocks = blockScope.allBlocks();

            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {
                return;

            }

            function test2() {
                return;
            }

            setTimeout(function () {
                test();
            }, 1000);


            setTimeout(function () {
                test2();
            }, 2000);

        }
    }

    return Grid;

});