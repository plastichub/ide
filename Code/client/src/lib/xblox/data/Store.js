/** @module xblox/data/Store **/
define([
    "dojo/_base/declare",
    'xide/data/TreeMemory',
    'xide/data/ObservableStore',
    'dstore/Trackable',
    'dojo/Deferred'
], function (declare, TreeMemory, ObservableStore, Trackable, Deferred) {
    return declare("xblox.data.Store", [TreeMemory, Trackable, ObservableStore], {
        idProperty: 'id',
        parentField: 'parentId',
        parentProperty: 'parentId',
        filter: function (data) {
            var _res = this.inherited(arguments);
            delete this._state.filter;
            this._state.filter = data;
            return _res;
        },
        getRootItem:function(){
            return {
                canAdd:function(){
                    return true
                },
                id:this.id +'_root',
                group:null,
                name:'root',
                isRoot:true,
                parentId:null
            }
        },
        getChildren: function (object) {
            return this.root.filter({parentId: this.getIdentity(object)});
        },
        _fetchRange: function (kwArgs) {
            var deferred = new Deferred();
            var _res = this.fetchRangeSync(kwArgs);
            var _items;
            if (this._state.filter) {
                //the parent query
                if (this._state.filter['parentId']) {
                    var _item = this.getSync(this._state.filter.parentId);
                    if (_item) {
                        this.reset();
                        _items = _item.items;
                        if (_item.getChildren) {
                            _items = _item.getChildren();
                        }
                        deferred.resolve(_items);
                        _res = _items;
                    }
                }

                //the group query
                if (this._state && this._state.filter && this._state.filter['group']) {
                    _items = this.getSync(this._state.filter.parent);
                    if (_item) {
                        this.reset();
                        _res = _item.items;
                    }
                }
            }
            deferred.resolve(_res);
            return deferred;
        },
        mayHaveChildren: function (parent) {
            if (parent.mayHaveChildren) {
                return parent.mayHaveChildren(parent);
            }
            return parent.items != null && parent.items.length > 0;
        }
    });
});