define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'xide/views/BeanView',
    'xide/views/BeanTreeView',
    'xide/factory',
    'xide/utils',
    "xide/views/_EditorMixin",
    "xide/layout/ContentPane",
    'xblox/views/GroupedBlockView',
    'xblox/views/BlocksGridViewDefault',
    'xblox/model/variables/Variable',
    'dojo/Deferred',
    'xide/layout/TabContainer',
    'xide/views/CIActionDialog',
    'xide/types',
    'xide/form/FilterSelect'

], function (declare, lang, BeanView, BeanTreeView, factory, utils, _EditorMixin, ContentPane, GroupedBlockView, BlocksGridViewDefault, Variable, Deferred,TabContainer,CIActionDialog,types,FilterSelect) {

    return declare("xblox.views.BlocksFileEditor", [BeanView, BeanTreeView, _EditorMixin], {


        //////////////////////////////////////////////////////////
        //
        //  object instances
        //

        /**
         * xFile item, tracked in ::openItem
         */
        _item: null,
        cssClass: 'bloxEditor',
        blockManager: null,
        blockManagerClass: 'xblox.manager.BlockManager',
        model: null,
        store: null,
        tree: null,
        currentItem: null,
        didLoad: false,
        selectable: false,

        beanType: 'BLOCK',
        newGroupPrefix:'',
        _debug:false,

        clearGroupViews:function(all){


            this.destroyWidgets();
            return;

            var container = this.getGroupContainer(),
                thiz = this;

            var panes = container.getChildren();
            for (var i = 0; i < panes.length; i++) {
                if(panes[i].isNewTab){
                    container.removeChild(panes[i]);
                }
            }
            for (var i = 0; i < panes.length; i++) {

                var pane = panes[i];
                /*
                if(pane.title=='Variables' && all!==true){
                    continue;
                }*/
                container.removeChild(pane);


            }

            this.createNewTab();



        },

        getContainerLabel:function(group){

            var title = '' + group;
            console.log('add new block group ' + group);
            if(utils.isNativeEvent(group)){
                title = title.replace('on','');
            }


            //device variable changed: onDriverVariableChanged__deviceId__dd2985b9-9071-1682-226c-70b84b481117/9ab3eabe-ef9a-7613-c3c8-099cde54ef39
            if(group.indexOf(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED)!==-1){

                var deviceManager = this.ctx.getDeviceManager();
                var parts = group.split('__');
                var event = parts[0];
                var deviceId = parts[1];
                var driverId = parts[2];
                var variableId = parts[3];
                var device = deviceManager.getDeviceById(deviceId);

                var driverScope = device ? device.driver : null;

                //not initiated driver!
                if(driverScope && driverScope.blockScope){
                    driverScope=driverScope.blockScope;
                }

                if(!driverScope){
                    console.error('have no driver, use driver from DB',group);
                    if(device) {
                        var driverId = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                        //var driverManager = this.ctx.getDriverManager();
                        driverScope = this.ctx.getBlockManager().getScope(driverId);

                    }
                }

                var deviceTitle = device ? deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE) : 'Invalid Device';
                var variable=driverScope ? driverScope.getVariableById(driverId + '/' + variableId) : 'Invalid Variable(Have no driver)';
                title = 'Variable Changed('+deviceTitle+'/'+ (variable ? variable.title : 'Unknown Variable') +')';


            }
            return title;
        },

        addNewBlockGroup:function(group){

            if(!group){
                return;
            }

            var blockScope = this.blockScope;
            var container = this.getGroupContainer();

            var title = this.getContainerLabel(group);
            var contentPane = this.createGroupView(container, title, blockScope,true,'fa-bell');

            var gridViewConstructurArgs = {};


            var newGroup = this.newGroupPrefix + group;
            gridViewConstructurArgs.newRootItemGroup = newGroup;

            if(this._debug) {
                console.log('add new group:' + newGroup);
            }

            var view = this.createGroupedBlockView(contentPane.containerNode, newGroup, blockScope, gridViewConstructurArgs);

            container.selectChild(contentPane);
        },
        createGroupedBlockView: function (container, group, scope, extra,gridBaseClass) {

            var thiz = this;

            var args = {
                attachTo: container,
                beanContextName: this.beanContextName || scope.id,
                blockGroup: group,
                title: group,
                gridViewProto: BlocksGridViewDefault,
                blockScope: scope,
                ctx: this.ctx,
                delegate: this,
                showAllBlocks: true,
                open: true,
                lazy: true,
                titlePane: false,
                canToggle: false,
                gridParams: {
                    cssClass: 'bloxGridView'
                },
                gridBaseClass:gridBaseClass
            };

            if (extra) {
                args = lang.mixin(args, extra);
            }

            var view = new GroupedBlockView(args);
            view.startup();

            return view;
        },
        getDeviceVariablesAsEventOptions:function(startIntend){

            var options = [];
            var _item = function(label,value,intend,selected,displayValue){


                var string="<span style=''>" +label + "</span>";
                var pre = "";
                if(intend>0){
                    for (var i = 0; i < intend; i++) {
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label:_final,
                    label2:displayValue,
                    value:value
                };
            };

            var deviceManager = this.ctx.getDeviceManager();
            var items = deviceManager.getDevices(false,true);

            for (var i = 0; i < items.length; i++) {
                var device = items[i];
                var driver = device.driver;
                if(!driver){
                    continue;
                }

                var title = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                var id = deviceManager.getMetaValue(device, types.DEVICE_PROPERTY.CF_DEVICE_ID);
                options.push(_item(title,driver.id+'/' +driver.id,startIntend,false));


                var blockScope = driver.blockScope;
                var variables = blockScope.getVariables();

                console.log(device);

                for (var j = 0; j < variables.length; j++) {
                    var variable = variables[j];
                    var value = types.EVENTS.ON_DRIVER_VARIABLE_CHANGED+ '__' + id+'__'+driver.id + '__'+ variable.id;
                    var selected = false;
                    options.push(_item(variable.title,value,startIntend + 1,selected,title + '/' + variable.title));
                }
            }

            return options;
        },
        openNewGroupDialog:function(){

            //var options = utils.getEventsAsOptions();

            var options = [];

            var _item = function(label,value,intend){


                var string="<span style=''>" +label + "</span>";
                var pre = "";

                if(intend>0){
                    for (var i = 0; i < intend; i++) {
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                        pre+="&nbsp;";
                    }
                }
                var _final = pre + string;
                return {
                    label:_final,
                    value:value,
                    label2:label
                };
            };

            options.push( _item('HTML','',0));
            options = options.concat([
                _item('onclick','click',1),
                _item('ondblclick','dblclick',1),
                _item('onmousedown','mousedown',1),
                _item('onmouseup','mouseup',1),
                _item('onmouseover','mouseover',1),
                _item('onmousemove','mousemove',1),
                _item('onmouseout','mouseout',1),
                _item('onkeypress','keypress',1),
                _item('onkeydown','keydown',1),
                _item('onkeyup','keyup',1),
                _item('onfocus','focus',1),
                _item('onblur','blur',1),
                _item('load','load',1)
            ]);

            options.push( _item('Device Variable Changed','',0));
            options = options.concat(this.getDeviceVariablesAsEventOptions(1));



            var thiz = this;
            var actionDialog = new CIActionDialog({
                title: 'Create a new block group',
                style: 'width:500px;min-height:200px;',
                resizeable: true,
                delegate: {
                    onOk: function (dlg, data) {

                        var event = data[0].value;
                        thiz.addNewBlockGroup(event);
                    }
                },
                cis: [
                    utils.createCI('Event', types.ECIType.ENUMERATION, '',
                        {
                            group: 'Event',
                            delegate: null,
                            options:options,
                            value:'HTML',
                            widget:{
                                "class":"xide.form.FilterSelect"
                            }
                        }

                    )
                ],
                viewArgs:{
                    inserts: [{
                        query: '.dijitDialogPaneContent',
                        insert: '<div><span class="simpleText">Select the event for the new group:</span></div>',
                        place: 'first'
                    }]
                }
            });
            actionDialog.show();
        },
        createNewTab:function(){

            var thiz=this;
            utils.addWidget(ContentPane,{
                iconClass:'fa-magic',
                canSelect:false,
                isNewTab:true,
                onSelect:function(){
                    thiz.openNewGroupDialog();
                }
            },this,this.getGroupContainer(),true,'',null,false);

        },
        onGroupsCreated:function(){

            if(this.canAddGroups){
                this.createNewTab();
            }
        },

        /**
         * Current Blox Scope {xblox.model.Scope}
         */
        blockScope: null,
        //////////////////////////////////////////////////////////
        //
        //  Widget Instances
        //
        groupContainer: null,

        getTabContainer:function(){
            return this.groupContainer;
        },

        canAddGroups:true,

        ////////////////////////////////////////////////////////////////
        //
        //  item bean protocol
        //
        ///////////////////////////////////////////////////////////////
        hasItemActions: function () {
            return true;
        },
        /**
         * Returns item actions
         * @returns {Array}
         */
        getItemActions: function () {
            return this.inherited(arguments);//done in xide/bean/Grouped
        },
        //////////////////////////////////////////////////////////
        //
        //  Public API
        //
        onReloaded: function () {
            this.destroyWidgets();
            this.openItem(this._item);
        },
        /**
         * default entry when opening a file item through xfile
         * @param item
         */
        openItem: function (item) {

            var dfd = new Deferred();
            this._item = item;
            var thiz = this;

            var blockManager = this.getBlockManager();

            blockManager.load(item.mount, item.path).then(function (scope) {
                thiz.initWithScope(scope);
                dfd.resolve(thiz);

            });

            return dfd;

        },
        /**
         * Init with serialized string, forward to
         * @param content
         */
        initWithContent: function (content) {

            var data = null;
            try {
                data = utils.getJson(content);
            } catch (e) {
                console.error('invalid block data');
            }

            if (data) {
                this.initWithData(data);
            }

        },
        /**
         * Entry point when a blox scope is fully parsed
         * @param blockScope
         */
        initWithScope: function (blockScope) {

            this.blockScope = blockScope;
            var allBlockGroups = blockScope.allGroups(),
                thiz = this;

            //console.log('       block groups', allBlockGroups);

            if (allBlockGroups.indexOf('Variables') == -1) {
                allBlockGroups.push('Variables');
            }
            if (allBlockGroups.indexOf('Events') == -1) {
                allBlockGroups.push('Events');
            }
            if (allBlockGroups.indexOf('On Load') == -1) {
                allBlockGroups.push('On Load');
            }
            thiz.renderGroups(allBlockGroups, blockScope);

            this.onLoaded();

        },
        getScopeUserData: function () {
            return {
                owner: this
            };
        },
        initWithData: function (data) {

            if(this._debug) {
                console.log('init with data', data);
            }

            this.onLoaded();

            var scopeId = utils.createUUID(),
                blockInData = data,
                variableInData = data;

            //check structure
            if (lang.isArray(data)) {// a flat list of blocks

            } else if (lang.isObject(data)) {
                scopeId = data.scopeId || scopeId;
                blockInData = data.blocks || [];
                variableInData = data.variables || [];
            }

            var blockManager = this.getBlockManager();
            this.blockManager = blockManager;
            var scopeUserData = this.getScopeUserData();

            var blockScope = blockManager.getScope(scopeId, scopeUserData, true);
            var allBlocks = blockScope.blocksFromJson(blockInData);

            for (var i = 0; i < allBlocks.length; i++) {
                var obj = allBlocks[i];

                obj._lastRunSettings = {
                    force: false,
                    highlight: true
                }
            }

            var allVariables = blockScope.variablesFromJson(variableInData);
            blockManager.onBlocksReady(blockScope);
            if(this._debug) {
                console.log('   got blocks', allBlocks);
                console.log('   got variables', allVariables);
            }
            if (allBlocks) {
                return this.initWithScope(blockScope);
            }
            /**
             * a blocks file must be in that structure :
             */
        },
        //////////////////////////////////////////////////////////
        //
        //  utils
        //
        destroyWidgets: function () {
            utils.destroyWidget(this.groupContainer);
            if (this.blockManager && this.blockScope) {
                this.blockManager.removeScope(this.blockScope.id);
            }
            this.groupContainer = null;
            this.blockManager = null;
        },
        destroy: function () {
            this.destroyWidgets();
            this.inherited(arguments);
        },
        /**
         * Get/Create a block manager implicit
         * @returns {xblox.manager.BlockManager}
         */
        getBlockManager: function () {

            if (!this.blockManager) {

                if (this.ctx.blockManager) {
                    return this.ctx.blockManager;
                }

                this.blockManager = factory.createInstance(this.blockManagerClass, {
                    ctx: this.ctx
                });
                if(this._debug) {
                    console.log('_createBlockManager ', this.blockManager);
                }
            }
            return this.blockManager;
        },
        //////////////////////////////////////////////////////////
        //
        //  UI - Factory
        //
        createGroupContainer: function () {

            var tabContainer = utils.addWidget(TabContainer, {
                tabStrip: true,
                tabPosition: "top",
                /*splitter: true,*/
                style: "min-width:450px;min-height:400px;height:inherit;padding:0px;"
            }, this, this.containerNode, true,'ui-widget-content');
            return tabContainer;
        },
        getGroupContainer: function () {
            if (this.groupContainer) {
                return this.groupContainer;
            }
            this.groupContainer = this.createGroupContainer();
            return this.groupContainer;
        },
        createGroupView: function (groupContainer, group,scope,closable,iconClass,select) {

            return utils.addWidget(ContentPane, {
                delegate: this,
                iconClass:iconClass || '',
                title: group,
                allowSplit:true,
                closable:closable,
                style: 'padding:0px',
                cssClass: 'blocksEditorPane',
                blockView: null,
                onSelect: function () {
                    if (this.blockView) {
                        this.blockView.onShow();
                    }
                }
            }, this, groupContainer, true,null,null,select,null);
        },
        renderGroup:function(group,blockScope,gridBaseClass){


            blockScope = blockScope || this.blockScope;


            var groupContainer = this.getGroupContainer(),
                thiz = this;



            var title = group.replace(this.newGroupPrefix,'');
            if(utils.isNativeEvent(group)){
                title = title.replace('on','');
            }


            title = this.getContainerLabel(group.replace(this.newGroupPrefix,''));
            if(this._debug) {
                console.log('render group : ' + title);
            }

            var isVariableView = group === 'Variables';

            var contentPane = this.createGroupView(groupContainer, title, blockScope,!isVariableView,isVariableView ? ' fa-info-circle' : 'fa-bell',!isVariableView);

            var gridViewConstructurArgs = {};

            if (group === 'Variables') {

                gridViewConstructurArgs.newRootItemFunction = function () {
                    try {
                        var newItem = new Variable({
                            title: 'No-Title-Yet',
                            type: 13,
                            value: 'No Value',
                            enumType: 'VariableType',
                            save: false,
                            initialize: '',
                            group: 'Variables',
                            id: utils.createUUID(),
                            scope: blockScope
                        });
                    } catch (e) {
                        debugger;
                    }
                };

                gridViewConstructurArgs.onGridDataChanged = function (evt) {

                    var item = evt.item;
                    if (item) {
                        item[evt.field] = evt.newValue;
                    }
                    thiz.save();
                };
                gridViewConstructurArgs.showAllBlocks = false;
                gridViewConstructurArgs.newRootItemLabel = 'New Variable';
                gridViewConstructurArgs.newRootItemIcon = 'fa-code';
                gridViewConstructurArgs.storeField = 'variableStore';
                gridViewConstructurArgs.gridParams ={
                    cssClass: 'bloxGridView',
                    getColumns:function(){
                        return [
                            {
                                label: "Name",
                                field: "title",
                                sortable: true

                            },
                            {
                                label: "Value",
                                field: "value",
                                sortable: false
                            }
                        ]
                    }
                };
            }


            gridViewConstructurArgs.newRootItemGroup = group;

            var view = this.createGroupedBlockView(contentPane.containerNode, group, blockScope, gridViewConstructurArgs,gridBaseClass);
            contentPane.blockView = view;
            view.parentContainer = contentPane;

            return view;


        },
        renderGroups: function (_array, blockScope) {

            var groupContainer = this.getGroupContainer();
            var lastChild = null, thiz = this;

            for (var i = 0; i < _array.length; i++) {

                var group =  _array[i];

                if(this.newGroupPrefix=='' && group.indexOf('__')!==-1){
                    continue;
                }

                try {

                    var title = group.replace(this.newGroupPrefix,'');

                    var groupBlocks = blockScope.getBlocks({
                        group: group
                    });

                    if (group !== 'Variables' && (!groupBlocks || !groupBlocks.length)) {//skip empty
                        continue;
                    }

                    this.renderGroup(group,blockScope);


                } catch (e) {
                    debugger;
                }
            }


            groupContainer.resize();
            setTimeout(function () {
                if (thiz.parentContainer) {
                    thiz.parentContainer.resize();
                }
            }, 500);

            this.onGroupsCreated();

        },
        onSave: function (groupedBlockView) {
            this.save();
        },
        //////////////////////////////////////////////////////////
        //
        //  Editor related
        //
        save: function () {

            if (this.blockScope) {

                var all = {
                    blocks: null,
                    variables: null
                };
                var blocks = this.blockScope.blocksToJson();
                try {
                    //test integrity
                    dojo.fromJson(JSON.stringify(blocks));
                } catch (e) {
                    console.error('invalid data');
                    return;
                }

                var _onSaved = function () {};

                var variables = this.blockScope.variablesToJson();
                try {
                    //test integrity
                    dojo.fromJson(JSON.stringify(variables));
                } catch (e) {
                    console.error('invalid data');
                    return;
                }
                all.blocks = blocks;
                all.variables = variables;
                this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
            }
        },
        onGridKeyEnter: function () {
            this.editBlock(this.getItem());
        },
        onGridMouseDoubleClick: function () {
            this.editBlock(this.getItem());
        }
    });
});