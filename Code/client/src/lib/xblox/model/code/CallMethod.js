define([
    'dcl/dcl',
    "xblox/model/Block",
    'xide/utils'
], function(dcl,Block,utils){

    // summary:
    //		The Call Block model.
    //      This block makes calls to another blocks in the same scope by action name

    // module:
    //		xblox.model.code.CallMethod
    return dcl(Block,{
        declaredClass:"xblox.model.code.CallMethod",
        //method: (String)
        //  block action name
        name:'Call Method',
        //method: (String)
        //  block action name
        method:'',
        args:'',

        sharable:true,
        /***
         * Returns the block run result
         * @param scope
         */
        solve:function(scope,settings) {
            var context = this.getContext();
            if (context && context[this.method]!=null)
            {


                var res = [];
                var _fn = context[this.method];
                try{
                    var _args = this.getArgs(settings);
                    console.log('args',_args);
                    var _res = _fn.apply(context,_args||[]);
                    res = _res;
                    this.onSuccess(this,settings);
                    return res;
                }catch(e){
                    console.error('call method ' + this.method + ' failed: '+e);
                    logError(e);
                    this.onFailed(this,settings);
                }
            }else{
                this.onFailed(this,settings);
                return [];
            }
            return [];
        },
        toText:function(){

            var result = this.getBlockIcon() + ' ' + this.name + ' ';
            if(this.method){
                result+= this.makeEditable('method','bottom','text','Enter a driver method','inline');
            }
            return result;
        },

        //  standard call for editing
        getFields:function(){

            var fields = this.getDefaultFields();

            var context = this.getContext();
/*
            console.log('call method ', this.getScope().getContext());
            console.log('call method ', context);*/


            fields.push(utils.createCI('value',13,this.method,{
                    group:'General',
                    title:'Method',
                    dst:'method'
                }));

            fields.push(utils.createCI('value',27,this.args,{
                    group:'Arguments',
                    dst:'args',
                    widget:{
                        title:''
                    }
                }));

            return fields;
        },
        getBlockIcon:function(){
            return '<span class="fa-caret-square-o-right"></span>';
        }
    });
});