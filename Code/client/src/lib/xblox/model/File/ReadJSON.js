/** @module xblox/model/File/ReadJSON **/
define([
    'dcl/dcl',
    "dojo/Deferred",
    "xblox/model/Block",
    'xide/utils',
    'xblox/model/Contains',
    'xide/types',
    "xdojo/has!xblox-ui?xfile/data/DriverStore",
    'xdojo/has!xblox-ui?xfile/views/FileGridLight'
], function (dcl, Deferred, Block, utils, Contains, types, DriverStore, FileGridLight) {

    /**
     *
     * @param ext
     * @param config
     * @param options
     * @param fileServer
     * @returns {*}
     */
    function createStore(ext,options,fileServer) {
        return new DriverStore({
            data: [],
            config: {},
            mount: 'none',
            options: options,
            driver: fileServer,
            micromatch: "(*.json)|!(*.*)", // Only folders and json files
            //micromatch: "(*.mp3)|(*.wav)|(*.webm)|!(*.*)", // Only folders and json files
            glob: ext
        });
    }
    /**
     *
     * @class module:xblox/model/code/RunScript
     * @extends module:xblox/model/Block
     * @augments module:xblox/model/Block_UI
     */
    return dcl([Block, Contains], {
        declaredClass: "xblox.model.File.ReadJSON",
        name: 'Read JSON',
        deferred: false,
        sharable: false,
        context: null,
        icon: 'fa-file',
        observed: [
            'path'
        ],
        getContext: function () {
            return this.context || (this.scope.getContext ? this.scope.getContext() : this);
        },
        getFileContent: function (path) {
            var scope = this.getScope();
            var ctx = scope.ctx;
            var deviceManager = ctx.getDeviceManager();
            var fileServer = deviceManager.getInstanceByName('File-Server');
            return fileServer.callCommand('GetProg', {
                override: {
                    args: [path]
                }
            });
        },
        processJSON: function (data, settings) {
            var path = this.jsonPath;
            if (path) {
                this._lastResult = utils.getAt(data, path);
            } else {
                this._lastResult = data;
            }
            this.onSuccess(this, settings);
            this.runByType(types.BLOCK_OUTLET.FINISH, settings);
        },
        /**
         *
         * @param scope
         * @param settings
         * @param isInterface
         * @param run
         * @param error
         * @returns {Deferred}
         */
        solve: function (scope, settings, isInterface, run, error) {
            this._currentIndex = 0;
            this._return = [];
            settings = this._lastSettings = settings || this._lastSettings || {};
            var _script = ('' + this._get('path')),
                thiz = this,
                dfd = new Deferred(),
                self = this;

            this.onRunThis(settings);
            var expression = scope.expressionModel.replaceVariables(scope, _script, null, null);
            var getDfd = this.getFileContent(expression);
            getDfd.then(function (data) {
                var content = data.content;
                if (content) {
                    content = utils.getJson(content, true);
                    if (content) {
                        self.processJSON(content, settings);
                    }
                }
            }.bind(this));
            try {
                if (run) {
                    run('Expression ' + _script + ' evaluates to ' + expression);
                }
            } catch (e) {
                thiz.onDidRunItemError(dfd, e, settings);
                thiz.onFailed(thiz, settings);
                if (error) {
                    error('invalid expression : \n' + _script + ': ' + e);
                }
            }
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UI impl.
        //
        /////////////////////////////////////////////////////////////////////////////////////
        toText: function () {
            var result = '<span style="">' + this.getBlockIcon() + ' ' + this.name + ' :: ' + '</span>';
            if (this.path) {
                result += this.path.substr(0, 50);
            }
            return result;
        },
        //  standard call from interface
        canAdd: function () {
            return [];
        },
        //  standard call for editing
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            var scope = this.getScope();
            var ctx = scope.ctx;
            var deviceManager = ctx.getDeviceManager();
            var fileServer = deviceManager.getInstanceByName('File-Server');//system's default
            var permissions = utils.clone(types.DEFAULT_FILE_GRID_PERMISSIONS);
            if (fileServer && DriverStore) {
                var FilePickerOptions = {
                    ctx: ctx,
                    owner: this,
                    selection: '/',
                    resizeToParent: true,
                    Module: FileGridLight,
                    permissions: permissions
                },
                options = {
                    fields: types.FIELDS.SHOW_ISDIR | types.FIELDS.SHOW_OWNER | types.FIELDS.SHOW_SIZE |
                    types.FIELDS.SHOW_FOLDER_SIZE |
                    types.FIELDS.SHOW_MIME |
                    types.FIELDS.SHOW_PERMISSIONS |
                    types.FIELDS.SHOW_TIME |
                    types.FIELDS.SHOW_MEDIA_INFO
                };
                FilePickerOptions.leftStore = createStore("/*",options,fileServer);
                FilePickerOptions.rightStore = createStore("/*",options,fileServer);
                fields.push(utils.createCI('path', 4, this.path, {
                    group: 'General',
                    title: 'Path',
                    dst: 'path',
                    filePickerOptions: FilePickerOptions,
                    widget: {
                        item: this
                    }
                }));
            }
            fields.push(utils.createCI('jsonPath', 13, this.jsonPath, {
                group: 'General',
                title: 'Select',
                dst: 'jsonPath'
            }));
            return fields;
        }
    });
});