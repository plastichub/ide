define([
    'dcl/dcl',
    "dojo/_base/declare",
    "xide/mixins/ReferenceMixin",
    "xide/utils"
], function (dcl,declare, ReferenceMixin,utils) {
    var Implementation = {
        /**
         * JSON String in that format : reference(string) | mode (string)
         */
        reference: null,
        /**
         * 'reference' is a JSON structure
         * @param value
         * @returns {*}
         */
        deserialize: function (value) {
            if (!value || value.length == 0) {
                return {};
            }
            try {
                return utils.fromJson(value);
            } catch (e) {
                return {};
            }
        }
    };
    /**
     * Holds information to locate an object by string or direct reference.
     * This must be used as mixin rather as base class!
     */
    var Module = declare('xblox.model.Referenced', [ReferenceMixin],Implementation);
    Module.dcl = dcl(ReferenceMixin.dcl,Implementation);
    return Module;
});