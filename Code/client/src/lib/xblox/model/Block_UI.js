/** @module xblox/model/Block_UI **/
define([
    'dcl/dcl',
    "xide/utils",
    "xide/types",
    "xide/data/Source",
    "xaction/ActionProvider"
], function (dcl,utils, types,Source,ActionProvider) {
    /**
     * All ui - related addons/interfaces for blocks
     * Please read {@link module:xide/types}
     * @class module:xblox/model/Block_UI
     * @lends module:xblox/model/Block
     */
    return dcl([Source.dcl,ActionProvider.dcl],{
        declaredClass:"xblox.model.Block_UI",
        _statusIcon:null,
        /**
         * hash-map per scope - id
         */
        _statusClass:null,
        /**
         * The blocks internal user description
         * Description is used for the interface. This should be short and expressive and supports plain and html text.
         *
         * @todo: same as name, move that in user space, combine that with a template system, so any block ui parts gets off from here!
         * @type {string}
         * @default 'No Description'
         * @required true
         */
        description: 'No Description',
        /**
         * UI flag, prevents that a block can be deleted.
         * @todo, move to block flags
         * @type {boolean}
         * @default true
         */
        canDelete: true,
        renderBlockIcon: true,
        /**
         * Return current status icon
         * @returns {string|null}
         */
        getStatusIcon:function(){
          return this._statusIcon;
        },
        /**
         * Returns the stored highlight class per scope-id
         * @param scopeId {string}
         * @returns {object|null}
         */
        getStatusClass:function(scopeId){
            if(this._statusClass){
                return this._statusClass[scopeId];
            }
            return null;
        },
        /**
         * Store the highlight class per scope id
         * @param scopeId
         * @param statusClass
         */
        setStatusClass:function(scopeId,statusClass){
            if(!this._statusClass){
                this._statusClass = {};
            }
            delete this._statusClass[scopeId];
            statusClass && (this._statusClass[scopeId]=statusClass);
        },
        _getText: function (url) {
            var result;
            dojo.xhrGet({
                url: url,
                sync: true,
                handleAs: 'text',
                load: function (text) {
                    result = text;
                }
            });
            return '' + result + '';
        },
        /**
         * implements
         */
        getHelp:function(){
        },
        /**
         * provides
         * @param which
         * @param style
         * @param after
         * @returns {string}
         */
        getIcon:function(which,style,after){
            return '<span style="' + (style||"")+'" class="'+which+'"></span> ' + (after || "");
        },
        /**
         *
         * @param field
         * @param pos
         * @param type
         * @param title
         * @param mode: inline | popup
         * @returns {string}
         */
        makeEditable2:function(field,pos,type,title,mode){
            return "<a tabIndex=\"-1\" pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='editable'  href='#'>" + this[field] +"</a>";
        },
        makeEditable:function(field,pos,type,title,mode){
            var editableClass = this.canEdit() ? 'editable' : 'disabled';
            return "<a data-value='" + this[field] +  "' tabIndex='-1' pos='" + pos +"' display-mode='" + (mode||'popup') + "' display-type='" + (type || 'text') +"' data-prop='" + field + "' data-title='" + title + "' class='" +editableClass + "' href='#'>" + this[field] +"</a>";
        },
        /**
         * Called by UI, determines whether a block can be moved up or down
         * @param item
         * @param dir
         * @returns {boolean}
         */
        canMove: function (item, dir) {
            item = item || this;
            if (!item) {
                return false;
            }
            var parent = item.getParent(),
                items = null;
            if (parent) {
                items = parent[parent._getContainer(item)];
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }
                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var upperItem = items[cIndex + (dir)];
                if (!upperItem) {
                    return false;
                }
            }else{
                var store = this._store;
                items = store.storage.fullData;
                var _next = this.next(items,dir);
                return _next!=null;
            }
            return true;
        },
        /**
         * Moves an item up/down in the container list
         * @param item
         * @param dir
         * @returns {boolean}
         */
        move: function (item, dir) {
            item = item || this;
            if (!item) {
                return false;
            }
            var parent = item.getParent();
            var items = null;
            var store = item._store;
            if(parent) {
                items = parent[parent._getContainer(item)];
                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                    return false;
                }

                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var upperItem = items[cIndex + (dir)];
                if (!upperItem) {
                    return false;
                }
                items[cIndex + (dir)] = item;
                items[cIndex] = upperItem;
                return true;

            }else{
                if(store && item.group){
                    items = store.storage.fullData;
                }
                var _dstIndex = 0;
                var step = 1;
                function _next(item,items,dir){
                    var cIndex = item.indexOf(items, item);
                    var upperItem = items[cIndex + (dir * step)];
                    if(upperItem){
                        if(!upperItem.parentId && upperItem.group && upperItem.group===item.group){

                            _dstIndex = cIndex + (dir * step);
                            return upperItem;
                        }else{
                            step++;
                            return _next(item,items,dir);
                        }
                    }
                    return null;
                }
                var cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                var next = _next(item,items,dir);
                if (!next) {
                    return false;
                }
                items[_dstIndex]=item;
                items[cIndex] = next;
                store._reindex();
                return true;
            }
        },
        /**
         * provides
         * @param block
         * @param settings
         * @param event
         * @param args
         */
        onActivity: function (block, settings, event,args) {
            args = args || {};
            args.target = block;
            this._emit(event, args, block);
            this.publish(event,args,block);
        },
        /**
         * provides
         * @param block
         * @param settings
         */
        onRun: function (block, settings,args) {
            var highlight = settings && settings.highlight;
            if (block && highlight) {
                this.onActivity(block, settings, types.EVENTS.ON_RUN_BLOCK,args);
            }
            this._statusIcon = 'text-info fa-spinner fa-spin';
        },
        /**
         * provides
         * @param block
         * @param settings
         * @param args
         */
        onFailed: function (block, settings,args) {
            var highlight = settings && settings.highlight;
            if (block && highlight) {
                this.onActivity(block, settings, types.EVENTS.ON_RUN_BLOCK_FAILED,args);
            }
            this._statusIcon = 'text-danger fa-exclamation';
        },
        /**
         * provides
         * @param block
         * @param settings
         * @param args
         */
        onSuccess: function (block, settings,args) {
            var highlight = settings && settings.highlight;
            if (block && highlight) {
                this.onActivity(block, settings, types.EVENTS.ON_RUN_BLOCK_SUCCESS,args);
            }
            this._statusIcon = 'text-success fa-check';
        },
        /**
         * implements
         * @returns {boolean}
         */
        canDisable: function () {
            return true;
        },
        /**
         * provides defaults
         * @param icon {boolean=true}
         * @param share {boolean=true}
         * @param outlets {boolean=true}
         * @returns {Array}
         */
        getDefaultFields: function (icon,share,outlets) {
            var fields = [];
            if (this.canDisable && this.canDisable() !== false) {
                fields.push(
                    utils.createCI('enabled', 0, this.enabled, {
                        group: 'General',
                        title: 'Enabled',
                        dst: 'enabled',
                        actionTarget:'value',
                        order:210
                    })
                );
            }
            fields.push(utils.createCI('description', 26, this.description, {
                group: 'Description',
                title: 'Description',
                dst: 'description',
                useACE: false
            }));

            icon!==false && fields.push(utils.createCI('icon', 17, this.icon, {
                group: 'General',
                title: 'Icon',
                dst: 'icon',
                useACE: false,
                order:206
            }));

            outlets!==false && fields.push(utils.createCI('outlet', 5, this.outlet, {
                group: 'Special',
                title: 'Type',
                dst: 'outlet',
                order:205,
                data:[
                    {
                        value: 0x00000001,
                        label: 'Progress',
                        title: 'Executed when progress'
                    },
                    {
                        value: 0x00000002,
                        label: 'Error',
                        title: "Executed when errors"
                    },
                    {
                        value: 0x00000004,
                        label: 'Paused',
                        title: "Executed when paused"
                    },
                    {
                        value: 0x00000008,
                        label: 'Finish',
                        title: "Executed when finish"
                    },
                    {
                        value: 0x00000010,
                        label: 'Stopped',
                        title: "Executed when stopped"
                    }
                ],
                widget:{
                    hex:true
                }
            }));
            if (this.sharable) {
                fields.push(
                    utils.createCI('enabled', 13, this.shareTitle, {
                        group: 'Share',
                        title: 'Title',
                        dst: 'shareTitle',
                        toolTip: 'Enter an unique name to share this block!'
                    })
                );
            }
            return fields;
        },
        /**
         * implements
         * @returns {*|Array}
         */
        getFields: function () {
            return this.getDefaultFields();
        },
        /**
         * util
         * @param str
         * @returns {*}
         */
        toFriendlyName: function (str) {
            var special = ["[", "]", "(", ")", "{", "}"];
            for (var n = 0; n < special.length; n++) {
                str = str.replace(special[n], '');
            }
            str = utils.replaceAll('==', ' equals ', str);
            str = utils.replaceAll('<', ' is less than ', str);
            str = utils.replaceAll('=<', ' is less than ', str);
            str = utils.replaceAll('>', ' is greater than ', str);
            str = utils.replaceAll('>=', ' is greater than ', str);
            str = utils.replaceAll("'", '', str);
            return str;
        },
        /**
         * implements
         * @returns {string}
         */
        getIconClass: function () {
            return this.icon;
        },
        /**
         * implements
         * @param symbol
         * @returns {string}
         */
        getBlockIcon: function (symbol) {
            symbol = symbol || '';
            return this.renderBlockIcon == true ? '<span class="xBloxIcon ' + this.icon + '">' + symbol + '</span>' : '';
        },
        /**
         * provides
         * @param block
         * @param cis
         */
        onFieldsRendered: function (block, cis) {
        },
        /**
         * inherited
         * @param field
         * @param newValue
         */
        onChangeField: function (field, newValue) {
            if (field == 'enabled') {
                if (newValue == true) {
                    this.activate();
                } else {
                    this.deactivate();
                }
            }
        },
        /**
         * implements
         */
        destroy:function(){
            this.setStatusClass(this.getScope().id,null);
        }
    });
});