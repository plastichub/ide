define([
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/variables/Variable",
    'xblox/model/Contains',
    "dojo/promise/all",
    "dojo/Deferred"
], function (dcl, Block, Variable, Contains, all, Deferred) {

    /**
     * @augments module:xide/mixins/EventedMixin
     * @lends module:xblox/model/Block_UI
     * @extends module:xblox/model/Block
     * @extends module:xblox/model/ModelBase
     */
    // summary:
    //		The for block model. It repeats a block a number of times, while the condition is true.
    //

    // module:
    //		xblox.model.loops.ForBlock
    return dcl([Block, Contains], {
        declaredClass: "xblox.model.loops.ForBlock",
        // initial: xcf.model.Expression
        // the initial value
        initial: null,
        // final: xcf.model.Expression
        // the final value to be compared with the counter. Once the final value equals to the counter, the loop stops
        "final": null,
        //comparator: xblox.model.Comparator
        // Comparison to be applied -> compare <counter variable> width <final>
        comparator: null,
        // modifier: xcf.model.Expression
        // expression to be applied to the counter on every step. Expression: "<counter><modifier>"
        modifier: null,

        //items: Array (xblox.model.Block)
        //  block to be executed while the condition compare <counter variable> width <final> is false
        items: null,

        //counterName: String
        // the counter variable name
        counterName: null,
        // (private) counter: xblox.model.Variable
        // counter to be compared and updated on every step
        _counter: null,
        name: 'For',
        sharable: true,
        icon: '',
        ignoreErrors: false,
        deferred: true,
        _forState: false,
        _currentForIndex: 0,
        runFrom: function (_blocks, index, settings) {
            var thiz = this,
                blocks = _blocks || this.items,
                allDfds = [];

            var onFinishBlock = function (block, results) {
                block._lastResult = block._lastResult || results;
                thiz._currentIndex++;
                thiz.runFrom(blocks, thiz._currentIndex, settings);
            };

            var wireBlock = function (block) {
                block._deferredObject.then(function (results) {
                    onFinishBlock(block, results);
                });
            };

            if (blocks.length) {
                for (var n = index; n < blocks.length; n++) {
                    var block = blocks[n];
                    if (block.enabled === false) {
                        continue;
                    }
                    if (block.deferred === true) {
                        block._deferredObject = new Deferred();
                        this._currentIndex = n;
                        wireBlock(block);
                        allDfds.push(block.solve(this.scope, settings));
                        break;
                    } else {
                        allDfds.push(block.solve(this.scope, settings));
                    }
                }

            } else {
                this.onSuccess(this, settings);
            }
            return allDfds;
        },
        runFromDirect: function (_blocks, index, settings) {
            var thiz = this,
                blocks = _blocks || this.items,
                allDfds = [];

            var onFinishBlock = function (block, results) {
                block._lastResult = block._lastResult || results;
                thiz._currentIndex++;
                thiz.runFrom(blocks, thiz._currentIndex, settings);
            };

            var wireBlock = function (block) {
                block._deferredObject.then(function (results) {
                    onFinishBlock(block, results);
                });
            };
            if (blocks.length) {
                for (var n = index; n < blocks.length; n++) {
                    var block = blocks[n];
                    if (block.enabled === false) {
                        continue;
                    }
                    if (block.deferred === true) {
                        block._deferredObject = new Deferred();
                        this._currentIndex = n;
                        wireBlock(block);
                        allDfds.push(block.solve(this.scope, settings));
                        break;
                    } else {
                        allDfds.push(block.solve(this.scope, settings));
                    }
                }
            } else {
                this.onSuccess(this, settings);
            }

            return allDfds;
        },
        solveSubs: function (dfd, result, items, settings) {
            var thiz = this;
            settings.override = settings.override || {};
            settings.override['args'] = [this._currentForIndex];
            //more blocks?
            if (items.length) {
                var subDfds = thiz.runFrom(items, 0, settings);
                all(subDfds).then(function (what) {
                }, function (err) {
                    thiz.onDidRunItem(dfd, err, settings);
                });
                return subDfds;

            } else {
                thiz.onDidRunItem(dfd, result, settings);
            }
        },
        solveSubsDirect: function (dfd, result, items, settings) {
            var thiz = this;
            settings.override = settings.override || {};
            settings.override['args'] = [this._currentForIndex];
            //more blocks?
            if (items.length) {
                return thiz.runFromDirect(items, 0, settings);
            }
        },
        _solve: function (scope, settings) {
            var dfd = new Deferred(),
                self = this;
            var result = this.solveSubs(dfd, null, this.items, settings);
            if (result) {
                all(result).then(function (res) {
                    var falsy = res.indexOf(false);
                    if (self.ignoreErrors !== true && falsy !== -1) {
                        dfd.resolve(false);
                    } else {
                        dfd.resolve(true);
                    }
                });
            } else {
                dfd.resolve(true);
            }

            return dfd;
        },
        step: function (scope, settings) {
            var state = this._checkCondition(scope, settings);
            var dfd = new Deferred();
            if (state) {
                //run blocks
                var subs = this._solve(scope, settings);
                subs.then(function (result) {
                    if (result == true) {
                        dfd.resolve(true);
                    } else {
                        dfd.resolve(false);
                    }
                });
            }
            return dfd;
        },
        loop: function (scope, settings) {
            var stepResult = this.step(scope, settings),
                self = this;
            stepResult.then(function (proceed) {
                self._updateCounter(scope);
                self._currentForIndex = self._counter.value;
                if (proceed == true) {
                    self.loop(scope, settings);
                } else {
                    self.onFailed(self, settings);
                }
            });
        },
        _solveDirect: function (scope, settings) {
            return this.solveSubsDirect(null, null, this.items, settings);
        },
        stepDirect: function (scope, settings) {
            return this._solveDirect(scope, settings);
        },
        loopDirect: function (scope, settings) {
            this.stepDirect(scope, settings)
            for (var index = parseInt(this.initial, 10); index < parseInt(this['final'], 10); index++) {
                this.stepDirect(scope, settings);
            }
        },

        // solves the for block (runs the loop)
        solve: function (scope, settings) {
            // 1. Create and initialize counter variable
            this._counter = new Variable({
                title: this.counterName,
                value: this.initial,
                scope: scope,
                register: false
            });
            //prepare
            this._forState = true;
            this._currentForIndex = this.initial;
            this.deferred ? this.loop(scope, settings) : this.loopDirect(scope, settings);
            return [];
        },
        // checks the loop condition
        _checkCondition: function (scope, settings) {
            var expression = '' + this._counter.value + this.comparator + this['final'];
            var result = scope.parseExpression(expression);
            if (result != false) {
                this.onSuccess(this, settings);
            }
            this._forState = result;
            return result;
        },
        // updates the counter
        _updateCounter: function (scope) {
            var value = this._counter.value;
            var expression = '' + value + this.modifier;
            value = scope.parseExpression(expression);
            // Detect infinite loops
            if (value == this._counter.value) {
                return false;
            } else {
                this._counter.value = value;
                return true;
            }
        },
        /**
         * Store function override
         * @returns {boolean}
         */
        mayHaveChildren: function () {
            return this.items != null && this.items.length > 0;
        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            var result = [];
            if (this.items) {
                result = result.concat(this.items);
            }
            return result;
        },
        /**
         * should return a number of valid classes
         * @returns {Array}
         */
        canAdd: function () {
            return [];
        },
        /**
         * UI, Block row editor, returns the entire text for this block
         * @returns {string}
         */
        toText: function () {
            return this.getBlockIcon('F') + this.name + ' ' + this.initial + ' ' + this.comparator + ' ' + this['final'] + ' with ' + this.modifier;
        },
        /**
         * UI
         * @returns {*[]}
         */
        getFields: function () {
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields = fields.concat([
                this.utils.createCI('initial', 13, this.initial, {
                    group: 'General',
                    title: 'Initial',
                    dst: 'initial'
                }),
                this.utils.createCI('Final', 13, this['final'], {
                    group: 'General',
                    title: 'Final',
                    dst: 'final'
                }),
                this.utils.createCI('comparator', 13, this.comparator, {
                    group: 'General',
                    title: 'Comparision',
                    dst: 'comparator'
                }),
                this.utils.createCI('modifier', 13, this.modifier, {
                    group: 'General',
                    title: 'Modifier',
                    dst: 'modifier'
                }),
                this.utils.createCI('Abort on Error', 0, this.ignoreErrors, {
                    group: 'General',
                    title: 'Ignore Errors',
                    dst: 'ignoreErrors'
                }),
                this.utils.createCI('Deferred', 0, this.deferred, {
                    group: 'General',
                    title: 'Use Deferred',
                    dst: 'deferred'
                })
            ]);
            return fields;
        }
    });
});