define([
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/variables/Variable"
], function(dcl,Block,Variable){

    // summary:
    //		The while block model. It repeats a block a number of times, while the condition is true.
    //

    // module:
    //		xblox.model.loops.WhileBlock
    return dcl(Block,{

        declaredClass:"xblox.model.loops.WhileBlock",
        // condition: (String) expression to be evaluated every step
        condition: null,
        /**
         * Blocks to be executed while the condition is true
         * @type {xblox.model.Block[]}
         * @inheritDoc
         */
        items: null,

        loopLimit: 1500,

        name:'While',

        wait:0,

        _currentIndex:0,

        sharable:true,

        icon:"",

        _timer:null,

        //  standard call from interface
        canAdd:function(){
            return [];
        },
        _solve:function(scope,settings){
            var ret=[];
            for(var n = 0; n < this.items.length ; n++)
            {
                this.items[n].solve(scope,settings);
            }
            return ret;
        },

        doStep:function(settings){

            if(this._currentIndex < this.loopLimit){

                var ret = [];

                var _cond = this._checkCondition(this.scope);
                if(_cond) {
                    this.onSuccess(this,settings);
                    this.addToEnd( ret , this._solve(this.scope,settings));
                    this._currentIndex++;
                }else{
                    if(this._timer){
                        clearInterval(this._timer);
                    }


                    this.onFailed(this,settings);
                }
            }else{
                console.error('--while block : reached loop limit');
                this.reset();
            }
        },
        reset:function(){
            if(this._timer){
                clearTimeout(this._timer);
                this._timer = null;
            }
            this._currentIndex=0;


        },
        // solves the while block (runs the loop)
        solve:function(scope,settings) {

            //console.log('solve while ');
            this.loopLimit = 1500;
            settings = settings || { };

            var iterations = 0;

            var ret = [],
                thiz = this;

            var delay = this._getArg(this.wait);

            this.reset();

            // has delay
            if(delay>0){

                this._timer = setInterval(function(){
                    thiz.doStep(settings);
                },delay);

                return [];
            }

            // Evaluate condition
            while ((this._checkCondition(scope)) && (iterations < this.loopLimit)) {
                this._solve(scope,settings);
                iterations++;
            }
            //cleanup

            this.reset();

            return ret;

        },

        /**
         * Block row editor, returns the entire text for this block
         * @returns {string}
         */
        toText:function(){
            return this.getBlockIcon('G') + this.name + ' ' + this.condition;
        },

        // checks the loop condition
        _checkCondition:function(scope) {
            return scope.parseExpression(this.condition);
        },
        /**
         * Store function override
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren:function(parent){
            return this.items!=null && this.items.length>0;
        },

        /**
         * Store function override
         * @param parent
         * @returns {Array}
         */
        getChildren:function(parent){
            var result=[];

            if(this.items){
                result=result.concat(this.items);
            }
            return result;
        },
        getFields:function(){


            var thiz=this;

            var fields = this.inherited(arguments) || this.getDefaultFields();

            fields.push(

                this.utils.createCI('condition',25,this.condition,{
                    group:'General',
                    title:'Expression',
                    dst:'condition',
                    delegate:{
                        runExpression:function(val,run,error){
                            return thiz.scope.expressionModel.parse(thiz.scope,val,false,run,error);
                        }
                    }
                })
            );

            fields.push(this.utils.createCI('wait',13,this.wait,{
                    group:'General',
                    title:'Wait',
                    dst:'wait'
            }));
            return fields;
        }


    });
});