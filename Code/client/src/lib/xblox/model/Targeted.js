define([
    "dojo/_base/declare",
    "./Referenced"
], function(declare,Referenced){

    /**
     * Targeted provides functions to get an object through various ways
     */
    return declare('xblox.model.Targeted',[Referenced],{

    });
});