/** @module xblox/model/logic/IfBlock **/
define([
    "dcl/dcl",
    "xblox/model/Block",
    "xblox/model/Statement",
    "xblox/model/logic/ElseIfBlock",
    "dojo/Deferred",
    "xide/utils"
], function (dcl, Block, Statement, ElseIfBlock, Deferred, utils) {

    /**
     * Base block class.
     *
     * @class module:xblox/model/logic/IfBlock
     * @augments module:xblox/model/ModelBase
     * @extends module:xblox/model/Block
     */
    return dcl(Block, {
        declaredClass: "xblox.model.logic.IfBlock",
        // condition: (String) expression to be evaluated
        condition: 'Invalid Expression',

        // consequent: (Block) block to be run if the condition is true
        consequent: null,

        // elseIfBlocks: (optional) Array[ElseIfBlock] -> blocks to be run if the condition is false. If any of these blocks condition is
        //          true, the elseIf/else sequence stops
        elseIfBlocks: null,

        // alternate: (optional) (Block) -> block to be run if the condition is false and none of the "elseIf" blocks is true
        alternate: null,

        //  standard call from interface
        canAdd: function () {
            return [];
        },

        //  autoCreateElse : does auto creates the else part
        autoCreateElse: true,

        //  name : this name is displayed in the block row editor
        name: 'if',

        icon: '',
        //  add
        //
        // @param proto {mixed : Prototype|Object} : the new block's call prototype or simply a ready to use block
        // @param ctrArgs {Array} : constructor arguments for the new block
        // @param where {String} : consequent or alternate or elseif
        // @returns {Block}
        //
        add: function (proto, ctrArgs, where) {
            if (where == null) {
                where = 'consequent';
            }
            return this._add(proto, ctrArgs, where, false);
        },
        //  overrides default store integration
        __addToStore: function (store) {
            //add our self to the store
            store.put(this);
        },
        /**
         * Store function override
         * @returns {boolean}
         */
        mayHaveChildren: function () {
            return (this.items !== null && this.items.length) ||
                (this.elseIfBlocks !== null && this.elseIfBlocks.length) ||
                (this.consequent != null && this.consequent.length) ||
                (this.alternate != null && this.alternate.length);

        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            var result = [];
            if (this.consequent) {
                result = result.concat(this.consequent);
            }
            if (this.elseIfBlocks) {
                result = result.concat(this.elseIfBlocks);
            }
            if (this.alternate) {
                result = result.concat(this.alternate);
            }
            return result;
        },
        /**
         * Block row editor, returns the entire text for this block
         * @returns {string}
         */
        toText: function () {
            return "<span class='text-primary'>" + this.getBlockIcon('E') + this.name + " </span>" + "<span class='text-warning small'>" + this.condition + "<span>";
        },
        _checkCondition: function (scope) {
            return scope.parseExpression(this.condition, null, null);
        },
        solve: function (scope, settings) {
            // 1. Check the condition
            var solvedCondition = this._checkCondition(scope);
            var elseIfBlocks = this.getElseIfBlocks();
            var others = this.childrenByNotClass(ElseIfBlock);
            var result = null;

            others = others.filter(function (block) {
                return !block.isInstanceOf(Statement);
            });

            // 2. TRUE? => run consequent
            if (solvedCondition == true || solvedCondition > 0) {
                this.onSuccess(this, settings);
                if (others && others.length) {
                    for (var i = 0; i < others.length; i++) {
                        result = others[i].solve(scope, settings);
                    }
                }
                return result;
            } else {
                // 3. FALSE?
                var anyElseIf = false;
                this.onFailed(this, settings);
                if (elseIfBlocks) {
                    // 4. ---- check all elseIf blocks. If any of the elseIf conditions is true, run the elseIf consequent and
                    //           stop the process
                    for (var n = 0; ( n < elseIfBlocks.length ) && (!anyElseIf); n++) {
                        var _elseIfBlock = elseIfBlocks[n];
                        if (_elseIfBlock._checkCondition(scope)) {
                            _elseIfBlock.onSuccess(_elseIfBlock, settings);
                            anyElseIf = true;
                            return _elseIfBlock.solve(scope, settings);
                        } else {
                            _elseIfBlock.onFailed(_elseIfBlock, settings);
                        }
                    }
                }

                var alternate = this.childrenByClass(Statement);
                // 5. ---- If none of the ElseIf blocks has been run, run the alternate
                if (alternate.length > 0 && (!anyElseIf)) {
                    result = null;
                    for (var i = 0; i < alternate.length; i++) {
                        result = alternate[i].solve(scope, settings);
                    }
                    return result;
                }
            }
            return [];
        },
        /**
         * Default override empty. We have 3 arrays to clean : items, alternate and consequent
         */
        empty: function () {
            this._empty(this.alternate);
            this._empty(this.consequent);
            this._empty(this.elseIfBlocks);
        },
        /**
         * Deletes us or children block in alternate or consequent
         * @param what
         */
        removeBlock: function (what) {
            if (what) {
                if (what && what.empty) {
                    what.empty();
                }
                delete what.items;
                what.parent = null;
                this.alternate.remove(what);
                this.consequent.remove(what);
                this.elseIfBlocks.remove(what);
            }
        },
        // evaluate the if condition
        _getContainer: function (item) {
            if (this.consequent.indexOf(item) != -1) {
                return 'consequent';
            } else if (this.alternate.indexOf(item) != -1) {
                return 'alternate';
            } else if (this.elseIfBlocks.indexOf(item) != -1) {
                return 'elseIfBlocks';
            }
            return '_';
        },
        /**
         * Default override, prepare all variables
         */
        init: function () {
            this.alternate = this.alternate || [];
            this.consequent = this.consequent || [];
            this.elseIfBlocks = this.elseIfBlocks || [];

            for (var i = 0; i < this.alternate.length; i++) {
                this.alternate[i].parentId = this.id;
                this.alternate[i].parent = this;
            }
            for (var i = 0; i < this.elseIfBlocks.length; i++) {
                this.elseIfBlocks[i].parentId = this.id;
                this.elseIfBlocks[i].parent = this;
            }
            for (var i = 0; i < this.consequent.length; i++) {
                this.consequent[i].parentId = this.id;
                this.consequent[i].parent = this;
            }
            //var store = this.scope.blockStore;
        },
        getFields: function () {
            var thiz = this;
            var fields = this.inherited(arguments) || this.getDefaultFields();
            fields.push(
                this.utils.createCI('condition', this.types.ECIType.EXPRESSION_EDITOR, this.condition, {
                    group: 'General',
                    title: 'Expression',
                    dst: 'condition',
                    delegate: {
                        runExpression: function (val, run, error) {
                            return thiz.scope.expressionModel.parse(thiz.scope, val, false, run, error);
                        }
                    }
                })
            );
            return fields;
        },
        postCreate: function () {
            if (this._postCreated) {
                return;
            }
            this._postCreated = true;
        },
        toCode: function (lang, params) {
        },
        getElseIfBlocks: function () {
            return this.childrenByClass(ElseIfBlock);
        },
        runAction: function (action) {
            var store = this.scope.blockStore;
            var command = action.command;
            if (command === 'New/Else' || command === 'New/Else If') {
                var newBlockClass = command === 'New/Else If' ? ElseIfBlock : Statement;
                var args = utils.mixin({
                        name: 'else',
                        items: [],
                        dstField: 'alternate',
                        parentId: this.id,
                        parent: this,
                        scope: this.scope,
                        canAdd: function () {
                            return [];
                        },
                        canEdit: function () {
                            return false;
                        }
                    }, newBlockClass == ElseIfBlock ? {name: 'else if', dstField: 'elseIfBlocks'} : {
                        name: 'else', dstField: 'alternate'
                    }
                );

                var newBlock = this.add(newBlockClass, args, newBlockClass == Statement ? 'alternate' : 'elseIfBlocks');
                var defaultDfdArgs = {
                    select: [newBlock],
                    focus: true,
                    append: false,
                    expand: true,
                    delay: 10
                };
                var dfd = new Deferred();
                store._emit('added', {
                    target: newBlock
                });
                dfd.resolve(defaultDfdArgs);
                newBlock.refresh();
                return dfd;
            }
        },
        getActions: function () {
            var result = [];
            if (this.alternate.length == 0) {
                result.push(this.createAction({
                    label: 'Else',
                    command: 'New/Else',
                    icon: this.getBlockIcon('I'),
                    tab: 'Home',
                    group: 'File',
                    mixin: {
                        addPermission: true,
                        custom: true
                    }
                }));
            }
            result.push(this.createAction({
                label: 'Else If',
                command: 'New/Else If',
                icon: this.getBlockIcon('I'),
                tab: 'Home',
                group: 'File',
                mixin: {
                    addPermission: true,
                    custom: true
                }
            }));
            return result;
        }
    });
});