/** @module xblox/model/logic/SwitchBlock **/
define([
    'dcl/dcl',
    "xblox/model/Block",
    "xblox/model/logic/CaseBlock",
    "xblox/model/logic/DefaultBlock",
    "dojo/Deferred",
    "xide/lodash"
], function (dcl, Block, CaseBlock, DefaultBlock, Deferred, _) {
    /**
     *
     * @class module:xblox/model/logic/SwitchBlock
     * @extends module:xblox/model/Block
     */
    return dcl(Block, {
        declaredClass: "xblox.model.logic.SwitchBlock",
        items: null,
        name: 'Switch',
        icon: null,
        toText: function () {
            return this.getBlockIcon('H') + this.name + ' ';
        },
        /**
         *
         * @param what {module:xblox/model/Block}
         * @returns {*}
         */
        canAdd: function (what) {
            if(what && what.isInstanceOf){
                return what.isInstanceOf(CaseBlock) || what.isInstanceOf(DefaultBlock);
            }
            return [];
        },
        getFields: function () {
            return this.getDefaultFields(false, false);
        },
        /***
         * Solve the switchblock
         * @param scope
         * @param settings
         * @returns {string} execution result
         */
        solve: function (scope, settings) {
            this._stopped = false;
            var anyCase = false;    // check if any case is reached
            var ret = [];
            this.onSuccess(this, settings);
            // iterate all case blocks
            for (var n = 0; n < this.items.length; n++) {
                var block = this.items[n];

                if (block.declaredClass === 'xblox.model.logic.CaseBlock'/* instanceof CaseBlock*/) {
                    var caseret;
                    // solve each case block. If the comparison result is false, the block returns "false"
                    caseret = block.solve(scope, this, settings);
                    if (caseret != false) {
                        // If the case block return is not false, don't run "else" block
                        anyCase = true;
                        this.addToEnd(ret, caseret);
                        break;
                    }
                }
                if (this._stopped) {
                    break;
                }
            }
            // iterate all "else" blocks if none of the cases occurs
            if (!anyCase) {
                for (var n = 0; n < this.items.length; n++) {
                    var block = this.items[n];
                    if (!(block.declaredClass == 'xblox.model.logic.CaseBlock')) {
                        this.addToEnd(ret, block.solve(scope, settings));
                    }
                }
            }
            return ret;
        },
        /**
         * Store function override
         * @returns {Array}
         */
        getChildren: function () {
            return this.items;
        },
        stop: function () {
            this._stopped = true;
        },
        runAction: function (action) {
            var command = action.command;
            if (command === 'New/Case' || action.command === 'New/Default') {
                var store = this.scope.blockStore;
                var dfd = new Deferred();
                var newBlock = null;

                switch (command) {
                    case 'New/Case': {
                        newBlock = this.add(CaseBlock, {
                            comparator: "==",
                            expression: "on",
                            group: null
                        });
                        break;
                    }
                    case 'New/Default': {
                        newBlock = this.add(DefaultBlock, {
                            group: null
                        });
                        break;
                    }
                }
                dfd.resolve({
                    select: [newBlock],
                    focus: true,
                    append: false
                });
                newBlock.refresh();
                store._emit('added', {
                    target: newBlock
                });
            }
        },
        getActions: function (permissions, owner) {
            var result = [this.createAction({
                label: 'New Case',
                command: 'New/Case',
                icon: this.getBlockIcon('I'),
                tab: 'Home',
                group: 'File',
                mixin: {
                    addPermission: true,
                    custom: true,
                    quick: false
                }
            })];
            if (!_.find(this.items, {declaredClass: 'xblox.model.logic.DefaultBlock'})) {
                result.push(this.createAction({
                    label: 'Default',
                    command: 'New/Default',
                    icon: 'fa-eject',
                    tab: 'Home',
                    group: 'File',
                    mixin: {
                        addPermission: true,
                        custom: true,
                        quick: false
                    }
                }));
            }
            return result;
        }
    });
});