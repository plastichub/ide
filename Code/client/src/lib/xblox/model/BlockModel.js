define([
    'dcl/dcl',
    'xdojo/declare',
    'xide/data/Model',
    'xide/data/Source'
], function(dcl,declare,Model,Source){
    /**
     * Contains provides implements functions to deal with sub blocks.
     */
    return declare('xblox.model.BlockModel',[Model,Source],{
        declaredClass:'xblox.model.BlockModel',
        icon:'fa-play',
        /**
         * Store function override
         * @param parent
         * @returns {boolean}
         */
        mayHaveChildren: function (parent) {
            return this.items != null && this.items.length > 0;
        },
        /**
         * Store function override
         * @param parent
         * @returns {Array}
         */
        getChildren: function (parent) {
            return this.items;
        },
        getBlockIcon:function(){
            return '<span class="' +this.icon + '"></span>';
        }
    });
});