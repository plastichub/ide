define([
    "dojo/_base/lang",
    "dojo/on",
    "dcl/dcl",//make sure
    "delite/register",
    "delite/CustomElement",
    //explicit because a bootstrap might not be loaded at some point
    "xide/factory/Events",
    //explicit because a bootstrap might not be loaded at some point
    'xide/utils/StringUtils',
    'xide/types/Types',
    'xblox/model/Referenced',
    'xide/mixins/EventedMixin',
    'xide/mixins/ReloadMixin',
    'xwire/Binding',
    'xwire/EventSource',
    'xwire/WidgetTarget'
], function (lang, on, dcl,register, CustomElement, Events, utils, Types, Referenced, EventedMixin, ReloadMixin, Binding, EventSource, WidgetTarget) {
    var debugWidgets = false;
    var debugApp = false;
    var debugAttach = false;
    var debugCreated = false;
    var debugBinding = false;
    var debugRun = false;
    /**
     * Proxy widget to run a selected blox script on the parent widget/node.
     *
     * @class xblox/RunScript
     */
    var Impl = {
        declaredClass: 'xblox/_State',
        script:"",
        bidirectional: false,
        _targetBlock: null,
        _targetReference: null,
        _complete: false,
        enabled: true,
        stop: false,
        _events: [],
        context: null,
        name:"Default",
        isState:true,
        _isState:function(){
            return true;
        },
        /**
         * soft destroy
         */
        reset:function(){
            
        },
        getChildren: function () {
            return [];
        },
        /**
         * @inheritDoc
         */
        destroy: function () {
            this.onDestroy && this.onDestroy();
            this.reset();
        },
        /**
         * The final execution when 'target event' has been triggered. This
         * will run the select block.
         * @param event
         * @param val
         */
        run: function (event, val) {
            if (!this.enabled) {
                return;
            }            
        },
        /**
         * Callback when the minimum parameters are given: targetReference & targetBlock
         */
        onReady: function () {            
        },
        getEnclosingWidget: function (node) {
            if(node) {
                do {
                    if (node.nodeType === 1 && node.render) {
                        return node;
                    }
                } while ((node = node.parentNode));
            }
            return null;
        },
        initWithReference: function (ref) {
            //target node or widget
            if(ref.nodeType!==1){
                return;
            }
            this._targetReference = ref;
        },
        /**
         * Function to setup the target reference
         * on the surrounding widget!
         *
         */
        _setupTargetReference: function () {
            var i = 0,
                element = this,
                widget = null;

            while (i < 2 && !widget) {
                if (element) {
                    element = element.parentNode;
                    widget = this.getEnclosingWidget(element, "widgetId");
                    if (!widget) {
                        widget = this.getEnclosingWidget(element, "widgetid");
                    }
                }
                i++;
            }
            if (widget) {
                debugWidgets && console.info('have widget reference' + '  : ', [widget,this]);
                this.initWithReference(widget);
                if(widget._attached && widget.stateReady){
                    widget.stateReady(this);
                }

            } else {
                if (this.domNode && this.domNode.parentNode) {
                    this.initWithReference(this.domNode.parentNode);
                    debugWidgets && console.error('cant find widget reference, using parent node', this._targetReference);
                } else {
                    if(this.parentNode){
                        this.initWithReference(this.parentNode);
                    }
                    debugWidgets && console.error('cant find widget reference', this);
                }
            }
        },
        onAppReady: function (evt) {
            debugApp && console.log('-ready');
            //resolve target reference
            //if (!this._targetReference) {
                this._setupTargetReference();
            //}

            //track context {xapp/manager/Context}
            if (evt && evt.context) {
                this.context = evt.context;
            }
        },
        detachedCallback:function(){
            debugAttach && console.info('detachedCallback', this);
            if(this._appContext){
                this.destroy();
            }

        },
        applyTo:function(widget){
            
        },
        /**
         * Delite created callback
         */
        createdCallback: function () {
            debugCreated && console.info('createdCallback', this);
            if (!this._targetReference) {
                this._setupTargetReference();
                if(this._targetReference && this._targetReference.stateReady){
                    this._targetReference.stateReady(this);
                }
            }
        },
        attachedCallback: function () {
            debugAttach && console.info('attachedCallback', this);
            if (this._started) {
                return;
            }
            this.onAppReady();//emulates
            this.subscribe(Types.EVENTS.ON_APP_READY);
            this._started = true;
        }

    };
    //package and declare via dcl
    var _class = dcl([EventedMixin.dcl,Referenced.dcl], Impl);
    return _class; 
});