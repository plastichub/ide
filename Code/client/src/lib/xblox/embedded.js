define([
    'dojo/_base/declare',
    'xide/types',
    'xblox/types/Types',
    'xide/factory',
    'xide/utils',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin',
    "xblox/model/logic/CaseBlock",
    "xblox/model/Block",
    "xblox/model/functions/CallBlock",
    "xblox/model/code/CallMethod",
    "xblox/model/code/RunScript",
    "xblox/model/code/RunBlock",
    "xblox/model/loops/ForBlock",
    "xblox/model/loops/WhileBlock",
    "xblox/model/variables/VariableAssignmentBlock",
    "xblox/model/logic/IfBlock",
    "xblox/model/logic/ElseIfBlock",
    "xblox/model/logic/SwitchBlock",
    "xblox/model/variables/VariableSwitch",
    "xblox/model/events/OnEvent",
    "xblox/model/events/OnKey",
    "xblox/model/logging/Log",
    "xblox/model/html/SetStyle",
    "xblox/model/html/SetCSS",
    "xblox/model/html/SetStyle",
    "xblox/manager/BlockManager",
    "xblox/factory/Blocks",
    "xdojo/has!xblox-ui?xblox/model/Block_UI"
], function () {
    if(!Array.prototype.remove){
        Array.prototype.remove= function(){
            var what, a= arguments, L= a.length, ax;
            while(L && this.length){
                what= a[--L];
                if(this.indexOf==null){
                    break;
                }
                while((ax= this.indexOf(what))!= -1){
                    this.splice(ax, 1);
                }
            }
            return this;
        };
    }
    if(!Array.prototype.swap){
        Array.prototype.swap = function (x,y) {
            var b = this[x];
            this[x] = this[y];
            this[y] = b;
            return this;
        };
    }

    if ( typeof String.prototype.startsWith != 'function' ) {
        String.prototype.startsWith = function( str ) {
            return this.substring( 0, str.length ) === str;
        };
    }

    if ( typeof String.prototype.endsWith != 'function' ) {
        String.prototype.endsWith = function( str ) {
            return this.substring( this.length - str.length, this.length ) === str;
        };
    }

    if(!Function.prototype.bind) {
        // Cheap polyfill to approximate bind(), make Safari happy
        Function.prototype.bind = Function.prototype.bind || function (that) {
            return dojo.hitch(that, this);
        };
    }
});