define([
	'intern!tdd',
	'intern/chai!assert',
	'intern/dojo/node!leadfoot/helpers/pollUntil',
	'intern/dojo/node!leadfoot/keys',
	'require'
], function (test, assert, pollUntil, keys, require) {
	function testUpDownKeys(gridId, cellNavigation) {

		return function () {
			return true;
		};
	}

	test.suite('Keyboard functional tests', function () {

		/*
		test.before(function () {
			// Get our html page. This page should load all necessary scripts
			// since this functional test module runs on the server and can't load
			// such scripts. Further, in the html page, set a global "ready" var
			// to true to tell the runner to continue.
			return this.get('remote')
				.get(require.toUrl('./Keyboard.html'))
				.then(pollUntil(function () {
					return window.ready;
				}, null, 5000));
		});
		*/

		test.test('grid (cellNavigation: true) -> up + down arrow keys',
			testUpDownKeys('grid', true));
	});
});
