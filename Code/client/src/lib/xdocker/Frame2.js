define([
    "dcl/dcl",
    "wcDocker/frame"
], function (dcl,frame) {

    var Module = dcl([frame],{
        _showTitlebar:true,
        panel: function (tabIndex, autoFocus) {
            if (typeof tabIndex !== 'undefined') {
                if (this.isCollapser() && tabIndex === this._curTab) {
                    this.collapse();
                    tabIndex = -1;
                }
                if (tabIndex < this._panelList.length) {
                    this.$tabBar.find('> .wcTabScroller > .wcPanelTab[id="' + this._curTab + '"]').removeClass('wcPanelTabActive active');
                    this.$center.children('.wcPanelTabContent[id="' + this._curTab + '"]').addClass('wcPanelTabContentHidden');
                    if (this._curTab !== tabIndex) {
                        this.collapse();
                    }
                    this._curTab = tabIndex;
                    if (tabIndex > -1) {
                        this.$tabBar.find('> .wcTabScroller > .wcPanelTab[id="' + tabIndex + '"]').addClass('wcPanelTabActive active');
                        this.$center.children('.wcPanelTabContent[id="' + tabIndex + '"]').removeClass('wcPanelTabContentHidden');
                        this.expand();
                    }
                    this.__updateTabs(autoFocus);
                }
            }

            if (this._curTab > -1 && this._curTab < this._panelList.length) {
                return this._panelList[this._curTab];
            } else if (this.isCollapser() && this._panelList.length) {
                return this._panelList[0];
            }
            return false;
        },
        __init: function () {
            this.$frame = $('<div class="wcFrame wcWide wcTall widget">');
            this.$title = $('<div class="wcFrameTitle">');
            this.$titleBar = $('<div class="wcFrameTitleBar wcFrameTopper">');
            this.$tabBar = $('<header class="wcFrameTitleBar">');
            this.$tabScroll = $('<ul class="wcTabScroller nav nav-tabs">');
            this.$center = $('<div class="wcFrameCenter wcPanelBackground">');
            this.$tabLeft = $('<div class="wcFrameButton" title="Scroll tabs to the left."><span class="fa fa-arrow-left"></span>&lt;</div>');
            this.$tabRight = $('<div class="wcFrameButton" title="Scroll tabs to the right."><span class="fa fa-arrow-right"></span>&gt;</div>');
            this.$close = $('<div class="wcFrameButton" title="Close the currently active panel tab"><div class="fa fa-close"></div>X</div>');
            this.$collapse = $('<div class="wcFrameButton" title="Collapse the active panel"><div class="fa fa-download"></div>C</div>');
            this.$buttonBar = $('<div class="wcFrameButtonBar">');
            this.$tabButtonBar = $('<div class="wcFrameButtonBar">');

            this.$tabBar.append(this.$tabScroll);
            this.$tabBar.append(this.$tabButtonBar);
            this.$frame.append(this.$buttonBar);
            this.$buttonBar.append(this.$close);
            this.$buttonBar.append(this.$collapse);
            this.$frame.append(this.$center);

            if (this._isFloating) {
                this.$top = $('<div class="wcFrameEdgeH wcFrameEdge"></div>').css('top', '-6px').css('left', '0px').css('right', '0px');
                this.$bottom = $('<div class="wcFrameEdgeH wcFrameEdge"></div>').css('bottom', '-6px').css('left', '0px').css('right', '0px');
                this.$left = $('<div class="wcFrameEdgeV wcFrameEdge"></div>').css('left', '-6px').css('top', '0px').css('bottom', '0px');
                this.$right = $('<div class="wcFrameEdgeV wcFrameEdge"></div>').css('right', '-6px').css('top', '0px').css('bottom', '0px');
                this.$corner1 = $('<div class="wcFrameCornerNW wcFrameEdge"></div>').css('top', '-6px').css('left', '-6px');
                this.$corner2 = $('<div class="wcFrameCornerNE wcFrameEdge"></div>').css('top', '-6px').css('right', '-6px');
                this.$corner3 = $('<div class="wcFrameCornerNW wcFrameEdge"></div>').css('bottom', '-6px').css('right', '-6px');
                this.$corner4 = $('<div class="wcFrameCornerNE wcFrameEdge"></div>').css('bottom', '-6px').css('left', '-6px');

                this.$frame.append(this.$top);
                this.$frame.append(this.$bottom);
                this.$frame.append(this.$left);
                this.$frame.append(this.$right);
                this.$frame.append(this.$corner1);
                this.$frame.append(this.$corner2);
                this.$frame.append(this.$corner3);
                this.$frame.append(this.$corner4);
            }
            this.__container(this.$container);
            if (this._isFloating) {
                this.$frame.addClass('wcFloating');
            }
            this.$center.scroll(this.__scrolled.bind(this));
        },
        /**
         * Get/Set for 'show title bar'
         * @param show {boolean}
         * @returns {boolean}
         */
        showTitlebar:function(show){
            if(show!=null) {
                var prop = show ? 'inherit' : 'none',
                    what = 'display',
                    who = this;

                who.$titleBar.css(what, prop);
                who.$tabBar.css(what, prop);
                who.$center.css('top', show ? 30 : 0);
                this._showTitlebar = show;
            }
            return this._showTitlebar
        },
        currentPanel:function(){
            return this._panelList[this._curTab];
        },
        __widgets:function(){
            return this._panelList;
        },
        panels:function(){
            return this._panelList;
        },
        panelAt:function(index){
            return this._panelList[index];
        },
        panelIndex:function(panel) {
            return _.findIndex(this._panelList,panel);
        }
    });    
    return Module;
});