define([
    "dcl/dcl",
    'wcDocker/splitter',
    'xide/mixins/EventedMixin'
], function (dcl,wcSplitter,EventedMixin) {



    /**
     * Function to set a wcFrame's tab bar elements
     * @param frame
     * @param hide
     */
    function hideFrameTabBar(frame,hide){
        _.invoke(frame.$tabBar, frame.$buttonBar,'css',['display',hide ===true ? 'none' : 'inherit']);
    }
    /**
     * Function to preserve a wcFrame's most important properties before collapse:
     *  - minSize
     *  - pos
     *  - all child panel's minSize
     *  - all child panel's titles
     *
     * @param frame
     * @param hide
     */
    function setFramePropsMin(frame,hide){

        frame._saveProp('minSize',[0,0]);
        frame._saveProp('pos');
        frame._saveProp('showTitlebar',[]);//call with undefined

        hide!==false && hideFrameTabBar(frame,hide);

        frame._panelList && _.each(frame._panelList,function(panel){
            //save title & min size
            panel._saveProp('minSize',[0,0]);
            panel._saveProp('title');
        });
        hide && frame.showTitlebar && frame.showTitlebar(false);
    }
    /**
     * Function to restore wcFrame properties: minSize, title, showTitlebar
     * @param frame
     */
    function restoreFrameProps(frame){
        hideFrameTabBar(frame,false);
        _.each(frame._panelList,function(panel){
            var ms = panel._restoreProp('minSize',false);
            ms && panel.minSize(ms.x, ms.y);
            panel._restoreProp('title');
        });
        frame._restoreProp('showTitlebar');
    }

    /**
     * Extend splitter for evented and collapse functions
     */
    return dcl([wcSplitter,EventedMixin.dcl], {
        _lastCollapsed:null,
        _isCollapsed:false,
        // Updates the size of the splitter.
        isExpanded:function(){
            return this._lastCollapsed ==null;
        },
        isCollapsed:function(){
            return this._lastCollapsed !=null;
        },
        collapse:function(side,pos){
            this._isCollapsed = true;
            this._saveProp('pos');
            setFramePropsMin(this._pane[0],side==0);
            setFramePropsMin(this._pane[1],side==1);
            this._lastCollapsed = side;
            this.pos( pos!=null ? pos : 1);
        },
        expand:function(){
            this._isCollapsed = false;
            restoreFrameProps(this._pane[0]);
            this._restoreProp('pos');
            restoreFrameProps(this._pane[1]);
            this._lastCollapsed = null;
        }
    });
});