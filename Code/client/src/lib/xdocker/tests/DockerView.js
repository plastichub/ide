/** @module xgrid/Base **/
define([
    'xdojo/declare',
    'xide/types',
    'xdocker/Docker2',
    'xdocker/Panel2',
    'xide/tests/TestUtils',
    'xfile/tests/TestUtils',
    'dcl/dcl',
    'require',
    'xide/registry',
    'xide/utils',
    'xide/views/_LayoutMixin',
    'xide/_base/_Widget',
    'xaction/ActionProvider',
    'xfile/Breadcrumb',
    'xide/widgets/MainMenu',
    'module'

], function (declare,types,Docker,Panel,TestUtils,FTestUtils,dcl,require,registry,utils,_LayoutMixin,_Widget,ActionProvider,Breadcrumb,MainMenu,module) {

    console.clear();


    /***
     *
     * playground
     */
    var leftGrid = window[module.id + 'leftGrid'];
    var ctx = window.sctx,
        parent;

    var LayoutClass = dcl(null,{});



    var BorderLayoutClass = dcl(LayoutClass,{

        createLayout:function(docker){

            var top;
            top = this.layoutTop = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.TOP, null, {
                h: 50,
                title:false,
                mixin:{
                    resizeToChildren:true,
                    _scrollable : {
                        x: false,
                        y: false
                    }
                }
            });


            //this.layoutTop._parent.$container.css('padding:0 !important');
            top.resizeToChildren = true;
            this.layoutTop._minSize.y = 50;
            //this.layoutTop._maxSize.y = 50;
            //this.layoutTop.initSize(100,'100%');
            this.layoutLeft = docker.addPanel('DefaultFixed', types.DOCKER.DOCK.BOTTOM, null, {
                w: 100,
                title: false
            });

            this.layoutLeft._minSize.x = 150;
            this.layoutLeft._maxSize.x = 150;
            //console.dir(top._parent.$container.css('padding'));
            //console.log( _c.height() + ' ' + _c.outerHeight(),_c);
            docker.resize();

        }
    });

    var DockerView = dcl([_Widget,_LayoutMixin.dcl,ActionProvider.dcl,BorderLayoutClass],{
        templateString:'<div style="height: 100%;width: 100%;"></div>',
        startup:function(){

            var docker = Docker.createDefault(this.domNode);
            this._docker = docker;

            //console.dir(docker);
            this.createLayout(docker);
        }

    });







    function completeView(view){

        var _args = {
            style:'height:auto',
            resizeToParent:false
        };
        var top = view.layoutTop;

        var mainMenu = utils.addWidget(MainMenu, _args, this, top, true);
        $(mainMenu.domNode).css('height:auto');


        var breadcrumb = utils.addWidget(Breadcrumb, {
            resizeToParent:false
        }, null, top, true);

        breadcrumb.resizeToParent=false;
        $(breadcrumb.domNode).css('height:auto');

        var grid = FTestUtils.createFileGrid('root',{},
            //overrides
            {

            },'TestGrid',module.id,true,view.layoutLeft);



        breadcrumb.setSource(grid);
        var srcStore = grid.collection;

        function _onChangeFolder(store,item,grid){
            if(breadcrumb.grid!=grid){
                breadcrumb.setSource(grid);
            }
            breadcrumb.clear();
            breadcrumb.setPath('.',srcStore.getRootItem(),item.getPath(),store);
        }
        grid._on('openedFolder', function (evt) {
            console.error('opened folder!!');
            _onChangeFolder(srcStore,evt.item,grid);
        });

        mainMenu._registerActionEmitter(grid);
        mainMenu.setActionEmitter(grid);

        var totalHeight = utils.getHeight([mainMenu,breadcrumb]);
        console.error('total height: ' +totalHeight );

        top._minSize.y = totalHeight;

        top.set('height',totalHeight);


        //top._maxSize.y = totalHeight;

        top.resize();




        var topFrame = top.getFrame();

        topFrame.panelIndex2=function(panel){
            return _.findIndex(this._panelList,panel);
        }

        console.log('panel index '+ topFrame.panelIndex2(top));
        //console.dir(top._findWidgets());




    }

    function testDockerView(parent){
        var view = utils.addWidget(DockerView,{
            resizeToParent:true
        },null,parent,true);

        completeView(view);
    }


    function testMain(tab){


        var mainView = ctx.mainView;
        var mainDocker = mainView.getDocker();
        var parent = TestUtils.createTab(null,null,module.id);
        parent.uuid = 'main tab';

        setTimeout(function(){
            testDockerView(parent);
        },1000);

    }

    if (ctx) {
        var doTest = true;
        if (doTest) {
            var mainView = ctx.mainView;
            testMain();

        }
    }
    return declare('m',null,[]);
});