/** @module xgrid/Base **/
define([
    'xide/types',
    'xgrid/Grid'

], function (types,
             Grid) {




    /***
     * playground
     */
    var _lastGrid = window._lastGrid;
    var ctx = window.sctx,
        parent;



    function testMain(grid){}

    if (ctx) {


        var doTest = true;
        if (doTest) {

            var mainView = ctx.mainView;

            var docker = mainView.getDocker();
            if(window._lastGrid){
                docker.removePanel(window._lastGrid);
            }
            parent = docker.addTab(null, {
                title: 'Documentation',
                icon: 'fa-folder'
            });

            var thiz = this;


            window._lastGrid = parent;


            setTimeout(function () {
                mainView.resize();
            }, 1000);

            function test() {
                testMain();
            }

            setTimeout(function () {
                test();
            }, 2000);



        }
    }

    return Grid;

});