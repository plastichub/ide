define([
    "dcl/dcl",
    "wcDocker/docker",
    "wcDocker/panel",
    "wcDocker/frame",
    "wcDocker/splitter",
    "wcDocker/base"
], function (dcl,wcDocker,wcPanel,wcFrame,wcSplitter,base){


    var myPanelClass = dcl(wcPanel,{
        close: dcl.superCall(function(sup){
            return function(){

                //call super

                return sup && sup.apply(this, arguments);
            };
        })
    })


    //  extend wcDocker::base for this functions so they are accessible in wcPanel and
    //  wcFrame and others
    $.extend(base.prototype,{
        _saveProp: function (what, args) {
            var who = this;
            who['__' + what] = _.isFunction(who[what]) ? who[what]() : who[what];
            args && who[what].apply(who, args);
        },
        _restoreProp: function (what, call) {

            var who = this;

            //this prop also exists
            if (who['_' + what]) {
                who['_' + what] = who['__' + what];
            }
            var _args = who['__' + what];
            if (call !== false) {
                return _.isFunction(who[what]) ? who[what].apply(who, [_args]) : who[what];
            } else {
                return _args;
            }
        }
    });


    //now sub class wcFrame
    wcFrame = dcl(wcFrame,{
        _showTitlebar:true,
        showTitlebar:function(show){

            if(show!=null) {

                var prop = show ? 'inherit' : 'none',
                    what = 'display',
                    who = this;

                who.$titleBar.css(what, prop);
                who.$tabBar.css(what, prop);
                who.$center.css('top', show ? 30 : 0);

                this._showTitlebar = show;
            }

            return this._showTitlebar
        },
        currentPanel:function(){
            return this._panelList[this._curTab];
        },
        panelIndex:function(panel) {
            return _.findIndex(this._panelList,panel);
        }
    });


    /**
     *  now sub class wcSplitter
     *
     *  it needs a few helper functions
     */

    /**
     * Function to set a wcFrame's tab bar elements
     * @param frame
     * @param hide
     */
    function hideFrameTabBar(frame,hide){
        _.invoke(frame.$tabBar, frame.$buttonBar,'css',['display',hide ===true ? 'none' : 'inherit']);
    }

    /**
     * Function to preserve a wcFrame's most important properties before collapse:
     *  - minSize
     *  - pos
     *  - all child panel's minSize
     *  - all child panel's titles
     *
     * It also calls
     *
     * @param frame
     * @param hide
     */
    function setFramePropsMin(frame,hide){

        frame._saveProp('minSize',[0,0]);
        frame._saveProp('pos');
        frame._saveProp('showTitlebar',[]);//call with undefined

        hide!==false && hideFrameTabBar(frame,hide);

        _.each(frame._panelList,function(panel){
            //save title & min size
            panel._saveProp('minSize',[0,0]);
            panel._saveProp('title');
        });

        hide && frame.showTitlebar(false);

    }
    function restoreFrameProps(frame){

        hideFrameTabBar(frame,false);

        _.each(frame._panelList,function(panel){
            //restore title & min size
            var ms = panel._restoreProp('minSize',false);
            if(ms) {
                panel.minSize(ms.x, ms.y);
            }
            panel._restoreProp('title');
        });

        frame._restoreProp('showTitlebar');

    }


    //the splitter sub class
    wcSplitter = dcl(wcSplitter,{
        _lastCollapsed:null,
        isExpanded:function(){
            return this._lastCollapsed ==null;
        },
        isCollapsed:function(){
            return this._lastCollapsed !=null;
        },
        collapse:function(side,pos){
            var pane0 = this._pane[0],
                pane1 = this._pane[1];

            this._saveProp('pos');

            setFramePropsMin(pane0,side==0);
            setFramePropsMin(pane1,side==1);

            this._lastCollapsed = side;

            this.pos( pos!=null ? pos : 1);
        },
        expand:function(){

            var pane0 = this._pane[0],
                pane1 = this._pane[1];

            restoreFrameProps(pane0);
            this._restoreProp('pos');
            restoreFrameProps(pane1);
            this._lastCollapsed = null;
        }

    });


    /**
     * Now we subclass wcPanel
     */

    wcPanel = dcl(wcPanel,{
        /**
         * Helper function to get a instance by class name upwards
         * @param className
         * @returns {*}
         * @private
         */
        _parentByClass: function(className) {
            var parent = this._parent;
            if(_.isString(className)) {
                while (parent && !(parent.declaredClass.indexOf(className)!==-1)) {
                    parent = parent._parent;
                }
            }
            return parent;
        },
        /**
         * Helper to return parent frame
         * @returns {*}
         */
        getFrame:function(){
            return this._parentByClass('Frame');
        },
        getSplitter:function(){
            return this._parentByClass('Splitter');
        },
        isExpanded:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return true;
            }
            return splitter.isExpanded();
        },
        isCollapsed:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            return splitter.isCollapsed();
        },
        expand:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            splitter.expand();
        },
        collapse:function(){
            var splitter = this.getSplitter();
            if(!splitter){
                return false;
            }
            splitter.collapse(splitter._pane[0].panelIndex(this) == -1 ? 1 : 0);
        }
    });

    var myDocker = new wcDocker('.dockerContainer', {
        allowDrawers: true,
        responseRate: 10,
        wcPanelClass:wcPanel,
        wcSplitterClass:wcSplitter,
        wcFrameClass:wcSplitter
    });


    //now you can call expand and collapse on panels :-)

});