define([
    "dcl/dcl",
    "dcl/inherited",
    'xide/types',
    'wcDocker/tabframe',
    "wcDocker/types"
], function (dcl,inherited,types, tabframe,wcDocker) {

    return dcl([tabframe], {

        // Initialize
        __init: function () {
            this.$frame = $('<div class="wcCustomTab wcWide wcTall widget">');
            this.$tabBar = $('<header class="wcFrameTitleBar wcCustomTabTitle wcWide">');
            this.$tabScroll = $('<div class="wcTabScroller">');
            this.$center = $('<div class="wcFrameCenter wcPanelBackground">');
            this.$tabLeft = $('<div class="wcFrameButton" title="Scroll tabs to the left."><span class="fa fa-arrow-left"></span>&lt;</div>');
            this.$tabRight = $('<div class="wcFrameButton" title="Scroll tabs to the right."><span class="fa fa-arrow-right"></span>&gt;</div>');
            this.$close = $('<div class="wcFrameButton" title="Close the currently active panel tab"><span class="fa fa-close"></span>X</div>');

            //this.$maximize = $('<div class="wcFrameButton" title="Close the currently active panel tab"><span class="fa fa-expand"></span>X</div>');
            this.$buttonBar = $('<div class="wcFrameButtonBar">');


            this.$tabBar.append(this.$tabScroll);
            this.$tabBar.append(this.$buttonBar);
            this.$buttonBar.append(this.$close);

            //this.$buttonBar.append(this.$maximize);

            this.$frame.append(this.$center);
            this.$frame.append(this.$tabBar);

            this.__container(this.$container);

            this._boundEvents.push({event: wcDocker.EVENT.UPDATED, handler: this.update.bind(this)});
            this._boundEvents.push({event: wcDocker.EVENT.CLOSED, handler: this.destroy.bind(this)});

            for (var i = 0; i < this._boundEvents.length; ++i) {
                this._parent.on(this._boundEvents[i].event, this._boundEvents[i].handler);
            }

            var docker = this.docker();
            if (docker) {
                docker._tabList.push(this);
            }
        }
    });
});