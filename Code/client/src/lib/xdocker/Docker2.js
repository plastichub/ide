/** @module xDocker/Docker2 */
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'wcDocker/docker',
    'xdocker/Panel2',
    'xdocker/Frame2',
    'xdocker/Splitter2',
    'xide/mixins/EventedMixin',
    'xide/registry',
    'wcDocker/types',
    'wcDocker/base',
    'xaction/ActionProvider',
    'xide/widgets/_Widget',
    'xide/widgets/ContextMenu',
    'require'
], function (dcl, types, utils, wcDocker, Panel2, Frame2, Splitter2, EventedMixin, registry, dTypes, base, ActionProvider, _CWidget, ContextMenu, require) {
    /**
     * @class module:xDocker/Docker2
     * @extends module:wcDocker/docker
     */
    var Module = dcl([wcDocker, base, ActionProvider.dcl, _CWidget.dcl, EventedMixin.dcl], {
        isResizing: false,
        _last: null,
        _lastResize: null,
        _needResize: false,
        _lastResizeTimer: null,
        _declaredClass: 'xdocker.Docker2',
        __eventHandles: [],
        contextMenu: null,
        contextMenuEventTarget: null,
        contextMenuPanelId: null,
        setupActions: function (_mixin) {
            this.__on(this._root.$container, 'contextmenu', '.wcPanelTab', function (e) {
                self.contextMenuEventTarget = e.target;
                self.contextMenuPanelId = e.target.parentNode ? e.target.parentNode.id : null;
            });

            var self = this,
                //default action mixin
                mixin = {
                    addPermission: true
                },
                defaultProps = {
                    group: 'Misc',
                    tab: 'View',
                    mixin: _mixin || mixin
                },
                _actions = [],
                action = function (label, command, icon) {
                    return self.createAction(_.extend({
                        label: label,
                        command: command,
                        icon: icon
                    }, defaultProps));
                },
                actions = this.addActions([
                    action('Close', 'View/Close', 'fa-close'),
                    action('Close others', 'View/Close Others', 'fa-close'),
                    action('Close all in group', 'View/Close all group', 'fa-close'),
                    action('Split Horizontal', 'View/Split Horizontal', 'fa-columns'),
                    action('Split Vertical', 'View/Split Vertical', 'fa-columns')
                ]),
                node = this._root.$container[0],
                contextMenu = new ContextMenu({
                    owner: this,
                    delegate: this,
                    limitTo: 'wcPanelTab',
                    openTarget: node
                }, node);

            contextMenu.init({
                preventDoubleContext: false
            });
            contextMenu.setActionEmitter(this, types.EVENTS.ON_VIEW_SHOW, this);
            this.contextMenu = contextMenu;
            this.add(contextMenu, null, false);

            return actions;
        },
        runAction: function (action, type, event) {

            var DOCKER_TYPES = types.DOCKER,
                _panel = this.contextMenuEventTarget ? this.__panel(this.contextMenuEventTarget) : null,
                self = this,
                frame = _panel ? _panel.getFrame() : null,
                panels = frame ? frame.panels() : null;

            if (action.command === 'View/Close') {
                if (_panel) {
                    this.removePanel(_panel);
                }
            }

            if (action.command === 'View/Close all group' && _panel) {
                if (frame) {
                    var toRemove = [];
                    _.each(panels, function (panel) {
                        if (panel && panel._moveable && panel._closeable) {
                            toRemove.push(panel);
                        }
                    });
                    _.each(toRemove, function (panel) {
                        self.removePanel(panel);
                    })
                }

            }
            if (action.command === 'View/Close Others' && _panel) {
                frame && _.each(panels, function (panel) {
                    if (panel && panel != _panel && panel._moveable && panel._closeable) {
                        self.removePanel(panel);
                    }
                });

                _panel.select();
            }

            function split(direction) {
                var otherPanel = _panel.next(-1) || _panel.next(1);
                if (otherPanel && _panel != otherPanel) {

                    self.movePanel(otherPanel, direction == DOCKER_TYPES.ORIENTATION.HORIZONTAL ?
                        DOCKER_TYPES.DOCK.BOTTOM : DOCKER_TYPES.DOCK.RIGHT, _panel, direction);

                    var splitter = otherPanel.getSplitter();
                    splitter && splitter.pos(0.5);
                }
            }

            if (action.command === 'View/Split Horizontal' && _panel) {
                split(DOCKER_TYPES.ORIENTATION.HORIZONTAL);
            }
            if (action.command === 'View/Split Vertical' && _panel) {
                split(DOCKER_TYPES.ORIENTATION.VERTICAL);
            }
        },
        __panel: function (el) {

            var panels = this.getPanels();
            var frame = null,
                self = this;

            _.each(this._frameList, function (_frame) {
                if ($.contains(_frame.$container[0], el)) {
                    frame = _frame;
                }
            });

            var id = self.contextMenuPanelId;
            this.contextMenuEventTarget = null;
            this.contextMenuPanelId = null;
            if (frame && id !== null) {
                var _panel = frame.panel(id);
                if (_panel) {
                    return _panel;
                }
            }
        },
        /**********************************************************************/
        allPanels: function () {
            var result = [];
            _.each(this._frameList, function (frame) {
                _.each(frame._panelList, function (panel) {
                    result.push(panel);
                });
            });
            return result;
        },
        destroy: function () {
            registry.remove(this.id);
            this.__destroy();
        },
        /**
         * Collect wcDocker's body event handles
         * @param element {HTMLElement}
         * @param type {string} event type
         * @param selector {string|null|function}
         * @param handler {function|null}
         * @private
         */
        __on: function (element, type, selector, handler) {

            if (typeof selector == 'function' && !handler) {
                //no selector given, swap arg[3] with arg[2]
                handler = selector;
                selector = null;
            }

            element.on(type, selector, handler);

            this.__eventHandles.push({
                element: element,
                type: type,
                selector: selector,
                handler: handler
            });
        },
        __destroy: function () {
            var self = this;
            //docker handles
            _.each(self.__eventHandles, function (handle) {
                handle && handle.element && handle.element.off(handle.type, handle.selector, handle.handler);
                self.__eventHandles.remove(handle);
            });

            //xide handles
            _.each(self._events, function (handles, type) {
                _.each(handles, function (handler) {
                    self.off(type, handler);
                });
            });

            self.clear();
            delete self._events;
            delete self.__eventHandles;
            self._updateId && clearTimeout(self._updateId);
        },
        resize: function (force, noob, why) {
            if (why === 'deselected') {
                return;
            }
            var self = this;

            function _resize() {
                if (self.$container) {
                    var h = self.$container.height();
                    var w = self.$container.width();
                    if (h < 110 || w < 110) {
                        return;
                    }
                }
                self._dirty = true;
                self._root && self._root.__update(true);
                _.invoke(self._floatingList, '__update');
            }
            return this.debounce('resize', _resize.bind(this), 100, null);
        },
        /**
         * Helper
         * @returns {xdocker/Panel[]}
         */
        getPanels: function () {
            var result = [];
            _.each(this._frameList, function (frame) {
                _.each(frame._panelList, function (panel) {
                    result.push(panel);
                });
            });
            return result;
        },
        /**
         * Returns a default panel
         * @returns {xdocker/Panel}
         */
        getDefaultPanel: function () {
            return _.find(this.getPanels(), {
                isDefault: true
            });
        },
        /**
         * Std API
         * @todo convert to addChild
         * @param mixed {string|object}
         * @param options {object}
         * @param options.title {string|null}
         * @param options.tabOrientation {string|null}
         * @param options.location {string|null}
         * @param options.select {boolean}
         * @param options.target {object}
         * @returns {*}
         */
        addTab: function (mixed, options) {

            var panel = null,
                thiz = this;

            options = options || {};
            mixed = mixed || 'DefaultTab';

            /**
             * Complete options to default
             * @param options
             */
            function completeOptions(options) {

                if (!('tabOrientation' in options)) {
                    options.tabOrientation = types.DOCKER.TAB.TOP
                }

                if (!('location' in options)) {
                    options.location = types.DOCKER.DOCK.STACKED;
                }

                if (!('target' in options)) {
                    options.target = thiz.getDefaultPanel();
                }

                if (!('select' in options)) {
                    options.select = true;
                }

                if (!('title' in options)) {
                    options.title = ' ';
                }
            }
            completeOptions(options);

            if (_.isString(mixed)) {
                panel = this.addPanel(mixed, options.location, options.target, options);
            } else if (_.isObject(mixed)) {
                panel = mixed;
            }

            function applyOptions(who, what) {
                for (var option in what) {
                    if (_.isFunction(who[option])) {
                        who[option](what[option]);
                    }
                }
            }
            applyOptions(panel, options);
            if (options.select === true) {
                panel.select();
            }
            return panel;
        }
    });
    /**
     * Registration of a panel type
     * @todo typos & API
     * @static
     * @param label
     * @param iconClass
     * @param closable
     * @param collapsible
     * @param movable
     * @param onCreate
     * @returns {{faicon: *, closeable: *, title: *, onCreate: Function}}
     */
    Module.defaultPaneType = function (label, iconClass, closable, collapsible, moveable, onCreate, isPrivate) {

        return {
            isPrivate: isPrivate !== null ? isPrivate : false,
            faicon: iconClass,
            closeable: closable,
            title: label,
            moveable: moveable,
            onCreate: function (panel, options) {

                var docker = panel.docker();

                utils.mixin(panel._options, options);

                panel.on(wcDocker.EVENT.ATTACHED, function () {
                    docker._emit(wcDocker.EVENT.ATTACHED, panel);
                });

                panel.on(wcDocker.EVENT.DETACHED, function () {
                    docker._emit(wcDocker.EVENT.DETACHED, panel);
                });

                panel.on(wcDocker.EVENT.CLOSED, function () {
                    docker._emit(wcDocker.EVENT.CLOSED, panel);
                });

                if (closable !== null) {
                    panel.closeable(closable);
                }

                if (collapsible !== null && panel.collapsible) {
                    panel.collapsible(collapsible);
                }

                if (moveable !== null) {
                    panel.moveable(moveable);
                }

                if (options) {
                    panel.title(options.title);
                    if (options.closeable) {
                        panel.closeable(options.closeable);
                    }
                }

                var parent = $('<div style="height: 100%;width: 100%;overflow: hidden;" class="panelParent"/>');
                panel.layout().addItem(parent).stretch('100%', '100%');
                panel.containerNode = parent[0];
                utils.mixin(panel, options.mixin);

                function resize(panel, parent, why) {
                    var extraH = 0;

                    var resizeToChildren = panel.resizeToChildren;
                    panel.onResize && panel.onResize();
                    if (resizeToChildren) {
                        var totalHeight = utils.getHeight(panel._findWidgets()),
                            panelParent = panel._parent.$container;

                        extraH = panelParent.outerHeight() - panelParent.height();
                        if (extraH) {
                            totalHeight += extraH;
                        }
                        parent.css('width', panel._actualSize.x + 'px');
                        panel.set('height', totalHeight);

                    } else {
                        parent.css('height', panel._actualSize.y + 'px');
                        parent.css('width', panel._actualSize.x + 'px');
                        //transfer size and resize on widgets
                        _.each(panel.containerNode.children, function (child) {
                            if (child.id) {

                                var _widget = registry.byId(child.id);
                                var doResize = true;
                                if (_widget && _widget.resizeToParent === false) {
                                    doResize = false;
                                }

                                if (doResize) {
                                    //update
                                    $(child).css('height', panel._actualSize.y - extraH + 'px');
                                    $(child).css('width', panel._actualSize.x + 'px');
                                }
                                if (_widget) {
                                    _widget.resize && _widget.resize(null, null, why);
                                } else {
                                    console.warn('cant get widget : ' + child.id);
                                }
                            }
                        });
                    }
                }


                function select(selected, reason) {
                    //console.log(reason + ' : select ' +panel.title() +' selected:'+ selected + ' visible:' + panel.isVisible() + ' p#selected:' + panel.selected + ' __isVisible'+panel.isVisible());

                    if (panel.selected && selected) {
                        return true;
                    }
                    panel.selected = selected;
                    selected && (docker._lastSelected = panel);
                    resize(panel, parent, selected ? 'selected' : 'deselected');

                    var apiMethod = selected ? 'onShow' : 'onHide';
                    panel[apiMethod] && panel[apiMethod]();




                    //tell widgets
                    _.each(panel._findWidgets(), function (widget) {
                        if (!widget) {
                            return;
                        }
                        if (selected) {
                            if (!widget._started) {
                                widget.startup && widget.startup();
                            }
                        }
                        //call std API
                        if (widget[apiMethod]) {
                            widget[apiMethod]();
                        }
                        //forward
                        if (widget._emit) {
                            widget._emit(selected ? types.EVENTS.ON_VIEW_SHOW : types.EVENTS.ON_VIEW_HIDE, widget);
                        }

                        //forward
                        if (widget.setFocused) {
                            widget.setFocused(selected);
                        }
                    });
                }

                panel.on(types.DOCKER.EVENT.VISIBILITY_CHANGED, function (visible) {
                    panel.silent !== true && select(visible, 'vis changed');
                });
                panel._on(types.DOCKER.EVENT.SELECT, function (what) {
                    panel.silent !== true && select(true, 'select');
                    docker._emit(types.DOCKER.EVENT.SELECT, panel);
                });

                panel._on(types.DOCKER.EVENT.MOVE_STARTED, function () {
                    docker.trigger(types.DOCKER.EVENT.MOVE_STARTED, panel);
                });

                panel._on(types.DOCKER.EVENT.MOVE_ENDED, function () {
                    docker.trigger(types.DOCKER.EVENT.MOVE_ENDED, panel);
                });

                panel.on(types.DOCKER.EVENT.SAVE_LAYOUT, function (customData) {


                    panel.onSaveLayout({
                        data: customData,
                        panel: panel
                    });

                    if (!customData.widgets) {
                        customData.widgets = [];
                    }

                    _.each(panel._findWidgets(), function (w) {
                        if (!w) {
                            console.warn('SAVE_LAYOUT : invalid widget');
                            return;
                        }
                        w._emit && w._emit(types.EVENTS.SAVE_LAYOUT, {
                            panel: panel,
                            data: customData
                        });
                        w.onSaveLayout && w.onSaveLayout({
                            data: customData,
                            owner: panel
                        })
                    });
                });
                panel.on(types.DOCKER.EVENT.RESTORE_LAYOUT, function (customData) {

                    var eventData = {
                        data: customData,
                        panel: panel
                    };
                    panel.onRestoreLayout(eventData);
                    //console.log('restore layout');
                    //docker.trigger('restorePanel',eventData);
                });

                panel.on(types.DOCKER.EVENT.RESIZE_STARTED, function () {
                    panel.onResizeBegin(arguments);
                    docker.trigger(types.DOCKER.EVENT.BEGIN_RESIZE, panel);
                });
                panel.on(types.DOCKER.EVENT.RESIZE_ENDED, function () {
                    panel.onResizeEnd(arguments);
                    resize(panel, parent);
                    docker.trigger(types.DOCKER.EVENT.END_RESIZE, panel);
                });
                panel.on(types.DOCKER.EVENT.RESIZED, function () {
                    resize(panel, parent);
                });
                utils.mixin(panel, options.mixin);

                if (onCreate) {
                    onCreate(panel);
                }
            }
        }
    };
    /**
     * Register default panel types on docker
     * @static
     * @param docker
     */
    Module.registerDefaultTypes = function (docker) {
        docker.registerPanelType('DefaultFixed', Module.defaultPaneType('', '', false, false, true, null, true));
        docker.registerPanelType('DefaultTab', Module.defaultPaneType('', '', true, false, true, null, true));
        docker.registerPanelType('Collapsible', Module.defaultPaneType('', '', false, true, true, null, true));
    };

    /**
     * Creates a default docker
     * @static
     * @param container
     * @param options
     */
    Module.createDefault = function (container, options) {
        require({
            cacheBust: null
        });
        options = options || {};

        //get or create new docker variant
        var _class = Module;
        options.extension && (_class = dcl([Module, options.extension], {}));
        var docker = new _class($(container), utils.mixin({
            allowCollapse: true,
            responseRate: 100,
            allowContextMenu: false,
            themePath: require.toUrl('xdocker') + '/Themes/',
            theme: 'transparent2',
            wcPanelClass: Panel2,
            wcFrameClass: Frame2,
            wcSplitterClass: Splitter2
        }, options || {}));

        Module.registerDefaultTypes(docker);

        function hideFrames(hide) {
            var frame = $('#xIFrames');
            frame.css('display', hide ? 'none' : 'block');
            frame.children().css('display', hide ? 'none' : 'block');
        }

        docker.on(wcDocker.EVENT.BEGIN_DOCK, _.partial(hideFrames, true));
        docker.on(wcDocker.EVENT.END_DOCK, _.partial(hideFrames, false));
        docker.on(wcDocker.EVENT.RESIZE_STARTED, _.partial(hideFrames, true));
        docker.on(wcDocker.EVENT.RESIZED, _.partial(hideFrames, false));
        if (!docker.id) {
            docker.id = registry.getUniqueId(docker._declaredClass.replace(/\./g, "_"));
            docker.$container.attr('id', docker.id);
        }
        registry.add(docker);
        return docker;
    };

    Module.DOCK = wcDocker.DOCK;
    Module.EVENT = wcDocker.EVENT;
    Module.LAYOUT = wcDocker.LAYOUT;
    Module.ORIENTATION = wcDocker.ORIENTATION;

    return Module;
});