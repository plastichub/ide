/** @module xaction/Toolbar **/
define([
    'dcl/dcl',
    "xdojo/declare",
    'xide/utils',
    'xide/types',
    'xide/widgets/ActionToolbar',
    'xide/widgets/_Widget'
], (dcl, declare, utils, types, ActionToolbar, _Widget) => {
    /**
     * An action oriented Toolbar
     * @class module:xaction/Toolbar
     */
    const Implementation = {
        _toolbar: null,
        toolbarInitiallyHidden: false,
        toolbarArgs:null,
        runAction: function (action) {
            if (action.command === types.ACTION.TOOLBAR) {
                this.showToolbar(this._toolbar == null, null, null, this);
            }
            return this.inherited(arguments);
        },
        getToolbar: function () {
            return this._toolbar;
        },
        showToolbar: function (show, toolbarClass, where, emitter) {
            !show && (show = this._toolbar == null);
            if (!this._toolbar) {
                
                const toolbar = this.add(toolbarClass || ActionToolbar, utils.mixin({
                    style: 'height:auto;width:100%'
                },this.toolbarArgs), where || this.header, true);

                //limit toolbar with to our header for now
                utils.resizeTo(toolbar,this.header,false,true);

                toolbar.addActionEmitter(emitter || this);
                toolbar.setActionEmitter(emitter || this);
                //now stretch header to toolbar
                utils.resizeTo(this.header,toolbar,true,false);

                this._toolbar = toolbar;
            }
            if (!show && this._toolbar) {
                utils.destroy(this._toolbar, true, this);
            }
            this.resize();
        },
        startup: function () {
            const TOOLBAR = types.ACTION.TOOLBAR;
            const hasToolbar = _.includes(this.permissions, TOOLBAR);
            hasToolbar && this.showToolbar(hasToolbar, null, null, this);

            const self = this;
            const node = self.domNode.parentNode;
            this._on('onAddActions', evt => {
                //be careful, it could be here already
                !evt.store.getSync(TOOLBAR) && evt.actions.push(self.createAction('Toolbar', TOOLBAR, types.ACTION_ICON.TOOLBAR, ['ctrl b'], 'View', 'Show', 'item|view', null, null, null, null, null, evt.permissions, node, self));
            });
        }
    };

    //package via declare
    const _class = declare('xaction/Toolbar', _Widget, Implementation);
    _class.Implementation = Implementation;
    _class.dcl = dcl([_Widget.dcl], Implementation);
    return _class;
});