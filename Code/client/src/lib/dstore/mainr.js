define([
    "dstore/Memory",
    "dstore/Promised",
    "dstore/QueryMethod",
    "dstore/QueryResults",
    "dstore/SimpleQuery",
    "dstore/Store",
    "dstore/Tree",
    "dstore/Filter",
    "dstore/Trackable"
    //"dstore/legacy/DstoreAdapter"
], function () {

});