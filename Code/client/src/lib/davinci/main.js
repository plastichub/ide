define([
	"dojo/_base/kernel",
    'dojo/_base/declare',
    'davinci/davinci',
    'davinci/ve/metadata',
    'davinci/Runtime',
    'davinci/Workbench',
    "davinci/ve/metadata",
    "davinci/ui/widgets/OutlineTree"
], function(dojo){
    return dojo.davinci;
});
