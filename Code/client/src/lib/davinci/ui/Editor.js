define([
    "../commands/CommandStack",
    "dojo/_base/declare"
], function (CommandStack, declare, Action) {

    var onSelectionChanged = function (selectionEvent) {
        if (this._progSelect) {
            return;
        }

//		var startPos=this._textModel.getPosition(selectionEvent.newValue.start);
//		var endPos=this._textModel.getPosition(selectionEvent.newValue.end);

        // User-initiated change in the source editor.  Synchronize with the model.
        this.selectionChange({
            startOffset: selectionEvent.newValue.start,
            endOffset: selectionEvent.newValue.end
        });
    };

    return declare(null, {

        constructor: function (element, fileName, existWhenVisible) {
            this.contentDiv = element;
            this.commandStack = new CommandStack(); //TODO: integrate with orion.editor.UndoFactory
            this._existWhenVisible = existWhenVisible;
            this._isVisible = !existWhenVisible;
        },

        setContent: function (filename, content) {
            if (!this.editor && (!this._existWhenVisible || this._isVisible)) {
                this._createEditor();
            }
            if (!this._textModel) {
                this._textModel = this.editor ? this.editor.getModel() : new mProjectionTextModel.ProjectionTextModel(new mTextModel.TextModel());
            }
            this.fileName = filename;

            this.setValue(content, true);


            // delay binding to the onChange event until after initializing the content
            if (this._textModel) {
                dojo.disconnect(this._textModelConnection);
                this._textModelConnection = dojo.connect(this._textModel, "onChanged", this, onTextChanged); // editor.onInputChange?
            }
        },

        setVisible: function (visible) {
            if (visible != this._isVisible && this._existWhenVisible) {
                if (visible && this._existWhenVisible) {
                    this._createEditor();
                    this._updateStyler();
                } else {
                    this.editor.getTextView().removeEventListener("Selection", dojo.hitch(this, onSelectionChanged));
//	            try {
//					this.editor.destroy();
//	            } catch (e){ console.error(e); }
                    delete this.editor;
                }
            }
            this._isVisible = visible;
        },

        setValue: function (content, dontNotify) {
            this._dontNotifyChange = dontNotify;
            if (this.editor) {
                this.editor.setText(content);
            } else {
                this._textModel.setText(content);
            }
        },

        _createEditor: function () {

        },

        _updateStyler: function () {

        },

        selectionChange: function (selection) {
        },

        destroy: function () {

        },

        select: function (selectionInfo) {
//		var start=this._textModel.getLineStart(selectionInfo.startLine)+selectionInfo.startCol;
//		var end=this._textModel.getLineStart(selectionInfo.endLine)+selectionInfo.endCol;
            if (this.editor) {
                try {
                    this._progSelect = true;
                    // reverse arguments so that insertion caret (and the scroll) happens at the beginning of the selection
                    this.editor.setSelection(selectionInfo.endOffset, selectionInfo.startOffset/*, true*/);
                } finally {
                    delete this._progSelect;
                }
            }
        },

        getText: function () {
            return this._textModel.getText(0);
        },

        /* Gets called before browser page is unloaded to give
         * editor a chance to warn the user they may lose data if
         * they continue. Should return a message to display to the user
         * if a warning is needed or null if there is no need to warn the
         * user of anything. In browsers such as FF 4, the message shown
         * will be the browser default rather than the returned value.
         *
         * NOTE: With auto-save, _not_ typically needed by most editors.
         */
        getOnUnloadWarningMessage: function () {
            return null;
        }
    });
});
