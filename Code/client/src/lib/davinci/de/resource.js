define([
	"davinci/Workbench",
	"davinci/workbench/Preferences",
	"system/resource",
	"davinci/de/DijitTemplatedGenerator",
	"davinci/library",
	"davinci/ui/Dialog",
	"davinci/ve/actions/ReplaceAction",
	"dojo/promise/all",
	"xide/views/_CIPanelDialog",
	"xide/utils",
	"xide/types",
	"dojo/when",
	"dojo/Deferred",
	"davinci/ve/metadata",
	"dojo/text!davinci/de/widgets/templates/Widget._js"
], function (Workbench, Preferences, Resource, DijitTemplatedGenerator, dLibrary, Dialog, ReplaceAction, all, _CIPanelDialog, utils, types, when, Deferred, metadata, widgetJSTemplate) {

	// For developer notes on how custom widgets work in Maqetta, see:
	// https://github.com/maqetta/maqetta/wiki/Custom-widgets	

	var prefix = './lib/custom/';
	const mkPath = (widgetName, fileName) => prefix + widgetName + '/' + fileName;

	var dt = {
		ctx: window['sctx'],
		/* base packages.json metadata */
		WIDGETS_JSON: {
			version: "1.0",
			localPath: true,
			customWidgetSpec: 1,
			"categories": {
				"custom": {
					name: "Custom widget",
					description: "Custom widget",
					widgetClass: "dijit"
				}
			},
			widgets: []
		},
		createDijiFromNewDialog: function () {
			var oldEditor = Workbench.getOpenEditor();
			var oldFileName = oldEditor.fileName;
			var oldResource = oldEditor.item;
			var model = oldEditor.model;

			var openEditor = Workbench.getOpenEditor();
			var context = openEditor.getContext();
			var selection = context.getSelection();
			if (!dt.validWidget(selection)) {
				Dialog.showModal("Invalid Selection.  Please select a single container widget to create a new Widget", "Error creating Widget...");
				return;
			}
			var cis = [
				utils.createCI('Name', types.ECIType.STRING, 'CustomWidget', {
					group: 'Expression'
				})
			];
			var actionDialog = new _CIPanelDialog({
				title: 'Save as custom widget',
				resizeable: true,
				onOk: function (changedCIS) {
					var name = cis[0].value;
					dt.createDijit({
						name: name
					}, model, oldResource, context, selection).then(() => {
						metadata.query(name + '/' + name);
					})
				},
				cis: cis
			});
			actionDialog.show();
/*
			return;


			Workbench.showModal(projectDialog, "Dijit Widget...", {
				height: 60,
				width: 250
			}, function () {
				if (!projectDialog.cancel) {
					var widgetData = projectDialog.attr('value');
					dt.createDijit(widgetData, model, oldResource, context, selection).then(function () {
						if (widgetData.replaceSelection) {
							new ReplaceAction().run(context, widgetData.group + "." + widgetData.name);
						}

						//FIXME: Force a browser refresh. This is the atom bomb approach.
						//Reason for doing this is that the custom palette list in widget palette
						//and all of the require/packages logic happens during application initialization.
						//It might be possible to prevent the reload without too much work, but for now we
						//do a browser refresh.
						window.location.reload(false);
					});
				}
				return true;
			});
			*/
		},

		/* 
		 * returns true if the selection is valid for creating a new widget. only 
		 * support creating widgets from selected container widgets 
		 * 
		 * */
		validWidget: function (selection) {
			/*
			 * 
			 * Need to check for dojo containers somehow..?
			 * 
			 */
			if (selection == null || selection.length != 1) return false;
			var widget = selection[0];
			return (widget.acceptsHTMLChildren);
		},

		_createNameSpace: function (name, parent) {
			var namesplit = name.split(".");
			var all = [];
			if (namesplit.length > 1) {
				for (var i = 0; i < namesplit.length - 1; i++) {
					var folder = parent.getChild(namesplit[i]);
					if (folder != null) {
						parent = folder;
					} else {
						const mkdir = this.ctx.getFileManager().mkdir(parent.mount, prefix + namesplit[i], {
							checkErrors: true,
							returnProm: false
						})
						all.push(mkdir);
						// parent = parent.createResource(namesplit[i], true);
					}
				}
			}
			if (all.length) {
				return Promise.all(all);
			}
			return parent || Promise.all(all);
		},

		_createFolder: function (name, parent, readyCB) {
			var namesplit = name.split("/");
			var base = Workbench.getProject();
			parent = parent || Resource.findResource(base);

			if (namesplit.length) {
				for (var i = 0; i < namesplit.length; i++) {

					if (namesplit[i] == ".") continue;
					console.error('get child sync : ' + namesplit[i]);
					var folder = parent._getChild(namesplit[i]);
					if (folder != null) {
						parent = folder;
					} else {
						/*parent = */
						parent.createResource(namesplit[i], true, false, readyCB);
						return;
					}
				}
				if (parent != null && readyCB) {
					readyCB(parent);
				}

			}
			return parent;
		},

		createDijit: function (widgetData, model, resource, context, selection) {

			var head = new Deferred();

			var qualifiedWidgetDot = widgetData.name + "." + widgetData.name;
			var qualifiedWidgetSlash = widgetData.name + "/" + widgetData.name;
			var qualifiedWidgetDash = widgetData.name + "/" + widgetData.name;
			var self = this;

			var store = this.ctx.getFileManager().getStore('workspace_user');
			var mainFn = function (parent) {

				var widgetNamespace = dt._createNameSpace(qualifiedWidgetDot, parent);
				when(widgetNamespace, () => {
					store._loadPath(prefix + widgetData.name, true).then(() => {
						widgetNamespace = store.getSync(prefix + widgetData.name);
						var customWidgets = widgetNamespace.getChild(widgetData.name + "_widgets.json");
						var customWidgetsPath = prefix + widgetData.name + '/' + widgetData.name + "_widgets.json";
						if (customWidgets == null) {
							customWidgets = self.ctx.getFileManager().mkfile(parent.mount, customWidgetsPath, '{}');
						}
						when(customWidgetsPath, () => {
							when(store._loadItem({
								path: customWidgetsPath
							}, true), (item) => {
								customWidgets = item;
								var customWidgetsJson = dojo.clone(dt.WIDGETS_JSON);
								customWidgetsJson.name = widgetData.name;
								customWidgetsJson.longName = widgetData.name;

								var widgetsObj = {
									name: widgetData.name,
									description: widgetData.name,
									type: qualifiedWidgetSlash,
									category: "custom",
									iconLocal: true
								};
								var topElementModel = selection[0]._srcElement;
								for (var i = 0, atts = topElementModel.attributes, len = atts.length; i < len; i++) {
									var att = atts[i];
									if (att.name === 'id') {
										continue;
									}
									if (!att.noPersist) {
										if (!widgetsObj.properties) {
											widgetsObj.properties = {};
										}
										widgetsObj.properties[att.name] = att.value;
									}
								}
								customWidgetsJson.widgets.push(widgetsObj);

								var promises = [self.ctx.getFileManager().setContent(customWidgets.mount, customWidgets.path, JSON.stringify(customWidgetsJson, undefined, '\t'))];

								var content = new DijitTemplatedGenerator({}).buildSource(model, qualifiedWidgetSlash, widgetData.name, false, context, selection);
								for (var type in content) {
									switch (type) {
										case 'amd':
											break;
										case 'html':
											promises.push(self.ctx.getFileManager().mkfile(parent.mount, mkPath(widgetData.name, widgetData.name + ".html"), content.html));
											break;
										case 'js':
											const js = utils.replace(widgetJSTemplate, null, {
												widgetName: widgetData.name
											}, {
												begin: '{{',
												end: '}}'
											});
											promises.push(self.ctx.getFileManager().mkfile(parent.mount, mkPath(widgetData.name, widgetData.name + ".js"), js));
											break;
										case 'metadata':
											promises.push(self.ctx.getFileManager().mkfile(parent.mount, mkPath(widgetData.name, widgetData.name + "_oam.json"), content.metadata));
											try {
												dLibrary.addCustomWidgets(customWidgets, widgetNamespace.getPath(), customWidgetsJson);
											} catch (e) {
												console.error('error adding custom widgets!', e);
											}
											break;
									}
								}

								all(promises).then(() => {
									head.resolve();
								});

							});
						});

					});
				});
			}

			const custom = when(store.getItem('./lib/custom', true), (folder) => {
				mainFn(folder);
			});
			return head;
		}
	};

	return dt;
});