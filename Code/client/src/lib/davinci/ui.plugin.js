define([
    "require"
], function (require) {

    return {
        id: "davinci.ui",
        "davinci.view": [{
                id: "navigator",
                title: "Files",
                viewClass: "davinci/workbench/Explorer",
                iconClass: "paletteIcon paletteIconFiles"
            },
            {
                id: "hierarchy",
                title: "Hierarchy"
            },
            {
                id: "devices",
                title: "Devices",
                viewClass: "davinci/workbench/DeviceTreeView",
                iconClass: "paletteIcon paletteIconOutline"
            },
            {
                id: "files",
                title: "XFiles",
                viewClass: "davinci/workbench/FileView",
                iconClass: "paletteIcon paletteIconOutline"
            },
            {
                id: "comment",
                title: "Comments",
                viewClass: "davinci/review/view/CommentView",
                iconClass: "paletteIcon paletteIconComments"
            },
            {
                id: "scope",
                title: "Scope"
            },
            {
                id: "properties",
                title: "Properties",
                viewClass: "davinci/workbench/PropertyEditor"
            },
            {
                id: "console",
                title: "Console"
            },
            {
                id: "history",
                title: "History"
            },
            {
                id: "search",
                title: "Search"
            }
        ],
        "davinci.preferences": [{
                name: "Project",
                id: "project",
                hide: true, //FIXME: temporarily don't show project setting preferences. See #3657, #3658
                category: "",
                pageContent: "Project Settings here"
            },
            {
                name: "Project Settings",
                id: "ProjectPrefs",
                category: "davinci.ui.project",
                pane: "davinci/ui/ProjectPreferences",
                defaultValues: {
                    "webContentFolder": "",
                    "themeFolder": "themes",
                    "widgetFolder": "lib/custom"
                }
            }
        ],
        "davinci.perspective": {
            id: "main",
            title: "AJAX IDE",
            views: [{
                    viewID: "davinci.ui.navigator",
                    position: "left-bottom"
                },
                {
                    viewID: "davinci.ui.outline",
                    position: "right"
                },
                {
                    viewID: "davinci.ui.properties",
                    position: "right-bottom"
                }
            ]
        },
        "davinci.actionSets": [{
                id: "editorActions",
                visible: true,
                menu: [],
                actions: []
            },
            {
                id: "main",
                visible: true,
                menu: [{
                        __mainMenu: true,
                        separator: [
                            "usersettings", false, "settings", false, "additions", false, "help",
                            false
                        ]
                    },
                    {
                        label: "User settings",
                        path: "usersettings",
                        id: "davinci.usersettings",
                        className: 'userSettingsMenu',
                        iconClass: 'userSettingsMenuIcon',
                        showLabel: false,
                        separator: [
                            "username", true, "logout", true, "additions", false
                        ]
                    },
                    {
                        label: "Settings",
                        path: "settings",
                        id: "davinci.settings",
                        className: 'appSettingsMenu',
                        iconClass: 'appSettingsMenuIcon',
                        showLabel: false,
                        separator: [
                            "settings", true, "additions", false
                        ]
                    }
                ],
                actions: [{
                        id: "editPreferences",
                        run: function () {
                            require(['davinci/workbench/Preferences'], function (Preferences) {
                                Preferences.showPreferencePage();
                            });
                        },
                        label: "Preferences...",
                        menubarPath: "davinci.settings/settings"
                    }

                ]
            },
            {
                id: "explorerActions",
                visible: true,
                actions: [{
                        id: "davinci.ui.rename",
                        label: "Rename...",
                        iconClass: "renameIcon",
                        run: function () {
                            require(['./ui/Resource'], function (r) {
                                r.renameAction();
                            });
                        },
                        isEnabled: function (item) {
                            return require('./ui/Resource').canModify(item);
                        },
                        menubarPath: "addFiles"
                    },
                    {
                        id: "davinci.ui.delete",
                        label: "Delete",
                        iconClass: "deleteIcon",
                        isEnabled: function (item) {
                            return require('./ui/Resource').canModify(item);
                        },
                        run: function () {
                            require(['./ui/Resource'], function (r) {
                                r.deleteAction();
                            });
                        },
                        menubarPath: "delete",
                        keyBinding: {
                            charOrCode: [dojo.keys.DELETE, dojo.keys.BACKSPACE]
                        }
                    },
                    {
                        id: "davinci.ui.download",
                        label: "Download",
                        iconClass: "downloadSomeIcon",
                        action: "davinci/actions/DownloadAction",
                        isEnabled: function (item) {
                            return require('./ui/Resource').canModify(item);
                        },
                        menubarPath: "delete"
                    }


                ]
            }
        ],
        "davinci.actionSetPartAssociations": [{
                targetID: "davinci.ui.editorActions",
                parts: [
                    "davinci.ui.editorMenuBar"
                ]
            },
            {
                targetID: "davinci.ui.explorerActions",
                parts: [
                    "davinci.ui.navigator"
                ]
            }
        ],
        "davinci.viewActions": [{
                viewContribution: {
                    targetID: "davinci.ui.problems",
                    actions: [{
                        id: "Copy2",
                        iconClass: 'copyIcon',
                        run: function () {
                            alert("view toolbar");
                        },
                        label: "Copy",
                        toolbarPath: "davinci.toolbar.main/edit",
                        menubarPath: "davinci.edit/cut"
                    }]
                }
            },

            /* deployment icon in the file explorer toolbar */
            {
                viewContribution: {
                    targetID: "workbench.Explorer",
                    actions: [{
                            id: "davinci.ui.newfile",
                            label: "New folder...",
                            iconClass: "newFolderIcon",
                            className: "FilesToolbarNewFolder",
                            run: function () {
                                require(['./ui/Resource'], function (r) {
                                    r.newFolder();
                                });
                            },
                            toolbarPath: "download"
                        },
                        {
                            id: "davinci.ui.deletefile",
                            label: "Delete file...",
                            iconClass: "FilesToolbarDeleteFileIcon",
                            className: "FilesToolbarDeleteFile",
                            run: function () {
                                require(['./ui/Resource'], function (r) {
                                    r.deleteAction();
                                });
                            },
                            toolbarPath: "download"
                        },
                        {
                            id: "davinci.ui.renamefile",
                            label: "Rename file...",
                            iconClass: "FilesToolbarRenameFileIcon",
                            className: "FilesToolbarRenameFile",
                            run: function () {
                                require(['./ui/Resource'], function (r) {
                                    r.renameAction();
                                });
                            },
                            toolbarPath: "download"
                        },
                        {
                            id: "download",
                            iconClass: 'downloadSomeIcon',
                            className: "FilesToolbarDownloadSelected",
                            run: function () {
                                require(['./Workbench', './ui/DownloadSelected'],
                                    function (workbench, DownloadSelected) {
                                        workbench.showModal(new DownloadSelected(), "Download", {
                                            width: 440
                                        });
                                    }
                                );
                            },
                            label: "Download Selected Files",
                            toolbarPath: "download"
                        },
                        {
                            id: "download",
                            iconClass: 'downloadAllIcon',
                            className: "FilesToolbarDownloadAll",
                            run: function () {
                                require(['./Workbench', './ui/Download'],
                                    function (workbench, Download) {
                                        workbench.showModal(new Download(), "Download", {
                                            width: 440
                                        });
                                    }
                                );
                            },
                            label: "Download Entire Project",
                            toolbarPath: "download"
                        }
                    ]
                }
            }
        ]
    };

});