define([
	"dojo/_base/declare"
], function (declare){
	
	const debug = false;
	// Disable dijit's automatic key handlers until there is time to do it right
	return declare("davinci.ve.palette.Palette", [], {
		
	});
});