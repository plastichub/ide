define([
	"davinci/Runtime",
	"./FontDataStore",
	"./FontComboBox",
	"./DropDown",
	"dijit/form/ComboBox",
	"./MetaDataStore",
	"./ColorPicker",
	"./Background2"
], function (Runtime, FontDataStore) {

	const HTMLStringUtil = dojo.getObject("davinci.ve.widgets.HTMLStringUtil", true);
	var idPrefix = "davinci_ve_widgets_properties_border_generated";
	var __id = 0,
		getId = function () {
			return idPrefix + (__id++);
		};

	dojo.mixin(HTMLStringUtil, {
		__id: 0,
		idPrefix: "davinci_ve_widgets_properties_generated",
		_currentPropSection: null,

		animSS: null, // Cache of style sheet that contains animation effects
		animRuleIndex: {}, // Cache of style rules that contains animation effects, indexed by selector

		getCurrentPropSection: function () {
			return this._currentPropSection;
		},

		getId: function () {
			return this.idPrefix + (this.__id++);
		},

		injectId: function (htmlText, id) {
			/* attempts to inject an ID in the top HTML element */

			const firstEndTag = htmlText.indexOf(">");

			if (!firstEndTag) {
				return "<span id='" + id + "'>" + htmlText + "</span>";
			}

			return htmlText.substring(0, firstEndTag) + " id='" + id + "'" + htmlText.substring(firstEndTag, htmlText.length);
		},


		getEditor: function (jsonString) {


			function getValueAndTitle(value) {
				const obj = {};
				obj.value = (typeof (value.value) != "undefined" && value.value !== null) ? value.value : value;
				// if it is an object use the value and look for a title
				obj.title = value.title || obj.value;
				return obj;
			}

			const metaType = jsonString.type;
			const id = this.getId();
			const extraAttribs = "";

			jsonString.id = id;

			const disabled = jsonString.disabled ? " disabled='true' " : "";

			/*
			 * 
			 * when writing dijit markup BE SURE TO INCLUDE class='propertyPaneEditableValue' to signify a onChange target and property target
			 * as well as the 'extraAttributes' string, which will contain the inputs target as parsed from JSON template.
			 */

			switch (metaType) {
				case "multi":
					var valuesText = "";
					if (jsonString.values) {
						valuesText = "data='" +
							dojo.toJson(dojo.map(jsonString.values, function (v) {
								return {
									value: v
								};
							})) +
							"'";
					}
					var text = "<div dojoType='davinci.ve.widgets.MultiInputDropDown' " + valuesText + "  class='propertyPaneEditablevalue' style='display:inline-block; width:100%;' id='" + id + "'" + disabled + "></div>";

					return text;
				case "boolean":
					var text = "<input type='checkbox' class='propertyPaneEditablevalue' style='display:inline-block;margin-left:5px' id='" + id + "'></input>";
					return text;

				case "comboEdit":
					var values = jsonString.values;
					var text = "<select dojoType='dijit.form.ComboBox' autoComplete='false' style='display:inline-block; width:100%;' id='" + id + "'" + disabled + ">";
					//var text = "<select dojoType='xide.form.Select' autoComplete='false' style='display:inline-block; width:100%;' id='"+ id + "'"+disabled+">";
					for (var i = 0; i < values.length; i++) {
						var obj = getValueAndTitle(values[i]);
						text += "<option class='option' value='" + obj.value + "'>" + obj.title + "</option>";
					}
					text += "</select>";
					return text;

				case "combo":
					var values = jsonString.values;
					var text = "<select class='form-control input-transparent' style='display:inline-block; width:100%;' id='" + id + "'" + disabled + ">";
					for (var i = 0; i < values.length; i++) {
						var obj = getValueAndTitle(values[i]);
						text += "<option class='option' value='" + obj.value + "'>" + obj.title + "</option>";
					}
					text += "</select>";
					return text;
				case "font":
					var text = "<div dojoType='davinci.ve.widgets.FontDataStore' jsId='" + id + ('_fontStore') + "'>";
					text += "<div dojoType='davinci.ve.widgets.FontComboBox' value='" + FontDataStore.fonts[0].value + "' store='" + id + ('_fontStore') + "'  id='" + id + "' class='propertyPaneEditablevalue' style='display:inline-block; width:100%;'></div>";
					return text;
				case "stateold":
					var text = "<div dojoType='davinci.ve.widgets.MetaDataStore' jsId='davinci.properties.event" + (id) + ('_Store') + "'>";
					text += "<div dojoType='dijit.form.ComboBox' id='" + id + "'store='davinci.properties.event" + (id) + ('_Store') + "' class='propertyPaneEditablevalue' style='display:inline-block; width:100%;' autoComplete='false'></div>";
					return text;
				case "color":
					var text = "<div class='propertyPaneEditablevalue' dojoType='davinci.ve.widgets.ColorPicker' id='" + id + "' ></div>";
					return text;
					/*todo - write color chooser widget */
				case "background":
					var valuesText = dojo.isArray(jsonString.values) ? " data='" + dojo.toJson(jsonString.values) + "'" : "";
					const propName = dojo.isArray(jsonString.target) ? jsonString.target[0] : jsonString.target;
					const propNameText = " propname='" + propName + "'";
					const swatchText = jsonString.colorswatch ? " colorswatch='true'" : '';
					var text = "<div dojoType='davinci.ve.widgets.Background' id='" + id + "'" + valuesText + propNameText + swatchText + "></div>";
					return text;

				case "border":
					/* todo - write border widget */
				case "number":
				case "object":
				case "text":
				case "array":
				case "widgetState":
				case "iconClass":
				case "state":
				case "string":
					if (jsonString.format == "date") {
						return "<div class='propertyPaneEditablevalue' dojoType='dijit.form.DateTextBox' id='" + id + "'></input>";
					} else if (jsonString.format == "time") {
						return "<div class='propertyPaneEditablevalue' dojoType='dijit.form.TimeTextBox' id='" + id + "'></input>";
					}
					// else fallthrough...
				default:
					{
						var text;
						if (jsonString.data && jsonString.data.readonly === true) {
							text = "<input readonly type='text' class='form-control input-transparent propertyPaneEditablevalue' style='display:inline-block; width:100%;' id='" + id + "'></input>";
						} else {
							text = "<input type='text' class='form-control input-transparent propertyPaneEditablevalue' style='display:inline-block; width:100%;' id='" + id + "'></input>";
						}
						return text;
					}
			}
		},

		generateTable: function (page, params) {
			const rowsOnly = params ? params.rowsOnly : false;
			const zeroSpaceForIncrDecr = params ? params.zeroSpaceForIncrDecr : false;
			const incrDecrSize = zeroSpaceForIncrDecr ? '0px' : '20px';
			let htmlText = "";
			if (page.html) {
				page.id = this.getId();
				return this.injectId(page.html, page.id);
			}

			let tableHtml = "<table class='property_table_stretchable' border='0' width='100%' align='center' cellspacing='0' cellpadding='0'>";
			tableHtml += "<colgroup>";
			tableHtml += "<col style='width:6px;' />";
			tableHtml += "<col class='gap02' />";
			tableHtml += "<col class='gap03' />";
			tableHtml += "<col style='width:" + incrDecrSize + ";' />";
			tableHtml += "<col style='width:6px;' />";
			tableHtml += "</colgroup>";
			//	tableHtml +="<tr class='property_table_rowgap property_table_rowgap_group_separator'><td colspan='7'/></tr>";
			if (!rowsOnly)
				htmlText += tableHtml;

			for (let i = 0; i < page.length; i++) {


				//console.log('render widget fields',page[i]);

				if (page[i].widgetHtml) {

					page[i].id = this.getId();
					page[i].rowId = this.getId();
					htmlText += "<tr id='" + page[i].rowId + "'";

					if (page[i].rowClass) {
						htmlText += " class='" + page[i].rowClass + "'";
					}
					htmlText += ">";
					htmlText += "<td colspan='5' width='100%'>";
					htmlText += this.injectId(page[i].widgetHtml, page[i].id);
					htmlText += "</td>";
					htmlText += "</tr>";
				} else if (page[i].html) {

					page[i].id = this.getId();
					page[i].rowId = this.getId();
					htmlText += "<tr id='" + page[i].rowId + "'";
					htmlText += " class='cssPropertySection";

					if (page[i].rowClass) {
						htmlText += " " + page[i].rowClass;
					}
					htmlText += "'>";
					htmlText += "<td colspan='5' width='100%'>";
					htmlText += page[i].html;
					htmlText += "</td>";
					htmlText += "</tr>";
				} else if (page[i].type == "toggleSection") {

					htmlText += "<tr id='" + page[i].id + "'  class='cssPropertySection'><td colspan='5'>";

					let onclick = "";
					const moreTable = this.generateTable(page[i].pageTemplate, {
						rowsOnly: true
					});
					for (let j = 0; j < page[i].pageTemplate.length; j++) {
						if (page[i].pageTemplate[j].rowId) {
							onclick += "dojo.toggleClass('" + page[i].pageTemplate[j].rowId + "','propertiesSectionHidden');";
						}
						if (page[i].pageTemplate[j].cascadeSectionRowId) {
							onclick += "if(this.checked){dojo.removeClass('" + page[i].pageTemplate[j].cascadeSectionRowId + "','propertiesSectionHidden');}else{{dojo.addClass('" + page[i].pageTemplate[j].cascadeSectionRowId + "','propertiesSectionHidden');}}";
						}
					}

					htmlText += "<input type='checkbox' onclick=\"" + onclick + "\"></input>";
					htmlText += page[i].display;
					htmlText += "</td></tr>";
					htmlText += moreTable;
				} else if (page[i].display) {
					page[i].toggleCascade = this.getId();
					//	page[i].showHelp = this.getId();
					page[i].cascadeSection = this.getId();
					page[i].rowId = this.getId();
					page[i].cascadeSectionRowId = this.getId();

					htmlText += "<tr id='" + page[i].rowId + "'";
					htmlText += " class='cssPropertySection";
					if (page[i].rowClass) {
						htmlText += " " + page[i].rowClass;
					}
					htmlText += "'";
					htmlText += " propName='" + page[i].display + "'";
					htmlText += ">";
					htmlText += "<td/>";
					htmlText += "<td class='propertyDisplayName'>" + page[i].display + ":&nbsp;</td>";

					htmlText += "<td class='propertyInputField propertyInputField" + page[i].type + "' >" + this.getEditor(page[i]) + "</td>";
					htmlText += "<td class='propertyExtra' nowrap='true'>";
					htmlText += "<td class='propertyExtra2' nowrap='true'>";
					if (page[i].target && !page[i].hideCascade) {

						htmlText += "<div width='100%'><button class='showCss propertyButton' id='" + page[i].toggleCascade + "'";
						htmlText += " onClick=\"davinci.ve.widgets.HTMLStringUtil.showProperty(";
						htmlText += "'" + page[i].rowId + "'";
						htmlText += ")\">&gt;</button>";
						htmlText += "</div>";
					}
					htmlText += "<td/>";
					htmlText += "</tr>";
					if (page[i].target && !page[i].hideCascade) {
						const toggleClasses = "{'cascadeSectionRowId':\"" + page[i].cascadeSectionRowId + "\",'toggleCascade':\"" + page[i].toggleCascade + '\"}';
						htmlText += "<tr id='" + page[i].cascadeSectionRowId + "' class='cssCascadeSection cascadeRowHidden'>";
						htmlText += "<td colspan='5' width='100%' class='showCascadeDiv'><div dojoType='davinci.ve.widgets.Cascade' toggleClasses=" + toggleClasses + " target='" + dojo.toJson(page[i].target) + "' targetField='\"" + page[i].id + "\"' id='" + page[i].cascadeSection + "'></div></td></tr>";
					}
				} else {
					htmlText += "</table>";
					htmlText += this.getEditor(page[i]);
					htmlText += tableHtml;
				}
			}
			if (!rowsOnly)
				htmlText += "</table>";
			return htmlText;

		},
		generateTemplate: function (jsonString) {
			let htmlText = "";

			if (jsonString.pageTemplate) {
				if (jsonString.key) {
					jsonString.id = this.getId();
					htmlText = "<div class='propGroup' id='" + jsonString.id + "' propGroup='" + jsonString.key + "'>";
				}

				htmlText += this.generateTable(jsonString.pageTemplate);
				htmlText += "</div>";
			} else if (jsonString.html) {
				htmlText += jsonString.html;
			} else if (jsonString.widgetHtml) {

				jsonString.id = this.getId();
				htmlText += this.injectId(jsonString.widgetHtml, jsonString.id);
			}
			return htmlText;
		},
		stylesheetHref: "propview.css",

		animShowSectionClass: "propRootDetailsContainer",
		animShowSectionClassSelector: ".propRootDetailsContainer",
		animShowDetailsClass: "property_table_stretchable",
		animShowDetailsClassSelector: ".property_table_stretchable",
		showPropAnimClasses: ["propRowFadeIn", "propRowFadeOut", "propRowTransparent", "propRowOpaque", "propRowHidden"],

		showRoot: function () {
			const Util = this;
			Util._hideSectionShowRoot();
			Util._currentPropSection = null;
			return false;
		},

		showSection: function (sectionKey, sectionTitle) {
			const Util = this;
			Util._initSection(sectionKey);
			return false;
		},

		showProperty: function (propertyRowId) {

			const hideAllButThisRow = function () {
				for (let i = 0; i < rowParent.children.length; i++) {
					const node = rowParent.children[i];
					if (node.nodeType == 1 && dojo.hasClass(node, "cssPropertySection")) { // 1=Element. IE7 bug - children[] includes comments
						if (node == thisPropertyRowTR) {
							Util._addRemoveClasses(node, allTRAnimClasses, []);
						} else {
							Util._addRemoveClasses(node, allTRAnimClasses, ["propRowHidden"]);
						}
					}
				}
			};

			const fadeInCascade = function () {
				if (thisCascadeRowTR) {
					dojo.removeClass(thisCascadeRowTR, "cascadeRowHidden");
					dojo.addClass(thisCascadeRowTR, "cascadeRowTransparent"); // To set transition starting point at opacity:0
					setTimeout(function () {
						dojo.removeClass(thisCascadeRowTR, "cascadeRowTransparent");
						dojo.addClass(thisCascadeRowTR, "cascadeRowFadeIn");
					}, 1);
				}
			};

			function transEnd(event) {
				dojo.disconnect(webkitConnection);
				dojo.disconnect(connection);
				hideAllButThisRow();
				const returnObj = Util._findRule(Util.animShowDetailsClassSelector);
				if (!returnObj) {
					console.error('HTMLStringUtil showProperty: transEnd: rule not found');
					return;
				} else {
					var ss = returnObj.ss;
					var ruleIndex = returnObj.ruleIndex;
				}
				ss.deleteRule(ruleIndex);
				ss.insertRule(Util.animShowDetailsClassSelector + " { margin-top:0px; }", ruleIndex);
				fadeInCascade();
			}

			//xmaqhack
			dojo.addClass(dojo.byId('root'), "showingCascade");

			var Util = this;
			var allTRAnimClasses = this.showPropAnimClasses;

			// Find various elements
			//   thisPropertyRowTR: TR corresponding to eventElem (element that received user click)
			//   rowParent: parent element of thisPropertyRowTR (either TBODY or TABLE)
			//   firstPropertyRowTR: 1st child of rowParent that is a TR with class "cssPropertySection". Holds prop's input entry widgets.
			//   thisCascadeRowTR: TR element just after firstPropertyRowTR, with class "cssCascadeSection". Holds prop's cascade info.
			//   propertySectionTABLE: TABLE element that is ancestor of thisPropertyRowTR.
			var thisPropertyRowTR = dojo.byId(propertyRowId);
			var rowParent = thisPropertyRowTR.parentNode;
			const firstPropertyRowTR = Util._searchSiblingsByTagClass(rowParent.children[0], "TR", "cssPropertySection");
			var thisCascadeRowTR = Util._searchSiblingsByTagClass(thisPropertyRowTR.nextSibling, "TR", "cssCascadeSection");
			const propertySectionTABLE = Util._searchUpByTagClass(rowParent, "TABLE", Util.animShowDetailsClass);
			const propertyGroupDIV = Util._searchUpByTagClass(thisPropertyRowTR, "DIV", "propGroup");
			const sectionKey = dojo.attr(propertyGroupDIV, "propGroup");
			const propName = dojo.attr(thisPropertyRowTR, "propName");
			const SwitchingStyleView = require("davinci/ve/views/SwitchingStyleView");
			const sectionTitle = SwitchingStyleView.prototype.sectionTitleFromKey(sectionKey);

			if (Runtime.supportsCSS3Transitions) {

				// Compute top coordinate diff between firstPropertyRowTR and thisPropertyRowTR
				const firstMetrics = dojo.marginBox(firstPropertyRowTR);
				const thisMetrics = dojo.marginBox(thisPropertyRowTR);
				const topDiff = thisMetrics.t - firstMetrics.t;
				const returnObj = Util._findRule(Util.animShowDetailsClassSelector);
				if (!returnObj) {
					console.error('HTMLStringUtil showProperty: transEnd: rule not found');
					return;
				} else {
					var ss = returnObj.ss;
					var ruleIndex = returnObj.ruleIndex;
				}

				var webkitConnection = dojo.connect(propertySectionTABLE, 'webkitTransitionEnd', transEnd);
				var connection = dojo.connect(propertySectionTABLE, 'transitionend', transEnd);
				ss.deleteRule(ruleIndex);
				// opacity:.99 to force animation to occur even if topDiff is zero.
				ss.insertRule(Util.animShowDetailsClassSelector + " { margin-top:-" + topDiff + "px; opacity:.99; -webkit-transition: all .6s ease; -moz-transition: all .6s ease; }", ruleIndex);

				// assign classes to cause fade-in/fade-out effects
				let foundThisPropertyRowTR = false;
				for (let i = 0; i < rowParent.children.length; i++) {
					const node = rowParent.children[i];
					if (node.nodeType == 1 && dojo.hasClass(node, "cssPropertySection")) { // 1=Element. IE7 bug - children[] includes comments
						if (node == thisPropertyRowTR) {
							Util._addRemoveClasses(node, allTRAnimClasses, ["propRowFadeIn"]);
							foundThisPropertyRowTR = true;
						} else if (!foundThisPropertyRowTR) {
							Util._addRemoveClasses(node, allTRAnimClasses, ["propRowTransparent"]);
						} else {
							Util._addRemoveClasses(node, allTRAnimClasses, ["propRowHidden"]);
						}
					}
				}

				// Else if browser does not support transitions (e.g., FF3.x)
			} else {
				hideAllButThisRow();
				fadeInCascade();
			}
		},

		_initSection: function (sectionKey) {
			const Util = this;
			const allTRAnimClasses = Util.showPropAnimClasses;
			const rootTD = Util._getRootTD();
			const propGroups = dojo.query(".propGroup", rootTD);

			let propGroupDIV;
			for (var i = 0; i < propGroups.length; i++) {
				const propGroup = propGroups[i];
				const name = dojo.attr(propGroup, "propGroup");
				if (name == sectionKey) {
					dojo.removeClass(propGroup, "dijitHidden");
					propGroupDIV = propGroup;
				} else {
					dojo.addClass(propGroup, "dijitHidden");
				}
			}
			const pSects = dojo.query(".cssPropertySection", propGroupDIV);
			for (var i = 0; i < pSects.length; i++) {
				Util._addRemoveClasses(pSects[i], allTRAnimClasses, []);
			}
			const cSects = dojo.query(".cssCascadeSection", propGroupDIV);
			for (var i = 0; i < cSects.length; i++) {
				dojo.addClass(cSects[i], "cascadeRowHidden");
			}
			Util._currentPropSection = sectionKey;
			dojo.removeClass(dojo.byId('root'), "showingCascade");
		},

		_hideSectionShowRoot: function () {
			const Util = this;
			const rootTD = Util._getRootTD();
			dojo.removeClass(rootTD, "dijitHidden");
		},

		// Search animSS (property palette's animation stylesheet) to find the style rule
		// with given the selectorText. Returns index for the rule.
		_findRule: function (selectorText) {
			if (this.animSS && typeof this.animRuleIndex[selectorText] == 'number') {
				const idx = this.animRuleIndex[selectorText];
				if (this.animSS.cssRules[idx].selectorText == selectorText) {
					return {
						ss: this.animSS,
						ruleIndex: idx
					};
				}
			}

			function searchRules(ss, selectorText) {
				for (let ruleIndex = 0; ruleIndex < ss.cssRules.length; ruleIndex++) {
					const rule = ss.cssRules[ruleIndex];
					if (rule.type === 3) { // 3=@import
						const importSS = rule.styleSheet;
						const retObj = searchRules(importSS, selectorText);
						if (retObj) {
							return retObj;
						}
					} else if (rule.selectorText == selectorText) {
						return {
							ss: ss,
							ruleIndex: ruleIndex
						};
					}
				}
			}
			for (let i = 0; i < document.styleSheets.length; i++) {
				const ss = document.styleSheets[i];
				const returnObj = searchRules(ss, selectorText);
				if (returnObj) {
					// Cache the results so hopefully we only have to search stylesheets once per session
					this.animSS = returnObj.ss;
					this.animRuleIndex[selectorText] = returnObj.ruleIndex;
					return returnObj;
				}
			}
			return null;
		},

		// Adds all classes in the classesToAdd array and removes any
		// classes from allClasses that aren't in classesToAdd
		_addRemoveClasses: function (elem, allClasses, classesToAdd) {
			const classesToRemove = [];
			for (var i = 0; i < allClasses.length; i++) {
				let found = false;
				for (let j = 0; j < classesToAdd.length; j++) {
					if (allClasses[i] == classesToAdd[j]) {
						found = true;
						break;
					}
				}
				if (!found) {
					classesToRemove.push(allClasses[i]);
				}
			}
			for (var i = 0; i < classesToRemove.length; i++) {
				dojo.removeClass(elem, classesToRemove[i]);
			}
			for (var i = 0; i < classesToAdd.length; i++) {
				dojo.addClass(elem, classesToAdd[i]);
			}
		},

		// Look at refElem and ancestors for a particular tag and optionally a particular class
		_searchUpByTagClass: function (refElem, tagName, className) {
			while (refElem != null && refElem.nodeName != "BODY") {
				if (refElem.nodeName == tagName && (!className || dojo.hasClass(refElem, className))) {
					return refElem;
				}
				refElem = refElem.parentNode;
			}
			return null;
		},

		// Look at refElem and nextSiblings for a particular tag and optionally a particular class
		_searchSiblingsByTagClass: function (refElem, tagName, className) {
			while (refElem != null) {
				if (refElem.nodeName == tagName && (!className || dojo.hasClass(refElem, className))) {
					return refElem;
				}
				refElem = refElem.nextSibling;
			}
			return null;
		},

		_getRootDetailsContainer: function () {
			const Util = this;
			if (!Util._rootDetailsContainer) {
				Util._rootDetailsContainer = dojo.query(Util.animShowSectionClassSelector)[0];
			}
			return Util._rootDetailsContainer;
		},

		_getRootTD: function () {
			const Util = this;
			const rootDetailsContainer = Util._getRootDetailsContainer();
			if (!Util._rootTD) {
				//FIXME: Make .propPaletteRoot into a var
				Util._rootTD = dojo.query(".propPaletteRoot", rootDetailsContainer)[0];
			}
			return Util._rootTD;
		}
	});
	return HTMLStringUtil;
});