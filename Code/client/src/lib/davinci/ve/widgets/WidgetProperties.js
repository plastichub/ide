define([
	"dojo/_base/declare",
	"dojo/_base/connect",
	"davinci/workbench/ViewLite",
	"davinci/ve/metadata",
	"davinci/commands/CompoundCommand",
	"davinci/ve/commands/ModifyCommand",
	"./HTMLStringUtil",
	"dijit/form/DateTextBox",
	"dijit/form/TimeTextBox",
    'xide/types',
    'xide/factory'
], function(
	declare,
	connect,
	ViewLite,
	Metadata,
	CompoundCommand,
	ModifyCommand,
	HTMLStringUtil,
	DateTextBox,
	TimeTextBox,
    types,factory
) {
	return declare("davinci.ve.widgets.WidgetProperties", [ViewLite], {
		key: "widgetSpecific", // Must match section key in SwitchingStylingViews table

		_connects: null,
	
		buildRendering: function(){
			this.domNode = this.propDom = dojo.doc.createElement("div");
			dojo.addClass(this.domNode, "propGroup");
			dojo.attr(this.domNode, "propGroup", this.key);
			this._connects = [];
			this.inherited(arguments);
		},

		onWidgetSelectionChange: function() {
			if (!this._widget || !this._editor || this._editor.editorID != "davinci.ve.HTMLPageEditor") {
				this._disconnectAll();
				this._destroyProperties();
				return;
			}
			
			// NOTE: In logic below, we use metadata.$ownproperty instead of metadata.property
			// $ownproperty contains only those properties that were explicitly defined in oam.json file
			// property also contains hidden properties 'class', 'style', 'title', etc.
			// Need to use $ownproperty in order to add HTML 'title' attribute at top of list
			// for any widgets that don't explicitly define their own title property
			
			const metadata = davinci.ve.metadata.query(this._widget);
			/* check to see if this widget is a child of a widget */
			const parentWidget = this._widget.getParent();
			if (parentWidget && parentWidget.isWidget) {
				const parentMetadata = Metadata.query(parentWidget);
				/* check the parent widget for extra props to add if it is a child of that widget */
				if (parentMetadata && parentMetadata.childProperties){
					if (!metadata.$ownproperty) {
						metadata.$ownproperty = parentMetadata.childProperties;
					} else {
						for (var prop in parentMetadata.childProperties) {
							metadata.$ownproperty[prop] = parentMetadata.childProperties[prop];
						}
					}
				}
			}
			if(!metadata || !metadata.$ownproperty) {
				return;
			}
			this._disconnectAll();
			this._destroyProperties();
	
			// put the HTML 'title' attribute at the top of the list of widget-specific properties
			// if the widget doesn't already have a title property
			const props = {};
			if(!metadata.$ownproperty.title){
				props.title = {datatype:'string'};
			}
			for(var prop in metadata.$ownproperty){
				props[prop] = metadata.$ownproperty[prop];
			}
			
			const rows = this.propDom.innerHTML = this._createWidgetRows(props);
			if (rows.indexOf('data-dojo-type') !== -1 || rows.indexOf('dojoType') !== -1) {
				dojo.parser.parse(this.propDom);
			}
			this._setValues();
			this._connectAll();
		},
		
		onEditorSelected: function(editorChange) {
			this._editor = editorChange;
			if (editorChange && editorChange.editorID == "davinci.ve.HTMLPageEditor") {
				// not all editors have a context
				//FIXME: test for context instead?
				this.context = editorChange.getContext();
                if(this.context) {
                    this._widget = this.context.getSelection()[0];
                }
			} else {
				this.context = null;
				this._widget = null;
			}
			this.onWidgetSelectionChange();
		 },	
	
		_createWidgetRows: function (properties){
			this._pageLayout = [];
			for(const name in properties){
				const property = properties[name];
				if(property.hidden){
					continue;
				}

				const prop = {
					display:(property.title || name),
						type: property.datatype,
						format: property.format,
						target:name,
						hideCascade:true,
                        data:property
				};

				if (property.dropdownQueryValues && property.dropdownQueryAttribute) {
					const values = [];

					dojo.forEach(property.dropdownQueryValues, dojo.hitch(this, function(query) {
						const results = dojo.query(query, this.context.rootNode);
						dojo.forEach(results, function(node) {
								values.push(node.getAttribute(property.dropdownQueryAttribute));
						});
					}));

					// store the values into the prop
					prop.values = values;

					// we want a comboEdit here, so force it
					prop.type = "comboEdit";
				}

				if (dojo.isArray(property.mustHaveAncestor)) {
					let found = false;
					let w = this._widget;
					while (!found && w && w.getParent() != this.context.rootWidget) {
						w = w.getParent();
						if (w && dojo.indexOf(property.mustHaveAncestor, w.type) > -1) {
							found = true;
						}
					}

					if (found) {
					} else {
						prop.disabled = true;
					}
				}
				this._pageLayout.push(prop);
				if(property.option){
					this._pageLayout[this._pageLayout.length-1].values = dojo.map(property.option, function(option){ return option;/*.value;*/ });
					this._pageLayout[this._pageLayout.length-1].type = property.unconstrained ? "comboEdit" : "combo";
				}
			}

			return HTMLStringUtil.generateTable(this._pageLayout);
		},
		
		_destroyProperties: function(){
			const containerNode = (this.propDom);
			dojo.forEach(dojo.query("[widgetId]", containerNode).map(dijit.byNode), function(w){
				w.destroy();
			});
			while(containerNode.firstChild){
				dojo._destroyElement(containerNode.firstChild);
			}
		},
		
		_connectAll: function() {
			function makeOnChange(target){
				return function(){
					return this._onChange({target:target});
				};
			}
			for (let i = 0, len = this._pageLayout.length; i < len; i++) {
                //NOTE: This comment was present here and the "var widget..." and "if(!widget){" lines 
                //were commented out:
                //
                //               FIXME: Probably can remove commented out code, but leaving
                //               it in for now in case there is a case where widget==null
                //               The way things are coded now, widget is always null
                //
                //HOWEVER, I found a case where the assumption widget would always be null is _wrong_. The
                //casvnse is when the property is unconstrained and a comboEdit is in place. So, I've uncommented the
                //aforementioned lines.
                const row = this._pageLayout[i];

                const widget = dijit.byId(row.id);
                const obj = widget || dojo.byId(row.id);

                const // onchange, et al, are lowercase for DOM/non dijit
                onchange = widget ? 'onChange' : 'change';

                const onfocus = widget ? 'onFocus' : 'focus';
                const onblur = widget ? 'onBlur' : 'blur';

                if(!obj){
					console.error('cant find '+row.id + ' node',row);
					continue;
				}

                obj.pageIndex = i;
                this._connect(obj, onchange, this, makeOnChange(i));
                this._connect(obj, onfocus, this, "_onFieldFocus");
                this._connect(obj, onblur, this, "_onFieldBlur");
                obj.owner=this;
                obj.row=i;

                factory.publish(types.EVENTS.WIDGET_PROPERTY_RENDERED,{
                    view:this,
                    row:row,
                    widget:widget,
                    node:obj
                },this);
            }
		},

		_connect: function(target, method, scope, targetFunction, dontFix) {
			this._connects.push(connect.connect.apply(null, arguments));
		},

		_disconnectAll: function() {
			this._connects.forEach(connect.disconnect);
			this._connects = [];
		},

		_onChange: function(a) {
            const index = a.target;
            const row = this._pageLayout[index];
            const widget = dijit.byId(row.id);
            let value;

            if (this.context) {
				this.context.blockChange(false);
			}

            if (widget) {
				value = widget.get('value');
				if (value && (value instanceof Date)) { // Date
					if (widget instanceof DateTextBox ) {
							value = value.toISOString().substring(0, 10);
					} else if (widget instanceof TimeTextBox ) {
							value = "T" + value.toTimeString().substring(0, 8);
					}
				}
			} else {
				const box = dojo.byId(row.id);
				const attr = box.type === 'checkbox' ? 'checked' : 'value';
				value = dojo.attr(box, attr);
			}

            if (row.value != value) { // keep '!=', we want type coercion from strings
				row.value = value;
				const valuesObject = {};
				valuesObject[row.target] = value;
				const compoundCommand = new CompoundCommand();
				const command = new ModifyCommand(this._widget, valuesObject, null);
				compoundCommand.add(command);
				const helper = this._widget.getHelper();
				if(helper && helper.onWidgetPropertyChange){
					helper.onWidgetPropertyChange({widget:this._widget, compoundCommand:compoundCommand, modifyCommand:command});
				}
				dojo.publish("/davinci/ui/widgetPropertiesChanges", [
					{
						source: this._editor.editor_id,
						compoundCommand: compoundCommand,
						command: command
					}
				]);
			}
        },
		_onFieldFocus: function() {
			if (this.context) {
				this.context.blockChange(true);
			}
		},
		_onFieldBlur: function() {
			if (this.context) {
				this.context.blockChange(false);
			}
		},
		_setValues: function() {
			try{
                for (let i = 0, len = this._pageLayout.length; i < len; i++) {
                    const row = this._pageLayout[i];
                    const propNode = dojo.byId(row.id);
                    // Verify that DOM element actually exists
                    if (!propNode) {
                        continue;
                    }

                    const widget = this._widget;
                    const targetProp = row.target;
                    let propValue;

                    if (targetProp === "_children") {
                        propValue = widget.getChildrenData();
                        if (propValue && propValue.length === 1) {
                            propValue = propValue[0];
                        } else {
                            // need to account for this case?
                            propValue = widget.getPropertyValue(targetProp);
                        }
                    } else {
                        propValue = widget.getPropertyValue(targetProp);
                    }
                    if (propValue && (propValue.toISOString)) { // Date
                        const format = widget.metadata.property[targetProp].format;
                        if (format) {
                            if (format == "date") {
                                propValue = propValue.toISOString().substring(0, 10);
                            } else if (format == "time" ) {
                                propValue = "T" + propValue.toTimeString().substring(0, 8);
                            }
                        }
                    }
                    if (row.value != propValue) { // keep '!=', we want type coercion from strings
                        row.value = propValue;
                        const attr = row.type === 'boolean' ? 'checked' : 'value';

                        // check if we have a dijit
                        const dijitwidget = dijit.byId(row.id);
                        if (dijitwidget) {
                            dijitwidget.attr(attr, row.value);
                        } else {
                            dojo.attr(propNode, attr, row.value);
                        }
                    }
                }
            }catch(e){
                debugger;
            }
		}
	});
});