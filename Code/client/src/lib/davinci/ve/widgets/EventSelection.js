define(["dojo/_base/declare",
	"dojo/_base/connect",
	"dojo/dom-class",
	'davinci/Runtime',
	"../../workbench/ViewLite",
	"../commands/EventCommand",
	"./HTMLStringUtil",
	"../States",
	"xide/types",
	'xide/factory'
], function (declare, connect, domClass, Runtime, ViewLite, EventCommand, HTMLStringUtil, States, types, factory) {


	const StateColonString = 'State:';
	const StatePatternDisplay = new RegExp('^' + StateColonString + '.*');
	const SetStateString = 'davinci.states.setState';
	const StatePatternSource = /^\s*davinci\.states\.setState\s*\(\s*([\'"])((?:(?!\1).)*)\1\s*\)\s*$/;

	const FileColonString = 'File:';
	const FilePatternDisplay = new RegExp('^' + FileColonString + '.*');
	const LocationHrefString = 'location.href';
	const FilePatternSource = /^\s*location\.href\s*\=\s*([\'"])((?:(?!\1).)*)\1\s*$/;

	const getEventSelectionValues = function (root) {
        //xmaqhack
        const items = [""];

        const states = [];
        const stateContainers = root && States.getAllStateContainers(root);

        /*
	if(stateContainers){
		states = stateContainers.reduce(function(statesList, container){
			return statesList.concat(States.getStates(container));
		}, []);
	}
	for(var i=0; i<states.length; i++){
		var state = states[i];
		var stateDisplayName = state == 'Normal' ? 'Background' : state;
		var val = StateColonString + stateDisplayName;
		if(items.indexOf(val) < 0){
			items.push(val);
		}
	}

	var base = Workbench.getProject();
	var prefs = Preferences.getPreferences('davinci.ui.ProjectPrefs',base);
	if(prefs.webContentFolder!=null && prefs.webContentFolder!=""){
		var fullPath = new Path(base).append(prefs.webContentFolder);
		base = fullPath.toString();
	}
	var folder = Resource.findResource(base);
	var samplesPath = new Path(base).append('samples');
	var samplesFolder = Resource.findResource(samplesPath.toString());
	var themePath = new Path(base).append(prefs.themeFolder);
	var themeFolder = Resource.findResource(themePath.toString());
	var customWidgetPath = new Path(base).append(prefs.widgetFolder);
	var customWidgetFolder = Resource.findResource(customWidgetPath.toString());
	var htmlFiles = [];
	function recurseFindHtmlFiles(folder){
		folder.getChildren(function(children){	// onComplete
			for(var i=0; i<children.length; i++){
				var child = children[i];
				if(child.elementType == 'Folder'){
					if(!child._readOnly && child != samplesFolder && child != themeFolder && child != customWidgetFolder){
						recurseFindHtmlFiles(child);
					}
				} else if(/^html?$/i.test(child.extension)) {
					htmlFiles.push(child);
				}
			}
		}.bind(htmlFiles), function(a, b){	// onError
			console.error('EventSelection.js: folder.getChildren error');
		});
	}
	recurseFindHtmlFiles(folder);
	var basePathString = folder.getPath().toString()+'/';
	for(var i=0; i<htmlFiles.length; i++){
		var htmlFile = htmlFiles[i];
		var htmlFilePath = htmlFile.getPath().toString();
		if(htmlFilePath.indexOf(basePathString) == 0){
			htmlFilePath = htmlFilePath.substr(basePathString.length);
		}
		var val = FileColonString + htmlFilePath;
		if(items.indexOf(val) < 0){
			items.push(val);
		}
	}
    */

        //items.push('onclick');
        return items;
    };

	const getEventScriptFromValue = function (value) {
		if (value && value.match(StatePatternDisplay)) {
			const state = value.substring(StateColonString.length);
			const stateRuntimeValue = state == 'Background' ? 'Normal' : state;
			value = SetStateString + "('" + stateRuntimeValue + "')";
		}
		if (value && value.match(FilePatternDisplay)) {
			value = LocationHrefString + "=\'" + value.substring(FileColonString.length) + "\'";
		}

		return value;
	};

	const getValueFromEventScript = function (value) {
		let match;
		if (value) {
			match = value.match(StatePatternSource);
			if (match) {
				const state = match[2];
				value = StateColonString + state;
			}
			match = value.match(FilePatternSource);
			if (match) {
				const filename = match[2];
				value = FileColonString + filename;
			}
		}
		return value;
	};

	const EventSelection = declare("davinci.ve.widgets.EventSelection", [ViewLite], {

		pageTemplate: [{
				display: "onclick",
				target: "onclick",
				type: "state",
				hideCascade: true
			},
			{
				display: "ondblclick",
				target: "ondblclick",
				type: "state",
				hideCascade: true
			},
			{
				display: "onmousedown",
				target: "onmousedown",
				type: "state",
				hideCascade: true
			},
			{
				display: "onmouseup",
				target: "onmouseup",
				type: "state",
				hideCascade: true
			},
			{
				display: "onmouseover",
				target: "onmouseover",
				type: "state",
				hideCascade: true
			},
			{
				display: "onmousemove",
				target: "onmousemove",
				type: "state",
				hideCascade: true
			},
			{
				display: "onmouseout",
				target: "onmouseout",
				type: "state",
				hideCascade: true
			},
			{
				display: "onkeypress",
				target: "onkeypress",
				type: "state",
				hideCascade: true
			},
			{
				display: "onkeydown",
				target: "onkeydown",
				type: "state",
				hideCascade: true
			},
			{
				display: "onkeyup",
				target: "onkeyup",
				type: "state",
				hideCascade: true
			},
			{
				display: "onfocus",
				target: "onfocus",
				type: "state",
				hideCascade: true
			},
			{
				display: "onblur",
				target: "onblur",
				type: "state",
				hideCascade: true
			},
			{
				display: "onchange",
				target: "onchange",
				type: "state",
				hideCascade: true
			}
		],

		buildRendering: function () {
			this.domNode = dojo.doc.createElement("div");
			this.domNode.innerHTML = HTMLStringUtil.generateTable(this.pageTemplate, {
				zeroSpaceForIncrDecr: true
			});
			domClass.add(this.domNode, 'EventSelection');
			this.inherited(arguments);
		},
		setReadOnly: function (isReadOnly) {
			for (let i = 0; i < this.pageTemplate.length; i++) {
				const widget = this.pageTemplate[i].widget;
				if (widget)
					widget.set("readOnly", isReadOnly);
				else {
					const node = this.pageTemplate[i].domNode;
					if (node)
						dojo.attr(node, "disabled", isReadOnly);
				}
			}
		},
		startup: function () {
			this.inherited(arguments);

			for (var i = 0; i < this.pageTemplate.length; i++) {
                const row = this.pageTemplate[i];
                const widget = dijit.byId(row.id);
                const obj = widget || dojo.byId(row.id);

                factory.publish(types.EVENTS.WIDGET_PROPERTY_RENDERED, {
					view: this,
					row: row,
					widget: widget,
					node: obj
				}, this);

                // var box = dijit.byId(this.pageTemplate[i].id);
            }

			return;

			function makeOnChange(target) {
				return function () {
					return this._onChange({
						target: target
					});
				};
			}

			for (var i = 0; i < this.pageTemplate.length; i++) {
				const box = dijit.byId(this.pageTemplate[i].id);
				this.pageTemplate[i].widget = box;
				connect.connect(box, "onChange", this, makeOnChange(i));
			}
			this._buildSelectionValues();

			const thiz = this;

			this.subscribe("/davinci/ui/context/loaded", dojo.hitch(this, this._buildSelectionValues));
			this.subscribe("/davinci/states/stored", dojo.hitch(this, this._buildSelectionValues));
			this.subscribe("/davinci/states/state/added", dojo.hitch(this, this._buildSelectionValues));
			this.subscribe("/davinci/states/state/removed", dojo.hitch(this, this._updateValues));
			this.subscribe("/davinci/states/state/renamed", dojo.hitch(this, this._updateValues));
			this.subscribe("/davinci/ui/widgetPropertiesChanged", dojo.hitch(this, this._widgetPropertiesChanged));

			this.publish(types.EVENTS.ON_EVENT_SELECTION_RENDERED, {
				eventSelection: this,
				pageTemplate: this.pageTemplate
			});
			this.setReadOnly(true);
		},

		onEditorSelected: function () {
			if (!this._editor || !this._editor.supports("states")) {
				delete this._editor;
			}
			this._buildSelectionValues();

		},

		_onChange: function (a) {
			const index = a.target;
			const widget = dijit.byId(this.pageTemplate[index].id);
			let value = widget.get('value');

			value = getEventScriptFromValue(value);
			const properties = {};

			properties[this.pageTemplate[index].target] = value;

			const command = new EventCommand(this._widget, properties);

			dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
				source: this._editor.editor_id,
				command: command
			}]);
			this.publish(types.EVENTS.ON_WIDGET_PROPERTY_CHANGED, {
				widget: widget,
				editor: this._editor,
				eventSelection: this,
				pageTemplate: this.pageTemplate,
				widgetProperty: this.pageTemplate[index].target,
				value: value,
				target: this._widget
			});
		},
		_getRoot: function () {
            const currentEditor = this._editor;
            let root;
            if (currentEditor && currentEditor.getContext) {
				const context = currentEditor.getContext();
				root = context && context.rootNode;
			}
            return root;
        },

		_updateValues: function (e) {
			if (!e || !e.node || !e.node._dvWidget) {
				return;
			}
			const context = e.node._dvWidget.getContext();
			const editor = (context && context.editor);
			if (!editor || editor != Runtime.currentEditor) {
				return;
			}
			this._buildSelectionValues();
			if (this._widget) {
				this._setValues();
			}
		},

		onWidgetSelectionChange: function () {
			if (!this._widget) {
				this.setReadOnly(true);
				this._clearValues();

			} else {
				this._setValues();
				this.setReadOnly(false);
			}
		},
		_clearValues: function () {
			return;
			for (let i = 0; i < this.pageTemplate.length; i++) {
				const box = dijit.byId(this.pageTemplate[i].id);
				box.set("value", "", false);
			}
		},

		_buildSelectionValues: function () {
			const root = this._getRoot();
			const items = getEventSelectionValues(root);
			return;

			for (let i = 0; i < this.pageTemplate.length; i++) {
				const box = dijit.byId(this.pageTemplate[i].id);
				box.store.clearValues();
				box.store.setValues(items);
			}
		},

		_setValues: function () {
			for (let i = 0; i < this.pageTemplate.length; i++) {
				const name = this.pageTemplate[i].target;
				const widget = this._widget;
				let value = "";

				if (widget.properties && widget.properties[name]) {
					value = widget.properties[name];
				} else /*if(widget._srcElement) */ {
					/* check the model for the events value */
					/*
					if(!widget._srcElement) {
					    var widgetUtils = require("davinci/ve/widget");
					    widget._srcElement = widgetUtils._createSrcElement(widget.domNode);
					    console.log('as');
					}
					*/
					if (widget._srcElement) {
						value = widget._srcElement.getAttribute(name);
					}
				}
				value = getValueFromEventScript(value);
				const box = dijit.byId(this.pageTemplate[i].id);
				if (box) {
					box.set('value', value, false);
				}
			}
		},

		_widgetPropertiesChanged: function (data) {
			// data is array of widgets
			this._updateValues({
				widget: data[0]
			});
		}
	});

	//Make helpers available as "static" functions
	EventSelection.getEventSelectionValues = getEventSelectionValues;
	EventSelection.getEventScriptFromValue = getEventScriptFromValue;
	EventSelection.getValueFromEventScript = getValueFromEventScript;

	return EventSelection;

});