define(["dojo/_base/declare",
    "dijit/_Templated",
    "dijit/_Widget",
    "davinci/Runtime",
    "davinci/workbench/ViewLite",
    "davinci/ve/commands/ModifyCommand",
    "dojo/store/Memory",
    "davinci/lang/ve",
    "davinci/ve/widget",
    "dojo/text!./templates/WidgetToolBar.html"
], function (declare, _Templated, _Widget, Runtime, ViewLite, ModifyCommand, Memory, veNLS, widgetUtils, templateString) {
    return declare("davinci.ve.widgets.WidgetToolBar", [ViewLite, _Widget, _Templated], {
        templateString: templateString,
        widgetsInTemplate: true,
        // attach points
        descNode: null,
        idTextBox: null,
        classComboBox: null,
        blockGroupComboBox: null,

        widgetDescStart: "",
        widgetDescUnselectEnd: "",
        _oldIDName: null,
        _oldClassName: null,
        _oldBlockGroup: null,
        veNLS: veNLS,
        cssRules: null,
        postCreate: function () {
            // id validation
            this.idTextBox.validator = dojo.hitch(this, function (value, constraints) {
                this.invalidMessage = null;

                if (!value || !this._widget || this.changing) {
                    return true;
                }

                const w = widgetUtils.byId(value);
                if (w && w !== this._widget) {
                    this.invalidMessage = veNLS.idAlreadyUsed;
                    return false
                }

                return true;
            })

            dojo.subscribe("/davinci/ui/widget/replaced", dojo.hitch(this, this._widgetReplaced));
            const thiz = this;
            this.subscribe('CSSRulesUpdated', function (evt) {
                thiz.cssRules = evt.names;
                thiz.onWidgetSelectionChange();
            });
        },

        initialize: function () {
            this._editor = Runtime.currentEditor;
            if (this._editor && this._editor.visualEditor && this._editor.visualEditor.context) {
                const selection = this._editor.visualEditor.context.getSelection();
                if (selection.length == 0) {
                    this._widget = null;
                } else {
                    this._widget = selection[0];
                }
            } else {
                this._widget = null;
            }

            this.onWidgetSelectionChange();
        },
        cssSelect: null,
        startup: function () {
            this.inherited(arguments);
            if (!this.cssSelect) {
                this.cssSelect = $(this.cssClasses).selectize({
                    delimiter: ',',
                    persist: true,
                    inputClass: 'selectize-input form-control input-transparent',
                    plugins: ['restore_on_backspace'],
                    //create: true,
                    createOnBlur: true,
                    dropdownParent: 'body',
                    create: function (input) {
                        return {
                            value: input,
                            text: input
                        }
                    }
                }).data().selectize;
                const thiz = this;
                this.cssSelect.on('change', function () {
                    const value = thiz.cssSelect.getValue();
                    if (!thiz._widget || thiz.changing) {
                        return;
                    }
                    if (thiz.context)
                        thiz.context.blockChange(false);

                    const className = value.split(',').join(' ');

                    if (className !== thiz._oldClassName) {
                        thiz._oldClassName = className;
                        const valuesObject = {};
                        valuesObject['class'] = className;
                        const command = new davinci.ve.commands.ModifyCommand(thiz._widget, valuesObject, null);
                        dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                            source: thiz._editor.editor_id,
                            command: command
                        }]);
                    }
                });
            }
        },
        onEditorSelected: function () {
            this.initialize();
            // only show during HTML editing
            if (this._editor && this._editor.editorID == "davinci.ve.HTMLPageEditor") {
                this.domNode.style.display = "block";
            } else {
                this.domNode.style.display = "none";
            }
        },

        _widgetReplaced: function (newWidget, oldWidget) {
            if (this._widget === oldWidget) {
                this._widget = newWidget;
                this.onWidgetSelectionChange();
            }
        },

        onWidgetSelectionChangeBak: function () {
            let displayName = "";

            if (this._widget) {
                displayName = widgetUtils.getLabel(this._widget);
                this.context = this._widget.getContext();
                this.classComboBox.set("disabled", false);
                //this.blockGroupComboBox.set("disabled", false);
                this.idTextBox.attr("disabled", false);
            } else {
                this.descNode.innerHTML = veNLS.noSelection;
                dojo.removeClass(this.domNode, "propertiesSelection");
                this.context = null;

                this.classComboBox.set("value", '');
                this.classComboBox.set("disabled", true);
                //this.blockGroupComboBox.set("value", '');
                //this.blockGroupComboBox.set("disabled", true);
                this.idTextBox.attr("value", '');
                this.idTextBox.attr("disabled", true);
                return;
            }
            this.changing = true;
            // clear out
            this._oldIDName = null;
            this._oldClassName = null;
            this._oldBlockGroup = null;
            dojo.addClass(this.domNode, "propertiesSelection");
            this.descNode.innerHTML = displayName;
            if (this._editor && this._editor.declaredClass === "davinci.ve.PageEditor") {
                // Provide a type-in box for the 'class' and ID attribute
                const srcElement = this._widget._srcElement;
                if (srcElement) {
                    // collect all classes
                    const classes = [];
                    const docEl = this._widget.getContext().getModel().getDocumentElement();
                    if (docEl) {
                        const visitor = {
                            visit: function (node) {
                                // skip the body/html nodes
                                if (node.elementType == "HTMLElement" && node.tag != "body" && node.tag != "html") {
                                    const c = node.getAttribute("class");
                                    if (c) {
                                        const classes = dojo.trim(c).split(" ");
                                        dojo.forEach(classes, dojo.hitch(this, function (className) {
                                            // remove dupes
                                            if (dojo.indexOf(this.classes, className) == -1) {
                                                this.classes.push(className);
                                            }
                                        }));
                                    }
                                }
                            },
                            classes: []
                        };
                        docEl.visit(visitor);
                        dojo.forEach(visitor.classes, function (className) {
                            classes.push({name: className});
                        });
                    }

                    if (this.cssRules) {
                        _.each(this.cssRules, function (rule) {
                            if (!_.find(classes, {name: rule})) {
                                classes.push({
                                    name: rule
                                })
                            }
                        });
                    }
                    // use a simply memory store
                    const memstore = new Memory({
                        data: classes
                    });
                    this.classComboBox.set("store", memstore);
                    const classAttr = srcElement.getAttribute("class");
                    const className = (classAttr && dojo.trim(classAttr)) || "";
                    if (this.cssSelect) {
                        _.each(classes, function (_class) {
                            this.cssSelect.addOption({
                                value: _class.name,
                                text: _class.name
                            });
                        }, this);
                        this.cssSelect.setValue(className.split(' '), true);
                    }

                    this.classComboBox.set("value", className);
                    this._oldClassName = className;
                    // id
                    const idAttr = this._widget.getId();
                    const idName = (idAttr && dojo.trim(idAttr)) || "";
                    this._oldIDName = idName;
                    this.idTextBox.attr("value", idName);

                    if (this._widget.domNode && this._widget.domNode) {
                        let group = this._widget.domNode.blockGroup;
                        if (!group) {
                            group = this._widget.domNode.getAttribute('blockGroup');
                        }
                        this._oldBlockGroup = group;
                        //this.blockGroupComboBox.set("value", group);
                    }
                }
            }
            this.changing = false;

        },
        onWidgetSelectionChange: function () {
            let displayName = "";
            if(this._widget && this._widget.isFake){
                    return;
            }
            if (this._widget && this._widget.getContext) {
                displayName = widgetUtils.getLabel(this._widget);
                this.context = this._widget.getContext();
                this.idTextBox.attr("disabled", false);
                this.cssSelect && this.cssSelect.enable();

            } else {
                this.descNode.innerHTML = veNLS.noSelection;
                dojo.removeClass(this.domNode, "propertiesSelection");
                this.context = null;
                this.cssSelect && this.cssSelect.disable();
                this.cssSelect && this.cssSelect.setValue([]);
                this.idTextBox.attr("value", '');
                this.idTextBox.attr("disabled", true);
                return;
            }

            this.changing = true;

            // clear out
            this._oldIDName = null;
            this._oldClassName = null;
            this._oldBlockGroup = null;
            dojo.addClass(this.domNode, "propertiesSelection");
            this.descNode.innerHTML = displayName;
            if (this._editor && this._editor.declaredClass === "davinci.ve.PageEditor") {
                // Provide a type-in box for the 'class' and ID attribute
                const srcElement = this._widget._srcElement;
                if (srcElement) {
                    // collect all classes
                    const classes = [];

                    const docEl = this._widget.getContext().getModel().getDocumentElement();
                    if (docEl) {
                        const visitor = {
                            visit: function (node) {
                                // skip the body/html nodes
                                if (node.elementType == "HTMLElement" && node.tag != "body" && node.tag != "html") {
                                    const c = node.getAttribute("class");
                                    if (c) {
                                        const classes = dojo.trim(c).split(" ");
                                        dojo.forEach(classes, dojo.hitch(this, function (className) {
                                            // remove dupes
                                            if (dojo.indexOf(this.classes, className) == -1) {
                                                this.classes.push(className);
                                            }
                                        }));
                                    }
                                }
                            },
                            classes: []
                        };
                        docEl.visit(visitor);
                        dojo.forEach(visitor.classes, function (className) {
                            classes.push({name: className});
                        });
                    }
                    if (this.cssRules) {
                        _.each(this.cssRules, function (rule) {
                            if (!_.find(classes, {name: rule})) {
                                classes.push({
                                    name: rule
                                })
                            }
                        });
                    }
                    const classAttr = srcElement.getAttribute("class");
                    const className = (classAttr && dojo.trim(classAttr)) || "";
                    if (this.cssSelect) {
                        _.each(classes, function (_class) {
                            this.cssSelect.addOption({
                                value: _class.name,
                                text: _class.name
                            });
                        }, this);

                        this.cssSelect.setValue(className.split(' '), true);
                    }
                    this._oldClassName = className;
                    // id
                    const idAttr = this._widget.getId();
                    const idName = (idAttr && dojo.trim(idAttr)) || "";
                    this._oldIDName = idName;
                    this.idTextBox.attr("value", idName);
                    if (this._widget.domNode && this._widget.domNode) {
                        let group = this._widget.domNode.blockGroup;
                        if (!group) {
                            group = this._widget.domNode.getAttribute('blockGroup');
                        }
                        this._oldBlockGroup = group;

                    }
                }
            }
            this.changing = false;

        },

        _onChangeIDAttribute: function () {
            if (!this._widget || this.changing) {
                return;
            }
            const inputElement = this.idTextBox;
            // make sure id is valid
            if (!inputElement || !inputElement.isValid()) {
                return;
            }
            if (this.context)
                this.context.blockChange(false);

            const value = inputElement.attr("value");
            if (value !== this._oldIDName) {
                this._oldIDName = value;
                const valuesObject = {};
                valuesObject['id'] = value;
                const command = new ModifyCommand(this._widget, valuesObject, null);
                dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                    source: this._editor.editor_id,
                    command: command
                }]);
            }
        },

        _onChangeClassAttribute: function () {
            if (!this._widget || this.changing) {
                return;
            }
            const inputElement = this.classComboBox;
            if (!inputElement) {
                return;
            }
            if (this.context)
                this.context.blockChange(false);

            const className = inputElement.attr("value");
            if (className !== this._oldClassName) {
                this._oldClassName = className;
                const valuesObject = {};
                valuesObject['class'] = className;
                const command = new davinci.ve.commands.ModifyCommand(this._widget, valuesObject, null);
                dojo.publish("/davinci/ui/widgetPropertiesChanges", [{
                    source: this._editor.editor_id,
                    command: command
                }]);
            }
        },

        _onChangeBlockGroupAttribute: function () {
            if (!this._widget || this.changing) {
                return;
            }
            const inputElement = this.blockGroupComboBox;
            if (!inputElement) {
                return;
            }
            if (this.context)
                this.context.blockChange(false);

            const className = inputElement.attr("value");
            if (className !== this._oldBlockGroup) {
                this._oldBlockGroup = className;
                const valuesObject = {};
                valuesObject['blockGroup'] = className;
                const command = new davinci.ve.commands.ModifyCommand(this._widget, valuesObject, null);
                this.publish("/davinci/ui/widgetPropertiesChanges", [{
                    source: this._editor.editor_id,
                    command: command
                }]);
            }
        },

        _onFieldFocus: function () {
            if (this.context) {
                this.context.blockChange(true);
            }
        },

        _onFieldBlur: function () {
            if (this.context) {
                this.context.blockChange(false);
            }
        },

        _onKeyPress: function (e) {
            // dijit textbox doesn't fire onChange for enter
            if (e.keyCode == dojo.keys.ENTER) {
                this._onChangeIDAttribute();
            }
        }
    });
});
