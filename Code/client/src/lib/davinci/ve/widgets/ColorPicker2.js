define([
    "xide/widgets/ColorPickerWidget",
    "xide/utils",
    "dcl/dcl",
    'xide/widgets/TemplatedWidgetBase',
    "dojo/_base/lang"
], function (ColorPickerWidget, utils, dcl, TemplatedWidgetBase, lang) {
    const ColorPicker = dcl(TemplatedWidgetBase, {
        templateString: '<div ></div>',
        numberDelta: 1,
        insertPosition: 9,
        data: null,
        _onChange: function (event) {
        },
        _setValueAttr: function (value, priority) {
            if(this._dropDown) {
                this._dropDown.set("value", value.replace('#', ''), true);
                this._currentValue = this._dropDown.get("value");
                priority !== false && this._onChange(this._currentValue);
                if (!priority) {
                    if (value !== this._currentValue) {
                        this.onChange();
                    }
                }
            }
        },
        set:function(what,value){
            if (what === 'value' && this._dropDown) {
                return this._setValueAttr(value);
            }
        },
        get: function (what) {
            if (what === 'value' && this._dropDown) {
                return this._getValueAttr();
            }
            return "";
        },
        postCreate: function () {
        },
        startup:function(){
            const widget =utils.addWidget(ColorPickerWidget,{
                title:""
            },null,this.domNode,true);
            this._dropDown = widget;
            this._value = this._value || "";
            this._dropDown.set("value", this._value.replace('#',""), true);
            const self = this;
            this._dropDown._on('change',function(evt){
                self._value = self._getValueAttr();
                self.onChange(self._value);
            });
            this.add(widget);
        },
        _setReadOnlyAttr: function (isReadOnly) {
            this._isReadOnly = isReadOnly;
            this._dropDown.set("disabled", isReadOnly);
        },
        onChange: function () {
            this._value = this._dropDown.get("value");
        },
        _getValueAttr: function () {
            return this._dropDown.get("value");
        }
    });
    dojo.mixin(ColorPicker, {divider: "---"});
    lang.setObject("davinci.ve.widgets.ColorPicker", ColorPicker);
    return ColorPicker;
});