define([
	"dojo/_base/declare",
	"davinci/Runtime",
	"davinci/ve/States",
	"davinci/actions/Action",
	"davinci/commands/CompoundCommand",
	"davinci/ve/commands/AppStateCommand"
], function (declare, Runtime, States, Action, CompoundCommand, AppStateCommand) {


	return declare("davinci.ve.actions.RemoveState", [Action], {

		run: function () {
			let context;
			if (Runtime.currentEditor && Runtime.currentEditor.currentEditor && Runtime.currentEditor.currentEditor.context) {
				context = Runtime.currentEditor.currentEditor.context;
			} else {
				return;
			}
			const statesFocus = States.getFocus(context.rootNode);
			if (!statesFocus || !statesFocus.state || statesFocus.state === States.NORMAL) {
				return;
			}
			const node = statesFocus.stateContainerNode;
			let state = davinci.ve.states.getState(node);
			if (state) {
				const command = new CompoundCommand();
				command.add(new AppStateCommand({
					action: 'remove',
					state: state,
					stateContainerNode: node,
					context: context
				}));
				context.getCommandStack().execute(command);
			}
		}
	});
});