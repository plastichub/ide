define([
	"dojo/_base/declare",
	"davinci/Runtime",
	"davinci/Workbench",
	"davinci/ve/States",
	"davinci/actions/Action",
	"davinci/lang/ve"
], function(
	declare,
	Runtime,
	Workbench,
	States,
	Action,
	veNls){

return declare("davinci.ve.actions.AddState", [Action], {

	run: function(){
		let context;
		if(Runtime.currentEditor && Runtime.currentEditor.currentEditor && Runtime.currentEditor.currentEditor.context){
			context = Runtime.currentEditor.currentEditor.context;
		}else{
			return;
		}
		const statesFocus = States.getFocus(context.rootNode);
		if(!statesFocus || !statesFocus.stateContainerNode){
			return;
		}

		const w = new davinci.ve.actions._AddManageStatesWidget({node: statesFocus.stateContainerNode });
		w._calledBy = 'AddState';

		Workbench.showModal(w, veNls.createNewState, null, null, true);
		w.okButton.set("disabled", true);
	}
});
});