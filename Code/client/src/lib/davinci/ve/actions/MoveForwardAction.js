define([
		"dojo/_base/declare",
		"./_ReorderAction",
		"davinci/commands/CompoundCommand",
		"davinci/ve/commands/ReparentCommand"
], function(declare, _ReorderAction, CompoundCommand, ReparentCommand){


return declare("davinci.ve.actions.MoveForwardAction", [_ReorderAction], {

	name: "MoveForward",
	iconClass: "editActionIcon editMoveForwardIcon",
	
	/**
	 * This is the routine that performs the actions for the MoveToFront command.
	 * @param {Object} context  context object for current visual editor
	 */
	// FIXME: Need to preserve order for siblings that are being moved at once
	run: function(context){
		context = this.fixupContext(context);
		if(!context){
			return;
		}
		const selection = (context && context.getSelection) ? context.getSelection() : [];
		if(selection.length === 0){
			return;
		}
		if(!this.selectionSameParentAllAbsoluteAdjacent(selection)){
			return;
		}
		let widget;
		const parent = selection[0].getParent();
		const children = parent.getChildren();
		const absSiblings = this.getAbsoluteSiblings(selection[0]);
		const compoundCommand = new CompoundCommand();
		// Find the absolutely positioned widget just after the last one in the selection list
		const tempSelection = selection.slice(0);	// clone selection array
		for(let j=0; j<absSiblings.length; j++){
			widget = absSiblings[j];
			const tempIndex = tempSelection.indexOf(widget);
			if(tempIndex>= 0){
				tempSelection.splice(tempIndex, 1);
			}else if(tempSelection.length === 0){
				// If we have encountered everything in tempSelection,
				// then "widget" is the first absolutely positioned widget after last one in the selection list
				break;
			}
		}
		const index = children.indexOf(widget) + 1;
		// By looping through absSiblings, we preserve the relative order of the 
		// currently selected widgets, while pushing all of those widgets to be topmost
		// within the given parent
		for(let i=0; i<absSiblings.length; i++){
			widget = absSiblings[i];
			if(selection.indexOf(widget) >= 0){
				compoundCommand.add(new ReparentCommand(widget, parent, index));
			}
		}
		context.getCommandStack().execute(compoundCommand);
	},

	/**
	 * Enable this command if this command would actually make a change to the document.
	 * Otherwise, disable.
	 */
	isEnabled: function(context){
		context = this.fixupContext(context);
		const selection = (context && context.getSelection) ? context.getSelection() : [];
		if(selection.length === 0){
			return false;
		}
		if(!this.selectionSameParentAllAbsoluteAdjacent(selection)){
			return false;
		}
		const absSiblings = this.getAbsoluteSiblings(selection[0]);
		for(let j=0; j<selection.length; j++){
			const widget = selection[j];
			// If any of the currently selected widgets has a non-selected absolutely positioned sibling
			// later in the list of siblings, then activate this command
			if(absSiblings.indexOf(widget) < absSiblings.length - selection.length){
				return true;
			}
		}
		return false;
	}

});
});