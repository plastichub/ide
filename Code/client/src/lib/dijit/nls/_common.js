define([],function(){
    return {
        buttonOk: "OK",
        buttonCancel: "Cancel",
        buttonSave: "Save",
        itemClose: "Close",
        loadingState: "Loading...",
        errorState: "Sorry, an error occurred"
    }
});
