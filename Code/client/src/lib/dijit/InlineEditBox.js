define([
    "dojo/_base/declare", // declare
    "dojo/sniff", // has("ie")
    "./_Widget",
	"./_TemplatedMixin",
	"./_WidgetsInTemplateMixin"
], function(declare, has, _Widget, _TemplatedMixin, _WidgetsInTemplateMixin){

	// module:
	//		dijit/InlineEditBox

	var InlineEditor = declare("dijit._InlineEditor", [_Widget, _TemplatedMixin, _WidgetsInTemplateMixin],{});
    var InlineEditBox = declare("dijit.InlineEditBox" + (has("dojo-bidi") ? "_NoBidi" : ""), _Widget, {});
	InlineEditBox._InlineEditor = InlineEditor;	// for monkey patching

	return InlineEditBox;
});
