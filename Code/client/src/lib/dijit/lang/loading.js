define([], function () {
    return {
        loadingState: "Loading...",
        errorState: "Sorry, an error occurred"
    }
});
