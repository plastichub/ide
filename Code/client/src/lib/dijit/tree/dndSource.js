define([
    // array.forEach array.indexOf array.map
	"dojo/_base/declare" // declare
], function(declare){

	// module:
	//		dijit/tree/dndSource

	/*=====
	var __Item = {
		// summary:
		//		New item to be added to the Tree, like:
		// id: Anything
		id: "",
		// name: String
		name: ""
	};
	=====*/

	var dndSource = declare("dijit.tree.dndSource", null, {});

	/*=====
	dndSource.__Item = __Item;
	=====*/

	return dndSource;
});
