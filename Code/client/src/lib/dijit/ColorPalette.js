define([
    "./_Widget", // used also to load dijit/hccss for setting has("highcontrast")
	"./_TemplatedMixin",
	"./_PaletteMixin",
    "dojo/_base/Color", // dojo.Color dojo.Color.named
	"dojo/_base/declare" // declare

], function(_Widget, _TemplatedMixin, _PaletteMixin, Color,
	declare){

	// module:
	//		dijit/ColorPalette

	var ColorPalette = declare("dijit.ColorPalette", [_Widget, _TemplatedMixin, _PaletteMixin],{});

	ColorPalette._Color = declare("dijit._Color", Color,{});

	return ColorPalette;
});
