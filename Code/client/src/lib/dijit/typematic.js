define([
    "./main"        // setting dijit.typematic global
], function(dijit){
	var typematic = (dijit.typematic = {});
	return typematic;
});
