define([
	"dojo/_base/declare" // declare
], function(declare){
	// module:
	//		dijit/form/RadioButton

	return declare("dijit.form.RadioButton", [], {
		// summary:
		//		Same as an HTML radio, but with fancy styling.

		baseClass: "dijitRadio"
	});
});
