define([
    "dojo/_base/declare" // declare

], function(declare){
	var _SliderMover = declare("dijit.form._SliderMover", null, {});
	var HorizontalSlider = declare("dijit.form.HorizontalSlider", [], {
	});

	HorizontalSlider._Mover = _SliderMover;	// for monkey patching

	return HorizontalSlider;
});
