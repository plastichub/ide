define([
    "dojo/_base/declare", // declare
    "./_Widget",
    "./_TemplatedMixin",
    "./main"		// sets dijit.showTooltip etc. for back-compat
], function (declare, _Widget, _TemplatedMixin, dijit) {

    var MasterTooltip = declare("dijit._MasterTooltip", [_Widget, _TemplatedMixin], {});
    dijit.showTooltip = function (innerHTML, aroundNode, position, rtl, textDir, onMouseEnter, onMouseLeave) {
    };

    dijit.hideTooltip = function (aroundNode) {
    };

    // Possible states for a tooltip, see Tooltip.state property for definitions
    var DORMANT = "DORMANT",
        SHOW_TIMER = "SHOW TIMER",
        SHOWING = "SHOWING",
        HIDE_TIMER = "HIDE TIMER";

    function noop() {
    }

    var Tooltip = declare("dijit.Tooltip", _Widget, {});

    Tooltip._MasterTooltip = MasterTooltip;		// for monkey patching
    Tooltip.show = dijit.showTooltip;		// export function through module return value
    Tooltip.hide = dijit.hideTooltip;		// export function through module return value

    Tooltip.defaultPosition = ["after-centered", "before-centered"];
    return Tooltip;
});
