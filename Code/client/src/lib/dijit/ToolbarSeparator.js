define([
	"dojo/_base/declare", // declare
    // dom.setSelectable
	"./_Widget",
	"./_TemplatedMixin"
], function(declare, _Widget, _TemplatedMixin){

	// module:
	//		dijit/ToolbarSeparator


	return declare("dijit.ToolbarSeparator", [_Widget, _TemplatedMixin], {});
});
