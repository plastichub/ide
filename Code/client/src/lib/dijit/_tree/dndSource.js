define([
	"dojo/_base/kernel", // kernel.deprecated
	"dojo/_base/lang", // lang.setObject
	"../tree/dndSource"
], function(kernel, lang, dndSource){
	lang.setObject("dijit._tree.dndSource", dndSource);
});
