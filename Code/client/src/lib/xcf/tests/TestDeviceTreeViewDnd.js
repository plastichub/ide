define([
    'dcl/dcl',
    'xdojo/declare',
    'xdojo/has',
    'dojo/_base/lang',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    "xide/views/_ActionMixin",
    'xaction/DefaultActions',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'xgrid/Search',
    'xide/tests/TestUtils',
    'module',
    'xide/views/CIGroupedSettingsView',
    'xide/widgets/WidgetBase',
    'dojo/when',
    'dojo/Deferred',
    "xide/views/_Dialog",
    "xcf/views/DeviceTreeView",
    "xgrid/Noob",
    "dojo/has!xideve?dijit/focus",
    "dojo/has!xideve?davinci/ui/dnd/DragManager",
    "dojo/has!xideve?davinci/ve/utils/GeomUtils",
    "dojo/has!xideve?davinci/ui/dnd/DragSource",
    "dojo/has!xideve?davinci/ve/metadata",
    "dojo/has!xideve?davinci/ve/tools/CreateTool"

], function (dcl,declare,has,lang, utils, types,factory,
             Grid, TreeRenderer, _ActionMixin,
             DefaultActions,KeyboardNavigation,_Widget,Search,TestUtils,module,
             CIGroupedSettingsView,WidgetBase,when,Deferred,_Dialog,DeviceTreeView,Noob,
             FocusUtils , DragManager, GeomUtils, DragSource, Metadata, CreateTool) {
    //http://localhost/projects/x4mm/Code/xapp/xcf/index.php?theme=gray&debug=true&run=run-release-debug&protocols=false&xideve=true&drivers=true&plugins=false&xblox=false&files=true&dijit=debug&xdocker=debug&xfile=debug&xgrid=debug&dgrid=debug



    const ACTION = types.ACTION;

    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const ctx = window.sctx;
    const test = true;
    let marantz;
    let marantzInstance;

    const isVE = has('xideve');

    //isVE = false;
    const _dragStart = function (from, node, context, e, proto, userData, properties) {
        proto = lang.clone(proto);
        lang.mixin(proto.properties, properties);
        const data = e.dragSource.data;
        userData = data.userData || userData;
        Metadata.getHelper(proto.type, 'tool').then(function (ToolCtor) {
            // Copy the data in case something modifies it downstream -- what types can data.data be?
            const tool = new (ToolCtor || CreateTool)(dojo.clone(proto), userData);
            context.setActiveTool(tool);
        }.bind(this));

        // Sometimes blockChange doesn't get cleared, force a clear upon starting a widget drag operation
        context.blockChange(false);
        // Place an extra DIV onto end of dragCloneDiv to allow
        // posting a list of possible parent widgets for the new widget
        // and register the dragClongDiv with Context
        if (e._dragClone) {
            $(e._dragClone).addClass('paletteDragContainer');
            //domClass.add(e._dragClone, 'paletteDragContainer');
            dojo.create('div', {className: 'maqCandidateParents'}, e._dragClone);
        }
        //FIXME: Attach dragClone and event listeners to tool instead of context?
        context.setActiveDragDiv(e._dragClone);
    };

    /**
     *
     * @private
     */
    const _dragEnd = function () {
        if (FocusUtils.curNode && FocusUtils.curNode.blur) {
            FocusUtils.curNode.blur();
        }
    };

    /**
     *
     * @param item
     * @returns {*}
     */
    function createProperties(item) {
        if (!item.ref) {
            return {};
        }
        const block = item.ref.item;


        const props = {
            block: '',
            blockid: block.id,
            scopeid: block.scope.id,
            script: '' + block.name,
            targetevent: 'click'
        };
        return props;
    }

    /**
     *
     * @param from
     * @param node
     * @param context
     * @param proto
     * @param userData
     * @param properties
     */
    const makeDNDG = function (from, node, context, proto, userData, properties) {

        if (!context) {
            return;
        }
        const clone = node.domNode;

        console.log('make dnd',arguments);
        const ds = new DragSource(node.domNode, "component", node, clone);

        ds.targetShouldShowCaret = true;
        ds.returnCloneOnFailure = false;

        /**
         * outer handlers:
         */
        dojo.connect(ds, "onDragStart", dojo.hitch(from, function (e) {
            //console.log('on drag start ');
            _dragStart(from, node, context, e, proto, userData, properties);
        })); // move start

        dojo.connect(ds, "onDragEnd", dojo.hitch(from, function (e) {
            _dragEnd();
        })); // move end


        /**
         * now the inner handlers
         */
        dojo.connect(node.domNode, "onmouseover", function (e) {
            //console.log('-on mouse over ');
            node._mouseover = true;
        });

        dojo.connect(node.domNode, "onmouseout", function (e) {
            // console.log('-on mouse out');
            node._mouseover = false;
        });


        dojo.connect(node.domNode, "onmousedown", function (e) {
            DragManager.document = context.getDocument();
            const frameNode = context.frameNode;
            console.log('-mouse down',[frameNode,DragManager]);
            if (frameNode) {
                const coords = dojo.coords(frameNode);
                const containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
            }
        });
    };


    const veExtras = isVE ? declare('xcf.views.DeviceTreeView',null,{
        onAppReady: function (evt) {
            this.editorContext = evt.context;
            this.updateDragManager(evt.context);
        },
        onDragStart: function (e) {
            const data = e.dragSource.data;
            console.log('drag start ', data);
        },
        onDragEnd: function (e) {
            const data = e.dragSource.data;
            console.log('drag end ', data);
        },
        isDnd: function (item) {
            //console.log('is dnd ',item);
            return item.isDir != true && item.isDnd !== false;
        },
        onDeviceStateChanged: function (e) {
            //console.log('state changed ' + e.item.state,e);
            //this.grid.refreshItem(e.item);
            this.grid.refresh().then(function(){
                console.log('refreshed');
            });
        },
        updateDragManager: function (context) {
            DragManager.document = context.getDocument();
            const frameNode = context.frameNode;
            if (frameNode) {
                const coords = dojo.coords(frameNode);
                const containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
            }
        },
        startup:function(){
            this.subscribe([
                    types.EVENTS.ON_APP_READY,
                    types.EVENTS.ON_DEVICE_STATE_CHANGED
                ]
            );
            const type = 'command';
            const thiz = this;
            //#playground only:
            if(window['lastVEApp']){
                this.onAppReady(window['lastVEApp']);
            }

            this._on('selectionChanged',function(evt){
                const selection = evt.selection;
                const item = selection && selection.length==1 ? selection[0] : null;

                if(!item){
                    return;
                }
                const isAccepted = accept(item,type=='command',type=='variable',type=='device');
                if(!isAccepted){
                    return;
                }
                const row = thiz.row(item);
                const node = row.element;

                if(row.element.__vednd){
                    return;
                }
                node.__vednd=true;
                const _props = createProperties(item);
                const _editorContext = thiz.editorContext;
                const rowObject = {
                    domNode: node
                };
                if (this.isDnd(item)) {
                    makeDNDG(this, rowObject, _editorContext, _protoDefault, item, _props);
                }
            });

            return this.inherited(arguments);
        }
    }) : Noob;



    const gridClass = Grid.createGridClass('driverTreeView',{
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress:false,
            isGroup:function(item){

                if(item){
                    return item.isDir===true;
                }else{
                    return false;
                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                }
            },
            postMixInProperties: function() {
                const thiz = this;

                function getIconClass(item) {

                    if(item.iconClass){
                        return item.iconClass;
                    }
                    const opened = true;
                    const iclass = (!item || item.isDir) ? (opened ? "dijitFolderOpened" : "dijitFolderClosed") : "dijitLeaf";

                    //commands or variables
                    if (item.ref && item.ref.item && item.ref.item.getIconClass) {
                        const _clz = item.ref.item.getIconClass();
                        if (_clz) {
                            return _clz;
                        }
                    } else if (!thiz.isGroup(item)) {
                        return thiz.delegate.getDeviceStatusIcon(item);
                    }

                    if (utils.toString(item.name) === 'Commands') {
                        return 'el-icon-video';
                    }
                    return iclass;
                }

                this.collection = this.collection.filter(this.getRootFilter());


                this.columns=[
                    {
                        renderExpando: true,
                        label: "Name",
                        field: "name",
                        sortable: false,
                        formatter: function (name, item) {

                            const meta = item['user'];

                            if(item.iconClass){
                                return '<span class="grid-icon ' + item.iconClass + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            }

                            if (thiz.isGroup(item)) {
                                return '<span class="grid-icon ' + 'fa-folder' + '"></span>' + item.name;
                            }

                            if (item.icon) {
                                return '<span class="grid-icon ' + item.icon + '"></span>' + item.name;
                            }

                            //commands or variables
                            if (item.ref && item.ref.item) {

                                //@TODO why is here a difference ?
                                if (item.ref.item.name) {
                                    return item.ref.item.name;
                                }
                                if (item.ref.item.title) {
                                    return item.ref.item.title;
                                }
                            }

                            //true device
                            if (!thiz.isGroup(item)) {

                                if (meta) {

                                    return '<span class="grid-icon ' + getIconClass(item) + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                                } else {
                                    return item.name;
                                }
                            } else {
                                return item.name;
                            }
                        }
                    }
                ];

                this.inherited(arguments);



                this._on('onAddAction',function(action){
                    if(action.command == types.ACTION.EDIT){
                        action.label = 'Settings';
                    }
                });
            },
            runAction:function(action,item){
                console.log('run action in',action);
                this.inherited(arguments);
                //thiz.runAction(action,item);
            },
            /**
             * Override render row to add additional CSS class
             * for indicating a block's enabled state
             * @param object
             * @returns {*}
             */
            renderRow:function(object){
                const res = this.inherited(arguments);
                const ref = object.ref;
                const item = ref ? ref.item  : null;


                if(item!=null && item.scope && item.enabled===false){
                    $(res).addClass('disabledBlock');
                }
                return res;
            },
            refresh2:function(force){
                if(this._refreshInProgress){
                    return this._refreshInProgress;
                }

                const _restore = this._preserveSelection();
                const thiz = this;
                const active = this.isActive();
                const res = this.inherited(arguments);

                this._refreshInProgress = res;

                res && res.then && res.then(function(){

                    thiz._refreshInProgress = null;
                    thiz._restoreSelection(_restore,10,true);

                    /*
                    if(_restore.focused && (active || force )) {
                        //console.log('restore focused');
                        thiz.focus(thiz.row(_restore.focused));
                    }*/
                });

                return res;
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR:types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            WIDGET:{
                CLASS:_Widget
            },
            SEARCH:{
                CLASS:Search
            },
            /*,
            DND:{
                CLASS:isVE ? DnD : Noob
            },*/
            EXTRAS:{
                CLASS:veExtras
            }
        },
        {
            RENDERER: TreeRenderer
        },
        null,
        null);


    var _protoDefault = {
        children: "",
        name: "RunScript",
        properties: {
            style: "position:relative",
            scopeid: "",
            targetevent: "click"
        },
        type: "xblox/RunScript",
        userData: {}
    };

    const makeDND = function (from, node, context, proto, userData, properties) {

        if (!context) {
            return;
        }
        const clone = node.domNode;
        const ds = new DragSource(node.domNode, "component", node, clone);
        ds.targetShouldShowCaret = true;
        ds.returnCloneOnFailure = false;
        /**
         * outer handlers:
         */
        from.connect(ds, "onDragStart", dojo.hitch(from, function (e) {
            console.log('on drag start ');
            _dragStart(from, node, context, e, proto, userData, properties);
        })); // move start

        from.connect(ds, "onDragEnd", dojo.hitch(from, function (e) {
            console.log('on drag end');
            _dragEnd();
        })); // move end


        /**
         * now the inner handlers
         */
        node.connect(node.domNode, "onmouseover", function (e) {
            node._mouseover = true;
            const div = node.domNode;
        });

        node.connect(node.domNode, "onmouseout", function (e) {
            node._mouseover = false;
            const div = node.domNode;
        });


        node.connect(node.domNode, "onmousedown", function (e) {

            console.log('mouse down');
            DragManager.document = context.getDocument();
            const frameNode = context.frameNode;
            if (frameNode) {
                const coords = dojo.coords(frameNode);
                const containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
                console.log('drag manager updated ', DragManager);
            }
        });
    };

    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.HEADER,
        ACTION.SEARCH,
        'File/ConnectDevice',
        'File/DisconnectDevice',
        'File/New Group',
        'File/Edit Driver Instance'
    ];

    const module2 = DeviceTreeView = declare('xcf.views.DeviceTreeView',gridClass,{
        iconClass: 'fa-sliders',
        beanType: 'device',
        permissions : DefaultPermissions,
        deselectOnRefresh: false,
        showHeader: false,
        toolbarInitiallyHidden:true,
        //attachDirect:true,
        groupOrder: {
            'Clipboard': 110,
            'Device':105,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'New': 5,
            'Navigation': 3,
            'Select': 0
        },

        focus:function(){
            const res = this.inherited(arguments);
            return res;
        },
        getDeviceActions: function (container,permissions,grid) {
            const actions = [];
            const thiz = this;
            const delegate = thiz.delegate;
            const ACTION = types.ACTION;

            function _selection(){
                const selection = grid.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                if(!item){
                    console.error('have no item');
                    return null;
                }
                return selection;
            }
            ///////////////////////////////////////////////////////////////
            //
            //      Disable Functions
            //
            ///////////////////////////////////////////////////////////////
            function shouldDisableEdit(){

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                return false;
            }
            function shouldDisableRun(){

                const selection = _selection();
                if (!selection || !selection[0].ref || !selection[0].ref.item) {
                    return true;
                }
                return false;
            }
            function shouldDisableConnect(){

                const selection = _selection();
                if (!selection || !selection[0] || !selection[0].path || selection[0].path.indexOf('.json')===-1) {
                    return true;
                }
                return false;
            }

            ///////////////////////////////////////////////////////////////
            //
            //      Actions
            //
            ///////////////////////////////////////////////////////////////

            const defaultMixin = {
                          addPermission:true
                      };

            const DEVICE_COMMAND_GROUP = 'Device/Command';


            actions.push(this.createAction('Edit',ACTION.EDIT,types.ACTION_ICON.EDIT,['f4', 'enter','dblclick'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(this.createAction('Edit Driver','File/Edit Driver',types.ACTION_ICON.EDIT,['f3', 'ctrl enter'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));


            actions.push(this.createAction('Edit Instance','File/Edit Driver Instance',types.ACTION_ICON.EDIT,['f6'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(this.createAction('Console','File/Console','el-icon-indent-left',['f5', 'ctrl s'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(thiz.createAction({
                label: 'Run',
                command:ACTION.RUN ,
                icon: types.ACTION_ICON.RUN,
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: shouldDisableRun,
                mixin: defaultMixin
            }));
            /*
            actions.push(this.createAction('Run', ACTION.RUN,types.ACTION_ICON.RUN, ['r'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableRun,permissions,container,grid
            ));
            */

            actions.push(this.createAction('New Group', 'File/New Group' ,types.ACTION_ICON.NEW_DIRECTORY,
                ['f7'],'Home','New','item|view',null,null,
                defaultMixin,null,null,permissions,container,grid
            ));

            actions.push(this.createAction('New Device', 'File/New Device' ,types.ACTION_ICON.NEW_FILE, ['ctrl n'],'Home','New','item',null,null,
                defaultMixin,null,null,permissions,container,grid
            ));



            ///////////////////////////////////////////////////////////////
            //  Device Control
            actions.push(this.createAction('Disconnect', 'File/DisconnectDevice','fa-unlink', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableConnect,permissions,container,grid
            ));

            actions.push(this.createAction('_Connect', 'File/ConnectDevice','fa-link', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableConnect,permissions,container,grid
            ));


            /*
             actions.push(this.createActionParameters('New Device Group', 'File/New Device Group', 'edit', types.ACTION_ICON.NEW_DIRECTORY, function () {
             delegate.newGroup(thiz.getSelectedItem());
             }, 'F7', 'f7', null, container, thiz));
             */



            return actions;
        },
        editItem: function (item) {
            const thiz = this;
            if (item.type === types.ITEM_TYPE.BLOCK) {
                return thiz.blockManager.editBlock(item.ref.item, null, false);
            }
            if(item.driver){
                item = item.driver;
            }

            thiz.openItem(item);
        },
        startup:function(){

            const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);
            this.inherited(arguments);
            this.addActions(_defaultActions);
            this.addActions(this.getDeviceActions(this.domNode,this.permissions,this));
            const thiz = this;
            this.refresh().then(function(){
                thiz.select([0], null, true, {
                    focus: true
                });
            });

            this.subscribe(types.EVENTS.ON_STORE_CHANGED,function(evt){
                if(evt.store == thiz.collection){
                    thiz.refresh();
                }
            });


            thiz.collection.on('update',function(){
                //console.log('store item update');
                thiz.refresh();
            });
            thiz.collection.on('delete',function(){
                //console.log('store item update');
                thiz.refresh();
            });

            this._on('selectionChanged',function(e){
                //console.log(e.selection);
            });
        },
        //////////////////////////////////////////////////////////////////////
        //
        //  Actions - Impl.
        //
        changeSource:function(source){
            console.log('change source',arguments);


            const thiz = this;
            const delegate = thiz.delegate;
            const scope = source.scope;
            let store = delegate.getStore(scope);


            function wireStore(store){

                if(thiz['_storeConnected'+scope]){
                    return;
                }

                thiz['_storeConnected'+scope]=true;

                function updateGrid() {
                    setTimeout(function () {
                        thiz.refresh();
                    }, 100);
                }

                store.on('update', function () {
                    console.warn(' store updated: ',arguments);
                    updateGrid();

                });

                store.on('added', function () {
                    //console.log(' added to store: ',arguments);
                    updateGrid();
                });

                store.on('remove', function () {
                    //console.log(' removed from store: ',arguments);
                    updateGrid();
                });

                store.on('delete', function () {
                    //console.log(' deleted from store: ',arguments);
                    updateGrid();
                });

            }

            function ready(store){


                const collection = store.filter(thiz.getRootFilter());
                thiz.set('collection',collection);
                thiz.set('title','Drivers ('+source.label+')');
                thiz.scope = scope;
                wireStore(store);


            }

            if(store.then){

                store.then(function(){
                    store = delegate.getStore(scope);
                    ready(store);
                });
            }else{

                //is store
                ready(store);
            }
        },
        ///////////////////////////////////////////////////////////////
        runAction: function (action,_item) {
            if (!action || !action.command) {
                return;
            }

            console.log('--run action ' + action.command);

            const ACTION = types.ACTION;
            const thiz = this;
            const delegate = thiz.delegate;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            const driverManager = ctx.getDriverManager();
            let item = thiz.getSelectedItem() || _item;
            var device = item ? item.device : null;

            if(action.command.indexOf('View/Source')!=-1){
                return this.changeSource(action.item);
            }

            switch (action.command) {

                case 'File/Edit Driver Instance':{
                    var driverInstance  =  item.driverInstance;
                    if(driverInstance){

                        var driver = driverInstance.driver;
                        return driverManager.openItemSettings(driver,item);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'File/Edit Driver':{
                    const meta = item['user'];
                    const driverId = meta ? utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER) : null;
                    const driver = driverId ? driverManager.getItemById(driverId) : null;

                    if(driver){
                        return driverManager.openItemSettings(driver,null);
                    }

                    /*
                     var driverInstance  =  item.driverInstance;
                     if(driverInstance){
                     var driver = driverInstance.driver;
                     return driverManager.openItemSettings(driver,item);
                     }
                     */

                    break;
                }

                case ACTION.RUN:{
                    if (item && item.ref) {
                        const _device = item.ref.device;
                        if(_device){
                            var driverInstance = _device.driverInstance;
                            if(driverInstance){
                                const scope = driverInstance.blockScope;
                                const block = scope.getBlockById(item.id);
                                if(block){
                                    block.solve(block.scope);
                                }
                            }
                        }
                    }
                    break;
                }
                case 'File/Code':{
                    var device = item.device || null;
                    if(device && device.driverInstance){
                        item = device.driverInstance.driver;
                    }
                    return delegate.editDriver(null,item);
                }
                case 'File/Console':
                {
                    if(device && !device.driverInstance) {
                        deviceManager.startDevice(device).then(function(){
                            deviceManager.openConsole(item);
                        });
                        return;
                    }
                    return deviceManager.openConsole(item);
                }
                case 'File/Log':
                {
                    return deviceManager.openLog(item.device);
                }

                case ACTION.EDIT:
                {
                    return thiz.editItem(item);
                }
                case ACTION.RELOAD:
                {
                    return thiz.refresh();
                }
                case ACTION.DELETE:
                {
                    var _res = delegate.onDeleteItem(item);

                    return _res;
                }
                case 'File/DisconnectDevice':{

                    const dfd = delegate.stopDevice(item);
                    thiz.refresh();
                    return false;
                }
                case 'File/ConnectDevice':{
                    delegate.startDevice(item);
                    thiz.refresh();
                    return false;
                }
                case 'File/New Device':{

                    const _rest = delegate.newItem(item);
                    thiz.refresh();
                    return _rest;
                }
                case 'File/New Group':{
                    var _res = delegate.newGroup(item,thiz.scope);

                    thiz.refresh();
                    return _res;
                }
            }

            return this.inherited(arguments);
        },
        changeScope: function (name) {

            this.delegate.ls(name + '_drivers').then(function (data) {
                console.log('got scope ', data);
            });

        },
        getScopeActions: function () {
            const actions = [];
            const thiz = this;
            container = this.containerNode;

            this.addAction(actions, _ActionMixin.createActionParameters('Settings', "View/Settings", 'edit', 'fa-cogs', function () {

            }, 'CTRL F1', ['ctrl f1'], null, container, thiz, {
                dummy: true,
                onCreate: function (action) {
                    action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                        widgetArgs: {
                            style: "float:right"
                        }
                    });
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('System Drivers', "View/Settings/System", 'view', 'fa-cloud', function () {

                thiz.changeScope('system');

            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                },
                tooltip: {
                    content: "Displays only drivers from the system library<br/> Hint: you can find system drivers also in the file manager!"
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('User Drivers', "View/Settings/User", 'view', 'fa-cloud', function () {
                thiz.changeScope('user');
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Project Drivers', "View/Settings/Project", 'view', 'fa-cloud', function () {
                thiz.changeScope('project');
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Add Driver Location', "View/Settings/Custom", 'view', 'fa-magic', function () {
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Show Values', "View/Settings/Values", 'viewFilter', 'fa-cogs', function () {
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));


            return actions;
        },
        openItem:function(item,device){
            if(item && !item.isDir && !item.virtual){
                this.delegate.openItemSettings(item,device);
            }
        }

    });

    /**
     * Url generator for device/driver/[command|block|variable]
     *
     * @param device
     * @param driver
     * @param block
     * @param prefix
     * @returns {*}
     */
    function toUrl(device,driver,block,prefix){

        prefix = prefix || '';

        const pattern = prefix + "deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";

        const url = lang.replace(
            pattern,
            {
                deviceId:device.id,
                deviceScope:device.scope,
                driverId:driver.id,
                driverScope:driver.scope,
                block:block.id
            });
        return url;
    }
    /**
     * Filter function to reject selections in a device - tree - view
     * @param item
     * @param acceptCommand
     * @param acceptVariable
     * @param acceptDevice
     * @returns {*}
     */
    function accept(item,acceptCommand,acceptVariable,acceptDevice){
        const reference = item.ref || {};
        const block       =       reference.item;
        const device      =       reference.device;
        const driver      =       reference.driver;
        const scope       =       block ? block.getScope() : null;
        const isCommand   =       block ? block.declaredClass.indexOf('model.Command')!==-1 : false;
        const isVariable  =       block ? block.declaredClass.indexOf('model.Variable')!==-1 : false;

        isCommand && console.log('have command ' + block.name);

        if(isCommand && acceptCommand){
            return toUrl(device,driver,block,'command://');
        }
        if(isVariable && acceptVariable){
            return toUrl(device,driver,block,'variable://');
        }

        return false;
    }
    /**
     * Renders a CIS inline
     * @param CIS
     * @param where
     * @param owner
     * @returns {*}
     */
    function renderCIS(CIS,where,owner){

        const widgetsHead = factory.createWidgetsFromArray(CIS, owner, null, false);
        const result = [];
        const dfd = new Deferred();

        when(widgetsHead,function(widgets){

            if (widgets) {


                for (let i = 0; i < widgets.length; i++) {

                    const widget = widgets[i];

                    widget.delegate = owner;

                    where.appendChild(widget.domNode);

                    if(where && where.lazy===true) {
                        widget._startOnShow = true;
                    }else{
                        widget.startup();
                    }


                    widget._on('valueChanged',function(evt){
                        //owner.onValueChanged(evt);
                    });

                    owner._emit('widget',{
                        widget:widget,
                        ci:widget.userData
                    })


                    result.push(widget);

                    widget.userData.view=owner;
                    widget.onAttached && widget.onAttached(where);

                    if(owner && owner.add && owner.add(widget,null,false)){

                    }else{
                        console.error('view has no add',owner);
                        owner.add(widget,null,false);
                    }
                }

                dfd.resolve(result);

            }
        });

        return dfd;

    }
    /**
     * Create a simple CIS
     * @returns {{inputs: *[]}}
     */
    function createPickerCIS(instance){



        const CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General9',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Command",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("86025507-97e0-99a4-0640-59c5725ef116"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'command'
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("6ab97609-4b3a-4f65-d018-1027d402a94d"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'variable'
                }
            ]
        };
        _.each(CIS.inputs,function(ci){
            ci.driver = instance.driver,
                ci.device = instance.device
        });
        return CIS;
    }
    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    function createDeviceView(parent,ctx,driver,device,type) {


        const grid = utils.addWidget(module2, {
            title: 'Devices',
            collection: ctx.getDeviceManager().getStore(),
            delegate: ctx.getDeviceManager(),
            blockManager: ctx.getBlockManager(),
            ctx: ctx,
            icon: 'fa-sliders',
            showHeader: false,
            scope: 'system_devices',
            resizeToParent: true,
            dndParams: {
                allowNested: true, // also pick up indirect children w/ dojoDndItem class
                checkAcceptance: function (source, nodes) {
                    return true;
                    return source !== this; // Don't self-accept.
                },
                isSource: true
            }
        }, null, parent.containerNode, true);

        /*
        grid._on('selectionChanged',function(evt){
            var selection = evt.selection,
                item = selection && selection.length==1 ? selection[0] : null;

            if(!item){
                return;
            }

            var isAccepted = accept(item,type=='command',type=='variable',type=='device');
            isAccepted  && console.log('-selected : ' + isAccepted,item);
            if(isAccepted){
                grid._selected = isAccepted;
            }
        });
        */

        return grid;
    }

    function createPickerWidget(device,driver){

        const Widget = dcl(WidgetBase, {
            declaredClass: "xcf.widgets.CommandPickerWidget",
            minHeight: "400px;",
            value: "",
            options: null,
            title:'Command',
            templateString: "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
            "<table border='0' cellpadding='5px' width='100%' >" +
            "<tbody>" +
            "<tr attachTo='extensionRoot'>" +
            "<td width='15%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
            "<td width='100px' class='widgetValue' valign='middle' attachTo='valueNode'></td>" +
            "<td class='extension' width='25px' attachTo='button0'></td>" +
            "<td class='extension' width='25px' attachTo='button1'></td>" +
            "<td class='extension' width='25px' attachTo='button2'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
            onSelect: function (ctx,driver,device,title) {
                const thiz = this;

                const _defaultOptions = {};
                const ci = this.userData;

                if (this.options) {
                    lang.mixin(_defaultOptions, this.options);
                }

                try {

                    const value = utils.toString(this.userData['value']);

                    const dlg = new _Dialog({
                        size: types.DIALOG_SIZE.SIZE_SMALL,
                        title: '%%Select a device ' +title,
                        type:types.DIALOG_TYPE.INFO,
                        bodyCSS: {
                            'height': 'auto',
                            'min-height': '300px',
                            'padding': '8px',
                            'margin-right': '16px'
                        },
                        picker: null,
                        onOk: function () {

                            const selected = this.picker._selected;
                            selected && thiz.onValueChanged(selected);

                        },
                        message: function (dlg) {
                            const thiz = dlg.owner;
                            const picker = createDeviceView($('<div class="container" style="height: inherit;" />'),ctx,driver,device,ci.pickerType);
                            thiz.picker = picker;
                            thiz.add(picker, null, false);
                            return $(picker.domNode);
                        },
                        onShow: function (dlg) {
                            const picker = this.picker;
                            const self = this;

                            picker.startup();
                            this.startDfd.resolve();
                        }
                    });


                    dlg.show().then(function () {
                        dlg.resize();
                        utils.resizeTo(dlg.picker, dlg, true, true);
                    });


                    this.add(dlg,null,false);


                } catch (e) {
                    logError(e);
                }
            },
            postMixInProperties: function () {

                this.inherited(arguments);

                if (this.userData && this.userData.title) {
                    this.title = this.userData.title;
                }

                if ((this.userData && this.userData.vertical === true) || this.vertical === true) {

                    this.templateString = "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
                        "<table border='0' cellpadding='5px' width='100%' >" +
                        "<tbody>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='100%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
                        "</tr>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='50%' class='widgetValue' valign='middle' attachTo='valueNode'></td>" +
                        "<td class='extension' width='25px' attachTo='button0'></td>" +
                        "<td class='extension' width='25px' attachTo='button1'></td>" +
                        "<td class='extension' width='25px' attachTo='button2'></td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
                        "<div attachTo='last'></div>" +
                        "</div>"
                }

            },
            onValueChanged:function(value){
                value = value.newValue || value;
                this.setValue(value);

                const ci = this.userData;
                const block = ci.block;
                const blockScope = block.scope;

                const title = blockScope.toFriendlyName(block, value);

                this.commandWidget._renderItem({
                    value:value,
                    label:title
                });



                this.commandWidget.set('value',value,title);
            },
            fillTemplate: function () {
                const thiz = this;

                let value = utils.toString(this.userData['value']);


                const ci = this.userData;
                const pickerType = ci.pickerType;
                const block = ci.block;
                const blockScope = block.scope;
                const ctx = blockScope.ctx;
                const driver = blockScope.driver;
                const device = blockScope.device;
                const deviceManager = ctx.getDeviceManager();
                const driverManager = ctx.getDriverManager();

                //add the Select Widget for local commands
                const CIS = [];

                const isCommand = pickerType == 'command';

                let title = isCommand ? 'Command' : 'Variable';
                const options = isCommand ? blockScope.getCommandsAsOptions() : blockScope.getVariablesAsOptions();







                //value = 'command://deviceScope=system_devices&device=e5a06e24-6aa4-c8c5-3ffc-9d84d85…9a66&driverScope=system_drivers&block=963944c2-c0b0-fe8a-504e-1b8bedd2a3cf';
                value = "command://deviceScope=system_devices&device=e5a06e24-6aa4-c8c5-3ffc-9d84d8528a91&driver=235eb680-cb87-11e3-9c1a-0800200c9a66&driverScope=system_drivers&block=6d0c5e0e-5c04-bb98-44a0-705c8269de07";

                if(value.indexOf('://')){
                    title = blockScope.toFriendlyName(block,value);
                }



                CIS.push(utils.createCI('value', 3, 'Volume', {
                    group: 'General',
                    title: title,
                    dst: pickerType,
                    //options: options,
                    widget: {
                        templateString: '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode">',
                        store:blockScope.blockStore,
                        labelField:'name',
                        valueField:'id',
                        search:true,
                        title:isCommand ? 'Command' : 'Variable',
                        query:{
                            group: isCommand ? 'basic' : 'basicVariables'
                        }
                    }
                }));


                if(pickerType == 'variable'){
                    //debugger;
                }


                factory.renderCIS(CIS, this.valueNode, this).then(function (widgets) {

                    const commandWidget = widgets[0];
                    thiz.commandWidget = commandWidget;

                    const btn = factory.createSimpleButton('', 'fa-magic', 'btn-default', {
                        style: ''
                    });

                    commandWidget._on('onValueChanged',function(e){
                        thiz.onValueChanged(e);
                    });


                    $(btn).click(function () {
                        thiz.onSelect(ctx,driver,device,isCommand ? 'command' : 'variable');
                    });
                    $(thiz.button0).append(btn);
                });
            },
            startup: function () {
                try {
                    this.fillTemplate();
                    this.onReady();
                }catch(e){
                    logError(e);
                }
            }
        });

        return Widget;

    }

    function testPicker(instance){
        const toolbar = ctx.mainView.getToolbar();
        const docker = ctx.mainView.getDocker();
        const parent  = TestUtils.createTab(null,null,module.id);
        const cisRenderer = dcl(CIGroupedSettingsView,{
            typeMap:null,
            getTypeMap:function(){
                if (this.typeMap) {
                    return this.typeMap;
                }
                const self = this;
                const typeMap = {};

                typeMap['Command'] = createPickerWidget();
                this.typeMap = typeMap;
                return typeMap;
            }
        });

        const ciView = utils.addWidget(cisRenderer,{
            cis:createPickerCIS(instance).inputs
        },null,parent,true);


        docker.resize();
    }

    if(test && ctx){
        console.clear();
        //var parent  = TestUtils.createTab(null,null,module.id);
        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();
        const grid = null;

        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        marantzInstance = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        const driver = marantz.driver;
        const device = marantz.device;

        //var parent  = TestUtils.createTab(null,null,module.id);
        const parent = TestUtils.createNavigationTab('Test',null,module.id);
        const view = createDeviceView(parent,ctx,driver,device,'command');
        //testPicker(marantzInstance);
        ctx.getWindowManager().registerView(view,true);
    }
    return DeviceTreeView;
});
