/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xblox/views/BlockGrid',
    'xide/views/CIGroupedSettingsView',
    'xcf/model/Command',
    'xcf/model/Variable',
    'xide/widgets/ToggleButton',
    'xide/widgets/_ActionValueWidgetMixin',
    'xide/layout/_Accordion',
    "xide/widgets/TemplatedWidgetBase",
    "dijit/form/TextBox",
    'xblox/model/variables/VariableAssignmentBlock',
    'dojo/promise/all',
    "dojo/Deferred",
    "xide/tests/TestUtils",
    'xide/_base/_Widget',
    'xide/widgets/_Widget',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'xaction/ActionProvider',
    'xide/widgets/ContextMenu',
    'xide/widgets/WidgetBase',
    'xide/widgets/ExpressionEditor',
    'xide/views/ConsoleView',
    "module"


], function (dcl, declare, types,
             utils, Grid, factory, CIViewMixin, BlockGrid,
             CIGroupedSettingsView,
             Command, Variable,
             ToggleButton, _ActionValueWidgetMixin, _Accordion, TemplatedWidgetBase,
             TextBox, VariableAssignmentBlock,
             all, Deferred, TestUtils, _Widget, Widget, _LayoutMixin, Docker, ActionProvider,
             ContextMenu, WidgetBase, ExpressionEditor, ConsoleView, module) {
    console.clear();



    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;
    let widget;
    let basicGridInstance;

    /***
     * playground
     */
    const _lastGrid = window._lastGrid;
    const ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;
    let scope;
    let blockManager;
    let driverManager;
    var marantz;


    const _actions = [


    ];







    function completeGrid(_grid) {

        _grid._on(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, function (evt) {

            const items = evt.items;

            const variables = this.blockScope.getVariables();
            const variableItems = [
                /*{
                name: 'None',
                //target: item,
                iconClass: 'el-icon-compass',
                proto: VariableAssignmentBlock,
                item: null,
                ctrArgs: {
                    variable: null,
                    scope: this.blockScope,
                    value: ''
                }
            }*/
            ];


            //console.log('on insert block end',variables);



            _.each(variables,function(variable){
                variableItems.push({
                    name: variable.name,
                    //target: item,
                    iconClass: 'el-icon-compass',
                    proto: VariableAssignmentBlock,
                    item: variable,
                    ctrArgs: {
                        variable: variable.name,
                        scope: this.blockScope,
                        value: ''
                    }
                });
            },this);

            items.push({
                name: 'Set Variable',
                iconClass: 'el-icon-pencil-alt',
                items: variableItems
            });









        });

        _grid._on('onAddActions', function (evt) {
            if(!evt.addAction){
                return;
            }
            const addAction = evt.addAction;
            const cmdAction = 'New/Command';
            const varAction = 'New/Variable';
            const permissions = evt.permissions;
            const VISIBILITY = types.ACTION_VISIBILITY;
            const thiz = this;

            /*
            addAction('Save', 'File/Save', 'fa-save', ['ctrl s'], 'Home', 'File', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);
            */



            addAction('Command', cmdAction, 'el-icon-plus-sign', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);

            addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);


            addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {
                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                            widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                            widgetArgs: {
                                icon1: 'fa-toggle-on',
                                icon2: 'fa-toggle-off',
                                delegate: thiz,
                                checked: false,
                                iconClass: 'fa-toggle-off'
                            }
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });



            const settingsWidget = declare('commandSettings', TemplatedWidgetBase,{
                templateString:'<div></div>',
                _getText: function (url) {
                    let result;
                    const def = dojo.xhrGet({
                        url: url,
                        sync: true,
                        handleAs: 'text',
                        load: function (text) {
                            result = text;
                        }
                    });
                    return '' + result + '';
                },
                startup:function(){

                    this.inherited(arguments);

                    if(!_grid.userData){
                        return;
                    }

                    const settings = utils.getJson(_grid.userData['params']) || {
                            constants: {
                                start: '',
                                end: ''
                            },
                            send: {
                                mode: false,
                                interval: 500,
                                timeout: 500,
                                onReply: ''
                            }
                        };




                    const settingsPane = utils.templatify(
                        null,
                        this._getText(require.toUrl('xcf/widgets/templates/commandSettings.html')),
                        this.domNode,
                        {
                            baseClass: 'settings',
                            style:'width:100%',
                            start: settings.constants.start,
                            end: settings.constants.end,
                            interval: settings.send.interval,
                            timeout: settings.send.timeout,
                            sendMode: settings.send.mode,
                            onReply: settings.send.onReply,
                            settings: settings
                        }, null
                    );

                    return;

                    if (settings.send.mode) {
                        settingsPane.rReply.set('checked', true);
                    } else {
                        settingsPane.rInterval.set('checked', true);
                    }

                    const _onSettingsChanged = function () {
                        //update params field of our ci
                        thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                        //thiz.save();
                        console.log('changd');
                    };



                    //wire events
                    dojo.connect(settingsPane.wStart, "onChange", function (item) {
                        settingsPane.settings.constants.start = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wEnd, "onChange", function (item) {
                        settingsPane.settings.constants.end = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wInterval, "onChange", function (item) {
                        settingsPane.settings.send.interval = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wTimeout, "onChange", function (item) {
                        settingsPane.settings.send.timeout = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wOnReply, "onChange", function (item) {
                        settingsPane.settings.send.onReply = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.rReply, "onChange", function (item) {
                        settingsPane.settings.send.mode = item;
                        _onSettingsChanged();
                    });
                }
            });


            addAction('Settings', 'File/Settings', 'fa-gears', null, 'Settings', 'Settings', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {

                        action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null);

                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON,{
                            widgetClass:settingsWidget
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });
        });

        _grid._on('selectionChanged', function (evt) {
            //console.log('selection ',evt);
            //since we can only add blocks to command and not
            //at root level, disable the 'Block/Insert' root action and
            //its widget //references
            const thiz = this;

            const selection = evt.selection;
            const item = selection[0];
            const blockInsert = thiz.getAction('Block/Insert');
            const blockEnable = thiz.getAction('Step/Enable');

            disable = function (disable) {
                blockInsert.set('disabled', disable);
                setTimeout(function () {
                    blockInsert.getReferences().forEach(function (ref) {
                        ref.set('disabled', disable);
                    });
                }, 100);

            };

            const _disable =item ? false : true;

            disable(_disable);


            if (item) {
                blockEnable.getReferences().forEach(function (ref) {
                    ref.set('checked', item.enabled);
                });
            }else{
                /*
                var props = _grid.getPropertyStruct();
                props._lastItem = null;
                _grid.setPropertyStruct(props);*/
            }
        });

        _grid.startup();

    }

    const propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };


    const DockerBase = dcl([ActionProvider.dcl,Widget.dcl],{
        permissions:[
          types.ACTION.RENAME
        ],
        __panel:function(el){
            console.log(el);
            const panels = this.getPanels();

            let frame = null;
            const self = this;
            _.each(this._frameList,function(_frame){
                if($.contains(_frame.$container[0],el)){
                    frame = _frame;
                }
            });




            const id = self.contextMenuPanelId;

            this.contextMenuEventTarget = null;
            this.contextMenuPanelId = null;

            if(frame && id!==null){

                const _panel = frame.panel(id);
                if(_panel){
                    return _panel;
                }
            }
        },
        runAction:function(action,type,event){


            const _panel = this.contextMenuEventTarget ? this.__panel(this.contextMenuEventTarget) : null;
            if(_panel){
                this.removePanel(_panel);
            }

            console.error('run action',_panel);
        },
        getDockerActions:function(){

            const actions = [];

            actions.push(this.createAction({
                label:'Close',
                command:types.ACTION.RENAME,
                tab:'View',
                group:'Misc',
                mixin: {
                    addPermission: true
                },
                icon:'fa-close'
                /*keycombo:['ctrl w','ctrl f4']*/
            }));


            return actions;

        },
        contextMenu:null,
        contextMenuEventTarget:null,
        contextMenuPanelId:null,
        setupActions:function(){

            const self = this;

            this.__on(this._root.$container,'contextmenu', '.wcPanelTab', function(e){
                console.error('context menu');
                self.contextMenuEventTarget  = e.target;
                self.contextMenuPanelId = e.target.parentNode ?  e.target.parentNode.id : null;
            });


            this.addActions(this.getDockerActions());


            const args = {
                owner:this,
                delegate:this,
                limitTo:'wcPanelTab'
            };

            const node = this._root.$container[0];

            const contextMenu = new ContextMenu(args,node);
            contextMenu.openTarget = node;
            contextMenu.init({preventDoubleContext: false});

            //contextMenu._registerActionEmitter(this);
            contextMenu.setActionEmitter(this,types.EVENTS.ON_VIEW_SHOW,this);

            this.contextMenu = contextMenu;


            this.add(contextMenu,null,false);
        },
        __init:dcl.superCall(function(sup){
            return function(){
                if(sup){

                    sup.apply(this, arguments);
                    this.setupActions();
                }
                return 0;
            };
        }),
        __destroy:function(){
            console.error('_destroy');
        }
    });

    const DriverViewClass = dcl([_Widget,_LayoutMixin.dcl],{
        templateString:'<div style="with:inherit;height: inherit"/>',
        showBasicCommands:true,
        showConditionalCommands:true,
        showResponseBlocks:true,
        showLog:true,
        showConsole:true,
        showVariables:true,
        logTab:null,
        consoleTab:null,
        basicCommandsTab:null,
        conditionalTab:null,
        variablesTab:null,
        responseTab:null,
        gridClass:null,
        createTab:function(type,args){

            const DOCKER = types.DOCKER;
            const defaultTabArgs = {
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:DOCKER.TAB.TOP,
                location:DOCKER.DOCK.STACKED

            };
            return this._docker.addTab(type || 'DefaultTab',utils.mixin(defaultTabArgs,args));
        },
        createLayout:function(){


            const DOCKER = types.DOCKER;
            this._docker = Docker.createDefault(this.domNode,{
                extension:DockerBase
            });

            const basicCommands = this.showBasicCommands ? this.createTab(null,{
                title:'Basic Commands'
            }) : null;

            const condCommands = this.showConditionalCommands ? this.createTab(null,{
                title:'Condition Commands',
                tabOrientation:DOCKER.TAB.TOP,
                target:basicCommands
            }) : null;

            const log = this.showLog ? this.createTab(null,{
                title:'Log',
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM,
                icon:'fa-calendar'
            }) : null;

            const consoleTab = this.showConsole ? this.createTab(null,{
                title:'Console',
                tabOrientation:DOCKER.TAB.TOP,
                target:log,
                icon:'fa-terminal'
            }) : null;

            const lastBottom = log || consoleTab;



            const responses = this.showResponseBlocks ? this.createTab(null,{
                title:'Responses',
                tabOrientation:DOCKER.TAB.TOP,
                target:lastBottom
            }) : null;

            const variables = this.showVariables ? this.createTab(null,{
                title:'Variables',
                tabOrientation:DOCKER.TAB.TOP,
                target:lastBottom
            }) : null;

            this.logTab=log;
            this.consoleTab=consoleTab;
            this.basicCommandsTab = basicCommands;
            this.conditionalTab = condCommands;
            this.variablesTab = variables;

            this.responseTab=responses;


            return this._docker;


        },
        completeGrid:function(grid){

            grid.userData = this.userData;
            const widget = this;
            function _completeGrid(_grid) {
                _grid._on('onAddActions', function (evt) {
                    const addAction = evt.addAction;
                    const cmdAction = 'New/Command';
                    const varAction = 'New/Variable';
                    const permissions = evt.permissions;
                    const VISIBILITY = types.ACTION_VISIBILITY;
                    const thiz = this;

                    if(!addAction){
                        return;
                    }

                    addAction('Command', cmdAction, 'el-icon-plus-sign', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,{
                        dummy: true,
                        addPermission: true
                    },null, null);

                    addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,{
                        dummy: true,
                        addPermission: true
                    },null, null);



                    addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                        {
                            addPermission: true,
                            onCreate: function (action) {
                                action.handle=false;
                                action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                                    widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                                    widgetArgs: {
                                        icon1: 'fa-toggle-on',
                                        icon2: 'fa-toggle-off',
                                        delegate: thiz,
                                        checked: false,
                                        iconClass: 'fa-toggle-off'
                                    }
                                });
                            }
                        }, null, function () {
                            return thiz.getSelection().length == 0;
                        });



                    const settingsWidget = declare('commandSettings', TemplatedWidgetBase,{
                        templateString:'<div></div>',
                        _getText: function (url) {
                            let result;
                            const def = dojo.xhrGet({
                                url: url,
                                sync: true,
                                handleAs: 'text',
                                load: function (text) {
                                    result = text;
                                }
                            });
                            return '' + result + '';
                        },
                        startup:function(){
                            if(this._started){
                                return;
                            }

                            this.inherited(arguments);

                            if(!_grid.userData){
                                return;
                            }

                            const settings = utils.getJson(_grid.userData['params']) || {
                                    constants: {
                                        start: '',
                                        end: ''
                                    },
                                    send: {
                                        mode: false,
                                        interval: 500,
                                        timeout: 500,
                                        onReply: ''
                                    }
                                };

                            if(!widget.settingsTemplate){
                                widget.settingsTemplate = this._getText(require.toUrl('xcf/widgets/templates/commandSettingsNew.html'));
                            }


                            const settingsPane = utils.templatify(
                                null,
                                widget.settingsTemplate,
                                this.domNode,
                                {
                                    baseClass: 'settings',
                                    start: settings.constants.start,
                                    end: settings.constants.end,
                                    interval: settings.send.interval,
                                    timeout: settings.send.timeout,
                                    sendMode: settings.send.mode,
                                    onReply: settings.send.onReply,
                                    settings: settings
                                }, null
                            );


                            if (settings.send.mode) {
                                $(settingsPane.rReply).prop('checked', true);
                            } else {
                                $(settingsPane.rInterval).prop('checked', true);
                            }



                            const _onSettingsChanged = function () {
                                //update params field of our ci
                                thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                                widget.setValue('{}');
                            };

                            //start
                            $(settingsPane.start).on('change',function(e){
                                settingsPane.settings.constants.start = e.target.value;
                                _onSettingsChanged();
                            });

                            //end
                            $(settingsPane.end).on('change',function(e){
                                settingsPane.settings.constants.end = e.target.value;
                                _onSettingsChanged();
                            });






                            //mode
                            $(settingsPane.rReply).on("change", function (e) {
                                const value = e.target.value=='on' ? 1:0;
                                settingsPane.settings.send.mode = value;
                                _onSettingsChanged();
                            });

                            $(settingsPane.rInterval).on("change", function (e) {
                                const value = e.target.value=='on' ? 0:1;
                                settingsPane.settings.send.mode = value;
                                _onSettingsChanged();
                            });


                            //interval time
                            $(settingsPane.wInterval).on('change',function(e){
                                settingsPane.settings.send.interval = e.target.value;
                                _onSettingsChanged();
                            });






                            //on reply value
                            $(settingsPane.wOnReply).on('change',function(e){

                                settingsPane.settings.send.onReply = e.target.value;
                                _onSettingsChanged();
                            });

                            //onReply - timeout
                            $(settingsPane.wTimeout).on('change',function (e) {
                                settingsPane.settings.send.timeout = e.target.value;
                                _onSettingsChanged();
                            });

                        }
                    });


                    addAction('Settings', 'File/Settings', 'fa-gears', null, 'Settings', 'Settings', 'item|view', null, null,
                        {
                            addPermission: true,
                            handle:false,
                            onCreate: function (action) {

                                action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, null);

                                action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                                action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null);

                                action.setVisibility(types.ACTION_VISIBILITY.RIBBON,{
                                    widgetClass:settingsWidget
                                });
                            }
                        }, null, function () {
                            return thiz.getSelection().length == 0;
                        });
                });
                _grid._on('selectionChanged', function (evt) {
                    //since we can only add blocks to command and not
                    //at root level, disable the 'Block/Insert' root action and
                    //its widget //references
                    const thiz = this;

                    const selection = evt.selection;
                    const item = selection[0];
                    const blockInsert = thiz.getAction('Block/Insert');
                    const blockEnable = thiz.getAction('Step/Enable');
                    const newCommand = thiz.getAction('New/Command');


                    disable = function (disable) {

                        blockInsert.set('disabled', disable);

                        setTimeout(function () {
                            blockInsert.getReferences().forEach(function (ref) {
                                ref.set('disabled', disable);
                            });
                        }, 100);

                    };

                    let _disable = item ? false : true;
                    if(_grid.blockGroup === 'conditionalProcess'){
                        _disable=false;
                        newCommand.set('disabled', true);
                        setTimeout(function () {
                            newCommand.getReferences().forEach(function (ref) {
                                ref.set('disabled', true);
                            });
                        }, 100);
                    }

                    disable(_disable);


                    if (item) {
                        blockEnable.getReferences().forEach(function (ref) {
                            ref.set('checked', item.enabled);
                        });
                    }else{
                        /*
                         var props = _grid.getPropertyStruct();
                         props._lastItem = null;
                         _grid.setPropertyStruct(props);*/
                    }
                });
            }

            _completeGrid(grid);

            this.add(grid,null,false);
        },
        createGrid:function(tab,scope,store,group,newItemLabel,options){
            const _docker = this._docker;
            const widget = this;
            let grid;
            const gridClass = this.getGridClass();
            const grids = this.grids;


            const args = utils.mixin({
                /*
                __getRight:function(){
                    return widget.__right;
                },
                */
                ctx:this.ctx,
                blockScope: scope,
                toolbarInitiallyHidden:true,
                blockGroup: group,
                attachDirect:true,
                collection: store.filter({
                    group: group
                }),
                //dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                //____right:this.__right,
                //__docker:_docker,
                setPanelSplitPosition:widget.setPanelSplitPosition,
                getPanelSplitPosition:widget.getPanelSplitPosition
            },options);

            try {
                grid = utils.addWidget(gridClass, args, null, tab, false);
                this.completeGrid(grid, newItemLabel);
                grids.push(grid);
            }catch(e){
                logError(e);
            }
            tab.add(grid,null,false);
            return grid;
        },
        getGridClass:function(){

            if(this.gridClass){
                //return this.gridClass;
            }

            const propertyStruct = {
                currentCIView:null,
                targetTop:null,
                _lastItem:null
            };

            const gridClass = declare('driverGrid', BlockGrid,{
                toolbarInitiallyHidden:true,
                highlightDelay:1000,
                propertyStruct : propertyStruct,
                /**
                 * Step/Run action
                 * @param block {block[]}
                 * @returns {boolean}
                 */
                _execute: function (blocks) {
                    const thiz = this;


                    function _clear(element){

                        if(element) {
                            setTimeout(function () {
                                element.removeClass('failedBlock successBlock activeBlock');
                            }, thiz.highlightDelay);
                        }
                    }

                    function _node(block){
                        if(block) {
                            const row = thiz.row(block);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    }

                    function mark(element,cssClass){

                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                        }
                    }

                    const dfds = [];
                    const EVENTS = types.EVENTS;

                    const _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                        mark(_node(evt),'activeBlock');

                    });
                    const _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        //console.log('mark success');
                        mark(_node(evt),'successBlock');
                        _clear(_node(evt));
                    });

                    const _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        mark(_node(evt),'failedBlock');
                        _clear(_node(evt));

                    });




                    function run(block) {

                        if (!block || !block.scope) {
                            console.error('have no scope');
                            return;
                        }
                        try {

                            const blockDfd = block.scope.solveBlock(block, {
                                highlight: true,
                                force: true,
                                listener:thiz
                            });

                            dfds.push(blockDfd);

                            /*
                             blockDfd.then(function(result){
                             console.log('did run! : ' + result);
                             });
                             */
                            //console.log('run block result:',result);


                        } catch (e)
                        {

                            console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                            //console.error(printStackTrace().join('\n\n'));
                        }
                        return true;
                    }

                    blocks  = _.isArray(blocks) ? blocks : [blocks];


                    function _patch(block){


                        block.runFrom=function(_blocks, index, settings)
                        {
                            const thiz = this;
                            const blocks = _blocks || this.items;
                            const allDfds = [];

                            const onFinishBlock = function (block, results) {
                                block._lastResult = block._lastResult || results;
                                thiz._currentIndex++;
                                thiz.runFrom(blocks, thiz._currentIndex, settings);
                            };


                            const wireBlock = function (block) {
                                block._deferredObject.then(function (results) {
                                    console.log('----def block finish');
                                    onFinishBlock(block, results);
                                });
                            };

                            if (blocks.length) {

                                for (let n = index; n < blocks.length; n++) {



                                    const block = blocks[n];

                                    console.log('run child \n'+block.method);



                                    /*
                                     _patch(block);
                                     var blockDfd = block.solve(this.scope, settings);
                                     allDfds.push(blockDfd);
                                     */
                                    _patch(block);

                                    if (block.deferred === true) {
                                        block._deferredObject = new Deferred();
                                        this._currentIndex = n;
                                        wireBlock(block);
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));
                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                        break;
                                    } else {
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));

                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                    }

                                }

                            } else {
                                this.onSuccess(this, settings);
                            }

                            return allDfds;
                        };

                        block.solve=function(scope,settings,run,error){
                            this._currentIndex = 0;
                            this._return=[];

                            const _script = '' + this._get('method');
                            const thiz=this;
                            const ctx = this.getContext();
                            const items = this[this._getContainer()];

                            const //outer,head dfd
                            dfd = new Deferred;

                            const listener = settings.listener;
                            const isDfd = thiz.deferred;



                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                            }

                            function _finish(result){

                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }

                                dfd.resolve(result);


                            }

                            function _error(result){
                                dfd.reject(result);
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                                }
                            }


                            function _headDone(result){
                                //console.log('_headDone : ',result);
                                //more blocks?
                                if(items.length) {
                                    const subDfds = thiz.runFrom(items,0,settings);
                                    all(subDfds).then(function(what){
                                        console.log('all solved!',what);
                                        _finish(result);
                                    },function(err){

                                        console.error('error in chain',err);
                                        if(listener) {
                                            listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                        }
                                        dfd.resolve(err);
                                    });

                                }else{
                                    if(listener) {
                                        listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                    }
                                    dfd.resolve(result);
                                }
                            }




                            if(_script && _script.length){
                                const runScript = function() {


                                    const _function = new Function("{" + _script + "}");
                                    const _args = thiz.getArgs() || [];
                                    try {

                                        if(isDfd){

                                            ctx.resolve=function(result){
                                                console.log('def block done');
                                                if(thiz._deferredObject) {
                                                    thiz._deferredObject.resolve();
                                                }
                                                _headDone(result);
                                            }
                                        }
                                        const _parsed = _function.apply(ctx, _args || {});
                                        thiz._lastResult = _parsed;
                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }

                                        if(!isDfd) {
                                            _headDone(_parsed);
                                        }

                                        if (_parsed !== 'false' && _parsed !== false) {

                                        } else {
                                            //thiz.onFailed(thiz, settings);
                                            //return [];
                                        }
                                    } catch (e) {

                                        e=e ||{};

                                        _error(e);

                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                };

                                if(scope.global){
                                    (function() {

                                        window = scope.global;


                                        const _args = thiz.getArgs() || [];
                                        try {
                                            let _parsed = null;
                                            if(!ctx.runExpression) {
                                                const _function = new Function("{" + _script + "}").bind(this);
                                                _parsed = _function.apply(ctx, _args || {});
                                            }else{
                                                _parsed = ctx.runExpression(_script,null,_args);
                                            }

                                            thiz._lastResult = _parsed;

                                            if (run) {
                                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                                            }
                                            if (_parsed !== 'false' && _parsed !== false) {
                                                thiz.onSuccess(thiz, settings);
                                            } else {
                                                thiz.onFailed(thiz, settings);
                                                return [];
                                            }


                                        } catch (e) {

                                            thiz._lastResult = null;
                                            if (error) {
                                                error('invalid expression : \n' + _script + ': ' + e);
                                            }
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }

                                    }).call(scope.global);

                                }else{
                                    runScript();
                                }

                            }else{
                                console.error('have no script');
                            }

                            return dfd;
                        }

                    }

                    _.each(blocks,_patch);

                    _.each(blocks,run);

                    all(dfds).then(function(){
                        console.log('did run all selected blocks!',thiz);
                        _runHandle.remove();
                        _successHandle.remove();
                        _errHandle.remove();
                    });

                    /*
                     function run(block) {
                     if (!block || !block.scope) {
                     console.error('have no scope');
                     return;
                     }
                     try {
                     var result = block.scope.solveBlock(block, {
                     highlight: true,
                     force: true
                     });
                     } catch (e) {
                     console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                     console.error(printStackTrace().join('\n\n'));
                     }
                     return true;
                     }

                     blocks  = _.isArray(blocks) ? blocks : [blocks];


                     _.each(blocks,run);
                     */
                },
                execute: function (_blocks) {
                    const thiz = this;

                    console.error('exe');

                    ////////////////////////////////////////////////////////////////////////
                    //
                    //  Visual helpers to indicate run status:
                    //
                    function _clear(element){
                        if(element) {
                            setTimeout(function () {
                                element.removeClass('failedBlock successBlock activeBlock');
                            }, thiz.highlightDelay);
                        }
                    }

                    function _node(block){
                        if(block) {
                            const row = thiz.row(block);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    }

                    function mark(element,cssClass){

                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                        }
                    }

                    const //all Deferreds of selected blocks to run
                    dfds = [];

                    const //shortcut
                    EVENTS = types.EVENTS;

                    const //normalize selection to array
                    blocks  = _.isArray(_blocks) ? _blocks : [_blocks];

                    const //event handle "Run"
                    _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                        mark(_node(evt),'activeBlock');
                    });

                    const //event handle "Success"
                    _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        //console.log('marke success',evt);
                        mark(_node(evt),'successBlock');
                        _clear(_node(evt));
                    });

                    const //event handle "Error"
                    _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        mark(_node(evt),'failedBlock');
                        _clear(_node(evt));
                    });






                    function run(block) {

                        if (!block || !block.scope) {
                            console.error('have no scope');
                            return;
                        }
                        try {

                            const blockDfd = block.scope.solveBlock(block, {
                                highlight: true,
                                force: true,
                                listener:thiz
                            });


                            dfds.push(blockDfd);

                            /*
                             blockDfd.then(function(result){
                             console.log('did run! : ' + result);
                             });
                             */
                            //console.log('run block result:',result);


                        } catch (e)
                        {
                            console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);

                            logError(e,'excecuting block -  ' + block.name + ' failed! : ');

                            //console.error(printStackTrace().join('\n\n'));

                        }
                        return true;
                    }



                    function _patch(block){


                        block.runFrom=function(_blocks, index, settings){
                            const thiz = this;
                            const blocks = _blocks || this.items;
                            const allDfds = [];

                            const onFinishBlock = function (block, results) {
                                block._lastResult = block._lastResult || results;
                                thiz._currentIndex++;
                                thiz.runFrom(blocks, thiz._currentIndex, settings);
                            };

                            const wireBlock = function (block) {
                                block._deferredObject.then(function (results) {
                                    console.log('----def block finish');
                                    onFinishBlock(block, results);
                                });
                            };

                            if (blocks.length) {

                                for (let n = index; n < blocks.length; n++) {



                                    const block = blocks[n];

                                    console.log('run child \n'+block.method);

                                    _patch(block);

                                    if (block.deferred === true) {
                                        block._deferredObject = new Deferred();
                                        this._currentIndex = n;
                                        wireBlock(block);
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));
                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                        break;
                                    } else {
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));

                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                    }

                                }

                            } else {
                                this.onSuccess(this, settings);
                            }

                            return allDfds;
                        };

                        block.solve=function(scope,settings,run,error){
                            this._currentIndex = 0;
                            this._return=[];

                            const _script = '' + this._get('method');

                            const thiz=this;
                            const ctx = this.getContext();
                            const items = this[this._getContainer()];

                            const //outer,head dfd
                            dfd = new Deferred;

                            const listener = settings.listener;
                            const isDfd = thiz.deferred;



                            //moved to Contains#onRunThis
                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                            }

                            //function when a block did run successfully,
                            // moved to Contains#onDidRunItem
                            function _finish(dfd,result,event){

                                if(listener) {
                                    listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }
                                dfd.resolve(result);


                            }

                            //function when a block did run successfully
                            function _error(result){
                                dfd.reject(result);
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                                }
                            }


                            //moved to Contains#onDidRunThis
                            function _headDone(result){


                                //more blocks?
                                if(items.length) {
                                    const subDfds = thiz.runFrom(items,0,settings);

                                    all(subDfds).then(function(what){
                                        console.log('all solved!',what);
                                        _finish(dfd,result);
                                    },function(err){
                                        console.error('error in chain',err);
                                        _finish(dfd,err);
                                    });

                                }else{
                                    _finish(dfd,result);
                                }
                            }


                            if(_script && _script.length){

                                const runScript = function() {

                                    const _function = new Function("{" + _script + "}");
                                    const _args = thiz.getArgs() || [];
                                    try {

                                        if(isDfd){

                                            ctx.resolve=function(result){
                                                console.log('def block done');
                                                if(thiz._deferredObject) {
                                                    thiz._deferredObject.resolve();
                                                }
                                                _headDone(result);
                                            }
                                        }
                                        const _parsed = _function.apply(ctx, _args || {});
                                        thiz._lastResult = _parsed;
                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }


                                        if(!isDfd) {
                                            console.log('root block done');
                                            //_headDone(_parsed);
                                            thiz.onDidRunThis(dfd,_parsed,items,settings);
                                        }

                                        if (_parsed !== 'false' && _parsed !== false) {

                                        } else {
                                            //thiz.onFailed(thiz, settings);
                                            //return [];
                                        }
                                    } catch (e) {

                                        e=e ||{};

                                        _error(e);

                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                };

                                if(scope.global){
                                    (function() {

                                        window = scope.global;


                                        const _args = thiz.getArgs() || [];
                                        try {
                                            let _parsed = null;
                                            if(!ctx.runExpression) {
                                                const _function = new Function("{" + _script + "}").bind(this);
                                                _parsed = _function.apply(ctx, _args || {});
                                            }else{
                                                _parsed = ctx.runExpression(_script,null,_args);
                                            }

                                            thiz._lastResult = _parsed;

                                            if (run) {
                                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                                            }
                                            if (_parsed !== 'false' && _parsed !== false) {
                                                thiz.onSuccess(thiz, settings);
                                            } else {
                                                thiz.onFailed(thiz, settings);
                                                return [];
                                            }


                                        } catch (e) {

                                            thiz._lastResult = null;
                                            if (error) {
                                                error('invalid expression : \n' + _script + ': ' + e);
                                            }
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }

                                    }).call(scope.global);

                                }else{
                                    runScript();
                                }

                            }else{
                                console.error('have no script');
                            }
                            return dfd;
                        }


                    }

                    //_.each(blocks,_patch);

                    _.each(blocks,run);

                    all(dfds).then(function(){
                        console.log('did run all selected blocks!',thiz);
                        _runHandle.remove();
                        _successHandle.remove();
                        _errHandle.remove();
                    });
                },
                onCIChanged: function (ci, block, oldValue, newValue, field) {

                    console.log('on ci changed', arguments);
                    const _col= this.collection;
                    block = this.collection.getSync(block.id);
                    block.set(field, newValue);
                    block[field]=newValue;
                    _col.refreshItem(block);
                },
                _itemChanged: function (type, item, store) {

                    store = store || this.getStore(item);

                    const thiz = this;

                    function _refreshParent(item, silent) {

                        const parent = item.getParent();
                        if (parent) {
                            const args = {
                                target: parent
                            };
                            if (silent) {
                                this._muteSelectionEvents = true;
                            }
                            store.emit('update', args);
                            if (silent) {
                                this._muteSelectionEvents = false;
                            }
                        } else {
                            thiz.refresh();
                        }
                    }

                    function select(item) {

                        thiz.select(item, null, true, {
                            focus: true,
                            delay: 20,
                            append: false
                        });
                    }

                    switch (type) {

                        case 'added':
                        {
                            //_refreshParent(item);
                            //this.deselectAll();
                            this.refresh();
                            select(item);
                            break;
                        }

                        case 'changed':
                        {
                            this.refresh();
                            select(item);
                            break;
                        }


                        case 'moved':
                        {
                            //_refreshParent(item,true);
                            //this.refresh();
                            //select(item);
                            break;
                        }

                        case 'deleted':
                        {

                            const parent = item.getParent();
                            //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                            const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                            if (parent) {
                                const _container = parent.getContainer();
                                if (_container) {
                                    _.each(_container, function (child) {
                                        if (child.id == item.id) {
                                            _container.remove(child);
                                        }
                                    });
                                }
                            }

                            this.refresh();
                            /*
                             if (_prev) {
                             select(_prev);
                             }
                             */
                            break;
                        }

                    }


                },
                _onFocusChanged:function(focused,type){
                    this.inherited(arguments);
                    if(!focused){
                        this._lastSelection = [];
                    }

                },
                save:function(){
                    const thiz = this;
                    const driver = thiz.userData.driver;
                    const ctx = thiz.userData.ctx;
                    const fileManager = ctx.getFileManager();

                    const //instance scope
                    scope = thiz.blockScope;

                    const instance = scope.instance;

                    const //original driver scope
                    originalScope = driver.blockScope;

                    const path = driver.path.replace('.meta.json','.xblox');
                    const scopeToSave = originalScope || scope;
                    const mount = driver.scope;



                    if(originalScope && scopeToSave!=originalScope){
                        originalScope.fromScope(scope);
                    }

                    if (scope) {

                        const all = {
                            blocks: null,
                            variables: null
                        };

                        const blocks = scope.blocksToJson();
                        try {
                            //test integrity
                            dojo.utils(JSON.stringify(blocks));
                        } catch (e) {
                            console.error('invalid data');
                            return;
                        }

                        const _onSaved = function () {};

                        all.blocks = blocks;

                        console.log('saving driver ' + mount + '/'+path,driver);


                        fileManager.setContent(mount,path,JSON.stringify(all, null, 2),_onSaved);
                        //this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
                    }
                },
                runAction: function (action) {

                    const thiz = this;
                    const sel = this.getSelection();



                    function addItem(_class,group){

                        const cmd = factory.createBlock(_class, {
                            name: "No Title",
                            send: "nada",
                            scope: thiz.blockScope,
                            group: group
                        });


                        thiz.deselectAll();
                        _.each(thiz.grids,function(grid){
                            if(grid) {
                                grid.refresh();
                            }
                        });
                        setTimeout(function () {
                            thiz.select([cmd],null,true,{
                                focus:true
                            });
                        }, 200);

                    }
                    if (action.command == 'New/Command') {
                        addItem(Command,this.blockGroup);
                    }

                    if (action.command == 'New/Variable') {
                        addItem(Variable,'basicVariables');
                    }

                    if (action.command == 'File/Save') {
                        this.save();
                    }
                    return this.inherited(arguments);
                },
                startup:function(){

                    const thiz = this;

                    this.inherited(arguments);

                    function _node(evt){
                        const item = evt.target;
                        if(item) {
                            const row = thiz.row(item);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    };

                    function mark(element,cssClass){
                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                            setTimeout(function () {
                                element.removeClass(cssClass);
                                thiz._isHighLighting = false;
                            }, thiz.highlightDelay);
                        }
                    };

                    this.subscribe(types.EVENTS.ON_RUN_BLOCK,function(evt){
                        mark(_node(evt),'activeBlock');
                    });
                    this.subscribe(types.EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        mark(_node(evt),'failedBlock');
                    });
                    this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        mark(_node(evt),'successBlock');
                    });


                    this.collection.on('update',function(evt){
                        const item = evt.target;
                        const node = _node(evt);
                        const type = evt.type;

                        if(type==='update' && evt.property ==='value'){
                            mark(node,'successBlock');
                        }

                        //console.warn('on store updated ', args);
                    });


                }
            });

            this.gridClass = gridClass;

            return gridClass;
        },
        createConsole:function(tab){
            const deviceManager = this.ctx.getDeviceManager();
            return openConsole.apply(deviceManager,[tab,this.driver,this.device]);

        },
        startup:function(){
            const self = this;
            const ci = self.userData;
            const device = self.device;
            const driver = self.driver;

            const //original or device instance
            scope = device ? device.blockScope : driver.blockScope;

            if(!scope){
                console.error('have no scope!');
                return ;
            }

            const store = scope.blockStore;
            const instance = driver.instance;




            const _docker = this.createLayout();

            this._docker = _docker;

            //this.__right = this.getRightPanel('Properties',1);

            /*
                        this.__rightBottom = this.createTab(null,{
                            title:'Description',
                            tabOrientation:types.DOCKER.TAB.BOTTOM,
                            target:this.__right
                        });
            */




            this.grids = [];

            this.basicCommandsGrid = this.createGrid(this.basicCommandsTab,scope,store,'basic','Command');
            this.conditionalCommandsGrid = this.createGrid(this.conditionalTab,scope,store,'conditional','Command');
            this.variablesGrid = this.createGrid(this.variablesTab,scope,store,'basicVariables','Variable',{
                columns:[
                    {
                        label: "Name",
                        field: "name",
                        sortable: true,
                        width:'20%',
                        editorArgs: {
                            required: true,
                            promptMessage: "Enter a unique variable name",
                            //validator: thiz.variableNameValidator,
                            //delegate: thiz.delegate,
                            intermediateChanges: false
                        }
                        //editor: TextBox,
                        //editOn:'click',
                        //_editor: ValidationTextBox
                    },
                    {
                        label: "Initialize",
                        field: "initial",
                        sortable: false,
                        _editor: TextBox,
                        editOn:'click'
                    },
                    {
                        label: "Value",
                        field: "value",
                        sortable: false,
                        _editor: TextBox,
                        editOn:'click',
                        formatter:function(value,object){
                            //console.log('format var ' + value,object);
                            return value;
                        },
                        editorArgs: {
                            autocomplete:'on',
                            templateString:'<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"'+
                            '><div class="dijitReset dijitInputField dijitInputContainer"'+
                            '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"'+
                            '${!nameAttrSetting} type="${type}"'+
                            '/></div'+
                            '></div>'
                        }
                    }
                ]
            });

            //debugger;
            this.basicCommandsTab.select();
            _docker.resize();

            const _console = this.createConsole(this.consoleTab);

            this.add(_console,null,false);


            /*
            this.basicCommandsGrid.startup();

            this.basicCommandsGrid.set('collection',store.filter({group: 'basic'}));

            */
        }
    });


    const ConsoleViewImplementation = ConsoleView.Implementation;

    const _ExpressionEditor = dcl([WidgetBase,_LayoutMixin.dcl,ConsoleView.HandlerClass],{
        type:'javascript',
        reparent:false,
        consoleTabTitle:'Editor',
        consoleTabOptions:null,
        consoleTabType:'DefaultTab',
        templateString: '<div class="" style="height: inherit">' +
        '<div attachTo="consoleParent" class="" style="height:inherit;padding: 0;margin: 0;overflow-y: auto"></div></div>',
        onAddEditorActions:function(evt){
            console.error('onAddEditorActions',arguments);


            //this.inherited(arguments);

            const actions  = evt.actions;

            const owner  = evt.owner;

            const mixin = {
                addPermission:true
            };
            actions.push(owner.createAction({
                label: 'Send',
                command: 'Console/Send',
                icon: 'fa-paper-plane',
                group: 'Console',
                tab:'Home',
                mixin:mixin,
                handler:function(){
                    debugger;
                }
            }));
        },
        getConsoleClass:function(){
            return ConsoleView.ConsoleWidget;
        },
        startup:function(){


            const thiz = this;


            this.EditorClass = dcl(ConsoleView.Editor,{
                runAction:function(action){


                    console.error('run editor action',action.command);


                    return this.inherited(arguments);

                }
            });

            const storeDelegate = {
                saveContent:function(value){
                    thiz.setValue(value);
                }
            };

            this.editorArgs  = {
                options:{
                    showGutter: false,
                    wordWrap: true
                },
                storeDelegate:storeDelegate
            };


            const consoleTarget = this.consoleParent;

            const userData = this.userData;

            const value = userData.value;

            const _console = utils.addWidget(this.getConsoleClass(), {
                style: 'width:inherit',
                delegate: this,
                type: this.type,
                value: value || this.value,
                ctx:this.ctx

            }, this, consoleTarget, true);


            this.add(_console, null, false);

            this.console = _console;


            return _console;
        }
    });

    //set global
    //types.registerType(types.ECIType.EXPRESSION_EDITOR,ExpressionEditor);
    //types.registerType('xide.widgets.ExpressionEditor',ExpressionEditor);










    function createGridClass(overrides) {

        const propertyStruct = {
            currentCIView:null,
            targetTop:null,
            _lastItem:null
        };


        const gridClass = declare('driverGrid', BlockGrid,{
            _console:null,
            _lastConsoleItem:null,
            toolbarInitiallyHidden:true,
            highlightDelay:2000,
            propertyStruct : propertyStruct,
            onShowProperties:function(item){
                const _console = this._console;
                if(_console && item){

                    const value = item.send || item.method;
                    const editor = _console.getConsoleEditor();

                    editor.set('value',value);
                    this._lastConsoleItem = item;
                }
            },
            /**
             *
             * @param action {Action}
             * @param editor {ACEEditor}
             */
            openTerminal:function(action,editor){

                const item = this.getSelectedItem() || {};

                // already open
                if(this._console){

                    utils.destroy(this._console);
                    utils.destroy(this.__bottom);
                    this.__bottom = null;
                    this._console = null;
                    this.getDocker().resize();
                    //this.getDocker().removePanel(this._console._parent);
                    return;
                }

                const ExpressionEditor = editor.delegate; // xide.widgets.ExpressionEditor

                const ctx = this.ctx;
                const deviceManager = ctx.getDeviceManager();
                const right = this.getRightPanel('Properties',null,'DefaultTab',{});
                const bottomPanel = this.getBottomPanel('Console',null,'DefaultTab',null,false);
                const value = item.send || item.method;
                const _console = deviceManager.openConsole(this.device,bottomPanel,{
                    value:value
                });
                this._lastConsoleItem = item;
                bottomPanel.closeable(false);
                this._console = _console;
            },
            getTypeMap:function(){

                if(this.typeMap){
                    return this.typeMap;
                }

                const self = this;

                const ExpressionEditorExtra = dcl(ExpressionEditor,{
                    EditorClass : dcl(ConsoleView.Editor,{
                        runAction:function(action){

                            console.error('run editor action',action.command);
                            if(action.command ==='Console/Terminal'){
                                return self.openTerminal(action,this);
                            }

                            return this.inherited(arguments);

                        }
                    }),
                    onAddEditorActions:function(evt) {
                        const actions  = evt.actions;
                        const owner  = evt.owner;

                        const mixin = {
                            addPermission:true
                        };
                        actions.push(owner.createAction({
                            label: 'Send',
                            command: 'Console/Terminal',
                            icon: 'fa-terminal',
                            group: 'Console',
                            tab:'Home',
                            mixin:mixin,
                            handler:function(){

                            }
                        }));
                    }
                });

                //noinspection UnterminatedStatementJS,UnterminatedStatementJS
                const typeMap = {};

                typeMap['xide.widgets.ExpressionEditor'] = ExpressionEditorExtra;


                this.typeMap = typeMap;



            },
            showProperties: function (item,force) {
                const dfd = new Deferred();
                const block = item || this.getSelection()[0];
                const thiz = this;
                const rightSplitPosition= thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);

                if(!block || rightSplitPosition==1) {
                    console.log(' show properties: abort',[block , rightSplitPosition]);
                    return;
                }


                const right = this.getRightPanel('Properties',null,'DefaultTab',{});

                right.closeable(false);

                let props = this.getPropertyStruct();

                if (block == props._lastItem && force!==true) {
                    //console.log('show properties : same item');
                    return;
                }

                this.clearPropertyPanel();

                props = this.getPropertyStruct();


                props._lastItem = item;


                const _title = block.name || block.title;


                //console.log('show properties for ' + _title+ ' : ' + item.declaredClass,props);

                function propertyTabShown(tab,show){
                    const title = tab.title;

                    if(show) {
                        props.lastSelectedTopTabTitle = title;

                    }else if (!show && props.lastSelectedTopTabTitle===title){
                        props.lastSelectedTopTabTitle = null;
                    }
                }


                let tabContainer = props.targetTop;
                if (!tabContainer) {
                    tabContainer = utils.addWidget(_Accordion, {

                    }, null, right.containerNode, true);

                    $(tabContainer.domNode).addClass('CIView Accordion');

                    props.targetTop = tabContainer;

                    tabContainer._on('show',function(evt){
                        propertyTabShown(evt.view,true)
                    });

                    tabContainer._on('hide',function(evt){
                        propertyTabShown(evt.view,false);
                    });

                }

                _.each(tabContainer.getChildren(), function (tab) {
                    tabContainer.removeChild(tab);
                });

                if (props.currentCIView) {
                    props.currentCIView.empty();
                }

                if (!block.getFields) {
                    console.log('have no fields', block);
                    return;
                }

                const cis = block.getFields();
                for (let i = 0; i < cis.length; i++) {
                    cis[i].vertical = true;
                }


                const ciView = new CIViewMixin({
                    typeMap:thiz.getTypeMap(),
                    tabContainerClass:_Accordion,
                    tabContainer: props.targetTop,
                    delegate: this,
                    viewStyle: 'padding:0px;',
                    autoSelectLast: true,
                    item: block,
                    source: this.callee,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Advanced': 2,
                            'Script':3,
                            'Arguments':4,
                            'Description':5,
                            'Share':6

                        }
                    },
                    cis: cis
                });


                ciView.initWithCIS(cis);
                props.currentCIView = ciView;

                if (block.onFieldsRendered) {
                    block.onFieldsRendered(block, cis);
                }



                ciView._on('valueChanged', function (evt) {

                    console.log('ci value changed ', evt);

                    setTimeout(function(){
                        thiz.onCIChanged && thiz.onCIChanged(evt.ci,block,evt.oldValue,evt.newValue,evt.ci.dst);
                    },10);


                });

                const containers = props.targetTop.getChildren();
                const descriptionView = null;

                const tabToOpen = tabContainer.getTab(props.lastSelectedTopTabTitle);
                if(tabToOpen){
                    tabContainer.selectChild(props.lastSelectedTopTabTitle,true);
                }else{
                    props.lastSelectedTopTabTitle= null;
                }

                this.setPropertyStruct(props);

                this.onShowProperties(block);
            },

            /**
             * Step/Run action
             * @param block {block[]}
             * @returns {boolean}
             */
            execute: function (_blocks) {
                const thiz = this;
                ////////////////////////////////////////////////////////////////////////
                //
                //  Visual helpers to indicate run status:
                //
                function _clear(element){
                    if(element) {
                        setTimeout(function () {
                            element.removeClass('failedBlock successBlock activeBlock');
                        }, thiz.highlightDelay);
                    }
                }

                function _node(block){

                    if(block && block.target){
                        block = block.target;
                    }


                    if(block) {
                        const row = thiz.row(block);
                        if (row) {
                            const element = row.element;
                            if (element) {
                                return $(element);
                            }else{
                                console.error('cant find element for block',block);
                            }
                        }
                    }else{
                        console.error('no block');
                    }
                    return null;
                }

                function mark(element,cssClass){

                    if(element) {
                        console.error('mark : ' + cssClass,element);
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.addClass(cssClass);
                    }else{
                        console.error('have no element for ');
                    }
                }

                const //all Deferreds of selected blocks to run
                dfds = [];

                let handles = [];

                const //shortcut
                EVENTS = types.EVENTS;

                const //normalize selection to array
                blocks  = _.isArray(_blocks) ? _blocks : [_blocks];

                /*
                //event handle "Run"
                    _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                        console.error('active');
                        mark(_node(evt),'activeBlock');
                    }),

                //event handle "Success"
                    _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        //console.log('marke success',evt);
                        console.error('success');
                        mark(_node(evt),'successBlock');
                        _clear(_node(evt));
                    }),

                //event handle "Error"
                    _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        console.error('failed');
                        mark(_node(evt),'failedBlock');
                        _clear(_node(evt));
                    });
*/







                function run(block) {
                    const _runHandle = block._on(EVENTS.ON_RUN_BLOCK,function(evt){
                                  console.error('active');
                                  mark(_node(evt),'activeBlock');
                                  _clear(_node(evt));
                              });

                    const //event handle "Success"
                    _successHandle = block._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        console.log('marke success',evt);
                        console.error('success');
                        mark(_node(evt),'successBlock');
                        _clear(_node(evt));
                    });

                    const //event handle "Error"
                    _errHandle = block._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        console.error('failed');
                        mark(_node(evt),'failedBlock');
                        _clear(_node(evt));
                    });


                    console.error('run block ');


                    if (!block || !block.scope) {
                        console.error('have no scope');
                        return;
                    }

                    try {
                        const blockDfd = block.scope.solveBlock(block, {
                            highlight: true,
                            force: true,
                            listener:thiz
                        });

                        dfds.push(blockDfd);

                    } catch (e)
                    {
                        console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);

                        logError(e,'excecuting block -  ' + block.name + ' failed! : ');

                    }

                    handles = handles.concat([_runHandle,_errHandle,_successHandle]);


                    return true;
                }





                function _patch(block){


                    block.runFrom=function(_blocks, index, settings){
                        const thiz = this;
                        const blocks = _blocks || this.items;
                        const allDfds = [];

                        const onFinishBlock = function (block, results) {
                            block._lastResult = block._lastResult || results;
                            thiz._currentIndex++;
                            thiz.runFrom(blocks, thiz._currentIndex, settings);
                        };

                        const wireBlock = function (block) {
                            block._deferredObject.then(function (results) {
                                console.log('----def block finish');
                                onFinishBlock(block, results);
                            });
                        };

                        if (blocks.length) {

                            for (let n = index; n < blocks.length; n++) {



                                const block = blocks[n];

                                console.log('run child \n'+block.method);

                                _patch(block);

                                if (block.deferred === true) {
                                    block._deferredObject = new Deferred();
                                    this._currentIndex = n;
                                    wireBlock(block);
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));
                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                    break;
                                } else {
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));

                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                }

                            }

                        } else {
                            this.onSuccess(this, settings);
                        }

                        return allDfds;
                    };

                    block.solve=function(scope,settings,run,error){
                        this._currentIndex = 0;
                        this._return=[];

                        const _script = '' + this._get('method');

                        const thiz=this;
                        const ctx = this.getContext();
                        const items = this[this._getContainer()];

                        const //outer,head dfd
                        dfd = new Deferred;

                        const listener = settings.listener;
                        const isDfd = thiz.deferred;



                        //moved to Contains#onRunThis
                        if(listener) {
                            listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                        }

                        //function when a block did run successfully,
                        // moved to Contains#onDidRunItem
                        function _finish(dfd,result,event){

                            if(listener) {
                                listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                            }
                            dfd.resolve(result);


                        }

                        //function when a block did run successfully
                        function _error(result){
                            dfd.reject(result);
                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                            }
                        }


                        //moved to Contains#onDidRunThis
                        function _headDone(result){


                            //more blocks?
                            if(items.length) {
                                const subDfds = thiz.runFrom(items,0,settings);

                                all(subDfds).then(function(what){
                                    console.log('all solved!',what);
                                    _finish(dfd,result);
                                },function(err){
                                    console.error('error in chain',err);
                                    _finish(dfd,err);
                                });

                            }else{
                                _finish(dfd,result);
                            }
                        }


                        if(_script && _script.length){

                            const runScript = function() {

                                const _function = new Function("{" + _script + "}");
                                const _args = thiz.getArgs() || [];
                                try {

                                    if(isDfd){

                                        ctx.resolve=function(result){
                                            console.log('def block done');
                                            if(thiz._deferredObject) {
                                                thiz._deferredObject.resolve();
                                            }
                                            _headDone(result);
                                        }
                                    }
                                    const _parsed = _function.apply(ctx, _args || {});
                                    thiz._lastResult = _parsed;
                                    if (run) {
                                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                                    }


                                    if(!isDfd) {
                                        console.log('root block done');
                                        //_headDone(_parsed);
                                        thiz.onDidRunThis(dfd,_parsed,items,settings);
                                    }

                                    if (_parsed !== 'false' && _parsed !== false) {

                                    } else {
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                } catch (e) {

                                    e=e ||{};

                                    _error(e);

                                    if (error) {
                                        error('invalid expression : \n' + _script + ': ' + e);
                                    }
                                    //thiz.onFailed(thiz, settings);
                                    //return [];
                                }
                            };

                            if(scope.global){
                                (function() {

                                    window = scope.global;


                                    const _args = thiz.getArgs() || [];
                                    try {
                                        let _parsed = null;
                                        if(!ctx.runExpression) {
                                            const _function = new Function("{" + _script + "}").bind(this);
                                            _parsed = _function.apply(ctx, _args || {});
                                        }else{
                                            _parsed = ctx.runExpression(_script,null,_args);
                                        }

                                        thiz._lastResult = _parsed;

                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }
                                        if (_parsed !== 'false' && _parsed !== false) {
                                            thiz.onSuccess(thiz, settings);
                                        } else {
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }


                                    } catch (e) {

                                        thiz._lastResult = null;
                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        thiz.onFailed(thiz, settings);
                                        return [];
                                    }

                                }).call(scope.global);

                            }else{
                                runScript();
                            }

                        }else{
                            console.error('have no script');
                        }
                        return dfd;
                    }


                }

                //_.each(blocks,_patch);

                _.each(blocks,run);

                all(dfds).then(function(){

                    console.log('did run all selected blocks!',thiz);

                    _.invoke(handles,'remove');

                    /*
                    _runHandle.remove();
                    _successHandle.remove();
                    _errHandle.remove();
                    */
                });
            },
            onCIChanged: function (ci, block, oldValue, newValue, field) {

                console.log('on ci changed', arguments);

                const _col= block._store;

                block = _col.getSync(block.id);

                block.set(field, newValue);

                block[field]=newValue;

                const col = this.collection;

                this.refresh();
            },
            _itemChanged: function (type, item, store) {

                store = store || this.getStore(item);

                const thiz = this;

                function _refreshParent(item, silent) {

                    const parent = item.getParent();
                    if (parent) {
                        const args = {
                            target: parent
                        };
                        if (silent) {
                            this._muteSelectionEvents = true;
                        }
                        store.emit('update', args);
                        if (silent) {
                            this._muteSelectionEvents = false;
                        }
                    } else {
                        thiz.refresh();
                    }
                }

                function select(item) {

                    thiz.select(item, null, true, {
                        focus: true,
                        delay: 20,
                        append: false
                    });
                }

                switch (type) {

                    case 'added':
                    {
                        //_refreshParent(item);
                        //this.deselectAll();
                        this.refresh();
                        select(item);
                        break;
                    }

                    case 'changed':
                    {
                        this.refresh();
                        select(item);
                        break;
                    }


                    case 'moved':
                    {
                        //_refreshParent(item,true);
                        //this.refresh();
                        //select(item);
                        break;
                    }

                    case 'deleted':
                    {

                        const parent = item.getParent();
                        //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                        const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                        if (parent) {
                            const _container = parent.getContainer();
                            if (_container) {
                                _.each(_container, function (child) {
                                    if (child.id == item.id) {
                                        _container.remove(child);
                                    }
                                });
                            }
                        }

                        this.refresh();
                        /*
                         if (_prev) {
                         select(_prev);
                         }
                         */
                        break;
                    }

                }


            },
            _onFocusChanged:function(focused,type){
                this.inherited(arguments);
                if(!focused){
                    this._lastSelection = [];
                }

            },
            save:function(){
                const thiz = this;
                const driver = thiz.userData.driver;
                const ctx = thiz.userData.ctx;
                const fileManager = ctx.getFileManager();

                const //instance scope
                scope = thiz.blockScope;

                const instance = scope.instance;

                const //original driver scope
                originalScope = driver.blockScope;

                const path = driver.path.replace('.meta.json','.xblox');
                const scopeToSave = originalScope || scope;
                const mount = driver.scope;



                if(originalScope && scopeToSave!=originalScope){
                    originalScope.fromScope(scope);
                }

                if (scope) {

                    const all = {
                        blocks: null,
                        variables: null
                    };

                    const blocks = scope.blocksToJson();
                    try {
                        //test integrity
                        utils.fromJson(JSON.stringify(blocks));
                    } catch (e) {
                        console.error('invalid data');
                        return;
                    }

                    const _onSaved = function () {};

                    all.blocks = blocks;

                    console.log('saving driver ' + mount + '/'+path,driver);


                    fileManager.setContent(mount,path,JSON.stringify(all, null, 2),_onSaved);
                    //this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
                }
            },
            runAction: function (action) {

                const thiz = this;
                const sel = this.getSelection();


                function addItem(_class,group){

                    const cmd = factory.createBlock(_class, {
                        name: "No Title",
                        send: "nada",
                        scope: thiz.blockScope,
                        group: group
                    });


                    thiz.deselectAll();
                    _.each(thiz.grids,function(grid){
                        if(grid) {
                            grid.refresh();
                        }
                    });
                    setTimeout(function () {
                        thiz.select([cmd],null,true,{
                            focus:true
                        });
                    }, 200);

                }
                if (action.command == 'New/Command') {
                    addItem(Command,this.blockGroup);
                }

                if (action.command == 'New/Variable') {
                    addItem(Variable,'basicVariables');
                }

                if (action.command == 'File/Save') {
                    this.save();
                }
                return this.inherited(arguments);
            },
            startup:function(){

                const thiz = this;

                this.inherited(arguments);


                function _node(evt){
                    const item = evt.target;
                    if(item) {
                        const row = thiz.row(item);
                        if (row) {
                            const element = row.element;
                            if (element) {
                                return $(element);
                            }else{
                                console.error('cant find element for : ',item);
                            }
                        }
                    }else{
                        console.error('cant find item');
                    }
                    return null;
                };


                function mark(element,cssClass){
                    console.log('mark ' + cssClass);
                    if(element) {
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.addClass(cssClass);
                        setTimeout(function () {
                            element.removeClass(cssClass);
                            thiz._isHighLighting = false;
                        }, thiz.highlightDelay);
                    }else{
                        console.error('have no element');
                    }
                };




                this.collection.on('update',function(evt){
                    const item = evt.target;
                    const node = _node(evt);
                    const type = evt.type;

                    if(type==='update' && evt.property ==='value'){
                        mark(node,'successBlock');
                    }

                    //console.warn('on store updated ', args);
                });


            }
        });

        return gridClass;
    }

    function doTests(){}

    function onReady(grid){}



    function openBasicBlocks(tab,driver,device){
        //ctx.getAction('Window/Navigation').handler(true);

        const _docker = this._docker;

        const widget = this;
        let grid;
        const gridClass = createGridClass();


        const commandsCI = utils.getCIByChainAndName(driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);

        if(commandsCI){
            commandsCI.device = device;
        }

        const group = 'basic';

        const //original or device instance
        scope = device ? device.blockScope : driver.blockScope;

        const store = scope.blockStore;
        const instance = driver.instance;



        const args = utils.mixin({
            ctx:ctx,
            userData:commandsCI,
            blockScope: scope,
            toolbarInitiallyHidden:true,
            blockGroup: group,
            attachDirect:true,
            collection: store.filter({
                group: group
            }),
            device:device,
            driver:driver
        },options);

        try {
            grid = utils.addWidget(gridClass, args, null, tab, false);
            completeGrid(grid, 'Command');
        }catch(e){
            logError(e);
        }

        tab.add(grid,null,false);


        ctx.getWindowManager().registerView(grid);

        grid.refresh().then(function(){
            grid.select([0]).then(function(){
                grid.showProperties(null,true);
                onReady(grid);
            });

        })
    }

    function openDriverSettings(driver,device){



        const parent = TestUtils.createTab(null,null,module.id);


        const title = 'Marantz Instance';




        let devinfo = null;
        if(device){
            devinfo  = ctx.getDeviceManager().toDeviceControlInfo(device);
        }

        //@Todo:driver, store device temporarly in Commands CI
        const commandsCI = utils.getCIByChainAndName(driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
        if(commandsCI){
            commandsCI.device = device;
        }



        const view = utils.addWidget(CIGroupedSettingsView, {
            style:"width: inherit;height: 100%;",
            title:  'title',
            cis: driver.user.inputs,
            storeItem: driver,
            storeDelegate: this,
            iconClass: 'fa-eye',
            closable: true,
            showAllTab: false,
            blockManager: ctx.getBlockManager(),
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'Settings'
            }


        }, null, parent, true);

        view.resize();
    }

    if (ctx) {

        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();

        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        var marantz = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.20.meta.json");

        const driver = marantz.driver;
        const device = marantz.device;

        const deviceManager = ctx.getDeviceManager();

        const parent = TestUtils.createTab(null,null,module.id);

        //openConsole.apply(deviceManager,[parent,driver,device]);

        openBasicBlocks(parent,driver,device);

        //openDriverView(driver,device);

        return Grid;
    }


    return Grid;
});