/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xaction/DefaultActions',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    'xdocker/Docker2',
    'xblox/views/BlockGrid',
    'xgrid/DnD',
    'xblox/views/BlocksGridDndSource',
    'xblox/widgets/DojoDndMixin',
    'xide/registry',
    'dojo/topic'


], function (declare, types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, DefaultActions, Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Docker,BlockGrid,
             Dnd,BlocksGridDndSource,DojoDndMixin,

             registry,topic
    ) {
    function patchToolbar(ribbon) {

        const _mixin = {

        };

        utils.mixin(ribbon,_mixin);
    }
    /**
     *
     */
    const _layoutMixin = {
        _docker:null,
        _parent:null,
        __right:null,
        getDocker:function(container){

            if(!this._docker){
                const _dst = container || this._domNode.parentNode;
                var thiz = this;

                thiz._docker = Docker.createDefault(_dst);
                thiz._oldParent = thiz._parent;

                const parent = thiz._docker.addPanel('DefaultFixed', types.DOCKER.TOP, null, {
                    w: '100%',
                    title:'  '
                });

                dojo.place(thiz._domNode,parent.containerNode);

                thiz._docker.$container.css('top',0);
                thiz._parent = parent;

                parent._parent.showTitlebar(false);


                /*
                                var right = this._docker.addPanel('Collapsible', types.DOCKER.RIGHT, parent, {
                                    w: '20%',
                                    title:'Properties'
                                });

                                parent._parent.showTitlebar(false);*/





                //parent._parent.$center.css('top',0);
            }

            return thiz._docker;
        },
        getRightPanel:function(){

            if(this.__right){
                return this.__right;
            }

            const docker = this.getDocker();


            const right = docker.addPanel('Collapsible', types.DOCKER.RIGHT, this._parent, {
                w: '300px',
                title:'  '
            });



            right._parent.showTitlebar(false);

            const splitter = right.getSplitter();

            splitter.pos(0.6);


            this.__right = right;

            return right;
        }
    };


    const _statusMixin = {
    };

    /**
     * Dnd impl.
     */
    const _dndMixin = {

    };


    var DojoDndMixin  = declare("xblox.widgets.DojoDndMixin",null,{
        dropEvent:"/dnd/drop",
        dragEvent:"/dnd/start",
        overEvent:"/dnd/source/over",
        _eDrop:false,
        isDragging:false,
        didInit:false,
        /***
         * Pre-Process DND Events
         * @param node
         * @param targetArea
         * @param indexChild
         */
        _calcNewItemList:function(items){
            const res = [];
            if(items){

                for(let i=0 ; i<items.length ; i++){
                    const widget =registry.getEnclosingWidget(items[i].item.node);
                    if(widget){
                        widget.item.dndCurrentIndex=i;
                        res.push(widget);
                    }
                }
            }
            return res;
        },
        _onOver:function(node, source){

            if(this.isDragging /*this.onOver && node*/){
                let widget=null;
                if(source){
                    widget = registry.getEnclosingWidget(source[0]);
                }
                console.log('on over : ' );
                /*this.onOver(widget,node);*/
            }

        },
        _onDrag:function(node, source){

            this.isDragging=true;
            /*
             if(this.onDrag){
             if(source && source[0]){
             var widget = registry.getEnclosingWidget(source[0]);
             this.onDrag(widget,node);
             }
             }
             */
        },
        checkAcceptance:function(sources,nodes){

            return false;
            if(!sources||!nodes){
                return;
            }
            this.setupDND();

            const node = nodes[0];

            const row = this.grid.row(node);
            if(!row || !row.data){
                return;
            }
            const item = row.data;

            //console.log('acce : ' + item.name);

            return true;
        },
        onDrop: function(source, nodes, copy,target){

            if(!source||!nodes){
                return;
            }
            this.isDragging=false;
            const node = nodes[0];
            let item = null;
            let dstItem = null;
            let isTree = false;
            let didNonTree=false;
            let didTree=false;
            const grid = this.grid;
            let itemToFocus;

            if(source.tree && source.anchor && source.anchor.item && target){

                item    = source.anchor.item;
                isTree  = true;

                var dstNode = target.current;
                if(!dstNode){

                    console.warn('have dstNode in target.current');

                    if (grid.onDrop) {
                        console.log('drop on nothing');
                        grid.onDrop(item, {
                            parentId:null
                        }, false, grid, source.targetState, false);
                    }

                    return;
                }
                var dstRow = grid.row(dstNode);
                if(!dstRow){
                    console.warn('have now row');
                    return;
                }
                dstItem = dstRow.data;
                if(!dstItem){
                    console.warn('have no dstItem');
                    return;
                }

                try {

                    if (grid.onDrop) {
                        itemToFocus = item;
                        grid.onDrop(item, dstItem, target.before, grid, source.targetState, target.hover);
                        didTree = true;
                    }
                }catch(e){
                    debugger;
                }



            }else{






                const row=grid.row(node);

                if(!row || !row.data){
                    return;
                }
                item = row.data;



                var dstNode = source.current;
                if(!dstNode){
                    console.warn('have no dstNode in source.current, unparent');
                    const parent = item.getParent();

                    if(parent) {
                        item.unparent(grid.blockGroup, false);
                    }
                    grid.refreshAll([item]);

                    return;
                }
                var dstRow = this.grid.row(dstNode);
                if(!dstRow){
                    console.warn('have now row');
                    return;
                }
                dstItem = dstRow.data;
                if(!dstItem){
                    console.warn('have no dstItem');
                    return;
                }

                try {
                    if (grid.onDrop) {
                        itemToFocus = item;
                        grid.onDrop(item, dstItem, source.before, grid, source.sourceState, source.hover);
                        didNonTree = true;
                    }
                }catch(e){
                    console.error('error grid:onDrop:',e);
                    console.trace();
                }
            }

            if(didNonTree==false && didTree==false){
                console.dir(arguments);
                console.log('something wrong')
            }else{
                console.dir(arguments);
                console.log('dropping : ' + item.name  + ' into ' + dstItem.name + ' before = ' + source.before);
                console.log('didNonTree ' + didNonTree + ' didTree ' + didTree);
                grid.refreshAll([itemToFocus]);

            }
        },
        setupDND:function(){


            const thiz = this;

            if(this.didInit){
                return;
            }
            this.didInit=true;

            topic.subscribe(this.dragEvent, function(node, targetArea, indexChild){
                thiz._onDrag(node, targetArea, indexChild);
            });
            if(this.overEvent){
                topic.subscribe(this.overEvent, function(node, targetArea, indexChild){
                    thiz._onOver(node, targetArea, indexChild);
                });
            }
            topic.subscribe(this.dropEvent, function(source, nodes, copy, target){
                thiz._onDrop(source, nodes, copy, target);
            });
        }
    });

    const SharedDndGridSource = declare([BlocksGridDndSource, DojoDndMixin], {
        /*
        blockScope: this.blockScope,
        delegate: this.delegate,*/
        onDndDrop: function (source, nodes, copy, target) {
            if (this.targetAnchor) {
                this._removeItemClass(this.targetAnchor, "Hover");
            }
            if (this == target) {
                // this one is for us => move nodes!
                this.onDrop(source, nodes, copy, target);
            }
            this.onDndCancel();
        }
    });


    function getFileActions(permissions) {
        const result = [];
        const ACTION = types.ACTION;
        const ACTION_ICON = types.ACTION_ICON;
        const VISIBILITY = types.ACTION_VISIBILITY;
        const thiz = this;
        const actionStore = thiz.getActionStore();


        return [];

        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            let action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        const who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }

        /*
         var rootAction = 'Block/Insert';
         permissions.push(rootAction);
         addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         dummy: true,
         onCreate: function (action) {
         action.setVisibility(VISIBILITY.CONTEXT_MENU, {
         label: 'Add'
         });

         }
         }, null, null);
         permissions.push('Block/Insert Variable');


         addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         }, null, null);
         */

        /*
         permissions.push('Clipboard/Paste/New');
         addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
         }, null, null);*/


        const newBlockActions = this.getAddActions();
        const addActions = [];
        let levelName = '';


        function addItems(commandPrefix, items) {

            for (let i = 0; i < items.length; i++) {
                const item = items[i];

                levelName = item.name;


                const path = commandPrefix + '/' + levelName;
                const isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }
        //console.clear();
        //addItems(rootAction, newBlockActions);
        //return result;


        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            const item = selection[0];
            const canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

            return !canMove;

        }


        function canParent(selection, reference, visibility) {

            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            const item = selection[0];
            if(!item){
                console.warn('bad item',selection);
                return false;
            }

            if(this.command === 'Step/Move Left'){
                return !item.getParent();
            }else{
                return item.getParent();
            }
            /*
             var canMove = item.canMove(item, this.command === 'Step/Move Left' ? -1 : 1);
             return !canMove;*/

            return true;

        }

        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */

        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    widgetArgs:{
                        label: ' ',
                        style:'font-size:25px!important;'
                    }
                });

            }
        }, null, isItem);
        permissions.push('Step/Run/From here');

        /**
         * run
         */

        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {

            onCreate: function (action) {

            }
        }, null, isItem);



        /**
         * move
         */

        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['alt up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);


        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['alt down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);
        /*


         permissions.push('Step/Edit');
         addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
         */
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        permissions.push('Step/Move Left');
        permissions.push('Step/Move Right');

        addAction('Move Left', 'Step/Move Left', 'fa-arrow-left', ['alt left'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canParent);

        addAction('Move Right', 'Step/Move Right', 'fa-arrow-right', ['alt right'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });


            }
        }, null, canParent);


        return result;
    }

    function doPost(view){

        const right  = view.getRightPanel();
        const splitter = right.getSplitter();
        //var ori = splitter.orientation();
        //splitter.orientation(false);
        //spl

        //splitter.collapse();
        //splitter.expand();
        splitter.minimize = function(){

            this._isToggledMin = true;

            //save pos
            if(this._isToggledMin){
                this._savedPosMin = this.pos();
            }

            const togglePosValue = this._isToggledMin ? 1 : 0;

            if(togglePosValue==1){
                //togglePosValue = this._savedPosMin;
            }


            //console.log('toggle ' + togglePosValue + ' cvalue ' + this.pos());


            this.pos(togglePosValue);

        }
        //splitter.minimize();

    }

    function clipboardTest(){

        this.select(['root2'],null,true,{
            focus:true
        });

        const actionStore = this.getActionStore();

        const copy = actionStore.getSync('Clipboard/Copy');
        const paste = actionStore.getSync('Clipboard/Paste');

        console.clear();


        this.runAction(copy);
        this.runAction(paste);

        //this.refresh();


    }

    function unparent(){

        this.select(['sub0'],null,true,{
            focus:true
        });
        this.runAction('Step/Move Left');
        this.refresh();
    }

    function reparent(){

        this.select(['root3'],null,true,{
            focus:true
        });
        this.runAction('Step/Move Right');
        this.refresh();
    }

    function expand(){


        this.select(['root3'],null,true,{
            focus:true
        });

        //clipboardTest.apply(this);

        //reparent.apply(this);


        return;



        //var row = grid.row('root');
        //var _t = this._normalize('root');
        //this.expand(_t);
        //var _expanded = this.isExpanded(_t);
        //debugger;
        //this.isRendered('root');
        const store = this.collection;
        //var item = store.getSync('sub0');
        //this._expandTo(item);

        const root = this.collection.getSync('click');


        this.select(['root2','root3'],null,true,{
            focus:true
        });

        const actionStore = this.getActionStore();

        const moveLeft = actionStore.getSync('Step/Move Left');


        const moveUp = actionStore.getSync('Step/Move Up');

        const moveDown = actionStore.getSync('Step/Move Down');

        const items = this.collection.storage.fullData;
        //console.log('before move',items);
        //this.printRootOrder();
        this.runAction(moveUp);
        //console.log('after move',this.collection.storage.fullData);
        //this._place('root','root2','below');
        //this.printRootOrder();


        const thiz = this;

        setTimeout(function(){

            //thiz.refresh();
            //this.runAction(moveDown);
            //thiz.refreshRoot();
            thiz.printRootOrder();
        },1500);

        //this.runAction(moveLeft);
    }


    const _gridBase = {
        _place:function(dst,ref,direction,items){


            const store = this.collection;

            ref = _.isString(ref) ? store.getSync(ref) : ref;
            dst = _.isString(dst) ? store.getSync(dst) : dst;

            items = items || store.storage.fullData;

            direction = direction == -1 ? 0 : 1;

            function index(what){
                for (let j = 0; j < items.length; j++) {
                    if(what.id===items[j].id){
                        return j;
                    }
                }
            };

            function compare(left,right){
                return index(left) - index(right);
            }

            items.remove(dst);
            if(direction==-1){
                direction=0;
            }
            items.splice( Math.max(index(ref) + direction,0), 0, dst);

            store._reindex();
        },
        __reParentBlock:function(dir){


            const item = this.getSelection()[0];

            if (!item /*|| !item.parentId*/) {
                console.log('cant move, no selection or parentId', item);
                return;
            }
            //console.log('reparenting block',item);

            const thiz = this;
            //thiz._preserveSelection();

            const store = item._store;

            item._next = function(dir){

                let _dstIndex = 0;
                let step = 1;

                const items = this._store.storage.fullData;

                //find item
                function _next(item,items,dir){

                    const cIndex = item.indexOf(items, item);
                    const upperItem = items[cIndex + (dir * step)];
                    if(upperItem){
                        if(!upperItem.parentId && upperItem.group && upperItem.group===item.group){

                            _dstIndex = cIndex + (dir * step);
                            return upperItem;
                        }else{
                            step++;
                            return _next(item,items,dir);
                        }
                    }
                    return null;
                }



                const cIndex = this.indexOf(items, item);
                if (cIndex + (dir) < 0) {
                    return false;
                }
                const next = _next(item,items,dir);
                if (!next) {
                    return false;
                }
                items[_dstIndex]=item;
                items[cIndex] = next;
                //store._reindex();
            };

            item.__unparent = function () {

                const item = this;

                if (!item) {
                    return false;
                }
                const parent = item.getParent();

                const items = null;

                let newParent = null;

                if(parent){
                    newParent = parent.getParent();
                }



                if(newParent){

                    item.group = newParent.group;

                    if(newParent.id == thiz.blockGroup){
                        item.group = newParent.id;
                    }
                    item.parentId = newParent.id;
                }else{

                    //var items = store.storage.fullData;

                    //console.log('next down - parent ',parent.next(items,1));

                    if (parent && parent.removeBlock) {
                        parent.removeBlock(item,false);
                    }
                    item.group = thiz.blockGroup;
                    item.parentId = null;
                    item.parent = null;
                }

                /*
                var scope = thiz.blockScope;
                var t = item._store.getSync('sub0');*/

                item._place(null,-1,null);
                item._place(null,-1,null);


                //item._place(null,-1,null);

                //console.log('new parent : ' + thiz.blockGroup ,scope);
            };

            item.reparent = function () {

                const item = this;

                if (!item) {
                    return false;
                }
                const parent = item.getParent();

                if(parent){
                }else{

                    const _next = item.next(null,1) || item.next(null,-1);
                    if(_next){
                        item.group=null;
                        _next._add(item);
                        thiz.refreshRoot();
                    }
                }
            }

            if(dir==-1) {
                item.unparent(thiz.blockGroup);
            }else{
                item.reparent();
            }
            thiz.deselectAll();
            this.refreshRoot();
            this.refresh();
            setTimeout(function(){
                thiz.select(item,null,true,{
                    focus:true
                });
            },20);


        },
        /**
         *
         * @param mixed
         * @param toRow {object} preserve super
         * @param select {boolean} preserve super
         * @param options {object}
         * @param options.focus {boolean}
         * @param options.silent {boolean}
         * @param options.append {boolean}
         */
        /**
         * @param menuItem
         */
        addItem: function (item,action) {
            if (!item) {
                return;
            }
            let target = this.getSelection()[0];
            const thiz = this;
            const proto = item.proto;
            const ctorArgs = item.ctrArgs;
            let where = null;
            let newBlock = null;


            //cache problem:
            if(!target){
                const _item = this.getSelection()[0];
                if(_item){
                    target=_item;
                }
            }


            if(ctorArgs) {
                if (ctorArgs.id) {
                    console.error('addItem:: new block constructor args had id!!!');
                    ctorArgs.id = utils.createUUID();
                }
                if (ctorArgs.parent) {
                    ctorArgs.parent = null;
                }
                if (ctorArgs.parentId) {
                    ctorArgs.parentId = null;
                }
                if (ctorArgs.items) {
                    console.error('addItem:: new block constructor args had items!!!');
                    ctorArgs.items = [];
                }
            }

            if (target && proto && ctorArgs) {

                if (target.owner && target.dstField) {
                    where = target.dstField;
                    target = target.owner;
                }

                ctorArgs['parentId'] = target.id;
                ctorArgs['group'] = null;
                ctorArgs['parent'] = target;//should be obselete

                newBlock = target.add(proto, ctorArgs, where);
                newBlock.parent = target;

            } else if (!target && proto && ctorArgs) {

                if(ctorArgs.group==="No Group" && this.newRootItemGroup){
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }
                if (!ctorArgs.group && this.blockGroup) {
                    ctorArgs.group = this.blockGroup;
                }

                if (ctorArgs.group && this.newRootItemGroup && ctorArgs.group !==this.newRootItemGroup) {
                    ctorArgs.group = this.newRootItemGroup;
                }

                newBlock = factory.createBlock(proto, ctorArgs);//root block
            }

            if (newBlock && newBlock.postCreate) {
                newBlock.postCreate();
            }
            //this.onItemAction();

            if(!newBlock){
                console.error('didnt create block');
                return;
            }


            const store = this.getStore(target);

            const storeItem = store.getSync(newBlock[store.idProperty]);
            if(!storeItem){
                console.error('new block not in store');
                store.putSync(newBlock);
            }

            try {
                scope._emit(types.EVENTS.ON_ITEM_ADDED, {
                    item: newBlock,
                    owner: scope
                });
            }catch(e){

                console.error('error emitting',e);
                debugger;
            }

            /*setTimeout(function(){*/

            /*
             thiz.gridView.grid.set("store", store.filter({
             group: this.blockGroup
             }));
             */
            //thiz.refresh();
            //thiz.onItemAdded(target, newBlock);
            /*},500);*/


            this._itemChanged('added',newBlock,store);
        },
        refreshItem:function(item){},
        _itemChanged:function(type,item,store){

            store = store || this.getStore(item);

            const thiz = this;

            function _refreshParent(item,silent){

                const parent  = item.getParent();
                if(parent) {
                    const args = {
                        target: parent
                    };
                    if(silent){
                        this._muteSelectionEvents=true;
                    }
                    store.emit('update', args);
                    if(silent){
                        this._muteSelectionEvents=false;
                    }
                }else{
                    thiz.refresh();
                }
            }




            function select(item){
                thiz.select(item,null,true,{
                    focus:true
                });
            }


            switch (type){

                case 'added':{
                    //_refreshParent(item);
                    this.refresh();
                    select(item);
                    break;
                }


                case 'moved':{
                    //_refreshParent(item,true);
                    //this.refresh();
                    //select(item);
                    break;
                }



                case 'deleted':{

                    const parent  = item.getParent();
                    if(parent) {

                        const _prev = item.getPreviousBlock() || item.getNextBlock() || parent;


                        const _container = parent.getContainer();
                        if(_container){
                            _.each(_container,function(child){
                                if(child.id == item.id) {
                                    _container.remove(child);
                                }
                            });
                        }
                        this.refresh();

                        setTimeout(function(){
                            if(_prev) {
                                select(_prev);
                            }
                        },1);


                    }

                    break;
                }
            }

        },
        startup:function(){

            this.inherited(arguments);
            /*
                var clickHandler = function(evt) {

                    if (evt && evt.target && domClass.contains(evt.target, 'dgrid-content')) {

                        $(this.domNode).find('.dgrid-focus').each(function(i,el){
                            $(el).removeClass('dgrid-focus');
                        });

                    }
                }.bind(this);

                this.on("click", function (evt) {
                    clickHandler(evt);
                }.bind(this));

            */






            this._on('selectionChanged',function(evt){

                const selection = evt.selection;

                //console.log('selection changed',selection);

                return;




                if(selection[0]){

                    selection[0].canParent = function (item, dir) {

                        try {
                            item = item || this;

                            if (!item) {
                                return false;
                            }
                            const parent = item.getParent();
                            let items = null;

                            if (parent) {
                                items = parent[parent._getContainer(item)];
                                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                                    return false;
                                }
                                const cIndex = this.indexOf(items, item);
                                if (cIndex + (dir) < 0) {
                                    return false;
                                }
                                const upperItem = items[cIndex + (dir)];
                                if (!upperItem) {
                                    return false;
                                }
                            } else {
                                const store = this._store;
                                items = store.storage.fullData;
                                const _next = this.next(items, dir);
                                return _next != null;
                            }

                        } catch (e) {
                            debugger;
                        }
                        return true;
                    }



                    return;
                    selection[0].canMove = function (item, dir) {

                        try {
                            item = item || this;

                            if (!item) {
                                return false;
                            }
                            const parent = item.getParent();
                            let items = null;
                            if (parent) {
                                items = parent[parent._getContainer(item)];
                                if (!items || items.length < 2 || !this.containsItem(items, item)) {
                                    return false;
                                }
                                const cIndex = this.indexOf(items, item);
                                if (cIndex + (dir) < 0) {
                                    return false;
                                }
                                const upperItem = items[cIndex + (dir)];
                                if (!upperItem) {
                                    return false;
                                }
                            } else {

                                const store = this._store;
                                items = store.storage.fullData;
                                const _next = this.next(items, dir);
                                return _next != null;
                            }

                        } catch (e) {
                            debugger;
                        }
                        return true;
                    }

                }

            })
        }
    };

    /***
     * playground
     */
    const _lastGrid = window._lastGrid;
    const ctx = window.sctx;
    const ACTION = types.ACTION;
    var root;


    const _actions = [
        ACTION.RENAME
    ];

    function fixScope(scope){

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function(source,target,before,add){




            console.log('scope::move, add: ' +add,arguments);

            if(!add){
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if(add){

                //remove it from the source parent and re-parent the source
                if(target.canAdd && target.canAdd()){

                    var sourceParent = this.getBlockById(source.parentId);
                    if(sourceParent){
                        sourceParent.removeBlock(source,false);
                    }
                    target.add(source,null,null);
                    return;
                }else{
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if(!target.parentId && add==false){

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if(sourceParent && sourceParent.removeBlock){
                    sourceParent.removeBlock(source,false);
                    source.parentId=null;
                    source.group=target.group;
                }

                const itemsToBeMoved=[];
                const groupItems = this.getBlocks({
                    group:target.group
                });

                const rootLevelIndex=[];
                const store = this.getBlockStore();

                const sourceIndex = store.storage.index[source.id];
                const targetIndex = store.storage.index[target.id];
                for(var i = 0; i<groupItems.length;i++){

                    const item = groupItems[i];
                    //keep all root-level items

                    if( groupItems[i].parentId==null && //must be root
                        groupItems[i]!=source// cant be source
                    ){

                        const itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if(add){
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if(before){
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if(!before){
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            }else if( !source.parentId && target.parentId && add==false){
                source.group = target.group;
                if(target){

                }

                //we move from root to into root item
            }else if( !source.parentId && !target.parentId && add){

                console.error('we are adding an item into root root item');
                if(target.canAdd && target.canAdd()){
                    source.group=null;
                    target.add(source,null,null);
                }
                return true;

                // we move within the same parent
            }else if( source.parentId && target.parentId && add==false && source.parentId === target.parentId){
                console.error('we move within the same parents');
                const parent = this.getBlockById(source.parentId);
                if(!parent){
                    console.error('     couldnt find parent ');
                    return false;
                }

                const maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items,source);
                var cIndexTarget = source.indexOf(items,target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    parent.move(source,direction);
                }
                return true;

                // we move within the different parents
            }else if( source.parentId && target.parentId && add==false && source.parentId !== target.parentId){                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if(!sourceParent){
                    console.error('     couldnt find source parent ');
                    return false;
                }

                const targetParent = this.getBlockById(target.parentId);
                if(!targetParent){
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if(sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()){
                    sourceParent.removeBlock(source,false);
                    targetParent.add(source,null,null);
                }else{
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if(items==null){
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items,source);
                var cIndexTarget = targetParent.indexOf(items,target);
                if(!cIndexSource || !cIndexTarget){
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    targetParent.move(source,direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        const topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId:null
        });


        const grouped = _.groupBy(blocks,function(block){
            return block.group;
        });

        function createDummyBlock(id,scope){

            const block = {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": null,
                    "id": id,
                    "items": [

                    ],
                    "name": id,
                    "method": "----group block ----",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": false,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"

                };

            return scope.blockFromJson(block);

        }

        for(const group in grouped){

            const groupBlock = createDummyBlock(group,scope);
            var blocks = grouped[group];
            _.each(blocks,function(block){
                groupBlock['items'].push(block);



                if(!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }

        console.clear();
        const root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }





    function createScope() {

        const data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }

    if (ctx) {


        var blockManager = ctx.getBlockManager();


        function createGridClass() {

            const renderers = [TreeRenderer];

            //, ThumbRenderer, TreeRenderer


            const multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);




            const _gridClass = Grid.createGridClass('driverTreeView',
                {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT:{
                        CLASS:declare('splitMixin',null,_layoutMixin)
                    },
                    DND:{
                        CLASS:Dnd//declare('splitMixin',Dnd,_dndMixin)
                    }

                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );



            return declare('gridFinal', BlockGrid, _gridBase);

            //return BlockGrid;
        }

        const blockScope = createScope('docs');


        const mainView = ctx.mainView;

        const docker = mainView.getDocker();


        if (mainView) {
            if (_lastGrid) {

                docker.removePanel(_lastGrid);
            }

            const parent = docker.addTab(null, {
                title: 'blox',
                icon: 'fa-folder'
            });
            window._lastGrid = parent;




            const actions = [];
            const thiz = this;
            const ACTION_TYPE = types.ACTION;
            const ACTION_ICON = types.ACTION_ICON;
            let grid;
            let ribbon;


            const store = blockScope.blockStore;


            const _gridClass = createGridClass();




            const gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                collection: store.filter({
                    group: "click"
                }),
                dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                dndParams: {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                }
            };



            grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);



            /*
            grid.select(['sub1'], null, true, {
                focus: true
            });
            */













            const blocks = blockScope.allBlocks();

            var root = blocks[0];


            const actionStore = grid.getActionStore();
            const toolbar = mainView.getToolbar();

            let _defaultActions = [];

            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [grid.permissions]));

            grid.addActions(_defaultActions);



            if (!toolbar) {


            } else {
                toolbar.addActionEmitter(grid);
                toolbar.setActionEmitter(grid);
            }

            doPost(grid);


            setTimeout(function () {
                mainView.resize();
                grid.resize();

            }, 1000);


            function test() {

                expand.apply(grid);

                return;

            }

            function test2() {

                return;


                /*
                 var c2 = actionStore.getSync('View/Columns/Show Size');
                 c2._originEvent='change';
                 var _c = c1.get('value');
                 */


                //console.log('c2 value : ' + _c);


                try {
                    //copy.set('value', false, true);
                    c2.set('value', false);


                } catch (e) {
                    //debugger;
                }

                return;

                console.log('-------------------------------------------------------------------------');

                const item = store.getSync('id3');
                const item1 = store.getSync('id1');
                const item2 = store.getSync('id2');


                //console.dir(grid._rows);


                /*
                 grid.select(item);
                 grid.focus(item);*/


                //grid.select([item,item1,item2]);
                //grid.select(item);
                grid.select([item1, item2], null, true, {
                    append: true,
                    focus: false,
                    silent: true

                });


                /*var isToolbared = grid.hasFeature('TOOLBAR');
                 console.warn('has Toolbar ');*/


                /*
                 var nameProperty = item.property('label');

                 var urlProperty = item.property('url');


                 nameProperty.observe(function () {
                 console.log('horray!', arguments);
                 });

                 urlProperty.observe(function () {
                 console.log('horray url', arguments);
                 });

                 item.set('label', 'new label');
                 item.set('url', null);

                 //store.notify(item,'id3');
                 store.emit('update', {target: item});
                 */


                /*grid.refresh();*/
                //console.log('item ', item);

                /*
                 grid.select(store.getSync('id99'), null, true, {
                 append: true,
                 focus: true,
                 silent:true
                 });*/

                const item99 = store.getSync('id99');
                const item98 = store.getSync('id98');

                grid.select([item99, item98], null, true, {
                    append: true,
                    focus: true,
                    silent: false
                });


                //console.log('is selected ' + grid.isSelected(item98));

                //console.dir(grid.getRows(true));

                /*console.dir(grid.getPrevious(item99, false, true));*/
                /*
                 console.dir(grid.getSelection(function(item){
                 return item.id!='id99';
                 }));*/


                //console.log(grid.getFocused());

                /*store.removeSync('id3');*/
            }

            setTimeout(function () {
                test();
            }, 1000);


            setTimeout(function () {
                test2();
            }, 2000);
        }
    }

    return Grid;
});