define([
    'dcl/dcl',
    'xdojo/declare',
    'dojo/_base/lang',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xaction/DefaultActions',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'xgrid/Search',
    'xide/tests/TestUtils',
    'module',

    'xide/views/CIGroupedSettingsView',
    'xide/widgets/WidgetBase',
    'dojo/when',
    'dojo/Deferred',
    "xide/views/_Dialog",
    "xcf/views/DeviceTreeView"

], function (dcl,declare, lang, utils, types,factory,
             Grid, TreeRenderer, DefaultActions,KeyboardNavigation,_Widget,Search,TestUtils,module,
             CIGroupedSettingsView,WidgetBase,when,Deferred,_Dialog,DeviceTreeView) {
    const ACTION = types.ACTION;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const ctx = window.sctx;
    const test = true;
    let marantz;
    let marantzInstance;



    const gridClass = Grid.createGridClass('driverTreeView',{
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress:false,
            isGroup:function(item){

                if(item){
                    return item.isDir===true;
                }else{
                    return false;
                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                }
            },
            postMixInProperties: function() {
                const thiz = this;

                function getIconClass(item) {

                    if(item.iconClass){
                        return item.iconClass;
                    }
                    const opened = true;
                    const iclass = (!item || item.isDir) ? (opened ? "dijitFolderOpened" : "dijitFolderClosed") : "dijitLeaf";

                    //commands or variables
                    if (item.ref && item.ref.item && item.ref.item.getIconClass) {
                        const _clz = item.ref.item.getIconClass();
                        if (_clz) {
                            return _clz;
                        }
                    } else if (!thiz.isGroup(item)) {
                        return thiz.delegate.getDeviceStatusIcon(item);
                    }

                    if (utils.toString(item.name) === 'Commands') {
                        return 'el-icon-video';
                    }
                    return iclass;
                }

                this.collection = this.collection.filter(this.getRootFilter());


                this.columns=[
                    {
                        renderExpando: true,
                        label: "Name",
                        field: "name",
                        sortable: false,
                        formatter: function (name, item) {

                            const meta = item['user'];

                            if(item.iconClass){
                                return '<span class="grid-icon ' + item.iconClass + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            }

                            if (thiz.isGroup(item)) {
                                return '<span class="grid-icon ' + 'fa-folder' + '"></span>' + item.name;
                            }

                            if (item.icon) {
                                return '<span class="grid-icon ' + item.icon + '"></span>' + item.name;
                            }

                            //commands or variables
                            if (item.ref && item.ref.item) {

                                //@TODO why is here a difference ?
                                if (item.ref.item.name) {
                                    return item.ref.item.name;
                                }
                                if (item.ref.item.title) {
                                    return item.ref.item.title;
                                }
                            }

                            //true device
                            if (!thiz.isGroup(item)) {

                                if (meta) {

                                    return '<span class="grid-icon ' + getIconClass(item) + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                                } else {
                                    return item.name;
                                }
                            } else {
                                return item.name;
                            }
                        }
                    }
                ];

                this.inherited(arguments);



                this._on('onAddAction',function(action){
                    if(action.command == types.ACTION.EDIT){
                        action.label = 'Settings';
                    }
                });
            },
            runAction:function(action,item){
                console.log('run action in',action);
                this.inherited(arguments);
                //thiz.runAction(action,item);
            },
            /**
             * Override render row to add additional CSS class
             * for indicating a block's enabled state
             * @param object
             * @returns {*}
             */
            renderRow:function(object){
                const res = this.inherited(arguments);
                const ref = object.ref;
                const item = ref ? ref.item  : null;


                if(item!=null && item.scope && item.enabled===false){
                    $(res).addClass('disabledBlock');
                }
                return res;
            },
            refresh2:function(force){
                if(this._refreshInProgress){
                    return this._refreshInProgress;
                }

                const _restore = this._preserveSelection();
                const thiz = this;
                const active = this.isActive();
                const res = this.inherited(arguments);

                this._refreshInProgress = res;

                res && res.then && res.then(function(){

                    thiz._refreshInProgress = null;
                    thiz._restoreSelection(_restore,10,true);

                    /*
                    if(_restore.focused && (active || force )) {
                        //console.log('restore focused');
                        thiz.focus(thiz.row(_restore.focused));
                    }*/
                });

                return res;
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR:types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            WIDGET:{
                CLASS:_Widget
            },
            SEARCH:{
                CLASS:Search
            }

        },
        {
            RENDERER: TreeRenderer
        },
        null,
        null);


    const _protoDefault = {
        children: "",
        name: "RunScript",
        properties: {
            style: "position:relative",
            scopeid: "",
            targetevent: "click"
        },
        type: "xblox/RunScript",
        userData: {}
    };
    const _dragStart = function (from, node, context, e, proto, userData, properties) {

        //this.selectedItem = null;
        proto = lang.clone(proto);

        lang.mixin(proto.properties, properties);

        const data = e.dragSource.data;

        userData = data.userData || userData;

        Metadata.getHelper(proto.type, 'tool').then(function (ToolCtor) {
            // Copy the data in case something modifies it downstream -- what types can data.data be?
            const tool = new (ToolCtor || CreateTool)(dojo.clone(proto), userData);
            context.setActiveTool(tool);
        }.bind(this));

        // Sometimes blockChange doesn't get cleared, force a clear upon starting a widget drag operation
        context.blockChange(false);

        // Place an extra DIV onto end of dragCloneDiv to allow
        // posting a list of possible parent widgets for the new widget
        // and register the dragClongDiv with Context
        if (e._dragClone) {
            domClass.add(e._dragClone, 'paletteDragContainer');
            dojo.create('div', {className: 'maqCandidateParents'}, e._dragClone);
        }
        //FIXME: Attach dragClone and event listeners to tool instead of context?
        context.setActiveDragDiv(e._dragClone);
    };
    const _dragEnd = function () {
        if (FocusUtils.curNode && FocusUtils.curNode.blur) {
            FocusUtils.curNode.blur();
        }
    };
    const makeDND = function (from, node, context, proto, userData, properties) {

        if (!context) {
            return;
        }
        const clone = node.domNode;

        const ds = new DragSource(node.domNode, "component", node, clone);

        ds.targetShouldShowCaret = true;
        ds.returnCloneOnFailure = false;

        /**
         * outer handlers:
         */
        from.connect(ds, "onDragStart", dojo.hitch(from, function (e) {
            console.log('on drag start ');
            _dragStart(from, node, context, e, proto, userData, properties);
        })); // move start

        from.connect(ds, "onDragEnd", dojo.hitch(from, function (e) {
            console.log('on drag end');
            _dragEnd();
        })); // move end


        /**
         * now the inner handlers
         */
        node.connect(node.domNode, "onmouseover", function (e) {
            node._mouseover = true;
            const div = node.domNode;
        });

        node.connect(node.domNode, "onmouseout", function (e) {
            node._mouseover = false;
            const div = node.domNode;
        });


        node.connect(node.domNode, "onmousedown", function (e) {

            console.log('mouse down');
            DragManager.document = context.getDocument();
            const frameNode = context.frameNode;
            if (frameNode) {
                const coords = dojo.coords(frameNode);
                const containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
                console.log('drag manager updated ', DragManager);
            }
        });
    };
    const makeDNDG = function (from, node, context, proto, userData, properties) {

        if (!context) {
            return;
        }
        const clone = node.domNode;

        const ds = new DragSource(node.domNode, "component", node, clone);

        ds.targetShouldShowCaret = true;
        ds.returnCloneOnFailure = false;

        /**
         * outer handlers:
         */
        dojo.connect(ds, "onDragStart", dojo.hitch(from, function (e) {
            console.log('on drag start ');
            _dragStart(from, node, context, e, proto, userData, properties);
        })); // move start

        dojo.connect(ds, "onDragEnd", dojo.hitch(from, function (e) {
            _dragEnd();
        })); // move end


        /**
         * now the inner handlers
         */
        dojo.connect(node.domNode, "onmouseover", function (e) {
            node._mouseover = true;
        });

        dojo.connect(node.domNode, "onmouseout", function (e) {
            node._mouseover = false;
        });


        dojo.connect(node.domNode, "onmousedown", function (e) {


            DragManager.document = context.getDocument();
            const frameNode = context.frameNode;
            if (frameNode) {
                const coords = dojo.coords(frameNode);
                const containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
            }
        });
    };

    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.HEADER,
        ACTION.SEARCH,
        'File/ConnectDevice',
        'File/DisconnectDevice',
        'File/New Group',
        'File/Edit Driver Instance',
        'Instance/Edit Variables',
        'Instance/Edit Commands',
        'Instance/Show Log'
    ];


    const module2 = DeviceTreeView = declare('xcf.views.DeviceTreeView',gridClass,{
        iconClass: 'fa-sliders',
        beanType: 'device',
        permissions : DefaultPermissions,
        deselectOnRefresh: false,
        showHeader: false,
        toolbarInitiallyHidden:true,
        //attachDirect:true,
        groupOrder: {
            'Clipboard': 110,
            'Device':105,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'New': 5,
            'Navigation': 3,
            'Select': 0
        },
        getDeviceActions: function (container,permissions,grid) {
            const actions = [];
            const thiz = this;
            const delegate = thiz.delegate;
            const ACTION = types.ACTION;

            function _selection(){
                const selection = grid.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                if(!item){
                    console.error('have no item');
                    return null;
                }
                return selection;
            }
            function _device(){
                const selection = _selection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                return item.device || item.ref && item.ref.device ? item.ref.device : item.isDevice ? item  : null;
            }
            ///////////////////////////////////////////////////////////////
            //
            //      Disable Functions
            //
            ///////////////////////////////////////////////////////////////
            function shouldDisableEdit(){

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                return false;
            }
            function shouldDisableEditInstance(){

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                const device = _device();
                if(!device){
                    return true;
                }
                const instance = delegate.getInstance(device);
                if(instance){
                    return false;
                }else{
                    return true;
                }

                return false;
            }
            function shouldDisableRun(){

                const selection = _selection();
                if (!selection || !selection[0].ref || !selection[0].ref.item) {
                    return true;
                }
                return false;
            }
            function shouldDisableConnect(){

                const selection = _selection();
                if (!selection || !selection[0] || !selection[0].path || selection[0].path.indexOf('.json')===-1) {
                    return true;
                }
                return false;
            }

            ///////////////////////////////////////////////////////////////
            //
            //      Actions
            //
            ///////////////////////////////////////////////////////////////

            const defaultMixin = {
                          addPermission:true
                      };

            const DEVICE_COMMAND_GROUP = 'Device/Command';


            actions.push(this.createAction('Edit',ACTION.EDIT,types.ACTION_ICON.EDIT,['f4', 'enter','dblclick'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(this.createAction('Edit Driver','File/Edit Driver',types.ACTION_ICON.EDIT,['f3', 'ctrl enter'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEdit,permissions,container,grid
            ));


            actions.push(this.createAction('Edit Instance','File/Edit Driver Instance',types.ACTION_ICON.EDIT,['f6'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            actions.push(this.createAction('Edit Driver Code','File/Edit Driver Code','fa-code',null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            actions.push(this.createAction('Open Console','Instance/Console','fa-terminal',['f5'],'Home','Instance','item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            actions.push(this.createAction('Open Expression Wizard','Instance/ConsoleWizard','fa-magic',['f7'],'Home','Instance','item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableEditInstance,permissions,container,grid
            ));

            actions.push(thiz.createAction({
                label: 'Run',
                command:ACTION.RUN ,
                icon: types.ACTION_ICON.RUN,
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: shouldDisableRun,
                mixin: utils.mixin({quick:true},defaultMixin)
            }));

            actions.push(thiz.createAction({
                label: 'Edit Variables',
                command:'Instance/Edit Variables',
                icon: 'fa-list-alt',
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEditInstance,
                mixin: defaultMixin
            }));
            actions.push(thiz.createAction({
                label: 'Edit Commands',
                command:'Instance/Edit Commands',
                icon: types.ACTION_ICON.EDIT,
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEditInstance,
                mixin: defaultMixin
            }));
            actions.push(thiz.createAction({
                label: 'Show Log',
                command:'Instance/Show Log',
                icon: 'fa-calendar',
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEdit,
                mixin: utils.mixin({quick:true},defaultMixin)
            }));
            /*
             actions.push(this.createAction('Run', ACTION.RUN,types.ACTION_ICON.RUN, ['r'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
             defaultMixin,null,shouldDisableRun,permissions,container,grid
             ));
             */

            actions.push(this.createAction('New Group', 'File/New Group' ,types.ACTION_ICON.NEW_DIRECTORY,
                ['f7'],'Home','New','item|view',null,null,
                utils.mixin({quick:true},defaultMixin),null,null,permissions,container,grid
            ));

            actions.push(this.createAction('New Device', 'File/New Device' ,types.ACTION_ICON.NEW_FILE, ['ctrl n'],'Home','New','item',null,null,
                utils.mixin({quick:true},defaultMixin),null,null,permissions,container,grid
            ));



            ///////////////////////////////////////////////////////////////
            //  Device Control
            actions.push(this.createAction('Disconnect', 'File/DisconnectDevice','fa-unlink', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableConnect,permissions,container,grid
            ));

            actions.push(this.createAction('%%Connect', 'File/ConnectDevice','fa-link', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                utils.mixin({quick:true},defaultMixin),null,shouldDisableConnect,permissions,container,grid
            ));


            /*
             actions.push(this.createActionParameters('New Device Group', 'File/New Device Group', 'edit', types.ACTION_ICON.NEW_DIRECTORY, function () {
             delegate.newGroup(thiz.getSelectedItem());
             }, 'F7', 'f7', null, container, thiz));
             */


            //add source actions
            actions.push(this.createAction('Source', 'View/Source', 'fa-hdd-o', ['f4'], 'Home', 'Navigation', 'item', null,
                function () {
                },
                {
                    addPermission: true,
                    tab: 'Home',
                    dummy: true
                }, null, null, permissions, container, thiz
            ));
            let i=0;
            _.each([
                {
                    label:'System',
                    scope:'system_devices'
                },
                {
                    label:'User',
                    scope:'user_devices'
                }
            ], function(item) {
                const label = item.label;
                actions.push(thiz.createAction(label, 'View/Source/' + label, 'fa-hdd-o', ['alt f' + i], 'Home', 'Navigation', 'item', null,
                    function () {
                    },
                    {
                        addPermission: true,
                        tab: 'Home',
                        item: item,
                        quick:true
                    }, null, null, permissions, container, thiz
                ));
                i++;
            });


            const root = 'File/Log';

            actions.push(this.createAction({
                label:'Logging',
                command:'File/Log',
                icon:'fa-calendar',
                tab:'File',
                group:'Logging',
                toggleGroup:thiz.id + 'Logging_Flags',
                mixin:{
                    addPermission: true,
                    quick:true
                },
                onCreate:function(action){

                }
            }));

            const node = this.domNode;

            function _createEntry(value,label) {
                const icon = 'fa-cogs';
                // Allow cols to opt out of the hider (e.g. for selector column).
                const _action = thiz.createAction(label, root + '/' + label , icon, null, 'File', 'Logging', 'item|view',
                    //oncreate
                    function(action){

                        const widgetImplementation = {
                            postMixInProperties: function() {
                                this.inherited(arguments);
                                this.checked = this.item.get('value') == true;
                            },
                            startup:function(){
                                this.inherited(arguments);
                                this.on('change',function(val){
                                    thiz.showColumn(id,val);
                                })
                            }
                        };
                        const widgetArgs  ={
                            checked:!value,
                            iconClass:icon,
                            style:'float:inherit;'
                        };


                        const _visibilityMixin = {
                            widgetArgs:widgetArgs,
                            actionType : 'multiToggle'
                        };

                        action.actionType = 'multiToggle';
                        action.setVisibility(types.ACTION_VISIBILITY_ALL,utils.cloneKeys(_visibilityMixin,false));
                        label = action.label.replace('Show ','');

                    }, /*handler*/ null ,
                    {
                        value:value,
                        addPermission:true,
                        quick:true,
                        tab:"File",
                        group:"Logging"
                    },
                    null, null, permissions, node,thiz,thiz);

                return _action;
            }



            actions.push(_createEntry(1,"On Device Started"));



            return actions;
        },
        editItem: function (item) {
            const thiz = this;
            if (item.type === types.ITEM_TYPE.BLOCK) {
                return thiz.blockManager.editBlock(item.ref.item, null, false);
            }
            if(item.driver){
                item = item.driver;
            }

            thiz.openItem(item);
        },
        startup:function(){

            const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);

            this.inherited(arguments);

            this.addActions(_defaultActions);

            this.addActions(this.getDeviceActions(this.domNode,this.permissions,this));

            const thiz = this;

            this.refresh().then(function(){
                thiz.select([0], null, true, {
                    focus: true
                });
            });



            this.subscribe(types.EVENTS.ON_STORE_CHANGED,function(evt){
                if(evt.store.id == thiz.collection.id){
                    thiz.refresh();
                }
                if(evt.store == thiz.collection){
                    thiz.refresh();
                }
            });

            thiz.collection.on('update',function(evt){
                if(evt && evt.target){
                    try{
                        const cell = thiz.cell(evt.target.path, '0');
                        if (cell) {
                            thiz.refreshCell(cell);
                        }
                    }catch(e){
                        logError(e);
                    }
                }else {
                    thiz.refresh();
                }
                thiz.refreshActions();
            });

            thiz.collection.on('delete',function(){
                thiz.refresh();
            });


        },
        //////////////////////////////////////////////////////////////////////
        //
        //  Actions - Impl.
        //
        changeSource:function(source){
            console.log('change source',arguments);


            const thiz = this;
            const delegate = thiz.delegate;
            const scope = source.scope;
            let store = delegate.getStore(scope);


            function wireStore(store){

                if(thiz['_storeConnected'+scope]){
                    return;
                }

                thiz['_storeConnected'+scope]=true;

                function updateGrid() {
                    setTimeout(function () {
                        thiz.refresh();
                    }, 100);
                }

                store.on('update', function () {
                    updateGrid();
                });

                store.on('added', function () {
                    //console.log(' added to store: ',arguments);
                    updateGrid();
                });

                store.on('remove', function () {
                    //console.log(' removed from store: ',arguments);
                    updateGrid();
                });

                store.on('delete', function () {
                    //console.log(' deleted from store: ',arguments);
                    updateGrid();
                });

            }

            function ready(store){
                const collection = store.filter(thiz.getRootFilter());
                thiz.set('collection',collection);
                thiz.set('title','Drivers ('+source.label+')');
                thiz.scope = scope;
                wireStore(store);
            }

            if(store.then){

                store.then(function(){
                    store = delegate.getStore(scope);
                    ready(store);
                });
            }else{

                //is store
                ready(store);
            }
        },
        ///////////////////////////////////////////////////////////////
        runAction: function (action,_item) {
            if (!action || !action.command) {
                return;
            }

            const ACTION = types.ACTION;
            const thiz = this;
            const delegate = thiz.delegate;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            const driverManager = ctx.getDriverManager();
            let item = thiz.getSelectedItem() || _item;
            let device;

            if(item) {
                device = item && item.device ? item.device : item.ref && item.ref.device ? item.ref.device : item.isDevice ? item : null;
            }



            if(action.command.indexOf('View/Source')!=-1){
                return this.changeSource(action.item);
            }


            const driverInstance  =  device ? deviceManager.getInstance(device) : null;
            const driverId = device ? device.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER) : null;
            const driver = driverInstance ? driverInstance.driver : driverManager.getItemById(driverId);
            if(!driverInstance)

                console.log('--run action ' + action.command,device);

            const defaultSelect = {
                focus:true,
                append:false,
                delay:1,
                expand:true
            };

            switch (action.command) {

                case 'File/Edit Driver Instance':{

                    if(driverInstance){
                        return driverManager.openItemSettings(driver,item);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }

                case 'Instance/Edit Variables':{

                    if(driverInstance){
                        return driverManager.openItemSettings(driver,item,{
                            showBasicCommands:false,
                            showConditionalCommands:false,
                            showResponseBlocks:false,
                            showLog:false,
                            showConsole:false,
                            showVariables:true,
                            showSettings:false,
                            showInitTab:false
                        },true);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'Instance/Show Log':{

                    /*if(driverInstance){*/
                    //var driver = driverInstance.driver;
                    return driverManager.openItemSettings(driver,item,{
                        showBasicCommands:false,
                        showConditionalCommands:false,
                        showResponseBlocks:false,
                        showLog:true,
                        showConsole:false,
                        showVariables:false,
                        showSettings:false,
                        showInitTab:false,
                        isSingle:true
                    },true);
                    //}
                    /*else{

                     this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                     text:'Can`t edit driver instance because device is not enabled or connected!',
                     type:'error'
                     });
                     }*/

                    break;

                }
                case 'Instance/Edit Commands':{

                    if(driverInstance){
                        return driverManager.openItemSettings(driver,item,{
                            showBasicCommands:true,
                            showConditionalCommands:true,
                            showResponseBlocks:false,
                            showLog:false,
                            showConsole:false,
                            showVariables:false,
                            showSettings:false,
                            showInitTab:false
                        },true);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'File/Edit Driver':{

                    console.log('---edit driver instance ');
                    if(driver){
                        return driverManager.openItemSettings(driver,null);
                    }

                    /*
                     var driverInstance  =  item.driverInstance;
                     if(driverInstance){
                     var driver = driverInstance.driver;
                     return driverManager.openItemSettings(driver,item);
                     }
                     */

                    break;

                }

                case ACTION.RUN:{
                    if (device && driverInstance) {
                        const scope = driverInstance.blockScope;
                        const block = scope.getBlockById(item.id);
                        if(block){
                            block.solve(block.scope);
                        }
                    }
                    break;
                }
                case 'File/Edit Driver Code':
                {
                    if(device && driverInstance){
                        item = driverInstance.driver;
                    }
                    return driverManager.editDriver(null,item);
                }
                case 'Instance/Console':
                {
                    if(device && driverInstance) {
                        deviceManager.startDevice(device,true).then(function(){
                            deviceManager.openConsole(device);
                        });
                        return;
                    }
                    return deviceManager.openConsole(device);
                }
                case 'Instance/ConsoleWizard':
                {
                    if(device && driverInstance) {
                        deviceManager.startDevice(device,true).then(function(){
                            deviceManager.openExpressionConsole(device);
                        });
                        return;
                    }
                    return deviceManager.openExpressionConsole(device);
                }
                case 'File/Log':
                {
                    return deviceManager.openLog(device);
                }

                case ACTION.EDIT:
                {
                    return thiz.editItem(item);
                }
                case ACTION.RELOAD:
                {
                    return thiz.refresh();
                }
                case ACTION.DELETE:
                {
                    const _res = delegate.onDeleteItem(item);

                    return _res;
                }
                case 'File/DisconnectDevice':{

                    const dfd = delegate.stopDevice(item);
                    thiz.refresh();
                    return false;
                }
                case 'File/ConnectDevice':{
                    delegate.startDevice(item,true);
                    thiz.refresh();
                    return false;
                }
                case 'File/New Device':{
                    var res = delegate.newItem(item,thiz.scope);
                    res.then(function(item){
                        thiz.refresh();
                        thiz.select(item,null,true,defaultSelect);
                    });
                    return res;
                }
                case 'File/New Group':{
                    var res = delegate.newGroup(item,thiz.scope);
                    res.then(function(item){
                        thiz.refresh();
                        thiz.select(item,null,true,defaultSelect);
                    });
                    return res;
                }
            }

            return this.inherited(arguments);
        },
        changeScope: function (name) {

            this.delegate.ls(name + '_devices').then(function (data) {
                console.log('got scope ', data);
            });

        },
        openItem:function(item,device){
            if(item && !item.isDir && !item.virtual){
                this.delegate.openItemSettings(item,device);
            }
        }
    });

    /**
     * Url generator for device/driver/[command|block|variable]
     *
     * @param device
     * @param driver
     * @param block
     * @param prefix
     * @returns {*}
     */
    function toUrl(device,driver,block,prefix){

        prefix = prefix || '';

        const pattern = prefix + "deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";

        const url = lang.replace(
            pattern,
            {
                deviceId:device.id,
                deviceScope:device.scope,
                driverId:driver.id,
                driverScope:driver.scope,
                block:block.id
            });
        return url;
    }

    /**
     * Filter function to reject selections in a device - tree - view
     * @param item
     * @param acceptCommand
     * @param acceptVariable
     * @param acceptDevice
     * @returns {*}
     */
    function accept(item,acceptCommand,acceptVariable,acceptDevice){
        const reference = item.ref || {};
        const block       =       reference.item;
        const device      =       reference.device;
        const driver      =       reference.driver;
        const scope       =       block ? block.getScope() : null;
        const isCommand   =       block ? block.declaredClass.indexOf('model.Command')!==-1 : false;
        const isVariable  =       block ? block.declaredClass.indexOf('model.Variable')!==-1 : false;

        isCommand && console.log('have command ' + block.name);

        if(isCommand && acceptCommand){
            return toUrl(device,driver,block,'command://');
        }
        if(isVariable && acceptVariable){
            return toUrl(device,driver,block,'variable://');
        }

        return false;
    }
    /**
     * Renders a CIS inline
     * @param CIS
     * @param where
     * @param owner
     * @returns {*}
     */
    function renderCIS(CIS,where,owner){

        const widgetsHead = factory.createWidgetsFromArray(CIS, owner, null, false);
        const result = [];
        const dfd = new Deferred();

        when(widgetsHead,function(widgets){

            if (widgets) {


                for (let i = 0; i < widgets.length; i++) {

                    const widget = widgets[i];

                    widget.delegate = owner;

                    where.appendChild(widget.domNode);

                    if(where && where.lazy===true) {
                        widget._startOnShow = true;
                    }else{
                        widget.startup();
                    }


                    widget._on('valueChanged',function(evt){
                        //owner.onValueChanged(evt);
                    });

                    owner._emit('widget',{
                        widget:widget,
                        ci:widget.userData
                    })


                    result.push(widget);

                    widget.userData.view=owner;
                    widget.onAttached && widget.onAttached(where);

                    if(owner && owner.add && owner.add(widget,null,false)){

                    }else{
                        console.error('view has no add',owner);
                        owner.add(widget,null,false);
                    }
                }

                dfd.resolve(result);

            }
        });

        return dfd;

    }


    /**
     * Create a simple CIS
     * @returns {{inputs: *[]}}
     */
    function createPickerCIS(instance){



        const CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General9',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Command",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("86025507-97e0-99a4-0640-59c5725ef116"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'command'
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("6ab97609-4b3a-4f65-d018-1027d402a94d"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'variable'
                }
            ]
        };
        _.each(CIS.inputs,function(ci){
            ci.driver = instance.driver,
                ci.device = instance.device
        });
        return CIS;
    }

    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    function createDeviceView(parent,ctx,driver,device,type) {

        const grid = utils.addWidget(module2, {
            title: 'Devices',
            collection: ctx.getDeviceManager().getStore('user_devices'),
            delegate: ctx.getDeviceManager(),
            blockManager: ctx.getBlockManager(),
            ctx: ctx,
            icon: 'fa-sliders',
            showHeader: false,
            scope: 'system_devices',
            resizeToParent: true
        }, null, parent, true);

        grid._on('selectionChanged',function(evt){
            const selection = evt.selection;
            const item = selection && selection.length==1 ? selection[0] : null;

            if(!item){
                return;
            }

            const isAccepted = accept(item,type=='command',type=='variable',type=='device');
            isAccepted  && console.log('-selected : ' + isAccepted,item);
            if(isAccepted){
                grid._selected = isAccepted;
            }
        });

        return grid;
    }

    function createPickerWidget(device,driver){

        const Widget = dcl(WidgetBase, {
            declaredClass: "xcf.widgets.CommandPickerWidget",
            minHeight: "400px;",
            value: "",
            options: null,
            title:'Command',
            templateString: "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
            "<table border='0' cellpadding='5px' width='100%' >" +
            "<tbody>" +
            "<tr attachTo='extensionRoot'>" +
            "<td width='15%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
            "<td width='100px' class='widgetValue' valign='middle' attachTo='valueNode'></td>" +
            "<td class='extension' width='25px' attachTo='button0'></td>" +
            "<td class='extension' width='25px' attachTo='button1'></td>" +
            "<td class='extension' width='25px' attachTo='button2'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
            onSelect: function (ctx,driver,device,title) {
                const thiz = this;

                const _defaultOptions = {};
                const ci = this.userData;

                if (this.options) {
                    lang.mixin(_defaultOptions, this.options);
                }

                try {

                    const value = utils.toString(this.userData['value']);

                    const dlg = new _Dialog({
                        size: types.DIALOG_SIZE.SIZE_SMALL,
                        title: '%%Select a device ' +title,
                        type:types.DIALOG_TYPE.INFO,
                        bodyCSS: {
                            'height': 'auto',
                            'min-height': '300px',
                            'padding': '8px',
                            'margin-right': '16px'
                        },
                        picker: null,
                        onOk: function () {

                            const selected = this.picker._selected;
                            selected && thiz.onValueChanged(selected);

                        },
                        message: function (dlg) {
                            const thiz = dlg.owner;
                            const picker = createDeviceView($('<div class="container" style="height: inherit;" />'),ctx,driver,device,ci.pickerType);
                            thiz.picker = picker;
                            thiz.add(picker, null, false);
                            return $(picker.domNode);
                        },
                        onShow: function (dlg) {
                            const picker = this.picker;
                            const self = this;

                            picker.startup();
                            this.startDfd.resolve();
                        }
                    });


                    dlg.show().then(function () {
                        dlg.resize();
                        utils.resizeTo(dlg.picker, dlg, true, true);
                    });


                    this.add(dlg,null,false);


                } catch (e) {
                    logError(e);
                }
            },
            postMixInProperties: function () {

                this.inherited(arguments);

                if (this.userData && this.userData.title) {
                    this.title = this.userData.title;
                }

                if ((this.userData && this.userData.vertical === true) || this.vertical === true) {

                    this.templateString = "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
                        "<table border='0' cellpadding='5px' width='100%' >" +
                        "<tbody>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='100%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
                        "</tr>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='50%' class='widgetValue' valign='middle' attachTo='valueNode'></td>" +
                        "<td class='extension' width='25px' attachTo='button0'></td>" +
                        "<td class='extension' width='25px' attachTo='button1'></td>" +
                        "<td class='extension' width='25px' attachTo='button2'></td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
                        "<div attachTo='last'></div>" +
                        "</div>"
                }

            },
            onValueChanged:function(value){
                value = value.newValue || value;
                this.setValue(value);

                const ci = this.userData;
                const block = ci.block;
                const blockScope = block.scope;

                const title = blockScope.toFriendlyName(block, value);

                this.commandWidget._renderItem({
                    value:value,
                    label:title
                });



                this.commandWidget.set('value',value,title);
            },
            fillTemplate: function () {
                const thiz = this;

                let value = utils.toString(this.userData['value']);


                const ci = this.userData;
                const pickerType = ci.pickerType;
                const block = ci.block;
                const blockScope = block.scope;
                const ctx = blockScope.ctx;
                const driver = blockScope.driver;
                const device = blockScope.device;
                const deviceManager = ctx.getDeviceManager();
                const driverManager = ctx.getDriverManager();

                //add the Select Widget for local commands
                const CIS = [];

                const isCommand = pickerType == 'command';

                let title = isCommand ? 'Command' : 'Variable';
                const options = isCommand ? blockScope.getCommandsAsOptions() : blockScope.getVariablesAsOptions();







                //value = 'command://deviceScope=system_devices&device=e5a06e24-6aa4-c8c5-3ffc-9d84d85…9a66&driverScope=system_drivers&block=963944c2-c0b0-fe8a-504e-1b8bedd2a3cf';
                value = "command://deviceScope=system_devices&device=e5a06e24-6aa4-c8c5-3ffc-9d84d8528a91&driver=235eb680-cb87-11e3-9c1a-0800200c9a66&driverScope=system_drivers&block=6d0c5e0e-5c04-bb98-44a0-705c8269de07";

                if(value.indexOf('://')){
                    title = blockScope.toFriendlyName(block,value);
                }



                CIS.push(utils.createCI('value', 3, 'Volume', {
                    group: 'General',
                    title: title,
                    dst: pickerType,
                    //options: options,
                    widget: {
                        templateString: '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode">',
                        store:blockScope.blockStore,
                        labelField:'name',
                        valueField:'id',
                        search:true,
                        title:isCommand ? 'Command' : 'Variable',
                        query:{
                            group: isCommand ? 'basic' : 'basicVariables'
                        }
                    }
                }));


                if(pickerType == 'variable'){
                    //debugger;
                }


                factory.renderCIS(CIS, this.valueNode, this).then(function (widgets) {

                    const commandWidget = widgets[0];
                    thiz.commandWidget = commandWidget;

                    const btn = factory.createSimpleButton('', 'fa-magic', 'btn-default', {
                        style: ''
                    });

                    commandWidget._on('onValueChanged',function(e){
                        thiz.onValueChanged(e);
                    });


                    $(btn).click(function () {
                        thiz.onSelect(ctx,driver,device,isCommand ? 'command' : 'variable');
                    });
                    $(thiz.button0).append(btn);
                });
            },
            startup: function () {
                try {
                    this.fillTemplate();
                    this.onReady();
                }catch(e){
                    logError(e);
                }
            }
        });

        return Widget;

    }

    function testPicker(instance){
        const toolbar = ctx.mainView.getToolbar();
        const docker = ctx.mainView.getDocker();
        const parent  = TestUtils.createTab(null,null,module.id);
        const cisRenderer = dcl(CIGroupedSettingsView,{
            typeMap:null,
            getTypeMap:function(){
                if (this.typeMap) {
                    return this.typeMap;
                }
                const self = this;
                const typeMap = {};

                typeMap['Command'] = createPickerWidget();
                this.typeMap = typeMap;
                return typeMap;
            }
        });

        const ciView = utils.addWidget(cisRenderer,{
            cis:createPickerCIS(instance).inputs
        },null,parent,true);


        docker.resize();
    }

    if(test && ctx){

        console.clear();

        var parent  = TestUtils.createTab(null,null,module.id);
        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();
        const grid = null;






        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        marantzInstance = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        const driver = marantz.driver;
        const device = marantz.device;

        var parent  = TestUtils.createTab(null,null,module.id);

        const view = createDeviceView(parent,ctx,driver,device,'command');


        //testPicker(marantzInstance);
        ctx.getWindowManager().registerView(view,true);

    }




    return DeviceTreeView;
});
