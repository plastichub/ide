/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/dom-class',
    "dojo/on",
    "dojo/debounce",
    'dojo/dom-construct',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/Grid',
    'xaction/DefaultActions',
    'xgrid/Defaults',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xblox/views/BlockGrid',
    'xide/views/CIGroupedSettingsView',
    'xcf/model/Command',
    'xcf/model/Variable',
    'xide/widgets/ToggleButton',
    'xide/widgets/_ActionValueWidgetMixin',
    'xide/layout/AccordionContainer',
    "xide/widgets/TemplatedWidgetBase",
    "dijit/form/TextBox",
    'xcf/widgets/CommandSettings',
    'xblox/model/variables/VariableAssignmentBlock',
    'dojo/promise/all',
    "dojo/Deferred",
    "xblox/manager/BlockManager"

], function (declare, domClass,on,debounce,domConstruct,types,
             utils, ListRenderer, Grid, DefaultActions, Defaults, Focus,
             OnDemandGrid, EventedMixin, factory,CIViewMixin,BlockGrid,
             CIGroupedSettingsView,
             Command,Variable,
             ToggleButton,_ActionValueWidgetMixin,AccordionContainer,TemplatedWidgetBase,

             TextBox, CommandSettings,
             VariableAssignmentBlock,
             all,Deferred,BlockManager


) {

    const blox = {
        "blocks": [
            {
                "_containsChildrenIds": [],
                "id": "83de87c0-f8c7-74da-161d-8e9cf51d67b1",
                "name": "value",
                "value": "MVMAX 98",
                "type": "added",
                "group": "processVariables",
                "gui": false,
                "cmd": false,
                "declaredClass": "xcf.model.Variable",
                "save": false,
                "target": "None",
                "register": true,
                "readOnly": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0
            },
            {
                "_containsChildrenIds": [],
                "name": "PowerOn",
                "send": "pwon",
                "group": "basic",
                "id": "53a10527-709b-4c7d-7a90-37f58f17c8db",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "auto": "-1",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            },
            {
                "_containsChildrenIds": [],
                "name": "PowerOff",
                "send": "pwstandby",
                "group": "basic",
                "id": "84961334-9cd2-d384-25dc-a6b943e8cb8e",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "auto": "-1",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            },
            {
                "_containsChildrenIds": [],
                "name": "VolumeUp",
                "send": "var vol = [Volume] + 2;\nreturn \"mv\" + vol;\n\n",
                "group": "basic",
                "id": "fe9b04f0-052c-4244-12a4-ecb3ac0ae96a",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            },
            {
                "_containsChildrenIds": [],
                "name": "VolumeDown",
                "send": "var vol = [Volume] - 2;\nreturn \"mv\" + vol;",
                "group": "basic",
                "id": "948da268-618e-ebb2-0d52-661458421471",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            },
            {
                "_containsChildrenIds": [],
                "name": "PowerState",
                "send": "nada",
                "group": "basicVariables",
                "id": "31c98cdd-02a8-3af1-3a49-11955c0fad48",
                "declaredClass": "xcf.model.Variable",
                "gui": "off",
                "cmd": "off",
                "save": false,
                "target": "None",
                "type": "added",
                "value": "on",
                "register": true,
                "readOnly": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "initial": "on"
            },
            {
                "_containsChildrenIds": [],
                "name": "Volume",
                "type": "added",
                "value": 28,
                "enumType": "VariableType",
                "save": false,
                "initial": "10",
                "group": "basicVariables",
                "id": "3403a69e-252a-30dc-b130-40a028d1cde4",
                "register": true,
                "readOnly": false,
                "declaredClass": "xblox.model.variables.Variable",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0
            },
            {
                "_containsChildrenIds": [],
                "name": "Mute",
                "send": "return \"mv10\";\n",
                "group": "basic",
                "id": "7c3fbd22-b2ca-42d4-e048-2152faf07c4a",
                "declaredClass": "xcf.model.Command",
                "startup": false,
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            },
            {
                "_containsChildrenIds": [
                    "consequent"
                ],
                "group": "conditionalProcess",
                "condition": "[value]=='@PWR:2'",
                "id": "76b3132d-abfe-606a-98dd-377ce956d637",
                "autoCreateElse": true,
                "name": "if",
                "declaredClass": "xblox.model.logic.IfBlock",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added",
                "consequent": [
                    "7da7aeb4-6be9-442e-fd74-b299d97872b2"
                ]
            },
            {
                "_containsChildrenIds": [],
                "parentId": "76b3132d-abfe-606a-98dd-377ce956d637",
                "id": "7da7aeb4-6be9-442e-fd74-b299d97872b2",
                "name": "Log Message",
                "level": "error",
                "message": "return \"power is on!\";",
                "_type": "XBlox",
                "host": "this host",
                "declaredClass": "xblox.model.logging.Log",
                "enabled": true,
                "shareTitle": "",
                "description": "No Description",
                "canDelete": true,
                "order": 0,
                "type": "added"
            }
        ],
        "variables": null
    };



    const module = declare('nada',null,{});

    function getTestScope(){

        const _blockManager = new BlockManager({
            ctx:window.sctx
        });

        const _scope = new _blockManager.createScope('block_scope_id');

        return _scope;



    }

    module.createScope = getTestScope();


    return module;


});