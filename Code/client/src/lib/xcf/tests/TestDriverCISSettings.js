/** @module xgrid/Base **/
define([
    'dcl/dcl',
    "xdojo/declare",
    'dojo/dom-class',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/TreeRenderer',
    'xgrid/Grid',
    'xgrid/MultiRenderer',
    'xaction/DefaultActions',
    'dgrid/Editor',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    'xdocker/Docker2',
    "xide/views/CIViewMixin",
    'xide/layout/TabContainer',
    'xblox/views/BlockGrid',
    'xgrid/DnD',

    'xide/views/CIGroupedSettingsView',
    'xide/widgets/WidgetBase',

    'xide/views/_LayoutMixin',

    'xcf/model/Command',
    'xcf/model/Variable',

    'xide/widgets/ToggleButton',
    'xide/widgets/_ActionValueWidgetMixin',

    'xide/layout/AccordionContainer',


    "dijit/form/TextBox",
    'dijit/form/ValidationTextBox'



], function (dcl,declare, domClass,types,
             utils, ListRenderer, TreeRenderer, Grid, MultiRenderer, DefaultActions, Editor,Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,Docker,CIViewMixin,TabContainer,

             BlockGrid,
             Dnd,CIGroupedSettingsView,
             WidgetBase,_LayoutMixin,Command,Variable,
             ToggleButton,_ActionValueWidgetMixin,AccordionContainer,TextBox, ValidationTextBox

    ) {
    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;

    function createDriverCIS(driver,actionTarget){
        const CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": -1,
                    "id": "CF_DRIVER_NAME",
                    "name": "CF_DRIVER_NAME",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Display Name",
                    "type": 13,
                    "uid": "-1",
                    "value": "My Marantz",
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": -1,
                    "id": "CF_DRIVER_ID",
                    "name": "CF_DRIVER_ID",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 13,
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": -1,
                    "id": "CF_DRIVER_ICON",
                    "name": "CF_DRIVER_ICON",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Driver Icon",
                    "type": 18,
                    "uid": "-1",
                    "value": ".\/project1\/318i.jpg",
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": -1,
                    "id": "CF_DRIVER_CLASS",
                    "name": "CF_DRIVER_CLASS",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Driver Class",
                    "type": 4,
                    "uid": "-1",
                    "value": ".\/Marantz\/MyMarantz.js",
                    "visible": true
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": "Settings",
                    "id": "CommandSettings",
                    "name": "CF_DRIVER_COMMANDS",
                    "order": 1,
                    "params": "{\"constants\":{\"start\":\"\",\"end\":\"\"},\"send\":{\"mode\":false,\"interval\":\"\",\"timeout\":\"\",\"onReply\":\"MV36\\\\r@VOL:-440\\\\r\"}}",
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Commands",
                    "type": "CommandSettings2",
                    "uid": "-1",
                    "value": "[{\"_containsChildrenIds\":[\"items\"],\"comparator\":\"==\",\"expression\":\"'PWON'\",\"id\":\"c5e54ce3-2c87-5e55-254d-72bdf3c97043\",\"items\":[\"3dacd32d-79de-b7db-737e-8748a8d3b499\"],\"parentId\":\"00e8d26b-0017-451a-4cde-6ba088477940\",\"declaredClass\":\"xblox.model.logic.CaseBlock\",\"name\":\"Case\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[\"items\"],\"comparator\":\"==\",\"expression\":\"'PWSTANDBY'\",\"id\":\"fa2b9010-58c1-d632-9814-622ce35cedb2\",\"items\":[\"6b01aad7-1c40-45a0-94b9-f841a7ada7c3\"],\"parentId\":\"00e8d26b-0017-451a-4cde-6ba088477940\",\"declaredClass\":\"xblox.model.logic.CaseBlock\",\"name\":\"Case\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"PowerState\",\"value\":\"On\",\"parentId\":\"c5e54ce3-2c87-5e55-254d-72bdf3c97043\",\"id\":\"3dacd32d-79de-b7db-737e-8748a8d3b499\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"PowerState\",\"value\":\"Off\",\"parentId\":\"fa2b9010-58c1-d632-9814-622ce35cedb2\",\"id\":\"6b01aad7-1c40-45a0-94b9-f841a7ada7c3\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[\"items\"],\"name\":\"FadeVolumeUp\",\"send\":\"\",\"group\":\"conditional\",\"id\":\"068fe05a-8d9c-dfc8-219c-f99ed7f9eb9d\",\"items\":[\"241be60e-93fa-8d9b-ad41-4c5c13a726ec\"],\"auto\":\"false\",\"declaredClass\":\"xcf.model.Command\",\"startup\":false,\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[\"items\"],\"condition\":\"[Volume]<=80\",\"parentId\":\"068fe05a-8d9c-dfc8-219c-f99ed7f9eb9d\",\"id\":\"241be60e-93fa-8d9b-ad41-4c5c13a726ec\",\"declaredClass\":\"xblox.model.loops.WhileBlock\",\"loopLimit\":200,\"name\":\"While\",\"wait\":\"0\",\"currentIndex\":32,\"serializeMe\":true,\"enabled\":true,\"order\":0,\"items\":[\"55e08ce2-11e1-e9b6-da4c-72b3475bb745\",\"96f75e8e-ac01-acf3-1562-fed24bc7b07f\"],\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[\"items\"],\"name\":\"FadeVolumeDown\",\"send\":\"\",\"group\":\"conditional\",\"id\":\"30fc6ba9-1700-a3df-b05e-5f140f96dc77\",\"items\":[\"47142697-5a19-b953-8362-63ab5fca844b\"],\"auto\":\"\",\"declaredClass\":\"xcf.model.Command\",\"startup\":false,\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[\"items\"],\"initial\":\"[Volume]\",\"comparator\":\">=0\",\"final\":\"0\",\"modifier\":\"-10\",\"counterName\":\"value\",\"parentId\":\"30fc6ba9-1700-a3df-b05e-5f140f96dc77\",\"id\":\"47142697-5a19-b953-8362-63ab5fca844b\",\"declaredClass\":\"xblox.model.loops.ForBlock\",\"name\":\"For\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"items\":[\"af4bf911-1d5c-c952-60b1-07ddf88900a8\",\"28814e91-087d-172c-a8c6-bc1d1a44aa87\"],\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]-10\",\"parentId\":\"47142697-5a19-b953-8362-63ab5fca844b\",\"id\":\"28814e91-087d-172c-a8c6-bc1d1a44aa87\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"condition\":\"{PowerStandby}\",\"parentId\":\"47142697-5a19-b953-8362-63ab5fca844b\",\"id\":\"af4bf911-1d5c-c952-60b1-07ddf88900a8\",\"command\":\"SetMasterVolume\",\"declaredClass\":\"xblox.model.functions.CallBlock\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"id\":\"91bb04d7-8976-4d63-8f31-ed4ed93b61b8\",\"declaredClass\":\"xblox.model.code.CallMethod\",\"name\":\"Call Method\",\"method\":\"\",\"args\":\"\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[\"items\"],\"group\":\"conditionalProcess\",\"id\":\"00e8d26b-0017-451a-4cde-6ba088477940\",\"items\":[\"fa2b9010-58c1-d632-9814-622ce35cedb2\",\"c5e54ce3-2c87-5e55-254d-72bdf3c97043\"],\"variable\":\"value\",\"declaredClass\":\"xblox.model.variables.VariableSwitch\",\"name\":\"Switch on Variable\",\"startup\":false,\"auto\":0,\"send\":\"\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"condition\":\"{PowerStandby}\",\"id\":\"1d77ac8b-3279-4a9d-01a1-49b5d0fe19d2\",\"declaredClass\":\"xblox.model.functions.CallBlock\",\"command\":\"PowerStandby\",\"serializeMe\":true,\"enabled\":true,\"renderBlockIcon\":true,\"order\":0,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]+1\",\"parentId\":\"241be60e-93fa-8d9b-ad41-4c5c13a726ec\",\"id\":\"3db498eb-6e7b-33d0-c558-d50ebc077de7\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]+1\",\"parentId\":\"241be60e-93fa-8d9b-ad41-4c5c13a726ec\",\"id\":\"81137b40-afea-57f5-5675-bc852c7a03ce\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]+1\",\"parentId\":\"241be60e-93fa-8d9b-ad41-4c5c13a726ec\",\"id\":\"d9d55e04-da33-d469-4af6-d88d187cef54\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]+11\",\"id\":\"55e08ce2-11e1-e9b6-da4c-72b3475bb745\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]+1\",\"id\":\"b875cf76-322f-9c35-408b-a4796f8ce47d\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]+1\",\"id\":\"bd7efb82-b2cf-b0b8-1329-d6aa32431689\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"name\":\"Set Variable\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"condition\":\"{PowerStandby}\",\"id\":\"96f75e8e-ac01-acf3-1562-fed24bc7b07f\",\"command\":\"SetMasterVolume\",\"declaredClass\":\"xblox.model.functions.CallBlock\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"name\":\"PowerOn\",\"send\":\"pwon\",\"group\":\"basic\",\"id\":\"bba495ad-e5de-d4d9-1550-7ea9e2fa7a0a\",\"startup\":false,\"declaredClass\":\"xcf.model.Command\",\"auto\":\"\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"name\":\"PowerStandby\",\"send\":\"pwstandby\",\"group\":\"basic\",\"id\":\"4313d13a-6fad-85e3-cf26-2cbac23c4934\",\"declaredClass\":\"xcf.model.Command\",\"startup\":false,\"auto\":0,\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[\"items\"],\"name\":\"VolUp\",\"send\":\"\",\"group\":\"conditional\",\"id\":\"534c27f0-048e-a7dc-a51b-7c9d5730b037\",\"items\":[\"31e880bf-9331-45f6-ed5a-5d37a8ff875c\",\"27c71597-16ce-b350-3fcd-0c6d58897c83\"],\"declaredClass\":\"xcf.model.Command\",\"startup\":false,\"enabled\":true,\"serializeMe\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true,\"renderBlockIcon\":true,\"order\":0},{\"_containsChildrenIds\":[],\"condition\":\"{PowerStandby}\",\"id\":\"31e880bf-9331-45f6-ed5a-5d37a8ff875c\",\"command\":\"SetMasterVolume\",\"declaredClass\":\"xblox.model.functions.CallBlock\",\"serializeMe\":true,\"enabled\":true,\"order\":0,\"renderBlockIcon\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true},{\"_containsChildrenIds\":[],\"variable\":\"Volume\",\"value\":\"[Volume]-1\",\"parentId\":\"534c27f0-048e-a7dc-a51b-7c9d5730b037\",\"id\":\"27c71597-16ce-b350-3fcd-0c6d58897c83\",\"name\":\"Set Variable\",\"declaredClass\":\"xblox.model.variables.VariableAssignmentBlock\",\"enabled\":true,\"serializeMe\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true,\"renderBlockIcon\":true,\"order\":0},{\"_containsChildrenIds\":[\"items\"],\"name\":\"Test Command\",\"send\":\"\",\"group\":\"basic\",\"id\":\"52197b7f-8f6e-8a60-820f-f882f8442e76\",\"items\":[\"16733e2d-ba8e-daa3-f928-0ce7effd5ace\"],\"declaredClass\":\"xcf.model.Command\",\"startup\":false,\"enabled\":true,\"serializeMe\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true,\"renderBlockIcon\":true,\"order\":0},{\"_containsChildrenIds\":[],\"parentId\":\"52197b7f-8f6e-8a60-820f-f882f8442e76\",\"id\":\"16733e2d-ba8e-daa3-f928-0ce7effd5ace\",\"name\":\"Call Method\",\"method\":\"testCall\",\"args\":\"\",\"declaredClass\":\"xblox.model.code.CallMethod\",\"enabled\":true,\"serializeMe\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true,\"renderBlockIcon\":true,\"order\":0},{\"_containsChildrenIds\":[],\"name\":\"SetMasterVolume\",\"send\":\"mv[Volume2]\",\"group\":\"conditional\",\"id\":\"b6fe0504-e175-f695-eeea-dce8c4beef06\",\"declaredClass\":\"xcf.model.Command\",\"startup\":false,\"enabled\":true,\"serializeMe\":true,\"shareTitle\":\"\",\"description\":\"No Description\",\"canDelete\":true,\"renderBlockIcon\":true,\"order\":0}]",
                    "visible": true,
                    "view": true
                }
            ]
        };


        _.each(CIS.inputs,function(ci){
            ci.driver=driver;
            ci.actionTarget=actionTarget;
        });

        return CIS;
    }

    function completeGrid(_grid) {

        _grid._on('onAddActions', function (evt) {

            const addAction = evt.addAction, cmdAction = 'New/Command', varAction = 'New/Variable', permissions = evt.permissions, VISIBILITY = types.ACTION_VISIBILITY, thiz = this;


            addAction('Command', cmdAction, 'el-icon-plus-sign', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    dummy: true,
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);

            addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    dummy: true,
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);


            addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {
                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                            widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                            widgetArgs: {
                                icon1: 'fa-toggle-on',
                                icon2: 'fa-toggle-off',
                                delegate: thiz,
                                checked: false,
                                iconClass: 'fa-toggle-off'
                            }
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });



        });


        _grid._on('selectionChanged', function (evt) {
            //console.log('selection ',evt);
            //since we can only add blocks to command and not
            //at root level, disable the 'Block/Insert' root action and
            //its widget //references
            const thiz = this, selection = evt.selection, item = selection[0], blockInsert = thiz.getAction('Block/Insert'), blockEnable = thiz.getAction('Step/Enable');

            disable = function (disable) {
                blockInsert.set('disabled', disable);
                setTimeout(function () {
                    blockInsert.getReferences().forEach(function (ref) {
                        ref.set('disabled', disable);
                    });
                }, 100);

            };

            const _disable =item ? false : true;

            disable(_disable);


            if (item) {
                blockEnable.getReferences().forEach(function (ref) {
                    ref.set('checked', item.enabled);
                });
            }else{
                /*
                var props = _grid.getPropertyStruct();
                props._lastItem = null;
                _grid.setPropertyStruct(props);*/
            }
        });

        _grid.startup();

    }

    const propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };

    function createGridClass(overrides) {

        const gridClass = declare('driverGrid', BlockGrid,{
            highlightDelay:1000,
            propertyStruct : propertyStruct,
            /**
             * Step/Run action
             * @param block {block[]}
             * @returns {boolean}
             */
            execute: function (blocks) {

                function run(block) {
                    if (!block || !block.scope) {
                        console.error('have no scope');
                        return;
                    }
                    try {
                        const result = block.scope.solveBlock(block, {
                            highlight: true,
                            force: true
                        });
                    } catch (e) {
                        console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                        console.error(printStackTrace().join('\n\n'));
                    }
                    return true;
                }

                blocks  = _.isArray(blocks) ? blocks : [blocks];


                _.each(blocks,run);

            },

            onCIChanged: function (ci, block, oldValue, newValue, field) {
                console.log('on ci changed', arguments);
                block.set(field, newValue);
            },
            _itemChanged: function (type, item, store) {

                store = store || this.getStore(item);

                const thiz = this;

                function _refreshParent(item, silent) {

                    const parent = item.getParent();
                    if (parent) {
                        const args = {
                            target: parent
                        };
                        if (silent) {
                            this._muteSelectionEvents = true;
                        }
                        store.emit('update', args);
                        if (silent) {
                            this._muteSelectionEvents = false;
                        }
                    } else {
                        thiz.refresh();
                    }
                }

                function select(item) {

                    thiz.select(item, null, true, {
                        focus: true,
                        delay: 20,
                        append: false
                    });
                }

                switch (type) {

                    case 'added':
                    {
                        //_refreshParent(item);
                        //this.deselectAll();
                        this.refresh();
                        select(item);
                        break;
                    }

                    case 'changed':
                    {
                        this.refresh();
                        select(item);
                        break;
                    }


                    case 'moved':
                    {
                        //_refreshParent(item,true);
                        //this.refresh();
                        //select(item);
                        break;
                    }

                    case 'deleted':
                    {

                        const parent = item.getParent();
                        //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                        const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                        if (parent) {
                            const _container = parent.getContainer();
                            if (_container) {
                                _.each(_container, function (child) {
                                    if (child.id == item.id) {
                                        _container.remove(child);
                                    }
                                });
                            }
                        }

                        this.refresh();
                        /*
                         if (_prev) {
                         select(_prev);
                         }
                         */
                        break;
                    }

                }


            },
            _onFocusChanged:function(focused,type){
                this.inherited(arguments);
                if(!focused){
                    this._lastSelection = [];
                }






            },
            showProperties: function (item,force) {


                const block = item || this.getSelection()[0], thiz = this, rightSplitPosition= thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);





                if(!block || rightSplitPosition==1) {
                    console.log(' show properties: abort',[block , rightSplitPosition]);
                    return;
                }
                const right = this.getRightPanel();
                let props = this.getPropertyStruct();

                if (item == props._lastItem && force!==true) {
                    console.log('show propertiess : same item');
                    //return;
                }

                this.clearPropertyPanel();
                props = this.getPropertyStruct();


                props._lastItem = item;

                const _title = item.name || item.title;

                console.log('show properties for ' + _title+ ' : ' + item.declaredClass,props);


                let tabContainer = props.targetTop;

                if (!tabContainer) {

                    tabContainer = utils.addWidget(AccordionContainer, {
                        delegate: this,
                        tabStrip: true,
                        tabPosition: "top",
                        attachParent: true,
                        style: "width:100%;height:100%;overflow-x:hidden;",
                        allowSplit: false
                    }, null, right.containerNode, true);

                    props.targetTop = tabContainer;
                }

                if (tabContainer.selectedChildWidget) {
                    props.lastSelectedTopTabTitle = tabContainer.selectedChildWidget.title;
                } else {
                    props.lastSelectedTopTabTitle = 'General';
                }


                _.each(tabContainer.getChildren(), function (tab) {
                    tabContainer.removeChild(tab);
                });

                if (props.currentCIView) {
                    props.currentCIView.empty();
                }

                if (!block.getFields) {
                    console.log('have no fields', block);
                    return;
                }

                const cis = block.getFields();
                for (var i = 0; i < cis.length; i++) {
                    cis[i].vertical = true;
                }

                const ciView = new CIViewMixin({

                    initWithCIS: function (data) {
                        this.empty();

                        data = utils.flattenCIS(data);

                        this.data = data;

                        const thiz = this;

                        let groups = _.groupBy(data,function(obj){
                            return obj.group;
                        });

                        const groupOrder = this.options.groupOrder || {};

                        groups = this.toArray(groups);

                        const grouped = _.sortByOrder(groups, function(obj){
                            return groupOrder[obj.name] || 100;
                        });

                        if (grouped != null && grouped.length > 1) {
                            this.renderGroups(grouped);
                        } else {
                            this.widgets = factory.createWidgetsFromArray(data, thiz, null, false);
                            if (this.widgets) {
                                this.attachWidgets(this.widgets);
                            }
                        }
                    },
                    tabContainer: props.targetTop,
                    delegate: this,
                    viewStyle: 'padding:0px;',
                    autoSelectLast: true,
                    item: block,
                    source: this.callee,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Advanced': 2,
                            'Script':3,
                            'Arguments':4,
                            'Description':5,
                            'Share':6

                        }
                    },
                    cis: cis
                });

                ciView.initWithCIS(cis);


                props.currentCIView = ciView;

                if (block.onFieldsRendered) {
                    block.onFieldsRendered(block, cis);
                }


                ciView._on('valueChanged', function (evt) {
                    //console.log('ci value changed ', evt);
                    thiz.onCIChanged(evt.ci,block,evt.oldValue,evt.newValue,evt.ci.dst);
                });






                const containers = props.targetTop.getChildren();
                let descriptionView = null;
                for (var i = 0; i < containers.length; i++) {

                    // @TODO : why is that not set?
                    containers[i].parentContainer = props.targetTop;

                    // track description container for re-rooting below
                    if (containers[i].title === 'Description') {
                        descriptionView = containers[i];
                    }

                    if (props.targetTop.selectedChildWidget.title !== props.lastSelectedTopTabTitle) {
                        if (containers[i].title === props.lastSelectedTopTabTitle) {
                            props.targetTop.selectChild(containers[i]);
                        }
                    }
                }

                props.targetTop.resize();

                this.setPropertyStruct(props);


                this._docker.resize();
            },
            runAction: function (action) {

                const thiz = this;
                const sel = this.getSelection();

                function addItem(_class,group){

                    const cmd = factory.createBlock(_class, {
                        name: "No Title",
                        send: "nada",
                        scope: thiz.blockScope,
                        group: group
                    });

                    thiz.deselectAll();


                    setTimeout(function () {
                        thiz.focus(thiz.row(cmd));
                        thiz._itemChanged('added', cmd);
                    }, 100);
                }


                if (action.command == 'New/Command') {
                    addItem(Command,'basic');
                }
                if (action.command == 'New/Variable') {
                    addItem(Variable,'basicVariables');
                }
                return this.inherited(arguments);
            },


            startup:function(){

                this.inherited(arguments);
                const thiz = this;

                function _node(evt){
                    const item = evt.callee;
                    if(item) {
                        const row = thiz.row(item);
                        if (row) {
                            const element = row.element;
                            if (element) {
                                return $(element);
                            }
                        }
                    }
                    return null;
                };




                function mark(element,cssClass){

                    if(element) {
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.addClass(cssClass);
                        setTimeout(function () {
                            element.removeClass(cssClass);
                            thiz._isHighLighting = false;
                        }, thiz.highlightDelay);
                    }
                }

                this.subscribe(types.EVENTS.ON_RUN_BLOCK,function(evt){
                    mark(_node(evt),'activeBlock');
                });
                this.subscribe(types.EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                    mark(_node(evt),'failedBlock');
                });
                this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                    mark(_node(evt),'successBlock');
                });

            }
        });
        return gridClass;
    }

    function addLog(tab){




        //return;

        /**
         * Block grid base class.
         * @class module:xblox/views/Grid
         */
        const GridClass = Grid.createGridClass('log',
            {
                options: utils.clone(types.DEFAULT_GRID_OPTIONS)
            },
            //features
            {

                SELECTION: true,
                KEYBOARD_SELECTION: true,
                PAGINATION: false,
                ACTIONS: types.GRID_FEATURES.ACTIONS
                //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                //CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                /*,
                BLOCK_ACTIONS:{
                    CLASS:BlockActions
                },
                DND:{
                    CLASS:DnD//declare('splitMixin',Dnd,_dndMixin)
                }
                */

            },
            {
                //base flip
                RENDERER: ListRenderer

            },
            {
                //args
                /*renderers: renderers,
                selectedRenderer: TreeRenderer*/
            },
            {
                GRID: OnDemandGrid,
                EDITOR: null,
                LAYOUT: null,
                DEFAULTS: Defaults,
                RENDERER: ListRenderer,
                EVENTED: EventedMixin,
                FOCUS: Focus
            }
        );


        const logManager = ctx.getLogManager();

        const store = logManager.store;

        const message={
            message:'test',
            level:'info',
            type:'device',
            details:{},
            terminatorMessage:null
        };





        factory.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, message);


        //logManager.addLoggingMessage()


        // default grid args
        const gridArgs = {
            ctx:ctx,
            attachDirect:true,
            collection: store.filter({
                show:true
            })
        };







        const logGridClass = declare("xlog.views.LogView", GridClass, {
            postMixInProperties: function () {
                this.columns = this.getColumns();
                return this.inherited(arguments);
            },
            formatDateSimple: function (data, format) {
                const momentUnix = moment.unix(data);
                return momentUnix.format("MMMM Do, h:mm:ss a");
            },
            getDefaultSort:function(){
                return [{property: 'time', descending: true}];
            },
            getMessageFormatter: function (message, item) {
                const thiz = this;
                if(item.progressHandler && !item._subscribedToProgress){
                    item.progressHandler._on('progress',function(_message){
                        thiz.refresh();
                    });
                    item._subscribedToProgress = true;
                }

                let _isTerminated = item.terminatorMessage !==null && item._isTerminated===true;
                if(!item.terminatorMessage){
                    _isTerminated = true;
                }

                if (!_isTerminated) {
                    return '<span class=\"fa-spinner fa-spin\" style=\"margin-right: 4px\"></span>' + message;
                }
                return message;
            },
            getColumns: function () {
                const thiz = this;
                const columns = {
                    Level: {
                        field: "level", // get whole item for use by formatter
                        label: "Level",
                        sortable: true,
                        formatter: function (level) {

                            switch (level) {
                                case 'info':
                                {
                                    return '<span style=\"color:gray\">' + level + '</span>';
                                }
                                case 'error':
                                {
                                    return '<span style=\"color:red\">' + level + '</span>';
                                }
                                case 'warning':
                                {
                                    return '<span style=\"color:orange\">' + level + '</span>';
                                }
                            }
                            return level;
                        }

                    },
                    Type: {
                        field: "type", // get whole item for use by formatter
                        label: "Type",
                        sortable: true
                    },
                    Host: {
                        field: "host", // get whole item for use by formatter
                        label: "Host",
                        sortable: true
                    },
                    Message: {
                        field: "message", // get whole item for use by formatter
                        label: "Message",
                        sortable: true,
                        formatter: function (message, item) {
                            return thiz.getMessageFormatter(message, item)
                        }
                    },
                    Time: {
                        field: "time", // get whole item for use by formatter
                        label: "Time",
                        sortable: true,
                        formatter: function (time) {
                            return thiz.formatDateSimple(time / 1000);
                        }
                    }/*,
                    Details:{
                        field: "details", // get whole item for use by formatter
                        label: "Details",
                        sortable: false,
                        editor:RowDetailEditor
                    }*/
                };



                if (!this.showSource) {
                    delete columns['Host'];
                }
                return columns;
            }
        });
        //tab.select();
        const grid = utils.addWidget(logGridClass,gridArgs,null,tab,false);
        grid.startup();


    }

    function onWidgetCreated(basicTab,condTab,varTab,logTab){
        addLog(logTab);
    }

    function createCommandSettingsWidget(){


        const _class = declare("xcf.widgets.CommandSettings2", [WidgetBase,_LayoutMixin], {

            templateString:'<div style="width: inherit;height: 100%;"><div>',
            _docker:null,
            grids:null,
            getDocker:function(){
                if(!this._docker){
                    this._docker = Docker.createDefault(this.domNode,{
                        allowContextMenu:false
                    });
                    this._docker.$container.css('top',0);
                }
                return this._docker;
            },
            createWidgets:function(){



                const docker = this.getDocker(this.domNode),
                      ci = this.userData,
                      driver = ci.driver,
                      scope = driver.blockScope,
                      store = scope.blockStore,
                      //variableStore = scope.variableStore,

                      instance = driver.instance,
                      widget = this,
                      defaultTabArgs = {
                          icon:false,
                          closeable:false,
                          movable:false,
                          moveable:true,
                          tabOrientation:types.DOCKER.TAB.TOP,
                          location:types.DOCKER.DOCK.STACKED
                      },
                      // 'Basic' commands tab
                      basicCommandsTab = docker.addTab('DefaultFixed',
                          utils.mixin(defaultTabArgs,{
                              title: 'Basic Commands',
                              h:'90%'
                          })),
                      // 'Conditional' commands tab
                      condCommandsTab = docker.addTab('DefaultFixed',
                          utils.mixin(defaultTabArgs,{
                              title: 'Conditional Commands',
                              target:basicCommandsTab,
                              select:false,
                              h:'90%',
                              tabOrientation:types.DOCKER.TAB.TOP
                          }));


                    // 'Variables' tab
                    const variablesTab = docker.addTab(null,
                        utils.mixin(defaultTabArgs,{
                            title: 'Variables',
                            //target:condCommandsTab,
                            select:false,
                            h:100,
                            tabOrientation:types.DOCKER.TAB.BOTTOM,
                            location:types.DOCKER.TAB.BOTTOM
                        }));


                var group = 'basic';

                const logsTab = null;
/*
                var logsTab = docker.addTab(null,
                    utils.mixin(defaultTabArgs,{
                        title: 'Log',
                        target:variablesTab,
                        select:false,
                        tabOrientation:types.DOCKER.TAB.BOTTOM,
                        location:types.DOCKER.DOCK.STACKED
                    }));
                */

                //logsTab.initSize('100%','10%');

                //variablesTab.getFrame().showTitlebar(false);










                // prepare right property panel but leave it closed
                this.getRightPanel(null,1);

                // default grid args
                const gridArgs = {
                    _getRight:function(){
                        return widget.__right;
                    },
                    ctx:ctx,
                    blockScope: scope,
                    blockGroup: 'basic',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'basic'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    __right:this.__right,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition
                };


                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //basic commands

                grid = utils.addWidget(createGridClass(),gridArgs,null,basicCommandsTab,false);
                completeGrid(grid,'Command');

                docker._on(types.DOCKER.EVENT.SPLITTER_POS_CHANGED,function(evt){

                    const position = evt.position, splitter = evt.splitter, right = widget.__right;

                    if(right && splitter == right.getSplitter()){
                        if(position<1){
                            grid.showProperties(grid.getSelection()[0],true);
                        }
                    }
                });


                //run some actions
                const cmdAction = grid.getAction('New/Command'), reloadAction = grid.getAction('File/Reload');

                grid.openRight(true);
                /*
                grid.select([0],null,true,{
                    focus:true
                });
                */
                this.grids.push(grid);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //conditional commands
                const gridArgs2 = {
                    ctx:ctx,
                    _getRight:function(){
                        return widget.__right;
                    },
                    blockScope: scope,
                    blockGroup: 'conditional',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'conditional'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    __right:this.__right,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition
                };

                const grid2 = utils.addWidget(createGridClass(),gridArgs2,null,condCommandsTab,false);

                completeGrid(grid2,'Command');

                this.grids.push(grid2);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //variables
                var group = 'basicVariables';

                const gridArgs3 = {
                    _getRight:function(){
                        return widget.__right;
                    },
                    ctx:ctx,
                    blockScope: scope,
                    blockGroup: 'basicVariables',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'basicVariables'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition,
                    showHeader:true,
                    columns:[
                        {
                            label: "Name",
                            field: "title",
                            sortable: true,
                            editorArgs: {
                                required: true,
                                promptMessage: "Enter a unique variable name",
                                //validator: thiz.variableNameValidator,
                                //delegate: thiz.delegate,
                                intermediateChanges: false
                            },
                            _editor: ValidationTextBox
                        },
                        {
                            label: "Initialize",
                            field: "initialize",
                            sortable: false,
                            editor: TextBox,
                            editOn:'click'
                        },
                        {
                            label: "Value",
                            field: "value",
                            sortable: false,
                            editor: TextBox,
                            editOn:'click',
                            editorArgs: {
                                autocomplete:'on',
                                templateString:'<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"'+
                                                '><div class="dijitReset dijitInputField dijitInputContainer"'+
                                                '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"'+
                                                '${!nameAttrSetting} type="${type}"'+
                                                '/></div'+
                                                '></div>'
                            }
                        }
                    ]
                };





                const grid3 = utils.addWidget(createGridClass(),gridArgs3,null,variablesTab,false);
                completeGrid(grid3,'Variable');


                domClass.add(grid3.domNode,'variableSettings');


                grid3.on("dgrid-datachange", function (evt) {

                    const cell = evt.cell;

                    //normalize data
                    let item = null;
                    if (cell && cell.row && cell.row.data) {
                        item = cell.row.data;
                    }
                    const id = evt.rowId;
                    const oldValue = evt.oldValue;
                    const newValue = evt.value;

                    const data = {
                        item: item,
                        id: id,
                        oldValue: oldValue,
                        newValue: newValue,
                        grid: grid3,
                        field: cell.column.field
                    };

                    if (item) {
                        item[data.field] = data.newValue;
                    }




                });

                this.grids.push(grid3);
                //variablesTab.select();
                docker.resize();
                grid3.refresh();
                basicCommandsTab.select();
                docker.resize();

                setTimeout(function(){

                    /*variablesTab.getFrame().showTitlebar(true);*/

                    function hide(who,show) {

                        const prop = show ? 'inherit' : 'none', what = 'display';

                        if(who.$titleBar) {
                            who.$titleBar.css(what, prop);
                            who.$center.css('top', show ? 30 : 0);
                        }
                    }

                    hide(variablesTab.getFrame(),false);
                    variablesTab.getSplitter().pos(0.6);
                    variablesTab.select();
                    onWidgetCreated(basicCommandsTab,condCommandsTab,variablesTab,logsTab);
                },10);

            },
            startup:function(){

                this.inherited(arguments);
                const thiz = this;
                this.grids = [];
                /*setTimeout(function(){*/
                thiz.createWidgets();
                const ci = this.userData;
                if(ci.actionTarget){
                    _.each(this.grids,function(grid){
                        ci.actionTarget.addActionEmitter(grid);
                    });
                }
                /*},1000);*/
            }

        });


        dojo.setObject('xcf.widgets.CommandSettings2',_class);


        return _class;
    }


    function getFileActions(permissions) {



        const result = [], ACTION = types.ACTION, ACTION_ICON = types.ACTION_ICON, VISIBILITY = types.ACTION_VISIBILITY, thiz = this, actionStore = thiz.getActionStore();


        return [];

        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            let action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        const who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }

        /*
         var rootAction = 'Block/Insert';
         permissions.push(rootAction);
         addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         dummy: true,
         onCreate: function (action) {
         action.setVisibility(VISIBILITY.CONTEXT_MENU, {
         label: 'Add'
         });

         }
         }, null, null);
         permissions.push('Block/Insert Variable');


         addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         }, null, null);
         */

        /*
         permissions.push('Clipboard/Paste/New');
         addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
         }, null, null);*/


        const newBlockActions = this.getAddActions();
        const addActions = [];
        let levelName = '';


        function addItems(commandPrefix, items) {

            for (let i = 0; i < items.length; i++) {
                const item = items[i];

                levelName = item.name;


                const path = commandPrefix + '/' + levelName;
                const isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }
        //console.clear();
        //addItems(rootAction, newBlockActions);
        //return result;


        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            const item = selection[0];
            const canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

            return !canMove;

        }


        function canParent(selection, reference, visibility) {

            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            const item = selection[0];
            if(!item){
                console.warn('bad item',selection);
                return false;
            }

            if(this.command === 'Step/Move Left'){
                return !item.getParent();
            }else{
                return item.getParent();
            }
            /*
             var canMove = item.canMove(item, this.command === 'Step/Move Left' ? -1 : 1);
             return !canMove;*/

            return true;

        }

        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */

        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    widgetArgs:{
                        label: ' ',
                        style:'font-size:25px!important;'
                    }
                });

            }
        }, null, isItem);
        permissions.push('Step/Run/From here');

        /**
         * run
         */

        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {

            onCreate: function (action) {

            }
        }, null, isItem);



        /**
         * move
         */

        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['alt up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);


        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['alt down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);
        /*


         permissions.push('Step/Edit');
         addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
         */
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        permissions.push('Step/Move Left');
        permissions.push('Step/Move Right');

        addAction('Move Left', 'Step/Move Left', 'fa-arrow-left', ['alt left'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canParent);

        addAction('Move Right', 'Step/Move Right', 'fa-arrow-right', ['alt right'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });


            }
        }, null, canParent);


        return result;

    }

    function doTests(){
    }


    const createDelegate = function(){

        return {

        }


    };


    function openDriverSettings(driver){


        //createCommandSettingsWidget();


        const toolbar = ctx.mainView.getToolbar();


        CIS = createDriverCIS(driver,toolbar);




        const docker = ctx.mainView.getDocker();
        let parent  = window.driverTab;
        if(parent){
            docker.removePanel(parent);
        }

        parent = docker.addTab(null, {
            title: 'Marantz/Marantz',
            icon: 'fa-exchange'
        });

        window.driverTab = parent;



        const cisRenderer = dcl([CIGroupedSettingsView],{


            createGroupContainer: function () {

                if (this.tabContainer) {
                    return this.tabContainer;
                }
                const tabContainer = utils.addWidget(TabContainer,{
                    tabStrip: true,
                    tabPosition: "left-h",
                    splitter: true,
                    style: "min-width:450px;",
                    "className": "ui-widget-content"
                },null,this.containerNode,true);
                this.tabContainer = tabContainer;
                return tabContainer;
            },
            attachWidgets: function (data, dstNode,view) {

                const thiz = this;
                this.helpNodes = [];
                this.widgets = [];

                dstNode = dstNode || this.domNode;
                if(!dstNode && this.tabContainer){
                    dstNode = this.tabContainer.containerNode;
                }
                if (!dstNode) {
                    console.error('have no parent dstNode!');
                    return;
                }


                for (let i = 0; i < data.length; i++) {
                    const widget = data[i];
                    widget.delegate = this.owner || this;
                    dstNode.appendChild(widget.domNode);
                    widget.startup();

                    widget._on('valueChanged',function(evt){
                        thiz.onValueChanged(evt);
                    });

                    this.widgets.push(widget);
                    widget.userData.view=view;
                }
            },
            initWithCIS: function (data) {
                this.empty();

                data = utils.flattenCIS(data);

                this.data = data;

                const thiz = this;

                let groups = _.groupBy(data,function(obj){
                    return obj.group;
                });

                const groupOrder = this.options.groupOrder || {};

                groups = this.toArray(groups);

                const grouped = _.sortByOrder(groups, function(obj){
                    return groupOrder[obj.name] || 100;
                });

                if (grouped != null && grouped.length > 1) {
                    this.renderGroups(grouped);
                } else {
                    this.widgets = factory.createWidgetsFromArray(data, thiz, null, false);
                    if (this.widgets) {
                        this.attachWidgets(this.widgets);
                    }
                }
            }
        });


        const view = utils.addWidget(CIGroupedSettingsView, {
            title:  'title',
            cis: driver.user.inputs,
            storeItem: driver,
            delegate: createDelegate(),
            storeDelegate: this,
            iconClass: 'fa-eye',
            blockManager: ctx.getBlockManager(),
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'General'
            }
        }, null, parent, true);


        docker.resize();
        view.resize();
    }

    /*
         * playground
         */
    const _lastGrid = window._lastGrid;
    var ctx = window.sctx;
    const ACTION = types.ACTION;
    var root;
    let scope;
    let blockManager;
    let driverManager;
    let marantz;



    const _actions = [
        ACTION.RENAME
    ];


    function fixScope(scope){

        return scope;

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function(source,target,before,add){




            console.log('scope::move, add: ' +add,arguments);

            if(!add){
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if(add){

                //remove it from the source parent and re-parent the source
                if(target.canAdd && target.canAdd()){

                    var sourceParent = this.getBlockById(source.parentId);
                    if(sourceParent){
                        sourceParent.removeBlock(source,false);
                    }
                    target.add(source,null,null);
                    return;
                }else{
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if(!target.parentId && add==false){

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if(sourceParent && sourceParent.removeBlock){
                    sourceParent.removeBlock(source,false);
                    source.parentId=null;
                    source.group=target.group;
                }

                const itemsToBeMoved=[];
                const groupItems = this.getBlocks({
                    group:target.group
                });

                const rootLevelIndex=[];
                const store = this.getBlockStore();

                const sourceIndex = store.storage.index[source.id];
                const targetIndex = store.storage.index[target.id];
                for(var i = 0; i<groupItems.length;i++){

                    const item = groupItems[i];
                    //keep all root-level items

                    if( groupItems[i].parentId==null && //must be root
                        groupItems[i]!=source// cant be source
                    ){

                        const itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if(add){
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if(before){
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if(!before){
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            }else if( !source.parentId && target.parentId && add==false){
                source.group = target.group;
                if(target){

                }

                //we move from root to into root item
            }else if( !source.parentId && !target.parentId && add){

                console.error('we are adding an item into root root item');
                if(target.canAdd && target.canAdd()){
                    source.group=null;
                    target.add(source,null,null);
                }
                return true;

                // we move within the same parent
            }else if( source.parentId && target.parentId && add==false && source.parentId === target.parentId){
                console.error('we move within the same parents');
                const parent = this.getBlockById(source.parentId);
                if(!parent){
                    console.error('     couldnt find parent ');
                    return false;
                }

                const maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items,source);
                var cIndexTarget = source.indexOf(items,target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    parent.move(source,direction);
                }
                return true;

                // we move within the different parents
            }else if( source.parentId && target.parentId && add==false && source.parentId !== target.parentId){                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if(!sourceParent){
                    console.error('     couldnt find source parent ');
                    return false;
                }

                const targetParent = this.getBlockById(target.parentId);
                if(!targetParent){
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if(sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()){
                    sourceParent.removeBlock(source,false);
                    targetParent.add(source,null,null);
                }else{
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if(items==null){
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items,source);
                var cIndexTarget = targetParent.indexOf(items,target);
                if(!cIndexSource || !cIndexTarget){
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    targetParent.move(source,direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        const topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId:null
        });


        const grouped = _.groupBy(blocks,function(block){
            return block.group;
        });

        function createDummyBlock(id,scope){

            const block = {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": null,
                    "id": id,
                    "items": [

                    ],
                    "name": id,
                    "method": "----group block ----",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": false,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"

                };

            return scope.blockFromJson(block);

        }

        for(const group in grouped){

            const groupBlock = createDummyBlock(group,scope);
            var blocks = grouped[group];
            _.each(blocks,function(block){
                groupBlock['items'].push(block);



                if(!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }

        console.clear();
        const root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }


    function createScope() {

        const data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }

    if (ctx) {


        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();
        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        //scope = createScope();

        openDriverSettings(marantz);


        return declare('a',null,{});


        //return createCommandSettingsWidget();


        function createGridClass() {

            const renderers = [TreeRenderer];

            //, ThumbRenderer, TreeRenderer


            const multiRenderer = declare.classFactory('multiRenderer', {}, renderers, MultiRenderer.Implementation);




            const _gridClass = Grid.createGridClass('driverTreeView',
                {
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {

                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                    SPLIT:{
                        CLASS:declare('splitMixin',null,_layoutMixin)
                    },
                    DND:{
                        CLASS:Dnd//declare('splitMixin',Dnd,_dndMixin)
                    }

                },
                {
                    //base flip
                    RENDERER: multiRenderer

                },
                {
                    //args
                    renderers: renderers,
                    selectedRenderer: TreeRenderer
                },
                {
                    GRID: OnDemandGrid,
                    EDITOR: Editor,
                    LAYOUT: Layout,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );



            return declare('gridFinal', BlockGrid, _gridBase);

            //return BlockGrid;
        }

        const blockScope = createScope('docs');


        const mainView = ctx.mainView;

        const docker = mainView.getDocker();

        if (mainView) {

            if (_lastGrid) {

                docker.removePanel(_lastGrid);
            }

            const parent = docker.addTab(null, {
                title: 'blox',
                icon: 'fa-folder'
            });
            window._lastGrid = parent;





            const store = blockScope.blockStore;


            const _gridClass = createGridClass();

            const gridArgs = {
                ctx:ctx,
                blockScope: blockScope,
                blockGroup: 'click',
                attachDirect:true,
                collection: store.filter({
                    group: "click"
                }),
                dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                dndParams: {
                    allowNested: true, // also pick up indirect children w/ dojoDndItem class
                    checkAcceptance: function (source, nodes) {
                        return true;//source !== this; // Don't self-accept.
                    },
                    isSource: true
                }
            };


            grid = utils.addWidget(_gridClass,gridArgs,null,parent,true);

            const blocks = blockScope.allBlocks();

            var root = blocks[0];


            const actionStore = grid.getActionStore();
            const toolbar = mainView.getToolbar();

            let _defaultActions = [];

            _defaultActions = _defaultActions.concat(getFileActions.apply(grid, [grid.permissions]));

            grid.addActions(_defaultActions);



            if (!toolbar) {


            } else {
                toolbar.addActionEmitter(grid);
                toolbar.setActionEmitter(grid);
            }



            setTimeout(function () {
                mainView.resize();
                grid.resize();
            }, 1000);


            function test() {

                expand.apply(grid);
                return;
            }

            setTimeout(function () {
                test();
            }, 1000);
        }
    }

    return Grid;
});