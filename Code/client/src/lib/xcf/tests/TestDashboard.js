/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/tests/TestUtils",
    'xide/_base/_Widget',
    "module"
], function (dcl,declare,types,utils,
             Grid, TestUtils,_Widget,module) {
    console.clear();


    console.log('--do-tests');

    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;

    /*
     * playground
     */
    const ctx = window.sctx;

    const ACTION = types.ACTION;
    let root;




    const _actions = [
        ACTION.RENAME
    ];

    if (ctx) {
        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();

        const mainView = ctx.mainView;
        const breadCrumb = mainView.breadcrumb;
        const parent = TestUtils.createTab(null,null,module.id);

        const widget = dcl(_Widget,{
            templateString:'<div>' +
            '</div>',
            sources:null,
            options:null,
            startup:function(){

            }
        });

        utils.addWidget(widget,utils.mixin({

        },{}),null,parent,true);

        parent.resize();
        return declare('a',null,{});
    }
    return Grid;
});