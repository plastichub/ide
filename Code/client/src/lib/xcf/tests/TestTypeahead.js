/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/tests/TestUtils",
    'xide/_base/_Widget',
    "module"
], function (dcl,declare,types,utils,
             Grid, TestUtils,_Widget,module) {
    console.clear();

    console.log('--do-tests');

    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;

    /*
     * playground
     */
    const ctx = window.sctx;

    const ACTION = types.ACTION;
    let root;




    const _actions = [
        ACTION.RENAME
    ];

    if (ctx) {



        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();

        const mainView = ctx.mainView,
              breadCrumb = mainView.breadcrumb,
              //parent = breadCrumb ? breadCrumb : TestUtils.createTab(null,null,module.id),
              parent = TestUtils.createTab(null,null,module.id);





        if(breadCrumb){
            //$(breadCrumb.domNode).empty();
        }



        mainView.onOpenView2=  function(evt){
            console.error('---open view');
        }




        /***
         * collect devices:
         */

        function getDevices(store){

            const items = store.query({
                isDevice:true
            });

            return items;
        }


        const devices = getDevices(deviceManager.store);

        console.log('devices ',devices);
        const sources = [];

        sources.push({
            label:'Devices',
            id:'Devices'
        })


        function addDevices(devices,showGroup,prefix){

            _.each(devices,function(device){

                //[{"label":"Test","id":"1"},{"label":"Tom","id":"2"}];

                const info = deviceManager.toDeviceControlInfo(device);


                //console.log(deviceManager.getDeviceStatusIcon(device));

                const parent = deviceManager.store.getSync(device.parentId);
                console.log(parent);

                sources.push({
                    label: showGroup && parent ? parent.name +'/' +info.title : info.title,
                    id:info.id,
                    icon:deviceManager.getDeviceStatusIcon(device),
                    prefix:prefix
                });
            });
        }

        addDevices(devices,false,'&nbsp;&nbsp;');

        const args = {

        };

        const options = {

            displayText: function (item,isValue) {
                if(isValue){
                    return item.label;
                }
                let icon ='';
                if (item.icon) {
                    icon = '<span class="grid-icon ' + item.icon + '"></span>';
                }
                return (item.prefix || '') +  (icon + '<span class="">'+item.label+'</span>');
            }
        };




        const widget = dcl(_Widget,{
            templateString:'<div>' +
            '<input attachTo="input" data-provide="typeahead" class="typeahead form-control input-transparent" type="text" placeholder="States of USA">'+
            '</div>',
            sources:null,
            options:null,
            startup:function(){

                const substringMatcher = function(strs) {
                    return function findMatches(q, cb) {
                        let matches, substringRegex;

                        // an array that will be populated with substring matches
                        matches = [];

                        // regex used to determine if a string contains the substring `q`
                        substrRegex = new RegExp(q, 'i');

                        // iterate through the pool of strings and for any string that
                        // contains the substring `q`, add it to the `matches` array
                        $.each(strs, function(i, str) {
                            if (substrRegex.test(str.label)) {
                                matches.push(str);
                            }
                        });

                        cb(matches);
                    };
                };
                const states = this.sources || [{"label":"Test","id":"1"},{"label":"Tom","id":"2"}];


                const typehead = this.$input.typeahead(utils.mixin({
                    hint: true,
                    highlight: true,
                    minLength: 1,
                    name: 'states',
                    showHintOnFocus:true,
                    source: substringMatcher(states),
                    appendTo:'#staticTopContainer',
                    select: function () {
                        const val = this.$menu.find('.active').data('value');
                        this.$element.data('active', val);
                        if(this.autoSelect || val) {
                            let newVal = this.updater(val);
                            // Updater can be set to any random functions via "options" parameter in constructor above.
                            // Add null check for cases when updater returns void or undefined.
                            if (!newVal) {
                                newVal = "";
                            }
                            this.$element
                                .val(this.displayText(newVal,true) || newVal)
                                .change();

                            this.afterSelect(newVal);
                        }
                        return this.hide();
                    },
                    highlighter: function(item)
                    {
                        return item;
                    },
                    displayText: function (item,isValue) {
                        //!isValue && console.log('---display text ' +item.label);
                        if(isValue){
                            return item.label;
                        }
                        return item.id + ' - ' + '<span class="text-info">'+item.label+'</span>';
                    }
                },this.options));

                this.typeahead = typehead.data('typeahead');

                console.log(typehead);

            }
        });




        utils.addWidget(widget,utils.mixin({
            options:options,
            sources:sources

        },args),null,parent,true);


        parent.resize();


        return declare('a',null,{});

    }

    return Grid;
});