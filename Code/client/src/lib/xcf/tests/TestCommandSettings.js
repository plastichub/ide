/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'dojo/dom-class',
    "dojo/on",
    // keys.END keys.HOME, keys.LEFT_ARROW etc.
    "dojo/debounce",
    'dojo/dom-construct',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/Grid',
    'xaction/DefaultActions',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xblox/views/BlockGrid',
    'xide/views/CIGroupedSettingsView',
    'xcf/model/Command',
    'xcf/model/Variable',
    'xide/widgets/ToggleButton',
    'xide/widgets/_ActionValueWidgetMixin',
    'xide/layout/AccordionContainer',
    "xide/widgets/TemplatedWidgetBase",


    "dijit/form/TextBox",
    'xcf/widgets/CommandSettings',

    'xblox/model/variables/VariableAssignmentBlock',
    'dojo/promise/all',
    "dojo/Deferred",
    "xgrid/KeyboardNavigation",
    "xgrid/Search",
    "xide/tests/TestUtils",
    "module"

], function (declare, domClass,on,debounce,domConstruct,types,
             utils, ListRenderer, Grid, DefaultActions, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,CIViewMixin,BlockGrid,
             CIGroupedSettingsView,
             Command,Variable,
             ToggleButton,_ActionValueWidgetMixin,AccordionContainer,TemplatedWidgetBase,

             TextBox, CommandSettings,
             VariableAssignmentBlock,
             all,Deferred,KeyboardNavigation,
             Search,TestUtils,module


    ) {
    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;
    let widget;
    let basicGridInstance;

    function completeGrid(_grid) {

        _grid._on(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, function (evt) {

            const items = evt.items;

            const variables = this.blockScope.getVariables();
            const variableItems = [
                /*{
                name: 'None',
                //target: item,
                iconClass: 'el-icon-compass',
                proto: VariableAssignmentBlock,
                item: null,
                ctrArgs: {
                    variable: null,
                    scope: this.blockScope,
                    value: ''
                }
            }*/
            ];

            //console.log('on insert block end',variables);


            _.each(variables,function(variable){
                variableItems.push({
                    name: variable.name,
                    //target: item,
                    iconClass: 'el-icon-compass',
                    proto: VariableAssignmentBlock,
                    item: variable,
                    ctrArgs: {
                        variable: variable.name,
                        scope: this.blockScope,
                        value: ''
                    }
                });
            },this);

            items.push({
                name: 'Set Variable',
                iconClass: 'el-icon-pencil-alt',
                items: variableItems
            });









        });

        _grid._on('onAddActions', function (evt) {
            if(!evt.addAction){
                return;
            }
            const addAction = evt.addAction;
            const cmdAction = 'New/Command';
            const varAction = 'New/Variable';
            const permissions = evt.permissions;
            const VISIBILITY = types.ACTION_VISIBILITY;
            const thiz = this;

            /*
            addAction('Save', 'File/Save', 'fa-save', ['ctrl s'], 'Home', 'File', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);
            */



            addAction('Command', cmdAction, 'el-icon-plus-sign', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);

            addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);


            addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {
                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                            widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                            widgetArgs: {
                                icon1: 'fa-toggle-on',
                                icon2: 'fa-toggle-off',
                                delegate: thiz,
                                checked: false,
                                iconClass: 'fa-toggle-off'
                            }
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });



            const settingsWidget = declare('commandSettings', TemplatedWidgetBase,{
                templateString:'<div></div>',
                _getText: function (url) {
                    let result;
                    const def = dojo.xhrGet({
                        url: url,
                        sync: true,
                        handleAs: 'text',
                        load: function (text) {
                            result = text;
                        }
                    });
                    return '' + result + '';
                },
                startup:function(){

                    this.inherited(arguments);

                    if(!_grid.userData){
                        return;
                    }

                    const settings = utils.getJson(_grid.userData['params']) || {
                            constants: {
                                start: '',
                                end: ''
                            },
                            send: {
                                mode: false,
                                interval: 500,
                                timeout: 500,
                                onReply: ''
                            }
                        };




                    const settingsPane = utils.templatify(
                        null,
                        this._getText(require.toUrl('xcf/widgets/templates/commandSettings.html')),
                        this.domNode,
                        {
                            baseClass: 'settings',
                            style:'width:100%',
                            start: settings.constants.start,
                            end: settings.constants.end,
                            interval: settings.send.interval,
                            timeout: settings.send.timeout,
                            sendMode: settings.send.mode,
                            onReply: settings.send.onReply,
                            settings: settings
                        }, null
                    );

                    return;

                    if (settings.send.mode) {
                        settingsPane.rReply.set('checked', true);
                    } else {
                        settingsPane.rInterval.set('checked', true);
                    }

                    const _onSettingsChanged = function () {
                        //update params field of our ci
                        thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                        //thiz.save();
                        console.log('changd');
                    };



                    //wire events
                    dojo.connect(settingsPane.wStart, "onChange", function (item) {
                        settingsPane.settings.constants.start = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wEnd, "onChange", function (item) {
                        settingsPane.settings.constants.end = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wInterval, "onChange", function (item) {
                        settingsPane.settings.send.interval = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wTimeout, "onChange", function (item) {
                        settingsPane.settings.send.timeout = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wOnReply, "onChange", function (item) {
                        settingsPane.settings.send.onReply = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.rReply, "onChange", function (item) {
                        settingsPane.settings.send.mode = item;
                        _onSettingsChanged();
                    });
                }
            });


            addAction('Settings', 'File/Settings', 'fa-gears', null, 'Settings', 'Settings', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {

                        action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null);

                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON,{
                            widgetClass:settingsWidget
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });
        });

        _grid._on('selectionChanged', function (evt) {
            //console.log('selection ',evt);
            //since we can only add blocks to command and not
            //at root level, disable the 'Block/Insert' root action and
            //its widget //references
            const thiz = this;

            const selection = evt.selection;
            const item = selection[0];
            const blockInsert = thiz.getAction('Block/Insert');
            const blockEnable = thiz.getAction('Step/Enable');

            disable = function (disable) {
                blockInsert.set('disabled', disable);
                setTimeout(function () {
                    blockInsert.getReferences().forEach(function (ref) {
                        ref.set('disabled', disable);
                    });
                }, 100);

            }

            const _disable =item ? false : true;

            disable(_disable);


            if (item) {
                blockEnable.getReferences().forEach(function (ref) {
                    ref.set('checked', item.enabled);
                });
            }else{
                /*
                var props = _grid.getPropertyStruct();
                props._lastItem = null;
                _grid.setPropertyStruct(props);*/
            }
        });

        _grid.startup();

    }

    const propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };


    console.clear();



    function createLogGridClass(){




        const filterClass = declare('filter',null,{
            showFooter: true,
            buildRendering: function () {

                this.inherited(arguments);
                const grid = this;
                const filterNode = this.filterNode = domConstruct.create('div', {
                    className: 'dgrid-filter'
                }, this.footerNode);
                this.filterStatusNode = domConstruct.create('div', {
                    className: 'dgrid-filter-status'
                }, filterNode);
                const inputNode = this.filterInputNode = domConstruct.create('input', {
                    className: 'dgrid-filter-input',
                    placeholder: 'Filter (regex)...'
                }, filterNode);
                this._filterTextBoxHandle = on(inputNode, 'keydown', debounce(function () {
                    grid.set("collection", grid.collection);
                }, 250));
            },
            destroy: function () {
                this.inherited(arguments);
                if (this._filterTextBoxHandle) {
                    this._filterTextBoxHandle.remove();
                }
            },
            _setCollection: function (collection) {
                this.inherited(arguments);
                const value = this.filterInputNode.value;
                const renderedCollection = this._renderedCollection;
                if (renderedCollection && value) {
                    const rootFilter = new renderedCollection.Filter();
                    const re = new RegExp(value, "i");
                    const columns = this.columns;
                    const matchFilters = [];
                    for (const p in columns) {
                        if (columns.hasOwnProperty(p)) {
                            matchFilters.push(rootFilter.match(columns[p].field, re));
                        }
                    }
                    const combined = rootFilter.or.apply(rootFilter, matchFilters);
                    const filtered = renderedCollection.filter(combined);
                    this._renderedCollection = filtered;
                    this.refresh();
                }
            },
            refresh: function() {
                this.inherited(arguments);
                const value = this.filterInputNode.value;
                if (value) {
                    this.filterStatusNode.innerHTML = this.get('total') + " filtered results";
                }else {
                    this.filterStatusNode.innerHTML = "";
                }
            }


        });





        /**
         * Block grid base class.
         * @class module:xblox/views/Grid
         */
        const GridClass = Grid.createGridClass('log',
            {
                options: utils.clone(types.DEFAULT_GRID_OPTIONS)
            },
            //features
            {

                SELECTION: true,
                KEYBOARD_SELECTION: true,
                PAGINATION: types.GRID_FEATURES.PAGINATION,
                ACTIONS: types.GRID_FEATURES.ACTIONS,
                //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                FILTER:{
                    CLASS:KeyboardNavigation
                },
                SEARCH:{
                    CLASS:Search
                }

            },
            {
                //base flip
                RENDERER: ListRenderer

            },
            {
                //args
                /*renderers: renderers,
                 selectedRenderer: TreeRenderer*/
            },
            {
                GRID: OnDemandGrid,
                EDITOR: null,
                LAYOUT: Layout,
                DEFAULTS: Defaults,
                RENDERER: ListRenderer,
                EVENTED: EventedMixin,
                FOCUS: Focus

            }
        );



        return GridClass;

    }

    function createGridClass(overrides) {
        const gridClass = declare('driverGrid', BlockGrid,{
            highlightDelay:500,
            propertyStruct : propertyStruct,
            /**
             * Step/Run action
             * @param block {block[]}
             * @returns {boolean}
             */
            execute: function (blocks) {
                console.clear();

                const thiz = this;



                function _clear(element){
                    if(element) {
                        setTimeout(function () {
                            element.removeClass('failedBlock successBlock activeBlock');
                        }, thiz.highlightDelay);
                    }
                }

                function _node(block){
                    if(block) {
                        const row = thiz.row(block);
                        if (row) {
                            const element = row.element;
                            if (element) {
                                return $(element);
                            }
                        }
                    }
                    return null;
                }

                function mark(element,cssClass){

                    if(element) {
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.addClass(cssClass);
                    }
                }

                const dfds = [];
                const EVENTS = types.EVENTS;

                const _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                    mark(_node(evt),'activeBlock');

                });
                const _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                    //console.log('mark success');
                    mark(_node(evt),'successBlock');
                    _clear(_node(evt));
                });

                const _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                    mark(_node(evt),'failedBlock');
                    _clear(_node(evt));

                });




                function run(block) {

                    if (!block || !block.scope) {
                        console.error('have no scope');
                        return;
                    }
                    try {

                        const blockDfd = block.scope.solveBlock(block, {
                            highlight: true,
                            force: true,
                            listener:thiz
                        });

                        dfds.push(blockDfd);

                        /*
                        blockDfd.then(function(result){
                            console.log('did run! : ' + result);
                        });
                        */
                        //console.log('run block result:',result);


                    } catch (e)
                    {
                        console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                        console.error(printStackTrace().join('\n\n'));
                    }
                    return true;
                }

                blocks  = _.isArray(blocks) ? blocks : [blocks];


                function _patch(block){


                    block.runFrom=function(_blocks, index, settings)
                    {
                        const thiz = this;
                        const blocks = _blocks || this.items;
                        const allDfds = [];

                        const onFinishBlock = function (block, results) {
                            block._lastResult = block._lastResult || results;
                            thiz._currentIndex++;
                            thiz.runFrom(blocks, thiz._currentIndex, settings);
                        };

                        const wireBlock = function (block) {
                            block._deferredObject.then(function (results) {
                                console.log('----def block finish');
                                onFinishBlock(block, results);
                            });
                        };

                        if (blocks.length) {

                            for (let n = index; n < blocks.length; n++) {



                                const block = blocks[n];

                                console.log('run child \n'+block.method);

                                _patch(block);

                                if (block.deferred === true) {
                                    block._deferredObject = new Deferred();
                                    this._currentIndex = n;
                                    wireBlock(block);
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));
                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                    break;
                                } else {
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));

                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                }

                            }

                        } else {
                            this.onSuccess(this, settings);
                        }

                        return allDfds;
                    };

                    block.solve=function(scope,settings,run,error){
                        this._currentIndex = 0;
                        this._return=[];

                        const _script = '' + this._get('method');
                        const thiz=this;
                        const ctx = this.getContext();
                        const items = this[this._getContainer()];

                        const //outer,head dfd
                        dfd = new Deferred;

                        const listener = settings.listener;
                        const isDfd = thiz.deferred;



                        if(listener) {
                            listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                        }

                        function _finish(result){

                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                            }

                            dfd.resolve(result);


                        }

                        function _error(result){
                            dfd.reject(result);
                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                            }
                        }


                        function _headDone(result){
                            //console.log('_headDone : ',result);
                            //more blocks?
                            if(items.length) {
                                const subDfds = thiz.runFrom(items,0,settings);
                                all(subDfds).then(function(what){
                                    console.log('all solved!',what);
                                    _finish(result);
                                },function(err){

                                    console.error('error in chain',err);
                                    if(listener) {
                                        listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                    }
                                    dfd.resolve(err);
                                });

                            }else{
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }
                                dfd.resolve(result);
                            }
                        }




                        if(_script && _script.length){
                            const runScript = function() {


                                const _function = new Function("{" + _script + "}");
                                const _args = thiz.getArgs() || [];
                                try {

                                    if(isDfd){

                                        ctx.resolve=function(result){
                                            console.log('def block done');
                                            if(thiz._deferredObject) {
                                                thiz._deferredObject.resolve();
                                            }
                                            _headDone(result);
                                        }
                                    }
                                    const _parsed = _function.apply(ctx, _args || {});
                                    thiz._lastResult = _parsed;
                                    if (run) {
                                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                                    }

                                    if(!isDfd) {
                                        _headDone(_parsed);
                                    }

                                    if (_parsed !== 'false' && _parsed !== false) {

                                    } else {
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                } catch (e) {

                                    e=e ||{};

                                    _error(e);

                                    if (error) {
                                        error('invalid expression : \n' + _script + ': ' + e);
                                    }
                                    //thiz.onFailed(thiz, settings);
                                    //return [];
                                }
                            };

                            if(scope.global){
                                (function() {

                                    window = scope.global;


                                    const _args = thiz.getArgs() || [];
                                    try {
                                        let _parsed = null;
                                        if(!ctx.runExpression) {
                                            const _function = new Function("{" + _script + "}").bind(this);
                                            _parsed = _function.apply(ctx, _args || {});
                                        }else{
                                            _parsed = ctx.runExpression(_script,null,_args);
                                        }

                                        thiz._lastResult = _parsed;

                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }
                                        if (_parsed !== 'false' && _parsed !== false) {
                                            thiz.onSuccess(thiz, settings);
                                        } else {
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }


                                    } catch (e) {

                                        thiz._lastResult = null;
                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        thiz.onFailed(thiz, settings);
                                        return [];
                                    }

                                }).call(scope.global);

                            }else{
                                runScript();
                            }

                        }else{
                            console.error('have no script');
                        }
                        /*
                        var ret=[], items = this[this._getContainer()];

                        if(items.length) {
                            this.runFrom(items,0,settings);
                        }else{
                            this.onSuccess(this, settings);
                        }

                        this.onDidRun();
                        */

                        return dfd;
                    }

                }

                _.each(blocks,_patch);

                _.each(blocks,run);

                all(dfds).then(function(){
                    console.log('did run all selected blocks!',thiz);
                    _runHandle.remove();
                    _successHandle.remove();
                    _errHandle.remove();
                });
            },
            onCIChanged: function (ci, block, oldValue, newValue, field) {

                console.log('on ci changed', arguments);
                block.set(field, newValue);
            },
            _itemChanged: function (type, item, store) {

                store = store || this.getStore(item);

                const thiz = this;

                function _refreshParent(item, silent) {

                    const parent = item.getParent();
                    if (parent) {
                        const args = {
                            target: parent
                        };
                        if (silent) {
                            this._muteSelectionEvents = true;
                        }
                        store.emit('update', args);
                        if (silent) {
                            this._muteSelectionEvents = false;
                        }
                    } else {
                        thiz.refresh();
                    }
                }

                function select(item) {

                    thiz.select(item, null, true, {
                        focus: true,
                        delay: 20,
                        append: false
                    });
                }

                switch (type) {

                    case 'added':
                    {
                        //_refreshParent(item);
                        //this.deselectAll();
                        this.refresh();
                        select(item);
                        break;
                    }

                    case 'changed':
                    {
                        this.refresh();
                        select(item);
                        break;
                    }


                    case 'moved':
                    {
                        //_refreshParent(item,true);
                        //this.refresh();
                        //select(item);

                        break;
                    }

                    case 'deleted':
                    {

                        const parent = item.getParent();
                        //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                        const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                        if (parent) {
                            const _container = parent.getContainer();
                            if (_container) {
                                _.each(_container, function (child) {
                                    if (child.id == item.id) {
                                        _container.remove(child);
                                    }
                                });
                            }
                        }

                        this.refresh();
                        /*
                         if (_prev) {
                         select(_prev);
                         }
                         */
                        break;
                    }

                }



            },
            _onFocusChanged:function(focused,type){
                this.inherited(arguments);
                if(!focused){
                    this._lastSelection = [];
                }
            },
            showProperties: function (item,force) {
                const block = item || this.getSelection()[0];
                const thiz = this;
                const rightSplitPosition= thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);




                if(!block || rightSplitPosition==1) {
                    //console.log(' show properties: abort',[block , rightSplitPosition]);
                    return;
                }
                const right = this.getRightPanel();
                let props = this.getPropertyStruct();

                if (item == props._lastItem && force!==true) {
                    console.log('show propertiess : same item');
                    //return;
                }

                this.clearPropertyPanel();
                props = this.getPropertyStruct();


                props._lastItem = item;

                const _title = item.name || item.title;



                let tabContainer = props.targetTop;

                if (!tabContainer) {

                    tabContainer = utils.addWidget(AccordionContainer, {
                        delegate: this,
                        tabStrip: true,
                        tabPosition: "top",
                        attachParent: true,
                        style: "width:100%;height:100%;overflow-x:hidden;",
                        allowSplit: false
                    }, null, right.containerNode, true);

                    props.targetTop = tabContainer;
                }

                if (tabContainer.selectedChildWidget) {
                    props.lastSelectedTopTabTitle = tabContainer.selectedChildWidget.title;
                } else {
                    props.lastSelectedTopTabTitle = 'General';
                }


                _.each(tabContainer.getChildren(), function (tab) {
                    tabContainer.removeChild(tab);
                });

                if (props.currentCIView) {
                    props.currentCIView.empty();
                }

                if (!block.getFields) {
                    console.log('have no fields', block);
                    return;
                }

                const cis = block.getFields();
                for (var i = 0; i < cis.length; i++) {
                    cis[i].vertical = true;
                }

                const ciView = new CIViewMixin({

                    initWithCIS: function (data) {
                        this.empty();

                        data = utils.flattenCIS(data);

                        this.data = data;

                        const thiz = this;

                        let groups = _.groupBy(data,function(obj){
                            return obj.group;
                        });

                        const groupOrder = this.options.groupOrder || {};

                        groups = this.toArray(groups);

                        const grouped = _.sortByOrder(groups, function(obj){
                            return groupOrder[obj.name] || 100;
                        });

                        if (grouped != null && grouped.length > 1) {
                            this.renderGroups(grouped);
                        } else {
                            this.widgets = factory.createWidgetsFromArray(data, thiz, null, false);
                            if (this.widgets) {
                                this.attachWidgets(this.widgets);
                            }
                        }
                    },
                    tabContainer: props.targetTop,
                    delegate: this,
                    viewStyle: 'padding:0px;',
                    autoSelectLast: true,
                    item: block,
                    source: this.callee,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Advanced': 2,
                            'Script':3,
                            'Arguments':4,
                            'Description':5,
                            'Share':6

                        }
                    },
                    cis: cis
                });

                ciView.initWithCIS(cis);


                props.currentCIView = ciView;

                if (block.onFieldsRendered) {
                    block.onFieldsRendered(block, cis);
                }


                ciView._on('valueChanged', function (evt) {
                    //console.log('ci value changed ', evt);
                    thiz.onCIChanged(evt.ci,block,evt.oldValue,evt.newValue,evt.ci.dst);
                });






                const containers = props.targetTop.getChildren();
                let descriptionView = null;
                for (var i = 0; i < containers.length; i++) {

                    // @TODO : why is that not set?
                    containers[i].parentContainer = props.targetTop;

                    // track description container for re-rooting below
                    if (containers[i].title === 'Description') {
                        descriptionView = containers[i];
                    }

                    if (props.targetTop.selectedChildWidget.title !== props.lastSelectedTopTabTitle) {
                        if (containers[i].title === props.lastSelectedTopTabTitle) {
                            props.targetTop.selectChild(containers[i]);
                        }
                    }
                }

                props.targetTop.resize();

                this.setPropertyStruct(props);


                this._docker.resize();
            },
            save:function(){
                const thiz = this;
                const driver = thiz.userData.driver;
                const ctx = thiz.ctx;
                const fileManager = ctx.getFileManager();
                const scope = thiz.blockScope;
                const instance = scope.instance;
                const originalScope = instance ? instance.blockScope : null;
                const path = driver.path.replace('.meta.json','.xblox');
                const scopeToSave = originalScope || scope;
                const mount = driver.scope;

                if(originalScope){
                    originalScope.fromScope(scope);
                }

                if (scope) {

                    const all = {
                        blocks: null,
                        variables: null
                    };

                    const blocks = scopeToSave.blocksToJson();
                    try {
                        //test integrity
                        dojo.fromJson(JSON.stringify(blocks));
                    } catch (e) {
                        console.error('invalid data');
                        return;
                    }

                    const _onSaved = function () {};

                    all.blocks = blocks;

                    fileManager.setContent(mount,path,JSON.stringify(all, null, 2),_onSaved);

                    //this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
                }
            },
            runAction: function (action) {

                const thiz = this;
                const sel = this.getSelection();


                console.log('run aciton innner ' + action.command);

                function addItem(_class,group){

                    const cmd = factory.createBlock(_class, {
                        name: "No Title",
                        send: "nada",
                        scope: thiz.blockScope,
                        group: group
                    });

                    thiz.deselectAll();
                    _.each(thiz.grids,function(grid){
                        grid.refresh();
                    });
                    setTimeout(function () {
                        thiz.select([cmd],null,true,{
                            focus:true
                        });
                    }, 200);
                }


                if (action.command == 'New/Command') {
                    addItem(Command,'basic');
                }
                if (action.command == 'New/Variable') {
                    addItem(Variable,'basicVariables');
                }

                if (action.command == 'File/Save') {
                    this.save();
                }

                return this.inherited(arguments);
            },
            startup:function(){


                domClass.add(this.domNode,'blockGrid');


                this.inherited(arguments);
                const thiz = this;

                function _node(evt){
                    const item = evt.callee;
                    if(item) {
                        const row = thiz.row(item);
                        if (row) {
                            const element = row.element;
                            if (element) {
                                return $(element);
                            }
                        }
                    }
                    return null;
                };

                function mark(element,cssClass){

                    if(element) {
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.addClass(cssClass);
                        setTimeout(function () {
                            element.removeClass(cssClass);
                            thiz._isHighLighting = false;
                        }, thiz.highlightDelay);
                    }
                }

            }
        });

        return gridClass;
    }

    function createLogEntry(){

        const logManager = ctx.getLogManager();
        const store = logManager.store;

        /*
         var message={
         message:'test',
         level:'info',
         type:'device',
         details:{},
         terminatorMessage:null
         };
         factory.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, message);*/

        //logManager.addLoggingMessage()
    }
    function addLog(tab,driver,device){

        const logManager = ctx.getLogManager();
        const store = logManager.store;

        var logGridClass = createLogGridClass();


        if(device){
            console.log('device : ', device);
        }




        // default grid args
        const gridArgs = {
            ctx:ctx,
            attachDirect:true,
            collection: store.filter({
                show:true,
                host:device.info.host + ':' + device.info.port
            })
        };


        const items = store.query({
            show:true,
            host:"192.168.1.20:23"
        });






        var logGridClass = declare("xlog.views.LogView", logGridClass, {
            _columns: {
                "Level": true,
                "Type": false,
                "Message": true,
                "Time": false
            },
            permissions: [
                //ACTION.EDIT,
                ACTION.RELOAD,
                ACTION.DELETE,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.PREVIEW,
                ACTION.SAVE,
                ACTION.SEARCH
            ],
            postMixInProperties: function () {
                this.columns = this.getColumns();
                return this.inherited(arguments);
            },
            formatDateSimple: function (data, format) {

                const momentUnix = moment.unix(data);

                return momentUnix.format("MMMM Do, h:mm:ss a");
            },
            getDefaultSort:function(){
                return [{property: 'time', descending: true}];
            },
            getMessageFormatter: function (message, item) {
                const thiz = this;
                if(item.progressHandler && !item._subscribedToProgress){
                    item.progressHandler._on('progress',function(_message){
                        thiz.refresh();
                    });
                    item._subscribedToProgress = true;
                }

                let _isTerminated = item.terminatorMessage !==null && item._isTerminated===true;
                if(!item.terminatorMessage){
                    _isTerminated = true;
                }

                if (!_isTerminated) {
                    return '<span class=\"fa-spinner fa-spin\" style=\"margin-right: 4px\"></span>' + message;
                }
                return message;
            },
            getColumns: function () {
                const thiz = this;
                const columns = {
                    Level: {
                        field: "level", // get whole item for use by formatter
                        label: "Level",
                        sortable: true,
                        formatter: function (level) {

                            switch (level) {
                                case 'info':
                                {
                                    return '<span class="text-info" style=\"">' + level + '</span>';
                                }
                                case 'error':
                                {
                                    return '<span class="text-danger" style=\"">' + level + '</span>';
                                }
                                case 'warning':
                                {
                                    return '<span class="text-warning" style=\"">' + level + '</span>';
                                }
                            }
                            return level;
                        }

                    },
                    Type: {
                        field: "type", // get whole item for use by formatter
                        label: "Type",
                        sortable: true
                    },
                    Host: {
                        field: "host", // get whole item for use by formatter
                        label: "Host",
                        sortable: true
                    },
                    Message: {
                        field: "message", // get whole item for use by formatter
                        label: "Message",
                        sortable: true,
                        formatter: function (message, item) {
                            return thiz.getMessageFormatter(message, item)
                        }
                    },
                    Time: {
                        field: "time", // get whole item for use by formatter
                        label: "Time",
                        sortable: true,
                        formatter: function (time) {
                            return thiz.formatDateSimple(time / 1000);
                        }
                    },
                    Details:{
                        field: "details", // get whole item for use by formatter
                        label: "Details",
                        sortable: false,
                        hidden:true
                        /*editor:RowDetailEditor*/
                    }
                };








                if (!this.showSource) {
                    delete columns['Host'];
                }
                return columns;
            },
            startup:function(){
                this.inherited(arguments);

                const thiz = this;
                const permissions = this.permissions;

                if (permissions) {

                    const _defaultActions = DefaultActions.getDefaultActions(permissions, this);
                    //_defaultActions = _defaultActions.concat(this.getBlockActions(permissions));

                    this.addActions(_defaultActions);
                    //this.onContainerClick();
                }
            }
        });
        //tab.select();
        const grid = utils.addWidget(logGridClass,gridArgs,null,tab,false,'logGridView');
        grid.startup();


        addSearch(grid);



    }

    function onWidgetCreated(basicTab,condTab,varTab,logTab,driver,device){

        addLog(logTab,driver,device);


        if(basicGridInstance){

            //basicGridInstance.runAction('Step/Run');
            const saveAction = basicGridInstance.getAction('File/Save');
            //basicGridInstance.runAction(saveAction);
        }


        logTab.select();

    }


    function createCommandSettingsWidget(){


        const _class = declare("xcf.widgets.CommandSettings2",CommandSettings, {
            templateString:'<div style="width: inherit;height: 100%;"><div>',
            _docker:null,
            grids:null,
            completeGrid:function(grid){
                grid.userData = this.userData;
                completeGrid(grid);
            },
            getGridClass:function(){
                return createGridClass();
            },
            createWidgets:function(){
                widget = this;

                const docker = this.getDocker(this.domNode);
                const ci = this.userData;
                const driver = ci.driver;
                const device = ci.device;

                const //original or device instance
                scope = device ? device.blockScope : driver.blockScope;

                const store = scope.blockStore;
                const instance = driver.instance;
                var widget = this;
                const grids = [];
                const gridClass = this.getGridClass();

                const defaultTabArgs = {
                    icon:false,
                    closeable:false,
                    movable:false,
                    moveable:true,
                    tabOrientation:types.DOCKER.TAB.TOP,
                    location:types.DOCKER.DOCK.STACKED
                };

                const // 'Basic' commands tab
                basicCommandsTab = docker.addTab('DefaultFixed',
                    utils.mixin(defaultTabArgs,{
                        title: 'Basic Commands',
                        h:'90%'
                    }));

                const // 'Conditional' commands tab
                condCommandsTab = docker.addTab('DefaultFixed',
                    utils.mixin(defaultTabArgs,{
                        title: 'Conditional Commands',
                        target:basicCommandsTab,
                        select:false,
                        h:'90%',
                        tabOrientation:types.DOCKER.TAB.TOP
                    }));


                // 'Variables' tab
                const variablesTab = docker.addTab(null,
                    utils.mixin(defaultTabArgs,{
                        title: 'Variables',
                        //target:condCommandsTab,
                        select:false,
                        h:100,
                        tabOrientation:types.DOCKER.TAB.BOTTOM,
                        location:types.DOCKER.TAB.BOTTOM
                    }));


                const logsTab = docker.addTab(null,
                    utils.mixin(defaultTabArgs,{
                        title: 'Log',
                        target:variablesTab,
                        select:false,
                        tabOrientation:types.DOCKER.TAB.BOTTOM,
                        location:types.DOCKER.DOCK.STACKED
                    }));



                // prepare right property panel but leave it closed

                this.getRightPanel(null,1);


                const basicArgs = {
                    _getRight:function(){
                        return widget.__right;
                    },
                    ctx:ctx,
                    blockScope: scope,
                    blockGroup: 'basic',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'basic'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    __right:this.__right,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition
                };


                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //basic commands

                const basicGrid = utils.addWidget(gridClass,basicArgs,null,basicCommandsTab,false);
                this.completeGrid(basicGrid,'Command');
                grids.push(basicGrid);


                basicGridInstance = basicGrid;



                docker._on(types.DOCKER.EVENT.SPLITTER_POS_CHANGED,function(evt){
                    const position = evt.position;
                    const splitter = evt.splitter;
                    const right = widget.__right;

                    if(right && splitter == right.getSplitter()){
                        if(position<1){
                            basicGrid.showProperties(grid.getSelection()[0],true);
                        }
                    }
                });

                basicGrid.select([0],null,true,{
                    focus:true
                });





                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //conditional commands
                const condArgs = {
                    ctx:ctx,
                    _getRight:function(){
                        return widget.__right;
                    },
                    blockScope: scope,
                    blockGroup: 'conditional',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'conditional'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    __right:this.__right,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition
                };

                const condGrid = utils.addWidget(gridClass,condArgs,null,condCommandsTab,false);
                completeGrid(condGrid,'Command');
                grids.push(condGrid);




                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                //variables
                const varArgs= {
                    _getRight:function(){
                        return widget.__right;
                    },
                    ctx:ctx,
                    blockScope: scope,
                    blockGroup: 'basicVariables',
                    attachDirect:true,
                    collection: store.filter({
                        group: 'basicVariables'
                    }),
                    //dndConstructor: SharedDndGridSource,
                    //dndConstructor:Dnd.GridSource,
                    _docker:docker,
                    setPanelSplitPosition:widget.setPanelSplitPosition,
                    getPanelSplitPosition:widget.getPanelSplitPosition,
                    showHeader:true,
                    __right:this.__right,
                    columns:[
                        {
                            label: "Name",
                            field: "name",
                            sortable: true,
                            width:'20%',
                            editorArgs: {
                                required: true,
                                promptMessage: "Enter a unique variable name",
                                //validator: thiz.variableNameValidator,
                                //delegate: thiz.delegate,
                                intermediateChanges: false
                            }
                            //editor: TextBox,
                            //editOn:'click',
                            //_editor: ValidationTextBox
                        },
                        {
                            label: "Initialize",
                            field: "initialize",
                            sortable: false,
                            editor: TextBox,
                            editOn:'click'
                        },
                        {
                            label: "Value",
                            field: "value",
                            sortable: false,
                            editor: TextBox,
                            editOn:'click',
                            editorArgs: {
                                autocomplete:'on',
                                templateString:'<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"'+
                                '><div class="dijitReset dijitInputField dijitInputContainer"'+
                                '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"'+
                                '${!nameAttrSetting} type="${type}"'+
                                '/></div'+
                                '></div>'
                            }
                        }
                    ]
                };
                const varGrid = utils.addWidget(gridClass,varArgs,null,variablesTab,false);

                completeGrid(varGrid,'Variable');

                grids.push(varGrid);


                domClass.add(varGrid.domNode,'variableSettings');

                varGrid.on("dgrid-datachange", function (evt) {

                    const cell = evt.cell;

                    //normalize data
                    let item = null;
                    if (cell && cell.row && cell.row.data) {
                        item = cell.row.data;
                    }
                    const id = evt.rowId;
                    const oldValue = evt.oldValue;
                    const newValue = evt.value;

                    const data = {
                        item: item,
                        id: id,
                        oldValue: oldValue,
                        newValue: newValue,
                        grid: varGrid,
                        field: cell.column.field
                    };

                    if (item) {
                        item[data.field] = data.newValue;
                    }




                });
                basicCommandsTab.select();

                const _grids = {
                    basic:basicGrid,
                    variables:varGrid,
                    cond:condGrid
                };

                basicGrid.grids=_grids;
                condGrid.grids=_grids;
                varGrid.grids=_grids;

                if(ci.actionTarget){
                    _.each(grids,function(grid){
                        ci.actionTarget.addActionEmitter(grid);
                    });
                }


                this.grids = grids;



                setTimeout(function(){
                    variablesTab.getFrame().showTitlebar(false);
                    variablesTab.getSplitter().pos(0.6);
                    variablesTab.select();
                    onWidgetCreated(basicCommandsTab,condCommandsTab,variablesTab,logsTab,driver,device);
                },10);
            }
        });



        dojo.setObject('xcf.widgets.CommandSettings2',_class);



        return _class;
    }

    function getFileActions(permissions) {
        const result = [];
        const ACTION = types.ACTION;
        const ACTION_ICON = types.ACTION_ICON;
        const VISIBILITY = types.ACTION_VISIBILITY;
        const thiz = this;
        const actionStore = thiz.getActionStore();


        return [];

        function addAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable) {

            let action = null;
            if (DefaultActions.hasAction(permissions, command)) {

                mixin = mixin || {};

                utils.mixin(mixin, {owner: thiz});

                if (!handler) {

                    handler = function (action) {
                        console.log('log run action', arguments);
                        const who = this;
                        if (who.runAction) {
                            who.runAction.apply(who, [action]);
                        }
                    }
                }
                action = DefaultActions.createAction(label, command, icon, keycombo, tab, group, filterGroup, onCreate, handler, mixin, shouldShow, shouldDisable, thiz.domNode);

                result.push(action);
                return action;

            }
        }

        /*
         var rootAction = 'Block/Insert';
         permissions.push(rootAction);
         addAction('Block', rootAction, 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         dummy: true,
         onCreate: function (action) {
         action.setVisibility(VISIBILITY.CONTEXT_MENU, {
         label: 'Add'
         });

         }
         }, null, null);
         permissions.push('Block/Insert Variable');


         addAction('Variable', 'Block/Insert Variable', 'el-icon-plus-sign', null, 'Home', 'Insert', 'item|view', null, null, {
         }, null, null);
         */

        /*
         permissions.push('Clipboard/Paste/New');
         addAction('New ', 'Clipboard/Paste/New', 'el-icon-plus-sign', null, 'Home', 'Clipboard', 'item|view', null, null, {
         }, null, null);*/


        const newBlockActions = this.getAddActions();
        const addActions = [];
        let levelName = '';


        function addItems(commandPrefix, items) {

            for (let i = 0; i < items.length; i++) {
                const item = items[i];

                levelName = item.name;


                const path = commandPrefix + '/' + levelName;
                const isContainer = !_.isEmpty(item.items);

                permissions.push(path);

                addAction(levelName, path, item.iconClass, null, 'Home', 'Insert', 'item|view', null, null, {}, null, null);


                if (isContainer) {
                    addItems(path, item.items);
                }


            }

        }
        //console.clear();
        //addItems(rootAction, newBlockActions);
        //return result;


        //run
        function canMove(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            const item = selection[0];
            const canMove = item.canMove(item, this.command === 'Step/Move Up' ? -1 : 1);

            return !canMove;

        }


        function canParent(selection, reference, visibility) {

            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }

            const item = selection[0];
            if(!item){
                console.warn('bad item',selection);
                return false;
            }

            if(this.command === 'Step/Move Left'){
                return !item.getParent();
            }else{
                return item.getParent();
            }
            /*
             var canMove = item.canMove(item, this.command === 'Step/Move Left' ? -1 : 1);
             return !canMove;*/

            return true;

        }

        function isItem(selection, reference, visibility) {
            var selection = thiz.getSelection();
            if (!selection || !selection.length) {
                return true;
            }
            return false;

        }

        /**
         * run
         */

        addAction('Run', 'Step/Run', 'el-icon-play', ['space'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    widgetArgs:{
                        label: ' ',
                        style:'font-size:25px!important;'
                    }
                });

            }
        }, null, isItem);
        permissions.push('Step/Run/From here');

        /**
         * run
         */

        addAction('Run from here', 'Step/Run/From here', 'el-icon-play', ['ctrl space'], 'Home', 'Step', 'item', null, null, {

            onCreate: function (action) {

            }
        }, null, isItem);



        /**
         * move
         */

        addAction('Move Up', 'Step/Move Up', 'fa-arrow-up', ['alt up'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canMove);


        addAction('Move Down', 'Step/Move Down', 'fa-arrow-down', ['alt down'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {

                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });

            }
        }, null, canMove);
        /*


         permissions.push('Step/Edit');
         addAction('Edit', 'Step/Edit', ACTION_ICON.EDIT, ['f4', 'enter'], 'Home', 'Step', 'item', null, null, null, null, isItem);
         */
        ///////////////////////////////////////////////////
        //
        //  Editors
        //
        ///////////////////////////////////////////////////

        permissions.push('Step/Move Left');
        permissions.push('Step/Move Right');

        addAction('Move Left', 'Step/Move Left', 'fa-arrow-left', ['alt left'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });
            }
        }, null, canParent);

        addAction('Move Right', 'Step/Move Right', 'fa-arrow-right', ['alt right'], 'Home', 'Step', 'item', null, null, {
            onCreate: function (action) {
                action.setVisibility(VISIBILITY.RIBBON, {
                    label: ''
                });


            }
        }, null, canParent);


        return result;
    }

    function doTests(){

    }


    function openDriverSettings(driver,device){


        const parent = TestUtils.createTab(null,null,module.id);


        const title = 'Marantz Instance';


        let devinfo = null;
        if(device){
            devinfo  = ctx.getDeviceManager().toDeviceControlInfo(device);
        }

        //@Todo:driver, store device temporarly in Commands CI
        const commandsCI = utils.getCIByChainAndName(driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
        if(commandsCI){
            commandsCI.device = device;
        }


        const view = utils.addWidget(CIGroupedSettingsView, {
            style:"width: inherit;height: 100%;",
            title:  'title',
            cis: driver.user.inputs,
            storeItem: driver,
            storeDelegate: this,
            iconClass: 'fa-eye',
            closable: true,
            showAllTab: false,
            blockManager: ctx.getBlockManager(),
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'Settings'
            }


        }, null, parent, true);

        view.resize();
    }
    /***
     * playground
     */
    const _lastGrid = window._lastGrid;
    var ctx = window.sctx;
    var ACTION = types.ACTION;
    let root;
    let scope;
    let blockManager;
    let driverManager;
    var marantz;



    const _actions = [
        ACTION.RENAME
    ];



    function fixScope(scope){

        return scope;

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function(source,target,before,add){




            console.log('scope::move, add: ' +add,arguments);

            if(!add){
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if(add){

                //remove it from the source parent and re-parent the source
                if(target.canAdd && target.canAdd()){

                    var sourceParent = this.getBlockById(source.parentId);
                    if(sourceParent){
                        sourceParent.removeBlock(source,false);
                    }
                    target.add(source,null,null);
                    return;
                }else{
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if(!target.parentId && add==false){

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if(sourceParent && sourceParent.removeBlock){
                    sourceParent.removeBlock(source,false);
                    source.parentId=null;
                    source.group=target.group;
                }

                const itemsToBeMoved=[];
                const groupItems = this.getBlocks({
                    group:target.group
                });

                const rootLevelIndex=[];
                const store = this.getBlockStore();

                const sourceIndex = store.storage.index[source.id];
                const targetIndex = store.storage.index[target.id];
                for(var i = 0; i<groupItems.length;i++){

                    const item = groupItems[i];
                    //keep all root-level items

                    if( groupItems[i].parentId==null && //must be root
                        groupItems[i]!=source// cant be source
                    ){

                        const itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if(add){
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if(before){
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if(!before){
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            }else if( !source.parentId && target.parentId && add==false){
                source.group = target.group;
                if(target){

                }

                //we move from root to into root item
            }else if( !source.parentId && !target.parentId && add){

                console.error('we are adding an item into root root item');
                if(target.canAdd && target.canAdd()){
                    source.group=null;
                    target.add(source,null,null);
                }
                return true;

                // we move within the same parent
            }else if( source.parentId && target.parentId && add==false && source.parentId === target.parentId){
                console.error('we move within the same parents');
                const parent = this.getBlockById(source.parentId);
                if(!parent){
                    console.error('     couldnt find parent ');
                    return false;
                }

                const maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items,source);
                var cIndexTarget = source.indexOf(items,target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    parent.move(source,direction);
                }
                return true;

                // we move within the different parents
            }else if( source.parentId && target.parentId && add==false && source.parentId !== target.parentId){                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if(!sourceParent){
                    console.error('     couldnt find source parent ');
                    return false;
                }

                const targetParent = this.getBlockById(target.parentId);
                if(!targetParent){
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if(sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()){
                    sourceParent.removeBlock(source,false);
                    targetParent.add(source,null,null);
                }else{
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if(items==null){
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items,source);
                var cIndexTarget = targetParent.indexOf(items,target);
                if(!cIndexSource || !cIndexTarget){
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    targetParent.move(source,direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        const topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId:null
        });


        const grouped = _.groupBy(blocks,function(block){
            return block.group;
        });

        function createDummyBlock(id,scope){

            const block = {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": null,
                    "id": id,
                    "items": [

                    ],
                    "name": id,
                    "method": "----group block ----",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": false,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"

                };

            return scope.blockFromJson(block);

        }

        for(const group in grouped){

            const groupBlock = createDummyBlock(group,scope);
            var blocks = grouped[group];
            _.each(blocks,function(block){
                groupBlock['items'].push(block);



                if(!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }

        console.clear();
        const root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }


    function createScope() {

        const data = {
            "blocks": [
                {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": "click",
                    "id": "root",
                    "items": [
                        "sub0",
                        "sub1"
                    ],
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 1",
                    "method": "console.log('asd',this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },

                {
                    "group": "click4",
                    "id": "root4",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 4",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },
                {
                    "group": "click",
                    "id": "root2",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 2",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },

                {
                    "group": "click",
                    "id": "root3",
                    "description": "Runs an expression.<br/>\n\n<b>Behaviour</b>\n\n<pre>\n\n    //to abort execution (child blocks), return something negative as -1 or false.\n    return false;\n\n</pre>",
                    "name": "Root - 3",
                    "method": "console.log(this);",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0

                },


                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub0",
                    "name": "On Event",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                },
                {
                    "_containsChildrenIds": [],
                    "parentId": "root",
                    "id": "sub1",
                    "name": "On Event2",
                    "event": "",
                    "reference": "",
                    "declaredClass": "xblox.model.events.OnEvent",
                    "_didRegisterSubscribers": false,
                    "enabled": true,
                    "serializeMe": true,
                    "shareTitle": "",
                    "description": "No Description",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"
                }
            ],
            "variables": []
        };

        return fixScope(blockManager.toScope(data));
    }



    if (ctx) {


        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();
        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        var marantz = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.20.meta.json");

        const driver = marantz.driver;
        const device = marantz.device;
        openDriverSettings(driver,device);
        return Grid;
    }


    return Grid;
});