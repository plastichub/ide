/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    'xide/views/CIGroupedSettingsView',
    'xide/registry',
    'xide/_base/_Widget',
    'dcl/dcl'
], function (declare,types,utils, Grid, CIGroupedSettingsView,registry,_Widget,dcl) {
    console.clear();

    const ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;
    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;
    const driverId = '235eb680-cb87-11e3-9c1a-0800200c9a66';
    let deviceId = '21201d2f-6d4f-99f9-5d10-873c9d629150';

    deviceId = '83a2e4ab-27b0-6c89-23c9-743ad6c40031';

    const widget = dcl(_Widget,{
        templateString:'<div>' +
        '<input attachTo="input" data-provide="typeahead" class="typeahead form-control input-transparent" type="text" placeholder="States of USA">'+
        '</div>',
        sources:null,
        options:null,
        startup:function(){
            const substringMatcher = function(strs) {
                return function findMatches(q, cb) {
                    let matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function(i, str) {
                        console.log('test ',str);
                        if (substrRegex.test(str.label)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };
            const states = this.sources || [{"label":"Test","id":"1"},{"label":"Tom","id":"2"}];
            const typehead = this.$input.typeahead(utils.mixin({
                hint: true,
                highlight: true,
                minLength: 1,
                name: 'states',
                showHintOnFocus:true,
                source: substringMatcher(states),
                select: function () {
                    const val = this.$menu.find('.active').data('value');
                    this.$element.data('active', val);
                    if(this.autoSelect || val) {
                        let newVal = this.updater(val);
                        // Updater can be set to any random functions via "options" parameter in constructor above.
                        // Add null check for cases when updater returns void or undefined.
                        if (!newVal) {
                            newVal = "";
                        }
                        this.$element
                            .val(this.displayText(newVal,true) || newVal)
                            .change();

                        this.afterSelect(newVal);
                    }
                    return this.hide();
                },
                highlighter: function(item)
                {
                    return item;
                },
                displayText: function (item,isValue) {
                    !isValue && console.log('---display text ' +item.label);
                    if(isValue){
                        return item.label;
                    }
                    return item.id + ' - ' + '<span class="text-info">'+item.label+'</span>';
                }
            },this.options));
            console.log(typehead);

        }
    });


    function doTests(tab){




        if (ctx) {
            function renderHostCI(view,ci,widget,protocol){
                if(view._hostTypeHead){
                    view._hostTypeHead.destroy();
                    delete view._hostTypeHead;
                }

                let query = {ports:'22'};
                if(protocol ===types.PROTOCOL.TCP){
                    query = {ports:'80'};
                }
                if(protocol ===types.PROTOCOL.MQTT){
                    query = {ports:'27017'};
                }

                const input = widget.nativeWidget;

                const spinner = $('<span class="input-group-addon" style="text-align: left"><i class="fa fa-spinner fa-spin"></i></span>');
                $(widget.previewNode).append(spinner);


                deviceManager.protocolMethod(protocol,'ls',[query]).then(function(data){

                    let items = data.result;
                    //console.error('items',items);
                    items = _.map(items,function(item){
                        return {
                            label:item.host,
                            id:item.host || utils.createUUID(),
                            description:item.description || "No Description"
                        }
                    });


                    ci.value && items.push({
                        label:ci.value,
                        id:ci.value,
                        description:"Last Selected"
                    });


                    if(protocol ===types.PROTOCOL.TCP || protocol ===types.PROTOCOL.SSH || protocol ===types.PROTOCOL.MQTT){
                        items.push({
                            label:"localhost",
                            id:'localhost',
                            description:"Default"
                        });
                        items.push({
                            label:"0.0.0.0",
                            id:'0.0.0.0',
                            description:"Default"
                        });

                    }

                    const substringMatcher = function(strs) {
                        return function findMatches(q, cb) {
                            let matches, substringRegex;
                            // an array that will be populated with substring matches
                            matches = [];

                            // regex used to determine if a string contains the substring `q`
                            substrRegex = new RegExp(q, 'i');

                            // iterate through the pool of strings and for any string that
                            // contains the substring `q`, add it to the `matches` array
                            $.each(strs, function(i, str) {
                                if (substrRegex.test(str.label)) {
                                    //matches.push(str);
                                }
                                matches.push(str);
                            });

                            cb(matches);
                        };
                    };
                    const options = {};




                    let typeahead = widget.nativeWidget.typeahead(utils.mixin({
                        hint: true,
                        highlight: true,
                        minLength: 0,
                        name: 'states',
                        showHintOnFocus:true,
                        source: substringMatcher(items),
                        autoSelect:false,
                        showHintOnFocus:'all',
                        select: function () {
                            const val = this.$menu.find('.active').data('value');
                            this.$element.data('active', val);
                            if(this.autoSelect || val) {
                                let newVal = this.updater(val);
                                // Updater can be set to any random functions via "options" parameter in constructor above.
                                // Add null check for cases when updater returns void or undefined.
                                if (!newVal) {
                                    newVal = "";
                                }
                                this.$element
                                    .val(this.displayText(newVal,true) || newVal)
                                    .change();

                                this.afterSelect(newVal);
                            }
                            return this.hide();
                        },
                        highlighter: function(item)
                        {
                            return item;
                        },
                        displayText: function (item,isValue) {
                            if(isValue){
                                return item.label;
                            }
                            return item.id + ' - ' + '<span class="text-info">'+item.description+'</span>';
                        }
                    },options));
                    typeahead = $(typeahead).data('typeahead');

                    /*
                    typeahead.$element.on('click',function(){
                        //typeahead.show();
                    })
                    */
                    view._hostTypeHead = typeahead;
                    spinner.remove();
                })
            }


            const blockManager = ctx.getBlockManager();
            const driverManager = ctx.getDriverManager();
            var deviceManager = ctx.getDeviceManager();
            const device = deviceManager.getDeviceById(deviceId);

            deviceManager.openItemSettings=function (item) {
                //1. sanity check
                const userData = item.user;
                if (!userData || !userData.inputs) {
                    return null;
                }


                //2. check its not open already
                const viewId = this.getViewId(item);
                let view = registry.byId(viewId);
                if(view && view._parent){
                    utils.destroy(view,true);
                    utils.destroy(view._parent,true);
                    view = null;
                }

                try {
                    if (view) {
                        if (view._parent) {
                            view._parent.select(view);
                        }
                        return view;
                    }
                } catch (e) {
                    utils.destroy(view);
                }


                const docker = this.ctx.mainView.getDocker();
                const parent = docker.addTab(null, {
                    title: utils.toString(item.name),
                    icon: this.beanIconClass
                });

                const title = this.getMetaValue(item, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                const meta = item['user'];
                this.fixDeviceCI(item);
                const driverOptions = utils.getCIByChainAndName(meta, 0, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);

                //console.dir(userData.inputs);
                const hostCI = utils.getCIByChainAndName(meta, 0, types.DEVICE_PROPERTY.CF_DEVICE_HOST);
                const protocolCI = utils.getCIByChainAndName(meta, 0, types.DEVICE_PROPERTY.CF_DEVICE_PROTOCOL);

                view = utils.addWidget(CIGroupedSettingsView, {
                    title: title || utils.toString(item.name),
                    cis: userData.inputs,
                    storeItem: item,
                    id: this.getViewId(item),
                    iconClass: "fa-sliders " + this.getDeviceStatusIcon(item),
                    delegate: this,
                    storeDelegate: this,
                    closable: true,
                    owner: this
                }, this, parent, false, 'deviceView');

                view._on('valueChanged', function (e) {
                    //console.log('on value changed',evt);
                    if(e.oldValue === e.newValue){
                        return;
                    }
                    if(e.ci == protocolCI){
                        renderHostCI.apply(view,[view,hostCI,view._hostWidget,protocolCI.value]);
                    }
                });


                view._on('destroy',function(){
                    delete view._hostTypeHead;
                    delete view._hostWidget;
                })

                view._on('widget',function(e){
                    if(e.ci == hostCI){
                        e.widget._on('startup',function(){
                            view._hostWidget=e.widget;
                            renderHostCI.apply(view,[view,e.ci,e.widget,protocolCI.value]);
                        });
                    }
                });

                view.startup();
                return view;
            }

            deviceManager.openItemSettings(device);


            /*
            deviceManager.protocolMethod(types.PROTOCOL.SERIAL,'ls',['query','*']).then(function(data){
                var items = data.result;

            })
            */

        }
    }

    const _actions = [
        ACTION.RENAME
    ];

    if (ctx) {
        doTests(null);
        return declare('a',null,{});

    }

    return Grid;
});