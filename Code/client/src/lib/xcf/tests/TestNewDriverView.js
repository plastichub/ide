/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'dojo/dom-class',
    "dojo/on",
    "dojo/debounce",
    'dojo/dom-construct',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/Grid',
    'xaction/DefaultActions',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xblox/views/BlockGrid',
    'xide/views/CIGroupedSettingsView',
    'xcf/model/Command',
    'xcf/model/Variable',
    'xide/widgets/ToggleButton',
    'xide/widgets/_ActionValueWidgetMixin',
    'xide/layout/_Accordion',
    "xide/widgets/TemplatedWidgetBase",
    "dijit/form/TextBox",
    'xblox/model/variables/VariableAssignmentBlock',
    'dojo/promise/all',
    "dojo/Deferred",
    "xgrid/KeyboardNavigation",
    "xgrid/Search",
    "xide/tests/TestUtils",
    'xide/_base/_Widget',
    'xide/widgets/_Widget',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'xaction/ActionProvider',
    'xide/widgets/ContextMenu',
    'xide/views/ConsoleView',
    'xide/encoding/MD5',
    'xcf/views/DriverConsole',
    "module"

], function (dcl,declare, domClass,on,debounce,domConstruct,types,
             utils, ListRenderer, Grid, DefaultActions, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,CIViewMixin,BlockGrid,
             CIGroupedSettingsView,
             Command,Variable,
             ToggleButton,_ActionValueWidgetMixin,_Accordion,TemplatedWidgetBase,
             TextBox, VariableAssignmentBlock,
             all,Deferred,KeyboardNavigation,
             Search,TestUtils,_Widget,_CWidget,_LayoutMixin,Docker,ActionProvider,ContextMenu,ConsoleView,MD5,DriverConsole,module) {
    console.clear();











    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;
    let widget;
    let basicGridInstance;







    /***
     * playground
     */
    const _lastGrid = window._lastGrid;
    const ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;
    let scope;
    let blockManager;
    let driverManager;
    var marantz;




    const _actions = [

    ];

    function completeGrid(_grid) {

        _grid._on(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, function (evt) {

            const items = evt.items;

            const variables = this.blockScope.getVariables();
            const variableItems = [
                /*{
                name: 'None',
                //target: item,
                iconClass: 'el-icon-compass',
                proto: VariableAssignmentBlock,
                item: null,
                ctrArgs: {
                    variable: null,
                    scope: this.blockScope,
                    value: ''
                }
            }*/
            ];


            //console.log('on insert block end',variables);


            _.each(variables,function(variable){
                variableItems.push({
                    name: variable.name,
                    //target: item,
                    iconClass: 'el-icon-compass',
                    proto: VariableAssignmentBlock,
                    item: variable,
                    ctrArgs: {
                        variable: variable.name,
                        scope: this.blockScope,
                        value: ''
                    }
                });
            },this);

            items.push({
                name: 'Set Variable',
                iconClass: 'el-icon-pencil-alt',
                items: variableItems
            });









        });

        _grid._on('onAddActions', function (evt) {

            if(!evt.addAction){
                return;
            }
            const addAction = evt.addAction, cmdAction = 'New/Command', varAction = 'New/Variable', permissions = evt.permissions, VISIBILITY = types.ACTION_VISIBILITY, thiz = this;

            /*
            addAction('Save', 'File/Save', 'fa-save', ['ctrl s'], 'Home', 'File', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);
            */



            addAction('Command', cmdAction, 'el-icon-plus-sign', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);

            addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);


            addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {
                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                            widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                            widgetArgs: {
                                icon1: 'fa-toggle-on',
                                icon2: 'fa-toggle-off',
                                delegate: thiz,
                                checked: false,
                                iconClass: 'fa-toggle-off'
                            }
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });



            const settingsWidget = declare('commandSettings', TemplatedWidgetBase,{
                templateString:'<div></div>',
                _getText: function (url) {
                    let result;
                    const def = dojo.xhrGet({
                        url: url,
                        sync: true,
                        handleAs: 'text',
                        load: function (text) {
                            result = text;
                        }
                    });
                    return '' + result + '';
                },
                startup:function(){

                    this.inherited(arguments);

                    if(!_grid.userData){
                        return;
                    }


                    const settings = utils.getJson(_grid.userData['params']) || {
                            constants: {
                                start: '',
                                end: ''
                            },
                            send: {
                                mode: false,
                                interval: 500,
                                timeout: 500,
                                onReply: ''
                            }
                        };




                    const settingsPane = utils.templatify(
                        null,
                        this._getText(require.toUrl('xcf/widgets/templates/commandSettings.html')),
                        this.domNode,
                        {
                            baseClass: 'settings',
                            style:'width:100%',
                            start: settings.constants.start,
                            end: settings.constants.end,
                            interval: settings.send.interval,
                            timeout: settings.send.timeout,
                            sendMode: settings.send.mode,
                            onReply: settings.send.onReply,
                            settings: settings
                        }, null
                    );

                    return;

                    if (settings.send.mode) {
                        settingsPane.rReply.set('checked', true);
                    } else {
                        settingsPane.rInterval.set('checked', true);
                    }

                    const _onSettingsChanged = function () {
                        //update params field of our ci
                        thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                        //thiz.save();
                        console.log('changd');
                    };



                    //wire events
                    dojo.connect(settingsPane.wStart, "onChange", function (item) {
                        settingsPane.settings.constants.start = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wEnd, "onChange", function (item) {
                        settingsPane.settings.constants.end = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wInterval, "onChange", function (item) {
                        settingsPane.settings.send.interval = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wTimeout, "onChange", function (item) {
                        settingsPane.settings.send.timeout = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wOnReply, "onChange", function (item) {
                        settingsPane.settings.send.onReply = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.rReply, "onChange", function (item) {
                        settingsPane.settings.send.mode = item;
                        _onSettingsChanged();
                    });
                }
            });


            addAction('Settings', 'File/Settings', 'fa-gears', null, 'Settings', 'Settings', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {

                        action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null);

                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON,{
                            widgetClass:settingsWidget
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });



        });

        _grid._on('selectionChanged', function (evt) {
            //console.log('selection ',evt);
            //since we can only add blocks to command and not
            //at root level, disable the 'Block/Insert' root action and
            //its widget //references
            const thiz = this, selection = evt.selection, item = selection[0], blockInsert = thiz.getAction('Block/Insert'), blockEnable = thiz.getAction('Step/Enable');

            disable = function (disable) {
                blockInsert.set('disabled', disable);
                setTimeout(function () {
                    blockInsert.getReferences().forEach(function (ref) {
                        ref.set('disabled', disable);
                    });
                }, 100);

            }

            const _disable =item ? false : true;

            disable(_disable);


            if (item) {
                blockEnable.getReferences().forEach(function (ref) {
                    ref.set('checked', item.enabled);
                });
            }else{
                /*
                var props = _grid.getPropertyStruct();
                props._lastItem = null;
                _grid.setPropertyStruct(props);*/
            }
        });

        _grid.startup();

    }

    const propertyStruct = {
        currentCIView:null,
        targetTop:null,
        _lastItem:null
    };

    const DockerBase = dcl([ActionProvider.dcl,_CWidget.dcl],{
        permissions:[
          types.ACTION.RENAME
        ],
        __panel:function(el){
            console.log(el);
            const panels = this.getPanels();

            let frame = null;
            const self = this;
            _.each(this._frameList,function(_frame){
                if($.contains(_frame.$container[0],el)){
                    frame = _frame;
                }
            });




            const id = self.contextMenuPanelId;

            this.contextMenuEventTarget = null;
            this.contextMenuPanelId = null;

            if(frame && id!==null){

                const _panel = frame.panel(id);
                if(_panel){
                    return _panel;
                }
            }
        },
        runAction:function(action,type,event){


            const _panel = this.contextMenuEventTarget ? this.__panel(this.contextMenuEventTarget) : null;
            if(_panel){
                this.removePanel(_panel);
            }

            console.error('run action',_panel);
        },
        getDockerActions:function(){

            const actions = [];

            actions.push(this.createAction({
                label:'Close',
                command:types.ACTION.RENAME,
                tab:'View',
                group:'Misc',
                mixin: {
                    addPermission: true
                },
                icon:'fa-close'
                /*keycombo:['ctrl w','ctrl f4']*/
            }));


            return actions;

        },
        contextMenu:null,
        contextMenuEventTarget:null,
        contextMenuPanelId:null,
        setupActions:function(){

            const self = this;

            this.__on(this._root.$container,'contextmenu', '.wcPanelTab', function(e){
                console.error('context menu');
                self.contextMenuEventTarget  = e.target;
                self.contextMenuPanelId = e.target.parentNode ?  e.target.parentNode.id : null;
            });


            this.addActions(this.getDockerActions());


            const args = {
                owner:this,
                delegate:this,
                limitTo:'wcPanelTab'
            };

            const node = this._root.$container[0];

            const contextMenu = new ContextMenu(args,node);
            contextMenu.openTarget = node;
            contextMenu.init({preventDoubleContext: false});

            //contextMenu._registerActionEmitter(this);
            contextMenu.setActionEmitter(this,types.EVENTS.ON_VIEW_SHOW,this);

            this.contextMenu = contextMenu;


            this.add(contextMenu,null,false);
        },
        __init:dcl.superCall(function(sup){
            return function(){
                if(sup){

                    sup.apply(this, arguments);
                    this.setupActions();
                }
                return 0;
            };
        }),
        __destroy:function(){
            console.error('_destroy');
        }
    });

    const DriverViewClass = dcl([_Widget,_LayoutMixin.dcl],{
        templateString:'<div style="with:inherit;height: inherit"/>',
        showBasicCommands:true,
        showConditionalCommands:true,
        showResponseBlocks:true,
        showLog:true,
        showConsole:true,
        showVariables:true,
        logTab:null,
        consoleTab:null,
        basicCommandsTab:null,
        conditionalTab:null,
        variablesTab:null,
        responseTab:null,
        gridClass:null,
        createTab:function(type,args){

            const DOCKER = types.DOCKER;
            const defaultTabArgs = {
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:DOCKER.TAB.TOP,
                location:DOCKER.DOCK.STACKED

            };
            return this._docker.addTab(type || 'DefaultTab',utils.mixin(defaultTabArgs,args));
        },
        createLayout:function(){


            const DOCKER = types.DOCKER;
            this._docker = Docker.createDefault(this.domNode,{
                extension:DockerBase
            });

            const basicCommands = this.showBasicCommands ? this.createTab(null,{
                title:'Basic Commands'
            }) : null;

            const condCommands = this.showConditionalCommands ? this.createTab(null,{
                title:'Condition Commands',
                tabOrientation:DOCKER.TAB.TOP,
                target:basicCommands
            }) : null;

            const log = this.showLog ? this.createTab(null,{
                title:'Log',
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM,
                icon:'fa-calendar'
            }) : null;

            const consoleTab = this.showConsole ? this.createTab(null,{
                title:'Console',
                tabOrientation:DOCKER.TAB.TOP,
                target:log,
                icon:'fa-terminal'
            }) : null;

            const lastBottom = log || consoleTab;



            const responses = this.showResponseBlocks ? this.createTab(null,{
                title:'Responses',
                tabOrientation:DOCKER.TAB.TOP,
                target:lastBottom
            }) : null;

            const variables = this.showVariables ? this.createTab(null,{
                title:'Variables',
                tabOrientation:DOCKER.TAB.TOP,
                target:lastBottom
            }) : null;

            this.logTab=log;
            this.consoleTab=consoleTab;
            this.basicCommandsTab = basicCommands;
            this.conditionalTab = condCommands;
            this.variablesTab = variables;

            this.responseTab=responses;


            return this._docker;


        },
        completeGrid:function(grid){

            grid.userData = this.userData;
            const widget = this;
            function _completeGrid(_grid) {
                _grid._on('onAddActions', function (evt) {


                    const addAction = evt.addAction, cmdAction = 'New/Command', varAction = 'New/Variable', permissions = evt.permissions, VISIBILITY = types.ACTION_VISIBILITY, thiz = this;

                    if(!addAction){
                        return;
                    }

                    addAction('Command', cmdAction, 'el-icon-plus-sign', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,{
                        dummy: true,
                        addPermission: true
                    },null, null);

                    addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,{
                        dummy: true,
                        addPermission: true
                    },null, null);



                    addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                        {
                            addPermission: true,
                            onCreate: function (action) {
                                action.handle=false;
                                action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                                    widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                                    widgetArgs: {
                                        icon1: 'fa-toggle-on',
                                        icon2: 'fa-toggle-off',
                                        delegate: thiz,
                                        checked: false,
                                        iconClass: 'fa-toggle-off'
                                    }
                                });
                            }
                        }, null, function () {
                            return thiz.getSelection().length == 0;
                        });



                    const settingsWidget = declare('commandSettings', TemplatedWidgetBase,{
                        templateString:'<div></div>',
                        _getText: function (url) {
                            let result;
                            const def = dojo.xhrGet({
                                url: url,
                                sync: true,
                                handleAs: 'text',
                                load: function (text) {
                                    result = text;
                                }
                            });
                            return '' + result + '';
                        },
                        startup:function(){
                            if(this._started){
                                return;
                            }

                            this.inherited(arguments);

                            if(!_grid.userData){
                                return;
                            }

                            const settings = utils.getJson(_grid.userData['params']) || {
                                    constants: {
                                        start: '',
                                        end: ''
                                    },
                                    send: {
                                        mode: false,
                                        interval: 500,
                                        timeout: 500,
                                        onReply: ''
                                    }
                                };

                            if(!widget.settingsTemplate){
                                widget.settingsTemplate = this._getText(require.toUrl('xcf/widgets/templates/commandSettingsNew.html'));
                            }


                            const settingsPane = utils.templatify(
                                null,
                                widget.settingsTemplate,
                                this.domNode,
                                {
                                    baseClass: 'settings',
                                    start: settings.constants.start,
                                    end: settings.constants.end,
                                    interval: settings.send.interval,
                                    timeout: settings.send.timeout,
                                    sendMode: settings.send.mode,
                                    onReply: settings.send.onReply,
                                    settings: settings
                                }, null
                            );


                            if (settings.send.mode) {
                                $(settingsPane.rReply).prop('checked', true);
                            } else {
                                $(settingsPane.rInterval).prop('checked', true);
                            }



                            const _onSettingsChanged = function () {
                                //update params field of our ci
                                thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                                widget.setValue('{}');
                            };

                            //start
                            $(settingsPane.start).on('change',function(e){
                                settingsPane.settings.constants.start = e.target.value;
                                _onSettingsChanged();
                            });

                            //end
                            $(settingsPane.end).on('change',function(e){
                                settingsPane.settings.constants.end = e.target.value;
                                _onSettingsChanged();
                            });






                            //mode
                            $(settingsPane.rReply).on("change", function (e) {
                                const value = e.target.value=='on' ? 1:0;
                                settingsPane.settings.send.mode = value;
                                _onSettingsChanged();
                            });

                            $(settingsPane.rInterval).on("change", function (e) {
                                const value = e.target.value=='on' ? 0:1;
                                settingsPane.settings.send.mode = value;
                                _onSettingsChanged();
                            });


                            //interval time
                            $(settingsPane.wInterval).on('change',function(e){
                                settingsPane.settings.send.interval = e.target.value;
                                _onSettingsChanged();
                            });






                            //on reply value
                            $(settingsPane.wOnReply).on('change',function(e){

                                settingsPane.settings.send.onReply = e.target.value;
                                _onSettingsChanged();
                            });

                            //onReply - timeout
                            $(settingsPane.wTimeout).on('change',function (e) {
                                settingsPane.settings.send.timeout = e.target.value;
                                _onSettingsChanged();
                            });

                        }
                    });


                    addAction('Settings', 'File/Settings', 'fa-gears', null, 'Settings', 'Settings', 'item|view', null, null,
                        {
                            addPermission: true,
                            handle:false,
                            onCreate: function (action) {

                                handle:false,

                                    action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, null);

                                action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                                action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null);

                                action.setVisibility(types.ACTION_VISIBILITY.RIBBON,{
                                    widgetClass:settingsWidget
                                });
                            }
                        }, null, function () {
                            return thiz.getSelection().length == 0;
                        });



                });
                _grid._on('selectionChanged', function (evt) {

                    //since we can only add blocks to command and not
                    //at root level, disable the 'Block/Insert' root action and
                    //its widget //references
                    const thiz = this, selection = evt.selection, item = selection[0], blockInsert = thiz.getAction('Block/Insert'), blockEnable = thiz.getAction('Step/Enable'), newCommand = thiz.getAction('New/Command');


                    disable = function (disable) {

                        blockInsert.set('disabled', disable);

                        setTimeout(function () {
                            blockInsert.getReferences().forEach(function (ref) {
                                ref.set('disabled', disable);
                            });
                        }, 100);

                    }

                    let _disable = item ? false : true;
                    if(_grid.blockGroup === 'conditionalProcess'){
                        _disable=false;
                        newCommand.set('disabled', true);
                        setTimeout(function () {
                            newCommand.getReferences().forEach(function (ref) {
                                ref.set('disabled', true);
                            });
                        }, 100);
                    }

                    disable(_disable);


                    if (item) {
                        blockEnable.getReferences().forEach(function (ref) {
                            ref.set('checked', item.enabled);
                        });
                    }else{
                        /*
                         var props = _grid.getPropertyStruct();
                         props._lastItem = null;
                         _grid.setPropertyStruct(props);*/
                    }
                });
            }

            _completeGrid(grid);

            this.add(grid,null,false);
        },
        createGrid:function(tab,scope,store,group,newItemLabel,options){
            const _docker = this._docker;
            const widget = this;
            let grid;
            const gridClass = this.getGridClass();
            const grids = this.grids;


            const args = utils.mixin({
                /*
                __getRight:function(){
                    return widget.__right;
                },
                */
                ctx:this.ctx,
                blockScope: scope,
                toolbarInitiallyHidden:true,
                blockGroup: group,
                attachDirect:true,
                collection: store.filter({
                    group: group
                }),
                //dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                //____right:this.__right,
                //__docker:_docker,
                setPanelSplitPosition:widget.setPanelSplitPosition,
                getPanelSplitPosition:widget.getPanelSplitPosition
            },options);

            try {
                grid = utils.addWidget(gridClass, args, null, tab, false);
                this.completeGrid(grid, newItemLabel);
                grids.push(grid);
            }catch(e){
                logError(e);
            }
            tab.add(grid,null,false);
            return grid;
        },
        getGridClass:function(){

            if(this.gridClass){
                //return this.gridClass;
            }

            const propertyStruct = {
                currentCIView:null,
                targetTop:null,
                _lastItem:null
            };

            const gridClass = declare('driverGrid', BlockGrid,{
                toolbarInitiallyHidden:true,
                highlightDelay:1000,
                propertyStruct : propertyStruct,
                /**
                 * Step/Run action
                 * @param block {block[]}
                 * @returns {boolean}
                 */
                _execute: function (blocks) {

                    const thiz = this;


                    function _clear(element){

                        if(element) {
                            setTimeout(function () {
                                element.removeClass('failedBlock successBlock activeBlock');
                            }, thiz.highlightDelay);
                        }
                    }

                    function _node(block){
                        if(block) {
                            const row = thiz.row(block);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    };

                    function mark(element,cssClass){

                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                        }
                    }

                    const dfds = [], EVENTS = types.EVENTS;

                    const _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                        mark(_node(evt),'activeBlock');

                    });
                    const _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        //console.log('mark success');
                        mark(_node(evt),'successBlock');
                        _clear(_node(evt));
                    });

                    const _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        mark(_node(evt),'failedBlock');
                        _clear(_node(evt));

                    });




                    function run(block) {

                        if (!block || !block.scope) {
                            console.error('have no scope');
                            return;
                        }
                        try {

                            const blockDfd = block.scope.solveBlock(block, {
                                highlight: true,
                                force: true,
                                listener:thiz
                            });

                            dfds.push(blockDfd);

                            /*
                             blockDfd.then(function(result){
                             console.log('did run! : ' + result);
                             });
                             */
                            //console.log('run block result:',result);


                        } catch (e)
                        {

                            console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                            //console.error(printStackTrace().join('\n\n'));
                        }
                        return true;
                    }

                    blocks  = _.isArray(blocks) ? blocks : [blocks];


                    function _patch(block){


                        block.runFrom=function(_blocks, index, settings)
                        {

                            const thiz = this, blocks = _blocks || this.items, allDfds = [];

                            const onFinishBlock = function (block, results) {
                                block._lastResult = block._lastResult || results;
                                thiz._currentIndex++;
                                thiz.runFrom(blocks, thiz._currentIndex, settings);
                            };


                            const wireBlock = function (block) {
                                block._deferredObject.then(function (results) {
                                    console.log('----def block finish');
                                    onFinishBlock(block, results);
                                });
                            };

                            if (blocks.length) {

                                for (let n = index; n < blocks.length; n++) {



                                    const block = blocks[n];

                                    console.log('run child \n'+block.method);



                                    /*
                                     _patch(block);
                                     var blockDfd = block.solve(this.scope, settings);
                                     allDfds.push(blockDfd);
                                     */
                                    _patch(block);

                                    if (block.deferred === true) {
                                        block._deferredObject = new Deferred();
                                        this._currentIndex = n;
                                        wireBlock(block);
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));
                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                        break;
                                    } else {
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));

                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                    }

                                }

                            } else {
                                this.onSuccess(this, settings);
                            }

                            return allDfds;
                        };

                        block.solve=function(scope,settings,run,error){

                            this._currentIndex = 0;
                            this._return=[];

                            const _script = '' + this._get('method');
                            const thiz=this,
                                  ctx = this.getContext(),
                                  items = this[this._getContainer()],
                                  //outer,head dfd
                                  dfd = new Deferred,
                                  listener = settings.listener,
                                  isDfd = thiz.deferred;



                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                            }

                            function _finish(result){

                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }

                                dfd.resolve(result);


                            }

                            function _error(result){
                                dfd.reject(result);
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                                }
                            }


                            function _headDone(result){
                                //console.log('_headDone : ',result);
                                //more blocks?
                                if(items.length) {
                                    const subDfds = thiz.runFrom(items,0,settings);
                                    all(subDfds).then(function(what){
                                        console.log('all solved!',what);
                                        _finish(result);
                                    },function(err){

                                        console.error('error in chain',err);
                                        if(listener) {
                                            listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                        }
                                        dfd.resolve(err);
                                    });

                                }else{
                                    if(listener) {
                                        listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                    }
                                    dfd.resolve(result);
                                }
                            }




                            if(_script && _script.length){
                                const runScript = function() {


                                    const _function = new Function("{" + _script + "}");
                                    const _args = thiz.getArgs() || [];
                                    try {

                                        if(isDfd){

                                            ctx.resolve=function(result){
                                                console.log('def block done');
                                                if(thiz._deferredObject) {
                                                    thiz._deferredObject.resolve();
                                                }
                                                _headDone(result);
                                            }
                                        }
                                        const _parsed = _function.apply(ctx, _args || {});
                                        thiz._lastResult = _parsed;
                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }

                                        if(!isDfd) {
                                            _headDone(_parsed);
                                        }

                                        if (_parsed !== 'false' && _parsed !== false) {

                                        } else {
                                            //thiz.onFailed(thiz, settings);
                                            //return [];
                                        }
                                    } catch (e) {

                                        e=e ||{};

                                        _error(e);

                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                };

                                if(scope.global){
                                    (function() {

                                        window = scope.global;


                                        const _args = thiz.getArgs() || [];
                                        try {
                                            let _parsed = null;
                                            if(!ctx.runExpression) {
                                                const _function = new Function("{" + _script + "}").bind(this);
                                                _parsed = _function.apply(ctx, _args || {});
                                            }else{
                                                _parsed = ctx.runExpression(_script,null,_args);
                                            }

                                            thiz._lastResult = _parsed;

                                            if (run) {
                                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                                            }
                                            if (_parsed !== 'false' && _parsed !== false) {
                                                thiz.onSuccess(thiz, settings);
                                            } else {
                                                thiz.onFailed(thiz, settings);
                                                return [];
                                            }


                                        } catch (e) {

                                            thiz._lastResult = null;
                                            if (error) {
                                                error('invalid expression : \n' + _script + ': ' + e);
                                            }
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }

                                    }).call(scope.global);

                                }else{
                                    runScript();
                                }

                            }else{
                                console.error('have no script');
                            }

                            return dfd;
                        }

                    }

                    _.each(blocks,_patch);

                    _.each(blocks,run);

                    all(dfds).then(function(){
                        console.log('did run all selected blocks!',thiz);
                        _runHandle.remove();
                        _successHandle.remove();
                        _errHandle.remove();
                    });

                    /*
                     function run(block) {
                     if (!block || !block.scope) {
                     console.error('have no scope');
                     return;
                     }
                     try {
                     var result = block.scope.solveBlock(block, {
                     highlight: true,
                     force: true
                     });
                     } catch (e) {
                     console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                     console.error(printStackTrace().join('\n\n'));
                     }
                     return true;
                     }

                     blocks  = _.isArray(blocks) ? blocks : [blocks];


                     _.each(blocks,run);
                     */

                },
                execute: function (_blocks) {

                    const thiz = this;

                    console.error('exe');

                    ////////////////////////////////////////////////////////////////////////
                    //
                    //  Visual helpers to indicate run status:
                    //
                    function _clear(element){
                        if(element) {
                            setTimeout(function () {
                                element.removeClass('failedBlock successBlock activeBlock');
                            }, thiz.highlightDelay);
                        }
                    }

                    function _node(block){
                        if(block) {
                            const row = thiz.row(block);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    };

                    function mark(element,cssClass){

                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                        }
                    }

                    const //all Deferreds of selected blocks to run
                          dfds = [],
                          //shortcut
                          EVENTS = types.EVENTS,
                          //normalize selection to array
                          blocks  = _.isArray(_blocks) ? _blocks : [_blocks],
                          //event handle "Run"
                          _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                              mark(_node(evt),'activeBlock');
                          }),
                          //event handle "Success"
                          _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                              //console.log('marke success',evt);
                              mark(_node(evt),'successBlock');
                              _clear(_node(evt));
                          }),
                          //event handle "Error"
                          _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                              mark(_node(evt),'failedBlock');
                              _clear(_node(evt));
                          });






                    function run(block) {

                        if (!block || !block.scope) {
                            console.error('have no scope');
                            return;
                        }
                        try {

                            const blockDfd = block.scope.solveBlock(block, {
                                highlight: true,
                                force: true,
                                listener:thiz
                            });


                            dfds.push(blockDfd);

                            /*
                             blockDfd.then(function(result){
                             console.log('did run! : ' + result);
                             });
                             */
                            //console.log('run block result:',result);


                        } catch (e)
                        {
                            console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);

                            logError(e,'excecuting block -  ' + block.name + ' failed! : ');

                            //console.error(printStackTrace().join('\n\n'));

                        }
                        return true;
                    }



                    function _patch(block){


                        block.runFrom=function(_blocks, index, settings){

                            const thiz = this, blocks = _blocks || this.items, allDfds = [];

                            const onFinishBlock = function (block, results) {
                                block._lastResult = block._lastResult || results;
                                thiz._currentIndex++;
                                thiz.runFrom(blocks, thiz._currentIndex, settings);
                            };

                            const wireBlock = function (block) {
                                block._deferredObject.then(function (results) {
                                    console.log('----def block finish');
                                    onFinishBlock(block, results);
                                });
                            };

                            if (blocks.length) {

                                for (let n = index; n < blocks.length; n++) {



                                    const block = blocks[n];

                                    console.log('run child \n'+block.method);

                                    _patch(block);

                                    if (block.deferred === true) {
                                        block._deferredObject = new Deferred();
                                        this._currentIndex = n;
                                        wireBlock(block);
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));
                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                        break;
                                    } else {
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));

                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                    }

                                }

                            } else {
                                this.onSuccess(this, settings);
                            }

                            return allDfds;
                        };

                        block.solve=function(scope,settings,run,error){

                            this._currentIndex = 0;
                            this._return=[];

                            const _script = '' + this._get('method');

                            const thiz=this,
                                  ctx = this.getContext(),
                                  items = this[this._getContainer()],
                                  //outer,head dfd
                                  dfd = new Deferred,
                                  listener = settings.listener,
                                  isDfd = thiz.deferred;



                            //moved to Contains#onRunThis
                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                            }

                            //function when a block did run successfully,
                            // moved to Contains#onDidRunItem
                            function _finish(dfd,result,event){

                                if(listener) {
                                    listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }
                                dfd.resolve(result);


                            }

                            //function when a block did run successfully
                            function _error(result){
                                dfd.reject(result);
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                                }
                            }


                            //moved to Contains#onDidRunThis
                            function _headDone(result){


                                //more blocks?
                                if(items.length) {
                                    const subDfds = thiz.runFrom(items,0,settings);

                                    all(subDfds).then(function(what){
                                        console.log('all solved!',what);
                                        _finish(dfd,result);
                                    },function(err){
                                        console.error('error in chain',err);
                                        _finish(dfd,err);
                                    });

                                }else{
                                    _finish(dfd,result);
                                }
                            }


                            if(_script && _script.length){

                                const runScript = function() {

                                    const _function = new Function("{" + _script + "}");
                                    const _args = thiz.getArgs() || [];
                                    try {

                                        if(isDfd){

                                            ctx.resolve=function(result){
                                                console.log('def block done');
                                                if(thiz._deferredObject) {
                                                    thiz._deferredObject.resolve();
                                                }
                                                _headDone(result);
                                            }
                                        }
                                        const _parsed = _function.apply(ctx, _args || {});
                                        thiz._lastResult = _parsed;
                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }


                                        if(!isDfd) {
                                            console.log('root block done');
                                            //_headDone(_parsed);
                                            thiz.onDidRunThis(dfd,_parsed,items,settings);
                                        }

                                        if (_parsed !== 'false' && _parsed !== false) {

                                        } else {
                                            //thiz.onFailed(thiz, settings);
                                            //return [];
                                        }
                                    } catch (e) {

                                        e=e ||{};

                                        _error(e);

                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                };

                                if(scope.global){
                                    (function() {

                                        window = scope.global;


                                        const _args = thiz.getArgs() || [];
                                        try {
                                            let _parsed = null;
                                            if(!ctx.runExpression) {
                                                const _function = new Function("{" + _script + "}").bind(this);
                                                _parsed = _function.apply(ctx, _args || {});
                                            }else{
                                                _parsed = ctx.runExpression(_script,null,_args);
                                            }

                                            thiz._lastResult = _parsed;

                                            if (run) {
                                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                                            }
                                            if (_parsed !== 'false' && _parsed !== false) {
                                                thiz.onSuccess(thiz, settings);
                                            } else {
                                                thiz.onFailed(thiz, settings);
                                                return [];
                                            }


                                        } catch (e) {

                                            thiz._lastResult = null;
                                            if (error) {
                                                error('invalid expression : \n' + _script + ': ' + e);
                                            }
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }

                                    }).call(scope.global);

                                }else{
                                    runScript();
                                }

                            }else{
                                console.error('have no script');
                            }
                            return dfd;
                        }


                    }

                    //_.each(blocks,_patch);

                    _.each(blocks,run);

                    all(dfds).then(function(){
                        console.log('did run all selected blocks!',thiz);
                        _runHandle.remove();
                        _successHandle.remove();
                        _errHandle.remove();
                    });



                },
                onCIChanged: function (ci, block, oldValue, newValue, field) {

                    console.log('on ci changed', arguments);
                    const _col= this.collection;
                    block = this.collection.getSync(block.id);
                    block.set(field, newValue);
                    block[field]=newValue;
                    _col.refreshItem(block);
                },
                _itemChanged: function (type, item, store) {

                    store = store || this.getStore(item);

                    const thiz = this;

                    function _refreshParent(item, silent) {

                        const parent = item.getParent();
                        if (parent) {
                            const args = {
                                target: parent
                            };
                            if (silent) {
                                this._muteSelectionEvents = true;
                            }
                            store.emit('update', args);
                            if (silent) {
                                this._muteSelectionEvents = false;
                            }
                        } else {
                            thiz.refresh();
                        }
                    }

                    function select(item) {

                        thiz.select(item, null, true, {
                            focus: true,
                            delay: 20,
                            append: false
                        });
                    }

                    switch (type) {

                        case 'added':
                        {
                            //_refreshParent(item);
                            //this.deselectAll();
                            this.refresh();
                            select(item);
                            break;
                        }

                        case 'changed':
                        {
                            this.refresh();
                            select(item);
                            break;
                        }


                        case 'moved':
                        {
                            //_refreshParent(item,true);
                            //this.refresh();
                            //select(item);
                            break;
                        }

                        case 'deleted':
                        {

                            const parent = item.getParent();
                            //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                            const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                            if (parent) {
                                const _container = parent.getContainer();
                                if (_container) {
                                    _.each(_container, function (child) {
                                        if (child.id == item.id) {
                                            _container.remove(child);
                                        }
                                    });
                                }
                            }

                            this.refresh();
                            /*
                             if (_prev) {
                             select(_prev);
                             }
                             */
                            break;
                        }

                    }


                },
                _onFocusChanged:function(focused,type){
                    this.inherited(arguments);
                    if(!focused){
                        this._lastSelection = [];
                    }

                },
                save:function(){



                    const thiz = this,
                          driver = thiz.userData.driver,
                          ctx = thiz.userData.ctx,
                          fileManager = ctx.getFileManager(),
                          //instance scope
                          scope = thiz.blockScope,
                          instance = scope.instance,
                          //original driver scope
                          originalScope = driver.blockScope,
                          path = driver.path.replace('.meta.json','.xblox'),
                          scopeToSave = originalScope || scope,
                          mount = driver.scope;



                    if(originalScope && scopeToSave!=originalScope){
                        originalScope.fromScope(scope);
                    }

                    if (scope) {

                        const all = {
                            blocks: null,
                            variables: null
                        };

                        const blocks = scope.blocksToJson();
                        try {
                            //test integrity
                            dojo.fromJson(JSON.stringify(blocks));
                        } catch (e) {
                            console.error('invalid data');
                            return;
                        }

                        const _onSaved = function () {};

                        all.blocks = blocks;

                        console.log('saving driver ' + mount + '/'+path,driver);


                        fileManager.setContent(mount,path,JSON.stringify(all, null, 2),_onSaved);
                        //this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
                    }
                },
                runAction: function (action) {

                    const thiz = this;
                    const sel = this.getSelection();



                    function addItem(_class,group){

                        const cmd = factory.createBlock(_class, {
                            name: "No Title",
                            send: "nada",
                            scope: thiz.blockScope,
                            group: group
                        });


                        thiz.deselectAll();
                        _.each(thiz.grids,function(grid){
                            if(grid) {
                                grid.refresh();
                            }
                        });
                        setTimeout(function () {
                            thiz.select([cmd],null,true,{
                                focus:true
                            });
                        }, 200);

                    }
                    if (action.command == 'New/Command') {
                        addItem(Command,this.blockGroup);
                    }

                    if (action.command == 'New/Variable') {
                        addItem(Variable,'basicVariables');
                    }

                    if (action.command == 'File/Save') {
                        this.save();
                    }
                    return this.inherited(arguments);
                },
                startup:function(){

                    const thiz = this;

                    this.inherited(arguments);

                    function _node(evt){
                        const item = evt.target;
                        if(item) {
                            const row = thiz.row(item);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    };

                    function mark(element,cssClass){
                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                            setTimeout(function () {
                                element.removeClass(cssClass);
                                thiz._isHighLighting = false;
                            }, thiz.highlightDelay);
                        }
                    };

                    this.subscribe(types.EVENTS.ON_RUN_BLOCK,function(evt){
                        mark(_node(evt),'activeBlock');
                    });
                    this.subscribe(types.EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        mark(_node(evt),'failedBlock');
                    });
                    this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        mark(_node(evt),'successBlock');
                    });


                    this.collection.on('update',function(evt){

                        const item = evt.target, node = _node(evt), type = evt.type;

                        if(type==='update' && evt.property ==='value'){
                            mark(node,'successBlock');
                        }

                        //console.warn('on store updated ', args);
                    });


                }
            });

            this.gridClass = gridClass;

            return gridClass;
        },
        createConsole:function(tab){
            const deviceManager = this.ctx.getDeviceManager();
            return openConsole.apply(deviceManager,[tab,this.driver,this.device]);

        },
        startup:function(){


            const self = this,
                  ci = self.userData,
                  device = self.device,
                  driver = self.driver,
                  //original or device instance
                  scope = device ? device.blockScope : driver.blockScope;

                if(!scope){
                    console.error('have no scope!');
                    return ;
                }

                const store = scope.blockStore, instance = driver.instance;




            const _docker = this.createLayout();

            this._docker = _docker;

            //this.__right = this.getRightPanel('Properties',1);

/*
            this.__rightBottom = this.createTab(null,{
                title:'Description',
                tabOrientation:types.DOCKER.TAB.BOTTOM,
                target:this.__right
            });
*/




            this.grids = [];

            this.basicCommandsGrid = this.createGrid(this.basicCommandsTab,scope,store,'basic','Command');
            this.conditionalCommandsGrid = this.createGrid(this.conditionalTab,scope,store,'conditional','Command');
            this.variablesGrid = this.createGrid(this.variablesTab,scope,store,'basicVariables','Variable',{
                columns:[
                    {
                        label: "Name",
                        field: "name",
                        sortable: true,
                        width:'20%',
                        editorArgs: {
                            required: true,
                            promptMessage: "Enter a unique variable name",
                            //validator: thiz.variableNameValidator,
                            //delegate: thiz.delegate,
                            intermediateChanges: false
                        }
                        //editor: TextBox,
                        //editOn:'click',
                        //_editor: ValidationTextBox
                    },
                    {
                        label: "Initialize",
                        field: "initial",
                        sortable: false,
                        _editor: TextBox,
                        editOn:'click'
                    },
                    {
                        label: "Value",
                        field: "value",
                        sortable: false,
                        _editor: TextBox,
                        editOn:'click',
                        formatter:function(value,object){
                            //console.log('format var ' + value,object);
                            return value;
                        },
                        editorArgs: {
                            autocomplete:'on',
                            templateString:'<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"'+
                            '><div class="dijitReset dijitInputField dijitInputContainer"'+
                            '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"'+
                            '${!nameAttrSetting} type="${type}"'+
                            '/></div'+
                            '></div>'
                        }
                    }
                ]
            });

            //debugger;
            this.basicCommandsTab.select();
            _docker.resize();

            const _console = this.createConsole(this.consoleTab);

            this.add(_console,null,false);


            /*
            this.basicCommandsGrid.startup();

            this.basicCommandsGrid.set('collection',store.filter({group: 'basic'}));

            */

        }
    });



    const EditorClass = dcl(ConsoleView.Editor,{
        didAddMCompleter:false,
        multiFileCompleter:null,
        blockScope:null,
        driverInstance:null,
        onACEReady: function (editor) {



            const add = true;
            const scope = this.blockScope;
            const driverInstance = this.driverInstance;
            const driver = this.driver;
            const device = this.device;

            let variables = scope.getVariables({
                group: 'basicVariables'
            });

            const _responseVariables = scope.getVariables({
                group: 'processVariables'
            });

            if (_responseVariables && _responseVariables.length) {
                variables = variables.concat(_responseVariables);
            }
            const completors = [];
            var path = driver.path;
            function createCompleter(text, value, help) {
                return {
                    word: text,
                    value: value,
                    meta: help || ''
                };
            }

            if (variables && variables.length > 0) {
                for (let i = 0; i < variables.length; i++) {
                    const obj = variables[i];

                    completors.push(createCompleter(obj.name, obj.value, 'Driver Variable'));

                }
            }


            for(var prop in driverInstance){

                var val = driverInstance[prop];

                if(_.isFunction(val)){
                    completors.push(createCompleter(prop, prop, 'Driver Method'));
                }else {

                    if (_.isString(val) || _.isObject(val)) {
                        completors.push(createCompleter(prop, prop, 'Driver Property'));
                    }
                }

                /*
                 if(_.isFunction(val) || _.isString(val) || _.isFunction(val)){
                 completors.push(createCompleter(prop, prop, 'Driver Method'));
                 }
                 */
            }



            if (completors.length) {
                this.addAutoCompleter(completors);
            }



            return ;


            const completer = this.addFileCompleter();
            const text = "var xxxTop = 2*3;";
            var path = driver.path;

            let _text = '';

            for(var prop in driverInstance){

                var val = driverInstance[prop];
                if(_.isFunction(val)){

                    _text +='\n';
                    _text +=prop;
                    _text +=':';
                    _text += val.toString();
                    _text +=',\n';
                }
            }
            //var _text = driverInstance.toString();
            //console.error(_text);

            completer.addDocument(path,_text);


        },
        onEditorCreated:function(editor,options){
            this.inherited(arguments);
            this.onACEReady(editor);

        }
    });

    const _DriverConsole = dcl(ConsoleView,{
        onAddEditorActions:dcl.superCall(function(sup){




            return function(evt){
                //grab the result from the handler

                const res = sup.call(this, evt);

                const actions = evt.actions, owner  = evt.owner;
                const save = _.find(actions,{
                    command:'File/Save'
                });

                actions.remove(_.find(actions,{
                    command:'File/Save'
                }));

                actions.remove(_.find(actions,{
                    command:"File/Reload"
                }));


                const mixin = {
                    addPermission:true
                };



                actions.push(owner.createAction({
                    label: 'Send',
                    command: 'Console/Send',
                    icon: 'fa-paper-plane',
                    group: 'Console',
                    tab:'Home',
                    mixin:mixin,
                    handler:function(){
                        debugger;
                    }
                }));



                //console.error('console - view : onConsoleEnter: ' + command,res);
                /*
                 var _console = this.getConsole(),
                 editor = this.getConsoleEditor();
                 */


                console.error('add actions');

            };
        }),
        logTemplate:'<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>',
        _parse : function (scope, expression) {
            let str = '' + expression;
            if (str.indexOf('{{') > 0 || str.indexOf('}}') > 0) {
                console.time('parse expression');
                const _parser = new Expression();
                str = _parser.parse(types.EXPRESSION_PARSER.FILTREX,
                    str, this,
                    {
                        variables: scope.getVariablesAsObject(),
                        delimiters: {
                            begin: '{{',
                            end: '}}'
                        }
                    }
                );
                console.timeEnd('parse expression');
            }else{
                const _text = scope.parseExpression(expression);
                if(_text){
                    str = _text;
                }
            }
            return str;
        },
        parse:function(str) {
            const driverInstance = this.driverInstance;
            if (driverInstance && driverInstance.blockScope) {
                return this._parse(driverInstance.blockScope, str);
            }
            return str;
        }
    });


    function openConsole(tab,driver,device){


        this.startDevice(device);
        const cInfo = this.toDeviceControlInfo(device);
        



        const hash = MD5(JSON.stringify(cInfo), 1), viewId = hash + '-Console', thiz = this, deviceInstance = this.deviceInstances[hash], docker = this.ctx.mainView.getDocker(), self = this;

            const scope = driver.blockScope, instance = scope.instance, originalScope = instance ? instance.blockScope : null, blockScope = originalScope || scope, driverInstance = this.getDriverInstance(cInfo, true);


        const ConsoleViewClass = dcl(DriverConsole,{
            destroy:function(){
                self.consoles[viewId].remove(this);
            }
        });

        const args =  {
            driver:driver,
            device:device,
            deviceInstance:deviceInstance,
            driverInstance:driverInstance,
            blockScope:blockScope
        };


        const view = utils.addWidget(ConsoleViewClass,utils.mixin({
            ctx:this.ctx,
            type:'javascript',
            value:'return this.getVariable(\'Volume\');',
            editorArgs:args,
            EditorClass:EditorClass,
            owner:this,
            log: function (msg) {

                const printTemplate = '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>';
                let out = '';
                if (_.isString(msg)) {
                    out += msg.replace(/\n/g, '<br/>');
                } else if (_.isObject(msg) || _.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                }
                const items = out.split('<br/>');

                for (let i = 0; i < items.length; i++) {
                    this.printCommand('',items[i],this.logTemplate,true);
                }
            },
            onConsoleEnter: function(command){

                const consoleWidget = this.getConsole();


                let _resolved = this.parse(command);

                this.owner.sendDeviceCommand(this.deviceInstance, _resolved);

                if (_resolved == command) {
                    _resolved == '';
                } else {
                    _resolved = '<span class="text-info"><b>' + command + '</span></b><span>'  +  '  evaluates to <span class="text-success">' + _resolved +  '</span>';
                }
                this.printCommand(_resolved,'');
                return _resolved;
            }

        },args),null,tab,false);



        tab.add(view,null,false);


        view.consoleId = viewId;

        if(!this.consoles){
            this.consoles = [];
        }

        if(!this.consoles[viewId]){
            this.consoles[viewId] = [];
        }
        this.consoles[viewId].push(view);

        return view;

    }

    function openDriverView(driver,device){
        const parent = TestUtils.createTab(null,null,module.id);
        const title = 'Marantz Instance';
        let devinfo = null;
        const deviceManager = ctx.getDeviceManager();
        if(device){
            devinfo  = ctx.getDeviceManager().toDeviceControlInfo(device);
        }


        //@Todo:driver, store device temporarly in Commands CI
        const commandsCI = utils.getCIByChainAndName(driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
        if(commandsCI){
            commandsCI.device = device;
        }

        //ctx.getAction('Window/Navigation').handler(true);


        //deviceManager.openConsole = openConsole;

        const driverView = utils.addWidget(DriverViewClass,{
            userData:commandsCI,
            driver:driver,
            device:device,
            ctx:ctx
        },null,parent,true);

        parent.add(driverView,null,false);
    }






    function createLogGridClass(){




        const filterClass = declare('filter',null,{
            showFooter: true,
            buildRendering: function () {

                this.inherited(arguments);
                const grid = this;
                const filterNode = this.filterNode = domConstruct.create('div', {
                    className: 'dgrid-filter'
                }, this.footerNode);
                this.filterStatusNode = domConstruct.create('div', {
                    className: 'dgrid-filter-status'
                }, filterNode);
                const inputNode = this.filterInputNode = domConstruct.create('input', {
                    className: 'dgrid-filter-input',
                    placeholder: 'Filter (regex)...'
                }, filterNode);
                this._filterTextBoxHandle = on(inputNode, 'keydown', debounce(function () {
                    grid.set("collection", grid.collection);
                }, 250));
            },
            destroy: function () {
                this.inherited(arguments);
                if (this._filterTextBoxHandle) {
                    this._filterTextBoxHandle.remove();
                }
            },
            _setCollection: function (collection) {
                this.inherited(arguments);
                const value = this.filterInputNode.value;
                const renderedCollection = this._renderedCollection;
                if (renderedCollection && value) {
                    const rootFilter = new renderedCollection.Filter();
                    const re = new RegExp(value, "i");
                    const columns = this.columns;
                    const matchFilters = [];
                    for (const p in columns) {
                        if (columns.hasOwnProperty(p)) {
                            matchFilters.push(rootFilter.match(columns[p].field, re));
                        }
                    }
                    const combined = rootFilter.or.apply(rootFilter, matchFilters);
                    const filtered = renderedCollection.filter(combined);
                    this._renderedCollection = filtered;
                    this.refresh();
                }
            },
            refresh: function() {
                this.inherited(arguments);
                const value = this.filterInputNode.value;
                if (value) {
                    this.filterStatusNode.innerHTML = this.get('total') + " filtered results";
                }else {
                    this.filterStatusNode.innerHTML = "";
                }
            }


        });





        /**
         * Block grid base class.
         * @class module:xblox/views/Grid
         */
        const GridClass = Grid.createGridClass('log',
            {
                options: utils.clone(types.DEFAULT_GRID_OPTIONS)
            },
            //features
            {

                SELECTION: true,
                KEYBOARD_SELECTION: true,
                PAGINATION: types.GRID_FEATURES.PAGINATION,
                ACTIONS: types.GRID_FEATURES.ACTIONS,
                //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                FILTER:{
                    CLASS:KeyboardNavigation
                },
                SEARCH:{
                    CLASS:Search
                }

            },
            {
                //base flip
                RENDERER: ListRenderer

            },
            {
                //args
                /*renderers: renderers,
                 selectedRenderer: TreeRenderer*/
            },
            {
                GRID: OnDemandGrid,
                EDITOR: null,
                LAYOUT: Layout,
                DEFAULTS: Defaults,
                RENDERER: ListRenderer,
                EVENTED: EventedMixin,
                FOCUS: Focus

            }
        );



        return GridClass;

    }

    function createGridClass(overrides) {

        const gridClass = declare('driverGrid', BlockGrid,{
            highlightDelay:500,
            propertyStruct : propertyStruct,
            /**
             * Step/Run action
             * @param block {block[]}
             * @returns {boolean}
             */
            execute: function (blocks) {

                
                console.clear();

                const thiz = this;



                function _clear(element){
                    if(element) {
                        setTimeout(function () {
                            element.removeClass('failedBlock successBlock activeBlock');
                        }, thiz.highlightDelay);
                    }
                }

                function _node(block){
                    if(block) {
                        const row = thiz.row(block);
                        if (row) {
                            const element = row.element;
                            if (element) {
                                return $(element);
                            }
                        }
                    }
                    return null;
                };

                function mark(element,cssClass){

                    if(element) {
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.addClass(cssClass);
                    }
                }

                const dfds = [], EVENTS = types.EVENTS;

                const _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                    mark(_node(evt),'activeBlock');

                });
                const _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                    //console.log('mark success');
                    mark(_node(evt),'successBlock');
                    _clear(_node(evt));
                });

                const _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                    mark(_node(evt),'failedBlock');
                    _clear(_node(evt));

                });




                function run(block) {

                    if (!block || !block.scope) {
                        console.error('have no scope');
                        return;
                    }
                    try {

                        const blockDfd = block.scope.solveBlock(block, {
                            highlight: true,
                            force: true,
                            listener:thiz
                        });

                        dfds.push(blockDfd);

                        /*
                        blockDfd.then(function(result){
                            console.log('did run! : ' + result);
                        });
                        */
                        //console.log('run block result:',result);


                    } catch (e)
                    {
                        console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                        console.error(printStackTrace().join('\n\n'));
                    }
                    return true;
                }

                blocks  = _.isArray(blocks) ? blocks : [blocks];


                function _patch(block){


                    block.runFrom=function(_blocks, index, settings)
                    {

                        const thiz = this, blocks = _blocks || this.items, allDfds = [];

                        const onFinishBlock = function (block, results) {
                            block._lastResult = block._lastResult || results;
                            thiz._currentIndex++;
                            thiz.runFrom(blocks, thiz._currentIndex, settings);
                        };

                        const wireBlock = function (block) {
                            block._deferredObject.then(function (results) {
                                console.log('----def block finish');
                                onFinishBlock(block, results);
                            });
                        };

                        if (blocks.length) {

                            for (let n = index; n < blocks.length; n++) {



                                const block = blocks[n];

                                console.log('run child \n'+block.method);

                                _patch(block);

                                if (block.deferred === true) {
                                    block._deferredObject = new Deferred();
                                    this._currentIndex = n;
                                    wireBlock(block);
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));
                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                    break;
                                } else {
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));

                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                }

                            }

                        } else {
                            this.onSuccess(this, settings);
                        }

                        return allDfds;
                    };

                    block.solve=function(scope,settings,run,error){

                        this._currentIndex = 0;
                        this._return=[];

                        const _script = '' + this._get('method');
                        const thiz=this,
                              ctx = this.getContext(),
                              items = this[this._getContainer()],
                              //outer,head dfd
                              dfd = new Deferred,
                              listener = settings.listener,
                              isDfd = thiz.deferred;



                        if(listener) {
                            listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                        }

                        function _finish(result){

                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                            }

                            dfd.resolve(result);


                        }

                        function _error(result){
                            dfd.reject(result);
                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                            }
                        }


                        function _headDone(result){
                            //console.log('_headDone : ',result);
                            //more blocks?
                            if(items.length) {
                                const subDfds = thiz.runFrom(items,0,settings);
                                all(subDfds).then(function(what){
                                    console.log('all solved!',what);
                                    _finish(result);
                                },function(err){

                                    console.error('error in chain',err);
                                    if(listener) {
                                        listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                    }
                                    dfd.resolve(err);
                                });

                            }else{
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }
                                dfd.resolve(result);
                            }
                        }




                        if(_script && _script.length){
                            const runScript = function() {


                                const _function = new Function("{" + _script + "}");
                                const _args = thiz.getArgs() || [];
                                try {

                                    if(isDfd){

                                        ctx.resolve=function(result){
                                            console.log('def block done');
                                            if(thiz._deferredObject) {
                                                thiz._deferredObject.resolve();
                                            }
                                            _headDone(result);
                                        }
                                    }
                                    const _parsed = _function.apply(ctx, _args || {});
                                    thiz._lastResult = _parsed;
                                    if (run) {
                                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                                    }

                                    if(!isDfd) {
                                        _headDone(_parsed);
                                    }

                                    if (_parsed !== 'false' && _parsed !== false) {

                                    } else {
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                } catch (e) {

                                    e=e ||{};

                                    _error(e);

                                    if (error) {
                                        error('invalid expression : \n' + _script + ': ' + e);
                                    }
                                    //thiz.onFailed(thiz, settings);
                                    //return [];
                                }
                            };

                            if(scope.global){
                                (function() {

                                    window = scope.global;


                                    const _args = thiz.getArgs() || [];
                                    try {
                                        let _parsed = null;
                                        if(!ctx.runExpression) {
                                            const _function = new Function("{" + _script + "}").bind(this);
                                            _parsed = _function.apply(ctx, _args || {});
                                        }else{
                                            _parsed = ctx.runExpression(_script,null,_args);
                                        }

                                        thiz._lastResult = _parsed;

                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }
                                        if (_parsed !== 'false' && _parsed !== false) {
                                            thiz.onSuccess(thiz, settings);
                                        } else {
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }


                                    } catch (e) {

                                        thiz._lastResult = null;
                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        thiz.onFailed(thiz, settings);
                                        return [];
                                    }

                                }).call(scope.global);

                            }else{
                                runScript();
                            }

                        }else{
                            console.error('have no script');
                        }
                        /*
                        var ret=[], items = this[this._getContainer()];

                        if(items.length) {
                            this.runFrom(items,0,settings);
                        }else{
                            this.onSuccess(this, settings);
                        }

                        this.onDidRun();
                        */

                        return dfd;
                    }

                }

                _.each(blocks,_patch);

                _.each(blocks,run);

                all(dfds).then(function(){
                    console.log('did run all selected blocks!',thiz);
                    _runHandle.remove();
                    _successHandle.remove();
                    _errHandle.remove();
                });



            },
            onCIChanged: function (ci, block, oldValue, newValue, field) {

                console.log('on ci changed', arguments);
                block.set(field, newValue);
            },
            _itemChanged: function (type, item, store) {

                store = store || this.getStore(item);

                const thiz = this;

                function _refreshParent(item, silent) {

                    const parent = item.getParent();
                    if (parent) {
                        const args = {
                            target: parent
                        };
                        if (silent) {
                            this._muteSelectionEvents = true;
                        }
                        store.emit('update', args);
                        if (silent) {
                            this._muteSelectionEvents = false;
                        }
                    } else {
                        thiz.refresh();
                    }
                }

                function select(item) {

                    thiz.select(item, null, true, {
                        focus: true,
                        delay: 20,
                        append: false
                    });
                }

                switch (type) {

                    case 'added':
                    {
                        //_refreshParent(item);
                        //this.deselectAll();
                        this.refresh();
                        select(item);
                        break;
                    }

                    case 'changed':
                    {
                        this.refresh();
                        select(item);
                        break;
                    }


                    case 'moved':
                    {
                        //_refreshParent(item,true);
                        //this.refresh();
                        //select(item);

                        break;
                    }

                    case 'deleted':
                    {

                        const parent = item.getParent();
                        //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                        const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                        if (parent) {
                            const _container = parent.getContainer();
                            if (_container) {
                                _.each(_container, function (child) {
                                    if (child.id == item.id) {
                                        _container.remove(child);
                                    }
                                });
                            }
                        }

                        this.refresh();
                        /*
                         if (_prev) {
                         select(_prev);
                         }
                         */
                        break;
                    }

                }



            },
            _onFocusChanged:function(focused,type){
                this.inherited(arguments);
                if(!focused){
                    this._lastSelection = [];
                }
            },
            _showProperties: function (item,force) {


                const block = item || this.getSelection()[0], thiz = this, rightSplitPosition= thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);




                if(!block || rightSplitPosition==1) {
                    //console.log(' show properties: abort',[block , rightSplitPosition]);
                    return;
                }
                const right = this.getRightPanel();
                let props = this.getPropertyStruct();

                if (item == props._lastItem && force!==true) {
                    console.log('show propertiess : same item');
                    //return;
                }

                this.clearPropertyPanel();
                props = this.getPropertyStruct();


                props._lastItem = item;

                const _title = item.name || item.title;



                let tabContainer = props.targetTop;

                if (!tabContainer) {

                    tabContainer = utils.addWidget(_Accordion, {
                        delegate: this,
                        tabStrip: true,
                        tabPosition: "top",
                        attachParent: true,
                        style: "width:100%;height:100%;overflow-x:hidden;",
                        allowSplit: false
                    }, null, right.containerNode, true);

                    props.targetTop = tabContainer;
                }

                if (tabContainer.selectedChildWidget) {
                    props.lastSelectedTopTabTitle = tabContainer.selectedChildWidget.title;
                } else {
                    props.lastSelectedTopTabTitle = 'General';
                }


                _.each(tabContainer.getChildren(), function (tab) {
                    tabContainer.removeChild(tab);
                });

                if (props.currentCIView) {
                    props.currentCIView.empty();
                }

                if (!block.getFields) {
                    console.log('have no fields', block);
                    return;
                }

                const cis = block.getFields();
                for (var i = 0; i < cis.length; i++) {
                    cis[i].vertical = true;
                }

                const ciView = new CIViewMixin({

                    initWithCIS: function (data) {
                        this.empty();

                        data = utils.flattenCIS(data);

                        this.data = data;

                        const thiz = this;

                        let groups = _.groupBy(data,function(obj){
                            return obj.group;
                        });

                        const groupOrder = this.options.groupOrder || {};

                        groups = this.toArray(groups);

                        const grouped = _.sortByOrder(groups, function(obj){
                            return groupOrder[obj.name] || 100;
                        });

                        if (grouped != null && grouped.length > 1) {
                            this.renderGroups(grouped);
                        } else {
                            this.widgets = factory.createWidgetsFromArray(data, thiz, null, false);
                            if (this.widgets) {
                                this.attachWidgets(this.widgets);
                            }
                        }
                    },
                    tabContainer: props.targetTop,
                    delegate: this,
                    viewStyle: 'padding:0px;',
                    autoSelectLast: true,
                    item: block,
                    source: this.callee,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Advanced': 2,
                            'Script':3,
                            'Arguments':4,
                            'Description':5,
                            'Share':6

                        }
                    },
                    cis: cis
                });

                ciView.initWithCIS(cis);


                props.currentCIView = ciView;

                if (block.onFieldsRendered) {
                    block.onFieldsRendered(block, cis);
                }


                ciView._on('valueChanged', function (evt) {
                    //console.log('ci value changed ', evt);
                    thiz.onCIChanged(evt.ci,block,evt.oldValue,evt.newValue,evt.ci.dst);
                });






                const containers = props.targetTop.getChildren();
                let descriptionView = null;
                for (var i = 0; i < containers.length; i++) {

                    // @TODO : why is that not set?
                    containers[i].parentContainer = props.targetTop;

                    // track description container for re-rooting below
                    if (containers[i].title === 'Description') {
                        descriptionView = containers[i];
                    }

                    if (props.targetTop.selectedChildWidget.title !== props.lastSelectedTopTabTitle) {
                        if (containers[i].title === props.lastSelectedTopTabTitle) {
                            props.targetTop.selectChild(containers[i]);
                        }
                    }
                }

                props.targetTop.resize();

                this.setPropertyStruct(props);


                this._docker.resize();
            },
            save:function(){



                const thiz = this, driver = thiz.userData.driver, ctx = thiz.ctx, fileManager = ctx.getFileManager(), scope = thiz.blockScope, instance = scope.instance, originalScope = instance ? instance.blockScope : null, path = driver.path.replace('.meta.json','.xblox'), scopeToSave = originalScope || scope, mount = driver.scope;

                if(originalScope){
                    originalScope.fromScope(scope);
                }

                if (scope) {

                    const all = {
                        blocks: null,
                        variables: null
                    };

                    const blocks = scopeToSave.blocksToJson();
                    try {
                        //test integrity
                        dojo.fromJson(JSON.stringify(blocks));
                    } catch (e) {
                        console.error('invalid data');
                        return;
                    }

                    const _onSaved = function () {};

                    all.blocks = blocks;

                    fileManager.setContent(mount,path,JSON.stringify(all, null, 2),_onSaved);

                    //this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
                }



            },
            __runAction: function (action) {

                const thiz = this;
                const sel = this.getSelection();


                console.log('run aciton innner ' + action.command);

                function addItem(_class,group){

                    const cmd = factory.createBlock(_class, {
                        name: "No Title",
                        send: "nada",
                        scope: thiz.blockScope,
                        group: group
                    });

                    thiz.deselectAll();
                    _.each(thiz.grids,function(grid){
                        if(grid) {
                            grid.refresh();
                        }
                    });
                    setTimeout(function () {
                        thiz.select([cmd],null,true,{
                            focus:true
                        });
                    }, 200);
                }


                if (action.command == 'New/Command') {
                    addItem(Command,'basic');
                }
                if (action.command == 'New/Variable') {
                    addItem(Variable,'basicVariables');
                }

                if (action.command == 'File/Save') {
                    this.save();
                }

                return this.inherited(arguments);
            },
            __startup:function(){


                domClass.add(this.domNode,'blockGrid');


                this.inherited(arguments);
                const thiz = this;

                function _node(evt){
                    const item = evt.callee;
                    if(item) {
                        const row = thiz.row(item);
                        if (row) {
                            const element = row.element;
                            if (element) {
                                return $(element);
                            }
                        }
                    }
                    return null;
                };

                function mark(element,cssClass){

                    if(element) {
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.addClass(cssClass);
                        setTimeout(function () {
                            element.removeClass(cssClass);
                            thiz._isHighLighting = false;
                        }, thiz.highlightDelay);
                    }
                }

            }
        });

        return gridClass;
    }
    function createLogEntry(){

        const logManager = ctx.getLogManager();
        const store = logManager.store;

        /*
         var message={
         message:'test',
         level:'info',
         type:'device',
         details:{},
         terminatorMessage:null
         };
         factory.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, message);*/

        //logManager.addLoggingMessage()
    }
    function addLog(tab,driver,device){

        const logManager = ctx.getLogManager();
        const store = logManager.store;

        var logGridClass = createLogGridClass();


        if(device){
            console.log('device : ', device);
        }




        // default grid args
        const gridArgs = {
            ctx:ctx,
            attachDirect:true,
            collection: store.filter({
                show:true,
                host:device.info.host + ':' + device.info.port
            })
        };


        const items = store.query({
            show:true,
            host:"192.168.1.20:23"
        });






        var logGridClass = declare("xlog.views.LogView", logGridClass, {
            _columns: {
                "Level": true,
                "Type": false,
                "Message": true,
                "Time": false
            },
            permissions: [
                //ACTION.EDIT,
                ACTION.RELOAD,
                ACTION.DELETE,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.PREVIEW,
                ACTION.SAVE,
                ACTION.SEARCH
            ],
            postMixInProperties: function () {
                this.columns = this.getColumns();
                return this.inherited(arguments);
            },
            formatDateSimple: function (data, format) {

                const momentUnix = moment.unix(data);

                return momentUnix.format("MMMM Do, h:mm:ss a");
            },
            getDefaultSort:function(){
                return [{property: 'time', descending: true}];
            },
            getMessageFormatter: function (message, item) {
                const thiz = this;
                if(item.progressHandler && !item._subscribedToProgress){
                    item.progressHandler._on('progress',function(_message){
                        thiz.refresh();
                    });
                    item._subscribedToProgress = true;
                }

                let _isTerminated = item.terminatorMessage !==null && item._isTerminated===true;
                if(!item.terminatorMessage){
                    _isTerminated = true;
                }

                if (!_isTerminated) {
                    return '<span class=\"fa-spinner fa-spin\" style=\"margin-right: 4px\"></span>' + message;
                }
                return message;
            },
            getColumns: function () {
                const thiz = this;
                const columns = {
                    Level: {
                        field: "level", // get whole item for use by formatter
                        label: "Level",
                        sortable: true,
                        formatter: function (level) {

                            switch (level) {
                                case 'info':
                                {
                                    return '<span class="text-info" style=\"">' + level + '</span>';
                                }
                                case 'error':
                                {
                                    return '<span class="text-danger" style=\"">' + level + '</span>';
                                }
                                case 'warning':
                                {
                                    return '<span class="text-warning" style=\"">' + level + '</span>';
                                }
                            }
                            return level;
                        }

                    },
                    Type: {
                        field: "type", // get whole item for use by formatter
                        label: "Type",
                        sortable: true
                    },
                    Host: {
                        field: "host", // get whole item for use by formatter
                        label: "Host",
                        sortable: true
                    },
                    Message: {
                        field: "message", // get whole item for use by formatter
                        label: "Message",
                        sortable: true,
                        formatter: function (message, item) {
                            return thiz.getMessageFormatter(message, item)
                        }
                    },
                    Time: {
                        field: "time", // get whole item for use by formatter
                        label: "Time",
                        sortable: true,
                        formatter: function (time) {
                            return thiz.formatDateSimple(time / 1000);
                        }
                    },
                    Details:{
                        field: "details", // get whole item for use by formatter
                        label: "Details",
                        sortable: false,
                        hidden:true
                        /*editor:RowDetailEditor*/
                    }
                };








                if (!this.showSource) {
                    delete columns['Host'];
                }
                return columns;
            },
            startup:function(){

                this.inherited(arguments);

                const thiz = this, permissions = this.permissions;

                if (permissions) {

                    const _defaultActions = DefaultActions.getDefaultActions(permissions, this);
                    //_defaultActions = _defaultActions.concat(this.getBlockActions(permissions));

                    this.addActions(_defaultActions);
                    //this.onContainerClick();
                }

            }
        });
        //tab.select();
        const grid = utils.addWidget(logGridClass,gridArgs,null,tab,false,'logGridView');
        grid.startup();


    }
    function doTests(){}
    function openDriverSettings(driver,device){


        createCommandSettingsWidget();

        const parent = TestUtils.createTab(null,null,module.id);


        const title = 'Marantz Instance';




        let devinfo = null;
        if(device){
            devinfo  = ctx.getDeviceManager().toDeviceControlInfo(device);
        }

        //@Todo:driver, store device temporarly in Commands CI
        const commandsCI = utils.getCIByChainAndName(driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
        if(commandsCI){
            commandsCI.device = device;
        }



        const view = utils.addWidget(CIGroupedSettingsView, {
            style:"width: inherit;height: 100%;",
            title:  'title',
            cis: driver.user.inputs,
            storeItem: driver,
            storeDelegate: this,
            iconClass: 'fa-eye',
            closable: true,
            showAllTab: false,
            blockManager: ctx.getBlockManager(),
            options:{
                groupOrder: {
                    'General': 1,
                    'Settings': 2,
                    'Visual':3
                },
                select:'Settings'
            }


        }, null, parent, true);

        view.resize();
    }

    function fixScope(scope){
        return scope;

        /**
         *
         * @param source
         * @param target
         * @param before
         * @param add: comes from 'hover' state
         * @returns {boolean}
         */
        scope.moveTo = function(source,target,before,add){




            console.log('scope::move, add: ' +add,arguments);

            if(!add){
                debugger;
            }
            /**
             * treat first the special case of adding an item
             */
            if(add){

                //remove it from the source parent and re-parent the source
                if(target.canAdd && target.canAdd()){

                    var sourceParent = this.getBlockById(source.parentId);
                    if(sourceParent){
                        sourceParent.removeBlock(source,false);
                    }
                    target.add(source,null,null);
                    return;
                }else{
                    console.error('cant reparent');
                    return false;
                }
            }


            //for root level move
            if(!target.parentId && add==false){

                //console.error('root level move');

                //if source is part of something, we remove it
                var sourceParent = this.getBlockById(source.parentId);
                if(sourceParent && sourceParent.removeBlock){
                    sourceParent.removeBlock(source,false);
                    source.parentId=null;
                    source.group=target.group;
                }

                const itemsToBeMoved=[];
                const groupItems = this.getBlocks({
                    group:target.group
                });

                const rootLevelIndex=[];
                const store = this.getBlockStore();

                const sourceIndex = store.storage.index[source.id];
                const targetIndex = store.storage.index[target.id];
                for(var i = 0; i<groupItems.length;i++){

                    const item = groupItems[i];
                    //keep all root-level items

                    if( groupItems[i].parentId==null && //must be root
                        groupItems[i]!=source// cant be source
                    ){

                        const itemIndex = store.storage.index[item.id];
                        var add = before ? itemIndex >= targetIndex : itemIndex <= targetIndex;
                        if(add){
                            itemsToBeMoved.push(groupItems[i]);
                            rootLevelIndex.push(store.storage.index[groupItems[i].id]);
                        }
                    }
                }

                //remove them the store
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.remove(itemsToBeMoved[j].id);
                }

                //remove source
                this.getBlockStore().remove(source.id);

                //if before, put source first
                if(before){
                    this.getBlockStore().putSync(source);
                }

                //now place all back
                for(var j = 0; j<itemsToBeMoved.length;j++){
                    store.put(itemsToBeMoved[j]);
                }

                //if after, place source back
                if(!before){
                    this.getBlockStore().putSync(source);
                }

                return true;

                //we move from root to lower item
            }else if( !source.parentId && target.parentId && add==false){
                source.group = target.group;
                if(target){

                }

                //we move from root to into root item
            }else if( !source.parentId && !target.parentId && add){

                console.error('we are adding an item into root root item');
                if(target.canAdd && target.canAdd()){
                    source.group=null;
                    target.add(source,null,null);
                }
                return true;

                // we move within the same parent
            }else if( source.parentId && target.parentId && add==false && source.parentId === target.parentId){
                console.error('we move within the same parents');
                const parent = this.getBlockById(source.parentId);
                if(!parent){
                    console.error('     couldnt find parent ');
                    return false;
                }

                const maxSteps = 20;
                var items = parent[parent._getContainer(source)];

                var cIndexSource = source.indexOf(items,source);
                var cIndexTarget = source.indexOf(items,target);
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    parent.move(source,direction);
                }
                return true;

                // we move within the different parents
            }else if( source.parentId && target.parentId && add==false && source.parentId !== target.parentId){                console.log('same parent!');

                console.error('we move within the different parents');
                //collect data

                var sourceParent = this.getBlockById(source.parentId);
                if(!sourceParent){
                    console.error('     couldnt find source parent ');
                    return false;
                }

                const targetParent = this.getBlockById(target.parentId);
                if(!targetParent){
                    console.error('     couldnt find target parent ');
                    return false;
                }


                //remove it from the source parent and re-parent the source
                if(sourceParent && sourceParent.removeBlock && targetParent.canAdd && targetParent.canAdd()){
                    sourceParent.removeBlock(source,false);
                    targetParent.add(source,null,null);
                }else{
                    console.error('cant reparent');
                    return false;
                }

                //now proceed as in the case above : same parents
                var items = targetParent[targetParent._getContainer(source)];
                if(items==null){
                    console.error('weird : target parent has no item container');
                }
                var cIndexSource = targetParent.indexOf(items,source);
                var cIndexTarget = targetParent.indexOf(items,target);
                if(!cIndexSource || !cIndexTarget){
                    console.error(' weird : invalid drop processing state, have no valid item indicies');
                    return;
                }
                var direction = cIndexSource > cIndexTarget ? -1 : 1;
                var distance = Math.abs(cIndexSource - ( cIndexTarget + (before ==true ? -1 : 1)));
                for(var i = 0 ; i < distance -1;  i++){
                    targetParent.move(source,direction);
                }
                return true;
            }

            return false;
        };

        return scope;

        const topLevelBlocks = [];
        var blocks = scope.getBlocks({
            parentId:null
        });


        const grouped = _.groupBy(blocks,function(block){
            return block.group;
        });

        function createDummyBlock(id,scope){

            const block = {
                    "_containsChildrenIds": [
                        "items"
                    ],
                    "group": null,
                    "id": id,
                    "items": [

                    ],
                    "name": id,
                    "method": "----group block ----",
                    "args": "",
                    "deferred": false,
                    "declaredClass": "xblox.model.code.RunScript",
                    "enabled": true,
                    "serializeMe": false,
                    "shareTitle": "",
                    "canDelete": true,
                    "renderBlockIcon": true,
                    "order": 0,
                    "additionalProperties": true,
                    "_scenario": "update"

                };

            return scope.blockFromJson(block);

        }

        for(const group in grouped){

            const groupBlock = createDummyBlock(group,scope);
            var blocks = grouped[group];
            _.each(blocks,function(block){
                groupBlock['items'].push(block);



                if(!block.parentId && block.group /*&& block.id !== group*/) {
                    block.parent = groupBlock;
                    block.parentId = groupBlock.id;
                }
            });
        }


        const root = scope.getBlockById('root');
        //console.dir(root.getParent());

        return scope;
    }
    if (ctx) {

        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();

        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        var marantz = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.20.meta.json");

        const driver = marantz.driver;
        const device = marantz.device;

        const deviceManager = ctx.getDeviceManager();

        const parent = TestUtils.createTab(null,null,module.id);

        //openConsole.apply(deviceManager,[parent,driver,device]);


        openDriverView(driver,device);



        return Grid;
    }


    return Grid;
});