/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/Grid',
    "xide/tests/TestUtils",
    "module",

    "dojo/promise/all"

], function (declare,types,
             utils, factory,Grid, TestUtils,module,
             all
) {
    console.clear();

    console.log('--do-tests');

    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;
    const driverId = '235eb680-cb87-11e3-9c1a-0800200c9a66';
    //marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");;







    function doCopyTest(driver,parent){


        console.dir(driver);


        const fileManager = ctx.getFileManager();

        const driverManager = ctx.getDriverManager();


        if(!parent){
            parent = driver.getParent();
        }

        if(!parent){
            console.error('copy error: have no destination');
        }

        const thiz = this;

        const title = driver.getMetaValue(types.DRIVER_PROPERTY.CF_DRIVER_NAME);

        const id = driver.getMetaValue(types.DRIVER_PROPERTY.CF_DRIVER_ID);
        const jsPath = driver.getMetaValue(types.DRIVER_PROPERTY.CF_DRIVER_CLASS);
        const path = driver.path;
        const pathParts = utils.parse_url('file:///'+path,'path');
        const pathInfo = utils.pathinfo(path,types.PATH_PARTS.ALL);
        const ext = pathInfo.extension;
        const fileName  = pathInfo.basename;
        const store = driver._store;



        const driverScope = driver.scope;

        const targetScope = parent.scope;
        const targetStore = parent._store;

        const blockScope = driver.blockScope;

        let driverMeta = null;
        let driverCode = null;

        //getContent: function (mount, path, readyCB, emit) {

        const dfds = [];


        //////////////////////////////
        //
        //  new vars
        //

        const parentParts = utils.pathinfo(parent.path);

        const titleNew = title + '-Copy';
        const idNew = utils.createUUID();
        const jsFileParts = utils.pathinfo(jsPath);
        const jsPathNew = parentParts.dirname + '/' + parentParts.basename + '/' + titleNew + '.js';
        const pathNew = titleNew + '.meta.json';




        console.log('js parts: ',jsPathNew);






        //pick meta content
        dfds.push(fileManager.getContent(driverScope,path,function(meta){
            driverMeta = utils.getJson(meta);
        }));

        dfds.push(fileManager.getContent(driverScope,jsPath,function(code){
            driverCode = code;
        }));





        function doCopy(){

            //update title CI
            utils.setCIValue(driverMeta.inputs,types.DRIVER_PROPERTY.CF_DRIVER_NAME,titleNew);
            utils.setCIValue(driverMeta.inputs,types.DRIVER_PROPERTY.CF_DRIVER_ID,idNew);
            utils.setCIValue(driverMeta.inputs,types.DRIVER_PROPERTY.CF_DRIVER_CLASS,jsPathNew);

            const met = driverMeta;


            const newItemDfd = driverManager.createItem(targetScope,parentParts.basename,titleNew,JSON.stringify(driverMeta,null,2),driverCode);

            newItemDfd.then(function(data){

                console.error('create driver item : ',data);



                let newItem = driverManager.createNewItem(titleNew, targetScope, parent.path);
                newItem.path += '.meta.json';
                newItem.user = driverMeta;
                newItem.id = idNew;

                newItem.blox = {
                    blocks:blockScope.blocksToJson(),
                    variables:[]
                }



                newItem = store.putSync(newItem);
                driverManager.onDriverCreated(store);

                driverManager.completeDriver(store, newItem, newItem);

                factory.publish(types.EVENTS.ON_STORE_CHANGED, {
                    owner: thiz,
                    store: store,
                    action: types.NEW_FILE,
                    item: newItem
                });

                targetStore.refreshItem(parent);
            });


        }


        all(dfds).then(function(){
            doCopy();
        });

        console.log('copy driver =' + title + ' id =' + id + ' path = ' + path + '|| ' + jsPath,pathInfo);
        ///////////////////////////////////////////////////////////
        //
        //  Copy options
        //
        const cloneJSDriver = true;
        const cloneBloxDriver = true;
    }

    function doTests(tab){

        console.clear();
        const driverManager = ctx.getDriverManager();

        const marantz  = driverManager.getDriverById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        const store = driverManager.getStore('user_drivers');// marantz._store;
        //var parent = store.getSync('Arduino');

        const scope = 'user_drivers';


        function doTest(store){
            const parent = store.getSync('Test');
            //console.log('ar',parent);
            doCopyTest(marantz,parent);
        }

        if(store.then){
            store.then(function(store){

                store = driverManager.getStore(scope);
                doTest(store);
            });
        }else{
            doTest(store);
        }



        return;

        doCopyTest(marantz,parent);




    }


    var ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;


    const _actions = [
        ACTION.RENAME
    ];

    if (ctx) {



        var parent = TestUtils.createTab(null,null,module.id);

        doTests(parent);

        return declare('a',null,{});

    }

    return Grid;
});