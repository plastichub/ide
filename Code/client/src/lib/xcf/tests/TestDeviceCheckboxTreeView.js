define([
    'xdojo/declare',
    'xide/utils',
    'xide/types',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xaction/DefaultActions',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'xgrid/Search',
    'xide/tests/TestUtils',
    'module',
    "xcf/views/DeviceTreeView",
    "xide/form/Checkbox",
    "dgrid/Editor"

], function (declare, utils, types, Grid, TreeRenderer, DefaultActions, KeyboardNavigation, _Widget, Search, TestUtils, module,
    DeviceTreeView, Checkbox, Editor) {
    const ACTION = types.ACTION;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const ctx = window.sctx;
    const test = true;
    let marantz;
    let marantzInstance;






    const gridClass = Grid.createGridClass('driverTreeView', {
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress: false,
            isGroup: function (item) {

                if (item) {
                    return item.isDir === true;
                } else {
                    return false;
                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                }
            },
            postMixInProperties: function () {
                const thiz = this;

                function getIconClass(item) {

                    if (item.iconClass) {
                        return item.iconClass;
                    }
                    const opened = true;
                    const iclass = (!item || item.isDir) ? (opened ? "dijitFolderOpened" : "dijitFolderClosed") : "dijitLeaf";

                    //commands or variables
                    if (item.ref && item.ref.item && item.ref.item.getIconClass) {
                        const _clz = item.ref.item.getIconClass();
                        if (_clz) {
                            return _clz;
                        }
                    } else if (!thiz.isGroup(item)) {
                        return thiz.delegate.getDeviceStatusIcon(item);
                    }

                    if (utils.toString(item.name) === 'Commands') {
                        return 'el-icon-video';
                    }
                    return iclass;
                }

                this.collection = this.collection.filter(this.getRootFilter());


                this.columns = [{
                    renderExpando: true,
                    label: "Name",
                    field: "name",
                    sortable: false,
                    formatter: function (name, item) {

                        const meta = item['user'];

                        if (item.iconClass) {
                            return '<span class="grid-icon ' + item.iconClass + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                        }

                        if (thiz.isGroup(item)) {
                            return '<span class="grid-icon ' + 'fa-folder' + '"></span>' + item.name;
                        }

                        if (item.icon) {
                            return '<span class="grid-icon ' + item.icon + '"></span>' + item.name;
                        }

                        //commands or variables
                        if (item.ref && item.ref.item) {

                            //@TODO why is here a difference ?
                            if (item.ref.item.name) {
                                return item.ref.item.name;
                            }
                            if (item.ref.item.title) {
                                return item.ref.item.title;
                            }
                        }

                        //true device
                        if (!thiz.isGroup(item)) {

                            if (meta) {

                                return '<span class="grid-icon ' + getIconClass(item) + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            } else {
                                return item.name;
                            }
                        } else {
                            return item.name;
                        }
                    }
                }];

                this.inherited(arguments);



                this._on('onAddAction', function (action) {
                    if (action.command == types.ACTION.EDIT) {
                        action.label = 'Settings';
                    }
                });
            },
            runAction: function (action, item) {
                console.log('run action in', action);
                this.inherited(arguments);
                //thiz.runAction(action,item);
            },
            /**
             * Override render row to add additional CSS class
             * for indicating a block's enabled state
             * @param object
             * @returns {*}
             */
            renderRow: function (object) {
                const res = this.inherited(arguments);
                const ref = object.ref;
                const item = ref ? ref.item : null;


                if (item != null && item.scope && item.enabled === false) {
                    $(res).addClass('disabledBlock');
                }
                return res;
            },
            refresh2: function (force) {
                if (this._refreshInProgress) {
                    return this._refreshInProgress;
                }

                const _restore = this._preserveSelection();
                const thiz = this;
                const active = this.isActive();
                const res = this.inherited(arguments);

                this._refreshInProgress = res;

                res && res.then && res.then(function () {

                    thiz._refreshInProgress = null;
                    thiz._restoreSelection(_restore, 10, true);

                    /*
                    if(_restore.focused && (active || force )) {
                        //console.log('restore focused');
                        thiz.focus(thiz.row(_restore.focused));
                    }*/
                });

                return res;
            }
        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR: types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION: {
                CLASS: KeyboardNavigation
            },
            WIDGET: {
                CLASS: _Widget
            },
            SEARCH: {
                CLASS: Search
            },
            EDITOR: {
                CLASS: Editor
            }
        }, {
            RENDERER: TreeRenderer
        },
        null,
        null);




    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.HEADER,
        ACTION.SEARCH,
        'File/ConnectDevice',
        'File/DisconnectDevice',
        'File/New Group',
        'File/Edit Driver Instance',
        'Instance/Edit Variables',
        'Instance/Edit Commands',
        'Instance/Show Log'
    ];



    const module2 = DeviceTreeView = declare('xcf.views.DeviceTreeView', gridClass, {
        iconClass: 'fa-sliders',
        beanType: 'device',
        permissions: DefaultPermissions,
        deselectOnRefresh: false,
        showHeader: false,
        toolbarInitiallyHidden: true,
        //attachDirect:true,
        groupOrder: {
            'Clipboard': 110,
            'Device': 105,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'New': 5,
            'Navigation': 3,
            'Select': 0
        },
        getDeviceActions: function (container, permissions, grid) {
            const actions = [];
            const thiz = this;
            const delegate = thiz.delegate;
            const ACTION = types.ACTION;

            function _selection() {
                const selection = grid.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                if (!item) {
                    console.error('have no item');
                    return null;
                }
                return selection;
            }

            function _device() {
                const selection = _selection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                return item.device || item.ref && item.ref.device ? item.ref.device : item.isDevice ? item : null;
            }
            ///////////////////////////////////////////////////////////////
            //
            //      Disable Functions
            //
            ///////////////////////////////////////////////////////////////
            function shouldDisableEdit() {

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                return false;
            }

            function shouldDisableEditInstance() {

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                const device = _device();
                if (!device) {
                    return true;
                }
                const instance = delegate.getInstance(device);
                if (instance) {
                    return false;
                } else {
                    return true;
                }

                return false;
            }

            function shouldDisableRun() {

                const selection = _selection();
                if (!selection || !selection[0].ref || !selection[0].ref.item) {
                    return true;
                }
                return false;
            }

            function shouldDisableConnect() {

                const selection = _selection();
                if (!selection || !selection[0] || !selection[0].path || selection[0].path.indexOf('.json') === -1) {
                    return true;
                }
                return false;
            }

            ///////////////////////////////////////////////////////////////
            //
            //      Actions
            //
            ///////////////////////////////////////////////////////////////

            const defaultMixin = {
                addPermission: true
            };

            const DEVICE_COMMAND_GROUP = 'Device/Command';


            actions.push(this.createAction('Edit', ACTION.EDIT, types.ACTION_ICON.EDIT, ['f4', 'enter', 'dblclick'], 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableEdit, permissions, container, grid
            ));

            actions.push(this.createAction('Edit Driver', 'File/Edit Driver', types.ACTION_ICON.EDIT, ['f3', 'ctrl enter'], 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableEdit, permissions, container, grid
            ));


            actions.push(this.createAction('Edit Instance', 'File/Edit Driver Instance', types.ACTION_ICON.EDIT, ['f6'], 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableEditInstance, permissions, container, grid
            ));

            actions.push(this.createAction('Edit Driver Code', 'File/Edit Driver Code', 'fa-code', null, 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableEditInstance, permissions, container, grid
            ));

            actions.push(this.createAction('Open Console', 'Instance/Console', 'fa-terminal', ['f5'], 'Home', 'Instance', 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableEditInstance, permissions, container, grid
            ));

            actions.push(this.createAction('Open Expression Wizard', 'Instance/ConsoleWizard', 'fa-magic', ['f7'], 'Home', 'Instance', 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableEditInstance, permissions, container, grid
            ));

            actions.push(thiz.createAction({
                label: 'Run',
                command: ACTION.RUN,
                icon: types.ACTION_ICON.RUN,
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: shouldDisableRun,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));

            actions.push(thiz.createAction({
                label: 'Edit Variables',
                command: 'Instance/Edit Variables',
                icon: 'fa-list-alt',
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEditInstance,
                mixin: defaultMixin
            }));
            actions.push(thiz.createAction({
                label: 'Edit Commands',
                command: 'Instance/Edit Commands',
                icon: types.ACTION_ICON.EDIT,
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEditInstance,
                mixin: defaultMixin
            }));
            actions.push(thiz.createAction({
                label: 'Show Log',
                command: 'Instance/Show Log',
                icon: 'fa-calendar',
                tab: 'Home',
                group: 'Instance',
                shouldDisable: shouldDisableEdit,
                mixin: utils.mixin({
                    quick: true
                }, defaultMixin)
            }));
            /*
             actions.push(this.createAction('Run', ACTION.RUN,types.ACTION_ICON.RUN, ['r'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
             defaultMixin,null,shouldDisableRun,permissions,container,grid
             ));
             */

            actions.push(this.createAction('New Group', 'File/New Group', types.ACTION_ICON.NEW_DIRECTORY, ['f7'], 'Home', 'New', 'item|view', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, null, permissions, container, grid
            ));

            actions.push(this.createAction('New Device', 'File/New Device', types.ACTION_ICON.NEW_FILE, ['ctrl n'], 'Home', 'New', 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, null, permissions, container, grid
            ));



            ///////////////////////////////////////////////////////////////
            //  Device Control
            actions.push(this.createAction('Disconnect', 'File/DisconnectDevice', 'fa-unlink', null, 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableConnect, permissions, container, grid
            ));

            actions.push(this.createAction('%%Connect', 'File/ConnectDevice', 'fa-link', null, 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                utils.mixin({
                    quick: true
                }, defaultMixin), null, shouldDisableConnect, permissions, container, grid
            ));


            /*
             actions.push(this.createActionParameters('New Device Group', 'File/New Device Group', 'edit', types.ACTION_ICON.NEW_DIRECTORY, function () {
             delegate.newGroup(thiz.getSelectedItem());
             }, 'F7', 'f7', null, container, thiz));
             */


            //add source actions
            actions.push(this.createAction('Source', 'View/Source', 'fa-hdd-o', ['f4'], 'Home', 'Navigation', 'item', null,
                function () {}, {
                    addPermission: true,
                    tab: 'Home',
                    dummy: true
                }, null, null, permissions, container, thiz
            ));
            let i = 0;
            _.each([{
                    label: 'System',
                    scope: 'system_devices'
                },
                {
                    label: 'User',
                    scope: 'user_devices'
                }
            ], function (item) {
                const label = item.label;
                actions.push(thiz.createAction(label, 'View/Source/' + label, 'fa-hdd-o', ['alt f' + i], 'Home', 'Navigation', 'item', null,
                    function () {}, {
                        addPermission: true,
                        tab: 'Home',
                        item: item,
                        quick: true
                    }, null, null, permissions, container, thiz
                ));
                i++;
            });


            const root = 'File/Log';

            actions.push(this.createAction({
                label: 'Logging',
                command: 'File/Log',
                icon: 'fa-calendar',
                tab: 'File',
                group: 'Logging',
                toggleGroup: thiz.id + 'Logging_Flags',
                mixin: {
                    addPermission: true,
                    quick: true
                },
                onCreate: function (action) {

                }
            }));

            const node = this.domNode;

            function _createEntry(value, label) {
                const icon = 'fa-cogs';
                // Allow cols to opt out of the hider (e.g. for selector column).
                const _action = thiz.createAction(label, root + '/' + label, icon, null, 'File', 'Logging', 'item|view',
                    //oncreate
                    function (action) {

                        const widgetImplementation = {
                            postMixInProperties: function () {
                                this.inherited(arguments);
                                this.checked = this.item.get('value') == true;
                            },
                            startup: function () {
                                this.inherited(arguments);
                                this.on('change', function (val) {
                                    thiz.showColumn(id, val);
                                })
                            }
                        };
                        const widgetArgs = {
                            checked: !value,
                            iconClass: icon,
                            style: 'float:inherit;'
                        };


                        const _visibilityMixin = {
                            widgetArgs: widgetArgs,
                            actionType: 'multiToggle'
                        };

                        action.actionType = 'multiToggle';
                        action.setVisibility(types.ACTION_VISIBILITY_ALL, utils.cloneKeys(_visibilityMixin, false));
                        label = action.label.replace('Show ', '');

                    }, /*handler*/ null, {
                        value: value,
                        addPermission: true,
                        quick: true,
                        tab: "File",
                        group: "Logging"
                    },
                    null, null, permissions, node, thiz, thiz);

                return _action;
            }



            actions.push(_createEntry(1, "On Device Started"));



            return actions;
        },
        postMixInProperties: function () {
            const thiz = this;

            this.inherited(arguments);

            function getIconClass(item) {

                if (item.iconClass) {
                    return item.iconClass;
                }
                const opened = true;

                const iclass = (!item || item.isDir) ? (opened ? "dijitFolderOpened" : "dijitFolderClosed") : "dijitLeaf";

                //commands or variables
                if (item.ref && item.ref.item && item.ref.item.getIconClass) {
                    const _clz = item.ref.item.getIconClass();
                    if (_clz) {
                        return _clz;
                    }
                } else if (!thiz.isGroup(item)) {
                    return thiz.delegate.getDeviceStatusIcon(item);
                }

                if (utils.toString(item.name) === 'Commands') {
                    return 'el-icon-video';
                }
                return iclass;
            }



            this.collection = this.collection.filter(this.getRootFilter());
            this.columns = [{

                    label: "Visibile",
                    field: "visible",
                    sortable: false,
                    get: function (item) {
                        // ensure initial rendering matches up with widget behavior
                        return true;
                    },
                    editorArgs: {
                        title: "",
                        style: "width:30px",
                        width: "30px"
                    },
                    width: "40px",
                    editor: Checkbox
                },
                {
                    renderExpando: true,
                    label: "Name",
                    field: "name",
                    sortable: false,
                    formatter: function (name, item) {

                        const meta = item['user'];


                        if (item.isDevice) {
                            return '<span class="grid-icon ' + getIconClass(item) + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                        }

                        if (item.iconClass) {
                            return '<span class="grid-icon ' + item.iconClass + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                        }

                        if (thiz.isGroup(item)) {
                            return '<span class="grid-icon ' + 'fa-folder' + '"></span>' + item.name;
                        }

                        if (item.icon) {
                            return '<span class="grid-icon ' + item.icon + '"></span>' + item.name;
                        }

                        //commands or variables
                        if (item.ref && item.ref.item) {

                            //@TODO why is here a difference ?
                            if (item.ref.item.name) {
                                return item.ref.item.name;
                            }
                            if (item.ref.item.title) {
                                return item.ref.item.title;
                            }
                        }

                        //true device
                        if (!thiz.isGroup(item)) {

                            if (meta) {
                                return '<span class="grid-icon ' + getIconClass(item) + '"></span>' + utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            } else {
                                return item.name;
                            }
                        } else {
                            return item.name;
                        }
                    }
                }
            ];


            this._on('onAddAction', function (action) {


                if (action.command == types.ACTION.EDIT) {
                    action.label = 'Settings';
                }
            });
        },
        editItem: function (item) {
            const thiz = this;
            if (item.type === types.ITEM_TYPE.BLOCK) {
                return thiz.blockManager.editBlock(item.ref.item, null, false);
            }
            if (item.driver) {
                item = item.driver;
            }

            thiz.openItem(item);
        },
        startup: function () {

            const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);

            this.inherited(arguments);

            this.addActions(_defaultActions);

            this.addActions(this.getDeviceActions(this.domNode, this.permissions, this));

            const thiz = this;



            this.subscribe(types.EVENTS.ON_STORE_CHANGED, function (evt) {
                if (evt.store.id == thiz.collection.id) {
                    thiz.refresh();
                }
                if (evt.store == thiz.collection) {
                    thiz.refresh();
                }
            });

            thiz.collection.on('update', function (evt) {
                if (evt && evt.target) {
                    try {
                        const cell = thiz.cell(evt.target.path, '0');
                        if (cell) {
                            thiz.refreshCell(cell);
                        }
                    } catch (e) {
                        logError(e);
                    }
                } else {
                    thiz.refresh();
                }
                thiz.refreshActions();
            });

            thiz.collection.on('delete', function () {
                thiz.refresh();
            });







        },
        //////////////////////////////////////////////////////////////////////
        //
        //  Actions - Impl.
        //
        changeSource: function (source) {
            console.log('change source', arguments);


            const thiz = this;
            const delegate = thiz.delegate;
            const scope = source.scope;
            let store = delegate.getStore(scope);


            function wireStore(store) {

                if (thiz['_storeConnected' + scope]) {
                    return;
                }

                thiz['_storeConnected' + scope] = true;

                function updateGrid() {
                    setTimeout(function () {
                        thiz.refresh();
                    }, 100);
                }

                store.on('update', function () {
                    updateGrid();
                });

                store.on('added', function () {
                    //console.log(' added to store: ',arguments);
                    updateGrid();
                });

                store.on('remove', function () {
                    //console.log(' removed from store: ',arguments);
                    updateGrid();
                });

                store.on('delete', function () {
                    //console.log(' deleted from store: ',arguments);
                    updateGrid();
                });

            }

            function ready(store) {
                const collection = store.filter(thiz.getRootFilter());
                thiz.set('collection', collection);
                thiz.set('title', 'Drivers (' + source.label + ')');
                thiz.scope = scope;
                wireStore(store);
            }

            if (store.then) {

                store.then(function () {
                    store = delegate.getStore(scope);
                    ready(store);
                });
            } else {

                //is store
                ready(store);
            }
        },
        ///////////////////////////////////////////////////////////////
        runAction: function (action, _item) {
            if (!action || !action.command) {
                return;
            }

            const ACTION = types.ACTION;
            const thiz = this;
            const delegate = thiz.delegate;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            const driverManager = ctx.getDriverManager();
            let item = thiz.getSelectedItem() || _item;
            let device;

            if (item) {
                device = item && item.device ? item.device : item.ref && item.ref.device ? item.ref.device : item.isDevice ? item : null;
            }



            if (action.command.indexOf('View/Source') != -1) {
                return this.changeSource(action.item);
            }


            const driverInstance = device ? deviceManager.getInstance(device) : null;
            const driverId = device ? device.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_DRIVER) : null;
            const driver = driverInstance ? driverInstance.driver : driverManager.getItemById(driverId);
            if (!driverInstance)

                console.log('--run action ' + action.command, device);

            const defaultSelect = {
                focus: true,
                append: false,
                delay: 1,
                expand: true
            };

            switch (action.command) {

                case 'File/Edit Driver Instance':
                    {

                        if (driverInstance) {
                            return driverManager.openItemSettings(driver, item);
                        } else {

                            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                                text: 'Can`t edit driver instance because device is not enabled or connected!',
                                type: 'error'
                            });
                        }

                        break;

                    }

                case 'Instance/Edit Variables':
                    {

                        if (driverInstance) {
                            return driverManager.openItemSettings(driver, item, {
                                showBasicCommands: false,
                                showConditionalCommands: false,
                                showResponseBlocks: false,
                                showLog: false,
                                showConsole: false,
                                showVariables: true,
                                showSettings: false,
                                showInitTab: false
                            }, true);
                        } else {

                            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                                text: 'Can`t edit driver instance because device is not enabled or connected!',
                                type: 'error'
                            });
                        }

                        break;

                    }
                case 'Instance/Show Log':
                    {

                        /*if(driverInstance){*/
                        //var driver = driverInstance.driver;
                        return driverManager.openItemSettings(driver, item, {
                            showBasicCommands: false,
                            showConditionalCommands: false,
                            showResponseBlocks: false,
                            showLog: true,
                            showConsole: false,
                            showVariables: false,
                            showSettings: false,
                            showInitTab: false,
                            isSingle: true
                        }, true);
                        //}
                        /*else{

                         this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                         text:'Can`t edit driver instance because device is not enabled or connected!',
                         type:'error'
                         });
                         }*/

                        break;

                    }
                case 'Instance/Edit Commands':
                    {

                        if (driverInstance) {
                            return driverManager.openItemSettings(driver, item, {
                                showBasicCommands: true,
                                showConditionalCommands: true,
                                showResponseBlocks: false,
                                showLog: false,
                                showConsole: false,
                                showVariables: false,
                                showSettings: false,
                                showInitTab: false
                            }, true);
                        } else {

                            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                                text: 'Can`t edit driver instance because device is not enabled or connected!',
                                type: 'error'
                            });
                        }

                        break;

                    }
                case 'File/Edit Driver':
                    {

                        console.log('---edit driver instance ');
                        if (driver) {
                            return driverManager.openItemSettings(driver, null);
                        }

                        /*
                         var driverInstance  =  item.driverInstance;
                         if(driverInstance){
                         var driver = driverInstance.driver;
                         return driverManager.openItemSettings(driver,item);
                         }
                         */

                        break;

                    }

                case ACTION.RUN:
                    {
                        if (device && driverInstance) {
                            const scope = driverInstance.blockScope;
                            const block = scope.getBlockById(item.id);
                            if (block) {
                                block.solve(block.scope);
                            }
                        }
                        break;
                    }
                case 'File/Edit Driver Code':
                    {
                        if (device && driverInstance) {
                            item = driverInstance.driver;
                        }
                        return driverManager.editDriver(null, item);
                    }
                case 'Instance/Console':
                    {
                        if (device && driverInstance) {
                            deviceManager.startDevice(device, true).then(function () {
                                deviceManager.openConsole(device);
                            });
                            return;
                        }
                        return deviceManager.openConsole(device);
                    }
                case 'Instance/ConsoleWizard':
                    {
                        if (device && driverInstance) {
                            deviceManager.startDevice(device, true).then(function () {
                                deviceManager.openExpressionConsole(device);
                            });
                            return;
                        }
                        return deviceManager.openExpressionConsole(device);
                    }
                case 'File/Log':
                    {
                        return deviceManager.openLog(device);
                    }

                case ACTION.EDIT:
                    {
                        return thiz.editItem(item);
                    }
                case ACTION.RELOAD:
                    {
                        return thiz.refresh();
                    }
                case ACTION.DELETE:
                    {
                        const _res = delegate.onDeleteItem(item);

                        return _res;
                    }
                case 'File/DisconnectDevice':
                    {

                        const dfd = delegate.stopDevice(item);
                        thiz.refresh();
                        return false;
                    }
                case 'File/ConnectDevice':
                    {
                        delegate.startDevice(item, true);
                        thiz.refresh();
                        return false;
                    }
                case 'File/New Device':
                    {
                        var res = delegate.newItem(item, thiz.scope);
                        res.then(function (item) {
                            thiz.refresh();
                            thiz.select(item, null, true, defaultSelect);
                        });
                        return res;
                    }
                case 'File/New Group':
                    {
                        var res = delegate.newGroup(item, thiz.scope);
                        res.then(function (item) {
                            thiz.refresh();
                            thiz.select(item, null, true, defaultSelect);
                        });
                        return res;
                    }
            }

            return this.inherited(arguments);
        },
        changeScope: function (name) {

            this.delegate.ls(name + '_devices').then(function (data) {
                console.log('got scope ', data);
            });

        },
        openItem: function (item, device) {
            if (item && !item.isDir && !item.virtual) {
                this.delegate.openItemSettings(item, device);
            }
        }
    });
    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    function createDeviceView(parent, ctx, driver, device, type) {

        const grid = utils.addWidget(module2, {
            title: 'Devices',
            collection: ctx.getDeviceManager().getStore('user_devices'),
            delegate: ctx.getDeviceManager(),
            blockManager: ctx.getBlockManager(),
            ctx: ctx,
            icon: 'fa-sliders',
            showHeader: false,
            scope: 'system_devices',
            resizeToParent: true
        }, null, parent, true);

        return grid;
    }




    if (test && ctx) {

        //console.clear();

        var parent = TestUtils.createTab(null, null, module.id);
        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();
        const grid = null;

        marantz = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        marantzInstance = driverManager.getStore('user_drivers').getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        const driver = marantz.driver;
        const device = marantz.device;
        var parent = TestUtils.createTab(null, null, module.id);

        const view = createDeviceView(parent, ctx, driver, device, 'command');
        //testPicker(marantzInstance);
        ctx.getWindowManager().registerView(view, true);

    }

    return DeviceTreeView;
});