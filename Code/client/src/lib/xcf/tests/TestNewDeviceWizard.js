/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "dojo/Deferred",
    "xcf/model/DefaultDevice",
    'xide/views/_CIPanelDialog',
    'xide/lodash',
    'xcf/widgets/CommandPicker'
], function (declare, types, utils, Grid, Deferred, DefaultDevice, _CIPanelDialog, _,CommandPicker) {
    console.clear();

    /**
     * @type {module:xcf/manager/Context}
     */
    const ctx = window.sctx;

    function isCyclic(obj) {
        const seenObjects = [];

        function detect(obj) {
            if (obj && typeof obj === 'object') {
                if (seenObjects.indexOf(obj) !== -1) {
                    return true;
                }
                seenObjects.push(obj);
                for (const key in obj) {
                    if (obj.hasOwnProperty(key) && detect(obj[key])) {
                        console.log(obj, 'cycle at ' + key);
                        return true;
                    }
                }
            }
            return false;
        }

        return detect(obj);
    }

    function findRecursive(orig) {
        const inspectedObjects = [];
        (function _find(o, s) {
            for (const i in o) {
                if (o[i] === orig) {
                    console.log('Found: obj.' + s.join('.') + '.' + i);
                    return;
                }
                if (inspectedObjects.indexOf(o[i]) >= 0) continue;
                else {
                    inspectedObjects.push(o[i]);
                    s.push(i);
                    _find(o[i], s);
                    s.pop(i);
                }
            }
        }(orig, []));
    }

    function dump(orig) {
        const inspectedObjects = [];
        console.log('== DUMP ==');
        (function _dump(o, t) {
            console.log(t + ' Type ' + (typeof o));
            for (const i in o) {
                if (o[i] === orig) {
                    console.log(t + ' ' + i + ': [recursive]');
                    continue;
                }
                const ind = 1 + inspectedObjects.indexOf(o[i]);
                if (ind > 0) console.log(t + ' ' + i + ':  [already inspected (' + ind + ')]');
                else {
                    console.log(t + ' ' + i + ': (' + inspectedObjects.push(o[i]) + ')');
                    _dump(o[i], t + '>>');
                }
            }
        }(orig, '>'));
    }


    function doTests() {

        if (ctx) {
            const deviceManager = ctx.getDeviceManager();
            const deviceId = '21201d2f-6d4f-99f9-5d10-873c9d629150';
            const device = deviceManager.getDeviceById(deviceId);

            deviceManager.newItem2 = function () {

                function transfer(changedCIS, prop, newDevice) {
                    _.find(changedCIS, {name: prop}) && newDevice.setMetaValue(prop, utils.getCIInputValueByName(changedCIS, prop));
                }
                function complete(newDevice, cis,changedCIS,driver) {
                    const DEVICE_PROPERTY = types.DEVICE_PROPERTY;
                    _.each([DEVICE_PROPERTY.CF_DEVICE_HOST,
                        DEVICE_PROPERTY.CF_DEVICE_PORT,
                        DEVICE_PROPERTY.CF_DEVICE_PROTOCOL,
                        DEVICE_PROPERTY.CF_DEVICE_DRIVER_OPTIONS,
                        DEVICE_PROPERTY.CF_DEVICE_OPTIONS,
                        DEVICE_PROPERTY.CF_DEVICE_LOGGING_FLAGS,
                        DEVICE_PROPERTY.CF_DEVICE_ENABLED], function (prop) {
                        transfer(changedCIS, prop, newDevice);
                    });

                    if(driver){
                        const driverCI = utils.getCIInputValueByName(cis, DEVICE_PROPERTY.CF_DEVICE_DRIVER);
                        driverCI.group = "Common";
                        driverCI.enumType = "Driver";
                        newDevice.setMetaValue(DEVICE_PROPERTY.CF_DEVICE_DRIVER,driver.id);
                    }
                }
                function _createNewDevice(cis, changedCIS,driver) {
                    const targetCI = utils.getInputCIByName(cis, 'In Group');
                    const titleCI = utils.getInputCIByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                    const device = utils.getInputCIByName(cis, 'copyFrom');
                    const dfd = new Deferred();
                    const deviceManager = ctx.getDeviceManager();
                    if (device && _.isObject(device.value)) {
                        //clone device wants parent to be a model
                        if (_.isString(targetCI.value)) {
                            targetCI.value = deviceManager.getItemByPath(targetCI.value);
                        }
                        deviceManager.cloneDevice(device.value, targetCI.value, titleCI.value).then(function (newDevice) {
                            complete(newDevice, cis,changedCIS,driver);
                            dfd.resolve(newDevice);
                        });
                    } else {
                        deviceManager._newItem(cis).then(function (newDevice) {
                            complete(newDevice, cis,changedCIS,driver);
                            dfd.resolve(newDevice);
                        });
                    }
                    return dfd;
                }
                function createNewDevice(cis, changedCIS) {

                    const deviceManager = ctx.getDeviceManager();
                    const driverManager = ctx.getDriverManager();
                    const scope = utils.getCIInputValueByName(cis, 'scope');
                    const dfd = new Deferred();
                    function createDriver(parent,title){
                        return driverManager.newItem(parent,title);
                    }
                    function getDriverGroup(scope,path,newTitle){
                        const dfd = new Deferred();
                        const driverParentPathObject = driverManager.getItemByPath(path);
                        if(driverParentPathObject){
                            dfd.resolve(driverParentPathObject)
                        }else{
                            const newDriverScope = scope==='user_devices' ? 'user_drivers' : 'system_drivers';
                            driverManager.newGroup(null,newDriverScope,newTitle).then(function(newDriverGroup){
                                dfd.resolve(newDriverGroup);
                            });
                        }
                        return dfd;
                    }
                    switch (utils.getCIInputValueByName(cis, 'Driver Creation')){
                        case 'auto-create':{
                            const targetCI = utils.getInputCIByName(cis, 'In Group');
                            const titleCI = utils.getInputCIByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_TITLE);
                            if (_.isString(targetCI.value)) {
                                targetCI.value = deviceManager.getItemByPath(targetCI.value);
                            }
                            getDriverGroup(scope,targetCI.value.name,targetCI.value.name).then(function(driverParent){
                                createDriver(driverParent,titleCI.value).then(function(driver){
                                    _createNewDevice(cis,changedCIS,driver).then(function (device) {
                                        dfd.resolve(device);
                                    })
                                });
                            });
                            return dfd;
                        }
                        default:{
                            _createNewDevice(cis,changedCIS,null).then(function (device) {
                                dfd.resolve(device);
                            });
                            return dfd;
                        }
                    }
                }

                const dfd = new Deferred();

                let cis = DefaultDevice(ctx);
                const optionsCI = utils.getInputCIByName(cis, types.DEVICE_PROPERTY.CF_DEVICE_OPTIONS);
                optionsCI.type = types.ECIType.STRUCTURE;
                optionsCI.name = 'Driver Options';
                cis = cis.concat([
                    utils.createCI('Driver Creation', types.ECIType.ENUMERATION, "auto-create", {
                        group: 'Driver',
                        visible: true,
                        description: "Specify the way how the device's driver is created",
                        value:'auto-create',
                        options: [
                            {
                                label: 'Create new driver',
                                value: 'auto-create',
                                description: "Create a driver with same name"
                            },
                            /*,
                            {
                                label: 'Copy driver',
                                value: 'copy',
                                description: "Copy the driver selected above"
                            },
                            */
                            {
                                label: 'None',
                                value: 'none',
                                description: "Don`t create any driver"
                            }
                        ]
                    }),
                    utils.createCI('copyFrom', CommandPicker, '', {
                        group: 'Common',
                        title: 'Copy from',
                        options: [],
                        pickerType: 'device',
                        value: ""
                    }),
                    utils.createCI('In Group', CommandPicker, 'Tests', {
                        group: 'Common',
                        title: 'In Group',
                        options: [],
                        pickerType: 'group',
                        value: "Tests"
                    }),
                    utils.createCI('Scope', types.ECIType.ENUMERATION, "user_devices", {
                        group: 'Common',
                        value: 'user_devices',
                        options: [
                            {
                                label: 'System Devices',
                                value: 'system_devices'
                            },
                            {
                                label: 'User Devices',
                                value: 'user_devices'
                            }
                        ]
                    })
                ]);

                cis.reverse();

                const panel = new _CIPanelDialog({
                    title: 'New Device',
                    onOk: function (changedCIS) {
                        changedCIS = _.map(changedCIS, function (obj) {
                            return obj.ci;
                        });
                        createNewDevice(cis, changedCIS).then(function(newDevice){
                            dfd.resolve(newDevice);
                        })

                        this.headDfd.resolve(changedCIS);
                    },
                    onCancel: function (changedCIS) {
                        this.headDfd.resolve(changedCIS);
                    },
                    cis: cis,
                    CIViewOptions: {
                        delegate: this,
                        ciSort: false,
                        cis: cis
                    }
                });
                panel.show();

                return dfd;
            }
            return deviceManager.newItem2(null, null);
        }
    }

    if (ctx) {
        doTests(null);
        return declare('a', null, {});

    }

    return Grid;

});