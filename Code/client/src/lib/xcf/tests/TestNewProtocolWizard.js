define([
    'dcl/dcl',
    'xdojo/declare',
    'dojo/_base/lang',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    "xide/views/_ActionMixin",
    'xaction/DefaultActions',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'xide/_base/_Widget',
    'xgrid/Search',
    "xide/views/_Dialog",
    "xcf/views/DeviceTreeView",
    "xcf/views/ProtocolTreeView",
    'xide/layout/_TabContainer',
    "xide/wizard/WizardBase"

], function (dcl,declare, lang, utils, types,factory,
             Grid, TreeRenderer, _ActionMixin,
             DefaultActions,KeyboardNavigation,_Widget,_XWidget,Search,_Dialog,DeviceTreeView,ProtocolTreeView,_TabContainer,WizardBase) {
    const DefaultEvents = [
        'onInit',
        'onShow',
        'onNext',
        'onPrevious',
        'onFirst',
        'onLast',
        'onBack',
        'onFinish',
        'onTabChange',
        'onTabClick',
        'onTabShow'
    ];

    /*************************************************************************************/
    /*      Wizard - View                                                                */
    const _WizardBase = dcl([_XWidget],{
        containerClass:_TabContainer,
        containerArgs:{

            direction:'above',
            //resizeContainer:false,
            navBarClass:'',
            resizeToParent:true
        },
        options:null,
        paneClass:null,
        panes:[],
        buttons:[],
        wizard:null,
        $wizard:null,
        buttonBar:null,
        templateString:'<div attachTo="domNode" style="height: inherit;position: relative">' +
        '<div attachTo="containerNode" style="height: auto;min-height: 300px"></div>'+
        '<div style="min-height:initial;position: absolute" attachTo="buttonBar" class="navbar bg-opaque navbar-fixed-bottom"></div>'+
        '</div>',
        getDefaultOptions:function(){

            const events = this.events || DefaultEvents;

            //result
            const options = {};


            //handler, emits & calls instance method if aviable
            function handler(eventKey,args){
                //var args = Array.prototype.slice.call(arguments, 1);

                 console.log('---'+eventKey,args);
                /*
                 if(eventKey =='onTabShow'){
                 //debugger;
                 }
                 */
                this._emit(eventKey,args);
                _.isFunction(this[eventKey]) && this[eventKey].apply(this,args);
            }

            const self = this;
            _.each(events,function(event){
                options[event]=function(){
                    //var _inner = [event].concat(arguments);
                    handler.apply(self,[event,arguments]);
                }
            });
            return options;
        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Stub functions, being overriden by the actual implementation of  https://github.com/VinceG/twitter-bootstrap-wizard
        //
        next:function(){},
        previous:function(){},
        first:function(){},
        last:function(){},
        finish:function(){

        },
        back:function(){},
        currentIndex:function(){},
        firstIndex:function(){},
        lastIndex:function(){},
        getIndex:function(){},
        nextIndex:function(){},
        previousIndex:function(){},
        navigationLength:function(){},
        activeTab:function(){},
        nextTab:function(){},
        previousTab:function(){},
        show:function(){},
        hide:function(){},
        display:function(){},
        enable:function(){},
        disable:function(){},
        remove:function(){},
        resetWizard:function(){},
        onWidgetAdded:function(what){

            this.add(what,null,false);
            this.resize();
        },

        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Added API
        //
        disableButton:function(selector,disabled){

            const btn = $(this.domNode).find(selector);
            btn.toggleClass('disabled', disabled);

        },
        hideButton:function(selector,hidden){
            $(this.domNode).find(selector).toggleClass('hidden', hidden);
        },
        onTabShow:function(a,b,index){
            const pane = this.panes[index];

            if(pane){

                if(!pane.widget) {
                    const parent = pane.container;

                    if (pane.create) {

                        const what =pane.create.apply(this, [parent, pane]);
                        this.onWidgetAdded(what);
                    }


                }

                pane.onShow && pane.onShow.apply(this,[pane]);
            }

        },
        /////////////////////////////////////////////////////////////////////////////////////////
        //
        //  STD API
        //
        startup:function(){



            const container = utils.addWidget(this.containerClass, this.containerArgs, this, this.$containerNode[0], true);
            this.add(container,null,false);
            const self = this;

            _.each(this.panes, function (pane) {
                pane.container = container._createTab(this.paneClass || pane.paneClass, pane);
            }, this);


            //var buttonParent = $('<div class="buttonBar"/>');


            //var buttonBarParent = container.$tabContentNode[0];


            const buttonBarParent = this.buttonParent || this.$buttonBar[0];

            this.buttons && _.each(this.buttons, function (button) {
                //pane.tab = container._createTab(this.paneClass||pane.paneClass,pane);
                if(!button.widget) {
                    button.widget = factory.createSimpleButton(button.title, button.icon, button.cls, button.mixin, buttonBarParent);
                    button.name && $(button.widget).attr('name', button.name);
                    button.value && $(button.widget).attr('value', button.value);
                }
            }, this);


            const options = utils.mixin(this.getDefaultOptions(),this.options);
            const wizard = this.$domNode.bootstrapWizard(options);
            this.$wizard = wizard;
            this.wizard = wizard.data('bootstrapWizard');

            this.options = options;
            utils.mixin(this,this.wizard);


            this.buttons && _.each(this.buttons, function (button) {

                $(button.widget).toggleClass('disabled', button.disabled);
            });

            this.resize();

        }

    });

    const ACTION = types.ACTION;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const ctx = window.sctx;
    const test = true;
    var marantz;
    let marantzInstance;

    /*************************************************************************************/
    /*      Protocol tree  - View                                                        */
    const _gridClass = Grid.createGridClass('ProtocolTreeView',{
        menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress:false,
            isGroup:function(item){

                if(item){
                    return item.isDir===true;
                }else{
                    return false;

                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                }
            },
            postMixInProperties: function() {
                const thiz = this;

                this.collection = this.collection.filter(this.getRootFilter());


                this.columns=this.getColumns();
                this.inherited(arguments);
            },
            runAction:function(action,item){
                console.log('run action in',action);
                this.inherited(arguments);
                //thiz.runAction(action,item);
            }

        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR:types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            WIDGET:{
                CLASS:_Widget
            },
            SEARCH:{
                CLASS:Search
            }

        },
        {
            RENDERER: TreeRenderer
        },
        null,
        null);

    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.HEADER,
        ACTION.SEARCH,
        'File/New Group'
    ];


    const Module = declare('xcf.views.ProtocolTreeView',_gridClass,{

        icon: 'fa-file-code-o',
        beanType: types.ITEM_TYPE.PROTOCOL,
        permissions : DefaultPermissions,
        deselectOnRefresh: false,
        showHeader: false,
        toolbarInitiallyHidden:true,
        //attachDirect:true,
        groupOrder: {
            'Clipboard': 110,
            'Device':105,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'New': 5,
            'Navigation': 3,
            'Select': 0
        },
        getDeviceActions: function (container,permissions,grid) {


            const actions = [], thiz = this, delegate = thiz.delegate, ACTION = types.ACTION;

            return [];

            function _selection(){
                const selection = grid.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                if(!item){
                    console.error('have no item');
                    return null;
                }
                return selection;
            }
            ///////////////////////////////////////////////////////////////
            //
            //      Disable Functions
            //
            ///////////////////////////////////////////////////////////////
            function shouldDisableEdit(){

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                return false;
            }
            function shouldDisableRun(){

                const selection = _selection();
                if (!selection || !selection[0].ref || !selection[0].ref.item) {
                    return true;
                }
                return false;
            }
            function shouldDisableConnect(){

                const selection = _selection();
                if (!selection || !selection[0] || !selection[0].path || selection[0].path.indexOf('.json')===-1) {
                    return true;
                }
                return false;
            }

            ///////////////////////////////////////////////////////////////
            //
            //      Actions
            //
            ///////////////////////////////////////////////////////////////

            const defaultMixin = {
                          addPermission:true
                      },
                  DEVICE_COMMAND_GROUP = 'Device/Command';


            actions.push(this.createAction('Edit',ACTION.EDIT,types.ACTION_ICON.EDIT,['f4', 'enter','dblclick'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(this.createAction('Edit Driver','File/Edit Driver',types.ACTION_ICON.EDIT,['f3', 'ctrl enter'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));


            actions.push(this.createAction('Edit Instance','File/Edit Driver Instance',types.ACTION_ICON.EDIT,['f6'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(this.createAction('Console','File/Console','el-icon-indent-left',['f5', 'ctrl s'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(thiz.createAction({
                label: 'Run',
                command:ACTION.RUN ,
                icon: types.ACTION_ICON.RUN,
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: shouldDisableRun,
                mixin: defaultMixin
            }));
            /*
            actions.push(this.createAction('Run', ACTION.RUN,types.ACTION_ICON.RUN, ['r'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableRun,permissions,container,grid
            ));
            */

            actions.push(this.createAction('New Group', 'File/New Group' ,types.ACTION_ICON.NEW_DIRECTORY,
                ['f7'],'Home','New','item|view',null,null,
                defaultMixin,null,null,permissions,container,grid
            ));

            actions.push(this.createAction('New Device', 'File/New Device' ,types.ACTION_ICON.NEW_FILE, ['ctrl n'],'Home','New','item',null,null,
                defaultMixin,null,null,permissions,container,grid
            ));



            ///////////////////////////////////////////////////////////////
            //  Device Control
            actions.push(this.createAction('Disconnect', 'File/DisconnectDevice','fa-unlink', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableConnect,permissions,container,grid
            ));

            actions.push(this.createAction('_Connect', 'File/ConnectDevice','fa-link', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableConnect,permissions,container,grid
            ));


            /*
             actions.push(this.createActionParameters('New Device Group', 'File/New Device Group', 'edit', types.ACTION_ICON.NEW_DIRECTORY, function () {
             delegate.newGroup(thiz.getSelectedItem());
             }, 'F7', 'f7', null, container, thiz));
             */



            return actions;
        },
        editItem: function (item) {
            const thiz = this;
            if (item.type === types.ITEM_TYPE.BLOCK) {
                return thiz.blockManager.editBlock(item.ref.item, null, false);
            }
            if(item.driver){
                item = item.driver;
            }

            thiz.openItem(item);
        },
        startup:function(){

            const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);

            this.inherited(arguments);

            this.addActions(_defaultActions);

            this.addActions(this.getDeviceActions(this.domNode,this.permissions,this));

            const thiz = this;

            this.refresh().then(function(){
                thiz.select([0], null, true, {
                    focus: true
                });
            });

            this.subscribe(types.EVENTS.ON_STORE_CHANGED,function(evt){
                if(evt.store == thiz.collection){
                    thiz.refresh();
                }
            });

            thiz.collection.on('update',function(){
                thiz.refresh();
            });

            thiz.collection.on('delete',function(){
                thiz.refresh();
            });

        },
        //////////////////////////////////////////////////////////////////////
        //
        //  Actions - Impl.
        //
        changeSource:function(source){
            console.log('change source',arguments);


            const thiz = this;
            const delegate = thiz.delegate;
            const scope = source.scope;
            let store = delegate.getStore(scope);


            function wireStore(store){

                if(thiz['_storeConnected'+scope]){
                    return;
                }

                thiz['_storeConnected'+scope]=true;

                function updateGrid() {
                    setTimeout(function () {
                        thiz.refresh();
                    }, 100);
                }

                store.on('update', function () {
                    console.warn(' store updated: ',arguments);
                    updateGrid();

                });

                store.on('added', function () {
                    updateGrid();
                });

                store.on('remove', function () {
                    updateGrid();
                });

                store.on('delete', function () {
                    //console.log(' deleted from store: ',arguments);
                    updateGrid();
                });

            }

            function ready(store){


                const collection = store.filter(thiz.getRootFilter());
                thiz.set('collection',collection);
                thiz.set('title','Drivers ('+source.label+')');
                thiz.scope = scope;
                wireStore(store);


            }

            if(store.then){

                store.then(function(){
                    store = delegate.getStore(scope);
                    ready(store);
                });
            }else{

                //is store
                ready(store);
            }
        },
        ///////////////////////////////////////////////////////////////
        runAction: function (action,_item) {
            if (!action || !action.command) {
                return;
            }

            console.log('--run action ' + action.command);

            const ACTION = types.ACTION;
            const thiz = this;
            const delegate = thiz.delegate;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            const driverManager = ctx.getDriverManager();
            let item = thiz.getSelectedItem() || _item;
            var device = item ? item.device : null;

            if(action.command.indexOf('View/Source')!=-1){
                return this.changeSource(action.item);
            }

            switch (action.command) {

                case 'File/Edit Driver Instance':{

                    console.log('---edit driver instance ');

                    var driverInstance  =  item.driverInstance;
                    if(driverInstance){

                        var driver = driverInstance.driver;
                        return driverManager.openItemSettings(driver,item);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'File/Edit Driver':{

                    console.log('---edit driver instance ');

                    const meta = item['user'], driverId = meta ? utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER) : null, driver = driverId ? driverManager.getItemById(driverId) : null;

                    if(driver){
                        return driverManager.openItemSettings(driver,null);
                    }

                    /*
                     var driverInstance  =  item.driverInstance;
                     if(driverInstance){
                     var driver = driverInstance.driver;
                     return driverManager.openItemSettings(driver,item);
                     }
                     */

                    break;

                }

                case ACTION.RUN:{
                    if (item && item.ref) {
                        const _device = item.ref.device;
                        if(_device){
                            var driverInstance = _device.driverInstance;
                            if(driverInstance){
                                const scope = driverInstance.blockScope;
                                const block = scope.getBlockById(item.id);
                                if(block){
                                    block.solve(block.scope);
                                }
                            }
                        }
                    }
                    break;
                }
                case 'File/Code':{
                    var device = item.device || null;
                    if(device && device.driverInstance){
                        item = device.driverInstance.driver;
                    }
                    return delegate.editDriver(null,item);
                }
                case 'File/Console':
                {
                    if(device && !device.driverInstance) {
                        deviceManager.startDevice(device).then(function(){
                            deviceManager.openConsole(item);
                        });
                        return;
                    }
                    return deviceManager.openConsole(item);
                }
                case 'File/Log':
                {
                    return deviceManager.openLog(item.device);
                }

                case ACTION.EDIT:
                {
                    return thiz.editItem(item);
                }
                case ACTION.RELOAD:
                {
                    return thiz.refresh();
                }
                case ACTION.DELETE:
                {
                    var _res = delegate.onDeleteItem(item);

                    return _res;
                }
                case 'File/DisconnectDevice':{

                    const dfd = delegate.stopDevice(item);
                    thiz.refresh();
                    return false;
                }
                case 'File/ConnectDevice':{
                    delegate.startDevice(item);
                    thiz.refresh();
                    return false;
                }
                case 'File/New Device':{

                    const _rest = delegate.newItem(item);
                    thiz.refresh();
                    return _rest;
                }
                case 'File/New Group':{
                    var _res = delegate.newGroup(item,thiz.scope);

                    thiz.refresh();
                    return _res;
                }
            }

            return this.inherited(arguments);
        },
        changeScope: function (name) {

            this.delegate.ls(name + '_drivers').then(function (data) {
                console.log('got scope ', data);
            });

        },
        getScopeActions: function () {

            const actions = [], thiz = this;
            container = this.containerNode;

            this.addAction(actions, _ActionMixin.createActionParameters('Settings', "View/Settings", 'edit', 'fa-cogs', function () {

            }, 'CTRL F1', ['ctrl f1'], null, container, thiz, {
                dummy: true,
                onCreate: function (action) {
                    action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                        widgetArgs: {
                            style: "float:right"
                        }
                    });
                }
            }));


            this.addAction(actions, _ActionMixin.createActionParameters('System Drivers', "View/Settings/System", 'view', 'fa-cloud', function () {

                thiz.changeScope('system');

            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                },
                tooltip: {
                    content: "Displays only drivers from the system library<br/> Hint: you can find system drivers also in the file manager!"
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('User Drivers', "View/Settings/User", 'view', 'fa-cloud', function () {
                thiz.changeScope('user');
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Project Drivers', "View/Settings/Project", 'view', 'fa-cloud', function () {
                thiz.changeScope('project');
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Add Driver Location', "View/Settings/Custom", 'view', 'fa-magic', function () {
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Show Values', "View/Settings/Values", 'viewFilter', 'fa-cogs', function () {
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));


            return actions;
        },
        openItem:function(item,device){
            if(item && !item.isDir && !item.virtual){
                this.delegate.openItemSettings(item,device);
            }
        },
        getColumns:function(){

            const thiz = this;

            return [
                {
                    renderExpando: true,
                    label: "Name",
                    field: "name",
                    sortable: false,
                    formatter: function (name, item) {


                        if (!thiz.isGroup(item) && item['user']) {
                            const meta = item['user'].meta;
                            const _in = meta ? utils.getCIInputValueByName(meta, types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE) : null;
                            if (meta) {
                                return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                            } else {
                                return item.name;
                            }
                            return _in;
                        } else {
                            return item.name;
                        }
                    }
                }
            ];
        }
    });

    /*************************************************************************************/
    /*      Helpers                                                                      */
    /**
     * Url generator for device/driver/[command|block|variable]
     *
     * @param device
     * @param driver
     * @param block
     * @param prefix
     * @returns {*}
     */
    function toUrl(device,driver,block,prefix){

        prefix = prefix || '';

        const pattern = prefix + "deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";

        const url = lang.replace(
            pattern,
            {
                deviceId:device.id,
                deviceScope:device.scope,
                driverId:driver.id,
                driverScope:driver.scope,
                block:block.id
            });
        return url;
    }

    /**
     * Filter function to reject selections in a device - tree - view
     * @param item
     * @param acceptCommand
     * @param acceptVariable
     * @param acceptDevice
     * @returns {*}
     */
    function accept(item,acceptCommand,acceptVariable,acceptDevice){


        const reference = item.ref || {}, block       =       reference.item, device      =       reference.device, driver      =       reference.driver, scope       =       block ? block.getScope() : null, isCommand   =       block ? block.declaredClass.indexOf('model.Command')!==-1 : false, isVariable  =       block ? block.declaredClass.indexOf('model.Variable')!==-1 : false;

        isCommand && console.log('have command ' + block.name);

        if(isCommand && acceptCommand){
            return toUrl(device,driver,block,'command://');
        }
        if(isVariable && acceptVariable){
            return toUrl(device,driver,block,'variable://');
        }

        return false;

    }

    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    function createProtocolView(parent,ctx,driver,device,type,mixin) {

        return utils.addWidget(ProtocolTreeView, utils.mixin({
            title: 'Devices',
            collection: ctx.getProtocolManager().store,
            delegate: ctx.getProtocolManager(),
            blockManager: ctx.getBlockManager(),
            ctx: ctx,
            icon: 'fa-sliders',
            showHeader: false,
            scope: 'system_protocols',
            resizeToParent: true
        },mixin), null, parent, true);
    }
    function createProtocolWizard(parent,ctx,driver,device,finish){
        const thiz = this;
        const selectView = null;
        const selectItemsView = null;
        let selectedItems = [];
        let selectedItem = null;
        const protocolMgr = ctx.getProtocolManager();
        const store = protocolMgr.getStore();
        let wizard = null;
        let activePane;


        const args = {

            onFinish:function(){

                const variableProto = utils.getObject('xcf/model/Variable');
                const commandProto = utils.getObject('xcf/model/Command');


                const _driver = driver;

                const scope = driver.blockScope, store = scope.blockStore;

                const items = selectedItems;


                for (let i = 0; i < items.length; i++) {

                    const protocolItem = items[i];
                    const isVariable = protocolItem.type == 'protocolVariable';



                    const name = protocolItem.name || protocolItem.title;


                    let ctrArgs = null;
                    if (isVariable) {
                        ctrArgs = {
                            value: protocolItem.value,
                            name: name,
                            group: 'basicVariables'

                        }
                    } else {
                        ctrArgs = {
                            send: protocolItem.value,
                            name: name,
                            group: 'basic'
                        }
                    }

                    const existing = scope.getBlockByName(name) || scope.getVariable(name);
                    if(existing){
                        isVariable ? (existing.value = protocolItem.value) : (existing.send = protocolItem.value);

                        for(var prop in protocolItem){
                            if(prop!=='name' && prop!=='value' && prop!=='title'){
                                existing[prop]=protocolItem[prop];
                            }
                        }

                        existing.refresh();
                        finish && finish();
                        return;
                    }



                    const _proto = isVariable ? variableProto : commandProto;


                    for(var prop in protocolItem){
                        if(prop!=='name' && prop!=='value' && prop!=='title'){
                            ctrArgs[prop]=protocolItem[prop];
                        }
                    }
                    let block = factory.createBlock(_proto, ctrArgs);

                    block = scope.blockStore.putSync(block);

                    block.scope  = scope;

                    finish && finish();

                }



            },
            options:{
                tabClass: 'nav',
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                'firstSelector': '.button-first',
                'lastSelector': '.button-last',
                'finishSelector': '.button-finish',
                'backSelector': '.button-back'

            },
            panes:[
                {

                    title: "Select Protocol",
                    icon: "fa-hand-o-up",
                    selected: true,
                    onShow:function(pane){
                        activePane = pane;
                        wizard && wizard.hideButton('.button-finish',true);
                    },
                    create:function(parent,pane){

                        const view = createProtocolView(parent,ctx,driver,device,'command');
                        pane.widget = view;
                        const store = view.collection;

                        view._on('selectionChanged', function (evt) {
                            const selection = evt.selection;
                            selectedItem = selection[0];
                            wizard.disableButton('.button-next',(!selectedItem || selectedItem.virtual));
                        });
                        return view;
                    }
                },
                {
                    title: "Select Items",
                    icon: "fa-hand-o-up",
                    onShow:function(pane){

                        activePane = pane;

                        wizard.disableButton('.button-next',true);
                        wizard.hideButton('.button-finish',false);
                    },
                    create:function(parent,pane){

                        const view = createProtocolView(parent,ctx,driver,device,'command',{
                            getRootFilter: function () {
                                if (selectedItem) {
                                    return {
                                        parentId: selectedItem.path
                                    }
                                } else {
                                    return {
                                        parentId: ''
                                    }
                                }
                            }
                        });
                        pane.widget = view;
                        const store = view.collection;
                        view._on('selectionChanged', function (evt) {
                            const selection = evt.selection;
                            selectedItems = selection;
                            wizard.disableButton('.button-finish',(!selection.length||selection[0].isDir));

                        });
                        wizard.hideButton('.button-finish',false);

                        return view;
                    }
                }
            ],
            buttons:[
                {
                    title: 'Previous',
                    cls: 'btn btn-default button-previous pull-left',
                    name: 'previous',
                    value: 'Previous',
                    disabled:true
                },
                {
                    title: 'Finish',
                    cls: 'btn btn-default button-finish pull-right',
                    name: 'finish',
                    value: 'Finish'
                },
                {
                    title: 'Next',
                    cls: 'btn btn-default button-next pull-right',
                    name: 'next',
                    value: 'Next',
                    disabled:true
                }
            ]
        };



        wizard = utils.addWidget(WizardBase,args,null,parent,false);

        return wizard;
    }
    function onFileChanged(evt) {



        if (evt._pp2) {
            return;
        }
        evt._pp2 = true;
        const data = evt.data;

        const thiz = this;
        if (!this.fileUpdateTimes) {
            this.fileUpdateTimes = {}
        };

        if (data.event === types.EVENTS.ON_FILE_CHANGED) {

            if (data.data && data.mask && data.mask.indexOf('delete') !== -1) {
                //thiz.publish(types.EVENTS.ON_FILE_DELETED, data);
                console.error('deleted');
                return;
            }
            const _path = data.path;
            const timeNow = new Date().getTime();
        }

        let path = utils.replaceAll('\\', '/', data.path);
        path = utils.replaceAll('//', '/', data.path);
        path = path.replace(/\\/g, "/");

        if (path.match(/\.json$/)) {

            const _start = 'data/system/protocols';
            if (path.indexOf(_start) != -1) {
                console.log('protocol changed' + path);
            }


            this.ls('system_protocols');
        }

        /*
        if (this.fileUpdateTimes[_path]) {

            var last = this.fileUpdateTimes[_path];
            var diff = timeNow - last;

            if (diff < 30) {
                this.fileUpdateTimes[_path] = timeNow;
                return;
            }
        }

        this.fileUpdateTimes[_path] = timeNow;
        */



    }
    function createProtocolWizardDialog(parent,ctx,driver,device,type){



        const dlg = new _Dialog({
            title: 'Import protocol ',
            type:types.DIALOG_TYPE.INFO,
            bodyCSS: {
                'height': '500px',
                'min-height': '300px',
                'padding': '4px',
                'margin-right': '0px'
            },
            buttons:[],
            picker: null,
            onOk: function () {

                const selected = this.picker._selected;
                //selected && thiz.onValueChanged(selected);


            },



            message: function (dlg) {
                const thiz = dlg.owner;
                const picker = createProtocolWizard($('<div class="" style="height: inherit;" />')[0],ctx,driver,device,function(){
                    dlg.owner.destroy();
                });
                thiz.picker = picker;
                thiz.add(picker, null, false);
                return $(picker.domNode);
            },
            onShow: function (dlg) {

                const picker = this.picker, self = this;

                picker.startup();
                this.startDfd.resolve();
            }
        });



        dlg.show().then(function () {
            dlg.resize();
            utils.resizeTo(dlg.picker, dlg, true, true);
        });
    }

    if(test && ctx){

        console.clear();

        //var parent  = TestUtils.createTab(null,null,module.id);

        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();
        const protocolManager = ctx.getProtocolManager();
        const grid = null;
        /* protocolManager.onFileChanged2 = function(evt){onFileChanged.apply(protocolManager,[evt]);} */
        var marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        //marantz  = driverManager.getItemById("715b7b19-b9ed-3c81-cfc7-39598a99a857");
        //marantzInstance = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        const driver = marantz.driver;
        const device = marantz.device;

        //var parent  = TestUtils.createTab(null,null,module.id);
        //var view = createProtocolView(parent,ctx,driver,device,'command');

        //var view = createProtocolWizard(parent,ctx,marantz,device,'command',true);
        const dlg = createProtocolWizardDialog(parent,ctx,marantz,device,'command',true);

        //testPicker(marantzInstance);
        //ctx.getWindowManager().registerView(view,true);

    }
    return DeviceTreeView;
});
