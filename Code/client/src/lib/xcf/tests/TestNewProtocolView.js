define([
    'xdojo/declare',
    'dojo/_base/lang',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    "xide/views/_ActionMixin",
    'xaction/DefaultActions',
    'xgrid/KeyboardNavigation',
    "xide/widgets/_Widget",
    'xgrid/Search',
    'xide/tests/TestUtils',
    'module',
    'dojo/when',
    'dojo/Deferred',
    "xcf/views/DeviceTreeView",
    "xcf/views/ProtocolTreeView"


], function (declare, lang, utils, types,factory,
             Grid, TreeRenderer, _ActionMixin,
             DefaultActions,KeyboardNavigation,_Widget,Search,TestUtils,module,
             when,Deferred,DeviceTreeView) {
    const ACTION = types.ACTION;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const ctx = window.sctx;
    const test = true;
    let marantz;
    let marantzInstance;



    const gridClass = Grid.createGridClass('ProtocolTreeView',{
        menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress:false,
            isGroup:function(item){

                if(item){
                    return item.isDir===true;
                }else{
                    return false;

                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                }
            },
            postMixInProperties: function() {
                const thiz = this;

                this.collection = this.collection.filter(this.getRootFilter());


                this.columns=this.getColumns();
                this.inherited(arguments);
            },
            runAction:function(action,item){
                console.log('run action in',action);
                this.inherited(arguments);
                //thiz.runAction(action,item);
            }

        },
        //features
        {
            SELECTION: true,
            KEYBOARD_SELECTION: true,
            PAGINATION: false,
            ACTIONS: types.GRID_FEATURES.ACTIONS,
            CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
            TOOLBAR:types.GRID_FEATURES.TOOLBAR,
            CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
            KEYBOARD_NAVIGATION:{
                CLASS:KeyboardNavigation
            },
            WIDGET:{
                CLASS:_Widget
            },
            SEARCH:{
                CLASS:Search
            }

        },
        {
            RENDERER: TreeRenderer
        },
        null,
        null);


    const DefaultPermissions = [
        ACTION.EDIT,
        ACTION.RENAME,
        ACTION.RELOAD,
        ACTION.DELETE,
        ACTION.CLIPBOARD,
        ACTION.LAYOUT,
        ACTION.COLUMNS,
        ACTION.SELECTION,
        ACTION.TOOLBAR,
        ACTION.HEADER,
        ACTION.SEARCH,
        'File/New Group'
    ];

    const Module =declare('xcf.views.ProtocolTreeView',gridClass,{

        icon: 'fa-file-code-o',
        beanType: types.ITEM_TYPE.PROTOCOL,
        permissions : DefaultPermissions,
        deselectOnRefresh: false,
        showHeader: false,
        toolbarInitiallyHidden:true,
        //attachDirect:true,
        groupOrder: {
            'Clipboard': 110,
            'Device':105,
            'File': 100,
            'Step': 80,
            'Open': 70,
            'Organize': 60,
            'Insert': 10,
            'New': 5,
            'Navigation': 3,
            'Select': 0
        },
        getDeviceActions: function (container,permissions,grid) {


            const actions = [], thiz = this, delegate = thiz.delegate, ACTION = types.ACTION;

            function _selection(){
                const selection = grid.getSelection();
                if (!selection || !selection.length) {
                    return null;
                }
                const item = selection[0];
                if(!item){
                    console.error('have no item');
                    return null;
                }
                return selection;
            }
            ///////////////////////////////////////////////////////////////
            //
            //      Disable Functions
            //
            ///////////////////////////////////////////////////////////////
            function shouldDisableEdit(){

                const selection = _selection();
                if (!selection || selection[0].isDir || selection[0].virtual) {
                    return true;
                }
                return false;
            }
            function shouldDisableRun(){

                const selection = _selection();
                if (!selection || !selection[0].ref || !selection[0].ref.item) {
                    return true;
                }
                return false;
            }
            function shouldDisableConnect(){

                const selection = _selection();
                if (!selection || !selection[0] || !selection[0].path || selection[0].path.indexOf('.json')===-1) {
                    return true;
                }
                return false;
            }

            ///////////////////////////////////////////////////////////////
            //
            //      Actions
            //
            ///////////////////////////////////////////////////////////////

            const defaultMixin = {
                          addPermission:true
                      },
                  DEVICE_COMMAND_GROUP = 'Device/Command';


            actions.push(this.createAction('Edit',ACTION.EDIT,types.ACTION_ICON.EDIT,['f4', 'enter','dblclick'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(this.createAction('Edit Driver','File/Edit Driver',types.ACTION_ICON.EDIT,['f3', 'ctrl enter'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));


            actions.push(this.createAction('Edit Instance','File/Edit Driver Instance',types.ACTION_ICON.EDIT,['f6'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(this.createAction('Console','File/Console','el-icon-indent-left',['f5', 'ctrl s'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableEdit,permissions,container,grid
            ));

            actions.push(thiz.createAction({
                label: 'Run',
                command:ACTION.RUN ,
                icon: types.ACTION_ICON.RUN,
                tab: 'Home',
                group: 'Step',
                keycombo: ['r'],
                shouldDisable: shouldDisableRun,
                mixin: defaultMixin
            }));
            /*
            actions.push(this.createAction('Run', ACTION.RUN,types.ACTION_ICON.RUN, ['r'],'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableRun,permissions,container,grid
            ));
            */

            actions.push(this.createAction('New Group', 'File/New Group' ,types.ACTION_ICON.NEW_DIRECTORY,
                ['f7'],'Home','New','item|view',null,null,
                defaultMixin,null,null,permissions,container,grid
            ));

            actions.push(this.createAction('New Device', 'File/New Device' ,types.ACTION_ICON.NEW_FILE, ['ctrl n'],'Home','New','item',null,null,
                defaultMixin,null,null,permissions,container,grid
            ));



            ///////////////////////////////////////////////////////////////
            //  Device Control
            actions.push(this.createAction('Disconnect', 'File/DisconnectDevice','fa-unlink', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableConnect,permissions,container,grid
            ));

            actions.push(this.createAction('_Connect', 'File/ConnectDevice','fa-link', null,'Home',DEVICE_COMMAND_GROUP,'item',null,null,
                defaultMixin,null,shouldDisableConnect,permissions,container,grid
            ));


            /*
             actions.push(this.createActionParameters('New Device Group', 'File/New Device Group', 'edit', types.ACTION_ICON.NEW_DIRECTORY, function () {
             delegate.newGroup(thiz.getSelectedItem());
             }, 'F7', 'f7', null, container, thiz));
             */



            return actions;
        },
        editItem: function (item) {
            const thiz = this;
            if (item.type === types.ITEM_TYPE.BLOCK) {
                return thiz.blockManager.editBlock(item.ref.item, null, false);
            }
            if(item.driver){
                item = item.driver;
            }

            thiz.openItem(item);
        },
        startup:function(){

            const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);

            this.inherited(arguments);

            this.addActions(_defaultActions);

            this.addActions(this.getDeviceActions(this.domNode,this.permissions,this));

            const thiz = this;
            this.refresh().then(function(){
                thiz.select([0], null, true, {
                    focus: true
                });
            });



            this.subscribe(types.EVENTS.ON_STORE_CHANGED,function(evt){
                if(evt.store == thiz.collection){
                    thiz.refresh();
                }
            });

            thiz.collection.on('update',function(){
                //console.log('store item update');
                thiz.refresh();
            });
            thiz.collection.on('delete',function(){
                //console.log('store item update');
                thiz.refresh();
            });

            this._on('selectionChanged',function(e){
                //console.log(e.selection);
            });

        },
        //////////////////////////////////////////////////////////////////////
        //
        //  Actions - Impl.
        //
        changeSource:function(source){
            console.log('change source',arguments);


            const thiz = this;
            const delegate = thiz.delegate;
            const scope = source.scope;
            let store = delegate.getStore(scope);


            function wireStore(store){

                if(thiz['_storeConnected'+scope]){
                    return;
                }

                thiz['_storeConnected'+scope]=true;

                function updateGrid() {
                    setTimeout(function () {
                        thiz.refresh();
                    }, 100);
                }

                store.on('update', function () {
                    console.warn(' store updated: ',arguments);
                    updateGrid();

                });

                store.on('added', function () {
                    //console.log(' added to store: ',arguments);
                    updateGrid();
                });

                store.on('remove', function () {
                    //console.log(' removed from store: ',arguments);
                    updateGrid();
                });

                store.on('delete', function () {
                    //console.log(' deleted from store: ',arguments);
                    updateGrid();
                });

            }

            function ready(store){


                const collection = store.filter(thiz.getRootFilter());
                thiz.set('collection',collection);
                thiz.set('title','Drivers ('+source.label+')');
                thiz.scope = scope;
                wireStore(store);


            }

            if(store.then){

                store.then(function(){
                    store = delegate.getStore(scope);
                    ready(store);
                });
            }else{

                //is store
                ready(store);
            }
        },
        ///////////////////////////////////////////////////////////////
        runAction: function (action,_item) {
            if (!action || !action.command) {
                return;
            }

            console.log('--run action ' + action.command);

            const ACTION = types.ACTION;
            const thiz = this;
            const delegate = thiz.delegate;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            const driverManager = ctx.getDriverManager();
            let item = thiz.getSelectedItem() || _item;
            var device = item ? item.device : null;

            if(action.command.indexOf('View/Source')!=-1){
                return this.changeSource(action.item);
            }

            switch (action.command) {

                case 'File/Edit Driver Instance':{

                    console.log('---edit driver instance ');

                    var driverInstance  =  item.driverInstance;
                    if(driverInstance){

                        var driver = driverInstance.driver;
                        return driverManager.openItemSettings(driver,item);
                    }else{

                        this.publish(types.EVENTS.ON_STATUS_MESSAGE,{
                            text:'Can`t edit driver instance because device is not enabled or connected!',
                            type:'error'
                        });
                    }

                    break;

                }
                case 'File/Edit Driver':{

                    console.log('---edit driver instance ');

                    const meta = item['user'], driverId = meta ? utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER) : null, driver = driverId ? driverManager.getItemById(driverId) : null;

                    if(driver){
                        return driverManager.openItemSettings(driver,null);
                    }

                    /*
                     var driverInstance  =  item.driverInstance;
                     if(driverInstance){
                     var driver = driverInstance.driver;
                     return driverManager.openItemSettings(driver,item);
                     }
                     */

                    break;

                }

                case ACTION.RUN:{
                    if (item && item.ref) {
                        const _device = item.ref.device;
                        if(_device){
                            var driverInstance = _device.driverInstance;
                            if(driverInstance){
                                const scope = driverInstance.blockScope;
                                const block = scope.getBlockById(item.id);
                                if(block){
                                    block.solve(block.scope);
                                }
                            }
                        }
                    }
                    break;
                }
                case 'File/Code':{
                    var device = item.device || null;
                    if(device && device.driverInstance){
                        item = device.driverInstance.driver;
                    }
                    return delegate.editDriver(null,item);
                }
                case 'File/Console':
                {
                    if(device && !device.driverInstance) {
                        deviceManager.startDevice(device).then(function(){
                            deviceManager.openConsole(item);
                        });
                        return;
                    }
                    return deviceManager.openConsole(item);
                }
                case 'File/Log':
                {
                    return deviceManager.openLog(item.device);
                }

                case ACTION.EDIT:
                {
                    return thiz.editItem(item);
                }
                case ACTION.RELOAD:
                {
                    return thiz.refresh();
                }
                case ACTION.DELETE:
                {
                    var _res = delegate.onDeleteItem(item);

                    return _res;
                }
                case 'File/DisconnectDevice':{

                    const dfd = delegate.stopDevice(item);
                    thiz.refresh();
                    return false;
                }
                case 'File/ConnectDevice':{
                    delegate.startDevice(item);
                    thiz.refresh();
                    return false;
                }
                case 'File/New Device':{

                    const _rest = delegate.newItem(item);
                    thiz.refresh();
                    return _rest;
                }
                case 'File/New Group':{
                    var _res = delegate.newGroup(item,thiz.scope);

                    thiz.refresh();
                    return _res;
                }
            }

            return this.inherited(arguments);
        },
        changeScope: function (name) {

            this.delegate.ls(name + '_drivers').then(function (data) {
                console.log('got scope ', data);
            });

        },
        getScopeActions: function () {

            const actions = [], thiz = this;
            container = this.containerNode;

            this.addAction(actions, _ActionMixin.createActionParameters('Settings', "View/Settings", 'edit', 'fa-cogs', function () {

            }, 'CTRL F1', ['ctrl f1'], null, container, thiz, {
                dummy: true,
                onCreate: function (action) {
                    action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, {
                        widgetArgs: {
                            style: "float:right"
                        }
                    });
                }
            }));


            this.addAction(actions, _ActionMixin.createActionParameters('System Drivers', "View/Settings/System", 'view', 'fa-cloud', function () {

                thiz.changeScope('system');

            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                },
                tooltip: {
                    content: "Displays only drivers from the system library<br/> Hint: you can find system drivers also in the file manager!"
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('User Drivers', "View/Settings/User", 'view', 'fa-cloud', function () {
                thiz.changeScope('user');
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Project Drivers', "View/Settings/Project", 'view', 'fa-cloud', function () {
                thiz.changeScope('project');
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Add Driver Location', "View/Settings/Custom", 'view', 'fa-magic', function () {
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));

            this.addAction(actions, _ActionMixin.createActionParameters('Show Values', "View/Settings/Values", 'viewFilter', 'fa-cogs', function () {
            }, '', null, null, container, thiz, {
                onCreate: function (action) {
                }
            }));


            return actions;
        },
        openItem:function(item,device){
            if(item && !item.isDir && !item.virtual){
                this.delegate.openItemSettings(item,device);
            }
        },
        getColumns:function(){

            const thiz = this;

            return [
                {
                    renderExpando: true,
                    label: "Name",
                    field: "name",
                    sortable: false,
                    formatter: function (name, item) {


                        if (!thiz.isGroup(item) && item['user']) {
                            const meta = item['user'].meta;
                            const _in = meta ? utils.getCIInputValueByName(meta, types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE) : null;
                            if (meta) {
                                return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                            } else {
                                return item.name;
                            }
                            return _in;
                        } else {
                            return item.name;
                        }
                    }
                }
            ];
        }
    });

    /**
     * Url generator for device/driver/[command|block|variable]
     *
     * @param device
     * @param driver
     * @param block
     * @param prefix
     * @returns {*}
     */
    function toUrl(device,driver,block,prefix){

        prefix = prefix || '';

        const pattern = prefix + "deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";

        const url = lang.replace(
            pattern,
            {
                deviceId:device.id,
                deviceScope:device.scope,
                driverId:driver.id,
                driverScope:driver.scope,
                block:block.id
            });
        return url;
    }

    /**
     * Filter function to reject selections in a device - tree - view
     * @param item
     * @param acceptCommand
     * @param acceptVariable
     * @param acceptDevice
     * @returns {*}
     */
    function accept(item,acceptCommand,acceptVariable,acceptDevice){


        const reference = item.ref || {}, block       =       reference.item, device      =       reference.device, driver      =       reference.driver, scope       =       block ? block.getScope() : null, isCommand   =       block ? block.declaredClass.indexOf('model.Command')!==-1 : false, isVariable  =       block ? block.declaredClass.indexOf('model.Variable')!==-1 : false;

        isCommand && console.log('have command ' + block.name);

        if(isCommand && acceptCommand){
            return toUrl(device,driver,block,'command://');
        }
        if(isVariable && acceptVariable){
            return toUrl(device,driver,block,'variable://');
        }

        return false;

    }
    /**
     * Renders a CIS inline
     * @param CIS
     * @param where
     * @param owner
     * @returns {*}
     */
    function renderCIS(CIS,where,owner){

        const widgetsHead = factory.createWidgetsFromArray(CIS, owner, null, false);
        const result = [];
        const dfd = new Deferred();

        when(widgetsHead,function(widgets){

            if (widgets) {


                for (let i = 0; i < widgets.length; i++) {

                    const widget = widgets[i];

                    widget.delegate = owner;

                    where.appendChild(widget.domNode);

                    if(where && where.lazy===true) {
                        widget._startOnShow = true;
                    }else{
                        widget.startup();
                    }


                    widget._on('valueChanged',function(evt){
                        //owner.onValueChanged(evt);
                    });

                    owner._emit('widget',{
                        widget:widget,
                        ci:widget.userData
                    })


                    result.push(widget);

                    widget.userData.view=owner;
                    widget.onAttached && widget.onAttached(where);

                    if(owner && owner.add && owner.add(widget,null,false)){

                    }else{
                        console.error('view has no add',owner);
                        owner.add(widget,null,false);
                    }
                }

                dfd.resolve(result);

            }
        });

        return dfd;

    }


    /**
     * Create a simple CIS
     * @returns {{inputs: *[]}}
     */
    function createPickerCIS(instance){

        const CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General9',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Command",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("86025507-97e0-99a4-0640-59c5725ef116"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'command'
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("6ab97609-4b3a-4f65-d018-1027d402a94d"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'variable'
                }
            ]
        };
        _.each(CIS.inputs,function(ci){
            ci.driver = instance.driver,
                ci.device = instance.device
        });
        return CIS;
    }




    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    function createProtocolView(parent,ctx,driver,device,type) {

        const grid = utils.addWidget(Module, {
            title: 'Devices',
            collection: ctx.getProtocolManager().store,
            delegate: ctx.getProtocolManager(),
            blockManager: ctx.getBlockManager(),
            ctx: ctx,
            icon: 'fa-sliders',
            showHeader: false,
            scope: 'system_protocols',
            resizeToParent: true
        }, null, parent, true);

        return grid;
    }

    if(test && ctx){

        console.clear();

        var parent  = TestUtils.createTab(null,null,module.id);
        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();
        const grid = null;



        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        marantzInstance = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        const driver = marantz.driver;
        const device = marantz.device;

        var parent  = TestUtils.createTab(null,null,module.id);

        const view = createProtocolView(parent,ctx,driver,device,'command');


        //testPicker(marantzInstance);
        ctx.getWindowManager().registerView(view,true);

    }




    return DeviceTreeView;
});
