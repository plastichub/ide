/** @module xgrid/Base **/
define([
    "xdojo/declare",
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    'xgrid/Grid',
    "xide/widgets/TemplatedWidgetBase",
    "xide/mixins/EventedMixin",
    "xide/tests/TestUtils",
    "xfile/tests/TestUtils",
    "xide/widgets/_Widget",
    "xide/registry",
    "module",
    "dojo/cache",	// dojo.cache
    "dojo/cookie",
    "dojo/dom-construct", // domConstruct.destroy, domConstruct.toDom
    "dojo/_base/lang", // lang.getObject
    "dojo/string",
    "xide/_base/_Widget",
    'xide/views/ConsoleView',
    'xide/views/History',
    'xace/views/ACEEditor',
    'xace/views/Editor',
    'xide/mixins/PersistenceMixin',
    'xide/views/_LayoutMixin',
    'xide/views/_Console',
    'xide/views/_ConsoleWidget',
    'xaction/ActionProvider',
    'xaction/Toolbar',
    'xaction/DefaultActions',
    'xide/widgets/ActionToolbar'

], function (declare, dcl, types,
             utils, Grid, TemplatedWidgetBase, EventedMixin,
             TestUtils, FTestUtils, _Widget, registry, module,
             cache, cookie, domConstruct, lang, string, _XWidget, ConsoleView,
             History, ACEEditor, Editor,PersistenceMixin,
             _LayoutMixin,
             Console, ConsoleWidget,ActionProvider,Toolbar,DefaultActions,ActionToolbar) {
    console.clear();


    console.log('--do-tests');

    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;

    function createConsoleClass(){
        const _Console = dcl([_XWidget,ActionProvider.dcl,Toolbar.dcl],{
            declaredClass:'xide.views.ConsoleView',
            templateString:'<div attachTo="template" class="grid-template widget" style="width: 100%;height: 100%;overflow: hidden !important;position: relative;padding: 0px;margin: 0px">' +
                '<div attachTo="header" class="view-header row bg-opaque" style=";width:100%;height: 30px;min-height: 30px;"></div>' +
                '<div attachTo="logNode" style="overflow: hidden">' +
                    '<div class="logView" style="display:inline-block;width:100%;overflow: auto;height:inherit" attachTo="logView">'+

                    '</div>'+
                '</div>'+
            '</div>',
            defaultPanelOptions:{
                w: '100%',
                title:'Output'
            },
            defaultPanelType:'DefaultFixed',
            resizeToParent:true,
            toolbarArgs:{
                attachToGlobal:true
            },
            permissions : [
                ACTION.RELOAD,
                ACTION.SAVE,
                //ACTION.FIND,
                //ACTION.TOOLBAR,
                ACTION.DELETE,
                'File/Stop',
                'File/Start'
            ],
            stopped:false,
            resize:function(){
                const parent = this.getParent();

                const thiz = this;
                const toolbar = this.getToolbar();
                let noToolbar = false;
                const topOffset = 0;
                const aceNode = $(this.aceNode);


                if(!toolbar ||  (toolbar && toolbar.isEmpty())){
                    noToolbar = true;
                }else{
                    if(toolbar) {
                        toolbar.resize();
                    }
                }

                const totalHeight = $(thiz.domNode).height();
                const topHeight = noToolbar==true ? 0 : $(thiz.header).height();
                const footerHeight = 0;
                let finalHeight = totalHeight - topHeight - footerHeight;

                if(toolbar){
                    finalHeight-=4;
                }
                if (finalHeight > 50) {
                    $(this.logNode).height(finalHeight + 'px');
                    /*
                    $(this.lologNode).css({
                        top:30
                    })*/
                } else {

                }
            },
            runAction: function (action) {
                action = this.getAction(action);
                if (!action) {
                    return false;
                }

                const self = this;
                const command = action.command;
                const ACTION = types.ACTION;
                const result = false;

                if(command === ACTION.DELETE){
                    $(this.logView).empty();
                }

                if(command === 'File/Stop'){
                    this.stopped=true;
                }
                if(command === 'File/Start'){
                    this.stopped=false;
                }
            },
            getSelection:function(){
              return null;
            },
            getLoggingActions: function (permissions) {
                const actions = [];
                const self = this;
                const ACTION = types.ACTION;
                const ICON = types.ACTION_ICON;

                actions.push(this.createAction({
                    label: 'Delete',
                    command: 'File/Delete',
                    icon: ACTION_ICON.DELETE,
                    tab: 'Home',
                    group: 'Organize',
                    mixin:{
                        quick:true
                    }
                }));
                actions.push(this.createAction({
                    label: 'Stop',
                    command: 'File/Stop',
                    icon: 'fa-stop',
                    tab: 'Home',
                    group: 'Organize',
                    mixin:{
                        quick:true
                    }
                }));
                actions.push(this.createAction({
                    label: 'Stop',
                    command: 'File/Start',
                    icon: 'fa-play',
                    tab: 'Home',
                    group: 'Organize',
                    mixin:{
                        quick:true
                    }
                }));
                return actions;
            },
            createWidgets:function(bottom,top){                
            },
            getConsole:function(){
                return this.console;
            },
            printTemplate:'<span style="display:block;margin-bottom: 6px" class="widget border-top-dark"># ${time} - ${command}<br/>\t ${result}</span><br/>',
            printCommand:function(command,result,template,addTime){
                const where = this.logView;
                const time = addTime!==false ? moment().format("HH:mm:SSS") : "";

                const content = utils.substituteString(template || this.printTemplate,{
                    command:command,
                    result:result,
                    time:time
                });
                const node = $(content);
                where.appendChild(node[0]);
                node[0].scrollIntoViewIfNeeded();

                return node;
            },
            log: function (msg,split,markHex) {

                const printTemplate = '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>';
                let out = '';
                

                if (_.isString(msg)) {
                    if(split!==false) {
                        out += msg.replace(/\n/g, '<br/>');
                    }else{
                        out = "" + msg;
                        if(markHex) {
                            out = utils.markHex(out, '<span class="">','</span>');
                        }
                    }
                } else if (_.isObject(msg) || _.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                }

                //var items = out.split('<br/>');
                const items = [out];

                for (let i = 0; i < items.length; i++) {
                    this.printCommand(items[i],'',this.logTemplate,true);
                }

                /*
                this.searchInput.hideseek({
                    list: this.$logView,
                    highlight:true
                });
                */
            },
            getDeviceString:function(device,deviceInfo,suffix){

                let out = (device.scope ==='user_devices' ? "User Device - " : "System Device - ");
                out+= "<b>" + device.name +"</b>" + " - " + deviceInfo.host + ':'+deviceInfo.port+'@'+deviceInfo.protocol + '<b><span class="text-default">' +(suffix || "") +'</b></span>';
                return out;
            },
            print:function(){


            },
            _lastDeviceString:'',
            onDeviceMessageExt:function(evt){
                const messages= evt.messages.length ? evt.messages : [evt.raw];

                const self = this;
                const device = evt.device;
                const deviceInfo = evt.deviceInfo;
                const OUTPUT = types.LOG_OUTPUT;


                if(!this.hasFlag(deviceInfo,OUTPUT.RESPONSE)){
                    return;
                }

                const deviceString = this.getDeviceString(device,deviceInfo," Message : ");
                _.each(messages,function (message) {
                    if(message.length) {
                        const string = '<span class="text-info">' + deviceString + ' ' + message;
                        self.log(string, false, true);
                    }
                })
            },
            hasFlag:function(deviceInfo,output){
                const ctx = this.ctx;
                const deviceManager = ctx.getDeviceManager();
                const device = deviceManager.getDeviceStoreItem(deviceInfo);

                if(!device){
                    return;
                }

                deviceInfo = deviceManager.toDeviceControlInfo(device);

                if(!deviceInfo){
                    return;
                }


                const LOGGING_FLAGS = types.LOGGING_FLAGS;
                const OUTPUT = types.LOG_OUTPUT;
                let flags = deviceInfo.loggingFlags;
                flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};

                const flag = flags[output];

                if(flag ==null){
                    return false;
                }

                if(!(flag & LOGGING_FLAGS.GLOBAL_CONSOLE)) {
                    return false;
                }

                return true;
            },
            onDeviceCommand:function(evt){
                const LOGGING_FLAGS = types.LOGGING_FLAGS;
                const OUTPUT = types.LOG_OUTPUT;


                const self = this;
                const device = evt.device;
                const deviceInfo = evt.deviceInfo;
                let flags = deviceInfo.loggingFlags;
                flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};

                if(!this.hasFlag(deviceInfo,OUTPUT.SEND_COMMAND)){
                    return;
                }

                const deviceString = this.getDeviceString(device,deviceInfo," Command: ") + "<span class='text-default'>\"" + evt.name + "\"</span> ";
                const string = '<span class="text-info">' + deviceString  + '</span><span>'  +  '<span class="text-warning">' + evt.command +  '</span>';

                self.log(string,false,true);
            },
            extract:function(evt){
                const ctx = this.ctx;
                const deviceManager = ctx.getDeviceManager();

                const device = evt.device && evt.device.info ? evt.device : deviceManager.getDeviceStoreItem(evt.device);
                const deviceInfo = device && device.info ? device.info : deviceManager.toDeviceControlInfo(device);
                return {
                    device:device,
                    info:deviceInfo
                }
            },
            onDeviceConnected:function(evt){
                const data = this.extract(evt);
                const OUTPUT = types.LOG_OUTPUT;


                if(!data.device){
                    return false;
                }
                if(evt.isReplay===true && data.device.state ===types.DEVICE_STATE.READY){
                    return false;
                }

                if(!this.hasFlag(data.info,OUTPUT.DEVICE_CONNECTED)){
                    return;
                }


                const deviceString = this.getDeviceString(data.device,data.info," has been connected");
                const string = '<span class="text-warning">' + deviceString  + '</span>';
                this.log(string,false,true);
            },
            onDeviceDisconnected:function(evt){
                const data = this.extract(evt);
                const OUTPUT = types.LOG_OUTPUT;

                if(!data.device){
                    return false;
                }
                if(!this.hasFlag(data.info,OUTPUT.DEVICE_DISCONNECTED)){
                    return;
                }
                const deviceString = this.getDeviceString(data.device,data.info," has been disconnected");

                const string = '<span class="text-warning">' + deviceString  + '</span></span>';

                this.log(string,false,true);
            },
            startup:function(){

                if (this.permissions) {
                    let _defaultActions = [];//DefaultActions.getDefaultActions(this.permissions, this, this);
                    _defaultActions = _defaultActions.concat(this.getLoggingActions(this.permissions));
                    this.addActions(_defaultActions);
                }
                this.showToolbar(true);

                this.getToolbar().setActionEmitter(this);

                this.subscribe(types.EVENTS.ON_DEVICE_MESSAGE_EXT);
                this.subscribe(types.EVENTS.ON_DEVICE_COMMAND);
                this.subscribe(types.EVENTS.ON_DEVICE_CONNECTED);
                this.subscribe(types.EVENTS.ON_DEVICE_DISCONNECTED);
                this.subscribe(types.EVENTS.ON_DEVICE_COMMAND);
                this.showToolbar(true);

                utils.resizeTo(this.getToolbar(),this.header,true,false);
                this.getToolbar().resize();


                const toolbar = this.getToolbar();

                /*
                toolbar.$navBar.append('<input name="search" placeholder="Start typing here" class="searchInput" type="text">');

                var input = toolbar.$navBar.find('.searchInput');
                this.searchInput = $(input);
                $(input).hideseek({
                    list: this.$logView,
                    highlight:true
                });*/

            }
        });
        return _Console;
    }

    function doTests(tab, type,value) {

        const console_ = createConsoleClass();
        const _console = utils.addWidget(console_,{
            ctx:ctx,
            type:type,
            value:value || 'return 2+2;'
        },null,tab,true);

        tab.add(_console,null,true);

    }

    var ctx = window.sctx;
    var ACTION = types.ACTION;
    let root;
    const _doTests=true;


    const _actions = [
        ACTION.RENAME
    ];




    if (ctx && _doTests) {

        const tab = TestUtils.createTab('Test', null, 'test');

        doTests(tab, 'javascript','return 2 + 2');

        return declare('a', null, {});

    }

    return Grid;
});