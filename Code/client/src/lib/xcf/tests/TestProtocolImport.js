/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'dojo/_base/lang',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/Grid',
    "xide/tests/TestUtils",
    "module",
    "dojo/promise/all",
    'xide/Wizards',
    'dojo/Stateful',
    'xide/views/_Dialog',
    'xide/wizard/WizardBase',
    'dojo/dom-class'

], function (dcl,declare,lang,types,
             utils, factory,Grid, TestUtils,module,
             all,Wizards,Stateful,
             Dialog,WizardBase,domClass
) {
    console.clear();

    console.log('--do-tests');

    const Wizard=

    WizardBase = dcl([Wizard], {

        declaredClass:"xide.wizard.WizardBase",
        context:null,
        nextButtonLabel:"",
        previousButtonLabel:"",
        cancelButtonLabel:"",
        doneButtonLabel:"Done",
        cancelFunction:null,
        doneButton:null,
        hideDisabled:false,
        showProgressIndicator:function(dstNode,show)
        {
            let progDiv = dojo.byId("progressIndicator");
            if(progDiv){
                xapp.utils.destroy(progDiv);
                progDiv=null;
                if(!show)
                    return;
            }

            if(!progDiv && show){
                progDiv = dojo.create("DIV",{
                    id:"progressIndicator",
                    className:"progressIndicator"
                },dstNode);

                const loaderGif = dojo.create("IMG",{
                    src:"images/ajax-loader.gif"
                },progDiv);
            }
        },
        __postMixInProperties:function () {

            this.inherited(arguments);

            const ctxo = sctx;

            if(ctxo && ctxo.getLocals)
            {
                this._messages = ctxo.getLocals("dojox.widget", "Wizard");
            }else{
                this._messages=[];
            }
        },
        startup:function () {
            this.inherited(arguments);
        },
        resize:function () {
            this.inherited(arguments);
        },
        __checkButtons:function () {

            const sw = this.selectedChildWidget;
            if(!sw){
                return;
            }
            const lastStep = sw.isLastChild;
            //this.nextButton.set("disabled", lastStep);
            this._setButtonClass(this.nextButton);
            if (sw.doneFunction) {
                this.doneButton.domNode.style.display = "";
                if (lastStep) {
                    this.nextButton.domNode.style.display = "none";
                }
            } else {
                // #1438 issue here.
                //this.doneButton.domNode.style.display = "none";
            }
            this.previousButton.set("disabled", !this.selectedChildWidget.canGoBack);
            this._setButtonClass(this.previousButton);
        },
        __setButtonClass:function (button) {
            button.domNode.style.display = (this.hideDisabled && button.disabled) ? "none" : "";
        },

        __forward:function () {
            if (this.selectedChildWidget._checkPass()) {
                this.forward();
            }
        },

        done:function () {
            this.selectedChildWidget.done();
        },

        destroy:function () {
            this.inherited(arguments);
        }
    });

    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;
    const driverId = '235eb680-cb87-11e3-9c1a-0800200c9a66';


    function createCISWizard(title,show,wizardMixin){

        const dlg = new Dialog({
            cssClass:'WizardDialog ui-state-default CIDialog noPadding',
            style:"height:350px; width:400px",//important
            title:title,
            resizable:true,
            fitContent:true,
            getCSSNode:function(){
                return this.domNode;
            }
        });


        dlg.resizable=true;

        domClass.add(dlg.domNode,'CIDialog');
        domClass.add(dlg.domNode,'noPadding');

        dlg.startup();


        /*
         var wizard = new WizardBase({
         style:"height:350px; width:400px",//important
         className:"widgetContainer widgetBorder",
         hideDisabled:false
         },dlg.containerNode);*/
        //wizardMixin
        const wizardArgs = {
            style:"height:inherit; width:inherit",//important
            /*className:"widgetContainer widgetBorder",*/
            hideDisabled:false
        };
        if(wizardMixin){
            lang.mixin(wizardArgs,wizardMixin);
        }

        const wizard = utils.addWidget(WizardBase,wizardArgs,this,dlg.containerNode,true);



        //dlg.content = wizard;

        wizard.startup();

        if(show==true) {
            dlg.show();
        }

        return {
            wizard:wizard,
            dialog:dlg
        }
    }

    const mixin = declare('ProtocolMixin',[Stateful],{

        __createProtocolWizard: function () {


            /**
             * tasks
             *
             * 1. wizard-page- one: read-only Protocol-tree-viewer (as picker)
             *
             * 1.1
             *
             *
             */
            try {
                var thiz = this;
                if (this._lastWizard) {
                    this._lastWizard.dialog.hide().then(function () {
                        thiz._lastWizard.dialog.destroy();
                    });
                }
            } catch (e) {

            }
            /**
             * setup
             *
             * 1. create the dialog,
             * 2. create wizard
             * 3. add panes
             *
             *
             */
            const wizardStruct = Wizards.createWizard('Select Protocol', false,{});
            this._lastWizard = wizardStruct;
            const wizard = wizardStruct.wizard;
            const dialog = wizardStruct.dialog;
            /*
             var CIS = [
             utils.createCI('Title', 13, ''),
             utils.createCI('Scope', 3, 'system_drivers', {
             "options": [
             {
             label: 'System',
             value: 'system_drivers'
             },
             {
             label: 'User',
             value: 'user_drivers'
             },
             {
             label: 'App',
             value: 'app_drivers'
             }
             ]
             })
             ];

             var ciView = utils.addWidget(CIView, {
             viewStyle: 'padding:0px;',
             options: {
             groupOrder: {
             'General': 0,
             'Advanced': 1,
             'Description': 2
             }
             },
             cis: CIS
             }, this, wizard, true, null, [WizardPaneBase]);

             ciView.initWithCIS(CIS);
             */

            const protocolMgr = thiz.ctx.getProtocolManager();
            let selectView = null;
            let selectItemsView = null;
            let selectedItems = [], selectedItem = null;

            const store = protocolMgr.getStore();

            const done = function(){
                wizard.destroy();
                dialog.destroy();
            };



            dialog.show().then(function () {

                /**
                 * First protocol view, select item
                 */
                selectView = utils.addWidget(ProtocolTreeView, {
                    title: 'Protocols',
                    store: protocolMgr.getStore(),
                    delegate: protocolMgr,
                    beanContextName:'3',// thiz.ctx.mainView.beanContextName
                    _doneFunction:function(){
                        return false;
                    },
                    passFunction:function(){
                        return true;
                    },
                    canGoBack:true,
                    gridParams:{
                        showHeader:true
                    }

                }, protocolMgr, wizard, true,null,[WizardPaneBase]).
                _on(types.EVENTS.ON_ITEM_SELECTED, function(evt){
                    selectedItem = evt.item;
                    wizard.adjustWizardButton('next',selectView.isGroup(evt.item));
                });

                setTimeout(function(){
                    selectView.grid.set('collection',store.filter(selectView.getRootFilter()));
                    selectView.grid.refresh();
                },200);


                /**
                 * Select commands/variables view
                 */
                /**
                 * First protocol view, select item
                 */
                selectItemsView = utils.addWidget(ProtocolTreeView, {
                    title: 'Protocols',
                    store: protocolMgr.getStore(),
                    delegate: protocolMgr,
                    beanContextName:'3',// thiz.ctx.mainView.beanContextName
                    doneFunction:function(){
                        done();
                        return true;
                    },
                    passFunction:function(){
                        //not used
                        return true;
                    },
                    canGoBack:true,
                    getRootFilter:function(){
                        if(selectedItem) {
                            return {
                                parentId: selectedItem.path
                            }
                        }else{
                            return {
                                parentId: ''
                            }
                        }
                    },
                    canSelect:function(item){
                        if(item.virtual===true && item.isDir===false){
                            return true;
                        }
                        return false;
                    }
                }, protocolMgr, wizard, true,null,[WizardPaneBase],null,{
                    getColumns:function(){

                        const _res = this.inherited(arguments);
                        _res.push({
                            label: "Value",
                            field: "value",
                            sortable: false,
                            _formatter: function (name, item) {
                                if (!thiz.isGroup(item) && item['user']) {
                                    const meta = item['user'].meta;
                                    const _in = meta ? utils.getCIInputValueByName(meta, types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE) : null;
                                    if (meta) {
                                        return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                                    } else {
                                        return item.name;
                                    }
                                    return _in;
                                } else {
                                    return item.name;
                                }
                            }
                        });
                        return _res;
                    }
                }).
                _on(types.EVENTS.ON_ITEM_SELECTED, function(evt){
                    selectedItems = selectItemsView.getSelection();
                    wizard.adjustWizardButton('done',selectedItems.length==0);

                });

                setTimeout(function(){
                    if(selectItemsView.grid) {
                        selectItemsView.grid.set('collection', store.filter(selectItemsView.getRootFilter()));
                        selectItemsView.grid.refresh();
                    }
                },200);
                wizard.onNext = function(){
                    dialog.set('title','Select Commands & Variables');
                    selectItemsView.grid.set('collection',store.filter(selectItemsView.getRootFilter()));
                    selectItemsView.grid.refresh();
                }
            });
        },
        __createWizard: function () {

            return;

            try {
                const thiz = this;
                if (this._lastWizard) {
                    this._lastWizard.dialog.hide().then(function () {
                        thiz._lastWizard.dialog.destroy();
                    });
                }
            } catch (e) {
                console.error('');
            }

            /**
             * setup
             *
             * 1. create the dialog,
             * 2. create wizard
             * 3. add panes
             *
             *
             */
            const wizardStruct = Wizards.createWizard('Select', false);
            this._lastWizard = wizardStruct;

            const wizard = wizardStruct.wizard;
            const dialog = wizardStruct.dialog;


            const CIS = [
                utils.createCI('Title', 13, ''),
                utils.createCI('Scope', 3, 'system_drivers', {
                    "options": [
                        {
                            label: 'System',
                            value: 'system_drivers'
                        },
                        {
                            label: 'User',
                            value: 'user_drivers'
                        },
                        {
                            label: 'App',
                            value: 'app_drivers'
                        }
                    ]
                })
            ];



            const ciView = utils.addWidget(CIView, {
                viewStyle: 'padding:0px;',
                options: {
                    groupOrder: {
                        'General': 0,
                        'Advanced': 1,
                        'Description': 2
                    }
                },
                cis: CIS
            }, this, wizard, true, null, [WizardPaneBase]);
            ciView.initWithCIS(CIS);



            dialog.show();






        },
        createProtocolWizard: function (item) {
            const thiz = this;
            let selectView = null;
            let selectItemsView = null;
            let selectedItems = [];
            let selectedItem = null;
            const driver = item || thiz.getSelectedItem();
            const protocolMgr = thiz.ctx.getProtocolManager();
            const store = protocolMgr.getStore();

            const wizardStruct = createCISWizard('Select Protocol', false, {});

            this._lastWizard = wizardStruct;
            const wizard = wizardStruct.wizard;
            const dialog = wizardStruct.dialog;


            const done = function () {
                wizard.destroy();
                dialog.destroy();
                thiz.importProtocol(selectedItem, selectedItems, driver);
            };


            dialog.show().then(function () {

                /**
                 * First protocol view, select item
                 */
                selectView = utils.addWidget(ProtocolTreeView, {
                    title: 'Protocols',
                    store: protocolMgr.getStore(),
                    delegate: protocolMgr,
                    beanContextName: '3',// thiz.ctx.mainView.beanContextName
                    _doneFunction: function () {
                        return false;
                    },
                    passFunction: function () {
                        return true;
                    },
                    canGoBack: true,
                    gridParams: {
                        showHeader: true
                    },
                    editItem: function () {

                        if (!this.isGroup(selectedItem)) {
                            wizard._forward();
                        }
                    }

                }, protocolMgr, wizard, true, null, [WizardPaneBase]).
                _on(types.EVENTS.ON_ITEM_SELECTED, function (evt) {
                    selectedItem = evt.item;
                    wizard.adjustWizardButton('next', selectView.isGroup(evt.item));

                });


                /**
                 * Select commands/variables view
                 */
                /**
                 * First protocol view, select item
                 */
                selectItemsView = utils.addWidget(ProtocolTreeView, {
                    title: 'Protocols',
                    store: protocolMgr.getStore(),
                    delegate: protocolMgr,
                    beanContextName: '3',// thiz.ctx.mainView.beanContextName
                    doneFunction: function () {
                        done();
                        return true;
                    },
                    passFunction: function () {
                        //not used
                        return true;
                    },
                    canGoBack: true,
                    editItem: function () {

                    },
                    getRootFilter: function () {
                        if (selectedItem) {
                            return {
                                parentId: selectedItem.path
                            }
                        } else {
                            return {
                                parentId: ''
                            }
                        }
                    },
                    canSelect: function (item) {
                        if (item.virtual === true && item.isDir === false) {
                            return true;
                        }
                        return false;
                    }
                }, protocolMgr, wizard, true, null, [WizardPaneBase], null, {
                    getColumns: function () {

                        const _res = this.inherited(arguments);
                        _res.push({
                            label: "Value",
                            field: "value",
                            sortable: false,
                            _formatter: function (name, item) {

                                if (!thiz.isGroup(item) && item['user']) {
                                    const meta = item['user'].meta;
                                    const _in = meta ? utils.getCIInputValueByName(meta, types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE) : null;
                                    if (meta) {
                                        return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                                    } else {
                                        return item.name;
                                    }
                                    return _in;
                                } else {
                                    return item.name;
                                }
                            }
                        });
                        return _res;
                    }
                }).
                _on(types.EVENTS.ON_ITEM_SELECTED, function (evt) {
                    selectedItems = selectItemsView.getSelection();
                    wizard.adjustWizardButton('done', selectedItems.length == 0);

                });


                wizard.onNext = function () {
                    dialog.set('title', 'Select Commands & Variables');
                    selectItemsView.grid.set('collection', store.filter(selectItemsView.getRootFilter()));
                    selectItemsView.grid.refresh();
                }
            });
        }


    });


    function doCopyTest(driver,parent){


        console.dir(driver);


        const fileManager = ctx.getFileManager();

        const driverManager = ctx.getDriverManager();


        if(!parent){
            parent = driver.getParent();
        }

        if(!parent){
            console.error('copy error: have no destination');
        }

        const thiz = this;

        const title = driver.getMetaValue(types.DRIVER_PROPERTY.CF_DRIVER_NAME);

        const id = driver.getMetaValue(types.DRIVER_PROPERTY.CF_DRIVER_ID);
        const jsPath = driver.getMetaValue(types.DRIVER_PROPERTY.CF_DRIVER_CLASS);
        const path = driver.path;
        const pathParts = utils.parse_url('file:///'+path,'path');
        const pathInfo = utils.pathinfo(path,types.PATH_PARTS.ALL);
        const ext = pathInfo.extension;
        const fileName  = pathInfo.basename;
        const store = driver._store;



        const driverScope = driver.scope;

        const targetScope = parent.scope;
        const targetStore = parent._store;

        const blockScope = driver.blockScope;

        let driverMeta = null;
        let driverCode = null;

        //getContent: function (mount, path, readyCB, emit) {

        const dfds = [];


        //////////////////////////////
        //
        //  new vars
        //

        const parentParts = utils.pathinfo(parent.path);

        const titleNew = title + '-Copy';
        const idNew = utils.createUUID();
        const jsFileParts = utils.pathinfo(jsPath);
        const jsPathNew = parentParts.dirname + '/' + parentParts.basename + '/' + titleNew + '.js';
        const pathNew = titleNew + '.meta.json';




        console.log('js parts: ',jsPathNew);






        //pick meta content
        dfds.push(fileManager.getContent(driverScope,path,function(meta){
            driverMeta = utils.getJson(meta);
        }));

        dfds.push(fileManager.getContent(driverScope,jsPath,function(code){
            driverCode = code;
        }));





        function doCopy(){

            //update title CI
            utils.setCIValue(driverMeta.inputs,types.DRIVER_PROPERTY.CF_DRIVER_NAME,titleNew);
            utils.setCIValue(driverMeta.inputs,types.DRIVER_PROPERTY.CF_DRIVER_ID,idNew);
            utils.setCIValue(driverMeta.inputs,types.DRIVER_PROPERTY.CF_DRIVER_CLASS,jsPathNew);

            const met = driverMeta;


            const newItemDfd = driverManager.createItem(targetScope,parentParts.basename,titleNew,JSON.stringify(driverMeta,null,2),driverCode);

            newItemDfd.then(function(data){

                console.error('create driver item : ',data);



                let newItem = driverManager.createNewItem(titleNew, targetScope, parent.path);
                newItem.path += '.meta.json';
                newItem.user = driverMeta;
                newItem.id = idNew;

                newItem.blox = {
                    blocks:blockScope.blocksToJson(),
                    variables:[]
                }



                newItem = store.putSync(newItem);
                driverManager.onDriverCreated(store);

                driverManager.completeDriver(store, newItem, newItem);

                factory.publish(types.EVENTS.ON_STORE_CHANGED, {
                    owner: thiz,
                    store: store,
                    action: types.NEW_FILE,
                    item: newItem
                });

                targetStore.refreshItem(parent);
            });


        }


        all(dfds).then(function(){
            doCopy();
        });

        console.log('copy driver =' + title + ' id =' + id + ' path = ' + path + '|| ' + jsPath,pathInfo);
        ///////////////////////////////////////////////////////////
        //
        //  Copy options
        //
        const cloneJSDriver = true;
        const cloneBloxDriver = true;
    }

    function doTests(tab){


        const driverManager = ctx.getDriverManager();
        const marantz  = driverManager.getDriverById("235eb680-cb87-11e3-9c1a-0800200c9a66");


        const theMixin = new mixin({
            delegate:driverManager,
            ctx:ctx
        });

        theMixin.createProtocolWizard(marantz);




        return;
    }


    var ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;


    const _actions = [
        ACTION.RENAME
    ];

    if (ctx) {

        const parent = TestUtils.createTab(null,null,module.id);

        doTests(parent);
        return declare('a',null,{});
    }
    return Grid;
});