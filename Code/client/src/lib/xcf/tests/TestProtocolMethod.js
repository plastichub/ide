/** @module xgrid/Base **/
define([
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xgrid/Grid',
    "xide/tests/TestUtils",
    "module",

    "dojo/promise/all"

], function (declare,types,
             utils, factory,Grid, TestUtils,module,
             all
) {
    console.clear();


    console.log('--do-tests');

    const ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;
    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let ribbon;
    let CIS;
    const driverId = '235eb680-cb87-11e3-9c1a-0800200c9a66';


    function doTests(tab){

        console.clear();
        if (ctx) {
            const blockManager = ctx.getBlockManager();
            const driverManager = ctx.getDriverManager();
            const deviceManager = ctx.getDeviceManager();
            deviceManager.protocolMethod(types.PROTOCOL.SERIAL,'ls',['query','*']);
        }
    }

    const _actions = [
        ACTION.RENAME
    ];

    if (ctx) {
        doTests(null);
        return declare('a',null,{});

    }

    return Grid;
});