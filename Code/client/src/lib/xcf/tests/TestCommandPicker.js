define([
    'dcl/dcl',
    'dojo/_base/lang',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xide/tests/TestUtils',
    'module',

    'xide/views/CIGroupedSettingsView',
    'xide/widgets/WidgetBase',
    'dojo/when',
    'dojo/Deferred',
    "xide/views/_Dialog",
    "xcf/views/DeviceTreeView"

], function (dcl,lang, utils, types,factory,
             TestUtils,module,
             CIGroupedSettingsView,WidgetBase,when,Deferred,_Dialog,DeviceTreeView) {
    const ACTION = types.ACTION;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const ctx = window.sctx;
    const test = true;
    let marantz;
    var marantzInstance;

    const DefaultPermissions = DeviceTreeView.DefaultPermissions;






    /**
     * Url generator for device/driver/[command|block|variable]
     *
     * @param device
     * @param driver
     * @param block
     * @param prefix
     * @returns {*}
     */
    function toUrl(device,driver,block,prefix){

        prefix = prefix || '';

        const pattern = prefix + "deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";

        const url = lang.replace(
            pattern,
            {
                deviceId:device.id,
                deviceScope:device.scope,
                driverId:driver.id,
                driverScope:driver.scope,
                block:block.id
            });
        return url;
    }

    /**
     * Filter function to reject selections in a device - tree - view
     * @param item
     * @param acceptCommand
     * @param acceptVariable
     * @param acceptDevice
     * @returns {*}
     */
    function accept(item,acceptCommand,acceptVariable,acceptDevice){
        const reference = item.ref || {};
        const block       =       reference.item;
        const device      =       reference.device;
        const driver      =       reference.driver;
        const scope       =       block ? block.getScope() : null;
        const isCommand   =       block ? block.declaredClass.indexOf('model.Command')!==-1 : false;
        const isVariable  =       block ? block.declaredClass.indexOf('model.Variable')!==-1 : false;

        isCommand && console.log('have command ' + block.name);

        if(isCommand && acceptCommand){
            return toUrl(device,driver,block,'command://');
        }
        if(isVariable && acceptVariable){
            return toUrl(device,driver,block,'variable://');
        }

        return false;
    }
    /**
     * Renders a CIS inline
     * @param CIS
     * @param where
     * @param owner
     * @returns {*}
     */
    function renderCIS(CIS,where,owner){

        const widgetsHead = factory.createWidgetsFromArray(CIS, owner, null, false);
        const result = [];
        const dfd = new Deferred();

        when(widgetsHead,function(widgets){

            if (widgets) {


                for (let i = 0; i < widgets.length; i++) {

                    const widget = widgets[i];

                    widget.delegate = owner;

                    where.appendChild(widget.domNode);

                    if(where && where.lazy===true) {
                        widget._startOnShow = true;
                    }else{
                        widget.startup();
                    }


                    widget._on('valueChanged',function(evt){
                        //owner.onValueChanged(evt);
                    });

                    owner._emit('widget',{
                        widget:widget,
                        ci:widget.userData
                    })


                    result.push(widget);

                    widget.userData.view=owner;
                    widget.onAttached && widget.onAttached(where);

                    if(owner && owner.add && owner.add(widget,null,false)){

                    }else{
                        console.error('view has no add',owner);
                        owner.add(widget,null,false);
                    }
                }

                dfd.resolve(result);

            }
        });

        return dfd;

    }


    /**
     * Create a simple CIS
     * @returns {{inputs: *[]}}
     */
    function createPickerCIS(instance){



        const CIS = {
            "inputs": [
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General9',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Command",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("86025507-97e0-99a4-0640-59c5725ef116"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'command'
                },
                {
                    "chainType": 0,
                    "class": "cmx.types.ConfigurableInformation",
                    "dataRef": "",
                    "dataSource": "",
                    "description": null,
                    "enabled": true,
                    "enumType": "-1",
                    "flags": -1,
                    "group": 'General1',
                    "id": "CF_DRIVER_ID2",
                    "name": "CF_DRIVER_ID2",
                    "order": 1,
                    "params": null,
                    "parentId": "myeventsapp108",
                    "platform": null,
                    "storeDestination": "metaDataStore",
                    "title": "Id",
                    "type": 'Command',
                    block:instance.driver.blockScope.blockStore.getSync("6ab97609-4b3a-4f65-d018-1027d402a94d"),
                    "uid": "-1",
                    "value": "235eb680-cb87-11e3-9c1a-0800200c9a66",
                    "visible": true,
                    "pickerType":'variable'
                }
            ]
        };
        _.each(CIS.inputs,function(ci){
            ci.driver = instance.driver,
                ci.device = instance.device
        });
        return CIS;
    }

    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    /**
     * Simple test of the device tree view
     * @param grid
     * @param accept
     */
    function createDeviceView(parent,ctx,driver,device,type) {

        const grid = utils.addWidget(DeviceTreeView, {
            title: 'Devices',
            collection: ctx.getDeviceManager().getStore(),
            delegate: ctx.getDeviceManager(),
            blockManager: ctx.getBlockManager(),
            ctx: ctx,
            icon: 'fa-sliders',
            showHeader: false,
            scope: 'user_devices',
            resizeToParent: true
        }, null, parent, true);

        grid._on('selectionChanged',function(evt){
            const selection = evt.selection;
            const item = selection && selection.length==1 ? selection[0] : null;

            if(!item){
                return;
            }

            const isAccepted = accept(item,type=='command',type=='variable',type=='device');
            isAccepted  && console.log('-selected : ' + isAccepted,item);
            if(isAccepted){
                grid._selected = isAccepted;
            }
        });

        return grid;
    }

    function createPickerWidget(device,driver){


        const Module = dcl(WidgetBase, {
            declaredClass: "xcf.widgets.CommandPickerWidget",
            minHeight: "400px;",
            value: "",
            options: null,
            title: 'Command',
            templateString: "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
            "<table border='0' cellpadding='5px' width='100%' >" +
            "<tbody>" +
            "<tr attachTo='extensionRoot'>" +
            "<td width='15%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
            "<td width='100px' class='widgetValue2' valign='middle' attachTo='valueNode'></td>" +
            "<td class='extension' width='25px' attachTo='button0'></td>" +
            "<td class='extension' width='25px' attachTo='button1'></td>" +
            "<td class='extension' width='25px' attachTo='button2'></td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
            "<div attachTo='last'></div>" +
            "</div>",
            onValueChanged:function(value){
                value = value.newValue || value;
                this.setValue(value);

                const ci = this.userData;
                const block = ci.block;
                const blockScope = block.scope;

                const title = blockScope.toFriendlyName(block, value);

                /*
                 this.commandWidget._renderItem({
                 value:value,
                 label:title
                 });*/

                this.commandWidget.set('value',value,title);
            },
            onSelect: function (ctx,driver,device) {
                const thiz = this;
                const _defaultOptions = {};
                const ci = this.userData;
                utils.mixin(_defaultOptions, this.options);

                try {
                    const value = utils.toString(this.userData['value']);

                    const dlg = new _Dialog({
                        size: types.DIALOG_SIZE.SIZE_SMALL,
                        title: '%%Select a device command',
                        bodyCSS: {
                            'height': 'auto',
                            'min-height': '400px',
                            'padding': '8px',
                            'margin-right': '16px'
                        },
                        picker: null,
                        onOk: function () {
                            const selected = this.picker._selected;
                            selected && thiz.onValueChanged(selected);
                        },
                        message: function (dlg) {
                            const thiz = dlg.owner;
                            const picker = createDeviceView($('<div class="container" style="height: inherit;" />'),ctx,driver,device,ci.pickerType);
                            thiz.picker = picker;
                            thiz.add(picker, null, false);
                            return $(picker.domNode);
                        },
                        onShow: function (dlg) {
                            const picker = this.picker;
                            const self = this;
                            picker.startup();
                            this.startDfd.resolve();
                        }
                    });

                    dlg.show().then(function () {
                        dlg.resize();
                        utils.resizeTo(dlg.picker, dlg, true, true);
                    });
                    this.add(dlg,null,false);
                } catch (e) {
                    logError(e);
                }
            },
            postMixInProperties: function () {
                this.inherited(arguments);
                if (this.userData && this.userData.title) {
                    this.title = this.userData.title;
                }
                if ((this.userData && this.userData.vertical === true) || this.vertical === true) {

                    this.templateString = "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
                        "<table border='0' cellpadding='5px' width='100%' >" +
                        "<tbody>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='100%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
                        "</tr>" +
                        "<tr attachTo='extensionRoot'>" +
                        "<td width='50%' class='widgetValue2' valign='middle' attachTo='valueNode'></td>" +
                        "<td class='extension' width='25px' attachTo='button0'></td>" +
                        "<td class='extension' width='25px' attachTo='button1'></td>" +
                        "<td class='extension' width='25px' attachTo='button2'></td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
                        "<div attachTo='last'></div>" +
                        "</div>"
                }
            },
            fillTemplate: function () {
                const thiz = this;
                const value = utils.toString(this.userData['value']) || '';
                const ci = this.userData;
                const block = ci.block;
                const blockScope = block.scope;
                const ctx = blockScope.ctx;
                const pickerType = ci.pickerType;
                const driver = blockScope.driver;
                const device = blockScope.device;
                const deviceManager = ctx.getDeviceManager();
                const driverManager = ctx.getDriverManager();

                //add the Select Widget for local commands
                const CIS = [];

                const isCommand = pickerType == 'command';

                let title = 'Command';


                if(value.indexOf('://')){
                    title = blockScope.toFriendlyName(block,value);
                }


                CIS.push(utils.createCI('value', 3, 'Volume', {
                    group: 'General',
                    title: title,
                    value:value,
                    //dst: 'command',
                    //options: blockScope.getCommandsAsOptions(),
                    widget: {
                        store:blockScope.blockStore,
                        labelField:'name',
                        valueField:'id',
                        search:true,
                        title:title,
                        query: this.query ? this.query :{
                            group: isCommand ? 'basic' : 'basicVariables'
                        },
                        templateString: '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode">'
                    }
                }));



                factory.renderCIS(CIS, this.valueNode, this).then(function (widgets) {

                    const commandWidget = widgets[0];
                    const btn = factory.createSimpleButton('', 'fa-magic', 'btn-default', {
                        style: ''
                    });
                    thiz.commandWidget = commandWidget;
                    commandWidget._on('onValueChanged',function(e){
                        thiz.onValueChanged(e);
                    });
                    $(btn).click(function () {
                        thiz.onSelect(ctx,driver,device);
                    });
                    $(thiz.button0).append(btn);
                });
            },
            startup: function () {
                try {
                    this.fillTemplate();
                    this.onReady();
                } catch (e) {
                    logError(e);
                }
            }
        });



        return Module;

    }

    function testPicker(instance){
        const toolbar = ctx.mainView.getToolbar();
        const docker = ctx.mainView.getDocker();
        const parent  = TestUtils.createTab(null,null,module.id);
        const cisRenderer = dcl(CIGroupedSettingsView,{
            typeMap:null,
            getTypeMap:function(){
                if (this.typeMap) {
                    return this.typeMap;
                }
                const self = this;
                const typeMap = {};

                typeMap['Command'] = createPickerWidget();
                this.typeMap = typeMap;
                return typeMap;
            }
        });

        const ciView = utils.addWidget(cisRenderer,{
            cis:createPickerCIS(instance).inputs
        },null,parent,true);


        docker.resize();
    }

    if(test && ctx){

        console.clear();

        const parent  = TestUtils.createTab(null,null,module.id);
        const blockManager = ctx.getBlockManager();
        const driverManager = ctx.getDriverManager();
        const deviceManager = ctx.getDeviceManager();
        const grid = null;
        marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");
        var marantzInstance = driverManager.getDriverByPath("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        const driver = marantz.driver;
        const device = marantz.device;
        //debugger;

        //testDeviceView();
        testPicker(marantzInstance);
        //windowManager.registerView(view.grid,false);
    }




    return DeviceTreeView;
});
