/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/Grid',
    'xgrid/Defaults',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    "dojo/Deferred",
    "xide/tests/TestUtils",
    'xide/widgets/_Widget',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'xaction/ActionProvider',
    'xide/widgets/ContextMenu',
    'xide/views/ConsoleView',
    'xide/widgets/WidgetBase',
    "module",
    "xide/data/TreeMemory",
    'dstore/QueryResults',
    'xcf/views/DriverConsole'
], function (dcl,declare, types,
             utils, ListRenderer, Grid, Defaults, Focus,
             OnDemandGrid, EventedMixin, Deferred,TestUtils,_CWidget,_LayoutMixin,Docker,ActionProvider,ContextMenu,ConsoleView,
             WidgetBase,module,TreeMemory,QueryResults,DriverConsole

) {
    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let CIS;
    let basicGridInstance;


    /***
     * playground
     */
    const _lastGrid = window._lastGrid;
    const ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;
    let scope;
    let blockManager;
    let driverManager;
    const doTests = true;

    const DockerBase = dcl([ActionProvider.dcl,_CWidget.dcl],{
        permissions:[
          types.ACTION.RENAME
        ],
        __panel:function(el){
            console.log(el);
            const panels = this.getPanels();

            let frame = null;
            const self = this;
            _.each(this._frameList,function(_frame){
                if($.contains(_frame.$container[0],el)){
                    frame = _frame;
                }
            });




            const id = self.contextMenuPanelId;

            this.contextMenuEventTarget = null;
            this.contextMenuPanelId = null;

            if(frame && id!==null){

                const _panel = frame.panel(id);
                if(_panel){
                    return _panel;
                }
            }
        },
        runAction:function(action,type,event){


            const _panel = this.contextMenuEventTarget ? this.__panel(this.contextMenuEventTarget) : null;
            if(_panel){
                this.removePanel(_panel);
            }

            console.error('run action',_panel);
        },
        getDockerActions:function(){

            const actions = [];

            actions.push(this.createAction({
                label:'Close',
                command:types.ACTION.RENAME,
                tab:'View',
                group:'Misc',
                mixin: {
                    addPermission: true
                },
                icon:'fa-close'
                /*keycombo:['ctrl w','ctrl f4']*/
            }));


            return actions;

        },
        contextMenu:null,
        contextMenuEventTarget:null,
        contextMenuPanelId:null,
        setupActions:function(){

            const self = this;

            this.__on(this._root.$container,'contextmenu', '.wcPanelTab', function(e){
                console.error('context menu');
                self.contextMenuEventTarget  = e.target;
                self.contextMenuPanelId = e.target.parentNode ?  e.target.parentNode.id : null;
            });


            this.addActions(this.getDockerActions());


            const args = {
                owner:this,
                delegate:this,
                limitTo:'wcPanelTab'
            };

            const node = this._root.$container[0];

            const contextMenu = new ContextMenu(args,node);
            contextMenu.openTarget = node;
            contextMenu.init({preventDoubleContext: false});

            //contextMenu._registerActionEmitter(this);
            contextMenu.setActionEmitter(this,types.EVENTS.ON_VIEW_SHOW,this);

            this.contextMenu = contextMenu;


            this.add(contextMenu,null,false);
        },
        __init:dcl.superCall(function(sup){
            return function(){
                if(sup){

                    sup.apply(this, arguments);
                    this.setupActions();
                }
                return 0;
            };
        }),
        __destroy:function(){
            console.error('_destroy');
        }
    });

    const ConsoleViewImplementation = ConsoleView.Implementation;

    const _ExpressionEditor = dcl([WidgetBase,_LayoutMixin.dcl,ConsoleView.HandlerClass],{
        type:'javascript',
        reparent:false,
        consoleTabTitle:'Editor',
        consoleTabOptions:null,
        consoleTabType:'DefaultTab',
        templateString: '<div class="" style="height: inherit">' +
        '</div>',
        onAddEditorActions:function(evt){

            console.error('onAddEditorActions',arguments);
            //this.inherited(arguments);

            const actions  = evt.actions, owner  = evt.owner;

            const mixin = {
                addPermission:true
            };
            actions.push(owner.createAction({
                label: 'Send',
                command: 'Console/Send',
                icon: 'fa-paper-plane',
                group: 'Console',
                tab:'Home',
                mixin:mixin,
                handler:function(){
                    debugger;
                }
            }));

        },
        getConsoleClass:function(){
            return ConsoleView.ConsoleWidget;
        },
        showDocumentation: function (text) {

            dojo.empty(this.helpContainer);
            const help = dojo.create('div', {
                innerHTML: '',
                className: 'ui-widget'
            });
            this.helpContainer.appendChild(help);
            const textText = dojo.create('span', {
                innerHTML: '<pre>'+text +'</pre>',
                className: 'ui-content'
            });
            help.appendChild(textText);
        },
        createTab:function(type,args){

            const DOCKER = types.DOCKER;
            const defaultTabArgs = {
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:DOCKER.TAB.TOP,
                location:DOCKER.DOCK.STACKED

            };
            return this._docker.addTab(type || 'DefaultTab',utils.mixin(defaultTabArgs,args));
        },
        createBranch: function (name, where) {
            const branch = {
                name: name,
                group: 'top',
                children: [],
                value: null
            };
            where.push(branch);
            return branch;

        },
        apiItemToLeaf: function (item) {
            const result = {};
        },
        addAutoCompleterWord: function (text, help) {

            if (!this.autoCompleterWords) {
                this.autoCompleterWords = [];
            }

            this.autoCompleterWords.push({
                word: text,
                value: text,
                meta: help || ''
            });

        },
        createLeaf: function (label, value, parent, help, isFunction, where) {
            const leaf = {
                name: label,
                group: 'leaf',
                value: value,
                parent: parent,
                help: help,
                isFunction: isFunction != null ? isFunction : true
            };

            this.addAutoCompleterWord(value, help);
            if (where) {
                where.children.push({
                    _reference: label
                })
            }
            return leaf;
        },
        getAPIField: function (item, title) {

            for (let i = 0; i < item.sectionHTMLs.length; i++) {
                const obj = item.sectionHTMLs[i];

                if (obj.indexOf("id = \"" + title + "\"") != -1) {
                    return obj;
                }
            }

            return '';
        },
        insertApiItems: function (startString, dst, where) {

            const result = [];
            for (let i = 0; i < this.api.length; i++) {
                const item = this.api[i];

                if (item.title.indexOf(startString) != -1) {
                    /*result.push(item);*/
                    const leafData = this.apiItemToLeaf(item);

                    const title = item.title.replace(startString, '');
                    let description = this.getAPIField(item, 'Description');
                    const parameters = this.getAPIField(item, 'Parameters');
                    const examples = this.getAPIField(item, 'Examples');
                    let syntax = this.getAPIField(item, 'Syntax');

                    description = description + '</br>' + parameters + '<br/>' + examples;
                    syntax = utils.strip_tags(syntax);
                    syntax = syntax.replace('Syntax', '');
                    syntax = utils.replaceAll('\n', '', syntax);
                    syntax = utils.replaceAll('\r', '', syntax);
                    where.push(this.createLeaf(title, syntax, startString.replace('.', ''), description, true, dst));

                }

            }

            return result;
        },
        _createExpressionData: function () {

            const data = {
                items: [],
                identifier: 'name',
                label: 'name'
            };

            const _Array = this.createBranch('Array', data.items);
            this.insertApiItems('Array.', _Array, data.items);

            const _Date = this.createBranch('Date', data.items);
            this.insertApiItems('Date.', _Date, data.items);

            const _Function = this.createBranch('Function', data.items);
            this.insertApiItems('Function.', _Function, data.items);

            const _Math = this.createBranch('Math', data.items);
            this.insertApiItems('Math.', _Math, data.items);

            const _Number = this.createBranch('Number', data.items);
            this.insertApiItems('Number.', _Number, data.items);

            const _Object = this.createBranch('Object', data.items);
            this.insertApiItems('Object.', _Object, data.items);

            const _String = this.createBranch('String', data.items);
            this.insertApiItems('String.', _String, data.items);

            const _RegExp = this.createBranch('RegExp', data.items);
            this.insertApiItems('RegExp.', _RegExp, data.items);

            this.publish(types.EVENTS.ON_EXPRESSION_EDITOR_ADD_FUNCTIONS, {
                root: data,
                widget: this,
                user: this.userData
            });

            console.dir(data);

            return data;
        },
        _createExpressionStore: function () {

            const store = declare('apiStore',[TreeMemory],{
                __fetchRange:function(){
                    // dstore/Memory#fetchRange always uses fetchSync, which we aren't extending,
                    // so we need to extend this as well.
                    console.log('fetchRange : ',this._state);
                    const results = this._fetchRange();
                    return new QueryResults(results.then(function (data) {
                        return data;
                    }), {
                        totalLength: results.then(function (data) {
                            return data.length;
                        })
                    });
                },
                __fetchRange: function (kwArgs) {

                    const deferred = new Deferred();

                    let _res = this.fetchRangeSync(kwArgs);

                    if(this._state.filter){

                        //the parent query
                        if(this._state && this._state.filter && this._state.filter['parentId']) {
                            var _item = this.getSync(this._state.filter.parentId);
                            if (_item) {
                                this.reset();
                                var _items = _item.items;

                                if(_item.getChildren){
                                    _items = _item.getChildren();
                                }

                                deferred.resolve(_items);
                                _res = _items;
                            }
                        }

                        //the group query
                        if(this._state && this._state.filter && this._state.filter['group']) {
                            var _items = this.getSync(this._state.filter.parent);
                            if (_item) {
                                this.reset();
                                _res=_item.items;
                            }
                        }
                    }
                    deferred.resolve(_res);

                    //return _res;
                    return deferred;
                },
                getChildren: function (parent, options) {
                    return this.query({parent: this.getIdentity(parent)});
                },
                mayHaveChildren: function (parent) {
                    return false;
                }
            });


            return new store({
                data: this._createExpressionData().items,
                idProperty:'name'
            });
        },
        createWidgets:function(){
            const thiz = this;



            const DOCKER = types.DOCKER;
            this._docker = Docker.createDefault(this.domNode,{

            });

            const top = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false
            });


            const _console = utils.addWidget(DriverConsole,{
                type:'javascript',
                value:'return 2+2;'
            },this,top,true);


            const docs = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM,
                h:'300px'

            });
            this.helpContainer = docs.containerNode;


            docs.minSize(200, 100);

            //docs.initSize(200, 100);



            const bottom = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM
            });

            //bottom.initSize(200, 150);
            //docs.initSize(200, 160);
            bottom.maxSize(200);
            bottom.getSplitter().pos(0.8);


            const bottomR = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.RIGHT,
                location:DOCKER.TAB.RIGHT,
                target:bottom
            });



            const _gridClass = Grid.createGridClass('driverTreeView',{
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {
                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD
                },
                {
                    //base flip


                },
                {
                    //args

                },
                {
                    GRID: OnDemandGrid,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );


            const store = this._createExpressionStore();

            const gridArgs = {
                ctx:ctx,
                attachDirect:true,
                collection: store.filter({
                    group:'top'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };

            grid = utils.addWidget(_gridClass,gridArgs,null,bottom,true);


            /**************************************************************/

            const gridArgsRight = {
                ctx:ctx,
                noDataMessage:'',
                attachDirect:true,
                collection: store.filter({
                    group:'nada'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };

            

            const gridRight = utils.addWidget(_gridClass,gridArgsRight,null,bottomR,true);


            grid._on('selectionChanged',function(evt){
                //console.error('on changed selection ',arguments);

                const selection = evt.selection;
                const item = selection[0];

                console.log('filter right',item);
                //console.dir();

                const right = store.query({
                    parent:item.name
                });

                gridRight.set("collection", store.filter({parent: 'xx'}));

                gridRight.renderArray([]);
                gridRight.renderArray(right);
            });

            gridRight._on('selectionChanged',function(evt){
                const selection = evt.selection;
                const item = selection[0];
                thiz.currentItem = item;

                if (item && item.help) {
                    thiz.showDocumentation(item.help || "");
                }
            });
/*
            docs.minSize();
            docs.maxSize();

            //debugger;
            bottom.maxSize();
            bottom.minSize();

            */

            bottom.getSplitter().pos(0.5);
            docs.getSplitter().pos(0.8);

            _console.__bottom.getSplitter().pos(0.2);
            _console.__bottom.title(false);
            _console.__masterPanel.title(false);

            const _p =docs.$container.find('.panelParent').css('overflow','auto');


            docs.$container.parent().css({
                overflow:'auto'
            })

        },
        startup:function(){

            const thiz = this;
            this.inherited(arguments);
            //pull in ExpressionJavaScript syntax and documentation, we hide here 'require' from the compiler, otherwise its adding 2.9MB to the final layer
            const _re = require;

            _re([
                'xide/widgets/ExpressionJavaScript'], function (ExpressionJavaScript) {
                thiz.api = ExpressionJavaScript.prototype.api;
                thiz.createWidgets();
            });
        }
    });

    function openExpressionEditor(){
        const parent = TestUtils.createTab(null,null,module.id);
        const expressionEditor = utils.addWidget(_ExpressionEditor,{
            userData:{
                value:'return 2+2;'
            }
        },this,parent,true);

    }

    if (ctx && doTests) {

        console.clear();


        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();
        let marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        const _marantz = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.meta.json");
        if(_marantz){
            marantz = _marantz;
        }
        const driver = marantz.driver;
        const device = marantz.device;

        /*
        var driver = marantz.driver;
        var device = marantz.device;
        var deviceManager = ctx.getDeviceManager();*/
        //openDriverView(driver,device);

        openExpressionEditor();

        return Grid;
    }

    return _ExpressionEditor;
});