/** @module xgrid/Base **/
define([
    "dcl/dcl",
    "xdojo/declare",
    "dojo/on",
    "dojo/debounce",
    'dojo/dom-construct',
    'xide/types',
    'xide/utils',
    'xgrid/ListRenderer',
    'xgrid/Grid',
    'xaction/DefaultActions',
    'xgrid/Defaults',
    'xgrid/Layout',
    'xgrid/Focus',
    'dgrid/OnDemandGrid',
    'xide/mixins/EventedMixin',
    'xide/factory',
    "xide/views/CIViewMixin",
    'xblox/views/BlockGrid',
    'xcf/model/Command',
    'xcf/model/Variable',
    'xide/widgets/ToggleButton',
    'xide/widgets/_ActionValueWidgetMixin',
    'xide/layout/_Accordion',
    "xide/widgets/TemplatedWidgetBase",
    'xblox/model/variables/VariableAssignmentBlock',
    'dojo/promise/all',
    "dojo/Deferred",
    "xgrid/KeyboardNavigation",
    "xgrid/Search",
    "xide/tests/TestUtils",
    'xide/_base/_Widget',
    'xide/widgets/_Widget',
    'xide/views/_LayoutMixin',
    'xdocker/Docker2',
    'xaction/ActionProvider',
    'xide/widgets/ContextMenu',
    'xide/views/ConsoleView',
    'xlog/views/LogGrid',
    'xide/widgets/WidgetBase',
    'xide/widgets/ExpressionEditor',
    "module",
    "xide/data/TreeMemory",
    'dstore/QueryResults'
], function (dcl,declare, on,debounce,domConstruct,types,
             utils, ListRenderer, Grid, DefaultActions, Defaults, Layout, Focus,
             OnDemandGrid, EventedMixin, factory,CIViewMixin,BlockGrid,
             Command,Variable,
             ToggleButton,_ActionValueWidgetMixin,_Accordion,TemplatedWidgetBase,
             VariableAssignmentBlock,
             all,Deferred,KeyboardNavigation,
             Search,TestUtils,_Widget,_CWidget,_LayoutMixin,Docker,ActionProvider,ContextMenu,ConsoleView,
             LogGrid,WidgetBase,ExpressionEditor,module,TreeMemory,QueryResults

) {
    const actions = [];
    const thiz = this;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    let grid;
    let CIS;
    let basicGridInstance;


    /***
     * playground
     */
    const _lastGrid = window._lastGrid;
    const ctx = window.sctx;
    const ACTION = types.ACTION;
    let root;
    let scope;
    let blockManager;
    let driverManager;
    const doTests = true;



    function completeGrid(_grid) {

        _grid._on(types.EVENTS.ON_BUILD_BLOCK_INFO_LIST_END, function (evt) {

            const items = evt.items;

            const variables = this.blockScope.getVariables();
            const variableItems = [
                /*{
                name: 'None',
                //target: item,
                iconClass: 'el-icon-compass',
                proto: VariableAssignmentBlock,
                item: null,
                ctrArgs: {
                    variable: null,
                    scope: this.blockScope,
                    value: ''
                }
            }*/
            ];


            //console.log('on insert block end',variables);


            _.each(variables,function(variable){
                variableItems.push({
                    name: variable.name,
                    //target: item,
                    iconClass: 'el-icon-compass',
                    proto: VariableAssignmentBlock,
                    item: variable,
                    ctrArgs: {
                        variable: variable.name,
                        scope: this.blockScope,
                        value: ''
                    }
                });
            },this);

            items.push({
                name: 'Set Variable',
                iconClass: 'el-icon-pencil-alt',
                items: variableItems
            });









        });

        _grid._on('onAddActions', function (evt) {

            if(!evt.addAction){
                return;
            }
            const addAction = evt.addAction, cmdAction = 'New/Command', varAction = 'New/Variable', permissions = evt.permissions, VISIBILITY = types.ACTION_VISIBILITY, thiz = this;

            /*
            addAction('Save', 'File/Save', 'fa-save', ['ctrl s'], 'Home', 'File', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);
            */



            addAction('Command', cmdAction, 'el-icon-plus-sign', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);

            addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {}
                },

                null, null);


            addAction('Properties', 'Step/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {
                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                            widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                            widgetArgs: {
                                icon1: 'fa-toggle-on',
                                icon2: 'fa-toggle-off',
                                delegate: thiz,
                                checked: false,
                                iconClass: 'fa-toggle-off'
                            }
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });



            const settingsWidget = declare('commandSettings', TemplatedWidgetBase,{
                templateString:'<div></div>',
                _getText: function (url) {
                    let result;
                    const def = dojo.xhrGet({
                        url: url,
                        sync: true,
                        handleAs: 'text',
                        load: function (text) {
                            result = text;
                        }
                    });
                    return '' + result + '';
                },
                startup:function(){

                    this.inherited(arguments);

                    if(!_grid.userData){
                        return;
                    }


                    const settings = utils.getJson(_grid.userData['params']) || {
                            constants: {
                                start: '',
                                end: ''
                            },
                            send: {
                                mode: false,
                                interval: 500,
                                timeout: 500,
                                onReply: ''
                            }
                        };




                    const settingsPane = utils.templatify(
                        null,
                        this._getText(require.toUrl('xcf/widgets/templates/commandSettings.html')),
                        this.domNode,
                        {
                            baseClass: 'settings',
                            style:'width:100%',
                            start: settings.constants.start,
                            end: settings.constants.end,
                            interval: settings.send.interval,
                            timeout: settings.send.timeout,
                            sendMode: settings.send.mode,
                            onReply: settings.send.onReply,
                            settings: settings
                        }, null
                    );

                    return;

                    if (settings.send.mode) {
                        settingsPane.rReply.set('checked', true);
                    } else {
                        settingsPane.rInterval.set('checked', true);
                    }

                    const _onSettingsChanged = function () {
                        //update params field of our ci
                        thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                        //thiz.save();
                        console.log('changd');
                    };



                    //wire events
                    dojo.connect(settingsPane.wStart, "onChange", function (item) {
                        settingsPane.settings.constants.start = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wEnd, "onChange", function (item) {
                        settingsPane.settings.constants.end = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wInterval, "onChange", function (item) {
                        settingsPane.settings.send.interval = item;
                        _onSettingsChanged();
                    });
                    dojo.connect(settingsPane.wTimeout, "onChange", function (item) {
                        settingsPane.settings.send.timeout = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.wOnReply, "onChange", function (item) {
                        settingsPane.settings.send.onReply = item;
                        _onSettingsChanged();
                    });

                    dojo.connect(settingsPane.rReply, "onChange", function (item) {
                        settingsPane.settings.send.mode = item;
                        _onSettingsChanged();
                    });
                }
            });


            addAction('Settings', 'File/Settings', 'fa-gears', null, 'Settings', 'Settings', 'item|view', null, null,
                {
                    addPermission: true,
                    onCreate: function (action) {

                        action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                        action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null);

                        action.setVisibility(types.ACTION_VISIBILITY.RIBBON,{
                            widgetClass:settingsWidget
                        });
                    }
                }, null, function () {
                    return thiz.getSelection().length == 0;
                });



        });

        _grid._on('selectionChanged', function (evt) {
            //console.log('selection ',evt);
            //since we can only add blocks to command and not
            //at root level, disable the 'Block/Insert' root action and
            //its widget //references
            const thiz = this, selection = evt.selection, item = selection[0], blockInsert = thiz.getAction('Block/Insert'), blockEnable = thiz.getAction('Step/Enable');

            disable = function (disable) {
                blockInsert.set('disabled', disable);
                setTimeout(function () {
                    blockInsert.getReferences().forEach(function (ref) {
                        ref.set('disabled', disable);
                    });
                }, 100);

            }

            const _disable =item ? false : true;

            disable(_disable);


            if (item) {
                blockEnable.getReferences().forEach(function (ref) {
                    ref.set('checked', item.enabled);
                });
            }else{
                /*
                var props = _grid.getPropertyStruct();
                props._lastItem = null;
                _grid.setPropertyStruct(props);*/
            }
        });

        _grid.startup();

    }

    const DockerBase = dcl([ActionProvider.dcl,_CWidget.dcl],{
        permissions:[
          types.ACTION.RENAME
        ],
        __panel:function(el){
            console.log(el);
            const panels = this.getPanels();

            let frame = null;
            const self = this;
            _.each(this._frameList,function(_frame){
                if($.contains(_frame.$container[0],el)){
                    frame = _frame;
                }
            });




            const id = self.contextMenuPanelId;

            this.contextMenuEventTarget = null;
            this.contextMenuPanelId = null;

            if(frame && id!==null){

                const _panel = frame.panel(id);
                if(_panel){
                    return _panel;
                }
            }
        },
        runAction:function(action,type,event){


            const _panel = this.contextMenuEventTarget ? this.__panel(this.contextMenuEventTarget) : null;
            if(_panel){
                this.removePanel(_panel);
            }

            console.error('run action',_panel);
        },
        getDockerActions:function(){

            const actions = [];

            actions.push(this.createAction({
                label:'Close',
                command:types.ACTION.RENAME,
                tab:'View',
                group:'Misc',
                mixin: {
                    addPermission: true
                },
                icon:'fa-close'
                /*keycombo:['ctrl w','ctrl f4']*/
            }));


            return actions;

        },
        contextMenu:null,
        contextMenuEventTarget:null,
        contextMenuPanelId:null,
        setupActions:function(){

            const self = this;

            this.__on(this._root.$container,'contextmenu', '.wcPanelTab', function(e){
                console.error('context menu');
                self.contextMenuEventTarget  = e.target;
                self.contextMenuPanelId = e.target.parentNode ?  e.target.parentNode.id : null;
            });


            this.addActions(this.getDockerActions());


            const args = {
                owner:this,
                delegate:this,
                limitTo:'wcPanelTab'
            };

            const node = this._root.$container[0];

            const contextMenu = new ContextMenu(args,node);
            contextMenu.openTarget = node;
            contextMenu.init({preventDoubleContext: false});

            //contextMenu._registerActionEmitter(this);
            contextMenu.setActionEmitter(this,types.EVENTS.ON_VIEW_SHOW,this);

            this.contextMenu = contextMenu;


            this.add(contextMenu,null,false);
        },
        __init:dcl.superCall(function(sup){
            return function(){
                if(sup){

                    sup.apply(this, arguments);
                    this.setupActions();
                }
                return 0;
            };
        }),
        __destroy:function(){
            console.error('_destroy');
        }
    });

    const ConsoleViewImplementation = ConsoleView.Implementation;



    function createGridClass(){


        const _debugHightlighting = false;
        const _debugRun = false;

        //return BlockGrid;

        const GridClass = declare('DriverBlockGridClass',BlockGrid, {

            _console: null,
            _lastConsoleItem: null,
            toolbarInitiallyHidden: true,
            highlightDelay: 1000,
            propertyStruct: null,
            onShowProperties: function (item) {
                const _console = this._console;
                if (_console && item) {

                    const value = item.send || item.method;
                    const editor = _console.getConsoleEditor();

                    editor.set('value', value);
                    this._lastConsoleItem = item;
                }
            },
            /**
             *
             * @param action {Action}
             * @param editor {ACEEditor}
             */
            openTerminal: function (action, editor) {

                const item = this.getSelectedItem() || {};

                // already open
                if (this._console) {

                    utils.destroy(this._console);
                    utils.destroy(this.__bottom);
                    this.__bottom = null;
                    this._console = null;
                    this.getDocker().resize();
                    //this.getDocker().removePanel(this._console._parent);
                    return;
                }

                const ExpressionEditor = editor.delegate; // xide.widgets.ExpressionEditor

                const ctx = this.ctx;
                const deviceManager = ctx.getDeviceManager();
                const right = this.getRightPanel('Properties', null, 'DefaultTab', {});
                const bottomPanel = this.getBottomPanel('Console', null, 'DefaultTab', null, false);
                const value = item.send || item.method;
                const _console = deviceManager.openConsole(this.device, bottomPanel, {
                    value: value
                });
                this._lastConsoleItem = item;
                bottomPanel.closeable(false);
                this._console = _console;
            },
            getTypeMap: function () {

                if (this.typeMap) {
                    return this.typeMap;
                }



                console.error('--get type map');

                const self = this;

                const ExpressionEditorExtra = dcl(ExpressionEditor, {
                    EditorClass: dcl(ConsoleView.Editor, {
                        runAction: function (action) {
                            if (action.command === 'Console/Terminal') {
                                return self.openTerminal(action, this);
                            }
                            return this.inherited(arguments);
                        }
                    }),
                    onAddEditorActions: function (evt) {

                        const actions = evt.actions, owner = evt.owner;


                        const mixin = {
                            addPermission: true
                        };


                        actions.push(owner.createAction({
                            label: 'Send',
                            command: 'Console/Terminal',
                            icon: 'fa-terminal',
                            group: 'Console',
                            tab: 'Home',
                            mixin: mixin,
                            handler: function () {

                            }
                        }));
                    },
                    onCreatedEditor:function(){



                        console.error('created editor');

                    },
                    onCreatedConsole:function(){
                        console.error('created console');
                    },
                    startup:dcl.superCall(function(sup) {
                        return function () {
                            const res = sup.call(this);

                        };
                    })
                });


                const typeMap = {};

                typeMap['xide.widgets.ExpressionEditor'] = ExpressionEditorExtra;

                this.typeMap = typeMap;

            },
            showProperties: function (item, force) {

                const dfd = new Deferred(), block = item || this.getSelection()[0], thiz = this, rightSplitPosition = thiz.getPanelSplitPosition(types.DOCKER.DOCK.RIGHT);

                if (!block || rightSplitPosition == 1) {
                    console.log(' show properties: abort', [block, rightSplitPosition]);
                    return;
                }


                const right = this.getRightPanel('Properties', null, 'DefaultTab', {});

                right.closeable(false);

                let props = this.getPropertyStruct();

                if (block == props._lastItem && force !== true) {
                    //console.log('show properties : same item');
                    return;
                }

                this.clearPropertyPanel();

                props = this.getPropertyStruct();


                props._lastItem = item;


                const _title = block.name || block.title;


                //console.log('show properties for ' + _title+ ' : ' + item.declaredClass,props);

                function propertyTabShown(tab, show) {
                    const title = tab.title;

                    if (show) {
                        props.lastSelectedTopTabTitle = title;

                    } else if (!show && props.lastSelectedTopTabTitle === title) {
                        props.lastSelectedTopTabTitle = null;
                    }
                }


                let tabContainer = props.targetTop;
                if (!tabContainer) {
                    tabContainer = utils.addWidget(_Accordion, {}, null, right.containerNode, true);

                    $(tabContainer.domNode).addClass('CIView Accordion');

                    props.targetTop = tabContainer;

                    tabContainer._on('show', function (evt) {
                        propertyTabShown(evt.view, true)
                    });

                    tabContainer._on('hide', function (evt) {
                        propertyTabShown(evt.view, false);
                    });

                }

                _.each(tabContainer.getChildren(), function (tab) {
                    tabContainer.removeChild(tab);
                });

                if (props.currentCIView) {
                    props.currentCIView.empty();
                }

                if (!block.getFields) {
                    console.log('have no fields', block);
                    return;
                }

                const cis = block.getFields();
                for (let i = 0; i < cis.length; i++) {
                    cis[i] && (cis[i].vertical = true);
                }




                const ciView = new CIViewMixin({
                    typeMap: thiz.getTypeMap(),
                    tabContainerClass: _Accordion,
                    tabContainer: props.targetTop,
                    delegate: this,
                    viewStyle: 'padding:0px;',
                    autoSelectLast: true,
                    item: block,
                    source: this.callee,
                    options: {
                        groupOrder: {
                            'General': 1,
                            'Advanced': 2,
                            'Script': 3,
                            'Arguments': 4,
                            'Description': 5,
                            'Share': 6

                        }
                    },
                    cis: cis
                });



                ciView.initWithCIS(cis);
                props.currentCIView = ciView;

                if (block.onFieldsRendered) {
                    block.onFieldsRendered(block, cis);
                }



                setTimeout(function () {
                    ciView._on('valueChanged', function (evt) {
                        //console.log('ci value changed ', evt);
                        setTimeout(function () {
                            thiz.onCIChanged && thiz.onCIChanged(evt.ci, block, evt.oldValue, evt.newValue, evt.ci.dst);
                        }, 10);
                    });
                },100);


                const containers = props.targetTop.getChildren();
                const descriptionView = null;

                const tabToOpen = tabContainer.getTab(props.lastSelectedTopTabTitle);
                if (tabToOpen) {
                    tabContainer.selectChild(props.lastSelectedTopTabTitle, true);
                } else {
                    props.lastSelectedTopTabTitle = null;
                }

                this.setPropertyStruct(props);

                this.onShowProperties(block);

            },
            execute: function (_blocks) {
                const thiz = this;


                const //all Deferreds of selected blocks to run
                dfds = [];

                let handles = [];

                const //shortcut
                EVENTS = types.EVENTS;

                const //normalize selection to array
                blocks = _.isArray(_blocks) ? _blocks : [_blocks];


                function run(block) {

                    const _runHandle = block._on(EVENTS.ON_RUN_BLOCK, function (evt) {
                                  _debugHightlighting && console.error('active');
                                  thiz.mark(thiz.toNode(evt), 'activeBlock',block);
                              }),
                          //event handle "Success"
                          _successHandle = block._on(EVENTS.ON_RUN_BLOCK_SUCCESS, function (evt) {
                              _debugHightlighting && console.log('marke success', evt);
                              thiz.mark(thiz.toNode(evt), 'successBlock',block);

                          }),
                          //event handle "Error"
                          _errHandle = block._on(EVENTS.ON_RUN_BLOCK_FAILED, function (evt) {
                              _debugHightlighting && console.error('failed',evt);
                              thiz.mark(thiz.toNode(evt), 'failedBlock',block);
                          });


                    _debugRun && console.error('run block ');


                    if (!block || !block.scope) {
                        console.error('have no scope');
                        return;
                    }

                    try {
                        const blockDfd = block.scope.solveBlock(block, {
                            highlight: true,
                            force: true,
                            listener: thiz
                        });

                        dfds.push(blockDfd);

                    } catch (e) {

                        console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                        logError(e, 'excecuting block -  ' + block.name + ' failed! : ');
                    }
                    handles = handles.concat([_runHandle, _errHandle, _successHandle]);


                    return true;
                }


                function _patch(block) {


                    block.runFrom = function (_blocks, index, settings) {

                        const thiz = this, blocks = _blocks || this.items, allDfds = [];

                        const onFinishBlock = function (block, results) {
                            block._lastResult = block._lastResult || results;
                            thiz._currentIndex++;
                            thiz.runFrom(blocks, thiz._currentIndex, settings);
                        };

                        const wireBlock = function (block) {
                            block._deferredObject.then(function (results) {
                                console.log('----def block finish');
                                onFinishBlock(block, results);
                            });
                        };

                        if (blocks.length) {

                            for (let n = index; n < blocks.length; n++) {


                                const block = blocks[n];

                                console.log('run child \n' + block.method);

                                _patch(block);

                                if (block.deferred === true) {
                                    block._deferredObject = new Deferred();
                                    this._currentIndex = n;
                                    wireBlock(block);
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));
                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                    break;
                                } else {
                                    //this.addToEnd(this._return, block.solve(this.scope, settings));

                                    var blockDfd = block.solve(this.scope, settings);
                                    allDfds.push(blockDfd);
                                }

                            }

                        } else {
                            this.onSuccess(this, settings);
                        }

                        return allDfds;
                    };

                    block.solve = function (scope, settings, run, error) {

                        this._currentIndex = 0;
                        this._return = [];

                        const _script = '' + this._get('method');

                        const thiz = this,
                              ctx = this.getContext(),
                              items = this[this._getContainer()],
                              //outer,head dfd
                              dfd = new Deferred,
                              listener = settings.listener,
                              isDfd = thiz.deferred;


                        //moved to Contains#onRunThis
                        if (listener) {
                            listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                        }

                        //function when a block did run successfully,
                        // moved to Contains#onDidRunItem
                        function _finish(dfd, result, event) {

                            if (listener) {
                                listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                            }
                            dfd.resolve(result);


                        }

                        //function when a block did run successfully
                        function _error(result) {
                            dfd.reject(result);
                            if (listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                            }
                        }


                        //moved to Contains#onDidRunThis
                        function _headDone(result) {


                            //more blocks?
                            if (items.length) {
                                const subDfds = thiz.runFrom(items, 0, settings);

                                all(subDfds).then(function (what) {
                                    console.log('all solved!', what);
                                    _finish(dfd, result);
                                }, function (err) {
                                    console.error('error in chain', err);
                                    _finish(dfd, err);
                                });

                            } else {
                                _finish(dfd, result);
                            }
                        }


                        if (_script && _script.length) {

                            const runScript = function () {

                                const _function = new Function("{" + _script + "}");
                                const _args = thiz.getArgs() || [];
                                try {

                                    if (isDfd) {

                                        ctx.resolve = function (result) {
                                            console.log('def block done');
                                            if (thiz._deferredObject) {
                                                thiz._deferredObject.resolve();
                                            }
                                            _headDone(result);
                                        }
                                    }
                                    const _parsed = _function.apply(ctx, _args || {});
                                    thiz._lastResult = _parsed;
                                    if (run) {
                                        run('Expression ' + _script + ' evaluates to ' + _parsed);
                                    }


                                    if (!isDfd) {
                                        console.log('root block done');
                                        //_headDone(_parsed);
                                        thiz.onDidRunThis(dfd, _parsed, items, settings);
                                    }

                                    if (_parsed !== 'false' && _parsed !== false) {

                                    } else {
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                } catch (e) {

                                    e = e || {};

                                    _error(e);

                                    if (error) {
                                        error('invalid expression : \n' + _script + ': ' + e);
                                    }
                                    //thiz.onFailed(thiz, settings);
                                    //return [];
                                }
                            };

                            if (scope.global) {
                                (function () {

                                    window = scope.global;


                                    const _args = thiz.getArgs() || [];
                                    try {
                                        let _parsed = null;
                                        if (!ctx.runExpression) {
                                            const _function = new Function("{" + _script + "}").bind(this);
                                            _parsed = _function.apply(ctx, _args || {});
                                        } else {
                                            _parsed = ctx.runExpression(_script, null, _args);
                                        }

                                        thiz._lastResult = _parsed;

                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }
                                        if (_parsed !== 'false' && _parsed !== false) {
                                            thiz.onSuccess(thiz, settings);
                                        } else {
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }


                                    } catch (e) {

                                        thiz._lastResult = null;
                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        thiz.onFailed(thiz, settings);
                                        return [];
                                    }

                                }).call(scope.global);

                            } else {
                                runScript();
                            }

                        } else {
                            console.error('have no script');
                        }
                        return dfd;
                    }


                }

                //_.each(blocks,_patch);

                _.each(blocks, run);

                all(dfds).then(function () {
                    _debugRun && console.log('did run all selected blocks!', thiz);
                    _.invoke(handles, 'remove');
                });
            },
            __onCIChanged: function (ci, block, oldValue, newValue, field) {

                console.log('on ci changed', arguments);

                const _col = block._store;

                block = _col.getSync(block.id);

                block.set(field, newValue);

                //block[field] = newValue;

                //block.refresh();




                /*
                //this._itemChanged('changed',block,_col);


                if(this.collection!=_col){
                    console.error('not the same collection');
                }

                _col.refreshItem(block);
                this.collection.refreshItem(block);
                block.refresh();
                */

                /*
                this.collection.emit('update',{
                    target:block
                })
                */




                const thiz = this;
                this.refresh();
                thiz._itemChanged('changed',block,_col);

            },
            ___onCIChanged: function (ci, block, oldValue, newValue, field) {

                console.log('on ci changed', arguments);

                const _col= this.collection;
                block = this.collection.getSync(block.id);
                block.set(field, newValue);
                block[field]=newValue;
                _col.refreshItem(block);

            },

            _itemChanged: function (type, item, store) {

                store = store || this.getStore(item);

                const thiz = this;

                function _refreshParent(item, silent) {

                    const parent = item.getParent();
                    if (parent) {
                        const args = {
                            target: parent
                        };
                        if (silent) {
                            this._muteSelectionEvents = true;
                        }
                        store.emit('update', args);
                        if (silent) {
                            this._muteSelectionEvents = false;
                        }
                    } else {
                        thiz.refresh();
                    }
                }

                function select(item) {
                    thiz.select(item, null, true, {
                        focus: true,
                        delay: 20,
                        append: false
                    });
                }

                switch (type) {

                    case 'added':
                    {
                        //_refreshParent(item);
                        //this.deselectAll();
                        this.refresh();
                        select(item);
                        break;
                    }

                    case 'changed':
                    {
                        //this.refresh();
                        select(item);
                        break;
                    }


                    case 'moved':
                    {
                        //_refreshParent(item,true);
                        //this.refresh();
                        //select(item);
                        break;
                    }

                    case 'deleted':
                    {

                        const parent = item.getParent();
                        //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                        const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                        if (parent) {
                            const _container = parent.getContainer();
                            if (_container) {
                                _.each(_container, function (child) {
                                    if (child.id == item.id) {
                                        _container.remove(child);
                                    }
                                });
                            }
                        }

                        this.refresh();
                        /*
                         if (_prev) {
                         select(_prev);
                         }
                         */
                        break;
                    }

                }


            },
            _onFocusChanged: function (focused, type) {
                this.inherited(arguments);
                if (!focused) {
                    this._lastSelection = [];
                }

            },
            save: function () {


                const thiz = this,
                      driver = thiz.userData.driver,
                      ctx = thiz.userData.ctx,
                      fileManager = ctx.getFileManager(),
                      //instance scope
                      scope = thiz.blockScope,
                      instance = scope.instance,
                      //original driver scope
                      originalScope = driver.blockScope,
                      path = driver.path.replace('.meta.json', '.xblox'),
                      scopeToSave = originalScope || scope,
                      mount = driver.scope;


                if (originalScope && scopeToSave != originalScope) {
                    originalScope.fromScope(scope);
                }

                if (scope) {

                    const all = {
                        blocks: null,
                        variables: null
                    };

                    const blocks = scope.blocksToJson();
                    try {
                        //test integrity
                        dojo.fromJson(JSON.stringify(blocks));
                    } catch (e) {
                        console.error('invalid data');
                        return;
                    }

                    const _onSaved = function () {
                    };

                    all.blocks = blocks;

                    console.log('saving driver ' + mount + '/' + path, driver);


                    fileManager.setContent(mount, path, JSON.stringify(all, null, 2), _onSaved);
                    //this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
                }
            },
            runAction: function (action) {

                const thiz = this;
                const sel = this.getSelection();


                function addItem(_class,group){


                    const dfd = new Deferred();

                    const cmd = factory.createBlock(_class, {
                        name: "No Title",
                        scope: thiz.blockScope,
                        group: group
                    });

                    const defaultDfdArgs = {
                        select: [cmd],
                        focus: true,
                        append: false
                    };



                    //cmd.refresh();
                    const ref = thiz.refresh();
                    ref && ref.then && ref.then(function(){
                        dfd.resolve(defaultDfdArgs);
                    });

                    return dfd;



                    //thiz.deselectAll();

                    /*
                     _.each(thiz.grids,function(grid){
                     if(grid) {
                     grid.refresh();
                     }
                     });
                     */
                    /*
                     setTimeout(function () {
                     thiz.select([cmd],null,true,{
                     focus:true
                     });
                     }, 200);
                     */

                }

                if (action.command == 'New/Command') {
                    addItem(Command, this.blockGroup);
                }

                if (action.command == 'New/Variable') {
                    addItem(Variable, 'basicVariables');
                }

                if (action.command == 'File/Save') {
                    this.save();
                }
                return this.inherited(arguments);
            },
            mark:function(element,cssClass,block){
                block = block || {};
                const thiz = this;
                if (element) {
                    element.removeClass('failedBlock successBlock activeBlock');
                    element.addClass(cssClass);
                    if(element.hightlightTimer){
                        clearTimeout(element.hightlightTimer);
                    }
                    element.hightlightTimer = setTimeout(function () {
                        element.removeClass(cssClass);
                        element.removeClass('failedBlock successBlock activeBlock');
                        element.hightlightTimer = null;
                    }, thiz.highlightDelay);
                }
            },
            _observeStore:function(collection, container, options) {
                const self = this;
                const rows = options.rows;
                let row;

                const handles = [
                    collection._on('update', function (event) {


                        console.error('update 1');

                        const from = event.previousIndex;
                        const to = event.index;

                        if (from !== undefined && rows[from]) {
                            if ('max' in rows && (to === undefined || to < rows.min || to > rows.max)) {
                                rows.max--;
                            }

                            row = rows[from];

                            // check to make the sure the node is still there before we try to remove it
                            // (in case it was moved to a different place in the DOM)
                            if (row.parentNode === container) {
                                self.removeRow(row, false, options);
                            }

                            // remove the old slot
                            rows.splice(from, 1);

                            if (event.type === 'delete' ||
                                (event.type === 'update' && (from < to || to === undefined))) {
                                // adjust the rowIndex so adjustRowIndices has the right starting point
                                rows[from] && rows[from].rowIndex--;
                            }
                        }
                        if (event.type === 'delete') {
                            // Reset row in case this is later followed by an add;
                            // only update events should retain the row variable below
                            row = null;
                        }
                    }),

                    collection._on('update', function (event) {

                        console.error('update 2');
                        const from = event.previousIndex;
                        const to = event.index;
                        let nextNode;

                        function advanceNext() {
                            nextNode = (nextNode.connected || nextNode).nextSibling;
                        }

                        // When possible, restrict observations to the actually rendered range
                        if (to !== undefined && (!('max' in rows) || (to >= rows.min && to <= rows.max))) {
                            if ('max' in rows && (from === undefined || from < rows.min || from > rows.max)) {
                                rows.max++;
                            }
                            // Add to new slot (either before an existing row, or at the end)
                            // First determine the DOM node that this should be placed before.
                            if (rows.length) {
                                nextNode = rows[to];
                                if (!nextNode) {
                                    nextNode = rows[to - 1];
                                    if (nextNode) {
                                        // Make sure to skip connected nodes, so we don't accidentally
                                        // insert a row in between a parent and its children.
                                        advanceNext();
                                    }
                                }
                            }
                            else {
                                // There are no rows.  Allow for subclasses to insert new rows somewhere other than
                                // at the end of the parent node.
                                nextNode = self._getFirstRowSibling && self._getFirstRowSibling(container);
                            }
                            // Make sure we don't trip over a stale reference to a
                            // node that was removed, or try to place a node before
                            // itself (due to overlapped queries)
                            if (row && nextNode && row.id === nextNode.id) {
                                advanceNext();
                            }
                            if (nextNode && !nextNode.parentNode) {
                                nextNode = document.getElementById(nextNode.id);
                            }
                            rows.splice(to, 0, undefined);
                            row = self.insertRow(event.target, container, nextNode, to, options);
                            self.highlightRow(row);
                        }
                        // Reset row so it doesn't get reused on the next event
                        row = null;
                    }),

                    collection._on('update', function (event) {

                        //console.error('update 3');

                        const from = (typeof event.previousIndex !== 'undefined') ? event.previousIndex : Infinity, to = (typeof event.index !== 'undefined') ? event.index : Infinity, adjustAtIndex = Math.min(from, to);
                        from !== to && rows[adjustAtIndex] && self.adjustRowIndices(rows[adjustAtIndex]);

                        // the removal of rows could cause us to need to page in more items
                        if (from !== Infinity && self._processScroll && (rows[from] || rows[from - 1])) {
                            self._processScroll();
                        }

                        // Fire _onNotification, even for out-of-viewport notifications,
                        // since some things may still need to update (e.g. Pagination's status/navigation)
                        self._onNotification(rows, event, collection);

                        // Update _total after _onNotification so that it can potentially
                        // decide whether to perform actions based on whether the total changed
                        if (collection === self._renderedCollection && 'totalLength' in event) {
                            self._total = event.totalLength;
                        }
                    })
                ];

                return {
                    remove: function () {
                        while (handles.length > 0) {
                            handles.pop().remove();
                        }
                    }
                };
            },
            startup: function () {
                if(this._started){
                    return;
                }


                console.error('---startup ' + this.id);


                const thiz = this;

                this.inherited(arguments);



                const self = this;
                const rows = options.rows;
                let row;

                /*
                this._observerHandle2 = this._observeStore(
                    this.collection,
                    this.contentNode,
                    { rows: this._rows }
                );
                */
                //this.set('collection',this.collection);
                /*
                this._observerHandle2 = this._observeCollection(
                    this.collection,
                    this.contentNode,
                    { rows: this._rows }
                );
                */

                this.collection._on('update', function (evt) {

                    //console.log('store update');
                    const item = evt.target, node = thiz.toNode(evt), type = evt.type;
                    if (type === 'update' && evt.property === 'value') {
                        thiz.mark(node, 'successBlock',item);
                    }
                });


                //this.subscribe(types.EVENTS.ON_RUN_BLOCK,this.onRunBlock);
                this.subscribe(types.EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                    thiz.mark(thiz.toNode(evt), 'failedBlock',evt.target);
                });
                this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                    thiz.mark(thiz.toNode(evt), 'successBlock',evt.target);
                });


                //this.typeMap = this.getTypeMap();

                //this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS,this.onRunBlockSuccess);
            }
        });


        return GridClass;
    }


    const DriverViewClass = dcl([_Widget,_LayoutMixin.dcl],{
        templateString:'<div style="with:inherit;height: inherit"/>',
        showBasicCommands:true,
        showConditionalCommands:true,
        showResponseBlocks:true,
        showLog:true,
        showConsole:true,
        showVariables:true,
        showSettings:true,
        logTab:null,
        consoleTab:null,
        basicCommandsTab:null,
        conditionalTab:null,
        variablesTab:null,
        responseTab:null,
        settingsTab:null,
        gridClass:null,
        createTab:function(type,args){

            const DOCKER = types.DOCKER;
            const defaultTabArgs = {
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:DOCKER.TAB.TOP,
                location:DOCKER.DOCK.STACKED

            };
            return this._docker.addTab(type || 'DefaultTab',utils.mixin(defaultTabArgs,args));
        },
        createLayout:function(){


            const DOCKER = types.DOCKER;
            this._docker = Docker.createDefault(this.domNode,{
                extension:DockerBase
            });

            const basicCommands = this.showBasicCommands ? this.createTab(null,{
                title:'Basic Commands',
                icon:'fa-long-arrow-right'
            }) : null;

            const condCommands = this.showConditionalCommands ? this.createTab(null,{
                title:'Conditional Commands',
                tabOrientation:DOCKER.TAB.TOP,
                target:basicCommands,
                icon:'fa-long-arrow-right'
            }) : null;

            const settingsTab = this.showSettings ? this.createTab(null,{
                title:'Settings',
                tabOrientation:DOCKER.TAB.TOP,
                target:basicCommands,
                icon:'fa-cogs'
            }) : null;

            const log = this.showLog ? this.createTab(null,{
                title:'Log',
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM,
                icon:'fa-calendar'
            }) : null;

            const consoleTab = this.showConsole ? this.createTab(null,{
                title:'Console',
                tabOrientation:DOCKER.TAB.TOP,
                target:log,
                icon:'fa-terminal'
            }) : null;

            const lastBottom = log || consoleTab;



            const responses = this.showResponseBlocks ? this.createTab(null,{
                title:'Responses',
                tabOrientation:DOCKER.TAB.TOP,
                target:lastBottom,
                icon:'fa-long-arrow-left'
            }) : null;

            const variables = this.showVariables ? this.createTab(null,{
                title:'Variables',
                tabOrientation:DOCKER.TAB.TOP,
                target:lastBottom,
                icon:'fa-list-alt'
            }) : null;

            this.logTab=log;
            this.consoleTab=consoleTab;
            this.basicCommandsTab = basicCommands;
            this.conditionalTab = condCommands;
            this.variablesTab = variables;
            this.responseTab=responses;

            this.settingsTab=settingsTab;



            return this._docker;


        },
        completeGrid:function(grid,newItemLabel){

            grid.userData = this.userData;


            const widget = this;


            function _completeGrid(_grid) {

                _grid._on('onAddActions', function (evt) {


                    const addAction = evt.addAction,
                          cmdAction = 'New/Command',
                          varAction = 'New/Variable',
                          result = evt.actions,
                          permissions = evt.permissions,
                          VISIBILITY = types.ACTION_VISIBILITY,
                          thiz = this,
                          defaultMixin = {
                              addPermission: true
                          };


                    if(!addAction){
                        return;
                    }




                    result.push(thiz.createAction({
                        label: 'Command',
                        command: cmdAction,
                        icon: 'fa-plus',
                        tab: 'Home',
                        group: 'Insert',
                        mixin:defaultMixin
                    }));
                    result.push(thiz.createAction({
                        label: 'Variable',
                        command: varAction,
                        icon: 'fa-code',
                        tab: 'Home',
                        group: 'Insert',
                        mixin:defaultMixin
                    }));




                    /*
                    addAction('Variable', varAction, 'fa-code', ['ctrl n'], 'Home', 'Insert', 'item|view', null, null,{
                        addPermission: true
                    },null, null);
                    */


                    /*
                    addAction('Properties', 'View/Show/Properties', 'fa-gears', ['alt enter'], 'Home', 'Step', 'item|view', null, null,{
                            addPermission: true,
                            onCreate: function (action) {
                                action.handle=false;
                                action.setVisibility(types.ACTION_VISIBILITY.RIBBON, {
                                    widgetClass: declare.classFactory('_Checked', [ToggleButton, _ActionValueWidgetMixin], null, {}, null),
                                    widgetArgs: {
                                        icon1: 'fa-toggle-on',
                                        icon2: 'fa-toggle-off',
                                        delegate: thiz,
                                        checked: false,
                                        iconClass: 'fa-toggle-off'
                                    }
                                });
                            }
                        }, null, function () {
                            return thiz.getSelection().length == 0;
                    });
                    */

/*
                    var settingsWidget = declare('commandSettings', TemplatedWidgetBase,{
                        templateString:'<div></div>',
                        _getText: function (url) {
                            var result;
                            var def = dojo.xhrGet({
                                url: url,
                                sync: true,
                                handleAs: 'text',
                                load: function (text) {
                                    result = text;
                                }
                            });
                            return '' + result + '';
                        },
                        startup:function(){
                            if(this._started){
                                return;
                            }

                            this.inherited(arguments);

                            if(!_grid.userData){
                                return;
                            }

                            var settings = utils.getJson(_grid.userData['params']) || {
                                    constants: {
                                        start: '',
                                        end: ''
                                    },
                                    send: {
                                        mode: false,
                                        interval: 500,
                                        timeout: 500,
                                        onReply: ''
                                    }
                                };

                            if(!widget.settingsTemplate){
                                widget.settingsTemplate = this._getText(require.toUrl('xcf/widgets/templates/commandSettingsNew.html'));
                            }


                            var settingsPane = utils.templatify(
                                null,
                                widget.settingsTemplate,
                                this.domNode,
                                {
                                    baseClass: 'settings',
                                    start: settings.constants.start,
                                    end: settings.constants.end,
                                    interval: settings.send.interval,
                                    timeout: settings.send.timeout,
                                    sendMode: settings.send.mode,
                                    onReply: settings.send.onReply,
                                    settings: settings
                                }, null
                            );


                            if (settings.send.mode) {
                                $(settingsPane.rReply).prop('checked', true);
                            } else {
                                $(settingsPane.rInterval).prop('checked', true);
                            }



                            var _onSettingsChanged = function () {
                                //update params field of our ci
                                thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                                widget.setValue('{}');
                            };

                            //start
                            $(settingsPane.start).on('change',function(e){
                                settingsPane.settings.constants.start = e.target.value;
                                _onSettingsChanged();
                            });

                            //end
                            $(settingsPane.end).on('change',function(e){
                                settingsPane.settings.constants.end = e.target.value;
                                _onSettingsChanged();
                            });






                            //mode
                            $(settingsPane.rReply).on("change", function (e) {
                                var value = e.target.value=='on' ? 1:0
                                settingsPane.settings.send.mode = value;
                                _onSettingsChanged();
                            });

                            $(settingsPane.rInterval).on("change", function (e) {
                                var value = e.target.value=='on' ? 0:1;
                                settingsPane.settings.send.mode = value;
                                _onSettingsChanged();
                            });


                            //interval time
                            $(settingsPane.wInterval).on('change',function(e){
                                settingsPane.settings.send.interval = e.target.value;
                                _onSettingsChanged();
                            });






                            //on reply value
                            $(settingsPane.wOnReply).on('change',function(e){

                                settingsPane.settings.send.onReply = e.target.value;
                                _onSettingsChanged();
                            });

                            //onReply - timeout
                            $(settingsPane.wTimeout).on('change',function (e) {
                                settingsPane.settings.send.timeout = e.target.value;
                                _onSettingsChanged();
                            });

                        }
                    });


                    addAction('Settings', 'File/Settings', 'fa-gears', null, 'Settings', 'Settings', 'item|view', null, null,
                        {
                            addPermission: true,
                            handle:false,
                            onCreate: function (action) {

                                handle:false,
                                    action.setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, null);

                                action.setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                                action.setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null);

                                action.setVisibility(types.ACTION_VISIBILITY.RIBBON,{
                                    widgetClass:settingsWidget
                                });
                            }
                        }, null, function () {
                            return thiz.getSelection().length == 0;
                        });
*/


                });

                _grid._on('selectionChanged', function (evt) {

                    //since we can only add blocks to command and not
                    //at root level, disable the 'Block/Insert' root action and
                    //its widget //references
                    const thiz = this, selection = evt.selection, item = selection[0], blockInsert = thiz.getAction('Block/Insert'), blockEnable = thiz.getAction('Step/Enable'), newCommand = thiz.getAction('New/Command');


                    disable = function (disable) {

                        blockInsert.set('disabled', disable);

                        setTimeout(function () {
                            blockInsert.getReferences().forEach(function (ref) {
                                ref.set('disabled', disable);
                            });
                        }, 100);

                    }

                    let _disable = item ? false : true;
                    if(_grid.blockGroup === 'conditionalProcess'){
                        _disable=false;
                        newCommand.set('disabled', true);
                        setTimeout(function () {
                            newCommand.getReferences().forEach(function (ref) {
                                ref.set('disabled', true);
                            });
                        }, 100);
                    }

                    disable(_disable);


                    if (item) {
                        blockEnable.getReferences().forEach(function (ref) {
                            ref.set('checked', item.enabled);
                        });
                    }else{
                        /*
                         var props = _grid.getPropertyStruct();
                         props._lastItem = null;
                         _grid.setPropertyStruct(props);*/
                    }
                });
            }

            _completeGrid(grid);

            this.add(grid,null,false);
            if(grid.registerView){
                this.ctx.getWindowManager().registerView(grid,false);
            }
        },
        createGrid:function(tab,scope,store,group,newItemLabel,options){
            const _docker = this._docker;
            const widget = this;
            let grid;
            const gridClass = this.getGridClass();
            const grids = this.grids;






            const args = utils.mixin({
                ctx:this.ctx,
                blockScope: scope,
                toolbarInitiallyHidden:true,
                blockGroup: group,
                attachDirect:true,
                shouldTrackCollection: true,
                propertyStruct: {
                    currentCIView:null,
                    targetTop:null,
                    _lastItem:null
                },
                //resizeToParent:true,ue
                collection: store.filter({
                    group: group
                }),
                onCIChanged: function (ci, block, oldValue, newValue, field) {


                    console.log('on ci changed1212', arguments);


                    const _col= this.collection;

                    block = this.collection.getSync(block.id);
                    block.set(field, newValue);


                    block[field]=newValue;

                    //_col.refreshItem(block);

                    _col.emit('update',{
                        target:block,
                        type:'update'
                    });

                    block._store.emit('update',{
                        target:block
                    });

                    this.refresh();

                    this._itemChanged('changed',block,_col);



                }

                //dndConstructor: SharedDndGridSource,
                //dndConstructor:Dnd.GridSource,
                //____right:this.__right,
                //__docker:_docker,
                /*
                setPanelSplitPosition:widget.setPanelSplitPosition,
                getPanelSplitPosition:widget.getPanelSplitPosition*/
            },options);

            try {
                grid = utils.addWidget(gridClass, args, null, tab, false);
                this.completeGrid(grid, newItemLabel);
                grids.push(grid);
            }catch(e){
                logError(e);
            }
            tab.add(grid,null,false);
            return grid;
        },
        getGridClass:function(){


            if(this.gridClass){
                return this.gridClass;
            }

            this.gridClass = createGridClass();

            return this.gridClass;


            const propertyStruct = {
                currentCIView:null,
                targetTop:null,
                _lastItem:null
            };



            const gridClass = declare('driverGrid', BlockGrid,{
                toolbarInitiallyHidden:true,
                highlightDelay:1000,
                propertyStruct : propertyStruct,
                /**
                 *
                 * @param action {Action}
                 * @param editor {ACEEditor}
                 */
                openTerminal:function(action,editor){

                    const item = this.getSelectedItem() || {};

                    // already open
                    if(this._console){

                        utils.destroy(this._console);
                        utils.destroy(this.__bottom);
                        this.__bottom = null;
                        this._console = null;
                        this.getDocker().resize();
                        //this.getDocker().removePanel(this._console._parent);
                        return;
                    }

                    const ExpressionEditor = editor.delegate; // xide.widgets.ExpressionEditor

                    const ctx = this.ctx;
                    const deviceManager = ctx.getDeviceManager();
                    const right = this.getRightPanel('Properties',null,'DefaultTab',{});
                    const bottomPanel = this.getBottomPanel('Console',null,'DefaultTab',null,false);
                    const value = item.send || item.method;
                    const _console = deviceManager.openConsole(this.device,bottomPanel,{
                        value:value
                    });
                    this._lastConsoleItem = item;
                    bottomPanel.closeable(false);
                    this._console = _console;
                },
                getTypeMap:function(){

                    if(this.typeMap){
                        return this.typeMap;
                    }

                    const self = this;



                    const ExpressionEditorExtra = dcl(ExpressionEditor,{
                        EditorClass : dcl(ConsoleView.Editor,{
                            runAction:function(action){

                                console.error('run editor action',action.command);
                                if(action.command ==='Console/Terminal'){
                                    return self.openTerminal(action,this);
                                }

                                return this.inherited(arguments);

                            }
                        }),
                        onAddEditorActions:function(evt) {
                            const actions  = evt.actions, owner  = evt.owner;

                            const mixin = {
                                addPermission:true
                            };
                            actions.push(owner.createAction({
                                label: 'Send',
                                command: 'Console/Terminal',
                                icon: 'fa-terminal',
                                group: 'Console',
                                tab:'Home',
                                mixin:mixin,
                                handler:function(){

                                }
                            }));
                        }
                    });

                    const typeMap = {};

                    typeMap['xide.widgets.ExpressionEditor'] = ExpressionEditorExtra;


                    this.typeMap = typeMap;



                },
                /**
                 * Step/Run action
                 * @param block {block[]}
                 * @returns {boolean}
                 */
                _execute: function (blocks) {

                    const thiz = this;


                    function _clear(element){

                        if(element) {
                            setTimeout(function () {
                                element.removeClass('failedBlock successBlock activeBlock');
                            }, thiz.highlightDelay);
                        }
                    }

                    function _node(block){
                        if(block) {
                            const row = thiz.row(block);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    };

                    function mark(element,cssClass){

                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                        }
                    }

                    const dfds = [], EVENTS = types.EVENTS;

                    const _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                        mark(_node(evt),'activeBlock');

                    });
                    const _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        //console.log('mark success');
                        mark(_node(evt),'successBlock');
                        _clear(_node(evt));
                    });

                    const _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        mark(_node(evt),'failedBlock');
                        _clear(_node(evt));

                    });




                    function run(block) {

                        if (!block || !block.scope) {
                            console.error('have no scope');
                            return;
                        }
                        try {

                            const blockDfd = block.scope.solveBlock(block, {
                                highlight: true,
                                force: true,
                                listener:thiz
                            });

                            dfds.push(blockDfd);

                            /*
                             blockDfd.then(function(result){
                             console.log('did run! : ' + result);
                             });
                             */
                            //console.log('run block result:',result);


                        } catch (e)
                        {

                            console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                            //console.error(printStackTrace().join('\n\n'));
                        }
                        return true;
                    }

                    blocks  = _.isArray(blocks) ? blocks : [blocks];


                    function _patch(block){


                        block.runFrom=function(_blocks, index, settings)
                        {

                            const thiz = this, blocks = _blocks || this.items, allDfds = [];

                            const onFinishBlock = function (block, results) {
                                block._lastResult = block._lastResult || results;
                                thiz._currentIndex++;
                                thiz.runFrom(blocks, thiz._currentIndex, settings);
                            };


                            const wireBlock = function (block) {
                                block._deferredObject.then(function (results) {
                                    console.log('----def block finish');
                                    onFinishBlock(block, results);
                                });
                            };

                            if (blocks.length) {

                                for (let n = index; n < blocks.length; n++) {



                                    const block = blocks[n];

                                    console.log('run child \n'+block.method);



                                    /*
                                     _patch(block);
                                     var blockDfd = block.solve(this.scope, settings);
                                     allDfds.push(blockDfd);
                                     */
                                    _patch(block);

                                    if (block.deferred === true) {
                                        block._deferredObject = new Deferred();
                                        this._currentIndex = n;
                                        wireBlock(block);
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));
                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                        break;
                                    } else {
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));

                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                    }

                                }

                            } else {
                                this.onSuccess(this, settings);
                            }

                            return allDfds;
                        };

                        block.solve=function(scope,settings,run,error){

                            this._currentIndex = 0;
                            this._return=[];

                            const _script = '' + this._get('method');
                            const thiz=this,
                                  ctx = this.getContext(),
                                  items = this[this._getContainer()],
                                  //outer,head dfd
                                  dfd = new Deferred,
                                  listener = settings.listener,
                                  isDfd = thiz.deferred;



                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                            }

                            function _finish(result){

                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }

                                dfd.resolve(result);


                            }

                            function _error(result){
                                dfd.reject(result);
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                                }
                            }


                            function _headDone(result){
                                //console.log('_headDone : ',result);
                                //more blocks?
                                if(items.length) {
                                    const subDfds = thiz.runFrom(items,0,settings);
                                    all(subDfds).then(function(what){
                                        console.log('all solved!',what);
                                        _finish(result);
                                    },function(err){

                                        console.error('error in chain',err);
                                        if(listener) {
                                            listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                        }
                                        dfd.resolve(err);
                                    });

                                }else{
                                    if(listener) {
                                        listener._emit(types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                    }
                                    dfd.resolve(result);
                                }
                            }




                            if(_script && _script.length){
                                const runScript = function() {


                                    const _function = new Function("{" + _script + "}");
                                    const _args = thiz.getArgs() || [];
                                    try {

                                        if(isDfd){

                                            ctx.resolve=function(result){
                                                console.log('def block done');
                                                if(thiz._deferredObject) {
                                                    thiz._deferredObject.resolve();
                                                }
                                                _headDone(result);
                                            }
                                        }
                                        const _parsed = _function.apply(ctx, _args || {});
                                        thiz._lastResult = _parsed;
                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }

                                        if(!isDfd) {
                                            _headDone(_parsed);
                                        }

                                        if (_parsed !== 'false' && _parsed !== false) {

                                        } else {
                                            //thiz.onFailed(thiz, settings);
                                            //return [];
                                        }
                                    } catch (e) {

                                        e=e ||{};

                                        _error(e);

                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                };

                                if(scope.global){
                                    (function() {

                                        window = scope.global;


                                        const _args = thiz.getArgs() || [];
                                        try {
                                            let _parsed = null;
                                            if(!ctx.runExpression) {
                                                const _function = new Function("{" + _script + "}").bind(this);
                                                _parsed = _function.apply(ctx, _args || {});
                                            }else{
                                                _parsed = ctx.runExpression(_script,null,_args);
                                            }

                                            thiz._lastResult = _parsed;

                                            if (run) {
                                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                                            }
                                            if (_parsed !== 'false' && _parsed !== false) {
                                                thiz.onSuccess(thiz, settings);
                                            } else {
                                                thiz.onFailed(thiz, settings);
                                                return [];
                                            }


                                        } catch (e) {

                                            thiz._lastResult = null;
                                            if (error) {
                                                error('invalid expression : \n' + _script + ': ' + e);
                                            }
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }

                                    }).call(scope.global);

                                }else{
                                    runScript();
                                }

                            }else{
                                console.error('have no script');
                            }

                            return dfd;
                        }

                    }

                    _.each(blocks,_patch);

                    _.each(blocks,run);

                    all(dfds).then(function(){
                        console.log('did run all selected blocks!',thiz);
                        _runHandle.remove();
                        _successHandle.remove();
                        _errHandle.remove();
                    });

                    /*
                     function run(block) {
                     if (!block || !block.scope) {
                     console.error('have no scope');
                     return;
                     }
                     try {
                     var result = block.scope.solveBlock(block, {
                     highlight: true,
                     force: true
                     });
                     } catch (e) {
                     console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);
                     console.error(printStackTrace().join('\n\n'));
                     }
                     return true;
                     }

                     blocks  = _.isArray(blocks) ? blocks : [blocks];


                     _.each(blocks,run);
                     */

                },
                execute: function (_blocks) {

                    const thiz = this;

                    console.error('exe');

                    ////////////////////////////////////////////////////////////////////////
                    //
                    //  Visual helpers to indicate run status:
                    //
                    function _clear(element){
                        if(element) {
                            setTimeout(function () {
                                element.removeClass('failedBlock successBlock activeBlock');
                            }, thiz.highlightDelay);
                        }
                    }

                    function _node(block){
                        if(block) {
                            const row = thiz.row(block);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    };

                    function mark(element,cssClass){

                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                        }
                    }

                    const //all Deferreds of selected blocks to run
                          dfds = [],
                          //shortcut
                          EVENTS = types.EVENTS,
                          //normalize selection to array
                          blocks  = _.isArray(_blocks) ? _blocks : [_blocks],
                          //event handle "Run"
                          _runHandle = this._on(EVENTS.ON_RUN_BLOCK,function(evt){
                              mark(_node(evt),'activeBlock');
                          }),
                          //event handle "Success"
                          _successHandle = this._on(EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                              //console.log('marke success',evt);
                              mark(_node(evt),'successBlock');
                              _clear(_node(evt));
                          }),
                          //event handle "Error"
                          _errHandle = this._on(EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                              mark(_node(evt),'failedBlock');
                              _clear(_node(evt));
                          });






                    function run(block) {

                        if (!block || !block.scope) {
                            console.error('have no scope');
                            return;
                        }
                        try {

                            const blockDfd = block.scope.solveBlock(block, {
                                highlight: true,
                                force: true,
                                listener:thiz
                            });


                            dfds.push(blockDfd);

                            /*
                             blockDfd.then(function(result){
                             console.log('did run! : ' + result);
                             });
                             */
                            //console.log('run block result:',result);


                        } catch (e)
                        {
                            console.error(' excecuting block -  ' + block.name + ' failed! : ' + e);

                            logError(e,'excecuting block -  ' + block.name + ' failed! : ');

                            //console.error(printStackTrace().join('\n\n'));

                        }
                        return true;
                    }



                    function _patch(block){


                        block.runFrom=function(_blocks, index, settings){

                            const thiz = this, blocks = _blocks || this.items, allDfds = [];

                            const onFinishBlock = function (block, results) {
                                block._lastResult = block._lastResult || results;
                                thiz._currentIndex++;
                                thiz.runFrom(blocks, thiz._currentIndex, settings);
                            };

                            const wireBlock = function (block) {
                                block._deferredObject.then(function (results) {
                                    console.log('----def block finish');
                                    onFinishBlock(block, results);
                                });
                            };

                            if (blocks.length) {

                                for (let n = index; n < blocks.length; n++) {



                                    const block = blocks[n];

                                    console.log('run child \n'+block.method);

                                    _patch(block);

                                    if (block.deferred === true) {
                                        block._deferredObject = new Deferred();
                                        this._currentIndex = n;
                                        wireBlock(block);
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));
                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                        break;
                                    } else {
                                        //this.addToEnd(this._return, block.solve(this.scope, settings));

                                        var blockDfd = block.solve(this.scope, settings);
                                        allDfds.push(blockDfd);
                                    }

                                }

                            } else {
                                this.onSuccess(this, settings);
                            }

                            return allDfds;
                        };

                        block.solve=function(scope,settings,run,error){

                            this._currentIndex = 0;
                            this._return=[];

                            const _script = '' + this._get('method');

                            const thiz=this,
                                  ctx = this.getContext(),
                                  items = this[this._getContainer()],
                                  //outer,head dfd
                                  dfd = new Deferred,
                                  listener = settings.listener,
                                  isDfd = thiz.deferred;



                            //moved to Contains#onRunThis
                            if(listener) {
                                listener._emit(types.EVENTS.ON_RUN_BLOCK, thiz);
                            }

                            //function when a block did run successfully,
                            // moved to Contains#onDidRunItem
                            function _finish(dfd,result,event){

                                if(listener) {
                                    listener._emit(event || types.EVENTS.ON_RUN_BLOCK_SUCCESS, thiz);
                                }
                                dfd.resolve(result);


                            }

                            //function when a block did run successfully
                            function _error(result){
                                dfd.reject(result);
                                if(listener) {
                                    listener._emit(types.EVENTS.ON_RUN_BLOCK_FAILED, thiz);
                                }
                            }


                            //moved to Contains#onDidRunThis
                            function _headDone(result){


                                //more blocks?
                                if(items.length) {
                                    const subDfds = thiz.runFrom(items,0,settings);

                                    all(subDfds).then(function(what){
                                        console.log('all solved!',what);
                                        _finish(dfd,result);
                                    },function(err){
                                        console.error('error in chain',err);
                                        _finish(dfd,err);
                                    });

                                }else{
                                    _finish(dfd,result);
                                }
                            }


                            if(_script && _script.length){

                                const runScript = function() {

                                    const _function = new Function("{" + _script + "}");
                                    const _args = thiz.getArgs() || [];
                                    try {

                                        if(isDfd){

                                            ctx.resolve=function(result){
                                                console.log('def block done');
                                                if(thiz._deferredObject) {
                                                    thiz._deferredObject.resolve();
                                                }
                                                _headDone(result);
                                            }
                                        }
                                        const _parsed = _function.apply(ctx, _args || {});
                                        thiz._lastResult = _parsed;
                                        if (run) {
                                            run('Expression ' + _script + ' evaluates to ' + _parsed);
                                        }


                                        if(!isDfd) {
                                            console.log('root block done');
                                            //_headDone(_parsed);
                                            thiz.onDidRunThis(dfd,_parsed,items,settings);
                                        }

                                        if (_parsed !== 'false' && _parsed !== false) {

                                        } else {
                                            //thiz.onFailed(thiz, settings);
                                            //return [];
                                        }
                                    } catch (e) {

                                        e=e ||{};

                                        _error(e);

                                        if (error) {
                                            error('invalid expression : \n' + _script + ': ' + e);
                                        }
                                        //thiz.onFailed(thiz, settings);
                                        //return [];
                                    }
                                };

                                if(scope.global){
                                    (function() {

                                        window = scope.global;


                                        const _args = thiz.getArgs() || [];
                                        try {
                                            let _parsed = null;
                                            if(!ctx.runExpression) {
                                                const _function = new Function("{" + _script + "}").bind(this);
                                                _parsed = _function.apply(ctx, _args || {});
                                            }else{
                                                _parsed = ctx.runExpression(_script,null,_args);
                                            }

                                            thiz._lastResult = _parsed;

                                            if (run) {
                                                run('Expression ' + _script + ' evaluates to ' + _parsed);
                                            }
                                            if (_parsed !== 'false' && _parsed !== false) {
                                                thiz.onSuccess(thiz, settings);
                                            } else {
                                                thiz.onFailed(thiz, settings);
                                                return [];
                                            }


                                        } catch (e) {

                                            thiz._lastResult = null;
                                            if (error) {
                                                error('invalid expression : \n' + _script + ': ' + e);
                                            }
                                            thiz.onFailed(thiz, settings);
                                            return [];
                                        }

                                    }).call(scope.global);

                                }else{
                                    runScript();
                                }

                            }else{
                                console.error('have no script');
                            }
                            return dfd;
                        }


                    }

                    //_.each(blocks,_patch);

                    _.each(blocks,run);

                    all(dfds).then(function(){
                        console.log('did run all selected blocks!',thiz);
                        _runHandle.remove();
                        _successHandle.remove();
                        _errHandle.remove();
                    });



                },
                onCIChanged: function (ci, block, oldValue, newValue, field) {


                    console.log('on ci changed', arguments);
                    //var _col= this.collection;
                    //block = this.collection.getSync(block.id);
                    block.set(field, newValue);
                    block.refresh();


                    this._itemChanged('changed',block);
                },
                _itemChanged: function (type, item, store) {

                    store = store || this.getStore(item);


                    const thiz = this;

                    function _refreshParent(item, silent) {

                        const parent = item.getParent();
                        if (parent) {
                            const args = {
                                target: parent
                            };
                            if (silent) {
                                this._muteSelectionEvents = true;
                            }
                            store.emit('update', args);
                            if (silent) {
                                this._muteSelectionEvents = false;
                            }
                        } else {
                            thiz.refresh();
                        }
                    }

                    function select(item) {

                        thiz.select(item, null, true, {
                            focus: true,
                            delay: 20,
                            append: false
                        });
                    }

                    switch (type) {

                        case 'added':
                        {
                            //_refreshParent(item);
                            //this.deselectAll();
                            this.refresh();
                            select(item);
                            break;
                        }

                        case 'changed':
                        {
                            //this.refresh();
                            select(item);
                            break;
                        }


                        case 'moved':
                        {
                            //_refreshParent(item,true);
                            //this.refresh();
                            //select(item);
                            break;
                        }

                        case 'deleted':
                        {

                            const parent = item.getParent();
                            //var _prev = item.getPreviousBlock() || item.getNextBlock() || parent;
                            const _prev = item.next(null, -1) || item.next(null, 1) || parent;
                            if (parent) {
                                const _container = parent.getContainer();
                                if (_container) {
                                    _.each(_container, function (child) {
                                        if (child.id == item.id) {
                                            _container.remove(child);
                                        }
                                    });
                                }
                            }

                            this.refresh();
                            /*
                             if (_prev) {
                             select(_prev);
                             }
                             */
                            break;
                        }

                    }


                },
                _onFocusChanged:function(focused,type){
                    this.inherited(arguments);
                    if(!focused){
                        this._lastSelection = [];
                    }

                },
                save:function(){




                    const thiz = this,
                          driver = thiz.userData.driver,
                          ctx = thiz.userData.ctx,
                          fileManager = ctx.getFileManager(),
                          //instance scope
                          scope = thiz.blockScope,
                          instance = scope.instance,
                          //original driver scope
                          originalScope = driver.blockScope,
                          path = driver.path.replace('.meta.json','.xblox'),
                          scopeToSave = originalScope || scope,
                          mount = driver.scope;



                    if(originalScope && scopeToSave!=originalScope){
                        originalScope.fromScope(scope);
                    }

                    if (scope) {

                        const all = {
                            blocks: null,
                            variables: null
                        };

                        const blocks = scope.blocksToJson();
                        try {
                            //test integrity
                            dojo.fromJson(JSON.stringify(blocks));
                        } catch (e) {
                            console.error('invalid data');
                            return;
                        }

                        const _onSaved = function () {};

                        all.blocks = blocks;

                        console.log('saving driver ' + mount + '/'+path,driver);


                        fileManager.setContent(mount,path,JSON.stringify(all, null, 2),_onSaved);
                        //this.saveContent(JSON.stringify(all, null, 2), this._item, _onSaved);
                    }
                },
                runAction: function (action) {

                    const thiz = this;
                    const sel = this.getSelection();


                    console.error('run action ' + action.command);

                    function addItem(_class,group){


                        const dfd = new Deferred();

                        const cmd = factory.createBlock(_class, {
                            name: "No Title",
                            scope: thiz.blockScope,
                            group: group
                        });

                        const defaultDfdArgs = {
                            select: [cmd],
                            focus: true,
                            append: false
                        };



                        //cmd.refresh();
                        thiz.refresh().then(function(){
                            dfd.resolve(defaultDfdArgs);
                        });

                        return dfd;



                        //thiz.deselectAll();

                        /*
                        _.each(thiz.grids,function(grid){
                            if(grid) {
                                grid.refresh();
                            }
                        });
                        */
                        /*
                        setTimeout(function () {
                            thiz.select([cmd],null,true,{
                                focus:true
                            });
                        }, 200);
                        */

                    }


                    if (action.command == 'New/Command') {
                        return addItem(Command,this.blockGroup);
                    }

                    if (action.command == 'New/Variable') {
                        return addItem(Variable,'basicVariables');
                    }

                    if (action.command == 'File/Save') {
                        return this.save();
                    }
                    return this.inherited(arguments);
                },
                startup:function(){

                    const thiz = this;


                    this.inherited(arguments);

                    function _node(evt){
                        const item = evt.target;
                        if(item) {
                            const row = thiz.row(item);
                            if (row) {
                                const element = row.element;
                                if (element) {
                                    return $(element);
                                }
                            }
                        }
                        return null;
                    };


                    function mark(element,cssClass){
                        if(element) {
                            element.removeClass('failedBlock successBlock activeBlock');
                            element.addClass(cssClass);
                            setTimeout(function () {
                                element.removeClass(cssClass);
                                thiz._isHighLighting = false;
                            }, thiz.highlightDelay);
                        }
                    };

                    this.subscribe(types.EVENTS.ON_RUN_BLOCK,function(evt){
                        mark(_node(evt),'activeBlock');
                    });
                    this.subscribe(types.EVENTS.ON_RUN_BLOCK_FAILED,function(evt){
                        mark(_node(evt),'failedBlock');
                    });
                    this.subscribe(types.EVENTS.ON_RUN_BLOCK_SUCCESS,function(evt){
                        mark(_node(evt),'successBlock');
                    });


                    this.collection.on('update',function(evt){

                        const item = evt.target, node = _node(evt), type = evt.type;

                        if(type==='update' && evt.property ==='value'){
                            mark(node,'successBlock');
                        }

                        //console.warn('on store updated ', args);
                    });


                }
            });

            this.gridClass = gridClass;

            return gridClass;
        },
        createConsole:function(tab){

            const view = this.ctx.getDeviceManager().openConsole(this.device,tab);

            //tab.add(view,null,false);
            this.consoleView = view;


        },
        openLog:function(_item,where){




            // default grid args
            const thiz = this, ctx = thiz.ctx, deviceManager = ctx.getDeviceManager(), logManager = ctx.getLogManager(), store = logManager.store, item = _item || this.device, info = item.info || deviceManager.toDeviceControlInfo(item), storeId = info.host + '_' + info.port + '_' + info.protocol;

            logManager.getStore(storeId).then(function(_store){


                console.log('got log store : ',_store);






                const logGridArgs = {
                    ctx: ctx,
                    //attachDirect: true,
                    storeId:storeId,
                    delegate:logManager,
                    __getRootFilter:function(){

                        return {
                            show:true
                            /*host:info.host + ':' + info.port*/
                        }
                    },
                    collection:_store,
                    __collection: _store.filter({
                        /*show:true,
                        host:info.host + ':' + info.port*/
                    }).sort([{property: 'time', descending: true}])
                };
                /*
                var tab = docker.addTab(null,{
                    title: info.host,
                    icon:'fa-calendar'
                });
                */

                const grid = utils.addWidget(LogGrid,logGridArgs,null,where,true,'logGridView');


                //grid.set('collection',_store);
                //grid.set('collection',grid.collection.filter(grid.getRootFilter()).sort(grid.getDefaultSort()));


            });

        },
        createSettingsWidget:function(ci,where){

            const thiz = this;

            const settingsWidget = dcl(TemplatedWidgetBase,{
                templateString:'<div style="padding: 16px"/>',
                _getText: function (url) {
                    let result;
                    const def = dojo.xhrGet({
                        url: url,
                        sync: true,
                        handleAs: 'text',
                        load: function (text) {
                            result = text;
                        }
                    });
                    return '' + result + '';
                },
                startup:function(){


                    const params = utils.getJson(ci['params']);


                    const settings = utils.getJson(ci['params']) || {
                            constants: {
                                start: '',
                                end: ''
                            },
                            send: {
                                mode: false,
                                interval: 500,
                                timeout: 500,
                                onReply: ''
                            }
                        };



                    const settingsPane = utils.templatify(
                        null,
                        this._getText(require.toUrl('xcf/widgets/templates/commandSettingsNew2.html')),
                        this.domNode,
                        {
                            baseClass: 'settings',
                            start: settings.constants.start,
                            end: settings.constants.end,
                            interval: settings.send.interval,
                            timeout: settings.send.timeout,
                            sendMode: settings.send.mode,
                            onReply: settings.send.onReply,
                            settings: settings
                        }, null
                    );


                    if (settings.send.mode) {
                        $(settingsPane.rReply).prop('checked', true);
                    } else {
                        $(settingsPane.rInterval).prop('checked', true);
                    }




                    const _onSettingsChanged = function () {
                        //update params field of our ci
                        thiz.userData['params'] = JSON.stringify(settingsPane.settings);
                        //widget.setValue('{}');

                        console.dir(settingsPane.settings);

                    };

                    //start
                    $(settingsPane.start).on('change',function(e){
                        settingsPane.settings.constants.start = e.target.value;
                        _onSettingsChanged();
                    });

                    //end
                    $(settingsPane.end).on('change',function(e){
                        settingsPane.settings.constants.end = e.target.value;
                        _onSettingsChanged();
                    });






                    //mode
                    $(settingsPane.rReply).on("change", function (e) {
                        const value = e.target.value=='on' ? 1:0;
                        settingsPane.settings.send.mode = value;
                        _onSettingsChanged();
                    });

                    $(settingsPane.rInterval).on("change", function (e) {
                        const value = e.target.value=='on' ? 0:1;
                        settingsPane.settings.send.mode = value;
                        _onSettingsChanged();
                    });


                    //interval time
                    $(settingsPane.wInterval).on('change',function(e){
                        settingsPane.settings.send.interval = e.target.value;
                        _onSettingsChanged();
                    });






                    //on reply value
                    $(settingsPane.wOnReply).on('change',function(e){

                        settingsPane.settings.send.onReply = e.target.value;
                        _onSettingsChanged();
                    });

                    //onReply - timeout
                    $(settingsPane.wTimeout).on('change',function (e) {
                        settingsPane.settings.send.timeout = e.target.value;
                        _onSettingsChanged();
                    });

                }
            });

            const widget = utils.addWidget(settingsWidget,{
                userData:ci,
                resizeToParent:true
            },null,where,true);


            where.add(widget,null,false);



        },
        createWidgets:function(){

            const self = this,
                  ci = self.userData,
                  device = self.device,
                  driver = self.driver,
                  //original or device instance
                  scope = device ? device.blockScope : driver.blockScope;

            if(!scope){
                console.error('have no scope!');
                return ;
            }

            const store = scope.blockStore, instance = driver.instance;

            this.grids = [];





            if(this.showBasicCommands) {
                this.basicCommandsGrid = this.createGrid(this.basicCommandsTab, scope, store, 'basic', 'Command',{
                    registerView:true
                });
            }

            if(this.showConditionalCommands) {
                this.conditionalCommandsGrid = this.createGrid(this.conditionalTab, scope, store, 'conditional', 'Command',{
                    registerView:true
                });
            }


            if(this.showVariables) {
                this.variablesGrid = this.createGrid(this.variablesTab, scope, store, 'basicVariables', 'Command', {
                    columns: [
                        {
                            label: "Name",
                            field: "name",
                            sortable: true,
                            width: '20%',
                            editorArgs: {
                                required: true,
                                promptMessage: "Enter a unique variable name",
                                //validator: thiz.variableNameValidator,
                                //delegate: thiz.delegate,
                                intermediateChanges: false
                            }
                        },
                        {
                            label: "Initialize",
                            field: "initial",
                            sortable: false,
                            editOn: 'click'
                        },
                        {
                            label: "Value",
                            field: "value",
                            sortable: false,
                            editOn: 'click',
                            formatter: function (value, object) {
                                //console.log('format var ' + value,object);
                                return value;
                            },
                            editorArgs: {
                                autocomplete: 'on',
                                templateString: '<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"' +
                                '><div class="dijitReset dijitInputField dijitInputContainer"' +
                                '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"' +
                                '${!nameAttrSetting} type="${type}"' +
                                '/></div' +
                                '></div>'
                            }
                        }
                    ]
                });
                this.variablesTab.select();
            }

            this.responseTab && (this.responseGrid = this.createGrid(this.responseTab,scope,store,'conditionalProcess','Variable',{
                _columns:[
                    {
                        label: "Name",
                        field: "name",
                        sortable: true,
                        width:'20%',
                        editorArgs: {
                            required: true,
                            promptMessage: "Enter a unique variable name",
                            //validator: thiz.variableNameValidator,
                            //delegate: thiz.delegate,
                            intermediateChanges: false
                        }

                    },
                    {
                        label: "Initialize",
                        field: "initial",
                        sortable: false,

                        editOn:'click'
                    },
                    {
                        label: "Value",
                        field: "value",
                        sortable: false,
                        editOn:'click',
                        formatter:function(value,object){
                            //console.log('format var ' + value,object);
                            return value;
                        },
                        editorArgs: {
                            autocomplete:'on',
                            templateString:'<div class="dijit dijitReset dijitInline dijitLeft" id="widget_${id}" role="presentation"'+
                            '><div class="dijitReset dijitInputField dijitInputContainer"'+
                            '><input class="dijitReset dijitInputInner" data-dojo-attach-point="textbox,focusNode" autocomplete="on"'+
                            '${!nameAttrSetting} type="${type}"'+
                            '/></div'+
                            '></div>'
                        }
                    }
                ]
            }));

            if(this.showLog && this.device) {
                this.openLog(this.device, this.logTab);
            }


            this.showBasicCommands && this.basicCommandsTab.select();


            if(this.showSettings){
                this.createSettingsWidget(ci,this.settingsTab);
            }

            if(this.showConsole && this.device) {
                setTimeout(function () {
                    const _console = self.createConsole(self.consoleTab);
                    self.add(_console, null, false);
                }, 2000);
            }

        },
        startup:function(){

            const self = this,
                  ci = self.userData,
                  device = self.device,
                  driver = self.driver,
                  //original or device instance
                  scope = device ? device.blockScope : driver.blockScope;

                if(!scope){
                    console.error('have no scope!');
                    return ;
                }

                const store = scope.blockStore, instance = driver.instance;

            const _docker = this.createLayout();
            this._docker = _docker;
            this.createWidgets();

            _docker.resize();
        }
    });

    function openDriverView(driver,device,name){
        const parent = TestUtils.createTab(null,null,name || module.id);
        const title = 'Marantz Instance';
        let devinfo = null;
        const deviceManager = ctx.getDeviceManager();
        if(device){
            devinfo  = ctx.getDeviceManager().toDeviceControlInfo(device);
        }


        //@Todo:driver, store device temporarly in Commands CI
        const commandsCI = utils.getCIByChainAndName(driver.user, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
        if(commandsCI){
            commandsCI.device = device;
        }

        //ctx.getAction('Window/Navigation').handler(true);


        //deviceManager.openConsole = openConsole;

        const driverView = utils.addWidget(DriverViewClass,{
            userData:commandsCI,
            driver:driver,
            device:device,
            ctx:ctx
        },null,parent,true);

        parent.add(driverView,null,false);
    }

    function createLogGridClass(){


        const filterClass = declare('filter',null,{
            showFooter: true,
            buildRendering: function () {

                this.inherited(arguments);
                const grid = this;
                const filterNode = this.filterNode = domConstruct.create('div', {
                    className: 'dgrid-filter'
                }, this.footerNode);
                this.filterStatusNode = domConstruct.create('div', {
                    className: 'dgrid-filter-status'
                }, filterNode);
                const inputNode = this.filterInputNode = domConstruct.create('input', {
                    className: 'dgrid-filter-input',
                    placeholder: 'Filter (regex)...'
                }, filterNode);
                this._filterTextBoxHandle = on(inputNode, 'keydown', debounce(function () {
                    grid.set("collection", grid.collection);
                }, 250));
            },
            destroy: function () {
                this.inherited(arguments);
                if (this._filterTextBoxHandle) {
                    this._filterTextBoxHandle.remove();
                }
            },
            _setCollection: function (collection) {
                this.inherited(arguments);
                const value = this.filterInputNode.value;
                const renderedCollection = this._renderedCollection;
                if (renderedCollection && value) {
                    const rootFilter = new renderedCollection.Filter();
                    const re = new RegExp(value, "i");
                    const columns = this.columns;
                    const matchFilters = [];
                    for (const p in columns) {
                        if (columns.hasOwnProperty(p)) {
                            matchFilters.push(rootFilter.match(columns[p].field, re));
                        }
                    }
                    const combined = rootFilter.or.apply(rootFilter, matchFilters);
                    const filtered = renderedCollection.filter(combined);
                    this._renderedCollection = filtered;
                    this.refresh();
                }
            },
            refresh: function() {
                this.inherited(arguments);
                const value = this.filterInputNode.value;
                if (value) {
                    this.filterStatusNode.innerHTML = this.get('total') + " filtered results";
                }else {
                    this.filterStatusNode.innerHTML = "";
                }
            }


        });



        /**
         * Block grid base class.
         * @class module:xblox/views/Grid
         */
        const GridClass = Grid.createGridClass('log',
            {
                options: utils.clone(types.DEFAULT_GRID_OPTIONS)
            },
            //features
            {

                SELECTION: true,
                KEYBOARD_SELECTION: true,
                PAGINATION: types.GRID_FEATURES.PAGINATION,
                ACTIONS: types.GRID_FEATURES.ACTIONS,
                //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                FILTER:{
                    CLASS:KeyboardNavigation
                },
                SEARCH:{
                    CLASS:Search
                }

            },
            {
                //base flip
                RENDERER: ListRenderer

            },
            {
                //args
                /*renderers: renderers,
                 selectedRenderer: TreeRenderer*/
            },
            {
                GRID: OnDemandGrid,
                EDITOR: null,
                LAYOUT: Layout,
                DEFAULTS: Defaults,
                RENDERER: ListRenderer,
                EVENTED: EventedMixin,
                FOCUS: Focus

            }
        );



        return GridClass;

    }


    function createLogEntry(){

        const logManager = ctx.getLogManager();
        const store = logManager.store;

        /*
         var message={
         message:'test',
         level:'info',
         type:'device',
         details:{},
         terminatorMessage:null
         };
         factory.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, message);*/

        //logManager.addLoggingMessage()
    }

    function addLog(tab,driver,device){

        const logManager = ctx.getLogManager();
        const store = logManager.store;

        var logGridClass = createLogGridClass();


        if(device){
            console.log('device : ', device);
        }




        // default grid args
        const gridArgs = {
            ctx:ctx,
            attachDirect:true,
            collection: store.filter({
                show:true,
                host:device.info.host + ':' + device.info.port
            })
        };


        const items = store.query({
            show:true,
            host:"192.168.1.20:23"
        });






        var logGridClass = declare("xlog.views.LogView", logGridClass, {
            _columns: {
                "Level": true,
                "Type": false,
                "Message": true,
                "Time": false
            },
            permissions: [
                //ACTION.EDIT,
                ACTION.RELOAD,
                ACTION.DELETE,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.PREVIEW,
                ACTION.SAVE,
                ACTION.SEARCH
            ],
            postMixInProperties: function () {
                this.columns = this.getColumns();
                return this.inherited(arguments);
            },
            formatDateSimple: function (data, format) {

                const momentUnix = moment.unix(data);

                return momentUnix.format("MMMM Do, h:mm:ss a");
            },
            getDefaultSort:function(){
                return [{property: 'time', descending: true}];
            },
            getMessageFormatter: function (message, item) {
                const thiz = this;
                if(item.progressHandler && !item._subscribedToProgress){
                    item.progressHandler._on('progress',function(_message){
                        thiz.refresh();
                    });
                    item._subscribedToProgress = true;
                }

                let _isTerminated = item.terminatorMessage !==null && item._isTerminated===true;
                if(!item.terminatorMessage){
                    _isTerminated = true;
                }

                if (!_isTerminated) {
                    return '<span class=\"fa-spinner fa-spin\" style=\"margin-right: 4px\"></span>' + message;
                }
                return message;
            },
            getColumns: function () {
                const thiz = this;
                const columns = {
                    Level: {
                        field: "level", // get whole item for use by formatter
                        label: "Level",
                        sortable: true,
                        formatter: function (level) {

                            switch (level) {
                                case 'info':
                                {
                                    return '<span class="text-info" style=\"">' + level + '</span>';
                                }
                                case 'error':
                                {
                                    return '<span class="text-danger" style=\"">' + level + '</span>';
                                }
                                case 'warning':
                                {
                                    return '<span class="text-warning" style=\"">' + level + '</span>';
                                }
                            }
                            return level;
                        }

                    },
                    Type: {
                        field: "type", // get whole item for use by formatter
                        label: "Type",
                        sortable: true
                    },
                    Host: {
                        field: "host", // get whole item for use by formatter
                        label: "Host",
                        sortable: true
                    },
                    Message: {
                        field: "message", // get whole item for use by formatter
                        label: "Message",
                        sortable: true,
                        formatter: function (message, item) {
                            return thiz.getMessageFormatter(message, item)
                        }
                    },
                    Time: {
                        field: "time", // get whole item for use by formatter
                        label: "Time",
                        sortable: true,
                        formatter: function (time) {
                            return thiz.formatDateSimple(time / 1000);
                        }
                    },
                    Details:{
                        field: "details", // get whole item for use by formatter
                        label: "Details",
                        sortable: false,
                        hidden:true
                        /*editor:RowDetailEditor*/
                    }
                };








                if (!this.showSource) {
                    delete columns['Host'];
                }
                return columns;
            },
            startup:function(){

                this.inherited(arguments);

                const thiz = this, permissions = this.permissions;

                if (permissions) {

                    const _defaultActions = DefaultActions.getDefaultActions(permissions, this);
                    //_defaultActions = _defaultActions.concat(this.getBlockActions(permissions));

                    this.addActions(_defaultActions);
                    //this.onContainerClick();
                }

            }
        });
        //tab.select();
        const grid = utils.addWidget(logGridClass,gridArgs,null,tab,false,'logGridView');
        grid.startup();


    }

    const _ExpressionEditor = dcl([WidgetBase,_LayoutMixin.dcl,ConsoleView.HandlerClass],{
        type:'javascript',
        reparent:false,
        consoleTabTitle:'Editor',
        consoleTabOptions:null,
        consoleTabType:'DefaultTab',
        templateString: '<div class="" style="height: inherit">' +
        '</div>',
        onAddEditorActions:function(evt){

            console.error('onAddEditorActions',arguments);
            //this.inherited(arguments);

            const actions  = evt.actions, owner  = evt.owner;

            const mixin = {
                addPermission:true
            };
            actions.push(owner.createAction({
                label: 'Send',
                command: 'Console/Send',
                icon: 'fa-paper-plane',
                group: 'Console',
                tab:'Home',
                mixin:mixin,
                handler:function(){
                    debugger;
                }
            }));

        },
        getConsoleClass:function(){
            return ConsoleView.ConsoleWidget;
        },
        showDocumentation: function (text) {

            dojo.empty(this.helpContainer);
            const help = dojo.create('div', {
                innerHTML: '',
                className: 'ui-widget'
            });
            this.helpContainer.appendChild(help);
            const textText = dojo.create('span', {
                innerHTML: '<pre>'+text +'</pre>',
                className: 'ui-content'
            });
            help.appendChild(textText);
        },
        createTab:function(type,args){

            const DOCKER = types.DOCKER;
            const defaultTabArgs = {
                icon:false,
                closeable:true,
                moveable:true,
                tabOrientation:DOCKER.TAB.TOP,
                location:DOCKER.DOCK.STACKED

            };
            return this._docker.addTab(type || 'DefaultTab',utils.mixin(defaultTabArgs,args));
        },
        createBranch: function (name, where) {
            const branch = {
                name: name,
                group: 'top',
                children: [],
                value: null
            };
            where.push(branch);
            return branch;

        },
        apiItemToLeaf: function (item) {
            const result = {};
        },
        addAutoCompleterWord: function (text, help) {

            if (!this.autoCompleterWords) {
                this.autoCompleterWords = [];
            }

            this.autoCompleterWords.push({
                word: text,
                value: text,
                meta: help || ''
            });

        },
        createLeaf: function (label, value, parent, help, isFunction, where) {
            const leaf = {
                name: label,
                group: 'leaf',
                value: value,
                parent: parent,
                help: help,
                isFunction: isFunction != null ? isFunction : true
            };

            this.addAutoCompleterWord(value, help);
            if (where) {
                where.children.push({
                    _reference: label
                })
            }
            return leaf;
        },
        getAPIField: function (item, title) {

            for (let i = 0; i < item.sectionHTMLs.length; i++) {
                const obj = item.sectionHTMLs[i];

                if (obj.indexOf("id = \"" + title + "\"") != -1) {
                    return obj;
                }
            }

            return '';
        },
        insertApiItems: function (startString, dst, where) {

            const result = [];
            for (let i = 0; i < this.api.length; i++) {
                const item = this.api[i];

                if (item.title.indexOf(startString) != -1) {
                    /*result.push(item);*/
                    const leafData = this.apiItemToLeaf(item);

                    const title = item.title.replace(startString, '');
                    let description = this.getAPIField(item, 'Description');
                    const parameters = this.getAPIField(item, 'Parameters');
                    const examples = this.getAPIField(item, 'Examples');
                    let syntax = this.getAPIField(item, 'Syntax');

                    description = description + '</br>' + parameters + '<br/>' + examples;
                    syntax = utils.strip_tags(syntax);
                    syntax = syntax.replace('Syntax', '');
                    syntax = utils.replaceAll('\n', '', syntax);
                    syntax = utils.replaceAll('\r', '', syntax);
                    where.push(this.createLeaf(title, syntax, startString.replace('.', ''), description, true, dst));

                }

            }

            return result;
        },
        _createExpressionData: function () {

            const data = {
                items: [],
                identifier: 'name',
                label: 'name'
            };

            const _Array = this.createBranch('Array', data.items);
            this.insertApiItems('Array.', _Array, data.items);

            const _Date = this.createBranch('Date', data.items);
            this.insertApiItems('Date.', _Date, data.items);

            const _Function = this.createBranch('Function', data.items);
            this.insertApiItems('Function.', _Function, data.items);

            const _Math = this.createBranch('Math', data.items);
            this.insertApiItems('Math.', _Math, data.items);

            const _Number = this.createBranch('Number', data.items);
            this.insertApiItems('Number.', _Number, data.items);

            const _Object = this.createBranch('Object', data.items);
            this.insertApiItems('Object.', _Object, data.items);

            const _String = this.createBranch('String', data.items);
            this.insertApiItems('String.', _String, data.items);

            const _RegExp = this.createBranch('RegExp', data.items);
            this.insertApiItems('RegExp.', _RegExp, data.items);

            this.publish(types.EVENTS.ON_EXPRESSION_EDITOR_ADD_FUNCTIONS, {
                root: data,
                widget: this,
                user: this.userData
            });

            console.dir(data);

            return data;
        },
        _createExpressionStore: function () {

            const store = declare('apiStore',[TreeMemory],{
                __fetchRange:function(){
                    // dstore/Memory#fetchRange always uses fetchSync, which we aren't extending,
                    // so we need to extend this as well.
                    console.log('fetchRange : ',this._state);
                    const results = this._fetchRange();
                    return new QueryResults(results.then(function (data) {
                        return data;
                    }), {
                        totalLength: results.then(function (data) {
                            return data.length;
                        })
                    });
                },
                __fetchRange: function (kwArgs) {

                    const deferred = new Deferred();

                    let _res = this.fetchRangeSync(kwArgs);

                    if(this._state.filter){

                        //the parent query
                        if(this._state && this._state.filter && this._state.filter['parentId']) {
                            var _item = this.getSync(this._state.filter.parentId);
                            if (_item) {
                                this.reset();
                                var _items = _item.items;

                                if(_item.getChildren){
                                    _items = _item.getChildren();
                                }

                                deferred.resolve(_items);
                                _res = _items;
                            }
                        }

                        //the group query
                        if(this._state && this._state.filter && this._state.filter['group']) {
                            var _items = this.getSync(this._state.filter.parent);
                            if (_item) {
                                this.reset();
                                _res=_item.items;
                            }
                        }
                    }
                    deferred.resolve(_res);

                    //return _res;
                    return deferred;
                },
                getChildren: function (parent, options) {
                    return this.query({parent: this.getIdentity(parent)});
                },
                mayHaveChildren: function (parent) {
                    return false;
                }
            });


            return new store({
                data: this._createExpressionData().items,
                idProperty:'name'
            });
        },
        createWidgets:function(){
            const thiz = this;



            const DOCKER = types.DOCKER;
            this._docker = Docker.createDefault(this.domNode,{

            });

            const top = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false
            });


            const _console = utils.addWidget(ConsoleView,{
                type:'javascript',
                value:'return 2+2;'
            },this,top,true);


            const docs = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM,
                h:'300px'

            });
            this.helpContainer = docs.containerNode;


            docs.minSize(200, 100);

            //docs.initSize(200, 100);



            const bottom = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.BOTTOM,
                location:DOCKER.TAB.BOTTOM
            });

            //bottom.initSize(200, 150);
            //docs.initSize(200, 160);
            bottom.maxSize(200);
            bottom.getSplitter().pos(0.8);


            const bottomR = this.createTab(null,{
                title:false,
                icon:'fa-long-arrow-right',
                closeable:false,
                tabOrientation:DOCKER.TAB.RIGHT,
                location:DOCKER.TAB.RIGHT,
                target:bottom
            });



            const _gridClass = Grid.createGridClass('driverTreeView',{
                    options: utils.clone(types.DEFAULT_GRID_OPTIONS)
                },
                //features
                {
                    SELECTION: true,
                    KEYBOARD_SELECTION: true,
                    PAGINATION: false,
                    ACTIONS: types.GRID_FEATURES.ACTIONS,
                    //CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                    //TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                    CLIPBOARD: types.GRID_FEATURES.CLIPBOARD
                },
                {
                    //base flip


                },
                {
                    //args

                },
                {
                    GRID: OnDemandGrid,
                    DEFAULTS: Defaults,
                    RENDERER: ListRenderer,
                    EVENTED: EventedMixin,
                    FOCUS: Focus
                }
            );


            const store = this._createExpressionStore();

            const gridArgs = {
                ctx:ctx,
                attachDirect:true,
                collection: store.filter({
                    group:'top'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };

            grid = utils.addWidget(_gridClass,gridArgs,null,bottom,true);



            /**************************************************************/

            const gridArgsRight = {
                ctx:ctx,
                noDataMessage:'',
                attachDirect:true,
                collection: store.filter({
                    group:'nada'
                }),
                showHeader: false,
                columns: [
                    {
                        label: "Name",
                        field: "name",
                        sortable: true
                    }
                ]
            };



            const gridRight = utils.addWidget(_gridClass,gridArgsRight,null,bottomR,true);


            grid._on('selectionChanged',function(evt){
                //console.error('on changed selection ',arguments);

                const selection = evt.selection;
                const item = selection[0];

                console.log('filter right',item);
                //console.dir();

                const right = store.query({
                    parent:item.name
                });

                gridRight.set("collection", store.filter({parent: 'xx'}));

                gridRight.renderArray([]);
                gridRight.renderArray(right);
            });

            gridRight._on('selectionChanged',function(evt){
                const selection = evt.selection;
                const item = selection[0];
                thiz.currentItem = item;

                if (item && item.help) {
                    thiz.showDocumentation(item.help || "");
                }
            });
/*
            docs.minSize();
            docs.maxSize();

            //debugger;
            bottom.maxSize();
            bottom.minSize();

            */

            bottom.getSplitter().pos(0.5);
            docs.getSplitter().pos(0.8);

            _console.__bottom.getSplitter().pos(0.2);
            _console.__bottom.title(false);
            _console.__masterPanel.title(false);

            const _p =docs.$container.find('.panelParent').css('overflow','auto');


            docs.$container.parent().css({
                overflow:'auto'
            })

        },
        startup:function(){

            const thiz = this;
            this.inherited(arguments);
            //pull in ExpressionJavaScript syntax and documentation, we hide here 'require' from the compiler, otherwise its adding 2.9MB to the final layer
            const _re = require;

            _re([
                'xide/widgets/ExpressionJavaScript'], function (ExpressionJavaScript) {
                thiz.api = ExpressionJavaScript.prototype.api;
                thiz.createWidgets();
            });
        }
    });



    function openExpressionEditor(){



        const parent = TestUtils.createTab(null,null,module.id);


        const expressionEditor = utils.addWidget(_ExpressionEditor,{
            userData:{
                value:'return 2+2;'
            }
        },this,parent,true);



    }

    if (ctx && doTests) {

        console.clear();


        blockManager = ctx.getBlockManager();
        driverManager = ctx.getDriverManager();
        let marantz  = driverManager.getItemById("235eb680-cb87-11e3-9c1a-0800200c9a66");

        marantz = driverManager.store.getSync("Marantz/My Marantz.meta.json_instances_instance_Marantz/Marantz.20.meta.json") || marantz;

        const driver = marantz.driver || marantz;
        const device = marantz.device;

        const deviceManager = ctx.getDeviceManager();


        openDriverView(driver,device);


        //openExpressionEditor();

        return Grid;
    }

    DriverViewClass.BlockGridClass = createGridClass();


    return DriverViewClass;
});