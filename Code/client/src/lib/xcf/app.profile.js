/**
 * This is a new Dojo 1.7 style build profile. Look at util/build/buildControlDefault.js if you want to explore the
 * default Dojo build profile options.
 */
// This function is used to determine whether or not a resource should be tagged as copy-only. See the resourceTags
// property below for more information.
function copyOnly(mid) {
    return mid in {
        // There are no modules right now in dojo boilerplate that are copy-only. If you have some, though, just add
        // them here like this:
        'xide/resources': 1,
        'cssMobile': 1
    };
}

var profile = {
    optimizeOptions: {
        languageIn: 'ECMASCRIPT6',
        languageOut: 'ECMASCRIPT5',
    },
    // basePath is relative to the directory containing this profile file; in this case, it is being set to the
    // src/ directory, which is the same place as the baseUrl directory in the loader configuration. (If you change
    // this, you will also need to update run.js).
    basePath: '../',
    packages: [{
            name: "davinci",
            location: "davinci"
        },
        {
            name: 'dojo',
            location: 'dojo'
        },
        {
            name: 'dijit',
            location: 'dijit'
        }
    ],
    // This is the directory within the release directory where built packages will be placed. The release directory
    // itself is defined by build.sh. You really probably should not use this; it is a legacy option from very old
    // versions of Dojo (like, version 0.1). If you do use it, you will need to update build.sh, too.
    // releaseName: '',

    // Builds a new release.
    action: 'release',

    // Strips all comments from CSS files.
    //cssOptimize: 'comments',

    // Excludes tests, demos, and original template files from being included in the built version.
    mini: true,

    // Uses Closure Compiler as the JavaScript minifier. This can also be set to "shrinksafe" to use ShrinkSafe.
    // Note that you will probably get some “errors” with CC; these are generally safe to ignore, and will be
    // fixed in a later version of Dojo. This defaults to "" (no compression) if not provided.
    optimize: 'closure',

    // We’re building layers, so we need to set the minifier to use for those, too. This defaults to "shrinksafe" if
    // it is not provided.
    layerOptimize: 'closure',

    // Strips all calls to console functions within the code. You can also set this to "warn" to strip everything
    // but console.error, and any other truthy value to strip everything but console.warn and console.error.
    stripConsole: 'warn',

    // The default selector engine is not included by default in a dojo.js build in order to make mobile builds
    // smaller. We add it back here to avoid that extra HTTP request. There is also a "lite" selector available; if
    // you use that, you’ll need to set selectorEngine in app/run.js too. (The "lite" engine is only suitable if you
    // are not supporting IE7 and earlier.)
    selectorEngine: 'lite',

    // Builds can be split into multiple different JavaScript files called “layers”. This allows applications to
    // defer loading large sections of code until they are actually required while still allowing multiple modules to
    // be compiled into a single file.
    layers: {
        // This is the main loader module. It is a little special because it is treated like an AMD module even though
        // it is actually just plain JavaScript. There is some extra magic in the build system specifically for this
        // module ID.
        'dojo/dojo': {
            // In addition to the loader (dojo/dojo) and the loader configuration file (app/run), we’re also including
            // the main application (app/main) and the dojo/i18n and dojo/domReady modules because they are one of the
            // conditional dependencies in app/main (the other being app/Dialog) but we don’t want to have to make
            // extra HTTP requests for such tiny files.
            include: [
                'dojo/dojo',
                'dojo/text',
                'dojo/domReady',
                'dojo/i18n',
                'dojo/_base/lang',
                'dojo/dom-class',
                'dojo/parser',
                'dojo/_base/window',
                'dojo/Deferred',
                'xdojo/declare',
                'xace/views/Editor', //  <--- important to be before xide/types/Types. No idea why


                'xide/manager/Application_XFILE',
                'xide/manager/Application_XBLOX',

                'xide/widgets/RichTextWidget',
                'xide/widgets/ToggleButton',
                'xide/widgets/EditBox',
                'xide/widgets/Reference',
                'xide/widgets/Ribbon',
                'xide/widgets/StructuralWidget',
                'xide/widgets/_MenuMixin4',
                'xide/widgets/_WidgetPickerMixin',
                'xide/widgets/NativeWidget',
                'xide/manager/Application_XNODE',
                'xide/manager/WindowManager',
                'xide/manager/NotificationManager',
                'xide/manager/Context_UI',
                'xide/mixins/_State',
                'xide/mixins/ResourceMixin',
                'xide/views/_Console',
                'xide/views/_ConsoleWidget',
                'xide/views/_LayoutMixin',
                'xide/views/_MainView',
                'xide/views/Dialog',
                'xide/views/TextEditor',
                'xide/views/_CIDialog',
                'xide/views/ACEEditor',
                'xide/views/JsonEditor',
                'xide/views/WelcomeGrid',
                'xide/views/_ActionMixin',
                'xide/views/JsonEditor',
                'xide/widgets/_MenuKeyboard',
                'xide/widgets/JSONDualEditorWidget',
                'xide/widgets/ExpressionJavaScript',
                'xide/wizard/WizardBase',
                "xide/json/JSONEditor",
                'xide/Wizards',
                'xide/widgets/MainMenu',
                'xide/widgets/JSONEditorWidget',
                'xide/widgets/IconPicker',
                'xide/container/_PaneBase',
                'xide/layout/Container',
                'xide/layout/ContentPane',
                'xide/layout/_Accordion',
                'xide/layout/_TabContainer',
                'xide/Lang',
                'xide/debug',
                'xide/manager/Application_UI',
                'xide/widgets/ArgumentsWidget',
                'xide/views/ActionDialog',
                'xide/interface/Track',
                'xide/views/CIGroupedSettingsView',
                'xide/widgets/DomStyleProperties',
                'xide/serverDebug',
                'xide/widgets/FileWidget',
                'xide/Features',
                'xide/manager/Application_XIDEVE',
                'xide/types/Types',
                'xide/manager/Router',

                'xide/registry',
                'xide/types',
                'xide/utils',
                'xide/factory',

                "xide/data/Reference",
                'xide/views/_CIDialog',
                "xide/data/Reference",
                'xide/action/ActionContext',
                'xide/editor/Base',
                'xide/encoding/_base',
                'xide/factory/Events',
                'xide/factory/Objects',
                'xide/manager/ContextBase',
                'xide/rpc/AdapterRegistry',
                'xide/rpc/JsonRPC',
                'xide/rpc/Service',
                'xide/utils/CIUtils',
                'xide/utils/StoreUtils',
                'xide/utils/StringUtils',
                'xide/utils/WidgetUtils',
                'xide/utils/HTMLUtils',
                'xide/utils/CSSUtils',
                'xide/popup',
                'xide/action/Action',
                'xide/json/ContextMenu',
                'xide/json/util',
                'xide/json/Highlighter',
                'xide/json/History',
                'xide/json/JSONEditor',
                'xide/json/Node',
                'xide/json/SearchBox',
                'xide/json/TreeMode',
                'xide/json/appendNodeFactory',
                'xaction/types',
                'xblox/embedded_ui',

                'xnode/component',
                'xnode/types',
                'xnode/xnode',
                'xnode/views/NodeServiceView',
                'xnode/manager/NodeServiceManager',
                'xnode/manager/NodeServiceManagerUI',

                'xblox/views/BlocksFileEditor',
                'xblox/views/Grid',
                'xblox/views/ThumbRenderer',
                'xblox/BlockActions',
                'xblox/component',

                'dstore/Cache',

                'dojo/debounce',
                'dojo/uacss',




                'xcf/types/Types',
                'xcf/Widgets',
                'xcf/Managers',
                'xcf/Views',
                'xcf/XCFCommons',
                'xcf/manager/Context',
                'xcf/GUIALL',
                'xcf/manager/BlockManager',
                'xconsole/types',
                'xcf/manager/BeanManager',
                'xcf/manager/DeviceManager_DeviceServer',
                'xcf/manager/DeviceManager_Server',
                'xcf/manager/DriverManager_Server',
                'xcf/types',
                'xcf/views/DeviceTreeView',
                'xcf/views/DriverTreeView',
                'xcf/views/GlobalConsole',
                'xcf/views/DriverView_Layout',
                'xcf/views/ProtocolTreeView',
                'xcf/views/_MainView',
                'xcf/widgets/LoggingWidget',
                'xcf/mixins/LogMixin',
                'xcf/model/Device',
                'xcf/model/Driver',
                'xcf/model/DefaultDevice',
                'xcf/model/ModelBase',
                'xcf/manager/DriverManager',
                'xcf/manager/DriverManager_UI',
                'xcf/views/DeviceTreeView_VE',
                'xcf/manager/Application_UI',
                'xcf/manager/Application_UI',
                'xcf/manager/DeviceManager_UI',
                'xcf/views/DriverConsole',
                'xcf/views/ExpressionConsole',
                'xcf/views/DriverView2',
                'xcf/widgets/CommandPicker',


                'dstore/Tree',
                'xaction/ActionContext',

                'dgrid/extensions/ColumnReorder',
                'dgrid/extensions/ColumnResizer',
                'dgrid/extensions/Pagination',




                'dijit/dijitb',
                'xace/views/Editor',



                'xlog/views/LogGrid',

                'xlog/xlog',
                'xideve/xideve',
                'xgrid/xgrid',
                'dgrid/dgrid',
                'xide/xide',
                'xfile/xfile',

                'xdocker/xdocker',


                'xlang/i18',

                'xfile/component',
                'xfile/FileActions',
                'xfile/FolderSize',
                'xfile/Statusbar',
                'xfile/ThumbRenderer',
                'xfile/data/Store',
                'xfile/factory/Store',
                'xfile/manager/BlockManager',
                'xfile/manager/FileManager',
                'xfile/manager/FileManagerActions',
                'xfile/manager/MountManager',
                'xfile/manager/Electron',
                'xfile/model/File',
                'xfile/views/FileConsole',
                'xfile/views/FileGrid',
                'xfile/views/FilePicker',
                'xfile/views/FilePreview',
                'xfile/views/FileSize',
                'xfile/views/Grid',
                'xfile/views/UploadMixin',

                'davinci/main',
                'davinci/davinci_',
                'davinci/ve/utils/pseudoClass',
                'davinci/ui/widgets/OutlineTree',
                'davinci/ui/dnd/DragManager',
                'davinci/model/resource/Folder',
                'davinci/ve/metadata',
                'davinci/de/resource',
                'davinci/de/DijitTemplatedGenerator',
                'davinci/de/widgets/NewDijit',
                'davinci/ve/actions/ReplaceAction',
                'davinci/repositoryinfo',
                //ve - extras
                "dgrid/Editor",

                "xcf/manager/WidgetManager",
                "xideve/Embedded",
                "xideve/component",
                "xideve/deliteTemplate",
                "xideve/stub",
                "xideve/metadata/dojo/1.8/callbacks",
                'xideve/types',
                'xideve/views/VisualEditor',
                'xideve/manager/WidgetManager',
                'xideve/manager/ContextManager',
                'xideve/Templates',
                'xideve/Template',
                "xwire/Binding",
                "xwire/Source",
                "xwire/Target",
                "xwire/WidgetSource",
                "xwire/WidgetTarget",
                "xwire/EventSource",
                "xwire/DeviceTarget",

                "system/resource",
                "preview/loadIndicator",
                "preview/silhouetteiframe",
                'dojo/cldr/supplemental',
                'dojo/date',
                'dojo/date/locale',

                'xblox/views/BlockGrid',
                'xblox/manager/BlockManagerUI',
                'xblox/model/Block_UI',
                'xblox/model/html/SetState',

                'dojox/dojox',
                'wcDocker/wcDocker',
                'xaction/xaction',
                'xblox/xblox',
                'xace/xace',
                'xwire/xwire',

                //'xide/widgets/EditBox',
                //'xide/layout/BorderContainer',


                /*
                 'xideve/delite/Context',
                 "xideve/delite/_ContextMobile",
                 "xideve/delite/_ContextTheme",
                 "xideve/delite/_ContextCSS",
                 "xideve/delite/_ContextJS",
                 "xideve/delite/_ContextDelite",
                 "xideve/delite/_ContextDojo",
                 "xideve/delite/_ContextInterface",
                 "xideve/delite/_ContextDocument",
                 "xideve/delite/_ContextWidgets",
                 "xideve/xblox/Wizards",
                 */

                "xideve/xblox/BlockInput",
                "xideve/xblox/ContainerInput",
                "xideve/xblox/Wizards",
                "xideve/metadata/delite/0.8/delite/Helper",
                "xideve/metadata/dojo/1.8/callbacks",
                "nxapp/protocols/ProtocolBase",
                "nxapp/utils/FileUtils",
                "xfile/views/FileGridLight",
                "xfile/data/DriverStore",
                "xblox/model/File/ReadJSON",
                "x-markdown/component",
                "x-markdown/views/MarkdownGrid",
                "dijit/form/Button",
                'dijit/form/Select',
                "dijit/Menu",
                "dijit/MenuBar",
                "dijit/MenuBarItem",
                "dijit/Tooltip",
                "dijit/TooltipDialog",
                'xcf/main',
                "davinci/lang/workbench",
                "davinci/lang/ve",
                "dijit/lang/_common",
                'xtrack/component',
                'xtrack/manager/TrackingManager',

                'xblox/views/BlockGridPalette',
                'xtrack/manager/TrackingManager',
                'xtrack/component',

                'davinci/ve/ThemeModifier',
                'davinci/ve/tools/SelectTool',
                'davinci/ve/ChooseParent',
                'davinci/ve/Snap',
                'davinci/ve/Focus',
                'davinci/ui/dnd/DragSource',
                'davinci/workbench/Preferences',
                'davinci/ve/tools/_Tool',
                'davinci/ve/DijitWidget',
                'davinci/ve/GenericWidget',
                'davinci/ve/DeliteWidget',
                'davinci/ve/HTMLWidget',
                'davinci/ve/ObjectWidget',
                'davinci/maqetta/AppStates',
                'davinci/ve/commands/EventCommand',
                'davinci/ve/utils/StyleArray',
                'davinci/ve/commands/_hierarchyCommand',
                'davinci/ve/utils/ImageUtils',
                'davinci/ve/commands/ResizeCommand',
                'davinci/ve/commands/MoveCommand',
                'davinci/ve/_Widget',
                'davinci/html/CSSModel',
                'davinci/lang/ve/ve',
                'dijit/layout/BorderContainer',
                'davinci/XPathUtils',
                'davinci/ve/commands/AppStateCommand',
                'davinci/workbench/WidgetLite',
                'davinci/ve/widgets/FontDataStore',
                'davinci/ve/widgets/FontComboBox',
                'davinci/ve/widgets/MultiInputDropDown',
                'dijit/form/ComboBox',
                'davinci/ve/widgets/MetaDataStore',
                'davinci/ve/widgets/ColorPicker',
                'davinci/ve/widgets/Background',
                'davinci/workbench/ViewLite',
                'dijit/form/ValidationTextBox',
                'davinci/Theme',
                'dijit/form/DateTextBox',
                'dijit/form/TimeTextBox',
                'davinci/ve/actions/_AddManageStatesWidget',
                'davinci/ve/actions/_ManageStatesWidget',
                'davinci/ve/VisualEditor',
                'davinci/ve/VisualEditorOutline',
                'davinci/html/HtmlFileXPathAdapter',
                'davinci/ve/widgets/ColorPickerFlat',
                'davinci/ve/widgets/ColorStore',
                'davinci/ve/widgets/MutableStore',
                'davinci/ve/widgets/BackgroundDialog',
                'davinci/ve/utils/CssUtils',
                'davinci/ve/Context',
                'dijit/form/Textarea',
                'davinci/ui/widgets/FileFieldDialog',
                'wcDocker/iframe',
                'xdocker/types',
                'xideve/views/BlocksFileEditor',
                'dojo/data/ItemFileWriteStore',
                'davinci/ve/commands/ModifyRuleCommand',
                'davinci/ve/widgets/DropDown',
                'davinci/ve/widgets/Background2',
                'davinci/ve/commands/ReparentCommand',
                'davinci/ve/widgets/WidgetToolBar',
                'davinci/ve/widgets/Cascade',
                'davinci/ve/widgets/CommonProperties',
                'davinci/ve/widgets/WidgetProperties',
                'davinci/ve/widgets/EventSelection',
                'davinci/ve/commands/ModifyRichTextCommand',
                'xide/mixins/ActionMixin',
                'dojo/cldr/nls/number',
                'xfile/newscene.html',
                'xideve/manager/LibraryManager',
                
                /*
                'davinci/ve/palette/Palette',
                'davinci/ve/palette/PaletteItem',
                'davinci/ve/palette/PaletteFolder',
                */


                'xgrid/ContextMenu',
                'xgrid/Noob',
                'xgrid/Toolbar',
                'xgrid/types',
                'xgrid/DnD'
            ],

            // By default, the build system will try to include dojo/main in the built dojo/dojo layer, which adds a
            // bunch of stuff we don’t want or need. We want the initial script load to be as small and quick as
            // possible, so we configure it as a custom, bootable base.
            boot: false,
            customBase: true
        }
        // In the demo application, we conditionally require app/Dialog on the client-side, so we’re building a
        // separate layer containing just that client-side code. (Practically speaking, you’d probably just want
        // to roll everything into a single layer, but I wanted to make sure to illustrate multi-layer builds.)

    },
    useSourceMaps: 1,
    cssOptimize: "",
    // Providing hints to the build system allows code to be conditionally removed on a more granular level than
    // simple module dependencies can allow. This is especially useful for creating tiny mobile builds.
    // Keep in mind that dead code removal only happens in minifiers that support it! Currently, ShrinkSafe does not
    // support dead code removal; Closure Compiler and UglifyJS do.
    staticHasFeatures: {
        // The trace & log APIs are used for debugging the loader, so we don’t need them in the build
        'dojo-trace-api': 0,
        'dojo-log-api': 0,
        'dojo-bidi': 0,
        'dojo-preload-i18n-Api': 1,
        // This causes normally private loader data to be exposed for debugging, so we don’t need that either
        'dojo-publish-privates': 0,
        // We’re fully async, so get rid of the legacy loader
        'dojo-sync-loader': 0,
        // dojo-xhr-factory relies on dojo-sync-loader
        'dojo-xhr-factory': 0,
        // We aren’t loading tests in production
        'dojo-test-sniff': 0,
        'dojo-v1x-i18n-Api': 1,

        'dojo-firebug': false,
        'tab-split': true,
        'dojo-undef-api': true,
        'xblox': true,
        'xblox-ui': true,
        'xlog': true,
        'config-stripStrict': 0,
        'xideve': true,
        'xreload': true,
        'xidebeans': true,
        'delite': true,
        'xexpression': false,
        'filtrex': true,
        'ace-formatters': true,
        'xnode-ui': true,
        'xfile': true,
        'xcf-ui': true,
        'xcf': true,
        'host-node': false,
        'xace': true,
        'drivers': true,
        'files': true,
        'protocols': false,
        'devices': true,
        'debug': false,
        'host-browser': true,
        'xaction': true,
        'electron': true,
        'FileConsole': false,
        'x-markdown': true,
        'xtrack': true,
        'nserver': true
    },

    // Resource tags are functions that provide hints to the compiler about a given file. The first argument is the
    // filename of the file, and the second argument is the module ID for the file.
    resourceTags: {
        // Files that contain test code.
        test: function (filename, mid) {
            if (filename) {
                if (
                    //filename.indexOf('tests') !==-1 ||
                    filename.indexOf('/out') !== -1 ||
                    ////filename.indexOf('xidebuild') !==-1 ||
                    filename.indexOf('serverbuild') !== -1 ||
                    filename.indexOf('node_modules') !== -1 ||
                    filename.indexOf('build/') !== -1 ||
                    filename.indexOf('/main_build') !== -1 ||
                    filename.indexOf('xapp/bootr') !== -1 ||
                    filename.indexOf('minimatch') !== -1 ||
                    filename.indexOf('xide/json/ace') !== -1 ||
                    filename.indexOf('bower_components') !== -1
                )
                    return true;
            }
        },

        // Files that should be copied as-is without being modified by the build system.
        copyOnly: function (filename, mid) {
            return copyOnly(mid);
        },

        // Files that are AMD modules.
        amd: function (filename, mid) {
            var result = /\.js$/.test(filename);
            return result;

        },

        // Files that should not be copied when the “mini” compiler flag is set to true.
        miniExclude: function (filename, mid) {
            return mid in {
                'xapp/profile': 1
            };
        }
    }
};