define([
    'dcl/dcl',
    "xblox/model/ModelBase"
], function(dcl,ModelBase){
    return dcl(ModelBase,{
        declaredClass:'xcf.model.ModelBase'
    });
});