/** @module xcf/model/Driver */
define([
    'dcl/dcl',
    'xide/data/Model',
    'xide/data/Source',
    'xide/mixins/EventedMixin',
    'xide/utils'
], function (dcl, Model, Source, EventedMixin, utils) {
    /**
     *
     * Model for a driver. It extends the base model class
     * and acts a source.
     *
     * @module xcf/model/Driver
     * @augments module:xide/mixins/EventedMixin
     * @augments module:xide/data/Model
     * @augments module:xide/data/Source
     */
    const Module = dcl([Model, Source.dcl, EventedMixin.dcl], {
        itemMetaPath: 'user.meta',
        getStore: function () {
            return this._store;
        },
        getScope: function () {
            const store = this.getStore();
            return store ? store.scope : this.scope;
        },
        /**
         * Return a value by field from the meta database
         * @param title
         * @returns {string|int|boolean|null}
         */
        getMetaValue: function (title) {
            return utils.getCIInputValueByName(this.user, title);
        },
        /**
         * Set a value in the meta database
         * @param title {string} The name of the CI
         * @returns {void|null}
         */
        setMetaValue: function (what, value, publish) {
            const item = this;
            const meta = this.user;
            const ci = utils.getCIByChainAndName(meta, 0, what);
            if (!ci) {
                return;
            }
            const oldValue = this.getMetaValue(what);
            utils.setCIValueByField(ci, 'value', value);
            this[what] = value;
            if (publish !== false) {
                this.publish(types.EVENTS.ON_CI_UPDATE, {
                    owner: this.owner,
                    ci: ci,
                    newValue: value,
                    oldValue: oldValue
                });
            }
        },
        /**
         * Return the parent folder
         * @returns {module:xcf/model/Driver}
         */
        getParent: function () {
            return this._store.getSync(this.parentId);
        },
        _mayHaveChildren:false
    });

    return Module;
});
