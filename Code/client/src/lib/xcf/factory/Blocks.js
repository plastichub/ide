define([
    'xblox/factory/Blocks',
    "xcf/model/Command",
    "xblox/model/functions/CallBlock",
    "xblox/model/functions/StopBlock",
    "xblox/model/functions/PauseBlock",
    "xide/factory"
], function (Blocks,Command,CallBlock,StopBlock,PauseBlock,factory){
    if(Blocks) {
        Blocks._getCommandBlocks = function (scope, owner, target, group) {
            const items = [];
            items.push({
                name: 'Commands',
                iconClass: 'el-icon-random',
                items: [
                    {
                        name: 'Call Command',
                        owner: owner,
                        iconClass: 'el-icon-video',
                        proto: CallBlock,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group,
                            condition: ""
                        }
                    },
                    {
                        name: 'Stop Command',
                        owner: owner,
                        iconClass: 'el-icon-video',
                        proto: StopBlock,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group,
                            condition: ""
                        }
                    },
                    {
                        name: 'Pause Command',
                        owner: owner,
                        iconClass: 'el-icon-video',
                        proto: PauseBlock,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group,
                            condition: ""
                        }
                    },
                    {
                        name: 'New Command',
                        owner: owner,
                        iconClass: 'el-icon-video',
                        proto: Command,
                        target: target,
                        ctrArgs: {
                            scope: scope,
                            group: group,
                            condition: "",
                            name: 'No-Title'
                        }
                    }
                ]
            });
            return items;
        };
        return Blocks;
    }else{
        return factory;
    }
});