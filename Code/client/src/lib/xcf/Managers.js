define([
    'dojo/_base/declare',
/***
 *  xide
 */
    'xide/manager/Context',
    'xide/manager/Application',
    'xide/manager/SettingsManager',
    'xide/manager/ManagerBase',
    'xide/manager/ServerActionBase',
    'xide/manager/Context',
    'xide/manager/PluginManager',
/***
 *  xcf
 */
    'xcf/manager/Context',
    'xcf/manager/Application',
    'xcf/manager/ProjectManager',
    'xcf/manager/DriverManager'

], function (declare) {
    return declare("xcf.Managers", null, {});
});
