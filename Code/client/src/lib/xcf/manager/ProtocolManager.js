define([
    'dcl/dcl',
    "dojo/_base/declare",
    "xide/manager/ServerActionBase",
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xaction/Action',
    'xcf/manager/BeanManager'
], function (dcl, declare, ServerActionBase, types, utils, factory, Action, BeanManager) {
    return dcl([ServerActionBase, BeanManager], {
        declaredClass: "xcf.manager.ProtocolManager",
        /***
         * The Bean-Manager needs a unique name of the bean:
         */
        beanNamespace: 'protocol',
        /***
         * The Bean-Manager has some generic function like creating Dialogs for adding new items, please
         * provide a title for the interface.
         */
        beanName: 'Protocol',
        /**
         * the icon class for bean edit views
         */
        beanIconClass: 'fa-code',
        /**
         * Bean group type
         */
        groupType: types.ITEM_TYPE.PROTOCOL_GROUP,
        /**
         * Bean item type
         */
        itemType: types.ITEM_TYPE.PROTOCOL,
        /**
         * the path to the meta data within the store item
         */
        itemMetaPath: 'user.meta',
        /**
         * the path to the meta data within the store item when saving
         */
        itemMetaStorePath: '/meta/inputs',
        /**
         * The name of the CI in the meta database for the title or name.
         */
        itemMetaTitleField: types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE,
        /**
         * Name of the system scope
         */
        systemScope: 'system_protocols',
        /**
         * Name of the user scope
         */
        userScope: 'user_protocols',
        /**
         * Name of the app scope
         */
        appScope: 'app_protocols',
        /**
         * Name of the default scope for new created items
         */
        defaultScope: 'system_protocols',
        /***
         * The RPC server class:
         */
        serviceClass: 'XCF_Protocol_Service',
        /***
         * A copy of all devices raw data from the server
         */
        rawData: null,
        /***
         * {dojo.data.ItemFileWriteStore}
         */
        store: null,
        /***
         * {xcf.views.ProtocolTreeView}
         */
        treeView: null,
        /***
         *  A map of scope names for module hot reloading
         */
        protocolScopes: null,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Bean protocol impl.
        //
        /////////////////////////////////////////////////////////////////////////////////////
        getItemActions: function () {

            const actions = [];
            const thiz = this;
            const selectedItem = this.getItem();
            const isGroup = selectedItem ? utils.toBoolean(selectedItem.isDir) === true : false;

            //New Group
            actions.push({
                title: 'New Group',
                icon: 'el-icon-folder',
                place: 'last',
                emit: false,
                style: '',
                handler: function (command, item, owner) {
                    thiz.newGroup()
                }
            });
            //reload
            actions.push({
                title: 'Reload',
                icon: 'fa-refresh',
                disabled: false,
                command: 'Reload',
                place: 'last',
                emit: false,
                style: '',
                handler: function () {
                    thiz.reload()
                }
            });

            //item actions, if
            if (this.getItem() != null) {

                //delete
                actions.push({
                    title: 'Delete',
                    icon: 'el-icon-remove-circle',
                    disabled: false,
                    command: 'Delete',
                    place: 'last',
                    emit: false,
                    style: '',
                    handler: function () {
                        thiz.onDeleteItem(selectedItem)
                    }
                });

                //new item
                if (isGroup) {
                    actions.push({
                        title: 'New Protocol',
                        icon: 'el-icon-file-new',
                        disabled: false,
                        command: 'NewItem',
                        place: 'last',
                        emit: false,
                        style: '',
                        handler: function () {
                            thiz.newItem(selectedItem)
                        }
                    });
                } else {
                    actions.push({
                        title: 'Connect',
                        icon: 'el-icon-play-circle',
                        disabled: false,
                        command: 'Start',
                        place: 'last',
                        emit: false,
                        style: '',
                        handler: function () {
                            thiz.startDevice(selectedItem)
                        }
                    });
                }
            }
            return actions;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX related functions, @TODO : move it to somewhere else
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onItemRemoved: function (item) {

            if (item == this.currentItem) {
                this.currentItem = null;
            }

            if (item) {
                const view = this.getView(item);
                if (view) {
                    utils.destroy(view);
                }
            }
            //this.reload();
        },
        _onDeleteItem: function (item) {


            const isDir = utils.toBoolean(item.isDir) === true;
            const name = utils.toString(item['name']);
            //pick the right service function
            const removeFn = isDir ? 'removeGroup' : 'removeItem';
            const thiz = this;
            const actionDialog = new ActionDialog({
                title: 'Remove Protocol' + (isDir ? ' Group' : '') + ' ' + '\'' + name + '\'',
                style: 'max-width:400px',
                titleBarClass: 'text-danger',
                delegate: {
                    isRemoving: false,
                    onOk: function (dlg) {
                        thiz[removeFn](
                            utils.toString(item.scope),
                            utils.toString(item.path),
                            function () {
                                thiz.onItemRemoved(item)
                            });
                    }
                },
                inserts: [{
                    query: '.dijitDialogPaneContent',
                    insert: '<div><span class="fileManagerDialogText">Do you really want to remove  this item' + '?</span></div>',
                    place: 'first'
                }]
            });
            actionDialog.show();
        },
        /**
         * Bean UX function to create a new item by opening a dialog, followed by a server call
         */
        newItem: function () {

            const thiz = this;
            let currentItem = this.getItem();
            const parent = currentItem ? currentItem.isDir === true ? currentItem.path : '' : '';
            if (!currentItem) {
                currentItem = {
                    path: ""
                }
            }

            const cis = [
                utils.createCI('In Group', 13, utils.toString(currentItem.path), {
                    widget: {
                        disabled: true
                    },
                    group: 'Common'
                }),
                utils.createCI('Title', 13, '', {
                    group: 'Common'
                })
            ];

            const actionDialog = new CIActionDialog({
                title: 'New Device',
                /*style: 'max-width:400px;width:400px;height:300px;',*/
                resizable: true,
                delegate: {
                    onOk: function (dlg, data) {
                        const idCi = utils.createCI('Id', 13, utils.createUUID(), {
                            visible: false,
                            group: "Common"
                        });
                        data.push(idCi);
                        const options = utils.toOptions(data);
                        const nameCi = utils.getInputCIByName(cis, "Title");
                        const scopeCi = utils.getInputCIByName(cis, "Scope");
                        const groupCi = utils.getInputCIByName(cis, "In Group");
                    }
                },
                cis: cis
            });

            actionDialog.show();
            actionDialog.resize();

        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Data related
        //
        /////////////////////////////////////////////////////////////////////////////////////
        reload: function () {},
        /***
         * Callback when the NodeJS service manager initialized its service store. That may
         * happen multiple times as user can reload the store.
         *
         * @param evt
         */
        onFileChanged: function (evt) {

            if (evt._pp2) {
                return;
            }
            evt._pp2 = true;
            const data = evt.data;

            const thiz = this;
            if (!this.fileUpdateTimes) {
                this.fileUpdateTimes = {}
            };

            if (data.event === types.EVENTS.ON_FILE_CHANGED) {
                if (data.data && data.mask && data.mask.indexOf('delete') !== -1) {
                    return;
                }
                const _path = data.path;
                const timeNow = new Date().getTime();
            }

            let path = utils.replaceAll('\\', '/', data.path);
            path = utils.replaceAll('//', '/', data.path);
            path = path.replace(/\\/g, "/");
            if (path.indexOf('protocols') == -1) {
                return;
            }
            if (path.match(/\.json$/)) {
                this.ls('system_protocols');
            }
            this['onFileChanged2'] && this['onFileChanged2'](evt);

        },
        /***
         * Common manager function, called by the context of the application
         */
        init: function () {
            this.subscribe(types.EVENTS.ON_FILE_CHANGED, this.onFileChanged);
        },
        _resetItem: function (item) {

            if (item.commandsItem) {
                utils.removeFromStore(this.getStore(), item.commandsItem, true, 'path', 'parentId');
            }

            if (item.variablesItem) {
                utils.removeFromStore(this.getStore(), item.variablesItem, true, 'path', 'parentId');
            }

        },
        _completeProtocolItem: function (item) {
            if (!item || !item.user) {
                return;
            }
            let contentCI = utils.getInputCIByName(item.user.meta, 'content');
            const thiz = this;
            const store = this.getStore();

            if (!contentCI) {

                //upgrade to file item
                item.mount = '' + item.scope;
                item.getPath = function () {
                    return item.path;
                };

                item.virtual = false;
                item.isDir = true;
                item.children = [];
                contentCI = utils.createCI('Content', types.ECIType.FILE_EDITOR, 'no value', {
                    group: 'Content',
                    editor: 'JSON Editor',
                    editorArgs: {
                        subscribers: [{
                            event: 'onSave',
                            handler: function (evt) {

                                const value = utils.getJson(evt.value);

                                if (value && value.meta) {
                                    item.user = value;
                                    item._completed = false;
                                    thiz._resetItem(item);
                                    thiz._completeProtocolItem(item);
                                    thiz.publish(types.EVENTS.ON_PROTOCOL_CHANGED, {
                                        item: item
                                    });


                                }

                            },
                            owner: thiz
                        }],
                        leftEditorArgs: {

                            subscribers: [{
                                event: 'addAction',
                                handler: function (action) {
                                    console.log('add action', arguments);
                                },
                                owner: thiz
                            }],
                            hiddenFields: {

                            },
                            readOnlyNodes: {
                                "commands": true,
                                "variables": true,
                                "meta": true
                            },
                            insertTemplates: [{
                                    label: 'New Command',
                                    path: 'commands',
                                    value: '{title:"No Title",send:""}',
                                    newNodeTemplate: '[]',
                                    collapse: true,
                                    select: true
                                },
                                {
                                    label: 'New Variable',
                                    path: 'variables',
                                    value: '{title:"No Title",value:""}',
                                    newNodeTemplate: '[]',
                                    collapse: true,
                                    select: true
                                }
                            ],
                            renderTemplates: [{
                                //
                                //  This segment is used to replace something in the node's dom structure
                                //
                                /**
                                 * @member {string} the path within the dom structure
                                 */
                                nodeValuePath: 'field.innerHTML',
                                /**
                                 * @member {RegExp|string|function|RegExp[]|string[]|function[]}
                                 */
                                match: [/^variables[\s]?\.(\d+)$/, /^commands[\s]?\.(\d+)$/],
                                /**
                                 * @member {string} the new value for the field specified in nodeValuePath
                                 */
                                replaceWith: '{nodeValue} - {title}',
                                /**
                                 * @member {object} additional variables
                                 */
                                variables: null,

                                /**
                                 * @member {function} a function to transform the node's dom value into something else
                                 */
                                nodeValueTransform: function (value) {
                                    return utils.capitalize(value);
                                },
                                //
                                //  This segment is about dom manipulation, todo!
                                //
                                /**
                                 * @type (object)
                                 */
                                insertIfMatch: {}
                            }]
                        },
                        rightEditorArgs: {

                            subscribers: [{
                                event: 'addAction',
                                handler: function (action) {
                                    console.log('add action', arguments);
                                },
                                owner: thiz
                            }]
                        }
                    },
                    editorItem: item,
                    editorOverrides: {},
                    isOwnTab: true
                });
                item.user.meta.inputs.push(contentCI);
                //append variables and commands
                if (!item._completed) {
                    item._completed = true;
                    const commands = {
                        path: utils.createUUID(),
                        name: 'Commands',
                        isDir: true,
                        type: 'leaf',
                        parentId: item.path,
                        virtual: true,
                        children: []
                    };

                    item.commandsItem = commands;
                    item.children.push(commands);
                    //complete commands
                    store.putSync(commands);

                    const commandItems = item.user.commands;
                    for (var i = 0; i < commandItems.length; i++) {
                        const command = commandItems[i];
                        commands.children.push(store.putSync({
                            path: utils.createUUID(),
                            name: command.name,
                            id: utils.createUUID(),
                            parentId: commands.path,
                            _mayHaveChildren: false,
                            virtual: true,
                            user: {},
                            isDir: false,
                            value: command.value,
                            ref: {
                                protocol: item,
                                item: command
                            },
                            type: 'protocolCommand'
                        }));
                    }

                    const variables = {
                        path: utils.createUUID(),
                        name: 'Variables',
                        isDir: true,
                        type: 'leaf',
                        parentId: item.path,
                        virtual: true,
                        items: [],
                        children: []
                    };
                    //complete commands
                    store.putSync(variables);

                    item.children.push(variables);

                    item.variablesItem = variables;

                    const variablesItems = item.user.variables;
                    for (var i = 0; i < variablesItems.length; i++) {

                        const variable = variablesItems[i];
                        variables.children.push(store.putSync({
                            isDir: false,
                            path: utils.createUUID(),
                            name: variable.name,
                            id: utils.createUUID(),
                            parentId: variables.path,
                            _mayHaveChildren: false,
                            value: variable.value,
                            virtual: true,
                            user: {},
                            ref: {
                                protocol: item,
                                item: variable
                            },
                            type: 'protocolVariable'
                        }));
                    }

                }

            }
        },

        /**
         * Protocol store is ready, extend each item with a file-editor CI (protocol content)
         * @param store
         */
        onStoreReady: function (store) {
            const items = store ? utils.queryStore(store, {
                isDir: false
            }) : [];

            const thiz = this;
            _.each(items, function (what, value, item) {
                thiz._completeProtocolItem(what);
            });
        },
        onMainViewReady: function () {

            /*
                        var thiz = this;
                        var newProtocol = Action.createDefault('New Protocol', 'fa-exchange', 'File/New/Protocol', '__anewAction', null, {
                            permanent: true,
                            handler: function () {
                                thiz.newItem();
                            }

                        }).setVisibility(types.ACTION_VISIBILITY.ACTION_TOOLBAR, null).
                        setVisibility(types.ACTION_VISIBILITY.MAIN_MENU, {}).
                        setVisibility(types.ACTION_VISIBILITY.CONTEXT_MENU, null);

                        thiz.publish(types.EVENTS.REGISTER_ACTION, {
                            owner: thiz,
                            action: newProtocol
                        });

            */
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /***
         * openItemSettings creates a new settings view for a protocol
         * @param item
         * @returns {xide.views.CIGroupedSettingsView|null}
         */
        openItemSettings: function (item) {
            //1. sanity check
            const userData = item.user;
            if (!userData || !userData.meta) {
                return null;
            }
            //2. check its not open already
            const viewId = this.getViewId(item);
            const view = registry.byId(viewId);
            try {
                if (view) {
                    if (view.parentContainer) {
                        view.parentContainer.selectChild(view);
                    }
                    return null;
                }
            } catch (e) {
                utils.destroy(view);
            }

            //3. get a view target
            const parent = this.getViewTarget();
            const title = this.getMetaValue(item, this.itemMetaTitleField);
            item.beanContextName = this.ctx.mainView.beanContextName;
            return utils.addWidget(CIGroupedSettingsView, {
                title: title || utils.toString(item.name),
                cis: userData.meta.inputs,
                beanContextName: this.ctx.mainView.beanContextName,
                storeItem: item,
                iconClass: this.beanIconClass,
                id: viewId,
                delegate: this,
                storeDelegate: this,
                closable: true,
                showAllTab: false,
                blockManager: this.ctx.getBlockManager()
            }, this, parent, true);
        }
    });
});