/** @module xcf/manager/DeviceManager_DeviceServer */
define([
    'dcl/dcl',
    'xide/encoding/MD5',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xdojo/has',
    'dojo/Deferred',
    'xide/mixins/ReloadMixin',
    'xide/mixins/EventedMixin',
    'require',
    'xide/lodash',
    'xcf/model/Variable',
    'xide/utils/HexUtils',
    'xdojo/has!host-node?nxapp/utils/_console'
], function (dcl, MD5, types, utils, factory, has, Deferred, ReloadMixin, EventedMixin, require, _, Variable, HexUtils, _console) {
    var console = typeof window !== 'undefined' ? window.console : typeof global !== 'undefined' ? global.console : _console;
    if (_console) {
        console = _console;
    }
    const isServer = has('host-node');
    const isIDE = has('xcf-ui');
    const runDrivers = true;


    /*
     var console = typeof window !== 'undefined' ? window.console : console;
     if(_console && _console.error && _console.warn){
     //console = _console;
     }
     */

    //debug mqtt activity
    const _debugMQTT = false;
    //debug device - server messages
    const debug = false;
    // debug device server connectivity
    let debugDevice = false;
    const debugStrangers = false;
    const debugConnect = false;
    const debugServerCommands = false;
    const debugCreateInstance = false;
    const debugServerMessages = false;
    const debugByteMessages = false;



    if (typeof sctx !== 'undefined' && sctx && !isServer) {

        const id = utils.createUUID();
        const rm = sctx.getResourceManager();
        const ctx = sctx;
        const application = sctx.getApplication();
        const doExport = false;
        if (!doExport) {
            return;
        }


        console.log('root : ' + ctx.getMount('root'));
        console.log('user : ' + rm.getVariable('USER_DIRECTORY'));
        console.log('system : ' + rm.getVariable('SYSTEM'));

        const ROOT = ctx.getMount('root');
        const USER = rm.getVariable('USER_DIRECTORY');
        const SYSTEM = ROOT + '/data/';
        const DIST = ROOT + '/server/nodejs/dist/';
        const TARGET = ROOT + '/mc007/';
        const linux32 = false;
        const linux64 = true;
        const osx = false;
        const arm = false;
        const windows = false;



        const options = {
            linux32: linux32,
            linux64: linux64,
            osx: osx,
            arm: arm,
            windows: windows,
            root: ROOT,
            system: SYSTEM,
            user: USER + '/',
            nodeServers: DIST,
            target: TARGET,
            debug: true,
            layout: has('debug') ? 'debug' : 'release',
            platform: rm.getVariable('PLATFORM')
        };
        console.log('options', options);
        const delegate = {
            onError: function (e) {
                console.error('have error : ', e);
            },
            onProgress: function (e) {
                console.info('have progress: ', e);
            },
            onFinish: function (e) {
                console.info('have finish: ', e);
            }
        };
        sctx.getDeviceManager().runAppServerClass('components/xideve/export/Exporter', {
            options: options
        }, delegate);


    }
    /***
     *
     * 1. startDevice
     *      ->createDriverInstance
     *          -> sendManagerCommand(CREATE_CONNECTION, cInfo);
     *              ->onDeviceConnected
     *                  serverVariables? ->
     *                      getDeviceServerVariables ->onSetDeviceServerVariables
     *
     *
     */

    /**
     *
     * @class module:xcf.manager.DeviceManager_DeviceServer
     * @augments module:xcf/manager/DeviceManager
     * @augments module:xide/mixins/EventedMixin
     */
    const Module = dcl(null, {
        declaredClass: "xcf.manager.DeviceManager_DeviceServer",
        running: null,
        /**
         * Callback when we are connected to a device.
         * We use this to fire all looping blocks
         * @param driverInstance {module:xcf/driver/DriverBase} the instance of the driver
         * @param deviceStoreItem {module:xcf/model/Device} the device model item
         * @param driver {module:xcf/model/Driver} the driver model item
         */
        onDeviceStarted: function (driverInstance, deviceStoreItem, driver) {
            if (!driverInstance || !deviceStoreItem || !driver) {
                debugConnect && console.log('onDeviceStarted failed, invalid params');
                return;
            }

            const info = this.toDeviceControlInfo(deviceStoreItem);
            const serverSide = info.serverSide;
            const _isServer = info.isServer;

            debugConnect && console.log('onDeviceStarted ' + info.toString(), driverInstance.id);

            //1. fire startup blocks
            const blockScope = driverInstance.blockScope;

            if (((isServer && (serverSide || _isServer)) || (!serverSide && !_isServer))) {
                blockScope.start();
            }
            //important to set this before publishing the connected event, otherwise it runs an infity loop
            driverInstance.__didStartBlocks = true;
            this.publish(types.EVENTS.ON_DEVICE_DRIVER_INSTANCE_READY, {
                device: deviceStoreItem,
                instance: driverInstance,
                driver: driver,
                blockScope: blockScope
            });
            this.publish(types.EVENTS.ON_DEVICE_CONNECTED, {
                device: info,
                instance: driverInstance,
                driver: driver,
                blockScope: blockScope
            });
            this.ctx.getDriverManager().addDeviceInstance(deviceStoreItem, driver);
        },
        runClass: function (_class, args, delegate) {
            this.checkDeviceServerConnection();
            if (this.deviceServerClient) {
                const id = utils.createUUID();
                args.id = id;
                const dataOut = {
                    "class": _class,
                    args: args,
                    manager_command: types.SOCKET_SERVER_COMMANDS.RUN_CLASS
                };
                !this.running && (this.running = {});
                this.running[id] = {
                    "class": _class,
                    args: args,
                    delegate: delegate
                };
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.RUN_CLASS);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        runAppServerClass: function (_class, args, delegate) {
            this.checkDeviceServerConnection();
            if (this.deviceServerClient) {
                const id = utils.createUUID();
                args.id = id;
                const dataOut = {
                    "class": _class,
                    args: args,
                    manager_command: types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_CLASS
                };
                !this.running && (this.running = {});
                this.running[id] = {
                    "class": _class,
                    args: args,
                    delegate: delegate
                };
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_CLASS);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        runAppServerClassMethod: function (_class, method, args, delegate) {
            this.checkDeviceServerConnection();
            if (this.deviceServerClient) {
                const id = utils.createUUID();
                args.id = id;
                const dataOut = {
                    "class": _class,
                    "method": _method,
                    args: args,
                    manager_command: types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_CLASS_METHOD
                };
                !this.running && (this.running = {});
                this.running[id] = {
                    "class": _class,
                    args: args,
                    delegate: delegate,
                    "method": _method
                };
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_CLASS_METHOD);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        runAppServerComponentMethod: function (component, method, args, options, delegate) {
            this.checkDeviceServerConnection();

            if (this.deviceServerClient) {
                const id = utils.createUUID();
                args.id = id;

                const dataOut = {
                    "component": component,
                    "method": method,
                    options: options,
                    args: args,
                    id: id,
                    manager_command: types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_COMPONENT_METHOD
                };
                !this.running && (this.running = {});
                this.running[id] = {
                    "component": component,
                    args: args,
                    delegate: delegate,
                    "method": method
                };
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.RUN_APP_SERVER_COMPONENT_METHOD);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        cancelAppServerComponentMethod: function (component, id) {
            this.checkDeviceServerConnection();
            if (this.deviceServerClient) {
                const dataOut = {
                    "component": component,
                    id: id,
                    manager_command: types.SOCKET_SERVER_COMMANDS.CANCEL_APP_SERVER_COMPONENT_METHOD
                };
                !this.running && (this.running = {});
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.CANCEL_APP_SERVER_COMPONENT_METHOD);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        answerAppServerComponentMethodInterrupt: function (component, id, answer) {
            this.checkDeviceServerConnection();
            if (this.deviceServerClient) {
                const dataOut = {
                    "component": component,
                    id: id,
                    manager_command: types.SOCKET_SERVER_COMMANDS.ANSWER_APP_SERVER_COMPONENT_METHOD_INTERRUPT,
                    answer: answer
                };
                !this.running && (this.running = {});
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.ANSWER_APP_SERVER_COMPONENT_METHOD_INTERRUPT);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        /**
         * Starts a device with a device store item
         * @param device
         * @param force
         */
        startDevice: function (device, force) {
            const thiz = this;
            if (!device) {
                console.error('start device invalid item');
                return null;
            }
            this.checkDeviceServerConnection();
            device.check();
            debugDevice = true;
            const dfd = new Deferred();
            if (device._startDfd && !device._startDfd.isResolved()) {
                if (!isServer) {
                    debugDevice && console.error('already starting ' + device.toString());
                    return device._startDfd;
                }
            } else {
                !device.driverInstance && device.reset(); //fresh
            }

            force === true && device.setMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED, true, false);
            const enabled = device.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED);
            if (!enabled && force !== true && !isServer) {
                debugDevice && console.error('---abort start device : device is not enabled!' + device.toString());
                this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                    text: 'Can`t start device because its not enabled! ' + device.toString(),
                    type: 'error'
                });
                setTimeout(function () {
                    dfd.reject();
                }, 10);
                return dfd;
            }

            const cInfo = this.toDeviceControlInfo(device);
            if (!cInfo) {
                console.error('invalid client info, assuming no driver found');
                dfd.reject('invalid client info, assuming no driver found');
                return dfd;
            }

            if (!cInfo) {
                console.error('couldnt start device, invalid control info ' + cInfo.toString());
                dfd.reject();
                return dfd;
            }

            const hash = cInfo.hash;
            const state = device.state;
            const wasLost = state === types.DEVICE_STATE.LOST_DEVICE_SERVER;
            if (this.deviceInstances[hash] && wasLost !== true) {
                debugDevice && console.warn('device already started : ' + cInfo.toString());
                dfd.resolve(this.deviceInstances[hash]);
                return dfd;
            }

            function buildMQTTParams(cInfo, driverInstance, deviceItem, driverItem) {
                if (!driverInstance) {
                    console.warn('buildMQTTParams:have no driver instance');
                }
                return {
                    driverScopeId: driverInstance && driverInstance.blockScope ? driverInstance.blockScope.id : 'have no driver instance',
                    driverId: driverInstance ? driverInstance.driver.id : 'have no driver id',
                    deviceId: device.path
                };
            }

            if (wasLost && this.deviceInstances[hash]) {
                thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                    text: 'Trying to re-connect to ' + cInfo.toString(),
                    type: 'info'
                });
                thiz.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.CREATE_CONNECTION, cInfo);
                device.setState(types.DEVICE_STATE.CONNECTING);
                dfd.resolve(this.deviceInstances[hash]);
                return dfd;

            }

            device.setState(types.DEVICE_STATE.CONNECTING);
            device._userStopped = null;
            device._startDfd = dfd;
            const baseDriverPrefix = this.driverScopes['system_drivers'];
            const baseDriverRequire = baseDriverPrefix + 'DriverBase';

            try {
                require([baseDriverRequire], function (baseDriver) {
                    baseDriver.prototype.declaredClass = baseDriverRequire;
                    const sub = thiz.createDriverInstance(cInfo, baseDriver, device).then(function (driverInstance) {
                        if (!driverInstance.id) {
                            driverInstance.id = utils.createUUID();
                        }
                        debugCreateInstance && console.info('created driver instance for ' + cInfo.toString());
                        cInfo.mqtt = buildMQTTParams(cInfo, driverInstance, device);
                        thiz.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                            text: 'Trying to connect to ' + cInfo.toString(),
                            type: 'info'
                        });

                        if (!isServer) {
                            if (!device.isServerSide() && !device.isServer()) {
                                thiz.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.CREATE_CONNECTION, cInfo);
                            } else {
                                thiz.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.START_DEVICE, cInfo);
                            }
                        } else {
                            //we're already starting
                            if (device._startDfd && !device._startDfd.isResolved()) {
                                thiz.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.CREATE_CONNECTION, cInfo);
                            } else {
                                thiz.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.START_DEVICE, cInfo);
                            }
                        }
                        dfd.resolve(thiz.deviceInstances[hash]);
                        delete cInfo.mqtt;
                    });
                    sub.then(function () {

                    }, function (e) {
                        debugCreateInstance && console.error('DeviceManager_DeviceServer::createDriverInstance error ', e);
                        dfd.resolve(null);
                    });
                }, function (e) {
                    console.error(e);
                });
            } catch (e) {
                logError(e, 'DeviceManager::startDevice: requiring base driver at ' + baseDriverRequire + ' failed! Base Driver - Prefix : ' + baseDriverPrefix);
            }

            return dfd;
        },
        /**
         * Creates a driver instance per device
         * @param deviceInfo {module:xide/types~DeviceInfo} The device info
         * @param driverBase {module:xcf/driver/DriverBase} The driver base class
         * @param device {module:xcf/model/Device} The device model item
         */
        createDriverInstance: function (deviceInfo, driverBase, device) {
            const hash = deviceInfo.hash;
            const driverPrefix = this.driverScopes[deviceInfo.driverScope];
            const isRequireJS = !require.cache;
            let packageUrl = require.toUrl(driverPrefix);

            packageUrl = utils.removeURLParameter(packageUrl, 'bust');

            if (isRequireJS) {
                packageUrl = packageUrl.replace('/.js', '/');
            }

            let requirePath = decodeURIComponent(packageUrl) + deviceInfo.driver;
            requirePath = requirePath.replace('', '').trim();

            function updateDevice(device, deviceInfo) {

            }

            if (isServer) {
                updateDevice(device, deviceInfo);

            }

            const thiz = this;
            const ctx = thiz.ctx;
            const meta = device['user'];
            const driverId = utils.getCIInputValueByName(meta, types.DEVICE_PROPERTY.CF_DEVICE_DRIVER);
            const driverManager = ctx.getDriverManager();
            const driver = driverManager.getDriverById(driverId);
            const dfd = new Deferred();
            const enabled = device.getMetaValue(types.DEVICE_PROPERTY.CF_DEVICE_ENABLED);
            const serverSide = deviceInfo.serverSide;

            debugCreateInstance && console.log('create driver instance : ' + device.path + ":" + device.scope + ' from ' + requirePath, {
                driver: driver,
                device: device,
                deviceInfo: deviceInfo
            });

            if (device.isEnabled() === false && !isServer) {
                debugConnect && console.warn('device not enabled, abort ' + deviceInfo.toString());
                setTimeout(function () {
                    dfd.reject('device not enabled, abort ' + deviceInfo.toString());
                });
                return dfd;
            }

            if (isServer && (!device.isServerSide() && !device.isServer() && !deviceInfo.isServer && !deviceInfo.serverSide)) {
                var e = new Error();
                dfd.reject('DeviceManager_DeviceServer: wont create driver instance! I am server and device isnt server side : ' + deviceInfo.title);
                return dfd;
            }


            debugCreateInstance && console.info('------create driver instance with DriverBase at ' + requirePath + ' with driver prefix : ' + driverPrefix, this.driverScopes);

            try {
                driverManager.getDriverModule(driver).then(function (driverModule) {
                    const baseClass = driverBase;
                    const driverProto = dcl([baseClass, EventedMixin.dcl, ReloadMixin.dcl, driverModule], {});
                    const driverInstance = new driverProto();
                    driverInstance.declaredClass = requirePath;
                    driverInstance.options = deviceInfo;
                    driverInstance.baseClass = baseClass.prototype.declaredClass;
                    driverInstance.modulePath = utils.replaceAll('//', '/', driverPrefix + deviceInfo.driver).replace('.js', '');
                    driverInstance.delegate = thiz;
                    driverInstance.driver = driver;
                    driverInstance.serverSide = deviceInfo.serverSide;
                    driverInstance.utils = utils;
                    driverInstance.types = types;
                    driverInstance.device = device;
                    driverInstance.Module = driverModule;
                    driverInstance.id = utils.createUUID();
                    driverInstance.getDevice = function () {
                        return this.device;
                    };
                    driverInstance.getDeviceInfo = function () {
                        return this.getDevice().info;
                    };
                    const meta = driver['user'];
                    const commandsCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
                    if (commandsCI && commandsCI['params']) {
                        driverInstance.sendSettings = utils.getJson(commandsCI['params']);
                    }

                    const responseCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES);
                    if (responseCI && responseCI['params']) {
                        driverInstance.responseSettings = utils.getJson(responseCI['params']);
                    }
                    try {
                        driverInstance.start();
                        driverInstance.initReload();
                    } catch (e) {
                        console.error('crash in driver instance startup! ' + device.toString());
                        logError(e, 'crash in driver instance startup!');
                    }

                    thiz.deviceInstances[hash] = driverInstance;
                    if (!driver.blox || !driver.blox.blocks) {
                        debugConnect && console.warn('Attention : INVALID driver, have no blocks', deviceInfo.toString());
                        driver.blox = {
                            blocks: []
                        };
                    }
                    device.driverInstance = driverInstance;
                    thiz.getDriverInstance(deviceInfo, true); //triggers to resolve settings
                    driverInstance._id = utils.createUUID();
                    dfd.resolve(driverInstance);
                    return driverInstance;

                });
            } catch (e) {
                console.error('DeviceManager::createDriverInstance:: requiring base driver at ' + requirePath + ' failed ' + e.message, utils.inspect(deviceInfo));
            }
            return dfd;
        },
        /**
         * Callback when server returns the variables of a device
         * @param data.device {module:xide/types~DeviceInfo}
         * @param data
         */
        onSetDeviceServerVariables: function (data) {
            const instance = this.getDriverInstance(data.device, true);
            const device = this.getDeviceStoreItem(data.device);
            if (!device) {
                debugDevice && console.log('did set device server variables failed, have no device', data);
                return;
            }

            if (!instance.blockScope) {
                const deviceInfo = device.info;
                const hash = deviceInfo.hash;
                const driver = instance.driver;
                const driverId = deviceInfo.driverId;
                const ctx = this.getContext();
                const serverSide = deviceInfo.serverSide;
                const scopeId = driverId + '_' + hash + '_' + device.path;
                if (!driver.blox || !driver.blox.blocks) {
                    debugConnect && console.warn('Attention : INVALID driver, have no blocks', deviceInfo.toString());
                    driver.blox = {
                        blocks: []
                    };
                }
                if (isServer && driver.blockPath) {
                    utils.getJson(utils.readFile(driver.blockPath));
                    driver.blox = utils.getJson(utils.readFile(driver.blockPath));
                }

                const scope = ctx.getBlockManager().createScope({
                    id: scopeId,
                    device: device,
                    driver: driver,
                    instance: instance,
                    serviceObject: this.serviceObject,
                    ctx: ctx,
                    serverSide: serverSide,
                    getContext: function () {
                        return this.instance;
                    }
                }, utils.clone(driver.blox.blocks), function (error) {
                    if (error) {
                        console.error(error + ' : in ' + driver.name + ' Resave Driver! in scope id ' + scopeId);
                    }
                });
                //important:
                instance.blockScope = scope;
                device.blockScope = scope;
                isIDE && this.completeDriverInstance(driver, instance, device);
            }

            if (instance) {
                const variables = data.variables;
                const _scope = instance.blockScope;
                device.serverVariables = data.variables;
                _.each(variables, function (variable) {
                    const _var = _scope.getVariable(variable.name);
                    if (_var) {
                        _var.value = variable.value;
                    }
                });
            }

            this.onDeviceConnected(data, false);
            device.setState(types.DEVICE_STATE.SYNCHRONIZING);
            device.setState(types.DEVICE_STATE.READY);
            device._startDfd && device._startDfd.resolve(device.driverInstance);
            delete device._userStopped;
            delete device.lastError;
            delete device._startDfd;
            device._startDfd = null;
        },
        /**
         *
         * @param device {module:xcf/model/Device}
         * @param driverInstance
         */
        getDeviceServerVariables: function (device, driverInstance) {
            const driver = driverInstance.driver;
            if (!driver.blox || !driver.blox.blocks) {
                debugConnect && console.warn('Attention : INVALID driver, have no blocks', device.toString());
                driver.blox = {
                    blocks: []
                };
            }
            const blocks = driver.blox.blocks;
            const basicVariables = [];
            _.each(blocks, function (block) {
                block.group === types.BLOCK_GROUPS.CF_DRIVER_BASIC_VARIABLES && basicVariables.push(block);
            });
            const out = [];
            for (let i = 0; i < basicVariables.length; i++) {
                out.push({
                    name: basicVariables[i].name,
                    value: basicVariables[i].value,
                    initial: basicVariables[i].value
                });
            }
            device.setState(types.DEVICE_STATE.SYNCHRONIZING);
            this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.GET_DEVICE_VARIABLES, {
                device: this.toDeviceControlInfo(device),
                variables: out
            });
        },
        /**
         *
         * @param data {Object}
         * @param data.device {module:xide/types~DeviceInfo}
         * @returns {*}
         */
        onDeviceConnected: function (data, setReadyState) {
            const deviceStoreItem = this.getDeviceStoreItem(data.device);
            if (data.isReplay && deviceStoreItem && deviceStoreItem.state === types.DEVICE_STATE.READY) {
                deviceStoreItem.setState(types.DEVICE_STATE.READY);
                return;
            }

            const instance = this.getDriverInstance(data.device, true) || data.instance;
            if (!instance) {
                debugStrangers && !isServer && console.error('--cant find device instance', this.deviceInstances);
                deviceStoreItem && deviceStoreItem.setState(types.DEVICE_STATE.DISCONNECTED);
                return;
            }
            if (!deviceStoreItem) {
                debugDevice && console.error('onDeviceConnected:: deviceStoreItem is null');
                return;
            }

            //console.error('weird '+data.device.id + ' ' + data.device.state + ' ' + deviceStoreItem.isServerSide());

            const cInfo = this.toDeviceControlInfo(deviceStoreItem);
            if (!cInfo) {
                debugDevice && console.error('onDeviceConnected:: device info  is null');
                return;
            }

            if (isServer && !cInfo.serverSide) {
                debugDevice && console.error('onDeviceConnected:: device info is not server side, abort');
            }

            if (instance && !deviceStoreItem.serverVariables) {
                deviceStoreItem.setState(types.DEVICE_STATE.CONNECTED);
                this.getDeviceServerVariables(deviceStoreItem, instance);
                return;
            }
            if (!cInfo) {
                console.error('couldnt start device, invalid control info');
                return;
            }

            const hash = cInfo.hash;

            if (this.deviceInstances[hash]) {
                if (!instance.__didStartBlocks) {
                    this.onDeviceStarted(instance, deviceStoreItem, instance.driver);
                }
                deviceStoreItem.setState(types.DEVICE_STATE.READY);
                this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                    text: 'Device is Ready <span class="text-success">' + cInfo.host + ':' + cInfo.port + '</span>',
                    type: 'success'
                });

                return this.deviceInstances[hash];
            }

            const thiz = this;
            const baseDriverPrefix = this.driverScopes['system_drivers'];
            const baseDriverRequire = baseDriverPrefix + 'DriverBase';
            //console.log('device conntected, load base driver with prefix : ' +baseDriverPrefix + ' and final require ' + baseDriverRequire);
            try {
                require([baseDriverRequire], function (baseDriver) {
                    baseDriver.prototype.declaredClass = baseDriverRequire;
                    thiz.createDriverInstance(cInfo, baseDriver, deviceStoreItem);
                });
            } catch (e) {
                console.error('requiring base driver at ' + baseDriverRequire + ' failed', e);
            }

        },
        onDeviceDisconnected: function (data) {
            if (!data && !data.device) {
                return;
            }

            const error = data.error;

            const code = error && error.code ? error.code : error || '';
            const deviceStoreItem = this.getDeviceStoreItem(data.device);
            if (!deviceStoreItem) {
                debugDevice && isIDE && console.error('deviceStoreItem is null');
                return;
            }
            if (data.stopped === true) {
                this.stopDevice(deviceStoreItem);
                return;
            }

            this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                text: 'Device has been disconnected ' + '<span class="text-warning">' + data.device.host + ':' + data.device.port + '</span>' + ' :  ' + '<span class="text-danger">' + code + '</span>',
                type: 'info'
            });

            const info = this.toDeviceControlInfo(deviceStoreItem);
            if (info && isServer && !info.serverSide) {
                return;
            }

            //kill old instance
            const instance = this.getDriverInstance(data.device, true);
            if (instance) {
                this.removeDriverInstance(data.device);
            }
            deviceStoreItem.reset();

            if (deviceStoreItem.state === types.DEVICE_STATE.DISABLED) {
                deviceStoreItem.setState(types.DEVICE_STATE.DISCONNECTED);
                return;
            }

            deviceStoreItem.setState(types.DEVICE_STATE.DISCONNECTED);
            deviceStoreItem.lastError = error;
            deviceStoreItem.refresh();

            function shouldRecconect(item) {
                if (item._userStopped || item.state === types.DEVICE_STATE.DISABLED) {
                    return false;
                }
                const enabled = thiz.getMetaValue(item, types.DEVICE_PROPERTY.CF_DEVICE_ENABLED);
                if (!enabled) {
                    return false;
                }
                return true;
            }

            const serverSide = info.serverSide;

            if ((isServer && serverSide) || (!serverSide && !isServer)) {
                if (deviceStoreItem) {
                    var thiz = this;
                    if (deviceStoreItem.reconnectTimer) {
                        return;
                    }
                    deviceStoreItem.isReconnecting = true;
                    if (!deviceStoreItem.lastReconnectTime) {
                        deviceStoreItem.lastReconnectTime = thiz.reconnectDevice;
                    } else if (deviceStoreItem.lastReconnectTime > 3600) {
                        deviceStoreItem.lastReconnectTime = 3600;
                    }
                    if (!deviceStoreItem.reconnectRetry) {
                        deviceStoreItem.reconnectRetry = 0;
                    }

                    deviceStoreItem.reconnectTimer = setTimeout(function () {
                        deviceStoreItem.reconnectTimer = null;
                        deviceStoreItem.lastReconnectTime *= 2;
                        deviceStoreItem.reconnectRetry += 1;
                        if (deviceStoreItem.shouldReconnect()) {
                            if (info) {
                                deviceStoreItem.setState(types.DEVICE_STATE.CONNECTING);
                                debugConnect && console.info('trying to reconnect to ' + info.toString());
                            }
                            thiz.startDevice(deviceStoreItem);
                        }
                    }, deviceStoreItem.lastReconnectTime);
                }
            }

        },
        onCommandFinish: function (deviceInfo, message) {
            const driverInstance = this.getDriverInstance(deviceInfo, true);
            if (!driverInstance) {
                return;
            }

            const params = message.params || {};
            if (params.src && params.id) {
                const scope = driverInstance.blockScope;
                const block = scope.getBlockById(params.src);
                if (block && block.onCommandFinish) {
                    let result = null;
                    const data = block.onCommandFinish(message);
                    try {
                        result = JSON.stringify(data, function (k, v) {
                            if (typeof v == 'object') {
                                return null;
                            }
                            return v;
                        }, 2);
                    } catch (e) {
                        logError(e, 'error serializing on command finish');
                    }
                    if (has('xcf-ui') && result) {
                        //console replay
                        const device = driverInstance.device;

                        const hash = deviceInfo.hash;
                        const viewId = hash + '-Console';
                        const messages = [result];
                        const consoleViews = this.consoles[viewId];

                        if (consoleViews) {
                            for (let h = 0; h < consoleViews.length; h++) {
                                const consoleView = consoleViews[h];
                                if (consoleView) {
                                    let split = true;
                                    let hex = false;

                                    if (consoleView.console) {
                                        const _consoleEditor = consoleView.console.getTextEditor();
                                        split = _consoleEditor.getAction("Console/Settings/Split").value;
                                        hex = _consoleEditor.getAction("Console/Settings/HEX").value;
                                    }

                                    if (!split && hex) {
                                        const hexStr = utils.bufferToHexString(deviceMessageData.data.bytes);
                                        consoleView.log(hexStr, split, false, types.LOG_OUTPUT.RESPONSE);
                                        continue;
                                    }
                                    for (let j = 0; j < messages.length; j++) {
                                        let _message = messages[j];
                                        if (_.isString(_message.string) && _message.string.length === 0) {
                                            continue;
                                        }
                                        if (hex) {
                                            _message = utils.stringToHex(_message);
                                        }
                                        consoleView.log(_message, split, true, types.LOG_OUTPUT.RESPONSE);
                                    }
                                }
                            }
                        }
                        if (messages && messages.length) {
                            this.publish(types.EVENTS.ON_DEVICE_MESSAGE_EXT, {
                                device: device,
                                deviceInfo: deviceInfo,
                                raw: result,
                                messages: messages,
                                bytes: []
                            });
                        }
                    }
                }
            }
        },
        onCommandProgress: function (deviceData, message) {
            const driverInstance = this.getDriverInstance(deviceData, true);
            if (!driverInstance) {
                return;
            }
            const params = message.params || {};
            if (params.src && params.id) {
                const scope = driverInstance.blockScope;
                if (scope) {
                    const block = scope.getBlockById(params.src);
                    if (block && block.onCommandProgress) {
                        block.onCommandProgress(message);
                    }
                } else {
                    debugServerMessages && console.warn('onCommandProgress: have no blockscope');
                }

            }
        },
        onCommandPaused: function (deviceData, message) {
            const driverInstance = this.getDriverInstance(deviceData, true);
            if (!driverInstance) {
                return;
            }
            const params = message.params || {};
            if (params.src && params.id) {
                const scope = driverInstance.blockScope;
                const block = scope.getBlockById(params.src);
                if (block && block.onCommandPaused) {
                    block.onCommandPaused(message);
                }
            }
        },
        onCommandStopped: function (deviceData, message) {
            const driverInstance = this.getDriverInstance(deviceData, true);
            if (!driverInstance) {
                return;
            }
            const params = message.params || {};
            if (params.src && params.id) {
                const scope = driverInstance.blockScope;
                const block = scope.getBlockById(params.src);
                if (block && block.onCommandStopped) {
                    block.onCommandStopped(message);
                }
            }
        },
        onCommandError: function (deviceData, message) {
            const driverInstance = this.getDriverInstance(deviceData, true);
            if (!driverInstance) {
                return;
            }
            const params = message.params || {};
            if (params.src && params.id) {
                const scope = driverInstance.blockScope;
                const block = scope.getBlockById(params.src);
                if (block && block.onCommandError) {
                    block.onCommandError(message);
                }
            }
        },
        /**
         * Primary callback when the device server has received a message from a device.
         * @param evt {object}
         * @param evt.event {string}
         * @param evt.data {string|object}
         * @param evt.data.data {object}
         */
        onDeviceServerMessage: function (evt) {
            const dataIn = evt['data'];
            let deviceMessageData = null;
            if (_.isString(dataIn) && dataIn.indexOf('{') != -1) {
                try {
                    deviceMessageData = utils.fromJson(dataIn);
                } catch (e) {
                    console.error('error parsing device message', evt);
                    return;
                }
            } else if (_.isObject(dataIn) && dataIn.data) {
                //emulated
                deviceMessageData = dataIn;
            }

            if (!deviceMessageData || !deviceMessageData.data || !deviceMessageData.data.device) {
                debug && console.error('bad device message : ', deviceMessageData);
                return;
            }
            let deviceInfo = deviceMessageData.data.device;
            if (!deviceInfo) {
                debug && console.error('onDeviceServerMessage: cant get device info');
                return;
            }
            if (isServer && !deviceInfo.serverSide) {
                return;
            }


            //pick driver instance
            const driverInstance = this.getDriverInstance(deviceMessageData.data.device, true);
            if (!driverInstance) {
                debugDevice && console.error(' onDeviceMessage : failed! Have no device instance for ' + deviceMessageData.data.device.host, deviceMessageData);
                return;
            }


            const device = this.getDeviceStoreItem(deviceInfo);

            if (!device) {
                debugStrangers && console.error('cant find device : ', deviceInfo.toString());
                return;
            }

            //important: use our info now
            deviceInfo = device.info;
            if (!deviceInfo) {
                console.error('invalid device : ' + device.toString());
            }

            const state = device.get('state');
            const serverSide = deviceInfo.serverSide;

            function clear(message) {
                delete message['resposeSettings'];
                delete message['driver'];
                delete message['lastResponse'];
                delete message['scope'];
                delete message['driverId'];
                delete message['device'];
                //delete message['src'];
                //delete message['id'];
                delete message['sourceHost'];
                delete message['sourcePort'];
            }

            const message = deviceMessageData.data.deviceMessage;
            if (message == null) {
                debugStrangers && console.warn('onDeviceServerMessage: abort, no message data');
                return;
            }
            let messages = [];
            if (_.isString(message)) {
                messages = driverInstance.split(message);
            } else if (_.isObject(message)) {
                clear(message);
                messages = [message];
            }

            const deviceMessages = messages;

            let _messages = driverInstance.onMessageRaw({
                device: deviceMessageData.data.device,
                message: message,
                bytes: deviceMessageData.data.bytes
            });

            if (_messages && !_messages.length) {
                _messages = null;
            }

            let bytes = [];

            for (let i = 0; i < messages.length; i++) {
                bytes.push(utils.stringToBuffer(messages[i]));
            }

            if (_messages && _messages.length) {
                messages = [];
                bytes = [];
                for (var j = 0; j < _messages.length; j++) {
                    const msg = _messages[j];
                    messages.push(msg.string);
                    bytes.push(msg.bytes);
                }
            }
            //replay on driver code instance
            if (messages && messages.length) {
                for (let k = 0; k < messages.length; k++) {
                    var _message = messages[k];
                    //driver replay as individual message
                    driverInstance.onMessage({
                        device: deviceMessageData.data.device,
                        message: _message,
                        raw: message,
                        bytes: bytes[k]
                    });


                    //driver replay as broadcast message
                    driverInstance.onBroadcastMessage({
                        device: deviceMessageData.data.device,
                        message: _message,
                        raw: message,
                        bytes: bytes[k]
                    });

                }
            }
            if (state !== types.DEVICE_STATE.READY) {
                device.set('state', types.DEVICE_STATE.READY);
            }
            var debugDevice = device.isDebug();
            //system replay:
            if (deviceMessageData.event) {

                //before publishing on the public system bus, we do bundle the driver instance
                //together with the origin event from the device server:
                deviceMessageData.data['driverInstance'] = driverInstance;
                //now tell everybody!
                //
                // 'deviceMessageData.event' is a system event and defined in xcf.types.EVENTS.ON_DEVICE_MESSAGE
                //
                // 'deviceMessageData.data' is the actual payload, build by the device server. it comes with this structure :
                //
                //  {
                //      device:{
                //          host:'192.168.1.20',
                //          port:'23',
                //          protocol:'tcp'
                //      },
                //      deviceMessage:'MV58\r@VOL:-220\r',
                //
                //  }
                //
                //  Current Subscribers : DriverManager & this
                //
                this.publish(deviceMessageData.event, deviceMessageData.data);
            }


            if (has('xcf-ui')) {
                //console replay
                const hash = deviceInfo.hash;

                const viewId = hash + '-Console';
                let messagesNew = [];
                const consoleViews = this.consoles[viewId];

                debug && console.log('on_device message ' + hash, driverInstance.options);
                if (debugDevice) {
                    const LOGGING_FLAGS = types.LOGGING_FLAGS;
                    const OUTPUT = types.LOG_OUTPUT;

                    let flags = deviceInfo.loggingFlags;

                    flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};
                    const flag = flags[types.LOG_OUTPUT.RESPONSE];
                    if (flag != null && (flag & LOGGING_FLAGS.STATUS_BAR)) {
                        let text = deviceMessageData.data.deviceMessage;
                        if (_.isObject(text)) {
                            clear(text);
                            try {
                                text = JSON.stringify(text);
                            } catch (e) {
                                logError(e, 'error serialize message');
                            }
                        }
                        this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                            text: "Device Message from " + driverInstance.options.host + " : " + '<span class="text-info">' + text + '</span>'
                        });
                    }
                }
                if (consoleViews) {
                    for (let h = 0; h < consoleViews.length; h++) {
                        const consoleView = consoleViews[h];
                        if (consoleView) {
                            let split = true;
                            let hex = false;

                            if (consoleView.console) {
                                const _consoleEditor = consoleView.console.getTextEditor();
                                split = _consoleEditor.getAction("Console/Settings/Split").value;
                                hex = _consoleEditor.getAction("Console/Settings/HEX").value;
                            }

                            if (!split && hex) {

                                const hexStr = utils.bufferToHexString(deviceMessageData.data.bytes);
                                consoleView.log(hexStr, split, false, types.LOG_OUTPUT.RESPONSE);
                                continue;
                            }

                            messagesNew = [];
                            if (_.isString(message)) {
                                messagesNew = split ? _messages && _messages.length ? _messages : driverInstance.split(message) : [message];
                            } else if (_.isObject(message)) {
                                clear(message);
                                messagesNew = [message];
                            }

                            for (var j = 0; j < messagesNew.length; j++) {
                                var _message = messagesNew[j];
                                if (_.isString(_message.string) && _message.string.length === 0) {
                                    continue;
                                }
                                if(!_message){
                                    continue;
                                }
                                if (hex) {
                                    _message = utils.stringToHex(_message.string || _message);
                                }
                                consoleView.log(_message, split, true, types.LOG_OUTPUT.RESPONSE);
                            }
                        }
                    }
                }
                if (messages && messages.length) {
                    this.publish(types.EVENTS.ON_DEVICE_MESSAGE_EXT, {
                        device: device,
                        deviceInfo: deviceInfo,
                        raw: message,
                        messages: messages,
                        bytes: deviceMessageData.data.bytes
                    });
                }
            }
            /****************   Forward to xblox    ******************/
            if (messages.length > 0 && driverInstance.blockScope) {
                const scope = driverInstance.blockScope;

                const responseBlocks = scope.blockStore._find({
                    group: types.BLOCK_GROUPS.CF_DRIVER_RESPONSE_BLOCKS
                });
                const responseVariables = scope.blockStore._find({
                    group: types.BLOCK_GROUPS.CF_DRIVER_RESPONSE_VARIABLES
                });

                let responseVariable = scope.getVariable('value');
                if (!responseVariable) {
                    responseVariable = new Variable({
                        id: utils.createUUID(),
                        name: 'value',
                        value: '',
                        scope: scope,
                        type: 13,
                        group: 'processVariables',
                        gui: false,
                        cmd: false
                    });
                    scope.blockStore.putSync(responseVariable);
                }
                for (let l = 0; l < messages.length; l++) {

                    if (messages[l].length === 0) {
                        continue;
                    }
                    responseVariable.value = new String(messages[l]);
                    responseVariable.value.setBytes(bytes[l]);
                    responseVariable.value.setString(messages[l]);
                    this.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, {
                        item: responseVariable,
                        scope: scope,
                        owner: this,
                        save: false, //dont save it
                        source: types.MESSAGE_SOURCE.DEVICE, //for prioritizing
                        publishMQTT: false //just for local processing
                    });
                    //now run each top-variable block in 'conditional process'
                    for (let o = 0; o < responseVariables.length; o++) {
                        var _var = responseVariables[o];
                        if (responseVariables[o].title == 'value') {
                            continue;
                        }
                        let _varResult = null;
                        let _cValue = responseVariable.value;
                        if (typeof _cValue !== "number") {
                            _cValue = '' + _cValue;
                            _cValue = "'" + _cValue + "'";
                        }

                        _varResult = _cValue;
                        if (_var.target && _var.target != 'None' && _varResult !== null && _varResult != 'null' && _varResult != "'null'") {
                            const targetVariable = scope.getVariable(_var.target);
                            if (targetVariable) {
                                targetVariable.value = _varResult;

                                this.publish(types.EVENTS.ON_DRIVER_VARIABLE_CHANGED, {
                                    item: targetVariable,
                                    scope: scope,
                                    owner: this,
                                    save: false,
                                    source: types.MESSAGE_SOURCE.BLOX //for prioritizing
                                });
                            }
                        }
                    }

                    if ((isServer && serverSide) || (!serverSide && !isServer)) {

                        for (let m = 0; m < messages.length; m++) {
                            const __message = messages[m];
                            if (_.isObject(__message)) {
                                if (__message.src) {

                                    var block = scope.getBlockById(__message.src);
                                    if (block && block.onData) {
                                        block.onData(__message);
                                    }
                                }
                            }
                        }


                        if (!runDrivers) {
                            return;
                        }
                        //now run each top-level block in 'conditional process'
                        for (let n = 0; n < responseBlocks.length; n++) {
                            var block = responseBlocks[n];
                            if (block.enabled === false) {
                                continue;
                            }
                            block.override = {
                                args: _var ? [_var.value] : null
                            };
                            try {
                                scope.solveBlock(responseBlocks[n], {
                                    highlight: isServer ? false : true
                                });
                            } catch (e) {
                                logError(e, '----solving response block crashed ');
                                debug && console.trace();
                            }
                        }
                    }
                }
            }

        },
        /**
         * Device Server management interface
         * @param cmd
         * @param data
         */
        sendManagerCommand: function (cmd, data) {
            this.checkDeviceServerConnection();
            const dataOut = {
                manager_command: cmd
            };
            utils.mixin(dataOut, data);
            if (this.deviceServerClient) {
                const res = this.deviceServerClient.emit(null, dataOut, cmd);
                debugServerCommands && console.log('send manager command ' + cmd, [dataOut, res]);
                return res;
            } else {
                console.error('Send Manager Command ' + cmd + ' failed, have no  device Server client');
                this.onHaveNoDeviceServer();
            }
        },
        /**
         *
         * @param driverInstance
         * @param data
         * @param src
         * @param id
         * @param print
         * @param wait
         * @param stop
         * @param pause
         * @param command {module:xcf/model/Command}
         * @param args {object}
         */
        sendDeviceCommand: function (driverInstance, data, src, id, print, wait, stop, pause, command, args) {
            this.checkDeviceServerConnection();
            const options = driverInstance.getDeviceInfo();
            utils.mixin({
                src: src
            }, options);

            const dataOut = {
                command: data,
                device_command: command || types.SOCKET_SERVER_COMMANDS.DEVICE_SEND,
                options: options

            };
            utils.mixin(dataOut.options, {
                params: {
                    src: src,
                    id: id,
                    wait: wait,
                    stop: stop,
                    pause: pause,
                    args: args
                }
            });

            debug && console.log("Device.Manager.Send.Message : " + dataOut.command.substr(0, 30) + ' = hex = ' + utils.stringToHex(dataOut.command) + ' l = ' + dataOut.command.length, dataOut); //sending device message
            const device = this.getDevice(options.id);
            if (!device || !_.isObject(device)) {
                console.error('invalid device');
                return;
            }

            if (device._userStopped) {
                return;
            }
            if (device && (device.state === types.DEVICE_STATE.DISABLED ||
                    device.state === types.DEVICE_STATE.DISCONNECTED ||
                    device.state === types.DEVICE_STATE.CONNECTING
                )) {
                debug && console.error('send command when disconnected');
                return;
            }

            const message = utils.stringFromDecString(dataOut.command);
            if (device.isDebug()) {
                this.publish(types.EVENTS.ON_STATUS_MESSAGE, {
                    text: "Did send message : " + '<span class="text-warnin">' + message.substr(0, 30) + '</span>' + " to " + '<span class="text-info">' + options.host + ":" + options.port + "@" + options.protocol + '</span>'
                });
            }

            //console replay
            const hash = MD5(JSON.stringify(driverInstance.options), 1);
            const viewId = hash + '-Console';
            if (this.deviceServerClient) {
                this.deviceServerClient.emit(null, dataOut, 'Device_Send');
                if (has('xcf-ui') && print !== false) {
                    const consoleViews = this.consoles[viewId];
                    _.each(consoleViews, function (view) {
                        const text = '<span class="text-info"><b>' + dataOut.command + '</span>';
                        view.printCommand(text, '');
                    });
                }
            } else {
                this.onHaveNoDeviceServer();
                console.error('this.deviceServerClient is null');
                console.error(' Send Device Command ' + data + 'failed, have no  device Server client');
            }
            if (!driverInstance.blockScope) {
                return;
            }
            var command = driverInstance.blockScope.getBlockById(src);
            this.publish(types.EVENTS.ON_DEVICE_COMMAND, {
                device: device,
                command: utils.stringFromDecString(data),
                deviceInfo: this.toDeviceControlInfo(device),
                name: command ? command.name : ""
            });
        },
        /**
         * Kick server
         * @param path {string} Absolute path to devices
         */
        loadDevices: function (path) {
            this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.INIT_DEVICES, {
                path: path
            });
        },
        /**
         *
         * @param protocol CONNECTION_PROTOCOL
         * @param method {string} The method
         * @param args {array} The arguments for the protocol method being called on the server
         * @param device {object}
         * @returns module:dojo/Deferred
         */
        protocolMethod: function (protocol, method, args, device) {
            const dfd = new Deferred();
            const event = types.SOCKET_SERVER_COMMANDS.PROTOCOL_METHOD;
            const id = utils.createUUID();
            const task = event + '_' + id;
            const data = {
                protocol: protocol,
                method: method,
                options: {
                    args: args,
                    id: id,
                    device: device
                }
            };
            const self = this;
            const handler = function (e) {
                self.unsubscribe(task);
                clearTimeout(timer);
                dfd.resolve(e);
            };
            this.subscribe(task, handler);
            try {
                this.sendManagerCommand(event, data);
                var timer = setTimeout(function () {
                    self.unsubscribe(task);
                    dfd.reject('timeout');
                    console.error('protocol method timeout');
                }, 8000);
            } catch (e) {
                dfd.reject(e);
            }
            return dfd;

        },
        /**
         *
         * @param driverInstance
         * @param method
         * @param args
         * @param src
         * @param id
         */
        callMethod: function (driverInstance, method, args, src, id) {
            this.checkDeviceServerConnection();
            const sendOptions = {
                id: id,
                src: src
            };
            const dataOut = {
                method: method,
                args: args,
                device_command: types.SOCKET_SERVER_COMMANDS.CALL_METHOD,
                params: sendOptions,
                options: driverInstance.options
            };
            if (this.deviceServerClient) {
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.CALL_METHOD);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        /**
         *
         * @param driverInstance
         * @param method
         * @param args
         * @param src
         * @param id
         */
        runShell: function (driverInstance, cmd, args, src, id) {
            const options = driverInstance.getDeviceInfo();
            this.sendManagerCommand(types.SOCKET_SERVER_COMMANDS.RUN_SHELL, {
                cmd: cmd,
                args: args,
                options: utils.mixin(options, {
                    params: {
                        id: id,
                        src: src
                    }
                })
            });
            /*
             this.checkDeviceServerConnection();
             var options = driverInstance.options,
             sendOptions = {
             id:id,
             src:src
             },
             dataOut = {
             method:method,
             args: args,
             manager_command: 'Run_Shell',
             host: options.host,
             port: options.port,
             protocol: options.protocol,
             options:sendOptions
             };
             if(this.deviceServerClient) {
             this.deviceServerClient.emit(null, dataOut, 'Run_Shell');
             }else{
             this.onHaveNoDeviceServer();
             }*/
        },
        /**
         *
         * @param path
         * @param watch
         */
        watchDirectory: function (path, watch) {
            this.checkDeviceServerConnection();
            const dataOut = {
                path: path,
                watch: watch,
                manager_command: types.SOCKET_SERVER_COMMANDS.WATCH
            };
            if (this.deviceServerClient) {
                this.deviceServerClient.emit(null, dataOut, types.SOCKET_SERVER_COMMANDS.WATCH);
            } else {
                this.onHaveNoDeviceServer();
            }
        },
        handle: function (msg) {
            const userDirectory = decodeURIComponent(this.ctx.getUserDirectory());
            const data = msg.data;
            return !(userDirectory && data && data.device && data.device.userDirectory && decodeURIComponent(data.device.userDirectory) !== userDirectory);
        },
        createDeviceServerClient: function (store) {
            const thiz = this;
            const dfd = new Deferred();
            this.deviceServerClient = null;
            this.deviceServerClient = factory.createClientWithStore(store, 'Device Control Server', {
                delegate: {
                    onConnected: function () {
                        thiz.onDeviceServerConnected();
                        dfd.resolve();
                        thiz.publish(types.EVENTS.ON_DEVICE_SERVER_CONNECTED);
                        if (isIDE) {
                            thiz.ctx.getNotificationManager().postMessage({
                                message: 'Connected to Device Server',
                                type: 'success',
                                duration: 3000
                            });
                        }
                    },
                    onLostConnection: function () {
                        thiz.onDeviceServerConnectionLost();
                    },
                    onServerResponse: function (data) {
                        const dataIn = data['data'];
                        let msg = null;
                        if (_.isString(dataIn)) {

                            try {
                                msg = JSON.parse(dataIn);
                            } catch (e) {
                                msg = dataIn;
                            }
                            debug && !msg && console.error('invalid incoming message', data);
                            msg = msg || {};

                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_FINISH) {
                                thiz.onCommandFinish(msg.data.device, msg.data.deviceMessage);
                                return;
                            }
                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_PROGRESS) {
                                thiz.onCommandProgress(msg.data.device, msg.data.deviceMessage);
                                return;
                            }
                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_PAUSED) {
                                thiz.onCommandPaused(msg.data.device, msg.data.deviceMessage);
                                return;
                            }
                            if (msg && msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_STOPPED) {
                                thiz.onCommandStopped(msg.data.device, msg.data.deviceMessage);
                                return;
                            }
                            if (msg.data && msg.data.deviceMessage && msg.data.deviceMessage.event === types.EVENTS.ON_COMMAND_ERROR) {
                                thiz.onCommandError(msg.data.device, msg.data.deviceMessage);
                                return;
                            }
                            if (msg.event === types.EVENTS.ON_DEVICE_DISCONNECTED) {
                                thiz.publish(types.EVENTS.ON_DEVICE_DISCONNECTED, msg.data);
                                return;
                            }
                            if (msg.event === types.EVENTS.SET_DEVICE_VARIABLES) {
                                return thiz.onSetDeviceServerVariables(msg.data);
                            }

                            if (msg.event === types.EVENTS.ON_RUN_CLASS_EVENT) {
                                return thiz.onRunClassEvent(msg.data);
                            }
                            if (msg.event === types.EVENTS.ON_DEVICE_CONNECTED) {
                                thiz.publish(types.EVENTS.ON_DEVICE_CONNECTED, msg.data);
                                return;
                            }
                            if (msg.event === types.SOCKET_SERVER_COMMANDS.PROTOCOL_METHOD) {
                                const event = types.SOCKET_SERVER_COMMANDS.PROTOCOL_METHOD + '_' + msg.data.options.id;
                                thiz.publish(event, msg.data);
                                return;
                            }
                            if (msg.event === types.EVENTS.ON_SERVER_LOG_MESSAGE) {
                                thiz.publish(types.EVENTS.ON_SERVER_LOG_MESSAGE, msg.data);
                                return;
                            }
                            if (msg.event === types.EVENTS.ON_MQTT_MESSAGE) {
                                thiz.publish(types.EVENTS.ON_MQTT_MESSAGE, msg.data);
                                thiz.onMQTTMessage(msg.data);
                                return;
                            }
                            if (msg.event === types.EVENTS.ON_FILE_CHANGED) {
                                return thiz.ctx.onXIDEMessage(utils.fromJson(data.data));
                            }

                            if (!thiz.handle(msg)) {
                                return;
                            }
                        }
                        thiz.onDeviceServerMessage(data);
                    }
                }
            });
            if (!this.deviceServerClient) {
                debug && console.log('couldnt connect to device server');
                return;
            } else {
                debug && console.log('did connect to device server');
            }
            this.deviceServerClient.dfd = dfd;
            return this.deviceServerClient;
        }
    });

    dcl.chainAfter(Module, 'onDeviceStarted');
    dcl.chainAfter(Module, 'onDeviceDisconnected');
    return Module;
});