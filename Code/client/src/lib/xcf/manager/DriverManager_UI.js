define([
    'dcl/dcl',
    'xide/types',
    'xide/utils',
    'xace/views/Editor',
    'xide/views/_CIDialog',
    "xcf/views/DriverView2",
    'dojo/Deferred',
    'xide/registry',
    "xdojo/has!xtrack?xide/interface/Track",
    "xide/noob",
    'xide/data/Reference',
    'xide/data/ReferenceMapping',
    "xide/interface/Route",
    "xide/mixins/ActionProvider",
    "xcf/views/DriverTreeView",
    "dojo/_base/lang"
], function (dcl, types, utils, Editor, _CIDialog, DriverView2, Deferred, registry, Track, noob, Reference, ReferenceMapping, Route, ActionProvider, DriverTreeView, lang) {

    const debug = false;
    const debugReferencing = true;

    const REFERENCE_CLASS = dcl([Reference, ReferenceMapping], {});
    let TRACKING_IMPL = null;

    if (Track) {
        TRACKING_IMPL = {
            track: function () {
                return true;
            },
            getTrackingCategory: function () {
                return utils.capitalize(this.beanNamespace);
            },
            getTrackingEvent: function () {
                return types.ACTION.OPEN;
            },
            getTrackingLabel: function (item) {
                return this.getMetaValue(item, types.DRIVER_PROPERTY.CF_DRIVER_NAME);
            },
            getActionTrackingUrl: function (command) {
                return lang.replace(
                    this.breanScheme + '?action={action}&' + this.beanUrlPattern,
                    {
                        //id: item.id,
                        action: command
                    });
            },
            getTrackingUrl: function (item) {
                return lang.replace(
                    this.breanScheme + '{view}/' + this.beanUrlPattern,
                    {
                        id: item.id,
                        view: 'settings'
                    });
            }
        };
    }

    return dcl([Route.dcl, ActionProvider.dcl, Track ? dcl(Track.dcl, TRACKING_IMPL) : noob.dcl], {
        declaredClass: "xcf.manager.DriverManager_UI",
        /**
         * Impl. router interface
         * @param {module:xide/interface/RouteDefinition} route
         */
        handleRoute: function (route) {
            switch (route.parameters.view) {
                case 'settings': {
                    return this.openItemSettings(this.getDriverById(route.parameters.id));
                }
            }
        },
        registerRoutes: function () {
            this.getContext().registerRoute({
                id: 'Drivers',
                path: 'driver://{view}/{id}',
                schemes: ['drivers'],
                delegate: this
            });
            this.getContext().registerRoute({
                id: 'New Driver',
                path: 'File/New Driver',
                schemes: [''],
                delegate: this
            });
        },
        /**
         * @param evt {object}
         * @param evt.item {object}
         * @param evt.items {array|null}
         * @param evt.store {module:xide/data/_Base}
         */
        onRenderWelcomeGridGroup: function (evt) {
            const ctx = this.getContext();
            const trackingManager = ctx.getTrackingManager();
            const router = ctx.getRouter();
            const store = evt.store;
            const self = this;
            const grid = evt.grid;

            switch (evt.item.group) {
                case 'New': {
                    const actions = [];
                    actions.push(this.createAction({
                        label: 'New Driver',
                        command: 'File/New Driver',
                        icon: 'fa-magic',
                        mixin: {
                            addPermission: true
                        }
                    }));

                    _.map(actions, function (item) {
                        return store.putSync(utils.mixin(item, {
                            url: item.command,
                            parent: 'New',
                            group: null,
                            label: item.title,
                            owner: self,
                            runAction: function (action, item, from) {
                                return DriverTreeView.prototype.runAction.apply(DriverTreeView.prototype, [{ command: this.url }, item, from]);
                            }
                        }));
                    });
                    break;
                }
                case 'Recent': {
                    if (!trackingManager) {
                        break;
                    }
                    //head
                    store.putSync({
                        group: 'Drivers',
                        label: 'Drivers',
                        url: 'Drivers',
                        parent: 'Recent',
                        directory: true
                    });

                    //collect recent
                    let devices = trackingManager.filterMin({
                        category: 'Driver'
                    }, 'counter', 1);//the '2' means here that those items had to be opened at least 2 times

                    //sort along usage
                    devices = trackingManager.most(null, devices);
                    //make last as first
                    trackingManager.last(devices);

                    //map
                    devices = _.map(devices, function (item) {
                        item.parent = 'Recent';
                        item.icon = 'fa-exchange';
                        return item;
                    });

                    //@TODO: find a way to split impl. from interface
                    const deviceActions = _.map(DriverTreeView.getActions.apply(DriverTreeView.prototype, [$('<div/>')[0], DriverTreeView.DefaultPermissions,
                        grid
                    ]), function (action) {
                        action.custom = true;
                        return action;
                    });
                    let deviceStoreItem = null;
                    _.each(devices, function (device) {
                        const route = router.match({ path: device.url });
                        if (!route) {
                            console.warn('cant get a router for ' + device.url);
                            return;
                        }
                        deviceStoreItem = self.routeToReference(route);
                        if (!deviceStoreItem) {
                            return;
                        }
                        store.removeSync(device.url, true);
                        let reference = new REFERENCE_CLASS({
                            url: device.url,
                            enabled: true,
                            label: device.label,
                            parent: 'Drivers',
                            _mayHaveChildren: false,
                            virtual: true,
                            icon: 'fa-exchange',
                            owner: self,
                            getActions: function () {
                                return deviceActions;
                            },
                            runAction: function (action, item, from) {
                                return DriverView2.prototype.runAction.apply(DriverView2.prototype, [action, item, from]);
                            }
                        });
                        reference = store.putSync(reference, false);
                        if (deviceStoreItem && deviceStoreItem.addReference) {
                            deviceStoreItem.addReference(reference, {
                                properties: {
                                    "name": true,
                                    "value": true,
                                    "state": true
                                },
                                onDelete: true,
                                onChange: true
                            }, true);
                        } else {
                            console.error('device store item has no addReference!', reference);
                        }
                    });
                    store.emit('added', { target: deviceStoreItem });
                    break;
                }
            }
        },
        /**
         * @param {module:xide/interface/RouteDefinition} route
         */
        getRouteRenderParams: function (route) {
            return {
                icon: null
            };
        },
        routeToReference: function (route) {
            return this.getDriverById(route ? route.parameters.id : null);
        },
        /**
         *
         * @param item
         * @returns {*}
         */
        referenceToRoute: function (item) {
            return lang.replace(
                this.breanScheme + '{view}/' + this.beanUrlPattern,
                {
                    id: item.id,
                    view: 'settings'
                });
        },
        ///////////////////////////////////////////////////////////////////
        /**
         *
         * @param store
         * @param device
         * @param driver
         */
        completeDriver: function (store, device, driver) {
            return;
        },
        onDriverSettingsChanged: function (ci, storeRef) {
            debugReferencing && console.log('onDriverSettingsChanged', ci);
            this.ctx.getDeviceManager().onDriverSettingsChanged(ci, storeRef);
        },
        onDriverModified: function (evt) {
            debugReferencing && console.log('on driver modified', this.getStore());
            const store = this.getStore();
            const item = evt.driver;
            if (item.commandsItem) {
                utils.removeFromStore(store, item.commandsItem, true, 'path', 'parentId');
            }
            if (item.variablesItem) {
                utils.removeFromStore(store, item.variablesItem, true, 'path', 'parentId');
            }
            item._completed = false;
            this.completeDriver(store, item, item);
        },
        openItemSettings: function (item, device, mixin, createNew) {
            const docker = this.ctx.mainView.getDocker();
            //1. sanity check
            const userData = item.user;
            if (!userData || !userData.inputs) {
                return null;
            }
            //2. check its not open already
            const viewId = this.getViewId(item);
            const view = registry.byId(viewId);
            try {
                if (view && createNew !== true) {
                    view._parent.select();
                    return null;
                }
            } catch (e) {
                utils.destroy(view);
            }

            const title = this.getMetaValue(item, this.itemMetaTitleField);
            let devinfo = null;
            const ctx = this.ctx;

            mixin = mixin || {};

            if (device) {
                devinfo = ctx.getDeviceManager().toDeviceControlInfo(device);
            }
            const parent = docker.addTab(null, {
                title: (title || item.name) + '' + (device ? ':' + device.name + ':' + devinfo.host.substr(0, 20) + ':' : ''),
                icon: this.beanIconClass
            });
            //@Todo:driver, store device temporarily in Commands CI
            const commandsCI = utils.getCIByChainAndName(userData, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
            if (commandsCI) {
                commandsCI.device = device;
            }
            if (item.blockScope && !item.blockScope.serviceObject) {
                item.blockScope.serviceObject = this.serviceObject;
            }
            //@Todo:driver, store device temporarily in Commands CI
            if (commandsCI) {
                commandsCI.device = device;
            }


            const self = this;
            const driverView = utils.addWidget(this.getViewClass(
                //tracking code
                {
                    track: function () {
                        return true;
                    },
                    getTrackingCategory: function () {
                        return utils.capitalize(self.beanNamespace);
                    },
                    getTrackingEvent: function () {
                        return types.ACTION.OPEN;
                    },
                    getTrackingLabel: function () {
                        return self.getMetaValue(this.item, types.DRIVER_PROPERTY.CF_DRIVER_NAME);
                    },
                    getActionTrackingUrl: function (command) {
                        return lang.replace(
                            self.breanScheme + '?action={action}&' + self.beanUrlPattern,
                            {
                                id: item.id,
                                action: command
                            });
                    },
                    getTrackingUrl: function (item) {
                        return lang.replace(
                            self.breanScheme + '{view}/' + self.beanUrlPattern,
                            {
                                id: item.id,
                                view: 'settings'
                            });
                    }
                }, DriverView2), utils.mixin({
                    userData: commandsCI,
                    driver: item,
                    device: device,
                    item: item,
                    ctx: ctx,
                    registerView: true,
                    id: viewId
                }, mixin), null, parent, true);

            parent.add(driverView, null, false);

            return driverView;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Server methods
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * updateItemMetaData updates a CI in the drivers meta data store
         * @param scope {string}
         * @param driverMetaPath {string}
         * @param dataPath {string} : /inputs
         * @param query
         * @param value
         * @param readyCB
         * @returns {*}
         */
        updateItemMetaData: function (scope, driverMetaPath, dataPath, query, value, readyCB) {
            return this.callMethodEx(null, 'updateItemMetaData', [scope, driverMetaPath, dataPath, query, value], readyCB, false);
        },
        /***
         * setDriverScriptContent is storing a driver's actual code in a given scope on the server
         * @param scope {string}
         * @param path  {string}
         * @param readyCB   {function}
         * @returns {*}
         */
        setDriverScriptContent: function (scope, path, content, readyCB) {
            return this.callMethod('set', [scope, path, content], readyCB, true);
        },
        /***
         * getDriverScriptContent is receiveing a driver's actual code in a given scope
         * @param scope {string}
         * @param path  {string}
         * @param readyCB   {function}
         * @returns {*}
         */
        getDriverScriptContent: function (scope, path, readyCB) {
            const _path = utils.buildPath(scope, path, false);
            return this.callMethod('get', [_path, false, false], readyCB, true);
        },
        init: function () {
            const EVENTS = types.EVENTS;
            this.subscribe([
                EVENTS.ON_CI_UPDATE,
                EVENTS.ON_WIDGET_READY,
                EVENTS.ON_MAIN_VIEW_READY,
                EVENTS.ON_SCOPE_CREATED,
                EVENTS.ON_DRIVER_MODIFIED,
                EVENTS.ON_STORE_CREATED]);
            this.subscribe([EVENTS.ON_DRIVER_GROUP_SELECTED, EVENTS.ON_DRIVER_SELECTED], this.onItemSelected);
            this.subscribe(EVENTS.ON_ACE_READY, this.onACEReady);
            this.subscribe(EVENTS.ON_RENDER_WELCOME_GRID_GROUP);
            DriverTreeView.prototype.delegate = this;
            DriverTreeView.prototype.ctx = this.getContext();
            this.registerRoutes();
        },
        onACEReady: function (evt) {
            if (evt.owner && evt.owner.userData && evt.owner.userData.widget && evt.owner.userData.widget.item) {
                const variable = evt.owner.userData.widget.item;
                const scope = variable.scope;
                let variables = scope.getVariables({
                    group: 'basicVariables'
                });
                const _responseVariables = scope.getVariables({
                    group: 'processVariables'
                });

                if (_responseVariables && _responseVariables.length) {
                    variables = variables.concat(_responseVariables);
                }
                const completors = [];
                function createCompleter(text, value, help) {
                    return {
                        word: text,
                        value: value,
                        meta: help || ''
                    };
                }
                if (variables && variables.length > 0) {
                    for (let i = 0; i < variables.length; i++) {
                        const obj = variables[i];
                        completors.push(createCompleter(obj.name, obj.value, 'Driver Variable'));
                    }
                }
                if (completors.length) {
                    evt.aceEditor.addAutoCompleter(completors);
                }
            }
        },
        onVariableAction: function (scope, driver) {
            if (driver && scope) {
                const meta = driver.user;
                //put variables from CI storage into scope
                const ci = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_VARIABLES);
                const variables = scope.variablesToJson();
                this.updateCI(ci, JSON.stringify(variables, null, 2), '', driver);
            }
        },
        reload: function () {
            const thiz = this;
            this.currentItem = null;
            this.ls('system_drivers', function () {
                if (thiz.treeView) {
                    thiz.treeView.reload(thiz.store);
                }
            });
        },
        /**
         * @param store {module:xide/data/_Base|null}
         * @param system {boolean=true}
         * @param user {boolean=true}
         * @returns {Array}
         */
        getDriversAsEnumeration: function (store, system, user) {
            try {
                const options = [];
                store = store || this.getStore() || this.store;
                const stores = utils.toArray(this.stores);
                _.each(stores, function (item) {
                    const store_name = item.name;
                    if (store_name === 'system_drivers' && system === false) {
                        return;
                    }
                    if (store_name === 'user_drivers' && user === false) {
                        return;
                    }
                    const store = item.value;
                    if (!store) {
                        console.error('getDriversAsEnumeration:: have no store!');
                        return options;
                    }

                    let items = store.query({
                        isDir: false
                    });

                    if (!_.isArray(items)) {
                        items = [items];
                    }
                    for (let i = 0; i < items.length; i++) {
                        const driver = items[i];
                        const meta = driver['user'];
                        const id = utils.getInputCIByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_ID);
                        const title = utils.getInputCIByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_NAME);
                        if (!meta) {
                            continue;
                        }
                        if (!id) {
                            console.error('have no id ci', driver);
                            continue;
                        }
                        options.push({
                            value: utils.toString(id['value']),
                            label: utils.toString(title['value']),
                            driver: driver,
                            group: utils.capitalize(store_name.replace("_drivers", ""))
                        });
                    }
                });

                return options;

            } catch (e) {
                debug && console.error('getDriversAsEnumeration failed', e);
                logError(e, 'getDriversAsEnumeration failed');
            }

        },
        saveDriver: function (storeRef) {
            debug && console.log('save driver ', storeRef);
            const meta = storeRef['user'];
            //save blocks
            const commandsCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS);
            const blocksEvent = {
                ci: commandsCI,
                newValue: '',
                oldValue: "0",
                storeItem: storeRef,
                owner: this
            };
            this.onCIUpdate(blocksEvent);
            //save response-params
            const resonseCI = utils.getCIByChainAndName(meta, 0, types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES);
            const rEvent = {
                ci: resonseCI,
                newValue: '',
                oldValue: "0",
                storeItem: storeRef,
                owner: this
            };
            const self = this;
            setTimeout(function () {
                self.onCIUpdate(rEvent);
            }, 1200);
            this.onDriverSettingsChanged(resonseCI, storeRef);
        },
        updateCI: function (ci, newValue, oldValue, storeRef) {
            if (ci && newValue !== null && storeRef) {
                if (utils.toString(ci.name) == types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS || utils.toString(ci.name) == types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES) {
                    this.onDriverSettingsChanged(ci, storeRef);
                }
                this.updateItemMetaData(
                    utils.toString(storeRef.scope), //the scope of the driver
                    utils.toString(storeRef.path),  //the relative path of the driver
                    '/inputs',                      //the path of the CIS in the meta db
                    {
                        id: utils.toString(ci.id)
                    },
                    {
                        value: newValue,
                        params: utils.toString(ci['params'])
                    }
                );
            }
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  CI Updates
        //
        /////////////////////////////////////////////////////////////////////////////////////
        onCIUpdate: function (evt) {
            const ci = evt.ci;
            const storeItem = evt.storeItem;
            if (storeItem) {
                const driver = this.getDriverById(storeItem.id);
                if (driver) {
                    this.updateCI(evt.ci, evt.newValue, evt.oldValue, driver);
                }
            }
        },
        /**
         * Callback when a CI based widget has been rendered
         * This will may be complete xide standard widgets.
         * @param evt
         */
        onWidgetReady: function (evt) {
            const thiz = this;
            /***
             * Widget completion
             */
            if (evt['owner'] == this                   // must come from us
                && evt['ci'] != null                   // we need an CI
                && evt['ci'].name === types.DRIVER_PROPERTY.CF_DRIVER_CLASS    // we need an CI
                && evt['widget'] != null               // we need a valid widget
                && evt['storeItem'] != null            // we need a store reference
                && utils.toInt(evt['ci'].type) === types.ECIType.FILE) //must be a file widget
            {
                this.onDriverClassFileWidgetCreated(evt.widget, evt.ci, evt.storeItem);
            }

            //add a scope to driver command settings widget
            if (evt['owner'] == this                   // must come from us
                && evt['ci'] != null                   // we need an CI
                && evt['ci'].name === types.DRIVER_PROPERTY.CF_DRIVER_COMMANDS    // we need an CI
                && evt['widget'] != null               // we need a valid widget
                && evt['storeItem'] != null)            // we need a store reference
            {
                //give this man a cookie!
                if (!evt['widget']['blockScope']) {
                    evt['widget']['blockScope'] = thiz.ctx.getBlockManager().getScope(utils.toString(evt['storeItem']['id']), evt);
                }
            }

            //add a scope to driver response settings widget
            if (evt['owner'] == this                   // must come from us
                && evt['ci'] != null                   // we need an CI
                && evt['ci'].name === types.DRIVER_PROPERTY.CF_DRIVER_RESPONSES    // we need an CI
                && evt['widget'] != null               // we need a valid widget
                && evt['storeItem'] != null)            // we need a store reference
            {
                const blockManager = thiz.ctx.getBlockManager();
                const scopeId = evt['storeItem']['id'];
                //give this man a cookie!
                if (!evt['widget']['blockScope']) {
                    evt['widget']['blockScope'] = thiz.ctx.getBlockManager().getScope(utils.toString(evt['storeItem']['id']), evt);
                }

            }


            //add a scope to driver variable settings widget
            if (evt['owner'] == this                   // must come from us
                && evt['ci'] != null                   // we need an CI
                && utils.toString(evt['ci'].name) === types.DRIVER_PROPERTY.CF_DRIVER_VARIABLES
                && evt['widget'] != null               // we need a valid widget
                && evt['storeItem'] != null)            // we need a store reference
            {
                //give this man a cookie!
                if (!evt['widget']['blockScope']) {
                    evt['widget']['blockScope'] = thiz.ctx.getBlockManager().getScope(utils.toString(evt['storeItem']['id']), evt);
                }
            }

            if (
                evt['ci'] != null                   // we need an CI
                && utils.toString(evt['ci'].name) === types.DEVICE_PROPERTY.CF_DEVICE_DRIVER
                && evt['widget'] != null               // we need a valid widget
                && evt['storeItem'] != null            // we need a store reference
                && utils.toInt(evt['ci'].type) === types.ECIType.ENUMERATION) //must be a file widget
            {
                //this.onDriverClassFileWidgetCreated(evt.widget,evt.ci,evt.storeItem);
            }

        },
        /***
         * Callback when a file widget has been created. We extend this widget for one more buttons:
         *
         * @param widget
         * @param ci
         * @param storeItem
         */
        onDriverClassFileWidgetCreated: function (widget, ci, storeItem) {

            /*
            var thiz = this;

            var ctx = this.ctx;
            var xFileConfig = types.config;


             var vfsConfigs = this.ctx.vfsConfigs;

             var vfsDriverConfig = factory.cloneConfig(xFileConfig);

             var xFileContext = null;

             var rootStore = xFileContext.getStore('system_drivers',
             {
             "fields": 1663,
             "includedFileExtensions": "js",
             "excludedFileExtensions": "*"
             });

             var filePanelOptions = {
             cookiePrefix: 'driver',
             sources: [
             {
             name: 'System',
             path: 'system_drivers',
             iconClass: 'fileSelectDiscIcon'
             },
             {
             name: 'User',
             path: 'user_drivers',
             iconClass: 'fileSelectDiscIcon'
             },
             {
             name: 'App',
             path: 'app_drivers',
             iconClass: 'fileSelectDiscIcon'
             }
             ],
             delegate: xFileContext.getPanelManager()
             };

             var xFileSelectDialogOptions = {
             store: rootStore,
             config: vfsDriverConfig,
             title: 'Select Driver',
             filePanelOptions: filePanelOptions
             };


             widget.options = xFileSelectDialogOptions;


             widget.debugButton = factory.createButton(
             widget.getFreeExtensionSlot(),//the widget knows where to add new items
             "el-icon-puzzle",             //eluisve icon class
             "elusiveButton",              //elusive button class adjustments
             "",                           //no label
             "",                           //no extra dom style markup
             function () {                   //button click callback
             thiz.debugDriver(ci, storeItem);
             },
             this);


             widget.editButton = factory.createButton(
             widget.getFreeExtensionSlot(),//the widget knows where to add new items
             "el-icon-file-edit",             //eluisve icon class
             "elusiveButton",              //elusive button class adjustments
             "",                           //no label
             "",                           //no extra dom style markup
             function () {                   //button click callback
             thiz.editDriver(ci, storeItem);
             },
             this);

             */
        },
        /**
         *
         * @param ci
         * @param storeItem
         */
        editDriver: function (ci, storeItem) {
            const thiz = this;
            const driver = storeItem;
            const ctx = thiz.ctx;
            const meta = driver['user'];
            const name = driver['name'];
            const driverPath = utils.getCIInputValueByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_CLASS);
            const scope = utils.toString(driver['scope']);
            const docker = ctx.mainView.getDocker();

            const where = ctx.mainView.layoutCenter;
            const title = name + '.' + utils.getFileExtension(driverPath);

            const parent = docker.addTab(null, {
                title: title,
                target: where ? where._parent : null,
                icon: 'fa-code'
            });
            try {
                const view = utils.addWidget(Editor, {
                    style: 'height:inherit',
                    ctx: this.ctx,
                    item: {},
                    /***
                     * Provide a text editor store delegate
                     */
                    storeDelegate: {
                        getContent: function (onSuccess) {
                            //1.pick up meta data
                            thiz.getDriverScriptContent(scope, driverPath, onSuccess);
                        },
                        saveContent: function (value, onSuccess, onError) {
                            //1.pick up meta data
                            thiz.setDriverScriptContent(scope, driverPath, value, onSuccess);
                        }
                    },
                    options: {
                        filePath: '.js',
                        fileName: name + '.' + utils.getFileExtension(driverPath)
                    },
                    title: name + '.' + utils.getFileExtension(driverPath),
                    fileName: driverPath

                }, this, parent, true);
                ctx.getWindowManager().registerView(view);
                view.resize();
                return view;
            } catch (e) {
                logError(e);
            }
        },
        /***
         * openDriverSettings creates a new settings view for a driver
         * @param item
         * @returns {xide.views.CIGroupedSettingsView}
         */
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX related functions, @TODO : move it to somewhere else
        //
        /////////////////////////////////////////////////////////////////////////////////////
        deleteItem: function (item) {
            //delete subs
            const store = this.getStore(item.scope) || this.store;
            const parent = item.getParent();
            store.removeSync(item.path);
            this.onDriverRemoved(store, item);
            if (parent) {
                store.refreshItem(parent);
            }
        },
        onDeleteItem: function (item) {
            debug && console.log('onDeleteItemx', item);
            const isDir = utils.toBoolean(item.isDir) === true;
            const dfd = new Deferred();
            //pick the right service function
            const removeFn = isDir ? 'removeGroup' : 'removeItem';
            const thiz = this;
            const actionDialog = new _CIDialog({
                title: 'Remove Driver' + (isDir ? ' Group' : '') + ' ' + "\"" + item.name + "\"  ",
                style: 'max-width:400px',
                titleBarClass: 'text-danger',
                onOk: function () {
                    const store = thiz.getStore(item.scope);
                    thiz[removeFn](
                        utils.toString(item.scope),
                        utils.toString(item.path),
                        utils.toString(item.name),
                        function () {
                            thiz.deleteItem(item);
                            thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                owner: thiz,
                                store: store,
                                action: types.DELETE,
                                item: item
                            });
                            store.emit(null);
                            dfd.resolve();
                        });
                }
            });

            actionDialog.show();
            return dfd;
        },
        /**
         *
         * @param item
         * @param scope
         * @param title
         * @returns {Deferred}
         */
        newGroup: function (item, scope, title) {
            const thiz = this;
            const currentItem = item;
            const parent = currentItem ? currentItem.isDir === true ? currentItem.path : '' : '';
            let store = this.getStore(scope);
            const dfd = new Deferred();

            function _createGroup(title, scope, parent) {
                thiz.createGroup(scope, parent + '/' + title, function () {
                    const newItem = thiz.createNewGroupItem(title, scope, parent);
                    store = thiz.getStore(scope);
                    const _newItem = store.putSync(newItem);
                    thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                        owner: thiz,
                        store: store,
                        action: types.NEW_DIRECTORY,
                        item: newItem
                    });
                    store.refreshItem(parent);
                    store.refreshItem(_newItem);
                    dfd.resolve(_newItem);
                });
                return dfd;
            }

            if (title && scope) {
                return _createGroup(title, scope, parent);
            }

            const actionDialog = new _CIDialog({
                title: 'New Driver Group',
                titleBarClass: 'text-info',
                size: types.DIALOG_SIZE.SIZE_SMALL,
                onOk: function () {
                    const title = this.getField('Title');
                    const scope = this.getField('Scope');
                    const _final = parent + '/' + title;
                    return _createGroup(title, scope, parent);
                    thiz.createGroup(scope, _final, function () {
                        const newItem = thiz.createNewGroupItem(title, scope, parent);
                        store = thiz.getStore(scope);
                        const _newItem = store.putSync(newItem);
                        thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                            owner: thiz,
                            store: store,
                            action: types.NEW_DIRECTORY,
                            item: newItem
                        });
                        store.refreshItem(parent);
                        store.refreshItem(_newItem);
                        dfd.resolve(_newItem);
                    });

                },
                delegate: {},
                ctx: this.ctx,
                cis: [
                    utils.createCI('Title', 13, ''),
                    utils.createCI('Scope', 3, scope || this.defaultScope, {
                        "options": [
                            {
                                label: 'System',
                                value: 'system_drivers'
                            },
                            {
                                label: 'User',
                                value: 'user_drivers'
                            }
                        ]
                    })
                ]
            });
            actionDialog.show();
            return dfd;
        },
        cloneDriver: function (driver, target) {
            const dfd = new Deferred();
            const self = this;
            const ctx = this.ctx;
            const fileManager = ctx.getFileManager();
            this.getFile(target).then(function (parentFolder) {

                self.getFile(driver).then(function (sourceDriverFile) {
                    if (sourceDriverFile) {
                        const srcParentFolder = sourceDriverFile.getParent();
                        if (srcParentFolder) {

                            const sourceFileStore = srcParentFolder._store;
                            const targetFileStore = parentFolder._store;

                            //neighbour items
                            const items = targetFileStore.getChildren(parentFolder);

                            let newName = fileManager.getNewName(sourceDriverFile, parentFolder.children);
                            newName = newName.replace('.meta.json', '');


                            function proceed(newName) {
                                fileManager.getContent(sourceDriverFile.mount, sourceDriverFile.path, function (content) {
                                    // update meta data
                                    const cis = utils.getJson(content);
                                    const DRIVER_PROPERTY = types.DRIVER_PROPERTY;
                                    const meta = cis['inputs'];

                                    const idCI = utils.getCIByChainAndName(meta, 0, DRIVER_PROPERTY.CF_DRIVER_ID);
                                    utils.setCIValueByField(idCI, 'value', utils.createUUID());
                                    const titleCI = utils.getCIByChainAndName(meta, 0, DRIVER_PROPERTY.CF_DRIVER_NAME);
                                    utils.setCIValueByField(titleCI, 'value', newName);

                                    const driverClass = utils.getCIByChainAndName(meta, 0, DRIVER_PROPERTY.CF_DRIVER_CLASS);
                                    utils.setCIValueByField(driverClass, 'value', parentFolder.path + '/' + newName + '.js');

                                    //store it as new file
                                    const newPath = parentFolder.path + '/' + newName + '.meta.json';

                                    const sourceScriptPath = sourceDriverFile.path.replace('.meta.json', '.js');
                                    const targetScriptPath = parentFolder.path + '/' + newName + '.js';

                                    const sourceBloxFilePath = sourceDriverFile.path.replace('.meta.json', '.xblox');
                                    const targetBloxFilePath = parentFolder.path + '/' + newName + '.xblox';
                                    const targetScriptFilePath = parentFolder.path + '/' + newName + '.js';

                                    //xblox
                                    fileManager.getContent(sourceDriverFile.mount, sourceBloxFilePath, function (content) {
                                        fileManager.mkfile(parentFolder.mount, targetBloxFilePath).then(function () {
                                            fileManager.setContent(parentFolder.mount, targetBloxFilePath, content, function () {

                                                //script
                                                fileManager.getContent(sourceDriverFile.mount, sourceScriptPath, function (content) {
                                                    fileManager.mkfile(parentFolder.mount, targetScriptPath).then(function () {
                                                        fileManager.setContent(parentFolder.mount, targetScriptPath, content, function () {

                                                            //meta
                                                            fileManager.mkfile(parentFolder.mount, newPath).then(function () {
                                                                fileManager.setContent(parentFolder.mount, newPath, JSON.stringify(cis, null, 2), function () {



                                                                    //create a temporary store and move it to the originating
                                                                    self.ls(target.scope, false).then(function (store) {

                                                                        if (!store) {
                                                                            return;
                                                                        }

                                                                        let _driver = store.query({
                                                                            id: idCI.value
                                                                        })[0];

                                                                        if (_driver) {



                                                                            //remove it from temp store
                                                                            store.removeSync(_driver.path);

                                                                            //remove it from target store (wtf?)
                                                                            target._store.removeSync(_driver.path);

                                                                            //set to the target's store
                                                                            _driver._store = target._store;

                                                                            //add it to the target store

                                                                            _driver = target._store.addSync(_driver);


                                                                            //trigger some events
                                                                            _driver.refresh();

                                                                            target._store.emit('added', {
                                                                                target: _driver
                                                                            });


                                                                            store = target._store;

                                                                            self.onDriverCreated(store);

                                                                            self.completeDriver(store, _driver, _driver);

                                                                            self.publish(types.EVENTS.ON_STORE_CHANGED, {
                                                                                owner: self,
                                                                                store: store,
                                                                                item: _driver
                                                                            });

                                                                            dfd.resolve(_driver);
                                                                        }
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });

                                }, false);
                            }

                            const cis = [
                                utils.createCI('Title', 13, newName, {
                                    group: 'Common'
                                })
                            ];

                            const useDialog = true;
                            if (useDialog) {
                                const actionDialog = new _CIDialog({
                                    title: 'New Name',
                                    resizable: true,
                                    onOk: function (data) {
                                        const options = utils.toOptions(data);
                                        proceed(utils.getInputCIByName(cis, "Title").value);
                                    },
                                    delegate: {},
                                    cis: cis
                                });
                                actionDialog.show();
                            } else {
                                proceed(newName);
                            }
                        }
                    } else {
                        dfd.reject("Cant get file object for device");
                    }
                });
            });
            return dfd;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Bean protocol impl.
        //
        /////////////////////////////////////////////////////////////////////////////////////
        /**
         * Bean UX function to create a new item by opening a dialog, followed by a server call
         */
        newItem: function (currentItem, title) {
            const dfd = new Deferred();
            if (!currentItem) {
                currentItem = {
                    path: ""
                };
            }
            let parent = currentItem ? currentItem.isDir === true ? currentItem.path : '' : '';
            const scope = currentItem.scope || 'user_drivers';
            const templateUrlMeta = require.toUrl('system_drivers/Default.meta.json');
            const meta = utils.getJson(this._getText(templateUrlMeta));
            const templateUrlDriverCode = require.toUrl('system_drivers/Default.js');
            const driverCode = this._getText(templateUrlDriverCode);
            const nameCi = utils.getInputCIByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_NAME);
            const idCi = utils.getInputCIByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_ID);
            const scriptPathCi = utils.getInputCIByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_CLASS);
            const store = this.getStore(scope);
            const thiz = this;

            idCi.value = utils.createUUID();
            nameCi.value = title || 'My New Driver';
            function _createDriver(title, parent, scope) {
                if (title !== 'Default') {
                    try {
                        scriptPathCi.value = './' + parent + '/' + title + '.js';
                        const metaOut = JSON.stringify(meta, null, 2);
                        if (parent.length == 0) {
                            parent = "/";
                        }
                        try {
                            thiz.createItem(scope, parent, title, metaOut, driverCode).then(function (data) {
                                let newItem = thiz.createNewItem(nameCi.value, scope, parent);
                                newItem.path += '.meta.json';
                                newItem.user = meta;
                                newItem.id = idCi.value;
                                newItem.blox = {
                                    blocks: [],
                                    variables: []
                                };
                                newItem = store.putSync(newItem);
                                thiz.onDriverCreated(store);
                                thiz.completeDriver(store, newItem, newItem);
                                thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                    owner: thiz,
                                    store: store,
                                    action: types.NEW_FILE,
                                    item: newItem
                                });
                                dfd.resolve(newItem);
                            });
                        } catch (e) {
                            logError(e);
                        }
                    } catch (e) {
                        logError(e, 'error in CIDialog');
                    }
                    return dfd;
                }
            }

            if (currentItem && title) {
                return _createDriver(title, parent, scope);
            }

            const actionDialog = new _CIDialog({
                title: 'New Driver',
                resizable: true,
                titleBarClass: 'text-info',
                onCancel: function () {
                    dfd.resolve();
                },
                onOk: function (cis) {
                    const scopeCi = utils.getInputCIByName(cis, 'Scope');
                    return _createDriver(nameCi.value, parent, scopeCi.value);

                    if (nameCi.value !== 'Default') {
                        try {
                            scriptPathCi.value = './' + parent + '/' + nameCi.value + '.js';
                            const metaOut = JSON.stringify(meta, null, 2);
                            if (parent.length == 0) {
                                parent = "/";
                            }
                            try {
                                thiz.createItem(scope, parent, nameCi.value, metaOut, driverCode).then(function (data) {
                                    let newItem = thiz.createNewItem(nameCi.value, scope, parent);
                                    newItem.path += '.meta.json';
                                    newItem.user = meta;
                                    newItem.id = idCi.value;
                                    newItem.blox = {
                                        blocks: [],
                                        variables: []
                                    };
                                    newItem = store.putSync(newItem);
                                    thiz.onDriverCreated(store);
                                    thiz.completeDriver(store, newItem, newItem);
                                    thiz.publish(types.EVENTS.ON_STORE_CHANGED, {
                                        owner: thiz,
                                        store: store,
                                        action: types.NEW_FILE,
                                        item: newItem
                                    });
                                    dfd.resolve(newItem);
                                });
                            } catch (e) {
                                logError(e);
                            }
                        } catch (e) {
                            logError(e, 'error in CIDialog');
                        }
                    }
                },
                cis: [
                    nameCi,
                    utils.createCI('Scope', 3, scope, {
                        group: 'General',
                        value: scope,
                        options: [
                            {
                                label: 'System',
                                value: 'system_drivers'
                            },
                            {
                                label: 'User',
                                value: 'user_drivers'
                            }
                        ]
                    })
                ]
            });
            actionDialog.show();
            return dfd;
        }
    });
});
