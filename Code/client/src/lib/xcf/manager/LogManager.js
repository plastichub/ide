define([
    'dcl/dcl',
    'xlog/manager/LogManager'
], function (dcl, LogManager) {
    return dcl(LogManager, {
        declaredClass: "xcf.manager.LogManager",
        ls: function (readyCB) {
            const thiz = this;
            return this.callMethodEx(null, 'ls', ['unset'], function (data) {
                //keep a copy
                thiz.rawData = data;
                thiz.initStore(data);
                if (readyCB) {
                    readyCB(data);
                }
            }, true);
        }
    });
});