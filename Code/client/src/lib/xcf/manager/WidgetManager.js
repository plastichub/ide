define([
    'dcl/dcl',
    "dojo/_base/declare",
    "dojo/_base/lang",
    'dojo/dom-construct',
    'xide/mixins/ReloadMixin',
    'xide/types',
    'xide/utils',
    'xide/factory',
    'xwire/Binding',
    'xwire/EventSource',
    'xwire/WidgetTarget',
    'xwire/WidgetSource',
    'xwire/DeviceTarget',
    'dojo/has!xideve?xideve/manager/WidgetManager'
], function (dcl, declare, lang, domConstruct, ReloadMixin, types, utils, factory, Binding, EventSource, WidgetTarget, WidgetSource, DeviceTarget, WidgetManager) {
    return dcl([WidgetManager, ReloadMixin.dcl], {
        declaredClass: "xcf/manager/WidgetManager",
        instances: null,
        _cbinding: null,
        options: null,
        _doOpenRight: false,
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Public API
        //
        /////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Storage & Persistence
        //
        /////////////////////////////////////////////////////////////////////////////////////

        /**
         *
         */
        onReloaded: function () {
            //console.error('reloaded!');

            /*this.init();*/
            this._createBinding();


            /*
             var _data = new dojox.wire.ml.Data();
             _data.startup();
             _data.setPropertyValue('tempData','');
             _data.setPropertyValue('value','value');

             var _action = new dojox.wire.ml.Action({
             trigger:"inputField",
             triggerEvent:"onChange"
             });
             _action.startup();

             var _transfer = new dojox.wire.ml.Transfer({
             source:"inputField.value",
             target:"data.tempData"
             });
             _transfer.startup();
             */


            /*
             <div dojoType="dojox.wire.ml.Action"
             id="action1"
             trigger="inputField"
             triggerEvent="onChange">
             <div dojoType="dojox.wire.ml.Transfer" source="inputField.value" target="data.tempData"></div>
             <div dojoType="dojox.wire.ml.Invocation" id="targetCopy" object="targetField1"  method="set" parameters="data.value, data.tempData"></div>
             </div>
             */


            /*
             <div dojoType="dojox.wire.ml.Data"
             id="data">
             <div dojoType="dojox.wire.ml.DataProperty"
             name="tempData"
             value="">
             </div>
             <div dojoType="dojox.wire.ml.DataProperty"
             name="value"
             value="value">
             </div>
             </div>
             */
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  UX factory and utils
        //
        /////////////////////////////////////////////////////////////////////////////////////
        makeDeviceBinding: function () {
            const source = {};
        },
        _createBinding: function () {
            return;
            if (!this._cbinding) {
                this._cbinding = [];
            }

            const editorDijit = this.currentContext.getDijit();
            const editorRegistry = editorDijit.registry;
            const slider = editorRegistry.byId('VolumeSlider');

            //destroy old bindings
            for (let i = 0; i < this._cbinding.length; i++) {
                this._cbinding[i].destroy();
            }

            this._cbinding = [];
            /***
             * Device Variable to Widget
             * @type {xwire.EventSource}
             */
            //wire to a standard event
            let bindingSource = new EventSource({
                //listen to variable changes
                trigger: types.EVENTS.ON_DRIVER_VARIABLE_CHANGED,
                //the path to value
                path: 'item.value',
                //add an event filter
                filters: [{
                    path: 'item.title',  // variable title must match :
                    value: 'Volume'  // 'value'
                }]
            });
            //we map the event source to a widget
            let bindingTarget = new WidgetTarget({
                //the path to value
                path: 'value',
                object: slider
            });
            //construct the binding
            let binding = new Binding({
                source: bindingSource,
                target: bindingTarget
            });
            binding.start();

            this._cbinding.push(binding);
            ////////////////////////////////////////////////////////////////
            //
            //  widget to device
            //
            ////////////////////////////////////////////////////////////////
            const deviceData = this.instances[0];
            /**
             * Widget to Device
             * @type {binding|*}
             * @private
             */
            bindingSource = new WidgetSource({
                trigger: 'onChange',
                object: slider
            });

            //we map the event source to a widget
            bindingTarget = new DeviceTarget({
                object: deviceData.instance,
                command: 'SetMasterVolume',
                variable: 'Volume'
            });
            //construct the binding
            binding = new Binding({
                source: bindingSource,
                target: bindingTarget
            });
            binding.start();
            //track instance
            this._cbinding.push(binding);
        },
        /**
         * Callback when a device driver instance is complete and ready!
         * @param evt
         */
        onDeviceReady: function (evt) {
            this.instances.push(evt);
        },
        getXFileOptions: function (extensions) {
            const thiz = this;

            /***
             * Change file picker options
             */
            const xFileContext = window['xFileContext'];
            const xFileConfig = xFileContext.config;

            //var vfsConfigs = this.ctx.vfsConfigs;
            const vfsDriverConfig = factory.cloneConfig(xFileConfig);
            const rootStore = xFileContext.getStore('workspace', {
                "fields": 1663,
                "includedFileExtensions": extensions || "*",
                "excludedFileExtensions": "*"
            });

            const filePanelOptions = {
                cookiePrefix: 'driver',
                sources: [
                    {
                        name: 'System',
                        path: 'system_drivers',
                        iconClass: 'fileSelectDiscIcon'
                    },
                    {
                        name: 'User',
                        path: 'user_drivers',
                        iconClass: 'fileSelectDiscIcon'
                    },
                    {
                        name: 'App',
                        path: 'app_drivers',
                        iconClass: 'fileSelectDiscIcon'
                    }
                ],
                delegate: xFileContext.getPanelManager()
            };

            return {
                store: rootStore,
                config: vfsDriverConfig,
                title: 'Select Driver',
                filePanelOptions: filePanelOptions
            };
        },
        onFileSelected: function (dlg, item) {
            dlg.custom.prop.value = utils.buildPath(item.mount, item.path, false);
            dlg.custom.prop.owner._onChange({target: dlg.custom.prop.row});
        },
        onSelectFile: function (node, prop) {
            const thiz = this;
            const ctx = window['xFileContext'];
            let _defaultOptions = {
                title: 'Select Script',
                owner: window['xFileContext'],
                dst: {
                    name: 'Select',
                    path: '.'
                },
                src: '.',
                ctx: window['xFileContext'],
                custom: {
                    node: node,
                    prop: prop
                }
            };

            let extensions = "*";
            if (prop.data && prop.data.custom && prop.data.custom.extensions) {
                extensions = prop.data.custom.extensions;
            }

            const xFileOptions = this.getXFileOptions(extensions);
            if (this.options) {
                _defaultOptions = lang.mixin(_defaultOptions, xFileOptions);
            }
            _defaultOptions.store = xFileOptions.store;
            _defaultOptions.config = xFileOptions.config;
            _defaultOptions.filePanelOptions = xFileOptions.filePanelOptions;
            _defaultOptions.defaultStoreName = 'workspace';
            _defaultOptions.defaultStoreOptions = {
                "fields": 1663,
                "includedFileExtensions": extensions || "*",
                "excludedFileExtensions": "*"
            };
            ctx.defaultOptions = _defaultOptions;
            const dlg = factory.createFileSelectDialogLarge(_defaultOptions,
                function (dlg, item) {
                    thiz.onFileSelected(dlg, item);
                });
        },
        /**
         * _addXAppApplication does a require of some xapp modules
         * @param context
         * @private
         */
        _addXAppApplication: function (context) {
            /*const global = context.getGlobal();
            const _require = global["require"];
            const _t = _require.toUrl('xapp/manager/Application');*/
        },
        onShowBlockSmartInput: function (evt) {
            evt.input.ctx = this.ctx;
        },
        /////////////////////////////////////////////////////////////////////////////////////
        //
        //  Main entries, called by the context. 'Managers' must have always an init function
        //
        /////////////////////////////////////////////////////////////////////////////////////
        init: function () {
            this.instances = [];
            this.subscribe(types.EVENTS.ON_DEVICE_DRIVER_INSTANCE_READY, this.onDeviceReady);
            this.subscribe(types.EVENTS.ON_SHOW_BLOCK_SMART_INPUT, this.onShowBlockSmartInput);
        }
    });
});

