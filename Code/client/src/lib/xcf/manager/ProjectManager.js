define([
    'dcl/dcl',
    'xide/manager/ManagerBase'
], function (dcl,ManagerBase){
    return dcl(ManagerBase,{
        declaredClass:"xcf.manager.ProjectManager"
    });
});

