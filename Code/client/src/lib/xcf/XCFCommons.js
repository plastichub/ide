define([
    'xide/utils',
    'xide/utils/StringUtils',
    'xide/utils/HexUtils',
    'xide/utils/HTMLUtils',
    'xide/utils/StoreUtils',
    'xide/utils/WidgetUtils',
    'xide/utils/CIUtils',
    'xide/utils/ObjectUtils',
    'xide/utils/CSSUtils',
    /***
     * XFILE
     */

    'xfile/types',
    /***
     * XIDE
     */

    'xide/types',
    'xide/types/Types',


    'xide/factory',
    'xide/factory/Objects',
    'xide/factory/Events',
    'xide/factory/Clients',

    /***
     * XCF
     */
    'xcf/model/ModelBase',
    'xcf/model/Command',
    'xcf/model/Variable',
    'xcf/factory/Blocks',
    'xcf/types',
    'xcf/types/Types',
    /**
     * XBLOX
     */
    'xblox/types/Types',
    'xaction/types',
    'xconsole/types'

], function (utils) {
    if (!Array.prototype.forEach) {

        Array.prototype.forEach = function (callback /*, thisArg*/ ) {

            var T, k;

            if (this == null) {
                throw new TypeError('this is null or not defined');
            }

            // 1. Let O be the result of calling toObject() passing the
            // |this| value as the argument.
            var O = Object(this);

            // 2. Let lenValue be the result of calling the Get() internal
            // method of O with the argument "length".
            // 3. Let len be toUint32(lenValue).
            var len = O.length >>> 0;

            // 4. If isCallable(callback) is false, throw a TypeError exception. 
            // See: http://es5.github.com/#x9.11
            if (typeof callback !== 'function') {
                throw new TypeError(callback + ' is not a function');
            }

            // 5. If thisArg was supplied, let T be thisArg; else let
            // T be undefined.
            if (arguments.length > 1) {
                T = arguments[1];
            }

            // 6. Let k be 0.
            k = 0;

            // 7. Repeat while k < len.
            while (k < len) {

                var kValue;

                // a. Let Pk be ToString(k).
                //    This is implicit for LHS operands of the in operator.
                // b. Let kPresent be the result of calling the HasProperty
                //    internal method of O with argument Pk.
                //    This step can be combined with c.
                // c. If kPresent is true, then
                if (k in O) {

                    // i. Let kValue be the result of calling the Get internal
                    // method of O with argument Pk.
                    kValue = O[k];

                    // ii. Call the Call internal method of callback with T as
                    // the this value and argument list containing kValue, k, and O.
                    callback.call(T, kValue, k, O);
                }
                // d. Increase k by 1.
                k++;
            }
            // 8. return undefined.
        };
    }

    if (!Array.prototype.shuffle) {
        Array.prototype.shuffle = function () {
            let counter = this.length,
                temp, index;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = (Math.random() * counter--) | 0;
                // And swap the last element with it
                temp = this[counter];
                this[counter] = this[index];
                this[index] = temp;
            }
        };
    }
    if (!Array.prototype.remove) {
        Array.prototype.remove = function () {
            let what;
            const a = arguments;
            let L = a.length;
            let ax;
            while (L && this.length) {
                what = a[--L];
                if (this.indexOf == null) {
                    break;
                }
                while ((ax = this.indexOf(what)) != -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };
    }
    Array.prototype.swap = function (x, y) {
        const b = this[x];
        this[x] = this[y];
        this[y] = b;
        return this;
    };

    if (!Array.prototype.indexOfPropertyValue) {
        Array.prototype.indexOfPropertyValue = function (prop, value) {
            for (let index = 0; index < this.length; index++) {
                if (this[index][prop]) {
                    if (this[index][prop] == value) {
                        return index;
                    }
                }
            }
            return -1;
        };
    }

    if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (str) {
            return this.substring(0, str.length) === str;
        };
    }

    if (typeof String.prototype.endsWith != 'function') {
        String.prototype.endsWith = function (str) {
            return this.substring(this.length - str.length, this.length) === str;
        };
    }



    // Cheap polyfill to approximate bind(), make Safari happy
    Function.prototype.bind = Function.prototype.bind || function (that) {
        return dojo.hitch(that, this);
    };
    window['xlog'] = function (what) {
        try {
            console.log(what);
        } catch (e) {}
    };
    window['xwarn'] = console.warn;
    window['xerror'] = console.error;
    window['xtrace'] = console.trace;
});